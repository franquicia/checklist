package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.PreguntaDTO;

public interface PreguntaTDAO {
	
	public List<PreguntaDTO> obtienePreguntaTemp(int idPregunta) throws Exception;
	
	public int insertaPreguntaTemp(PreguntaDTO bean) throws Exception;
	
	public boolean actualizaPreguntaTemp(PreguntaDTO bean) throws Exception;
	
	public boolean eliminaPreguntaTemp(int idPregunta) throws Exception;

}
