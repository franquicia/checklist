package com.gruposalinas.checklist.dao;

import java.util.Map;

public interface VisitaTiendaDAO {

	public Map<String, Object>  obtieneVisitas(String idChecklist, String idUsuario, String nuMes, String anio) throws Exception;
}
