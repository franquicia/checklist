package com.gruposalinas.checklist.domain;

public class SucursalHistDTO {
	private int idHistoricoSuc;
	private int idPais;
	private String numSucursal;
	private String nombreCC;
	private String longitud;
	private String latitud;
	private String fecha;
	private int tipoSucursal;
	
	public int getIdHistoricoSuc() {
		return idHistoricoSuc;
	}
	public void setIdHistoricoSuc(int idHistoricoSuc) {
		this.idHistoricoSuc = idHistoricoSuc;
	}
	public int getIdPais() {
		return idPais;
	}
	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}
	public String getNumSucursal() {
		return numSucursal;
	}
	public void setNumSucursal(String numSucursal) {
		this.numSucursal = numSucursal;
	}
	public String getNombreCC() {
		return nombreCC;
	}
	public void setNombreCC(String nombreCC) {
		this.nombreCC = nombreCC;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getTipoSucursal() {
		return tipoSucursal;
	}
	public void setTipoSucursal(int tipoSucursal) {
		this.tipoSucursal = tipoSucursal;
	}
	
	
	

}
