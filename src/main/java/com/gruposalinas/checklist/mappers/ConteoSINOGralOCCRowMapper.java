package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;

public class ConteoSINOGralOCCRowMapper implements RowMapper<ChecklistPreguntasComDTO> {

	public ChecklistPreguntasComDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistPreguntasComDTO con = new ChecklistPreguntasComDTO();

	    con.setClasif(rs.getString("FCCLASIFICA"));
		con.setIdBitaGral(rs.getInt("FIID_BITGRAL"));
		con.setTotPreg(rs.getInt("TOTALGRAL"));
		con.setSumGrupo(rs.getInt("SUMA_GRUPO"));
	    con.setPregSi(rs.getInt("SUMA_SI"));
	    //con.setPregNo(rs.getInt("RESP_NO"));
		con.setPregNa(rs.getInt("SUMA_NA"));
		//con.setPonTot(rs.getDouble("FCPONDTOTAL"));
		//con.setPrecalif(rs.getDouble("PRECALIFICACION"));
		//con.setCalif(rs.getDouble("CALIFICACION"));
		//con.setAux2(rs.getInt("IMPERDONABLE"));
		//con.setTotGral(rs.getDouble("TOTALGRAL"));
		
		
		return con;
	}
	
}
