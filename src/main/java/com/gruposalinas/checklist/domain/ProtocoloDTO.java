package com.gruposalinas.checklist.domain;

public class ProtocoloDTO {
	private int idProtocolo;
	private String descripcion;
	private String observaciones;
	private int status;
	private int statusRetro;
	
	public int getIdProtocolo() {
		return idProtocolo;
	}
	public void setIdProtocolo(int idProtocolo) {
		this.idProtocolo = idProtocolo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public int getStatus() {
		return status;
	
	}
	public void setStatus(int status) {
		this.status = status;
		
	}
	
	public int getStatusRetro() {
		return statusRetro;
	}
	public void setStatusRetro(int statusRetro) {
		this.statusRetro = statusRetro;
	}
	@Override
	public String toString() {
		return "ProtocoloDTO [idProtocolo=" + idProtocolo + ", descripcion=" + descripcion + ", observaciones=" + observaciones +",status="+ status +",statusRetro="+ statusRetro + " ]";
	}
	
}
