package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ConsultaDashBoardDTO;
import com.gruposalinas.checklist.mappers.ConsultaDashBoardRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ConsultaDashBoardDAOImpl extends DefaultDAO implements ConsultaDashBoardDAO {

    private Logger logger = LogManager.getLogger(ConsultaDashBoardDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneDashBoard;

    public void init() {
        jdbcObtieneDashBoard = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE_SUPRV")
                .withProcedureName("SPGETDASHBOARD")
                .returningResultSet("PA_CONSULTA", new ConsultaDashBoardRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaDashBoardDTO> obtieneDashBoard(ConsultaDashBoardDTO bean) throws Exception {
        Map<String, Object> out = null;
        List<ConsultaDashBoardDTO> listaCheck = null;

        int error = 0;
        int res1 = 0;
        int res2 = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_USUARIO", bean.getIdUsuario());

        out = jdbcObtieneDashBoard.execute(in);

        //logger.info("Funcion ejecutada:{checklist.PAREPORTE_SUPRV.PA_FIID_USUARIO}");
        listaCheck = (List<ConsultaDashBoardDTO>) out.get("PA_CONSULTA");

        String sucAsignada = (String) out.get("PA_SUCURASIGN");

        String sucVisitada = (String) out.get("PA_SUCURVISIT");

        if (listaCheck == null) {
            error = 1;
        } else {
            for (int i = 0; i < listaCheck.size(); i++) {
                listaCheck.get(i).setSucAsignada(sucAsignada);
                listaCheck.get(i).setSucVisitada(sucVisitada);
            }
        }

        if (error != 1) {
            logger.info("Algo ocurrió al consultar el obtieneDashBoard");
            return listaCheck;
        }
        return listaCheck;

    }

}
