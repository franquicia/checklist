package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AlertasResumenDTO;
import com.gruposalinas.checklist.domain.DatosVisitaResumenDTO;
import com.gruposalinas.checklist.domain.DetalleRespuestaReporteDTO;
import com.gruposalinas.checklist.domain.EvidenciasResumenDTO;
import com.gruposalinas.checklist.domain.FoliosResumenDTO;
import com.gruposalinas.checklist.mappers.AlertasResumenRowMapper;
import com.gruposalinas.checklist.mappers.DatosVisitaResumenRowMapper;
import com.gruposalinas.checklist.mappers.DetalleRespuestaReporteRowMapper;
import com.gruposalinas.checklist.mappers.EvidenciasResumenRowMapper;
import com.gruposalinas.checklist.mappers.FoliosResumenRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class DetalleRespuestaReporteDAOImpl extends DefaultDAO implements DetalleRespuestaReporteDAO {

    private Logger logger = LogManager.getLogger(DetalleRespuestaReporteDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneRepuestaReporte;
    private DefaultJdbcCall jdbcObtieneResumenReporte;

    private DefaultJdbcCall jdbcObtieneDatos;
    private DefaultJdbcCall jdbcObtieneFolios;
    private DefaultJdbcCall jdbcObtieneEvidencias;
    private DefaultJdbcCall jdbcObtieneAlertas;

    public void init() {

        jdbcObtieneRepuestaReporte = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADETREPORTES")
                .withProcedureName("SP_DETALLESRES")
                .returningResultSet("RCL_RESPUESTAS", new DetalleRespuestaReporteRowMapper());

        jdbcObtieneResumenReporte = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADETREPORTES")
                .withProcedureName("SP_RESUMEN")
                .returningResultSet("RCL_EVIDENCIAS", new EvidenciasResumenRowMapper())
                .returningResultSet("RCL_FOLIOS", new FoliosResumenRowMapper())
                .returningResultSet("RCL_DATOSVISITA", new DatosVisitaResumenRowMapper())
                .returningResultSet("RCL_ALERTA", new AlertasResumenRowMapper());

        jdbcObtieneDatos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADETREPORTES2")
                .withProcedureName("SP_DATOS")
                .returningResultSet("RCL_CONSULTA", new DatosVisitaResumenRowMapper());

        jdbcObtieneFolios = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADETREPORTES2")
                .withProcedureName("SP_FOLIOS")
                .returningResultSet("RCL_CONSULTA", new FoliosResumenRowMapper());

        jdbcObtieneEvidencias = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADETREPORTES2")
                .withProcedureName("SP_EVIDENCIAS")
                .returningResultSet("RCL_CONSULTA", new EvidenciasResumenRowMapper());

        jdbcObtieneAlertas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADETREPORTES2")
                .withProcedureName("SP_ALERTAS")
                .returningResultSet("RCL_CONSULTA", new AlertasResumenRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DetalleRespuestaReporteDTO> obtieneRespuesta(int checklist, String ceco, String fecha)
            throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;

        List<DetalleRespuestaReporteDTO> respuestas = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", checklist)
                .addValue("PA_CECO", ceco)
                .addValue("PA_FECHA", fecha);

        out = jdbcObtieneRepuestaReporte.execute(in);

        logger.info("Funcion ejecutada:{checklist.PADETREPORTES.SP_DETALLESRES}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        respuestas = (List<DetalleRespuestaReporteDTO>) out.get("RCL_RESPUESTAS");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al obtener las respuestas del checklist");
        }

        return respuestas;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> obtieneResumen(int checklist, String ceco, String fecha) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        Map<String, Object> resumen = new HashMap<String, Object>();

        List<EvidenciasResumenDTO> evidencias = null;
        List<FoliosResumenDTO> folios = null;
        List<DatosVisitaResumenDTO> datosVisita = null;
        List<AlertasResumenDTO> alertasVisita = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", checklist)
                .addValue("PA_CECO", ceco)
                .addValue("PA_FECHA", fecha);

        out = jdbcObtieneResumenReporte.execute(in);

        logger.info("Funcion ejecutada:{checklist.PADETREPORTES.SP_RESUMEN}");
        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        evidencias = (List<EvidenciasResumenDTO>) out.get("RCL_EVIDENCIAS");
        folios = (List<FoliosResumenDTO>) out.get("RCL_FOLIOS");
        datosVisita = (List<DatosVisitaResumenDTO>) out.get("RCL_DATOSVISITA");
        alertasVisita = (List<AlertasResumenDTO>) out.get("RCL_ALERTA");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al obtener el resumen del checklist");
            resumen.put("folios", null);
            resumen.put("evidencias", null);
            resumen.put("datosVisita", null);
            resumen.put("alertas", null);
        } else {
            resumen.put("folios", folios);
            resumen.put("evidencias", evidencias);
            resumen.put("datosVisita", datosVisita);
            resumen.put("alertas", alertasVisita);
        }

        return resumen;
    }

    @SuppressWarnings("unchecked")
    public List<DatosVisitaResumenDTO> obtieneDatos(int checklist, String ceco, String fecha) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        List<DatosVisitaResumenDTO> datosVisita = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", checklist)
                .addValue("PA_CECO", ceco)
                .addValue("PA_FECHA", fecha);

        out = jdbcObtieneDatos.execute(in);

        logger.info("Funcion ejecutada:{checklist.PADETREPORTES2.SP_DATOS}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        datosVisita = (List<DatosVisitaResumenDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al obtener el resumen de datos");
        }

        return datosVisita;

    }

    @SuppressWarnings("unchecked")
    public List<FoliosResumenDTO> obtieneFolios(int checklist, String ceco, String fecha) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        List<FoliosResumenDTO> folios = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", checklist)
                .addValue("PA_CECO", ceco)
                .addValue("PA_FECHA", fecha);

        out = jdbcObtieneFolios.execute(in);

        logger.info("Funcion ejecutada:{checklist.PADETREPORTES2.SP_FOLIOS}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        folios = (List<FoliosResumenDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al obtener el resumen de folios");
        }

        return folios;

    }

    @SuppressWarnings("unchecked")
    public List<EvidenciasResumenDTO> obtieneEvidencias(int checklist, String ceco, String fecha) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        List<EvidenciasResumenDTO> evidencias = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", checklist)
                .addValue("PA_CECO", ceco)
                .addValue("PA_FECHA", fecha);

        out = jdbcObtieneEvidencias.execute(in);

        logger.info("Funcion ejecutada:{checklist.PADETREPORTES2.SP_EVIDENCIAS}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        evidencias = (List<EvidenciasResumenDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al obtener el resumen de evidencias");
        }

        return evidencias;

    }

    @SuppressWarnings("unchecked")
    public List<AlertasResumenDTO> obtieneAlertas(int checklist, String ceco, String fecha) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        List<AlertasResumenDTO> alertasVisita = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", checklist)
                .addValue("PA_CECO", ceco)
                .addValue("PA_FECHA", fecha);

        out = jdbcObtieneAlertas.execute(in);

        logger.info("Funcion ejecutada:{checklist.PADETREPORTES2.SP_ALERTAS}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        alertasVisita = (List<AlertasResumenDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al obtener el resumen de evidencias");
        }

        return alertasVisita;

    }

}
