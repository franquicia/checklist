package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.PreguntaSupDTO;
import com.gruposalinas.checklist.mappers.PreguntaSupRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PreguntaSupDAOImpl extends DefaultDAO implements PreguntaSupDAO {

    private static Logger logger = LogManager.getLogger(PreguntaSupDTO.class);

    DefaultJdbcCall jdbcObtienePregunta;
    DefaultJdbcCall jdbcInsertaPregunta;
    DefaultJdbcCall jdbcInsertaPreguntaCom;
    DefaultJdbcCall jdbcActualizaPreguntaCom;
    DefaultJdbcCall jdbcActualizaPregunta;
    DefaultJdbcCall jdbcEliminaPregunta;

    public void init() {

        jdbcInsertaPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_PREGUNTA")
                .withProcedureName("SPINSPREGUNTA");

        jdbcActualizaPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_PREGUNTA")
                .withProcedureName("SPACTPREGUNTA");

        jdbcObtienePregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_PREGUNTA")
                .withProcedureName("SPGETPREGUNTA")
                .returningResultSet("PA_CONSULTA", new PreguntaSupRowMapper());

        jdbcEliminaPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_PREGUNTA")
                .withProcedureName("SPDELPREGUNTA");

    }

    public int insertaPregunta(PreguntaSupDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idPreg = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDMODULO", bean.getIdModulo())
                .addValue("PA_FIID_MODULO", bean.getIdModulo())
                .addValue("PA_FIID_TIPO_PREG", bean.getIdTipo())
                .addValue("PA_FIESTATUS", bean.getEstatus())
                .addValue("PA_FCDESCRIPCION", bean.getDescPregunta())
                .addValue("PA_FCDETALLE", bean.getDetalle())
                .addValue("PA_FICRITICA", bean.getCritica())
                .addValue("PA_FCSLA", bean.getSla())
                .addValue("PA_FCAREA", bean.getArea())
                .addValue("PA_FINUMSERIE", bean.getNumSerie())
                .addValue("PA_FCRESPONSABLE", bean.getResponsable())
                .addValue("PA_FCNEGOCIO", bean.getNegocio());

        out = jdbcInsertaPregunta.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADM_PREGUNTA.SPINSPREGUNTA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        error = resultado.intValue();

        BigDecimal idPregReturn = (BigDecimal) out.get("PA_IDPREGUNTA");
        idPreg = idPregReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar la Pregunta");
        } else {
            return idPreg;
        }

        return idPreg;

    }

    public boolean actualizaPregunta(PreguntaSupDTO bean) throws Exception {

        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREGUNTA", bean.getIdPregunta())
                .addValue("PA_FIID_MODULO", bean.getIdModulo())
                .addValue("PA_FIID_TIPO_PREG", bean.getIdTipo())
                .addValue("PA_FIESTATUS", bean.getEstatus())
                .addValue("PA_FCDESCRIPCION", bean.getDescPregunta())
                .addValue("PA_FCDETALLE", bean.getDetalle())
                .addValue("PA_FICRITICA", bean.getCritica())
                .addValue("PA_FCSLA", bean.getSla())
                .addValue("PA_FCAREA", bean.getArea())
                .addValue("PA_FINUMSERIE", bean.getNumSerie())
                .addValue("PA_FCRESPONSABLE", bean.getResponsable())
                .addValue("PA_FCNEGOCIO", bean.getNegocio());

        out = jdbcActualizaPregunta.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADM_PREGUNTA.SPACTPREGUNTA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al actualizar la Pregunta( " + bean.getIdPregunta() + ")");
        } else {
            return true;
        }

        return false;
    }

    public List<PreguntaSupDTO> obtienePregunta(String idPregunta, String idModulo, String tipoPregunta) throws Exception {
        Map<String, Object> out = null;
        List<PreguntaSupDTO> listaPregunta = null;
        int error = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREGUNTA", idPregunta)
                .addValue("PA_FIID_MODULO", idModulo)
                .addValue("PA_FIID_TIPO_PREG", tipoPregunta);
        out = jdbcObtienePregunta.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_PREG.SP_SEL_G_PREG}");

        listaPregunta = (List<PreguntaSupDTO>) out.get("PA_CONSULTA");

        if (listaPregunta == null) {
            error = 1;
        }

        if (error == 1) {
            logger.info("Algo ocurrió al obtener las Preguntas");
        } else {
            return listaPregunta;
        }

        return listaPregunta;

    }

    public boolean eliminaPregunta(int idPregunta) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREGUNTA", idPregunta);

        out = jdbcEliminaPregunta.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_PREG.SP_DEL_PREG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al borrar la pregunta id(" + idPregunta + ")");
        } else {
            return true;
        }

        return false;
    }

}
