package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.SucursalHistDTO;


public interface SucursalHistDAO {
	
	public List<SucursalHistDTO> obtieneSucursal(String idHistSuc, String numSuc) throws Exception;

}
