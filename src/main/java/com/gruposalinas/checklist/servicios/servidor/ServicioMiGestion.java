package com.gruposalinas.checklist.servicios.servidor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gruposalinas.checklist.resources.FRQConstantes;


@Controller
@RequestMapping("/ServicioMiGestion")

public class ServicioMiGestion {
	private static Logger logger = LogManager.getLogger(ServicioMiGestion.class);
	
    //http://10.51.210.220:8080/checklist/ServicioMiGestion/ajaxConsultaUsuario.json?idUsuario=666462
	@RequestMapping(value = "/ajaxConsultaUsuario", method = RequestMethod.GET)
	public @ResponseBody String ajaxConsultaUsuario(
			@RequestParam(value = "idUsuario", required = true, defaultValue = "0") String idUsuario) throws IOException, JSONException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		

		logger.info(FRQConstantes.getURLServer() + "/migestion/consultaGestionService/getUsuario.json?idUsuario=" + idUsuario);
	
		//URL url1 = new URL(FRQConstantes.getURLServer() + "/migestion/consultaGestionService/getUsuario.json?idUsuario=" + idUsuario);
		
		URL url1 = new URL("http://10.51.210.239:8080/migestion/consultaGestionService/getUsuario.json?idUsuario=" + idUsuario);
		BufferedReader in1 = null;
		//System.out.println(url1);
		String json = "";
		try {
			in1 = new BufferedReader(new InputStreamReader(url1.openStream()));
			//System.out.println(in1);
			String linea;
			while((linea=in1.readLine())!=null){
				json+=linea;
			}
		} catch (Throwable t) {
			json=null;
			logger.info("+-+-+- Ocurrio algo 2 " + t);
		} finally{
			if(in1!=null){
				in1.close();
			}
		}
		
		logger.info("+-+-+- json" + json);
		
		
		return json;
	}
	
    //http://10.51.210.220:8080/checklist/ServicioMiGestion/UpdateUsuario.json?idUsuario=666462
	@RequestMapping(value = "/UpdateUsuario", method = RequestMethod.GET)
	public @ResponseBody String UpdateUsuario(
			@RequestParam(value = "idUsuario", required = true, defaultValue = "0") String idUsuario,
			@RequestParam(value = "idPuesto", required = true, defaultValue = "0") String idPuesto,
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "nombre", required = true, defaultValue = "0") String nombre,
			@RequestParam(value = "activo", required = true, defaultValue = "0") String activo,
			@RequestParam(value = "fecha", required = true, defaultValue = "0") String fecha) throws IOException, JSONException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		

		logger.info(FRQConstantes.getURLServer() + "/migestion/catalogosService/updateUsuario.json?idUsuario=" + idUsuario + "&idPuesto=" + idPuesto + "&idCeco=" + idCeco + "&nombre=" + nombre + "&activo=" + activo + "&fecha=" + fecha);
	
		//URL url1 = new URL(FRQConstantes.getURLServer() + "/migestion/catalogosService/updateUsuario.json?idUsuario=" + idUsuario + "&idPuesto=" + idPuesto + "&idCeco=" + idCeco + "&nombre=" + nombre + "&activo=" + activo + "&fecha=" + fecha);
		
		URL url1 = new URL("http://10.51.210.239:8080/migestion/catalogosService/updateUsuario.json?idUsuario=" + idUsuario + "&idPuesto=" + idPuesto + "&idCeco=" + idCeco + "&nombre=" + nombre + "&activo=" + activo + "&fecha=" + fecha);
		BufferedReader in1 = null;
		//System.out.println(url1);
		String json = "";
		try {
			in1 = new BufferedReader(new InputStreamReader(url1.openStream()));
			//System.out.println(in1);
			String linea;
			while((linea=in1.readLine())!=null){
				json+=linea;
			}
		} catch (Throwable t) {
			json=null;
			logger.info("+-+-+- Ocurrio algo 2 " + t);
		} finally{
			if(in1!=null){
				in1.close();
			}
		}
		
		logger.info("+-+-+- json" + json);
		
		
		return json;
	}
}
