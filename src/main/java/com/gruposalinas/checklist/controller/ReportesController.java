package com.gruposalinas.checklist.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.FiltrosCecoBI;
import com.gruposalinas.checklist.domain.FiltrosCecoDTO;
import com.gruposalinas.checklist.domain.NegocioDTO;
import com.gruposalinas.checklist.domain.PaisDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;


@Controller
public class ReportesController {


@Autowired
FiltrosCecoBI filtrosCecobi;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="central/reporteOnlineApertura.htm", method = RequestMethod.GET)
	public ModelAndView reporteApertura(HttpServletRequest request, HttpServletResponse response, Model model){ 
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		//logger.info("DATOS USUARIO  ----->" + userSession.getIdUsuario() + userSession.getNombre() + userSession.getLlaveMaestra() + userSession.getPassword());
		//LO CORRECTO ES PASAR EL userSession. Codigo Duro para funcionar en desarrollo	 100082-191312	
		Map<String,Object> data = 	filtrosCecobi.obtieneFiltros(Integer.parseInt(userSession.getIdUsuario()));
		
		List<PaisDTO> pais = (List<PaisDTO>) data.get("pais");
		List<NegocioDTO> negocio = (List<NegocioDTO>) data.get("negocio");	
		List<FiltrosCecoDTO> territorio = (List<FiltrosCecoDTO>) data.get("territorio");
		List<FiltrosCecoDTO> zona = (List<FiltrosCecoDTO>) data.get("zona");
		List<FiltrosCecoDTO> region = (List<FiltrosCecoDTO>) data.get("region");
		
		String salida = "reporteApertura";
		ModelAndView mv = new ModelAndView(salida);
		mv.addObject("listaPaises", pais);
		mv.addObject("listaNegocios", negocio);
		mv.addObject("listaTerritorios", territorio);
		mv.addObject("listaZona", zona);
		mv.addObject("listaRegion", region);
		mv.addObject("rutaImg",FRQConstantes.getRutaImagen());
	
		return mv;
	}
		
	//NO SE UTILIZA PUES SE CAMBIÓ POR POP-UP
	@SuppressWarnings({ "unused", "static-access" })
	@RequestMapping(value="central/detalleReporteOnline.htm", method= RequestMethod.POST)
    public ModelAndView detalleReporteOnline(HttpServletRequest request,HttpServletResponse response, Model model){ 
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		
		FRQConstantes cons = new FRQConstantes();
		String idTienda = request.getParameter("idTienda");
		String iArray = request.getParameter("iArray");
		String fecha = request.getParameter("fecha");
		String idCheck =  request.getParameter("idCheck");
		String idModulo = request.getParameter("idModulo");
		String tienda = request.getParameter("tienda");

		
		////System.out.println("tienda" + tienda);
				
		String salida = "detalleReporteOnline";
		ModelAndView mv = new ModelAndView(salida);
		mv.addObject("idTienda", idTienda);
		mv.addObject("iArray", iArray);
		mv.addObject("fecha", fecha);
		mv.addObject("idCheck", idCheck);
		mv.addObject("idModulo", idModulo);
		mv.addObject("tienda", tienda);
		mv.addObject("rutaImg", cons.getRutaImagen());
		
		return mv;
    }
	
	//NO SE UTILIZA SE CAMBIÓ POR POP-UP
	@RequestMapping(value="/getDetalleReporteOnline", method= RequestMethod.GET)
    public @ResponseBody List<String> detalleReporteOnline(
    		
    		@RequestParam (value="idTienda") String idTienda,
    		@RequestParam (value="iArray") String iArray,
    		@RequestParam (value="fecha") String fecha,
    		@RequestParam (value="idCheck") String idCheck,
    		@RequestParam (value="idModulo") String idModulo,
    		@RequestParam (value="tienda") String tienda
    		, HttpRequest request){ 

		
		ArrayList<String> ret = new ArrayList<String>();
		ret.add(iArray);
		ret.add(fecha);
		ret.add(idCheck);
		ret.add(idModulo);
		ret.add(tienda);
		ret.add(FRQConstantes.getRutaImagen());
		return ret;
    }
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="central/reporteOnlineAperturaMovil.htm", method = RequestMethod.GET)
    public ModelAndView reporteAperturaMovil(HttpServletRequest request, HttpServletResponse response, Model model){ 
		//logger.info("DATOS USUARIO  ----->" + userSession.getIdUsuario() + userSession.getNombre() + userSession.getLlaveMaestra() + userSession.getPassword());
			//logger.info("DATOS USUARIO  ----->" + userSession.getIdUsuario() + userSession.getNombre() + userSession.getLlaveMaestra() + userSession.getPassword());
			UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
			//LO CORRECTO ES PASAR EL userSession. Codigo Duro para funcionar en desarrollo	 100082-191312	
			Map<String,Object> data = 	filtrosCecobi.obtieneFiltros(Integer.parseInt(userSession.getIdUsuario()));
			
			
			List<PaisDTO> pais = (List<PaisDTO>) data.get("pais");
			List<NegocioDTO> negocio = (List<NegocioDTO>) data.get("negocio");	
			List<FiltrosCecoDTO> territorio = (List<FiltrosCecoDTO>) data.get("territorio");
			List<FiltrosCecoDTO> zona = (List<FiltrosCecoDTO>) data.get("zona");
			List<FiltrosCecoDTO> region = (List<FiltrosCecoDTO>) data.get("region");
			
			String salida = "reporteAperturaMovil";
			ModelAndView mv = new ModelAndView(salida);
			mv.addObject("listaPaises", pais);
			mv.addObject("listaNegocios", negocio);
			mv.addObject("listaTerritorios", territorio);
			mv.addObject("listaZona", zona);
			mv.addObject("listaRegion", region);
			mv.addObject("rutaImg",FRQConstantes.getRutaImagen());
		
			return mv;
    }
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="central/reporteOnlineCierreMovil.htm", method = RequestMethod.GET)
    public ModelAndView reporteCierreMovil(HttpServletRequest request, HttpServletResponse response, Model model){ 
		//logger.info("DATOS USUARIO  ----->" + userSession.getIdUsuario() + userSession.getNombre() + userSession.getLlaveMaestra() + userSession.getPassword());
			//logger.info("DATOS USUARIO  ----->" + userSession.getIdUsuario() + userSession.getNombre() + userSession.getLlaveMaestra() + userSession.getPassword());
			UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
			//LO CORRECTO ES PASAR EL userSession. Codigo Duro para funcionar en desarrollo	 100082-191312	
			Map<String,Object> data = 	filtrosCecobi.obtieneFiltros(Integer.parseInt(userSession.getIdUsuario()));
			
			
			List<PaisDTO> pais = (List<PaisDTO>) data.get("pais");
			List<NegocioDTO> negocio = (List<NegocioDTO>) data.get("negocio");	
			List<FiltrosCecoDTO> territorio = (List<FiltrosCecoDTO>) data.get("territorio");
			List<FiltrosCecoDTO> zona = (List<FiltrosCecoDTO>) data.get("zona");
			List<FiltrosCecoDTO> region = (List<FiltrosCecoDTO>) data.get("region");
			
			String salida = "reporteCierreMovil";
			ModelAndView mv = new ModelAndView(salida);
			mv.addObject("listaPaises", pais);
			mv.addObject("listaNegocios", negocio);
			mv.addObject("listaTerritorios", territorio);
			mv.addObject("listaZona", zona);
			mv.addObject("listaRegion", region);
			mv.addObject("rutaImg",FRQConstantes.getRutaImagen());
		
			return mv;
	}
}