package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.ReporteChecksExpDTO;

public class ReporteChecksExpRowMapper implements RowMapper<ReporteChecksExpDTO> {

	public ReporteChecksExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteChecksExpDTO rep = new ReporteChecksExpDTO();
		
		
		rep.setBitacora(rs.getInt("FIID_BITACORA"));
		rep.setRespuesta(rs.getString("FIID_RESPUESTA"));
		rep.setIdArbol(rs.getInt("FIID_ARBOL_DES"));
		rep.setObserv(rs.getString("OBSERVACIONES"));
		rep.setIdPreg(rs.getInt("FIID_PREGUNTA"));
		rep.setIdcheck(rs.getInt("FIID_CHECKLIST"));
		rep.setRespuesta(rs.getString("FCRESPUESTA"));
		rep.setIdobserv(rs.getInt("FIREQOBS"));
		rep.setPosible(rs.getString("POSIBLE"));
		rep.setPregunta(rs.getString("PREGUNTA"));
		rep.setImperdon(rs.getInt("FICRITICA"));
		
	    
	
		return rep;
	}

}

