package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.CheckAutoProtoDTO;

public interface CheckAutoProtoDAO {

	

	
	public List<CheckAutoProtoDTO> obtieneAutoProto(String idTab) throws Exception;
	
	public int insertaAutoProto(CheckAutoProtoDTO bean) throws Exception;
	
	public boolean actualizaAutoProto(CheckAutoProtoDTO bean) throws Exception;
	
	public boolean actualizaAutoProtoStatus(CheckAutoProtoDTO bean) throws Exception;
	
	public boolean eliminaAutoProto(int idTab) throws Exception;
}
