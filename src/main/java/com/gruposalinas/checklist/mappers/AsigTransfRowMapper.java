package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.AsignaTransfDTO;

public class AsigTransfRowMapper implements RowMapper<AsignaTransfDTO> {

	public AsignaTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AsignaTransfDTO AsignaTransfDTO = new AsignaTransfDTO();
		
		AsignaTransfDTO.setCeco(rs.getInt("FCID_CECO"));
	    AsignaTransfDTO.setProyecto(rs.getInt("FIID_PROYECTO"));
	    AsignaTransfDTO.setUsuario_asig(rs.getInt("FIID_USUARIO"));
	    AsignaTransfDTO.setUsuario(rs.getInt("FCUSUARIO"));
	    AsignaTransfDTO.setFaseAct(rs.getInt("FIID_FASEACT"));
	    AsignaTransfDTO.setIdestatus(rs.getInt("FCSTATUS"));
	    AsignaTransfDTO.setObs(rs.getString("FCOBSERVACION"));
	    AsignaTransfDTO.setAgrupador(rs.getInt("FIID_AGRUPA"));
	    AsignaTransfDTO.setPeriodo(rs.getString("FCPERIODO"));
	 			
		return AsignaTransfDTO;
	}

}


