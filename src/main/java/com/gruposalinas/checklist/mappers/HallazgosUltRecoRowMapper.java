package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistHallazgosRepoDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.HallazgosExpDTO;
import com.gruposalinas.checklist.domain.PreguntaCheckDTO;

public class HallazgosUltRecoRowMapper implements RowMapper<HallazgosExpDTO> {

	public HallazgosExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		

		HallazgosExpDTO com = new HallazgosExpDTO();

       
	    com.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    com.setBitacora(rs.getInt("FIID_BITACORA"));
	    com.setObs(rs.getString("FCOBSERVACIONES"));
	    com.setIdcheck(rs.getInt("FIID_CHECKLIST"));
	    com.setAux2(rs.getInt("FCRESPUESTA"));
	    com.setRespuesta(rs.getString("POSIBLE"));
	    com.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    com.setPregPadre(rs.getInt("FIID_PREG_PADRE")); 
	    com.setPreg(rs.getString("FCDESCRIPCION"));   
	    com.setBitGral(rs.getInt("FIID_BITGRAL"));
	    com.setRuta(rs.getString("FCRUTA"));
	   // com.setIdPreg(rs.getInt("FIID_PREGUNTA"));

	 
	    return com;


	}
}



