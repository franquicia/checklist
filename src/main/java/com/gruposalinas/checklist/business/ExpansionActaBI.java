package com.gruposalinas.checklist.business;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.CenterTextMode;
import org.jfree.chart.plot.RingPlot;
import org.jfree.data.general.DefaultPieDataset;

import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;
import com.gruposalinas.checklist.domain.RepoFilExpDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.util.CleanPath;
import com.itextpdf.text.Document;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

public class ExpansionActaBI {
	
	private static final Logger logger = LogManager.getLogger(ExpansionActaBI.class);
	String direccion=FRQConstantes.getRutaImagen(); 
	
	private String idBitacora;
	List<String> imperdonables = null;
	
	private ChecklistPreguntasComDTO ektObj = null;
	private ChecklistPreguntasComDTO ektObjExt = null;
	private ChecklistPreguntasComDTO conteoHiijasEkt = null;
	private double ponderacionEktExt;
	private double totalGeneralEkt;
	private double ektSiHijas;
	private double ektNaHijas;

	private ChecklistPreguntasComDTO bazObj = null;
	private ChecklistPreguntasComDTO conteoHiijasBaz = null;
	private ChecklistPreguntasComDTO bazObjExt = null;   
	private double ponderacionBazExt;       
	private double totalGeneralBaz;                
	private double bazSiHijas;	                    
	private double bazNaHijas;   

	private ChecklistPreguntasComDTO generalesObj = null;
	private ChecklistPreguntasComDTO prestaPrendaObjExt = null;
	private ChecklistPreguntasComDTO conteoHiijasPrestaPrenda = null;
	private double ponderacionPrestaExt;
	private double totalGeneralPresta;
	private double prestaPrendaSiHijas;
	private double prestaPrendaNaHijas;

	private ChecklistPreguntasComDTO areasComunesObj = null;
	private ChecklistPreguntasComDTO areasComunesObjExt = null;
	private ChecklistPreguntasComDTO conteoHijasComunes = null;
	private double ponderacionComExt;
	private double totalGeneralComunes;
	private double comSiHijas;
	private double comNaHijas;

	private ChecklistPreguntasComDTO prestaPrendaObj = null;
	private ChecklistPreguntasComDTO generalesObjExt = null;
	private ChecklistPreguntasComDTO conteoHiijasGenerales = null;
	private double ponderacionGenExt;
	private double totalGeneralGenerales;
	private double generalesSiHijas;
	private double generalesNaHijas;
	
	private ChecklistPreguntasComDTO cYcObj = null;
	private ChecklistPreguntasComDTO cYcObjExt = null;
	private ChecklistPreguntasComDTO conteoHiijasCyC = null;
	private double ponderacionCyCExt;
	private double totalGeneralCyC;
	private double cYcSiHijas;
	private double cYcNaHijas;
	
	private DecimalFormat formatDecimales;
	private DecimalFormat formatEnteros;
	private List<ChecklistPreguntasComDTO> listaGrupos = null;
	private List<ChecklistPreguntasComDTO> listaGruposExt = null;
	private List<ChecklistPreguntasComDTO> listaConteosHijas = null;
	
	private String logoCom;
	private String logoBaz;
	private String logoEkt;
	private String logoPres;
	private String logoGen;
	private String logoCyC;
	private String logoTotal;

	private String comResSi;
	private String bazResSi;
	private String ektResSi;
	private String genResSi;
	private String prestaResSi;
	private String cYcResSi;
	private String totalResSi;

	private String comResNo;
	private String bazResNo;
	private String ektResNo;
	private String genResNo;
	private String prestaResNo;
	private String cYcResNo;
	private String totalResNo;

	private String comSiPor;
	private String bazSiPor;
	private String ektSiPor;
	private String genSiPor;
	private String prestaSiPor;
	private String cYcSiPor;
	private String totalSiPor;

	private String comNoPor;
	private String bazNoPor;
	private String ektNoPor;
	private String genNoPor;
	private String prestaNoPor;
	private String cYcNoPor;
	private String totalNoPor;
	
	String ektTotalPon;
	String genTotalPon;
	String prestaTotalPon;
	String bazTotalPon;
	String comTotalPon;
	String cYcTotalPon;
	double totalTotal;
	
	String ektCalifCant;
	String genCalifCant;
	String prestaCalifCant;
	String bazCalifCant;
	String comCalifCant;
	String cYcCalifCant;
	String totalCalifCant;
	
	private File graficaComunes = null;
	private File graficaBanco = null;
	private File graficaElektra = null;
	private File graficaGenerales = null;
	private File graficaPresta = null;
	private File graficaCyC = null;
	private File graficaTotal = null;
	
	private List<RepoFilExpDTO> preguntasPP;
	private List<RepoFilExpDTO> preguntasEkt;
	private List<RepoFilExpDTO> preguntasBaz;
	private List<RepoFilExpDTO> preguntasAreas;
	private List<RepoFilExpDTO> preguntasCyC;
	private List<RepoFilExpDTO> preguntasGenerales;
	
	private String operable;
	private String colorOperable;
	private String colorTexto;
	private double numeroDeCalificacion;
	private int numImperdonablesRecorrido;
	private double preCalificacionNum;
	private String totalItems;
	private String itemsPorRevisar;
	private String itemsAprobados;
	private String itemsNoConsiderados;
	
	private int colorBaner = 0; // 0=blanco
	private String imgBaner = "";
	
	
	public ExpansionActaBI(String idBitacora,String Negocio) {
		cadenaImperdonables(idBitacora);
		this.idBitacora=idBitacora;
		switch (Negocio) {
		case "EKT":
			obtieneDatos();
			calculaGraficos();
			break;
		case "OCC":
			obtieneDatosOCC();
			calculaGraficosOCC();
			break;
		case "DAZ":
			obtieneDatos();
			calculaGraficos();
			break;
			

		default:
			break;
		}
		
		obtieneDatosAperturables();
		cadenaTodasPreguntas();
		this.imgBaner=generaImagenBaner();
		
	}

	private void obtieneDatos() {
		formatDecimales = new DecimalFormat("###.##");
		formatEnteros = new DecimalFormat("###.##");
		
		// Obtenemos Datos
		ChecklistPreguntasComBI checklistPreguntasComBI = (ChecklistPreguntasComBI) FRQAppContextProvider
				.getApplicationContext().getBean("checklistPreguntasComBI");

		// Obtiene La información anterior
		List<ChecklistPreguntasComDTO> listaGrupos = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> listaGruposExt = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> listaConteosHijas = new ArrayList<ChecklistPreguntasComDTO>();
		
		try {
			listaGrupos = checklistPreguntasComBI.obtieneInfoCont(idBitacora);
		} catch (Exception e) {
			logger.info("AP al obtener al obtener obtieneInfoCont de bitacora: " + idBitacora);
		}
		try {
			listaGruposExt = checklistPreguntasComBI.obtieneInfoContExt(idBitacora);
		} catch (Exception e) {
			logger.info("AP al obtener al obtener obtieneInfoContExt de bitacora: " + idBitacora);
		}
		try {
			listaConteosHijas = checklistPreguntasComBI.obtieneInfoContHij(idBitacora);
		} catch (Exception e) {
			logger.info("AP al obtener al obtener obtieneInfoContHij de bitacora: " + idBitacora);
		}
		
		for(int i=0; i<listaGrupos.size();i++) {
			if ( listaGrupos.get(i).getClasif().equalsIgnoreCase("EKT")){
				ektObj = listaGrupos.get(i);
				
				ektObjExt = listaGruposExt.get(i);
				ponderacionEktExt = ektObjExt.getSumGrupo();
				totalGeneralEkt = ektObjExt.getTotGral();
				
				conteoHiijasEkt = listaConteosHijas.get(2);
				ektSiHijas = conteoHiijasEkt.getPregSi();
				ektNaHijas = conteoHiijasEkt.getPregNa();
			}
			if ( listaGrupos.get(i).getClasif().equalsIgnoreCase("BAZ")){
				bazObj = listaGrupos.get(i);
				
				bazObjExt = listaGruposExt.get(i);
				ponderacionBazExt = bazObjExt.getSumGrupo();
				totalGeneralBaz = bazObjExt.getTotGral();
				
				conteoHiijasBaz = listaConteosHijas.get(1);
				bazSiHijas = conteoHiijasBaz.getPregSi();
				bazNaHijas = conteoHiijasBaz.getPregNa();
			}
			if ( listaGrupos.get(i).getClasif().equalsIgnoreCase("PP")){
				prestaPrendaObj = listaGrupos.get(i);
				
				prestaPrendaObjExt = listaGruposExt.get(i);
				ponderacionPrestaExt = prestaPrendaObjExt.getSumGrupo();
				totalGeneralPresta = prestaPrendaObjExt.getTotGral();
				
				conteoHiijasPrestaPrenda = listaConteosHijas.get(i);
				prestaPrendaSiHijas = conteoHiijasPrestaPrenda.getPregSi();
				prestaPrendaNaHijas = conteoHiijasPrestaPrenda.getPregNa();
			}
			if ( listaGrupos.get(i).getClasif().equalsIgnoreCase("AREAS")){
				areasComunesObj = listaGrupos.get(i);
				
				areasComunesObjExt = listaGruposExt.get(i);
				ponderacionComExt = areasComunesObjExt.getSumGrupo();
				totalGeneralComunes = areasComunesObjExt.getTotGral();
				
				conteoHijasComunes = listaConteosHijas.get(i);
				comSiHijas = conteoHijasComunes.getPregSi();
				comNaHijas = conteoHijasComunes.getPregNa();
			}
			if ( listaGrupos.get(i).getClasif().equalsIgnoreCase("GENERALES")){
				generalesObj = listaGrupos.get(i);
				
				generalesObjExt = listaGruposExt.get(i);
				ponderacionGenExt = generalesObjExt.getSumGrupo();
			 	totalGeneralGenerales = generalesObjExt.getTotGral();
				
		 		conteoHiijasGenerales = listaConteosHijas.get(i);
		 		generalesSiHijas = conteoHiijasGenerales.getPregSi();
		 		generalesNaHijas = conteoHiijasGenerales.getPregNa();
			}
		}
	}

	
	private void calculaGraficos() {
		// 1.- GRÁFICA COMUNES ********
		// Cálculo Total de Preguntas
		// double comResNo1 = areasComunesObj.getPregNo();
		double comResNo1 = areasComunesObj.getPregNo();
		// NA
		double comResNa1 = areasComunesObj.getPregNa();
		//
		double comGrupoCompleto = areasComunesObj.getTotPreg();
		// double comResSi1 = areasComunesObj.getTotPreg() -
		// areasComunesObj.getPregNo();
		double comResSi1 = comSiHijas + (comGrupoCompleto - comResNa1) - comResNo1;

		// double comTotal = areasComunesObj.getTotPreg();
		double comTotal = comResSi1 + comResNo1 + comSiHijas;
		
		// PARA LOS NA FALTA SUMAR LO DESACTIVADO
		comResNa1 += (totalGeneralComunes - comTotal);

		double comSiPor1 = 0;
		double comNoPor1 = 0;

		if (comTotal != 0) {
			comSiPor1 = (comResSi1 / (comResSi1 + comResNo1)) * 100;
			comNoPor1 = (comResNo1 / (comResSi1 + comResNo1)) * 100;
		}
		

		
		double NAComunes = 0;

		if (areasComunesObj.getPregSi() != 0 && areasComunesObj.getPregNo() != 0 ) {
			NAComunes = (totalGeneralComunes - (areasComunesObj.getPregNo() + areasComunesObj.getPregSi()));
			NAComunes = NAComunes - (comSiHijas);
		}
		
		// Calculo Por Ponderación
		// double comPorcentajePonderacion = (areasComunesObj.getPrecalif() * 100) /
		// areasComunesObj.getSumGrupo();
		double comPorcentajePonderacion = (ponderacionComExt > 0 ? ((areasComunesObj.getPrecalif() * 100) / ponderacionComExt):0.0);
		
		int comunesImperdonables = areasComunesObj.getAux2();
		logoCom = getLogoExpansion(comPorcentajePonderacion, comunesImperdonables);

		// Cantidad Tabla (Si-No)
		if (areasComunesObj.getPregSi() == 0 && areasComunesObj.getPregNo() == 0 && comSiHijas == 0) {
			comResSi1 = 0;
			comResNo1 = 0;
			comSiPor1 = 0;
			comNoPor1 = 0;
		}
		
		comResSi = formatEnteros.format(comResSi1);
		comResNo = formatEnteros.format(comResNo1);

		// Porcentaje Tabla y Gráfica
		comSiPor = formatDecimales.format(comSiPor1);
		comNoPor = formatDecimales.format(comNoPor1);

		if (comResSi1 == 0 && comResNo1 == 0) {
			graficaComunes = grafica(1, 0, 100);
			logoCom = getLogoExpansion(100, 0);
		} else {
			graficaComunes = grafica(1, Double.parseDouble(comSiPor), Double.parseDouble(comNoPor));
		}

		// GRÁFICA COMUNES ********
		
		// 2.- GRÁFICA BANCO ********
		// double bazResNo1 = bazObj.getPregNo();
		double bazResNo1 = bazObj.getPregNo();
		// NA
		double bazResNa1 = bazObj.getPregNa();
		//
		double bazGrupoCompleto = bazObj.getTotPreg();
		// double bazResSi1 = bazObj.getTotPreg() - bazObj.getPregNo();
		double bazResSi1 = bazSiHijas + (bazGrupoCompleto - bazResNa1) - bazResNo1;

		// double bazTotal = bazObj.getTotPreg();
		double bazTotal = bazResSi1 + bazResNo1 + bazSiHijas;

		// PARA LOS NA FALTA SUMAR LO DESACTIVADO
		bazResNa1 += (totalGeneralBaz - bazTotal);
		
		double bazSiPor1 = 0;
		double bazNoPor1 = 0;

		if (bazTotal != 0) {
			bazSiPor1 = (bazResSi1 / (bazResSi1 + bazResNo1)) * 100;
			bazNoPor1 = (bazResNo1 / (bazResSi1 + bazResNo1)) * 100;
		}

		
		double NABaz = 0.0;
		
		if (bazObj.getPregSi() != 0 && bazObj.getPregNo() != 0 ) {
			NABaz = (totalGeneralBaz - (bazObj.getPregNo() + bazObj.getPregSi()));
			NABaz = NABaz - (bazSiHijas);
		}
		// Calculo Por Ponderación
		// double bazPorcentajePonderacion = (bazObj.getPrecalif() * 100) /
		// bazObj.getSumGrupo();
		double bazPorcentajePonderacion = (ponderacionBazExt>0 ? ((bazObj.getPrecalif() * 100) / ponderacionBazExt) : 0.0);

		int bazImperdonables = bazObj.getAux2();
		logoBaz = getLogoExpansion(bazPorcentajePonderacion, bazImperdonables);

		// Cantidad Tabla (Si-No)
		if (bazObj.getPregSi() == 0 && bazObj.getPregNo() == 0 && bazSiHijas == 0) {
			bazResSi1 = 0;
			bazResNo1 = 0;
			bazSiPor1 = 0;
			bazNoPor1 = 0;
		}
		
		bazResSi = formatEnteros.format(bazResSi1);
		bazResNo = formatEnteros.format(bazResNo1);

		// Porcentaje Tabla y Gráfica
		bazSiPor = formatDecimales.format(bazSiPor1);
		bazNoPor = formatDecimales.format(bazNoPor1);

		if (bazResSi1 == 0 && bazResNo1 == 0) {
			graficaBanco = grafica(1, 0, 100);
			logoBaz = getLogoExpansion(100, 0);

		} else {
			graficaBanco = grafica(1, Double.parseDouble(bazSiPor), Double.parseDouble(bazNoPor));
		}

		// GRÁFICA BANCO ********

		// 3.- GRÁFICA ELEKTRA ********
		// double ektResNo1 = ektObj.getPregNo();
		double ektResNo1 = ektObj.getPregNo();
		// NA
		double ektResNa1 = ektObj.getPregNa();
		// TOTAL DE GRUPO COMPLETO
		double ektGrupoCompleto = ektObj.getTotPreg();
		// double ektResSi1 = ektObj.getTotPreg() - ektObj.getPregNo();
		double ektResSi1 = ektSiHijas + (ektGrupoCompleto - ektResNa1) - ektResNo1;

		// double ektTotal = ektObj.getTotPreg();
		double ektTotal = ektResSi1 + ektResNo1 + ektSiHijas;

		// PARA LOS NA FALTA SUMAR LO DESACTIVADO
		ektResNa1 += (totalGeneralEkt - ektTotal);

		double ektSiPor1 = 0;
		double ektNoPor1 = 0;
		
		if (ektTotal != 0) {
			ektSiPor1 = (ektResSi1 / (ektResSi1 + ektResNo1)) * 100;
			ektNoPor1 = (ektResNo1 / (ektResSi1 + ektResNo1)) * 100;
		}

		
		double NAEkt = 0;

		if (ektObj.getPregSi() != 0 && ektObj.getPregNo() != 0 ) {
			NAEkt = (totalGeneralEkt - (ektObj.getPregNo() + ektObj.getPregSi()));
			NAEkt = NAEkt - (ektSiHijas);
		}

		// Calculo Por Ponderación
		// double ektPorcentajePonderacion = (ektObj.getPrecalif() * 100) /
		// ektObj.getSumGrupo();
		double ektPorcentajePonderacion = (ponderacionEktExt > 0 ? ((ektObj.getPrecalif() * 100) / ponderacionEktExt):0.0);
		
		int ektImperdonables = ektObj.getAux2();
		logoEkt = getLogoExpansion(ektPorcentajePonderacion, ektImperdonables);

		// Cantidad Tabla (Si-No)
		if (ektObj.getPregSi() == 0 && ektObj.getPregNo() == 0 && ektSiHijas == 0) {
			ektResSi1 = 0;
			ektResNo1 = 0;
			ektSiPor1 = 0;
			ektNoPor1 = 0;
		}

		ektResSi = formatEnteros.format(ektResSi1);
		ektResNo = formatEnteros.format(ektResNo1);
		
		// Porcentaje Tabla y Gráfica
		ektSiPor = formatDecimales.format(ektSiPor1);
		ektNoPor = formatDecimales.format(ektNoPor1);

		if (ektResSi1 == 0 && ektResNo1 == 0) {
			graficaElektra = grafica(1, 0, 100);
			logoEkt = getLogoExpansion(100, 0);

		} else {
			graficaElektra = grafica(1, Double.parseDouble(ektSiPor), Double.parseDouble(ektNoPor));
		}
		
		// GRÁFICA ELEKTRA ********

		// 4.- GRÁFICA GENERALES ********
		// double genResNo1 = generalesObj.getPregNo();
		double genResNo1 = generalesObj.getPregNo();
		// NA
		double genResNa1 = generalesObj.getPregNa();
		//
		double genGrupoCompleto = generalesObj.getTotPreg();

		// double genResSi1 = generalesObj.getTotPreg() - generalesObj.getPregNo();
		double genResSi1 = generalesSiHijas + (genGrupoCompleto - genResNa1) - genResNo1;

		// double genTotal = generalesObj.getTotPreg();
		double genTotal = genResSi1 + genResNo1 + generalesSiHijas;

		// PARA LOS NA FALTA SUMAR LO DESACTIVADO
		genResNa1 += (totalGeneralGenerales - genTotal);

		double genSiPor1 = 0;
		double genNoPor1 = 0;

		if (genTotal != 0) {
			genSiPor1 = (genResSi1 / (genResSi1 + genResNo1)) * 100;
			genNoPor1 = (genResNo1 / (genResSi1 + genResNo1)) * 100;
		}
		

		double NAGenerales = 0.0;
		
		if (generalesObj.getPregSi() != 0 && generalesObj.getPregNo() != 0 ) {
			NAGenerales = (totalGeneralBaz - (generalesObj.getPregNo() + generalesObj.getPregSi()));
			NAGenerales = NAGenerales - (generalesSiHijas);
		}
		
		// Calculo Por Ponderación
		// double generalesPorcentajePonderacion = (generalesObj.getPrecalif() * 100) /
		// generalesObj.getSumGrupo();
		double generalesPorcentajePonderacion = (ponderacionGenExt > 0 ? ((generalesObj.getPrecalif() * 100) / ponderacionGenExt):0.0);

		int generalesImperdonables = generalesObj.getAux2();
		logoGen = getLogoExpansion(generalesPorcentajePonderacion, generalesImperdonables);

		// Cantidad Tabla (Si-No)
		if (generalesObj.getPregSi() == 0 && generalesObj.getPregNo() == 0 && generalesSiHijas == 0) {
			genResSi1 = 0;
			genResNo1 = 0;
			genSiPor1 = 0;
			genNoPor1 = 0;
		}

		genResSi = formatEnteros.format(genResSi1);
		genResNo = formatEnteros.format(genResNo1);

		// Porcentaje Tabla y Gráfica
		genSiPor = formatDecimales.format(genSiPor1);
		genNoPor = formatDecimales.format(genNoPor1);
		
		if (genResSi1 == 0 && genResNo1 == 0) {
			graficaGenerales = grafica(1, 0, 100);
			logoGen = getLogoExpansion(100, 0);

		} else {
			graficaGenerales = grafica(1, Double.parseDouble(genSiPor), Double.parseDouble(genNoPor));
		}
		
		// GRÁFICA GENERALES ********

		// 5.- GRÁFICA PRESTA PRENDA *******
		// double prestaResNo1 = prestaPrendaObj.getPregNo();
		double prestaResNo1 = prestaPrendaObj.getPregNo();
		// NA
		double prestaResNa1 = prestaPrendaObj.getPregNa();
		//
		double prestaGrupoCompleto = prestaPrendaObj.getTotPreg();
		// double prestaResSi1 = prestaPrendaObj.getTotPreg() -
		// prestaPrendaObj.getPregNo();
		double prestaResSi1 = prestaPrendaSiHijas + (prestaGrupoCompleto - prestaResNa1) - prestaResNo1;

		// double prestaTotal = prestaPrendaObj.getTotPreg();
		double prestaTotal = prestaResSi1 + prestaResNo1 + prestaPrendaSiHijas;

		// PARA LOS NA FALTA SUMAR LO DESACTIVADO
		prestaResNa1 += (totalGeneralPresta - prestaTotal);

		double prestaSiPor1 = 0;
		double prestaNoPor1 = 0;
		
		if (prestaTotal != 0) {
			prestaSiPor1 = (prestaResSi1 / (prestaResSi1 + prestaResNo1)) * 100;
			prestaNoPor1 = (prestaResNo1 / (prestaResSi1 + prestaResNo1)) * 100;
		}

		
		//double NAPresta = ( - (.getPregNo() + prestaPrendaObj.getPregSi()));
		//NAPresta = NAPresta - ();
		
		double NAPresta = 0.0;
		
		if (prestaPrendaObj.getPregSi() != 0 && prestaPrendaObj.getPregNo() != 0 ) {
			NAPresta = (totalGeneralPresta - (prestaPrendaObj.getPregNo() + prestaPrendaObj.getPregSi()));
			NAPresta = NAPresta - (prestaPrendaSiHijas);
		}
		
		// Calculo Por Ponderación
		// double prestaPorcentajePonderacion = (prestaPrendaObj.getPrecalif() * 100) /
		// prestaPrendaObj.getSumGrupo();
		double prestaPorcentajePonderacion = ( ponderacionPrestaExt > 0 ? ((prestaPrendaObj.getPrecalif() * 100) / ponderacionPrestaExt):0.0);

		int prestaImperdonables = prestaPrendaObj.getAux2();
		logoPres = getLogoExpansion(prestaPorcentajePonderacion, prestaImperdonables);

		// Cantidad Tabla (Si-No)
		if (prestaPrendaObj.getPregSi() == 0 && prestaPrendaObj.getPregNo() == 0 && prestaPrendaSiHijas == 0) {
			prestaResSi1 = 0;
			prestaResNo1 = 0;
			prestaSiPor1 = 0;
			prestaNoPor1 = 0;
		}

		prestaResSi = formatEnteros.format(prestaResSi1);
		prestaResNo = formatEnteros.format(prestaResNo1);

		// Porcentaje Tabla y Gráfica
		prestaSiPor = formatDecimales.format(prestaSiPor1);
		prestaNoPor = formatDecimales.format(prestaNoPor1);
		
		if (prestaResSi1 == 0 && prestaResNo1 == 0) {
			graficaPresta = grafica(1, 0, 100);
			logoPres = getLogoExpansion(100, 0);
		} else {
			graficaPresta = grafica(1, Double.parseDouble(prestaSiPor), Double.parseDouble(prestaNoPor));
		}

		// GRÁFICA PRESTA PRENDA ********

		// 6.- VALORES TOTAL ********
		// double totalResSi1 = (totalTotal - totalResNo1);
		double totalResNo1 = (areasComunesObj.getPregNo() + bazObj.getPregNo() + ektObj.getPregNo()
				+ generalesObj.getPregNo() + prestaPrendaObj.getPregNo());
		// NA
		double totalResNa1 = (areasComunesObj.getPregNa() + bazObj.getPregNa() + ektObj.getPregNa()
				+ generalesObj.getPregNa() + prestaPrendaObj.getPregNa());

		double totalResSi1 = (comResSi1 + bazResSi1 + ektResSi1 + genResSi1 + prestaResSi1);

		totalTotal = (totalResNo1 + totalResSi1);

		double totalSiPor1 = 0;
		double totalNoPor1 = 0;

		if (totalTotal != 0) {
			totalSiPor1 = (totalResSi1 / totalTotal) * 100;
			totalNoPor1 = (totalResNo1 / totalTotal) * 100;
		}

		// Calculo Por Ponderación
		// double totalPrecalif = (areasComunesObj.getPrecalif() + bazObj.getPrecalif()
		// + ektObj.getPrecalif() + generalesObj.getPrecalif() +
		// prestaPrendaObj.getPrecalif());
		double totalPrecalif = (areasComunesObj.getPonTot() + bazObj.getPonTot() + ektObj.getPonTot()
				+ generalesObj.getPonTot() + prestaPrendaObj.getPonTot());
		double totalSumGrupo = (ponderacionComExt + ponderacionBazExt + ponderacionEktExt + ponderacionGenExt
				+ ponderacionPrestaExt);
		double tPorcentajePonderacion = (totalPrecalif * 100) / totalSumGrupo;

		int tImperdonables = (areasComunesObj.getAux2() + bazObj.getAux2() + ektObj.getAux2() + generalesObj.getAux2()
				+ prestaPrendaObj.getAux2());
		logoTotal = getLogoExpansion(tPorcentajePonderacion, tImperdonables);
		
		
		// Cantidad Tabla (Si-No)
		totalResSi = formatEnteros.format(totalResSi1);
		totalResNo = formatEnteros.format(totalResNo1);

		// Porcentaje Tabla y Gráfica
		totalSiPor = formatDecimales.format(totalSiPor1);
		totalNoPor = formatDecimales.format(totalNoPor1);

		// ********* Calificación *********
		// double totalCalif1 = (totalSiPor1 * totalValorTotal) / 100;
		double totalCalif1 = (totalTotal>0 ? ((totalSiPor1 * 100) / totalTotal): 0.0);
		String totalCalif = formatDecimales.format(totalCalif1);

		// ********* Calificacion y Pre Calificacion *********
		// Calculo de porcentajes por Zona
		// Porcentaje en relación al total Comunes
		// Calificación
		double porcentajeComTotal = (totalTotal>0 ? ((comTotal * 100) / totalTotal) : 0.0);
		double comCalif1 = (comTotal > 0 ? ((comResSi1 * porcentajeComTotal) / comTotal):0.0);
		comTotal = Double.parseDouble(formatDecimales.format(porcentajeComTotal));

		String comCalifAnt = formatDecimales.format(comCalif1);
		// Double comPonderacion = (Double.parseDouble((areasComunesObj.getAux()!=null ?
		// !areasComunesObj.getAux().equals("") ? areasComunesObj.getAux(): "0":"0")));

		// Cuanto vale la zona
		double comPonderacion = ponderacionComExt;
		// Calcular ponderación pondTotal (ponderacion SI)
		Double comPreCal = areasComunesObj.getPrecalif();
		
		String comCalif = formatDecimales.format(comPreCal);

		if (comResSi1 == 0 && comResNo1 == 0) {
			comGrupoCompleto = 0;
			comPonderacion = 0;
		}
		comTotalPon = formatDecimales.format(comPonderacion);
		comCalifCant = formatDecimales.format(comPonderacion > 0 ? (((comPreCal/comPonderacion)*100)):0.0);
		// Fin Porcentaje en relación al total Comunes

		// Porcentaje en relación al total BAZ
		double porcentajeBazTotal = (totalTotal > 0 ? ((bazTotal * 100) / totalTotal):0.0);
		double bazCalif1 = (bazTotal > 0 ? ((bazResSi1 * porcentajeBazTotal) / bazTotal):0.0);
		bazTotal = Double.parseDouble(formatDecimales.format(porcentajeBazTotal));
		
		String bazCalifAnt = formatDecimales.format(bazCalif1);
		// Double bazPonderacion = (Double.parseDouble((bazObj.getAux()!=null ?
		// !bazObj.getAux().equals("") ? bazObj.getAux(): "0":"0")));

		double bazPonderacion = ponderacionBazExt;
		// Calcular ponderación pondTotal (ponderacion SI)
		Double bazPreCal = bazObj.getPrecalif();

		String bazCalif = formatDecimales.format(bazPreCal);
		
		if (bazResSi1 == 0 && bazResNo1 == 0) {
			bazGrupoCompleto = 0;
			bazPonderacion = 0;
		}

		bazTotalPon = formatDecimales.format(bazPonderacion);
		bazCalifCant = formatDecimales.format(bazPonderacion > 0 ? (((bazPreCal/bazPonderacion)*100)):0.0);

		// Fin Porcentaje en relación al total BAZ

		// Porcentaje en relación al total EKT
		double porcentajeEktTotal = (totalTotal>0 ? ((ektResSi1 * 100) / totalTotal) : 0.0);
		double ektCalif1 = (ektTotal > 0 ? ((ektResSi1 * porcentajeEktTotal) / ektTotal):0.0);
		ektTotal = Double.parseDouble(formatDecimales.format(porcentajeEktTotal));

		String ektCalifAnt = formatDecimales.format(ektCalif1);
		// Double ektPonderacion = (Double.parseDouble((ektObj.getAux()!=null ?
		// !ektObj.getAux().equals("") ? ektObj.getAux(): "0":"0")));

		double ektPonderacion = ponderacionEktExt;
		// Calcular ponderación pondTotal (ponderacion SI)
		Double ektPreCal = ektObj.getPrecalif();

		String ektCalif = formatDecimales.format(ektPreCal);

		if (ektResSi1 == 0 && ektResNo1 == 0) {
			ektGrupoCompleto = 0;
			ektPonderacion = 0;
		}
		
		ektTotalPon = formatDecimales.format(ektPonderacion);
		ektCalifCant = formatDecimales.format(ektPonderacion > 0 ? (((ektPreCal/ektPonderacion)*100)):0.0);

		// Fin Porcentaje en relación al total EKT

		// Porcentaje en relación al total Generales
		double porcentajeGeneralesTotal = (totalTotal>0 ? ((genTotal * 100) / totalTotal):0.0);
		double genCalif1 = (genTotal>0 ? ((genResSi1 * porcentajeGeneralesTotal) / genTotal):0.0);
		genTotal = Double.parseDouble(formatDecimales.format(porcentajeGeneralesTotal));

		String genCalifAnt = formatDecimales.format(genCalif1);
		// Double generalesPonderacion =
		// (Double.parseDouble((generalesObj.getAux()!=null ?
		// !generalesObj.getAux().equals("") ? generalesObj.getAux(): "0":"0")));

		double generalesPonderacion = ponderacionGenExt;
		// Calcular ponderación pondTotal (ponderacion SI)
		Double generalesPreCal = generalesObj.getPrecalif();

		String genCalif = formatDecimales.format(generalesPreCal);

		if (genResSi1 == 0 && genResNo1 == 0) {
			genGrupoCompleto = 0;
			generalesPonderacion = 0;
		}

		genTotalPon = formatDecimales.format(generalesPonderacion);
		genCalifCant = formatDecimales.format(generalesPonderacion > 0 ? (((generalesPreCal/generalesPonderacion)*100)):0.0);

		// Fin Porcentaje en relación al total Generales

		// Porcentaje en relación al total PP
		double porcentajePrestaTotal = (totalTotal>0 ? ((prestaTotal * 100) / totalTotal):0.0);
		double prestaCalif1 = (prestaTotal>0 ? ((prestaResSi1 * porcentajePrestaTotal) / prestaTotal):0.0);
		prestaTotal = Double.parseDouble(formatDecimales.format(porcentajePrestaTotal));
		
		String prestaCalifAnt = formatDecimales.format(prestaCalif1);
		// Double prestaPrendaPonderacion =
		// (Double.parseDouble((prestaPrendaObj.getAux()!=null ?
		// !prestaPrendaObj.getAux().equals("") ? prestaPrendaObj.getAux(): "0":"0")));

		double prestaPrendaPonderacion = ponderacionPrestaExt;
		// Calcular ponderación pondTotal (ponderacion SI)
		Double prestaPrendaPreCal = prestaPrendaObj.getPrecalif();

		String prestaCalif = formatDecimales.format(prestaPrendaPreCal);

		if (prestaResSi1 == 0 && prestaResNo1 == 0) {
			prestaGrupoCompleto = 0;
			prestaPrendaPonderacion = 0;
		}

		prestaTotalPon = formatDecimales.format(prestaPrendaPonderacion);
		prestaCalifCant = formatDecimales.format(prestaPrendaPonderacion > 0 ? (((prestaPrendaPreCal/prestaPrendaPonderacion)*100)):0.0);

		// Fin Porcentaje en relación al total PP
		// Fin Calculo de porcentaje por Zona
		// Items
		totalItems = formatDecimales.format((comGrupoCompleto - areasComunesObj.getPregNa() + comSiHijas)
				+ (bazGrupoCompleto - bazObj.getPregNa() + bazSiHijas)
				+ (ektGrupoCompleto - ektObj.getPregNa() + ektSiHijas)
				+ (prestaGrupoCompleto - prestaPrendaObj.getPregNa() + prestaPrendaSiHijas)
				+ (genGrupoCompleto - generalesObj.getPregNa() + generalesSiHijas));
		// String totalItems = formatDecimales.format(comTotal + bazTotal + ektTotal +
		// prestaTotal + genTotal);
		itemsPorRevisar = formatDecimales.format(comResNo1 + bazResNo1 + ektResNo1 + prestaResNo1 + genResNo1);

		double itemAprobadosResta = ((((comGrupoCompleto - areasComunesObj.getPregNa()) - comResNo1) + comSiHijas)
				+ (((bazGrupoCompleto - bazObj.getPregNa()) - bazResNo1) + bazSiHijas)
				+ (((ektGrupoCompleto - ektObj.getPregNa()) - ektResNo1) + ektSiHijas)
				+ (((prestaGrupoCompleto - prestaPrendaObj.getPregNa()) - prestaResNo1) + prestaPrendaSiHijas)
				+ (((genGrupoCompleto - generalesObj.getPregNa()) - genResNo1) + generalesSiHijas));
		itemsAprobados = formatDecimales.format(itemAprobadosResta);

		//itemsNoConsiderados = formatDecimales.format(NAComunes + NABaz + NAEkt + NAPresta + NAGenerales);
		double sumaItemsRevisados = (comGrupoCompleto - areasComunesObj.getPregNa() + comSiHijas)
				+ (bazGrupoCompleto - bazObj.getPregNa() + bazSiHijas)
				+ (ektGrupoCompleto - ektObj.getPregNa() + ektSiHijas)
				+ (prestaGrupoCompleto - prestaPrendaObj.getPregNa() + prestaPrendaSiHijas)
				+ (genGrupoCompleto - generalesObj.getPregNa() + generalesSiHijas);
		
		itemsNoConsiderados = formatDecimales.format(943 - (sumaItemsRevisados));

		totalTotal = comPonderacion + bazPonderacion + ektPonderacion + generalesPonderacion + prestaPrendaPonderacion;

		// --- PONDERACIÓNES ---
		double ponderacionTotalObtenida = areasComunesObj.getPrecalif() + bazObj.getPrecalif() + ektObj.getPrecalif()
				+ generalesObj.getPrecalif() + prestaPrendaObj.getPrecalif();//
		double ponderacionTotalGrupos = comPonderacion + bazPonderacion + ektPonderacion + generalesPonderacion
				+ prestaPrendaPonderacion;
		
		double ponderacionObtenida = (ponderacionTotalGrupos >0 ? (((ponderacionTotalObtenida) * 100) / (ponderacionTotalGrupos)):0.0);
		double ponderacionTotal = (ponderacionTotalObtenida);
		totalCalifCant = formatDecimales.format((totalTotal > 0 ? (((ponderacionTotal/totalTotal)*100)):0.0));
		
		preCalificacionNum = ponderacionObtenida;
		String preCalificacion = formatDecimales.format(preCalificacionNum);
		String calificacion = "0.0";
		String colorCalificacion = "";
		String colorLetraCalificacion = "white";

		if (imperdonables!=null || imperdonables.size() == 0) {
			calificacion = preCalificacion;
			colorLetraCalificacion = "black";
		}

		colorCalificacion = getColorExpansion(Double.parseDouble(calificacion));

		if (colorCalificacion.contains("#000000")) {
			colorLetraCalificacion = "white";
		}

		graficaTotal = grafica(2, Double.parseDouble(totalSiPor), Double.parseDouble(totalNoPor));
		

	}
	////Nuevo metodo para OCC
	private void obtieneDatosOCC() {
		formatDecimales = new DecimalFormat("###.##");
		formatEnteros = new DecimalFormat("###.##");
		
		// Obtenemos Datos
		ChecklistPreguntasComBI checklistPreguntasComBI = (ChecklistPreguntasComBI) FRQAppContextProvider
				.getApplicationContext().getBean("checklistPreguntasComBI");

		// Obtiene La información anterior
		List<ChecklistPreguntasComDTO> listaGrupos = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> listaGruposExt = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> listaConteosHijas = new ArrayList<ChecklistPreguntasComDTO>();
		
		try {
			//listaGrupos = checklistPreguntasComBI.obtieneInfoCont(idBitacora);
			listaGrupos = checklistPreguntasComBI.obtieneInfoOCC(idBitacora);
		} catch (Exception e) {
			logger.info("AP al obtener al obtener obtieneInfoCont de bitacora: " + idBitacora);
		}
		try {
			//listaGruposExt = checklistPreguntasComBI.obtieneInfoContExt(idBitacora);
			listaGruposExt = checklistPreguntasComBI.obtieneComplemOCC(idBitacora);
		} catch (Exception e) {
			logger.info("AP al obtener al obtener obtieneInfoContExt de bitacora: " + idBitacora);
		}
		try {
			//listaConteosHijas = checklistPreguntasComBI.obtieneInfoContHij(idBitacora);
			listaConteosHijas = checklistPreguntasComBI.obtieneComplemSiNaOCC(idBitacora);

		} catch (Exception e) {
			logger.info("AP al obtener al obtener obtieneInfoContHij de bitacora: " + idBitacora);
		}
		
		for(int i=0; i<listaGrupos.size();i++) {
			if ( listaGrupos.get(i).getClasif().equalsIgnoreCase("CYC")){
				cYcObj = listaGrupos.get(i);
				
				cYcObjExt = listaGruposExt.get(i);
				ponderacionCyCExt = cYcObjExt.getSumGrupo();
				totalGeneralCyC = cYcObjExt.getTotGral();
				
				conteoHiijasCyC = listaConteosHijas.get(i);
				cYcSiHijas = conteoHiijasCyC.getPregSi();
				cYcNaHijas = conteoHiijasCyC.getPregNa();
			}
			if ( listaGrupos.get(i).getClasif().equalsIgnoreCase("AREAS")){
				areasComunesObj = listaGrupos.get(i);
				
				areasComunesObjExt = listaGruposExt.get(i);
				ponderacionComExt = areasComunesObjExt.getSumGrupo();
				totalGeneralComunes = areasComunesObjExt.getTotGral();
				
				conteoHijasComunes = listaConteosHijas.get(i);
				comSiHijas = conteoHijasComunes.getPregSi();
				comNaHijas = conteoHijasComunes.getPregNa();
			}
			if ( listaGrupos.get(i).getClasif().equalsIgnoreCase("GENERALES")){
				generalesObj = listaGrupos.get(i);
				
				generalesObjExt = listaGruposExt.get(i);
				ponderacionGenExt = generalesObjExt.getSumGrupo();
			 	totalGeneralGenerales = generalesObjExt.getTotGral();
				
		 		conteoHiijasGenerales = listaConteosHijas.get(i);
		 		generalesSiHijas = conteoHiijasGenerales.getPregSi();
		 		generalesNaHijas = conteoHiijasGenerales.getPregNa();
			}
		}
	}
	private void calculaGraficosOCC() {
		// 1.- GRÁFICA COMUNES ********
		// Cálculo Total de Preguntas
		// double comResNo1 = areasComunesObj.getPregNo();
		double comResNo1 = areasComunesObj.getPregNo();
		// NA
		double comResNa1 = areasComunesObj.getPregNa();
		//
		double comGrupoCompleto = areasComunesObj.getTotPreg();
		// double comResSi1 = areasComunesObj.getTotPreg() -
		// areasComunesObj.getPregNo();
		double comResSi1 = comSiHijas + (comGrupoCompleto - comResNa1) - comResNo1;

		// double comTotal = areasComunesObj.getTotPreg();
		double comTotal = comResSi1 + comResNo1 + comSiHijas;
		
		// PARA LOS NA FALTA SUMAR LO DESACTIVADO
		comResNa1 += (totalGeneralComunes - comTotal);

		double comSiPor1 = 0;
		double comNoPor1 = 0;

		if (comTotal != 0) {
			comSiPor1 = (comResSi1 / (comResSi1 + comResNo1)) * 100;
			comNoPor1 = (comResNo1 / (comResSi1 + comResNo1)) * 100;
		}
		

		
		double NAComunes = 0;

		if (areasComunesObj.getPregSi() != 0 && areasComunesObj.getPregNo() != 0 ) {
			NAComunes = (totalGeneralComunes - (areasComunesObj.getPregNo() + areasComunesObj.getPregSi()));
			NAComunes = NAComunes - (comSiHijas);
		}
		
		// Calculo Por Ponderación
		// double comPorcentajePonderacion = (areasComunesObj.getPrecalif() * 100) /
		// areasComunesObj.getSumGrupo();
		double comPorcentajePonderacion = (ponderacionComExt > 0 ? ((areasComunesObj.getPrecalif() * 100) / ponderacionComExt):0.0);
		
		int comunesImperdonables = areasComunesObj.getAux2();
		logoCom = getLogoExpansion(comPorcentajePonderacion, comunesImperdonables);

		// Cantidad Tabla (Si-No)
		if (areasComunesObj.getPregSi() == 0 && areasComunesObj.getPregNo() == 0 && comSiHijas == 0) {
			comResSi1 = 0;
			comResNo1 = 0;
			comSiPor1 = 0;
			comNoPor1 = 0;
		}
		
		comResSi = formatEnteros.format(comResSi1);
		comResNo = formatEnteros.format(comResNo1);

		// Porcentaje Tabla y Gráfica
		comSiPor = formatDecimales.format(comSiPor1);
		comNoPor = formatDecimales.format(comNoPor1);

		if (comResSi1 == 0 && comResNo1 == 0) {
			graficaComunes = grafica(1, 0, 100);
			logoCom = getLogoExpansion(100, 0);
		} else {
			graficaComunes = grafica(1, Double.parseDouble(comSiPor), Double.parseDouble(comNoPor));
		}

		// GRÁFICA COMUNES ********
		
		// 2.- GRÁFICA CYC ********
		// double bazResNo1 = bazObj.getPregNo();
		double cYcResNo1 = cYcObj.getPregNo();
		// NA
		double cYcResNa1 = cYcObj.getPregNa();
		//
		double cYcGrupoCompleto = cYcObj.getTotPreg();
		// double bazResSi1 = bazObj.getTotPreg() - bazObj.getPregNo();
		double cYcResSi1 = cYcSiHijas + (cYcGrupoCompleto - cYcResNa1) - cYcResNo1;

		// double bazTotal = bazObj.getTotPreg();
		double cYcTotal = cYcResSi1 + cYcResNo1 + cYcSiHijas;

		// PARA LOS NA FALTA SUMAR LO DESACTIVADO
		cYcResNa1 += (totalGeneralCyC - cYcTotal);
		
		double cYcSiPor1 = 0;
		double cYcNoPor1 = 0;

		if (cYcTotal != 0) {
			cYcSiPor1 = (cYcResSi1 / (cYcResSi1 + cYcResNo1)) * 100;
			cYcNoPor1 = (cYcResNo1 / (cYcResSi1 + cYcResNo1)) * 100;
		}

		
		double NAcYc = 0.0;
		
		if (cYcObj.getPregSi() != 0 && cYcObj.getPregNo() != 0 ) {
			NAcYc = (totalGeneralCyC - (cYcObj.getPregNo() + cYcObj.getPregSi()));
			NAcYc = NAcYc - (cYcSiHijas);
		}
		// Calculo Por Ponderación
		// double bazPorcentajePonderacion = (bazObj.getPrecalif() * 100) /
		// bazObj.getSumGrupo();
		double cYcPorcentajePonderacion = (ponderacionCyCExt>0 ? ((cYcObj.getPrecalif() * 100) / ponderacionCyCExt) : 0.0);

		int cYcImperdonables = cYcObj.getAux2();
		logoCyC = getLogoExpansion(cYcPorcentajePonderacion, cYcImperdonables);

		// Cantidad Tabla (Si-No)
		if (cYcObj.getPregSi() == 0 && cYcObj.getPregNo() == 0 && cYcSiHijas == 0) {
			cYcResSi1 = 0;
			cYcResNo1 = 0;
			cYcSiPor1 = 0;
			cYcNoPor1 = 0;
		}
		
		cYcResSi = formatEnteros.format(cYcResSi1);
		cYcResNo = formatEnteros.format(cYcResNo1);

		// Porcentaje Tabla y Gráfica
		cYcSiPor = formatDecimales.format(cYcSiPor1);
		cYcNoPor = formatDecimales.format(cYcNoPor1);

		if (cYcResSi1 == 0 && cYcResNo1 == 0) {
			graficaCyC = grafica(1, 0, 100);
			logoCyC = getLogoExpansion(100, 0);

		} else {
			graficaCyC = grafica(1, Double.parseDouble(cYcSiPor), Double.parseDouble(cYcNoPor));
		}

	

		// 3.- GRÁFICA GENERALES ********
		// double genResNo1 = generalesObj.getPregNo();
		double genResNo1 = generalesObj.getPregNo();
		// NA
		double genResNa1 = generalesObj.getPregNa();
		//
		double genGrupoCompleto = generalesObj.getTotPreg();

		// double genResSi1 = generalesObj.getTotPreg() - generalesObj.getPregNo();
		double genResSi1 = generalesSiHijas + (genGrupoCompleto - genResNa1) - genResNo1;

		// double genTotal = generalesObj.getTotPreg();
		double genTotal = genResSi1 + genResNo1 + generalesSiHijas;

		// PARA LOS NA FALTA SUMAR LO DESACTIVADO
		genResNa1 += (totalGeneralGenerales - genTotal);

		double genSiPor1 = 0;
		double genNoPor1 = 0;

		if (genTotal != 0) {
			genSiPor1 = (genResSi1 / (genResSi1 + genResNo1)) * 100;
			genNoPor1 = (genResNo1 / (genResSi1 + genResNo1)) * 100;
		}
		

		double NAGenerales = 0.0;
		
		if (generalesObj.getPregSi() != 0 && generalesObj.getPregNo() != 0 ) {
			NAGenerales = (totalGeneralBaz - (generalesObj.getPregNo() + generalesObj.getPregSi()));
			NAGenerales = NAGenerales - (generalesSiHijas);
		}
		
		// Calculo Por Ponderación
		// double generalesPorcentajePonderacion = (generalesObj.getPrecalif() * 100) /
		// generalesObj.getSumGrupo();
		double generalesPorcentajePonderacion = (ponderacionGenExt > 0 ? ((generalesObj.getPrecalif() * 100) / ponderacionGenExt):0.0);

		int generalesImperdonables = generalesObj.getAux2();
		logoGen = getLogoExpansion(generalesPorcentajePonderacion, generalesImperdonables);

		// Cantidad Tabla (Si-No)
		if (generalesObj.getPregSi() == 0 && generalesObj.getPregNo() == 0 && generalesSiHijas == 0) {
			genResSi1 = 0;
			genResNo1 = 0;
			genSiPor1 = 0;
			genNoPor1 = 0;
		}

		genResSi = formatEnteros.format(genResSi1);
		genResNo = formatEnteros.format(genResNo1);

		// Porcentaje Tabla y Gráfica
		genSiPor = formatDecimales.format(genSiPor1);
		genNoPor = formatDecimales.format(genNoPor1);
		
		if (genResSi1 == 0 && genResNo1 == 0) {
			graficaGenerales = grafica(1, 0, 100);
			logoGen = getLogoExpansion(100, 0);

		} else {
			graficaGenerales = grafica(1, Double.parseDouble(genSiPor), Double.parseDouble(genNoPor));
		}
		
		// GRÁFICA GENERALES ********
		
		// 4.- VALORES TOTAL ********
		// double totalResSi1 = (totalTotal - totalResNo1);
		double totalResNo1 = (areasComunesObj.getPregNo() + cYcObj.getPregNo() + generalesObj.getPregNo() );
		// NA
		double totalResNa1 = (areasComunesObj.getPregNa() + cYcObj.getPregNa() + generalesObj.getPregNa() );

		double totalResSi1 = (comResSi1 + cYcResSi1  + genResSi1 );

		totalTotal = (totalResNo1 + totalResSi1);

		double totalSiPor1 = 0;
		double totalNoPor1 = 0;

		if (totalTotal != 0) {
			totalSiPor1 = (totalResSi1 / totalTotal) * 100;
			totalNoPor1 = (totalResNo1 / totalTotal) * 100;
		}

		// Calculo Por Ponderación
		// double totalPrecalif = (areasComunesObj.getPrecalif() + bazObj.getPrecalif()
		// + ektObj.getPrecalif() + generalesObj.getPrecalif() +
		// prestaPrendaObj.getPrecalif());
		double totalPrecalif = (areasComunesObj.getPonTot() + cYcObj.getPonTot() + generalesObj.getPonTot() );
		double totalSumGrupo = (ponderacionComExt + ponderacionCyCExt  + ponderacionGenExt);
		double tPorcentajePonderacion = (totalPrecalif * 100) / totalSumGrupo;

		int tImperdonables = (areasComunesObj.getAux2() + cYcObj.getAux2() + generalesObj.getAux2());
		logoTotal = getLogoExpansion(tPorcentajePonderacion, tImperdonables);
		
		
		// Cantidad Tabla (Si-No)
		totalResSi = formatEnteros.format(totalResSi1);
		totalResNo = formatEnteros.format(totalResNo1);

		// Porcentaje Tabla y Gráfica
		totalSiPor = formatDecimales.format(totalSiPor1);
		totalNoPor = formatDecimales.format(totalNoPor1);

		// ********* Calificación *********
		// double totalCalif1 = (totalSiPor1 * totalValorTotal) / 100;
		double totalCalif1 = (totalTotal>0 ? ((totalSiPor1 * 100) / totalTotal): 0.0);
		String totalCalif = formatDecimales.format(totalCalif1);

		// ********* Calificacion y Pre Calificacion *********
		// Calculo de porcentajes por Zona
		// Porcentaje en relación al total Comunes
		// Calificación
		double porcentajeComTotal = (totalTotal>0 ? ((comTotal * 100) / totalTotal) : 0.0);
		double comCalif1 = (comTotal > 0 ? ((comResSi1 * porcentajeComTotal) / comTotal):0.0);
		comTotal = Double.parseDouble(formatDecimales.format(porcentajeComTotal));

		String comCalifAnt = formatDecimales.format(comCalif1);
		// Double comPonderacion = (Double.parseDouble((areasComunesObj.getAux()!=null ?
		// !areasComunesObj.getAux().equals("") ? areasComunesObj.getAux(): "0":"0")));

		// Cuanto vale la zona
		double comPonderacion = ponderacionComExt;
		// Calcular ponderación pondTotal (ponderacion SI)
		Double comPreCal = areasComunesObj.getPrecalif();
		
		String comCalif = formatDecimales.format(comPreCal);

		if (comResSi1 == 0 && comResNo1 == 0) {
			comGrupoCompleto = 0;
			comPonderacion = 0;
		}
		comTotalPon = formatDecimales.format(comPonderacion);
		comCalifCant = formatDecimales.format(comPonderacion > 0 ? (((comPreCal/comPonderacion)*100)):0.0);
		// Fin Porcentaje en relación al total Comunes

		// Porcentaje en relación al total CYC
		double porcentajeCyCTotal = (totalTotal > 0 ? ((cYcTotal * 100) / totalTotal):0.0);
		double cYcCalif1 = (cYcTotal > 0 ? ((cYcResSi1 * porcentajeCyCTotal) / cYcTotal):0.0);
		cYcTotal = Double.parseDouble(formatDecimales.format(porcentajeCyCTotal));
		
		String cYcCalifAnt = formatDecimales.format(cYcCalif1);
		// Double bazPonderacion = (Double.parseDouble((bazObj.getAux()!=null ?
		// !bazObj.getAux().equals("") ? bazObj.getAux(): "0":"0")));

		double cYcPonderacion = ponderacionCyCExt;
		// Calcular ponderación pondTotal (ponderacion SI)
		Double cYcPreCal = cYcObj.getPrecalif();

		String cYcCalif = formatDecimales.format(cYcPreCal);
		
		if (cYcResSi1 == 0 && cYcResNo1 == 0) {
			cYcGrupoCompleto = 0;
			cYcPonderacion = 0;
		}

		cYcTotalPon = formatDecimales.format(cYcPonderacion);
		cYcCalifCant = formatDecimales.format(cYcPonderacion > 0 ? (((cYcPreCal/cYcPonderacion)*100)):0.0);

		// Fin Porcentaje en relación al total BAZ

		// Porcentaje en relación al total Generales
		double porcentajeGeneralesTotal = (totalTotal>0 ? ((genTotal * 100) / totalTotal):0.0);
		double genCalif1 = (genTotal>0 ? ((genResSi1 * porcentajeGeneralesTotal) / genTotal):0.0);
		genTotal = Double.parseDouble(formatDecimales.format(porcentajeGeneralesTotal));

		String genCalifAnt = formatDecimales.format(genCalif1);
		// Double generalesPonderacion =
		// (Double.parseDouble((generalesObj.getAux()!=null ?
		// !generalesObj.getAux().equals("") ? generalesObj.getAux(): "0":"0")));

		double generalesPonderacion = ponderacionGenExt;
		// Calcular ponderación pondTotal (ponderacion SI)
		Double generalesPreCal = generalesObj.getPrecalif();

		String genCalif = formatDecimales.format(generalesPreCal);

		if (genResSi1 == 0 && genResNo1 == 0) {
			genGrupoCompleto = 0;
			generalesPonderacion = 0;
		}

		genTotalPon = formatDecimales.format(generalesPonderacion);
		genCalifCant = formatDecimales.format(generalesPonderacion > 0 ? (((generalesPreCal/generalesPonderacion)*100)):0.0);


		// Fin Calculo de porcentaje por Zona
		// Items
		totalItems = formatDecimales.format((comGrupoCompleto - areasComunesObj.getPregNa() + comSiHijas)
				+ (cYcGrupoCompleto - cYcObj.getPregNa() + cYcSiHijas)
				+ (genGrupoCompleto - generalesObj.getPregNa() + generalesSiHijas));
		// String totalItems = formatDecimales.format(comTotal + bazTotal + ektTotal +
		// prestaTotal + genTotal);
		itemsPorRevisar = formatDecimales.format(comResNo1 + cYcResNo1 + genResNo1);

		double itemAprobadosResta = ((((comGrupoCompleto - areasComunesObj.getPregNa()) - comResNo1) + comSiHijas)
				+ (((cYcGrupoCompleto - cYcObj.getPregNa()) - cYcResNo1) + cYcSiHijas)
				+ (((genGrupoCompleto - generalesObj.getPregNa()) - genResNo1) + generalesSiHijas));
		itemsAprobados = formatDecimales.format(itemAprobadosResta);

		//itemsNoConsiderados = formatDecimales.format(NAComunes + NAcYc  + NAGenerales);
		double sumaItemsRevisados = (comGrupoCompleto - areasComunesObj.getPregNa() + comSiHijas)
				+ (cYcGrupoCompleto - cYcObj.getPregNa() + cYcSiHijas)
				+ (genGrupoCompleto - generalesObj.getPregNa() + generalesSiHijas);
		
		itemsNoConsiderados = formatDecimales.format(416 - (sumaItemsRevisados));
		
		totalTotal = comPonderacion + cYcPonderacion  + generalesPonderacion ;

		// --- PONDERACIÓNES ---
		double ponderacionTotalObtenida = areasComunesObj.getPrecalif() + cYcObj.getPrecalif() + generalesObj.getPrecalif();//
		double ponderacionTotalGrupos = comPonderacion + cYcPonderacion + generalesPonderacion;
		
		double ponderacionObtenida = (ponderacionTotalGrupos >0 ? (((ponderacionTotalObtenida) * 100) / (ponderacionTotalGrupos)):0.0);
		double ponderacionTotal = (ponderacionTotalObtenida);
		totalCalifCant = formatDecimales.format((totalTotal > 0 ? (((ponderacionTotal/totalTotal)*100)):0.0));
		
		preCalificacionNum = ponderacionObtenida;
		String preCalificacion = formatDecimales.format(preCalificacionNum);
		String calificacion = "0.0";
		String colorCalificacion = "";
		String colorLetraCalificacion = "white";

		if (imperdonables!=null || imperdonables.size() == 0) {
			calificacion = preCalificacion;
			colorLetraCalificacion = "black";
		}

		colorCalificacion = getColorExpansion(Double.parseDouble(calificacion));

		if (colorCalificacion.contains("#000000")) {
			colorLetraCalificacion = "white";
		}

		graficaTotal = grafica(2, Double.parseDouble(totalSiPor), Double.parseDouble(totalNoPor));
		

	}
	////
	
	private void obtieneDatosAperturables() {
		numeroDeCalificacion = (preCalificacionNum);
		numImperdonablesRecorrido = imperdonables.size();
		colorTexto = "white";

		if (numImperdonablesRecorrido > 0) {
			// Tiene imperdonables el color es NEGRO, NO OPERABLE Y SIN RECEPCIÓN, letra
			// blanca
			operable = "NO OPERABLE ";
			operable += "";
			colorOperable = "#000";
			colorTexto = "white";
			colorBaner = 0; //blanco
		} else {

			if (numeroDeCalificacion >= 100) {
				operable = "OPERABLE ";
				operable += " (Con recepción)";
				// VERDE
				colorOperable = "#46ad35";
				colorTexto = "white";
				colorBaner = 0; //blanco
			} else if (numeroDeCalificacion >= 70 && numeroDeCalificacion <= 99) {
				operable = "OPERABLE ";
				operable += " (Sin recepción)";
				// AMARILLO
				colorOperable = "#F4C431";
				colorTexto = "black";
				colorBaner = 1; //negro
			} else if (numeroDeCalificacion >= 50 && numeroDeCalificacion <= 69) {
				operable = "NO OPERABLE ";
				operable += " ";
				// ROJO
				colorOperable = "#FE003D";
				colorTexto = "white";
				colorBaner = 0; //blanco
			} else if (numeroDeCalificacion <= 49) {
				operable = "NO OPERABLE ";
				operable += "";
				// NEGRO
				colorOperable = "#000";
				colorTexto = "white";
				colorBaner = 0; //blanco
			}
		}
	}
	
	private String getLogoExpansion(double num, int imperdonable) {

		if (imperdonable > 0) {

			return "vt01.png";

		} else {

			if (num >= 90) {

				return "vt03.png";

			} else if (num >= 70) {

				return "vt04.png";

			} else if (num >= 51) {

				return "vt02.png";

			} else {

				return "vt01.png";

			}

		}

	}

	public String getColorExpansion(double num) {

		if (num >= 90) {

			return "#46ad35";

		} else if (num >= 70) {

			return "#F4C431";

		} else if (num >= 51) {

			return "#FE003D";

		} else {

			return "#000";

		}

	}
	
	private File grafica(int tipo, double aprobados, double porAtender) {
		// Tipo 1 Backround Blanco - Tipo 2 Backroung Gris
		Color gris = new Color(240, 239, 240);

		DefaultPieDataset dataset = new DefaultPieDataset();
		dataset.setValue("Items Aprobados", aprobados);
		dataset.setValue("Items Por Atender", porAtender);

		RingPlot plot = new RingPlot(dataset);

		Color vrd = new Color(227, 227, 227);
		Color rojo = new Color(142, 142, 142);

		plot.setSectionPaint("Items Aprobados", vrd);
		plot.setSectionPaint("Items Por Atender", rojo);

		JFreeChart chart = new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, plot, true);

		// Fondo de la Gráfica
		if (tipo == 1) {
			chart.getPlot().setBackgroundPaint(Color.white);
		} else {
			chart.getPlot().setBackgroundPaint(gris);
		}

		// plot.setCenterTextMode(CenterTextMode.VALUE);
		Font font1 = new Font(null, Font.BOLD, 45);
		plot.setCenterTextMode(CenterTextMode.FIXED);
		plot.setCenterText(aprobados + "%");

		plot.setCenterTextFont(font1);
		plot.setCenterTextColor(Color.BLACK);

		plot.setOutlineVisible(false);
		plot.setSectionDepth(0.35);
		// plot.setSectionOutlinesVisible(true);
		// plot.setSimpleLabels(true);
		plot.setOuterSeparatorExtension(0);
		plot.setInnerSeparatorExtension(0);

		Font font = new Font("FONT", 0, 22);
		plot.setLabelFont(font);

		chart.getLegend().setFrame(BlockBorder.NONE);
		// chart.getLegend().setPosition(RectangleEdge.);

		// FONDO DE LA GRÁFICA
		if (tipo == 1) {
			chart.setBackgroundPaint(java.awt.Color.white);
			plot.setLabelBackgroundPaint(Color.white);
		} else {
			chart.setBackgroundPaint(gris);
			plot.setLabelBackgroundPaint(gris);

		}

		return setGraficaTemp(chart);
	}
	
	private File setGraficaTemp(JFreeChart chart) {

		File temp = null;
		// create a temp file
		try {
			temp = File.createTempFile("grafica", ".png");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		BufferedImage chartImage = chart.createBufferedImage(600, 400, null);

		try {
			ImageIO.write(chartImage, "png", temp);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return temp;

	}

	public String obtenGrafica(String clasif) {
		String retorno="";
		if(clasif.equalsIgnoreCase("EKT")) {
		
			retorno = generaGrafica(logoEkt,clasif,ektTotalPon,graficaElektra,ektCalifCant,ektResSi,ektSiPor,ektResNo,ektNoPor);
		}
		if(clasif.equalsIgnoreCase("AREAS")) {
			retorno = generaGrafica(logoCom,clasif,comTotalPon,graficaComunes,comCalifCant,comResSi,comSiPor,comResNo,comNoPor);
		}
		if(clasif.equalsIgnoreCase("BAZ")) {
			retorno = generaGrafica(logoBaz,clasif,bazTotalPon,graficaBanco,bazCalifCant,bazResSi,bazSiPor,bazResNo,bazNoPor);
		}
		if(clasif.equalsIgnoreCase("GENERALES")) {
			retorno = generaGrafica(logoGen,clasif,genTotalPon,graficaGenerales,genCalifCant,genResSi,genSiPor,genResNo,genNoPor);
		}
		if(clasif.equalsIgnoreCase("PP")) {
			retorno = generaGrafica(logoPres,clasif,prestaTotalPon,graficaPresta,prestaCalifCant,prestaResSi,prestaSiPor,prestaResNo,prestaNoPor);
		}
		if(clasif.equalsIgnoreCase("CYC")) {
			retorno = generaGrafica(logoCyC,clasif,cYcTotalPon,graficaCyC,cYcCalifCant,cYcResSi,cYcSiPor,cYcResNo,cYcNoPor);
		}
		if(clasif.equalsIgnoreCase("TOTALES")) {
			retorno = generaGrafica(logoTotal,clasif,""+totalTotal,graficaTotal,totalCalifCant,totalResSi,totalSiPor,totalResNo,totalNoPor);
		}
		
		
		
		
		return retorno;
	}
	
	private String clasiffImg(String clasiff) {
		String clasifs="";
		if(clasiff.equalsIgnoreCase("EKT")) {
			clasifs = "zonaElektra.png";
		}
		if(clasiff.equalsIgnoreCase("AREAS")) {
			clasifs = "zonaAreasComunes.png";
		}
		if(clasiff.equalsIgnoreCase("BAZ")) {
			clasifs = "zonaBAZ.png";
		}
		if(clasiff.equalsIgnoreCase("GENERALES")) {
			clasifs = "zonaGenerales.png";
		}
		if(clasiff.equalsIgnoreCase("PP")) {
			clasifs = "zonaPrestaPrenda.png";
		}
		if(clasiff.equalsIgnoreCase("CYC")) {
			clasifs = "zonaBAZ.png";
		}
		return clasifs;
	}
	
	private String generaGrafica(String logo, String clasif, String totalPon, File grafica,
			String califCant,String resSi,String siPor,String resNo,String noPor) {
		String clasifs = "";
		String clasiff = "";
		int totales=0;
		clasifs = clasiffImg(clasif);
		if(clasif.equalsIgnoreCase("EKT")) {
			clasiff = "Elektra &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		}
		if(clasif.equalsIgnoreCase("AREAS")) {
			clasiff = "Áreas comunes &nbsp;";
		}
		if(clasif.equalsIgnoreCase("BAZ")) {
			clasiff = "Banco Azteca &nbsp;&nbsp;";
		}
		if(clasif.equalsIgnoreCase("GENERALES")) {
			clasiff = "Generales &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		}
		if(clasif.equalsIgnoreCase("PP")) {
			clasiff = "Presta Prenda";
		}
		if(clasif.equalsIgnoreCase("CYC")) {
			clasiff = "Credito Y Cobranza";
		}
		if(clasif.equalsIgnoreCase("TOTALES")) {
			clasiff = "Totales &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			totales=1;
		}
		
		
		String cuadroGrafica="";

		if(totales==0)
			cuadroGrafica+="<div style='width:100%; border: .25px solid #F0EFF0;'>" +
			// Titulo
			"<br></br>" + "<div>" + "<b><font size=\"1\" >"+
			
			"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='"+direccion+"/franquicia/firmaChecklist/"+clasifs+"' width='15' />"+
			"&nbsp; "+clasiff

			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" /*height=10' height='10' */
			+ "<img src='"+direccion+"/franquicia/firmaChecklist/" + logo + "' width='35'/>"
			+ "<br></br>"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "Total: " + formatDecimales.format((Double.parseDouble(totalPon))) + "</font></b>" + "</div>";
		else {	
			cuadroGrafica+="<div style='width:100%; background-color:#F0EFF0; border: 1px;'>" +
			// Titulo
			"<br></br>" + "<div>" + "<b><font size=\"1\" >"+
			
			"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
			"&nbsp; "+clasiff

			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" /*height=10' height='10' */
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "<img src='"+direccion+"/franquicia/firmaChecklist/" + logo + "' width='35'/>"
			+ "<br></br>"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "Total: " + formatDecimales.format((Double.parseDouble(totalPon))) + "</font></b>" + "</div>";
		}	
		cuadroGrafica+=
				// Div Imagen
				"<div>" + "<div style='float: left; '>" + "<img src='" + grafica.getAbsolutePath()
				+ "' width='130px' height='90px' />" + "</div>" +

				// Mini tabla
				
				"<br></br><br></br>"
				
				+"<div style='float: right; width: 20px; bottom:0; padding-top: 0px; '>" +

				"<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
				+ "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"
				+ "<th style='text-align:center; font-size: 5px;'>items</th>"
				+ "<th style='text-align:center; font-size: 5px;' >items %</th>"
				+ "<th style='text-align:center; font-size: 5px;'>Calificación</th>" + "</tr>" + "<tr>"
				+ "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='"+direccion+"/franquicia/firmaChecklist/ItemsAprovados02.png' width='10' height='10'/> </td>"
				+ "<td style='font-size: 10px;'>" + resSi + "</td>" + "<td style='font-size: 10px;'>" + siPor + "</td>"
				+ "<td rowspan='2' style='background-color:#F0EFF0; font-size: 10px;'>"
				+ califCant + "</td>" + "</tr>" + "<tr>"
				+ "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='"+direccion+"/franquicia/firmaChecklist/ItemsAtender02.png' width='10' height='10'/> </td>"
				+ "<td style='font-size: 10px;'>" + resNo + "</td>" + "<td style='font-size: 10px;'>" + noPor + "</td>" + "</tr>" + "</table>" +

				"</div>" +

				"</div>" +
				" </div>";
		return cuadroGrafica;
	}

	public String generaFirma(FirmaCheckDTO firma) {
		String firmaImg="";

		firmaImg+="<img style=\"width: 45px; height: 45px;\" src=\""+direccion+firma.getRuta()+"\"/>";
		
		return firmaImg;
	}
	
	public String generaImagenBaner() {
		String imagenes ="";
		String ruta1="";
		String ruta2="";
		
		if(colorBaner==1) {//http://10.53.33.82/franquicia/firmaChecklist/BAZ_TRANS_NEGRO.png
			ruta1 = direccion+"/franquicia/firmaChecklist/EKT_TRANS_NEGRO.png";
			ruta2 = direccion+"/franquicia/firmaChecklist/BAZ_TRANS_NEGRO.png";
		}
		if(colorBaner==0) {
			ruta1 = direccion+"/franquicia/firmaChecklist/EKT_TRANS_BLANCO.png";
			ruta2 = direccion+"/franquicia/firmaChecklist/BAZ_TRANS_BLANCO.png";
		}
		
		 imagenes="<img src=\""+ruta1+"\" style=\"height: 25%;\"></img> &nbsp;&nbsp;" + 
				  "<img src=\""+ruta2+"\" style=\"height: 25%;\"></img>";
		 return imagenes;
	}
	
	//proceso para generar pdf imperdonables
	public String cadenaImperdonables(String idBitacora) { 
		imperdonables = new ArrayList<>();
		ChecklistPreguntasComBI checklistPreguntasComBI = (ChecklistPreguntasComBI) FRQAppContextProvider
				.getApplicationContext().getBean("checklistPreguntasComBI");

		List<ChecklistPreguntasComDTO> chklistPreguntasImp  = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> listaResult  = new ArrayList<ChecklistPreguntasComDTO>();
		listaResult.clear();


		chklistPreguntasImp = checklistPreguntasComBI.obtieneInfoImp(Integer.parseInt(idBitacora));
		for (int i = 0; i < chklistPreguntasImp.size() ; i++) {
			if (chklistPreguntasImp.get(i).getPregPadre() == 0 && chklistPreguntasImp.get(i).getIdcritica() == 1 ) {
				for (int j = 0; j < chklistPreguntasImp.size() ; j++) {
					if ( chklistPreguntasImp.get(i).getIdPreg() == chklistPreguntasImp.get(j).getPregPadre() ) {
						ChecklistPreguntasComDTO data = chklistPreguntasImp.get(j);
						data.setPregunta(chklistPreguntasImp.get(i).getPregunta()+ " : "+ chklistPreguntasImp.get(j).getPregunta());
						imperdonables.add(data.getPregunta());
						listaResult.add(data);
					}
				}
			}
		}

		String imperdonablesHtml = "<html><body>";
		imperdonablesHtml  += "<div style='position: fixed; top: 10px; right: 30px; left: 30px; bottom: 10px; background: white; border-radius: 30px; border-style: solid;  border-color: gray; border-width: 1px;  padding: 20px;  text-align: right;  overflow-y: scroll;'>";
		imperdonablesHtml += "<div style = 'background-color: #ECEBEB;float:right; width:100%;'><img src='"+direccion+"/franquicia/firmaChecklist/LogoElektra.png' height='30px' style='float: right;'></img>&nbsp;&nbsp;<img src='"+direccion+"/franquicia/firmaChecklist/LogoBAZ.png' height='30px' style='float: right;'></img></div> <br></br> <br></br> <br></br> <div style='text-align: left;background-color: #ECEBEB; padding-top: .5px; padding-bottom: .5px;' ><p > <b>&nbsp;&nbsp;Detalles Imperdonables</b>  | Entrega de Sucursal</p></div> <br></br>"; 
		imperdonablesHtml += " <div style='background-color: #C82663; padding-top: .5px; padding-bottom: .5px;  text-align: center;'><h1 style='color: white'>"+((listaResult!=null && listaResult.size()>0)?listaResult.size():0)+"</h1></div> <br></br>";
		if(listaResult!=null && listaResult.size()>0) {
			imperdonablesHtml += "<table  style='width: 100%; text-align: left'>";
			for (int i = 0; i < listaResult.size(); i++) {
			      imperdonablesHtml += "<tr>";
			      imperdonablesHtml += "<td style='text-align: left; padding-left: 10px; border-bottom: 1px solid gray; '>" + (i + 1) + ".- " + listaResult.get(i).getPregunta() + "</td>";
			      imperdonablesHtml += "<td style='border-bottom: 1px solid gray;'><div width='60'height='60' ><img src='"+ direccion+listaResult.get(i).getRuta() + "' width='60'height='60'></img></div></td>";
			      //imperdonablesHtml += "<td style='border-bottom: 1px solid gray;'><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='60'height='60'/></td>";
			      imperdonablesHtml += "</tr>";
		    }
			imperdonablesHtml += " </table>";
		}
	    imperdonablesHtml += "</div>";
	    imperdonablesHtml += "</body></html>";
	    logger.info(imperdonablesHtml);

		    
		return imperdonablesHtml;

	}
	
	public String paginaIndice() {
		String retorno = "<html><head></head><body><div><br></br>" + 
				"<div style=\"background-color: #000; padding-top: 20px; padding-bottom: 40px;\">" + 
				"  <div style=\"width: 55%;float: left;\">" + 
				"  <font style=\"text-align: center;padding: 10px;\" size=\"3\">" + 
				"  <b style=\"color: white;\">&nbsp;&nbsp;Acta de Entrega de Sucursal</b></font>" + 
				"  </div><div style=\"text-align: right;width: 45%;float: left;\">" + 
				"      <img src=\""+direccion+"/franquicia/firmaChecklist/EKT_TRANS_BLANCO.png\" height=\"30px\"> </img>&nbsp;&nbsp;" + 
				"      <img src=\""+direccion+"/franquicia/firmaChecklist/BAZ_TRANS_BLANCO.png\" height=\"30px\"> </img>" + 
				"  </div>" + 
				"</div>" + 
				"  <br></br><br></br>" + 
				"<div style=\"margin-left: 10%;\">" + 
				"  <table cellspacing=\"10\" cellpadding=\"10\">" + 
				"    <thead>\n" + 
				"      <tr><th><b>Índice</b></th></tr>" + 
				"    </thead><tbody>" + 
				"      <tr>" + 
				"        <td>1. Detalles Generales de la Entrega de Sucursal </td>" + 
				"      </tr><tr>" + 
				"        <td>2. Detalles de Imperdonables</td>" + 
				"      </tr><tr>" + 
				"        <td>3. Detalles de Calificación Elektra</td>" + 
				"      </tr><tr>" + 
				"        <td>4. Detalles de Calificación Banco Azteca</td>" + 
				"      </tr><tr>" + 
				"        <td>5. Detalles de Calificación Presta Prenda</td>" + 
				"      </tr><tr>" + 
				"        <td>6. Detalles de Calificación Áreas Comunes</td>" + 
				"      </tr><tr>" + 
				"        <td>7. Detalles de Calificación Generales</td>" + 
				"      </tr>" + 
				"    </tbody>" + 
				"  </table>" + 
				"</div> <br></br></div></body></html>";
		logger.info(retorno);
		return retorno;
	}
	public String paginaIndiceOCC() {
		String retorno = "<html><head></head><body><div><br></br>" + 
				"<div style=\"background-color: #000; padding-top: 20px; padding-bottom: 40px;\">" + 
				"  <div style=\"width: 55%;float: left;\">" + 
				"  <font style=\"text-align: center;padding: 10px;\" size=\"3\">" + 
				"  <b style=\"color: white;\">&nbsp;&nbsp;Acta de Entrega de Sucursal</b></font>" + 
				"  </div><div style=\"text-align: right;width: 45%;float: left;\">" + 
				"      <img src=\"//franquicia/firmaChecklist/EKT_TRANS_BLANCO.png\" height=\"30px\"> </img>&nbsp;&nbsp;" + 
				"      <img src=\"//franquicia/firmaChecklist/BAZ_TRANS_BLANCO.png\" height=\"30px\"> </img>" + 
				"  </div>" + 
				"</div>" + 
				"  <br></br><br></br>" + 
				"<div style=\"margin-left: 10%;\">" + 
				"  <table cellspacing=\"10\" cellpadding=\"10\">" + 
				"    <thead>\n" + 
				"      <tr><th><b>Índice</b></th></tr>" + 
				"    </thead><tbody>" + 
				"      <tr>" + 
				"        <td>1. Detalles Generales de la Entrega de Sucursal </td>" + 
				"      </tr><tr>" + 
				"        <td>2. Detalles de Imperdonables</td>" + 
				"      </tr><tr>" + 
				"        <td>3. Detalles de Calificación Credito y Cobranza</td>" + 
				"      </tr><tr>" + 
				"        <td>4. Detalles de Calificación Áreas Comunes</td>" + 
				"      </tr><tr>" + 
				"        <td>5. Detalles de Calificación Generales</td>" + 
				"      </tr>" + 
				"    </tbody>" + 
				"  </table>" + 
				"</div> <br></br></div></body></html>";
		logger.info(retorno);
		return retorno;
	}
	//Proceso para generar pdf a detalle
	public void cadenaTodasPreguntas() {
		RepoFilExpBI repoFilExpBI = (RepoFilExpBI) FRQAppContextProvider
				.getApplicationContext().getBean("repoFilExpBI");

		List<RepoFilExpDTO> todasPreguntas = new ArrayList<RepoFilExpDTO>();
		preguntasPP = new ArrayList<RepoFilExpDTO>();
		preguntasEkt = new ArrayList<RepoFilExpDTO>();
		preguntasBaz = new ArrayList<RepoFilExpDTO>();
		preguntasAreas = new ArrayList<RepoFilExpDTO>();
		preguntasGenerales = new ArrayList<RepoFilExpDTO>();
		preguntasCyC = new ArrayList<RepoFilExpDTO>();
		List<RepoFilExpDTO> preguntas = new ArrayList<RepoFilExpDTO>();

		todasPreguntas = repoFilExpBI.obtieneInfofActa(idBitacora);
		preguntas = repoFilExpBI.obtienePreguntasActa(idBitacora);

		for (int i = 0; i < todasPreguntas.size(); i++) {
			if (todasPreguntas.get(i).getPregPadre() == 0) {
				if (todasPreguntas.get(i).getPosible().equalsIgnoreCase("SI")) {
					for (int k = 0; k < preguntas.size(); k++) {
						if (todasPreguntas.get(i).getIdPreg() == preguntas.get(k).getPregPadre()) {
							RepoFilExpDTO data2 = preguntas.get(k);
							data2.setPregunta(todasPreguntas.get(i).getPregunta()
									+ " : " + preguntas.get(k).getPregunta());
							data2.setCalif(todasPreguntas.get(i).getCalif());
							data2.setPosible(todasPreguntas.get(i).getPosible());
							data2.setRuta(todasPreguntas.get(i).getRuta());
							data2.setObserv(todasPreguntas.get(i).getObserv());
							switch (data2.getClasif()) {

								case "PP":
									preguntasPP.add(data2);
									break;
	
								case "GENERALES":
									preguntasGenerales.add(data2);
									break;
	
								case "AREAS":
									preguntasAreas.add(data2);
									break;
	
								case "BAZ":
									preguntasBaz.add(data2);
									break;
	
								case "EKT":
									preguntasEkt.add(data2);
									break;
								case "CYC":
									preguntasCyC.add(data2);
									break;
	
								default:
									break;

							}

						}

					}

				} else {

					for (int j = 0; j < todasPreguntas.size(); j++) {

						if (todasPreguntas.get(j).getPregPadre() == todasPreguntas.get(i).getIdPreg()) {
							RepoFilExpDTO data = todasPreguntas.get(j);
							data.setPregunta(todasPreguntas.get(i).getPregunta()
									+ " : " + todasPreguntas.get(j).getPregunta());

							switch (data.getClasif()) {

							case "PP":
								preguntasPP.add(data);
								break;

							case "Generales":
								preguntasGenerales.add(data);
								break;

							case "Areas":
								preguntasAreas.add(data);
								break;
								
							case "BAZ":
								preguntasBaz.add(data);
								break;

							case "EKT":
								preguntasEkt.add(data);
								break;
							case "CYC":
								preguntasCyC.add(data);
								break;

							default:
								break;

							}

						}

					}

				}

			}

		}
	}

	
	
	
	public String mergePDF(List<String> rutas){
		String rutaMerge = "";
		try {
            List<InputStream> pdfs = new ArrayList<InputStream>();
            if(rutas!=null && rutas.size()>0) rutaMerge = ""+rutas.get(0).replace(rutas.get(0).split("/")[rutas.get(0).split("/").length - 1], "nuevo.PDF");
            for(int i=0; i<rutas.size();i++) {
            	 	File archivo = new File(CleanPath.cleanString(rutas.get(i)));
                 if(archivo.exists() && archivo.length()>0){
                	 	pdfs.add(new FileInputStream(rutas.get(i)));
                 }else {
                	 	logger.info("No se pudo agregar Archivo: "+rutas.get(i));
                 }
            		
            }
            
            OutputStream output = new FileOutputStream(rutaMerge);
            concatPDFs(pdfs, output, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		return rutaMerge;
	}

	public static void concatPDFs(List<InputStream> streamOfPDFFiles,
            OutputStream outputStream, boolean paginate) {
 
        Document document = new Document();
        try {
            List<InputStream> pdfs = streamOfPDFFiles;
            List<PdfReader> readers = new ArrayList<PdfReader>();
            int totalPages = 0;
            Iterator<InputStream> iteratorPDFs = pdfs.iterator();
 
            while (iteratorPDFs.hasNext()) {
                InputStream pdf = iteratorPDFs.next();
                PdfReader pdfReader = new PdfReader(pdf);
                readers.add(pdfReader);
                totalPages += pdfReader.getNumberOfPages();
            }
 
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);
 
            document.open();
            PdfContentByte cb = writer.getDirectContent();
 
            PdfImportedPage page;
            int currentPageNumber = 0;
            int pageOfCurrentReaderPDF = 0;
            Iterator<PdfReader> iteratorPDFReader = readers.iterator();
 
            while (iteratorPDFReader.hasNext()) {
                PdfReader pdfReader = iteratorPDFReader.next();
 
                while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {
 
                    Rectangle rectangle = pdfReader.getPageSizeWithRotation(1);
                    document.setPageSize(rectangle);
                    document.newPage();
                    
                    pageOfCurrentReaderPDF++;
                    currentPageNumber++;
                    page = writer.getImportedPage(pdfReader,
                            pageOfCurrentReaderPDF);
                    
                    switch (rectangle.getRotation()) {
                    case 0:
                        cb.addTemplate(page, 1f, 0, 0, 1f, 0, 0);
                        break;
                    case 90:
                        cb.addTemplate(page, 0, -1f, 1f, 0, 0, pdfReader
                                .getPageSizeWithRotation(1).getHeight());
                        break;
                    case 180:
                        cb.addTemplate(page, -1f, 0, 0, -1f, 0, 0);
                        break;
                    case 270:
                        cb.addTemplate(page, 0, 1.0F, -1.0F, 0, pdfReader
                                .getPageSizeWithRotation(1).getWidth(), 0);
                        break;
                    default:
                        break;
                    }
                    if (paginate) {
                        cb.beginText();
                        cb.getPdfDocument().getPageSize();
                        //PdfContentByte.
                        cb.endText();
                    }
                    
                    
                }
                pageOfCurrentReaderPDF = 0;
            }
            
            outputStream.flush();
            document.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (document.isOpen())
                document.close();
            try {
                if (outputStream != null)
                    outputStream.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

	//Get And Set

	public ChecklistPreguntasComDTO getcYcObj() {
		return cYcObj;
	}

	public void setcYcObj(ChecklistPreguntasComDTO cYcObj) {
		this.cYcObj = cYcObj;
	}

	public ChecklistPreguntasComDTO getcYcObjExt() {
		return cYcObjExt;
	}

	public void setcYcObjExt(ChecklistPreguntasComDTO cYcObjExt) {
		this.cYcObjExt = cYcObjExt;
	}

	public ChecklistPreguntasComDTO getConteoHiijasCyC() {
		return conteoHiijasCyC;
	}

	public void setConteoHiijasCyC(ChecklistPreguntasComDTO conteoHiijasCyC) {
		this.conteoHiijasCyC = conteoHiijasCyC;
	}

	public double getPonderacionCyCExt() {
		return ponderacionCyCExt;
	}

	public void setPonderacionCyCExt(double ponderacionCyCExt) {
		this.ponderacionCyCExt = ponderacionCyCExt;
	}

	public double getTotalGeneralCyC() {
		return totalGeneralCyC;
	}

	public void setTotalGeneralCyC(double totalGeneralCyC) {
		this.totalGeneralCyC = totalGeneralCyC;
	}

	public double getcYcSiHijas() {
		return cYcSiHijas;
	}

	public void setcYcSiHijas(double cYcSiHijas) {
		this.cYcSiHijas = cYcSiHijas;
	}

	public double getcYcNaHijas() {
		return cYcNaHijas;
	}

	public void setcYcNaHijas(double cYcNaHijas) {
		this.cYcNaHijas = cYcNaHijas;
	}

	public String getLogoCyC() {
		return logoCyC;
	}

	public void setLogoCyC(String logoCyC) {
		this.logoCyC = logoCyC;
	}

	public String getcYcResSi() {
		return cYcResSi;
	}

	public void setcYcResSi(String cYcResSi) {
		this.cYcResSi = cYcResSi;
	}

	public String getcYcResNo() {
		return cYcResNo;
	}

	public void setcYcResNo(String cYcResNo) {
		this.cYcResNo = cYcResNo;
	}

	public String getcYcSiPor() {
		return cYcSiPor;
	}

	public void setcYcSiPor(String cYcSiPor) {
		this.cYcSiPor = cYcSiPor;
	}

	public String getcYcNoPor() {
		return cYcNoPor;
	}

	public void setcYcNoPor(String cYcNoPor) {
		this.cYcNoPor = cYcNoPor;
	}

	public String getcYcTotalPon() {
		return cYcTotalPon;
	}

	public void setcYcTotalPon(String cYcTotalPon) {
		this.cYcTotalPon = cYcTotalPon;
	}

	public String getcYcCalifCant() {
		return cYcCalifCant;
	}

	public void setcYcCalifCant(String cYcCalifCant) {
		this.cYcCalifCant = cYcCalifCant;
	}

	public File getGraficaCyC() {
		return graficaCyC;
	}

	public void setGraficaCyC(File graficaCyC) {
		this.graficaCyC = graficaCyC;
	}

	public String obtieneHTMLDetalleClasif(String clasif) {
		String retorno="";

		if(clasif.equalsIgnoreCase("EKT")) {
			retorno=generaHTMLDetalleClasif("ELEKTRA",preguntasEkt,logoEkt,clasiffImg(clasif));
		}
		if(clasif.equalsIgnoreCase("AREAS")) {
			retorno=generaHTMLDetalleClasif("ÁREAS COMUNES",preguntasAreas,logoCom,clasiffImg(clasif));
		}
		if(clasif.equalsIgnoreCase("BAZ")) {
			retorno=generaHTMLDetalleClasif("BANCO AZTECA",preguntasBaz,logoBaz,clasiffImg(clasif));
		}
		if(clasif.equalsIgnoreCase("GENERALES")) {
			retorno=generaHTMLDetalleClasif("GENERALES",preguntasGenerales,logoGen,clasiffImg(clasif));
		}
		if(clasif.equalsIgnoreCase("PP")) {
			retorno=generaHTMLDetalleClasif("PRESTA PRENDA",preguntasPP,logoPres,clasiffImg(clasif));
		}
		if(clasif.equalsIgnoreCase("CYC")) {
			retorno=generaHTMLDetalleClasif("Credito y Cobranza",preguntasCyC,logoCyC,clasiffImg(clasif));
		}
		
		return retorno;
	}
	
	private String generaHTMLDetalleClasif(String clasif,List<RepoFilExpDTO> preguntas,String logo,String clasifImg) {

		String zonaHtml = "<html><body>";
		zonaHtml += "<div style='position: fixed; top: 10px; right: 30px; left: 30px; bottom: 10px; background: white; border-radius: 30px; border-style: solid;  border-color: gray; border-width: 1px;  padding: 20px;  text-align: right; overflow-y: scroll;'>";
		zonaHtml += "<div style = 'background-color: #ECEBEB; width:100%;'><img src='http://10.53.33.82/franquicia/firmaChecklist/LogoElektra.png' height='30px' style='float: right;'></img>&nbsp;&nbsp;<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoBAZ.png' height='30px' style='float: right;'></img></div> <br></br> <br></br> <br></br>";
		zonaHtml += "<div><table><tr><td><img src='http://10.53.33.82/franquicia/firmaChecklist/"+clasifImg+"' height='50' align='left'/></td><td> <p style='text-align: left; padding-top: 10px'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+clasif+"</b></p></td><td><p style='text-align: center; padding-top: 10px; '>Calififcacion:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
				 + ((preguntas.size() > 0) ? preguntas.get(0).getCalif() : " ")
				 + "</p></td><td><img src='http://10.53.33.82/franquicia/firmaChecklist/"+logo+"' width='50'height='25' align='rigth' style='padding-left: 30px;padding-top: 10px;'/></td></tr></table></div>";
		zonaHtml += "<div style='text-align: left; background-color: #ECEBEB;padding-top: .5px;padding-bottom: .5px;' ><table style=' width: 100%;  '> <tr><td><p > <b>&nbsp;&nbsp;Detalles de Calificacion</b>  | Entrega de Sucursal</p></td><td><p></p></td></tr></table> </div> <br>  </br>";

		zonaHtml += "<div style='text-align: left;'>";
		
		if(preguntas != null && preguntas.size()>0) {
			zonaHtml += "<table style='width: 100%'><tr><th>Item</th><th>Respuesta</th><th>Foto</th></tr>";
			for (int i = 0; i < preguntas.size(); i++) {
	
				zonaHtml += "<tr>";
				zonaHtml += "<td style='font-size: 13; width:60%;'>" + (i + 1) + ".-" + preguntas.get(i).getPregunta() + "<br></br>" + "Comenatrio Adicional: "+ ((preguntas.get(i).getObserv() != null) ? preguntas.get(i).getObserv() : " ")
						+ "</td>";
				zonaHtml += "<td style='font-size: 13'>" + preguntas.get(i).getPosible() + "</td>";
				//zonaHtml += "<td ><img src='" + preguntas.get(i).getRuta() + "' width='60'height='60'/></td>";
				zonaHtml += "<td ><div  width='60'height='60'><img src='" + preguntas.get(i).getRuta() + "' width='60'height='60'/></div></td>";
				zonaHtml += "</tr>";
	
			}
			zonaHtml += "</table>";
		} else {
			zonaHtml += "<table style='width: 100%'><tr><th>Item</th><th>Respuesta</th><th>Foto</th></tr>";
			zonaHtml += "<tr><td></td><td></td><td></td></tr>";
			zonaHtml += "</table>";
		}

		zonaHtml += "  </div></div>";
		zonaHtml += "</body></html>";
		logger.info(zonaHtml);
		return zonaHtml;
	}
	/*no se usa
	public String obtieneHTMLDetalleClasif(String clasif) {
		switch (clasif) {

		case "EKT":

			String zonaEktHtml = "<html><boy>";
			zonaEktHtml += "<div style='position: fixed; top: 10px; right: 30px; left: 30px; bottom: 10px; background: white; border-radius: 30px; border-style: solid;  border-color: gray; border-width: 1px;  padding: 20px;  text-align: center; overflow-y: scroll;'>";
			zonaEktHtml += "<div style = 'background-color: #ECEBEB;float:right; width:100%;'><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='35'height='30' style='float: right;'/><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg'width='30'height='30' style='float: right;'/></div> <br></br> <br></br> <br></br>";
			zonaEktHtml += "<div><table><tr><td><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='50'height='50' align='left'/></td><td> <p style='text-align: left; padding-top: 10px'><b>Elecktra</b></p></td><td><p style='text-align: center; padding-top: 10px; '>Calififcacion: "
					+ ((preguntasEkt.size() > 0) ? preguntasEkt.get(0).getCalif() : " ")
					+ "</p></td><td><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='50'height='25' align='rigth' style='padding-left: 30px;padding-top: 10px;'/></td></tr></table></div>";
			zonaEktHtml += "<div style='text-align: left; background-color: #ECEBEB;padding-top: .5px;padding-bottom: .5px;' ><table style=' width: 100%;  '> <tr><td><p > <b>Detalles de Calificacion</b>  | Entrega de Sucursal</p></td><td><p></p></td></tr></table> </div> <br>  </br>";

			// imperdonablesHtml += " <div style='text-align: left; background-color:
			// black;'> <p style='color: white;'><b>Planta Baja</b></p></div>";

			zonaEktHtml += "<div style='text-align: left;'><table style='width: 100%'>";
			zonaEktHtml += "<tr><th>Item</th><th>Respuesta</th><th>Foto</th></tr>";
			for (int i = 0; i < preguntasEkt.size(); i++) {

				zonaEktHtml += "<tr>";
				zonaEktHtml += "<td style='font-size: 13'>" + (i + 1) + ".-" + preguntasEkt.get(i).getPregunta()
						+ "</td>";
				zonaEktHtml += "<td style='font-size: 13'>" + preguntasEkt.get(i).getPosible() + "</td>";
				zonaEktHtml += "<td ><img src='" + preguntasEkt.get(i).getRuta() + "' width='60'height='60'/></td>";
				zonaEktHtml += "</tr>";

			}

			zonaEktHtml += " </table> </div></div>";
			zonaEktHtml += "</body></html>";
			 return zonaEktHtml;

		case "BAZ":

			String zonaBazHtml = "<html><body>";
			zonaBazHtml += "<div style='position: fixed; top: 10px; right: 30px; left: 30px; bottom: 10px; background: white; border-radius: 30px; border-style: solid;  border-color: gray; border-width: 1px;  padding: 20px;  text-align: center; overflow-y: scroll;'>";
			zonaBazHtml += "<div style = 'background-color: #ECEBEB;float:right; width:100%;'><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='35'height='30' style='float: right;'/><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg'width='30'height='30' style='float: right;'/></div> <br></br> <br></br> <br></br>";
			zonaBazHtml += "<div><table><tr><td><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='50'height='50' align='left'/></td><td> <p style='text-align: left; padding-top: 10px'><b>Banco Azteca</b></p></td><td><p style='text-align: center; padding-top: 10px; '>Calififcacion: "
					+ ((preguntasBaz.size() > 0) ? preguntasBaz.get(0).getCalif() : " ")
					+ "</p></td><td><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='50'height='25' align='rigth' style='padding-left: 30px;padding-top: 10px;'/></td></tr></table></div>";
			zonaBazHtml += "<div style='text-align: left; background-color: #ECEBEB;padding-top: .5px;padding-bottom: .5px;' ><table style=' width: 100%;  '> <tr><td><p > <b>Detalles de Calificacion</b>  | Entrega de Sucursal</p></td><td><p></p></td></tr></table> </div> <br>  </br>";

			// imperdonablesHtml += " <div style='text-align: left; background-color:
			// black;'> <p style='color: white;'><b>Planta Baja</b></p></div>";

			zonaBazHtml += "<div style='text-align: left;'><table style='width: 100%'>";
			zonaBazHtml += "<tr><th>Item</th><th>Respuesta</th><th>Foto</th></tr>";

			for (int i = 0; i < preguntasBaz.size(); i++) {
				zonaBazHtml += "<tr>";
				zonaBazHtml += "<td style='font-size: 13'>" + (i + 1) + ".-" + preguntasBaz.get(i).getPregunta()
						+ "</td>";
				zonaBazHtml += "<td style='font-size: 13'>" + preguntasBaz.get(i).getPosible() + "</td>";
				zonaBazHtml += "<td ><img src='" + preguntasBaz.get(i).getRuta() + "' width='60'height='60'/></td>";
				zonaBazHtml += "</tr>";

			}

			zonaBazHtml += " </table> </div></div>";
			zonaBazHtml += "</body></html>";
			return zonaBazHtml;

		case "PP":

			String zonaPPHtml = "<html><body>";
			zonaPPHtml += "<div style='position: fixed; top: 10px; right: 30px; left: 30px; bottom: 10px; background: white; border-radius: 30px; border-style: solid;  border-color: gray; border-width: 1px;  padding: 20px;  text-align: center; overflow-y: scroll;'>";
			zonaPPHtml += "<div style = 'background-color: #ECEBEB;float:right; width:100%;'><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='35'height='30' style='float: right;'/><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg'width='30'height='30' style='float: right;'/></div> <br></br> <br></br> <br></br>";
			zonaPPHtml += "<div><table><tr><td><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='50'height='50' align='left'/></td><td> <p style='text-align: left; padding-top: 10px'><b>Presta Prenda</b></p></td><td><p style='text-align: center; padding-top: 10px; '>Calififcacion: "
					+ ((preguntasPP.size() > 0) ? preguntasPP.get(0).getCalif() : " ")
					+ "</p></td><td><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='50'height='25' align='rigth' style='padding-left: 30px;padding-top: 10px;'/></td></tr></table></div>";
			zonaPPHtml += "<div style='text-align: left; background-color: #ECEBEB;padding-top: .5px;padding-bottom: .5px;' ><table style=' width: 100%;  '> <tr><td><p > <b>Detalles de Calificacion</b>  | Entrega de Sucursal</p></td><td><p></p></td></tr></table> </div> <br>  </br>";

			// imperdonablesHtml += " <div style='text-align: left; background-color:
			// black;'> <p style='color: white;'><b>Planta Baja</b></p></div>";

			zonaPPHtml += "<div style='text-align: left;'><table style='width: 100%'>";
			zonaPPHtml += "<tr><th>Item</th><th>Respuesta</th><th>Foto</th></tr>";
			for (int i = 0; i < preguntasPP.size(); i++) {
				zonaPPHtml += "<tr>";
				zonaPPHtml += "<td style='font-size: 13'>" + (i + 1) + ".-" + preguntasPP.get(i).getPregunta()
						+ "</td>";
				zonaPPHtml += "<td style='font-size: 13'>" + preguntasPP.get(i).getPosible() + "</td>";
				zonaPPHtml += "<td ><img src='" + preguntasPP.get(i).getRuta() + "' width='60'height='60'/></td>";
				zonaPPHtml += "</tr>";

			}

			zonaPPHtml += " </table> </div></div>";
			zonaPPHtml += "</body></html>";
			return zonaPPHtml;

		case "AREAS":

			String zonaAreasHtml = "<html><body>";
			zonaAreasHtml += "<div style='position: fixed; top: 10px; right: 30px; left: 30px; bottom: 10px; background: white; border-radius: 30px; border-style: solid;  border-color: gray; border-width: 1px;  padding: 20px;  text-align: center; overflow-y: scroll;'>";
			zonaAreasHtml += "<div style = 'background-color: #ECEBEB;float:right; width:100%;'><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='35'height='30' style='float: right;'/><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg'width='30'height='30' style='float: right;'/></div> <br></br> <br></br> <br></br>";
			zonaAreasHtml += "<div><table><tr><td><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='50'height='50' align='left'/></td><td> <p style='text-align: left; padding-top: 10px'><b>Areas Comunes</b></p></td><td><p style='text-align: center; padding-top: 10px; '>Calififcacion: "
					+ ((preguntasAreas.size() > 0) ? preguntasAreas.get(0).getCalif() : " ")
					+ "</p></td><td><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='50'height='25' align='rigth' style='padding-left: 30px;padding-top: 10px;'/></td></tr></table></div>";
			zonaAreasHtml += "<div style='text-align: left; background-color: #ECEBEB;padding-top: .5px;padding-bottom: .5px;' ><table style=' width: 100%;  '> <tr><td><p > <b>Detalles de Calificacion</b>  | Entrega de Sucursal</p></td><td><p></p></td></tr></table> </div> <br>  </br>";

			// imperdonablesHtml += " <div style='text-align: left; background-color:
			// black;'> <p style='color: white;'><b>Planta Baja</b></p></div>";

			zonaAreasHtml += "<div style='text-align: left;'><table style='width: 100%'>";
			zonaAreasHtml += "<tr><th>Item</th><th>Respuesta</th><th>Foto</th></tr>";

			for (int i = 0; i < preguntasAreas.size(); i++) {
				zonaAreasHtml += "<tr>";
				zonaAreasHtml += "<td style='font-size: 13'>" + (i + 1) + ".-" + preguntasAreas.get(i).getPregunta()
						+ "</td>";
				zonaAreasHtml += "<td style='font-size: 13'>" + preguntasAreas.get(i).getPosible() + "</td>";
				zonaAreasHtml += "<td ><img src='" + preguntasAreas.get(i).getRuta() + "' width='60'height='60'/></td>";
				zonaAreasHtml += "</tr>";

			}

			zonaAreasHtml += " </table> </div></div>";
			zonaAreasHtml += "</body></html>";

			return zonaAreasHtml;

		case "GENERALES":

			String zonaGeneralesHtml = "<html><body>";
			zonaGeneralesHtml += "<div style='position: fixed; top: 10px; right: 30px; left: 30px; bottom: 10px; background: white; border-radius: 30px; border-style: solid;  border-color: gray; border-width: 1px;  padding: 20px;  text-align: center; overflow-y: scroll;'>";
			zonaGeneralesHtml += "<div style = 'background-color: #ECEBEB;float:right; width:100%;'><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='35'height='30' style='float: right;'/><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg'width='30'height='30' style='float: right;'/></div> <br></br> <br></br> <br></br>";
			zonaGeneralesHtml += "<div><table><tr><td><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='50'height='50' align='left'/></td><td> <p style='text-align: left; padding-top: 10px'><b>Generales</b></p></td><td><p style='text-align: center; padding-top: 10px; '>Calififcacion: "
					+ ((preguntasGenerales.size() > 0) ? preguntasGenerales.get(0).getCalif() : " ")
					+ "</p></td><td><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='50'height='25' align='rigth' style='padding-left: 30px;padding-top: 10px;'/></td></tr></table></div>";
			zonaGeneralesHtml += "<div style='text-align: left; background-color: #ECEBEB;padding-top: .5px;padding-bottom: .5px;' ><table style=' width: 100%;  '> <tr><td><p > <b>Detalles de Calificacion</b>  | Entrega de Sucursal</p></td><td><p></p></td></tr></table> </div> <br>  </br>";

			// imperdonablesHtml += " <div style='text-align: left; background-color:
			// black;'> <p style='color: white;'><b>Planta Baja</b></p></div>";

			zonaGeneralesHtml += "<div style='text-align: left;'><table style='width: 100%'>";
			zonaGeneralesHtml += "<tr><th>Item</th><th>Respuesta</th><th>Foto</th></tr>";

			for (int i = 0; i < preguntasGenerales.size(); i++) {
				zonaGeneralesHtml += "<tr>";
				zonaGeneralesHtml += "<td style='font-size: 13'>" + (i + 1) + ".-"
						+ preguntasGenerales.get(i).getPregunta() + "</td>";
				zonaGeneralesHtml += "<td style='font-size: 13'>" + preguntasGenerales.get(i).getPosible() + "</td>";
				zonaGeneralesHtml += "<td ><img src='" + preguntasGenerales.get(i).getRuta()
						+ "' width='60'height='60'/></td>";
				zonaGeneralesHtml += "</tr>";

			}

			zonaGeneralesHtml += " </table> </div></div>";
			zonaGeneralesHtml += "</body></html>";

			return zonaGeneralesHtml;

		default:
			break;

		}
		return "";

	}
	*/
	
	
	public String getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(String idBitacora) {
		this.idBitacora = idBitacora;
	}

	public List<String> getImperdonables() {
		return imperdonables;
	}

	public void setImperdonables(List<String> imperdonables) {
		this.imperdonables = imperdonables;
	}

	public ChecklistPreguntasComDTO getEktObj() {
		return ektObj;
	}

	public void setEktObj(ChecklistPreguntasComDTO ektObj) {
		this.ektObj = ektObj;
	}

	public ChecklistPreguntasComDTO getEktObjExt() {
		return ektObjExt;
	}

	public void setEktObjExt(ChecklistPreguntasComDTO ektObjExt) {
		this.ektObjExt = ektObjExt;
	}

	public ChecklistPreguntasComDTO getConteoHiijasEkt() {
		return conteoHiijasEkt;
	}

	public void setConteoHiijasEkt(ChecklistPreguntasComDTO conteoHiijasEkt) {
		this.conteoHiijasEkt = conteoHiijasEkt;
	}

	public double getPonderacionEktExt() {
		return ponderacionEktExt;
	}

	public void setPonderacionEktExt(double ponderacionEktExt) {
		this.ponderacionEktExt = ponderacionEktExt;
	}

	public double getTotalGeneralEkt() {
		return totalGeneralEkt;
	}

	public void setTotalGeneralEkt(double totalGeneralEkt) {
		this.totalGeneralEkt = totalGeneralEkt;
	}

	public double getEktSiHijas() {
		return ektSiHijas;
	}

	public void setEktSiHijas(double ektSiHijas) {
		this.ektSiHijas = ektSiHijas;
	}

	public double getEktNaHijas() {
		return ektNaHijas;
	}

	public void setEktNaHijas(double ektNaHijas) {
		this.ektNaHijas = ektNaHijas;
	}

	public ChecklistPreguntasComDTO getBazObj() {
		return bazObj;
	}

	public void setBazObj(ChecklistPreguntasComDTO bazObj) {
		this.bazObj = bazObj;
	}

	public ChecklistPreguntasComDTO getConteoHiijasBaz() {
		return conteoHiijasBaz;
	}

	public void setConteoHiijasBaz(ChecklistPreguntasComDTO conteoHiijasBaz) {
		this.conteoHiijasBaz = conteoHiijasBaz;
	}

	public ChecklistPreguntasComDTO getBazObjExt() {
		return bazObjExt;
	}

	public void setBazObjExt(ChecklistPreguntasComDTO bazObjExt) {
		this.bazObjExt = bazObjExt;
	}

	public double getPonderacionBazExt() {
		return ponderacionBazExt;
	}

	public void setPonderacionBazExt(double ponderacionBazExt) {
		this.ponderacionBazExt = ponderacionBazExt;
	}

	public double getTotalGeneralBaz() {
		return totalGeneralBaz;
	}

	public void setTotalGeneralBaz(double totalGeneralBaz) {
		this.totalGeneralBaz = totalGeneralBaz;
	}

	public double getBazSiHijas() {
		return bazSiHijas;
	}

	public void setBazSiHijas(double bazSiHijas) {
		this.bazSiHijas = bazSiHijas;
	}

	public double getBazNaHijas() {
		return bazNaHijas;
	}

	public void setBazNaHijas(double bazNaHijas) {
		this.bazNaHijas = bazNaHijas;
	}

	public ChecklistPreguntasComDTO getGeneralesObj() {
		return generalesObj;
	}

	public void setGeneralesObj(ChecklistPreguntasComDTO generalesObj) {
		this.generalesObj = generalesObj;
	}

	public ChecklistPreguntasComDTO getPrestaPrendaObjExt() {
		return prestaPrendaObjExt;
	}

	public void setPrestaPrendaObjExt(ChecklistPreguntasComDTO prestaPrendaObjExt) {
		this.prestaPrendaObjExt = prestaPrendaObjExt;
	}

	public ChecklistPreguntasComDTO getConteoHiijasPrestaPrenda() {
		return conteoHiijasPrestaPrenda;
	}

	public void setConteoHiijasPrestaPrenda(ChecklistPreguntasComDTO conteoHiijasPrestaPrenda) {
		this.conteoHiijasPrestaPrenda = conteoHiijasPrestaPrenda;
	}

	public double getPonderacionPrestaExt() {
		return ponderacionPrestaExt;
	}

	public void setPonderacionPrestaExt(double ponderacionPrestaExt) {
		this.ponderacionPrestaExt = ponderacionPrestaExt;
	}

	public double getTotalGeneralPresta() {
		return totalGeneralPresta;
	}

	public void setTotalGeneralPresta(double totalGeneralPresta) {
		this.totalGeneralPresta = totalGeneralPresta;
	}

	public double getPrestaPrendaSiHijas() {
		return prestaPrendaSiHijas;
	}

	public void setPrestaPrendaSiHijas(double prestaPrendaSiHijas) {
		this.prestaPrendaSiHijas = prestaPrendaSiHijas;
	}

	public double getPrestaPrendaNaHijas() {
		return prestaPrendaNaHijas;
	}

	public void setPrestaPrendaNaHijas(double prestaPrendaNaHijas) {
		this.prestaPrendaNaHijas = prestaPrendaNaHijas;
	}

	public ChecklistPreguntasComDTO getAreasComunesObj() {
		return areasComunesObj;
	}

	public void setAreasComunesObj(ChecklistPreguntasComDTO areasComunesObj) {
		this.areasComunesObj = areasComunesObj;
	}

	public ChecklistPreguntasComDTO getAreasComunesObjExt() {
		return areasComunesObjExt;
	}

	public void setAreasComunesObjExt(ChecklistPreguntasComDTO areasComunesObjExt) {
		this.areasComunesObjExt = areasComunesObjExt;
	}

	public ChecklistPreguntasComDTO getConteoHijasComunes() {
		return conteoHijasComunes;
	}

	public void setConteoHijasComunes(ChecklistPreguntasComDTO conteoHijasComunes) {
		this.conteoHijasComunes = conteoHijasComunes;
	}

	public double getPonderacionComExt() {
		return ponderacionComExt;
	}

	public void setPonderacionComExt(double ponderacionComExt) {
		this.ponderacionComExt = ponderacionComExt;
	}

	public double getTotalGeneralComunes() {
		return totalGeneralComunes;
	}

	public void setTotalGeneralComunes(double totalGeneralComunes) {
		this.totalGeneralComunes = totalGeneralComunes;
	}

	public double getComSiHijas() {
		return comSiHijas;
	}

	public void setComSiHijas(double comSiHijas) {
		this.comSiHijas = comSiHijas;
	}

	public double getComNaHijas() {
		return comNaHijas;
	}

	public void setComNaHijas(double comNaHijas) {
		this.comNaHijas = comNaHijas;
	}

	public ChecklistPreguntasComDTO getPrestaPrendaObj() {
		return prestaPrendaObj;
	}

	public void setPrestaPrendaObj(ChecklistPreguntasComDTO prestaPrendaObj) {
		this.prestaPrendaObj = prestaPrendaObj;
	}

	public ChecklistPreguntasComDTO getGeneralesObjExt() {
		return generalesObjExt;
	}

	public void setGeneralesObjExt(ChecklistPreguntasComDTO generalesObjExt) {
		this.generalesObjExt = generalesObjExt;
	}

	public ChecklistPreguntasComDTO getConteoHiijasGenerales() {
		return conteoHiijasGenerales;
	}

	public void setConteoHiijasGenerales(ChecklistPreguntasComDTO conteoHiijasGenerales) {
		this.conteoHiijasGenerales = conteoHiijasGenerales;
	}

	public double getPonderacionGenExt() {
		return ponderacionGenExt;
	}

	public void setPonderacionGenExt(double ponderacionGenExt) {
		this.ponderacionGenExt = ponderacionGenExt;
	}

	public double getTotalGeneralGenerales() {
		return totalGeneralGenerales;
	}

	public void setTotalGeneralGenerales(double totalGeneralGenerales) {
		this.totalGeneralGenerales = totalGeneralGenerales;
	}

	public double getGeneralesSiHijas() {
		return generalesSiHijas;
	}

	public void setGeneralesSiHijas(double generalesSiHijas) {
		this.generalesSiHijas = generalesSiHijas;
	}

	public double getGeneralesNaHijas() {
		return generalesNaHijas;
	}

	public void setGeneralesNaHijas(double generalesNaHijas) {
		this.generalesNaHijas = generalesNaHijas;
	}

	public DecimalFormat getFormatDecimales() {
		return formatDecimales;
	}

	public void setFormatDecimales(DecimalFormat formatDecimales) {
		this.formatDecimales = formatDecimales;
	}

	public DecimalFormat getFormatEnteros() {
		return formatEnteros;
	}

	public void setFormatEnteros(DecimalFormat formatEnteros) {
		this.formatEnteros = formatEnteros;
	}

	public List<ChecklistPreguntasComDTO> getListaGrupos() {
		return listaGrupos;
	}

	public void setListaGrupos(List<ChecklistPreguntasComDTO> listaGrupos) {
		this.listaGrupos = listaGrupos;
	}

	public List<ChecklistPreguntasComDTO> getListaGruposExt() {
		return listaGruposExt;
	}

	public void setListaGruposExt(List<ChecklistPreguntasComDTO> listaGruposExt) {
		this.listaGruposExt = listaGruposExt;
	}

	public List<ChecklistPreguntasComDTO> getListaConteosHijas() {
		return listaConteosHijas;
	}

	public void setListaConteosHijas(List<ChecklistPreguntasComDTO> listaConteosHijas) {
		this.listaConteosHijas = listaConteosHijas;
	}

	public String getLogoCom() {
		return logoCom;
	}

	public void setLogoCom(String logoCom) {
		this.logoCom = logoCom;
	}

	public String getLogoBaz() {
		return logoBaz;
	}

	public void setLogoBaz(String logoBaz) {
		this.logoBaz = logoBaz;
	}

	public String getLogoEkt() {
		return logoEkt;
	}

	public void setLogoEkt(String logoEkt) {
		this.logoEkt = logoEkt;
	}

	public String getLogoPres() {
		return logoPres;
	}

	public void setLogoPres(String logoPres) {
		this.logoPres = logoPres;
	}

	public String getLogoGen() {
		return logoGen;
	}

	public void setLogoGen(String logoGen) {
		this.logoGen = logoGen;
	}

	public String getLogoTotal() {
		return logoTotal;
	}

	public void setLogoTotal(String logoTotal) {
		this.logoTotal = logoTotal;
	}

	public String getComResSi() {
		return comResSi;
	}

	public void setComResSi(String comResSi) {
		this.comResSi = comResSi;
	}

	public String getBazResSi() {
		return bazResSi;
	}

	public void setBazResSi(String bazResSi) {
		this.bazResSi = bazResSi;
	}

	public String getEktResSi() {
		return ektResSi;
	}

	public void setEktResSi(String ektResSi) {
		this.ektResSi = ektResSi;
	}

	public String getGenResSi() {
		return genResSi;
	}

	public void setGenResSi(String genResSi) {
		this.genResSi = genResSi;
	}

	public String getPrestaResSi() {
		return prestaResSi;
	}

	public void setPrestaResSi(String prestaResSi) {
		this.prestaResSi = prestaResSi;
	}

	public String getTotalResSi() {
		return totalResSi;
	}

	public void setTotalResSi(String totalResSi) {
		this.totalResSi = totalResSi;
	}

	public String getComResNo() {
		return comResNo;
	}

	public void setComResNo(String comResNo) {
		this.comResNo = comResNo;
	}

	public String getBazResNo() {
		return bazResNo;
	}

	public void setBazResNo(String bazResNo) {
		this.bazResNo = bazResNo;
	}

	public String getEktResNo() {
		return ektResNo;
	}

	public void setEktResNo(String ektResNo) {
		this.ektResNo = ektResNo;
	}

	public String getGenResNo() {
		return genResNo;
	}

	public void setGenResNo(String genResNo) {
		this.genResNo = genResNo;
	}

	public String getPrestaResNo() {
		return prestaResNo;
	}

	public void setPrestaResNo(String prestaResNo) {
		this.prestaResNo = prestaResNo;
	}

	public String getTotalResNo() {
		return totalResNo;
	}

	public void setTotalResNo(String totalResNo) {
		this.totalResNo = totalResNo;
	}

	public String getComSiPor() {
		return comSiPor;
	}

	public void setComSiPor(String comSiPor) {
		this.comSiPor = comSiPor;
	}

	public String getBazSiPor() {
		return bazSiPor;
	}

	public void setBazSiPor(String bazSiPor) {
		this.bazSiPor = bazSiPor;
	}

	public String getEktSiPor() {
		return ektSiPor;
	}

	public void setEktSiPor(String ektSiPor) {
		this.ektSiPor = ektSiPor;
	}

	public String getGenSiPor() {
		return genSiPor;
	}

	public void setGenSiPor(String genSiPor) {
		this.genSiPor = genSiPor;
	}

	public String getPrestaSiPor() {
		return prestaSiPor;
	}

	public void setPrestaSiPor(String prestaSiPor) {
		this.prestaSiPor = prestaSiPor;
	}

	public String getTotalSiPor() {
		return totalSiPor;
	}

	public void setTotalSiPor(String totalSiPor) {
		this.totalSiPor = totalSiPor;
	}

	public String getComNoPor() {
		return comNoPor;
	}

	public void setComNoPor(String comNoPor) {
		this.comNoPor = comNoPor;
	}

	public String getBazNoPor() {
		return bazNoPor;
	}

	public void setBazNoPor(String bazNoPor) {
		this.bazNoPor = bazNoPor;
	}

	public String getEktNoPor() {
		return ektNoPor;
	}

	public void setEktNoPor(String ektNoPor) {
		this.ektNoPor = ektNoPor;
	}

	public String getGenNoPor() {
		return genNoPor;
	}

	public void setGenNoPor(String genNoPor) {
		this.genNoPor = genNoPor;
	}

	public String getPrestaNoPor() {
		return prestaNoPor;
	}

	public void setPrestaNoPor(String prestaNoPor) {
		this.prestaNoPor = prestaNoPor;
	}

	public String getTotalNoPor() {
		return totalNoPor;
	}

	public void setTotalNoPor(String totalNoPor) {
		this.totalNoPor = totalNoPor;
	}

	public String getEktTotalPon() {
		return ektTotalPon;
	}

	public void setEktTotalPon(String ektTotalPon) {
		this.ektTotalPon = ektTotalPon;
	}

	public String getGenTotalPon() {
		return genTotalPon;
	}

	public void setGenTotalPon(String genTotalPon) {
		this.genTotalPon = genTotalPon;
	}

	public String getPrestaTotalPon() {
		return prestaTotalPon;
	}

	public void setPrestaTotalPon(String prestaTotalPon) {
		this.prestaTotalPon = prestaTotalPon;
	}

	public String getBazTotalPon() {
		return bazTotalPon;
	}

	public void setBazTotalPon(String bazTotalPon) {
		this.bazTotalPon = bazTotalPon;
	}

	public String getComTotalPon() {
		return comTotalPon;
	}

	public void setComTotalPon(String comTotalPon) {
		this.comTotalPon = comTotalPon;
	}

	public double getTotalTotal() {
		return totalTotal;
	}

	public void setTotalTotal(double totalTotal) {
		this.totalTotal = totalTotal;
	}

	public String getEktCalifCant() {
		return ektCalifCant;
	}

	public void setEktCalifCant(String ektCalifCant) {
		this.ektCalifCant = ektCalifCant;
	}

	public String getGenCalifCant() {
		return genCalifCant;
	}

	public void setGenCalifCant(String genCalifCant) {
		this.genCalifCant = genCalifCant;
	}

	public String getPrestaCalifCant() {
		return prestaCalifCant;
	}

	public void setPrestaCalifCant(String prestaCalifCant) {
		this.prestaCalifCant = prestaCalifCant;
	}

	public String getBazCalifCant() {
		return bazCalifCant;
	}

	public void setBazCalifCant(String bazCalifCant) {
		this.bazCalifCant = bazCalifCant;
	}

	public String getComCalifCant() {
		return comCalifCant;
	}

	public void setComCalifCant(String comCalifCant) {
		this.comCalifCant = comCalifCant;
	}

	public String getTotalCalifCant() {
		return totalCalifCant;
	}

	public void setTotalCalifCant(String totalCalifCant) {
		this.totalCalifCant = totalCalifCant;
	}

	public File getGraficaComunes() {
		return graficaComunes;
	}

	public void setGraficaComunes(File graficaComunes) {
		this.graficaComunes = graficaComunes;
	}

	public File getGraficaBanco() {
		return graficaBanco;
	}

	public void setGraficaBanco(File graficaBanco) {
		this.graficaBanco = graficaBanco;
	}

	public File getGraficaElektra() {
		return graficaElektra;
	}

	public void setGraficaElektra(File graficaElektra) {
		this.graficaElektra = graficaElektra;
	}

	public File getGraficaGenerales() {
		return graficaGenerales;
	}

	public void setGraficaGenerales(File graficaGenerales) {
		this.graficaGenerales = graficaGenerales;
	}

	public File getGraficaPresta() {
		return graficaPresta;
	}

	public void setGraficaPresta(File graficaPresta) {
		this.graficaPresta = graficaPresta;
	}

	public File getGraficaTotal() {
		return graficaTotal;
	}

	public void setGraficaTotal(File graficaTotal) {
		this.graficaTotal = graficaTotal;
	}
	
	public String getOperable() {
		return operable;
	}
	
	public void setOperable(String operable) {
		this.operable = operable;
	}

	public String getColorOperable() {
		return colorOperable;
	}

	public void setColorOperable(String colorOperable) {
		this.colorOperable = colorOperable;
	}

	public String getColorTexto() {
		return colorTexto;
	}

	public void setColorTexto(String colorTexto) {
		this.colorTexto = colorTexto;
	}

	public double getNumeroDeCalificacion() {
		return numeroDeCalificacion;
	}

	public void setNumeroDeCalificacion(double numeroDeCalificacion) {
		this.numeroDeCalificacion = numeroDeCalificacion;
	}

	public int getNumImperdonablesRecorrido() {
		return numImperdonablesRecorrido;
	}

	public void setNumImperdonablesRecorrido(int numImperdonablesRecorrido) {
		this.numImperdonablesRecorrido = numImperdonablesRecorrido;
	}

	public double getPreCalificacionNum() {
		return preCalificacionNum;
	}

	public void setPreCalificacionNum(double preCalificacionNum) {
		this.preCalificacionNum = preCalificacionNum;
	}

	public String getTotalItems() {
		return totalItems;
	}

	public void setTotalItems(String totalItems) {
		this.totalItems = totalItems;
	}

	public String getItemsPorRevisar() {
		return itemsPorRevisar;
	}

	public void setItemsPorRevisar(String itemsPorRevisar) {
		this.itemsPorRevisar = itemsPorRevisar;
	}

	public String getItemsAprobados() {
		return itemsAprobados;
	}

	public void setItemsAprobados(String itemsAprobados) {
		this.itemsAprobados = itemsAprobados;
	}

	public String getItemsNoConsiderados() {
		return itemsNoConsiderados;
	}

	public void setItemsNoConsiderados(String itemsNoConsiderados) {
		this.itemsNoConsiderados = itemsNoConsiderados;
	}

	public String getImgBaner() {
		return imgBaner;
	}

	public void setImgBaner(String imgBaner) {
		this.imgBaner = imgBaner;
	}
	
}
