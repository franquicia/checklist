$body = $("body");
$(document).on({
    ajaxStart: function() {$body.addClass("loading");},
    ajaxStop: function() {$body.removeClass("loading");}    
});

function myFunction() {
	$body.removeClass("loading");
	location.reload();
}

function nobackbutton(){
	window.location.hash="no-back-button";
	window.location.hash="Again-No-back-button" //chrome
	window.onhashchange=function(){window.location.hash="";}
}

function nuevaVistaResumen(idBitacora){
	//CAMBIA A LA VISTA RESUMEN
	var banderaPinta=false;
	var tamListaDetalle=0;
	var detalleArray = new Array();
	
	//LIMPIA NOMBRE SUCURSAL
	document.getElementById("sucursalContainer").innerHTML = "";	
	//LIMPIA LOS DATOS DE LA PESTAÑA FOLIOS
	document.getElementById("idTotalFolios").innerHTML = "";		
	document.getElementById('tablaFolioContenido').innerHTML = "";
	//LIMPIA LOS DATOS DE LA PESTAÑA EVIDENCIAS
	document.getElementById("idTotalEvidencia").innerHTML = "";
	document.getElementById('tablaEvidenciasContenido').innerHTML = "";
	//LIMPIA LOS DATOS DE LA PESTAÑA FOLIOS
	document.getElementById('tipoFuenteDosFolio').innerHTML = "";
	document.getElementById('tipoFuenteUnoPregunta').innerHTML = "";
	document.getElementById('tipoFuenteUnoCuadro').innerHTML = "";
	//LIMPIA LA IMAGEN
	document.getElementById('margenImagen').innerHTML = "";

	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxVistaResumen.json?idBitacora='+idBitacora,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {	
			
			if(json!=null){
				
				if(json[0]!=null){
					//LLENADO DE LA OPCION FOLIOS Y EVIDENCIAS TOTALES
					document.getElementById("idTotalFolios").innerHTML = json[0][1];
					document.getElementById("idTotalEvidencia").innerHTML ="Total de Evidencias: "+ json[0][0];
				}
				if(json[3]!=null){
					//LLENADO DE LA OPCION FOLIOS
					for (var j = 0; j < Object.keys(json[3]).length; j++) {
						$("#tablaFolioContenido").append("" +
								"<tr class='renglonEvidencia'>" +
									"<td class='anchoTotalFolioIzq'>"+(j+1)+".- "+json[3][j].pregunta+"</td>" +
									"<td class='anchoTotalFolioDer'><a onclick='linkDetalle("+json[3][j].idPregunta+");' class='fuenteLinkFolio'>Ver</a></td>" +
								"</tr>");
					}
				}
				
				if(json[2]!=null){
					//LLENADO DE LA OPCION EVIDENCIAS	
					var r="";
					for (var j = 0; j < Object.keys(json[2]).length; j++) {
						r=rutaHostImagen+json[2][j].ruta;
						var rutaPreview=r.replace("imagenes", "preview");
						$("#tablaEvidenciasContenido").append("" +
								"<tr>" +
									"<td>"+(j+1)+".- "+json[2][j].descPreg+"</td>" +
									"<td><img onclick='linkDetalle("+json[2][j].idPregunta+");' src='"+rutaPreview+"' class='imagenPreviewEvidencia'></td>" +
								"</tr>");
					}
				}
				
				if(json[1]!=null){
					//NOMBRE SUCURSAL
					document.getElementById("sucursalContainer").innerHTML = json[1][0].nombresuc;					
					//LLENADO DE LA FECHA EN LA PESTAÑA FECHA Y HORA
					document.getElementById("fInicio").innerHTML =json[1][0].fecha_inicio;
					document.getElementById("fFin").innerHTML =json[1][0].fecha_fin;
					document.getElementById("fEnvio").innerHTML =json[1][0].fechaEnvio;
					//LLENADO DE LA HORA EN LA PESTAÑA FECHA Y HORA
					document.getElementById("hInicio").innerHTML =json[1][0].horaInicio;
					document.getElementById("hFin").innerHTML =json[1][0].horaFin;
					document.getElementById("hModo").innerHTML =json[1][0].modo;						
				}
				
				if(json[4]!=null){
					detalleArray=json[4];
					tamListaDetalle=Object.keys(json[4]).length;
					
					if(tamListaDetalle>0){
						//LLENA EL PRIMER LA PRIMER PREGUNTA EN LA VISTA DETALLE
						document.getElementById("tipoFuenteDosFolio").innerHTML = json[4][0].folio;
						document.getElementById("tipoFuenteUnoPregunta").innerHTML = json[4][0].orden+".- "+json[4][0].desPregunta;
						document.getElementById("tipoFuenteUnoCuadro").innerHTML = json[4][0].descripcion;
						document.getElementById("tipoFuenteDosAccion").innerHTML = json[4][0].accion;
						
						var rutaPreview1=rutaHostImagen+""+json[4][0].evidencia;
						
						if(json[4][0].evidencia!="" || json[4][0].evidencia!=null)
							$("#margenImagen").append("<img onclick='muestraPopup(&apos;"+rutaPreview1+"&apos;);' class='tamaImagen' src='"+rutaPreview1+"'>");
						else
							$("#margenImagen").append("<img class='tamaImagenNoRequerida' src='../images/reporteOnline/evidencia_no_requerida.png'>");
				
					}
				}
				creaCirculos(tamListaDetalle);				
				banderaPinta=true;
				
			}else{
				alert('No hay datos de visita en este dia');
			}	
		}		
	});
}

/*FUNCIONES PARA EL CARRUSEL DE IMAGENES*/
var arrayEvidencias = new Array();
var indiceActual = 0;
var htm;

function init(){
	indiceActual = 0;
		
	for (var i = 0; i < json.length ; i++){
    	arrayEvidencias.push(json[i]);
	}
	var html = "<tr>";
	for(var i = 0; i < arrayEvidencias.length ; i++){
		html += "<td class='tcenter' style='width:"+100/arrayEvidencias.length+"%;'><span class='raya'>|</span><br><p>"+(i+1)+"</p></td>";
	}
	html += "</tr>";

	document.getElementById("tblrangeSlider").innerHTML = html;		

	document.getElementById("preguntaN").innerHTML = "";					
	document.getElementById("respuestaN").innerHTML = "";						
	document.getElementById("folioN").innerHTML = "";						
	document.getElementById("accionN").innerHTML = "";					
	document.getElementById("fechaCorreccionN").innerHTML = "";	
	document.getElementById("tamaImagen").src = "";	

	document.getElementById("preguntaN").innerHTML =  arrayEvidencias[0].orden +".- "+arrayEvidencias[0].desPregunta + "- ";					
	document.getElementById("respuestaN").innerHTML = arrayEvidencias[0].descripcion;						
	document.getElementById("folioN").innerHTML = arrayEvidencias[0].folio;						
	document.getElementById("accionN").innerHTML = arrayEvidencias[0].accion;		
	if (arrayEvidencias[0].evidencia == null){
	document.getElementById("tamaImagen").src = "../images/reporteOnline/evidencia_no_requerida.png";	
	} else{
		document.getElementById("tamaImagen").src = rutaHostImagen+arrayEvidencias[iArray].evidencia;	
	}				
}

//Funcion para cargar el carrusel de evidencias por posicion en el array
function cargaEvidencias(iArray){
	document.getElementById("preguntaN").innerHTML = "";					
	document.getElementById("respuestaN").innerHTML = "";						
	document.getElementById("folioN").innerHTML = "";						
	document.getElementById("accionN").innerHTML = "";					
	document.getElementById("fechaCorreccionN").innerHTML = "";	
	document.getElementById("tamaImagen").src = "";	

	document.getElementById("preguntaN").innerHTML =  arrayEvidencias[iArray].orden +".- "+arrayEvidencias[iArray].desPregunta + "- ";					
	document.getElementById("respuestaN").innerHTML = arrayEvidencias[iArray].descripcion;						
	document.getElementById("folioN").innerHTML = arrayEvidencias[iArray].folio;						
	document.getElementById("accionN").innerHTML = arrayEvidencias[iArray].accion;		
	if (arrayEvidencias[iArray].evidencia == null){
		document.getElementById("tamaImagen").src = "../images/reporteOnline/evidencia_no_requerida.png";	
		} else{
			document.getElementById("tamaImagen").src = rutaHostImagen+arrayEvidencias[iArray].evidencia ;	

		}
	//Recorrer el array y pintar el carrusel
	indiceActual = iArray;
	slider(1, json.length, 1, indiceActual+1, '.amount div');
}

//Funcion para cargar evidencias en especifico por idPregunta
function cargaEvidenciasXIdPreg(idPreg){
	var i = 0;
	if (idPreg == 0)
		cargaEvidencias(0);
	else{
		for (i ; i < arrayEvidencias.length ; i++){
	    	if (arrayEvidencias[i].idPregunta == (idPreg)){
	    		cargaEvidencias(i);
	    		break;
	    	}
		}
	}
}

/*METODOS DEL SCRIPT PARA EL CARRUSEL*/
function siguienteDetalle(){
	indiceActual++;
	if (indiceActual < arrayEvidencias.length){
		document.getElementById("preguntaN").innerHTML = "";					
		document.getElementById("respuestaN").innerHTML = "";						
		document.getElementById("folioN").innerHTML = "";						
		document.getElementById("accionN").innerHTML = "";					
		document.getElementById("fechaCorreccionN").innerHTML = "";	
		document.getElementById("tamaImagen").src = "";	
			
		document.getElementById("preguntaN").innerHTML =  arrayEvidencias[indiceActual].orden +".- "+arrayEvidencias[indiceActual].desPregunta + "- ";					
		document.getElementById("respuestaN").innerHTML = arrayEvidencias[indiceActual].descripcion;						
		document.getElementById("folioN").innerHTML = arrayEvidencias[indiceActual].folio;						
		document.getElementById("accionN").innerHTML = arrayEvidencias[indiceActual].accion;		
			if (arrayEvidencias[indiceActual].evidencia == null){
			document.getElementById("tamaImagen").src = "../images/reporteOnline/evidencia_no_requerida.png";	
			} else{
				document.getElementById("tamaImagen").src = rutaHostImagen+arrayEvidencias[indiceActual].evidencia ;	
			}
	} else if (indiceActual = arrayEvidencias.length){
		indiceActual = arrayEvidencias.length-1;
	}
	slider(1, json.length, 1, indiceActual+1, '.amount div');
}

function anteriorDetalle(){
	indiceActual--;

	if (indiceActual > -1){
		document.getElementById("preguntaN").innerHTML = "";					
		document.getElementById("respuestaN").innerHTML = "";						
		document.getElementById("folioN").innerHTML = "";						
		document.getElementById("accionN").innerHTML = "";					
		document.getElementById("fechaCorreccionN").innerHTML = "";	
		document.getElementById("tamaImagen").src = "";	
			
		document.getElementById("preguntaN").innerHTML =  arrayEvidencias[indiceActual].orden +".- "+arrayEvidencias[indiceActual].desPregunta + "- ";					
		document.getElementById("respuestaN").innerHTML = arrayEvidencias[indiceActual].descripcion;						
		document.getElementById("folioN").innerHTML = arrayEvidencias[indiceActual].folio;						
		document.getElementById("accionN").innerHTML = arrayEvidencias[indiceActual].accion;		
			if (arrayEvidencias[indiceActual].evidencia == null){
			document.getElementById("tamaImagen").src = "../images/reporteOnline/evidencia_no_requerida.png";	
			} else{
				document.getElementById("tamaImagen").src = rutaHostImagen+arrayEvidencias[indiceActual].evidencia ;	
			}
	} else if (indiceActual = -1){
		indiceActual = 0;
	}
	slider(1, json.length, 1, indiceActual+1, '.amount div');
}
/*FIN DE LOS METODOS SCRIPT PARA EL CARRUSEL*/

function regresaVistaTerrit(porcentajeG,banderaCecoPadre,nivelPerfil,idCecoPadre,idCeco,nombreCeco,seleccionAno,seleccionMes,seleccionCanal,seleccionPais){

	//alert()
	setTimeout(myFunction, 20000);
	$body.addClass("loading");
	//alert('formTerri..... '+ banderaCecoPadre + " " + nivelPerfil + " "+idCecoPadre + " "+nombreCeco + " "+seleccionAno + " "+seleccionMes + " "+seleccionCanal + " "+seleccionPais );
	
	$("#formTerri").append("<input type='hidden' name='porcentajeG' id='porcentajeG' value='"+porcentajeG+"' />");
	$("#formTerri").append("<input type='hidden' name='banderaCecoPadre' id='banderaCecoPadre' value='"+banderaCecoPadre+"' />");
	$("#formTerri").append("<input type='hidden' name='nivelPerfil' id='nivelPerfil' value='"+nivelPerfil+"' />");
	$("#formTerri").append("<input type='hidden' name='idCeco' id='idCeco' value='"+idCeco+"' />");
	$("#formTerri").append("<input type='hidden' name='idCecoPadre' id='idCecoPadre' value='"+idCecoPadre+"' />");
	$("#formTerri").append("<input type='hidden' name='nombreCeco' id='nombreCeco' value='"+nombreCeco+"' />");
	$("#formTerri").append("<input type='hidden' name='seleccionAno' id='seleccionAno' value='"+seleccionAno+"' />");
	$("#formTerri").append("<input type='hidden' name='seleccionMes' id='seleccionMes' value='"+seleccionMes+"' />");
	$("#formTerri").append("<input type='hidden' name='seleccionCanal' id='seleccionCanal' value='"+seleccionCanal+"' />");
	$("#formTerri").append("<input type='hidden' name='seleccionPais' id='seleccionPais' value='"+seleccionPais+"' />");
	
	document.getElementById("formTerri").submit();
	return true;
}