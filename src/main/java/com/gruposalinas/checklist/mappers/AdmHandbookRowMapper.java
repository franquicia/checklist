package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.AdmHandbookDTO;


public class AdmHandbookRowMapper implements RowMapper<AdmHandbookDTO> {

	public AdmHandbookDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AdmHandbookDTO admHandbookDTO = new AdmHandbookDTO();
		
		admHandbookDTO.setIdBook(rs.getString("FIID_BOOK"));
		admHandbookDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
		admHandbookDTO.setRuta(rs.getString("FCRUTA"));
		admHandbookDTO.setActivo(rs.getString("FIACTIVO"));
		admHandbookDTO.setPadre(rs.getString("FIPADRE"));
		admHandbookDTO.setTipo(rs.getString("FCTIPO"));
		admHandbookDTO.setUsuarioMod(rs.getString("FCUSUARIO_MOD"));
		admHandbookDTO.setFechaMod(rs.getString("FDFECHA_MOD"));
		admHandbookDTO.setFecha(rs.getString("FDFECHA"));
		
		
		return admHandbookDTO;
	}
}
