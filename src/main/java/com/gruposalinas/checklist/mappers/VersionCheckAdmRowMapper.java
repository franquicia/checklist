package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.VersionExpanDTO;

public class VersionCheckAdmRowMapper implements RowMapper<VersionExpanDTO> {

	public VersionExpanDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		VersionExpanDTO verAdmin = new VersionExpanDTO();
		
		verAdmin.setIdTab(rs.getInt("FIID_VERSCHEC"));
		verAdmin.setIdCheck(rs.getInt("FIID_CHECKLIST"));
		verAdmin.setIdVers(rs.getInt("FIVERSION"));
		verAdmin.setIdactivo(rs.getInt("FIIDACTIVO"));
		verAdmin.setAux(rs.getString("FCAUX"));
		verAdmin.setObs(rs.getString("FCOBSERVACION"));
		verAdmin.setPeriodo(rs.getString("FCPERIODO"));
		verAdmin.setNego(rs.getString("FCNEGO"));
	    

		return verAdmin;
	}

}


