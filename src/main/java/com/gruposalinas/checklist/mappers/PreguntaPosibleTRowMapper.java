package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.PreguntaPosibleTDTO;


public class PreguntaPosibleTRowMapper  implements RowMapper<PreguntaPosibleTDTO>  {
	public PreguntaPosibleTDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		PreguntaPosibleTDTO posiblesPregDTO = new PreguntaPosibleTDTO();
		
		posiblesPregDTO.setIdPosiblePreg(rs.getInt("FIID_PREGUNTA"));
		posiblesPregDTO.setIdPregunta(rs.getInt("FIIDPOSIPREG"));
		posiblesPregDTO.setNumeroRevision(rs.getString("FINUMREVISION"));
		posiblesPregDTO.setTipoCambio(rs.getString("FCTIPOMODIF"));
		
		return posiblesPregDTO;
	}
}
