package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gruposalinas.checklist.dao.PerfilUsuarioDAO;
import com.gruposalinas.checklist.domain.PerfilUsuarioDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class PerfilUsuarioBI {

	private static Logger logger = LogManager.getLogger(PerfilBI.class);
	
	@Autowired
	PerfilUsuarioDAO perfilUsuarioDAO;
	
	
	public List<PerfilUsuarioDTO> obtienePerfiles(String idUsuario, String idPerfil){
	
		List<PerfilUsuarioDTO> listaPerfiles = null;
		
		try {
			listaPerfiles = perfilUsuarioDAO.obtienePerfiles(idUsuario, idPerfil);
		} catch (Exception e) {
			logger.info("No fue posible obtener los perfiles");
			
		}
		
		return listaPerfiles;
	}
	
	public boolean insertaPerfilUsuario(PerfilUsuarioDTO perfilUsuarioDTO){
		
		boolean respuesta = false ;
		
	    try {
			respuesta = perfilUsuarioDAO.insertaPerfilUsuario(perfilUsuarioDTO);
		} catch (Exception e) {
			logger.info("No fue posible insertar el perfil");
			
		}
	    
		return respuesta;		
	
	}
public boolean insertaPerfilUsuarioNvo(PerfilUsuarioDTO perfilUsuarioDTO){
		
		boolean respuesta = false ;
		
	    try {
			respuesta = perfilUsuarioDAO.insertaPerfilUsuarioNvo(perfilUsuarioDTO);
		} catch (Exception e) {
			logger.info("No fue posible insertar el perfil");
			
		}
	    
		return respuesta;		
	
	}
	public boolean actualizaPerfilUsuario(PerfilUsuarioDTO perfilUsuarioDTO){
	
		boolean respuesta = false ;
		
	    try {
			respuesta = perfilUsuarioDAO.actualizaPerfilUsuario(perfilUsuarioDTO);
		} catch (Exception e) {
			logger.info("No fue posible actualziar el perfil");
			
		}
	    
		return respuesta;	
	}

	public boolean eliminaPerfilUsaurio(PerfilUsuarioDTO perfilUsuarioDTO){
		boolean respuesta = false ;
		
	    try {
			respuesta = perfilUsuarioDAO.eliminaPerfilUsaurio(perfilUsuarioDTO);
		} catch (Exception e) {
			logger.info("No fue posible eliminar el perfil");
			
		}
	    
		return respuesta;	
	}
	
	public boolean verificaPerfilTienda(String idUsuario){
		
		boolean respuesta = false;
		
		try {
			
			respuesta = perfilUsuarioDAO.verificaPerfilTienda(idUsuario);
			
		} catch (Exception e) {
			logger.info("Algo sucedio al verificar el perfil");
		}
		
		return respuesta;
	}
}
