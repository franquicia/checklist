package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.PosiblesDTO;
import com.gruposalinas.checklist.mappers.PosiblesTRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PosiblesTDAOImpl extends DefaultDAO implements PosiblesTDAO {

    private static Logger logger = LogManager.getLogger(PosiblesTDAOImpl.class);

    private DefaultJdbcCall jdbcBuscaPosibleTemp;
    private DefaultJdbcCall jdbcInsertaPosibleTemp;
    private DefaultJdbcCall jdbcActualizaPosibleTemp;
    private DefaultJdbcCall jdbcEliminaPosibleTemp;

    public void init() {

        jdbcBuscaPosibleTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPOSIBLESTEMP")
                .withProcedureName("SP_SELGRALPOS")
                .returningResultSet("RCL_POS", new PosiblesTRowMapper());

        jdbcInsertaPosibleTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPOSIBLESTEMP")
                .withProcedureName("SP_INSPOSIBLE_TEMP");

        jdbcActualizaPosibleTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPOSIBLESTEMP")
                .withProcedureName("SP_ACTPOSIBLE_TEMP");

        jdbcEliminaPosibleTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPOSIBLESTEMP")
                .withProcedureName("SP_DELPOSIBLE_TEMP");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PosiblesDTO> buscaPosibleTemp() throws Exception {
        Map<String, Object> out = null;

        List<PosiblesDTO> listaPosible = null;
        int ejecucion = 0;

        out = jdbcBuscaPosibleTemp.execute();

        logger.info("Funcion ejecutada: {checklist.PAADMPOSIBLESTEMP.SP_SELGPOS_TEMP}");

        listaPosible = (List<PosiblesDTO>) out.get("RCL_POS");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al obtener las Posibles Respuestas de la tabla temporal");
        } else {
            return listaPosible;
        }

        return null;
    }

    @SuppressWarnings("unused")
    @Override
    public boolean insertaPosibleTemp(PosiblesDTO bean) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_NUM_REV", bean.getNumeroRevision()).addValue("PA_TIPO_MOD", bean.getTipoCambio());

        out = jdbcInsertaPosibleTemp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPOSIBLESTEMP.SP_INSPOSIBLE_TEMP}");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al insertar la Posible Respuesta en la tabla temporal");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaPosibleTemp(PosiblesDTO bean) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPOS", bean.getIdPosible())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_NUM_REV", bean.getNumeroRevision()).addValue("PA_TIPO_MOD", bean.getTipoCambio());

        out = jdbcActualizaPosibleTemp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPOSIBLESTEMP.SP_ACTPOSIBLE_TEMP}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al actualizar la Posible Respuesta  en la tabla temporal");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaPosibleTemp(int idPosible) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPOS", idPosible);

        out = jdbcEliminaPosibleTemp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPOSIBLESTEMP.SP_DELPOSIBLE_TEMP}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al eliminar la Posible Respuesta de la tabla temporal ");
        } else {
            return true;
        }

        return false;
    }

}
