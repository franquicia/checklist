package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.FiltrosCecoDTO;
import com.gruposalinas.checklist.domain.PaisDTO;

public interface FiltrosCecoDAO {

	public Map<String, Object> obtieneFiltros(int idUsuario) throws Exception;
	
	public List<FiltrosCecoDTO> obtieneCecos(int idCeco, int tipoBusqueda) throws Exception;
	
	public List<PaisDTO> obtienePaises(int negocio) throws Exception;
	
	public List<FiltrosCecoDTO> obtieneTerritorios(int pais, int negocio) throws Exception;
	
	public List<ChecklistDTO> obtieneChecklist(int idUsuario) throws Exception;
	
	public List<PaisDTO> obtienePaises(int negocio, int idUsuario) throws Exception;
}
