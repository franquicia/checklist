<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Banco Azteca | Reporte RH</title>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/vistaReportes/estiloTiempoReal.css" />

<link rel="stylesheet" media="screen"
	href="../css/vistaCumplimientoVisitas/modal.css">

<!-- jquery Start -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/vistaCumplimientoVisitas/jquery.js"></script>

<!-- jquery 2.2.4 -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery/jquery-2.1.1.min.js"></script>

<!-- dropdown -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/vistaCumplimientoVisitas/menu.js"></script>

<!-- dropdown -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/vistaReportes/script-reporteTiempoRealHoy.js"></script>

<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">

</head>
<body onload="nobackbutton();">


	<div id="tipoModal" class="modal3"></div>

	<input type="hidden" id="longitudRegiones"
		value="${fn:length(listaCecos)}"></input>

	<div class="wrapper">

		<div class="cifraTitulo">
			<div class="cifraTituloHijo">
				<p style="margin-right: 20px;">*Cifras en miles</p>
			</div>
		</div>

		<div class="hspacer"></div>

		<div class="header"
			style="z-index: 10; top: 0px; height: 140px; min-width: 600px; background: #f2f2f2;">
			<div class="wrapperFixed" style="background: #f2f2f2;">
				<table
					style="width: 96%; max-width: 800px; min-width: 600px; margin: 0 auto;">
					<tr>
						<td style="width: 21%;">
							<table id="unicaTabla" onclick="menuTiempoReal();"
								onkeyup="menuTiempoRealDesaparece();" class="avance"
								style="max-width: 150px; min-width: 100px; height: 50px; background: #a6b0b3; border-radius: 20px 20px 20px 20px; cursor: pointer; margin-top: -30px;">
								<thead>
									<tr style="height: 50px;">
										<th style="width: 60%; text-align: right;">
											<p class="margenArriba"
												style="font-size: 40px; margin: auto;">Hoy</p>
										</th>
										<th style="width: 30%;"><img
											style="width: 17px; height: 17px; margin-right: 10px; margin-top: 6px"
											src="../images/flecha.png">
											</p></th>
									</tr>
								</thead>

								<div id="idMenuTiempoReal" class="opcion" style="display: none;">
									<ul class="sinStyle">
										<li>
											<ul class="sinStyle">
												<li class="linkMenu arriba"><a
													onclick="menuTiempoRealDesaparece();"
													class="linkMenuHijo arribaHijo" href="/checklist/reportes/reporteTiempoReal.htm"><p
															class="textoCen">Semana</p></a></li>
												<li class="linkMenu laterales"><a
													onclick="menuTiempoRealDesaparece();" class="linkMenuHijo"
													href="#"><p class="textoCen">4 semanas</p></a></li>
												<li class="linkMenu laterales"><a
													onclick="menuTiempoRealDesaparece();" class="linkMenuHijo"
													href="#"><p class="textoCen">13 semanas</p></a></li>
												<li class="linkMenu abajo"><a
													onclick="menuTiempoRealDesaparece();"
													class="linkMenuHijo abajoHijo" href="#"><p
															class="textoCen">52 semanas</p></a></li>
											</ul>
										</li>
									</ul>
								</div>

								<div class="margenOpcionRegresar">

									<p id="idBtnReg" onclick="regresar();"
										class="pointer botonRegresa">Regresar</p>
									<p id="idBtnRegNieto" onclick="regresarNieto();"
										class="pointer botonRegresa">Regresar</p>

								</div>


							</table>
						</td>


						<td style="width: 39.5%">
							<table class="avance"
								style="max-width: 345px; min-width: 232px; height: 110px; background: #a6b0b3; border-radius: 20px 20px 20px 20px;">
								<thead>
									<tr style="height: 30px;">
										<th style="width: ${70}%;" colspan="3"><p
												class="margenArriba">Préstamos Personales</p></th>
									</tr>
									<tr style="height: 70px;">
										<th style="width: ${(70/3)}%;"><p class="margenAbajo">
												Mi <br>compromiso
											</p></th>
										<th style="width: ${(70/3)}%;"><p class="margenAbajo">Real</p></th>
										<th style="width: ${(70/3)}%;"><p class="margenAbajo">%
												Avance</p></th>

										<!-- <th style="width: ${(70/3)}%; min-width:110px;">
											<table style="width: 98%; height: 100%; float: right;">
												<tr style="height: ${100/3}%;">
													<th> 
														<div class="radioP verdeP"><p class=margenCirculos>  >65% </p> </div> 
													</th>
												</tr>
												<tr style="height: ${100/3}%;">
													<th style="width:50%"> 
														<div class="radioP amarilloP"><p class="margenCirculos"> >55% </p></div> 
													</th>
													<th style="width:50%"> 
														<div><p class="margenCirculos" style="margin-left: 7px"> <65% </p></div> 
													</th>
												</tr>
												<tr style="height: ${100/3}%;">
													<th> 
														<div class="radioP rojoP"><p class="margenCirculos"> <55%</p></div> 
													</th>
												</tr>
											</table>
										</th> 
									-->
									</tr>
								</thead>
							</table>
						</td>
						<td style="width: 0.1%"></td>
						<td style="width: 39%">
							<table class="avance"
								style="max-width: 345px; min-width: 232px; height: 110px; background: #a6b0b3; border-radius: 20px 20px 20px 20px;">
								<thead>
									<tr style="height: 30px;">
										<th style="width: ${70}%;" colspan="3"><p
												class="margenArriba">Consumo</p></th>
									</tr>
									<tr style="height: 70px;">
										<th style="width: ${(70/3)}%;"><p class="margenAbajo">
												Mi <br>compromiso
											</p></th>
										<th style="width: ${(70/3)}%;"><p class="margenAbajo">Real</p></th>
										<th style="width: ${(70/3)}%;"><p class="margenAbajo">%
												Avance</p></th>

										<!-- <th style="width: ${(70/3)}%; min-width:110px;">
											<table style="width: 98%; height: 100%; float: right;">
												<tr style="height: ${100/3}%;">
													<th> 
														<div class="radioP verdeP"><p class=margenCirculos>  >65% </p> </div> 
													</th>
												</tr>
												<tr style="height: ${100/3}%;">
													<th style="width:50%"> 
														<div class="radioP amarilloP"><p class="margenCirculos"> >55% </p></div> 
													</th>
													<th style="width:50%"> 
														<div><p class="margenCirculos" style="margin-left: 7px"> <65% </p></div> 
													</th>
												</tr>
												<tr style="height: ${100/3}%;">
													<th> 
														<div class="radioP rojoP"><p class="margenCirculos"> <55%</p></div> 
													</th>
												</tr>
											</table>
										</th> 
										-->
									</tr>
								</thead>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>


		<div style="margin-top: 40px; background: #f2f2f2; position: fixed; width: 100%;"
			id="idGeneralTablaNombreTitulo">
			<dl class="accordion3 box">
				<dt style="background: #ffffff;">
					<table class="bloquer linea goshadow1">
						<tbody>
							<tr>
								<td onclick="ordenaNombre('div.divDT','')" id="nomTotal" class="nombreCheck"
									style="background-image: none;">TOTAL MÉXICO
								</td>
								<td onclick="ordenaNombre('div.divDT','')" class="margenFechasGris">
									<img id="nomVista" src="../images/flechasGris.png">
									<img id="nomAscendente" src="../images/flechaNegra.png">
									<img id="nomDescendente" src="../images/flechaNegra.png">
								</td>
								<td id="preCTotal" class="nombreCheck"
									style="width: ${70/6}%; 
									background-image: url('../images/vistaCumplimientoVisitas/lineaNegra.png');
									background-repeat: no-repeat;
									background-position: 100% 6px;">
									-</td>
								<td id="preRTotal" class="nombreCheck"
									style="width: ${70/6}%;
									background-image: url('../images/vistaCumplimientoVisitas/lineaNegra.png');
									background-repeat: no-repeat;
									background-position: 100% 6px;">
									-</td>
								<td onclick="ordenaAvancePrestamos('div.divDT','')" id="prePTotal" class="nombreCheck" style="width: ${70/6}%;">
									<div class="radio grisN">-</div>
								</td>
								<td onclick="ordenaAvancePrestamos('div.divDT','')" class="margenFechasGris">
									<img id="presVista" src="../images/flechasGris.png">
									<img id="presAscendente" src="../images/flechaNegra.png">
									<img id="presDescendente" src="../images/flechaNegra.png">
								</td>
								<td style="width: 0.1%; background: #f2f2f2;"></td>
								<td id="conCTotal" class="nombreCheck"
									style="width: ${70/6}%;
									background-image: url('../images/vistaCumplimientoVisitas/lineaNegra.png');
									background-repeat: no-repeat;
									background-position: 100% 6px;">
									-</td>
								<td id="conRTotal" class="nombreCheck"
									style="width: ${70/6}%;
									background-image: url('../images/vistaCumplimientoVisitas/lineaNegra.png');
									background-repeat: no-repeat;
									background-position: 100% 6px;">
									-</td>
								<td onclick="ordenaAvanceConsumo('div.divDT','')" id="conPTotal" class="nombreCheck" style="width: ${70/6}%;">
									<div class="radio grisN">-</div>
								</td>
								<td onclick="ordenaAvanceConsumo('div.divDT','')" class="margenFechasGris">
									<img id="consVista" src="../images/flechasGris.png">
									<img id="consAscendente" src="../images/flechaNegra.png">
									<img id="consDescendente" src="../images/flechaNegra.png">
								</td>
							</tr>
						</tbody>
					</table>
				</dt>
			</dl>
		</div>

		<div style="margin-top: 40px; background: #f2f2f2; position: fixed; width: 100%; display: none;"
			id="idGeneralTablaNombreTituloHijo">
			<dl class="accordion3 box">
				<dt style="background: #ffffff;">
					<table class="bloquer linea goshadow1">
						<tbody>
							<tr>
								<td onclick="ordenaNombre('div.divDTHijo','Hijo')" id="nomTotalHijo" class="nombreCheck"
									style="background-image: none;">TOTAL DE LA ZONA</td>
								
								<td onclick="ordenaNombre('div.divDTHijo','Hijo')" class="margenFechasGris">
									<img id="nomVistaHijo" src="../images/flechasGris.png">
									<img id="nomAscendenteHijo" src="../images/flechaNegra.png">
									<img id="nomDescendenteHijo" src="../images/flechaNegra.png">
								</td>
									
								<td id="preCTotalHijo" class="nombreCheck"
									style="width: ${70/6}%; 
									background-image: url('../images/vistaCumplimientoVisitas/lineaNegra.png');
									background-repeat: no-repeat;
									background-position: 100% 6px;">
									-</td>
								<td id="preRTotalHijo" class="nombreCheck"
									style="width: ${70/6}%;
									background-image: url('../images/vistaCumplimientoVisitas/lineaNegra.png');
									background-repeat: no-repeat;
									background-position: 100% 6px;">
									-</td>
								<td onclick="ordenaAvancePrestamos('div.divDTHijo','Hijo')" id="prePTotalHijo" class="nombreCheck"
									style="width: ${70/6}%;">
									<div class="radio grisN">-</div>
								</td>
								<td onclick="ordenaAvancePrestamos('div.divDTHijo','Hijo')" class="margenFechasGris">
									<img id="presVistaHijo" src="../images/flechasGris.png">
									<img id="presAscendenteHijo" src="../images/flechaNegra.png">
									<img id="presDescendenteHijo" src="../images/flechaNegra.png">
								</td>
								<td style="width: 0.1%; background: #f2f2f2;"></td>
								<td id="conCTotalHijo" class="nombreCheck"
									style="width: ${70/6}%;
									background-image: url('../images/vistaCumplimientoVisitas/lineaNegra.png');
									background-repeat: no-repeat;
									background-position: 100% 6px;">
									-</td>
								<td id="conRTotalHijo" class="nombreCheck"
									style="width: ${70/6}%;
									background-image: url('../images/vistaCumplimientoVisitas/lineaNegra.png');
									background-repeat: no-repeat;
									background-position: 100% 6px;">
									-</td>
								<td onclick="ordenaAvanceConsumo('div.divDTHijo','Hijo')" id="conPTotalHijo" class="nombreCheck"
									style="width: ${70/6}%;">
									<div class="radio grisN">-</div>
								</td>
								<td onclick="ordenaAvanceConsumo('div.divDTHijo','Hijo')" class="margenFechasGris">
									<img id="consVistaHijo" src="../images/flechasGris.png">
									<img id="consAscendenteHijo" src="../images/flechaNegra.png">
									<img id="consDescendenteHijo" src="../images/flechaNegra.png">
								</td>
							</tr>
						</tbody>
					</table>
				</dt>
			</dl>
		</div>

		<div style="margin-top: 40px; background: #f2f2f2; position: fixed; width: 100%; display: none;"
			id="idGeneralTablaNombreTituloNieto">
			<dl class="accordion3 box">
				<dt style="background: #ffffff;">
					<table class="bloquer linea goshadow1">
						<tbody>
							<tr>
								<td onclick="ordenaNombre('div.divDTNieto','Nieto')" id="nomTotalNieto" class="nombreCheck"
									style="background-image: none;">TOTAL DE LA REGION</td>
									
								<td onclick="ordenaNombre('div.divDTNieto','Nieto')" class="margenFechasGris">
									<img id="nomVistaNieto" src="../images/flechasGris.png">
									<img id="nomAscendenteNieto" src="../images/flechaNegra.png">
									<img id="nomDescendenteNieto" src="../images/flechaNegra.png">
								</td>
									
								<td id="preCTotalNieto" class="nombreCheck"
									style="width: ${70/6}%; 
									background-image: url('../images/vistaCumplimientoVisitas/lineaNegra.png');
									background-repeat: no-repeat;
									background-position: 100% 6px;">
									-</td>
								<td id="preRTotalNieto" class="nombreCheck"
									style="width: ${70/6}%;
									background-image: url('../images/vistaCumplimientoVisitas/lineaNegra.png');
									background-repeat: no-repeat;
									background-position: 100% 6px;">
									-</td>
								<td onclick="ordenaAvancePrestamos('div.divDTNieto','Nieto')" id="prePTotalNieto" class="nombreCheck"
									style="width: ${70/6}%;">
									<div class="radio grisN">-</div>
								</td>
								<td onclick="ordenaAvancePrestamos('div.divDTNieto','Nieto')" class="margenFechasGris">
									<img id="presVistaNieto" src="../images/flechasGris.png">
									<img id="presAscendenteNieto" src="../images/flechaNegra.png">
									<img id="presDescendenteNieto" src="../images/flechaNegra.png">
								</td>
								<td style="width: 0.1%; background: #f2f2f2;"></td>
								<td id="conCTotalNieto" class="nombreCheck"
									style="width: ${70/6}%;
									background-image: url('../images/vistaCumplimientoVisitas/lineaNegra.png');
									background-repeat: no-repeat;
									background-position: 100% 6px;">
									-</td>
								<td id="conRTotalNieto" class="nombreCheck"
									style="width: ${70/6}%;
									background-image: url('../images/vistaCumplimientoVisitas/lineaNegra.png');
									background-repeat: no-repeat;
									background-position: 100% 6px;">
									-</td>
								<td onclick="ordenaAvanceConsumo('div.divDTNieto','Nieto')" id="conPTotalNieto" class="nombreCheck"
									style="width: ${70/6}%;">
									<div class="radio grisN">-</div>
								</td>
								<td onclick="ordenaAvanceConsumo('div.divDTNieto','Nieto')" class="margenFechasGris">
									<img id="consVistaNieto" src="../images/flechasGris.png">
									<img id="consAscendenteNieto" src="../images/flechaNegra.png">
									<img id="consDescendenteNieto" src="../images/flechaNegra.png">
								</td>
							</tr>
						</tbody>
					</table>
				</dt>
			</dl>
		</div>


		<div id="idGeneralTablaTerritorios" class="box" style="margin-top: 110px;"></div>

		<div id="idGeneralTablaHijo" class="box" style="margin-top: 110px; display: none;"></div>

		<div id="idGeneralTablaNieto" class="box" style="margin-top: 110px; display: none;"></div>

		<div class="fspacer"></div>

	</div>

</body>
</html>