package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.RecursoDTO;

public interface RecursoDAO {

	public List<RecursoDTO> buscaRecurso(String idRecurso) throws Exception;
	
	public int insertaRecurso(RecursoDTO bean) throws Exception;
	
	public boolean actualizaRecurso(RecursoDTO bean) throws Exception;
	
	public boolean eliminaRecurso(int idRecurso) throws Exception;
}
