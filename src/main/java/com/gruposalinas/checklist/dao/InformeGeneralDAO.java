package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.InformeGeneralDTO;
import java.util.List;


public interface InformeGeneralDAO {

    public boolean insertaInformeGeneral(String idCeco, String fecha, int idUsuario, String rutaInforme, int idEstatus, String estatus, int numeroBitacora) throws Exception;
		
		public boolean eliminaInformeGeneral(String idCeco, String fecha, int idUsuario)throws Exception;
		
		public List<InformeGeneralDTO> obtieneDatosInformeGeneral(String idCeco, String fecha, int idUsuario, int estatus) throws Exception; 
		
		
		public boolean actualizaInformeGeneral(String idCeco, String fecha, String idUsuario, String rutaInforme, String idEstatus, String estatus, String numeroBitacora)throws Exception;
		 
		
	}