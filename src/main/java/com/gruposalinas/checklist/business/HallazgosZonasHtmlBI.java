package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.gruposalinas.checklist.domain.HallazgosTransfDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;

public class HallazgosZonasHtmlBI {

	public List<String> generaHtmlAdicionales(String ceco, String fase, String proyecto, String recorrido,
			String sucursal, int parametro) {

		List<HallazgosTransfDTO> listaAdicionales = new ArrayList<>();

		SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
				.getApplicationContext().getBean("soporteHallazgosTransfBI");

		listaAdicionales = soporteHallazgosTransfBI.obtieneAdicionalesCecoFaseProyecto(ceco, fase, proyecto, 0);

		// Separa hallazgos por zona.
		Map<String, List<HallazgosTransfDTO>> mapZonas = separaPorZonas(listaAdicionales); // Contemplar por si truena

		// Genera html
		List<String> zonasHtml = generaHtmlPorZonas(mapZonas, recorrido, sucursal);

		return zonasHtml;
	}

	public List<String> generaHtmlZonas(String ceco, String fase, String proyecto, String recorrido, String sucursal) {

		List<HallazgosTransfDTO> listaHallazgos = new ArrayList<>();

		HallazgosTransfBI hallazgosTransfBI = (HallazgosTransfBI) FRQAppContextProvider.getApplicationContext()
				.getBean("hallazgosTransfBI");

		listaHallazgos = hallazgosTransfBI.obtieneHallazgosExpansionIni(ceco, proyecto, fase);

		// Separa hallazgos por zona.
		Map<String, List<HallazgosTransfDTO>> mapZonas = separaPorZonas(listaHallazgos); // Contemplar por si truena

		// Genera html
		List<String> zonasHtml = generaHtmlPorZonas(mapZonas, recorrido, sucursal);

		return zonasHtml;
	}

	public List<String> generaHtmlZonasReporte(String ceco, String fase, String proyecto, String recorrido,
			String sucursal) {

		List<HallazgosTransfDTO> listaHallazgos = new ArrayList<>();

		HallazgosTransfBI hallazgosTransfBI = (HallazgosTransfBI) FRQAppContextProvider.getApplicationContext()
				.getBean("hallazgosTransfBI");

		listaHallazgos = hallazgosTransfBI.obtieneHallazgosExpansionIni(ceco, proyecto, fase);

		// Separa hallazgos por zona.
		Map<String, List<HallazgosTransfDTO>> mapZonas = separaPorZonas(listaHallazgos); // Contemplar por si truena

		// Genera html
		List<String> zonasHtml = generaHtmlPorZonasReporte(mapZonas, recorrido, sucursal);

		return zonasHtml;
	}

	public Map<String, List<HallazgosTransfDTO>> separaPorZonas(List<HallazgosTransfDTO> listaHallazgos) {

		Map<String, List<HallazgosTransfDTO>> mapZonas = new HashMap<>();

		if (listaHallazgos != null && listaHallazgos.size() > 0) {

			// Por zonaClasi
			for (HallazgosTransfDTO hallazgo : listaHallazgos) {

				putHallazgo(mapZonas, hallazgo);

			}

		}

		return mapZonas;

	}

	public void putHallazgo(Map<String, List<HallazgosTransfDTO>> mapZonas, HallazgosTransfDTO hallazgo) {

		boolean respuesta = false;
		List<HallazgosTransfDTO> listaZona = null;

		for (String key : mapZonas.keySet()) {

			if (key.equals(hallazgo.getZonaClasi())) {
				respuesta = true;
				break;
			}

		}

		if (!respuesta) {

			listaZona = new ArrayList<>();
			listaZona.add(hallazgo);
			mapZonas.put(hallazgo.getZonaClasi(), listaZona);

		} else {

			listaZona = mapZonas.get(hallazgo.getZonaClasi());
			listaZona.add(hallazgo);

		}

	}

	public List<String> generaHtmlPorZonas(Map<String, List<HallazgosTransfDTO>> zonas, String recorrido,
			String sucursal) {

		List<String> zonasHtml = new ArrayList<>();

		// Se recorre por zona
		for (String keyZona : zonas.keySet()) {

			try {
				// Genera las paginas que sean necesarioas dependeiendo de cuantos hallazgos
				// sean
				List<String> paginasZona = generaHtmlHallazgos(zonas.get(keyZona), recorrido, sucursal, keyZona);

				// Genera html con todas las paginas que se generaron
				String htmlZona = generaHtmlZona(paginasZona);

				zonasHtml.add(htmlZona);

			} catch (Exception e) {
				// POner Logger de que no se agrego tal zona
			}

		}

		return zonasHtml;

	}

	public List<String> generaHtmlPorZonasReporte(Map<String, List<HallazgosTransfDTO>> zonas, String recorrido,
			String sucursal) {

		List<String> zonasHtml = new ArrayList<>();

		// Se recorre por zona
		for (String keyZona : zonas.keySet()) {

			try {
				// Genera las paginas que sean necesarioas dependeiendo de cuantos hallazgos
				// sean
				List<String> paginasZona = generaHtmlHallazgosReporte(zonas.get(keyZona), recorrido, sucursal, keyZona);

				// Genera html con todas las paginas que se generaron
				String htmlZona = generaHtmlZona(paginasZona);

				zonasHtml.add(htmlZona);

			} catch (Exception e) {
				// POner Logger de que no se agrego tal zona
			}

		}

		return zonasHtml;

	}

	public List<String> generaHtmlHallazgos(List<HallazgosTransfDTO> hallazgos, String recorrido, String sucursal,
			String zona) {

		String htmlZona = "";

		int hallazgosPorHoja = 8;
		int hallazgosrecorridos = 0;
		int conteoHallazgos = 0;

		List<String> filas = new ArrayList<>();

		List<String> paginas = new ArrayList<>();

		for (HallazgosTransfDTO hallazgo : hallazgos) {

			hallazgosrecorridos++;
			conteoHallazgos++;

			String colorRow = "#FAFAFA";

			if (hallazgosrecorridos % 2 == 0) {
				colorRow = "#EDEDED";

			}

			/*
			 * String tr = "<tr width= '100%' height='100px' style='background-color: " +
			 * colorRow + "; border-top: 5px solid '>" +
			 * "	<td style='font-size: 13; text-align: center; width: 15%'>" +
			 * "		<div style='height:100px; width:100%;'>" +
			 * "       	<div style='background-color: "+getColorCriticidad(hallazgo.
			 * getCriticidad())
			 * +";height: 100px;width: 5%;display: flex;float: left;'> </div>" +
			 * "       	<div style='height: 100px;width: 95%;display: flex;float: left; '>"
			 * + "       			<p style='margin: auto;'>"+getTextCriticidad(hallazgo.
			 * getCriticidad())+"</p>" + "       	</div>" + "       </div>" + "   </td>" +
			 * "   <td style='font-size: 13; text-align: center; width: 15% '>"+hallazgo.
			 * getArea()+"</td>" +
			 * "	<td style='font-size: 13; text-align: center; width: 35% '>"+hallazgo.
			 * getPreg()+"</td>" +
			 * "   <td style='font-size: 13; text-align: center; width: 35% '>"+hallazgo.
			 * getObs()+"</td>" + "</tr>";
			 */

			String tr = "<tr width= '100%' height='100px' style='background-color: " + colorRow
					+ "; border-top: 5px solid '>"
					+ "	<td style='font-size: 13; text-align: center; width: 1%; background-color: "
					+ getColorCriticidad(hallazgo.getCriticidad()) + "'>" + "   </td>"
					+ "	<td style='font-size: 13; text-align: center; width: 14%'>"
					+ getTextCriticidad(hallazgo.getCriticidad()) + "   </td>"
					+ "   <td style='font-size: 13; text-align: center; width: 15% '>" + hallazgo.getArea() + "</td>"
					+ "	<td style='font-size: 13; text-align: center; width: 35% '>" + hallazgo.getPreg() + "</td>"
					+ "   <td style='font-size: 13; text-align: center; width: 35% '>" + hallazgo.getObs() + "</td>"
					+ "</tr>";

			filas.add(tr);

			if ((conteoHallazgos < hallazgos.size() && hallazgosrecorridos == hallazgosPorHoja)
					|| conteoHallazgos == hallazgos.size()) {

				// Genera pagina con los hallazgos que se encuentran en la lista "filas"
				String pagina = generaPagina(filas, recorrido, sucursal, zona);
				paginas.add(pagina);

				hallazgosrecorridos = 0;
				filas.clear();
			}

		}

		return paginas;

	}

	public List<String> generaHtmlHallazgosReporte(List<HallazgosTransfDTO> hallazgos, String recorrido,
			String sucursal, String zona) {

		String htmlZona = "";

		int hallazgosPorHoja = 8;
		int hallazgosrecorridos = 0;
		int conteoHallazgos = 0;

		List<String> filas = new ArrayList<>();

		List<String> paginas = new ArrayList<>();

		for (HallazgosTransfDTO hallazgo : hallazgos) {

			hallazgosrecorridos++;
			conteoHallazgos++;

			String colorRow = "#FAFAFA";

			if (hallazgosrecorridos % 2 == 0) {
				colorRow = "#EDEDED";

			}

			/*
			 * String tr = "<tr width= '100%' height='100px' style='background-color: " +
			 * colorRow + "; border-top: 5px solid '>" +
			 * "	<td style='font-size: 13; text-align: center; width: 15%'>" +
			 * "		<div style='height:100px; width:100%;'>" +
			 * "       	<div style='background-color: "+getColorCriticidad(hallazgo.
			 * getCriticidad())
			 * +";height: 100px;width: 5%;display: flex;float: left;'> </div>" +
			 * "       	<div style='height: 100px;width: 95%;display: flex;float: left; '>"
			 * + "       			<p style='margin: auto;'>"+getTextCriticidad(hallazgo.
			 * getCriticidad())+"</p>" + "       	</div>" + "       </div>" + "   </td>" +
			 * "   <td style='font-size: 13; text-align: center; width: 15% '>"+hallazgo.
			 * getArea()+"</td>" +
			 * "	<td style='font-size: 13; text-align: center; width: 35% '>"+hallazgo.
			 * getPreg()+"</td>" +
			 * "   <td style='font-size: 13; text-align: center; width: 35% '>"+hallazgo.
			 * getObs()+"</td>" + "</tr>";
			 */

			String tr = "<tr width= '100%' height='110px' style='background-color: white; border-top: 5px solid '>"
					+ "	<td style='font-size: 13; text-align: justify; width: 1%; height=110px;  background-color: #EAF2F8;'>"
					+ "   </td>"
					+ "	<td style='font-size: 13; text-align: justify; width: 90%; height=110px;  background-color: #EAF2F8;'>"
					+ hallazgo.getNombChek() + "" + "<br></br>" + hallazgo.getPreg() + "<br></br>Comentario:"
					+ hallazgo.getObs() + "</td>"
					+ "   <td style='font-size: 13; text-align: center; width: 10%; height=110px;  background-color: #EAF2F8;'>"
					+ hallazgo.getRespuesta() + "</td>" + "</tr>";

			filas.add(tr);

			if ((conteoHallazgos < hallazgos.size() && hallazgosrecorridos == hallazgosPorHoja)
					|| conteoHallazgos == hallazgos.size()) {

				// Genera pagina con los hallazgos que se encuentran en la lista "filas"
				String pagina = generaPaginaReporte(filas, recorrido, sucursal, zona);
				paginas.add(pagina);

				hallazgosrecorridos = 0;
				filas.clear();
			}

		}

		return paginas;

	}

	public String generaPagina(List<String> filas, String recorrido, String sucursal, String zona) {

		String divRecorridoSucursal = "<div style='border-bottom: 2px solid #F0F0F0;'>"
				+ "	<table style='width:100%; height:50px; padding: 5px; ' >" + "		<tr>" + "			<th>"
				+ recorrido + "</th>" + "			<td  rowspan='2' style='text-align: right;'>"
				+ "				<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoElektra.png' width='36' height='36' />"
				+ "					&nbsp;&nbsp;&nbsp;"
				+ "				<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoBAZ.png' width='45' height='30'/>"
				+ "			</td>" + "		</tr>" + "		<tr>" + "			<td>" + sucursal + "</td>"
				+ "		</tr>" + "	</table>" + "</div>"
				+ "<div style='width: 100%; height: 10px; margin-bottom: 15px'>"
				+ "	<div style='width: 10%;  height: 100%; display: flex; float: left; background-color: #f2f2f2;'></div>"
				+ "   <div style='width: 90%;  height: 100%; display: flex; float: left;'>"
				+ "   	<div style='background-color: #f2f2f2 ; width: 100%; height: 2px; margin: auto; vertical-align: middle'></div>"
				+ "	</div>" + "</div>";

		String divZona = "<div style='height: 50px; width: 100%;'>"
				+ "	<table width='100%'  cellspacing='0' cellpadding='0' style='margin-top: 10px'>"
				+ "   	<tr style='width: 100%; vertical-align: middle'>" + "       	<td >"
				+ "				<img width='45' height='30' src='" + getUrlImagenZona(zona) + "'/>" + "			</td>"
				+ "           <td style='font-size: 20px; vertical-align: middle; text-align: left;'>Listado Hallazgos | <strong>"
				+ getTextZona(zona) + "</strong> </td>" + "       </tr>" + "   </table>" + " </div>"
				+ "<div style='background-color: #f2f2f2 ; width: 100%; height: 2px; margin-bottom: 5px'></div>";

		String thead = "<table style='width: 100%; border-spacing: 5px ; margin-top: 5px'  >" + "	<tbody>"
				+ "   	<tr>" + "			<td style='text-align: center; width: 15%'>Severidad</td>"
				+ "       	<td style='text-align: center; width: 15%'>Área</td>"
				+ "       	<td style='text-align: center; width: 35%'>Hallazgo</td>"
				+ "       	<td style='text-align: center; width: 35%'>Comentarios</td>" + "		</tr>"
				+ "   </tbody>" + " </table>"
				+ "<div style='background-color: #FFD733 ; width: 100%; height: 2px; '></div>";

		String html = divRecorridoSucursal + divZona + thead;

		html += "<table style='width: 100%; border-spacing: 5px; margin-bottom: 70px'>" + "	<tbody>"
				+ "  		<tr></tr>";

		for (String fila : filas) {
			html += fila;
		}

		html += "	</tbody>" + "</table>";

		return html;

	}

	public String generaPaginaReporte(List<String> filas, String recorrido, String sucursal, String zona) {

		String divRecorridoSucursal = "<div style='border-bottom: 0px solid white;'>"
				+ "	<table style='width:100%; height:75px; padding: 5px; ' >" + "		<tr>" + "			<th>" + ""
				+ "</th>" + "			<td  rowspan='2' style='text-align: right;'>"
				+ "				<img src='' width='36' height='36' />" + "					&nbsp;&nbsp;&nbsp;"
				+ "				<img src='' width='45' height='30'/>" + "			</td>" + "		</tr>"
				+ "		<tr>" + "			<td>" + "" + "</td>" + "		</tr>" + "	</table>" + "</div>"
				+ "<div style='width: 100%; height: 10px; margin-bottom: 15px'>"
				+ "	<div style='width: 10%;  height: 100%; display: flex; float: left; background-color: white;'></div>"
				+ "   <div style='width: 90%;  height: 100%; display: flex; float: left;'>"
				+ "   	<div style='background-color: white ; width: 100%; height: 2px; margin: auto; vertical-align: middle'></div>"
				+ "	</div>" + "</div>";

		String divZona = "<div style='height: 50px; width: 100%; background-color: #FDEDEC;'>"
				+ "	<table width='100%'  cellspacing='0' cellpadding='0' style='margin-top: 10px'>"
				+ "   	<tr style='width: 100%; vertical-align: middle; background-color: #FDEDEC;'>"
				+ "       	<td >" + "				<img width='30' height='30' src='" + getUrlImagenZona(zona) + "'/>"
				+ "			</td>"
				+ "           <td style='font-size: 18px; background-color: #FDEDEC; vertical-align: middle; text-align: left;'>"
				+ getTextZona(zona) + " | <strong> Listado Hallazgos </strong> </td>" + "       </tr>" + "   </table>"
				+ " </div>"
				+ "<div style='background-color: #FDEDEC ; width: 100%; height: 2px; margin-bottom: 5px'></div>";

		String thead = "<table style='width: 100%; border-spacing: 0px ; margin-top: 5px;'  >" + "	<tbody>"
				+ "   	<tr>" + "			<td style='text-align: center; width: 25%'></td>"
				+ "       	<td style='text-align: center; width: 35%'></td>"
				+ "       	<td style='text-align: center; width: 30%'></td>"
				+ "       	<td style='text-align: center; width: 10%'></td>" + "		</tr>" + "   </tbody>"
				+ " </table>" + "<div style='background-color: white ; width: 100%; height: 2px; '></div>";

		String html = divRecorridoSucursal + divZona + thead;

		html += "<table style='width: 100%; border-spacing: 0px; margin-bottom: 70px;'>" + "	<tbody>"
				+ "  		<tr></tr>";

		for (String fila : filas) {
			html += fila;
		}

		html += "	</tbody>" + "</table>";

		return html;

	}

	public String generaHtmlZona(List<String> paginasZona) {

		String html = "<html>" + "	<head>" + "		<style>" + "  			table {"
				+ "        		border-collapse: separate;" + "           	width: 100%;" + "        	}"
				+ "		</style>" + "	</head>";

		html += "<body>";
		// html += "<div style='position: fixed; right: 30px; left: 30px; bottom: 10px;
		// background: white; padding: 20px; text-align: right; overflow-y: scroll;'>";

		for (String pagina : paginasZona) {

			html += pagina;
		}

		html += "" + "</body>" + "</html>";

		return html;

	}

	public String getColorCriticidad(int criticidad) {

		String color = "";

		/*
		 * 
		 * 0 - BAJO 1 - IMPERDONABLE 2 - CRÍTICO 3 - MEDIO
		 * 
		 */

		switch (criticidad) {
		case 0:
			color = "#0DBB42";
			break;
		case 1:
			color = "#000";
			break;
		case 2:
			color = "#F10424";
			break;
		case 3:
			color = "#FAEC04";
			break;
		default:
			color = "#0DBB42";
			break;
		}

		return color;
	}

	public String getTextCriticidad(int criticidad) {

		String txtCriticidad = "";

		switch (criticidad) {
		case 0:
			txtCriticidad = "Bajo";
			break;
		case 1:
			txtCriticidad = "Imperdonable";
			break;
		case 2:
			txtCriticidad = "Crítico";
			break;
		case 3:
			txtCriticidad = "Medio";
			break;
		default:
			txtCriticidad = "Bajo";
			break;
		}

		return txtCriticidad;
	}

	public String getTextZona(String zona) {

		String strZona = "";

		switch (zona) {
		case "AREAS":
			strZona = "Áreas Comunes";
			break;
		case "GENERALES":
			strZona = "Generales";
			break;
		case "EKT":
			strZona = "EKT";
			break;
		case "PP":
			strZona = "Presta Prenda";
			break;
		case "BAZ":
			strZona = "BAZ";
			break;
		case "CYC":
			strZona = "Crédito y cobranza";
			break;
		case "ITK":
			strZona = "Italika";
			break;
		case "GEOGRAFIA":
			strZona = "Geografía";
			break;
		default:
			break;
		}

		return strZona;
	}

	public String getUrlImagenZona(String zona) {

		String strUrl = "";

		switch (zona) {
		case "AREAS":
			strUrl = "http://10.53.33.82/franquicia/firmaChecklist/zonaAreasComunes.png";
			break;
		case "GENERALES":
			strUrl = "http://10.53.33.82/franquicia/firmaChecklist/zonaGenerales.png";
			break;
		case "EKT":
			strUrl = "http://10.53.33.82/franquicia/firmaChecklist/LogoElektra.png";
			break;
		case "PP":
			strUrl = "http://10.53.33.82/franquicia/firmaChecklist/zonaPrestaPrenda.png";
			break;
		case "BAZ":
			strUrl = "http://10.53.33.82/franquicia/firmaChecklist/LogoBAZ.png";
			break;
		case "CYC":
			strUrl = "http://10.53.33.82/franquicia/firmaChecklist/LogoBAZ.png";
			break;
		case "ITK":
			strUrl = "http://10.53.33.82/franquicia/firmaChecklist/Italika_logo.png";
			break;
		case "GEOGRAFIA":
			strUrl = "http://10.53.33.82/franquicia/firmaChecklist/geografia_logo.png";
			break;
		default:
			break;
		}

		return strUrl;

	}

	public List<String> generaHtmlImperdonablesReporte(String ceco, String fase, String proyecto, String recorrido,
			String sucursal) {

		List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();
		List<HallazgosTransfDTO> listaHallazgosImperdonables = new ArrayList<HallazgosTransfDTO>();
		try {
			
			HallazgosTransfBI hallazgosTransfBI = (HallazgosTransfBI) FRQAppContextProvider
					.getApplicationContext().getBean("hallazgosTransfBI");
			
			listaHallazgos = new ArrayList<HallazgosTransfDTO>();
			listaHallazgos = hallazgosTransfBI.obtieneDatos(ceco,proyecto,fase);

			listaHallazgosImperdonables = (List<HallazgosTransfDTO>) listaHallazgos.stream()
					.filter(arbol -> arbol.getArbol().equalsIgnoreCase("1")).collect(Collectors.toList());

		} catch (Exception e) {
			
			return new ArrayList<String>();

		}

		// Genera html
		List<String> zonasHtml = generaHtmlPorZonasReporte(listaHallazgosImperdonables);

		return zonasHtml;
	}
	
	public List<String> generaHtmlPorZonasReporte(List<HallazgosTransfDTO> listaHallazgosImperdonables) {

		String htmlZona = "";

		int hallazgosPorHoja = 8;
		int hallazgosrecorridos = 0;
		int conteoHallazgos = 0;

		List<String> filas = new ArrayList<>();

		List<String> paginas = new ArrayList<>();

		for (HallazgosTransfDTO hallazgo : listaHallazgosImperdonables) {

			hallazgosrecorridos++;
			conteoHallazgos++;

			String colorRow = "#FAFAFA";

			if (hallazgosrecorridos % 2 == 0) {
				colorRow = "#EDEDED";

			}

			/*
			 * String tr = "<tr width= '100%' height='100px' style='background-color: " +
			 * colorRow + "; border-top: 5px solid '>" +
			 * "	<td style='font-size: 13; text-align: center; width: 15%'>" +
			 * "		<div style='height:100px; width:100%;'>" +
			 * "       	<div style='background-color: "+getColorCriticidad(hallazgo.
			 * getCriticidad())
			 * +";height: 100px;width: 5%;display: flex;float: left;'> </div>" +
			 * "       	<div style='height: 100px;width: 95%;display: flex;float: left; '>"
			 * + "       			<p style='margin: auto;'>"+getTextCriticidad(hallazgo.
			 * getCriticidad())+"</p>" + "       	</div>" + "       </div>" + "   </td>" +
			 * "   <td style='font-size: 13; text-align: center; width: 15% '>"+hallazgo.
			 * getArea()+"</td>" +
			 * "	<td style='font-size: 13; text-align: center; width: 35% '>"+hallazgo.
			 * getPreg()+"</td>" +
			 * "   <td style='font-size: 13; text-align: center; width: 35% '>"+hallazgo.
			 * getObs()+"</td>" + "</tr>";
			 */

			String tr = "<tr width= '100%' height='110px' style='background-color: white; border-top: 5px solid '>"
					+ "	<td style='font-size: 13; text-align: justify; width: 1%; height=110px;  background-color: #EAF2F8;'>"
					+ "   </td>"
					+ "	<td style='font-size: 13; text-align: justify; width: 90%; height=110px;  background-color: #EAF2F8;'>"
					+ hallazgo.getNombChek() + "" + "<br></br>" + hallazgo.getPreg() + "<br></br>Comentario:"
					+ hallazgo.getObs() + "</td>"
					+ "   <td style='font-size: 13; text-align: center; width: 10%; height=110px;  background-color: #EAF2F8;'>"
					+ hallazgo.getRespuesta() + "</td>" + "</tr>";

			filas.add(tr);

			if ((conteoHallazgos < listaHallazgosImperdonables.size() && hallazgosrecorridos == hallazgosPorHoja)
					|| conteoHallazgos == listaHallazgosImperdonables.size()) {

				// Genera pagina con los hallazgos que se encuentran en la lista "filas"
				String pagina = generaPaginaReporteImperdonable(filas);
				paginas.add(pagina);

				hallazgosrecorridos = 0;
				filas.clear();
			}

		}

		return paginas;

	}
	

	public String generaPaginaReporteImperdonable(List<String> filas) {

		String divRecorridoSucursal = "<div style='border-bottom: 0px solid white;'>"
				+ "	<table style='width:100%; height:75px; padding: 5px; ' >" + "		<tr>" + "			<th>" + ""
				+ "</th>" + "			<td  rowspan='2' style='text-align: right;'>"
				+ "				<img src='' width='36' height='36' />" + "					&nbsp;&nbsp;&nbsp;"
				+ "				<img src='' width='45' height='30'/>" + "			</td>" + "		</tr>"
				+ "		<tr>" + "			<td>" + "" + "</td>" + "		</tr>" + "	</table>" + "</div>"
				+ "<div style='width: 100%; height: 10px; margin-bottom: 15px'>"
				+ "	<div style='width: 10%;  height: 100%; display: flex; float: left; background-color: white;'></div>"
				+ "   <div style='width: 90%;  height: 100%; display: flex; float: left;'>"
				+ "   	<div style='background-color: white ; width: 100%; height: 2px; margin: auto; vertical-align: middle'></div>"
				+ "	</div>" + "</div>";

		String divZona = "<div style='height: 50px; width: 100%; background-color: #FDEDEC;'>"
				+ "	<table width='100%'  cellspacing='0' cellpadding='0' style='margin-top: 10px'>"
				+ "   	<tr style='width: 100%; vertical-align: middle; background-color: #FDEDEC;'>"
				+ "       	<td >" + "				<img width='30' height='30' src=''/>"
				+ "			</td>"
				+ "           <td style='font-size: 18px; background-color: #FDEDEC; vertical-align: middle; text-align: center;'>"
				+ " <strong> Imperdonables </strong> </td>" + "       </tr>" + "   </table>"
				+ " </div>"
				+ "<div style='background-color: #FDEDEC ; width: 100%; height: 2px; margin-bottom: 5px'></div>";

		String thead = "<table style='width: 100%; border-spacing: 0px ; margin-top: 5px;'  >" + "	<tbody>"
				+ "   	<tr>" + "			<td style='text-align: center; width: 25%'></td>"
				+ "       	<td style='text-align: center; width: 35%'></td>"
				+ "       	<td style='text-align: center; width: 30%'></td>"
				+ "       	<td style='text-align: center; width: 10%'></td>" + "		</tr>" + "   </tbody>"
				+ " </table>" + "<div style='background-color: white ; width: 100%; height: 2px; '></div>";

		String html = divRecorridoSucursal + divZona + thead;

		html += "<table style='width: 100%; border-spacing: 0px; margin-bottom: 70px;'>" + "	<tbody>"
				+ "  		<tr></tr>";

		for (String fila : filas) {
			html += fila;
		}

		html += "	</tbody>" + "</table>";

		return html;

	}

}
