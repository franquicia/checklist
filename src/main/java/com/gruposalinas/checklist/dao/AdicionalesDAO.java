package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.AdicionalesDTO;


public interface AdicionalesDAO {

	   public int inserta(AdicionalesDTO bean)throws Exception;
		
		public boolean elimina(String bita)throws Exception;
		
		public List<AdicionalesDTO> obtieneDatos(String ceco) throws Exception; 
		
		public List<AdicionalesDTO> obtieneDatosBita(String bita,String idPreg) throws Exception; 
		
		public List<AdicionalesDTO> obtieneInfo() throws Exception; 
		
		public boolean actualiza(AdicionalesDTO bean)throws Exception;
		
		//HISTORIAL		
		public int insertaHist(AdicionalesDTO bean)throws Exception;
		
		public boolean eliminaHist(String bita)throws Exception;
		
		public List<AdicionalesDTO> obtieneDatosHist(String bita) throws Exception; 
		
		public List<AdicionalesDTO> obtieneInfoHist() throws Exception; 
		
		
		
	}