package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.CheckinDTO;

public class CheckinRowMapper implements RowMapper<CheckinDTO> {

	public CheckinDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		CheckinDTO checkinDTO = new CheckinDTO();
		
	
		checkinDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		checkinDTO.setIdUsuario(rs.getInt("FCID_USUARIO"));
		checkinDTO.setIdCeco(rs.getString("FCID_CECO"));
		checkinDTO.setFecha(rs.getString("FDFECHA"));
		checkinDTO.setLatitud(rs.getString("FCLATITUD"));
		checkinDTO.setLongitud(rs.getString("FCLONGITUD"));
		checkinDTO.setBandera(rs.getInt("FCBANDERA"));
		checkinDTO.setPlataforma(rs.getString("PLATAFORMA"));
		checkinDTO.setTiempoConexion(rs.getInt("TIEMPO_CONEXION"));
	
		return checkinDTO;
	}

}
