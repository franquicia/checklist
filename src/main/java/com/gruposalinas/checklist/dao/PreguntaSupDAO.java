package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.PreguntaSupDTO;

public interface PreguntaSupDAO {
	
	public int insertaPregunta(PreguntaSupDTO bean) throws Exception;
	
	public boolean actualizaPregunta(PreguntaSupDTO bean) throws Exception;
	
	public List<PreguntaSupDTO> obtienePregunta(String idPregunta,String idModulo,String tipoPregunta) throws Exception;
	
	
	public boolean eliminaPregunta(int idPregunta) throws Exception;
	
}
