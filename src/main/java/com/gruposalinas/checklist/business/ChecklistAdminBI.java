package com.gruposalinas.checklist.business;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.checklist.domain.AdmPregZonaDTO;
import com.gruposalinas.checklist.domain.AdmTipoZonaDTO;
import com.gruposalinas.checklist.domain.AdminZonaDTO;
import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.domain.CheckAutoProtoDTO;
import com.gruposalinas.checklist.domain.CheckSoporteAdmDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistLayoutDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.OrdenGrupoDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.ProtocoloDTO;
import com.gruposalinas.checklist.domain.VersionProtoCheckDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

public class ChecklistAdminBI {

    @Autowired
    PosiblesBI posiblesbi;

    @Autowired
    ArbolDecisionBI arbolDecisionbi;

    @Autowired
    ArbolDecisionTBI arbolDecisionAdmbi;

    @Autowired
    PreguntaAdmBI preguntaAdmBI;
    @Autowired
    ChecklistProtocoloBI checklistProtocoloBI;

    @Autowired
    ChecklistProtocoloAdmBI checklistProtocoloAdmBI;

    @Autowired
    ReporteImagenesBI reporteImagenesBI;

    @Autowired
    ChecklistPreguntaBI checklistPreguntabi;

    @Autowired
    ProtocoloBI protocoloBI;

    @Autowired
    BitacoraCheckUsuAdmBI bitacoraCheckUsuAdmBI;
    @Autowired
    ArbolRespAdiEviAdmBI arbolRespAdiEviAdmBI;

    @Autowired
    CheckAutProtoBI checkAutProtoBI;

    @Autowired
    CheckSoporteAdmBI checkSoporteAdmBI;
    @Autowired
    VersionCheckAdmBI versionCheckAdmBI;

    @Autowired
    OrdenGrupoBI ordenGrupoBI;
    @Autowired
    AdmTipoZonaBI admTipoZonaBI;
    @Autowired
    AdmZonasBI admZonasBI;
    @Autowired
    AdmPregZonasBI admPregZonasBI;

    @Autowired
    AdmPregZonasTempBI admPregZonasTempBI;

    @Autowired
    ReporteImagenesAdmBI reporteImagenesAdmBI;

    @Autowired
    VersionProtoCheckBI versionProtoCheckBI;

    private static final Logger logger = LogManager.getLogger(ChecklistAdminBI.class);

    /* MÓDULOS */
    public ArrayList<ModuloDTO> getListaModulos(ArrayList<ChecklistLayoutDTO> listaData) {

        ArrayList<ModuloDTO> listaModulos = new ArrayList<ModuloDTO>();

        try {

            String seccionActual = "";
            boolean existe = false;
            for (ChecklistLayoutDTO objLista : listaData) {

                if (!objLista.getSeccionProtocolo().equalsIgnoreCase(seccionActual)) {

                    // VALIDAR SECCIONES SALTADAS
                    existe = false;
                    seccionActual = objLista.getSeccionProtocolo();

                    ModuloDTO modulo = new ModuloDTO();
                    modulo.setIdModulo(0);
                    modulo.setNombre(objLista.getSeccionProtocolo());
                    modulo.setIdModuloPadre(0);
                    modulo.setNombrePadre("");
                    modulo.setNumVersion("0");
                    modulo.setTipoCambio("");
                    modulo.setIdChecklist(0);

                    for (ModuloDTO obj : listaModulos) {

                        if (objLista.getSeccionProtocolo().compareTo(obj.getNombre()) == 0) {

                            existe = true;
                            break;
                        }

                    }

                    if (!existe) {
                        listaModulos.add(modulo);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listaModulos;

    }

    public ArrayList<ArrayList<ModuloDTO>> insertaModulos(ArrayList<ModuloDTO> listaModulos) {

        ArrayList<ArrayList<ModuloDTO>> response = new ArrayList<ArrayList<ModuloDTO>>();
        ArrayList<ModuloDTO> listaModulosCorrectos = new ArrayList<ModuloDTO>();

        try {
            ArrayList<ArrayList<ModuloDTO>> modulosResponse = setIdModulos(listaModulos);

            listaModulosCorrectos = modulosResponse.get(1);

            if (listaModulosCorrectos.size() == listaModulos.size()) {

                response.add(0, new ArrayList<ModuloDTO>());
                response.add(1, listaModulosCorrectos);

                return response;

            } else {

                modulosResponse = setIdModulos(modulosResponse.get(0));

                for (ModuloDTO obj : modulosResponse.get(1)) {
                    listaModulosCorrectos.add(obj);
                }

                if (listaModulosCorrectos.size() == listaModulos.size()) {

                    response.add(0, new ArrayList<ModuloDTO>());
                    response.add(1, listaModulosCorrectos);

                    return response;

                } else {

                    response.add(0, modulosResponse.get(0));
                    response.add(1, listaModulosCorrectos);

                    return response;
                }

            }
        } catch (Exception e) {
            System.out.println("ChecklistAdminBI " + e);
            return null;
        }

    }

    public ArrayList<ArrayList<ModuloDTO>> setIdModulos(ArrayList<ModuloDTO> listaModulos) {

        ArrayList<ArrayList<ModuloDTO>> response = new ArrayList<ArrayList<ModuloDTO>>();

        try {
            ModuloBI modulobi = (ModuloBI) FRQAppContextProvider.getApplicationContext().getBean("moduloBI");

            int idMod = 0;

            ArrayList<ModuloDTO> listaModulosCorrectos = new ArrayList<ModuloDTO>();
            ArrayList<ModuloDTO> listaModulosIncorrectos = new ArrayList<ModuloDTO>();

            for (int i = 0; i < listaModulos.size(); i++) {
                //System.out.println("i == " + i);
                //System.out.println("size listaModulos == " + listaModulos.size());

                idMod = 0;

                idMod = modulobi.insertaModulo(listaModulos.get(i));

                if (idMod != 0) {

                    listaModulos.get(i).setIdModulo(idMod);
                    listaModulosCorrectos.add(listaModulos.get(i));
                } else {

                    listaModulosIncorrectos.add(listaModulos.get(i));
                }
            }

            response.add(0, listaModulosIncorrectos);
            response.add(1, listaModulosCorrectos);

        } catch (Exception e) {
            System.out.println("AP al insertar modulo");
            System.out.println(e.getMessage());
            System.out.println("AP :" + e);
        }

        return response;

    }

    /* FIN MÓDULOS */

 /* PREGUNTAS */
    public ArrayList<PreguntaDTO> getListaPreguntas(ArrayList<ChecklistLayoutDTO> listaData, ArrayList<ModuloDTO> listaModulos, int tipoCarga) {

        ArrayList<PreguntaDTO> listaPreguntas = new ArrayList<PreguntaDTO>();

        try {

            for (ChecklistLayoutDTO objLista : listaData) {

                PreguntaDTO pregunta = new PreguntaDTO();

                // Se agrega a PreguntaDTO
                pregunta.setIdConsecutivo(objLista.getIdConsecutivo());
                pregunta.setIdPreguntaPadre(objLista.getIdPreguntaPadre());

                // COMÚNES
                pregunta.setPregunta(objLista.getPregunta());
                pregunta.setCritica(objLista.getFactorCritico());
                pregunta.setIdPregunta(objLista.getIdConsecutivoProtocolo());
                // Tipo de Respuesta
                pregunta.setIdTipo(objLista.getTipoRespuesta());

                // Para Aseguramiento
                pregunta.setCodigo(objLista.getCodigoChecklist());
                pregunta.setDetalle(objLista.getDefinicion());

                // Para Infraestructura
                pregunta.setSla(objLista.getSla());
                // Área Responsables

                if (tipoCarga == 1) {
                    pregunta.setArea(objLista.getResponsable1());
                } else {
                    pregunta.setArea(objLista.getZona());
                }

                listaPreguntas.add(pregunta);

                // Se Agrega el Módulo a la Pregunta
                for (ModuloDTO moduloObj : listaModulos) {

                    if (moduloObj.getNombre().compareTo(objLista.getSeccionProtocolo()) == 0) {

                        pregunta.setIdModulo(moduloObj.getIdModulo());

                    }

                }

            }

        } catch (Exception e) {

            System.out.println("ChecklistAdminBI " + e);
            return listaPreguntas;

        }

        return listaPreguntas;

    }

    public ArrayList<ArrayList<PreguntaDTO>> insertaPreguntas(ArrayList<PreguntaDTO> listaPreguntas, int tipoCarga) {

        ArrayList<ArrayList<PreguntaDTO>> response = new ArrayList<ArrayList<PreguntaDTO>>();
        ArrayList<PreguntaDTO> listaPreguntasCorrectas = new ArrayList<PreguntaDTO>();

        try {
            ArrayList<ArrayList<PreguntaDTO>> preguntasResponse = setIdPreguntas(listaPreguntas, tipoCarga);

            listaPreguntasCorrectas = preguntasResponse.get(1);

            if (listaPreguntasCorrectas.size() == listaPreguntas.size()) {

                response.add(0, new ArrayList<PreguntaDTO>());
                response.add(1, listaPreguntasCorrectas);

                return response;

            } else {

                preguntasResponse = setIdPreguntas(preguntasResponse.get(0), tipoCarga);

                for (PreguntaDTO obj : preguntasResponse.get(1)) {
                    listaPreguntasCorrectas.add(obj);
                }

                if (listaPreguntasCorrectas.size() == listaPreguntas.size()) {

                    response.add(0, new ArrayList<PreguntaDTO>());
                    response.add(1, listaPreguntasCorrectas);

                    return response;

                } else {

                    response.set(0, preguntasResponse.get(0));
                    response.set(1, listaPreguntasCorrectas);

                    return response;
                }

            }
        } catch (Exception e) {
            logger.info("Ocurrio algo en AP ChecklistAdminBI ALTA PREGUNTA ");
            return null;
        }

    }

    public ArrayList<ArrayList<PreguntaDTO>> setIdPreguntas(ArrayList<PreguntaDTO> listaPreguntas, int tipoCarga) {

        ArrayList<ArrayList<PreguntaDTO>> response = new ArrayList<ArrayList<PreguntaDTO>>();

        try {
            PreguntaBI preguntabi = (PreguntaBI) FRQAppContextProvider.getApplicationContext().getBean("preguntaBI");

            int idPregunta = 0;

            ArrayList<PreguntaDTO> listaPreguntasCorrectos = new ArrayList<PreguntaDTO>();
            ArrayList<PreguntaDTO> listaPreguntasIncorrectos = new ArrayList<PreguntaDTO>();

            for (int i = 0; i < listaPreguntas.size(); i++) {
                //System.out.println("i == " + i);
                //System.out.println("size listaPreguntas == " + listaPreguntas.size());

                idPregunta = 0;

                idPregunta = preguntabi.insertaPreguntaCom(listaPreguntas.get(i));

                if (idPregunta != 0) {

                    int idPregAnterior = listaPreguntas.get(i).getIdPregunta();

                    //System.out.println("ID ANTERIOR " + idPregAnterior);
                    listaPreguntas.get(i).setIdPregunta(idPregunta);

                    listaPreguntasCorrectos.add(listaPreguntas.get(i));

                    /*AdmPregZonaDTO zona = new AdmPregZonaDTO();
                    zona.setIdPreg(idPregAnterior + "");

                    ArrayList<AdmPregZonaDTO> resZonas = admPregZonasTempBI.obtienePregZonaById(zona);
                    if (resZonas != null && resZonas.size() > 0) {

                        System.out.println("ANTES DEL CAMBIO DEL ID " + zona);

                        AdmPregZonaDTO zonaPregProd = resZonas.get(0);
                        zonaPregProd.setIdPreg(idPregunta + "");

                        System.out.println("SE CAMBIO EL ID DE LA PREGUNTA A LA PRODUCTIVA " + zonaPregProd);
                        admPregZonasBI.insertaPregZona(zonaPregProd);

                    }*/
                    //listaPreguntas.get(i).setIdPregunta(idPregunta);
                    //listaPreguntasCorrectos.add(listaPreguntas.get(i));
                } else {

                    listaPreguntasIncorrectos.add(listaPreguntas.get(i));
                }
            }

            response.add(0, listaPreguntasIncorrectos);
            response.add(1, listaPreguntasCorrectos);

        } catch (Exception e) {
            logger.info("Ocurrio algo en AP al insertar PREGUNTA");
            //System.out.println(e.getMessage());
            //System.out.println("AP :" + e);
        }

        /*try {
            PreguntaBI preguntabi = (PreguntaBI) FRQAppContextProvider.getApplicationContext().getBean("preguntaBI");

            int idPregunta = 0;

            ArrayList<PreguntaDTO> listaPreguntasCorrectos = new ArrayList<PreguntaDTO>();
            ArrayList<PreguntaDTO> listaPreguntasIncorrectos = new ArrayList<PreguntaDTO>();

            for (int i = 0; i < listaPreguntas.size(); i++) {
                System.out.println("i == " + i);
                System.out.println("size listaPreguntas == " + listaPreguntas.size());

                idPregunta = 0;

                idPregunta = preguntabi.insertaPreguntaCom(listaPreguntas.get(i));

                if (idPregunta != 0) {
                    int idPregAnterior=listaPreguntas.get(i).getIdPregunta();

                    System.out.println("ID ANTERIOR "+idPregAnterior);

                    listaPreguntas.get(i).setIdPregunta(idPregunta);

                    listaPreguntasCorrectos.add(listaPreguntas.get(i));


                    AdmPregZonaDTO zona=new AdmPregZonaDTO();
                    zona.setIdPreg(idPregAnterior+"");


                    ArrayList<AdmPregZonaDTO>resZonas=admPregZonasTempBI.obtienePregZonaById(zona);
                    if(resZonas!=null && resZonas.size()>0){


                        System.out.println("ANTES DEL CAMBIO DEL ID "+zona);

                        AdmPregZonaDTO zonaPregProd= resZonas.get(0);
                        zonaPregProd.setIdPreg(idPregunta+"");

                        System.out.println("SE CAMBIO EL ID DE LA PREGUNTA A LA PRODUCTIVA "+zonaPregProd);
                        admPregZonasBI.insertaPregZona(zonaPregProd);



                    }


                } else {

                    listaPreguntasIncorrectos.add(listaPreguntas.get(i));
                }
            }

            response.add(0, listaPreguntasIncorrectos);
            response.add(1, listaPreguntasCorrectos);

        } catch (Exception e) {
            logger.info("Ocurrio algo en AP al insertar PREGUNTA: " + e.getMessage());
            //System.out.println(e.getMessage());
            //System.out.println("AP :" + e);
        }*/
        return response;
    }

    /* FIN PREGUNTAS */
 /* METODOS PARA ALTA PROTOCOLO */
    public ArrayList<ChecklistProtocoloDTO> getListaProtocolos(ArrayList<ChecklistLayoutDTO> listaData, int tipoNegocio) {

        // Infraestructura == 1 - Supervisión 2
        ArrayList<ChecklistProtocoloDTO> listaProtocolos = new ArrayList<ChecklistProtocoloDTO>();

        ArrayList<ChecklistLayoutDTO> listaProtocolosData = new ArrayList<ChecklistLayoutDTO>();

        if (tipoNegocio == 1) {
            // Los módulos para EXPANSIÓN(1) son protocolos
            listaProtocolosData = getListaProtocolosExpansion(listaData);
            listaProtocolos = setObjetoProtocolo(listaData, listaProtocolosData, 1);

        } else {
            // Para SUPERVISIÓN (2) los protocolos son la columna protocolo
            listaProtocolosData = getListaProtocolosSupervision(listaData);
            listaProtocolos = setObjetoProtocolo(listaData, listaProtocolosData, 2);
        }

        return listaProtocolos;
    }

    public ArrayList<ChecklistProtocoloDTO> setObjetoProtocolo(ArrayList<ChecklistLayoutDTO> listaData, ArrayList<ChecklistLayoutDTO> listaProtocolosData, int tipoNegocio) {

        ArrayList<ChecklistProtocoloDTO> listaProtocolos = new ArrayList<ChecklistProtocoloDTO>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String fechaHoy = sdf.format(new Date());

        for (ChecklistLayoutDTO obj : listaProtocolosData) {

            //CONDICIÓN
            String nombreCheck = "";
            String ordenGrupo = "";
            String idUsuario = "";
            double ponderacionTot = 0;
            String clasifica = "";
            String idProtocolo = "0";

            //GENÉRICOS
            String idTipoChecklist = "61";
            String idHorario = "23";
            String vigente = "1";
            String fecha_inicio = fechaHoy;
            String fecha_fin = null;
            String idEstado = "21";
            String dia = "";
            String periodo = "1";
            String version = "1";
            int commit = 1;
            String negocio = "";

            if (tipoNegocio == 1) {

                // EXPANSIÓN
                //Obtenemos de listaData
                nombreCheck = obj.getSeccionProtocolo();
                //**(Infraestrcutura 321 - Aseguramiento 323)**
                ordenGrupo = "321";
                //Se debe obtener de la sesión
                idUsuario = "189871";
                //**LEER EXCEL PARA EXPANSIÓN CAMPO ZONA**
                clasifica = obj.getZona();
                //**LEER EXCEL PARA ASEGURAMIENTO CAMPO PROTOCOLO 0**
                idProtocolo = "0";
                //**CALCULAR CON EXCEL**
                ponderacionTot = getPonderacionProtocolo(listaData, obj, tipoNegocio);

                //GENÉRICOS
                idTipoChecklist = "61";
                idHorario = "21";
                vigente = "1";
                fecha_inicio = fechaHoy;
                fecha_fin = "";
                idEstado = "21";
                dia = "";
                periodo = "1";
                version = "1";
                commit = 1;
                negocio = obj.getNegocio();
                //Genéricos

            } else {
                // SUPERVISIÓN
                //Obtenemos de listaData
                nombreCheck = obj.getNombreProtocolo();
                //**(Infraestrcutura 321 - Aseguramiento 323)**
                ordenGrupo = "323";
                //Se debe obtener de la sesión
                idUsuario = "189871";
                //**LEER EXCEL PARA EXPANSIÓN CAMPO ZONA**
                clasifica = "";
                //**LEER EXCEL PARA ASEGURAMIENTO CAMPO PROTOCOLO 0**

                //**CALCULAR CON EXCEL**
                ponderacionTot = getPonderacionProtocolo(listaData, obj, tipoNegocio);

                //GENÉRICOS
                idTipoChecklist = "61";
                idHorario = "21";
                vigente = "1";
                fecha_inicio = fechaHoy;
                fecha_fin = "";
                idEstado = "21";
                dia = "";
                periodo = "1";
                version = "1";
                commit = 1;
                negocio = obj.getNegocio();

                List<ProtocoloDTO> respuesta = protocoloBI.getProtocolos();

                String numProtocolo = obj.getNombreProtocolo().trim().split(" ")[0];

                int idProt = 0;
                for (ProtocoloDTO aux : respuesta) {
                    if (aux.getObservaciones().trim().equals(numProtocolo)) {
                        idProt = aux.getIdProtocolo();
                    }
                }

                idProtocolo = "" + idProt;

            }

            ChecklistProtocoloDTO checklistProtocolo = new ChecklistProtocoloDTO();
            checklistProtocolo.setIdTipoChecklist(Integer.parseInt(idTipoChecklist));
            checklistProtocolo.setNombreCheck(nombreCheck);
            checklistProtocolo.setIdHorario(Integer.parseInt(idHorario));
            checklistProtocolo.setVigente(Integer.parseInt(vigente));
            checklistProtocolo.setFecha_inicio(fecha_inicio);
            checklistProtocolo.setFecha_fin(fecha_fin);
            checklistProtocolo.setIdEstado(Integer.parseInt(idEstado));
            checklistProtocolo.setIdUsuario(idUsuario);
            checklistProtocolo.setDia(dia);
            checklistProtocolo.setPeriodo(periodo);
            checklistProtocolo.setVersion(version);
            checklistProtocolo.setOrdenGrupo(ordenGrupo);
            checklistProtocolo.setCommit(commit);
            checklistProtocolo.setPonderacionTot(ponderacionTot);
            checklistProtocolo.setClasifica(clasifica);
            checklistProtocolo.setIdProtocolo(idProtocolo);
            checklistProtocolo.setNegocio(negocio);
            //set Negocio

            listaProtocolos.add(checklistProtocolo);

        }

        return listaProtocolos;

    }

    public double getPonderacionProtocolo(ArrayList<ChecklistLayoutDTO> listaData, ChecklistLayoutDTO protocolo, int tipoNegocio) {

        double ponderacion = 0.0;

        try {

            for (ChecklistLayoutDTO obj : listaData) {

                if (tipoNegocio == 1 && (obj.getSeccionProtocolo().compareTo(protocolo.getSeccionProtocolo()) == 0)) {

                    ponderacion += obj.getPonderacion();

                    System.out.println(obj.getNombreProtocolo());
                    System.out.println(protocolo.getNombreProtocolo());
                } else if (tipoNegocio == 2 && (obj.getNombreProtocolo().compareTo(protocolo.getNombreProtocolo()) == 0)) {

                    ponderacion += obj.getPonderacion();

                }
            }
        } catch (Exception e) {
            logger.info("Ocurrio algo en AP getPonderacionProtocolo ");
            ponderacion = -1;
        }

        return ponderacion;

    }

    public ArrayList<ChecklistLayoutDTO> getListaProtocolosExpansion(ArrayList<ChecklistLayoutDTO> lista) {

        ArrayList<ChecklistLayoutDTO> listaProtocolos = new ArrayList<ChecklistLayoutDTO>();

        String seccionActual = "";
        boolean existe = false;
        System.out.println(lista.size());

        for (ChecklistLayoutDTO objLista : lista) {

            if (!objLista.getSeccionProtocolo().equalsIgnoreCase(seccionActual)) {

                existe = false;
                seccionActual = objLista.getSeccionProtocolo();

                ChecklistLayoutDTO objCheck = objLista;

                for (ChecklistLayoutDTO obj : listaProtocolos) {

                    if (objLista.getSeccionProtocolo().compareTo(obj.getSeccionProtocolo()) == 0) {

                        existe = true;
                        break;
                    }

                }

                if (!existe) {
                    listaProtocolos.add(objCheck);
                }
            }
        }

        return listaProtocolos;

    }

    public ArrayList<ChecklistLayoutDTO> getListaProtocolosSupervision(ArrayList<ChecklistLayoutDTO> lista) {

        ArrayList<ChecklistLayoutDTO> listaProtocolos = new ArrayList<ChecklistLayoutDTO>();

        String seccionActual = "";
        boolean existe = false;
        System.out.println(lista.size());

        for (ChecklistLayoutDTO objLista : lista) {

            if (!objLista.getNombreProtocolo().equalsIgnoreCase(seccionActual)) {

                existe = false;
                seccionActual = objLista.getNombreProtocolo();

                ChecklistLayoutDTO objCheck = objLista;

                for (ChecklistLayoutDTO obj : listaProtocolos) {

                    if (objLista.getNombreProtocolo().compareTo(obj.getNombreProtocolo()) == 0) {

                        existe = true;
                        break;
                    }

                }

                if (!existe) {
                    listaProtocolos.add(objCheck);
                }
            }
        }

        return listaProtocolos;

    }

    public ArrayList<ArrayList<ChecklistProtocoloDTO>> insertaInfoProtocolos(ArrayList<ChecklistProtocoloDTO> listaInfoProtocolos, String Usuario) {

        ArrayList<ArrayList<ChecklistProtocoloDTO>> response = new ArrayList<ArrayList<ChecklistProtocoloDTO>>();
        ArrayList<ChecklistProtocoloDTO> listaProtocolosCorrectas = new ArrayList<ChecklistProtocoloDTO>();

        try {
            ArrayList<ArrayList<ChecklistProtocoloDTO>> protocolosResponse = setIdProtocolos(listaInfoProtocolos);

            listaProtocolosCorrectas = protocolosResponse.get(1);

            if (listaProtocolosCorrectas.size() == listaInfoProtocolos.size()) {

                response.add(0, new ArrayList<ChecklistProtocoloDTO>());
                response.add(1, listaProtocolosCorrectas);

                return response;

            } else {

                protocolosResponse = setIdProtocolos(protocolosResponse.get(0));

                for (ChecklistProtocoloDTO obj : protocolosResponse.get(1)) {
                    listaProtocolosCorrectas.add(obj);
                }

                if (listaProtocolosCorrectas.size() == listaInfoProtocolos.size()) {

                    response.add(0, new ArrayList<ChecklistProtocoloDTO>());
                    response.add(1, listaProtocolosCorrectas);

                    return response;

                } else {

                    response.set(0, protocolosResponse.get(0));
                    response.set(1, listaProtocolosCorrectas);

                    return response;
                }

            }
        } catch (Exception e) {
            logger.info("Ocurrio algo en AP ChecklistAdminBI ALTA PROTOCOLO ");
            return null;
        }

    }

    public ArrayList<ArrayList<ChecklistProtocoloDTO>> setIdProtocolos(ArrayList<ChecklistProtocoloDTO> listaChecklist) {

        ArrayList<ArrayList<ChecklistProtocoloDTO>> response = new ArrayList<ArrayList<ChecklistProtocoloDTO>>();

        try {
            System.out.println("INICIO PARA CREAR EL OBJETO DE CHK: " + listaChecklist);
            ChecklistProtocoloBI checklistProtocoloBI = (ChecklistProtocoloBI) FRQAppContextProvider.getApplicationContext().getBean("checklistProtocoloBI");

            int idProtocolo = 0;

            ArrayList<ChecklistProtocoloDTO> listaPreguntasCorrectos = new ArrayList<ChecklistProtocoloDTO>();
            ArrayList<ChecklistProtocoloDTO> listaPreguntasIncorrectos = new ArrayList<ChecklistProtocoloDTO>();

            for (int i = 0; i < listaChecklist.size(); i++) {
                //System.out.println("i == " + i);
                //System.out.println("size listaChecklist == " + listaChecklist.size());

                idProtocolo = 0;
                //se obtiene el nombre del protocolo para buscarlo en el catalogo de protocolos
                String nomProtocoloFile = (listaChecklist != null && listaChecklist.size() > 0 ? listaChecklist.get(i).getNombreCheck() : null);
                int idProtocoloChk = 0;
                List<ProtocoloDTO> protocolosCat = protocoloBI.getProtocolos();

                //System.out.println("PROTOCOLO QUE SE ASIGNARA AL CHK: " + protocolosCat);
                if (protocolosCat != null) {
                    for (ProtocoloDTO p : protocolosCat) {
                        if (p.getDescripcion().toUpperCase().contains(nomProtocoloFile.toUpperCase())) {
                            idProtocoloChk = p.getIdProtocolo();
                            //System.out.println("PROTOCOLO QUE SE ASIGNARA AL CHK: " + idProtocoloChk);
                            break;

                        }
                    }
                }

                //System.out.println("ITEM:  " + listaChecklist.get(i));
                listaChecklist.get(i).setIdProtocolo(idProtocoloChk + "");
                idProtocolo = checklistProtocoloBI.insertaChecklistCom(listaChecklist.get(i));

                if (idProtocolo != 0) {

                    listaChecklist.get(i).setIdChecklist(idProtocolo);

                    listaPreguntasCorrectos.add(listaChecklist.get(i));

                    //Se inserta el registro en versiones de protocolos dashboard
                    try {

                        VersionProtoCheckDTO bean = new VersionProtoCheckDTO();
                        bean.setIdProtocolo("" + idProtocoloChk);

                        // Verifica la version Vigentes del protocolo para inactivarla
                        List<VersionProtoCheckDTO> listaVersiones = versionProtoCheckBI.obtieneVersiones(bean);
                        Calendar cal = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
                        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yy", new Locale("es", "ES"));

                        for (VersionProtoCheckDTO versAux : listaVersiones) {
                            if (versAux.getVigente().equalsIgnoreCase("1")) {
                                versAux.setVigente("0");

                                String fecha = versAux.getFecha();
                                try {
                                    if (fecha != null && fecha != "null") {

                                        cal.setTime(sdf.parse(fecha));

                                        String fechaBase = sdf.format(cal);
                                        versAux.setFecha(fechaBase);

                                        logger.info("Actualizo Version protocolo : " + versAux.getIdProtocolo() + " y checklist: " + versAux.getIdChecklist() + " Resultado: " + versionProtoCheckBI.actualiza(versAux));

                                    }

                                } catch (Exception e) {
                                    logger.info("Ocurrio algo al actualizar las versiones anteriores");
                                }

                            }
                        }

                        Date fechaActual = new Date();

                        String fechaSistema = sdf.format(fechaActual);

                        bean.setIdChecklist("" + idProtocolo);
                        bean.setVigente("1");
                        bean.setFecha(fechaSistema);

                        logger.info("Inserta Version protocolo : " + idProtocoloChk + " y checklist: " + idProtocolo + " Resultado: " + versionProtoCheckBI.insertaVersion(bean));
                    } catch (Exception e) {
                        logger.info("Ocurrio algo al insertar version");
                    }

                } else {

                    listaPreguntasIncorrectos.add(listaChecklist.get(i));
                }
            }

            response.add(0, listaPreguntasIncorrectos);
            response.add(1, listaPreguntasCorrectos);

        } catch (Exception e) {
            logger.info("Ocurrio algo en AP al insertar PROTOCOLO");
            //System.out.println(e.getMessage());
            //System.out.println("AP :" + e);
        }

        return response;
    }

    /* FIN METODOS ALTA PROTOCOLO */

 /* SET INFO LAYOUT*/
    public ArrayList<ChecklistLayoutDTO> setInfoLayout(ArrayList<ChecklistLayoutDTO> listaLayout,
            ArrayList<PreguntaDTO> listaPreguntas, ArrayList<ChecklistProtocoloDTO> listaProtocolos, int tipoNegocio) {

        ArrayList<ChecklistLayoutDTO> response = new ArrayList<ChecklistLayoutDTO>();

        // Pregunta necesita idChecklist
        for (ChecklistProtocoloDTO objProtocolo : listaProtocolos) {

            //System.out.println("Checklist --- " + objProtocolo.getNombreCheck());
            // SE AGREGA INFO DE PROTOCOLO A OBJETO LAYOUT
            for (int i = 0; i < listaLayout.size(); i++) {

                if (tipoNegocio == 1) {

                    if (objProtocolo.getNombreCheck().compareTo(listaLayout.get(i).getSeccionProtocolo()) == 0) {

                        listaLayout.get(i).setChecklistProtocoloDTO(objProtocolo);

                    }

                } else {

                    if (objProtocolo.getNombreCheck().compareTo(listaLayout.get(i).getNombreProtocolo()) == 0) {

                        listaLayout.get(i).setChecklistProtocoloDTO(objProtocolo);

                        response.add(listaLayout.get(i));

                    }
                }
            }

        }

        // SE AGREGA INFO DE PREGUNTA A OBJETO LAYOUT
        for (PreguntaDTO objPregunta : listaPreguntas) {

            for (int i = 0; i < listaLayout.size(); i++) {

                if (objPregunta.getPregunta().compareTo(listaLayout.get(i).getPregunta()) == 0 && objPregunta.getIdConsecutivo() == listaLayout.get(i).getIdConsecutivo()) {

                    listaLayout.get(i).setPreguntaDTO(objPregunta);

                }
            }
        }

        return listaLayout;
    }

    /* FIN SET INFO LAYOUT */
 /* INSERTA CHECKUSUAS  */
    public ArrayList<ArrayList<ChecklistLayoutDTO>> insertaCheckusua(ArrayList<ChecklistLayoutDTO> listaCheckusu) {

        ArrayList<ArrayList<ChecklistLayoutDTO>> response = new ArrayList<ArrayList<ChecklistLayoutDTO>>();
        ArrayList<ChecklistLayoutDTO> listaModulosCorrectos = new ArrayList<ChecklistLayoutDTO>();
        ArrayList<ChecklistLayoutDTO> listaModulosIncorrectos = new ArrayList<ChecklistLayoutDTO>();

        try {
            ArrayList<ArrayList<ChecklistLayoutDTO>> checkusuaResponse = setCheckUsuaCheckusua(listaCheckusu);

            listaModulosCorrectos = checkusuaResponse.get(1);

            if (listaModulosCorrectos.size() == listaCheckusu.size()) {

                response.add(0, new ArrayList<ChecklistLayoutDTO>());
                response.add(1, listaModulosCorrectos);

                return response;

            } else {

                checkusuaResponse = setCheckUsuaCheckusua(checkusuaResponse.get(0));

                for (ChecklistLayoutDTO obj : checkusuaResponse.get(1)) {
                    listaModulosCorrectos.add(obj);
                }

                if (listaModulosCorrectos.size() == listaCheckusu.size()) {

                    response.add(0, new ArrayList<ChecklistLayoutDTO>());
                    response.add(1, listaModulosCorrectos);

                    return response;

                } else {

                    response.add(0, listaModulosIncorrectos);
                    response.add(1, listaModulosCorrectos);

                    return response;
                }

            }
        } catch (Exception e) {
            logger.info("Ocurrio algo en ChecklistAdminBI ");
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<ArrayList<ChecklistLayoutDTO>> setCheckUsuaCheckusua(ArrayList<ChecklistLayoutDTO> infoLayout) {

        ArrayList<ArrayList<ChecklistLayoutDTO>> response = new ArrayList<ArrayList<ChecklistLayoutDTO>>();

        ArrayList<ChecklistLayoutDTO> listaCheckusuasCorrectos = new ArrayList<ChecklistLayoutDTO>();
        ArrayList<ChecklistLayoutDTO> listaCheckusuasIncorrectos = new ArrayList<ChecklistLayoutDTO>();

        ChecklistPreguntaBI checklistPreguntabi = (ChecklistPreguntaBI) FRQAppContextProvider.getApplicationContext().getBean("checklistPreguntaBI");

        try {
            boolean respuesta = false;

            for (int i = 0; i < infoLayout.size(); i++) {

                //System.out.println("i == " + i);
                //System.out.println("size listaChecusuas == " + infoLayout.size());
                respuesta = false;

                int idCheck = infoLayout.get(i).getChecklistProtocoloDTO().getIdChecklist();
                int ordenPreg = infoLayout.get(i).getIdConsecutivoProtocolo();
                int idPreg = infoLayout.get(i).getPreguntaDTO().getIdPregunta();
                int idPadre = buscaPadre(infoLayout, infoLayout.get(i).getChecklistProtocoloDTO().getIdChecklist(), infoLayout.get(i).getIdPreguntaPadre());
                int commit = 1;

                ChecklistPreguntaDTO pregDTO = new ChecklistPreguntaDTO();
                pregDTO.setIdChecklist(idCheck);
                pregDTO.setOrdenPregunta(ordenPreg);
                pregDTO.setIdPregunta(idPreg);
                pregDTO.setCommit(commit);
                pregDTO.setPregPadre(idPadre);

                respuesta = checklistPreguntabi.insertaChecklistPregunta(pregDTO);

                if (respuesta) {

                    listaCheckusuasCorrectos.add(infoLayout.get(i));

                } else {

                    listaCheckusuasIncorrectos.add(infoLayout.get(i));
                }
            }

            response.add(0, listaCheckusuasIncorrectos);
            response.add(1, listaCheckusuasCorrectos);

        } catch (Exception e) {
            logger.info("Ocurrio algo en AP al obtener CHECKUSUAS");
        }

        return response;

    }

    /* FIN INSERTA CHECKUSUA */
    //GENÉRICO
    public ArrayList<ChecklistLayoutDTO> llenaLista() {

        ArrayList<ChecklistLayoutDTO> lista = new ArrayList<ChecklistLayoutDTO>();

        for (int i = 0; i < 10; i++) {

            ChecklistLayoutDTO obj = new ChecklistLayoutDTO();

            obj.setIdConsecutivo(i);
            obj.setCodigoChecklist(i);
            obj.setNegocio("negocio" + i);
            obj.setNombreProtocolo("nombreProtocolo");

            obj.setSeccionProtocolo("seccionProtocolo");
            /*if (i%2==0)
				obj.setSeccionProtocolo("seccionProtocolo");
			else
				obj.setSeccionProtocolo("seccionProtocolo"+i);*/

            obj.setSubSeccionProtocolo("subSeccionProtocolo" + i);
            obj.setIdConsecutivoProtocolo(i);
            obj.setFactorCritico((int) (Math.random() * 4 + 1));
            obj.setPregunta("pregunta" + i);
            obj.setIdPreguntaPadre(0);
            obj.setTipoRespuesta(21);
            obj.setOpcionesCombo("opcionesCombo");
            obj.setCumplimiento("cumplimiento");
            obj.setPonderacion(i);
            obj.setRequiereComentarios("requiereComentarios");
            obj.setRequiereEvidencia("requiereEvidencia");
            obj.setTipoEvidencia("tipoEvidencia");
            obj.setPlantillaSi("plantillaSi");
            obj.setPlantillaNo("plantillaNo");
            obj.setResponsable1("responsable1" + i);
            obj.setResponsable2("responsable2" + i);
            obj.setZona("zona");
            obj.setDefinicion("definicion" + i);
            obj.setDisciplina("disciplina" + i);
            obj.setSla("" + i);

            lista.add(obj);

        }

        return lista;

    }

    public ArrayList<ChecklistLayoutDTO> leerExcell(File file) {
        ArrayList<ChecklistLayoutDTO> lista = new ArrayList<ChecklistLayoutDTO>();
        try {

            // leer archivo excel
            XSSFWorkbook worbook = new XSSFWorkbook(file);
            // obtener la hoja que se va leer
            XSSFSheet sheet = worbook.getSheetAt(0);
            int filas = 0;
            boolean pregError = false;
            for (Row row : sheet) {
                ChecklistLayoutDTO obj = new ChecklistLayoutDTO();
                filas++;
                int column = 0;
                for (Cell cell : row) {
                    column++;

                    String format = "";
                    DataFormatter formatter = new DataFormatter();
                    // calcula el resultado de la formula

                    // obtiene el valor y se le da formato, en este caso String
                    switch (cell.getCellTypeEnum()) {
                        case STRING:
                            // logger.info(cell.getRichStringCellValue().getString());
                            format = formatter.formatCellValue(cell);
                            ////logger.info(format);
                            break;
                        case NUMERIC:
                            if (DateUtil.isCellDateFormatted(cell)) {
                                // logger.info(cell.getDateCellValue());
                                format = formatter.formatCellValue(cell);
                                ////logger.info(format);
                            } else {
                                // logger.info(cell.getNumericCellValue());
                                format = formatter.formatCellValue(cell);
                                ////logger.info(format);
                            }
                            break;
                        case BOOLEAN:
                            // logger.info(cell.getBooleanCellValue());
                            format = formatter.formatCellValue(cell);
                            ////logger.info(format);
                            break;
                        case FORMULA:
                            // logger.info(cell.getCellFormula());
                            XSSFFormulaEvaluator evaluator = worbook.getCreationHelper().createFormulaEvaluator();
                            CellReference cellRef = new CellReference(cell.getColumnIndex(), cell.getRowIndex());
                            CellValue cellValue = evaluator.evaluate(cell);
                            logger.info("Cell Value: " + cellValue.formatAsString());

                            format = formatter.formatCellValue(cell);
                            format = cellValue.formatAsString();
                            //logger.info(format);
                            break;
                        case BLANK:
                            logger.info("");
                            format = "";
                            break;
                        default:
                            logger.info("*");
                            format = "";
                    } // Cierra switch

                    if (column == 1 && format.equals("")) {
                        break;
                    }

                    if (filas != 1) {
                        int dato = 0;
                        switch (column) {
                            case 1:

                                if (format.contains(".")) {
                                    dato = (int) Double.parseDouble(format);
                                } else {
                                    dato = Integer.parseInt(format);
                                }

                                obj.setIdConsecutivo(dato);
                                break;
                            case 2:
                                if (format.contains(".")) {
                                    dato = (int) Double.parseDouble(format);
                                } else {
                                    dato = Integer.parseInt(format);
                                }
                                obj.setCodigoChecklist(dato);
                                break;
                            case 3:
                                obj.setNegocio(format);
                                break;
                            case 4:
                                obj.setNombreProtocolo(format);
                                break;
                            case 5:
                                obj.setSeccionProtocolo(format);
                                break;
                            case 6:
                                obj.setSubSeccionProtocolo(format);
                                break;
                            case 7:
                                if (format.contains(".")) {
                                    dato = (int) Double.parseDouble(format);
                                } else {
                                    dato = Integer.parseInt(format);
                                }
                                obj.setIdConsecutivoProtocolo(dato);
                                break;
                            case 8:
                                int critico = 0;
                                if (format.toLowerCase().contains("imperdonable")) {
                                    critico = 1;
                                }
                                if (format.toLowerCase().contains("crítico")) {
                                    critico = 2;
                                }
                                if (format.toLowerCase().contains("alto")) {
                                    critico = 2;
                                }
                                if (format.toLowerCase().contains("medio")) {
                                    critico = 3;
                                }
                                if (format.toLowerCase().contains("bajo")) {
                                    critico = 4;
                                }
                                obj.setFactorCritico(critico);
                                break;
                            case 9:
                                if (format == null || format.equals("")) {
                                    pregError = true;
                                }
                                obj.setPregunta(format);
                                break;
                            case 10:
                                if (!format.equals("")) {
                                    if (format.contains(".")) {
                                        dato = (int) Double.parseDouble(format);
                                    } else {
                                        dato = Integer.parseInt(format);
                                    }
                                }
                                obj.setIdPreguntaPadre(dato);
                                break;
                            case 11:
                                int tipoPreg = 0;
                                if (format.toLowerCase().equals("si-no")) {
                                    tipoPreg = 21;
                                }
                                if (format.toLowerCase().equals("si-no-n/e")) {
                                    tipoPreg = 26;
                                }
                                if (format.toLowerCase().equals("si - no - ne")) {
                                    tipoPreg = 26;
                                }
                                if (format.toLowerCase().equals("si-no-na")) {
                                    tipoPreg = 26;
                                }
                                if (format.toLowerCase().equals("ok")) {
                                    tipoPreg = 28;
                                    obj.setOpcionesCombo("OK");
                                    obj.setFlagVF(2);
                                }
                                if (format.toLowerCase().equals("v-f")) {
                                    tipoPreg = 28;
                                    obj.setFlagVF(1);
                                }

                                if (format.toLowerCase().equals("verdadero - falso")) {
                                    tipoPreg = 28;
                                    obj.setFlagVF(1);
                                }

                                if (format.toLowerCase().equals("texto")) {
                                    tipoPreg = 24;
                                }
                                if (format.toLowerCase().equals("numérica")) {
                                    tipoPreg = 23;
                                }
                                if (format.toLowerCase().equals("combo")) {
                                    tipoPreg = 28;
                                    obj.setFlagVF(0);
                                }

                                obj.setTipoRespuesta(tipoPreg);
                                break;
                            case 12:
                                obj.setOpcionesCombo(format);
                                break;
                            case 13:
                                obj.setCumplimiento(format);
                                break;
                            case 14:
                                if (format.contains("%")) {
                                    format = format.replace("%", "");
                                }
                                if (format.contains(",")) {
                                    format = format.replace(",", ".");
                                }
                                if (format.contains("NA")) {
                                    format = format.replace("NA", "0.0");
                                }
                                obj.setPonderacion(Double.parseDouble(format));
                                break;
                            case 15:
                                int comentarios = 0;
                                if (format.toLowerCase().contains("si")) {
                                    comentarios = 1;
                                }
                                obj.setRequiereComentarios("" + comentarios);
                                break;
                            case 16:
                                int evid = 0;
                                if (format.toLowerCase().contains("si")) {
                                    evid = 1;
                                }
                                obj.setRequiereEvidencia("" + evid);
                                break;
                            case 17:
                                int tipoEvid = 0;
                                if (format.toLowerCase().contains("foto")) {
                                    tipoEvid = 1;
                                }
                                if (format.toLowerCase().contains("video")) {
                                    tipoEvid = 2;
                                }
                                obj.setTipoEvidencia("" + tipoEvid);
                                break;
                            case 18:
                                obj.setPlantillaSi(format);
                                break;
                            case 19:
                                obj.setPlantillaNo(format);
                                break;
                            case 20:
                                obj.setResponsable1(format);
                                break;
                            case 21:
                                obj.setResponsable2(format);
                                break;
                            case 22:
                                obj.setZona(format);
                                System.out.println("COLUMNA DE ZONA: " + format);
                                break;
                            case 23:
                                obj.setDefinicion(format);
                                break;
                            case 24:
                                obj.setDisciplina(format);
                                break;
                            case 25:
                                obj.setSla(format);
                                break;
                            case 26:
                                obj.setNegocioPregunta(format);
                                break;
                            default:
                                break;

                        }
                    }

                } // Cierra for cell

                if (filas != 1) {

                    if (obj.getPregunta() != null) {
                        lista.add(obj);
                    }

                }

            } // Cierra for row

        } catch (Exception e) {
            logger.info("Ocurrio algo en LeerExcell: " + e.getMessage());
            return null;
        }
        return lista;
    }

    public int ecuentraProtocolo(String descripcionProtocolo) {
        int idProtocolo = 0;
        List<ProtocoloDTO> protocolos = protocoloBI.getProtocolos();
        logger.info("Entro a encontrar protocolo " + descripcionProtocolo);
        descripcionProtocolo = descripcionProtocolo.trim();
        descripcionProtocolo = descripcionProtocolo.trim().substring(2, descripcionProtocolo.length());
        descripcionProtocolo = descripcionProtocolo.trim();

        for (ProtocoloDTO protocolo : protocolos) {
            if (descripcionProtocolo.trim().toLowerCase().contains(protocolo.getDescripcion().trim().toLowerCase())) {
                idProtocolo = protocolo.getIdProtocolo();
            }
        }
        if (idProtocolo == 0) {
            ProtocoloDTO protocolo = new ProtocoloDTO();
            protocolo.setDescripcion(descripcionProtocolo);
            protocolo.setObservaciones(descripcionProtocolo);
            protocolo.setStatus(1);
            protocolo.setStatusRetro(0);

            int respuesta = protocoloBI.insertaProtocolo(protocolo);
            idProtocolo = respuesta;
        }
        return idProtocolo;
    }

    public ArrayList<ChecklistLayoutDTO> getListaArbol(ArrayList<ChecklistLayoutDTO> listaData, int tipoCarga) {

        ArrayList<ChecklistLayoutDTO> response = new ArrayList<ChecklistLayoutDTO>();

        try {
            //Consulta protocolos

            int pos = 0;
            for (ChecklistLayoutDTO layout : listaData) {

                // Busca protocolo-
                int idProtocolo = 0;
                if (tipoCarga == 2) {
                    idProtocolo = ecuentraProtocolo(layout.getZona());
                }

                ArrayList<ArbolDecisionDTO> arbol = new ArrayList<ArbolDecisionDTO>();

                //---------------- Tipo evidencia y plantilla ---------------------
                int tipoEvidSi = 0;
                int obligaSi = 0;
                int plantillaSi = 0;
                if (!layout.getRequiereEvidencia().equals("0")) {
                    if (layout.getTipoEvidencia().equals("2")) {
                        tipoEvidSi = 3;
                    } else {
                        switch (layout.getPlantillaSi().trim()) {
                            case "A":
                                tipoEvidSi = 1;
                                obligaSi = 0;
                                break;
                            case "B":
                                tipoEvidSi = 1;
                                obligaSi = 1;
                                break;
                            case "C":
                                tipoEvidSi = 5;
                                obligaSi = 0;
                                plantillaSi = 5;
                                break;
                            case "D":
                                tipoEvidSi = 5;
                                obligaSi = 0;
                                plantillaSi = 1;
                                break;
                            default:

                        }

                    }

                }

                int tipoEvidNo = 0;
                int obligaNo = 0;
                int plantillaNo = 0;
                if (!layout.getRequiereEvidencia().equals("0")) {
                    if (layout.getTipoEvidencia().equals("2")) {
                        tipoEvidNo = 3;
                    } else {
                        switch (layout.getPlantillaNo().trim()) {
                            case "A":
                                tipoEvidNo = 1;
                                obligaNo = 0;
                                break;
                            case "B":
                                tipoEvidNo = 1;
                                obligaNo = 1;
                                break;
                            case "C":
                                tipoEvidNo = 5;
                                obligaNo = 0;
                                plantillaNo = 5;
                                break;
                            case "D":
                                tipoEvidNo = 5;
                                obligaNo = 0;
                                plantillaNo = 1;
                                break;
                            default:

                        }

                    }

                }
                //------------------------------------------------
                //---------------- Salto preguntas --------------------
                boolean padre = false;
                int saltoSi = 0;
                int saltoNo = 0;
                if (pos + 1 != listaData.size()) {
                    saltoSi = listaData.get(pos + 1).getIdConsecutivoProtocolo();

                    if (saltoSi == 1) {
                        saltoSi = 0;
                    }

                    if (listaData.get(pos).getIdPreguntaPadre() == 0) {
                        if (listaData.get(pos + 1).getIdPreguntaPadre() != 0) {
                            System.out.println("Entro a verificar el salto de preguntas");
                            //Busca posicion para salto
                            for (int i = pos + 1; i < listaData.size(); i++) {
                                if (listaData.get(i).getIdPreguntaPadre() == 0) {
                                    if (pos + 1 == listaData.size()) {
                                        saltoNo = 0;
                                    } else {
                                        saltoNo = listaData.get(i).getIdConsecutivoProtocolo();

                                    }
                                    padre = true;
                                    break;
                                }

                                if ((tipoCarga == 1) && (pos + 1 == (listaData.size() - 1))) {
                                    padre = true;
                                    break;
                                }
                            }

                        } else {
                            saltoNo = listaData.get(pos + 1).getIdConsecutivoProtocolo();
                        }
                    } else {
                        saltoNo = listaData.get(pos + 1).getIdConsecutivoProtocolo();
                    }
                }

                if (saltoNo == 1) {
                    saltoNo = 0;
                }
                //------------------------------------------------------

                //---------------- Obtiene ponderaciones --------------------
                double pondNa = 0.0;
                if (padre == true) {
                    pondNa = listaData.get(pos).getPonderacion();
                    if (listaData.get(pos).getIdPreguntaPadre() == 0) {
                        if (listaData.get(pos + 1).getIdPreguntaPadre() != 0) {
                            //Busca posicion para salto
                            //System.out.println("Entro a verificar la ponderación de preguntas padre");
                            for (int i = pos + 1; i < listaData.size(); i++) {
                                if (listaData.get(i).getIdPreguntaPadre() != 0) {
                                    pondNa = pondNa + listaData.get(i).getPonderacion();

                                } else {
                                    break;
                                }
                            }

                        }
                    }

                }
                //------------------------------------------------------

                //---------------- Tipo SI-NO --------------------
                if (layout.getPreguntaDTO().getIdTipo() == 21) {
                    ArbolDecisionDTO arbolSi = new ArbolDecisionDTO();

                    double pondSi = layout.getPonderacion();
                    double pondNo = 0;
                    if (padre == true) {
                        if (layout.getCumplimiento().toLowerCase().trim().equals("si")) {

                            if (tipoCarga == 1) {
                                pondNo = 0.0;
                            } else {
                                pondNo = 0.0;
                            }
                        } else {
                            pondSi = pondNa;
                            int saltoTempNo = saltoNo;
                            int saltoTempSi = saltoSi;
                            saltoSi = saltoTempNo;
                            saltoNo = saltoTempSi;
                        }
                    }

                    if (tipoEvidSi == 0) {
                        arbolSi.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolSi.setEstatusEvidencia(tipoEvidSi);//Depende lista entrada
                    }
                    arbolSi.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolSi.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolSi.setOrdenCheckRespuesta(saltoSi);//Depende lista entrada
                    arbolSi.setRespuesta("1");
                    arbolSi.setReqAccion(0);
                    arbolSi.setCommit(1);
                    arbolSi.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolSi.setReqOblig(obligaSi);//Depende de la plantilla
                    arbolSi.setDescEvidencia("Tomar Evidencia");
                    arbolSi.setPonderacion("" + pondSi);//Depende de la lista Data
                    arbolSi.setIdPlantilla(plantillaSi);//Depende Lista data
                    arbolSi.setIdProtocolo(idProtocolo);//Aplica solo Zonas

                    ArbolDecisionDTO arbolNo = new ArbolDecisionDTO();
                    if (tipoEvidNo == 0) {
                        arbolNo.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolNo.setEstatusEvidencia(tipoEvidNo);//Depende lista entrada
                    }
                    arbolNo.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolNo.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolNo.setOrdenCheckRespuesta(saltoNo);//Depende lista entrada
                    arbolNo.setRespuesta("2");
                    arbolNo.setReqAccion(0);
                    arbolNo.setCommit(1);
                    arbolNo.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolNo.setReqOblig(obligaNo);//
                    arbolNo.setDescEvidencia("Tomar Evidencia");
                    arbolNo.setPonderacion("" + pondNo);//Depende de la lista Data

                    arbolNo.setIdPlantilla(plantillaNo);//Depende Lista data
                    arbolNo.setIdProtocolo(idProtocolo);//Aplica solo Zonas

                    arbol.add(arbolSi);
                    arbol.add(arbolNo);
                }
                //------------------------------------------------------

                //---------------- Tipo SI-NO-NE o SI-NO-NA --------------------
                if (layout.getPreguntaDTO().getIdTipo() == 26) {

                    double pondSi = layout.getPonderacion();
                    double pondNo = 0;

                    ArbolDecisionDTO arbolSi = new ArbolDecisionDTO();
                    if (tipoEvidSi == 0) {
                        arbolSi.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolSi.setEstatusEvidencia(tipoEvidSi);//Depende lista entrada
                    }
                    arbolSi.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolSi.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolSi.setOrdenCheckRespuesta(saltoSi);//Depende lista entrada
                    arbolSi.setRespuesta("1");
                    arbolSi.setReqAccion(0);
                    arbolSi.setCommit(1);
                    arbolSi.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolSi.setReqOblig(obligaSi);//Depende de la plantilla
                    arbolSi.setDescEvidencia("Tomar Evidencia");
                    arbolSi.setPonderacion("" + layout.getPonderacion());//Depende de la lista Data
                    arbolSi.setIdPlantilla(plantillaSi);//Depende Lista data
                    arbolSi.setIdProtocolo(idProtocolo);//Aplica solo Zonas

                    ArbolDecisionDTO arbolNo = new ArbolDecisionDTO();
                    if (tipoEvidNo == 0) {
                        arbolNo.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolNo.setEstatusEvidencia(tipoEvidNo);//Depende lista entrada
                    }
                    arbolNo.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolNo.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolNo.setOrdenCheckRespuesta(saltoNo);//Depende lista entrada
                    arbolNo.setRespuesta("2");
                    arbolNo.setReqAccion(0);
                    arbolNo.setCommit(1);
                    arbolNo.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolNo.setReqOblig(obligaNo);//
                    arbolNo.setDescEvidencia("Tomar Evidencia");
                    arbolNo.setPonderacion("0");//Depende de la lista Data
                    arbolNo.setIdPlantilla(plantillaNo);//Depende Lista data
                    arbolNo.setIdProtocolo(idProtocolo);//Aplica solo Zonas

                    ArbolDecisionDTO arbolNa = new ArbolDecisionDTO();
                    arbolNa.setEstatusEvidencia(0);//Depende lista entrada
                    arbolNa.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolNa.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolNa.setOrdenCheckRespuesta(saltoNo);//Depende lista entrada
                    arbolNa.setRespuesta("3");
                    arbolNa.setReqAccion(0);
                    arbolNa.setCommit(1);
                    arbolNa.setReqObservacion(0);//Depende lista entrada
                    arbolNa.setReqOblig(0);//
                    arbolNa.setDescEvidencia("Tomar Evidencia");
                    if (padre == true) {
                        arbolNa.setPonderacion("" + pondNa);//Depende de la lista Data
                    } else {
                        arbolNa.setPonderacion("" + pondSi);//Depende de la lista Data
                    }
                    arbolNa.setIdPlantilla(0);//Depende Lista data
                    arbolNa.setIdProtocolo(idProtocolo);//Aplica solo Zonas

                    arbol.add(arbolSi);
                    arbol.add(arbolNo);
                    arbol.add(arbolNa);
                }
                //------------------------------------------------------

                //---------------- Tipo Texto --------------------
                if (layout.getPreguntaDTO().getIdTipo() == 24) {
                    ArbolDecisionDTO arbolSi = new ArbolDecisionDTO();
                    if (tipoEvidSi == 0) {
                        arbolSi.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolSi.setEstatusEvidencia(tipoEvidSi);//Depende lista entrada
                    }
                    arbolSi.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolSi.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolSi.setOrdenCheckRespuesta(saltoSi);//Depende lista entrada
                    arbolSi.setRespuesta("4");
                    arbolSi.setReqAccion(0);
                    arbolSi.setCommit(1);
                    arbolSi.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolSi.setReqOblig(obligaSi);//Depende de la plantilla
                    arbolSi.setDescEvidencia("Tomar Evidencia");
                    arbolSi.setPonderacion("" + layout.getPonderacion());//Depende de la lista Data
                    arbolSi.setIdPlantilla(plantillaSi);//Depende Lista data
                    arbolSi.setIdProtocolo(idProtocolo);//Aplica solo Zonas

                    arbol.add(arbolSi);
                }
                //------------------------------------------------------

                //---------------- Tipo Numerica --------------------
                if (layout.getPreguntaDTO().getIdTipo() == 23) {
                    ArbolDecisionDTO arbolSi = new ArbolDecisionDTO();
                    if (tipoEvidSi == 0) {
                        arbolSi.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolSi.setEstatusEvidencia(tipoEvidSi);//Depende lista entrada
                    }
                    arbolSi.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolSi.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolSi.setOrdenCheckRespuesta(saltoSi);//Depende lista entrada
                    arbolSi.setRespuesta("4");
                    arbolSi.setReqAccion(0);
                    arbolSi.setCommit(1);
                    arbolSi.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolSi.setReqOblig(obligaSi);//Depende de la plantilla
                    arbolSi.setDescEvidencia("Tomar Evidencia");
                    arbolSi.setPonderacion("" + layout.getPonderacion());//Depende de la lista Data
                    arbolSi.setIdPlantilla(plantillaSi);//Depende Lista data
                    arbolSi.setIdProtocolo(idProtocolo);//Aplica solo Zonas

                    arbol.add(arbolSi);
                }
                //------------------------------------------------------

                //---------------- Tipo V-F --------------------
                if (layout.getPreguntaDTO().getIdTipo() == 28 && layout.getFlagVF() == 1) {
                    ArbolDecisionDTO arbolSi = new ArbolDecisionDTO();
                    if (tipoEvidSi == 0) {
                        arbolSi.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolSi.setEstatusEvidencia(tipoEvidSi);//Depende lista entrada
                    }
                    arbolSi.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolSi.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolSi.setOrdenCheckRespuesta(saltoSi);//Depende lista entrada
                    arbolSi.setRespuesta("432");
                    arbolSi.setReqAccion(0);
                    arbolSi.setCommit(1);
                    arbolSi.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolSi.setReqOblig(obligaSi);//Depende de la plantilla
                    arbolSi.setDescEvidencia("Tomar Evidencia");
                    arbolSi.setPonderacion("" + layout.getPonderacion());//Depende de la lista Data
                    arbolSi.setIdPlantilla(plantillaSi);//Depende Lista data
                    arbolSi.setIdProtocolo(idProtocolo);//Aplica solo Zonas

                    ArbolDecisionDTO arbolNo = new ArbolDecisionDTO();
                    if (tipoEvidNo == 0) {
                        arbolNo.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolNo.setEstatusEvidencia(tipoEvidNo);//Depende lista entrada
                    }
                    arbolNo.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolNo.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolNo.setOrdenCheckRespuesta(saltoNo);//Depende lista entrada
                    arbolNo.setRespuesta("433");
                    arbolNo.setReqAccion(0);
                    arbolNo.setCommit(1);
                    arbolNo.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolNo.setReqOblig(obligaNo);//
                    arbolNo.setDescEvidencia("Tomar Evidencia");
                    if (padre == true && tipoCarga == 2) {
                        arbolNo.setPonderacion("" + pondNa);//Depende de la lista Data
                    } else {
                        arbolNo.setPonderacion("0");//Depende de la lista Data
                    }
                    arbolNo.setIdPlantilla(plantillaNo);//Depende Lista data
                    arbolNo.setIdProtocolo(idProtocolo);//Aplica solo Zonas

                    arbol.add(arbolSi);
                    arbol.add(arbolNo);
                }
                //------------------------------------------------------

                //---------------- Tipo OK --------------------
                if (layout.getPreguntaDTO().getIdTipo() == 28 && layout.getFlagVF() == 2) {
                    ArbolDecisionDTO arbolSi = new ArbolDecisionDTO();
                    if (tipoEvidSi == 0) {
                        arbolSi.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolSi.setEstatusEvidencia(tipoEvidSi);//Depende lista entrada
                    }
                    arbolSi.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolSi.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolSi.setOrdenCheckRespuesta(saltoSi);//Depende lista entrada
                    arbolSi.setRespuesta("485");
                    arbolSi.setReqAccion(0);
                    arbolSi.setCommit(1);
                    arbolSi.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolSi.setReqOblig(obligaSi);//Depende de la plantilla
                    arbolSi.setDescEvidencia("Tomar Evidencia");
                    arbolSi.setPonderacion("" + layout.getPonderacion());//Depende de la lista Data
                    arbolSi.setIdPlantilla(plantillaSi);//Depende Lista data
                    arbolSi.setIdProtocolo(idProtocolo);//Aplica solo Zonas

                    arbol.add(arbolSi);
                }
                //------------------------------------------------------

                //---------------- Tipo Combo --------------------
                if (layout.getPreguntaDTO().getIdTipo() == 28 && layout.getFlagVF() == 0) {

                    String[] opcCombo = layout.getOpcionesCombo().split(",");

                    for (String opc : opcCombo) {
                        int idCombo = 0;
                        for (int i = 0; i < 3; i++) {
                            idCombo = getOpcionCombo(opc);
                            if (idCombo != 0) {
                                break;
                            }
                        }
                        if (idCombo != 0) {
                            ArbolDecisionDTO arbolSi = new ArbolDecisionDTO();
                            if (tipoEvidSi == 0) {
                                arbolSi.setEstatusEvidencia(0);//Depende lista entrada
                            } else {
                                arbolSi.setEstatusEvidencia(tipoEvidSi);//Depende lista entrada
                            }
                            arbolSi.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                            arbolSi.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                            arbolSi.setOrdenCheckRespuesta(saltoSi);//Depende lista entrada
                            arbolSi.setRespuesta("" + idCombo);
                            arbolSi.setReqAccion(0);
                            arbolSi.setCommit(1);
                            arbolSi.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                            arbolSi.setReqOblig(obligaSi);//Depende de la plantilla
                            arbolSi.setDescEvidencia("Tomar Evidencia");
                            arbolSi.setPonderacion("" + layout.getPonderacion());//Depende de la lista Data
                            arbolSi.setIdPlantilla(plantillaSi);//Depende Lista data
                            arbolSi.setIdProtocolo(idProtocolo);//Aplica solo Zonas

                            arbol.add(arbolSi);
                        } else {
                            //Algo ocurrio al generar el combo
                            System.out.println("Algo ocurrio al generar el combo");
                        }
                    }
                }
                //------------------------------------------------------

                pos++;
                layout.setArbolDTO(arbol);

                response.add(layout);
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo en ChecklistAdminBI ");
            e.printStackTrace();
            return response;

        }

        return response;

    }

    public int getOpcionCombo(String opc) {
        int response = 0;

        try {
            //inserta Opcion combo
            int res = posiblesbi.insertaPosible(opc);
            response = res;

        } catch (Exception e) {
            logger.info("Ocurrio algo en getOpcionCombo");
            response = 0;
        }

        return response;
    }

    public ArrayList<ChecklistLayoutDTO> altaArbol(ArrayList<ChecklistLayoutDTO> listaData, int tipoCarga) {

        ArrayList<ChecklistLayoutDTO> response = new ArrayList<ChecklistLayoutDTO>();

        try {
            int pos = 0;
            for (ChecklistLayoutDTO layout : listaData) {
                ArrayList<ArbolDecisionDTO> arbolDes = new ArrayList<ArbolDecisionDTO>();
                for (ArbolDecisionDTO arbol : layout.getArbolDTO()) {

                    int idArbol = 0;
                    for (int i = 0; i < 3; i++) {
                        idArbol = ejecutaArbol(arbol);
                        if (idArbol != 0) {
                            arbol.setIdArbolDesicion(idArbol);
                            break;
                        }
                    }
                    if (idArbol != 0) {
                        arbolDes.add(arbol);
                        pos = pos + 1;
                        System.out.println("Inserto el arbol=" + idArbol + ", pos=" + pos);
                    } else {
                        layout.setFlagErrorArbol(1);
                        break;
                    }
                }
                layout.setArbolDTO(arbolDes);
                response.add(layout);
            }

        } catch (Exception e) {

            logger.info("Ocurrio algo en ChecklistAdminBI ");
            return response;

        }

        return response;

    }

    public int ejecutaArbol(ArbolDecisionDTO arbol) {
        int response = 0;

        try {
            //inserta Opcion combo
            System.out.println("ARBOL PROD: " + arbol);
            int resArbol = arbolDecisionbi.insertaArbolDecision(arbol);
            response = resArbol;

        } catch (Exception e) {
            e.printStackTrace();
            response = 0;
        }

        return response;
    }

    public int buscaPadre(ArrayList<ChecklistLayoutDTO> infoLayout, int idProtocolo, int numeroPadre) {
        int response = 0;
        for (ChecklistLayoutDTO layout : infoLayout) {
            if (layout.getChecklistProtocoloDTO().getIdChecklist() == idProtocolo && numeroPadre == layout.getIdConsecutivoProtocolo()) {
                response = layout.getPreguntaDTO().getIdPregunta();
                break;
            }
        }

        return response;
    }

    public ArrayList<ChecklistLayoutDTO> leerJson(String datos) {
        ArrayList<ChecklistLayoutDTO> retorno = new ArrayList<ChecklistLayoutDTO>();
        try {
            System.out.println("Json Entrada:  " + datos);

            int tipoCarga = 0;
            if (datos != null) {
                JSONObject rec = new JSONObject(datos);

                JSONObject generales = rec.getJSONObject("datos_generales");
                String nomProtocolo = generales.getString("nombre");
                String tipoCar = generales.getString("tipo_protocolo");

                if (tipoCar.equals("Infraestructura")) {
                    tipoCarga = 1;
                } else {
                    tipoCarga = 2;
                }
                JSONArray secciones = rec.getJSONArray("secciones");
                int contGlobal = 0;
                for (int i = 0; i < secciones.length(); i++) {
                    JSONObject generalesSeccion = secciones.getJSONObject(i).getJSONObject("datos_generales");
                    String seccion = generalesSeccion.getString("nombre");
                    String sub_seccion = generalesSeccion.getString("sub_seccion");

                    JSONArray preguntas = secciones.getJSONObject(i).getJSONArray("preguntas");

                    for (int j = 0; j < preguntas.length(); j++) {
                        contGlobal++;
                        ChecklistLayoutDTO objLayout = new ChecklistLayoutDTO();
                        objLayout.setIdConsecutivo(contGlobal);
                        objLayout.setCodigoChecklist(preguntas.getJSONObject(j).getInt("codigo"));
                        objLayout.setNegocio(preguntas.getJSONObject(j).getString("negocio"));
                        objLayout.setNombreProtocolo(nomProtocolo);
                        objLayout.setSeccionProtocolo(seccion);
                        objLayout.setSubSeccionProtocolo(sub_seccion);
                        objLayout.setIdConsecutivoProtocolo(preguntas.getJSONObject(j).getInt("no"));

                        int critico = 0;
                        String format = preguntas.getJSONObject(j).getString("factorCritico");
                        if (format.toLowerCase().contains("imperdonable")) {
                            critico = 1;
                        }
                        if (format.toLowerCase().contains("crítico")) {
                            critico = 2;
                        }
                        if (format.toLowerCase().contains("alto")) {
                            critico = 2;
                        }
                        if (format.toLowerCase().contains("medio")) {
                            critico = 3;
                        }

                        objLayout.setFactorCritico(critico);

                        objLayout.setPregunta(preguntas.getJSONObject(j).getString("pregunta"));

                        int tipoPreg = 0;

                        format = preguntas.getJSONObject(j).getString("tipoRespuesta");

                        if (format.toLowerCase().equals("si-no")) {
                            tipoPreg = 21;
                        }
                        if (format.toLowerCase().equals("si - no")) {
                            tipoPreg = 21;
                        }
                        if (format.toLowerCase().equals("si-no-n/e")) {
                            tipoPreg = 26;
                        }
                        if (format.toLowerCase().equals("si - no - ne")) {
                            tipoPreg = 26;
                        }
                        if (format.toLowerCase().equals("si-no-na")) {
                            tipoPreg = 26;
                        }
                        if (format.toLowerCase().equals("si - no - na")) {
                            tipoPreg = 26;
                        }
                        if (format.toLowerCase().equals("ok")) {
                            tipoPreg = 28;
                            objLayout.setOpcionesCombo("OK");
                            objLayout.setFlagVF(2);
                        }
                        if (format.toLowerCase().equals("v-f")) {
                            tipoPreg = 28;
                            objLayout.setFlagVF(1);
                        }
                        if (format.toLowerCase().equals("verdadero - falso")) {
                            tipoPreg = 28;
                            objLayout.setFlagVF(1);
                        }
                        if (format.toLowerCase().equals("texto")) {
                            tipoPreg = 24;
                        }
                        if (format.toLowerCase().equals("numérica")) {
                            tipoPreg = 23;
                        }
                        if (format.toLowerCase().equals("combo")) {
                            tipoPreg = 28;
                            objLayout.setFlagVF(0);
                        }

                        objLayout.setTipoRespuesta(tipoPreg);
                        objLayout.setOpcionesCombo(preguntas.getJSONObject(j).getString("opcionesCombo"));
                        objLayout.setIdPreguntaPadre(preguntas.getJSONObject(j).getInt("padre"));
                        objLayout.setCumplimiento(preguntas.getJSONObject(j).getString("cumplimiento"));

                        double valorP = 0.0;
                        format = preguntas.getJSONObject(j).getString("valorPonderacion");
                        if (format.contains("%")) {
                            format = format.replace("%", "");
                        }
                        if (format.contains(",")) {
                            format = format.replace(",", ".");
                        }
                        if (format.contains("NA")) {
                            format = format.replace("NA", "0.0");
                        }
                        if (format.contains("N/A")) {
                            format = format.replace("NA", "0.0");
                        }
                        valorP = Double.parseDouble(format);

                        objLayout.setPonderacion(valorP);

                        int comentarios = 0;
                        format = preguntas.getJSONObject(j).getString("comentario");
                        if (format.toLowerCase().contains("si")) {
                            comentarios = 1;
                        }
                        objLayout.setRequiereComentarios("" + comentarios);

                        int evid = 0;
                        format = preguntas.getJSONObject(j).getString("evidencia");
                        if (format.toLowerCase().contains("si")) {
                            evid = 1;
                        }
                        objLayout.setRequiereEvidencia("" + evid);

                        int tipoEvid = 0;
                        format = preguntas.getJSONObject(j).getString("tipoEvidencia");
                        if (format.toLowerCase().contains("foto")) {
                            tipoEvid = 1;
                        }
                        if (format.toLowerCase().contains("video")) {
                            tipoEvid = 2;
                        }
                        objLayout.setTipoEvidencia("" + tipoEvid);

                        objLayout.setPlantillaSi(preguntas.getJSONObject(j).getString("plantillaSi"));
                        objLayout.setPlantillaNo(preguntas.getJSONObject(j).getString("plantillaNo"));
                        objLayout.setResponsable1(preguntas.getJSONObject(j).getString("responsable"));
                        objLayout.setResponsable2(preguntas.getJSONObject(j).getString("responsable"));
                        objLayout.setZona(preguntas.getJSONObject(j).getString("zona"));
                        objLayout.setDefinicion(preguntas.getJSONObject(j).getString("definicion"));
                        objLayout.setDisciplina(preguntas.getJSONObject(j).getString("negocio"));
                        objLayout.setSla(preguntas.getJSONObject(j).getString("sla"));
                        retorno.add(objLayout);

                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            retorno = null;
        }

        return retorno;

    }

    public String retornaJson(ArrayList<ChecklistLayoutDTO> layout, int tipoCarga) {
        String retorno = null;
        try {
            retorno = "";
            JsonObject envoltorioJsonObj = new JsonObject();

            JsonObject generales = new JsonObject();
            int contador = 0;

            ArrayList<ModuloDTO> modulos = getListaModulos(layout);

            HashMap<String, ArrayList<ChecklistLayoutDTO>> mapa = new HashMap<String, ArrayList<ChecklistLayoutDTO>>();
            for (int i = 0; i < modulos.size(); i++) {
                modulos.get(i).setIdModulo(i);
                mapa.put("" + i, new ArrayList<ChecklistLayoutDTO>());
            }

            String Null = null;
            JsonArray secciones = new JsonArray();
            for (ChecklistLayoutDTO aux : layout) {

                if (contador == 0) {

                    generales.addProperty("no", Null);
                    generales.addProperty("nombre", aux.getNombreProtocolo());

                    if (tipoCarga == 1) {
                        generales.addProperty("tipo_protocolo", "Infraestructura");
                    } else {
                        generales.addProperty("tipo_protocolo", "Aseguramiento de calidad");
                    }

                }

                for (int i = 0; i < modulos.size(); i++) {
                    if (aux.getSeccionProtocolo().trim().equals(modulos.get(i).getNombre().trim())) {
                        mapa.get("" + i).add(aux);
                        break;
                    }
                }

                contador++;
            }

            /*
			for(int i=0;i<modulos.size();i++) {
				ArrayList<ChecklistLayoutDTO> auxSeccion=mapa.get(""+i);
				ArrayList<ModuloDTO> subModulos=getSubModulos(auxSeccion);

				HashMap <String, ArrayList<ChecklistLayoutDTO>> mapaSub=new HashMap<String, ArrayList<ChecklistLayoutDTO>>();
				for(int j=0;j<subModulos.size();j++) {
					subModulos.get(j).setIdModulo(j);
					mapaSub.put(""+j, new ArrayList<ChecklistLayoutDTO>());
				}

				for(ChecklistLayoutDTO aux:auxSeccion) {

					for(int j=0;j<subModulos.size();j++) {
						if(aux.getSubSeccionProtocolo().trim().equals(subModulos.get(i).getNombre().trim())) {
							mapaSub.get(""+i).add(aux);
							break;
						}
					}

				}

				for(int j=0;j<subModulos.size();j++) {

					ArrayList<ChecklistLayoutDTO> auxSubSeccion=mapaSub.get(""+i);
					JsonObject Seccion = new JsonObject();
					JsonObject generalesSeccion = new JsonObject();
					JsonArray preguntas=new JsonArray();
					for(ChecklistLayoutDTO aux:auxSubSeccion) {
						//Incluir subseccion al json

						if(contador==0) {

							generalesSeccion.addProperty("no", Null);
							generalesSeccion.addProperty("nombre", aux.getSeccionProtocolo());
							generalesSeccion.addProperty("sub_seccion", aux.getSubSeccionProtocolo());


						}
						//secciones.add(generalesSeccion);




						JsonObject pregunta = new JsonObject();
						pregunta.addProperty("no", aux.getIdConsecutivoProtocolo());
						pregunta.addProperty("codigo", aux.getCodigoChecklist());
						pregunta.addProperty("pregunta", aux.getPregunta());
						pregunta.addProperty("factorCritico", aux.getFactorCritico());
						pregunta.addProperty("padre", aux.getIdPreguntaPadre());
						pregunta.addProperty("tipoRespuesta", aux.getTipoRespuesta());
						pregunta.addProperty("opcionesCombo", aux.getOpcionesCombo());
						pregunta.addProperty("cumplimiento", aux.getCumplimiento());
						pregunta.addProperty("valorPonderacion", aux.getPonderacion());
						pregunta.addProperty("comentario", aux.getRequiereComentarios());
						pregunta.addProperty("evidencia", aux.getRequiereEvidencia());
						pregunta.addProperty("tipoEvidencia", aux.getTipoEvidencia());
						pregunta.addProperty("plantillaSi", aux.getPlantillaSi());
						pregunta.addProperty("plantillaNo", aux.getPlantillaNo());
						pregunta.addProperty("definicion", aux.getDefinicion());
						pregunta.addProperty("zona", aux.getZona());
						pregunta.addProperty("responsable", aux.getZona());
						pregunta.addProperty("sla", aux.getSla());

						preguntas.add(pregunta);


					}
					Seccion.add("datos_generales", generalesSeccion);
					Seccion.add("preguntas", preguntas);
					envoltorioJsonObj.add("secciones", Seccion);
				}


			}*/
            JsonArray Secciones = new JsonArray();
            for (int j = 0; j < modulos.size(); j++) {

                ArrayList<ChecklistLayoutDTO> auxSubSeccion = mapa.get("" + j);
                JsonObject Seccion = new JsonObject();
                JsonObject generalesSeccion = new JsonObject();
                JsonArray preguntas = new JsonArray();
                int cont = 0;
                for (ChecklistLayoutDTO aux : auxSubSeccion) {
                    //Incluir subseccion al json

                    if (cont == 0) {

                        generalesSeccion.addProperty("no", Null);
                        generalesSeccion.addProperty("nombre", aux.getSeccionProtocolo());
                        generalesSeccion.addProperty("sub_seccion", aux.getSubSeccionProtocolo());

                    }
                    //secciones.add(generalesSeccion);

                    JsonObject pregunta = new JsonObject();
                    pregunta.addProperty("no", aux.getIdConsecutivoProtocolo());
                    pregunta.addProperty("codigo", aux.getCodigoChecklist());
                    pregunta.addProperty("negocio", aux.getNegocio());
                    pregunta.addProperty("pregunta", aux.getPregunta());

                    String fCritico = "";
                    if (aux.getFactorCritico() == 1) {
                        fCritico = "Imperdonable";
                    }
                    if (aux.getFactorCritico() == 2) {
                        fCritico = "Alto";
                    }
                    if (aux.getFactorCritico() == 3) {
                        fCritico = "Medio";
                    }
                    if (aux.getFactorCritico() == 4) {
                        fCritico = "Bajo";
                    }

                    pregunta.addProperty("factorCritico", fCritico);
                    pregunta.addProperty("padre", aux.getIdPreguntaPadre());

                    String tRespuesta = "";

                    if (aux.getTipoRespuesta() == 21) {
                        tRespuesta = "SI - NO";
                    }
                    if (aux.getTipoRespuesta() == 28 && aux.getFlagVF() == 1) {
                        tRespuesta = "Verdadero - Falso";
                    }
                    if (aux.getTipoRespuesta() == 28 && aux.getFlagVF() == 0) {
                        tRespuesta = "Combo";
                    }
                    if (aux.getTipoRespuesta() == 28 && aux.getFlagVF() == 2) {
                        tRespuesta = "OK";
                    }
                    if (aux.getTipoRespuesta() == 26) {
                        tRespuesta = "SI - NO - NE";
                    }
                    if (aux.getTipoRespuesta() == 24) {
                        tRespuesta = "Texto";
                    }
                    if (aux.getTipoRespuesta() == 23) {
                        tRespuesta = "Numérica";
                    }

                    pregunta.addProperty("tipoRespuesta", tRespuesta);
                    pregunta.addProperty("opcionesCombo", aux.getOpcionesCombo());

                    pregunta.addProperty("cumplimiento", aux.getCumplimiento().toUpperCase());
                    pregunta.addProperty("valorPonderacion", aux.getPonderacion());

                    String comentario = "";
                    if (aux.getRequiereComentarios().trim().equals("1")) {
                        comentario = "SI";
                    } else {
                        comentario = "NO";
                    }

                    pregunta.addProperty("comentario", comentario);

                    String evidecia = "";
                    if (aux.getRequiereEvidencia().trim().equals("1")) {
                        evidecia = "SI";
                    } else {
                        evidecia = "NO";
                    }

                    pregunta.addProperty("evidencia", evidecia);

                    String tEvidecia = "";
                    if (aux.getTipoEvidencia().trim().equals("1")) {
                        tEvidecia = "Foto";
                    } else {
                        tEvidecia = "";
                    }
                    if (aux.getTipoEvidencia().trim().equals("2")) {
                        tEvidecia = "Video";
                    }
                    pregunta.addProperty("tipoEvidencia", tEvidecia);
                    pregunta.addProperty("plantillaSi", aux.getPlantillaSi());
                    pregunta.addProperty("plantillaNo", aux.getPlantillaNo());
                    pregunta.addProperty("definicion", aux.getDefinicion());

                    String zona = "";
                    if (tipoCarga == 1) {
                        switch (aux.getZona()) {
                            case "Generales":
                                zona = "GENERALES";
                                break;
                            case "Áreas comunes":
                                zona = "AREAS";
                            case "Areas":
                                zona = "AREAS";
                                break;
                            case "BAZ":
                                zona = "BAZ";
                                break;
                            case "EKT":
                                zona = "EKT";
                                break;
                            case "Presta Prenda":
                                zona = "PP";
                                break;
                            case "Crédito y cobranza":
                                zona = "CYC";
                            case "Credito y cobranza":
                                zona = "CYC";
                                break;
                            case "Italika":
                                zona = "ITK";
                                break;
                            default:
                                zona = aux.getZona();
                                ;
                        }
                    } else {
                        zona = aux.getZona();
                    }
                    pregunta.addProperty("zona", zona);
                    pregunta.addProperty("responsable", aux.getResponsable2());
                    pregunta.addProperty("sla", aux.getSla());

                    preguntas.add(pregunta);
                    cont++;

                }
                Seccion.add("datos_generales", generalesSeccion);
                Seccion.add("preguntas", preguntas);
                Secciones.add(Seccion);
            }
            envoltorioJsonObj.add("secciones", Secciones);

            envoltorioJsonObj.add("datos_generales", generales);

            //logger.info("RESPUESTAS: " + envoltorioJsonObj.toString());
            retorno = envoltorioJsonObj.toString();

        } catch (Exception e) {
            e.printStackTrace();
            retorno = null;
        }
        return retorno;
    }

    public ArrayList<ModuloDTO> getSubModulos(ArrayList<ChecklistLayoutDTO> listaData) {

        ArrayList<ModuloDTO> listaModulos = new ArrayList<ModuloDTO>();

        try {

            String seccionActual = "";
            boolean existe = false;
            for (ChecklistLayoutDTO objLista : listaData) {

                if (!objLista.getSubSeccionProtocolo().equalsIgnoreCase(seccionActual)) {

                    // VALIDAR SECCIONES SALTADAS
                    existe = false;
                    seccionActual = objLista.getSubSeccionProtocolo();

                    ModuloDTO modulo = new ModuloDTO();
                    modulo.setIdModulo(0);
                    modulo.setNombre(objLista.getSubSeccionProtocolo());
                    modulo.setIdModuloPadre(0);
                    modulo.setNombrePadre("");
                    modulo.setNumVersion("0");
                    modulo.setTipoCambio("");
                    modulo.setIdChecklist(0);

                    for (ModuloDTO obj : listaModulos) {

                        if (objLista.getSubSeccionProtocolo().compareTo(obj.getNombre()) == 0) {

                            existe = true;
                            break;
                        }

                    }

                    if (!existe) {
                        listaModulos.add(modulo);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listaModulos;

    }

    public String retornaProtocoloBase(int idProtocolo, int tipoCarga) {
        String resultado = null;

        try {
            resultado = "";
            String nula = null;
            int contador = 0;
            JsonObject jsonRespuesta = new JsonObject();
            JsonObject datosGrales = new JsonObject();
            List<ModuloDTO> listaModulos = null;
            List<ChecklistPreguntaDTO> listaChecklistPregunta = null;

            Map<String, Object> res = reporteImagenesBI.obtieneModulosTiendas("", String.valueOf(idProtocolo));
            listaModulos = (List<ModuloDTO>) res.get("modulos");
            //logger.info("Modulos: " + listaModulos.toString());
            listaChecklistPregunta = preguntaAdmBI.obtienePregXcheck(idProtocolo);
            //logger.info("Lista Checklist Pregunta: " + listaChecklistPregunta.toString());

            ChecklistProtocoloDTO checklistProtocolo = new ChecklistProtocoloDTO();
            checklistProtocolo.setIdChecklist(idProtocolo);
            List<ChecklistProtocoloDTO> listaProtocolos = checklistProtocoloBI.buscaChecklist(checklistProtocolo);
            //logger.info("Lista Checklist Protocolo: " + listaProtocolos.toString());

            for (int i = 0; i < listaModulos.size(); i++) {
                if (contador == 0) {
                    datosGrales.addProperty("no", nula);
                    if (listaProtocolos.get(i).getIdChecklist() == idProtocolo) {
                        datosGrales.addProperty("nombre", listaProtocolos.get(i).getNombreCheck());
                    }
                    if (tipoCarga == 1) {
                        datosGrales.addProperty("tipo_protocolo", "Infraestructura");
                    } else {
                        datosGrales.addProperty("tipo_protocolo", "Aseguramiento de calidad");
                    }

                }
                contador++;

            }

            JsonArray arrSecciones = new JsonArray();
            for (int j = 0; j < listaModulos.size(); j++) {
                int cont = 0;
                JsonObject seccionesGrales = new JsonObject();
                JsonObject secciones = new JsonObject();
                JsonArray arrPreguntas = new JsonArray();

                secciones.addProperty("no", nula);
                secciones.addProperty("nombre", listaModulos.get(j).getNombre());
                secciones.addProperty("sub_seccion", "");
                for (int k = 0; k < listaChecklistPregunta.size(); k++) {

                    JsonObject pregunta = new JsonObject();
                    pregunta.addProperty("no", "");
                    pregunta.addProperty("codigo", listaChecklistPregunta.get(k).getCodigo());
                    pregunta.addProperty("negocio", "");
                    pregunta.addProperty("pregunta", listaChecklistPregunta.get(k).getPregunta());
                    pregunta.addProperty("factorCritico", "");
                    pregunta.addProperty("padre", listaChecklistPregunta.get(k).getPregPadre());

                    String tRespuesta = "";

                    if (listaChecklistPregunta.get(k).getIdTipoPregunta() == 21) {
                        tRespuesta = "SI - NO";
                    } else if (listaChecklistPregunta.get(k).getIdTipoPregunta() == 28 && listaChecklistPregunta.get(k).getIdTipoPregunta() == 1) {
                        tRespuesta = "Verdadero - Falso";
                    } else if (listaChecklistPregunta.get(k).getIdTipoPregunta() == 28 && listaChecklistPregunta.get(k).getIdTipoPregunta() == 0) {
                        tRespuesta = "Combo";
                    } else if (listaChecklistPregunta.get(k).getIdTipoPregunta() == 28 && listaChecklistPregunta.get(k).getIdTipoPregunta() == 2) {
                        tRespuesta = "OK";
                    } else if (listaChecklistPregunta.get(k).getIdTipoPregunta() == 26) {
                        tRespuesta = "SI - NO - NE";
                    } else if (listaChecklistPregunta.get(k).getIdTipoPregunta() == 24) {
                        tRespuesta = "Texto";
                    } else if (listaChecklistPregunta.get(k).getIdTipoPregunta() == 23) {
                        tRespuesta = "Numérica";
                    }
                    pregunta.addProperty("tipoRespuesta", tRespuesta);
                    pregunta.addProperty("opcionesCombo", "");
                    pregunta.addProperty("cumplimiento", "");
                    pregunta.addProperty("valorPonderacion", "");
                    pregunta.addProperty("definicion", listaChecklistPregunta.get(k).getDetalle());

                    arrPreguntas.add(pregunta);
                    cont++;
                }
                seccionesGrales.add("datos_generales", secciones);
                seccionesGrales.add("preguntas", arrPreguntas);
                arrSecciones.add(seccionesGrales);
            }
            jsonRespuesta.add("secciones", arrSecciones);
            jsonRespuesta.add("datos_generales", datosGrales);

            //logger.info("RESULTADO: " + jsonRespuesta.toString());
            resultado = jsonRespuesta.toString();
        } catch (Exception e) {
            e.printStackTrace();
            resultado = null;
        }

        return resultado;
    }

    public ArrayList<ChecklistLayoutDTO> retornaLayoutBase(int idProtocolo, int tipoCarga) {
        ArrayList<ChecklistLayoutDTO> lista = new ArrayList<ChecklistLayoutDTO>();
        try {
            //System.out.println("<<<<<<<<PROTOCOLO:>>>>>>>>> " + idProtocolo);

            HashMap<String, ModuloDTO> mapaMod = new HashMap<String, ModuloDTO>();

            List<ModuloDTO> listaModulos = null;
            List<ChecklistPreguntaDTO> listaChecklistPregunta = null;

            Map<String, Object> res = reporteImagenesBI.obtieneModulosTiendas("", String.valueOf(idProtocolo));
            listaModulos = (List<ModuloDTO>) res.get("modulos");
            //logger.info("Modulos: " + listaModulos.toString());
            listaChecklistPregunta = checklistPreguntabi.obtienePregXcheck(idProtocolo);
            //logger.info("Lista Checklist Pregunta: " + listaChecklistPregunta.toString());

            ChecklistProtocoloDTO checklistProtocolo = new ChecklistProtocoloDTO();
            checklistProtocolo.setIdChecklist(idProtocolo);
            List<ChecklistProtocoloDTO> listaProtocolos = checklistProtocoloBI.buscaChecklist(checklistProtocolo);
            //logger.info("Lista Checklist Protocolo: " + listaProtocolos.toString());

            List<ArbolDecisionDTO> listaArbol = arbolDecisionbi.buscaArbolModificaciones(idProtocolo);//Obtiene Arbol desicion

            HashMap<String, List<ArbolDecisionDTO>> mapaArbol = obtieneArbolesPreg(listaArbol);

            for (ModuloDTO mod : listaModulos) {
                mapaMod.put("" + mod.getIdModulo(), mod);
            }

            String nomProtocolo = "";
            String negocio = "";
            String zonaInfra = "";
            for (ChecklistProtocoloDTO prot : listaProtocolos) {
                nomProtocolo = prot.getNombreCheck();
                negocio = prot.getNegocio();
                zonaInfra = prot.getClasifica();
            }

            for (ChecklistPreguntaDTO preg : listaChecklistPregunta) {
                ChecklistLayoutDTO objLayout = new ChecklistLayoutDTO();

                System.out.println("PREG.------>>" + preg);
                List<ArbolDecisionDTO> listaArbolPreg = mapaArbol.get("" + preg.getIdPregunta());

                objLayout.setIdConsecutivo(preg.getOrdenPregunta());
                if (preg.getCodigo() == null) {
                    objLayout.setCodigoChecklist(0);
                } else {
                    objLayout.setCodigoChecklist(preg.getCodigo());
                }
                objLayout.setNegocio(negocio);
                objLayout.setNombreProtocolo(nomProtocolo);
                objLayout.setSeccionProtocolo(mapaMod.get("" + preg.getIdModulo()).getNombre());
                objLayout.setSubSeccionProtocolo("");
                objLayout.setIdConsecutivoProtocolo(preg.getOrdenPregunta());
                //objLayout.setFactorCritico(preg.getCritica()); //Falta factor critico
                if (preg.getCritica() != null) {
                    objLayout.setFactorCritico(preg.getCritica());
                } else {
                    objLayout.setFactorCritico(0);
                }
                objLayout.setPregunta(preg.getPregunta());
                objLayout.setTipoRespuesta(preg.getIdTipoPregunta());
                if (preg.getIdTipoPregunta() == 28) {
                    for (ArbolDecisionDTO auxArbol : listaArbolPreg) {
                        if (auxArbol.getRespuesta().equals("432")) {
                            objLayout.setFlagVF(1);
                            objLayout.setOpcionesCombo("");
                        }

                        if (auxArbol.getRespuesta().equals("485")) {
                            objLayout.setFlagVF(2);
                            objLayout.setOpcionesCombo("");
                        }
                    }
                    if (objLayout.getFlagVF() == 0) {
                        String combo = "";
                        for (ArbolDecisionDTO auxArbol : listaArbolPreg) {
                            combo = combo + auxArbol.getDescResp() + ",";
                        }
                        objLayout.setOpcionesCombo(combo.substring(0, combo.length() - 1));
                    }

                } else {
                    objLayout.setOpcionesCombo("");//Faltan opciones combo
                }
                if (preg.getPregPadre() == 0) {
                    objLayout.setIdPreguntaPadre(0);
                } else {
                    objLayout.setIdPreguntaPadre(buscaPadreLayout(listaChecklistPregunta, preg.getPregPadre()));
                }

                boolean flagCombo = false;

                if (preg.getIdTipoPregunta() == 21) {
                    objLayout.setCumplimiento("SI");
                } else if (preg.getIdTipoPregunta() == 26) {
                    objLayout.setCumplimiento("SI");
                } else if (preg.getIdTipoPregunta() == 28) {
                    for (ArbolDecisionDTO auxArbol : listaArbolPreg) {
                        if (auxArbol.getRespuesta().equals("432")) {
                            objLayout.setCumplimiento("VERDADERO");
                            flagCombo = true;
                            break;
                        }
                    }

                    if (flagCombo != true) {
                        objLayout.setCumplimiento("NA");
                    }

                } else {
                    objLayout.setCumplimiento("NA");
                }

                double ponderacion = 0.0;

                if (preg.getIdTipoPregunta() == 21 || preg.getIdTipoPregunta() == 26) {

                    for (ArbolDecisionDTO auxArbol : listaArbolPreg) {
                        if (auxArbol.getRespuesta().equals("1")) {
                            ponderacion = Double.parseDouble(auxArbol.getPonderacion());
                            objLayout.setPonderacion(ponderacion);//Faltan Ponderacion
                        }
                    }

                } else {
                    objLayout.setPonderacion(0.0);//Faltan Ponderacion
                }

                objLayout.setRequiereComentarios("" + listaArbolPreg.get(0).getReqObservacion());//Faltan Comentaios
                boolean flagReqEvidencia = false;
                String reqEvidencia = "";
                String tipoEvidencia = "";
                String pantillaSi = "";
                String pantillaNo = "";

                for (ArbolDecisionDTO auxArbol : listaArbolPreg) {
                    if (auxArbol.getEstatusEvidencia() != 0) {
                        flagReqEvidencia = true;
                        if (reqEvidencia.equals("")) {
                            reqEvidencia = "1";
                        }
                        if (auxArbol.getRespuesta().equals("1") || auxArbol.getRespuesta().equals("432")) {
                            if (auxArbol.getEstatusEvidencia() == 1) {
                                tipoEvidencia = "1";
                                if (auxArbol.getReqOblig() == 1) {
                                    pantillaSi = "B";
                                } else {
                                    pantillaSi = "A";
                                }
                            }
                            if (auxArbol.getEstatusEvidencia() == 5) {
                                tipoEvidencia = "1";

                                if (auxArbol.getIdPlantilla() == 5) {
                                    pantillaSi = "C";
                                } else {
                                    pantillaSi = "B";
                                }

                            }
                            if (auxArbol.getEstatusEvidencia() == 3) {
                                tipoEvidencia = "2";
                            }
                        }
                        if (auxArbol.getRespuesta().equals("2") || auxArbol.getRespuesta().equals("433")) {
                            if (auxArbol.getEstatusEvidencia() == 1) {
                                tipoEvidencia = "1";
                                if (auxArbol.getReqOblig() == 1) {
                                    pantillaNo = "B";
                                } else {
                                    pantillaNo = "A";
                                }
                            }
                            if (auxArbol.getEstatusEvidencia() == 5) {
                                tipoEvidencia = "1";

                                if (auxArbol.getIdPlantilla() == 5) {
                                    pantillaNo = "C";
                                } else {
                                    pantillaNo = "B";
                                }

                            }
                            if (auxArbol.getEstatusEvidencia() == 3) {
                                tipoEvidencia = "2";
                            }
                        }
                    }
                }

                objLayout.setRequiereEvidencia(reqEvidencia);//Faltan Evidencia BASE
                objLayout.setTipoEvidencia(tipoEvidencia);//Faltan TIPO Evidencia BASE
                objLayout.setPlantillaSi(pantillaSi);;//Faltan TIPO Evidencia BASE
                objLayout.setPlantillaNo(pantillaNo);;//Faltan TIPO Evidencia BASE

                objLayout.setResponsable1(preg.getResponsable());
                objLayout.setResponsable2("");

                if (tipoCarga == 1) {
                    objLayout.setZona(zonaInfra);
                } else {
                    objLayout.setZona(preg.getResponsable());
                }
                objLayout.setDefinicion(preg.getDetalle());
                objLayout.setDisciplina("");
                objLayout.setSla("" + preg.getSla());

                lista.add(objLayout);
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Ocurrio Algo al retornar el layout de base: " + e.getMessage());
        }

        return lista;
    }

    public int buscaPadreLayout(List<ChecklistPreguntaDTO> objListaPreg, int idPreg) {
        int res = 0;
        try {
            for (ChecklistPreguntaDTO aux : objListaPreg) {
                if (idPreg == aux.getIdPregunta()) {
                    res = aux.getOrdenPregunta();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            res = 0;
        }
        return res;
    }

    public HashMap<String, List<ArbolDecisionDTO>> obtieneArbolesPreg(List<ArbolDecisionDTO> listaArbol) {
        HashMap<String, List<ArbolDecisionDTO>> listaArbolRetorno = new HashMap<String, List<ArbolDecisionDTO>>();
        try {
            for (ArbolDecisionDTO aux : listaArbol) {

                if (!listaArbolRetorno.containsKey("" + aux.getIdPregunta())) {
                    List<ArbolDecisionDTO> listaaux = new ArrayList<ArbolDecisionDTO>();
                    listaaux.add(aux);
                    listaArbolRetorno.put("" + aux.getIdPregunta(), listaaux);
                } else {
                    listaArbolRetorno.get("" + aux.getIdPregunta()).add(aux);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            listaArbolRetorno = null;
        }
        return listaArbolRetorno;
    }

    public boolean cargaProtocolosProduccion(ArrayList<ChecklistLayoutDTO> checkTemp, int tipoCarga, String usuario) {
        boolean res = false;
        String jsonSalida = null;
        try {
            boolean respuesta = false;
            ArrayList<PreguntaDTO> listaPreguntas = new ArrayList<PreguntaDTO>();
            ArrayList<ChecklistLayoutDTO> listaEntrante = checkTemp;
            ArrayList<ChecklistProtocoloDTO> listaProtocolos = new ArrayList<ChecklistProtocoloDTO>();

            //logger.info("El archivo: " + listaEntrante.toString());
            //System.out.println("\nTIPO DE CARGA----->: " + tipoCarga);
            // MÓDULOS
            ArrayList<ModuloDTO> listaModulos = new ArrayList<ModuloDTO>();
            listaModulos = getListaModulos(listaEntrante);

            ArrayList<ArrayList<ModuloDTO>> responseInsertaModulos = insertaModulos(listaModulos);

            ArrayList<ModuloDTO> listaModulosInsertados = responseInsertaModulos.get(1);
            ArrayList<ModuloDTO> listaModulosNoInsertados = responseInsertaModulos.get(0);

            // Se insertaron correctamente todos los módulos
            if (listaModulosNoInsertados.size() == 0 && (listaModulosInsertados.size() == listaModulos.size())) {

                // PREGUNTAS
                listaPreguntas = getListaPreguntas(listaEntrante, listaModulosInsertados, tipoCarga);

                ArrayList<ArrayList<PreguntaDTO>> responseInsertaPreguntas = insertaPreguntas(listaPreguntas, tipoCarga);
                ArrayList<PreguntaDTO> listaPreguntasInsertados = responseInsertaPreguntas.get(1);
                ArrayList<PreguntaDTO> listaPreguntasNoInsertados = responseInsertaPreguntas.get(0);

                System.out.println("PREGUNTAS INS: " + listaPreguntasNoInsertados.size());
                if (listaPreguntasNoInsertados.size() == 0
                        && (listaPreguntasInsertados.size() == listaPreguntas.size())) {

                    respuesta = true;

                } else {

                    respuesta = false;
                }

                // No se insertaron todos los módulos correctamente
            } else {
                respuesta = false;

            }

            // INSERCIÓN DE PROTOCOLOS
            ArrayList<ChecklistProtocoloDTO> listaProtocolosInsertadosTot = new ArrayList<ChecklistProtocoloDTO>();
            if (respuesta) {

                listaProtocolos = getListaProtocolos(listaEntrante, tipoCarga);

                ArrayList<ArrayList<ChecklistProtocoloDTO>> responseInsertaProtocolos = insertaInfoProtocolos(listaProtocolos, usuario);

                ArrayList<ChecklistProtocoloDTO> listaProtocolosInsertados = responseInsertaProtocolos.get(1);
                ArrayList<ChecklistProtocoloDTO> listaProtocolosNoInsertados = responseInsertaProtocolos.get(0);

                if (listaProtocolosNoInsertados.size() == 0
                        && (listaProtocolosInsertados.size() == listaProtocolos.size())) {

                    listaProtocolosInsertadosTot = listaProtocolosInsertados;

                    respuesta = true;

                } else {

                    respuesta = false;
                }

            } else {
                logger.info("NO SE INSERTA INFORMACIÓN DE PROTOCOLOS ");
            }

            // INSERCIÓN DE CHECKUSUA
            if (respuesta) {

                ArrayList<ChecklistLayoutDTO> infoLayout = setInfoLayout(listaEntrante, listaPreguntas,
                        listaProtocolos, tipoCarga);

                logger.info("infoLayout" + infoLayout.size());

                ArrayList<ArrayList<ChecklistLayoutDTO>> responseInsertaCheckusua = insertaCheckusua(infoLayout);

                ArrayList<ChecklistLayoutDTO> listaCheckusuaInsertados = responseInsertaCheckusua.get(1);
                ArrayList<ChecklistLayoutDTO> listaCheckusuaNoinsertados = responseInsertaCheckusua.get(0);

                // Se insertaron correctamente todos los módulos
                if (listaCheckusuaNoinsertados.size() == 0 && (listaCheckusuaInsertados.size() == infoLayout.size())) {
                    respuesta = true;
                    listaEntrante = responseInsertaCheckusua.get(1);
                    // No se insertaron todos los módulos correctamente
                } else {
                    respuesta = false;

                }

            } else {
                logger.info("NO SE INSERTA INFORMACIÓN DE CHECKUSUA");

            }

            // ---------ALTA ARBOL DE DESICION-------------
            if (respuesta) {

                ArrayList<ChecklistLayoutDTO> listaArbol = getListaArbol(listaEntrante, tipoCarga);

                // ArrayList<ChecklistLayoutDTO> listaArbolSalida =
                // checklistAdminBI.getListaArbol(listaEntrante, 2);
                ArrayList<ChecklistLayoutDTO> listaArbolSalida = altaArbol(listaArbol, tipoCarga);

                String preguntasError = "";
                for (ChecklistLayoutDTO layout : listaArbolSalida) {
                    if (layout.getFlagErrorArbol() != 0) {
                        preguntasError = preguntasError + layout.getIdConsecutivo() + ", ";
                    }

                }
                if (preguntasError.equals("")) {
                    jsonSalida = "{\"ALTA\":true}";
                    respuesta = true;

                } else {
                    //logger.info("PREGUNTAS CON ERROR AL CREAR ARBOL: " + preguntasError);
                    respuesta = false;

                    //HACEMOS ROLL BACK
                    boolean resp = arbolRespAdiEviAdmBI.depuraTabParam(listaProtocolos.get(0).getIdChecklist());
                    if (!resp) {
                        arbolRespAdiEviAdmBI.depuraTabParam(listaProtocolos.get(0).getIdChecklist());
                    }

                }

            } else {
                logger.info("NO SE INSERTA LOS ÁRBOLES ");

                //HACEMOS ROLL BACK
                boolean resp = arbolRespAdiEviAdmBI.depuraTabParam(listaProtocolos.get(0).getIdChecklist());
                if (!resp) {
                    arbolRespAdiEviAdmBI.depuraTabParam(listaProtocolos.get(0).getIdChecklist());
                }
            }

            //código para agregar las zonas
            if (respuesta) {

                try {
                    //PreguntaBI preguntabi = (PreguntaBI) FRQAppContextProvider.getApplicationContext().getBean("preguntaBI");

                    for (int i = 0; i < listaPreguntas.size(); i++) {
                        int idPregunta = listaPreguntas.get(i).getIdPregunta();
                        //System.out.println("ID_PREGUNTA_TEMP: " + idPregunta);

                        //listaPreguntasCorrectos.add(listaPreguntas.get(i));
                        //Se agrega el id pregunta e id zona en la tabla de Preg-Zona
                        AdmTipoZonaDTO tipoZona = new AdmTipoZonaDTO();
                        tipoZona.setIdTipoZona(tipoCarga + "");
                        ArrayList<AdmTipoZonaDTO> tipoZonaComp = admTipoZonaBI.obtieneTipoZonaById(tipoZona);
                        if (tipoZonaComp.get(0).getDescripcion().toUpperCase().contains("ASEGURAMIENTO")) {
                            //Se obtiene el catalogo de zonas para aseguramiento
                            AdminZonaDTO zona = new AdminZonaDTO();
                            zona.setIdTipo(tipoCarga + "");
                            ArrayList<AdminZonaDTO> zonaComp = admZonasBI.obtieneZonaById(zona);

                            //Se recorre el catalogo de zonas de aseguramiento
                            for (AdminZonaDTO zonaDTO : zonaComp) {
                                //System.out.println("ZONA DEL LAYOUT: " + listaPreguntas.get(i).getArea());
                                //System.out.println("ZONA DE CATALOGO: " + zonaDTO.getDescripcion());
                                if (listaPreguntas.get(i).getArea().toUpperCase().contains(zonaDTO.getDescripcion().toUpperCase())) {
                                    //logger.info("Preg: " + listaPreguntas.get(i).getArea());
                                    AdmPregZonaDTO pregZona = new AdmPregZonaDTO();
                                    pregZona.setIdPreg(idPregunta + "");
                                    pregZona.setIdZona(zonaDTO.getIdZona());
                                    respuesta = admPregZonasBI.insertaPregZona(pregZona);
                                    if (respuesta) {
                                        break;
                                    }
                                } else {
                                    logger.info("La zona no se encuentra en el catalogo de zonas");
                                }
                            }
                        } else if (tipoZonaComp.get(0).getDescripcion().toUpperCase().contains("INFRAESTRUCTURA")) {
                            //Se obtiene el catalogo de zonas para infraestructura
                            AdminZonaDTO zona = new AdminZonaDTO();
                            zona.setIdTipo(tipoCarga + "");
                            ArrayList<AdminZonaDTO> zonaComp = admZonasBI.obtieneZonaById(zona);

                            //Se recorre el catalogo de zonas de infraestructura
                            for (AdminZonaDTO zonaDTO : zonaComp) {
                                if (listaPreguntas.get(i).getArea().toUpperCase().contains(zonaDTO.getDescripcion().toUpperCase())) {
                                    //logger.info("Preg: " + listaPreguntas.get(i).getArea());
                                    AdmPregZonaDTO pregZona = new AdmPregZonaDTO();
                                    pregZona.setIdPreg(idPregunta + "");
                                    pregZona.setIdZona(zonaDTO.getIdZona());
                                    respuesta = admPregZonasBI.insertaPregZona(pregZona);
                                    if (respuesta) {
                                        break;
                                    }
                                } else {
                                    logger.info("La zona no se encuentra en el catalogo de zonas");
                                }
                            }
                        } else if (tipoZonaComp.get(0).getDescripcion().toUpperCase().contains("TRANSFORMACIÓN")) {
                            //Se obtiene el catalogo de zonas para transformacion
                            AdminZonaDTO zona = new AdminZonaDTO();
                            zona.setIdTipo(tipoCarga + "");
                            ArrayList<AdminZonaDTO> zonaComp = admZonasBI.obtieneZonaById(zona);

                            //Se recorre el catalogo de zonas de transformaciones
                            for (AdminZonaDTO zonaDTO : zonaComp) {
                                if (listaPreguntas.get(i).getArea().toUpperCase().contains(zonaDTO.getDescripcion().toUpperCase())) {
                                    //logger.info("Preg: " + listaPreguntas.get(i).getArea());
                                    AdmPregZonaDTO pregZona = new AdmPregZonaDTO();
                                    pregZona.setIdPreg(idPregunta + "");
                                    pregZona.setIdZona(zonaDTO.getIdZona());
                                    respuesta = admPregZonasBI.insertaPregZona(pregZona);
                                    if (respuesta) {
                                        break;
                                    }
                                } else {
                                    logger.info("La zona no se encuentra en el catalogo de zonas");
                                }
                            }
                        }
                    }

                    //response.add(0, listaPreguntasIncorrectos);
                    //response.add(1, listaPreguntasCorrectos);
                } catch (Exception e) {
                    logger.info("Ocurrio algo en AP al insertar PREGUNTA: " + e.getMessage());
                    //System.out.println(e.getMessage());
                    //System.out.println("AP :" + e);
                }

            }

            if (respuesta == true) {
                res = true;
                String nomProtocolo = listaEntrante.get(0).getNombreProtocolo();
                boolean resp = versionCheckAdmBI.cargaVesionesTabProd(nomProtocolo);
                for (ChecklistProtocoloDTO aux : listaProtocolosInsertadosTot) {
                    OrdenGrupoDTO grupoOrden = new OrdenGrupoDTO();
                    grupoOrden.setIdGrupo(0);
                    if (tipoCarga == 1) {
                        grupoOrden.setIdGrupo(321);
                    } else {
                        grupoOrden.setIdGrupo(323);
                    }

                    grupoOrden.setIdChecklist(aux.getIdChecklist());
                    grupoOrden.setOrden(1);
                    grupoOrden.setCommit(1);

                    int result = ordenGrupoBI.insertaGrupo(grupoOrden);

                    desactivaProtocolosAnteriores(aux.getIdChecklist());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            res = false;
        }

        return res;
    }

    // Metodo para armar un json a partir de una cadena con ids de checklist
    public ArrayList<ChecklistLayoutDTO> armaLayout(String ids, String nomProtocolo, int tipoCarga) {
        ArrayList<ChecklistLayoutDTO> res = null;
        try {

            ChecklistAdminValidaBI checklistAdminValidabi = (ChecklistAdminValidaBI) FRQAppContextProvider.getApplicationContext().getBean("checklistAdminValidaBI");

            if (ids != null) {
                if (!ids.equals("")) {
                    String[] idCheck = ids.split(",");
                    if (idCheck.length > 0) {
                        res = new ArrayList<ChecklistLayoutDTO>();
                    }

                    for (String auxCheck : idCheck) {

                        ArrayList<ChecklistLayoutDTO> layoutTMP = checklistAdminValidabi.retornaLayoutBase(Integer.parseInt(auxCheck), tipoCarga);
                        for (ChecklistLayoutDTO aux : layoutTMP) {
                            aux.setNombreProtocolo(nomProtocolo);
                            res.add(aux);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        return res;
    }

    public boolean ejecutaCargaProd() {
        boolean res = false;
        try {

            //Verifica Versiones
            List<CheckAutoProtoDTO> lista = checkAutProtoBI.obtieneAutoProto("");

            List<CheckAutoProtoDTO> listaHoy = new ArrayList<CheckAutoProtoDTO>();

            //Verifica cuales se liberan hoy
            for (CheckAutoProtoDTO auxLibera : lista) {
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("es", "ES"));
                String fecha = auxLibera.getFechaLiberacion();
                try {
                    if (fecha != null && fecha != "null") {

                        Date fechaActual = new Date();

                        String fechaSistema = sdf.format(fechaActual);

                        cal.setTime(sdf.parse(fecha));
                        Calendar c1 = Calendar.getInstance();

                        //c1.add(Calendar.MONTH, 8);
                        int dia = c1.get(Calendar.DATE);
                        int mes = c1.get(Calendar.MONTH) + 1;
                        String diaS = "";
                        if (dia < 10) {
                            diaS = "0" + Integer.toString(c1.get(Calendar.DATE));
                        } else {
                            diaS = Integer.toString(c1.get(Calendar.DATE));
                        }

                        String mesS = "";

                        if (mes < 10) {
                            mesS = "0" + Integer.toString(c1.get(Calendar.MONTH) + 1);
                        } else {
                            mesS = Integer.toString(c1.get(Calendar.MONTH) + 1);
                        }

                        String anio = Integer.toString(c1.get(Calendar.YEAR));
                        String FActual = anio + "-" + mesS + "-" + diaS + " 00:00:00";

                        c1.setTime(sdf.parse(FActual));

                        if (cal.compareTo(c1) == 0) {

                            if (auxLibera.getStatus() == 1) {
                                listaHoy.add(auxLibera);
                            }

                        } else {
                            //System.out.println("No es de hoy: "+auxLibera.getFiversion());
                        }

                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (listaHoy.size() > 0) {
                for (CheckAutoProtoDTO auxLibera : listaHoy) {
                    String vers = "" + auxLibera.getFiversion();
                    List<CheckSoporteAdmDTO> listaChecklist = checkSoporteAdmBI.checkVersParam(vers, "0");

                    if (listaChecklist.size() > 0) {
                        String nomProtocolo = listaChecklist.get(0).getNego();
                        String idChecks = "";
                        int tipoCarga = 0;
                        String nomUsuario = "";
                        for (CheckSoporteAdmDTO aux : listaChecklist) {
                            idChecks = idChecks + aux.getIdChecklist() + ",";
                            if (aux.getOrdeGrupo().equals("326")) {
                                tipoCarga = 2;
                            } else {
                                tipoCarga = 1;
                            }
                            nomUsuario = aux.getNomusu();
                        }

                        //System.out.println("IDS CHECKLIST: <<::::-----------::::>>" + idChecks.substring(0, idChecks.length() - 1));
                        ArrayList<ChecklistLayoutDTO> layout = armaLayout(idChecks.substring(0, idChecks.length() - 1), nomProtocolo, tipoCarga);
                        boolean rp = cargaProtocolosProduccion(layout, tipoCarga, nomUsuario);

                    }
                }
            } else {
                System.out.println("No existe nada por liberar");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;
    }

    private String compararFechasConDate(String fecha1, String fechaActual) {
        System.out.println(
                "Parametro String Fecha 1 = " + fecha1 + "\n" + "Parametro String fechaActual = " + fechaActual + "\n");
        String resultado = "";
        try {
            /**
             * Obtenemos las fechas enviadas en el formato a comparar
             */
            SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd", new Locale("es", "ES"));
            Date fechaDate1 = formateador.parse(fecha1);
            Date fechaDate2 = formateador.parse(fechaActual);

            //System.out.println("Parametro Date Fecha 1 = " + fechaDate1 + "\n" + "Parametro Date fechaActual = "
            //       + fechaDate2 + "\n");
            if (fechaDate1.before(fechaDate2)) {
                resultado = "La Fecha 1 es menor ";
            } else {
                if (fechaDate2.before(fechaDate1)) {
                    resultado = "La Fecha 1 es Mayor ";
                } else {
                    resultado = "Las Fechas Son iguales ";
                }
            }
        } catch (ParseException e) {
            System.out.println("Se Produjo un Error!!!  " + e.getMessage());
        }
        return resultado;
    }

    public void desactivaProtocolosAnteriores(int idChecklist) {
        //checklistBI

        //System.out.println("PROTOCOLO PARA PROD " + idChecklist);
        ChecklistBI checklistBI = (ChecklistBI) FRQAppContextProvider.getApplicationContext().getBean("checklistBI");
        List<ChecklistDTO> checklistNuevo = checklistBI.buscaChecklist(idChecklist);
        ChecklistDTO checkNuevo = null;
        int idProtocolo = 0;
        if (checklistNuevo != null && checklistNuevo.size() > 0) {
            checkNuevo = checklistNuevo.get(0);
            idProtocolo = checkNuevo.getIdProtocolo();
            //System.out.println("PROTOCOLO AGREGADO PARA DEPURAR " + idProtocolo);

            if (idProtocolo != 0) {
                List<ChecklistDTO> chksProtocolo = checklistBI.buscaChecklistByProtocolo(idProtocolo);
                if (chksProtocolo != null && chksProtocolo.size() > 0) {
                    for (ChecklistDTO chk : chksProtocolo) {
                        if (chk.getIdChecklist() != idChecklist) {
                            //Se desactiva el checklist
                            checklistBI.actualizaCheckVigente(chk.getIdChecklist(), 0);
                            //System.out.println("CHECKLIST DESACTIVADO");

                        }

                    }

                }
            }

        }

    }

    public ArrayList<ChecklistLayoutDTO> leerExcellMejorado(File file) {
        ArrayList<ChecklistLayoutDTO> lista = new ArrayList<ChecklistLayoutDTO>();

        try (XSSFWorkbook worbook = new XSSFWorkbook(file)) {
            // obtener la hoja que se va leer
            XSSFSheet sheet = worbook.getSheetAt(0);
            int filas = 0;
            for (Row row : sheet) {
                ChecklistLayoutDTO obj = new ChecklistLayoutDTO();
                filas++;
                int column = 0;
                for (Cell cell : row) {
                    column++;
                    // calcula el resultado de la formula
                    cell.setCellType(CellType.STRING);
                    String format = cell.getStringCellValue();

                    if (column == 1 && format.equals("")) {
                        break;
                    }

                    if (filas != 1) {
                        int dato = 0;
                        switch (column) {
                            case 1:

                                if (format.contains(".")) {
                                    dato = (int) Double.parseDouble(format);
                                } else {
                                    dato = Integer.parseInt(format);
                                }

                                obj.setIdConsecutivo(dato);
                                break;
                            case 2:
                                if (format.contains(".")) {
                                    dato = (int) Double.parseDouble(format);
                                } else {
                                    dato = Integer.parseInt(format);
                                }
                                obj.setCodigoChecklist(dato);
                                break;
                            case 3:
                                obj.setNegocio(format);
                                break;
                            case 4:
                                obj.setNombreProtocolo(format);
                                break;
                            case 5:
                                obj.setSeccionProtocolo(format);
                                break;
                            case 6:
                                obj.setSubSeccionProtocolo(format);
                                break;
                            case 7:
                                if (format.contains(".")) {
                                    dato = (int) Double.parseDouble(format);
                                } else {
                                    dato = Integer.parseInt(format);
                                }
                                obj.setIdConsecutivoProtocolo(dato);
                                break;
                            case 8:
                                int critico = 0;
                                if (format.toLowerCase().contains("imperdonable")) {
                                    critico = 1;
                                }
                                if (format.toLowerCase().contains("crítico")) {
                                    critico = 2;
                                }
                                if (format.toLowerCase().contains("alto")) {
                                    critico = 2;
                                }
                                if (format.toLowerCase().contains("medio")) {
                                    critico = 3;
                                }
                                if (format.toLowerCase().contains("bajo")) {
                                    critico = 4;
                                }
                                obj.setFactorCritico(critico);
                                break;
                            case 9:
                                obj.setPregunta(format);
                                break;
                            case 10:
                                if (!format.equals("")) {
                                    if (format.contains(".")) {
                                        dato = (int) Double.parseDouble(format);
                                    } else {
                                        dato = Integer.parseInt(format);
                                    }
                                }
                                obj.setIdPreguntaPadre(dato);
                                break;
                            case 11:
                                int tipoPreg = 0;
                                if (format.equalsIgnoreCase("si-no")) {
                                    tipoPreg = 21;
                                }
                                if (format.equalsIgnoreCase("si-no-n/e")) {
                                    tipoPreg = 26;
                                }
                                if (format.equalsIgnoreCase("si - no - ne")) {
                                    tipoPreg = 26;
                                }
                                if (format.equalsIgnoreCase("si-no-ne")) {
                                    tipoPreg = 26;
                                }
                                if (format.equalsIgnoreCase("si-no-na")) {
                                    tipoPreg = 26;
                                }
                                if (format.equalsIgnoreCase("ok")) {
                                    tipoPreg = 28;
                                    obj.setOpcionesCombo("OK");
                                    obj.setFlagVF(2);
                                }
                                if (format.equalsIgnoreCase("v-f")) {
                                    tipoPreg = 28;
                                    obj.setFlagVF(1);
                                }

                                if (format.equalsIgnoreCase("verdadero - falso")) {
                                    tipoPreg = 28;
                                    obj.setFlagVF(1);
                                }

                                if (format.equalsIgnoreCase("texto")) {
                                    tipoPreg = 24;
                                }
                                if (format.equalsIgnoreCase("numérica")) {
                                    tipoPreg = 23;
                                }
                                if (format.equalsIgnoreCase("combo")) {
                                    tipoPreg = 28;
                                    obj.setFlagVF(0);
                                }

                                obj.setTipoRespuesta(tipoPreg);
                                break;
                            case 12:
                                obj.setOpcionesCombo(format);
                                break;
                            case 13:
                                obj.setCumplimiento(format);
                                break;
                            case 14:
                                if (format.contains("%")) {
                                    format = format.replace("%", "");
                                }
                                if (format.contains(",")) {
                                    format = format.replace(",", ".");
                                }
                                if (format.contains("NA")) {
                                    format = format.replace("NA", "0.0");
                                }
                                obj.setPonderacion(Double.parseDouble(format));
                                break;
                            case 15:
                                int comentarios = 0;
                                if (format.toLowerCase().contains("si")) {
                                    comentarios = 1;
                                }
                                obj.setRequiereComentarios("" + comentarios);
                                break;
                            case 16:
                                int evid = 0;
                                if (format.toLowerCase().contains("si")) {
                                    evid = 1;
                                }
                                obj.setRequiereEvidencia("" + evid);
                                break;
                            case 17:
                                int tipoEvid = 0;
                                if (format.toLowerCase().contains("foto")) {
                                    tipoEvid = 1;
                                }
                                if (format.toLowerCase().contains("video")) {
                                    tipoEvid = 2;
                                }
                                obj.setTipoEvidencia("" + tipoEvid);
                                break;
                            case 18:
                                obj.setPlantillaSi(format);
                                break;
                            case 19:
                                obj.setPlantillaNo(format);
                                break;
                            case 20:
                                obj.setResponsable1(format);
                                break;
                            case 21:
                                obj.setResponsable2(format);
                                break;
                            case 22:
                                obj.setZona(format);
                                System.out.println("COLUMNA DE ZONA: " + format);
                                break;
                            case 23:
                                obj.setDefinicion(format);
                                break;
                            case 24:
                                obj.setDisciplina(format);
                                break;
                            case 25:
                                obj.setSla(format);
                                break;
                            default:
                                break;

                        }
                    }

                } // Cierra for cell

                if (filas != 1) {

                    if (obj.getPregunta() != null) {
                        lista.add(obj);
                    }

                }

            } // Cierra for row

        } catch (Exception e) {
            logger.info("Ocurrio algo en LeerExcell: " + e.getMessage());
            return null;
        }
        return lista;
    }

}
