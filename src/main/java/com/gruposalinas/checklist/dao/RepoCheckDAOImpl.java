package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.RepoCheckDTO;
import com.gruposalinas.checklist.mappers.RepoCheckRowMapper;
import com.gruposalinas.checklist.mappers.RepoCheckRowMapper2;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class RepoCheckDAOImpl extends DefaultDAO implements RepoCheckDAO {

    private static Logger logger = LogManager.getLogger(RepoCheckDAOImpl.class);

    DefaultJdbcCall jdbcObtieneUsuaCheck;
    DefaultJdbcCall jdbcObtieneDetalleEmp;

    public void init() {

        // obtiene Informacion de Checklist del usuario
        jdbcObtieneUsuaCheck = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOCHECK")
                .withProcedureName("SP_PROTOCOLOS")
                .returningResultSet("CUR", new RepoCheckRowMapper());

        //obtiene detalle del usaurio nombre puesto ceco
        jdbcObtieneDetalleEmp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOCHECK")
                .withProcedureName("SP_PROTOCOLOS")
                .returningResultSet("CUR_USU", new RepoCheckRowMapper2());

    }

    //Checklist
    @SuppressWarnings("unchecked")
    public List<RepoCheckDTO> obtieneInfo(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        List<RepoCheckDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDEMP", idUsuario);

        out = jdbcObtieneUsuaCheck.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPOCHECK.SP_PROTOCOLOS}");

        listaFijo = (List<RepoCheckDTO>) out.get("CUR");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener los Checklist del Empleado ");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //parametro

    //Detalle Empleado
    @SuppressWarnings("unchecked")
    public List<RepoCheckDTO> obtieneDatos(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        List<RepoCheckDTO> lista = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDEMP", idUsuario);

        out = jdbcObtieneDetalleEmp.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPOCHECK.SP_PROTOCOLOS}");

        lista = (List<RepoCheckDTO>) out.get("CUR_USU");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener los Detalles del EMPLEADO");
        } else {
            return lista;
        }

        return lista;

    }

}
