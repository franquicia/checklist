package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.EstadosDAO;
import com.gruposalinas.checklist.domain.EstadosDTO;

public class EstadosBI {

	private static Logger logger = LogManager.getLogger(EstadosBI.class);

private List<EstadosDTO> listafila;
	
	@Autowired
	EstadosDAO estadosDAO;
		
	
	public int inserta(EstadosDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = estadosDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean elimina(String idEstado){
		boolean respuesta = false;
		
		try {
			respuesta = estadosDAO.elimina(idEstado);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<EstadosDTO> obtieneDatos( int idEstado) {
		
		try {
			listafila =  estadosDAO.obtieneDatos(idEstado);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<EstadosDTO> obtieneInfo() {

		try {
			listafila = estadosDAO.obtieneInfo();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de la tabla estados");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}

	public List<EstadosDTO> obtieneDetalle() {

		try {
			listafila = estadosDAO.obtieneDetalle();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de la tabla estados");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}

	
	public boolean actualiza(EstadosDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = estadosDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	

}