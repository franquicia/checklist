package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.SupervisorDTO;

public class SupervisorRowMapper implements RowMapper<SupervisorDTO> {

	public SupervisorDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		SupervisorDTO supervisorDTO = new SupervisorDTO();
		
		supervisorDTO.setNomSupervisor(rs.getString("Valor"));
	
		return supervisorDTO;
	}

}
