package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.PreguntaCheckDTO;

public class PreguntaCheckImpConcatRowMapper implements RowMapper<ChecklistPreguntasComDTO> {

	public ChecklistPreguntasComDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		

		ChecklistPreguntasComDTO com = new ChecklistPreguntasComDTO();

        com.setIdBita(rs.getInt("FIID_BITACORA"));
	    com.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    com.setIdArbol(rs.getInt("FIID_ARBOL_DES"));
	    com.setObserv(rs.getString("OBSERVACIONES"));
	    com.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    com.setIdCheck(rs.getInt("FIID_CHECKLIST"));
	    com.setIdPosible(rs.getInt("FCRESPUESTA"));
	    com.setIdObserv(rs.getInt("FIREQOBS"));
	    com.setPosible(rs.getString("POSIBLE"));
	    com.setPregunta(rs.getString("PREGUNTA"));   
	    com.setIdcritica(rs.getInt("FICRITICA"));
	    com.setClasif(rs.getString("FCCLASIFICA"));
	     com.setSla(rs.getString("FCSLA"));
	     com.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
	    
		return com;
	}
}



