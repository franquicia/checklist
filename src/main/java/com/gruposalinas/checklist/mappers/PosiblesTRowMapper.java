package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.PosiblesDTO;

public class PosiblesTRowMapper implements RowMapper<PosiblesDTO>   {
	public PosiblesDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		PosiblesDTO posiblesDTO = new PosiblesDTO();
		
		posiblesDTO.setIdPosible(rs.getInt("FIIDPOSIBLE"));
		posiblesDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
		posiblesDTO.setNumeroRevision(rs.getString("PA_NUM_REV"));
		posiblesDTO.setTipoCambio(rs.getString("PA_TIPO_MOD"));
		
		return posiblesDTO;
	}
}
