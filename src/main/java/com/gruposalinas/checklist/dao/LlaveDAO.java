package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.LlaveDTO;

public interface LlaveDAO {
	

	public boolean insertaLllave(LlaveDTO bean)throws Exception;
	
	public boolean actualizaLlave(LlaveDTO bean)throws Exception;
	
	public boolean eliminaLlave(int idLlave)throws Exception;
	
	public List<LlaveDTO> obtieneLlave() throws Exception;
	
	public List<LlaveDTO> obtieneLlave(int idLlave) throws Exception;

}
