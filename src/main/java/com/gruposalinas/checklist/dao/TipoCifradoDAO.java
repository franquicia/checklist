package com.gruposalinas.checklist.dao;

import java.util.List;
import com.gruposalinas.checklist.domain.TipoCifradoDTO;

public interface TipoCifradoDAO {

	public boolean insertaTipoCifrado(TipoCifradoDTO bean) throws Exception;

	public boolean actualizaTipoCifrado(TipoCifradoDTO bean) throws Exception;

	public boolean eliminaTipoCifrado(int bean) throws Exception;

	public List<TipoCifradoDTO> buscaTipoCifrado(TipoCifradoDTO bean) throws Exception;
}
