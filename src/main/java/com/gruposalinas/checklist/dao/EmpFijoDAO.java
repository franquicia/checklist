package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.EmpFijoDTO;
import java.util.List;

public interface EmpFijoDAO {

    public int inserta(EmpFijoDTO bean) throws Exception;

    public boolean elimina(String idEmpFijo) throws Exception;

    public List<EmpFijoDTO> obtieneDatos(int idUsuario) throws Exception;

    public List<EmpFijoDTO> obtieneInfo() throws Exception;

    public boolean actualiza(EmpFijoDTO bean) throws Exception;

}
