package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;


public interface ChecklistDAO {
	
	public List<ChecklistDTO> buscaChecklistActivos(int noUsuario, double latitud, double longitud) throws Exception;
	
	public Map<String, Object> buscaChecklistCompleto(int idCheckUsua) throws Exception;
	
	public List<CompromisoDTO> compromisosChecklist(int idChecklist, int idSucursal) throws Exception;
	
	public List<CompromisoDTO> compromisosChecklistWeb(int idBitacora) throws Exception;

	//public List<ChecklistDTO>  buscaResumenCheck(int idUsuario) throws Exception;

	public List<ChecklistDTO>  buscaResumenCheck() throws Exception;
	
	//public List<ChecklistDTO> buscaChecklistCompletoPrueba(int noUsuario, int idCheck, double latitud, double longitud) throws Exception;
	public List<ChecklistDTO> buscaChecklistCompletoPrueba(int noUsuario, int idCheck, String latitud, String longitud) throws Exception;

	public int insertaChecklist(ChecklistDTO bean) throws Exception;
	
	//ponderacion total
	public int insertaChecklistCom(ChecklistDTO bean) throws Exception;

	public boolean actualizaChecklist(ChecklistDTO bean) throws Exception;
	
	//ponderacion total
	public boolean actualizaChecklistCom(ChecklistDTO bean) throws Exception;
	
	public boolean actualizaFechatermino(String fechaTermino, int idBitacora, String preCalificacion, String calificacion) throws Exception;
	
	public boolean actualizaCheckVigente(int idCheck, int estatus) throws Exception;
	
	public boolean eliminaChecklist(int idCheckList) throws Exception;
	
	public List<ChecklistDTO> buscaChecklist() throws Exception;
	
	public List<ChecklistDTO> buscaChecklistSoporte() throws Exception;
	
	public List<ChecklistDTO> buscaChecklist(int idChecklist) throws Exception;
	
	public List<ChecklistDTO> buscaTChecklist(int idTipoCheck) throws Exception;
	
	public boolean asignaChecklist(String ceco, int puesto, int idChecklist )throws Exception;
	
	public boolean registraChecklist(String checklist, String preguntas, String arbolDesiciones) throws Exception;
	
	//public List<ChecklistDTO> validaCheckUsua (int checkUsua,  double latitud, double longitud) throws Exception;
	public List<ChecklistDTO> validaCheckUsua (int checkUsua,  String latitud, String longitud) throws Exception;
	
	public Map<String, Object> ReportePila(int idUsuario) throws Exception;
        
        List<ChecklistDTO> buscaChecklistByProtocolo(int idProtocolo) throws Exception ;
}
