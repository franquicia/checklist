<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="../css/bootstrap/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap-table/bootstrap-table.min.css">

	<script	src="../js/jquery/jquery-2.2.4.min.js"></script>
	<script	src="../js/bootstrap.min.js"></script>
	<script	src="../js/bootstrap-table.min.js"></script>

	<c:choose>
		<c:when test="${isGCC=='si'}">
			<title>Soporte - Lista de Sucursales GCC</title>
		</c:when>
		<c:otherwise>
			<title>Soporte - Lista de Sucursales</title>
		</c:otherwise>
	</c:choose>

	<script src="../js/bootstrap-table-es-MX.min.js"></script>
	
</head>
<body>

	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuSoporte.jsp" flush="true">
		<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuSoporte.jsp" />
	</tiles:insertTemplate>

	<div id="container">
		<div id="container">
			<br>
			<c:choose>
				<c:when test="${typeList=='sucursalGCC'}">
					<p class="text-center lead"><strong>Lista de Sucursales GCC</strong></p>	
				</c:when>
				<c:when test="${typeList=='sucursal'}">
					<p class="text-center lead"><strong>Lista de Sucursales</strong></p>
				</c:when>
				<c:otherwise>
					<p class="text-center lead"><strong>Lista de Checklist Usuario</strong></p>	
				</c:otherwise>
			</c:choose>
			<br>
		</div>

		<c:choose>
			<c:when test="${typeList=='checkUsu'}">
				<div class="container">
					<table	class="table table-striped table-bordered table-hover table-condensed" 
							data-toggle="table" 
							data-sort-name="idCheckUsuario" 
							data-sort-order="asc"
							data-pagination="true"
							data-pagination-loop="true"
							data-page-size="25"
							data-height="540"
							data-smart-display="true"
							data-pagination-v-align="top"
							data-silent-sort="false"
							data-search="true"
							data-show-refresh="true"
							data-show-toggle="true"
							data-show-columns="true"
							data-show-pagination-switch="true">
						<thead>
							<tr>
								<th data-formatter="counter">#</th>
								<th data-field="idCheckUsuario" data-sortable="true" data-searchable="true">ID Checklist Usuario</th>
								<th data-field="idChecklist" data-sortable="true" >ID Checklist</th>
								<th data-field="idUsuario" data-sortable="true" data-searchable="true">ID Usuario</th>
								<th data-field="idCeco" data-sortable="true" data-searchable="true">ID CECO</th>
								<th data-field="activo" data-sortable="true" >ID Activo</th>
								<th data-field="fechaIni" data-sortable="true" >Fecha Ini</th>
								<th data-field="fechaResp" data-sortable="true" >Fecha Resp</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="list" items="${list}">
								<tr>
									<td></td>
									<td><c:out value="${list.idCheckUsuario}"></c:out></td>
									<td><c:out value="${list.idChecklist}"></c:out></td>
									<td><c:out value="${list.idUsuario}"></c:out></td>
									<td><c:out value="${list.idCeco}"></c:out></td>
									<td><c:out value="${list.activo}"></c:out></td>
									<td><c:out value="${list.fechaIni}"></c:out></td>
									<td><c:out value="${list.fechaResp}"></c:out></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:when>
			<c:otherwise>
				<div class="container">
					<table	class="table table-striped table-bordered table-hover table-condensed" 
							data-toggle="table" 
							data-sort-name="idSucursal" 
							data-sort-order="asc"
							data-pagination="true"
							data-pagination-loop="true"
							data-page-size="25"
							data-height="540"
							data-smart-display="true"
							data-pagination-v-align="top"
							data-silent-sort="false"
							data-search="true"
							data-show-refresh="true"
							data-show-toggle="true"
							data-show-columns="true"
							data-show-pagination-switch="true">
						<thead>
							<tr>
								<th data-formatter="counter">#</th>
								<th data-field="idSucursal" data-sortable="true" >ID Sucursal</th>
								<th data-field="idPais" data-sortable="true" >País</th>
								<th data-field="idCanal" data-sortable="true" >Canal</th>
								<th data-field="idCeco" data-sortable="true" >CECO</th>
								<th data-field="nuSucursal" data-sortable="true" data-searchable="true">Número de Sucursal</th>
								<th data-field="nombresuc" data-sortable="true" >Nombre de Sucursal</th>
								<th data-field="latitud" data-sortable="true" >Latitud</th>
								<th data-field="longitud" data-sortable="true" >Longitud</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="list" items="${list}">
								<tr>
									<td></td>
									<td><c:out value="${list.idSucursal}"></c:out></td>
									<td><c:out value="${list.idPais}"></c:out></td>
									<td><c:out value="${list.idCanal}"></c:out></td>
									<td><c:out value="${list.idCeco}"></c:out></td>
									<td><c:out value="${list.nuSucursal}"></c:out></td>
									<td><c:out value="${list.nombresuc}"></c:out></td>
									<td><c:out value="${list.latitud}"></c:out></td>
									<td><c:out value="${list.longitud}"></c:out></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:otherwise>
		</c:choose>

	</div>
	
	<br>
	<br>

	<script>
		function counter(value, row, index) {
			return index;
		}
	</script>
</body>
</html>