package com.gruposalinas.checklist.domain;

public class RespuestaDTO {

	private int idRespuesta;
	private int idCheckUsuario;
	private int idBitacora;
	private int idArboldecision;
    private CompromisoDTO compromiso;
    private EvidenciaDTO evidencia;
    private int commit;
    private String observacion;
    private int idTipoRespuesta;
    private int idPregunta;
    private String fechaModificacion;
	
    private String respuestaAbierta;
	
	public int getIdRespuesta() {
		return idRespuesta;
	}
	public void setIdRespuesta(int idRespuesta) {
		this.idRespuesta = idRespuesta;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public int getIdArboldecision() {
		return idArboldecision;
	}
	public void setIdArboldecision(int idArboldecision) {
		this.idArboldecision = idArboldecision;
	}
	public int getIdCheckUsuario() {
		return idCheckUsuario;
	}
	public void setIdCheckUsuario(int idCheckUsuario) {
		this.idCheckUsuario = idCheckUsuario;
	}
	public EvidenciaDTO getEvidencia() {
		return evidencia;
	}
	public void setEvidencia(EvidenciaDTO evidencia) {
		this.evidencia = evidencia;
	}
	public CompromisoDTO getCompromiso() {
		return compromiso;
	}
	public void setCompromiso(CompromisoDTO compromiso) {
		this.compromiso = compromiso;
	}
	public int getCommit() {
		return commit;
	}
	public void setCommit(int commit) {
		this.commit = commit;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public int getIdTipoRespuesta() {
		return idTipoRespuesta;
	}
	public void setIdTipoRespuesta(int idTipoRespuesta) {
		this.idTipoRespuesta = idTipoRespuesta;
	}
	public String getRespuestaAbierta() {
		return respuestaAbierta;
	}
	public void setRespuestaAbierta(String respuestaAbierta) {
		this.respuestaAbierta = respuestaAbierta;
	}
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	@Override
	public String toString() {
		return "RespuestaDTO [idRespuesta=" + idRespuesta + ", idCheckUsuario=" + idCheckUsuario + ", idBitacora="
				+ idBitacora + ", idArboldecision=" + idArboldecision + ", compromiso=" + compromiso + ", evidencia="
				+ evidencia + ", commit=" + commit + ", observacion=" + observacion + ", idTipoRespuesta="
				+ idTipoRespuesta + ", idPregunta=" + idPregunta + ", fechaModificacion=" + fechaModificacion
				+ ", respuestaAbierta=" + respuestaAbierta + "]";
	}
	
	
	
}
