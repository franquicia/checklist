package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.AdmFirmasCatDTO;

public class FirmasCatalogoRowMapper implements RowMapper<AdmFirmasCatDTO> {

	public AdmFirmasCatDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AdmFirmasCatDTO AdmFirmasCatDTO = new AdmFirmasCatDTO();
		
		AdmFirmasCatDTO.setIdFirmaCatalogo(rs.getInt("FIID_FIRM"));
	    AdmFirmasCatDTO.setNombreFirmaCatalogo(rs.getString("FCNOMBRE"));
	    AdmFirmasCatDTO.setTipoProyecto(rs.getInt("FIIDTIPOPROY"));
	    AdmFirmasCatDTO.setAux2(rs.getString("FCAUX"));
	    AdmFirmasCatDTO.setPeriodo(rs.getString("FCPERIODO"));
	    

		return AdmFirmasCatDTO;
	}

}


