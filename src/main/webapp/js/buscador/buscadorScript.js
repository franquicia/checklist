/*SCRIPT UTILIZADO POR EL BUSCADOR*/

/*Document Ready Function, sirve para la busqueda de autocompletar*/
function init() {
	$('#w-input-search').autocomplete({
		serviceUrl : urlServer + '../getTags.json',
		paramName : "tagName",
		delimiter : ",",
		transformResult : function(response) {
			return {
				// must convert json to javascript object before process
				suggestions : $.map($.parseJSON(response), function(item) {

					return {
						value : item.tagName,
						data : item.descripcion,
						recurso : item.recurso
					};
				})

			};

		}
	});

	$('#w-input-search').keypress(function(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == '13') {
			muestraSeleccion();
		}
		event.stopPropagation();
	});
}

/*
 * OBTIENE EL TERMINO SELECCIONADO, LO MUESTRA EN UN DIV Y MUESTRA EL RESULTADO
 * EN LA PARTE DE ABAJO LLAMANDO A LA FUNCION functionAjax
 */
function muestraSeleccion() {
	var termino = document.getElementById("w-input-search").value;
	getResultado(termino);
}

/*
 * OBTIENE EL RESULTADO DE LA BUSQUEDA DE LOS TERMINOS RELACIONADOS CON EL
 * TERMINO INGRESADO
 */
function getResultado(termino) {
	$
			.ajax({
				type : "GET",
				url : urlServer + '../ajaxtest.json?termino='
						+ termino,
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				paramName : "tagName",
				delimiter : ",",
				success : function(json) {
					document.getElementById("containerResultadosBusqueda").innerHTML = "";
					var jsonC = JSON.stringify(json);
					// var parseJson = JSON.parse(jsonC);
					var i = 0;
					if (json[i] == null) {
						$("#containerResultadosBusqueda").append(
								"<div>SIN RESULTADOS!!!</div><br>");
					}
					while (json[i] != null) {
						$("#containerResultadosBusqueda").append(
			"<div id='recursoBusqueda'><a id='linkRecurso' href='muestraDoc.htm?recurso="+json[i].recurso+"'>"
										+ json[i].tagName + "</a>"
										+ json[i].descripcion + "</div><br>");
						i++;
					}
				},
				error : function() {
					alert('Algo paso');
				}
			});
}
