package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.DistribCecoDTO;

public interface DistribCecoDAO {
	
	public int insertaDistrib() throws Exception;
	
	public  List<DistribCecoDTO> obtieneCeco(DistribCecoDTO bean ) throws Exception;
	
	public boolean EliminaDistrib(String ceco) throws Exception;

}
