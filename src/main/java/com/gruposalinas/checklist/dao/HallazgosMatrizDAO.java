package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.HallazgosExpDTO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;


public interface HallazgosMatrizDAO {

	  
		
		public List<HallazgosTransfDTO> obtieneHallazgoMatriz(String tipoProy) throws Exception; 
			
		public int insertaHallazgoMatriz(HallazgosTransfDTO bean)throws Exception;
		
		public boolean actualizaHallazgoMatriz(HallazgosTransfDTO bean)throws Exception;
		
		public boolean eliminaHallazgoMatriz(String idMatriz)throws Exception;
		
		
	}