package com.gruposalinas.checklist.domain;

public class RecursoPerfilDTO {

	private String idRecurso;
	private String idPerfil;
	private int consulta;
	private int inserta;
	private int modifica;
	private int elimina;
	public String getIdRecurso() {
		return idRecurso;
	}
	public void setIdRecurso(String idRecurso) {
		this.idRecurso = idRecurso;
	}
	public String getIdPerfil() {
		return idPerfil;
	}
	public void setIdPerfil(String idPerfil) {
		this.idPerfil = idPerfil;
	}
	public int getConsulta() {
		return consulta;
	}
	public void setConsulta(int consulta) {
		this.consulta = consulta;
	}
	public int getInserta() {
		return inserta;
	}
	public void setInserta(int inserta) {
		this.inserta = inserta;
	}
	public int getModifica() {
		return modifica;
	}
	public void setModifica(int modifica) {
		this.modifica = modifica;
	}
	public int getElimina() {
		return elimina;
	}
	public void setElimina(int elimina) {
		this.elimina = elimina;
	}
	
	
	
}
