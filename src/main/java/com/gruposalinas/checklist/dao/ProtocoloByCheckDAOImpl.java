package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ProtocoloByCheckDTO;
import com.gruposalinas.checklist.mappers.ProtocoloByCheckRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ProtocoloByCheckDAOImpl extends DefaultDAO implements ProtocoloByCheckDAO {

    private static final Logger logger = LogManager.getLogger(ProtocoloByCheckDAOImpl.class);

    private DefaultJdbcCall jdbcProtocolo;

    public void init() {

        jdbcProtocolo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE_MEDICION")
                .withProcedureName("SPGETPROTOCOLO")
                .returningResultSet("RCL_PROTOCOLO", new ProtocoloByCheckRowMapper());

    }

    @Override
    public List<ProtocoloByCheckDTO> obtieneProtocolo(int idChecklist) throws Exception {
        Map<String, Object> out = null;
        List<ProtocoloByCheckDTO> protocolosByCheck = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_CHECKLIST", idChecklist);

        out = jdbcProtocolo.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAREPORTE_MEDICION.SPGETPROTOCOLO}");

        protocolosByCheck = (List<ProtocoloByCheckDTO>) out.get("RCL_PROTOCOLO");

        if (protocolosByCheck == null) {
            throw new Exception("El Store Procedure retorno protocolosByCheck vacio");
        }
        return protocolosByCheck;

    }

}
