package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.VersionChecklistGralDAO;
import com.gruposalinas.checklist.domain.VersionChecklistGralDTO;

public class VersionChecklistGralBI {

	private static Logger logger = LogManager.getLogger(VersionChecklistGralBI.class);

private List<VersionChecklistGralDTO> listafila;
	
	@Autowired
	VersionChecklistGralDAO versionChecklistGralDAO;
		
	
	public int inserta(VersionChecklistGralDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = versionChecklistGralDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean elimina(String idTab){
		boolean respuesta = false;
		
		try {
			respuesta = versionChecklistGralDAO.elimina(idTab);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	
	
	public List<VersionChecklistGralDTO> obtieneDatos(int idVers) {

		try {
			listafila = versionChecklistGralDAO.obtieneDatos(idVers);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}
	
	public List<VersionChecklistGralDTO> obtieneInfo() {

		try {
			listafila = versionChecklistGralDAO.obtieneInfo();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	
	public boolean actualiza(VersionChecklistGralDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = versionChecklistGralDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public boolean actualiza2(VersionChecklistGralDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = versionChecklistGralDAO.actualiza2(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	

}