package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.FirmaCheckDAO;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;

public class FirmaCheckBI {

//x-X 2	
private static Logger logger = LogManager.getLogger(FirmaCheckBI.class);

private List<FirmaCheckDTO> listafila;
	
	@Autowired
	FirmaCheckDAO firmaCheckDAO;
		
	
	public int inserta(FirmaCheckDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = firmaCheckDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	public int insertaN(FirmaCheckDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = firmaCheckDAO.insertaN(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	public boolean elimina(String idFirma){
		boolean respuesta = false;
		
		try {
			respuesta = firmaCheckDAO.elimina(idFirma);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<FirmaCheckDTO> obtieneDatos( String ceco) {
		
		try {
			listafila =  firmaCheckDAO.obtieneDatos(ceco);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<FirmaCheckDTO> obtieneInfo() {

		try {
			listafila = firmaCheckDAO.obtieneInfo();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	
	public boolean actualiza(FirmaCheckDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = firmaCheckDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<FirmaCheckDTO> obtieneDatosFirmaCecoFaseProyecto(String ceco,String fase,String proyecto) {
		
		try {
			listafila =  firmaCheckDAO.obtieneDatosFirmaCecoFaseProyecto(ceco,fase,proyecto);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}

}