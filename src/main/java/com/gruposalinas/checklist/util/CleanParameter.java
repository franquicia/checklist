package com.gruposalinas.checklist.util;

import java.io.UnsupportedEncodingException;

public class CleanParameter {
	
	public static String cleanUrlParameter(String parameter){
		if(parameter != null){
			if (parameter.matches("\\d{4}/\\d{2}/\\d{2}") || parameter.matches("\\d{2}/\\d{2}/\\d{4}")){
				parameter = parameter.replaceAll(":", "").replaceAll("\\\\", "").replaceAll("\\.", "");
				parameter = parameter.replaceAll("%2e", "").replaceAll("%2E", "").replaceAll("%2f", "").replaceAll("%2F", "").replaceAll("%5c", "").replaceAll("%5C", "");
			} else {
				parameter = parameter.replaceAll(":", "").replaceAll("/", "").replaceAll("\\\\", "").replaceAll("\\.", "");
				parameter = parameter.replaceAll("%2e", "").replaceAll("%2E", "").replaceAll("%2f", "").replaceAll("%2F", "").replaceAll("%5c", "").replaceAll("%5C", "");
			}
		}
		
		return parameter;
	}
	
	public String cleanParameter (String parameter){
		return cleanUrlParameter(parameter);
	}
	
	public static void main (String[]args){
		String q = "¿Cómo se llama esto 100%?";
		
		try {
			String l = new String(q.getBytes("ISO-8859-1"), "UTF-8");
			//System.out.println(l);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
