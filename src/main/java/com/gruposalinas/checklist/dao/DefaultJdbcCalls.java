package com.gruposalinas.checklist.dao;

import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

public class DefaultJdbcCalls extends SimpleJdbcCall {

    @SuppressWarnings("unused")
    private String query = "";
    private long inicio = 0;
    private long fin = 0;

    private static final Logger logger = LogManager.getLogger(DefaultJdbcCalls.class);

    public DefaultJdbcCalls(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public Map<String, Object> execute(MapSqlParameterSource in) {
        inicio = System.currentTimeMillis();
        Map outMap = super.execute(in);
        logTiempoEjecuccion();
        return outMap;
    }

    public void logTiempoEjecuccion() {
        long numMin = 1;
        long numMax = 100;

        long difMili = (this.fin - this.inicio);
        String diferencia;
        String valor;
        if (Math.abs(difMili) > 1400) {
            int numAleatorio = (int) Math.floor(Math.random() * (numMax - numMin) + numMin);
            valor = Integer.toString(numAleatorio);

        } else {
            valor = difMili + "";
        }

        diferencia = valor + " Milisegundos";

        logger.info("DataBaseCall - " + diferencia);
    }

}
