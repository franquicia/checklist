$body = $("body");
$(document).on({
    ajaxStart: function() {$body.addClass("loading");},
    ajaxStop: function() {$body.removeClass("loading");}    
});

$(document).on("click",function(e) {
    var container = $("#unicaTabla");          
       if (!container.is(e.target) && container.has(e.target).length === 0) { 
    	   menuTiempoRealDesaparece(1);
       }
});

var modal = 2;
var valida=-1;
var lugarPaisL="";
var validaCheck=-1;
var nombreCecoPasaL="";
var porcentajeL=0;
var banTiempo = 0;
var intervalo=null;
var intervaloDespliegue=null;
var urlCache = "";

$(window).load(function() {
	fecha();
	document.getElementById("tipoModal").className="modal1";
	intervalo = setInterval("reporte()",300000);
});

function menuTiempoReal(){
	if(banTiempo == 0){
		$("#idMenuTiempoReal").css('display', 'inline-block');
		banTiempo = 1;
	}else{
		menuTiempoRealDesaparece(1);
	}
}

function menuTiempoRealDesaparece(valor){
	if(valor==1){
		$("#idMenuTiempoReal").css('display', 'none');
		banTiempo = 0;
	}else{
		banTiempo = 1;
	}
}

function activaCarga(){
	document.getElementById("tipoModal").className="modal3";
}

function reporte(){
		
	modal = 1;
	document.getElementById("tipoModal").className="modal3";

	$.ajax({
		type : "GET",
		url :  "/checklist/central/vistaChecklistFecha.htm?idUsuario=147247&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno="+seleccionAnoL+"&seleccionMes="+seleccionMesL+"&seleccionCanal="+seleccionCanalL+"&seleccionPais=21",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			location.href = "/checklist/central/vistaChecklistFecha.htm?idUsuario=147247&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno="+seleccionAnoL+"&seleccionMes="+seleccionMesL+"&seleccionCanal="+seleccionCanalL+"&seleccionPais=21";
		},
		error : function() {
			location.href = "/checklist/central/vistaChecklistFecha.htm?idUsuario=147247&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno="+seleccionAnoL+"&seleccionMes="+seleccionMesL+"&seleccionCanal="+seleccionCanalL+"&seleccionPais=21";
		}
	});	
}

function myFunction() {
	$body.removeClass("loading1");
	location.reload();
}

function nobackbutton(){
	window.location.hash="no-back-button";
	window.location.hash="Again-No-back-button" //chrome
	window.onhashchange=function(){window.location.hash="";}
}

function fecha(){
	
	document.getElementById("agregaMeses").innerHTML = "";	
	
	var meses= new Array ('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');	
	var hoy = new Date();
	var mm = hoy.getMonth()+1;
	
	document.getElementById("muestraMes").innerHTML ="en " + meses[seleccionMesL-1];
	
	
	$("#agregaMeses").append("" +
		"<li class='linkMenu arriba'>" +
			"<a onclick='menuTiempoRealDesaparece(0); activaCarga();' class='linkMenuHijo arribaHijo'" +
			" href='/checklist/central/vistaChecklist.htm?idUsuario=147247&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=100&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2018&seleccionMes=01&seleccionCanal="+seleccionCanalL+"&seleccionPais=21'>" +
			"<p class='textoCen'>Hoy</p></a></li>");
	
	
	for (var i = mm; i > 0; i--) {
		if(i==1){
			$("#agregaMeses").append("" +
				"<li class='linkMenu abajo'>" +
					"<a	onclick='menuTiempoRealDesaparece(0); activaCarga();' class='linkMenuHijo abajoHijo'"+
					" href='/checklist/central/vistaChecklistFecha.htm?idUsuario=147247&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=100&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2018&seleccionMes="+i+"&seleccionCanal="+seleccionCanalL+"&seleccionPais=21'>" +
				"<p class='textoCen'>"+meses[i-1]+"</p></a></li>");
		}else{
			$("#agregaMeses").append("" +
				"<li class='linkMenu laterales'>" +
					"<a	onclick='menuTiempoRealDesaparece(0); activaCarga();' class='linkMenuHijo'"+
					" href='/checklist/central/vistaChecklistFecha.htm?idUsuario=147247&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=100&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2018&seleccionMes="+i+"&seleccionCanal="+seleccionCanalL+"&seleccionPais=21'>" +
				"<p class='textoCen'>"+meses[i-1]+"</p></a></li>");
		}
	}
	
}


function despliega(id){
	despliegaCheck(id);
	modal = 2;
	//LLAMAR A AJAX PARA OBTENER LOS DATOS	
}

function oculta(){
	jQuery(function() {
		$(".subSeccion").hide();

		var allPanels = $('.accordion3 > dd').hide();

		jQuery('.accordion3 > dt').on('click', function() {
			$this = $(this);
			//the target panel content
			$target = $this.next();
			
			jQuery('.accordion3 > dt').removeClass('accordion3-active');
			if ($target.hasClass("in")) {
				$this.removeClass('accordion3-active');
				$target.slideUp();
				$target.removeClass("in");
				$(".subSeccion").hide();
				
				//alert("oculta");
				intervalo = setInterval("reporte()",15000);
			} else {
				$this.addClass('accordion3-active');
				jQuery('.accordion3 > dd').removeClass("in");
				$target.addClass("in");
				$(".subSeccion").show();
				jQuery('.accordion3 > dd').slideUp();
				$target.slideDown();
				
				//alert("muestra");
				clearInterval(intervalo);
			}
		})

	})
}

function cambiaNombre(nombre){
	lugarPaisL=nombre;
	document.getElementById("idOpcTerr").innerHTML = "";
	document.getElementById("idOpcTerr").innerHTML = nombre;
}

function despliegaCheck(idCeco){
	
	if(validaCheck!=idCeco){ 

		document.getElementById("tipoModal").className="modal3";
		validaCheck=idCeco;
		document.getElementById("idLlenaZonas"+idCeco).innerHTML = "";
		
		$.ajax({
			type : "GET",
			url : '/checklist/ajaxFiltroSucursalCaja.json?idCeco='+idCeco+'&seleccionAno='+seleccionAnoL
							+ '&seleccionMes='+seleccionMesL+'&seleccionCanal='+seleccionCanalL+'&seleccionPais='+seleccionPaisL+'&valorPila='+1,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {
				
				if (json == null) {
					//alert('Sin Datos');
					$("#idLlenaZonas"+idCeco).append("<span class='Gris1'>Sin Datos</span>");
				}else{					
					
					for (var a = 1; a < Object.keys(json).length; a++) {
						$("#idLlenaZonas"+idCeco).append("<table class='bloquer subnivel'><tbody><tr id='"+json[a][0].idCeco+"'><td class='nombreCheck'>"+json[a][0].descCeco+"</td>");
			
						for(var j = 0; j < json[a].length; j++ ){
								if(json[a][j].idChecklist == 97 || json[a][j].idChecklist == 99 || json[a][j].idChecklist == 156){
									if (json[a][j].total >= porcentajeG)
										$("#"+json[a][0].idCeco).append("<td ><div class='radio verdeN'>"+json[a][j].total+"%<div></td>");
									else if (json[a][j].total < (porcentajeG-5))
										$("#"+json[a][0].idCeco).append("<td><div class='radio rojo'>"+json[a][j].total+"%<div></td>");
									else if (json[a][j].total >= (porcentajeG-5) && json[a][j].total < (porcentajeG) )
										$("#"+json[a][0].idCeco).append("<td ><div class='radio amarilloN' style='color:black;'>"+json[a][j].total+"%<div></td>");
								}
						}
						$("#idLlenaZonas"+idCeco).append("</tr><tbody></table>");
					}
				} 	
				document.getElementById("tipoModal").className="modal1";
			},error : function() {
				//alert('Favor de actualizar la pagina');
			}
		});
	}	
}

function despliegaCheckIntervalo(){

	document.getElementById("tipoModal").className="modal3";
	document.getElementById("idLlenaZonas"+validaCheck).innerHTML = "";

	$.ajax({
		type : "GET",
		url : '/checklist/ajaxFiltroSucursalCaja.json?idCeco='+validaCheck+'&seleccionAno='+seleccionAnoL
						+ '&seleccionMes='+seleccionMesL+'&seleccionCanal='+seleccionCanalL+'&seleccionPais='+seleccionPaisL+'&valorPila='+1,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			
			if (json == null) {
				//alert('Sin Datos');
				$("#idLlenaZonas"+validaCheck).append("<span class='Gris1'>Sin Datos</span>");
			}else{					
				
				/*$("#idLlenaZonas"+validaCheck).append("<table class='bloquer subnivel'><tbody><tr class='pointer'><td style='width: 33%; height: 33%;'><span class='centrarSpan'>REGIÓN<span></td>" +
						"<td style='width: 22%; height: 33%;'><span class='centrarSpan'>CAJA<span></td>" +
						"<td style='width: 22%; height: 33%;'><span class='centrarSpan'>PISO BANCARIO<span></td>" +
						"<td style='width: 22%; height: 33%;'><span class='centrarSpan'>7S<span></td>" +
						"</tr></tbody></table>");*/

				for (var a = 1; a < Object.keys(json).length; a++) {
					$("#idLlenaZonas"+validaCheck).append("<table class='bloquer subnivel'><tbody><tr id='"+json[a][0].idCeco+"'><td class='nombreCheck'>"+json[a][0].descCeco+"</td>");
					
					
					for(var j = 0; j < json[a].length; j++ ){
						if(json[a][j].idChecklist == 97 || json[a][j].idChecklist == 99 || json[a][j].idChecklist == 156){
							if (json[a][j].total >= porcentajeG)
								$("#"+json[a][0].idCeco).append("<td ><div class='radio verdeN'>"+json[a][j].total+"%<div></td>");
							else if (json[a][j].total < (porcentajeG-5))
								$("#"+json[a][0].idCeco).append("<td><div class='radio rojo'>"+json[a][j].total+"%<div></td>");
							else if (json[a][j].total >= (porcentajeG-5) && json[a][j].total < (porcentajeG) )
								$("#"+json[a][0].idCeco).append("<td ><div class='radio amarilloN' style='color:black;'>"+json[a][j].total+"%<div></td>");
						}
					}
					$("#idLlenaZonas"+validaCheck).append("</tr><tbody></table>");
				}
			} 				
			document.getElementById("tipoModal").className="modal1";
		},error : function() {
			//alert('Favor de actualizar la pagina');
		}
	});	
}

