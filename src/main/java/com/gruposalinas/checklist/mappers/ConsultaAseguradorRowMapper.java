package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ConsultaAseguradorDTO;

public class ConsultaAseguradorRowMapper implements RowMapper<ConsultaAseguradorDTO> {

	public ConsultaAseguradorDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConsultaAseguradorDTO consultaAseguradorDTO = new ConsultaAseguradorDTO();
		
		consultaAseguradorDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
		consultaAseguradorDTO.setNomUsuario(rs.getString("FCNOMBRE"));
		
		return consultaAseguradorDTO;
		
		
	}

}
