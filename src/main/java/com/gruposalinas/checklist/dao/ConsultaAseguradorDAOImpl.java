package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ConsultaAseguradorDTO;
import com.gruposalinas.checklist.mappers.ConsultaAsegNomProtoRowMapper;
import com.gruposalinas.checklist.mappers.ConsultaAseguradorBitacoraRowMapper;
import com.gruposalinas.checklist.mappers.ConsultaAseguradorRowMapper;
import com.gruposalinas.checklist.mappers.ConsultaAseguradorSucursalRowMapper;
import com.gruposalinas.checklist.mappers.ConsultaDetalleProRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ConsultaAseguradorDAOImpl extends DefaultDAO implements ConsultaAseguradorDAO {

    private static Logger logger = LogManager.getLogger(ConsultaAseguradorDAOImpl.class);

    DefaultJdbcCall jdbcObtieneConsulta1;
    DefaultJdbcCall jdbcObtieneConsulta2;
    DefaultJdbcCall jdbcObtieneConsulta3;
    DefaultJdbcCall jdbcObtieneConsulta4;
    DefaultJdbcCall jdbcObtieneConsulta5;

    public void init() {

        jdbcObtieneConsulta1 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASEGURADOR")
                .withProcedureName("SPGETASEGURADOR")
                .returningResultSet("PA_CONSULTA", new ConsultaAseguradorRowMapper());

        jdbcObtieneConsulta2 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASEGURADOR")
                .withProcedureName("SPGETSUCURSAL")
                .returningResultSet("PA_CONSULTA", new ConsultaAseguradorSucursalRowMapper());

        jdbcObtieneConsulta3 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASEGURADOR")
                .withProcedureName("SPGETBITACOCERR")
                .returningResultSet("PA_CONSULTA", new ConsultaAseguradorBitacoraRowMapper());

        jdbcObtieneConsulta4 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASEGURADOR")
                .withProcedureName("SPPROTOCOLO")
                .returningResultSet("PA_CONSULTA", new ConsultaAsegNomProtoRowMapper());

        jdbcObtieneConsulta5 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASEGURADOR")
                .withProcedureName("SPDETALLEPRO")
                .returningResultSet("PA_CONSULTA", new ConsultaDetalleProRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaAseguradorDTO> obtieneConsultaAsegurador() throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaAseguradorDTO> listaConsulta = null;

        out = jdbcObtieneConsulta1.execute();

        //logger.info("Función ejecutada: FRANQUICIA.PAASEGURADOR.SPGETASEGURADOR");
        listaConsulta = (List<ConsultaAseguradorDTO>) out.get("PA_CONSULTA");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al consultar el StoreProcedure");
        } else {
            return listaConsulta;
        }

        return listaConsulta;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ConsultaAseguradorDTO> obtieneConsultaSucursal(ConsultaAseguradorDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaAseguradorDTO> listaConsulta2 = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_USUARIO", bean.getIdUsuario());

        out = jdbcObtieneConsulta2.execute(in);

        //logger.info("Función ejecutada: FRANQUICIA.PAASEGURADOR.SPGETSUCURSAL");
        listaConsulta2 = (List<ConsultaAseguradorDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al consultar el StoreProcedure");
        } else {
            return listaConsulta2;
        }

        return listaConsulta2;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ConsultaAseguradorDTO> obtieneConsultaBitaCerr(ConsultaAseguradorDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaAseguradorDTO> listaConsulta3 = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_USUARIO", bean.getIdUsuario())
                .addValue("PA_FCIDCECO", bean.getIdCeco());

        out = jdbcObtieneConsulta3.execute(in);

        //logger.info("Función ejecutada: FRANQUICIA.PAASEGURADOR.SPGETBITACOCERR");
        listaConsulta3 = (List<ConsultaAseguradorDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo ocurrió al consultar el StoreProcedure");
        } else {
            return listaConsulta3;
        }

        return listaConsulta3;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaAseguradorDTO> obtieneProtocolo(ConsultaAseguradorDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaAseguradorDTO> listaConsulta4 = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCIDCECO", bean.getIdCeco())
                .addValue("PA_FECHAFIN", bean.getFechaTermino());

        out = jdbcObtieneConsulta4.execute(in);

        //logger.info("Función ejecutada: FRANQUICIA.PAASEGURADOR.SPPROTOCOLO");
        listaConsulta4 = (List<ConsultaAseguradorDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al consultar el StoreProcedure");
        } else {
            return listaConsulta4;
        }
        return listaConsulta4;

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaAseguradorDTO> obtieneDetalleBitacora(ConsultaAseguradorDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaAseguradorDTO> listaConsulta5 = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_BITACORA", bean.getIdBitacora());

        out = jdbcObtieneConsulta5.execute(in);

        //logger.info("Función ejecutada: FRANQUICIA.PAASEGURADOR.SPDETALLEPRO");
        listaConsulta5 = (List<ConsultaAseguradorDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al consultar el StoreProcedure");
        } else {
            return listaConsulta5;
        }
        return listaConsulta5;

    }
}
