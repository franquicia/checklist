package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.SoftNvoDTO;


public interface SoftNvoDAO {

	  public int insertaSoftN(SoftNvoDTO bean)throws Exception;
		
		public boolean eliminaSoftN(int idFirma)throws Exception;
		
		public List<SoftNvoDTO> obtieneDatosSoftN(String idFirma) throws Exception; 
		
		public List<SoftNvoDTO> obtieneDatosSoftNDeta(String idFirma) throws Exception; 
		
		public boolean actualizaSoftN(SoftNvoDTO bean)throws Exception;
		 
		public int insertaCalculosActa(SoftNvoDTO bean)throws Exception;
		
		public boolean eliminaCalculosActa(int idFirma)throws Exception;
		
		public List<SoftNvoDTO> obtieneDatosCalculosActa(String idFirma) throws Exception; 
			
		public boolean actualizaCalculosActa(SoftNvoDTO bean)throws Exception;
		
		public int insertaSoftNuevoFlujo(SoftNvoDTO bean)throws Exception;
		
		  public int insertaSoftNuevoFlujoMtto(SoftNvoDTO bean)throws Exception;
	}