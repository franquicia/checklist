package com.gruposalinas.checklist.mappers;

import com.gruposalinas.checklist.domain.ProtocoloDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ProtocoloRowMapper implements RowMapper<ProtocoloDTO> {

	public ProtocoloDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ProtocoloDTO protoDTO = new ProtocoloDTO();
		
		protoDTO.setIdProtocolo(rs.getInt("FIID_PROTOCOLO"));
		protoDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
		protoDTO.setObservaciones(rs.getString("FCOBSERVACION"));
		protoDTO.setStatus(rs.getInt("FCSTATUS"));
		protoDTO.setStatusRetro(rs.getInt("FCSTATUSRETRO")); 

		return protoDTO;
	}

}
