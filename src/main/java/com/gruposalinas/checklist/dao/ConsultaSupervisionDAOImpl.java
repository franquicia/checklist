package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ConsultaSupervisionDTO;
import com.gruposalinas.checklist.mappers.ConsultaSupervisionRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ConsultaSupervisionDAOImpl extends DefaultDAO implements ConsultaSupervisionDAO {

    private static Logger logger = LogManager.getLogger(ProtocoloDAOImpl.class);

    DefaultJdbcCall jdbcObtieneConsultaSupervision;

    public void init() {

        jdbcObtieneConsultaSupervision = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE_SUPRV").withProcedureName("SPGETINFO_SUCUR")
                .returningResultSet("PA_CONSULTA", new ConsultaSupervisionRowMapper());
    }

    @SuppressWarnings("unchecked")
    /* @Override */
    public List<ConsultaSupervisionDTO> obtieneSucursal(ConsultaSupervisionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaSupervisionDTO> listaSucursal = new ArrayList<ConsultaSupervisionDTO>();

        String idSucursal = null;
        if (bean.getIdSucursal() != 0) {
            idSucursal = "" + bean.getIdSucursal();
        }

        String fechaInicio = null;
        if (bean.getFechaInicio() != null) {
            fechaInicio = bean.getFechaInicio();
        }

        String fechaFin = null;
        if (bean.getFechaFin() != null) {
            fechaFin = bean.getFechaFin();
        }

        String idProtocolo = null;
        if (bean.getIdProtocolo() != 0) {
            idProtocolo = "" + bean.getIdProtocolo();
        } else {
            idProtocolo = null;
        }

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FECHAINI", fechaInicio)
                .addValue("PA_FECHAFIN", fechaFin)
                .addValue("PA_FCID_CECO", idSucursal)
                .addValue("PA_FIPROTOCOLO", idProtocolo);

        out = jdbcObtieneConsultaSupervision.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAREPORTE_SUPRV.SPGETINFO_SUCUR");

        listaSucursal = (List<ConsultaSupervisionDTO>) out.get("PA_CONSULTA");

        if (listaSucursal == null) {
            error = 1;
        }

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los resultados de la consulta de sucursal");
        } else {
            return listaSucursal;
        }

        return listaSucursal;
    }

}
