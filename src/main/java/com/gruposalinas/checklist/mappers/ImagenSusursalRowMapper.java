package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;

public class ImagenSusursalRowMapper implements RowMapper<SucursalChecklistDTO> {

	public SucursalChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		SucursalChecklistDTO is = new SucursalChecklistDTO();
		
		is.setIdSucursal(rs.getInt("FCID_CECO"));
		is.setIdImag(rs.getInt("FIID_IMAG"));
		is.setNombre(rs.getString("FCNOMBRE"));
		is.setRuta(rs.getString("FCRUTA"));
		is.setObserv(rs.getString("FCOBSERVACION"));
		is.setPeriodo(rs.getString("FCPERIODO"));
		
		
		return is;
	}

}


