package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.VersionChecklistGralDTO;


public interface VersionChecklistGralDAO {

	  public int inserta(VersionChecklistGralDTO bean)throws Exception;
		
		public boolean elimina(String idTab)throws Exception;
		
		public List<VersionChecklistGralDTO> obtieneDatos(int idVers) throws Exception; 
		
		public List<VersionChecklistGralDTO> obtieneInfo() throws Exception; 
		
		public boolean actualiza(VersionChecklistGralDTO bean)throws Exception;
		
		public boolean actualiza2(VersionChecklistGralDTO bean)throws Exception;
		
	}