package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.CanalDAO;
import com.gruposalinas.checklist.domain.CanalDTO;

public class CanalBI {
		
	private static Logger logger = LogManager.getLogger(CanalBI.class);
	
	@Autowired
	CanalDAO canalDAO;
	
	List<CanalDTO> listaCanal = null;
	List<CanalDTO> listaCanales = null;
	
	public int insertaCanal(CanalDTO bean){
		
		int idCanal = 0;
		
		try {
			idCanal = canalDAO.insertaCanal(bean);			
		} catch (Exception e) {
			logger.info("No fue posible insertar el Canal");
		}
		
		return idCanal;
		
	}
		
	public boolean actualizaCanal(CanalDTO bean){
		
		boolean respuesta = false ;
		
		try {
			respuesta = canalDAO.actualizaCanal(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar el Canal");
		}
		
		return respuesta;
	}
	
	public boolean eliminaCanal(int idCanal){
		
		boolean respuesta = false ;
		
		try {
			respuesta = canalDAO.eliminaCanal(idCanal);
		} catch (Exception e) {
			logger.info("No fue posible insertar el Canal");
		}
		
		return respuesta;		
	}
	
	public List<CanalDTO> buscaCanal(int idCanal){
		
		try {
			listaCanal = canalDAO.buscaCanales(idCanal);
		} catch (Exception e) {
			logger.info("No fue posible consultar el Canal");
		}
				
		return listaCanal;		
	}

	public List<CanalDTO> buscaCanales(int activo){
	
		try {
			listaCanales = canalDAO.buscaCanales( activo);
		} catch (Exception e) {
			logger.info("No fue posible consultar los canales");
		}			
		
		return listaCanales;
	
	}
}
