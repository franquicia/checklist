package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ActorEdoHallaDTO;

public class ActorHallazgoRowMapper implements RowMapper<ActorEdoHallaDTO> {

	public ActorEdoHallaDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ActorEdoHallaDTO ActorEdoHallaDTO = new ActorEdoHallaDTO();
		
		ActorEdoHallaDTO.setIdActorConfig(rs.getInt("FIID_CONFIGACT"));
		ActorEdoHallaDTO.setIdConfiguracion(rs.getInt("FIID_CONFIG"));
		ActorEdoHallaDTO.setIdPerfil(rs.getInt("FIIDPERFIL"));
	    ActorEdoHallaDTO.setNombreActor(rs.getString("FCNOMBRE"));
	    ActorEdoHallaDTO.setObservacion(rs.getString("FCOBSERVACION"));
	    ActorEdoHallaDTO.setAux(rs.getString("FCAUX"));
	    ActorEdoHallaDTO.setStatus(rs.getInt("FISTATUS"));
	    ActorEdoHallaDTO.setPeriodo(rs.getString("FCPERIODO"));
	    
	 
		return ActorEdoHallaDTO;
		
	}

}


