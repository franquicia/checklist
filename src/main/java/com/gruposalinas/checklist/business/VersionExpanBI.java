package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.EmpFijoDAO;
import com.gruposalinas.checklist.dao.VersionExpanDAO;
import com.gruposalinas.checklist.domain.VersionExpanDTO;

public class VersionExpanBI {

	private static Logger logger = LogManager.getLogger(VersionExpanBI.class);

private List<VersionExpanDTO> listafila;
	
	@Autowired
	VersionExpanDAO versionExpanDAO;
		
	
	public int inserta(VersionExpanDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = versionExpanDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean elimina(String idTab){
		boolean respuesta = false;
		
		try {
			respuesta = versionExpanDAO.elimina(idTab);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	
	
	public List<VersionExpanDTO> obtieneDatos(int idVers) {

		try {
			listafila = versionExpanDAO.obtieneDatos(idVers);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}
	
	public List<VersionExpanDTO> obtieneInfo() {

		try {
			listafila = versionExpanDAO.obtieneInfo();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	
	public boolean actualiza(VersionExpanDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = versionExpanDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public boolean actualiza2(VersionExpanDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = versionExpanDAO.actualiza2(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public HashMap<String,List<VersionExpanDTO>> getInfoVersiones(){
		HashMap<String,List<VersionExpanDTO>> mapaVersiones = new HashMap<String,List<VersionExpanDTO>>();
		
		try {
			
			listafila = versionExpanDAO.obtieneInfo();
			
			if (listafila != null && listafila.size() != 0) {
				
				mapaVersiones = getMapaVersiones(listafila);
			}
			
			
		} catch (Exception e) {
			logger.info("No fue posible obtener datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return mapaVersiones;
		
	}
	
	public HashMap<String,List<VersionExpanDTO>> getMapaVersiones(List<VersionExpanDTO> listaVersiones) {
		
		HashMap<String,List<VersionExpanDTO>> mapaVersiones = new HashMap<String,List<VersionExpanDTO>>();
		List<VersionExpanDTO> grupo = new ArrayList<VersionExpanDTO>();


		  int actual = 0;
		  int siguiente = 0;
		  		  
		  
		  for (int i = 0; i < listaVersiones.size(); i++) {

			  actual = listaVersiones.get(i).getIdVers();
			  			  
			  if (i+1 < listaVersiones.size()) {
				  siguiente = listaVersiones.get(i+1).getIdVers();
			  }  
			  			  
			  grupo.add(listaVersiones.get(i));
			  
			  if (actual != siguiente) {
				  mapaVersiones.put(""+actual, grupo);
				  grupo = new ArrayList<VersionExpanDTO>();
			  } 
			  
			  if (i+1 == listaVersiones.size()) {
				  mapaVersiones.put(""+actual, grupo);
			  }
			  
		}
		  
		  return mapaVersiones;
	}
	

}