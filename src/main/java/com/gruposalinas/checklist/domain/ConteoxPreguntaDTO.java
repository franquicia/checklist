package com.gruposalinas.checklist.domain;

public class ConteoxPreguntaDTO {

	private int tiendas;
	private int idPregunta;
	private int respuesta;
	private int orden;
	
	public int getTiendas() {
		return tiendas;
	}
	public void setTiendas(int tiendas) {
		this.tiendas = tiendas;
	}
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public int getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(int respuesta) {
		this.respuesta = respuesta;
	}
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	
	
		
	
}
