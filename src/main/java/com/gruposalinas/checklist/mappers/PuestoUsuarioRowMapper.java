package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.PuestoDTO;
import com.gruposalinas.checklist.domain.PuestoUsuarioDTO;

public class PuestoUsuarioRowMapper implements RowMapper<PuestoUsuarioDTO>{

	public PuestoUsuarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		PuestoUsuarioDTO puestoDTO = new PuestoUsuarioDTO();
		
		
		puestoDTO.setIdPuesto(rs.getInt("FIID_PUESTO"));
		puestoDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
		
		return puestoDTO;
	}

}
