package com.gruposalinas.checklist.domain;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class DetalleTareasCreadorDTO {
	@SerializedName("PID") private String pid;
	@SerializedName("PName") private String pName;
	@SerializedName("Start") private String start;
	@SerializedName("Due") private String due;
	@SerializedName("Owner") private String owner;
	@SerializedName("OwnerId") private String ownerId;
	@SerializedName("OwnerRole") private String ownerRole;
	@SerializedName("AssignedTo") private String assignedTo;
	@SerializedName("PType") private String pType;
	@SerializedName("PTypeDesc") private String pTypeDesc;
	@SerializedName("CommentCount") private String commentCount;
	@SerializedName("Priority") private String priority;
	@SerializedName("StatusCd") private String statusCd;
	@SerializedName("StDesc") private String stDesc;
	@SerializedName("evidencia") private String evidencia;
	@SerializedName("SpecificData") private ArrayList<SpecificData> specificData;
	@SerializedName("Tasks") private ArrayList<Tasks> tasks;
	@SerializedName("Notes") private String notes;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getDue() {
		return due;
	}

	public void setDue(String due) {
		this.due = due;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getOwnerRole() {
		return ownerRole;
	}

	public void setOwnerRole(String ownerRole) {
		this.ownerRole = ownerRole;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getpType() {
		return pType;
	}

	public void setpType(String pType) {
		this.pType = pType;
	}

	public String getpTypeDesc() {
		return pTypeDesc;
	}

	public void setpTypeDesc(String pTypeDesc) {
		this.pTypeDesc = pTypeDesc;
	}

	public String getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(String commentCount) {
		this.commentCount = commentCount;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getStatusCd() {
		return statusCd;
	}

	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

	public String getStDesc() {
		return stDesc;
	}

	public void setStDesc(String stDesc) {
		this.stDesc = stDesc;
	}

	public String getEvidencia() {
		return evidencia;
	}

	public void setEvidencia(String evidencia) {
		this.evidencia = evidencia;
	}

	public ArrayList<SpecificData> getSpecificData() {
		return specificData;
	}

	public void setSpecificData(ArrayList<SpecificData> specificData) {
		this.specificData = specificData;
	}

	public ArrayList<Tasks> getTasks() {
		return tasks;
	}

	public void setTasks(ArrayList<Tasks> tasks) {
		this.tasks = tasks;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	class SpecificData {
		@SerializedName("Title") private String title;
		@SerializedName("Value") private String value;
		
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
	}
	
	class Tasks {
		@SerializedName("TaskId") private String taskId;
		@SerializedName("TaskName") private String taskName;
		@SerializedName("Start") private String start;
		@SerializedName("Due") private String due;
		@SerializedName("AssignedTo") private String assignedTo;
		@SerializedName("StatusCd") private String statusCd;
		@SerializedName("NumPaso") private String numPaso;
		@SerializedName("StatusDesc") private String statusDesc;
		@SerializedName("TipoPaso") private String tipoPaso;
		@SerializedName("Attachments") private ArrayList<Attachments> attachments;

		public String getTaskId() {
			return taskId;
		}
		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}
		public String getTaskName() {
			return taskName;
		}
		public void setTaskName(String taskName) {
			this.taskName = taskName;
		}
		public String getStart() {
			return start;
		}
		public void setStart(String start) {
			this.start = start;
		}
		public String getDue() {
			return due;
		}
		public void setDue(String due) {
			this.due = due;
		}
		public String getAssignedTo() {
			return assignedTo;
		}
		public void setAssignedTo(String assignedTo) {
			this.assignedTo = assignedTo;
		}
		public String getStatusCd() {
			return statusCd;
		}
		public void setStatusCd(String statusCd) {
			this.statusCd = statusCd;
		}
		public String getNumPaso() {
			return numPaso;
		}
		public void setNumPaso(String numPaso) {
			this.numPaso = numPaso;
		}
		public String getStatusDesc() {
			return statusDesc;
		}
		public void setStatusDesc(String statusDesc) {
			this.statusDesc = statusDesc;
		}
		public String getTipoPaso() {
			return tipoPaso;
		}
		public void setTipoPaso(String tipoPaso) {
			this.tipoPaso = tipoPaso;
		}
		public ArrayList<Attachments> getAttachments() {
			return attachments;
		}
		public void setAttachments(ArrayList<Attachments> attachments) {
			this.attachments = attachments;
		}		
	}
	
	class Attachments {
		@SerializedName("ID") private int id;
		@SerializedName("Filename") private String filename;
		@SerializedName("FileUrl") private String fileUrl;
		@SerializedName("TipoAnexo") private String tipoAnexo;
		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getFilename() {
			return filename;
		}
		public void setFilename(String filename) {
			this.filename = filename;
		}
		public String getFileUrl() {
			return fileUrl;
		}
		public void setFileUrl(String fileUrl) {
			this.fileUrl = fileUrl;
		}
		public String getTipoAnexo() {
			return tipoAnexo;
		}
		public void setTipoAnexo(String tipoAnexo) {
			this.tipoAnexo = tipoAnexo;
		}
	}
}
