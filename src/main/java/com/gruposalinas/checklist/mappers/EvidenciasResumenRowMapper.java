package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.EvidenciasResumenDTO;

public class EvidenciasResumenRowMapper implements RowMapper<EvidenciasResumenDTO>{

	@Override
	public EvidenciasResumenDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		EvidenciasResumenDTO evidenciasResumenDTO = new EvidenciasResumenDTO();
		
		evidenciasResumenDTO.setEvidencia(rs.getString("FCRUTA"));
		evidenciasResumenDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		evidenciasResumenDTO.setPregunta(rs.getString("PREGUNTA"));
		evidenciasResumenDTO.setRespuesta(rs.getString("RESPUESTA"));
		
		return evidenciasResumenDTO;
	}
	

}
