package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ModuloDTO;

public interface ModuloTDAO {
	
	public List<ModuloDTO> obtieneModuloTemp(String idModulo) throws Exception;
	
	public int insertaModuloTemp(ModuloDTO bean) throws Exception;
	
	public boolean actualizaModuloTemp (ModuloDTO bean) throws Exception;
	
	public boolean eliminaModuloTemp (int idModulo) throws Exception;
}
