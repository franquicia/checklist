<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="es">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
<title>Supervisi&oacute;n Sucursales</title>
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/estilos.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/menu.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/secciones.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/dropkick.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/paginador.css" media="screen">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/calendario.css" media="screen">

</head>

<body>

	<div class="page">
		<div class="clear"></div>
		<div class="title" id="title">
			<div class="wrapper">
				<div class="h2">
					<a href="/checklist/central/vistaSupervisionSistemas.htm">P&aacute;gina Principal </a> / <a>Supervisi&oacute;n Sistemas</a>
				</div>
				<h1>Supervisi&oacute;n Sistemas</h1>
			</div>
		</div>

		<div class="clear"></div>

		<!-- Contenido -->
		<div class="contHome">
			<div class="titN">
				<table class="tblHeader">
					<tbody>
						<tr>
							<td>&nbsp;</td>
							<c:forEach var="i" begin="0"
								end="${(fn:length(listareporteTotales))-1}">
								<td>Total visitas programadas:<strong>${listareporteTotales[i].totales}</strong></td>
						</tr>
						<tr>
							<td>Eligue un sistema para ver su detalle</td>
							<td>Total visitas actuales: <strong>${listareporteTotales[i].actuales}</strong></td>
							</c:forEach>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="clear"></div>
			<br>
			<div class="home">

				<!-- Hacer aqui el loop, mostrar la informacion que viene en el  -->

				<c:forEach var="k" begin="0"
					end="${(fn:length(listareporteSistemas))-1}">
					<a
						href="vistaFiltroSupervision.htm?idCeco=${listareporteSistemas[k].idCecos}&nombreCeco=${listareporteSistemas[k].nombreCeco}"
						class="btnHome">
						<table class="tblbtnHome">
							<tr>
								<td>${listareporteSistemas[k].nombreCeco}</td>
								<td class="tRight"><span>${listareporteSistemas[k].terminados}</span>/<span
									class="txtVerde"> ${listareporteSistemas[k].asignados} </span></td>
							</tr>
						</table>
					</a>
				</c:forEach>
				<a
					href="vistaFiltroSupervision.htm?idCeco=000000&nombreCeco=General"
					class="btnHome">
					<table class="tblbtnHome">
						<tr>
							<td>TODAS LAS DIRECCIONES</td>
							<td class="tRight"></td>
						</tr>
					</table>
				</a>
			</div>

			<div class="clear"></div>
		</div>

		<div class="clear"></div>
		<div class="footer1">
			<table class="tblFooter1">
				<tbody>
					<tr>
						<td style="width: 200px;">Sistema Reportes</td>
						<td style="width: 200px;"><a href="#" class="btnRojo">Malas Pr&aacute;cticas</a></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="footer">
			<table class="tblFooter">
				<tbody>
					<tr>
						<td style="width: 200px;">Banco Azteca S.A. Instituci&oacute;n de Banca M&uacute;ltiple</td>
						<td style="width: 200px;">Derechos Reservados 2014 (T&eacute;rminos y Condiciones de uso
							del Portal.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>

<script type="text/javascript"
	src="../js/supervisionSistemas/script_reportes.js"></script>

<script type="text/javascript"
	src="../js/supervisionSistemas/jquery-1.12.4.min.js"></script>
<script type="text/javascript"
	src="../js/supervisionSistemas/jquery.dropkick.js"></script>
<script type="text/javascript"
	src="../js/supervisionSistemas/content_height.js"></script>
<script type="text/javascript"
	src="../js/supervisionSistemas/jquery-ui.js"></script>

<script type="text/javascript"
	src="../js/supervisionSistemas/jquery.paginate.js"></script>
<script type="text/javascript"
	src="../js/supervisionSistemas/funciones.js"></script>

<script type="text/javascript">
	$(function() {
		$("#demo5").paginate(
				{
					count : 3,
					start : 1,
					display : 3,
					border : false,
					border_color : false,
					text_color : '#8a8a8a',
					background_color : 'transparent',
					border_hover_color : 'transparent',
					text_hover_color : '#000',
					background_hover_color : 'transparent',
					images : true,
					mouse : 'press',
					onChange : function(page) {
						$('._current', '#paginationdemo').removeClass(
								'_current').hide();
						$('#p' + page).addClass('_current').show();
					}
				});
	});
</script>