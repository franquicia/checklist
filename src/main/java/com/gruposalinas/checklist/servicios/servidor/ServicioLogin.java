package com.gruposalinas.checklist.servicios.servidor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.checklist.business.AdmHandbookBI;
import com.gruposalinas.checklist.business.AdmInformProtocolosBI;
import com.gruposalinas.checklist.business.AdmZonasBI;
import com.gruposalinas.checklist.business.AsignaTransfBI;
import com.gruposalinas.checklist.business.BitacoraAdministradorBI;
import com.gruposalinas.checklist.business.BitacoraBI;
import com.gruposalinas.checklist.business.BitacoraGralBI;
import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.business.CheckSoporteAdmBI;
import com.gruposalinas.checklist.business.CheckinBI;
import com.gruposalinas.checklist.business.ChecklistBI;
import com.gruposalinas.checklist.business.ChecklistPreguntasComBI;
import com.gruposalinas.checklist.business.ChecksOfflineBI;
import com.gruposalinas.checklist.business.ComentariosRepAsgBI;
import com.gruposalinas.checklist.business.Correo7SBI;
import com.gruposalinas.checklist.business.CorreoBI;
import com.gruposalinas.checklist.business.DatosInformeBI;
import com.gruposalinas.checklist.business.DetalleProtoBI;
import com.gruposalinas.checklist.business.DocsHandlerBI;
import com.gruposalinas.checklist.business.EmpContacExtBI;
import com.gruposalinas.checklist.business.EvidenciaBI;
import com.gruposalinas.checklist.business.FirmaCheckBI;
import com.gruposalinas.checklist.business.FirmaExpansionBI;
import com.gruposalinas.checklist.business.HallazgosEviExpBI;
import com.gruposalinas.checklist.business.HallazgosExpBI;
import com.gruposalinas.checklist.business.HallazgosRepoBI;
import com.gruposalinas.checklist.business.HallazgosTransfBI;
import com.gruposalinas.checklist.business.IncidentesBI;
import com.gruposalinas.checklist.business.InformeGeneralBI;
import com.gruposalinas.checklist.business.ListaChkAsignBI;
import com.gruposalinas.checklist.business.LoginBI;
import com.gruposalinas.checklist.business.MovilInfoBI;
import com.gruposalinas.checklist.business.NotificacionBI;
import com.gruposalinas.checklist.business.ParametroBI;
import com.gruposalinas.checklist.business.ProtocoloBI;
import com.gruposalinas.checklist.business.RepoCheckPaperBI;
import com.gruposalinas.checklist.business.ReporteChecksExpBI;
import com.gruposalinas.checklist.business.RespuestaBI;
import com.gruposalinas.checklist.business.RespuestaPdfBI;
import com.gruposalinas.checklist.business.SoftNvoBI;
import com.gruposalinas.checklist.business.SoftOpeningBI;
import com.gruposalinas.checklist.business.SucUbicacionBI;
import com.gruposalinas.checklist.business.SucursalChecklistBI;
import com.gruposalinas.checklist.business.TipoCifradoBI;
import com.gruposalinas.checklist.business.Usuario_ABI;
import com.gruposalinas.checklist.business.VersionBI;
import com.gruposalinas.checklist.business.VersionExpanBI;
import com.gruposalinas.checklist.business.VisitaTiendaBI;
import com.gruposalinas.checklist.domain.AdjuntoDTO;
import com.gruposalinas.checklist.domain.AdmHandbookDTO;
import com.gruposalinas.checklist.domain.AdmInformProtocolosDTO;
import com.gruposalinas.checklist.domain.AdminZonaDTO;
import com.gruposalinas.checklist.domain.AsignaTransfDTO;
import com.gruposalinas.checklist.domain.BitacoraAdministradorDTO;
import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.BitacoraGralDTO;
import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.CheckSoporteAdmDTO;
import com.gruposalinas.checklist.domain.CheckinDTO;
import com.gruposalinas.checklist.domain.ChecklistCompletoDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistHallazgosRepoDTO;
import com.gruposalinas.checklist.domain.ChecksOfflineCompletosDTO;
import com.gruposalinas.checklist.domain.ChecksOfflineDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.domain.DatosInformeDTO;
import com.gruposalinas.checklist.domain.DetalleProtoDTO;
import com.gruposalinas.checklist.domain.EmpContacExtDTO;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;
import com.gruposalinas.checklist.domain.HallazgosEviExpDTO;
import com.gruposalinas.checklist.domain.HallazgosExpDTO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;
import com.gruposalinas.checklist.domain.IncidenteDTO;
import com.gruposalinas.checklist.domain.ListaAsignChkDTO;
import com.gruposalinas.checklist.domain.LoginDTO;
import com.gruposalinas.checklist.domain.MovilInfoDTO;
import com.gruposalinas.checklist.domain.ParametroDTO;
import com.gruposalinas.checklist.domain.PerfilUsuarioDTO;
import com.gruposalinas.checklist.domain.ProtocoloDTO;
import com.gruposalinas.checklist.domain.RegistroRespuestaDTO;
import com.gruposalinas.checklist.domain.RegistroRespuestasCompletasDTO;
import com.gruposalinas.checklist.domain.RepoCheckPaperDTO;
import com.gruposalinas.checklist.domain.ReporteChecksExpDTO;
import com.gruposalinas.checklist.domain.SoftNvoDTO;
import com.gruposalinas.checklist.domain.SucUbicacionDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;
import com.gruposalinas.checklist.domain.TipoCifradoDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.domain.Usuario_ADTO;
import com.gruposalinas.checklist.domain.VersionExpanDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.util.StrCipher;
import com.gruposalinas.checklist.util.UtilCryptoGS;
import com.gruposalinas.checklist.util.UtilDate;
import com.gruposalinas.checklist.util.UtilMail;
import com.gruposalinas.checklist.util.UtilObject;
import com.gruposalinas.checklist.util.UtilResource;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/servicios")
public class ServicioLogin {

    private static Logger logger = LogManager.getLogger(ServicioLogin.class);

    @Autowired
    LoginBI loginBI;
    @Autowired
    ChecklistBI checklistBI;
    @Autowired
    BitacoraBI bitacoraBI;
    @Autowired
    RegistroRespuestaDTO respuestaRespuestaDTO;
    @Autowired
    RegistroRespuestasCompletasDTO registroRespuestasCompletasDTO;
    @Autowired
    RespuestaBI respuestaBI;
    @Autowired
    Usuario_ABI usuario_ABI;
    @Autowired
    ChecksOfflineBI checksOfflineBI;
    @Autowired
    TipoCifradoBI tipoCifradoBI;
    @Autowired
    MovilInfoBI movilInfoBI;
    @Autowired
    CorreoBI correoBI;
    @Autowired
    ParametroBI parametroBI;
    @Autowired
    VisitaTiendaBI visitaBI;
    @Autowired
    CecoBI cecoBI;
    @Autowired
    VisitaTiendaBI visitaTiendaBI;
    @Autowired
    NotificacionBI notificacionBI;
    @Autowired
    RespuestaPdfBI respuestaPdfBI;
    @Autowired
    VersionBI versionBI;

    @Autowired
    IncidentesBI incidenteBi;

    @Autowired
    Correo7SBI correo7SBI;

    @Autowired
    CecoBI cecobi;

    @Autowired
    BitacoraBI bitacorabi;

    @Autowired
    SucursalChecklistBI sucursalChecklistBI;

    @Autowired
    BitacoraGralBI bitacoraGralBI;

    @Autowired
    RepoCheckPaperBI repoCheckPaperBI;

    @Autowired
    FirmaCheckBI firmaCheckBI;

    @Autowired
    ReporteChecksExpBI reporteChecksBI;

    @Autowired
    ChecklistPreguntasComBI checklistPreguntasComBI;

    @Autowired
    CheckinBI checkinBI;

    @Autowired
    SoftOpeningBI softOpeningBI;

    @Autowired
    EvidenciaBI evidenciabi;

    @Autowired
    VersionExpanBI versionExpanBI;

    @Autowired
    DetalleProtoBI detalleProtoBI;

    @Autowired
    EmpContacExtBI empContactExtBI;

    @Autowired
    HallazgosExpBI hallazgosExpBI;

    @Autowired
    HallazgosEviExpBI hallazgosEviExpBI;

    @Autowired
    HallazgosRepoBI hallazgosRepoBI;

    @Autowired
    BitacoraAdministradorBI bitacoraAdministradorBI;

    @Autowired
    SucUbicacionBI sucUbicacionBI;

    @Autowired
    AdmHandbookBI admHandbookBI;

    @Autowired
    ListaChkAsignBI listaChkAsignBI;

    @Autowired
    ProtocoloBI protocoloBI;

    @Autowired
    ComentariosRepAsgBI comentariosRepAsgBI;

    @Autowired
    CheckSoporteAdmBI checkSoporteAdmBI;

    @Autowired
    AsignaTransfBI asignaTransfBI;

    @Autowired
    HallazgosTransfBI hallazgosTransfBI;

    @Autowired
    AdmInformProtocolosBI admInformProtocolosBI;

    @Autowired
    SoftNvoBI softNvoBI;

    @Autowired
    AdmZonasBI admZonasBI;

    @Autowired
    InformeGeneralBI informeGeneralBI;

    @Autowired
    DatosInformeBI datosInformeBI;

    // servicios/loginRest.json?id=191312&pass=Uriel
    @SuppressWarnings("static-access")
    // servicios/loginRest.json?Lg/M4L45zj4Zl05KmbXZmB0FfUnUG08UnAcW0OXp8q0=&token=
    @RequestMapping(value = "/loginRest", method = RequestMethod.GET)
    public @ResponseBody
    String getUsrArts(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        // urides = cifra.decryptParams(uri);
        int uriinf3 = Integer.parseInt(urides.split("=")[1]);

        LoginDTO regEmpleado = null;
        String json = "";
        JsonObject object = new JsonObject();
        List<Usuario_ADTO> regChecklist = null;

        int user = 0;
        // String pass = "";

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);

        try {
            regChecklist = usuario_ABI.obtieneUsuario(uriinf3);
        } catch (Exception e) {
            logger.info("No hay usuario en login");
        }
        if (regChecklist.size() > 0) {
            try {
                user = uriinf3;
                // pass = uriinf4;
                if (user > 0) {

                    try {
                        regEmpleado = loginBI.consultaUsuario(user);
                    } catch (Exception e) {
                        logger.info("Ocurrió algo  al retornar Objeto usuario");
                    }
                    if (regEmpleado != null) {
                        // LoginDTO userSession = new LoginDTO();
                        object.addProperty("nombre", regEmpleado.getNombreEmpelado());
                        object.addProperty("idPerfil", regEmpleado.getIdperfil());
                        object.addProperty("fecha", fecha);
                        object.addProperty("idPuesto", regEmpleado.getIdPuesto());
                        object.addProperty("descripcion", regEmpleado.getDescPuesto());
                        json = object.toString();
                    } else {
                        object.addProperty("nombre", "null");
                        object.addProperty("fecha", fecha);
                        json = object.toString();
                    }
                } else {
                    object.addProperty("nombre", "null");
                    object.addProperty("fecha", fecha);
                    json = object.toString();
                }
            } catch (Exception e) {
                logger.info("Ocurrió algo  al validar el usuario " + e);
            }
        } else {
            object.addProperty("nombre", "null");
            object.addProperty("fecha", fecha);
            json = object.toString();
        }
        response.setContentType("application/json");
        response.getWriter().write(json);
        return null;
    }

    /*-----------------------------------------------------------NUEVO LOGIN-----------------------------------------------------------*/
    // servicios/loginRest2.json?id=191312
    // servicios/loginRest2.json?Lg/M4L45zj4Zl05KmbXZmB0FfUnUG08UnAcW0OXp8q0=&token=
    @SuppressWarnings({"static-access", "unchecked", "null"})
    @RequestMapping(value = "/loginRest2", method = RequestMethod.GET)
    public @ResponseBody
    String getUsrArts2(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        int uriinf3 = Integer.parseInt(urides.split("=")[1]);

        String json = "";
        JsonObject object1 = null;
        JsonArray array = new JsonArray();
        JsonArray arrayPerfil = new JsonArray();

        JsonObject object = null;
        List<Usuario_ADTO> regChecklist = null;

        Map<String, Object> regEmpleado = null;
        List<LoginDTO> regListaUsuarios = null;
        List<ChecklistDTO> reglistaChecklist = null;
        List<PerfilUsuarioDTO> listaPerfiles = null;

        int user = 0;
        // String pass = "";

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);

        try {
            regChecklist = usuario_ABI.obtieneUsuario(uriinf3);
        } catch (Exception e) {
            logger.info("Ocurrió algo  al retornar el usuario en login");
        }
        if (regChecklist.size() > 0) {
            try {
                user = uriinf3;
                // pass = uriinf4;
                if (user > 0) {

                    try {
                        regEmpleado = loginBI.buscaUsuarioCheck(user);
                    } catch (Exception e) {
                        logger.info("Ocurrió algo  al retornar Objeto usuario");
                    }

                    if (regEmpleado != null) {

                        Iterator<Map.Entry<String, Object>> enMap = regEmpleado.entrySet().iterator();
                        while (enMap.hasNext()) {
                            Map.Entry<String, Object> enMaps = enMap.next();
                            String nomH = enMaps.getKey();
                            if (nomH == "listaUsuarios") {
                                regListaUsuarios = (List<LoginDTO>) enMaps.getValue();
                            }
                            if (nomH == "listaChecklist") {
                                reglistaChecklist = (List<ChecklistDTO>) enMaps.getValue();
                            }
                            if (nomH == "listaPerfiles") {
                                listaPerfiles = (List<PerfilUsuarioDTO>) enMaps.getValue();

                            }

                            logger.info("VALOR LISTA 1: " + nomH);

                        }

                        if (regListaUsuarios != null) {

                            object1 = new JsonObject();

                            object1.addProperty("nombre", regListaUsuarios.get(0).getNombreEmpelado());
                            object1.addProperty("idPerfil", regListaUsuarios.get(0).getIdperfil());
                            object1.addProperty("fecha", fecha);
                            object1.addProperty("idPuesto", regListaUsuarios.get(0).getIdPuesto());
                            object1.addProperty("descripcion", regListaUsuarios.get(0).getDescPuesto());
                            object1.addProperty("idPais", regListaUsuarios.get(0).getIdPais());
                            object1.addProperty("idTerritorio", regListaUsuarios.get(0).getIdTerritorio());
                            object1.addProperty("idZona", regListaUsuarios.get(0).getZona());
                            object1.addProperty("idRegion", regListaUsuarios.get(0).getRegion());
                            object1.addProperty("desCeco", regListaUsuarios.get(0).getDescCeco());

                            if (reglistaChecklist != null) {
                                Iterator<ChecklistDTO> it = reglistaChecklist.iterator();
                                while (it.hasNext()) {
                                    ChecklistDTO o = it.next();
                                    object = new JsonObject();
                                    // checkList
                                    object.addProperty("id", o.getIdChecklist());
                                    object.addProperty("nombre", o.getNombreCheck());
                                    array.add(object);
                                }
                                object1.add("checklist", array);
                            } else {
                                object1.addProperty("checklist", "null");
                            }

                            if (listaPerfiles != null) {
                                logger.info("se agrega contenido a la lista de perfiles");
                                Iterator<PerfilUsuarioDTO> itP = listaPerfiles.iterator();
                                while (itP.hasNext()) {
                                    logger.info("perfil: " + itP);

                                    PerfilUsuarioDTO p = (PerfilUsuarioDTO) itP.next();
                                    object = new JsonObject();
                                    // checkList
                                    object.addProperty("idUsuario", p.getIdUsuario());
                                    object.addProperty("idPerfil", p.getIdPerfil());
                                    object.addProperty("descripcion", p.getDescripcion());

                                    arrayPerfil.add(object);

                                }

                                object1.add("listaPerfiles", arrayPerfil);
                            } else {
                                logger.info("lista de perfiles vacia");
                                object1.addProperty("listaPerfiles", "null");
                            }

                        } else {
                            object1.addProperty("nombre", "null");
                            object1.addProperty("fecha", fecha);
                        }
                    } else {
                        object1.addProperty("nombre", "null");
                        object1.addProperty("fecha", fecha);
                    }
                } else {
                    object1.addProperty("nombre", "null");
                    object1.addProperty("fecha", fecha);
                }
            } catch (Exception e) {
                logger.info("Ocurrió algo  al validar el usuario " + e);
            }
        } else {
            object1.addProperty("nombre", "null");
            object1.addProperty("fecha", fecha);
        }
        json = object1.toString();
        response.setContentType("application/json");
        response.getWriter().write(json);
        return null;
    }

    // servicios/checklist.json?Uh4D1aCk/i/+pjNGXM+AgNhCmS05nnARS/JueMF0L0GseeiwsjOK7DPo0wdWpwmOm6HSL49nkrdVBE9Q4SCXoQ==
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/checklist", method = RequestMethod.GET)
    public @ResponseBody
    String getChecklistDTOInJSON(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        uri = uri.split("&")[0];
        UtilDate fec = new UtilDate();
        String fechaprueba = fec.getSysDate("ddMMyyyyHHmmss");

        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        // String urides = cifra.decryptParams(uri);
        String uriinf[] = urides.split("&");
        int use = Integer.parseInt(uriinf[0].split("=")[1]);
        double latitud = Double.parseDouble(uriinf[1].split("=")[1]);
        double longitud = Double.parseDouble(uriinf[2].split("=")[1]);

        List<ChecklistDTO> regChecklist = null;
        JsonArray array = new JsonArray();
        JsonObject object = null;
        JsonObject princ = new JsonObject();

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);
        String json = null;

        try {
            if (use > 0) {
                try {
                    regChecklist = checklistBI.consultaChecklist(use, latitud, longitud);
                } catch (Exception e) {
                    logger.info("Ocurrió algo  al retornar regChecklist");
                }
                if (regChecklist != null) {
                    Iterator<ChecklistDTO> it = regChecklist.iterator();
                    while (it.hasNext()) {
                        ChecklistDTO o = it.next();
                        object = new JsonObject();
                        JsonObject object1 = new JsonObject();
                        JsonObject object2 = new JsonObject();
                        JsonObject object3 = new JsonObject();
                        // tipoCheck
                        object1.addProperty("id", o.getIdTipoChecklist().getIdTipoCheck());
                        object1.addProperty("descripcion", o.getIdTipoChecklist().getDescTipo());
                        // checkList
                        object2.addProperty("id", o.getIdChecklist());
                        object2.addProperty("nombre", o.getNombreCheck());
                        object2.addProperty("fechaInicio", o.getFecha_inicio());
                        object2.addProperty("fechaFin", o.getFecha_fin());
                        // checkUsuario
                        object3.addProperty("id", o.getIdCheckUsua());

                        object.add("tipoCheck", object1);
                        object.add("checkList", object2);
                        object.add("checkUsuario", object3);
                        // nombre
                        object.addProperty("nombre", o.getNombresuc());
                        object.addProperty("zona", o.getZona());
                        object.addProperty("latitud", latitud);
                        object.addProperty("longitud", longitud);
                        object.addProperty("ultimaVisita", o.getUltimaVisita());

                        array.add(object);
                    }
                    princ.add("tiendasProx", array);
                    princ.addProperty("fecha", fecha);
                    json = princ.toString();
                } else {
                    princ.add("tiendasProx", array);
                    princ.addProperty("fecha", fecha);
                    json = princ.toString();
                }
            } else {
                princ.add("tiendasProx", array);
                princ.addProperty("fecha", fecha);
                json = princ.toString();
            }
            response.setContentType("application/json");
            response.getWriter().write(json);
        } catch (Exception e) {
            logger.info("Ocurrió algo  en regChecklist " + e);
        }
        return null;
    }

    // servicios/checklist.json?Uh4D1aCk/i/+pjNGXM+AgNhCmS05nnARS/JueMF0L0GseeiwsjOK7DPo0wdWpwmOm6HSL49nkrdVBE9Q4SCXoQ==
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/checklist2", method = RequestMethod.GET)
    public @ResponseBody
    String getChecklist2DTOInJSON(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        uri = uri.split("&")[0];
        UtilDate fec = new UtilDate();
        String fechaprueba = fec.getSysDate("ddMMyyyyHHmmss");

        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        // String urides = cifra.decryptParams(uri);
        String uriinf[] = urides.split("&");
        int use = Integer.parseInt(uriinf[0].split("=")[1]);
        //double latitud = Double.parseDouble(uriinf[1].split("=")[1]);
        //double longitud = Double.parseDouble(uriinf[2].split("=")[1]);
        String latitud = uriinf[1].split("=")[1];
        String longitud = uriinf[2].split("=")[1];
        int idCheck = Integer.parseInt(uriinf[3].split("=")[1]);

        List<ChecklistDTO> regChecklist = null;
        JsonArray array = new JsonArray();
        JsonObject object = null;
        JsonObject princ = new JsonObject();

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);
        String json = null;

        try {
            if (use > 0) {
                try {
                    regChecklist = checklistBI.buscaChecklistCompletoPrueba(use, idCheck, latitud, longitud);
                } catch (Exception e) {
                    logger.info("Ocurrió algo  al retornar regChecklist");
                }
                if (regChecklist != null) {
                    Iterator<ChecklistDTO> it = regChecklist.iterator();
                    while (it.hasNext()) {
                        ChecklistDTO o = it.next();
                        object = new JsonObject();
                        JsonObject object1 = new JsonObject();
                        JsonObject object2 = new JsonObject();
                        JsonObject object3 = new JsonObject();
                        // tipoCheck
                        object1.addProperty("id", o.getIdTipoChecklist().getIdTipoCheck());
                        object1.addProperty("descripcion", o.getIdTipoChecklist().getDescTipo());
                        // checkList
                        object2.addProperty("id", o.getIdChecklist());
                        object2.addProperty("nombre", o.getNombreCheck());
                        object2.addProperty("fechaInicio", o.getFecha_inicio());
                        object2.addProperty("fechaFin", o.getFecha_fin());
                        // checkUsuario
                        object3.addProperty("id", o.getIdCheckUsua());

                        object.add("tipoCheck", object1);
                        object.add("checkList", object2);
                        object.add("checkUsuario", object3);
                        // nombre
                        object.addProperty("idCeco", o.getIdCeco());
                        object.addProperty("nombre", o.getNombresuc());
                        object.addProperty("zona", o.getZona());
                        object.addProperty("latitud", o.getLatitud());
                        object.addProperty("longitud", o.getLongitud());
                        object.addProperty("ultimaVisita", o.getUltimaVisita());

                        array.add(object);
                    }
                    princ.add("tiendasProx", array);
                    princ.addProperty("fecha", fecha);
                    json = princ.toString();
                } else {
                    princ.add("tiendasProx", array);
                    princ.addProperty("fecha", fecha);
                    json = princ.toString();
                }
            } else {
                princ.add("tiendasProx", array);
                princ.addProperty("fecha", fecha);
                json = princ.toString();
            }
            response.setContentType("application/json");
            response.getWriter().write(json);
        } catch (Exception e) {
            logger.info("Ocurrió algo  en regChecklist " + e);
        }
        return null;
    }

    // servicios/checklistUsuario.json?id=191312&idCheck=1
    // servicios/checklistUsuario.json?xzOx3NpCcqin/sVUqLeRdHqaR0E/Riq4JX6WGLh0L8k=
    @SuppressWarnings({"unchecked", "static-access", "unused"})
    @RequestMapping(value = "/checklistUsuario", method = RequestMethod.GET)
    public @ResponseBody
    String getChecklistUsuarioDTOInJSON(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        uri = uri.split("&")[0];
        UtilDate fec = new UtilDate();
        String fechaprueba = fec.getSysDate("ddMMyyyyHHmmss");

        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        urides = urides.split("&")[1];
        // String urides = (cifra.decryptParams(uri)).split("&")[1];

        int idcheck = Integer.parseInt(urides.split("=")[1]);

        List<ChecklistCompletoDTO> regChecklist = null;
        List<Usuario_ADTO> regUsuariosADTO = null;
        Map<String, Object> reg = new HashMap<String, Object>();
        JsonArray array0 = new JsonArray();
        JsonArray array1 = new JsonArray();
        JsonArray array2 = new JsonArray();
        JsonArray array3 = new JsonArray();
        JsonArray array4 = new JsonArray();
        JsonObject object = null;
        JsonObject princ = new JsonObject();

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);
        String json = null;

        try {
            if (idcheck > 0) {
                try {
                    reg = checklistBI.buscaChecklistCompleto(idcheck);
                } catch (Exception e) {
                    logger.info("Ocurrió algo  al retornar regChecklistUsuario");
                }

                if (reg != null) {

                    Iterator<Map.Entry<String, Object>> enMap = reg.entrySet().iterator();
                    while (enMap.hasNext()) {
                        Map.Entry<String, Object> enMaps = enMap.next();
                        String nomH = enMaps.getKey();
                        if (nomH == "checklistCompleto") {
                            regChecklist = (List<ChecklistCompletoDTO>) enMaps.getValue();
                        }
                        if (nomH == "listaUsuarios") {
                            regUsuariosADTO = (List<Usuario_ADTO>) enMaps.getValue();
                        }
                    }
                    if (regChecklist != null) {
                        Iterator<ChecklistCompletoDTO> it = regChecklist.iterator();
                        Iterator<ChecklistCompletoDTO> it1 = regChecklist.iterator();
                        Iterator<ChecklistCompletoDTO> it2 = regChecklist.iterator();
                        Iterator<ChecklistCompletoDTO> it3 = regChecklist.iterator();
                        Iterator<ChecklistCompletoDTO> it4 = regChecklist.iterator();

                        JsonObject object01 = null;
                        JsonObject object1 = null;
                        JsonObject object2 = null;
                        JsonObject object3 = null;
                        JsonObject object4 = null;

                        boolean comparaobj1 = true;
                        boolean comparaobj2 = true;
                        boolean comparaobj3 = true;
                        boolean comparaobj4 = true;
                        while (it.hasNext()) {
                            ChecklistCompletoDTO o = it.next();
                            object1 = new JsonObject();
                            object2 = new JsonObject();
                            object3 = new JsonObject();
                            object4 = new JsonObject();

                            // modulo FRTAMODULO
                            if (comparaobj1) {
                                object1.addProperty("id", o.getIdModulo());
                                object1.addProperty("nombre", o.getNombreModulo());
                                object1.addProperty("idPadre", o.getIdModuloPadre());
                                object1.addProperty("nomPadre", o.getNombrePadre());
                                comparaobj1 = false;
                                array1.add(object1);
                            } else if (o != null) {
                                ChecklistCompletoDTO o1 = it1.next();
                                if (o1.getIdModulo() != o.getIdModulo()) {
                                    object1.addProperty("id", o.getIdModulo());
                                    object1.addProperty("nombre", o.getNombreModulo());
                                    object1.addProperty("idPadre", o.getIdModuloPadre());
                                    object1.addProperty("nomPadre", o.getNombrePadre());
                                    array1.add(object1);
                                }
                            }
                            // tipoPreg FRCTTIPO_PREG
                            if (comparaobj2) {
                                object2.addProperty("id", o.getIdTipoPregunta());
                                object2.addProperty("clave", o.getCvePregunta());
                                object2.addProperty("descripcion", o.getDescripcionTipo());
                                comparaobj2 = false;
                                array2.add(object2);
                            } else if (o != null) {
                                ChecklistCompletoDTO o2 = it2.next();
                                if (o2.getIdTipoPregunta() != o.getIdTipoPregunta()) {
                                    object2.addProperty("id", o.getIdTipoPregunta());
                                    object2.addProperty("clave", o.getCvePregunta());
                                    object2.addProperty("descripcion", o.getDescripcionTipo());
                                    array2.add(object2);
                                }
                            }

                            // preguntas FRTAPREGUNTA
                            if (comparaobj3) {
                                object3.addProperty("id", o.getIdPregunta());
                                object3.addProperty("idModulo", o.getIdModulo());
                                object3.addProperty("idTipo", o.getIdTipoPregunta());
                                object3.addProperty("descripcion", o.getPregunta());
                                object3.addProperty("orden", o.getOrdenPregunta());
                                comparaobj3 = false;
                                array3.add(object3);
                            } else if (o != null) {
                                ChecklistCompletoDTO o3 = it3.next();
                                if (o3.getIdPregunta() != o.getIdPregunta()) {
                                    object3.addProperty("id", o.getIdPregunta());
                                    object3.addProperty("idModulo", o.getIdModulo());
                                    object3.addProperty("idTipo", o.getIdTipoPregunta());
                                    object3.addProperty("descripcion", o.getPregunta());
                                    object3.addProperty("orden", o.getOrdenPregunta());
                                    array3.add(object3);
                                }
                            }
                            // resDes FRTARES_DES
                            if (comparaobj4) {
                                object4.addProperty("id", o.getIdArbolDesicion());
                                object4.addProperty("idCheck", idcheck);
                                object4.addProperty("idPreg", o.getIdPregunta());
                                object4.addProperty("respuesta", o.getRespuesta());
                                object4.addProperty("estatusE", o.getEstatusEvidencia());
                                object4.addProperty("ordenCheck", o.getOrdenCheckRespuesta());
                                object4.addProperty("reqAccion", o.getReqAccion());
                                object4.addProperty("reqObs", o.getReqObs());
                                object4.addProperty("obliga", o.getObliga());
                                object4.addProperty("etiqueta", o.getDesEvidencia());
                                comparaobj4 = false;
                                array4.add(object4);
                            } else if (o != null) {
                                ChecklistCompletoDTO o4 = it4.next();
                                if (o4.getIdArbolDesicion() != o.getIdArbolDesicion()) {
                                    object4.addProperty("id", o.getIdArbolDesicion());
                                    object4.addProperty("idCheck", idcheck);
                                    object4.addProperty("idPreg", o.getIdPregunta());
                                    object4.addProperty("respuesta", o.getRespuesta());
                                    object4.addProperty("estatusE", o.getEstatusEvidencia());
                                    object4.addProperty("ordenCheck", o.getOrdenCheckRespuesta());
                                    object4.addProperty("reqAccion", o.getReqAccion());
                                    object4.addProperty("reqObs", o.getReqObs());
                                    object4.addProperty("obliga", o.getObliga());
                                    object4.addProperty("etiqueta", o.getDesEvidencia());
                                    array4.add(object4);
                                }
                            }
                        }

                        if (regUsuariosADTO != null) {
                            Iterator<Usuario_ADTO> it0 = regUsuariosADTO.iterator();
                            while (it0.hasNext()) {
                                Usuario_ADTO oUS = it0.next();
                                object01 = new JsonObject();
                                // modulo USUARIOS
                                object01.addProperty("id", oUS.getIdUsuario());
                                object01.addProperty("nombre", oUS.getNombre());
                                array0.add(object01);
                            }
                        }

                        object = new JsonObject();
                        object.add("modulo", array1);
                        object.add("tipoPreg", array2);
                        object.add("preguntas", array3);
                        object.add("resDes", array4);

                        princ.add("visita", object);
                        princ.add("usuarios", array0);

                        princ.addProperty("fecha", fecha);
                        json = princ.toString();

                    } else {
                        princ.addProperty("fecha", fecha);
                        json = princ.toString();
                    }

                } else {
                    princ.addProperty("fecha", fecha);
                    json = princ.toString();
                }
            } else {
                princ.addProperty("fecha", fecha);
                json = princ.toString();
            }
            response.setContentType("application/json");
            response.getWriter().write(json);
        } catch (Exception e) {
            logger.info("Ocurrió algo  en regChecklistUsuario " + e);
        }

        return null;
    }

    // servicios/bitacora.json?id=191312&idCheck=1
    @SuppressWarnings("static-access")
    // servicios/bitacora.json?KYUXWXqY5lkJ2vt8NyDBXtgRzFF/qJGHQ64Gz/6wH/Y=
    @RequestMapping(value = "/checkBitacora", method = RequestMethod.GET)
    public @ResponseBody
    String getBitacora(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        uri = uri.split("&")[0];

        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (descifra.decryptParams(uri));
        }

        // String urides = descifra.decryptParams(uri);
        String uriinf = urides.split("&")[1];
        int uriidBit = Integer.parseInt(uriinf.split("=")[1]);

        String json = "";
        JsonObject object = new JsonObject();
        int regBitacora = 0;

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);

        try {
            if (uriidBit > 0) {
                try {
                    regBitacora = bitacoraBI.verificaBitacora(uriidBit);
                } catch (Exception e) {
                    logger.info("Ocurrió algo  al retornar bitacora");
                }
                if (regBitacora > 0) {
                    // LoginDTO userSession = new LoginDTO();
                    object.addProperty("regBitacora", regBitacora);
                    object.addProperty("fecha", fecha);
                    json = object.toString();
                } else {
                    object.addProperty("regBitacora", 0);
                    object.addProperty("fecha", fecha);
                    json = object.toString();
                }
            } else {
                object.addProperty("regBitacora", 0);
                object.addProperty("fecha", fecha);
                json = object.toString();
            }
            response.setContentType("application/json");
            response.getWriter().write(json);

        } catch (Exception e) {
            logger.info("Ocurrió algo  en bitacora " + e);
        }
        return null;
    }

    // servicios/respuesta.json?id=191312
    // servicios/respuesta.json?/LrmW7p+t0tWWlzvO5N3vg===
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/respuesta", method = RequestMethod.POST)
    public @ResponseBody
    String addRespuesta(@RequestBody String provider, HttpServletRequest request)
            throws KeyException, GeneralSecurityException, IOException {
        //System.out.println(":::::::::::::::::::::DENTRO DE RESPUESTA:::::::::::::::::::::");
        /*
		 * //System.out.println("SI ENTRO MIKE AL SERVICIO!!!!!");
		 * //System.out.println("LO QUE ME MANDA MIKE____________");
		 * //System.out.println(provider);
		 * //System.out.println("____________________________"); String result =
		 * java.net.URLDecoder.decode(provider, "UTF-8"); result =
		 * result.split("finiOSswift")[0].split("inicioiOSswift")[1];
         */

        UtilCryptoGS descifra = new UtilCryptoGS();
        Gson gson = new Gson();
        String aux = "";

        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

        if (provider != null) {

            String json = "";

            if (idLlave == 7) {
                try {
                    //System.out.println(provider);
                    String result = provider.split("finiOSswift")[0].split("inicioiOSswift")[1];
                    //System.out.println("LO QUE TRAE RESULT------>" + result);
                    result = java.net.URLDecoder.decode(result, "UTF-8");
                    //System.out.println("DECODEADO----->" + result);
                    result = result.replaceAll(" ", "+");
                    //System.out.println("DESCODEADO Y MANEJADO----->" + result);
                    // :::::: CAMBIAMOS EL METODO DE LA CLASE StrCripher POR
                    // decrypt2(?)
                    json = (cifraIOS.decrypt2(result, FRQConstantes.getLlaveEncripcionLocalIOS()));
                    //System.out.println("A VER SI ES CIERTO--->" + json);
                    json = json.replaceAll("iOsCI", "[");
                    json = json.replaceAll("iOSCD", "]");
                    System.out.println("RESPUESTA---->" + json);
                } catch (Exception e) {
                    //System.out.println("Ocurrió algo  en servicio respuesta" + e);
                    return "Ocurrió algo ";
                }
            } else if (idLlave == 666) {
                json = (descifra.decryptParams(provider));
                System.out.println("RESPUESTA---->" + json);
            }

            // //System.out.println(json);
            logger.info("JSON ENVIADO:\n " + json);

            // String json = descifra.decryptParams(provider);
            respuestaRespuestaDTO = gson.fromJson(json, RegistroRespuestaDTO.class);
            boolean respuestas = respuestaBI.registraRespuestas(respuestaRespuestaDTO);
            if (respuestas) {
                aux = "Realizado";
            } else {
                aux = "Ocurrió algo ";
            }

        } else {
            aux = "Ocurrió algo ";
        }
        return aux;
    }

    // SERVICIO PARA AGREGAR RESPUESTAS SIN ENCRIPTADO
    @SuppressWarnings({"unused"})
    @RequestMapping(value = "/alternativo", method = RequestMethod.POST)
    public @ResponseBody
    String alternativo(@RequestBody String provider, HttpServletRequest request)
            throws KeyException, GeneralSecurityException, IOException {
        //System.out.println(":::::::::::::::::::::DENTRO DE ALTERNATIVO:::::::::::::::::::::");
        UtilCryptoGS descifra = new UtilCryptoGS();
        Gson gson = new Gson();
        String aux = "";

        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

        if (provider != null) {
            String json = "";
            if (idLlave == 7) {
                try {
                    String result = provider.split("finiOSswift")[0].split("inicioiOSswift")[1];
                    result = java.net.URLDecoder.decode(result, "UTF-8");
                    result = result.replaceAll(" ", "+");
                    json = new String(Base64.getDecoder().decode(result.getBytes()));
                    json = json.replaceAll("iOsCI", "[");
                    json = json.replaceAll("iOSCD", "]");
                } catch (Exception e) {
                    logger.info("Ocurrió algo  en servicio alternativo" + e);
                }

            } else if (idLlave == 666) {
                json = (descifra.decryptParams(provider));
            }

            // String json = descifra.decryptParams(provider);
            respuestaRespuestaDTO = gson.fromJson(json, RegistroRespuestaDTO.class);
            boolean respuestas = respuestaBI.registraRespuestas(respuestaRespuestaDTO);
            if (respuestas) {
                aux = "Realizado";
            } else {
                aux = "Ocurrió algo ";
            }

        } else {
            aux = "Ocurrió algo ";
        }
        return aux;
    }

    // servicios/tipoCifrado.json?id=191312&tipoSer=1&tipoApp=1
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/tipoCifrado", method = RequestMethod.POST)
    public @ResponseBody
    String tipoCifrado(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS descifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        String uri = (request.getQueryString()).split("&")[0];
        String aux = "";
        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (descifra.decryptParams(uri));
        }

        int id = Integer.parseInt((urides.split("&")[0]).split("=")[1]);
        int tipoSer = Integer.parseInt((urides.split("&")[1]).split("=")[1]);
        int tipoApp = Integer.parseInt((urides.split("&")[2]).split("=")[1]);

        if (provider != null) {
            if (idLlave == 7) {
                try {
                    String result = provider.split("finiOSswift")[0].split("inicioiOSswift")[1];
                    result = java.net.URLDecoder.decode(result, "UTF-8");
                    result = result.replaceAll(" ", "+");
                    provider = result.replaceAll("iOsCI", "[");
                    provider = result.replaceAll("iOSCD", "]");
                } catch (Exception e) {
                    logger.info("FALLO EN EL SERVICIO DE tipoCofrado" + e.getStackTrace());
                }
            }
            // provider=(java.net.URLDecoder.decode(provider,
            // "UTF-8")).replaceAll("token=", "");

            boolean resultado = false;
            TipoCifradoDTO tipoCifradoDTO = new TipoCifradoDTO();
            tipoCifradoDTO.setNumEmpleado(id);
            tipoCifradoDTO.setTipoServicio(tipoSer);
            tipoCifradoDTO.setTipoApp(tipoApp);
            tipoCifradoDTO.setFecha(null);
            tipoCifradoDTO.setJson(provider);

            try {
                resultado = tipoCifradoBI.insertaTipoCifrado(tipoCifradoDTO);
                if (resultado) {
                    aux = "Realizado";
                } else {
                    aux = "Ocurrió algo ";
                }
            } catch (Exception e) {
                //logger.info("Ocurrió algo  en servicio tipoCifrado... " + e);
            }
        } else {
            aux = "Ocurrió algo ";
        }
        return aux;
    }

    // servicios/infoMovil.json?id=191312&idSO=1&version=1&modelo=w810&fabricante=samsung&numMovil=1234&tipoCon=0&identif=abc
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/infoMovil", method = RequestMethod.GET)
    public @ResponseBody
    String infoMovil(HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS descifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        String uri = (request.getQueryString()).split("&")[0];
        String urides = "";
        String json = "";
        JsonObject object = new JsonObject();
        boolean resultado = false;
        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (descifra.decryptParams(uri));
        }

        int id = Integer.parseInt((urides.split("&")[0]).split("=")[1]);
        String idSO = (urides.split("&")[1]).split("=")[1];
        String version = (urides.split("&")[2]).split("=")[1];
        String modelo = (urides.split("&")[3]).split("=")[1];
        String fabricante = (urides.split("&")[4]).split("=")[1];
        String numMovil = (urides.split("&")[5]).split("=")[1];
        String tipoCon = (urides.split("&")[6]).split("=")[1];
        String identif = (urides.split("&")[7]).split("=")[1];

        MovilInfoDTO movilInfoDTO = new MovilInfoDTO();
        movilInfoDTO.setIdUsuario(id);
        movilInfoDTO.setSo(idSO);
        movilInfoDTO.setVersion(version);
        movilInfoDTO.setModelo(modelo);
        movilInfoDTO.setFabricante(fabricante);
        movilInfoDTO.setNumMovil(numMovil);
        movilInfoDTO.setTipoCon(tipoCon);
        movilInfoDTO.setIdentificador(identif);
        movilInfoDTO.setFecha(null);

        try {
            resultado = movilInfoBI.insertaInfoMovil(movilInfoDTO);

            if (resultado) {
                object.addProperty("nombre", "aceptado");
                object.addProperty("fecha", fecha);
                json = object.toString();
            } else {
                object.addProperty("nombre", "null");
                object.addProperty("fecha", fecha);
                json = object.toString();
            }

            response.setContentType("application/json");
            response.getWriter().write(json);
        } catch (Exception e) {
            logger.info("Ocurrió algo  en servicio infoMovil... " + e);
        }
        return null;
    }

    // servicios/checklistOffline.json?id=191312
    // servicios/checklistOffline.json?Lg/M4L45zj4Zl05KmbXZmB0FfUnUG08UnAcW0OXp8q0=
    @SuppressWarnings({"unchecked", "static-access"})
    @RequestMapping(value = "/checklistOffline", method = RequestMethod.GET)
    public @ResponseBody
    String getChecklistOfflineDTOInJSON(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        uri = uri.split("&")[0];

        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        // String urides = cifra.decryptParams(uri);
        int use = Integer.parseInt(urides.split("=")[1]);

        Map<String, Object> regChecklistOffline = null;
        JsonArray array = new JsonArray();
        JsonObject object = null;
        JsonObject princ = new JsonObject();

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);
        String json = null;

        try {
            if (use > 0) {
                try {
                    regChecklistOffline = checksOfflineBI.obtieneChecksOffline(use);
                } catch (Exception e) {
                    logger.info("Ocurrió algo  al retornar el Map regChecklistOffline");
                }
                if (regChecklistOffline != null) {
                    List<ChecksOfflineDTO> listaChecksOfflineDTO = null;
                    Map<String, Object> mapGeneralChecksOfflineCompletosDTO = null;
                    List<Object> listaUsuariosChecksOfflineCompletosDTO = null;
                    Iterator<Map.Entry<String, Object>> entries = regChecklistOffline.entrySet().iterator();
                    while (entries.hasNext()) {
                        Map.Entry<String, Object> entry = entries.next();
                        String nom = entry.getKey();
                        if (nom == "checklists") {
                            listaChecksOfflineDTO = (List<ChecksOfflineDTO>) entry.getValue();
                        } else if (nom == "checklistsCompletos") {
                            mapGeneralChecksOfflineCompletosDTO = (Map<String, Object>) entry.getValue();
                        } else if (nom == "listaUsuarios") {
                            listaUsuariosChecksOfflineCompletosDTO = (List<Object>) entry.getValue();
                        }
                    }
                    List<ChecksOfflineCompletosDTO> listaGeneralChecksOfflineCompletosDTO = null;

                    //Iterator<ChecksOfflineDTO> it=null;
                    if (listaChecksOfflineDTO != null) {
                        Iterator<ChecksOfflineDTO> it = listaChecksOfflineDTO.iterator();

                        while (it.hasNext()) {
                            ChecksOfflineDTO o = it.next();

                            object = new JsonObject();
                            JsonObject object1 = new JsonObject();
                            JsonObject object2 = new JsonObject();
                            JsonObject object3 = new JsonObject();
                            // tipoCheck
                            object1.addProperty("id", o.getIdTipoCheck());
                            object1.addProperty("descripcion", o.getDescripcion());
                            // checkList
                            object2.addProperty("id", o.getIdCheck());
                            object2.addProperty("nombre", o.getNombreCheck());
                            object2.addProperty("fechaInicio", o.getFechaInicio());
                            object2.addProperty("fechaFin", o.getFechaFin());
                            // checkUsuario
                            object3.addProperty("id", o.getIdCheckUsua());

                            object.add("tipoCheck", object1);
                            object.add("checkList", object2);
                            object.add("checkUsuario", object3);
                            // nombre
                            object.addProperty("idCeco", o.getIdCeco());
                            object.addProperty("nombre", o.getNombreCeco());
                            object.addProperty("zona", o.getZona());
                            object.addProperty("latitud", o.getLatitud());
                            object.addProperty("longitud", o.getLongitud());
                            object.addProperty("ultimaVisita", o.getUltimaVisita());

                            /* EMPIEZA EL CHECKCOMPLETO */
                            JsonArray array1 = new JsonArray();
                            JsonArray array2 = new JsonArray();
                            JsonArray array3 = new JsonArray();
                            JsonArray array4 = new JsonArray();
                            JsonObject object01 = null;

                            if (mapGeneralChecksOfflineCompletosDTO != null) {
                                listaGeneralChecksOfflineCompletosDTO = (List<ChecksOfflineCompletosDTO>) mapGeneralChecksOfflineCompletosDTO
                                        .get("" + o.getIdCheckUsua());
                                if (listaGeneralChecksOfflineCompletosDTO != null) {
                                    Iterator<ChecksOfflineCompletosDTO> it0 = listaGeneralChecksOfflineCompletosDTO
                                            .iterator();
                                    Iterator<ChecksOfflineCompletosDTO> it1 = listaGeneralChecksOfflineCompletosDTO
                                            .iterator();
                                    Iterator<ChecksOfflineCompletosDTO> it2 = listaGeneralChecksOfflineCompletosDTO
                                            .iterator();
                                    Iterator<ChecksOfflineCompletosDTO> it3 = listaGeneralChecksOfflineCompletosDTO
                                            .iterator();
                                    Iterator<ChecksOfflineCompletosDTO> it4 = listaGeneralChecksOfflineCompletosDTO
                                            .iterator();

                                    JsonObject object11 = null;
                                    JsonObject object12 = null;
                                    JsonObject object13 = null;
                                    JsonObject object14 = null;

                                    boolean comparaobj1 = true;
                                    boolean comparaobj2 = true;
                                    boolean comparaobj3 = true;
                                    boolean comparaobj4 = true;
                                    while (it0.hasNext()) {
                                        ChecksOfflineCompletosDTO oC = it0.next();
                                        object11 = new JsonObject();
                                        object12 = new JsonObject();
                                        object13 = new JsonObject();
                                        object14 = new JsonObject();

                                        // modulo FRTAMODULO
                                        if (comparaobj1) {
                                            object11.addProperty("id", oC.getIdModulo());
                                            object11.addProperty("nombre", oC.getNombreModulo());
                                            object11.addProperty("idPadre", oC.getIdModuloPadre());
                                            object11.addProperty("nomPadre", oC.getModuloPadre());
                                            comparaobj1 = false;
                                            array1.add(object11);
                                        } else if (oC != null) {
                                            ChecksOfflineCompletosDTO o1 = it1.next();
                                            if (o1.getIdModulo() != oC.getIdModulo()) {
                                                object11.addProperty("id", oC.getIdModulo());
                                                object11.addProperty("nombre", oC.getNombreModulo());
                                                object11.addProperty("idPadre", oC.getIdModuloPadre());
                                                object11.addProperty("nomPadre", oC.getModuloPadre());
                                                array1.add(object11);
                                            }
                                        }
                                        // tipoPreg FRCTTIPO_PREG
                                        if (comparaobj2) {
                                            object12.addProperty("id", oC.getIdTipoPreg());
                                            object12.addProperty("clave", oC.getCveTipoPregunta());
                                            object12.addProperty("descripcion", oC.getDescripcionTipo());
                                            comparaobj2 = false;
                                            array2.add(object12);
                                        } else if (oC != null) {
                                            ChecksOfflineCompletosDTO o2 = it2.next();
                                            if (o2.getIdTipoPreg() != oC.getIdTipoPreg()) {
                                                object12.addProperty("id", oC.getIdTipoPreg());
                                                object12.addProperty("clave", oC.getCveTipoPregunta());
                                                object12.addProperty("descripcion", oC.getDescripcionTipo());
                                                array2.add(object12);
                                            }
                                        }

                                        // preguntas FRTAPREGUNTA
                                        if (comparaobj3) {
                                            object13.addProperty("id", oC.getIdPregunta());
                                            object13.addProperty("idModulo", oC.getIdModulo());
                                            object13.addProperty("idTipo", oC.getIdTipoPreg());
                                            object13.addProperty("descripcion", oC.getPregunta());
                                            object13.addProperty("orden", oC.getOrdepregunta());
                                            object13.addProperty("pregPadre", oC.getPreguntaPadre());
                                            comparaobj3 = false;
                                            array3.add(object13);
                                        } else if (oC != null) {
                                            ChecksOfflineCompletosDTO o3 = it3.next();
                                            if (o3.getIdPregunta() != oC.getIdPregunta()) {
                                                object13.addProperty("id", oC.getIdPregunta());
                                                object13.addProperty("idModulo", oC.getIdModulo());
                                                object13.addProperty("idTipo", oC.getIdTipoPreg());
                                                object13.addProperty("descripcion", oC.getPregunta());
                                                object13.addProperty("orden", oC.getOrdepregunta());
                                                object13.addProperty("pregPadre", oC.getPreguntaPadre());
                                                array3.add(object13);
                                            }
                                        }
                                        // resDes FRTARES_DES
                                        if (comparaobj4) {
                                            object14.addProperty("id", oC.getIdArbolDes());
                                            object14.addProperty("idCheck", oC.getIdCheckUsua());
                                            object14.addProperty("idPreg", oC.getIdPregunta());
                                            object14.addProperty("respuesta", oC.getRespuesta());
                                            object14.addProperty("estatusE", oC.getEstatusEvidencia());
                                            object14.addProperty("ordenCheck", oC.getSiguienteOrden());
                                            object14.addProperty("reqAccion", oC.getReqAccion());
                                            object14.addProperty("reqObs", oC.getReqObs());
                                            object14.addProperty("obliga", oC.getObliga());
                                            object14.addProperty("etiqueta", oC.getEtiqueta());
                                            object14.addProperty("idProtocolo", oC.getIdProtocolo());

                                            comparaobj4 = false;
                                            array4.add(object14);
                                        } else if (oC != null) {
                                            ChecksOfflineCompletosDTO o4 = it4.next();
                                            if (o4.getIdArbolDes() != oC.getIdArbolDes()) {
                                                object14.addProperty("id", oC.getIdArbolDes());
                                                object14.addProperty("idCheck", oC.getIdCheckUsua());
                                                object14.addProperty("idPreg", oC.getIdPregunta());
                                                object14.addProperty("respuesta", oC.getRespuesta());
                                                object14.addProperty("estatusE", oC.getEstatusEvidencia());
                                                object14.addProperty("ordenCheck", oC.getSiguienteOrden());
                                                object14.addProperty("reqAccion", oC.getReqAccion());
                                                object14.addProperty("reqObs", oC.getReqObs());
                                                object14.addProperty("obliga", oC.getObliga());
                                                object14.addProperty("etiqueta", oC.getEtiqueta());

                                                object14.addProperty("idProtocolo", oC.getIdProtocolo());

                                                array4.add(object14);
                                            }
                                        }
                                    }

                                    object01 = new JsonObject();
                                    object01.add("modulo", array1);
                                    object01.add("tipoPreg", array2);
                                    object01.add("preguntas", array3);
                                    object01.add("resDes", array4);

                                    object.add("visita", object01);
                                    object.addProperty("bitacora", o.getIdBitacora());

                                }
                            } else {
                                object.addProperty("fecha", fecha);
                            }

                            /* TERMINA EL COMPLETO */
                            array.add(object);

                        }
                    }

                    JsonArray array0 = new JsonArray();
                    JsonObject object02 = null;

                    if (listaUsuariosChecksOfflineCompletosDTO != null) {

                        Iterator<Object> it01 = listaUsuariosChecksOfflineCompletosDTO.iterator();
                        while (it01.hasNext()) {
                            List<Usuario_ADTO> listaUsuarios = (List<Usuario_ADTO>) it01.next();
                            if (listaUsuarios != null) {
                                Iterator<Usuario_ADTO> it02 = listaUsuarios.iterator();
                                while (it02.hasNext()) {
                                    Usuario_ADTO objUsuarios = (Usuario_ADTO) it02.next();
                                    object02 = new JsonObject();
                                    // modulo USUARIOS
                                    object02.addProperty("id", objUsuarios.getIdUsuario());
                                    object02.addProperty("nombre", objUsuarios.getNombre());
                                    object02.addProperty("sucursal", objUsuarios.getDesPuestof());
                                    array0.add(object02);
                                }
                            }
                        }
                    }
                    princ.add("tiendasProx", array);
                    princ.add("usuarios", array0);
                    princ.addProperty("fecha", fecha);
                    json = princ.toString();

                } else {
                    princ.add("tiendasProx", array);
                    princ.addProperty("fecha", fecha);
                    json = princ.toString();
                }
            } else {
                princ.add("tiendasProx", array);
                princ.addProperty("fecha", fecha);
                json = princ.toString();
            }
            response.setContentType("application/json");
            response.getWriter().write(json);
        } catch (Exception e) {
            logger.info("Ocurrió algo  en servicio regChecklistOffline " + e);
        }
        return null;
    }

    // servicio para enviar una pregunta
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/preguntaOnline", method = RequestMethod.POST)
    public @ResponseBody
    boolean setPreguntaOnline(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        Gson gson = new Gson();

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        //logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");

                json = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("JSON IOS: " + json);
                respuestaRespuestaDTO = gson.fromJson(json, RegistroRespuestaDTO.class);
                boolean respuestas = respuestaBI.insertaUnaRespuesta(respuestaRespuestaDTO);
                //logger.info("RESPUESTA DTO: "+respuestaRespuestaDTO.toString());
                if (respuestas) {
                    logger.info("SE AGREGO CORRECTAMENTE");
                    return true;

                }

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));

                json = descifra.decryptParams(provider);

                //logger.info("JSON ANDROID: " + json);
                respuestaRespuestaDTO = gson.fromJson(json, RegistroRespuestaDTO.class);
                boolean respuestas = respuestaBI.insertaUnaRespuesta(respuestaRespuestaDTO);
                //logger.info("RESPUESTA DTO: "+respuestaRespuestaDTO.toString());
                if (respuestas) {
                    logger.info("SE AGREGO CORRECTAMENTE");
                    return true;

                }

            }
        } catch (Exception e) {
            logger.info("Ocurrió algo  " + e.getMessage());
        }

        return false;
    }

    // servicio para enviar una pregunta
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/preguntaOnlineEliminar", method = RequestMethod.POST)
    public @ResponseBody
    boolean setPreguntaOnlineEliminar(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        Gson gson = new Gson();

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");

                json = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("JSON IOS: " + json);
                respuestaRespuestaDTO = gson.fromJson(json, RegistroRespuestaDTO.class);
                boolean respuestas = respuestaBI.eliminaRespuestas(respuestaRespuestaDTO);
                if (respuestas) {
                    logger.info("SE ELIMINO CORRECTAMENTE");
                    return true;

                }

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));

                json = descifra.decryptParams(provider);

                //logger.info("JSON ANDROID: " + json);
                respuestaRespuestaDTO = gson.fromJson(json, RegistroRespuestaDTO.class);
                boolean respuestas = respuestaBI.eliminaRespuestas(respuestaRespuestaDTO);
                if (respuestas) {
                    logger.info("SE ELIMINO CORRECTAMENTE");
                    return true;

                }

            }
        } catch (Exception e) {
            logger.info("Ocurrió algo  ");
        }

        return false;
    }

    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getCompromisosUsuario", method = RequestMethod.GET)
    public @ResponseBody
    String getCompromisosUsuario(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        List<CompromisoDTO> compromisos = null;

        Gson g = new Gson();

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        int usuario = Integer.parseInt(urides.split("=")[1].split("&")[0]);
        int idChecklist = Integer.parseInt(urides.split("&")[1].split("=")[1]);
        int idCeco = Integer.parseInt(urides.split("&")[2].split("=")[1]);

        logger.info("OBTENER COMPROMISOS DEL USUARIO:  " + usuario);

        try {
            compromisos = checklistBI.compromisosChecklist(idChecklist, idCeco);

        } catch (Exception e) {
            compromisos = null;
            logger.info("Ocurrió algo  AL OBTENER LOS COMPROMISOS " + e.getMessage());
        }

        String jsonSalida = "";
        jsonSalida = "{\"compromisosSucursal\":" + g.toJson(compromisos) + "}";

        logger.info("JSON DE SALIDA: " + jsonSalida);

        return jsonSalida;
    }

    /*
	 * @SuppressWarnings( "static-access" )
	 *
	 * @RequestMapping(value = "/getCompromisosUsuario", method =
	 * RequestMethod.GET) public @ResponseBody Map<String,Object>
	 * getCompromisosUsuario(HttpServletRequest request, HttpServletResponse
	 * response) throws KeyException, GeneralSecurityException, IOException {
	 *
	 * UtilCryptoGS cifra = new UtilCryptoGS(); StrCipher cifraIOS = new
	 * StrCipher(); String uri = request.getQueryString(); String uriAp =
	 * request.getParameter("token"); int idLlave =
	 * Integer.parseInt((uriAp.split(";")[1]).split("=")[1]); uri =
	 * uri.split("&")[0]; String urides = ""; String json = "";
	 * Map<String,Object> compromisos=null;
	 *
	 * // se lee los valores enviados para la pregunta UtilCryptoGS descifra =
	 * new UtilCryptoGS();
	 *
	 *
	 * if (idLlave == 7) { urides = (cifraIOS.decrypt(uri,
	 * FRQConstantes.getLlaveEncripcionLocalIOS())); } else if (idLlave == 666)
	 * { urides = (cifra.decryptParams(uri)); }
	 *
	 * int usuario = Integer.parseInt(urides.split("=")[1]);
	 *
	 * logger.info("OBTENER COMPROMISOS DEL USUARIO:  "+usuario);
	 *
	 * try { compromisos= checklistBI.compromisosChecklist(usuario); } catch
	 * (Exception e) { compromisos=null; logger.info(
	 * "Ocurrió algo  AL OBTENER LOS COMPROMISOS "+e.getMessage()); } return
	 * compromisos; }
     */
    // servicio para enviar una pregunta
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/estadisticaDispositivo", method = RequestMethod.POST)
    public @ResponseBody
    boolean setEstadisticaDispositivo(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        Gson gson = new Gson();

        String fabricante = null;
        String identificador = null;
        String modelo = null;
        String so = null;
        String versionSO = null;
        String numCelular = null;
        String numEmpleado = null;
        String tipoConexion = null;
        String versionApp = null;
        String token = null;

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");

                json = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("JSON IOS: " + json);
                JSONObject jsonObject = new JSONObject(json);

                fabricante = jsonObject.getString("fabricante");
                identificador = jsonObject.getString("identificador");
                modelo = jsonObject.getString("modelo");
                so = jsonObject.getString("so");
                versionSO = jsonObject.getString("versionSO");
                numCelular = jsonObject.getString("numCelular");
                numEmpleado = jsonObject.getString("numEmpleado");
                tipoConexion = jsonObject.getString("tipoConexion");
                versionApp = jsonObject.getString("versionApp");
                token = jsonObject.getString("tokenUsuario");

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));

                json = descifra.decryptParams(provider);

                //logger.info("JSON ANDROID: " + json);
                JSONObject jsonObject = new JSONObject(json);

                fabricante = jsonObject.getString("fabricante");
                identificador = jsonObject.getString("identificador");
                modelo = jsonObject.getString("modelo");
                so = jsonObject.getString("so");
                versionSO = jsonObject.getString("versionSO");
                numCelular = jsonObject.getString("numCelular");
                numEmpleado = jsonObject.getString("numEmpleado");
                tipoConexion = jsonObject.getString("tipoConexion");
                versionApp = jsonObject.getString("versionApp");
                token = jsonObject.getString("tokenUsuario");

            }

            int usuario = Integer.parseInt(urides.split("=")[1]);

            MovilInfoDTO movilInfoDTO = new MovilInfoDTO();
            movilInfoDTO.setIdUsuario(usuario);
            movilInfoDTO.setSo(so);
            movilInfoDTO.setVersion(versionSO);
            movilInfoDTO.setModelo(modelo);
            movilInfoDTO.setFabricante(fabricante);
            movilInfoDTO.setNumMovil(numCelular);
            movilInfoDTO.setTipoCon(tipoConexion);
            movilInfoDTO.setVersionApp(versionApp);
            movilInfoDTO.setIdentificador(identificador);
            movilInfoDTO.setToken(token);

            logger.info("MOVIL DTO: " + movilInfoDTO);

            boolean infoMovil = movilInfoBI.insertaInfoMovil(movilInfoDTO);

            if (!infoMovil) {
                logger.info("Ocurrió algo  AL INSERTA LA INFORMACIÓN DEL MOVIL");
            }
            return infoMovil;

        } catch (Exception e) {
            logger.info("Ocurrió algo  " + e.getMessage());
        }

        return false;
    }

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/enviarFechaTermino", method = RequestMethod.POST)
    public @ResponseBody
    boolean setFechaTermino(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        boolean retorno = false;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        int idBitacora = 0;
        String preCalificacion = "";
        String calificacionFinal = "";
        String fecha = "";

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");

                json = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                logger.info("JSON IOS BITACORA: " + json);

                JSONObject jsonObject = new JSONObject(json);
                idBitacora = jsonObject.getInt("idBitacora");
                fecha = jsonObject.getString("fecha");

                try {
                    preCalificacion = jsonObject.getDouble("precalificacion") + "";
                    calificacionFinal = jsonObject.getDouble("calificacionFinal") + "";

                    logger.info("PRECALIFICACION: " + preCalificacion);
                    logger.info("CALIFICACION_FINAL: " + calificacionFinal);

                } catch (Exception ex) {
                    logger.info("NO CONTIENE CALIFICACIONES");

                }

                boolean respuestas = checklistBI.actualizaFechatermino(fecha, idBitacora, preCalificacion, calificacionFinal);
                if (respuestas) {

                    logger.info("SE ACTUALIZO LA FECHA TERMINO CORRECTAMENTE");
                    //Verificar Bitacoras Hijas y padre
                    try {
                        int idBitPapa = bitacoraGralBI.ejecutaSP(idBitacora + "");
                        logger.info("Bitácora Papá Creada ---> + ID: " + idBitPapa);

                        if (idBitPapa != 0 && idBitPapa != 2) {
                            int resp = softOpeningBI.inserta2(idBitacora);
                            logger.info("softOpeningBI: " + resp);
                            logger.info("Bitácora Papá Creada ---> + ID: " + idBitPapa);
                            //logger.info(resp);

                            try {

                                //boolean cargaHallazgos = hallazgosExpBI.cargaHallazgos(idBitacora);
                                List<ChecklistHallazgosRepoDTO> lista = hallazgosRepoBI.obtieneDatosNego(Integer.toString(idBitacora));
                                int version = 0;
                                List<SucursalChecklistDTO> sucursales = sucursalChecklistBI.obtieneDatosSuc(lista.get(0).getCeco());
                                //String tipoNegocio = sucursal.get(0).getNegocio();
                                version = sucursales.get(0).getAux();
                                //boolean cargaHallazgos = hallazgosExpBI.cargaHallazgos(idBitacora);
                                //String tipoNegocio = lista.get(0).getNegocio();
                                boolean cargaHallazgos;
                                //if (tipoNegocio == "EKT"){
                                if (version == 1) {
                                    cargaHallazgos = hallazgosExpBI.cargaHallazgosUpdate(idBitacora);
                                } else {
                                    cargaHallazgos = hallazgosExpBI.cargaHallazgosUpdateNuevo(idBitacora);//probando luis
                                }
                                if (cargaHallazgos) {
                                    logger.info("HALLAZGOS CARGADOS DE LA BIT PAPÁ ---> " + idBitacora);
                                } else {
                                    logger.info("AP al CARGAR HALLAZGOS DE LA BIT PAPÁ ---> " + idBitacora);
                                }

                            } catch (Exception e) {
                                logger.info("AP al CARGAR HALLAZGOS DE LA BIT PAPÁ ---> " + idBitacora);
                            }

                        }

                    } catch (Exception e) {
                        logger.info("BITÁCORA PAPÁ SIN CREAR" + e);
                    }
                    //FIn Verificar Bitacoras Hijas y padre

                    retorno = true;

                }

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));

                json = descifra.decryptParams(provider);

                logger.info("JSON ANDROID BITACORA: " + json);

                JSONObject jsonObject = new JSONObject(json);
                idBitacora = jsonObject.getInt("idBitacora");
                fecha = jsonObject.getString("fecha");

                try {
                    preCalificacion = jsonObject.getDouble("precalificacion") + "";
                    calificacionFinal = jsonObject.getDouble("calificacionFinal") + "";

                    logger.info("PRECALIFICACION: " + preCalificacion);
                    logger.info("CALIFICACION_FINAL: " + calificacionFinal);

                } catch (Exception ex) {
                    logger.info("NO CONTIENE CALIFICACIONES");

                }

                boolean respuestas = checklistBI.actualizaFechatermino(fecha, idBitacora, preCalificacion, calificacionFinal);
                if (respuestas) {

                    logger.info("SE ACTUALIZO LA FECHA TERMINO CORRECTAMENTE");
                    //Verificar Bitacoras Hijas y padre
                    try {
                        logger.info("Bitácora Papá Creada ---> + ID: " + idBitacora);
                        int idBitPapa = bitacoraGralBI.ejecutaSP(idBitacora + "");

                        if (idBitPapa != 0 && idBitPapa != 2) {
                            int resp = softOpeningBI.inserta2(idBitacora);
                            logger.info("softOpeningBI: " + resp);
                            logger.info("Bitácora Papá Creada ---> + ID: " + idBitPapa);

                            try {
                                List<ChecklistHallazgosRepoDTO> lista = hallazgosRepoBI.obtieneDatosNego(Integer.toString(idBitacora));
                                //boolean cargaHallazgos = hallazgosExpBI.cargaHallazgos(idBitacora);
                                //String tipoNegocio = lista.get(0).getNegocio();
                                int version = 0;
                                List<SucursalChecklistDTO> sucursales = sucursalChecklistBI.obtieneDatosSuc(lista.get(0).getCeco());
                                //String tipoNegocio = sucursal.get(0).getNegocio();
                                version = sucursales.get(0).getAux();

                                boolean cargaHallazgos;
                                //if (tipoNegocio == "EKT"){
                                if (version == 1) {
                                    cargaHallazgos = hallazgosExpBI.cargaHallazgosUpdate(idBitacora);
                                } else {
                                    cargaHallazgos = hallazgosExpBI.cargaHallazgosUpdateNuevo(idBitacora);//probando luis
                                }

                                if (cargaHallazgos) {
                                    logger.info("HALLAZGOS CARGADOS DE LA BIT PAPÁ ---> " + idBitacora);
                                } else {
                                    logger.info("AP al CARGAR HALLAZGOS DE LA BIT PAPÁ ---> " + idBitacora);
                                }

                            } catch (Exception e) {
                                logger.info("AP al CARGAR HALLAZGOS DE LA BIT PAPÁ ---> " + idBitacora);
                            }

                        }

                    } catch (Exception e) {
                        logger.info("BITÁCORA PAPÁ SIN CREAR" + e);
                    }
                    //FIn Verificar Bitacoras Hijas y padre

                    retorno = true;

                }

            }
        } catch (Exception e) {
            logger.info(e.getMessage());
            retorno = false;
        }

        // empieza a ejecutar la lista de compromisos para enviar el correo
        int usuario = Integer.parseInt(urides.split("=")[1]);

        List<CompromisoDTO> listaCompromiso = new ArrayList<CompromisoDTO>();
        String letraNegocio = "";
        int numNegocio = 0;
        String numTienda = "";
        String idUsuario = "";
        int numChecklist = 0;
        try {
            logger.info("Obtiene la lista de compromisos... ");
            logger.info("idBitacora... " + idBitacora);
            listaCompromiso = checklistBI.compromisosChecklistWeb(idBitacora);

            if (listaCompromiso != null) {
                numTienda = listaCompromiso.get(0).getEconomico();
                idUsuario = listaCompromiso.get(0).getIdUsuario();
                numChecklist = listaCompromiso.get(0).getIdChecklist();
                numNegocio = listaCompromiso.get(0).getIdNegocio();
                // idUsuario="189870";
                logger.info("Numero de Negocio... " + numNegocio);
                logger.info("Numero de Tienda... " + numTienda);

                List<ParametroDTO> listaParametro = parametroBI.obtieneParametros("negocio" + numNegocio);

                if (listaParametro != null && listaParametro.size() > 0) {
                    letraNegocio = listaParametro.get(0).getValor();
                }
                logger.info("Letra de Negocio... " + letraNegocio);

                if (numTienda.length() == 4) {
                    numTienda = letraNegocio + numTienda;
                } else if (numTienda.length() == 3) {
                    numTienda = letraNegocio + "0" + numTienda;
                } else if (numTienda.length() == 2) {
                    numTienda = letraNegocio + "00" + numTienda;
                } else if (numTienda.length() == 1) {
                    numTienda = letraNegocio + "000" + numTienda;
                }

                logger.info("Numero de Negocio Final... " + numTienda);
                logger.info("idUsuario... " + idUsuario);
                logger.info("Numero de checklist... " + numChecklist);
            } else {
                logger.info("No cuenta con compromisos...");
            }
        } catch (Exception e1) {
            logger.info("Ocurrio algo: " + e1);
        }

        logger.info("listaCompromiso... " + listaCompromiso);

        UsuarioDTO user = new UsuarioDTO();
        user.setIdUsuario("" + usuario);
        user.setNombre(numTienda);

        //INICIO ENVIO ACUEDOS
        List<ParametroDTO> valida = null;
        String arr[] = null;
        boolean bandeCorreo = true;

        if (listaCompromiso != null) {
            try {
                valida = parametroBI.obtieneParametros("enviaCorreo");
                logger.info("Estado del activo " + valida.get(0).getActivo());
                if (valida.get(0).getActivo() == 1) {
                    bandeCorreo = true;
                    logger.info("Valores del parametro enviaCorreo " + valida.get(0).getValor());
                    arr = valida.get(0).getValor().split(",");
                    logger.info("Tamaño del array de parametros " + arr.length);
                    for (int i = 0; i < arr.length; i++) {
                        logger.info("Valor de parametro " + (i + 1) + ": " + arr[i]);
                        if (Integer.parseInt(arr[i]) == numChecklist) {
                            if (listaCompromiso != null) {
                                try {
                                    logger.info("Mandando correo... ");
                                    correoBI.sendMailAcuerdos(user, listaCompromiso);
                                    logger.info("Correo Enviado");
                                    bandeCorreo = false;
                                } catch (Exception e) {
                                    logger.info(e);
                                }
                            }
                        }
                    }
                    if (bandeCorreo) {
                        logger.info("Correo no enviado por que el checklist no existe en el activo");
                    }
                } else {
                    logger.info("Cerro bitacora pero no envio correo por que esta desactivado... ");
                }
            } catch (Exception e) {
                logger.info("Ocurrio algo " + e);
            }
        } else {
            logger.info("No envia correo por que la lista de compromisos esta vacia...");
        }
        //FIN ENVIO ACUERDOS

        //ENVIO DE PDF DE 5 CHECKS'
        List<ParametroDTO> validaPDF = null;
        Map<String, String> resultado = new HashMap<String, String>();
        String cecoAux = "";

        try {
            validaPDF = parametroBI.obtieneParametros("checklistsPDF");
            logger.info("Estado del activo " + validaPDF.get(0).getActivo());
            if (validaPDF.get(0).getActivo() == 1) {
                bandeCorreo = true;
                int banderaEnvio = 0;
                try {
                    logger.info("COMPROBANDO ENVIO DE PDF COMPROMISOS 5 CHECKS... ");
                    //VERUFICAMOS SI SE DEBE ENVIAR O NO
                    logger.info("VERIFICANDO ENVIO... ");
                    logger.info("idUsuario " + usuario);
                    logger.info("idBitacora " + idBitacora);

                    banderaEnvio = respuestaPdfBI.obtieneEnvioCompromiso(usuario, idBitacora);
                    logger.info("RESPUESTA DE VERIFICANDO ENVIO... " + banderaEnvio);
                    if (banderaEnvio == 1) {
                        logger.info("CONSULTA CECO POR BITACORA... ");
                        resultado = respuestaPdfBI.obtieneCecoPorBitacora(usuario, idBitacora);
                        String ceco = resultado.get("idCeco");
                        String cecoCom = resultado.get("idCeco");
                        String nNegocio = resultado.get("numNegocio");
                        logger.info("CECO QUE ESTOY REGRESANDO " + ceco);
                        logger.info("NUMERO NEGOCIO QUE ESTOY REGRESANDO " + nNegocio);

                        letraNegocio = (parametroBI.obtieneParametros("negocio" + nNegocio)).get(0).getValor();
                        logger.info("Letra de Negocio... " + letraNegocio);

                        if (ceco.length() == 6) {
                            ceco = ceco.substring(2, 6);
                        }
                        if (ceco.length() == 5) {
                            ceco = ceco.substring(1, 5);
                        }

                        if (ceco.length() == 4) {
                            cecoAux = letraNegocio + ceco;
                        } else if (ceco.length() == 3) {
                            cecoAux = letraNegocio + "0" + ceco;
                        } else if (ceco.length() == 2) {
                            cecoAux = letraNegocio + "00" + ceco;
                        } else if (ceco.length() == 1) {
                            cecoAux = letraNegocio + "000" + ceco;
                        }

                        logger.info("CECO AUX ---> " + cecoAux);
                        user.setNombre(cecoAux);
                        logger.info("ESTES ES EL VALOR DE MI CECO!!!" + ceco);
                        logger.info("Mandando correo compromisos... ");
                        correoBI.sendMailCompromisosChecklist(user, cecoCom);
                        logger.info("Correo compromisos Enviado");
                        bandeCorreo = false;
                    }
                } catch (Exception e) {
                    logger.info("Ocurrio algo " + e);
                }
                if (banderaEnvio == 0) {
                    logger.info("Correo no enviado por que no se tienen completados los checklist correspondientes");
                }
            } else {
                logger.info("Cerro bitacora pero no envio correo por que esta desactivado ENVIO DE PDF COMPROMISOS 5 CHECKS.... ");
            }
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
        }
        //FIN ENVIO PEDF 5 CHECKS
        //----------------------------Inicia el envio de corre de calificacion 7S--------------------
        //----------------------------FIN del envio de corre de calificacion 7S--------------------
        return retorno;
    }

    // http://10.51.210.239:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getCorreosAcuerdos.json?id=189870&idtienda=G2244&idBitacora=3611
    // COMPROMINSOS  http://10.51.210.239:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getCorreosAcuerdos.json?id=189870&idtienda=189871&idBitacora=5774
    // 5 CHECKS http://10.51.210.239:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getCorreosAcuerdos.json?id=189870&idtienda=189871&idBitacora=9926
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getCorreosAcuerdos", method = RequestMethod.GET)
    public @ResponseBody
    boolean getCorreosAcuerdos(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        Map<String, Object> compromisos = null;

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        int usuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
        String idTienda = urides.split("&")[1].split("=")[1];
        int idBitacora = Integer.parseInt(urides.split("&")[2].split("=")[1]);

        List<CompromisoDTO> listaCompromiso = new ArrayList<CompromisoDTO>();
        String letraNegocio = "";
        int numNegocio = 0;
        String numTienda = "";
        String idUsuario = "";
        int numChecklist = 0;

        try {
            logger.info("Obtiene la lista de compromisos... ");
            logger.info("idBitacora... " + idBitacora);
            listaCompromiso = checklistBI.compromisosChecklistWeb(idBitacora);
            if (listaCompromiso != null) {
                numTienda = listaCompromiso.get(0).getEconomico();
                idUsuario = listaCompromiso.get(0).getIdUsuario();
                numChecklist = listaCompromiso.get(0).getIdChecklist();
                numNegocio = listaCompromiso.get(0).getIdNegocio();
                // idUsuario="189870";
                logger.info("Numero de Negocio... " + numNegocio);
                logger.info("Numero de Tienda... " + numTienda);

                letraNegocio = (parametroBI.obtieneParametros("negocio" + numNegocio)).get(0).getValor();
                logger.info("Letra de Negocio... " + letraNegocio);

                if (numTienda.length() == 4) {
                    numTienda = letraNegocio + numTienda;
                } else if (numTienda.length() == 3) {
                    numTienda = letraNegocio + "0" + numTienda;
                } else if (numTienda.length() == 2) {
                    numTienda = letraNegocio + "00" + numTienda;
                } else if (numTienda.length() == 1) {
                    numTienda = letraNegocio + "000" + numTienda;
                }

                logger.info("Numero de Negocio Final... " + numTienda);
                logger.info("idUsuario... " + idUsuario);
                logger.info("Numero de checklist... " + numChecklist);
            } else {
                logger.info("No cuenta con compromisos...");
            }
        } catch (Exception e1) {
            logger.info("Ocurrio algo: " + e1);
        }

        logger.info("listaCompromiso... " + listaCompromiso);

        UsuarioDTO user = new UsuarioDTO();
        user.setIdUsuario("" + usuario);
        user.setNombre(idTienda);

        //INICIO ENVIO ACUEDOS
        List<ParametroDTO> valida = null;
        String arr[] = null;
        boolean bandeCorreo = true;

        if (listaCompromiso != null) {
            try {
                valida = parametroBI.obtieneParametros("enviaCorreo");
                logger.info("Estado del activo " + valida.get(0).getActivo());
                if (valida.get(0).getActivo() == 1) {
                    bandeCorreo = true;
                    logger.info("Valores del parametro enviaCorreo " + valida.get(0).getValor());
                    arr = valida.get(0).getValor().split(",");
                    logger.info("Tamaño del array de parametros " + arr.length);
                    for (int i = 0; i < arr.length; i++) {
                        logger.info("Valor de parametro " + (i + 1) + ": " + arr[i]);
                        if (Integer.parseInt(arr[i]) == numChecklist) {
                            if (listaCompromiso != null) {
                                try {
                                    logger.info("Mandando correo... ");
                                    correoBI.sendMailAcuerdos(user, listaCompromiso);
                                    logger.info("Correo Enviado");
                                    bandeCorreo = false;
                                } catch (Exception e) {
                                    logger.info(e);
                                }
                            }
                        }
                    }
                    if (bandeCorreo) {
                        logger.info("Correo no enviado por que el checklist no existe en el activo");
                    }
                } else {
                    logger.info("Cerro bitacora pero no envio correo por que esta desactivado... ");
                }
            } catch (Exception e) {
                logger.info("Ocurrio algo " + e);
            }
        } else {
            logger.info("No envia correo por que la lista de compromisos esta vacia...");
        }
        //FIN ENVIO ACUERDOS

        //ENVIO DE PDF DE 5 CHECKS'
        List<ParametroDTO> validaPDF = null;
        Map<String, String> resultado = new HashMap<String, String>();
        String cecoAux = "";

        try {
            validaPDF = parametroBI.obtieneParametros("checklistsPDF");
            logger.info("Estado del activo " + validaPDF.get(0).getActivo());
            if (validaPDF.get(0).getActivo() == 1) {
                bandeCorreo = true;
                int banderaEnvio = 0;
                try {
                    logger.info("COMPROBANDO ENVIO DE PDF COMPROMISOS 5 CHECKS... ");
                    //VERUFICAMOS SI SE DEBE ENVIAR O NO
                    logger.info("VERIFICANDO ENVIO... ");
                    logger.info("idUsuario " + usuario);
                    logger.info("idBitacora " + idBitacora);

                    banderaEnvio = respuestaPdfBI.obtieneEnvioCompromiso(usuario, idBitacora);
                    logger.info("RESPUESTA DE VERIFICANDO ENVIO... " + banderaEnvio);
                    if (banderaEnvio == 1) {
                        logger.info("CONSULTA CECO POR BITACORA... ");
                        resultado = respuestaPdfBI.obtieneCecoPorBitacora(usuario, idBitacora);
                        String ceco = resultado.get("idCeco");
                        String cecoCom = resultado.get("idCeco");
                        String nNegocio = resultado.get("numNegocio");
                        logger.info("CECO QUE ESTOY REGRESANDO " + ceco);
                        logger.info("NUMERO NEGOCIO QUE ESTOY REGRESANDO " + nNegocio);

                        letraNegocio = (parametroBI.obtieneParametros("negocio" + nNegocio)).get(0).getValor();
                        logger.info("Letra de Negocio... " + letraNegocio);

                        if (ceco.length() == 6) {
                            ceco = ceco.substring(2, 6);
                        }
                        if (ceco.length() == 5) {
                            ceco = ceco.substring(1, 5);
                        }

                        if (ceco.length() == 4) {
                            cecoAux = letraNegocio + ceco;
                        } else if (ceco.length() == 3) {
                            cecoAux = letraNegocio + "0" + ceco;
                        } else if (ceco.length() == 2) {
                            cecoAux = letraNegocio + "00" + ceco;
                        } else if (ceco.length() == 1) {
                            cecoAux = letraNegocio + "000" + ceco;
                        }

                        logger.info("CECO AUX ---> " + cecoAux);
                        user.setNombre(cecoAux);
                        logger.info("ESTES ES EL VALOR DE MI CECO!!!" + ceco);
                        logger.info("Mandando correo compromisos... ");
                        correoBI.sendMailCompromisosChecklist(user, cecoCom);
                        logger.info("Correo compromisos Enviado");
                        bandeCorreo = false;
                    }
                } catch (Exception e) {
                    logger.info("Ocurrio algo " + e);
                }
                if (banderaEnvio == 0) {
                    logger.info("Correo no enviado por que no se tienen completados los checklist correspondientes");
                }
            } else {
                logger.info("Cerro bitacora pero no envio correo por que esta desactivado ENVIO DE PDF COMPROMISOS 5 CHECKS.... ");
            }
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
        }
        //FIN ENVIO PEDF 5 CHECKS
        return true;
    }

    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/envioBDdispositivo", method = RequestMethod.POST)
    public @ResponseBody
    boolean envioBDdispositivo(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        boolean retorno = false;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";
        // int idBitacora=0;

        List<BitacoraDTO> bitacora = null;
        List<CecoDTO> ceco = null;
        List<ChecklistDTO> checklist = null;
        List<Usuario_ADTO> usr = null;

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        // logger.info("JSON SIN DESENCRIPTAR: " + provider);

        List<ParametroDTO> valida = parametroBI.obtieneParametros("correoSoporte");
        if (valida.size() > 0) {
            if (valida.get(0).getActivo() == 1) {
                try {
                    if (idLlave == 7) {
                        urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                        //logger.info("URIDES IOS: " + urides);

                        String tmp = "";

                        if (provider.contains("finiOSswift") && provider.contains("inicioiOSswift")) {
                            tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                            tmp = tmp.replaceAll(" ", "+");
                            ////logger.info("JSON IOS TMP: " + tmp);
                            contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                        } else {

                            contenido = "";
                        }

                    } else if (idLlave == 666) {
                        urides = (cifra.decryptParams(uri));
                        contenido = descifra.decryptParams(provider);
                        // logger.info("JSON ANDROID: " + contenido);
                    }

                    int usuario = Integer.parseInt(urides.split("=")[1].split("&")[0]);
                    int idChecklist = Integer.parseInt(urides.split("&")[1].split("=")[1]);
                    int idCeco = Integer.parseInt(urides.split("&")[2].split("=")[1]);
                    int idBitacora = Integer.parseInt(urides.split("&")[3].split("=")[1]);

                    logger.info("usuario: " + usuario);
                    logger.info("idChecklist: " + idChecklist);
                    logger.info("idCeco: " + idCeco);
                    logger.info("idBitacora: " + idBitacora);

                    usr = usuario_ABI.obtieneUsuario(usuario);
                    bitacora = bitacoraBI.buscaBitacora(null, idBitacora + "", null, null);
                    ceco = cecoBI.buscaCeco(idCeco);
                    checklist = checklistBI.buscaChecklist(idChecklist);

                } catch (Exception e) {
                    logger.info(e);
                    retorno = false;
                }

                try {

                    if (ceco != null && bitacora != null && checklist != null && usr != null) {

                        if (ceco.size() > 0 && bitacora.size() > 0 && checklist.size() > 0 && usr.size() > 0) {

                            ArrayList<String> copiados = new ArrayList<String>();
                            String titulo = "Correo de soporte " + " " + checklist.get(0).getNombreCheck() + " "
                                    + ceco.get(0).getIdCeco() + " - " + ceco.get(0).getDescCeco() + " Fecha: "
                                    + bitacora.get(0).getFechaInicio();

                            // contenido=contenido.replace(";", ";<br>");
                            // contenido=contenido.replace("\n", "<br>");
                            // logger.info(contenido);
                            Resource correoColab = UtilResource.getResourceFromInternet("mailSoporteBD.html");

                            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
                            strCorreoOwner = strCorreoOwner.replace("@Titulo", "Correo de soporte");

                            strCorreoOwner = strCorreoOwner.replace("@datosChecklist",
                                    "<br>ID USUARIO: " + usr.get(0).getIdUsuario() + "<br>USUARIO: "
                                    + usr.get(0).getNombre() + "<br>CECO: " + ceco.get(0).getIdCeco()
                                    + "<br>DESCRIPCION CECO: " + ceco.get(0).getDescCeco() + "<br>CHECKLIST: "
                                    + checklist.get(0).getNombreCheck() + "<br>CHECKUSUA: "
                                    + bitacora.get(0).getIdCheckUsua() + "<br>BITACORA: "
                                    + bitacora.get(0).getIdBitacora() + "<br>FECHA DE INICIO: "
                                    + bitacora.get(0).getFechaInicio() + "<br>FECHA DE TERMINO: "
                                    + bitacora.get(0).getFechaFin()
                            );
                            strCorreoOwner = strCorreoOwner.replace("@contenido", contenido);

                            CorreosLotus correosLotus = new CorreosLotus();
                            // String destinatario = "mdelgadoa@elektra.com.mx";
                            String destinatario = valida.get(0).getValor().split(";")[0];

                            // copiados.add("mdelgadoa@elektra.com.mx");
                            // copiados.add("cvidal@bancoazteca.com.mx");
                            for (int i = 1; i < valida.get(0).getValor().split(";").length; i++) {
                                copiados.add(valida.get(0).getValor().split(";")[i]);
                            }

                            retorno = UtilMail.enviaCorreo(destinatario, copiados, "" + titulo, strCorreoOwner, null);
                        } else {
                            retorno = false;
                        }
                    } else {
                        retorno = false;
                    }

                } catch (IOException e) {
                    logger.info(e);
                    retorno = false;
                }
            } else {
                // en caso de que se desactive el parametro
                retorno = true;
            }
        } else {
            retorno = false;
        }
        return retorno;
    }

    // http://localhost:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getVisitas.json?id=191312&idChecklist=54&nuMes=1&anio=<?>
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getVisitas", method = RequestMethod.GET)
    public @ResponseBody
    String getVisitas(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        String usuario = urides.split("&")[0].split("=")[1];
        String idChecklist = urides.split("&")[1].split("=")[1];
        String nuMes = urides.split("&")[2].split("=")[1];
        String anio = urides.split("&")[3].split("=")[1];

        json = visitaBI.obtieneVisitas(idChecklist, usuario, nuMes, anio);

        return json;

    }

    // http://localhost:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getValidaCheckUsua.json?id=191312&idCheckUsua=&latitud=&longitud=
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getValidaCheckUsua", method = RequestMethod.GET)
    public @ResponseBody
    String getValidaCheckUsua(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        String usuario = urides.split("&")[0].split("=")[1];
        String idCheckUsua = urides.split("&")[1].split("=")[1];
        String latitud = urides.split("&")[2].split("=")[1];
        String longitud = urides.split("&")[3].split("=")[1];

        //List<ChecklistDTO> regChecklist = checklistBI.validaCheckUsua(Integer.parseInt(idCheckUsua), Double.parseDouble(latitud), Double.parseDouble(longitud));
        List<ChecklistDTO> regChecklist = checklistBI.validaCheckUsua(Integer.parseInt(idCheckUsua), latitud, longitud);

        JsonArray array = new JsonArray();
        JsonObject object = null;
        JsonObject princ = new JsonObject();

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);
        String json = null;

        if (regChecklist != null) {
            Iterator<ChecklistDTO> it = regChecklist.iterator();
            while (it.hasNext()) {
                ChecklistDTO o = it.next();
                object = new JsonObject();
                JsonObject object1 = new JsonObject();
                JsonObject object2 = new JsonObject();
                JsonObject object3 = new JsonObject();
                // tipoCheck
                object1.addProperty("id", o.getIdTipoChecklist().getIdTipoCheck());
                object1.addProperty("descripcion", o.getIdTipoChecklist().getDescTipo());
                // checkList
                object2.addProperty("id", o.getIdChecklist());
                object2.addProperty("nombre", o.getNombreCheck());
                object2.addProperty("fechaInicio", o.getFecha_inicio());
                object2.addProperty("fechaFin", o.getFecha_fin());
                // checkUsuario
                object3.addProperty("id", o.getIdCheckUsua());

                object.add("tipoCheck", object1);
                object.add("checkList", object2);
                object.add("checkUsuario", object3);
                // nombre
                object.addProperty("nombre", o.getNombresuc());
                object.addProperty("idCeco", o.getIdCeco());
                object.addProperty("zona", o.getZona());
                object.addProperty("latitud", o.getLatitud());
                object.addProperty("longitud", o.getLongitud());
                object.addProperty("ultimaVisita", o.getUltimaVisita());
                object.addProperty("estatus", o.getEstatus());
                object.addProperty("distancia", o.getDistancia());

                array.add(object);
            }
            princ.add("tiendasProx", array);
            princ.addProperty("fecha", fecha);
            json = princ.toString();
        } else {
            princ.add("tiendasProx", array);
            princ.addProperty("fecha", fecha);
            json = princ.toString();
        }
        return json;
    }
    // http://localhost:8080/checklist/consultaChecklistService/getVisitas.json?idChecklist=<?>&idUsuario=<?>&nuMes=<?>

    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getVisitasService", method = RequestMethod.GET)
    public @ResponseBody
    String getRepVisitas(HttpServletRequest request, HttpServletResponse response) {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String visitas = "";
        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();

        String idUsuario = "";
        String idChecklist = "";
        String nuMes = "";
        String anio = "";
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            idUsuario = urides.split("&")[0].split("=")[1];
            idChecklist = urides.split("&")[1].split("=")[1];
            nuMes = urides.split("&")[2].split("=")[1];
            anio = urides.split("&")[3].split("=")[1];
            logger.info("usuario: " + idUsuario);
            logger.info("idChecklist: " + idChecklist);
            logger.info("mes: " + nuMes);
            logger.info("anio: " + anio);

            visitas = visitaTiendaBI.obtieneVisitas(idChecklist, idUsuario, nuMes, anio);
        } catch (Exception e) {

        }

        /*
		 * String idChecklist = request.getParameter("idChecklist"); String
		 * idUsuario = request.getParameter("idUsuario"); String nuMes =
		 * request.getParameter("nuMes");
         */
        return visitas;
    }

    // http://localhost:8080/checklist/consultaChecklistService/getVisitas.json?idChecklist=<?>&idUsuario=<?>&nuMes=<?>
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getOffline", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getOffline(HttpServletRequest request, HttpServletResponse response) {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String visitas = "";
        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();

        String idUsuario = "";
        Map<String, Object> checksUsuario = null;
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            idUsuario = urides.split("&")[0].split("=")[1];
            checksUsuario = checksOfflineBI.obtieneChecksNuevo(Integer.parseInt(idUsuario));
        } catch (Exception e) {
            checksUsuario = null;
        }
        return checksUsuario;
    }

    // servicios/loginRest2.json?id=191312
    // servicios/loginRest2.json?Lg/M4L45zj4Zl05KmbXZmB0FfUnUG08UnAcW0OXp8q0=&token=
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/getReportePila", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getReportePila(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        Map<String, Object> reporte = null;

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }
        int idUsuario = Integer.parseInt(urides.split("=")[1]);
        logger.info("USUARIO > " + idUsuario);
        try {
            List<ParametroDTO> ejecutaReportePila = parametroBI.obtieneParametros("ejecutaReportePila");

            if (ejecutaReportePila != null) {
                for (ParametroDTO param : ejecutaReportePila) {
                    try {
                        if (param.getValor().equals("1")) {
                            reporte = checklistBI.ReportePila(idUsuario);

                        }
                    } catch (Exception ex) {
                        logger.info("No existe parametro");
                    }
                }
            }

        } catch (Exception e) {
            logger.info("ERROR AL OBTENER EL REPORTE PILA: " + e.getMessage());
            reporte = null;
        }
        return reporte;
    }

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/enviaCorreoBuzon", method = RequestMethod.POST)
    public @ResponseBody
    boolean setCorreosBuzon(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        boolean retorno = true;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        int idUsuario = 0;
        String nombre = "";
        String asunto = "";
        String comentario = "";
        String lugar = "";
        String telefono = "";
        String idCeco = "";
        String puesto = "";

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");

                json = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("JSON IOS: " + json);
                JSONObject jsonObject = new JSONObject(json);
                idUsuario = Integer.parseInt(urides.split("=")[1]);
                nombre = jsonObject.getString("nombre");
                asunto = jsonObject.getString("asunto");
                lugar = jsonObject.getString("lugar");
                telefono = jsonObject.getString("telefono");
                comentario = jsonObject.getString("comentario");
                idCeco = jsonObject.getString("idCeco");
                puesto = jsonObject.getString("puesto");
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));

                json = descifra.decryptParams(provider);

                //logger.info("JSON ANDROID: " + json);
                JSONObject jsonObject = new JSONObject(json);
                idUsuario = Integer.parseInt(urides.split("=")[1]);
                nombre = jsonObject.getString("nombre");
                asunto = jsonObject.getString("asunto");
                lugar = jsonObject.getString("lugar");
                telefono = jsonObject.getString("telefono");
                comentario = jsonObject.getString("comentario");
                idCeco = jsonObject.getString("idCeco");
                puesto = jsonObject.getString("puesto");
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
            retorno = false;
        }

        UsuarioDTO user = new UsuarioDTO();
        user.setIdUsuario("" + idUsuario);
        user.setNombre(nombre);
        List<ParametroDTO> valida = null;
        List<ParametroDTO> copiados = null;
        boolean bandeCorreo = true;

        try {
            valida = parametroBI.obtieneParametros("enviaBuzon");
            copiados = parametroBI.obtieneParametros("correoBuzon");
            logger.info("Estado del activo " + valida.get(0).getActivo());
            if (valida.get(0).getActivo() == 1) {
                bandeCorreo = true;

                try {
                    logger.info("Mandando correo... ");
                    correoBI.sendMailBuzon(user, asunto, idCeco, puesto, lugar, telefono, comentario, copiados.get(0).getValor());
                    logger.info("Correo Enviado");
                    bandeCorreo = false;
                } catch (Exception e) {
                    logger.info(e);
                }

                if (bandeCorreo) {
                    logger.info("Correo no enviado...");
                }
            } else {
                logger.info("Correo no enviado por que el parametro esta desactivado... ");
            }
        } catch (Exception e) {
            logger.info(e);
            retorno = false;
        }

        return retorno;
    }

    // http://10.51.210.239:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/enviaCorreoCompromisosCheck.json?id=191312&idCeco=484747
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/enviaCorreoCompromisosCheck", method = RequestMethod.GET)
    public @ResponseBody
    boolean setCorreosCompromisosCheck(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        boolean retorno = true;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        int idUsuario = 0;
        String nombre = "";

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                json = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());
                //logger.info("JSON IOS: " + json);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                json = descifra.decryptParams(provider);
                //logger.info("JSON ANDROID: " + json);
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
            retorno = false;
        }

        String idUser = urides.split("&")[0].split("=")[1];
        String idCeco = urides.split("&")[1].split("=")[1];
        logger.info("DATOS!!!!! idUser  ----->" + idUser + "idCeco--->" + idCeco);
        //SET USER
        UsuarioDTO user = new UsuarioDTO();
        user.setIdUsuario("" + idUser);
        user.setNombre(nombre);

        List<ParametroDTO> valida = null;
        List<ParametroDTO> copiados = null;
        boolean bandeCorreo = true;

        try {

            valida = parametroBI.obtieneParametros("enviaBuzon");
            copiados = parametroBI.obtieneParametros("correoBuzon");
            logger.info("Estado del activo " + valida.get(0).getActivo());

            if (valida.get(0).getActivo() == 1) {
                bandeCorreo = true;

                try {

                    logger.info("Mandando correo... ");
                    //IDUSUARIO-CECO
                    correoBI.sendMailCompromisosChecklist(user, idCeco);
                    logger.info("Correo Enviado");
                    /*bandeCorreo = false;*/
                } catch (Exception e) {
                    logger.info(e);
                }

                if (bandeCorreo) {
                    logger.info("Correo no enviado...");
                }
            } else {
                logger.info("Correo no enviado por que el parametro esta desactivado... ");
            }
        } catch (Exception e) {
            logger.info(e);
            retorno = false;
        }

        return true;
    }

    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/notificacionPorcentaje", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> notificacionPorcentaje(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        boolean retorno = true;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        int idUsuario = 0;
        Map<String, Object> reporte = null;

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                idUsuario = Integer.parseInt(urides.split("=")[1]);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                idUsuario = Integer.parseInt(urides.split("=")[1]);
            }
            reporte = notificacionBI.NotificacionPorcentaje(idUsuario);
        } catch (Exception e) {
            logger.info(e.getMessage());
            reporte = null;
        }
        return reporte;
    }

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getOffline2", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getOffline2(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        int idUsuario = 0;
        Map<String, Object> listasOffline = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                idUsuario = Integer.parseInt(urides.split("=")[1]);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                idUsuario = Integer.parseInt(urides.split("=")[1]);
            }
            listasOffline = checksOfflineBI.obtieneChecksNuevo(idUsuario);
        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
            listasOffline = null;
        }

        //logger.info("Json getOffline2: "+listasOffline);
        return listasOffline;
    }

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getOffline3", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getOffline3(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        int idUsuario = 0;
        int idChecklist = 0;
        Map<String, Object> listasOffline = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                //idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idChecklist = Integer.parseInt(urides.split("&")[1].split("=")[1]);

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                //idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idChecklist = Integer.parseInt(urides.split("&")[1].split("=")[1]);

            }

            System.out.println("ID_USUARIO : " + idUsuario + " ID_CHECKLIST: " + idChecklist);
            listasOffline = checksOfflineBI.obtieneChecksNuevo3(idUsuario, idChecklist);
        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
            listasOffline = null;
        }

        //logger.info("Json getOffline2: "+listasOffline);
        return listasOffline;
    }

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getOffline4", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getOffline4(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        int idUsuario = 0;
        Map<String, Object> listasOffline = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                idUsuario = Integer.parseInt(urides.split("=")[1]);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                idUsuario = Integer.parseInt(urides.split("=")[1]);
            }
            listasOffline = checksOfflineBI.obtieneChecksNuevo4(idUsuario);
        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
            listasOffline = null;
        }

        //logger.info("Json getOffline2: "+listasOffline);
        return listasOffline;
    }

    // http://10.51.210.239:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getCompruebaVersion.json?id=189870&so=ANDROID
    // http://10.50.50.109.47:8080/checklist/servicios/getCompruebaVersion.json?id=189870&so=IOS
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getCompruebaVersion", method = RequestMethod.GET)
    public @ResponseBody
    String getCompruebaVersion(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        List<String> listaVersion = null;
        String sitemaOpe = "";
        JsonObject object = new JsonObject();
        String json = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                sitemaOpe = (urides.split("&")[1]).split("=")[1];
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                sitemaOpe = (urides.split("&")[1]).split("=")[1];
            }

            listaVersion = (List<String>) versionBI.obtieneUltVersion(sitemaOpe);

            if (listaVersion != null) {
                object.addProperty("version", listaVersion.get(0));
                object.addProperty("fecha", listaVersion.get(1));
            } else {
                object.addProperty("version", "null");
                object.addProperty("fecha", "null");
            }
            json = object.toString();
        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
        }

        logger.info("json:" + json);
        return json;
    }

    // servicio para enviar una pregunta
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/preguntaOnline2", method = RequestMethod.POST)
    public @ResponseBody
    boolean setPreguntaOnline2(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        Gson gson = new Gson();

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        //logger.info("JSON RESP SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");

                json = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                logger.info("JSON IOS RESPUESTAS: " + json);

                respuestaRespuestaDTO = gson.fromJson(json, RegistroRespuestaDTO.class);
                boolean respuestas = respuestaBI.insertaNuevaRespuesta(respuestaRespuestaDTO);
                //logger.info("RESPUESTA DTO: "+respuestaRespuestaDTO.toString());
                if (respuestas) {
                    logger.info("SE AGREGO CORRECTAMENTE");
                    return true;

                }

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));

                json = descifra.decryptParams(provider);

                logger.info("JSON ANDROID RESPUESTAS: " + json);
                respuestaRespuestaDTO = gson.fromJson(json, RegistroRespuestaDTO.class);
                boolean respuestas = respuestaBI.insertaNuevaRespuesta(respuestaRespuestaDTO);
                //logger.info("RESPUESTA DTO: "+respuestaRespuestaDTO.toString());
                if (respuestas) {
                    logger.info("SE AGREGO CORRECTAMENTE");
                    return true;

                }

            }
        } catch (Exception e) {
            logger.info("Ocurrio algo  " + e.getMessage());
        }

        return false;
    }

    // servicio para enviar una pregunta
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/preguntaOnline3", method = RequestMethod.POST)
    public @ResponseBody
    boolean setPreguntaOnline3(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        Gson gson = new Gson();

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        boolean respuestaInsert = true;

        try {
            if (idLlave == 7) {

                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");

                json = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                logger.info("JSON IOS: " + json);
                registroRespuestasCompletasDTO = gson.fromJson(json, RegistroRespuestasCompletasDTO.class);

            } else if (idLlave == 666) {

                urides = (cifra.decryptParams(uri));
                json = descifra.decryptParams(provider);

                logger.info("JSON ANDROID: PARA PROBAR RUTAX ---  " + json);
                registroRespuestasCompletasDTO = gson.fromJson(json, RegistroRespuestasCompletasDTO.class);

            }

            if (registroRespuestasCompletasDTO != null) {

                List<RegistroRespuestaDTO> listaRespuestaCompleta = registroRespuestasCompletasDTO.getListaRespuestaCompleta();

                for (RegistroRespuestaDTO registroRespuestaDTO : listaRespuestaCompleta) {

                    boolean respuesta = respuestaBI.insertaNuevaRespuesta(registroRespuestaDTO);

                    if (!respuesta) {
                        respuestaInsert = false;
                    }

                    logger.info("INSERT DE RESPUESTA EN BASE: " + respuestaInsert);
                }

                //logger.info("RESPUESTA DTO: "+respuestaRespuestaDTO.toString());
                logger.info(respuestaInsert);
                return respuestaInsert;

            }

        } catch (Exception e) {
            logger.info("Ocurrió algo  " + e);
            return false;
        }

        return respuestaInsert;
    }

    // http://10.50.50.109.47:8080/checklist/servicios/getInformesIncidentes.json?id=<?>&status=<?>&motivo=<?>&fechaInicio=<?>&fechaFin=<?>&director=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getInformesIncidentes", method = RequestMethod.GET)
    public @ResponseBody
    String getInformesIncidentes(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String json = "";

        String usuario = "";
        String estatus = "";
        String motivo = "";
        String fechaInicio = "";
        String fechaFin = "";
        String director = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String[] arrayParametros = urides.split("&");
            String param[];

            param = arrayParametros[0].split("=");
            if (param.length > 1) {
                usuario = param[1];
            }

            param = arrayParametros[1].split("=");
            if (param.length > 1) {
                estatus = param[1];
            }

            param = arrayParametros[2].split("=");
            if (param.length > 1) {
                motivo = param[1];
            }

            param = arrayParametros[3].split("=");
            if (param.length > 1) {
                fechaInicio = param[1].replace("%20", " ");
            }

            param = arrayParametros[4].split("=");
            if (param.length > 1) {
                fechaFin = param[1].replace("%20", " ");
            }

            param = arrayParametros[5].split("=");
            if (param.length > 1) {
                director = param[1].replace("%20", " ");
            }

            json = incidenteBi.getInformes(estatus, motivo, fechaInicio, fechaFin, director);

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
            json = "";
        }

        //logger.info("json:" + json);
        return json;
    }

    // http://10.50.50.109.47:8080/checklist/servicios/getReporteMotivos.json?id=189870&fechaInicio=2018-05-11 00:00:00&fechaFin=2018-05-23 23:59:59&motivo=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getReporteMotivos", method = RequestMethod.GET)
    public @ResponseBody
    String getReporteMotivos(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String json = "";

        String usuario = "";
        String fechaInicio = "";
        String fechaFin = "";
        String motivo = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String[] arrayParametros = urides.split("&");
            String param[];

            param = arrayParametros[1].split("=");
            if (param.length > 1) {
                fechaInicio = param[1].replace("%20", " ");
            }

            param = arrayParametros[2].split("=");
            if (param.length > 1) {
                fechaFin = param[1].replace("%20", " ");
            }

            param = arrayParametros[2].split("=");
            if (param.length > 1) {
                motivo = param[3].replace("%20", " ");
            }

            json = incidenteBi.getIncidentesPorMotivo(fechaInicio, fechaFin, motivo);

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
            json = "";
        }

        //logger.info("json:" + json);
        return json;
    }

    // http://10.50.50.109.47:8080/checklist/servicios/getDirectores.json?id=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDirectores", method = RequestMethod.GET)
    public @ResponseBody
    String getDirectores(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String json = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            json = incidenteBi.getDirectores();

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
            json = "";
        }

        //logger.info("json:" + json);
        return json;
    }

    // http://10.50.50.109.47:8080/checklist/servicios/getTemplates.json?id=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getTemplates", method = RequestMethod.GET)
    public @ResponseBody
    String getTemplates(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String json = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            json = incidenteBi.getTemplates();

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
            json = "";
        }

        //logger.info("json:" + json);
        return json;
    }

    //Enviar incidente
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/createIncident", method = RequestMethod.POST)
    public @ResponseBody
    String createIncident(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";

        String respuestaService = "";

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        //logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider, "UTF-8");
                tmp = tmp.replaceAll(" ", "+");

                json = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                json = java.net.URLDecoder.decode(json, "UTF-8");

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));

                json = descifra.decryptParams(provider);

            }

            JsonParser jsonParser;
            JsonObject jsonIncidente;

            IncidenteDTO incidenteDTO = null;
            AdjuntoDTO adjuntoDTO = null;;

            if (!json.equals("")) {

                jsonParser = new JsonParser();
                jsonIncidente = jsonParser.parse(json).getAsJsonObject();

                incidenteDTO = new IncidenteDTO();

                incidenteDTO.setIdPlantilla(jsonIncidente.get("idPlantilla").getAsString());
                incidenteDTO.setDescripcion(jsonIncidente.get("descripcion").getAsString());
                incidenteDTO.setAdjunto(jsonIncidente.get("adjunto").getAsBoolean());
                incidenteDTO.setDirector(jsonIncidente.get("director").getAsString());
                incidenteDTO.setId(jsonIncidente.get("idIncidente").getAsString());
                incidenteDTO.setSucursal(jsonIncidente.get("sucursal").getAsString());
                incidenteDTO.setPrioridad(jsonIncidente.get("prioridad").getAsString());
                incidenteDTO.setTipoError(jsonIncidente.get("tipoError").getAsString());

                if (jsonIncidente.get("adjunto").getAsBoolean()) {
                    adjuntoDTO = new AdjuntoDTO();
                    adjuntoDTO.setNombreArchivo(jsonIncidente.get("nombreAdjunto").getAsString());
                    adjuntoDTO.setNumBytesRead(jsonIncidente.get("bytesAdjunto").getAsInt());
                    adjuntoDTO.setArchivoBytes(jsonIncidente.get("archivoBase64").getAsString().replace(" ", "+"));
                    adjuntoDTO.setIdIncidente(jsonIncidente.get("idIncidente").getAsString());
                }

                ////System.out.println(jsonIncidente.get("archivoBase64").getAsString());
                ////System.out.println(adjuntoDTO.getArchivoBytes());
                //Sysstem.out.println(jsonIncidente.toString());
                respuestaService = incidenteBi.createIncidente(incidenteDTO, adjuntoDTO);

            }

        } catch (Exception e) {
            logger.info("Ocurrió algo  " + e.getMessage());
            respuestaService = "";
        }

        return respuestaService;
    }

    // http://localhost:8080/checklist//servicios/nuevaBitacora.json?idCheckUsua=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/nuevaBitacora", method = RequestMethod.GET)
    public @ResponseBody
    String nuevaBitacora(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {

            UtilCryptoGS cifra = new UtilCryptoGS();
            String uri = request.getQueryString();
            uri = uri.split("&")[0];
            UtilDate fec = new UtilDate();
            String fechaprueba = fec.getSysDate("ddMMyyyyHHmmss");

            StrCipher cifraIOS = new StrCipher();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            //urides = urides.split("&")[1];
            String checkUsua = urides.split("&")[1].split("=")[1];
            String ceco = null;

            if (urides.split("&").length == 3) {
                logger.info("Longitud: " + urides.split("&").length + " , " + urides);
                ceco = urides.split("&")[2].split("=")[1];
            }

            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            //System.out.println("Fecha: "+dateFormat.format(date));

            //int idBitacora=0;
            String fechBitacora = dateFormat.format(date);
            BitacoraDTO bitacora = new BitacoraDTO();
            bitacora.setFechaInicio(fechBitacora);
            bitacora.setFechaFin(null);
            bitacora.setIdCheckUsua(Integer.parseInt(checkUsua));
            bitacora.setLatitud(null);
            bitacora.setLongitud(null);
            bitacora.setModo(0);

            BitacoraAdministradorDTO bitacoraAdministrador = new BitacoraAdministradorDTO();

            bitacoraAdministrador.setIdCheckUsua(Integer.parseInt(checkUsua));
            bitacoraAdministrador.setModo(0);
            bitacoraAdministrador.setCeco(ceco);

            //int idBitacora = bitacorabi.insertaBitacora(bitacora);
            int idBitacora = bitacoraAdministradorBI.insertaBitacora(bitacoraAdministrador);;

            bitacora.setIdBitacora(idBitacora);

            JsonObject envoltorioJsonObj = new JsonObject();

            if (idBitacora == 0) {
                envoltorioJsonObj.add("idBitacora", null);
            } else {
                envoltorioJsonObj.addProperty("idBitacora", idBitacora);
            }

            res = envoltorioJsonObj.toString();
        } catch (Exception e) {
            logger.info(e);
        }
        if (res != null) {
            return res.toString();
        } else {
            return "{null}";
        }
    }

    // http://10.50.50.109.47:8080/checklist/servicios/getListaSucChecklist.json?
    @RequestMapping(value = "/getListaSucChecklist", method = RequestMethod.GET)
    public @ResponseBody
    String getListaSucChecklist(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        String json = "";

        try {

            json = sucursalChecklistBI.obtieneInfo();

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
            json = "";
        }

        //logger.info("json:" + json);
        return json;
    }

    // http://localhost:8080/checklist/servicios/getBitacoraGral.json?idUsu=<?>&finicio=<?>&fin=<?>&status=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getBitacoraGral", method = RequestMethod.GET)
    public @ResponseBody
    String getBitacoraGral(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            String finicio = urides.split("&")[1].split("=")[1];
            String fin = urides.split("&")[2].split("=")[1];
            Integer status = Integer.parseInt(urides.split("&")[3].split("=")[1]);

            JsonObject envoltorioJsonObj = new JsonObject();

            BitacoraGralDTO bg = new BitacoraGralDTO();

            bg.setFinicio(finicio);
            bg.setFin(fin);
            bg.setStatus(status);

            int resp = bitacoraGralBI.inserta(bg);

            int aux = 0;
            if (resp != 0) {
                aux = 1;
            }

            envoltorioJsonObj.addProperty("ALTA", aux);
            envoltorioJsonObj.addProperty("ID", resp);

            logger.info("JSON Alta Bitacora Gral: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/checklist/servicios/getBitacoraGralHi.json?idUsu=<?>&finicio=<?>&fin=<?>&status=<?>&idBitaGral=<?>idBita=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getBitacoraGralHi", method = RequestMethod.GET)
    public @ResponseBody
    String getBitacoraGralHi(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            String finicio = urides.split("&")[1].split("=")[1];
            String fin = urides.split("&")[2].split("=")[1];
            Integer status = Integer.parseInt(urides.split("&")[3].split("=")[1]);
            Integer idBitaGral = Integer.parseInt(urides.split("&")[4].split("=")[1]);
            Integer idBita = Integer.parseInt(urides.split("&")[5].split("=")[1]);

            JsonObject envoltorioJsonObj = new JsonObject();

            BitacoraGralDTO bg = new BitacoraGralDTO();

            bg.setFinicio(finicio);
            bg.setFin(fin);
            bg.setStatus(status);
            bg.setIdBitaGral(idBitaGral);
            bg.setIdBita(idBita);

            int resp = bitacoraGralBI.insertaH(bg);

            int aux = 0;
            if (resp != 0) {
                aux = 1;
            }

            envoltorioJsonObj.addProperty("ALTA", aux);
            envoltorioJsonObj.addProperty("ID", resp);

            logger.info("JSON Alta Bitacora Hija: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/checklist/servicios/getReporteChecksSemana.json?idUsu=<?>&idCeco=<?>&idChecklist=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getReporteChecksSemana", method = RequestMethod.GET)
    public @ResponseBody
    String getReporteChecksSemana(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            String ceco = urides.split("&")[1].split("=")[1];
            String checklist = urides.split("&")[2].split("=")[1];

            JsonObject envoltorioJsonObj = new JsonObject();

            List<RepoCheckPaperDTO> lista = repoCheckPaperBI.obtieneDatos(ceco, Integer.parseInt(checklist));
            JsonArray repArr = new JsonArray();
            if (lista != null && !lista.isEmpty()) {
                for (RepoCheckPaperDTO aux : lista) {
                    JsonObject auxJsonObj = new JsonObject();
                    auxJsonObj.addProperty("CALIFICACION", aux.getCali());
                    auxJsonObj.addProperty("NOMBRE_CHECKLIST", aux.getNombreCheck());
                    auxJsonObj.addProperty("ID_CECO", aux.getCeco());
                    auxJsonObj.addProperty("SEMANA", aux.getSemana());
                    auxJsonObj.addProperty("FECHA_INICIO", aux.getFini());
                    auxJsonObj.addProperty("ID_BITACORA", aux.getBitacora());
                    auxJsonObj.addProperty("BANDERA", aux.getBandera());
                    auxJsonObj.addProperty("ID_CHECKLIST", aux.getIdCheclist());
                    auxJsonObj.addProperty("FECHA_FIN", aux.getFfin());
                    repArr.add(auxJsonObj);
                }
            }

            if (lista == null && lista.isEmpty()) {
                envoltorioJsonObj.add("REPORTE", null);
            } else {
                envoltorioJsonObj.add("REPORTE", repArr);
            }

            logger.info("JSON reporte check por semana: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    /*
// http://localhost:8080/checklist/servicios/sendCorreoApertura.json?idUsuario=<?>
   @SuppressWarnings("static-access")
    @RequestMapping(value = "/sendCorreoApertura", method = RequestMethod.POST)
    public @ResponseBody
    Boolean sendCorreoApertura(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

	   logger.info("ENTRE sendCorreoApertura");

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }

          	//Datos
            String economico = "";
            String sucursal = "";
            String tipo = "";
            String territorio = "";
            String zona = "";
            String aperturable = "";
            int idBitacora = 0;

            String tipoVisita = "";
            String fechaVisita = "";
            String tipoNegocio = "";
            //Json

            List<String> imperdonablesLista =  new ArrayList<String>();

    		String rutaImg = "/franquicia/firmaChecklist/firmalogo_fachada.png";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);

                economico =  rec.getString("eco");
                sucursal = rec.getString("sucursal");
                territorio = rec.getString("territorio");
                tipo = rec.getString("tipo");
                zona = rec.getString("zona");
                aperturable = rec.getString("aperturbale");
                idBitacora = rec.getInt("idBitacora");

                tipoVisita = rec.getString("tipoVisita");
                fechaVisita = rec.getString("fecha");
                tipoNegocio = rec.getString("tipoNegocio");

                JSONArray results = rec.getJSONArray("imperdonable");

                for (int i = 0; i < results.length();i ++)
                {
                		JSONObject data = results.getJSONObject(i);
                		String result = data.getString("descripcion");
                		imperdonablesLista.add(result);
                }

                //Firmas
        			List<FirmaCheckDTO> firmasB = firmaCheckBI.obtieneDatos(idBitacora+"");
            		//List<FirmaCheckDTO> firmasB = firmaCheckBI.obtieneDatos("20878");
            		//Respuestas NO
            		List<ReporteChecksExpDTO> incidencias = reporteChecksBI.obtieneRespuestasNo(idBitacora);
            		//List<ReporteChecksExpDTO> incidencias = reporteChecksBI.obtieneRespuestasNo(Integer.parseInt("18365"));


            		if (firmasB.get(0).getCeco() != null  && firmasB.get(0).getCeco() != "") {
            			try {
                    		List<SucursalChecklistDTO> lista = sucursalChecklistBI.obtieneDatos(Integer.parseInt(firmasB.get(0).getCeco()));

                    		if(lista!=null) {
	        	        			for (SucursalChecklistDTO sucursalChecklistDTO : lista) {
	        						if(sucursalChecklistDTO!=null && sucursalChecklistDTO.getRuta()!=null && !sucursalChecklistDTO.getRuta().equals("")) {
	        							rutaImg = sucursalChecklistDTO.getRuta();
	        						}
	        					}
	        	        		}



            			} catch (Exception e) {
            				logger.info("SIN IMAGEN DE FACHADA DE SUCURSALES ");
            			}

            		}

            		//correoBI.sendMailExpansion(economico,sucursal,tipo,territorio,zona,imperdonablesLista,firmasB,incidencias,aperturable,rutaImg);
            		//correoBI.sendMailExpansionNuevoFormato(idBitacora+"",tipoVisita,fechaVisita,economico,sucursal,tipo,territorio,zona,imperdonablesLista,firmasB,incidencias,aperturable,rutaImg);

            		try {
            			correoBI.sendMailExpansionNuevoFormatoConteo(idBitacora+"",tipoVisita,fechaVisita,economico,sucursal,tipo,territorio,zona,imperdonablesLista,firmasB,incidencias,aperturable,rutaImg,tipoNegocio);
            			//correoBI.actaEntregaSucursal(idBitacora+"", tipoVisita, fechaVisita, economico,sucursal, tipo, territorio, zona, imperdonablesLista, firmasB, incidencias, aperturable,rutaImg,tipoNegocio);
            		} catch (Exception e) {
            			logger.info(e);
            			logger.info("AP AL ENVIAR EL EXPANSION ANTERIOR");
            		}

            		try {
            			correoBI.actaEntregaSucursal(idBitacora+"", tipoVisita, fechaVisita, economico,sucursal, tipo, territorio, zona, imperdonablesLista, firmasB, incidencias, aperturable,rutaImg,tipoNegocio); //SeAAgregaNuevaActa
            		} catch (Exception e) {
            			logger.info(e);
            			logger.info("AP AL ENVIAR EL CORREO EXPANSION NUEVO ALEXANDER");
            		}


            }
        } catch (Exception e) {
                logger.info(e);
                return false;
        }

        logger.info("SALI sendCorreoApertura");
            return true;
    }
     */
// http://localhost:8080/checklist/servicios/sendCorreoApertura.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/sendCorreoAperturaNuevoFormato", method = RequestMethod.POST)
    public @ResponseBody
    Boolean sendCorreoAperturaNuevoFormato(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                //logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                //logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }

            /*
            contenido = "{ " +
            	   "\"eco\":\"9995 \","+
            	   "\"sucursal\":\"MEGA AV BELEN QRO \"," +
            	   "\"territorio\":\"TERRITORIO SB CENTRO PONIENTE\"," +
            	   "\"tipo\":\"Nueva\"," +
            	   "\"zona\":\"ZONA SB QUERETARO BAJIO\"," +
            	   "\"region\":\"REGIONAL SB QUERETARO\","+
            	   "\"imperdonable\":["+
            	            "{"+
            	               "\"descripcion\":\"Esto es una Imperdonable nuevo 1 \""+
            	            "}"+

            	            ","+

						"{"+
						"\"descripcion\":\"Esto es una Imperdonable nuevo 2 \""+
						"}"+

						","+

						"{"+
						"\"descripcion\":\"Esto es una Imperdonable nuevo 3 \""+
						"}"+
            	   "],"+
            	   "\"idBitacora\":"+
            	      "20878" +
            	   ","+
            	   "\"aperturbale\":\"false\""+
            	"}";*/
            //Datos
            String economico = "";
            String sucursal = "";
            String tipo = "";
            String territorio = "";
            String zona = "";
            String aperturable = "";
            int idBitacora = 0;

            String tipoVisita = "";
            String fechaVisita = "";
            //Json

            List<String> imperdonablesLista = new ArrayList<String>();

            String rutaImg = "/franquicia/firmaChecklist/firmalogo_fachada.png";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);

                economico = rec.getString("eco");
                sucursal = rec.getString("sucursal");
                territorio = rec.getString("territorio");
                tipo = rec.getString("tipo");
                zona = rec.getString("zona");
                aperturable = rec.getString("aperturbale");
                idBitacora = rec.getInt("idBitacora");

                tipoVisita = rec.getString("tipoVisita");
                fechaVisita = rec.getString("fecha");

                try {

                    boolean cargaHallazgos = hallazgosExpBI.cargaHallazgos(idBitacora);

                    if (cargaHallazgos) {
                        logger.info("HALLAZGOS CARGADOS DE LA BIT PAPÁ ---> " + idBitacora);
                    } else {
                        logger.info("AP al CARGAR HALLAZGOS DE LA BIT PAPÁ ---> " + idBitacora);
                    }

                } catch (Exception e) {
                    logger.info("AP al CARGAR HALLAZGOS DE LA BIT PAPÁ ---> " + idBitacora);
                }

                JSONArray results = rec.getJSONArray("imperdonable");

                for (int i = 0; i < results.length(); i++) {
                    JSONObject data = results.getJSONObject(i);
                    String result = data.getString("descripcion");
                    imperdonablesLista.add(result);
                }

                //Firmas
                List<FirmaCheckDTO> firmasB = firmaCheckBI.obtieneDatos(idBitacora + "");
                //List<FirmaCheckDTO> firmasB = firmaCheckBI.obtieneDatos("20878");
                //Respuestas NO
                List<ReporteChecksExpDTO> incidencias = reporteChecksBI.obtieneRespuestasNo(idBitacora);
                //List<ReporteChecksExpDTO> incidencias = reporteChecksBI.obtieneRespuestasNo(Integer.parseInt("18365"));

                if (firmasB.get(0).getCeco() != null && firmasB.get(0).getCeco() != "") {
                    try {
                        List<SucursalChecklistDTO> lista = sucursalChecklistBI.obtieneDatos(Integer.parseInt(firmasB.get(0).getCeco()));

                        if (lista.get(0) != null) {
                            rutaImg = lista.get(0).getRuta();

                        }

                        if (lista.get(1) != null) {
                            rutaImg = lista.get(1).getRuta();
                        }

                    } catch (Exception e) {

                        //System.out.println("SIN IMAGEN DE FACHADA DE SUCURSALES");
                    }

                }

                correoBI.sendMailExpansionNuevoFormato(idBitacora + "", tipoVisita, fechaVisita, economico, sucursal, tipo, territorio, zona, imperdonablesLista, firmasB, incidencias, aperturable, rutaImg);

            }
        } catch (Exception e) {
            logger.info(e);
            return false;
        }

        logger.info("SALI sendCorreoApertura");
        return true;
    }

    /*
// http://localhost:8080/checklist/servicios/postAltaFirmaCheck.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/postAltaFirmaCheck", method = RequestMethod.POST)
    public @ResponseBody

    String postAltaFirmaCheck(@RequestBody String provider, HttpServletRequest request,HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {
        String json = "";
        String res = null;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";
        boolean guardada = false;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                //logger.info("URIDES IOS: " + urides);
                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                //logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());
                //logger.info("JSON IOS: " + contenido);

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];
            String ceco = "";
            String bitacora = "";
            String puesto = "";
            String responsable = "";
            String correo = "";
            String nombre = "";
            String ruta = "";
            String FProveedor = "";
            String FGerente = "";

            if (contenido != null) {

            		contenido = contenido.replaceAll("\\n", "");
            		contenido = contenido.replaceAll("\\r", "");
                JSONObject rec = new JSONObject(contenido);
                ceco = rec.getString("ceco");
                bitacora = rec.getString("bitacora");
                puesto = rec.getString("puesto");
                responsable = rec.getString("responsable");
                correo = rec.getString("correo");
                FProveedor = rec.getString("ruta");
            }


            boolean respuesta = false;
            File r = null;
            File r2 = null;
            String rootPath = File.listRoots()[0].getAbsolutePath();
            Calendar cal = Calendar.getInstance();
            String rutaFirmaBD = "";
            String rutaFirmaJBD = "";

            // DEFINIR RUTA
            String rutaFirma = "";
            // 1.- ::: VERIFICAR EN DONDE ESTOY PARA GUARDAR EN (
            // /Sociounico/checklist/imagenes/ - /Sociounico/checklist/preview/ ) O EN EL
            // NUEVO
            // COMENTO ESTO POR QUE AUN NO SE APLICA LA VERSION DE checklist

            rutaFirma = "/franquicia/firmaChecklist/firma/";
            String rutaPATHFirma = rootPath + rutaFirma;
            r2 = new File(rutaPATHFirma);

            if (r2.mkdirs()) {
                logger.info("SE HA CREADA LA CARPETA");
            } else {
                logger.info("EL DIRECTORIO YA EXISTE");
            }

            FileOutputStream fos = null;
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] firmaDecodificada = decoder.decodeBuffer(FProveedor);
            // byte[] rutaGerenteDecodificada = decoder.decodeBuffer(FGerente);
            // //System.out.println(ruta+new String(imgDecodificada)+"."+new
            // String(extDecodificada));
            Date fechaActual = new Date();

            //Formateando la fecha:

            DateFormat formatoHora = new SimpleDateFormat("HHmmss");
            DateFormat formatoFecha = new SimpleDateFormat("ddMMyyyy");
            String hora = formatoFecha.format(fechaActual);
            String fecha = formatoHora.format(fechaActual);
            String nomDecodificadoStr = "" + ceco + "" + fecha + "" + hora;
            String extDecodificadaStr = "jpg";
            String rutaTotalF = rutaPATHFirma + nomDecodificadoStr + "." + extDecodificadaStr;
            String rutaTotalFJ = rutaPATHFirma + nomDecodificadoStr + "J" + "." + extDecodificadaStr;
            FileOutputStream fileOutputF = new FileOutputStream(rutaTotalF);
            //FileOutputStream fileOutputFJ = new FileOutputStream(rutaTotalFJ);

            try {
                BufferedOutputStream bufferOutputF = new BufferedOutputStream(fileOutputF);
                //BufferedOutputStream bufferOutputFJ = new BufferedOutputStream(fileOutputFJ);
                // se escribe el archivo decodificado
                bufferOutputF.write(firmaDecodificada);
                //   bufferOutputFJ.write(rutaGerenteDecodificada);
                bufferOutputF.close();
                //bufferOutputFJ.close();
                //rutaFoto = rutalimpia;
                rutaFirmaBD = rutaTotalF;
                //rutaFirmaJBD = rutaTotalFJ;
            } finally {
                fileOutputF.close();
                //fileOutputFJ.close();
            }
            respuesta = true;
            respuesta = false;

            FirmaCheckDTO firm = new FirmaCheckDTO();
            firm.setCeco(ceco);
            firm.setBitacora(bitacora);
            firm.setPuesto(puesto);
            firm.setResponsable(responsable);
            firm.setCorreo(correo);
            firm.setRuta(rutaFirmaBD);

            int resp = firmaCheckBI.inserta(firm);
            if (resp != 0) {
                resp = 1;
            }

            JsonObject envoltorioJsonObj = new JsonObject();
            envoltorioJsonObj.addProperty("response", resp);
            logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }*/
    // http://localhost:8080/checklist/servicios/postAltaFirmaCheck.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/postAltaFirmaCheckArray", method = RequestMethod.POST)
    public @ResponseBody
    String postAltaFirmaCheckArray(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        String res = null;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                //logger.info("URIDES IOS: " + urides);
                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                //logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());
                //logger.info("JSON IOS: " + contenido);

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }

            if (contenido != null) {

                contenido = contenido.replaceAll("\\n", "");
                contenido = contenido.replaceAll("\\r", "");

                JSONArray jsonArray = new JSONArray(contenido);

                // **************************************************************************
                FirmaExpansionBI firmaHilos[] = new FirmaExpansionBI[jsonArray.length()];

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject rec = jsonArray.getJSONObject(i);
                    firmaHilos[i] = new FirmaExpansionBI(rec.getString("ceco"), rec.getString("bitacora"), rec.getString("puesto"), rec.getString("responsable"), rec.getString("correo"), rec.getString("ruta"), (i));
                }

                for (int j = 0; j < firmaHilos.length; j++) {
                    firmaHilos[j].start();
                    Thread.sleep(1000);
                }

                boolean terminaron = false;
                boolean fallo = false;

                long startTime1 = System.currentTimeMillis();
                while (!terminaron) {
                    for (int j = 0; j < firmaHilos.length; j++) {

                        logger.info("getRespuesta en Hilos: " + j);

                        if (firmaHilos[j].getRespuesta() == 1) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            fallo = true;
                            break;
                        }

                    }

                    for (int j = 0; j < firmaHilos.length; j++) {

                        logger.info("TIEMPO RESP en Hilos: " + j);

                        if (!firmaHilos[j].isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }

                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                logger.info("TIEMPO RESP en Hilos: " + endTime1);

                // **************************************************************************
                JsonObject envoltorioJsonObj = new JsonObject();
                if (fallo) {
                    envoltorioJsonObj.addProperty("response", false);
                } else {
                    envoltorioJsonObj.addProperty("response", true);
                }

                logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
                res = envoltorioJsonObj.toString();

            }

        } catch (Exception e) {
            logger.info("postAltaFirmaCheckArray.json e " + e);
            res = null;
        }

        if (res != null) {
            return res.toString();
        } else {
            JsonObject envoltorioJsonObj = new JsonObject();
            envoltorioJsonObj.addProperty("response", false);
            return envoltorioJsonObj.toString();
        }

    }

    // http://localhost:8080/checklist/servicios/postAltaImagSuc.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/postAltaImagSuc", method = RequestMethod.POST)
    public @ResponseBody

    String postAltaImagSuc(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {
        String json = "";
        String res = null;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";
        boolean guardada = false;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                //logger.info("URIDES IOS: " + urides);
                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                //logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());
                //logger.info("JSON IOS: " + contenido);

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];
            int idSucursal = 0;
            String nombre = "";
            String observ = "";
            String FProveedor = "";

            if (contenido != null) {

                contenido = contenido.replaceAll("\\n", "");
                contenido = contenido.replaceAll("\\r", "");

                JSONObject rec = new JSONObject(contenido);
                idSucursal = rec.getInt("idSucursal");
                nombre = rec.getString("nombre");
                observ = rec.getString("observ");
                FProveedor = rec.getString("ruta");
            }

            boolean respuesta = false;
            File r = null;
            File r2 = null;
            String rootPath = File.listRoots()[0].getAbsolutePath();
            Calendar cal = Calendar.getInstance();
            String rutaFirmaBD = "";

            // DEFINIR RUTA
            String rutaFirma = "";
            // 1.- ::: VERIFICAR EN DONDE ESTOY PARA GUARDAR EN (
            // /Sociounico/checklist/imagenes/ - /Sociounico/checklist/preview/ ) O EN EL
            // NUEVO
            // COMENTO ESTO POR QUE AUN NO SE APLICA LA VERSION DE checklist

            rutaFirma = "/franquicia/ImagSucursal/";
            String rutaPATHFirma = rootPath + rutaFirma;
            r2 = new File(rutaPATHFirma);

            logger.info("rutaPATHFirma: " + rutaPATHFirma);

            if (r2.mkdirs()) {
                logger.info("SE HA CREADA LA CARPETA");
            } else {
                logger.info("EL DIRECTORIO YA EXISTE");
            }

            FileOutputStream fos = null;
            Base64.Decoder decoder = Base64.getDecoder();
            byte[] firmaDecodificada = decoder.decode(FProveedor);
            // byte[] rutaGerenteDecodificada = decoder.decodeBuffer(FGerente);
            // //System.out.println(ruta+new String(imgDecodificada)+"."+new
            // String(extDecodificada));
            Date fechaActual = new Date();

            //Formateando la fecha:
            DateFormat formatoHora = new SimpleDateFormat("HHmmss");
            DateFormat formatoFecha = new SimpleDateFormat("ddMMyyyy");
            String hora = formatoFecha.format(fechaActual);
            String fecha = formatoHora.format(fechaActual);
            String nomDecodificadoStr = "" + idSucursal + "" + fecha + "" + hora;
            String extDecodificadaStr = "jpg";
            String rutaTotalF = rutaPATHFirma + nomDecodificadoStr + "." + extDecodificadaStr;
            String rutaTotalFJ = rutaPATHFirma + nomDecodificadoStr + "J" + "." + extDecodificadaStr;
            FileOutputStream fileOutputF = new FileOutputStream(rutaTotalF);
            //FileOutputStream fileOutputFJ = new FileOutputStream(rutaTotalFJ);

            try {
                BufferedOutputStream bufferOutputF = new BufferedOutputStream(fileOutputF);
                //BufferedOutputStream bufferOutputFJ = new BufferedOutputStream(fileOutputFJ);
                // se escribe el archivo decodificado
                bufferOutputF.write(firmaDecodificada);
                //   bufferOutputFJ.write(rutaGerenteDecodificada);
                bufferOutputF.close();
                //bufferOutputFJ.close();
                //rutaFoto = rutalimpia;
                rutaFirmaBD = rutaTotalF;
                //rutaFirmaJBD = rutaTotalFJ;
            } finally {
                fileOutputF.close();
                //fileOutputFJ.close();
            }
            respuesta = true;
            respuesta = false;

            SucursalChecklistDTO suci = new SucursalChecklistDTO();

            suci.setIdSucursal(idSucursal);
            suci.setNombre(nombre);
            suci.setObserv(observ);
            suci.setRuta(rutaFirmaBD);

            int resp = sucursalChecklistBI.inserta(suci);
            if (resp != 0) {
                resp = 1;
            }

            JsonObject envoltorioJsonObj = new JsonObject();
            envoltorioJsonObj.addProperty("response", resp);
            logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/franquicia/servicios/getFotoSucursal.json?idUsuario<?>&idCeco<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getFotoSucursal", method = RequestMethod.GET)
    public @ResponseBody
    String getFotoSucursal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int ceco = Integer.parseInt(urides.split("&")[1].split("=")[1]);

            List<SucursalChecklistDTO> lista = sucursalChecklistBI.obtieneDatos(ceco);

            JsonObject envoltorioJsonObj = new JsonObject();
            JsonArray provJsonArray = new JsonArray();
            String encoded = null;
            String rutaFoto = null;
            if (!lista.isEmpty()) {
                String rutapreview = "";
                for (SucursalChecklistDTO aux : lista) {
                    JsonObject provJsonObj = new JsonObject();
                    if (aux.getRuta() != null) {
                        rutapreview = aux.getRuta();
                        rutaFoto = aux.getRuta();

                        if (!rutapreview.equals("")) {
                            if (rutapreview != null) {
                                if (!rutapreview.contains("franquicia")) {
                                    rutapreview = null;
                                }
                            }

                            if (!rutapreview.equals("")) {
                                Path pdfPath = Paths.get(rutapreview);
                                byte[] pdf = Files.readAllBytes(pdfPath);

                                encoded = new String(Base64.getEncoder().encode(pdf), "UTF-8");

                            }
                        }

                    }
                    provJsonObj.addProperty("CECO", aux.getIdSucursal());
                    provJsonObj.addProperty("ID_IMAGEN", aux.getIdImag());
                    provJsonObj.addProperty("NOMBRE", aux.getNombre());
                    provJsonObj.addProperty("OBSERVACION", aux.getObserv());
                    provJsonObj.addProperty("PERIODO", aux.getPeriodo());
                    provJsonObj.addProperty("IMAGEN64", encoded);

                    provJsonArray.add(provJsonObj);
                }

            }

            /*
	                //envoltorioJsonObj.addProperty("IMAGEN", encoded);


            if (lista.isEmpty() || lista == null) {
                envoltorioJsonObj.add("FOTOSUC", null);
                envoltorioJsonObj.addProperty("FOTO", encoded);
                envoltorioJsonObj.addProperty("RUTA_FOTO", rutaFoto);

	            } else {
	                envoltorioJsonObj.add("FOTOSUC", provJsonArray);
	                envoltorioJsonObj.addProperty("FOTO", encoded);
	                envoltorioJsonObj.addProperty("RUTA_FOTO", rutaFoto);
	            }
             */
            logger.info("JSON Foto Suc : " + provJsonArray.toString());
            res = provJsonArray.toString();

        } catch (Exception e) {
            logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    //*Este servicio se utiliza para la descarga de las respuestas de la última bitacóta de una sucursal
    //* Si el parámetro de bitácora es diferente a 0 se obtienen las respuestas correspondiente al idChecklist que le corresponde a esa bitácora
    //* Si el parámetro de bitácora es 0 se obtenen las ultimas respuestas correspondientes a un idChecklist de una asignación (CECO-PROYECTO-FASE)
    //http://localhost:8080/franquicia/servicios/getRespuestasByBitacoraNueva.json?idUsuario<?>&idBitacora<?>&idChecklist=<?>&idCeco=<?>&idProyecto=<?>&idFase=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getRespuestasByBitacoraNueva", method = RequestMethod.GET)
    public @ResponseBody
    String getRespuestasByBitacoraNueva(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idChecklist = 0;
            int idCeco = 0;
            int idFase = 0;
            int idProyecto = 0;

            //int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            int idBitacora = Integer.parseInt(urides.split("&")[1].split("=")[1]);

            if (idBitacora == 0 && urides.split("&").length > 2) {

                idChecklist = Integer.parseInt(urides.split("&")[2].split("=")[1]);
                idCeco = Integer.parseInt(urides.split("&")[3].split("=")[1]);
                idFase = Integer.parseInt(urides.split("&")[4].split("=")[1]);
                idProyecto = Integer.parseInt(urides.split("&")[5].split("=")[1]);
            }

            String lista = "";

            if (idBitacora != 0) {
                lista = checklistPreguntasComBI.obtieneDatos(idBitacora + "");
            } else {
                lista = checklistPreguntasComBI.obtieneSoftCecoFaseProyectoCom(idCeco + "", idFase + "", idProyecto + "", idChecklist + "");
            }

            //JsonObject envoltorioJsonObj = new JsonObject();
            /*JsonArray repArr=new JsonArray();

    		if(lista!=null && !lista.isEmpty()) {

    			for(ChecklistPreguntasComDTO aux:lista) {

    				JsonObject auxJsonObj = new JsonObject();

    				auxJsonObj.addProperty("idBitacora", aux.getIdBita());
    				auxJsonObj.addProperty("idRespuesta", aux.getIdResp());
    				//RESPREG
    				auxJsonObj.addProperty("idArbol", aux.getIdArbol());
    				auxJsonObj.addProperty("observaciones", aux.getObserv());
    				auxJsonObj.addProperty("idPregunta", aux.getIdPreg());
    				auxJsonObj.addProperty("idChecklist", aux.getIdCheck());
    				auxJsonObj.addProperty("idPosibleRespuesta", aux.getIdPosible());
    				auxJsonObj.addProperty("idObservacion", aux.getIdObserv());
    				auxJsonObj.addProperty("posibleRespuesta", aux.getPosible());
    				auxJsonObj.addProperty("pregunta", aux.getPregunta());
    				auxJsonObj.addProperty("critica", aux.getIdcritica());
    				auxJsonObj.addProperty("ruta", aux.getRuta());
    				auxJsonObj.addProperty("idCheckUsua", aux.getIdcheckUsua());
    				auxJsonObj.addProperty("idUsuario", aux.getIdUsu());
    				auxJsonObj.addProperty("idCeco", aux.getCeco());
    				auxJsonObj.addProperty("nombreChecklist", aux.getChecklist());
    				auxJsonObj.addProperty("idOrdenGrupo", aux.getIdOrdenGru());
    				auxJsonObj.addProperty("idPeridicidad", aux.getIdperiodicidad());
    				auxJsonObj.addProperty("respuestaAbierta", aux.getRespuestaAbierta());
    				auxJsonObj.addProperty("idRespuestaAbierta", aux.getIdRespAb());
    				auxJsonObj.addProperty("banderaRespuestaAbierta", aux.getBanderaRespAb());
    				auxJsonObj.addProperty("modulo", aux.getModulo());
    				auxJsonObj.addProperty("precalificación", aux.getPrecalif());
    				auxJsonObj.addProperty("calificación", aux.getCalif());
    				//Es id Grupo


    				repArr.add(auxJsonObj);
    			}
    		}

    		if(lista==null && lista.isEmpty()) {
    			envoltorioJsonObj.add("Respuestas", null);
    		}
    		else {
    			envoltorioJsonObj.add("Respuestas", repArr);
    		}


    		logger.info("RESPUESTAS: " + envoltorioJsonObj.toString());
             */
            res = lista;

        } catch (Exception e) {
            logger.info(e);
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    //http://localhost:8080/checklist/servicios/getAltaCeckin.json?idUsuario<?>&idBitacora=<?>&idCeco=<?>&fecha=<?>&latitud=<?>&longitud=<?>&bandera=<?>&plataforma=<?>&tipoConexion=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getAltaCeckin", method = RequestMethod.GET)
    public @ResponseBody
    String getAltaCeckin(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = null;
        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];

            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            int idBitacora = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            String idCeco = urides.split("&")[2].split("=")[1];
            String fecha = urides.split("&")[3].split("=")[1];
            String latitud = urides.split("&")[4].split("=")[1];
            String longitud = urides.split("&")[5].split("=")[1];
            int bandera = Integer.parseInt(urides.split("&")[6].split("=")[1]);
            String plataforma = urides.split("&")[7].split("=")[1];
            int tipoConexion = Integer.parseInt(urides.split("&")[8].split("=")[1]);

            CheckinDTO asistencia = new CheckinDTO();
            asistencia.setIdBitacora(idBitacora);
            asistencia.setIdUsuario(idUsu);
            asistencia.setIdCeco(idCeco);
            asistencia.setFecha(fecha);
            asistencia.setLatitud(latitud);
            asistencia.setLongitud(longitud);
            asistencia.setBandera(bandera);
            asistencia.setPlataforma(plataforma);
            asistencia.setTiempoConexion(tipoConexion);

            int respuesta = checkinBI.insertaCheckin(asistencia);

            JsonObject envoltorioJsonObj = new JsonObject();

            if (respuesta == 0) {
                envoltorioJsonObj.addProperty("ALTA", 0);
            } else {
                envoltorioJsonObj.addProperty("ALTA", 1);
            }

            logger.info("CHECK-IN: Bitacora:" + idBitacora + " " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info("AP CHECK_IN SERVICIO LOGIN 5072");
            return "{}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/checklist/servicios/postEvidenciaChecklist.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/postEvidenciaChecklist", method = RequestMethod.POST)
    public @ResponseBody
    String postEvidenciaChecklist(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        //System.out.println("-------------------Entro a guardar evidencia checklist 1--------------------------");
        String res = null;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }

            //System.out.println("-------------------Entro a guardar evidencia checklist 2-------------------------- "+contenido);
            String nomArchivo = "";
            String archivo = "";
            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                nomArchivo = rec.getString("nomArchivo");
                archivo = rec.getString("archivo");
            }
            //System.out.println("-------------------Entro a guardar evidencia checklist 3-------------------------- "+nomArchivo);

            Date fechaActual = new Date();
            //System.out.println(fechaActual);
            //System.out.println("-------------------Entro a guardar evidencia checklist 4--------------------------");
            //Formateando la fecha:
            DateFormat formatoAnio = new SimpleDateFormat("yyyy");
            String anio = formatoAnio.format(fechaActual);
            File r2 = null;
            String rootPath = File.listRoots()[0].getAbsolutePath();
            String rutaArchivo = "/franquicia/imagenes/" + anio + "/";

            String rutaPATHArchivo = rootPath + rutaArchivo;
            r2 = new File(rutaPATHArchivo);

            if (r2.mkdirs()) {
                logger.info("SE HA CREADA LA CARPETA");
            } else {
                logger.info("EL DIRECTORIO YA EXISTE");
            }

            Base64.Decoder decoder = Base64.getDecoder();
            byte[] archivoDecodificada = decoder.decode(archivo);

            String rutaTotalF = rutaPATHArchivo + nomArchivo;
            //System.out.println("-------------------Entro a guardar evidencia checklist 5--------------------------" + rutaTotalF);
            FileOutputStream fileOutputF = new FileOutputStream(rutaTotalF);
            //System.out.println("-------------------Entro a guardar evidencia checklist 6-------------------------- " + fileOutputF);
            int alta = 0;
            try {
                BufferedOutputStream bufferOutputF = new BufferedOutputStream(fileOutputF);
                bufferOutputF.write(archivoDecodificada);
                bufferOutputF.close();
                alta = 1;
                logger.info("SE HA GUARDADO LA IMAGEN EN: " + rutaTotalF);
            } catch (Exception ex) {
                alta = 0;
                logger.info("Ocurrio algo: ");
            } finally {
                fileOutputF.close();
            }

            JsonObject envoltorioJsonObj = new JsonObject();
            envoltorioJsonObj.addProperty("ALTA", alta);
            logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info("Ocurrio algo en postEvidenciaChecklist");
            return "{}";
        }

        if (res != null) {
            //System.out.println("-------------------Entro a guardar evidencia checklist 7-------------------------- " + res);
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/checklist/servicios/getVersionesExpansion.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getVersionesExpansion.json", method = RequestMethod.GET)
    public @ResponseBody
    String getVersionesChecklist(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        String res = null;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            HashMap<String, List<VersionExpanDTO>> listaVersion = new HashMap<String, List<VersionExpanDTO>>();
            listaVersion = versionExpanBI.getInfoVersiones();

            //System.out.println(listaVersion.size());
            JSONObject envoltorioJsonObj = new JSONObject();

            JSONArray objArrayVersiones = new JSONArray();

            JSONArray objArrayId = new JSONArray();

            for (Entry<String, List<VersionExpanDTO>> entry : listaVersion.entrySet()) {

                objArrayId = new JSONArray();

                for (VersionExpanDTO obj : entry.getValue()) {

                    objArrayId.put(obj.getIdCheck());

                }

                JSONObject versionObj = new JSONObject();
                versionObj.put("Version", entry.getKey());
                versionObj.put("ids", objArrayId);

                objArrayVersiones.put(versionObj);

            }

            envoltorioJsonObj.put("resultado", objArrayVersiones);

            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info("AP con el Servicio de Versiones Expansion");
            return "{}";
        }

        return res;

    }

    // http://localhost:8080/checklist/servicios/getCecosSup.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getCecosSup.json", method = RequestMethod.GET)
    public @ResponseBody
    String getCecosSup(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        String res = null;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);

            DetalleProtoDTO detalle = new DetalleProtoDTO();
            detalle.setIdUsuario(idUsu);
            detalle.setIdOrdeGrupo(324);

            List<DetalleProtoDTO> lista = detalleProtoBI.ObtieneNombreCeco(detalle);

            JsonObject envoltorioJsonObj = new JsonObject();

            JsonArray cecoJArray = new JsonArray();

            if (lista != null && !lista.isEmpty()) {
                for (DetalleProtoDTO aux : lista) {
                    JsonObject ccJsonObj = new JsonObject();
                    ccJsonObj.addProperty("ID_CECO", aux.getIdCeco());
                    ccJsonObj.addProperty("NOM_CECO", aux.getNomCeco());

                    cecoJArray.add(ccJsonObj);
                }
            }
            envoltorioJsonObj.add("CECOS", cecoJArray);

            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info("AP con el Servicio de Versiones Expansion");
            return "{}";
        }

        return res;

    }

    // http://localhost:8080/checklist/servicios/altaEmpContacto.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/altaEmpContacto", method = RequestMethod.POST)
    public @ResponseBody
    Boolean altaEmpContacto(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        int cantidadAlta = 0;
        int iteraciones = 0;

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String contenido = "";
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                //logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                //logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }

            //Datos
            String ceco = "";
            String domicilio = "";
            String nombre = "";
            String telefono = "";
            String contacto = "";
            String area = "";
            String compania = "";
            String aux = "";
            String aux2 = "";

            int bitacoraHermana = 0;
            int idUsuario = 0;

            int respuesta = 0;

            //Json
            try {
                if (contenido != null) {
                    JSONObject rec = new JSONObject(contenido);
                    ceco = rec.getString("ceco");
                    bitacoraHermana = rec.getInt("idBitacora");
                    idUsuario = rec.getInt("idUsuario");

                    JSONArray results = rec.getJSONArray("contactos");
                    iteraciones = results.length();
                    for (int i = 0; i < results.length(); i++) {
                        respuesta = 0;
                        JSONObject data = results.getJSONObject(i);
                        domicilio = data.getString("domicilio");
                        nombre = data.getString("nombre");
                        telefono = data.getString("telefono");
                        contacto = data.getString("contacto");
                        compania = data.getString("compania");
                        aux = data.getString("aux");
                        aux2 = data.getString("aux2");
                        area = data.getString("area");

                        EmpContacExtDTO empContacExtDAO = new EmpContacExtDTO();

                        empContacExtDAO.setCeco(ceco);
                        empContacExtDAO.setArea(area);
                        empContacExtDAO.setAux(aux2);
                        empContacExtDAO.setAux2(aux2);
                        empContacExtDAO.setBitGral(bitacoraHermana);
                        empContacExtDAO.setCeco(ceco);
                        empContacExtDAO.setCompania(compania);
                        empContacExtDAO.setContacto(contacto);
                        empContacExtDAO.setDomicilio(domicilio);
                        empContacExtDAO.setIdUsu(idUsuario);
                        empContacExtDAO.setNombre(nombre);
                        empContacExtDAO.setTelefono(telefono);
                        respuesta = empContactExtBI.inserta(empContacExtDAO);
                        if (respuesta == 0) {
                            logger.info("No fue posible dar de Alta el Contacto " + empContacExtDAO);
                        } else {
                            cantidadAlta++;
                        }
                    }
                }
            } catch (Exception e) {
                logger.info("No fue posible dar de Alta el Contacto " + e);
                return false;
            }
        } catch (Exception e) {
            logger.info("No fue posible dar de Alta el Contacto " + e);
            return false;
        }
        if (cantidadAlta != iteraciones) {
            logger.info("No fue posible dar de Alta el Contacto ");
            return false;
        } else {
            logger.info("Contactos dados de Alta");
            return true;
        }
    }

    //SERVICIOS DE EVIDENCIAS
    // http://localhost:8080/checklist/servicios/getHallazgosPorCeco.json?idUsuario=<?>&idCeco=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getHallazgosPorCeco", method = RequestMethod.GET)
    public @ResponseBody
    String getHallazgosPorCeco(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = "";
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String listaHallazgosResult = "[ ]";
        String listaEvidenciasResult = "[ ]";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            //480100
            int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            int ceco = Integer.parseInt(urides.split("&")[1].split("=")[1]);

            //Obtiene los hallazgos por CECO
            try {

                List<HallazgosExpDTO> listaHallazgos = hallazgosExpBI.obtieneDatos(ceco);

                List<HallazgosEviExpDTO> listaEvidencias = hallazgosEviExpBI.obtieneDatosBita(ceco);

                if (listaHallazgos != null && listaHallazgos.size() > 0) {

                    listaHallazgosResult = new Gson().toJson(listaHallazgos);

                }

                if (listaEvidencias != null && listaEvidencias.size() > 0) {

                    listaEvidenciasResult = new Gson().toJson(listaEvidencias);

                }

                res = "{";
                res += " \"hallazgos\"  : " + listaHallazgosResult + ",";
                res += " \"evidencias\" : " + listaEvidenciasResult;
                res += "}";

            } catch (Exception e) {
                logger.info("AP Al obtener la lista de hallazgos/evidencias");
                return "{}";
            }

        } catch (Exception e) {
            logger.info("AP con los parametros de obtener hallaazgos");
            return "{}";
        }

        return res;

    }

    // http://localhost:8080/checklist/servicios/updateHallazgo.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/updateHallazgo", method = RequestMethod.POST)
    public @ResponseBody
    boolean updateHallazgo(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        logger.info(provider);

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String contenido = "";
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                // logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                // logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }
            // Json
            try {
                /*
				 contenido = "{ " +
			        	   "\"hallazgos\":["+
			        	   		"{"+
			        	               "\"idResp\":19224"+
			        	               ","+

										"\"status\":666"+
										","+

										"\"respuesta\":\"Esto es una Respuesta ACTUALIZADA 1\""+
										","+

										"\"ruta\":\"Esto es una Ruta 1\""+

								"}"+

								","+

									"{"+
									"\"idResp\":19225"+
									","+

										"\"status\":999"+
										","+

										"\"respuesta\":\"Esto es una Respuesta ACTUALIZADA 2 \""+
										","+

										"\"ruta\":\"Esto es una Ruta 2 \""+

									"}"+

			        	   "]" +
			        	"}";*/

                if (contenido != null) {

                    JSONObject rec = new JSONObject(contenido);

                    JSONArray results = rec.getJSONArray("hallazgos");

                    boolean banderaCompleto = true;
                    Boolean actualizaResponse = true;

                    for (int i = 0; i < results.length(); i++) {

                        JSONObject data = results.getJSONObject(i);

                        HallazgosExpDTO bean = new HallazgosExpDTO();

                        int idResp = 0;
                        int idHallazgo = 0;
                        int status = 0;
                        String respuesta = "";
                        String ruta = "";
                        String fechaAutorizacion = "";
                        String fechaFin = "";
                        String comentario = "";
                        String observacionNueva = "";
                        String motivoRechazo = "";
                        String usuModif = "";

                        idResp = data.getInt("idResp");
                        idHallazgo = data.getInt("idHallazgo");
                        status = data.getInt("status");
                        respuesta = data.getString("respuesta");
                        ruta = data.getString("ruta");
                        fechaAutorizacion = data.getString("fechaAutorizacion");
                        fechaFin = data.getString("fechaFin");
                        comentario = data.getString("comentario");
                        observacionNueva = data.getString("obsNueva");
                        motivoRechazo = data.getString("motivoRechazo");
                        usuModif = data.getString("usuModif");

                        bean.setIdHallazgo(idHallazgo);
                        bean.setIdResp(idResp);
                        bean.setStatus(status);
                        bean.setRespuesta(respuesta);
                        bean.setRuta(ruta);
                        bean.setFechaAutorizo(fechaAutorizacion);
                        bean.setFechaFin(fechaFin);
                        bean.setObs(comentario);
                        //Son observaciones del hallazgo atendido
                        bean.setObsNueva(observacionNueva);
                        bean.setMotivrechazo(motivoRechazo);
                        bean.setUsuModif(usuModif);

                        actualizaResponse = hallazgosExpBI.actualizaResp(bean);

                        if (!actualizaResponse) {
                            logger.info("No fue posible ACTUALIZAR el HALLAZGO ");
                            banderaCompleto = false;
                        }
                    }

                    if (banderaCompleto) {
                        logger.info("ACTUALIZACION COMPLETA");
                        return true;

                    } else {
                        logger.info("ACTUALIZACION IN-COMPLETA");
                        return false;
                    }
                }
            } catch (Exception e) {
                logger.info("AP AL OBTENER EL JSON DE HALLAZGOS A ACTUALIZAR " + e);
                return false;
            }
        } catch (Exception e) {
            logger.info("AP AL OBTENER PARAMETROS DE REQUEST" + e);
            return false;
        }

        return false;
    }

    // http://localhost:8080/checklist/servicios/insertaEvidenciaHallazgo.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/insertaEvidenciaHallazgo", method = RequestMethod.POST)
    public @ResponseBody
    boolean insertaEvidenciaHallazgo(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String contenido = "";
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                // logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                // logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }
            // Json
            try {

                /*contenido = "{ " +
			        	   "\"evidencias\":["+
			        	   		"{"+
			        	               "\"idHallazgo\":32"+
			        	               ","+

										"\"idRespuesta\":19224"+
										","+

										"\"ruta\":\"inserteUnaEvidencia.jpg\""+
										","+

										"\"nombre\":\"EvidenciaInsertada\""+

								"}"+

								","+

									"{"+
									"\"idHallazgo\":33"+
									","+

										"\"idRespuesta\":19225"+
										","+

										"\"ruta\":\"inserteUnaEvidencia2.jpg\""+
										","+

										"\"nombre\":\"EvidenciaInsertada2\""+

									"}"+

			        	   "]" +
			        	"}";*/
                if (contenido != null) {

                    JSONObject rec = new JSONObject(contenido);

                    JSONArray results = rec.getJSONArray("evidencias");

                    boolean banderaCompleto = true;
                    int actualizaResponse = 0;

                    for (int i = 0; i < results.length(); i++) {

                        JSONObject data = results.getJSONObject(i);

                        HallazgosEviExpDTO bean = new HallazgosEviExpDTO();

                        int idHallazgo = 0;
                        int idRespuesta = 0;
                        String ruta = "";
                        String nombre = "";
                        int numEvidencia = 1;

                        idHallazgo = data.getInt("idHallazgo");
                        idRespuesta = data.getInt("idRespuesta");
                        ruta = data.getString("ruta");
                        nombre = data.getString("nombre");

                        bean.setIdHallazgo(idHallazgo);
                        bean.setIdResp(idRespuesta);
                        bean.setRuta(ruta);
                        bean.setNombre(nombre);
                        bean.setTotEvi(numEvidencia);

                        actualizaResponse = hallazgosEviExpBI.inserta(bean);

                        if (actualizaResponse == 0) {
                            logger.info("No fue posible INSERTAR LA EVIDENCIA DEL HALLAZGO ");
                            banderaCompleto = false;
                        }
                    }

                    if (banderaCompleto) {
                        logger.info("INSERCION EVIDENCIA COMPLETA");
                        return true;

                    } else {
                        logger.info("INSERCION EVIDENCIA IN-COMPLETA");
                        return false;
                    }
                }
            } catch (Exception e) {
                logger.info("AP AL OBTENER EL JSON DE EVIDENCIA A INSERTAR ");
                return false;
            }
        } catch (Exception e) {
            logger.info("AP AL OBTENER PARAMETROS DE REQUEST DE EVIDENCIA A INSERTAR ");
            return false;
        }

        return false;
    }

    //SERVICIOS PARA EVIDENCIAS
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/getCecosTodos", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getCecosTodos(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        Map<String, Object> reporte = null;

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }
        int idUsuario = Integer.parseInt(urides.split("=")[1]);
        logger.info("USUARIO > " + idUsuario);
        try {
            List<SucUbicacionDTO> lista = sucUbicacionBI.obtieneDatos("21");
            if (lista.size() > 0) {
                reporte = new HashMap<String, Object>();
                reporte.put("listaTodosCecos", lista);
            }

        } catch (Exception e) {
            logger.info("ERROR AL OBTENER EL LISTADO DE CECOS: " + e.getMessage());
            reporte = null;
        }
        return reporte;
    }

    // http://localhost:8080/checklist/servicios/getHandbook.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getHandbook.json", method = RequestMethod.GET)
    public @ResponseBody
    String getHandbook(HttpServletRequest request, HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        String res = null;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);

            List<AdmHandbookDTO> lista = admHandbookBI.obtieneTodos();

            JsonObject envoltorioJsonObj = new JsonObject();

            JsonArray cecoJArray = new JsonArray();

            if (lista != null && !lista.isEmpty()) {
                for (AdmHandbookDTO aux : lista) {
                    JsonObject ccJsonObj = new JsonObject();
                    ccJsonObj.addProperty("ID_BOOK", aux.getIdBook());
                    ccJsonObj.addProperty("DESCRIPCION", aux.getDescripcion());
                    ccJsonObj.addProperty("RUTA", aux.getRuta());
                    ccJsonObj.addProperty("ACTIVO", aux.getActivo());
                    ccJsonObj.addProperty("PADRE", aux.getPadre());
                    ccJsonObj.addProperty("TIPO", aux.getTipo());
                    ccJsonObj.addProperty("FECHA", aux.getFecha());

                    cecoJArray.add(ccJsonObj);
                }
            }
            envoltorioJsonObj.add("HANDBOOK", cecoJArray);

            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info("AP con el Servicio de Versiones Expansion");
            return "{}";
        }

        return res;

    }

    // http://localhost:8080/checklist/servicios/updateHallazgoMov.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/updateHallazgoMov", method = RequestMethod.POST)
    public @ResponseBody
    boolean updateHallazgoMov(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        logger.info(provider);

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String contenido = "";
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                // logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                // logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }
            // Json
            try {

                if (contenido != null) {

                    JSONObject rec = new JSONObject(contenido);

                    JSONArray results = rec.getJSONArray("hallazgos");

                    boolean banderaCompleto = true;
                    Boolean actualizaResponse = true;

                    for (int i = 0; i < results.length(); i++) {

                        JSONObject data = results.getJSONObject(i);

                        HallazgosExpDTO bean = new HallazgosExpDTO();

                        int idResp = 0;
                        int idHallazgo = 0;
                        int status = 0;
                        String respuesta = "";
                        String ruta = "";
                        String fechaAutorizacion = "";
                        String fechaFin = "";
                        String comentario = "";
                        String observacionNueva = "";
                        String motivoRechazo = "";
                        String usuModif = "";

                        idResp = data.getInt("idResp");
                        idHallazgo = data.getInt("idHallazgo");
                        status = data.getInt("status");
                        respuesta = data.getString("respuesta");
                        ruta = data.getString("ruta");
                        fechaAutorizacion = data.getString("fechaAutorizacion");
                        fechaFin = data.getString("fechaFin");
                        comentario = data.getString("comentario");
                        observacionNueva = data.getString("obsNueva");
                        motivoRechazo = data.getString("motivoRechazo");
                        usuModif = data.getString("usuModif");

                        bean.setIdHallazgo(idHallazgo);
                        bean.setIdResp(idResp);
                        bean.setStatus(status);
                        bean.setRespuesta(respuesta);
                        bean.setRuta(ruta);
                        bean.setFechaAutorizo(fechaAutorizacion);
                        bean.setFechaFin(fechaFin);
                        bean.setObs(comentario);
                        //Son observaciones del hallazgo atendido
                        bean.setObsNueva(observacionNueva);
                        bean.setMotivrechazo(motivoRechazo);
                        bean.setUsuModif(usuModif);

                        actualizaResponse = hallazgosExpBI.actualizaMov(bean);

                        if (!actualizaResponse) {
                            logger.info("No fue posible ACTUALIZAR el HALLAZGO ");
                            banderaCompleto = false;
                        }
                    }

                    if (banderaCompleto) {
                        logger.info("ACTUALIZACION COMPLETA");
                        return true;

                    } else {
                        logger.info("ACTUALIZACION IN-COMPLETA");
                        return false;
                    }
                }
            } catch (Exception e) {
                logger.info("AP AL OBTENER EL JSON DE HALLAZGOS A ACTUALIZAR " + e);
                return false;
            }
        } catch (Exception e) {
            logger.info("AP AL OBTENER PARAMETROS DE REQUEST" + e);
            return false;
        }

        return false;
    }

    // servicios/checklistOffline.json?Lg/M4L45zj4Zl05KmbXZmB0FfUnUG08UnAcW0OXp8q0=
    @SuppressWarnings({"unchecked", "static-access"})
    @RequestMapping(value = "/getListaAsigChk", method = RequestMethod.GET)
    public @ResponseBody
    String getListaAsigChk(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        uri = uri.split("&")[0];

        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        System.out.println("CADENA= " + urides);

        int use = Integer.parseInt(urides.split("&")[0].split("=")[1]);
        //int checklist = Integer.parseInt(urides.split("&")[1].split("=")[1]);
        //System.out.println("USUARIO= " + use + "CHECKLIST= " + checklist);
        List<ListaAsignChkDTO> lista = listaChkAsignBI.getListaAsginChk(use);

        Gson gson = null;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        gson = gsonBuilder.create();
        String jsonSalida = gson.toJson(lista);

        return "{\"listaAsignChk\":" + jsonSalida + "}";
    }

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getBitacorasByChk", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getBitacorasByChk(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        int idUsuario = 0;
        int idChecklist = 0;
        Map<String, Object> listasOffline = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                //idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idChecklist = Integer.parseInt(urides.split("&")[1].split("=")[1]);

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                //idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idChecklist = Integer.parseInt(urides.split("&")[1].split("=")[1]);

            }

            System.out.println("ID_CHECKLIST  " + idChecklist);
            System.out.println("ID_USUARIO " + idUsuario);
            listasOffline = checksOfflineBI.getBitacoras(idUsuario, idChecklist);
        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
            listasOffline = null;
        }

        //logger.info("Json getOffline2: "+listasOffline);
        return listasOffline;
    }

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getCatalogoProtocolos", method = RequestMethod.GET)
    public @ResponseBody
    String getCatalogoProtocolos(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        int idUsuario = 0;

        List<ProtocoloDTO> respuesta;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                //idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                //idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);

            }
            respuesta = protocoloBI.getProtocolos();

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
            respuesta = null;
        }
        Gson gson = null;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        gson = gsonBuilder.create();
        String jsonSalida = gson.toJson(respuesta);

        //logger.info("Json getOffline2: "+listasOffline);
        return "{\"listaProtocolos\":" + jsonSalida + "}";
    }

    // http://localhost:8080/checklist/servicios/insertaEvidenciaHallazgo.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/insertaComentarioAsgCalidad", method = RequestMethod.POST)
    public @ResponseBody
    boolean insertaComentarioAsgCalidad(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String contenido = "";
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                // logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                // logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                System.out.println("provider " + provider);
                contenido = descifra.decryptParams(provider);
            }
            // Json
            try {

                if (contenido != null) {
                    System.out.println("json: " + contenido);

                    JSONObject rec = new JSONObject(contenido);

                    JSONObject data = rec.getJSONObject("datosComentario");

                    boolean actualizaResponse = false;

                    String ceco;
                    String fecha;
                    String comentario;

                    ceco = data.getString("ceco");
                    fecha = data.getString("fecha");
                    comentario = data.getString("comentario");

                    actualizaResponse = comentariosRepAsgBI.setComentarios(ceco, fecha, comentario);

                    return actualizaResponse;
                }
            } catch (Exception e) {

                e.printStackTrace();
                logger.info("AP AL OBTENER EL JSON DE EVIDENCIA A INSERTAR ");
                return false;
            }
        } catch (Exception e) {

            e.printStackTrace();
            logger.info("AP AL OBTENER PARAMETROS DE REQUEST DE EVIDENCIA A INSERTAR ");
            return false;
        }

        return false;
    }

    // http://localhost:8080/checklist/servicios/insertaEvidenciaHallazgo.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/enviaRepAsgCalidad", method = RequestMethod.POST)
    public @ResponseBody
    boolean enviaRepAsgCalidad(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String contenido = "";
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                // logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));

                contenido = descifra.decryptParams(provider);

                //logger.info("JSON ANDROID: " + contenido);
            }
            // Json
            try {

                String idUsuario = urides.split("&")[0].split("=")[1];

                if (contenido != null) {

                    JSONObject rec = new JSONObject(contenido);

                    JSONObject data = rec.getJSONObject("reporteMovil");

                    boolean actualizaResponse = false;
                    boolean insertaInformeGeneralT = false;

                    String ceco;
                    String fechaEnvio;
                    String pdfBase64;
                    int numeroBitacora = 0;
                    String nombreArchivo = "";

                    ceco = data.getString("ceco");
                    fechaEnvio = data.getString("fecha"); //ddMMyyyy //  dd/MM/yyyy HH:mm:ss
                    pdfBase64 = data.getString("pdfBase64");

                    List<ParametroDTO> validaProtocolo = parametroBI.obtieneParametros("GuardaPuntosControl");

                    boolean banderaNumProtocolos = false;

                    if (validaProtocolo != null) {
                        for (ParametroDTO param : validaProtocolo) {
                            try {
                                if (param.getValor().equals("1")) {
                                    try {
                                        JSONArray protocolos = data.getJSONArray("protocolos");
                                        System.out.println("objeto protocolos de " + idUsuario + " en " + ceco + " " + data.getString("protocolos"));

                                        for (int i = 0; i < protocolos.length(); i++) {
                                            JSONObject protocolo = protocolos.getJSONObject(i);
                                            int idProtocolo = protocolo.getInt("idProtocolo");
                                            String nombreProtocolo = protocolo.getString("nombre");
                                            int numeroImperdonables = protocolo.getInt("numeroImperdonables");
                                            Double calificacion = protocolo.getDouble("calificacion");
                                            int numeroRespuestas = protocolo.getInt("numeroRespuestas");
                                            System.out.println("Paso a insertar objeto objeto " + i + 1);
                                            DatosInformeDTO informe = new DatosInformeDTO();
                                            informe.setIdCeco(Integer.parseInt(ceco));
                                            informe.setIdUsuario(Integer.parseInt(idUsuario));
                                            informe.setIdProtocolo(idProtocolo);
                                            informe.setImperdonables(numeroImperdonables);
                                            informe.setTotalRespuestas(numeroRespuestas);
                                            informe.setCalificacion(calificacion);
                                            informe.setFecha(fechaEnvio);
                                            int respuestaPuntos = datosInformeBI.insertaDatosInforme(informe);
                                        }
                                        if (data != null) {
                                            if (data.length() == 0) {
                                                banderaNumProtocolos = true;
                                            }
                                        } else {
                                            banderaNumProtocolos = true;
                                        }
                                    } catch (Exception e) {
                                        System.out.println("No se encontro parametro GuardaPuntosControl");
                                    }
                                }
                            } catch (Exception e) {
                                System.out.println("Fallo al guardar el ");
                                e.printStackTrace();
                            }
                        }
                    }
                    System.out.println("ceco: " + ceco + " fecha: " + fechaEnvio);

                    nombreArchivo = idUsuario + "_" + ceco;

                    //Mientras no guarde el reporte no debe tomar el parametro de bitacoras
                    List<ParametroDTO> validaGuardaInformeG = parametroBI.obtieneParametros("guardarInformeGene");
                    Boolean guardaInforme = false;
                    if (validaGuardaInformeG != null) {
                        for (ParametroDTO param : validaGuardaInformeG) {
                            try {
                                if (param.getValor().equals("1")) {
                                    guardaInforme = true;
                                }
                            } catch (Exception e) {
                                guardaInforme = false;
                            }
                        }
                    }

                    try {
                        if (guardaInforme) {
                            numeroBitacora = data.getInt("numeroBitacoras");

                            String dia = fechaEnvio.split(" ")[0].split("/")[0];
                            String mes = fechaEnvio.split(" ")[0].split("/")[1];
                            String anio = fechaEnvio.split(" ")[0].split("/")[2];
                            String hora = fechaEnvio.split(" ")[1].split(":")[0];
                            String minutos = fechaEnvio.split(" ")[1].split(":")[1];
                            String segundos = fechaEnvio.split(" ")[1].split(":")[2];
                            nombreArchivo = ceco + "_" + idUsuario + "_" + dia + mes + anio + "_" + hora + minutos + segundos;
                        } else {
                            nombreArchivo = ceco + "_" + fechaEnvio.replaceAll("/", "") + "_" + idUsuario;
                        }
                    } catch (Exception e) {
                        logger.info("Aseguramiento AP AL OBTENER PARAMETROS ENTRADA DEL WS: " + ceco + " - " + fechaEnvio);
                    }

                    List<ParametroDTO> valida = parametroBI.obtieneParametros("correoRepAsgCalidad");
                    ArrayList<String> correos = new ArrayList<>();
                    if (valida != null) {
                        for (ParametroDTO param : valida) {
                            String[] correosTmp = param.getValor().split(",");

                            for (String item : correosTmp) {
                                correos.add(item);

                            }
                        }
                    }

                    List<ParametroDTO> validaEnvio = parametroBI.obtieneParametros("enviarSucRepAsgC");
                    Boolean enviar = false;
                    if (validaEnvio != null) {
                        for (ParametroDTO param : validaEnvio) {
                            try {

                                enviar = Boolean.parseBoolean(param.getValor());

                            } catch (Exception e) {
                                enviar = false;
                            }
                        }
                    }

                    List<ParametroDTO> validaGuardaRep = parametroBI.obtieneParametros("guardaRepAsgCalidad");
                    Boolean guarda = false;
                    if (validaGuardaRep != null) {
                        for (ParametroDTO param : validaGuardaRep) {
                            try {
                                if (param.getValor().equals("1")) {
                                    guarda = true;
                                }
                            } catch (Exception e) {
                                guarda = false;
                            }
                        }
                    }

                    DocsHandlerBI handler = new DocsHandlerBI();

                    handler.saveDoc("repAsgCalidad", pdfBase64, nombreArchivo, "pdf");
                    String fullPath = handler.getFullPath("repAsgCalidad", nombreArchivo, "pdf");

                    CorreoBI correoBI = new CorreoBI();
                    actualizaResponse = correoBI.sendMailToAsgCalidad(ceco, idUsuario, new File(fullPath), enviar, correos);

                    if (guarda) {
                        Date fechaActual = new Date();

                        DateFormat formatoAnio = new SimpleDateFormat("yyyy");
                        DateFormat formatoMes = new SimpleDateFormat("MM");
                        String anioCarpeta = formatoAnio.format(fechaActual);
                        String mesCarpeta = formatoMes.format(fechaActual);

                        File r1 = null;

                        String rootPath = File.listRoots()[0].getAbsolutePath();
                        String rutaArchivo = "/franquicia/informesRevision/" + anioCarpeta + "/" + mesCarpeta + "/";
                        String rutaPATHArchivo = rootPath + rutaArchivo;
                        r1 = new File(rutaPATHArchivo);
                        if (r1.mkdirs()) {
                            logger.info("SE HA CREADA LA CARPETA");
                        } else {
                            logger.info("EL DIRECTORIO YA EXISTE");
                        }

                        Base64.Decoder decoder = Base64.getDecoder();
                        byte[] archivoDecodificada = decoder.decode(pdfBase64);

                        String rutaTotalF = rutaPATHArchivo + nombreArchivo + ".pdf";

                        FileOutputStream fileOutputF = null;

                        try {
                            fileOutputF = new FileOutputStream(rutaTotalF);

                            BufferedOutputStream bufferOutputF = new BufferedOutputStream(fileOutputF);
                            bufferOutputF.write(archivoDecodificada);
                            bufferOutputF.close();

                            if (guardaInforme) {

                                Integer usuario = Integer.parseInt(idUsuario);
                                if (banderaNumProtocolos == false) {
                                    insertaInformeGeneralT = informeGeneralBI.insertaInformeGeneral(ceco, fechaEnvio, usuario, rutaTotalF, 1, "PENDIENTE", numeroBitacora);
                                } else {
                                    insertaInformeGeneralT = true;
                                }
                                actualizaResponse = insertaInformeGeneralT;
                            }
                        } catch (Exception ex) {

                            logger.info("Ocurrio algo al guardar el informe de revision: ");
                        } finally {
                            try {
                                fileOutputF.close();
                            } catch (Exception e) {
                                logger.info("Error al guardar el informe de revision");
                            }
                        }

                    }

                    //actualizaResponse = comentariosRepAsgBI.setComentarios(ceco, fecha, comentario);
                    return actualizaResponse;
                }
            } catch (Exception e) {
                logger.info("AP AL OBTENER EL JSON DE EVIDENCIA A INSERTAR ");

                return false;
            }
        } catch (Exception e) {
            logger.info("AP AL OBTENER PARAMETROS DE REQUEST DE EVIDENCIA A INSERTAR ");
            return false;
        }

        return false;
    }

    // http://localhost:8080/checklist/servicios/getProtocolosRevisionByGrupo.json?idUsuario=<?>&idGrupo=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getProtocolosRevisionByGrupo", method = RequestMethod.GET)
    public @ResponseBody
    String getProtocolosRevisionByGrupo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String jsonResponse = "";

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        int idUsuario = 0;
        int idGrupo = 0;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idGrupo = Integer.parseInt(urides.split("&")[1].split("=")[1]);

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                // idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idGrupo = Integer.parseInt(urides.split("&")[1].split("=")[1]);

            }

            List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkVersParam("0", idGrupo + "");
            Gson gson = null;
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.serializeNulls();
            gson = gsonBuilder.create();
            jsonResponse = gson.toJson(lista);

        } catch (Exception e) {

            jsonResponse = "{ }";

        }

        return jsonResponse;

    }

    // http://localhost:8080/checklist/servicios/getIDProtocolosRevisionByVersion.json?idUsuario=<?>&idVersion=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getIDProtocolosRevisionByVersion", method = RequestMethod.GET)
    public @ResponseBody
    String getIDProtocolosRevisionByVersion(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String jsonResponse = "";

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        int idUsuario = 0;
        int idVersion = 0;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idVersion = Integer.parseInt(urides.split("&")[1].split("=")[1]);

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                // idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idVersion = Integer.parseInt(urides.split("&")[1].split("=")[1]);

            }

            List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkVersParam(idVersion + "", "0");
            Gson gson = null;
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.serializeNulls();
            gson = gsonBuilder.create();
            jsonResponse = gson.toJson(lista);

        } catch (Exception e) {

            jsonResponse = "{ }";

        }

        return jsonResponse;

    }

    //SERVICIOS PARA TRANSFORMACIÓN
    // http://localhost:8080/checklist/servicios/getInformacionTransformacion.json?usuario=
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getInformacionTransformacion", method = RequestMethod.GET)
    public @ResponseBody
    String getInformacionTransformacion(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String jsonResponse = "";

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        int idUsuario = 0;

        try {

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                // idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            }

            jsonResponse = asignaTransfBI.obtieneDatosCursores(idUsuario + "");
            logger.info(jsonResponse);

            return jsonResponse;

        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return "{}";
        }
    }

    // http://localhost:8080/checklist/servicios/cargaHallazgosTransformacion.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/cargaHallazgosTransformacion", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    boolean cargaHallazgosTransformacion(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        logger.info(provider);

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String contenido = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                // logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                // logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }

            // Json
            try {

                if (contenido != null) {

                    JSONObject rec = new JSONObject(contenido);
                    JSONObject results = rec.getJSONObject("data");

                    boolean insertaSoft = false;
                    int fase = 0;

                    int usuario = results.getInt("idUsuario");
                    int ceco = results.getInt("idCeco");
                    int version = results.getInt("idVersion");
                    int idProyecto = results.getInt("idProyecto");
                    int idFase = results.getInt("idFase");

                    // SI TRAEL PARÁMETRO TIPO CONSUMO EN 0 HACE EL NORMAL, SI TRAE EN 1 HACE 1 POR
                    // I
                    int tipoConsumo = 0;

                    try {
                        tipoConsumo = results.getInt("tipoConsumo");

                    } catch (Exception e) {
                        tipoConsumo = 0;
                        logger.info("Sin parámetro tipo consumo");
                    }

                    // SE EJECUTA EL PROCESO COMO SE MAPEO INICIALMENTE
                    if (tipoConsumo == 0) {

                        HallazgosTransfDTO objetoHallazgos = new HallazgosTransfDTO();
                        objetoHallazgos.setCeco(ceco + "");
                        objetoHallazgos.setUsuario(usuario);
                        objetoHallazgos.setIdVersion(version);
                        objetoHallazgos.setIdProyecto(idProyecto);

                        try {
                            if (results.toString().contains("idFase") && results.toString().contains("insertaSoft")) {
                                fase = results.getInt("idFase");
                                insertaSoft = results.getBoolean("insertaSoft");
                            } else {
                                fase = 0;
                                insertaSoft = false;
                            }

                        } catch (Exception e) {
                            logger.info("Tipo de Carga de Transformacion");
                        }

                        int res = hallazgosTransfBI.inserta(objetoHallazgos);

                        if (res == 1) {

                            // LLAMAR AL BI PARA CARGAR LOS HALLAZGOS QUE SE GENERAN DEL RECORRIDO
                            try {
                                SoftNvoDTO soft = new SoftNvoDTO();
                                soft.setCeco(ceco + "");
                                soft.setIdFase(fase);
                                soft.setIdProyecto(idProyecto);
                                soft.setBitacora(0);
                                soft.setIdUsuario(usuario);

                                softNvoBI.insertaSoftN(soft);

                            } catch (Exception e) {
                                logger.info("AP AL INSERTAR SOFT" + e);
                                logger.info(e.getStackTrace());
                                return false;
                            }

                            // LLAMAR AL BI PARA CARGAR LOS HALLAZGOS QUE SE GENERAN DEL RECORRIDO
                            logger.info("HALLAZGOS CARGADOS CON ÉXITO ");

                            // SE ACTUALIZAN BANDERA DE FASE Y ESTATUS
                            AsignaTransfDTO asig = new AsignaTransfDTO();

                            asig.setCeco(ceco);
                            asig.setProyecto(idProyecto);
                            // Fase (Se actualiza solo en ulimo hallazgo con un 1)
                            asig.setIdOrden(null);
                            // Estatus
                            asig.setIdestatus(0);

                            boolean respuesta = asignaTransfBI.actualiza(asig);

                            if (respuesta) {

                                logger.info("HALLAZGOS ESTATUS ACTUALIZADOS CON EXITO");

                                // LLAMAR a BI de insertaSoft()
                                return true;

                            } else {

                                logger.info("AP AL ACTUALIZAR HALLAZGOS ESTATUS");
                                return false;

                            }

                        } else {

                            logger.info("AP AL CARGAR HALLAZGOS");
                            return false;
                        }

                    } else if (tipoConsumo == 1) {

                        // SE EJECUTA EL GUARDADO DE BITÁCORA 1 X 1
                        String cadenaBitacoras = results.getString("bitacoras");
                        // "" error
                        int idBitacoraParam = 0;
                        String[] listaBitacoras = cadenaBitacoras.split(",");
                        ArrayList<String> listaBitacorasFallo = new ArrayList<String>();

                        int cantidadBitacoras = listaBitacoras.length;
                        int conteoBitacorasCorrectas = 0;

                        for (String idBitacora : listaBitacoras) {
                            boolean res = hallazgosTransfBI.cargaHallazgosxBitaNvo(Integer.parseInt(idBitacora), ceco,
                                    idFase, idProyecto);

                            if (res) {
                                conteoBitacorasCorrectas++;
                                idBitacoraParam = Integer.parseInt(idBitacora);
                            } else {
                                listaBitacorasFallo.add(idBitacora);
                            }

                        }

                        // SI TODAS LAS BITÁCORAS FUERON TRUE SE EJECUTA LA INSERCIÓN DEL SOFT
                        if (conteoBitacorasCorrectas == cantidadBitacoras) {

                            SoftNvoDTO soft = new SoftNvoDTO();
                            soft.setCeco(ceco + "");
                            soft.setIdFase(idFase);
                            soft.setIdProyecto(idProyecto);
                            soft.setIdUsuario(usuario);
                            soft.setBitacora(idBitacoraParam);

                            int respuesta = softNvoBI.insertaSoftNuevoFlujo(soft);

                            if (respuesta > 0) {
                                logger.info("correctoss");
                                return true;
                            } else {
                                return false;
                            }

                        } else {
                            return false;
                        }

                    } else if (tipoConsumo == 2) {

                        // ESTE TIPO DEBE TRAER EL ID_PROGRAMACION
                        int idProgramacion = 0;

                        try {
                            idProgramacion = results.getInt("idProgramacion");

                        } catch (Exception e) {
                            idProgramacion = 0;
                            logger.info("Sin parámetro tipo consumo");
                        }

                        // SI TODAS LAS BITÁCORAS FUERON TRUE SE EJECUTA LA INSERCIÓN DEL SOFT
                        int bitacoraParam = 0;
                        boolean responseParam = false;

                        // SE EJECUTA EL GUARDADO DE BITÁCORA 1 X 1
                        String cadenaBitacoras = results.getString("bitacoras");

                        //
                        String[] listaBitacoras = cadenaBitacoras.split(",");
                        ArrayList<String> listaBitacorasFallo = new ArrayList<String>();

                        int cantidadBitacoras = listaBitacoras.length;
                        int conteoBitacorasCorrectas = 0;

                        bitacoraParam = Integer.parseInt(listaBitacoras[0]);

                        SoftNvoDTO soft = new SoftNvoDTO();
                        soft.setCeco(ceco + "");
                        soft.setIdProyecto(idProyecto);
                        soft.setIdProgramacion(idProgramacion);
                        soft.setIdUsuario(usuario);
                        soft.setBitacora(bitacoraParam);

                        int respuesta = softNvoBI.insertaSoftNuevoFlujoMtto(soft);

                        if (respuesta > 0) {

                            logger.info("Soft Correcto :" + bitacoraParam);

                        } else {

                            return false;
                        }

                        for (String idBitacora : listaBitacoras) {

                            boolean res = hallazgosTransfBI.cargaHallazgosxBitaNvo(Integer.parseInt(idBitacora), ceco,
                                    idFase, idProyecto);

                            if (res) {

                                conteoBitacorasCorrectas++;

                            } else {

                                listaBitacorasFallo.add(idBitacora);
                            }

                        }

                        if (cantidadBitacoras == conteoBitacorasCorrectas) {

                            logger.info("HALLAZGOS CARGADOS CORRECTAMENTE");
                            return true;

                        } else {

                            logger.info("HALLAZGOS NO CARGADOS CORRECTAMENTE");
                            return false;

                        }

                    } else {

                        return false;
                    }

                }
            } catch (Exception e) {
                logger.info("AP AL OBTENER EL JSON DE HALLAZGOS A ACTUALIZAR " + e);
                return false;
            }
        } catch (Exception e) {
            logger.info("AP AL OBTENER PARAMETROS DE REQUEST" + e);
            return false;
        }

        return false;
    }

    // http://localhost:8080/checklist/servicios/updateHallazgoTransformacion.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/updateHallazgoTransformacion", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    boolean updateHallazgoTransformacion(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        logger.info(provider);

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String contenido = "";
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                // logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                // logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }
            // Json
            try {

                if (contenido != null) {

                    JSONObject rec = new JSONObject(contenido);

                    JSONArray results = rec.getJSONArray("hallazgos");

                    boolean banderaCompleto = true;
                    boolean banderaResponse = true;

                    for (int i = 0; i < results.length(); i++) {

                        JSONObject data = results.getJSONObject(i);
                        HallazgosTransfDTO objHallazgo = new HallazgosTransfDTO();

                        int idHallazgo = 0;
                        int idFolio = 0;
                        int idResp = 0;
                        int status = 0;
                        String respuesta = "";
                        String ruta = "";
                        String obs = "";
                        String obsNueva = "";
                        String fechaAut = "";
                        String fechafin = "";
                        String usuModif = "";
                        String motiRecha = "";
                        int tipoEvidencia = 0;

                        int idCeco = 0;
                        int idProyecto = 0;
                        int idFase = 0;

                        idHallazgo = data.getInt("idHallazgo");
                        idFolio = data.getInt("idFolio");
                        idResp = data.getInt("idResp");
                        status = data.getInt("status");
                        respuesta = data.getString("respuesta");
                        ruta = data.getString("ruta");
                        obs = data.getString("obs");
                        obsNueva = data.getString("obsNueva");
                        fechaAut = data.getString("fechaAut");
                        fechafin = data.getString("fechaFin");
                        usuModif = data.getString("usuModif");
                        motiRecha = data.getString("motivoRechazo");
                        tipoEvidencia = data.getInt("tipoEvidencia");

                        idCeco = data.getInt("idCeco");
                        idProyecto = data.getInt("idProyecto");
                        idFase = data.getInt("idFase");

                        objHallazgo.setIdHallazgo(idHallazgo);
                        objHallazgo.setIdFolio(idFolio);
                        objHallazgo.setIdResp(idResp);
                        objHallazgo.setStatus(status);
                        objHallazgo.setRespuesta(respuesta);
                        objHallazgo.setRuta(ruta);
                        objHallazgo.setObs(obs);
                        objHallazgo.setObsNueva(obsNueva);
                        objHallazgo.setFechaAutorizo(fechaAut);
                        objHallazgo.setFechaFin(fechafin);
                        objHallazgo.setUsuModif(usuModif);
                        objHallazgo.setMotivrechazo(motiRecha);
                        objHallazgo.setTipoEvi(tipoEvidencia);

                        Map<String, Object> resp = hallazgosTransfBI.actualizaMov(objHallazgo);

                        int banderaRespuesta = (int) resp.get("response");
                        String banderaCierreHallazgos = (String) resp.get("validaCompleto");

                        if (banderaRespuesta == 0) {
                            logger.info("No fue posible ACTUALIZAR el HALLAZGO " + idHallazgo);
                            banderaResponse = false;
                            banderaCompleto = false;
                        } else {
                            logger.info("HALLAZGO: " + idHallazgo + " ACTUALIZADO ");
                            banderaResponse = true;
                        }

                        if (banderaRespuesta == 1 && banderaCierreHallazgos.compareToIgnoreCase("true") == 0) {
                            banderaCompleto = true;
                            //Se hace el envío del correo
                        }

                    }

                    if (banderaResponse) {
                        logger.info("ACTUALIZACION COMPLETA");
                        return banderaResponse;

                    } else {
                        logger.info("ACTUALIZACION IN-COMPLETA");
                        return banderaResponse;
                    }
                }
            } catch (Exception e) {
                logger.info("AP AL OBTENER EL JSON DE HALLAZGOS A ACTUALIZAR " + e);
                return false;
            }
        } catch (Exception e) {
            logger.info("AP AL OBTENER PARAMETROS DE REQUEST" + e);
            return false;
        }

        return false;
    }

    // http://localhost:8080/checklist/servicios/getTablaAsignaciones.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getTablaAsignaciones", method = RequestMethod.GET)
    public @ResponseBody
    List<AsignaTransfDTO> getTablaAsignaciones(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        List<AsignaTransfDTO> lista = null;

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        int idUsuario = 0;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                // idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);

            }

            String idProyecto = null;

            lista = asignaTransfBI.obtieneDatos(idProyecto);

            if (lista == null || lista.size() == 0) {

                logger.info("AP la lista de las asignaciones es vacia");
                lista = null;

            }

        } catch (Exception e) {
            logger.info("AP al Obtene la información de getTablaAsignaciones()");
            logger.info("AP" + e);

            return null;
        }

        return lista;
    }

    //http://localhost:8080/checklist/servicios/getInfoTransformacionByCeco.json?idUsuario=<?>&idCeco=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getInfoTransformacionByCeco", method = RequestMethod.GET)
    public @ResponseBody
    String getInfoTransformacionByCeco(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String respuesta = "";

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        int idUsuario = 0;
        String idCeco = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idCeco = urides.split("&")[1].split("=")[1];

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                // idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idCeco = urides.split("&")[1].split("=")[1];

            }

            respuesta = asignaTransfBI.obtieneDatosCursoresCeco(idCeco);

        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            respuesta = "{}";
        }

        return respuesta;
    }

    // http://localhost:8080/checklist/servicios/getHallazgosByCecoFaseProyecto.json?idUsuario=<?>&idCeco=<?>&idProyecto=<?>&idFase=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getHallazgosByCecoFaseProyecto", method = RequestMethod.GET)
    public @ResponseBody
    String getHallazgosByCecoFaseProyecto(HttpServletRequest request,
            HttpServletResponse response) throws UnsupportedEncodingException {

        List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();
        List<HallazgosTransfDTO> listaEvidencias = new ArrayList<HallazgosTransfDTO>();

        String listaHallazgosResult = "[]";
        String listaEvidenciasResult = "[]";

        String res = "";

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        int idUsuario = 0;
        String idCeco = null;
        String idProyecto = null;
        String idFase = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idCeco = urides.split("&")[1].split("=")[1];
                idProyecto = urides.split("&")[2].split("=")[1];
                idFase = urides.split("&")[3].split("=")[1];

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                // idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idCeco = urides.split("&")[1].split("=")[1];
                idProyecto = urides.split("&")[2].split("=")[1];
                idFase = urides.split("&")[3].split("=")[1];
            }

            try {

                listaHallazgos = hallazgosTransfBI.obtieneDatos(idCeco, idProyecto, idFase);
                listaEvidencias = hallazgosTransfBI.obtieneDatosBita(Integer.parseInt(idProyecto), Integer.parseInt(idFase), Integer.parseInt(idCeco));

                if (listaHallazgos != null && listaHallazgos.size() > 0) {

                    listaHallazgosResult = new Gson().toJson(listaHallazgos);

                }

                if (listaEvidencias != null && listaEvidencias.size() > 0) {

                    listaEvidenciasResult = new Gson().toJson(listaEvidencias);

                }

                res = "{";
                res += " \"hallazgos\"  : " + listaHallazgosResult + ",";
                res += " \"evidencias\" : " + listaEvidenciasResult;
                res += "}";

            } catch (Exception e) {
                logger.info("AP Al obtener la lista de hallazgos/evidencias");
                return "{}";
            }

        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            res = "{}";
        }

        return res;
    }

    // http://localhost:8080/checklist/servicios/getHallazgosTransformacionActa.json?idUsuario=<?>&idCeco=<?>&idProyecto=<?>&idFase=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getHallazgosTransformacionActa", method = RequestMethod.GET)
    public @ResponseBody
    String getHallazgosTransformacionActa(HttpServletRequest request,
            HttpServletResponse response) throws UnsupportedEncodingException {

        List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();
        List<HallazgosTransfDTO> listaEvidencias = new ArrayList<HallazgosTransfDTO>();

        String listaHallazgosResult = "";
        String listaEvidenciasResult = "";

        String res = "";

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        int idUsuario = 0;
        String idCeco = null;
        String idProyecto = null;
        String idFase = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idCeco = urides.split("&")[1].split("=")[1];
                idProyecto = urides.split("&")[2].split("=")[1];
                idFase = urides.split("&")[3].split("=")[1];

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                // idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idCeco = urides.split("&")[1].split("=")[1];
                idProyecto = urides.split("&")[2].split("=")[1];
                idFase = urides.split("&")[3].split("=")[1];
            }

            try {

                listaHallazgos = hallazgosTransfBI.obtieneDatos(idCeco, idProyecto, idFase);

                hallazgosTransfBI.generaArchivoTrans(idCeco, idProyecto, idFase);

                if (listaHallazgos != null && listaHallazgos.size() > 0) {

                    listaHallazgosResult = new Gson().toJson(listaHallazgos);

                }

                if (listaEvidencias != null && listaEvidencias.size() > 0) {

                    listaEvidenciasResult = new Gson().toJson(listaEvidencias);

                }

                res = "{";
                res += " \"hallazgos\"  : " + listaHallazgosResult;
                //res += " \"evidencias\" : " + listaEvidenciasResult;
                res += "}";

            } catch (Exception e) {
                logger.info("AP Al obtener la lista de hallazgos/evidencias");
                return "{}";
            }

        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            res = "{}";
        }

        return res;
    }

    //FIN SERVICIOS PARA TRANSFORMACIÓN
    //--------------------- Servicio para insertar el informe de retralimentacion de aseguramiento ---------------
    // http://localhost:8080/checklist/servicios/postInformeRetro.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/postInformeRetro", method = RequestMethod.POST)
    public @ResponseBody
    String postInformeRetro(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        //System.out.println("-------------------Entro a guardar evidencia checklist 1--------------------------");
        String res = null;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());
                contenido = contenido.replaceAll("\\n", "");
                contenido = contenido.replaceAll("\\r", "");

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }

            String ceco = "";
            String usuario = "";
            String fecha = "";
            String ruta = "";
            String lider = "";
            String firmaLider = "";
            String numLider = "";
            String firmaFrq = "";

            String archivoRetro = "";
            String archFirmaLider = "";
            String archFirmaFranquicia = "";
            String idProtocolo = "";
            System.out.println("Json retro: " + contenido);
            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                ceco = rec.getString("ceco");
                usuario = rec.getString("idUsuario");
                fecha = rec.getString("fecha");
                lider = rec.getString("nomLider");
                numLider = rec.getString("numLider");
                idProtocolo = rec.getString("idProtocolo");

                archivoRetro = rec.getString("archivoRetro");
                archFirmaLider = rec.getString("archFirmaLider");
                archFirmaFranquicia = rec.getString("archFirmaFranquicia");

            }

            Date fechaActual = new Date();

            DateFormat formatoAnio = new SimpleDateFormat("yyyy");
            DateFormat formatoMes = new SimpleDateFormat("MM");
            DateFormat formatoFCompleta = new SimpleDateFormat("yyyyMMdd");
            String anio = formatoAnio.format(fechaActual);
            String mes = formatoMes.format(fechaActual);
            String fActual = formatoFCompleta.format(fechaActual);

            File r2 = null;
            File r1 = null;
            String rootPath = File.listRoots()[0].getAbsolutePath();
            String rutaArchivo = "/franquicia/retroProtocolos/" + anio + "/" + mes + "/";
            String rutaFirmas = "/franquicia/firmasRetroProtocolos/" + anio + "/" + mes + "/";

            String rutaPATHArchivo = rootPath + rutaArchivo;

            String rutaPATHFirmas = rootPath + rutaFirmas;
            r2 = new File(rutaPATHArchivo);
            r1 = new File(rutaPATHFirmas);

            if (r1.mkdirs()) {
                logger.info("SE HA CREADA LA CARPETA");
            } else {
                logger.info("EL DIRECTORIO YA EXISTE");
            }

            if (r2.mkdirs()) {
                logger.info("SE HA CREADA LA CARPETA");
            } else {
                logger.info("EL DIRECTORIO YA EXISTE");
            }

            String nomArchivo = idProtocolo + "_" + fActual + "_" + ceco + "_" + usuario + ".pdf";
            String nomFirmaLider = fActual + "_" + numLider + ".png";
            String nomFirmaAsegura = fActual + "_" + usuario + ".png";;

            Base64.Decoder decoder = Base64.getDecoder();
            byte[] archivoDecodificada = decoder.decode(archivoRetro);

            byte[] archivoDecodiLider = decoder.decode(archFirmaLider);
            byte[] archivoDecodiAsegura = decoder.decode(archFirmaFranquicia);

            String rutaTotalF = rutaPATHArchivo + nomArchivo;

            String rutaTotalFAsegu = rutaPATHFirmas + nomFirmaAsegura;
            String rutaTotalFLider = rutaPATHFirmas + nomFirmaLider;

            FileOutputStream fileOutputF = null;
            FileOutputStream fileOutputFLider = null;
            FileOutputStream fileOutputFAsegu = null;

            int alta = 0;

            try {
                fileOutputF = new FileOutputStream(rutaTotalF);

                BufferedOutputStream bufferOutputF = new BufferedOutputStream(fileOutputF);
                bufferOutputF.write(archivoDecodificada);
                bufferOutputF.close();

                alta = 1;

            } catch (Exception ex) {
                alta = 0;
                logger.info("Ocurrio algo: " + ex);
            } finally {
                try {
                    fileOutputF.close();
                } catch (Exception e) {
                    logger.info("AP" + e);
                }
            }

            try {
                fileOutputFLider = new FileOutputStream(rutaTotalFLider);

                BufferedOutputStream bufferOutputFLider = new BufferedOutputStream(fileOutputFLider);
                bufferOutputFLider.write(archivoDecodiLider);
                bufferOutputFLider.close();

            } catch (Exception ex) {
                alta = 0;
                logger.info("Ocurrio algo: " + ex);
            } finally {
                try {
                    fileOutputFLider.close();
                } catch (Exception e) {
                    logger.info("AP" + e);
                }
            }

            try {
                fileOutputFAsegu = new FileOutputStream(rutaTotalFAsegu);

                BufferedOutputStream bufferOutputFAsegu = new BufferedOutputStream(fileOutputFAsegu);
                bufferOutputFAsegu.write(archivoDecodiAsegura);
                bufferOutputFAsegu.close();

            } catch (Exception ex) {
                alta = 0;
                logger.info("Ocurrio algo: " + ex);
            } finally {
                try {
                    fileOutputFAsegu.close();
                } catch (Exception e) {
                    logger.info("AP" + e);
                }
            }

            AdmInformProtocolosDTO admInformProtocolos = new AdmInformProtocolosDTO();
            admInformProtocolos.setCeco(ceco);
            admInformProtocolos.setUsuario(usuario);
            admInformProtocolos.setFecha(fecha);
            admInformProtocolos.setRuta(rutaTotalF);
            admInformProtocolos.setLider(lider);
            admInformProtocolos.setFirmaLider(rutaTotalFLider);
            admInformProtocolos.setNumLider(numLider);
            admInformProtocolos.setFirmaFrq(rutaTotalFAsegu);
            admInformProtocolos.setRetroAlimentacion("" + alta);
            admInformProtocolos.setIdProtocolo(idProtocolo);

            boolean respuesta = admInformProtocolosBI.insertaInformProtocolos(admInformProtocolos);

            if (alta == 1) {
                if (respuesta == false) {
                    alta = 0;
                }
            }

            JsonObject envoltorioJsonObj = new JsonObject();
            envoltorioJsonObj.addProperty("ALTA", alta);
            logger.info("Inseta Retro: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            logger.info(e);
            return "{}";
        }

        if (res != null) {
            //System.out.println("-------------------Entro a guardar evidencia checklist 7-------------------------- " + res);
            return res.toString();
        } else {
            return "{}";
        }
    }

    // http://localhost:8080/checklist/servicios/sendCorreoTransformacion.json?idUsuario=<?>&idCeco=<?>&idFase=<?>&idProyecto=<?>&correo=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/sendCorreoTransformacion", method = {RequestMethod.POST})
    public @ResponseBody
    boolean sendCorreoTransformacion(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();
        List<HallazgosTransfDTO> listaEvidencias = new ArrayList<HallazgosTransfDTO>();
        boolean banderaCompleto = false;

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String contenido = "";
        boolean respuesta = false;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                // logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                // logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }

            // Json
            try {

                if (contenido != null) {

                    JSONObject rec = new JSONObject(contenido);
                    JSONObject results = rec.getJSONObject("data");

                    int idCeco = results.getInt("idCeco");
                    int idFase = results.getInt("idFase");
                    int idProyecto = results.getInt("idProyecto");
                    boolean enviar = true;
                    if (results.toString().contains("cancelaEnvio")) {
                        enviar = (results.getBoolean("cancelaEnvio") ? false : true);
                    }

                    //INCREMENTA NÚMERO RECORRIDOMAP
                    //SE ACTUALIZAN BANDERA DE FASE Y ESTATUS
                    AsignaTransfDTO asig = new AsignaTransfDTO();
                    asig.setCeco(idCeco);
                    asig.setProyecto(idProyecto);
                    //Fase (Se actualiza solo en ulimo hallazgo con un 1)
                    asig.setIdOrden("1");
                    //Estatus
                    asig.setIdestatus(1);

                    //Esta es la bandera que lleva el peso para la respuesta (Si es true,no importa lo adentro, retorno un true)
                    boolean respuestaAct = asignaTransfBI.actualiza(asig);

                    if (respuestaAct) {
                        logger.info("HALLAZGOS ESTATUS ACTUALIZADOS CON EXITO  _");
                        logger.info("__");
                        banderaCompleto = true;

                        if (enviar) {
                            //LLAMAR AL BI PARA CARGAR LOS HALLAZGOS QUE SE GENERAN DEL RECORRIDO
                            listaHallazgos = hallazgosTransfBI.obtieneDatosRepo(idCeco + "", idProyecto + "", idFase + "");
                            //listaEvidencias = hallazgosTransfBI.obtieneDatosBita(idProyecto,idFase,idCeco);

                            if (idCeco != 0 && idFase != 0 && idProyecto != 0) {

                                correoBI.sendMailTransformacion(listaHallazgos, "" + idFase, "Acta Transformacion", "" + idCeco, "" + 0, "", "", "Acta transformacion");

                                banderaCompleto = true;

                            } else {
                                logger.info("AP ALAGUN VALOR == 0");

                                banderaCompleto = true;

                            }
                            //LLAMAR AL BI PARA CARGAR LOS HALLAZGOS QUE SE GENERAN DEL RECORRIDO
                        } else {

                            try {

                                boolean res = correoBI.sendMailExpansionCoreNotifica(idCeco, idFase, idProyecto);

                                if (res) {
                                    banderaCompleto = true;
                                } else {
                                    banderaCompleto = false;
                                }

                            } catch (Exception e) {
                                logger.info("AP AL OBTENER EL JSON CORREO HALLAZGOS TRANSFORMACIÓN " + e);
                                return false;
                            }

                        }

                    } else {
                        logger.info("AP AL ENVIAR CORREO HALLAZGOS TRANSFORMACIÓN ESTATUS");
                        banderaCompleto = false;

                    }
                    //

                }
            } catch (Exception e) {
                logger.info("AP AL OBTENER EL JSON CORREO HALLAZGOS TRANSFORMACIÓN " + e);
                return false;
            }
        } catch (Exception e) {
            logger.info("AP AL OBTENER PARAMETROS DE REQUEST CORREO HALLAZGOS TRANSFORMACIÓN" + e);
            return false;
        }

        return banderaCompleto;
    }

    // http://localhost:8080/checklist/servicios/sendCorreoTransformacionDirecto.json?idUsuario=<?>&idCeco=<?>&idFase=<?>&idProyecto=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/sendCorreoTransformacionDirecto", method = {RequestMethod.GET})
    public @ResponseBody
    boolean sendCorreoTransformacionDirecto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();
        List<HallazgosTransfDTO> listaEvidencias = new ArrayList<HallazgosTransfDTO>();
        boolean banderaCompleto = false;

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        int idUsuario = 0;
        String idCeco = null;
        String idProyecto = null;
        String idFase = null;

        boolean respuesta = false;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // idUsuario = Integer.parseInt(urides.split("=")[1]);
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idCeco = urides.split("&")[1].split("=")[1];
                idProyecto = urides.split("&")[2].split("=")[1];
                idFase = urides.split("&")[3].split("=")[1];

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
                idCeco = urides.split("&")[1].split("=")[1];
                idProyecto = urides.split("&")[2].split("=")[1];
                idFase = urides.split("&")[3].split("=")[1];
            }

            // Json
            try {
                // LLAMAR AL BI PARA CARGAR LOS HALLAZGOS QUE SE GENERAN DEL RECORRIDO
                listaHallazgos = hallazgosTransfBI.obtieneDatosRepo(idCeco, idProyecto, idFase);
                //listaEvidencias = hallazgosTransfBI.obtieneDatosBita(Integer.parseInt(idProyecto), Integer.parseInt(idFase), Integer.parseInt(idCeco));

                correoBI.sendMailTransformacion(listaHallazgos, idFase, "Informe de entrega ATM Nueva Experiencia", "" + idCeco, "" + 0, "", "",
                        "Acta transformacion");

                // LLAMAR AL BI PARA CARGAR LOS HALLAZGOS QUE SE GENERAN DEL RECORRIDO
            } catch (Exception e) {
                logger.info("AP AL OBTENER EL JSON CORREO HALLAZGOS TRANSFORMACIÓN " + e);
                return false;
            }
        } catch (Exception e) {
            logger.info("AP AL OBTENER PARAMETROS DE REQUEST CORREO HALLAZGOS TRANSFORMACIÓN" + e);
            return false;
        }

        return banderaCompleto;
    }
    //---------------------  FIN del Servicio para insertar el informe de retralimentacion de aseguramiento ---------------

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getActorHallazgo", method = RequestMethod.GET)
    public @ResponseBody
    String getCatalogoZonas(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        int idUsuario = 0;

        ArrayList<AdminZonaDTO> respuesta;
        ArrayList<AdminZonaDTO> listaLimpia = new ArrayList<>();

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);

            }

            AdminZonaDTO zona = new AdminZonaDTO();
            respuesta = admZonasBI.obtieneZonaById(zona);
            for (AdminZonaDTO zonaAux : respuesta) {
                if (Integer.parseInt(zonaAux.getIdStatus()) == 1) {
                    listaLimpia.add(zonaAux);
                }

            }

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
            respuesta = null;
            listaLimpia = null;
        }

        Gson gson = null;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        gson = gsonBuilder.create();
        String jsonSalida = gson.toJson(listaLimpia);

        return "{\"listaZonas\":" + jsonSalida + "}";

    }

    //SE REUTILIZA EL NOMBRE DEL SERVICIO CADUCADO "SENDCORREOAPERTURA" POR TEMAS DE WAF
    //ESTE SERVICIO GUARDA UN ARCHIVO EN LA RUTA QUE SE LE INDIQUE
    // http://localhost:8080/checklist/servicios/sendCorreoApertura.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/sendCorreoApertura", method = RequestMethod.POST)
    public @ResponseBody
    String sendCorreoApertura(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {
        String json = "";
        String res = null;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        boolean guardada = false;
        boolean respuesta = false;

        // logger.info("Resultado " + serviciosRemedyBI.levantaFolioXML());
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                //logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                //logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                //logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                //logger.info("JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];

            int idSuc = 0;

            String rutaEntrada = (urides.split("&")[1]).split("=")[1];
            String nomArchivo = (urides.split("&")[2]).split("=")[1];
            String archivo = "";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);

                archivo = rec.getString("archivo");
            }

            File r = null;

            String rootPath = File.listRoots()[0].getAbsolutePath();
            Calendar cal = Calendar.getInstance();

            String rutaFotoA = "";
            String rutaFotoD = "";

            // DEFINIR RUTA
            String rutaImagen = "";
            // 1.- ::: VERIFICAR EN DONDE ESTOY PARA GUARDAR EN (
            // /Sociounico/checklist/imagenes/ - /Sociounico/checklist/preview/ ) O EN EL
            // NUEVO
            // COMENTO ESTO POR QUE AUN NO SE APLICA LA VERSION DE checklist

            rutaImagen = rutaEntrada;

            String ruta = rootPath + rutaImagen;

            // System.out.println(cal.getTime().toString());
            // System.out.println(File.listRoots()[0].getAbsolutePath());
            r = new File(ruta);

            //logger.info("RUTA: " + ruta);
            if (r.mkdirs()) {
                logger.info("SE HA CREADA LA CARPETA");
            } else {
                logger.info("EL DIRECTORIO YA EXISTE");
            }

            FileOutputStream fos = null;

            Base64.Decoder decoder = Base64.getDecoder();
            byte[] imgDecodificada = decoder.decode(archivo);

            Date fechaActual = new Date();
            System.out.println(fechaActual);
            System.out.println("---------------------------------------------");

            String rutaTotal = ruta + nomArchivo;

            String rutalimpia = rutaTotal;

            FileOutputStream fileOutput = new FileOutputStream(rutalimpia);

            try {
                respuesta = true;
                BufferedOutputStream bufferOutput = new BufferedOutputStream(fileOutput);

                // se escribe el archivo decodificado
                bufferOutput.write(imgDecodificada);
                bufferOutput.close();

                rutaFotoA = rutalimpia;

            } catch (Exception e) {
                logger.info(e);
                respuesta = false;
            } finally {

                fileOutput.close();

            }

            JsonObject envoltorioJsonObj = new JsonObject();

            envoltorioJsonObj.addProperty("ARCHIVO", respuesta);

            //logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            res = envoltorioJsonObj.toString();

        } catch (Exception e) {
            //logger.info(e);
            return "{\"ARCHIVO\": false}";
        }

        if (res != null) {
            return res.toString();
        } else {
            return "{\"ARCHIVO\": false}";
        }
    }
}
