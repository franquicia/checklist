<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>Galer&iacute;a de evidencias</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervisionPI2/estilos.css">
</head>

<body ng-app="app" ng-controller="validarForm">

  <div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="${pageContext.request.contextPath}/img/supervisionPI2/btn_hamburguer.svg" alt="Men� principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="inicio.htm"><img src="${pageContext.request.contextPath}/img/supervisionPI2/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.html" class="selMenu">
          <div>Salir</div>
          <div><img src="${pageContext.request.contextPath}/img/supervisionPI2/supervisionPI2/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="indexGaleria.htm">Reporte de Supervisores</a> 
		</div>
    	Visor
    </div>

	<!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervisionPI2/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Men&uacute; Principal<br>
          </div>
        </div>
        <%-- <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la p�gina">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> --%>
        <div id="menu">
          
        <c:if test="${menu==1}">
        	<ul class="l-navegacion nivel1">
				<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galer&iacute;a de Evidencias</div></a>
				</li>
			</ul>
        </c:if>
        <c:if test="${menu==0}">
        	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuAseguramiento.jsp" flush="true">
				<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuAseguramiento.jsp" />
			</tiles:insertTemplate>
        	<!--
        	 <ul class="l-navegacion nivel1">
				<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Asegurador</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>B&uacute;squeda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>B&uacute;squeda Asegurador</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galer&iacute;a de Evidencias</div></a>
				</li>
				 <li>
					<a href="#"><div>Administrador</div></a>
					<ul class="nivel2" style="height: 350px; overflow-y: auto;">
			            <li><a href="asignaciones.htm"><div>Asignaci�n de Perfiles</div></a></li>
			            <li><a href="asignaPersonal.htm"><div>Asignaci�n de Personal</div></a></li>
			            <li><a href="asignaCecos.htm"><div>Asignaci�n de Cecos</div></a></li>
			            <li><a href="asignaCecosMasiva.htm"><div>Asignaci�n de Cecos Masiva</div></a></li>
		          	</ul>
				</li> 
			</ul>
			-->
        </c:if>
        
          
        </div>
      </div>
    </div>

  <!-- Contenido -->
  <!-- <div class="contImagen"> -->
  <div class="contSecc">
  
  		<div class="liga">
			<a href="#" onclick="javascript:history.go(-1)" class="liga">
			<img style="height: 15px; width: 15px;" src="${pageContext.request.contextPath}/img/supervision/arrowmleft.png">
			<span>P&Aacute;GINA ANTERIOR</span>
			</a>
		</div>
		<br>
  
		<div class="imgGrande" style="height: 800px; width: 1200px; margin: 0 auto;">
      <img src="${ruta}">  
    </div>
	</div>	<!-- Fin conSecc -->
	
	<!-- Footer -->
	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualizaci&oacute;n Febrero 2019</div>
	</div>
	
</div><!-- Fin page -->

</body>
</html>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervisionPI2/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervisionPI2/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervisionPI2/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervisionPI2/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/supervisionPI2/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervisionPI2/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supervisionPI2/custom-file-input.js"></script><!--Unpload -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervisionPI2/funciones.js"></script>
