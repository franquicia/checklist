package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.HallazgosTransfDTO;

public class HallazgosMatrizRowMapper implements RowMapper<HallazgosTransfDTO> {

	public HallazgosTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		HallazgosTransfDTO HallazgosTransfDTO = new HallazgosTransfDTO();
		
		HallazgosTransfDTO.setIdMatriz(rs.getInt("FIID_MATRIZ"));
	    HallazgosTransfDTO.setTipoProy(""+rs.getInt("FIIDTIPOPROY"));
	    HallazgosTransfDTO.setStatusEnvio(rs.getInt("FIIDSTATUSENVIO"));
	    HallazgosTransfDTO.setStatusHallazgo(rs.getInt("FIIDSTATUSHALLA"));
	    HallazgosTransfDTO.setIdPerfil(rs.getInt("FIIDPERFIL"));
	    HallazgosTransfDTO.setColor(rs.getString("FCCOLOR"));
	    HallazgosTransfDTO.setNombreStatus(rs.getString("FCNOMBRESTATUS"));
	    HallazgosTransfDTO.setPeriodo(rs.getString("FCPERIODO"));


		return HallazgosTransfDTO;
	}

}


