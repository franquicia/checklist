package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ReporteImgDTO;
import com.gruposalinas.checklist.mappers.ReporteAcervoRowMapper;
import com.gruposalinas.checklist.mappers.ReporteGetProtocolosRowMapper;
import com.gruposalinas.checklist.mappers.ReporteGetSucursalesRowMapper;
import com.gruposalinas.checklist.mappers.ReporteImgRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ReporteImgDAOImpl extends DefaultDAO implements ReporteImgDAO {

    private static Logger logger = LogManager.getLogger(ReporteImgDAOImpl.class);

    DefaultJdbcCall jdbcObtieneConsulta;
    DefaultJdbcCall jdbcObtieneConsulta2;
    DefaultJdbcCall jdbcObtieneConsulta3;
    DefaultJdbcCall jdbcAcervo;

    public void init() {

        jdbcObtieneConsulta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTEIMG")
                .withProcedureName("SPGETDETCECO")
                .returningResultSet("PA_CONSULTA", new ReporteImgRowMapper());

        jdbcObtieneConsulta2 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTEIMG")
                .withProcedureName("SPGETPROTOCOLOS")
                .returningResultSet("PA_CONSULTA", new ReporteGetProtocolosRowMapper());

        jdbcObtieneConsulta3 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTEIMG")
                .withProcedureName("SPGETSUCURSALES")
                .returningResultSet("PA_CONSULTA", new ReporteGetSucursalesRowMapper());

        jdbcAcervo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTEIMG")
                .withProcedureName("SPGETACERVOIMG")
                .returningResultSet("PA_CONSULTA", new ReporteAcervoRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReporteImgDTO> obtieneDetalleCeco(String idCeco, String fechaInicio, String fechaFin, String idChecklist) throws Exception {
        Map<String, Object> out = null;
        List<ReporteImgDTO> listaConsulta = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FCID_CECO", idCeco)
                .addValue("PA_FECHAINI", fechaInicio)
                .addValue("PA_FECHAFIN", fechaFin)
                .addValue("PA_FIID_CHECKLIST", idChecklist);

        out = jdbcObtieneConsulta.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAREPORTEIMG.SPGETDETCECO");

        listaConsulta = (List<ReporteImgDTO>) out.get("PA_CONSULTA");

        if (listaConsulta.size() == 0) {
            logger.info("Algo ocurrió al consultar SPGETDETCECO: listaConsulta Vacia");
        }

        return listaConsulta;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ReporteImgDTO> obtieneProtocolosZonas(int idGrupo) throws Exception {
        Map<String, Object> out = null;
        List<ReporteImgDTO> listaPZ = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIORDEN_GRUPO", idGrupo);

        out = jdbcObtieneConsulta2.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAREPORTEIMG.SPGETPROTOCOLOS");

        listaPZ = (List<ReporteImgDTO>) out.get("PA_CONSULTA");

        if (listaPZ.size() == 0) {
            logger.info("Algo ocurrió al consultar el StoreProcedure: listaPZ Vacia");
        }

        return listaPZ;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ReporteImgDTO> obtieneSucursales() throws Exception {
        Map<String, Object> out = null;
        List<ReporteImgDTO> listaSucursales = null;

        SqlParameterSource in = new MapSqlParameterSource();

        out = jdbcObtieneConsulta3.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAREPORTEIMG.SPGETSUCURSALES");

        listaSucursales = (List<ReporteImgDTO>) out.get("PA_CONSULTA");

        if (listaSucursales.size() == 0) {
            logger.info("Algo ocurrió al consultar el StoreProcedure: listaSucursales Vacia");
        }

        return listaSucursales;
    }

    @Override
    public List<ReporteImgDTO> obtieneAcervo(int idBitacora, int idChecklist) throws Exception {
        Map<String, Object> out = null;
        List<ReporteImgDTO> listaAcervo = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_BITACORA", idBitacora)
                .addValue("PA_FIID_CHECKLIST", idChecklist);

        out = jdbcAcervo.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAREPORTEIMG.SPGETACERVOIMG");

        listaAcervo = (List<ReporteImgDTO>) out.get("PA_CONSULTA");

        if (listaAcervo.size() == 0) {
            logger.info("Algo ocurrió al consultar el StoreProcedure: listaAcervo Vacia");
        }

        return listaAcervo;
    }

}
