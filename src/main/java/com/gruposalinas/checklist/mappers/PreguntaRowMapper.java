package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.PreguntaDTO;

public class PreguntaRowMapper implements RowMapper<PreguntaDTO> {

	public PreguntaDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		PreguntaDTO preguntaDTO = new PreguntaDTO();

		preguntaDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		preguntaDTO.setEstatus(rs.getInt("FIESTATUS"));
		preguntaDTO.setPregunta(rs.getString("FCDESCRIPCION"));
		preguntaDTO.setIdModulo(rs.getInt("FIID_MODULO"));
		preguntaDTO.setIdTipo(rs.getInt("FIID_TIPO_PREG"));
                preguntaDTO.setResponsable(rs.getString("FCRESPONSABLE"));
                preguntaDTO.setNegocio(rs.getString("FCNEGOCIO"));
		return preguntaDTO;
	}
}
