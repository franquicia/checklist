
<div id="content" class="carouselDiv" style="width: 70%; margin: 0 auto; padding: 10px">
	<!--  <img src="/checklist/images/checklist_espera.jpg"/> -->

	<!-- Carousel -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel" style="width: 100%; height: 80%;">
		<!-- Indicadores -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>
		<!-- Imagenes -->
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<img src="../images/carousel/7s/7s1.png"
					alt="imagen1" style="height: 100%; width: 100%;">
			</div>

			<div class="item">
				<img src="../images/carousel/7s/7s2.png"
					alt="imagen2" style="height: 100%; width: 100%;">
			</div>

			<div class="item">
				<img src="../images/carousel/7s/7s3.png"
					alt="imagen3" style="height: 100%; width: 100%;">
			</div>
			<!-- Controles Izquierda y Derecha -->
			<a class="left carousel-control" href="#myCarousel" role="button"data-slide="prev"> 
				<span class="flechas" id="fAnterior"> <img
					src="../images/carousel/slide/flechaizqover.png" alt="imagen2"
					width="40" height="55">
				</span>
			</a> 
			<a class="right carousel-control" href="#myCarousel" role="button"
				data-slide="next"> <!--Imagenes Flecha Derecha --> <span
				class="flechas" id="fSiguiente"> <img
					src="../images/carousel/slide/flechaderover.png" alt="imagen2"
					width="40" height="55">
			</span>
			</a>
		</div>
	</div>
</div>

<script type="text/javascript" src="../js/bootstrap.min.js"></script>

<!--  <a href="#popup" class="popup-link">Ver mas información</a> -->

<!-- BOTON PARA OCULTAR EL MENU 
<div id = "ocultaButtonContainer">
<div id="button"><button onclick="ocultaMenu();"><img id="buttonOculta" src="images/menu/menu.png" alt="menu"></button></div>
</div> 
<script>
function ocultaMenu() {
		  if ( $( "#menu" ).is( ":hidden" ) ) {
		    $( "#menu" ).show( "slow" );
		  } else {
		    $( "#menu" ).slideUp();
		  }
}
</script>

-->

