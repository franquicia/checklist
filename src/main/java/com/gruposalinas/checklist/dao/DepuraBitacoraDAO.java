package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.AdminSupDTO;
import com.gruposalinas.checklist.domain.DepuraBitacoraDTO;

public interface DepuraBitacoraDAO {
	
	public List<DepuraBitacoraDTO> depuraBitacorasSem(int fecha) throws Exception;
	
	
}
