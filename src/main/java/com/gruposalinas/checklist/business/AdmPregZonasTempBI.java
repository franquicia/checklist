package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.AdmPregZonasTempDAO;
import com.gruposalinas.checklist.domain.AdmPregZonaDTO;

public class AdmPregZonasTempBI {

    private static Logger logger = LogManager.getLogger(AdmPregZonasBI.class);

    @Autowired
    AdmPregZonasTempDAO admPregZonasDAO;
    ArrayList<AdmPregZonaDTO> lista = null;

    public ArrayList<AdmPregZonaDTO> obtienePregZonaById(AdmPregZonaDTO bean) {
        try {
            lista = admPregZonasDAO.obtienePregZonaById(bean);
        } catch (Exception e) {
            logger.info("No fue posible obtener la pregunta zona: " + e.getMessage());
        }

        return lista;
    }

    public boolean insertaPregZona(AdmPregZonaDTO bean) {

        boolean respuesta = false;
        try {
            respuesta = admPregZonasDAO.insertaPregZona(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar la pregunta zona: " + e.getMessage());
        }

        return respuesta;
    }

    public boolean actualizaPregZona(AdmPregZonaDTO bean) {

        boolean respuesta = false;
        try {
            respuesta = admPregZonasDAO.actualizaPregZona(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar la pregunta zona: " + e.getMessage());

        }

        return respuesta;
    }

    public boolean eliminaPregZona(AdmPregZonaDTO bean) {

        boolean respuesta = false;
        try {
            respuesta = admPregZonasDAO.eliminaPregZona(bean);
        } catch (Exception e) {
            logger.info("No fue posible borrar la pregunta zona: " + e.getMessage());
        }

        return respuesta;
    }

}
