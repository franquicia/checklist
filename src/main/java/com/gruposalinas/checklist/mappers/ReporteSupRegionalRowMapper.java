package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteDTO;

public class ReporteSupRegionalRowMapper implements RowMapper<ReporteDTO>  {
	@Override
	public ReporteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		ReporteDTO reporteDTO = new ReporteDTO();
		
		reporteDTO.setIdCeco(rs.getString("FCID_CECO"));
		reporteDTO.setNombreCeco(rs.getString("FCNOMBRE"));
		reporteDTO.setSi(rs.getString("SI"));
		reporteDTO.setNo(rs.getString("NO"));
		reporteDTO.setAvance(rs.getInt("AVANCE"));
		reporteDTO.setFondo(rs.getString("FONDO"));;
		
		return reporteDTO;
	}

}
