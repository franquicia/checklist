package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteSistemasDTO;

public class ReporteEvidenciaSupervisionSistRowMapper implements RowMapper<ReporteSistemasDTO> {

	public ReporteSistemasDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteSistemasDTO reporteCecosSistemas = new ReporteSistemasDTO();
		
		reporteCecosSistemas.setIdEvidencia(rs.getInt("FIIDEVIDENCIA"));
		reporteCecosSistemas.setRuta(rs.getString("FCRUTA"));
		reporteCecosSistemas.setIdrespuesta(rs.getInt("FIID_RESPUESTA"));
		reporteCecosSistemas.setRespuesta(rs.getString("EVIDENCIA"));
		reporteCecosSistemas.setFecha(rs.getString("FECHA_E"));
		
		return reporteCecosSistemas;
	}
}

