/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.servicios.servidor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gruposalinas.checklist.business.ServerLogsBI;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/api-local/monitoreo/logs")
public class ServiceServerLogs {

    @Autowired
    private ServerLogsBI serverLogsBI;

    private final Logger logger = LogManager.getLogger();

    /**
     *
     * @param appender
     * @param jsonPath
     * @param orderBy
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ObjectNode readLogs(@RequestParam("appender") String appender, @RequestParam(value = "jsonPath", required = false) String jsonPath, @RequestParam(value = "orderBy", required = false) String orderBy) throws DataNotFoundException {
        ObjectNode objectNodeOrderBy = null;
        if (orderBy != null) {
            try {
                objectNodeOrderBy = new ObjectMapper().readValue(orderBy, ObjectNode.class);
            } catch (JsonProcessingException ex) {
                logger.warn("Error", ex);
            }
        }
        ObjectNode list = serverLogsBI.readLogs(appender, jsonPath, objectNodeOrderBy);
        if (list == null) {
            throw new DataNotFoundException();
        }
        return list;
    }

    /**
     *
     * @param appender
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @RequestMapping(value = "/historial", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ObjectNode> listLogsHistory(@RequestParam("appender") String appender) throws DataNotFoundException {
        List<ObjectNode> list = serverLogsBI.listLogsHistory(appender);
        if (list == null) {
            throw new DataNotFoundException();
        }
        return list;
    }

    /**
     *
     * @param file
     * @param appender
     * @param jsonPath
     * @param orderBy
     * @return
     * @throws com.gs.baz.frq.model.commons.DataNotFoundException
     */
    @RequestMapping(value = "/historial/{file}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object readLogsHistory(@PathVariable("file") String file, @RequestParam("appender") String appender, @RequestParam(value = "jsonPath", required = false) String jsonPath, @RequestParam(value = "orderBy", required = false) String orderBy) throws DataNotFoundException {
        ObjectNode objectNodeOrderBy = null;
        if (orderBy != null) {
            try {
                objectNodeOrderBy = new ObjectMapper().readValue(orderBy, ObjectNode.class);
            } catch (JsonProcessingException ex) {
                logger.warn("Error", ex);
            }
        }
        ObjectNode list = serverLogsBI.readLogsHistory(file, appender, jsonPath, objectNodeOrderBy);
        if (list == null) {
            throw new DataNotFoundException();
        }
        return list;
    }

}
