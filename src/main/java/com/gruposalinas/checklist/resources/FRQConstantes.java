package com.gruposalinas.checklist.resources;

import java.net.InetAddress;
import java.net.UnknownHostException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FRQConstantes {

    private static Logger logger = LogManager.getLogger(FRQConstantes.class);

    /*
     * 0.- Produccion = (true y false) ó (true true)
     * 1.- Desarrollo = (false y true)
     * 2.- Local = (false y false)
     */
    public static final boolean PRODUCCION = false;
    public static final boolean DESARROLLO = false;

    //public static final String URL_SERVIDOR_DESARROLLO 			= "http://10.50.109.39:8080"; //SOCIOSdesarrollobienestar.socio.gs
    //public static final String URL_SERVIDOR_DESARROLLO 			= "http://desarrollobienestar.socio.gs:8080";
    public static final String URL_SERVIDOR_DESARROLLO = "http://10.50.109.47:8080"; //Aquí va la ip de ambiente de desarrollo
    public static final String URL_SERVIDOR_LOCAL = "http://" + FRQConstantes.getIPLocal() + ":8080"; //Aquí va la ip del desarrollador
    //public static final String URL_SERVIDOR_PRODUCCION 			= "http://10.53.29.67:9991";
    public static final String URL_SERVIDOR_PRODUCCION = "http://10.53.33.83:80";

    //public static final String URL_SERVIDOR_A_DESARROLLO 		= "http://10.50.109.39";
    public static final String URL_SERVIDOR_A_DESARROLLO = "http://10.50.109.47"; //Aquí va la ip de ambiente de desarrollo
    public static final String URL_SERVIDOR_A_LOCAL = "http://" + FRQConstantes.getIPLocal(); //Aquí va la ip del desarrollador
    //public static final String URL_SERVIDOR_A_PRODUCCION 		= "http://10.53.29.67";
    public static final String URL_SERVIDOR_A_PRODUCCION = "http://10.53.33.83";

    //public static final String IP_ORIGEN_DESARROLLO 			= "10.50.109.39";
    public static final String IP_ORIGEN_DESARROLLO = "10.50.109.47";
    public static final String IP_ORIGEN_LOCAL = FRQConstantes.getIPLocal();
    //public static final String IP_ORIGEN_PRODUCCION 			= "10.53.29.67";
    public static final String IP_ORIGEN_PRODUCCION = "10.53.33.83";

    //public static final String IP_HOST_DESARROLLO 				= "10.50.109.47"; //Ip para pruebas en desarrollo
    public static final String IP_HOST_DESARROLLO = "192.168.2.1"; //Ip para pruebas en equipo local
    //public static final String IP_HOST_PRODUCCION 				= "10.53.29.153";//Esta es la ip del servidor donde sera ejecutado el Scheduler
    public static final String IP_HOST_PRODUCCION = "10.53.33.76";//Esta es la ip del servidor donde sera ejecutado el Scheduler

    //public static final String LLAVE_ENCRIPCION_DESARROLLO 		= "fce099383332d9ffa34c4f8a50dda9a0="; //desarrollo original se debe comentar para activar simulacion de producción
    public static final String LLAVE_ENCRIPCION_DESARROLLO = "eJG7R3O/Z+8a/FzQb4X7zv5Um++9tHo="; //desarrollo nueva se debe comentar para activar simulacion de producción
    //public static final String LLAVE_ENCRIPCION_PRODUCCION 		= "fce099383332d9ffa34c4f8a50dda9a0=";
    public static final String LLAVE_ENCRIPCION_PRODUCCION = "Ux+Z68QHllMW6jyUfPaO2f5h7Oost8U=";//llave nueva de produccion
    public static final String LLAVE_ENCRIPCION_LOCAL = "fce099383332d9ffa34c4f8a50dda9a0=";//id=666
    public static final String LLAVE_ENCRIPCION_LOCAL_IOS = "fce099383332d9ff";//id=7
    public static final String LLAVE_ENCRIPCION_LOCAL_DATACENTER = "721490d02e1641118f208b85c47c0dfa";//id=10

    //public static final String URL_WSTOKEN_DESARROLLO			= "http://desarrollo.socio.gs/Login5/token_esp/token_esp.asmx?WSDL"; //unreacheable
    //public static final String URL_WSTOKEN_PRODUCCION			= "http://portal.socio.gs/Login5/token_esp/token_esp.asmx?WSDL"; //unreacheable
    public static final String URL_WSTOKEN_DESARROLLO = "http://10.50.158.63/Login5/token_esp/token_esp.asmx?WSDL"; //desarrollo original se debe comentar para activar simulacion de producción
    public static final String URL_WSTOKEN_PRODUCCION = "https://portal.socio.gs/Login5/token_esp/token_esp.asmx?WSDL";

    //public static final String URL_LOGIN_LLAVE_DESARROLLO		= "http://desarrollo.socio.gs/Login5/login/default.aspx?token="; //unreacheable
    //public static final String URL_LOGIN_LLAVE_PRODUCCION		= "http://portal.socio.gs/Login5/login/default.aspx?token="; //unreacheable
    public static final String URL_LOGIN_LLAVE_DESARROLLO = "https://10.50.158.63/Login5/login/default.aspx?token=";  //desarrollo original se debe comentar para activar simulacion de producción
    public static final String URL_LOGIN_LLAVE_PRODUCCION = "https://portal.socio.gs/Login5/login/default.aspx?token=";

    public static final String ID_APP_PORTALES_DESARROLLO = "2"; //desarrollo original se debe comentar para activar simulacion de producción
    public static final String ID_APP_PORTALES_PRODUCCION = "59";
    public static final String ID_APP_PORTALES_ID_LLAVE = "666";
    public static final String ID_APP_PORTALES_ID_LLAVE_IOS = "7";
    public static final String ID_APP_PORTALES_ID_LLAVE_DATACENTER = "10";

    public static final String RUTA_IMAGEN_LOCAL = "http://" + FRQConstantes.getIPLocal();
    public static final String RUTA_IMAGEN_DESARROLLO = "http://10.50.109.39/Sociounico"; //Aquí va la url de desarrollo
    //public static final String RUTA_IMAGEN_PRODUCCION 			= "http://imgsbienestar.socio.gs/sociounico";
    public static final String RUTA_IMAGEN_PRODUCCION = "http://10.53.33.82";

    public static final String RUTA_IMAGEN_DESARROLLO_MOVIL = "http://10.50.109.39/Sociounico"; //Aquí va la url de desarrollo
    //public static final String RUTA_IMAGEN_PRODUCCION_MOVIL		= "https://200.38.122.186:8443/sociounico";
    public static final String RUTA_IMAGEN_PRODUCCION_MOVIL = "http://10.53.33.82";

    /*Constantes para el envío de correo*/
 /*De momento solo como prueba*/
    public static final String CTE_HOST_CORREO = "10.63.200.79"; // Productivo
    //public static final String CTE_HOST_CORREO 					= "10.50.40.233";//Desarrollo
    //public static final String CTE_HOST_CORREO 					= "200.38.122.65";//Desarrollo

    public static final String CTE_REMITENTE_SG = "sistemas-checklist@bancoazteca.com.mx";
    public static final String CTE_REMITENTE_VISITAS_SG = "visita-checklist@bancoazteca.com.mx";
    public static final String CTE_REMITENTE_EXPANSION_SG = "expansion@bancoazteca.com.mx";
    public static final String CTE_REMITENTE_CARGA_CECOS = "carga-cecos@bancoazteca.com.mx";

    public final static String MASTEREY_ENCRYPT = "4WMJz+AdWabMjaiV";
    public final static String MASTERKEY_URL = "http://10.50.169.89/wsAppLoginParam/Service.asmx?WSDL";

    public final static String URL_TAREAS_DESARROLO = "http://10.50.109.47:8081";
    public final static String URL_TAREAS_PRODUCCION = "http://10.63.21.39:28080";

    public final static String URL_INCIDENTES_DESARROLLO = "http://10.54.28.194/wsGestionIncFranquiciasApp/Service1.svc";
    public final static String URL_INCIDENTES_PRODUCCION = "http://10.54.21.38/wsGestionIncFranquiciasApp/Service1.svc";

    public final static String URL_INCIDENTES_ADJUNTO_DESARROLLO = "http://10.54.28.194/wsGestionIncFranquicias/Service1.svc";
    public final static String URL_INCIDENTES_ADJUNTO_PRODUCCION = "http://10.54.21.38/wsGestionIncFranquicias/Service1.svc";

    /*public final static String mailContactoMaria = "mhuertar@elektra.com.mx";
     public final static String mailContactoJorge = "jmoralesm@elektra.com.mx";
     public final static String mailContactoGustavo = "gmirandar@elektra.com.mx";
     public final static String mailLAM = "mlmondragon@bancoazteca.com.mx";*/
 /*public final static String mailContactoMaria      =   "jsepulveda@elektra.com.mx";
     public final static String mailContactoJorge      =   "lrodriguezv@elektra.com.mx";
     public final static String mailContactoGustavo    =   "amorales@elektra.com.mx";
     public final static String mailLAM    			 =  	 "fjaramillo@elektra.com.mx";*/
    public final static String mailContactoMaria = "cvidal@bancoazteca.com.mx";
    public final static String mailContactoJorge = "jhvazquez@elektra.com.mx";
    public final static String mailContactoGustavo = "mdelgadoa@elektra.com.mx";
    public final static String mailLAM = "alejandro.morales@elektra.com.mx";

    public static final String URL_SERVIDOR_IMAGENES_LOCAL = "http://" + FRQConstantes.getIPLocal(); //Aquí va la ip del desarrollador
    //public static final String URL_SERVIDOR_A_PRODUCCION 		= "http://10.53.29.67";
    public static final String URL_SERVIDOR_IMAGENES_PRODUCCION = "http://10.53.33.82";

    //perfiles
    public static final String PERFIL_EXPANSION_AUTORIZACION = "227";
    public static final String PERFIL_EXPANSION_ATIENDE = "233";
    public static final int PERFIL_TRANSFORMACION_SUP_OBRA = 241;
    public static final int PERFIL_TRANSFORMACION_CORDINADOR = 242;
    public static final int PERFIL_TRANSFORMACION_FRANQUICIA = 243;

    public final static String URL_WS_GENERA_TOKEN_CECO_DESARROLLO = "http://10.50.109.69:8080/cecows/rest/token";
    public final static String URL_WS_GENERA_TOKEN_CECO_PRODUCCION = "http://10.53.33.85:8643/cecows/rest/token";

    public final static String URL_WS_CONSULTA_CECOS_DESARROLLO = "http://10.50.109.69:8080/cecows/rest/ekt/unificada";
    public final static String URL_WS_CONSULTA_CECOS_PRODUCCION = "http://10.53.33.85:8643/cecows/rest/ekt/unificada";

    public final static String URL_WS_CONSULTA_CECOS_DESARROLLO_PRESENTACION = "http://10.50.109.69:8080/cecows/rest/ekt/presentacion/estructuras";

    public final static String usuarioWSCecoTokenDes = "USRSISF";
    public final static String passwordWSCecoTokenDes = "UsrFrQc71";

    public final static String usuarioWSCecoTokenProd = "USRSISF";
    public final static String passwordWSCecoTokenPro = "UsrFrQc71";

    public final static String depuraTablaDesarrollo = "TAPASO_CECO";
    public final static String depuraTablaProduccion = "TAPASO_CECO";

    public final static String depuraTablaOriginal = "FRTPPASO_CECO";
    public final static String depuraTablaOriginalProducion = "FRTPPASO_CECO";

    public final static String URL_WS_MENSAJE_PERSONALIZADO_WIFI_TOKEN_DEV = "http://10.54.36.125:8339/dev/oauth";
    public final static String URL_WS_MENSAJE_PERSONALIZADO_WIFI_TOKEN_PRODUCCION = "http://10.54.36.125:8339/dev/oauth";

    public final static String URL_WS_MENSAJE_PERSONALIZADO_WIFI_DEV = "http://10.54.36.125:8339/dev/crm/v1/transaccional/wifi/registrar";
    public final static String URL_WS_MENSAJE_PERSONALIZADO_WIFI_PRODUCCION = "http://10.54.36.125:8339/dev/crm/v1/transaccional/wifi/registrar";

    public final static String usuarioTokenWSCRMDes = "TRANSACCIONESWIFI";
    public final static String passwordTokenWSCRMDes = "2f2CM6McZ99FBbQNNSaX5RJ6E4Qp2UJF3iXcbiVvdAwPNCHS";

    public final static String usuarioTokenWSCRMProd = "TRANSACCIONESWIFI";
    public final static String passwordTokenWSCRMProd = "2f2CM6McZ99FBbQNNSaX5RJ6E4Qp2UJF3iXcbiVvdAwPNCHS";

    public static String getURLServer() {
        String server = URL_SERVIDOR_LOCAL;
        if (DESARROLLO) {
            server = URL_SERVIDOR_DESARROLLO;
        }
        if (PRODUCCION) {
            server = URL_SERVIDOR_PRODUCCION;
        }
        return server;
    }

    public static String getURLApacheServer() {
        String server = URL_SERVIDOR_A_LOCAL;
        if (DESARROLLO) {
            server = URL_SERVIDOR_A_DESARROLLO;
        }
        if (PRODUCCION) {
            server = URL_SERVIDOR_A_PRODUCCION;
        }
        return server;
    }

    public static String getIpOrigen() {
        String ip = IP_ORIGEN_LOCAL;
        if (DESARROLLO) {
            ip = IP_ORIGEN_DESARROLLO;
        }
        if (PRODUCCION) {
            ip = IP_ORIGEN_PRODUCCION;
        }
        return ip;
    }

    public static String getUrlWsToken() {
        String url = URL_WSTOKEN_DESARROLLO;
        if (PRODUCCION) {
            url = URL_WSTOKEN_PRODUCCION;
        }
        return url;
    }

    public static String getUrlLoginLlave() {
        String url = URL_LOGIN_LLAVE_DESARROLLO;
        if (PRODUCCION) {
            url = URL_LOGIN_LLAVE_PRODUCCION;
        }
        return url;
    }

    public static String getIdApp() {
        String url = ID_APP_PORTALES_DESARROLLO;
        if (PRODUCCION) {
            url = ID_APP_PORTALES_PRODUCCION;
        }
        return url;
    }

    public static String getLLave() {
        String llave = LLAVE_ENCRIPCION_DESARROLLO;
        if (PRODUCCION) {
            llave = LLAVE_ENCRIPCION_PRODUCCION;
        }
        return llave;
    }

    public static String getLlaveEncripcionLocal() {
        return LLAVE_ENCRIPCION_LOCAL;
    }

    public static String getLlaveEncripcionLocalIOS() {
        return LLAVE_ENCRIPCION_LOCAL_IOS;
    }

    public static String getLlaveEncripcionLocalDatacenter() {
        return LLAVE_ENCRIPCION_LOCAL_DATACENTER;
    }

    public static String getIpHost() {
        String ip = IP_HOST_DESARROLLO;
        if (PRODUCCION) {
            ip = IP_HOST_PRODUCCION;
        }
        return ip;
    }

    public static String getRutaImagen() {
        String server = RUTA_IMAGEN_LOCAL;
        if (DESARROLLO) {
            server = RUTA_IMAGEN_DESARROLLO;
        }
        if (PRODUCCION) {
            server = RUTA_IMAGEN_PRODUCCION;
        }
        return server;
    }

    public static String getRutaImagenMovil() {
        String server = RUTA_IMAGEN_DESARROLLO_MOVIL;
        if (PRODUCCION) {
            server = RUTA_IMAGEN_PRODUCCION_MOVIL;
        }
        return server;
    }

    public static String getRutaTareas() {
        String server = URL_TAREAS_DESARROLO;
        if (PRODUCCION) {
            server = URL_TAREAS_PRODUCCION;
        }
        return server;
    }

    public static String getURLIncidentes() {
        String server = URL_INCIDENTES_DESARROLLO;
        if (PRODUCCION) {
            server = URL_INCIDENTES_PRODUCCION;
        }
        return server;
    }

    public static String getURLIncidentesAdjuntos() {
        String server = URL_INCIDENTES_ADJUNTO_DESARROLLO;
        if (PRODUCCION) {
            server = URL_INCIDENTES_ADJUNTO_PRODUCCION;
        }
        return server;
    }

    //GENERA TOKEN CECOS
    public static String getURLTokenCecos() {
        String server = URL_WS_GENERA_TOKEN_CECO_DESARROLLO;
        if (PRODUCCION) {
            server = URL_WS_GENERA_TOKEN_CECO_PRODUCCION;
        }
        return server;
    }

    //CONSULTA CECOS
    public static String getURLConsultaCecos() {
        String server = URL_WS_CONSULTA_CECOS_DESARROLLO;
        if (PRODUCCION) {
            server = URL_WS_CONSULTA_CECOS_PRODUCCION;
        }
        return server;
    }

    public static String getURLConsultaCecosPresentacion() {
        String server = URL_WS_CONSULTA_CECOS_DESARROLLO_PRESENTACION;
        if (PRODUCCION) {
            server = URL_WS_CONSULTA_CECOS_DESARROLLO_PRESENTACION;
        }
        return server;
    }

    //Usuario ws ceco
    public static String getUsuarioWSCeco() {
        String usuario = usuarioWSCecoTokenDes;

        if (PRODUCCION) {
            usuario = usuarioWSCecoTokenProd;
        }
        return usuario;
    }

    //Password ws ceco
    public static String getPasswordWSCeco() {
        String password = passwordWSCecoTokenDes;

        if (PRODUCCION) {
            password = passwordWSCecoTokenPro;
        }
        return password;
    }

    //Depurar Tabla de Paso
    public static String getDepuraTabla() {
        String password = depuraTablaDesarrollo;

        if (PRODUCCION) {
            password = depuraTablaProduccion;
        }
        return password;
    }

    public static String getDepuraTablaOriginal() {
        String password = depuraTablaOriginal;

        if (PRODUCCION) {
            password = depuraTablaOriginalProducion;
        }
        return password;
    }

    public static String getIPLocal() {
        try {
            InetAddress inetAddress = InetAddress.getLocalHost();
            return inetAddress.getHostAddress();
        } catch (UnknownHostException ex) {
            logger.error("Error", ex);
        }
        return "127.0.0.1";

        //return "10.51.218.140";
    }

    //public final static String URL_GESTION_DESARROLLO			= "http://10.51.218.140:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=";
    public final static String URL_GESTION_DESARROLLO = "http://10.53.33.83:80/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=";
    public final static String URL_GESTION_PRODUCCION = "http://10.53.33.83:80/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=";

    public static String getURLMiGestion() {
        String server = URL_GESTION_DESARROLLO;
        if (DESARROLLO) {
            server = URL_GESTION_DESARROLLO;
        }
        if (PRODUCCION) {
            server = URL_GESTION_PRODUCCION;
        }
        return server;
    }

    public static String getURLServerImagenes() {
        String server = URL_SERVIDOR_IMAGENES_LOCAL;
        if (DESARROLLO) {
            server = URL_SERVIDOR_IMAGENES_LOCAL;
        }
        if (PRODUCCION) {
            server = URL_SERVIDOR_IMAGENES_PRODUCCION;
        }
        return server;
    }
//token crm

    public static String getURLMensajesPersonalizadosTokenWiFI() {
        String server = URL_WS_MENSAJE_PERSONALIZADO_WIFI_TOKEN_DEV;
        if (DESARROLLO) {
            server = URL_WS_MENSAJE_PERSONALIZADO_WIFI_TOKEN_DEV;
        }
        if (PRODUCCION) {
            server = URL_WS_MENSAJE_PERSONALIZADO_WIFI_TOKEN_PRODUCCION;
        }
        return server;
    }

    //Usuario ws CRM token
    public static String getUsuarioTokenWSCRM() {
        String usuario = usuarioTokenWSCRMDes;

        if (PRODUCCION) {
            usuario = usuarioTokenWSCRMProd;
        }
        return usuario;
    }

    //Password ws CRM token
    public static String getPasswordTokenWSCRM() {
        String password = passwordTokenWSCRMDes;

        if (PRODUCCION) {
            password = passwordTokenWSCRMProd;
        }
        return password;
    }

    public static String getURLMensajesPersonalizadosWiFI() {
        String server = URL_WS_MENSAJE_PERSONALIZADO_WIFI_DEV;
        if (DESARROLLO) {
            server = URL_WS_MENSAJE_PERSONALIZADO_WIFI_DEV;
        }
        if (PRODUCCION) {
            server = URL_WS_MENSAJE_PERSONALIZADO_WIFI_PRODUCCION;
        }
        return server;
    }

}
