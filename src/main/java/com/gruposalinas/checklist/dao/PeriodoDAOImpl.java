package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.PeriodoDTO;
import com.gruposalinas.checklist.mappers.PeriodoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PeriodoDAOImpl extends DefaultDAO implements PeriodoDAO {

    private static Logger logger = LogManager.getLogger(PeriodoDAOImpl.class);

    DefaultJdbcCall jdbcObtienePeriodo;
    DefaultJdbcCall jdbcInsertaPeriodo;
    DefaultJdbcCall jdbcActualizaPeriodo;
    DefaultJdbcCall jdbcEliminaPeriodo;

    public void init() {

        jdbcObtienePeriodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERIODO")
                .withProcedureName("SP_SEL_PERIODO")
                .returningResultSet("RCL_GRUPO", new PeriodoRowMapper());

        jdbcInsertaPeriodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERIODO")
                .withProcedureName("SP_INS_PERIODO");

        jdbcActualizaPeriodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERIODO")
                .withProcedureName("SP_ACT_PERIODO");

        jdbcEliminaPeriodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERIODO")
                .withProcedureName("SP_DEL_PERIODO");
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PeriodoDTO> obtienePeriodo(String idPeriodo) throws Exception {
        Map<String, Object> out = null;
        List<PeriodoDTO> listaPeriodo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPER", idPeriodo);

        out = jdbcObtienePeriodo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPERIODO.SP_SEL_PERIODO}");

        listaPeriodo = (List<PeriodoDTO>) out.get("RCL_GRUPO");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Peirodo");
        } else {
            return listaPeriodo;
        }

        return null;
    }

    @Override
    public int insertaPeriodo(PeriodoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idGrupo = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcInsertaPeriodo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPERIODO.SP_INS_PERIODO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDPER");
        idGrupo = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Periodo");
        } else {
            return idGrupo;
        }

        return idGrupo;
    }

    @Override
    public boolean actualizaPeriodo(PeriodoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPER", bean.getIdPeriodo())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcActualizaPeriodo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPERIODO.SP_ACT_PERIODO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el Periodo");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaPeriodo(int idPeriodo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPER", idPeriodo);

        out = jdbcEliminaPeriodo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPERIODO.SP_DEL_PERIODO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el Periodo");
        } else {
            return true;
        }

        return false;
    }
}
