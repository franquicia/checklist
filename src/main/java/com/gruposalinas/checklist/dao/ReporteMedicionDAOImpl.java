package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ReporteMedPregDTO;
import com.gruposalinas.checklist.domain.ReporteMedicionDTO;
import com.gruposalinas.checklist.mappers.ReporteMedPregRowMapper;
import com.gruposalinas.checklist.mappers.ReporteMedicionRowMapper;
import com.gruposalinas.checklist.mappers.ReporteProtMedPregRowMapper;
import com.gruposalinas.checklist.mappers.ReporteProtMedRespRowMapper2;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ReporteMedicionDAOImpl extends DefaultDAO implements ReporteMedicionDAO {

    private static final Logger logger = LogManager.getLogger(ReporteMedicionDAOImpl.class);

    private DefaultJdbcCall jdbcReporteCompleto;
    private DefaultJdbcCall jdbcReporteProtocolo;
    private DefaultJdbcCall jdbcPreguntas;

    public void init() {

        jdbcReporteCompleto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE_MEDICION")
                .withProcedureName("SPGETREPORTE")
                .returningResultSet("RCL_PREGUNTAS", new ReporteMedPregRowMapper())
                .returningResultSet("RCL_REPORTE", new ReporteMedicionRowMapper());

        jdbcReporteProtocolo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE2")
                .withProcedureName("SP_REPORTE_G_COMP")
                .returningResultSet("RCL_PREGUNTAS", new ReporteProtMedPregRowMapper())
                .returningResultSet("RCL_REPORTE", new ReporteProtMedRespRowMapper2());

        jdbcPreguntas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE_MEDICION")
                .withProcedureName("SPGETREPORTE")
                .returningResultSet("RCL_PREGUNTAS", new ReporteMedPregRowMapper());

    }

    @Override
    public Map<String, Object> ReporteMedicion(int idProtocolo, String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<ReporteMedPregDTO> listaPreguntas = null;
        List<ReporteMedicionDTO> listaRespuestas = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PROTOCOLO", idProtocolo)
                .addValue("PA_FECHAINI", fechaInicio)
                .addValue("PA_FECHAFIN", fechaFin);

        out = jdbcReporteCompleto.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAREPORTE_MEDICION.SPGETREPORTE}");

        listaPreguntas = (List<ReporteMedPregDTO>) out.get("RCL_PREGUNTAS");
        listaRespuestas = (List<ReporteMedicionDTO>) out.get("RCL_REPORTE");

        if (listaPreguntas.size() == 0) {
            throw new Exception("El Store Procedure retorno listaPreguntas vacio");
        }

        if (listaRespuestas.size() == 0) {
            throw new Exception("El Store Procedure retorno listaRespuestas vacio");
        }

        lista = new HashMap<String, Object>();
        lista.put("listaPregunta", listaPreguntas);
        lista.put("listaRespuesta", listaRespuestas);
        return lista;

    }

    @Override
    public Map<String, Object> ReporteProtMedicion(int idProtocolo, String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<ReporteMedPregDTO> listaPreguntas = null;
        List<ReporteMedicionDTO> listaRespuestas = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCHECKLIST", idProtocolo)
                .addValue("PA_FECHA_I", fechaInicio)
                .addValue("PA_FECHA_F", fechaFin);

        out = jdbcReporteProtocolo.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAREPORTE_MEDICION.SPGETREPORTE}");

        listaPreguntas = (List<ReporteMedPregDTO>) out.get("RCL_PREGUNTAS");
        listaRespuestas = (List<ReporteMedicionDTO>) out.get("RCL_REPORTE");

        if (listaPreguntas.size() == 0) {
            throw new Exception("El Store Procedure retorno listaPreguntas vacio");
        }

        if (listaRespuestas.size() == 0) {
            throw new Exception("El Store Procedure retorno listaRespuestas vacio");
        }

        lista = new HashMap<String, Object>();
        lista.put("listaPregunta", listaPreguntas);
        lista.put("listaRespuesta", listaRespuestas);
        return lista;

    }

    @Override
    public List<ReporteMedPregDTO> getCursor(int idProtocolo, String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        List<ReporteMedPregDTO> listaPreguntas = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PROTOCOLO", idProtocolo)
                .addValue("PA_FECHAINI", fechaInicio)
                .addValue("PA_FECHAFIN", fechaFin);

        out = jdbcReporteCompleto.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAREPORTE_MEDICION.SPGETREPORTE}");

        listaPreguntas = (List<ReporteMedPregDTO>) out.get("RCL_PREGUNTAS");

        if (listaPreguntas.size() == 0) {
            throw new Exception("El Store Procedure retorno listaPreguntas vacio");
        }

        return listaPreguntas;
    }

}
