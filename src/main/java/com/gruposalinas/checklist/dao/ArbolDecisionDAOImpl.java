package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.mappers.ArbolDecisionRowMapper2;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ArbolDecisionDAOImpl extends DefaultDAO implements ArbolDecisionDAO {

    private static Logger logger = LogManager.getLogger(ArbolDecisionDAOImpl.class);

    private DefaultJdbcCall insertaArbolDecision;
    private DefaultJdbcCall actualizaArbolDecision;
    private DefaultJdbcCall eliminaArbolDecision;
    private DefaultJdbcCall buscaChecklist;
    private DefaultJdbcCall cambiaRespuestas;

    public void init() {

        insertaArbolDecision = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_ARBOL_DES")
                .withProcedureName("SP_INS_ARBOL_DES");

        actualizaArbolDecision = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_ARBOL_DES")
                .withProcedureName("SP_ACT_ARBOL_DES");

        eliminaArbolDecision = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_ARBOL_DES")
                .withProcedureName("SP_DEL_ARBOL_DES");

        buscaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_ARBOL_DES")
                .withProcedureName("SP_SEL_G_ARBOL_DES")
                .returningResultSet("RCL_ARBOL_DES", new ArbolDecisionRowMapper2());

        cambiaRespuestas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPOSIBLESIDS")
                .withProcedureName("SPCAMBIARESPUESTAS");

    }

    public int insertaArbolDecision(ArbolDecisionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int id = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_CHECK", bean.getIdCheckList())
                .addValue("PA_ID_PREG", bean.getIdPregunta())
                .addValue("PA_RESPUESTA", bean.getRespuesta())
                .addValue("PA_ESTATUS_E", bean.getEstatusEvidencia())
                .addValue("PA_ORDENPREG", bean.getOrdenCheckRespuesta())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_REQACCION", bean.getReqAccion())
                .addValue("PA_REQOBS", bean.getReqObservacion())
                .addValue("PA_OBLIGA", bean.getReqOblig())
                .addValue("PA_DESC_E", bean.getDescEvidencia())
                .addValue("PA_PONDERACION", bean.getPonderacion())
                .addValue("PA_PLANTILLA", bean.getIdPlantilla())
                .addValue("PA_PROTOCOLO", bean.getIdProtocolo());

        //System.out.println(bean.getPonderacion());
        out = insertaArbolDecision.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PA_ADM_ARBOL_DES.SP_INS_ARBOL_DES}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal returnId = (BigDecimal) out.get("PA_FIID_ARBOL_DES");
        id = returnId.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar arbol de decision");
        } else {
            return id;
        }

        return 0;
    }

    public boolean actualizaArbolDecision(ArbolDecisionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_ARBOL_DES", bean.getIdArbolDesicion())
                .addValue("PA_ID_CHECK", bean.getIdCheckList())
                .addValue("PA_ID_PREG", bean.getIdPregunta())
                .addValue("PA_RESPUESTA", bean.getRespuesta())
                .addValue("PA_ESTATUS_E", bean.getEstatusEvidencia())
                .addValue("PA_ORDENPREG", bean.getOrdenCheckRespuesta())
                .addValue("PA_REQACCION", bean.getReqAccion())
                .addValue("PA_REQOBS", bean.getReqObservacion())
                .addValue("PA_OBLIGA", bean.getReqOblig())
                .addValue("PA_DESC_E", bean.getDescEvidencia())
                .addValue("PA_PONDERACION", bean.getPonderacion())
                .addValue("PA_PLANTILLA", bean.getIdPlantilla())
                .addValue("PA_PROTOCOLO", bean.getIdProtocolo());

        out = actualizaArbolDecision.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PA_ADM_ARBOL_DES.SP_ACT_ARBOL_DES}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar arbol de decision");
        } else {
            return true;
        }

        return false;

    }

    public boolean eliminaArbolDecision(int idArbolDecision) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_ARBOL_DES", idArbolDecision);

        out = eliminaArbolDecision.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PA_ADM_ARBOL_DES.SP_DEL_ARBOL_DES}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al eliminar arbol de decision");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<ArbolDecisionDTO> buscaArbolDecision(int idChecklist) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<ArbolDecisionDTO> listaArbolDecision = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_CHECKLIST", idChecklist);

        out = buscaChecklist.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PA_ADM_ARBOL_DES.SP_SEL_G_ARBOL_DES}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listaArbolDecision = (List<ArbolDecisionDTO>) out.get("RCL_ARBOL_DES");

        if (error == 1) {
            logger.info("Algo ocurrió al eliminar arbol de decision");
        } else {
            return listaArbolDecision;
        }

        return null;
    }

    @Override
    public boolean cambiaRespuestasPorId() throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        out = cambiaRespuestas.execute();

        logger.info("Funci�n ejecutada:{checklist.PAPOSIBLESIDS.SPCAMBIARESPUESTAS}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al cambiar las respuestas por id Posibles");
        } else {
            return true;
        }

        return false;
    }

}
