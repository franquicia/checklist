<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css"
	href="../css/expancion/detalleExpancion.css">
<link rel="stylesheet" type="text/css"
	href="../css/expancion/jquery.dataTablesImperdonables.css">

<script src="../js/expancion/jquery-3.3.1.js"></script>
<script src="../js/expancion/jquery.dataTables.js"></script>
<script src="../js/expancion/tableExpand.js"></script>
<style type="text/css"></style>
<title>DETALLES IMPERDONABLES</title>
</head>
<body style="max-width:1500px; margin: auto;">
	<div class="marginTotal">
		<div class="left">
			<div class="centrar">
				<img alt="elektra" src="../images/expancion/LogoElektra.svg"
					class="imagenBanner"> <img alt="baz"
					src="../images/expancion/LogoBAZ.svg" class="imagenBanner">
			</div>
		</div>
		<div class="main">
			<h1 style="color: #6B696E;" class="fuenteBold">
				Entrega de Sucursales / <strong class="fuenteBold">DETALLES
					IMPERDONABLES</strong>
			</h1>
			<br />

		</div>
		<div class="right">
			<div class="centrar">
				<img alt="elektra" src="../images/expancion/LogoElektra.svg"
					class="imagenBanner"> <img alt="baz"
					src="../images/expancion/LogoBAZ.svg" class="imagenBanner">
			</div>
		</div>

	</div>
	<div class="marginTotal">
		<div class="left"></div>
		<div class="main">
			<div id="fuenteNumImperdonable">${numImperdonables}</div>
			<h2 class="fuenteBold">Imperdonables</h2>
			<br />
			<hr />
			<div style="padding-left: 5%; width: 80%;">
				<!-- <button id="btn-show-all-children" type="button">Expand All</button>
				<button id="btn-hide-all-children" type="button">Collapse All</button> -->
				<table id="tabla" class="display" cellspacing="0" width="100%">
					<!--<thead>
						<tr>
							<th>Name</th>
							<th>Position</th>
							<th>Office</th>
							<!-- se coloca class none para crear como detalle de columna es necesario descomentar ../js/expancion/tableExpand.js <th class="none">Age</th>
						</tr>
					</thead> -->

					<tbody>
						<c:if test="${chklistPreguntasImp != null }">
							<c:set var="count" value="0" scope="page" />
							<c:forEach var="valor" items="${chklistPreguntasImp}">
								<tr>
									<c:set var="count" value="${count + 1}" scope="page" />
									<td><b>${count}</b>.- ${valor.pregunta}</td>
									<td>  <img alt="img" src="http://10.53.33.82${valor.ruta}"
										class="" height="200" width="200">
									</td>
								</tr>
							</c:forEach>
						</c:if>
					</tbody>
				</table>
			</div>
			<br /> <br /> <br />
			<p>Regresar al Reportes</p>
		</div>
		<div class="right"></div>

	</div>





</body>
</html>