package com.gruposalinas.checklist.business;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.AsignacionesSupDAO;
import com.gruposalinas.checklist.domain.AsignacionesSupDTO;

public class AsignacionesSupBI {
		
	private static Logger logger = LogManager.getLogger(AsignacionesSupBI.class);
	
	@Autowired
	AsignacionesSupDAO asignacionesSupDAO;
	List<AsignacionesSupDTO> listaConsulta = null;
	int respuesta = 0;

	public int insertaAsignacion(int idUsuario, int idPerfil, String idCeco, int idGerente, int activo, String fechaInicio, String fechaFin){	
		try {
			respuesta = asignacionesSupDAO.insertAsignacion(idUsuario, idPerfil, idCeco, idGerente, activo, fechaInicio, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}			
		return respuesta;
	}

	public int updateAsignacion(int idUsuario, int idPerfil, String idCeco, int idGerente, int activo, String fechaInicio, String fechaFin){		
		try {
			respuesta = asignacionesSupDAO.updateAsignacion(idUsuario, idPerfil, idCeco, idGerente, activo, fechaInicio, fechaFin);
		} catch (Exception e) {
			logger.info(e);
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}			
		return respuesta;
	}

	public int deleteAsignacion(int idUsuario, String idCeco){
		try {
			respuesta = asignacionesSupDAO.deleteAsignacion(idUsuario, idCeco);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}
		return respuesta;
	}
	
	public List<AsignacionesSupDTO> getAsignaciones(String idUsuario, String idPerfil, String idCeco, String idGerente, String activo, String fechaInicio, String fechaFin){		
		try {
			listaConsulta = asignacionesSupDAO.getAsignaciones(idUsuario, idPerfil, idCeco, idGerente, activo, fechaInicio, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}
		return listaConsulta;
	}

	public int updateStatus(){	
		try {
			respuesta = asignacionesSupDAO.updateStatus();
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}
		return respuesta;
	}
	
	public int mergeAsignacion(int idUsuario, int idPerfil, String idCeco, String idGerente, int activo, String fechaInicio, String fechaFin){		
		try {
			respuesta = asignacionesSupDAO.mergeAsignaciones(idUsuario, idPerfil, idCeco, idGerente, activo, fechaInicio, fechaFin);
		} catch (Exception e) {
			logger.info(e);
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}			
		return respuesta;
	}
        
        public List<AsignacionesSupDTO> getAsignacionesByUsr(String idUsuario, String activo){		
		try {
			listaConsulta = asignacionesSupDAO.getAsignacionesByUsr(idUsuario,activo);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}
		return listaConsulta;
	}

}
