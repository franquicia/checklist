package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AsistenciaSupervisorDTO;
import com.gruposalinas.checklist.mappers.AsistenciaSupervisorRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AsistenciaSupervisorDAOImpl extends DefaultDAO implements AsistenciaSupervisorDAO {

    private static Logger logger = LogManager.getLogger(ProtocoloDAOImpl.class);

    DefaultJdbcCall jdbcObtieneAsistenciaSupervisor;

    public void init() {

        jdbcObtieneAsistenciaSupervisor = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE_SUPRV").withProcedureName("SPGETASISTENCIA")
                .returningResultSet("PA_CONSULTA", new AsistenciaSupervisorRowMapper());
    }

    @SuppressWarnings("unchecked")
    /* @Override */
    public List<AsistenciaSupervisorDTO> obtieneAsistenciaSupervisor(AsistenciaSupervisorDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AsistenciaSupervisorDTO> listaAsistencia = new ArrayList<AsistenciaSupervisorDTO>();

        int idUsuarioS = 0;
        if (bean.getIdUsuario() != 0) {
            idUsuarioS = bean.getIdUsuario();
        }

        String fechaEntrada = null;
        if (bean.getFechaInicio() != null) {
            fechaEntrada = bean.getFechaInicio();
        }

        String fechaSalida = null;
        if (bean.getFechaFin() != null) {
            fechaSalida = bean.getFechaFin();
        }

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_USUARIO", idUsuarioS)
                .addValue("PA_FECHAINI", fechaEntrada)
                .addValue("PA_FECHAFIN", fechaSalida);

        out = jdbcObtieneAsistenciaSupervisor.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAREPORTE_SUPRV.SPGETASISTENCIA");

        listaAsistencia = (List<AsistenciaSupervisorDTO>) out.get("PA_CONSULTA");

        if (listaAsistencia == null) {
            error = 1;
        }

        if (error == 1) {
            logger.info("Algo ocurrió al obtener la asistencia del supervisor");
        } else {
            return listaAsistencia;
        }

        return listaAsistencia;
    }

}
