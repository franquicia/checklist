package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.PosiblesTipoPreguntaDTO;

public interface PosibleTipoPreguntaTDAO {
	
	public List<PosiblesTipoPreguntaDTO> obtienePosiblesRespuestasTemp(String idTipoPregunta) throws Exception;
	
	public boolean insertaPosibleTipoPregunta(PosiblesTipoPreguntaDTO bean) throws Exception;
	
	public boolean actualizaPosibleTipoPregunta(PosiblesTipoPreguntaDTO bean) throws Exception;
	
	public boolean eliminaPosibleTipoPregunta(int idPosibleTipoPregunta) throws Exception;

}
