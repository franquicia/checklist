/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.logs.config;

/**
 *
 * @author cescobarh
 */
import java.io.PrintStream;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.internal.DefaultLogBuilder;

public class SystemOutToLog4j extends PrintStream {

    private static final PrintStream originalSystemOut = System.out;
    private static SystemOutToLog4j systemOutToLogger;
    private static final PrintStream originalSystemErr = System.err;
    private static SystemOutToLog4j systemErrToLogger;
    private final Level level;

    public static void disable() {
        System.setOut(originalSystemOut);
        System.setErr(originalSystemErr);
        systemOutToLogger = null;
        systemErrToLogger = null;
    }

    public static void enable() {
        systemOutToLogger = new SystemOutToLog4j(originalSystemOut, Level.INFO);
        systemErrToLogger = new SystemOutToLog4j(originalSystemErr, Level.ERROR);
        System.setOut(systemOutToLogger);
        System.setErr(systemErrToLogger);
    }

    private SystemOutToLog4j(PrintStream original, Level level) {
        super(original);
        this.level = level;
    }

    @Override
    public void println(String value) {
        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        if (stack[2] == null) {
            super.println(value);
            return;
        }
        Logger logger = LogManager.getLogger(stack[2].getClassName());
        LogBuilder lb = new DefaultLogBuilder(logger, level);
        lb.withLocation(stack[2]).log(value);
    }

    @Override
    public void println(Object value) {
        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        if (stack[2] == null) {
            super.println(value);
            return;
        }
        Logger logger = LogManager.getLogger(stack[2].getClassName());
        LogBuilder lb = new DefaultLogBuilder(logger, level);
        lb.withLocation(stack[2]).log(value);
    }
}
