package com.gruposalinas.checklist.test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.gruposalinas.checklist.domain.ChecklistLayoutDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;

public class MainTestCarga {

	public static HashMap<String, ArrayList<ChecklistLayoutDTO>> mapaProtocolos = new HashMap<String, ArrayList<ChecklistLayoutDTO>>();

	public static void main(String... args) {

		ArrayList<ChecklistLayoutDTO> listaEntrante = new ArrayList<ChecklistLayoutDTO>();

		listaEntrante = new MainTestCarga().llenaLista();

		// MÓDULOS
		/*
		 * ArrayList<ModuloDTO> listaModulos= new ArrayList<ModuloDTO>(); listaModulos =
		 * new MainTestCarga().getListaModulos(listaEntrante);
		 * 
		 * for (ModuloDTO objModulo : listaModulos) {
		 * System.out.println(objModulo.getNombre()); }
		 */

		// ALTA PROTOCOLOS
		ArrayList<ChecklistProtocoloDTO> listaProtocolos = new MainTestCarga().getListaProtocolos(listaEntrante, 2);

		/*
		 * for (ChecklistProtocoloDTO objProtocolo : listaProtocolos) {
		 * System.out.println("PROTCOLO -->" + objProtocolo.getNombreCheck() +
		 * "PONDERACION --> " + objProtocolo.getPonderacionTot() + "ID -->" +
		 * objProtocolo.getIdChecklist()); }
		 */

		//new MainTestCarga().setIdCheckusua(listaEntrante, listaProtocolos, 2);

		/*
		 * ArrayList<ArrayList<ModuloDTO>> responseInsertaModulos =
		 * obj.insertaModulos(listaModulos);
		 * 
		 * ArrayList<ModuloDTO> listaModulosInsertados = responseInsertaModulos.get(1);
		 * ArrayList<ModuloDTO> listaModulosNoInsertados =
		 * responseInsertaModulos.get(0);
		 * 
		 * //Se insertaron correctamente todos los módulos if
		 * (listaModulosNoInsertados.size() != 0 && (listaModulosInsertados.size() ==
		 * listaModulos.size())) {
		 * 
		 * //No se insertaron todos los módulos correctamente } else {
		 * 
		 * 
		 * }
		 * 
		 * for (ModuloDTO objModulo : listaModulos) {
		 * System.out.println(objModulo.getNombre()); }
		 * 
		 * //PREGUNTAS ArrayList<PreguntaDTO> listaPreguntas= new
		 * ArrayList<PreguntaDTO>(); listaPreguntas =
		 * obj.getListaPreguntas(listaEntrante);
		 */
	}

	public ArrayList<ModuloDTO> getListaModulos(ArrayList<ChecklistLayoutDTO> lista) {

		ArrayList<ModuloDTO> listaSecciones = new ArrayList<ModuloDTO>();

		String seccionActual = "";
		boolean existe = false;

		for (ChecklistLayoutDTO objLista : lista) {

			if (!objLista.getSeccionProtocolo().equalsIgnoreCase(seccionActual)) {

				existe = false;
				seccionActual = objLista.getSeccionProtocolo();

				ModuloDTO modulo = new ModuloDTO();
				modulo.setIdModulo(0);
				modulo.setNombre(objLista.getSeccionProtocolo());
				modulo.setIdModuloPadre(0);
				modulo.setNombrePadre("");
				modulo.setNumVersion("0");
				modulo.setTipoCambio("");
				modulo.setIdChecklist(0);

				for (ModuloDTO obj : listaSecciones) {

					if (objLista.getSeccionProtocolo().compareTo(obj.getNombre()) == 0) {

						existe = true;
						break;
					}

				}

				if (!existe)
					listaSecciones.add(modulo);
			}
		}

		return listaSecciones;

	}

	public ArrayList<ChecklistLayoutDTO> llenaLista() {

		ArrayList<ChecklistLayoutDTO> lista = new ArrayList<ChecklistLayoutDTO>();

		for (int i = 0; i < 10; i++) {

			ChecklistLayoutDTO obj = new ChecklistLayoutDTO();

			obj.setIdConsecutivo(i);
			obj.setCodigoChecklist(i);
			obj.setNegocio("negocio" + i);
			obj.setNombreProtocolo("nombreProtocolo");

			if (i % 2 == 0)
				obj.setSeccionProtocolo("seccionProtocolo");
			else
				obj.setSeccionProtocolo("seccionProtocolo" + i);

			obj.setSubSeccionProtocolo("subSeccionProtocolo" + i);
			obj.setIdConsecutivo(i);
			obj.setFactorCritico(i);
			obj.setPregunta("pregunta" + i);
			obj.setIdPreguntaPadre(0);
			obj.setTipoRespuesta(21);
			obj.setOpcionesCombo("opcionesCombo");
			obj.setCumplimiento("cumplimiento");
			obj.setPonderacion(i);
			obj.setRequiereComentarios("requiereComentarios");
			obj.setRequiereEvidencia("requiereEvidencia");
			obj.setTipoEvidencia("tipoEvidencia");
			obj.setPlantillaSi("plantillaSi");
			obj.setPlantillaNo("plantillaNo");
			obj.setResponsable1("responsable1" + i);
			obj.setResponsable2("responsable2" + i);
			obj.setZona("zona");
			obj.setDefinicion("definicion" + i);
			obj.setDisciplina("disciplina" + i);
			obj.setSla(i + "");

			lista.add(obj);

		}
		return lista;

	}

	/* LISTA PROTOCOLOS */
	/*
	 * public ArrayList<ModuloDTO>
	 * obtieneListaProtocolos(ArrayList<ChecklistLayoutDTO> listaData) {
	 * 
	 * ArrayList<ModuloDTO> listaProtocolos = new ArrayList<ModuloDTO>();
	 * 
	 * String protocoloActual = ""; boolean existe = false; for (ChecklistLayoutDTO
	 * objLista : listaData) {
	 * 
	 * if (!objLista.getNombreProtocolo().equalsIgnoreCase(protocoloActual)) {
	 * 
	 * // VALIDAR SECCIONES SALTADAS existe = false; protocoloActual =
	 * objLista.getNombreProtocolo();
	 * 
	 * ModuloDTO modulo = new ModuloDTO(); modulo.setIdModulo(0);
	 * modulo.setNombre(objLista.getNombreProtocolo()); modulo.setIdModuloPadre(0);
	 * modulo.setNombrePadre(""); modulo.setNumVersion("0");
	 * modulo.setTipoCambio(""); modulo.setIdChecklist(0);
	 * 
	 * for (ModuloDTO obj : listaProtocolos) {
	 * 
	 * if (objLista.getNombreProtocolo().compareTo(obj.getNombre()) == 0) {
	 * 
	 * existe = true; break; }
	 * 
	 * }
	 * 
	 * if (!existe) listaProtocolos.add(modulo); } }
	 * 
	 * return listaProtocolos;
	 * 
	 * }
	 */
	/* FIN LISTA PROTOCOLOS */

	/* METODOS PARA ALTA PROTOCOLO */
	public ArrayList<ChecklistProtocoloDTO> getListaProtocolos(ArrayList<ChecklistLayoutDTO> listaData,
			int tipoNegocio) {

		// Infraestructura == 1 - Supervisión 2
		ArrayList<ChecklistProtocoloDTO> listaProtocolos = new ArrayList<ChecklistProtocoloDTO>();
		ArrayList<ChecklistLayoutDTO> listaProtocolosData = new ArrayList<ChecklistLayoutDTO>();

		if (tipoNegocio == 1) {
			// Los módulos para EXPANSIÓN(1) son protocolos
			listaProtocolosData = getListaProtocolosExpansion(listaData);
			listaProtocolos = setObjetoProtocolo(listaData, listaProtocolosData, 1);

		} else {
			// Para SUPERVISIÓN (2) los protocolos son la columna protocolo
			listaProtocolosData = getListaProtocolosSupervision(listaData);
			listaProtocolos = setObjetoProtocolo(listaData, listaProtocolosData, 2);

		}

		return listaProtocolos;
	}

	public ArrayList<ChecklistProtocoloDTO> setObjetoProtocolo(ArrayList<ChecklistLayoutDTO> listaData,
			ArrayList<ChecklistLayoutDTO> listaProtocolosData, int tipoNegocio) {

		ArrayList<ChecklistProtocoloDTO> listaProtocolos = new ArrayList<ChecklistProtocoloDTO>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
		String fechaHoy = sdf.format(new Date());

		for (ChecklistLayoutDTO obj : listaProtocolosData) {

			// CONDICIÓN
			String nombreCheck = "";
			String ordenGrupo = "";
			String idUsuario = "";
			double ponderacionTot = 0;
			String clasifica = "";
			String idProtocolo = "0";

			// GENÉRICOS
			String idTipoChecklist = "61";
			String idHorario = "21";
			String vigente = "1";
			String fecha_inicio = fechaHoy;
			String fecha_fin = "";
			String idEstado = "21";
			String dia = "";
			String periodo = "1";
			String version = "1";
			int commit = 1;

			if (tipoNegocio == 1) {

				// EXPANSIÓN
				// Obtenemos de listaData
				nombreCheck = obj.getSeccionProtocolo();
				// **(Infraestrcutura 321 - Aseguramiento 323)**
				ordenGrupo = "321";
				// Se debe obtener de la sesión
				idUsuario = "189871";
				// **LEER EXCEL PARA EXPANSIÓN CAMPO ZONA**
				clasifica = obj.getZona();
				// **LEER EXCEL PARA ASEGURAMIENTO CAMPO PROTOCOLO 0**
				idProtocolo = "0";
				// **CALCULAR CON EXCEL**
				ponderacionTot = getPonderacionProtocolo(listaData, obj, tipoNegocio);

				// GENÉRICOS
				idTipoChecklist = "61";
				idHorario = "21";
				vigente = "1";
				fecha_inicio = fechaHoy;
				fecha_fin = "";
				idEstado = "21";
				dia = "";
				periodo = "1";
				version = "1";
				commit = 1;
				// Genéricos

			} else {
				// SUPERVISIÓN

				// EXPANSIÓN
				// Obtenemos de listaData
				nombreCheck = obj.getNombreProtocolo();
				// **(Infraestrcutura 321 - Aseguramiento 323)**
				ordenGrupo = "323";
				// Se debe obtener de la sesión
				idUsuario = "189871";
				// **LEER EXCEL PARA EXPANSIÓN CAMPO ZONA**
				clasifica = "";
				// **LEER EXCEL PARA ASEGURAMIENTO CAMPO PROTOCOLO 0**
				idProtocolo = "0";
				// **CALCULAR CON EXCEL**
				ponderacionTot = getPonderacionProtocolo(listaData, obj, tipoNegocio);

				// GENÉRICOS
				idTipoChecklist = "61";
				idHorario = "21";
				vigente = "1";
				fecha_inicio = fechaHoy;
				fecha_fin = "";
				idEstado = "21";
				dia = "";
				periodo = "1";
				version = "1";
				commit = 1;
			}

			ChecklistProtocoloDTO checklistProtocolo = new ChecklistProtocoloDTO();
			checklistProtocolo.setIdTipoChecklist(Integer.parseInt(idTipoChecklist));
			checklistProtocolo.setNombreCheck(nombreCheck);
			checklistProtocolo.setIdHorario(Integer.parseInt(idHorario));
			checklistProtocolo.setVigente(Integer.parseInt(vigente));
			checklistProtocolo.setFecha_inicio(fecha_inicio);
			checklistProtocolo.setFecha_fin(fecha_fin);
			checklistProtocolo.setIdEstado(Integer.parseInt(idEstado));
			checklistProtocolo.setIdUsuario(idUsuario);
			checklistProtocolo.setDia(dia);
			checklistProtocolo.setPeriodo(periodo);
			checklistProtocolo.setVersion(version);
			checklistProtocolo.setOrdenGrupo(ordenGrupo);
			checklistProtocolo.setCommit(commit);
			checklistProtocolo.setPonderacionTot(ponderacionTot);
			checklistProtocolo.setClasifica(clasifica);
			checklistProtocolo.setIdProtocolo(idProtocolo);

			listaProtocolos.add(checklistProtocolo);

		}

		return listaProtocolos;

	}

	public double getPonderacionProtocolo(ArrayList<ChecklistLayoutDTO> listaData, ChecklistLayoutDTO protocolo,
			int tipoNegocio) {

		double ponderacion = 0.0;

		try {

			for (ChecklistLayoutDTO obj : listaData) {

				if (tipoNegocio == 1 && (obj.getSeccionProtocolo().compareTo(protocolo.getSeccionProtocolo()) == 0)) {

					ponderacion += obj.getPonderacion();

				} else if (tipoNegocio == 2
						&& (obj.getNombreProtocolo().compareTo(protocolo.getNombreProtocolo()) == 0)) {

					ponderacion += obj.getPonderacion();

				}
			}
		} catch (Exception e) {
			System.out.println("AP getPonderacionProtocolo " + e);
			ponderacion = -1;
		}

		return ponderacion;

	}

	public ArrayList<ChecklistLayoutDTO> getListaProtocolosExpansion(ArrayList<ChecklistLayoutDTO> lista) {

		ArrayList<ChecklistLayoutDTO> listaProtocolos = new ArrayList<ChecklistLayoutDTO>();

		String seccionActual = "";
		boolean existe = false;

		for (ChecklistLayoutDTO objLista : lista) {

			if (!objLista.getSeccionProtocolo().equalsIgnoreCase(seccionActual)) {

				existe = false;
				seccionActual = objLista.getSeccionProtocolo();

				ChecklistLayoutDTO objCheck = objLista;

				for (ChecklistLayoutDTO obj : listaProtocolos) {

					if (objLista.getSeccionProtocolo().compareTo(obj.getSeccionProtocolo()) == 0) {

						existe = true;
						break;
					}

				}

				if (!existe)
					listaProtocolos.add(objCheck);
			}
		}

		return listaProtocolos;

	}

	public ArrayList<ChecklistLayoutDTO> getListaProtocolosSupervision(ArrayList<ChecklistLayoutDTO> lista) {

		ArrayList<ChecklistLayoutDTO> listaProtocolos = new ArrayList<ChecklistLayoutDTO>();

		String seccionActual = "";
		boolean existe = false;

		for (ChecklistLayoutDTO objLista : lista) {

			if (!objLista.getNombreProtocolo().equalsIgnoreCase(seccionActual)) {

				existe = false;
				seccionActual = objLista.getNombreProtocolo();

				ChecklistLayoutDTO objCheck = objLista;

				for (ChecklistLayoutDTO obj : listaProtocolos) {

					if (objLista.getNombreProtocolo().compareTo(obj.getNombreProtocolo()) == 0) {

						existe = true;
						break;
					}

				}

				if (!existe)
					listaProtocolos.add(objCheck);
			}
		}

		return listaProtocolos;

	}
	/* FIN METODOS ALTA PROTOCOLO */

	/* INSERTA CHECKUSUAS */
	public ArrayList<ChecklistLayoutDTO> setInfoLayout(ArrayList<ChecklistLayoutDTO> listaLayout,
			ArrayList<PreguntaDTO> listaPreguntas, ArrayList<ChecklistProtocoloDTO> listaProtocolos, int tipoNegocio) {

		ArrayList<ChecklistLayoutDTO> response = new ArrayList<ChecklistLayoutDTO>();

		// Pregunta necesita idChecklist
		for (ChecklistProtocoloDTO objProtocolo : listaProtocolos) {

			System.out.println("Checklist --- " + objProtocolo.getNombreCheck());

			// SE AGREGA INFO DE PROTOCOLO A OBJETO LAYOUT
			for (int i = 0; i < listaLayout.size(); i++) {

				if (tipoNegocio == 1) {

					if (objProtocolo.getNombreCheck().compareTo(listaLayout.get(i).getSeccionProtocolo()) == 0) {

						listaLayout.get(i).setChecklistProtocoloDTO(objProtocolo);

						response.add(listaLayout.get(i));

					}

				} else {

					if (objProtocolo.getNombreCheck().compareTo(listaLayout.get(i).getNombreProtocolo()) == 0) {

						listaLayout.get(i).setChecklistProtocoloDTO(objProtocolo);

						response.add(listaLayout.get(i));

					}
				}
			}

		}

		// SE AGREGA INFO DE PREGUNTA A OBJETO LAYOUT
		for (PreguntaDTO objPregunta : listaPreguntas) {

			for (int i = 0; i < listaLayout.size(); i++) {


					if (objPregunta.getPregunta().compareTo(listaLayout.get(i).getPregunta()) == 0) {

						listaLayout.get(i).setPreguntaDTO(objPregunta );

						response.add(listaLayout.get(i));

					}
			}
		}

		return response;
	}

	/* FIN INSERTA CHECKUSUAS */

}
