package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.PaisDTO;
import com.gruposalinas.checklist.mappers.PaisRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PaisDAOImpl extends DefaultDAO implements PaisDAO {

    private static Logger logger = LogManager.getLogger(PaisDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtienePais;
    DefaultJdbcCall jdbcObtienePaisVisualizador;
    DefaultJdbcCall jdbcInsertaPais;
    DefaultJdbcCall jdbcActualizaPais;
    DefaultJdbcCall jdbcEliminaPais;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PAIS")
                .withProcedureName("SP_SEL_G_PAIS")
                .returningResultSet("RCL_PAIS", new PaisRowMapper());

        jdbcObtienePais = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PAIS")
                .withProcedureName("SP_SEL_PAIS")
                .returningResultSet("RCL_PAIS", new PaisRowMapper());

        jdbcInsertaPais = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PAIS")
                .withProcedureName("SP_INS_PAIS");

        jdbcActualizaPais = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PAIS")
                .withProcedureName("SP_ACT_PAIS");

        jdbcEliminaPais = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PAIS")
                .withProcedureName("SP_DEL_PAIS");

        jdbcObtienePaisVisualizador = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PAIS")
                .withProcedureName("SP_SEL_V_PAIS")
                .returningResultSet("RCL_PAIS", new PaisRowMapper());

    }

    @SuppressWarnings("unchecked")
    public List<PaisDTO> obtienePais() throws Exception {

        Map<String, Object> out = null;
        List<PaisDTO> listaPais = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funcion ejecutada: {checklist.PA_ADM_PAIS.SP_SEL_G_PAIS}");

        listaPais = (List<PaisDTO>) out.get("RCL_PAIS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Paises");
        } else {
            return listaPais;
        }

        return listaPais;

    }

    @SuppressWarnings({"unchecked", "unused"})
    public List<PaisDTO> obtienePais(int idPais) throws Exception {
        Map<String, Object> out = new HashMap<String, Object>();
        int error = 0;
        List<PaisDTO> listaPais = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PAIS", idPais);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_PAIS.SP_SEL_PAIS}");

        listaPais = (List<PaisDTO>) out.get("RCL_PAIS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Pais con id(" + idPais + ")");
        } else {
            return listaPais;
        }

        return null;
    }

    public int insertaPais(PaisDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idPais = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_ACTIVO", bean.getActivo());

        out = jdbcInsertaPais.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_PAIS.SP_INS_PAIS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_FIID_PAIS");
        idPais = idTipoReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Pais");
        } else {
            return idPais;
        }

        return idPais;
    }

    public boolean actualizaPais(PaisDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PAIS", bean.getIdPais())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_ACTIVO", bean.getActivo());

        out = jdbcActualizaPais.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_PAIS.SP_ACT_PAIS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el Pais  id( " + bean.getIdPais() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaPais(int idPais) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PAIS", idPais);

        out = jdbcEliminaPais.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_PAIS.SP_DEL_PAIS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el Pais id(" + idPais + ")");
        } else {
            return true;
        }

        return false;

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PaisDTO> obtienePaisVisualizador() throws Exception {
        Map<String, Object> out = null;
        List<PaisDTO> listaPais = null;
        int error = 0;

        out = jdbcObtienePaisVisualizador.execute();

        logger.info("Funcion ejecutada: {checklist.PA_ADM_PAIS.SP_SEL_V_PAIS}");

        listaPais = (List<PaisDTO>) out.get("RCL_PAIS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Paises para el Visualizador");
        } else {
            return listaPais;
        }

        return listaPais;
    }

}
