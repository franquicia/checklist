package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.TipoChecklistDTO;


public class TipoChecklistRowMapper3 implements RowMapper<TipoChecklistDTO> {
	
	public TipoChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		TipoChecklistDTO tipoChecklistDTO = new TipoChecklistDTO();
		
		tipoChecklistDTO.setIdTipoCheck(rs.getInt("FIID_TIPO_CHECK"));
		tipoChecklistDTO.setDescTipo(rs.getString("FCDESCRIPCION"));
		
		return tipoChecklistDTO;
	}

}
