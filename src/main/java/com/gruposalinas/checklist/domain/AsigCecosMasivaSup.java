package com.gruposalinas.checklist.domain;

public class AsigCecosMasivaSup {
	private int numSucursal;
	private String nomSucursal;
	private int idEmpleado;
	private String nomEmpleado;
	private String zona;
	private String region;
	private int idGerente;
	private String nomGerente;
	
	public int getNumSucursal() {
		return numSucursal;
	}
	public void setNumSucursal(int numSucursal) {
		this.numSucursal = numSucursal;
	}
	public String getNomSucursal() {
		return nomSucursal;
	}
	public void setNomSucursal(String nomSucursal) {
		this.nomSucursal = nomSucursal;
	}
	public int getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(int idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public String getNomEmpleado() {
		return nomEmpleado;
	}
	public void setNomEmpleado(String nomEmpleado) {
		this.nomEmpleado = nomEmpleado;
	}
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public int getIdGerente() {
		return idGerente;
	}
	public void setIdGerente(int idGerente) {
		this.idGerente = idGerente;
	}
	public String getNomGerente() {
		return nomGerente;
	}
	public void setNomGerente(String nomGerente) {
		this.nomGerente = nomGerente;
	}
	
	@Override
	public String toString() {
		return "AsigCecosMasivaSup [numSucursal=" + numSucursal + ", nomSucursal=" + nomSucursal + ", idEmpleado="
				+ idEmpleado + ", nomEmpleado=" + nomEmpleado + ", zona=" + zona + ", region=" + region
				+ ", numGerente=" + idGerente + ", nomGerente=" + nomGerente + "]";
	}
	
}


