package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ConteoGenExpDTO;

public class ConteoGenExpCheckRowMapper implements RowMapper<ConteoGenExpDTO> {

	public ConteoGenExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConteoGenExpDTO con = new ConteoGenExpDTO();
		
	    con.setIdCheck(rs.getInt("FIID_CHECKLIST"));
	    con.setNomCheck(rs.getString("CHECKLIST"));
	    con.setClasifica(rs.getString("FCCLASIFICA"));
	    con.setPondTotal(rs.getDouble("FCPONDTOTAL"));
	    con.setVersion(rs.getInt("FIVERSION"));
	    con.setNegocio(rs.getString("NEGOCIO"));

		return con;
	}

}


