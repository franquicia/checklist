package com.gruposalinas.checklist.domain;

public class VersionChecklistGralDTO {
	


		private int idCheck;
		private int idVers;
		private int idTab;
		private int idactivo;
		private String nombrecheck;
		private int versionAnt;
		private String aux;
		private String obs;
		private String periodo;

        
		public int getIdCheck() {
			return idCheck;
		}
		public void setIdCheck(int idCheck) {
			this.idCheck = idCheck;
		}
		public int getIdVers() {
			return idVers;
		}
		public void setIdVers(int idVers) {
			this.idVers = idVers;
		}
		public int getIdTab() {
			return idTab;
		}
		public void setIdTab(int idTab) {
			this.idTab = idTab;
		}
		public int getIdactivo() {
			return idactivo;
		}
		public void setIdactivo(int idactivo) {
			this.idactivo = idactivo;
		}
		public String getAux() {
			return aux;
		}
		public void setAux(String aux) {
			this.aux = aux;
		}
		public String getObs() {
			return obs;
		}
		public void setObs(String obs) {
			this.obs = obs;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		
		public String getNombrecheck() {
			return nombrecheck;
		}
		public void setNombrecheck(String nombrecheck) {
			this.nombrecheck = nombrecheck;
		}
		public int getVersionAnt() {
			return versionAnt;
		}
		public void setVersionAnt(int versionAnt) {
			this.versionAnt = versionAnt;
		}
		@Override
		public String toString() {
			return "VersionChecklistGralDTO [idCheck=" + idCheck + ", idVers=" + idVers + ", idTab=" + idTab
					+ ", idactivo=" + idactivo + ", nombrecheck=" + nombrecheck + ", versionAnt=" + versionAnt
					+ ", aux=" + aux + ", obs=" + obs + ", periodo=" + periodo + "]";
		}
		
		
	
	}