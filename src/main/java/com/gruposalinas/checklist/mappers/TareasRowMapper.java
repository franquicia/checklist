package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.TareaDTO;

public class TareasRowMapper implements RowMapper<TareaDTO>{
	
	@Override
	public TareaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		TareaDTO tareaDTO = new TareaDTO();
		
		tareaDTO.setIdTarea(rs.getInt("FIIDTAREA"));
		tareaDTO.setCveTarea(rs.getString("FCCVETAREA"));
		tareaDTO.setStrFechaTarea(rs.getString("FCFECHATAREA"));
		tareaDTO.setActivo(rs.getInt("FIACTIVO"));
		
		return tareaDTO;
	}

}
