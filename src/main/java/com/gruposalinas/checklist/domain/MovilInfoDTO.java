package com.gruposalinas.checklist.domain;

public class MovilInfoDTO {
	
	private int idMovil;
	private int idUsuario;
	private String fecha;
	private String so;
	private String version;
	private String versionApp;
	private String modelo;
	private String fabricante;
	private String numMovil;
	private String tipoCon;
	private String identificador;
	private String token;
	private String nombreU;
	private String idPuesto;
	
	public int getIdMovil() {
		return idMovil;
	}
	public void setIdMovil(int idMovil) {
		this.idMovil = idMovil;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getSo() {
		return so;
	}
	public void setSo(String so) {
		this.so = so;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getFabricante() {
		return fabricante;
	}
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}
	public String getNumMovil() {
		return numMovil;
	}
	public void setNumMovil(String numMovil) {
		this.numMovil = numMovil;
	}
	public String getTipoCon() {
		return tipoCon;
	}
	public void setTipoCon(String tipoCon) {
		this.tipoCon = tipoCon;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getVersionApp() {
		return versionApp;
	}
	public void setVersionApp(String versionApp) {
		this.versionApp = versionApp;
	}
	
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getNombreU() {
		return nombreU;
	}
	public void setNombreU(String nombreU) {
		this.nombreU = nombreU;
	}
	public String getIdPuesto() {
		return idPuesto;
	}
	public void setIdPuesto(String idPuesto) {
		this.idPuesto = idPuesto;
	}
	@Override
	public String toString() {
		return "MovilInfoDTO [idMovil=" + idMovil + ", idUsuario=" + idUsuario + ", fecha=" + fecha + ", so=" + so
				+ ", version=" + version + ", versionApp=" + versionApp + ", modelo=" + modelo + ", fabricante="
				+ fabricante + ", numMovil=" + numMovil + ", tipoCon=" + tipoCon + ", identificador=" + identificador
				+ "]";
	}
	
	
	
}
