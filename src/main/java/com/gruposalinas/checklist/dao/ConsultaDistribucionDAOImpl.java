package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ConsultaDistribucionDTO;
import com.gruposalinas.checklist.mappers.ConsultaEstructuraRowMapper;
import com.gruposalinas.checklist.mappers.ConsultaNivelRowMapper;
import com.gruposalinas.checklist.mappers.ConsultaTerritorioRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ConsultaDistribucionDAOImpl extends DefaultDAO implements ConsultaDistribucionDAO {

    private static Logger logger = LogManager.getLogger(ProtocoloDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTerritorio;
    DefaultJdbcCall jdbcObtieneNivelZona;
    DefaultJdbcCall jdbcObtieneNivelRegion;
    DefaultJdbcCall jdbcObtieneNivelGerente;
    DefaultJdbcCall jdbcObtieneNivelSucursal;
    DefaultJdbcCall jdbcObtieneEstructura;

    public void init() {

        jdbcObtieneTerritorio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACONSULTACECO")
                .withProcedureName("SPGET_TERRITORIO")
                .returningResultSet("PA_CONSULTA", new ConsultaTerritorioRowMapper());

        jdbcObtieneNivelZona = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACONSULTACECO")
                .withProcedureName("SPGETNIVELZONA")
                .returningResultSet("PA_CONSULTA", new ConsultaNivelRowMapper());

        jdbcObtieneNivelRegion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACONSULTACECO")
                .withProcedureName("SPGETNIVELREGI")
                .returningResultSet("PA_CONSULTA", new ConsultaNivelRowMapper());

        jdbcObtieneNivelGerente = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACONSULTACECO")
                .withProcedureName("SPGETNIVELGERE")
                .returningResultSet("PA_CONSULTA", new ConsultaNivelRowMapper());

        jdbcObtieneNivelSucursal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACONSULTACECO")
                .withProcedureName("SPGETNIVELSUCU")
                .returningResultSet("PA_CONSULTA", new ConsultaNivelRowMapper());

        jdbcObtieneEstructura = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACONSULTACECO")
                .withProcedureName("SPGETESTRUCTURA")
                .returningResultSet("PA_CONSULTA", new ConsultaEstructuraRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaDistribucionDTO> ObtieneTerritorio() throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaDistribucionDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource();

        out = jdbcObtieneTerritorio.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADETPROTO.SPDETALLEPRO");

        lista = (List<ConsultaDistribucionDTO>) out.get("PA_CONSULTA");

        if (lista == null) {
            error = 1;
        }

        if (error == 1) {
            logger.info("Algo ocurrió obtener el territorio");
        } else {

            return lista;

        }

        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaDistribucionDTO> ObtieneNivelZona(ConsultaDistribucionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaDistribucionDTO> lista = null;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_TERR", bean.getTerritorio());
        out = jdbcObtieneNivelZona.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PACONSULTACECO.SPGETNIVELZONA");

        lista = (List<ConsultaDistribucionDTO>) out.get("PA_CONSULTA");

        if (lista == null) {
            error = 1;
        }

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el Checkin");
        } else {
            return lista;
        }
        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaDistribucionDTO> ObtieneNivelRegion(ConsultaDistribucionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaDistribucionDTO> lista = null;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_TERR", bean.getTerritorio())
                .addValue("PA_FCID_ZONA", bean.getZona());

        out = jdbcObtieneNivelRegion.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PACONSULTACECO.SPGETNIVELREGI");

        lista = (List<ConsultaDistribucionDTO>) out.get("PA_CONSULTA");

        if (lista == null) {
            error = 1;
        }

        if (error == 1) {
            logger.info("Algo ocurrió al consultar la region");
        } else {
            return lista;
        }
        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaDistribucionDTO> ObtieneNivelGerente(ConsultaDistribucionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaDistribucionDTO> lista = null;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_TERR", bean.getTerritorio())
                .addValue("PA_FCID_ZONA", bean.getZona())
                .addValue("PA_FCID_REGI", bean.getRegion());

        out = jdbcObtieneNivelGerente.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PACONSULTACECO.SPGETNIVELGERE");

        lista = (List<ConsultaDistribucionDTO>) out.get("PA_CONSULTA");

        if (lista == null) {
            error = 1;
        }

        if (error == 1) {
            logger.info("Algo ocurrió al consultar la region");
        } else {
            return lista;
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaDistribucionDTO> ObtieneNivelSucursal(ConsultaDistribucionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaDistribucionDTO> lista = null;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_TERR", bean.getTerritorio())
                .addValue("PA_FCID_ZONA", bean.getZona())
                .addValue("PA_FCID_REGI", bean.getRegion())
                .addValue("PA_FCID_GERE", bean.getGerente());

        out = jdbcObtieneNivelSucursal.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PACONSULTACECO.SPGETNIVELGERE");

        lista = (List<ConsultaDistribucionDTO>) out.get("PA_CONSULTA");

        if (lista == null) {
            error = 1;
        }

        if (error == 1) {
            logger.info("Algo ocurrió al consultar la region");
        } else {
            return lista;
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaDistribucionDTO> ObtieneEstructura(ConsultaDistribucionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaDistribucionDTO> lista = null;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_CECO", bean.getIdCeco());

        out = jdbcObtieneEstructura.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PACONSULTACECO.SPGETNIVELGERE");

        lista = (List<ConsultaDistribucionDTO>) out.get("PA_CONSULTA");

        if (lista == null) {
            error = 1;
        }

        if (error == 1) {
            logger.info("Algo ocurrió al consultar la region");
        } else {
            return lista;
        }
        return lista;

    }

}
