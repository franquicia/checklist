package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.EvidenciaDTO;
import com.gruposalinas.checklist.domain.RespuestaAdDTO;
import com.gruposalinas.checklist.domain.RespuestaDTO;

public interface ArbolRespAdiEviAdmDAO {
	
	public int insertaArbolDecision(ArbolDecisionDTO bean) throws Exception;
	
	public boolean actualizaArbolDecision(ArbolDecisionDTO bean) throws Exception;
	
	public boolean eliminaArbolDecision(int idArbolDecision) throws Exception;
	
	public List<ArbolDecisionDTO> buscaArbolDecision(int idChecklist) throws Exception;
	
	public boolean cambiaRespuestasPorId() throws Exception;
	
	//RESPUESTAS
	
	public List<RespuestaDTO> obtieneRespuesta() throws Exception;
	
	public List<RespuestaDTO> obtieneRespuesta(String idArbol, String idRespuesta, String idBitacora) throws Exception;
	
	public int insertaRespuesta (RespuestaDTO bean) throws Exception;
	
	public boolean insertaUnaRespuesta(int idCheckU, int idBitacora, int idPreg, int idArbol, String obsv, String compromiso, String evidencia, String respAd) throws Exception;

	public boolean insertaRespuestaNueva(int idCheckU, int idBitacora, int idPreg, int idArbol, String obsv, String compromiso, String evidencia, String respAd) throws Exception;
	
	public boolean actualizaRespuesta(RespuestaDTO bean) throws Exception;
	
	public boolean actualizaFechaResp(String idRespuesta, String fechaTermino, String commit) throws Exception;
	
	public boolean eliminaRespuesta (int idRespuesta) throws Exception;
	
	public boolean eliminaRespuestas (String respuestas) throws Exception;
	
	public boolean eliminaRespuestasDuplicadas () throws Exception;
	
	public boolean registraRespuestas(int idCheckUsua, BitacoraDTO bitacora, String idRespuestas, String compromisos, String evidencias)throws Exception;
//ADICIONALES
	public List<RespuestaAdDTO> buscaRespADP(String idRespAD, String idResp) throws Exception;

	public int insertaRespAD(RespuestaAdDTO bean) throws Exception;
	
	public boolean eliminaRespAD(int idRespAD) throws Exception;
	
	public boolean actualizaRespAD(RespuestaAdDTO bean) throws Exception;
	//DEPURA TABLAS		
	public boolean depuraTabs() throws Exception;
	public boolean depuraTabParam(int idCheck) throws Exception;

//EVIDENCIAS
	
	public List<EvidenciaDTO> obtieneEvidencia() throws Exception;
	
	public List<EvidenciaDTO> obtieneEvidencia(int idEvidencia) throws Exception;
	
	public int insertaEvidencia(EvidenciaDTO bean) throws Exception;
	
	public boolean actualizaEvidencia(EvidenciaDTO bean) throws Exception;
	
	public boolean eliminaEvidencia(int idEvidencia) throws Exception;
}
