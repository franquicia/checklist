
function menuPrincipal(){
	location.href = "/checklist/";
}

function linkSistemas(){
	location.href = "/checklist/central/vistaSupervisionSistemas.htm";
}

function linkSoporte(){
	location.href = "/checklist/central/vistaSupervisionSistemasSoporte.htm";
}

function linkImagen(){
	location.href = "/checklist/central/vistaSupervisionSistemasImagen.htm";
}

function linkAsignacion(){
	location.href = "/checklist/central/vistaProgramaVisitas.htm";
}

function linkIncidencias(){
	location.href = "/checklist/central/vistaReporteIncidencias.htm";
}

function linkPortalQa(){
	location.href = "http://10.51.58.252:8080/WsPortalQa/#!/menuPrincipal";
}


function regresar(){
	$(".margenRegresaPrincipal").show();
	$(".margenRegresa").hide();
	
	$(".titFecha").show();
	$(".titChecklist").hide();
	$(".titSucursal").hide();
	$("#paginationdemo").show();
	$(".divSucursal").hide();
	$('#miForm')[0].reset();
	$(".aviso").text('');
}

function resumen(){
	document.getElementById("formActivaAdmin").submit();
	return true;
}

