package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.ConsultaDTO;

public class ConsultaCecoRowMapper implements RowMapper<ConsultaDTO> {

	public ConsultaDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConsultaDTO consultaDTO = new ConsultaDTO();
		
		consultaDTO.setIdCeco(rs.getInt("FCID_CECO"));
		consultaDTO.setNomCeco(rs.getString("FCNOMBRE"));
		
		return consultaDTO;
		
		
	}

}
