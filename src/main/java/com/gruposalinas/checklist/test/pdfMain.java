package com.gruposalinas.checklist.test;

import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.CenterTextMode;
import org.jfree.chart.plot.RingPlot;
import org.jfree.chart.title.TextTitle;
import org.jfree.chart.title.Title;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.HorizontalAlignment;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.VerticalAlignment;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.checklist.business.ChecklistPreguntasComBI;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;
import com.gruposalinas.checklist.domain.ReporteChecksExpDTO;
import com.gruposalinas.checklist.domain.VersionExpanDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.log.SysoCounter;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import freemarker.template.TemplateDateModel;

public class pdfMain {

	public static void main(String[]args) throws DocumentException, IOException, ImageProcessingException, KeyException, GeneralSecurityException {
		
        String urides = "";
        com.gruposalinas.checklist.util.UtilCryptoGS cifra = new com.gruposalinas.checklist.util.UtilCryptoGS();
        
        
       String cadenaString = "ip=10.89.85.167&fecha=30072021171700&uri=http://10.89.85.167:8080/checklist/servicios/postAltaFirmaCheck.json";
       //String cadenaString = "idUsuario=189871&tipoServicio=5";
       //String cadenaString = "{\"USERNAME\":\"BazEktWFauto\",\"MACADDRS\":\"96:05:21:4C:03:31\",\"CLIENTID\":\"4f33d95d44dc460ab5c1234367033977\",\"CALLEDID\":\"EKT-Maqueta\",\"FECHA\":\"2021-07-29 11:45:32\"}";
        System.out.println("CADENA ORIGINAL: "+ cadenaString);

        
        String res1 = cifra.encryptParams(cadenaString, "fce099383332d9ffa34c4f8a50dda9a0");
        System.out.println("CADENA ENCRIPTADA: "+ res1);
        
        urides = (cifra.decryptParams(res1));
        System.out.println("CADENA DESENCRIPTADA: "+ urides);
        
        



		/*
		 URL url = new URL("http://10.53.33.82//franquicia/ImagSucursal/99999517291615062021.jpg");+obj.getRuta()
		InputStream fis =  url.openStream();
		
		InputStream fisData =  url.openStream();
		Metadata metadata = ImageMetadataReader.readMetadata(fisData);

		File fotoPaso = File.createTempFile("fotoPaso", ".jpg");
		BufferedImage imgBuff = ImageIO.read(fis);
        ImageIO.write(imgBuff, "png", fotoPaso);
        
        System.out.println(fotoPaso.getAbsolutePath());

		javaxt.io.Image image = new javaxt.io.Image(fotoPaso.getAbsolutePath());
		File fotoCorrecta = File.createTempFile("fotoFachada", ".jpg");

		String desc = "";
		for (Directory directory : metadata.getDirectories()) {
            for (Tag tag : directory.getTags()) {
                if(tag.getTagName().equals("Orientation")){
                	desc =  tag.getDescription();
                }
            }
         }
		
		switch (desc) {
		
		case "Top, left side (Horizontal / normal)":
			break;
		
		case "Top, right side (Mirror horizontal)":
			break;
			
		case "Bottom, right side (Rotate 180)":
			image.rotate(180);
			break;
			
		case "Bottom, left side (Mirror vertical)":
			image.rotate(-180);
			break;
			
		case "Left side, top (Mirror horizontal and rotate 270 CW)":
			image.rotate(-90);
			break;
			
		case "Right side, top (Rotate 90 CW)":
			image.rotate(90);
			break;
			
		case "Right side, bottom (Mirror horizontal and rotate 90 CW)":
			image.rotate(-270);
			break;
			
		case "Left side, bottom (Rotate 270 CW)":
			image.rotate(270);
			break;
			
		default:
			break;

		}
		BufferedImage imgBuff2 = image.getBufferedImage();
        ImageIO.write(imgBuff2, "png", fotoCorrecta);
    
        fisData.close();
        fisData.close();
        fotoPaso.delete();
        fotoPaso.deleteOnExit();
      
        
        System.out.println(fotoCorrecta.getAbsolutePath());
		*/
        
        
		/*
		URL url = new URL("http://10.53.33.82//franquicia/ImagSucursal/23871209075317062021.jpg");
		InputStream fis =  url.openStream();
		
		try {
			Metadata metadata = ImageMetadataReader.readMetadata(fis);
			
			for (Directory directory : metadata.getDirectories()) {
                for (Tag tag : directory.getTags()) {
                    if(tag.getTagName().equals("Orientation")){
                    	
                    	System.out.println(tag.getDescription());
                    	System.out.println(tag.getTagType());
                    	System.out.println(tag.getTagTypeHex());
                    	System.out.println(tag.getDirectoryName());

                    	
                    }
                }
             }			
			
		} catch (ImageProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		//pdfMain ob = new pdfMain();
		//ob.getLista();
		//ob.sendMailExpansion();
		//ob.grafica();
		
		/*
		List<VersionExpanDTO> listaVersiones = new ArrayList<VersionExpanDTO>();
		
		
		VersionExpanDTO ob1 = new VersionExpanDTO();
		ob1.setIdVers(1);
		ob1.setIdCheck(100);
		
		VersionExpanDTO ob2 = new VersionExpanDTO();
		ob2.setIdVers(1);
		ob2.setIdCheck(200);
		
		VersionExpanDTO ob3 = new VersionExpanDTO();
		ob3.setIdVers(1);
		ob3.setIdCheck(300);
		
		VersionExpanDTO ob4 = new VersionExpanDTO();
		ob4.setIdVers(2);
		ob4.setIdCheck(100);
		
		VersionExpanDTO ob5 = new VersionExpanDTO();
		ob5.setIdVers(2);
		ob5.setIdCheck(200);
		
		VersionExpanDTO ob6 = new VersionExpanDTO();
		ob6.setIdVers(3);
		ob6.setIdCheck(200);
		
		VersionExpanDTO ob7 = new VersionExpanDTO();
		ob7.setIdVers(3);
		ob7.setIdCheck(700);
		
		VersionExpanDTO ob8 = new VersionExpanDTO();
		ob8.setIdVers(4);
		ob8.setIdCheck(10099);
		
		
		listaVersiones.add(ob1);
		listaVersiones.add(ob2);
		listaVersiones.add(ob3);
		listaVersiones.add(ob4);
		listaVersiones.add(ob5);
		listaVersiones.add(ob6);
		listaVersiones.add(ob7);
		listaVersiones.add(ob8);

		  
		  
		//System.out.println(new pdfMain().getMapaVersiones(listaVersiones).toString());
		 * 
		  */
		  
		/*int [] numeros = {1,2,3,4,5,6};
		
		for (int i = 0; i < numeros.length ; i++) {
			
			if (i == 0) {
				System.out.println("<tr>");
			}
		
			if ( (i != 0) && ((i) % 3  == 0)) {
				System.out.println("<tr>");
			}
			
			System.out.println("<td>"+numeros[i]+"</td>");
			
			if ((i+1) % 3  == 0) {
				System.out.println("</tr>");
			}
			
			if (i == numeros.length-1 && (numeros.length % 3 != 0)) {
				System.out.println("</tr>");
			}
			
		}*/
		
	}
	
	File graficaComunes = null;
	File graficaBanco = null;
	File graficaElektra = null;
	File graficaGenerales = null;
	File graficaPresta = null;
	File graficaTotal = null;
	
	public void getLista() {
		ChecklistPreguntasComBI checklistPreguntasComBI = (ChecklistPreguntasComBI) FRQAppContextProvider.getApplicationContext().getBean("checklistPreguntasComBI");
		checklistPreguntasComBI.obtieneInfoCont(26283+"");
		List<ChecklistPreguntasComDTO> listaGrupos = new ArrayList<ChecklistPreguntasComDTO>();		
	}

	public boolean sendMailExpansion() throws DocumentException, IOException {
		String titulo = "ACTA ENTREGA FRANQUICIA RECEPCIÓN DE SUCURSAL";
		String html = "";

		html = creaPDFExpansionNuevoFormato();

		List<String> docHtml = new ArrayList<String>();
		docHtml.add(html);

		File adjunto = escribirPDFHtml(docHtml);
		System.out.println(adjunto.getAbsolutePath());
		
		//Eliminar Gráficas
		graficaComunes.delete();
		graficaBanco.delete();
		graficaElektra.delete();
		graficaGenerales.delete();
		graficaPresta.delete();
		graficaTotal.delete();
				
		//System.out.println("****************RUTA*************** :"+ adjunto.getAbsolutePath() );
		return true;

	}

	public String creaPDFExpansionNuevoFormato() {
	
			//Obtengo Files de Gráfica
			//Valores de PDF
			String operable = "";
			String colorOperable = "";
						
			//Aperturable
				operable = "OPERABLE";
				operable += " (Sin recepción)";
				colorOperable = "#ECE90C";
				String colorTexto = "black";

			//Imperdonables
			String numImperdonables = "2";
			
			DecimalFormat formatDecimales = new DecimalFormat("###.##");
			DecimalFormat formatEnteros = new DecimalFormat("###.##");
			
			String idBitacora = "123456";
	
			//Grafica Cómunes
			double comResSi1 = 5;
			double comResNo1 = 5;			
			double comTotal = 10;
			double comSiPor1 = (comResSi1 / comTotal) * 100;
			double comNoPor1 = (comResNo1 / comTotal) * 100;

			String logoCom = getLogoExpansion(comSiPor1);
			String comResSi = formatEnteros.format(comResSi1);
			String comResNo = formatEnteros.format(comResNo1);
			String comSiPor = formatDecimales.format(comSiPor1);
			String comNoPor = formatDecimales.format(comNoPor1);
			
			graficaComunes = grafica(1,Double.parseDouble(comSiPor),Double.parseDouble(comNoPor));
			
			//Grafica Banco
			double bazResSi1 = 8;
			double bazResNo1 = 2;			
			double bazTotal = 10;
			double bazSiPor1 = (bazResSi1 / bazTotal) * 100;
			double bazNoPor1 = (bazResNo1 / bazTotal) * 100;

			String logoBaz = getLogoExpansion(bazSiPor1);
			String bazResSi = formatEnteros.format(bazResSi1);
			String bazResNo = formatEnteros.format(bazResNo1);
			String bazSiPor = formatDecimales.format(bazSiPor1);
			String bazNoPor = formatDecimales.format(bazNoPor1);
			
			graficaBanco = grafica(1,Double.parseDouble(bazSiPor),Double.parseDouble(bazNoPor));
			
			//Grafica Elektra
			double ektResSi1 = 7;
			double ektResNo1 = 3;			
			double ektTotal = 10;
			double ektSiPor1 = (ektResSi1 / ektTotal) * 100;
			double ektNoPor1 = (ektResNo1 / ektTotal) * 100;

			String logoEkt = getLogoExpansion(ektSiPor1);
			String ektResSi = formatEnteros.format(ektResSi1);
			String ektResNo = formatEnteros.format(ektResNo1);
			String ektSiPor = formatDecimales.format(ektSiPor1);
			String ektNoPor = formatDecimales.format(ektNoPor1);
			
			graficaElektra = grafica(1,Double.parseDouble(ektSiPor),Double.parseDouble(ektNoPor));

			//Grafica Generales
			double genResSi1 = 6;
			double genResNo1 = 4;			
			double genTotal = 10;
			double genSiPor1 = (genResSi1 / genTotal) * 100;
			double genNoPor1 = (genResNo1 / genTotal) * 100;

			String logoGen = getLogoExpansion(genSiPor1);
			String genResSi = formatEnteros.format(genResSi1);
			String genResNo = formatEnteros.format(genResNo1);
			String genSiPor = formatDecimales.format(genSiPor1);
			String genNoPor = formatDecimales.format(genNoPor1);
			
			graficaGenerales = grafica(1,Double.parseDouble(genSiPor),Double.parseDouble(genNoPor));

			//Grafica Presta Prenda
			double prestaResSi1 = 9;
			double prestaResNo1 = 1;			
			double prestaTotal = 10;
			double prestaSiPor1 = (prestaResSi1 / prestaTotal) * 100;
			double prestaNoPor1 = (prestaResNo1 / prestaTotal) * 100;

			String logoPres = getLogoExpansion(prestaSiPor1);
			String prestaResSi = formatEnteros.format(prestaResSi1);
			String prestaResNo = formatEnteros.format(prestaResNo1);
			String prestaSiPor = formatDecimales.format(prestaSiPor1);
			String prestaNoPor = formatDecimales.format(prestaNoPor1);
			
			graficaPresta = grafica(1,Double.parseDouble(prestaSiPor),Double.parseDouble(prestaNoPor));

			//Valores Total
			double totalResNo1 = 50;			
			double totalTotal = 35;
			double totalResSi1 = 15;
			double totalSiPor1 = (totalResSi1 / totalTotal) * 100;
			double totalNoPor1 = (totalResNo1 / totalTotal) * 100;
			//Sup
			double totalValorTotal = 100;
			//Calificación
			double totalCalif1 = (totalSiPor1 * totalValorTotal) / 100;
			String logoTotal = getLogoExpansion(totalSiPor1);
			String totalCalif = formatDecimales.format(totalCalif1);
			String totalResSi = formatEnteros.format(totalResSi1);
			String totalResNo = formatEnteros.format(totalResNo1);
			String totalSiPor = formatDecimales.format(totalSiPor1);
			String totalNoPor = formatDecimales.format(totalNoPor1);
			
						
			//Calificacion y Pre Calificacion
			double preCalificacion = Double.parseDouble(totalCalif);
			double calificacion = 0.0;
			String colorCalificacion = "";
			String colorLetraCalificacion = "white";
			
				calificacion = preCalificacion;
				colorLetraCalificacion = "black";			
			
			graficaTotal = grafica(2,Double.parseDouble(totalSiPor),Double.parseDouble(totalNoPor));
			
			//Calculo de porcentajes por Zona
			
			// Porcentaje en relación al total Comunes
			//Calificación
			double porcentajeComTotal = (comTotal * 100) / totalTotal;			
			double comCalif1 = (comResSi1 * porcentajeComTotal) / comTotal;
			
			comTotal = Double.parseDouble(formatDecimales.format(porcentajeComTotal));
			String comCalif = formatDecimales.format(comCalif1);
			// Fin Porcentaje en relación al total Comunes
			
			// Porcentaje en relación al total BAZ
			double porcentajeBazTotal = (bazTotal * 100) / totalTotal;
			double bazCalif1 = (bazResSi1 * porcentajeBazTotal) / bazTotal;
			
			bazTotal = Double.parseDouble(formatDecimales.format(porcentajeBazTotal));
			String bazCalif = formatDecimales.format(bazCalif1);
			// Fin Porcentaje en relación al total BAZ
			
			// Porcentaje en relación al total EKT
			double porcentajeEktTotal = (ektTotal * 100) / totalTotal;			
			double ektCalif1 = (ektResSi1 * porcentajeEktTotal) / ektTotal;
			
			ektTotal = Double.parseDouble(formatDecimales.format(porcentajeEktTotal));
			String ektCalif = formatDecimales.format(ektCalif1);
			//Fin Porcentaje en relación al total EKT
			
			// Porcentaje en relación al total Generales
			double porcentajeGeneralesTotal = (genTotal * 100) / totalTotal;			
			double genCalif1 = (genResSi1 * porcentajeGeneralesTotal) / genTotal;
			
			genTotal = Double.parseDouble(formatDecimales.format(porcentajeGeneralesTotal));
			String genCalif = formatDecimales.format(genCalif1);
			// Fin Porcentaje en relación al total Generales
			
			// Porcentaje en relación al total PP
			double porcentajePrestaTotal = (prestaTotal * 100) / totalTotal;			
			double prestaCalif1 = (prestaResSi1 * porcentajePrestaTotal) / prestaTotal;
			
			prestaTotal = Double.parseDouble(formatDecimales.format(porcentajePrestaTotal));
			String prestaCalif = formatDecimales.format(prestaCalif1);
			// Fin Porcentaje en relación al total PP
			//Fin Calculo de porcentaje por Zona
			
			//Items
			String totalItems = "100";
			String itemsPorRevisar = totalResNo;
			String itemsAprobados = (Integer.parseInt(totalItems) - Integer.parseInt(itemsPorRevisar)) + "";
			String itemsNoConsiderados = "100";

			totalTotal = 100.00;
			
			String responsables = "";

			for (int i = 0; i < 5; i++) {
				
				responsables += "<tr>" +
						
									"<td style='border: 0 px solid gray;' align='center'>Alejandro Morales Domínguez ff  efrgAlejandro Morales Domínguez ff  efrg" + i + "</td>" +
									"<td style='border: 0 px solid gray;' align='center'>Jose Jorge Sepulveda Maya " + i + "</td>" +
									"<td style='border: 0 px solid gray;' align='center'>Uriel Jaramillo Ugalde " + i + "</td>" +
									"<td style='border: 0 px solid gray;' align='center'>Miguel Angel Delgado Acosta " + i + "</td>" +
								"</tr>";
			}
			
			
			String html = "";
	
			// html = "<!DOCTYPE html>" +
			html = "<html>" + "<head>" + "<title>Acta Entrega</title>" + "</head>"
					+"<style>"
					+"div {color:black;}"
					+"table,td,th {font-size: 9px;}"
					+"a:link {color: white;}" 
					+"a:visited {color: white;}"
					+"a:hover {color: white;}"
					+"a:active {color: white;}"
					+ "td,th {border: 0.25px solid gray;}"
					+"#detalle {text-align:center;}"
					+"#mini {text-color:blue;}"
					+"#mini {font-size: 9px;}" 
					
					+"</style>"
					
					+ "<body>" +
					//Nuevo Formato
	
					//ABRE DIV LOGOS
					"<div>"  +
					"<label>ENTREGA DE SUCURSAL</label>"+
					"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
					"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
					"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
					"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
					"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
					"<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoElektra.png' width='33' height='33' />" +
					 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
					+"<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoBAZ.png' width='45' height='30'/>"+
					"</div>" +
					// CIERRA DIV LOGOS
	
					"<TABLE cellspacing='5' style=' width:100%; height:340px; text-align:center;' >"+
					"<TR>"+
							
					    "<TD ROWSPAN='3' style=' width:25%; border: 0 px solid gray;' > " +
						
						"<div style='width:'200'; height:'150';' >" +
						
						"<div bgcolor='red' style='padding-top:5px; width:100%; background-color:"+ colorOperable +"; color:"+colorTexto+"; font-size: 13px;'> <h3>"+ operable +" </h3></div>"+

						
						 "<img src='" + "/var/folders/k3/wxxyt2816w1986p6tlgdy26c0000gp/T/ej_suc.png" +
						
						"' width='195%' height='111%'/>" + 
												
						"</div>"
						
					    + "</TD>" +
	
					    "<TD colspan='2' style='text-align:left; background-color:#F0EFF0;'>" +
					    
					    	"<div style='float:left; width:50%; margin:auto; padding-left:5px; padding-top:10px; padding-bottom:10px; '>" +
					    	
					     		"<b>Sucursal:</b>" + "Sucursal Nombre de sucursal demasiado" + " <br></br> <b>Zona: </b>"+ "Nombre de la zona que es demasiado" +"<br></br> <b>Territorio: </b>"+ "Nombre del territorio demasiado" +
					    	
					    	"</div>"+
					    	
							"<div style='float:left; width:100%; heigth:200%; padding-left:2px; padding-top:30px; border-left: 1px solid black;'>" +
							
								"<b>No Económico:</b>" + "10029988" +"<br> </br> <b>Tipo Visita: </b>"+"Soft Opening"+"<br></br> <b>Fecha: </b>"+ "29/10/2019"+
							
							"</div>"+
					    
					    
					    "</TD> " +
					    

            			"<td>Ver <br></br>Participantes</td>"+
					    
					    //"<TD style='text-align:left; font-size: 10px; background-color:#F0EFF0;'> <b>No Económico:</b>"+ "10029988" +"<br></br> <b>Tipo Visita: </b>"+"Soft Opening"+"<br></br> <b>Fecha: </b>"+ "29/10/2019" +" </TD> " +
					    
					    
					"</TR>"+
	
					// <b></b>  <br></br> 
					"<TR>"+
						"<TD style='text-align:center; font-size: 12px; height:40px; background-color:red;'><strong style='text-align:center; font-size: 10px;'>Generales de la recepción</strong></TD> " +
						"<TD style='text-align:center; font-size: 12px; height:40px; background-color:red;'> Calificación  <strong style='text-align:center; font-size: 15px;'>"+ preCalificacion +"</strong> </TD> " +

						//+ "<TD style='background-color:"+ colorCalificacion +"; color:"+ colorLetraCalificacion +"'> Calificación: <strong style='text-align:center; font-size: 15px;'>"+ calificacion +"</strong> </TD>"+
						
 						"<td style='text-align:center; font-size: 12px; height:40px; background-color:red;'>Ver <br></br>Participantes</td>"+
 						
					"</TR>"+
	
					"<TR >"+
					"<TD colspan='2' style='text-align:center; background-color:#F0EFF0;'> <p><b>Imperdonables:</b> <strong style='text-align:center; font-size: 15px;'>"+ numImperdonables +"</strong></p> </TD>"
					+ "<TD style='background-color:black;'> <a style='color:white;' href='http://10.51.218.140:8080/checklist/central/detalleChecklistExpancionImperdonable.htm?idUsuario=189871&idBitacora="+idBitacora+"'>Ver Detalles</a></TD>"+
					
					//http://10.51.218.140:8080/checklist/central/detalleChecklistExpancionImperdonable.htm?idBitacora=26282
					"</TR>"+
					
	
				"</TABLE>"+
				
	
				//ITEMS
				"<TABLE style=' width:100%; height:35px; text-align:center'>"+			
					"<TR>"+
					"<TD style='width:25%; text-align:center; font-size: 12px; background-color:#F0EFF0;'> <strong style='text-align:center; font-size: 15px;'>"+ totalItems +"</strong><br></br><br></br> Items Revisados </TD>"+
					"<TD style='width:25%; text-align:center; font-size: 12px; background-color:#F0EFF0;'> <strong style='text-align:center; font-size: 15px;'>"+ itemsNoConsiderados +"</strong><br></br><br></br> Items No Considerados </TD>"+
					"<TD style='width:25%; text-align:center; font-size: 12px; background-color:#D8D8D8;'><strong style='text-align:center; font-size: 15px;'>"+ itemsAprobados +"</strong><br></br> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='20' height='20'/>&nbsp; Items Aprobados </TD>"+
					"<TD style='width:25%; text-align:center; font-size: 12px; background-color:#6B696E; color:white;'><strong style='text-align:center; font-size: 15px;'>"+ itemsPorRevisar  +"</strong><br></br> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='20' height='20'/>&nbsp; Items Por Revisar </TD>"+
					"</TR>"+
				//ITEMS
				"</TABLE>"+
					
				//"<div id='detalle'> Detalles de la Entrega </div>" +
				//Div items
				"<TABLE style='width:100%; text-align:center' cellspacing='5'>"+
				
				" <TR>"
				
 				+"<TD style='width:50%; height:202px; background-color:white;'> " + 				
 
					"<div style='width:100%; height:202px;'>"+
					
						//Titulo
						"<br></br>"+
						"<div>" + "<h5 align='center'>" +

									"<div style='float: left; background-color:red;'>"+
									"<img src='"+ graficaComunes.getAbsolutePath() +"'/>" +
									"</div>"+
						
								"<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaAreasComunes.png' width='25' height=20'/>"+
								"&nbsp; Áreas Comunes"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "<img src='http://10.53.33.82/franquicia/firmaChecklist/"+logoCom+"' width='60' height='25'/>"+
								 "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "Total: "+ comTotal +
						"</h5>" + "</div>" +			
							
							//Div Imagen
							"<div>"+
							"<div style='float: left; width: 200px;'>"+
							"<img src='"+ graficaComunes.getAbsolutePath() +"' width='180px' height='90%' />" +
						"</div>"+
							
							//Mini tabla
							"<div style='float: right; width: 40px; bottom:0; padding-top: 0px;'>"+
							
								"<table cellspacing='0'>"+
								  "<tr  style='color:white; background-color:black;'>"+
								    "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"+
								    "<th style='text-align:center; font-size: 8px;'>items</th>"+
								    "<th style='text-align:center; font-size: 8px;'>items %</th>"+
								    "<th style='text-align:center; font-size: 8px;'>Calificación</th>"+
								  "</tr>"+
								  "<tr>"+
								  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"+
								    "<td>"+ comResSi +"</td>"+
								    "<td>"+ comSiPor +"</td>"+
								    "<td rowspan='2' style='background-color:#F0EFF0;'>"+ comCalif +"</td>"+
								  "</tr>"+
								  "<tr>"+
								  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"+
								  	 "<td>"+ comResNo +"</td>"+
									 "<td>"+ comNoPor +"</td>"+
								  "</tr>"+
								"</table>"+
								  
							"</div>"+
						
						"</div>"+
	
						//"<br></br>"+
						"<a style='color:black;' href='http://10.51.218.140:8080/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="+idBitacora+"&tipo=1'>Ver Detalles</a>"+
					
					" </div>"
	
				 + "</TD>" 
				 
				 +"<TD style='width:50%; height:202px; background-color:white;'> " + 				
				 
					"<div style='width:100%; height:202px;'>"+
	
							//Titulo
							"<br></br>"+
							"<div>" + "<h5 align='center'>" +
							
									"<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaBAZ.png' width='35' height='25'/>"+
									"&nbsp; Banco Azteca"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "<img src='http://10.53.33.82/franquicia/firmaChecklist/"+ logoBaz +"' width='60' height='25'/>"+
									 "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "Total: "+ bazTotal +
							"</h5>" + "</div>" +				
							
							//Div Imagen
							"<div>"+
								"<div style='float: left; width: 200px;'>"+
								"<img src='"+ graficaBanco.getAbsolutePath() +"' width='180px' height='90%' />" +
							"</div>"+
							
								//Mini tabla
								"<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"+
								
									"<table cellspacing='0'>"+
									  "<tr  style='color:white; background-color:black;'>"+
									    "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"+
									    "<th style='text-align:center; font-size: 8px;'>items</th>"+
									    "<th style='text-align:center; font-size: 8px;'>items %</th>"+
									    "<th style='text-align:center; font-size: 8px;'>Calificación</th>"+
									  "</tr>"+
									  "<tr>"+
									  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"+
									  	"<td>"+ bazResSi +"</td>"+
									    "<td>"+ bazSiPor +"</td>"+
									    "<td rowspan='2' style='background-color:#F0EFF0;'>"+ bazCalif +"</td>"+
									  "</tr>"+
									  "<tr>"+
									  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"+
									  	"<td>"+ bazResNo +"</td>"+
									    "<td>"+ bazNoPor +"</td>"+
									  "</tr>"+
									"</table>"+
								  
							"</div>"+
						
						"</div>"+
	
						"<br></br>"+
						"<a style='color:black;' href='http://10.51.218.140:8080/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="+idBitacora+"&tipo=2'>Ver Detalles</a>"+
					
					" </div>"+
				 
				 "</TD> " +
		
	 	 
				 "</TR>"+
				 
				 
				 "<TR>"+
				 
				 "<TD style='width:50%; height:202px; background-color:white;'> " + 				
				  
					"<div style='width:100%; height:202px;'>"+
	
							//Titulo
							"<br></br>"+
							"<div>" + "<h5 align='center'>" +
							
									"<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaElektra.png' width='25' height='25'/>"+
									"&nbsp; Elektra"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "<img src='http://10.53.33.82/franquicia/firmaChecklist/"+ logoEkt +"' width='60' height='25'/>"+
									 "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "Total: "+ ektTotal +
							"</h5>" + "</div>" +				
							
							//Div Imagen
							"<div>"+
								"<div style='float: left; width: 200px;'>"+
								"<img src='"+ graficaElektra.getAbsolutePath() +"' width='180px' height='90%' />" +
							"</div>"+
							
								//Mini tabla
								"<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"+
								
									"<table cellspacing='0'>"+
									  "<tr  style='color:white; background-color:black;'>"+
									    "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"+
									    "<th style='text-align:center; font-size: 8px;'>items</th>"+
									    "<th style='text-align:center; font-size: 8px;'>items %</th>"+
									    "<th style='text-align:center; font-size: 8px;'>Calificación</th>"+
									  "</tr>"+
									  "<tr>"+
									  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"+
									  	"<td>"+ ektResSi +"</td>"+
									    "<td>"+ ektSiPor +"</td>"+
									    "<td rowspan='2' style='background-color:#F0EFF0;'>"+ ektCalif +"</td>"+
									  "</tr>"+
									  "<tr>"+
									  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"+
									  	"<td>"+ ektResNo +"</td>"+
									    "<td>"+ ektNoPor +"</td>"+
									  "</tr>"+
									"</table>"+
								  
							"</div>"+
						
						"</div>"+
	
						"<br></br>"+
						"<a style='color:black;' href='http://10.51.218.140:8080/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="+idBitacora+"&tipo=3'>Ver Detalles</a>"+
					
					" </div>"+
				  
				  "</TD> "+
				  
				 "<TD style='width:50%; height:202px; background-color:white;'> " + 				
				  
						"<div style='width:100%; height:202px;'>"+
	
							//Titulo
							"<br></br>"+
							"<div>" + "<h5 align='center'>" +
							
									"<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaGenerales.png' width='25' height='25'/>"+
									"&nbsp; Generales"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "<img src='http://10.53.33.82/franquicia/firmaChecklist/"+ logoGen +"' width='60' height='25'/>"+
									 "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "Total: "+ genTotal +
							"</h5>" + "</div>" +				
							
							//Div Imagen
							"<div>"+
								"<div style='float: left; width: 200px;'>"+
								"<img src='"+ graficaGenerales.getAbsolutePath() +"' width='180px' height='90%' />" +
							"</div>"+
							
								//Mini tabla
								"<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"+
								
									"<table cellspacing='0'>"+
									  "<tr  style='color:white; background-color:black;'>"+
									    "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"+
									    "<th style='text-align:center; font-size: 8px;'>items</th>"+
									    "<th style='text-align:center; font-size: 8px;'>items %</th>"+
									    "<th style='text-align:center; font-size: 8px;'>Calificación</th>"+
									  "</tr>"+
									  "<tr>"+
									  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"+
									  	"<td>"+ genResSi +"</td>"+
									    "<td>"+ genSiPor +"</td>"+
									    "<td rowspan='2' style='background-color:#F0EFF0;'>"+ genCalif +"</td>"+
									  "</tr>"+
									  "<tr>"+
									  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"+
									  	"<td>"+ genResNo +"</td>"+
									    "<td>"+ genNoPor +"</td>"+
									  "</tr>"+
									"</table>"+
								  
							"</div>"+
						
						"</div>"+
	
						"<br></br>"+
						"<a style='color:black;' href='http://10.51.218.140:8080/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="+idBitacora+"&tipo=4'>Ver Detalles</a>"+
					
					" </div>"+
				  
				  "</TD>" +
				  
				 "</TR>"+
	
				 
	 			"<TR>"+
	 
				 "<TD style='width:50%; height:202px; background-color:white;'> " + 				
				  
						"<div style='width:100%; height:202px;'>"+
	
							//Titulo
							"<br></br>"+
							"<div>" + "<h5 align='center'>" +
							
									"<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaPrestaPrenda.png' width='35' height='25'/>"+
									"&nbsp; Presta Prenda"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "<img src='http://10.53.33.82/franquicia/firmaChecklist/"+ logoPres +"' width='60' height='25'/>"+
									 "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "Total: "+ prestaTotal +
							"</h5>" + "</div>" +				
							
							//Div Imagen
							"<div>"+
								"<div style='float: left; width: 200px;'>"+
								"<img src='"+ graficaPresta.getAbsolutePath() +"' width='180px' height='90%' />" +
							"</div>"+
							
								//Mini tabla
								"<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"+
								
									"<table cellspacing='0'>"+
									  "<tr  style='color:white; background-color:black;'>"+
									    "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"+
									    "<th style='text-align:center; font-size: 8px;'>items</th>"+
									    "<th style='text-align:center; font-size: 8px;'>items %</th>"+
									    "<th style='text-align:center; font-size: 8px;'>Calificación</th>"+
									  "</tr>"+
									  "<tr>"+
									  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"+
									  	"<td>"+ prestaResSi +"</td>"+
									    "<td>"+ prestaSiPor +"</td>"+
									    "<td rowspan='2' style='background-color:#F0EFF0;'>"+ prestaCalif +"</td>"+
									  "</tr>"+
									  "<tr>"+
									  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"+
									  	"<td>"+ prestaResNo +"</td>"+
									    "<td>"+ prestaNoPor +"</td>"+
									  "</tr>"+
									"</table>"+
								  
							"</div>"+
						
						"</div>"+
	
						"<br></br>"+
						"<a style='color:black;' href='http://10.51.218.140:8080/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="+idBitacora+"&tipo=5'>Ver Detalles</a>"+
					
					" </div>"+
				  
				    "</TD> "
				  
				 +"<TD style='width:50%; height:202px; background-color:#F0EFF0;'> " +
				  
	"					<div style='width:100%; height:202px;'>"+
	
						//Titulo
						"<br></br>"+
						"<div>" + "<h5 align='center'>" +
						
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "TOTAL"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "<img src='http://10.53.33.82/franquicia/firmaChecklist/"+ logoTotal +"' width='60' height='25'/>"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "Total: "+ totalTotal +
								 
								 
						"</h5>" + "</div>" +				
							
							//Div Imagen
							"<div>"+
								"<div style='float: left; width: 200px;'>"+
								"<img src='"+ graficaTotal.getAbsolutePath() +"' width='180px' height='90%' />" +
							"</div>"+
							
								//Mini tabla
								"<div style='float: right; width: 80px; bottom:0; padding-top: 50px; '>"+
								
									"<table cellspacing='0'>"+
									  "<tr  style='color:white; background-color:black;'>"+
									    "<th style='color:white; background-color:#F0EFF0; border: 0 px solid #F0EFF'></th>"+
									    "<th style='text-align:center; font-size: 8px;'>items</th>"+
									    "<th style='text-align:center; font-size: 8px;'>items %</th>"+
									    "<th style='text-align:center; font-size: 8px;'>Calificación</th>"+
									  "</tr>"+
									  "<tr>"+
									  	"<td style='color:white; background-color:#F0EFF0; border: 0 px solid #F0EFF'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"+
									  	"<td>"+ totalResSi +"</td>"+
									    "<td>"+ totalSiPor +"</td>"+
									    "<td rowspan='2' style='background-color:#F0EFF0;'>"+ totalCalif +"</td>"+
									  "</tr>"+
									  "<tr>"+
									  	"<td style='color:white; background-color:#F0EFF0; border: 0 px solid #F0EFF'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"+
									  	"<td>"+ totalResNo +"</td>"+
									    "<td>"+ totalNoPor +"</td>"+
									  "</tr>"+
									"</table>"+
								  
							"</div>"+
						
						"</div>"+
	
						"<br></br>"+
						"<a style='color:black;' href='http://10.51.218.140:8080/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="+idBitacora+"&tipo=6'>Ver Detalles</a>"+
					
					" </div>"+
				  		 
				 "</TD>" +
				  
				 "</TR>"+
				 
				 
				"</TABLE>"+
				
					"<table cellspacing='5' align='right'>"+
					  "<tr align='center'>"+
					    "<td style=' border: 0.25px solid white;'>Código</td>"+
					    "<td style=' border: 0.25px solid white; text-align:center; background-color:black; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>pésimo</td>"+
					    "<td style='border: 0.25px solid white;  text-align:center; background-color:#FE003D; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>en peligro</td>"+
					    "<td style='border: 0.25px solid white;  text-align:center; background-color:#F7CA44; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>alerta</td>"+
					    "<td style='border: 0.25px solid white;  text-align:center; background-color:#006240; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>bien</td>"+
					  "</tr>"+
					"</table>"+
					  
				"<div>Detalles de Responsables de Obra</div>" +
				
				
						"<table cellspacing='0' align='center' style='text-align:center; width:100%; height:300px; background-color:#F0EFF0; border: 0 px solid gray;'>" + 
							"<thead>" + 
								"<tr>" +
									"<th style='border: 0 px solid gray;'><b>Área</b></th>" + 
									"<th style='border: 0 px solid gray;' ><b>Responsable</b></th>" + 
									"<th style='border: 0 px solid gray;' ><b>Teléfono</b></th>" + 
									"<th style='border: 0 px solid gray;' ><b>Organización</b></th>" + 
								"</tr>" + 
							"</thead>" + 
								
								"<tbody>"+
								
								responsables +
								
								"</tbody>" + 
								
						"</table>"+
				
					"</body>"+
					
					"</html>";
	
			return html;
		}


	public File escribirPDFHtml(List<String> html) {
		File temp = null;
		try {
			// create a temp file
			temp = File.createTempFile("compromisos", ".pdf");
			FileOutputStream file = new FileOutputStream(temp);
			Document document = new Document();
			PdfWriter writer = PdfWriter.getInstance(document, file);
			document.open();

			java.util.Iterator<String> it = html.iterator();

			while (it.hasNext()) {
				String k = it.next();
				XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
				ByteArrayInputStream is = new ByteArrayInputStream(k.getBytes());
				worker.parseXHtml(writer, document, is);
				document.newPage();
			}
			document.close();
			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return temp;
	}
	 
	public File grafica(int tipo , double aprobados, double porAtender) {
		//Tipo 1 Backround Blanco - Tipo 2 Backroung Gris
		Color gris = new Color(240,239,240);

	    DefaultPieDataset dataset = new DefaultPieDataset();
	    dataset.setValue("Items Aprobados", aprobados);
	    dataset.setValue("Items Por Atender", porAtender);
		
		RingPlot plot = new RingPlot(dataset);
		
		Color vrd = new Color(227,227,227);
		Color rojo = new Color(142,142,142);
			
		plot.setSectionPaint("Items Aprobados", vrd);
		plot.setSectionPaint("Items Por Atender", rojo);

		JFreeChart chart = new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, plot, true);

		//Fondo de la Gráfica
		if (tipo == 1) {
			chart.getPlot().setBackgroundPaint( Color.white );
		} else {
			chart.getPlot().setBackgroundPaint( gris );
		}

		//plot.setCenterTextMode(CenterTextMode.VALUE);
		Font font1 = new Font(null, Font.BOLD,45);
		plot.setCenterTextMode(CenterTextMode.FIXED);
		plot.setCenterText(aprobados+"%");

		plot.setCenterTextFont(font1);
		plot.setCenterTextColor(Color.BLACK);
		
		plot.setOutlineVisible(false);
		plot.setSectionDepth(0.35);
		//plot.setSectionOutlinesVisible(true);
		//plot.setSimpleLabels(true);
		plot.setOuterSeparatorExtension(0);
		plot.setInnerSeparatorExtension(0);

		Font font=new Font("FONT",0,22);
		plot.setLabelFont(font);

		chart.getLegend().setFrame(BlockBorder.NONE);
		//chart.getLegend().setPosition(RectangleEdge.);
		
		//FONDO DE LA GRÁFICA
		if (tipo == 1) {
			chart.setBackgroundPaint(java.awt.Color.white);
			plot.setLabelBackgroundPaint(Color.white);
		} else {
			chart.setBackgroundPaint(gris);
			plot.setLabelBackgroundPaint(gris);

		}

		return setGraficaTemp(chart);
    }
	
	public File setGraficaTemp(JFreeChart chart){
		
		File temp = null;
			// create a temp file
			try {
				temp = File.createTempFile("grafica", ".png");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			BufferedImage chartImage = chart.createBufferedImage( 600, 400, null); 
		    
		    try {
				ImageIO.write( chartImage, "png", temp );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 	
		return temp;
		
	}
	
	public String getLogoExpansion(double num) {

		if (num >= 90) {
			
			return "vt03.png";
			
		} else if (num >= 70) {
			
			return "vt04.png";
			
		} else if (num >= 51) {
			
			return "vt02.png"; 
			
		}else {
			
			return "vt01.png";
			
		}
		
	}
	
	public HashMap<String,List<VersionExpanDTO>> getMapaVersiones(List<VersionExpanDTO> listaVersiones) {
		
		HashMap<String,List<VersionExpanDTO>> mapaVersiones = new HashMap<String,List<VersionExpanDTO>>();
		List<VersionExpanDTO> grupo = new ArrayList<VersionExpanDTO>();


		  int actual = 0;
		  int siguiente = 0;
		  		  
		  
		  for (int i = 0; i < listaVersiones.size(); i++) {

			  actual = listaVersiones.get(i).getIdVers();
			  			  
			  if (i+1 < listaVersiones.size()) {
				  siguiente = listaVersiones.get(i+1).getIdVers();
			  }  
			  			  
			  grupo.add(listaVersiones.get(i));
			  
			  if (actual != siguiente) {
				  mapaVersiones.put(""+actual, grupo);
				  grupo = new ArrayList<VersionExpanDTO>();
			  } 
			  
			  if (i+1 == listaVersiones.size()) {
				  mapaVersiones.put(""+actual, grupo);
			  }
			  
		}
		  
		  return mapaVersiones;
	}

}
