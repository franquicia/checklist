package com.gruposalinas.checklist.domain;

public class ActorEdoHallaDTO {
	


		private int idActorConfig;
		private String nombreActor;
		private String observacion;
		private String aux;
		private int status;
		private String periodo;
		private int idEdoHallazgo;
		private String nombStatus;
		private int idConfiguracion;
		private int idPerfil;
		private String color;
		
		public int getIdActorConfig() {
			return idActorConfig;
		}
		public void setIdActorConfig(int idActorConfig) {
			this.idActorConfig = idActorConfig;
		}
		public String getNombreActor() {
			return nombreActor;
		}
		public void setNombreActor(String nombreActor) {
			this.nombreActor = nombreActor;
		}
		public String getObservacion() {
			return observacion;
		}
		public void setObservacion(String observacion) {
			this.observacion = observacion;
		}
		public String getAux() {
			return aux;
		}
		public void setAux(String aux) {
			this.aux = aux;
		}
		public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		public int getIdEdoHallazgo() {
			return idEdoHallazgo;
		}
		public void setIdEdoHallazgo(int idEdoHallazgo) {
			this.idEdoHallazgo = idEdoHallazgo;
		}
		public String getNombStatus() {
			return nombStatus;
		}
		public void setNombStatus(String nombStatus) {
			this.nombStatus = nombStatus;
		}
		public int getIdConfiguracion() {
			return idConfiguracion;
		}
		public void setIdConfiguracion(int idConfiguracion) {
			this.idConfiguracion = idConfiguracion;
		}
		
		public int getIdPerfil() {
			return idPerfil;
		}
		public void setIdPerfil(int idPerfil) {
			this.idPerfil = idPerfil;
		}
		
		public String getColor() {
			return color;
		}
		public void setColor(String color) {
			this.color = color;
		}
		@Override
		public String toString() {
			return "ActorEdoHallaDTO [idActorConfig=" + idActorConfig + ", nombreActor=" + nombreActor
					+ ", observacion=" + observacion + ", aux=" + aux + ", status=" + status + ", periodo=" + periodo
					+ ", idEdoHallazgo=" + idEdoHallazgo + ", nombStatus=" + nombStatus + ", idConfiguracion="
					+ idConfiguracion + ", idPerfil=" + idPerfil + ", color=" + color + "]";
		}
		
	
		
	}