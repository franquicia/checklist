package com.gruposalinas.checklist.domain;

public class DistribCecoDTO {
	private int ejecucion;
	private String ceco;
	private String nombre;
	private String territorio;
	private String zona;
	private String region;
	private String gerente;
	private String usuario;
	private String fecha;

	public int getEjecucion() {
		return ejecucion;
	}

	public void setEjecucion(int ejecucion) { 
		this.ejecucion = ejecucion;
	}
	
	
	

	public String getCeco() {
		return ceco;
	}

	public void setCeco(String ceco) {
		this.ceco = ceco;
	}
	
	

	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTerritorio() {
		return territorio;
	}

	public void setTerritorio(String territorio) {
		this.territorio = territorio;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getGerente() {
		return gerente;
	}

	public void setGerente(String gerente) {
		this.gerente = gerente;
	}
	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString() {
		return "DistribCecoDTO [ejecucion=" + ejecucion + ", ceco=" + ceco + ", nombre=" + nombre + ", territorio="
				+ territorio + ", zona=" + zona + ", region=" + region + ", gerente=" + gerente + ", usuario=" + usuario
				+ ", fecha=" + fecha + "]";
	}
	
	

	
}
