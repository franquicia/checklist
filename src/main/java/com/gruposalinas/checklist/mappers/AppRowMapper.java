package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.AppPerfilDTO;


public class AppRowMapper implements RowMapper<AppPerfilDTO>{


	public AppPerfilDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		AppPerfilDTO app = new AppPerfilDTO();
		
		app.setIdApp(rs.getInt("FIID_APP"));
		app.setDescripcion(rs.getString("FCDESCRIPCION"));
		
		return app;
	}
	

}
