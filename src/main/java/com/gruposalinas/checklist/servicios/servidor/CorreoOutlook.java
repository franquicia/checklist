/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.servicios.servidor;

/**
 *
 * @author leodan1991
 */
import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public class CorreoOutlook {

    //@Autowired
    //private CecoBI cecoBI;
    private String https_url_pro = "https://service.onuriscp.com/WebServicecorreo/Administracion.asmx?WSDL";

    public String validaUsuarioLotusCorreos(String idUsuario) throws IOException {
        if (idUsuario == null) {
            return "";
        }
        String correo = "";
        HttpURLConnection rc = null;
        BufferedReader read = null;
        OutputStream outStr = null;
        //logger.info("El numero de empleado que pasamos" +idUsuario); 

        try {

            //crear la llamada al servidor
            //HttpsClient secureClient = new HttpsClient() ;
            //logger.info("CORREOS LOTUS");
            URL url = new URL(https_url_pro);
            /*Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.50.8.20", 80));
                                   
                                Authenticator authenticator = new Authenticator() {

                                    public PasswordAuthentication getPasswordAuthentication() {
            
                                        return (new PasswordAuthentication("bancoazteca\\B196228",
                                                "Gruposalinas2345".toCharArray()));
                                    }
    
                                };
    
                                Authenticator.setDefault(authenticator);                                
				rc = (HttpURLConnection) url.openConnection(proxy);
             */

            rc = (HttpURLConnection) url.openConnection();

            //rc = secureClient.testIt(https_url_pro);		
            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Accept-Charset", "UTF-8");
            rc.setRequestProperty("Content-Type", "text/xml");  //application/soap+xml
            //logger.info("CORREOS LOTUS 2");

            String reqStr
                    = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                    + "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                    + "  <soap:Body>\n"
                    + "    <Get_Mail xmlns=\"https://dev.azteca-comunicaciones.com.pe/\">\n"
                    + "    	\n"
                    + "    	<user>196228</user>\n"
                    + "    	<pass>bGVvU2lzdGVtYXMxMjM0NTY3QA==</pass>\n"
                    + "    	<Empleados>" + idUsuario + "</Empleados>\n"
                    + "      \n"
                    + "    </Get_Mail>\n"
                    + "  </soap:Body>\n"
                    + "</soap:Envelope>";

            //several more definitions to request
            int len = reqStr.length();
            rc.setRequestProperty("Content-Length", Integer.toString(len));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.setConnectTimeout(5000);
            //logger.info("CORREOS LOTUS 3");
            rc.connect();
            //logger.info("CORREOS LOTUS 4");

            outStr = rc.getOutputStream();
            outStr.write(reqStr.getBytes("UTF-8"));
            outStr.flush();
            //logger.info("CORREOS LOTUS 5");

            read = null;
            try {
                read = new BufferedReader(new InputStreamReader(rc.getInputStream()));

            } catch (Exception exception) {

                //if something wrong instead of the output, read the error
                read = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
                //error.setMsj(read.toString());
                // logger.info("error1"+exception.getMessage());
                return "Algo paso: " + exception.getMessage();
                //  objJson = format(error);
            } finally {

                if (outStr != null) {
                    outStr.close();
                }
            }

            //read server response			
            String line;

            while ((line = read.readLine()) != null) {
                System.out.println(line);

                if (line.contains("MAIL")) {
                    String[] tags = line.split("MAIL");
                    correo = tags[1].replace("&lt;/", "").replace("&gt;", "");

                }

            }
        } catch (Exception e) {
            //e.printStackTrace();
            //logger.info("Ocurrio algo... "+e.getMessage());
            return "Ocurrio algo... " + e.getMessage() + e.getStackTrace() + e.getCause() + e.hashCode() + e.toString() + e.getLocalizedMessage();
            //return validaLlave;
        } finally {
            if (read != null) {
                read.close();
            }
            if (rc != null) {
                rc.disconnect();
            }
            if (outStr != null) {
                outStr.close();
            }

            rc = null;
            read = null;
            outStr = null;
        }
        return correo;
    }

    public String validaCorreoSuc(String idCeco) {
        String correoSuc = "";                
        String eco = "";

        /*try {
            //cecoBi= new CecoBI();
            CecoBI cecoBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
            List<CecoDTO> cecos = cecoBI.buscaCeco(Integer.parseInt(idCeco));
            if (cecos != null && cecos.size() > 0) {
                CecoDTO ceco = cecos.get(0);
                System.out.println("CECO:\n"+ceco.toString());
                int canal = ceco.getIdCanal();
                switch (canal) {

                    case 21:
                    case 25:
                        eco = "G" + ceco.getIdCeco().substring(2);
                        break;
                    case 26:
                        eco = "PP" + ceco.getIdCeco().substring(2);
                        break;
                    default:
                        eco = "G" + ceco.getIdCeco().substring(2);
                        break;

                }


            }
        } catch (Exception e) {
            //e.printStackTrace();
            eco="G"+idCeco.substring(2);

        }*/
                          
        eco="G"+idCeco.substring(2);
          
        correoSuc = eco + "@elektra.com.mx";


        return correoSuc;
    }
    
    
}
