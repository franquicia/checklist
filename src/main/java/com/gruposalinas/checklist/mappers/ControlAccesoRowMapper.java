package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.DetalleProtoDTO;

public class ControlAccesoRowMapper implements RowMapper<DetalleProtoDTO>{

public DetalleProtoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		DetalleProtoDTO detalleProtoDTO = new DetalleProtoDTO();

		detalleProtoDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		detalleProtoDTO.setIdUsuario(rs.getInt("FCID_USUARIO"));
		detalleProtoDTO.setIdCeco(rs.getString("FCID_CECO"));
		detalleProtoDTO.setNomCeco(rs.getString("FCNOMBRE"));
		detalleProtoDTO.setFdfecha(rs.getString("FDFECHA"));
		detalleProtoDTO.setFcLatitud(rs.getString("FCLATITUD"));
		detalleProtoDTO.setFcLongitud(rs.getString("FCLONGITUD"));
		detalleProtoDTO.setBandera(rs.getInt("FCBANDERA"));
		detalleProtoDTO.setPlataforma(rs.getString("PLATAFORMA"));
		detalleProtoDTO.setTiempoConexion(rs.getInt("TIEMPO_CONEXION"));
		
		
		;
		
		return detalleProtoDTO;
	}

}
