package com.gruposalinas.checklist.business;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.TruncTableTokenDAO;



public class TruncTableTokenBI {
	
	private Logger logger = LogManager.getLogger(TruncTableTokenBI.class);
	
	@Autowired
	TruncTableTokenDAO truncTableTokenDAO;
	
	
	public boolean truncTable(){
		boolean respuesta = false;
		
		try {
			respuesta = truncTableTokenDAO.truncTable();
		} catch (Exception e) {
			logger.info("No fue posible truncar la tabla FRANQUICIA.FRTATOKEN (truncTable)");
				
		}
		
		return respuesta;
	}
	

}
