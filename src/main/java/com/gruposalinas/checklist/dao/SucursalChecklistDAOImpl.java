package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.SucursalChecklistDTO;
import com.gruposalinas.checklist.mappers.SucCecoFaseProyectoRowMapper;
import com.gruposalinas.checklist.mappers.SucChecklistRowMapper;
import com.gruposalinas.checklist.mappers.SucImagenExpRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class SucursalChecklistDAOImpl extends DefaultDAO implements SucursalChecklistDAO {

    private static Logger logger = LogManager.getLogger(SucursalChecklistDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneImagSuc;
    DefaultJdbcCall jdbcObtieneImagSucG;
    DefaultJdbcCall jdbcInsertaImagSuc;
    DefaultJdbcCall jdbcEliminaImagSuc;
    DefaultJdbcCall jbdcActualizaImagSuc;
    DefaultJdbcCall jdbcObtieneSuc;
    DefaultJdbcCall jdbcObtieneSucCecoFaseProyecto;

    public void init() {

        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADCONSSUC")
                .withProcedureName("SP_CONSULTA")
                .returningResultSet("RCL_APERTURA", new SucChecklistRowMapper())
                .returningResultSet("RCL_TRANSF", new SucChecklistRowMapper())
                .returningResultSet("RCL_CUARTEL", new SucChecklistRowMapper());

        jdbcInsertaImagSuc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADSUCCHK")
                .withProcedureName("SP_INSERTAIMG");

        jdbcEliminaImagSuc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADSUCCHK")
                .withProcedureName("SP_ELIMINAIMG");

        jbdcActualizaImagSuc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADSUCCHK")
                .withProcedureName("SP_ACTUALIZAIMG");

        jdbcObtieneImagSuc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADSUCCHK")
                .withProcedureName("SP_CONSULTAIMG")
                .returningResultSet("RCL_CONSULTA", new SucImagenExpRowMapper());

        jdbcObtieneImagSucG = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADSUCCHK")
                .withProcedureName("SP_CONSULDETIMG")
                .returningResultSet("RCL_CONSULTA", new SucImagenExpRowMapper());

        jdbcObtieneSuc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADCONSSUC")
                .withProcedureName("SP_SUCU")
                .returningResultSet("RCL_SUC", new SucChecklistRowMapper());

        jdbcObtieneSucCecoFaseProyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEXPNVOFLUJO")
                .withProcedureName("SP_SUCU")
                .returningResultSet("RCL_SUC", new SucCecoFaseProyectoRowMapper());

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<SucursalChecklistDTO> listaAper = null;
        List<SucursalChecklistDTO> listatrans = null;
        List<SucursalChecklistDTO> listcuartel = null;

        int error = 0;

        out = jdbcObtieneTodo.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADCONSSUC.SP_CONSULTA}");

        listaAper = (List<SucursalChecklistDTO>) out.get("RCL_APERTURA");
        listatrans = (List<SucursalChecklistDTO>) out.get("RCL_TRANSF");
        listcuartel = (List<SucursalChecklistDTO>) out.get("RCL_CUARTEL");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            throw new Exception("Algo ocurrió en el Store Procedure");
        }

        lista = new HashMap<String, Object>();
        lista.put("listaAper", listaAper);
        lista.put("listatrans", listatrans);
        lista.put("listcuartel", listcuartel);

        return lista;
    }
    //parametro

    @Override
    public int inserta(SucursalChecklistDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NUSUCURSAL", bean.getIdSucursal())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_OBSERV", bean.getObserv());

        out = jdbcInsertaImagSuc.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADSUCCHK.SP_INSERTAIMG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDIMAG");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar la imagen de la sucursal");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    @Override
    public boolean elimina(String idSucursal) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDIMAG", idSucursal);

        out = jdbcEliminaImagSuc.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADSUCCHK.SP_ELIMINAIMG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el la imagen de la suc(" + idSucursal + ")");
        } else {
            return true;
        }

        return false;

    }

    @Override
    public boolean actualiza(SucursalChecklistDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDIMAG", bean.getIdImag())
                .addValue("PA_NUSUCURSAL", bean.getIdSucursal())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_OBSERV", bean.getObserv());

        out = jbdcActualizaImagSuc.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADSUCCHK.SP_ACTUALIZAIMG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id de la suc( " + bean.getIdSucursal() + ")");
        } else {
            return true;
        }
        return false;

    }

    @Override
    @SuppressWarnings("unchecked")
    public List<SucursalChecklistDTO> obtieneDatos(int idSucursal) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        List<SucursalChecklistDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", idSucursal);

        out = jdbcObtieneImagSuc.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADSUCCHK.SP_CONSULTAIMG}");
        //lleva el nombre del cursor del procedure
        lista = (List<SucursalChecklistDTO>) out.get("RCL_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la imagen de la suc(" + idSucursal + ")");
        }
        return lista;

    }

    @Override
//sin parametro
    @SuppressWarnings("unchecked")
    public List<SucursalChecklistDTO> obtieneInfoG() throws Exception {
        Map<String, Object> out = null;
        List<SucursalChecklistDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneImagSucG.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADSUCCHK.SP_CONSULDETIMG}");

        listaFijo = (List<SucursalChecklistDTO>) out.get("RCL_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los  el detalle de la imagen suc");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    @SuppressWarnings("unchecked")
    public List<SucursalChecklistDTO> obtieneDatosSuc(String ceco) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<SucursalChecklistDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco);

        out = jdbcObtieneSuc.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADSUCCHK.SP_CONSULTAIMG}");
        //lleva el nombre del cursor del procedure
        lista = (List<SucursalChecklistDTO>) out.get("RCL_SUC");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la imagen de la suc(" + ceco + ")");
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<SucursalChecklistDTO> obtieneDatosSucCecoFaseProyecto(String ceco, String fase, String proyecto) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<SucursalChecklistDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto);

        out = jdbcObtieneSucCecoFaseProyecto.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAEXPNVOFLUJO.SP_SUCU}");
        //lleva el nombre del cursor del procedure
        lista = (List<SucursalChecklistDTO>) out.get("RCL_SUC");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener DATOS de la suc(" + ceco + ")");
        }
        return lista;

    }

}
