package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.BitacoraAdministradorDTO;

public interface BitacoraAdministradorDAO {
	
	public int insertaBitacora(BitacoraAdministradorDTO bean)throws Exception;
	
	public boolean actualizaBitacora(BitacoraAdministradorDTO bean)throws Exception;
	
	public boolean eliminaBitacora(int idBitacora  )throws Exception;
	
	public List<BitacoraAdministradorDTO> buscaBitacoraCompleto(BitacoraAdministradorDTO bean)throws Exception;
	
	public List<BitacoraAdministradorDTO> buscaBitacorasPorCampo(int idCheckUsua, int idBitacora, String fechaInicio, String fechaFin)throws Exception;
	
}
