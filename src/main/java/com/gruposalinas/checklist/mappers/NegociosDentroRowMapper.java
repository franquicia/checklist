package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.NegociosDentroDTO;
import com.gruposalinas.checklist.domain.PuntosCercanosDTO;
import com.gruposalinas.checklist.domain.SucursalesCercanasDTO;

public class NegociosDentroRowMapper implements RowMapper<NegociosDentroDTO> {

	@Override
	public NegociosDentroDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		NegociosDentroDTO sucursalesCercanasDTO = new NegociosDentroDTO();
		
		sucursalesCercanasDTO.setCeco(rs.getString("CENTRO_COSTO"));
		sucursalesCercanasDTO.setNombreCeco(rs.getString("NOMBRE"));
		sucursalesCercanasDTO.setCanal(rs.getInt("CANAL"));
		sucursalesCercanasDTO.setNegocio(rs.getInt("NEGOCIO"));
		
		return sucursalesCercanasDTO;
	}
	
	

}
