package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;

import com.gruposalinas.checklist.dao.VersionCheckAdmDAO;
import com.gruposalinas.checklist.domain.VersionExpanDTO;

public class VersionCheckAdmBI {

	private static Logger logger = LogManager.getLogger(VersionCheckAdmBI.class);

private List<VersionExpanDTO> listafila;
	
	@Autowired
	VersionCheckAdmDAO versionCheckAdmDAO;
		
	
	public int inserta(VersionExpanDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = versionCheckAdmDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean elimina(String idTab){
		boolean respuesta = false;
		
		try {
			respuesta = versionCheckAdmDAO.elimina(idTab);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	
	
	public List<VersionExpanDTO> obtieneDatos(String idVers) {

		try {
			listafila = versionCheckAdmDAO.obtieneDatos(idVers);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}
	


	
	public boolean actualiza(VersionExpanDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = versionCheckAdmDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public boolean actualiza2(VersionExpanDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = versionCheckAdmDAO.actualiza2(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	//NEGOCIO VERSION
	
	public int insertaNegoVer(VersionExpanDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = versionCheckAdmDAO.insertaNegoVer(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaNegoVer(String idTab){
		boolean respuesta = false;
		
		try {
			respuesta = versionCheckAdmDAO.eliminaNegoVer(idTab);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	
	
	public List<VersionExpanDTO> obtieneDatosNegoVer(String idVers) {

		try {
			listafila = versionCheckAdmDAO.obtieneDatosNegoVer(idVers);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}
		
	public boolean actualizaNegoVer(VersionExpanDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = versionCheckAdmDAO.actualizaNegoVer(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public boolean cargaVesionesTab(String protocolo){
		boolean respuesta = false;
		
		try {
			respuesta = versionCheckAdmDAO.cargaVesionesTab(protocolo);
		} catch (Exception e) {
			logger.info("No fue posible cargar: "+e.getMessage());
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	public boolean cargaVesionesTabProd(String protocolo){
		boolean respuesta = false;
		
		try {
			respuesta = versionCheckAdmDAO.cargaVesionesTabProd(protocolo);
		} catch (Exception e) {
			logger.info("No fue posible cargar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

}