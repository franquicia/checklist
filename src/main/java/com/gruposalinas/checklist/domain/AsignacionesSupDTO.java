package com.gruposalinas.checklist.domain;

public class AsignacionesSupDTO {
	
	private int idUsuario;
	private String usuaModificado;
	private int idPerfil;
	private String idCeco;
	private int idGerente;
	private int activo;
	private String fechaIniAsigna;
	private String fechaFinAsigna;
	private String fechaModificacion;
	
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdPerfil() {
		return idPerfil;
	}
	public void setIdPerfil(int idPerfil) {
		this.idPerfil = idPerfil;
	}
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public int getIdGerente() {
		return idGerente;
	}
	public void setIdGerente(int idGerente) {
		this.idGerente = idGerente;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	public String getFechaIniAsigna() {
		return fechaIniAsigna;
	}
	public void setFechaIniAsigna(String fechaIniAsigna) {
		this.fechaIniAsigna = fechaIniAsigna;
	}
	public String getFechaFinAsigna() {
		return fechaFinAsigna;
	}
	public void setFechaFinAsigna(String fechaFinAsigna) {
		this.fechaFinAsigna = fechaFinAsigna;
	}
	public String getUsuaModificado() {
		return usuaModificado;
	}
	public void setUsuaModificado(String usuaModificado) {
		this.usuaModificado = usuaModificado;
	}
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	@Override
	public String toString() {
		return "AsignacionesSupDTO [idUsuario=" + idUsuario + ", usuaModificado=" + usuaModificado + ", idPerfil=" + idPerfil
				+ ", idCeco=" + idCeco + ", idGerente=" + idGerente + ", activo=" + activo + ", fechaIniAsigna="
				+ fechaIniAsigna + ", fechaFinAsigna=" + fechaFinAsigna + ", fechaModificacion=" + fechaModificacion
				+ "]";
	}
		
}


