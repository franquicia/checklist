package com.gruposalinas.checklist.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class UtilXML {
	
	public static StringBuilder getResponse(String url, String xmlRequest , String soapAction) {
		
		StringBuilder xmlString = new StringBuilder() ;
		
		OutputStreamWriter wr = null;
		BufferedReader rd = null;
		HttpURLConnection con = null;
		
		try {
			URL urlConnection = new URL(url);
			con = (HttpURLConnection) urlConnection.openConnection();
			
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestProperty("Content-Type", "text/xml; charset=utf-8");		
			con.setRequestProperty("Content-Length", Integer.toString(xmlRequest.length()));
			con.setRequestProperty("Connection", "Keep-Alive");
			con.setRequestProperty("SOAPAction", soapAction);
						
			con.connect();
			
			//System.out.println("Peticion al servicio: "+xmlRequest);
			
			OutputStream outputStream = con.getOutputStream();
			try {
				wr = new OutputStreamWriter(outputStream);
				wr.write(xmlRequest,0,xmlRequest.length());
				wr.flush();
				
			}finally {
				wr.close();
				wr = null;
			}
			
			
			
			//InputStream inputStream = con.getInputStream(); 
			try {
				rd = new BufferedReader(new InputStreamReader(con.getInputStream()));			
			}catch(Exception e) {
				rd = new BufferedReader(new InputStreamReader(con.getErrorStream()));
			}
			
			String line;
			while((line = rd.readLine()) != null) {
				xmlString.append(line);
			}
			
			//inputStream.close();
			//inputStream = null;
			
			
		}catch(Exception e) {
			xmlString = null;
			
			e.printStackTrace();
		}
		
		
		if(xmlString != null) {
			//System.out.println("Respuesta del servicio SOA: \n"+xmlString.toString());
		}else{
			//System.out.println("Respuesta del servicio SOA: \n");
		}
		
		
		return xmlString;
	}
	

}
