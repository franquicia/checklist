package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ReporteDAO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.CompromisoCecoDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.domain.PuestoDTO;
import com.gruposalinas.checklist.domain.ReporteDTO;
import com.gruposalinas.checklist.domain.VistaCDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class ReporteBI {

	private static final Logger logger = LogManager.getLogger(ReporteBI.class);

	@Autowired
	ReporteDAO reporteDAO;

	List<ReporteDTO> listaReporte = null;
	List<CompromisoDTO> listaCompromisos = null;
	List<VistaCDTO> listaCeco = null;
	List<CompromisoCecoDTO> listaCeco1 = null;
	Map<String, Object> mapReporteGeneral = null;
	Map<String, Object> listaReporteReg = null;
	Map<String, Object> listaReporteVistaC = null;

	public List<ReporteDTO> buscaReporte(String idCheck, String idUsuario, String nombreUsuario, String nombreSucursal,
			String fechaRespuesta, String nombreCheck, String metodoCheck) throws Exception {

		if (idCheck.equals("NULL"))
			idCheck = null;
		if (idUsuario.equals("NULL"))
			idUsuario = null;
		if (nombreUsuario.equals("NULL"))
			nombreUsuario = null;
		if (nombreSucursal.equals("NULL"))
			nombreSucursal = null;
		if (fechaRespuesta.equals("NULL"))
			fechaRespuesta = null;
		if (nombreCheck.equals("NULL"))
			nombreCheck = null;
		if (metodoCheck.equals("NULL"))
			metodoCheck = null;

		listaReporte = reporteDAO.buscaReporte(idCheck, idUsuario, nombreUsuario, nombreSucursal, fechaRespuesta,
				nombreCheck, metodoCheck);

		return listaReporte;
	}

	public Map<String, Object> buscaReporteURL(String idCategoria, String idTipo, String idPuesto, String idUsuario,
			String idCeco, String idSucursal, int idNivel, String fecha, String idPais) throws Exception {

		Map<String, Object> mapReporte = null;
		List<ReporteDTO> listaReporte = null;
		List<ReporteDTO> listaAux = new ArrayList<ReporteDTO>();

		listaReporte = reporteDAO.buscaReporteURL(idCategoria, idTipo, idPuesto, idUsuario, idCeco, idSucursal, idNivel,
				fecha, idPais);

		if (!listaReporte.isEmpty()) {
			// //System.out.println("listaReporte " + listaReporte);
			mapReporte = new HashMap<String, Object>();
			for (int i = 0; i < listaReporte.size(); i++) {
				if (i + 1 == listaReporte.size()) {
					listaAux.add(listaReporte.get(i));
					mapReporte.put("" + listaReporte.get(i).getNombreGeografia(), listaAux);
				} else {
					if (listaReporte.get(i).getIdGeografia() != listaReporte.get(i + 1).getIdGeografia()) {
						listaAux.add(listaReporte.get(i));
						mapReporte.put("" + listaReporte.get(i).getNombreGeografia(), listaAux);
						listaAux = new ArrayList<ReporteDTO>();
					} else {
						listaAux.add(listaReporte.get(i));
					}
				}
			}
			// //System.out.println("mapReporte despues del for " + mapReporte);
			if (mapReporte.isEmpty() && (!listaReporte.isEmpty())) {
				mapReporte.put("" + listaReporte.get(0).getNombreGeografia(), listaReporte);
			}
		} else {
			mapReporte = null;
			//logger.info("Saliendo del BI " + mapReporte);
		}
		// logger.info("Saliendo del BI " + mapReporte);
		return mapReporte;
	}

	public List<ReporteDTO> ComparaImagen(int idRuta, int idPregunta, int idCheckUsuario, String fecha)
			throws Exception {
		try {
			listaReporte = reporteDAO.ComparaImagen(idRuta, idPregunta, idCheckUsuario, fecha);
		} catch (Exception e) {
			logger.info("No fue posible consultar las Imagenes" + e);
		}

		return listaReporte;
	}

	public List<ReporteDTO> ReporteRegional(String fechaIni, String fechaFin) throws Exception {
		try {
			listaReporte = reporteDAO.ReporteRegional(fechaIni, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporte;
	}

	public Map<String, Object> ReporteGeneral(int idChecklist, String fechaInicio, String fechaFin) throws Exception {
		try {
			mapReporteGeneral = reporteDAO.ReporteGeneral(idChecklist, fechaInicio, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return mapReporteGeneral;
	}

	public Map<String, Object> ReporteGeneralCompromisos(int idChecklist, String fechaInicio, String fechaFin)
			throws Exception {
		try {
			mapReporteGeneral = reporteDAO.ReporteGeneralCompromisos(idChecklist, fechaInicio, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return mapReporteGeneral;
	}
	
	public Map<String, Object> ReporteGeneralProtocolos(int idChecklist, String fechaInicio, String fechaFin)
			throws Exception {
		try {
			mapReporteGeneral = reporteDAO.ReporteProtocolos(idChecklist, fechaInicio, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return mapReporteGeneral;
	}

	public List<ReporteDTO> ReporteCheckUsua(String idCheck, String idPuesto) throws Exception {
		try {
			listaReporte = reporteDAO.ReporteCheckUsua(idCheck, idPuesto);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporte;
	}

	public List<ReporteDTO> ReporteSupReg(String nivelTerritorio, String nivelZona, String nivelRegion, String region,
			String idCecoSup, String pais, String negocio, String fechaI, String fechaF, String idChecklist)
			throws Exception {
		try {
			listaReporte = reporteDAO.ReporteSupReg(nivelTerritorio, nivelZona, nivelRegion, region, idCecoSup, pais,
					negocio, fechaI, fechaF, idChecklist);
			logger.info("lista de reporte... " + listaReporte);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporte;
	}

	public Map<String, Object> ReporteDetSupReg(int idChecklist, String idCeco, String pais, String negocio,
			String fechaI, String fechaF) throws Exception {
		try {
			listaReporteReg = reporteDAO.ReporteDetSupReg(idChecklist, idCeco, pais, negocio, fechaI, fechaF);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporteReg;
	}

	public Map<String, Object> ReporteDetRegional(int idBitacora) throws Exception {
		try {
			listaReporteReg = reporteDAO.ReporteDetRegional(idBitacora);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporteReg;
	}

	public Map<String, Object> ReporteDetVC(int idCeco, int idChecklist, int idUsuario, String fecha) throws Exception {
		try {
			listaReporteReg = reporteDAO.ReporteDetVC(idCeco, idChecklist, idUsuario, fecha);
		} catch (Exception e) {
			listaReporteReg = null;
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporteReg;
	}

	public Map<String, Object> ReporteDetSupReg_versBorra(int idChecklist, String idCeco, String pais, String negocio,
			String fechaI, String fechaF) throws Exception {
		try {
			listaReporteReg = reporteDAO.ReporteDetSupReg_versBorra(idChecklist, idCeco, pais, negocio, fechaI, fechaF);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporteReg;
	}

	public Map<String, Object> ReporteOperacion(int idChecklist, String fechaInicio, String fechaFin, String pais,
			String negocio, String territorio, String zona, String region, String ceco) throws Exception {
		Map<String, Object> listaReporteOpe = null;
		try {
			listaReporteOpe = reporteDAO.ReporteOperacion(idChecklist, fechaInicio, fechaFin, pais, negocio, territorio,
					zona, region, ceco);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporteOpe;

	}

	public List<ReporteDTO> ReporteRegSup(int idCheck, int idPregunta, String fechaI, String fechaFin, String pais,
			String negocio, String nivelRegion, String idCeco) throws Exception {
		try {
			listaReporte = reporteDAO.ReporteRegSup(idCheck, idPregunta, fechaI, fechaFin, pais, negocio, nivelRegion,
					idCeco);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporte;

	}

	public List<CompromisoDTO> ReporteDetPreguntasRegional(int idBitacora) throws Exception {
		try {
			listaCompromisos = reporteDAO.ReporteDetPreguntasRegional(idBitacora);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaCompromisos;

	}

	public List<ReporteDTO> ReporteDetReg(int idCheck, int idCeco, String fechaIni, String fechaFin) throws Exception {
		try {
			listaReporte = reporteDAO.ReporteDetReg(idCheck, idCeco, fechaIni, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporte;

	}

	public List<VistaCDTO> ReporteVistaC(int idUsuario, int idCeco, int idChecklist, int pais, int canal, String fecha,
			int bandera) throws Exception {
		try {
			listaCeco = reporteDAO.ReporteVistaC(idUsuario, idCeco, idChecklist, pais, canal, fecha, bandera);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaCeco;

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> ReporteVistaCumplimiento(int idUsuario, String idCeco, int pais, int canal, String fecha,
			int bandera) throws Exception {
		Map<String, Object> mapaCheck = null;
		try {
			mapaCheck = new HashMap<String, Object>();
			List<VistaCDTO> listaReporteCum = new ArrayList<VistaCDTO>();
			List<VistaCDTO> listaReporteCumOrdenada = new ArrayList<VistaCDTO>();
			List<VistaCDTO> listaReporteCumOrdenadaAux = null;
			List<List> listaAuxReporteCum = new ArrayList<List>();
			List<List> listaAuxReporteCumOrdenada = new ArrayList<List>();
			List<ChecklistDTO> listaChecklist = null;

			logger.info(
					"listaReporteVistaC " + idUsuario + "   " + pais + "   " + canal + "   " + fecha + "   " + bandera);
			listaReporteVistaC = reporteDAO.ReporteVistaCumplimiento(idUsuario, idCeco, pais, canal, fecha);
			logger.info("listaReporteVistaC " + listaReporteVistaC);

			if (listaReporteVistaC != null) {
				int porcentaje = (Integer) listaReporteVistaC.get("porcentaje");
				int ceco = (Integer) listaReporteVistaC.get("ceco");
				listaChecklist = (List<ChecklistDTO>) listaReporteVistaC.get("listaCheck");
				int conteo = (Integer) listaReporteVistaC.get("conteo");

				logger.info("ceco " + ceco);
				if (!listaChecklist.isEmpty()) {
					for (ChecklistDTO check : listaChecklist) {
						logger.info("check.getIdChecklist() " + check.getIdChecklist());
						listaAuxReporteCum.add(reporteDAO.ReporteVistaC(idUsuario, ceco, check.getIdChecklist(), pais,
								canal, fecha, bandera));
					}

					if (listaAuxReporteCum.size() > 0) {
						for (int i = 0; i < listaAuxReporteCum.get(0).size(); i++) {
							listaReporteCumOrdenadaAux = new ArrayList<VistaCDTO>();
							for (int j = 0; j < listaAuxReporteCum.size(); j++) {
								/*
								 * VistaCDTO vista= new VistaCDTO();
								 * vista=(VistaCDTO)
								 * listaAuxReporteCum.get(j).get(i);
								 * logger.info("listaSucursales: "+ vista)
								 */
								listaReporteCum.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
								listaReporteCumOrdenadaAux.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
							}
							listaAuxReporteCumOrdenada.add(listaReporteCumOrdenadaAux);
						}
						if(listaAuxReporteCum.get(0).size()>1)
							listaReporteCumOrdenada = ordena(listaAuxReporteCum.get(0).size(), listaAuxReporteCumOrdenada);
						else
							listaReporteCumOrdenada=listaReporteCum;
					}
					mapaCheck.put("listaChecklist", listaChecklist);
					mapaCheck.put("listaSucursales", listaReporteCumOrdenada);
					mapaCheck.put("porcentaje", porcentaje);
					mapaCheck.put("conteo", conteo);
				} else {
					mapaCheck = null;
				}

			}

		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
			mapaCheck = null;
		}

		return mapaCheck;
	}

	//private List<List> listaGlobal = new ArrayList<List>();


	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public Map<String, Object> ReporteVistaCumplimientoSucursal(int idUsuario, String idCeco, int pais, int canal,
			String fecha, int bandera) throws Exception {
		
		/*
		String id[]={"236738","236736"};
		Map<String, Object> mapaCheck = null;
		List<ChecklistDTO> listaChecklist = null;
		List<VistaCDTO> listaReporteCumOrdenada = null;
		int porcentaje = 0;
		int conteo = 0;
		
		for (int z = 0; z < 2; z++) {			
			try {
				mapaCheck = new HashMap<String, Object>();
				List<VistaCDTO> listaReporteCum = new ArrayList<VistaCDTO>();
				listaReporteCumOrdenada = new ArrayList<VistaCDTO>();
				List<VistaCDTO> listaReporteCumOrdenadaAux = null;
				List<List> listaAuxReporteCum = new ArrayList<List>();
				List<List> listaAuxReporteCumOrdenada = new ArrayList<List>();

				logger.info("listaReporteVistaC " + idUsuario + "   " + pais + "   " + canal + "   " + fecha);
				listaReporteVistaC = reporteDAO.ReporteVistaCumplimiento(idUsuario, id[z], pais, canal, fecha);
				logger.info("listaReporteVistaC " + listaReporteVistaC);

				if (listaReporteVistaC != null) {
					porcentaje = (Integer) listaReporteVistaC.get("porcentaje");
					int ceco = (Integer) listaReporteVistaC.get("ceco");
					listaChecklist = (List<ChecklistDTO>) listaReporteVistaC.get("listaCheck");
					conteo = (Integer) listaReporteVistaC.get("conteo");
					logger.info("idCeco " + id[z] + "   bandera " + bandera);

					if (!listaChecklist.isEmpty()) {
						for (ChecklistDTO check : listaChecklist) {
							logger.info("check.getIdChecklist() " + check.getIdChecklist());
							listaAuxReporteCum.add(reporteDAO.ReporteVistaC(idUsuario, Integer.parseInt(id[z]),
									check.getIdChecklist(), pais, canal, fecha, bandera));
						}
						if (listaAuxReporteCum.size() > 0) {
							for (int i = 0; i < listaAuxReporteCum.get(0).size(); i++) {
								listaReporteCumOrdenadaAux = new ArrayList<VistaCDTO>();
								for (int j = 0; j < listaAuxReporteCum.size(); j++) {
									//listaReporteCum.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
									listaReporteCum.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
									listaReporteCumOrdenadaAux.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
								}
								listaAuxReporteCumOrdenada.add(listaReporteCumOrdenadaAux);	
								listaGlobal.add(listaReporteCumOrdenadaAux);	
							}
							if(listaAuxReporteCum.get(0).size()>1)
								listaReporteCumOrdenada = ordena(listaAuxReporteCum.get(0).size(), listaGlobal);
							else
								listaReporteCumOrdenada=listaReporteCum;
						}

					} else {
						listaAuxReporteCum.add(reporteDAO.ReporteVistaC(idUsuario, Integer.parseInt(id[z]), 0, pais, canal,
								fecha, bandera));
						if (listaAuxReporteCum.size() > 0) {
							for (int i = 0; i < listaAuxReporteCum.get(0).size(); i++) {
								for (int j = 0; j < listaAuxReporteCum.size(); j++) {
									//listaReporteCum.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
									listaReporteCumOrdenada.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));								
								}
							}
						}
					}
					
				}

			} catch (Exception e) {
				logger.info("No fue posible consultar la información" + e);
				mapaCheck = null;
			}
		}
		
		mapaCheck.put("listaChecklist", listaChecklist);
		mapaCheck.put("listaSucursales", listaReporteCumOrdenada);
		mapaCheck.put("porcentaje", porcentaje);
		mapaCheck.put("conteo", conteo);
		
		*/
		
		Map<String, Object> mapaCheck = null;
		try {
			mapaCheck = new HashMap<String, Object>();
			List<VistaCDTO> listaReporteCum = new ArrayList<VistaCDTO>();
			List<VistaCDTO> listaReporteCumOrdenada = new ArrayList<VistaCDTO>();
			List<VistaCDTO> listaReporteCumOrdenadaAux = null;
			List<List> listaAuxReporteCum = new ArrayList<List>();
			List<List> listaAuxReporteCumOrdenada = new ArrayList<List>();
			List<ChecklistDTO> listaChecklist = null;

			logger.info("listaReporteVistaC " + idUsuario + "   " + pais + "   " + canal + "   " + fecha);
			listaReporteVistaC = reporteDAO.ReporteVistaCumplimiento(idUsuario, idCeco, pais, canal, fecha);
			logger.info("listaReporteVistaC " + listaReporteVistaC);

			if (listaReporteVistaC != null) {
				int porcentaje = (Integer) listaReporteVistaC.get("porcentaje");
				int ceco = (Integer) listaReporteVistaC.get("ceco");
				listaChecklist = (List<ChecklistDTO>) listaReporteVistaC.get("listaCheck");
				int conteo = (Integer) listaReporteVistaC.get("conteo");
				logger.info("idCeco " + idCeco + "   bandera " + bandera);

				if (!listaChecklist.isEmpty()) {
					for (ChecklistDTO check : listaChecklist) {
						logger.info("check.getIdChecklist() " + check.getIdChecklist());
						listaAuxReporteCum.add(reporteDAO.ReporteVistaC(idUsuario, Integer.parseInt(idCeco),
								check.getIdChecklist(), pais, canal, fecha, bandera));
					}
					if (listaAuxReporteCum.size() > 0) {
						for (int i = 0; i < listaAuxReporteCum.get(0).size(); i++) {
							listaReporteCumOrdenadaAux = new ArrayList<VistaCDTO>();
							for (int j = 0; j < listaAuxReporteCum.size(); j++) {
								//listaReporteCum.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
								listaReporteCum.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
								listaReporteCumOrdenadaAux.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
							}
							listaAuxReporteCumOrdenada.add(listaReporteCumOrdenadaAux);							
						}
						if(listaAuxReporteCum.get(0).size()>1)
							listaReporteCumOrdenada = ordena(listaAuxReporteCum.get(0).size(), listaAuxReporteCumOrdenada);
						else
							listaReporteCumOrdenada=listaReporteCum;
					}

				} else {
					listaAuxReporteCum.add(reporteDAO.ReporteVistaC(idUsuario, Integer.parseInt(idCeco), 0, pais, canal,
							fecha, bandera));
					if (listaAuxReporteCum.size() > 0) {
						for (int i = 0; i < listaAuxReporteCum.get(0).size(); i++) {
							for (int j = 0; j < listaAuxReporteCum.size(); j++) {
								//listaReporteCum.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
								listaReporteCumOrdenada.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));								
							}
						}
					}
				}
				mapaCheck.put("listaChecklist", listaChecklist);
				mapaCheck.put("listaSucursales", listaReporteCumOrdenada);
				mapaCheck.put("porcentaje", porcentaje);
				mapaCheck.put("conteo", conteo);
			}

		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
			mapaCheck = null;
		}
		 
		return mapaCheck;
	}
	
	/*____________PILA__________*/
	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public Map<String, Object> ReporteVistaCumplimientoSucursalPila(int idUsuario, String idCeco, int pais, int canal,
			String fecha, int bandera) throws Exception {
		
		Map<String, Object> mapaCheck = null;
		try {
			mapaCheck = new HashMap<String, Object>();
			List<VistaCDTO> listaReporteCum = new ArrayList<VistaCDTO>();
			List<VistaCDTO> listaReporteCumOrdenada = new ArrayList<VistaCDTO>();
			List<VistaCDTO> listaReporteCumOrdenadaAux = null;
			List<List> listaAuxReporteCum = new ArrayList<List>();
			List<List> listaAuxReporteCumOrdenada = new ArrayList<List>();
			List<ChecklistDTO> listaChecklist = null;

			logger.info("listaReporteVistaC " + idUsuario + "   " + pais + "   " + canal + "   " + fecha);
			listaReporteVistaC = reporteDAO.ReporteVistaCumplimiento(idUsuario, idCeco, pais, canal, fecha);
			logger.info("listaReporteVistaC " + listaReporteVistaC);

			if (listaReporteVistaC != null) {
				int porcentaje = (Integer) listaReporteVistaC.get("porcentaje");
				int ceco = (Integer) listaReporteVistaC.get("ceco");
				listaChecklist = (List<ChecklistDTO>) listaReporteVistaC.get("listaCheck");
				int conteo = (Integer) listaReporteVistaC.get("conteo");
				logger.info("idCeco " + idCeco + "   bandera " + bandera);

				if (!listaChecklist.isEmpty()) {
					for (ChecklistDTO check : listaChecklist) {
						logger.info("check.getIdChecklist() " + check.getIdChecklist());
						listaAuxReporteCum.add(reporteDAO.ReporteVistaC(idUsuario, Integer.parseInt(idCeco),
								check.getIdChecklist(), pais, canal, fecha, bandera));
					}
					if (listaAuxReporteCum.size() > 0) {
						for (int i = 0; i < listaAuxReporteCum.get(0).size(); i++) {
							listaReporteCumOrdenadaAux = new ArrayList<VistaCDTO>();
							for (int j = 0; j < listaAuxReporteCum.size(); j++) {
								//listaReporteCum.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
								listaReporteCum.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
								listaReporteCumOrdenadaAux.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
							}
							listaAuxReporteCumOrdenada.add(listaReporteCumOrdenadaAux);							
						}
						if(listaAuxReporteCum.get(0).size()>1)
							listaReporteCumOrdenada = ordenaPila(listaAuxReporteCum.get(0).size(), listaAuxReporteCumOrdenada);
						else
							listaReporteCumOrdenada=listaReporteCum;
					}

				} else {
					listaAuxReporteCum.add(reporteDAO.ReporteVistaC(idUsuario, Integer.parseInt(idCeco), 0, pais, canal,
							fecha, bandera));
					if (listaAuxReporteCum.size() > 0) {
						for (int i = 0; i < listaAuxReporteCum.get(0).size(); i++) {
							for (int j = 0; j < listaAuxReporteCum.size(); j++) {
								//listaReporteCum.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
								listaReporteCumOrdenada.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));								
							}
						}
					}
				}
				mapaCheck.put("listaChecklist", listaChecklist);
				mapaCheck.put("listaSucursales", listaReporteCumOrdenada);
				mapaCheck.put("porcentaje", porcentaje);
				mapaCheck.put("conteo", conteo);
			}

		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
			mapaCheck = null;
		}
		 
		return mapaCheck;
	}
	
	
	/*__________PILA_____________*/

	public List<CompromisoCecoDTO> CecoInfo(int idUsuario) throws Exception {
		try {
			listaCeco1 = reporteDAO.CecoInfo(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaCeco1;

	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<VistaCDTO> ordena(int tam, List<List> listaReporteCum) {

		boolean flag = false;
		int i = 1;
		double porcentAnt = 0.0;
		double porcentAct = 0.0;
		List<VistaCDTO> act = null;
		List<VistaCDTO> ant = null;
		List<VistaCDTO> retorno = new ArrayList<VistaCDTO>();

		while (i < tam) {
			porcentAnt = getPorcentaje(listaReporteCum.get(i - 1));
			porcentAct = getPorcentaje(listaReporteCum.get(i));

			if (porcentAnt < porcentAct) {
				ant = listaReporteCum.get(i - 1);
				act = listaReporteCum.get(i);
				listaReporteCum.set(i - 1, act);
				listaReporteCum.set(i, ant);
				flag = true;
			}
			if (flag && (i + 1) == tam) {
				i = 0;
				flag = false;
			}
			i++;
		}
		for (int k = 0; k < listaReporteCum.size(); k++) {			
			for (int j = 0; j < listaReporteCum.get(k).size(); j++) {
				retorno.add((VistaCDTO) listaReporteCum.get(k).get(j));
			}
		}
		return retorno;
	}

	@SuppressWarnings("rawtypes")
	public double getPorcentaje(List<VistaCDTO> lista) {
		double res = 0.0;
		Iterator it = lista.iterator();
		while (it.hasNext()) {
			VistaCDTO obj = (VistaCDTO) it.next();
			if (obj.getIdChecklist().equals("97")){
				res += obj.getTotal();
			}
		}

		return res ;
	}
	/*________________PILA________________*/
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<VistaCDTO> ordenaPila(int tam, List<List> listaReporteCum) {

		boolean flag = false;
		int i = 1;
		double porcentAnt = 0.0;
		double porcentAct = 0.0;
		List<VistaCDTO> act = null;
		List<VistaCDTO> ant = null;
		List<VistaCDTO> retorno = new ArrayList<VistaCDTO>();

		while (i < tam) {
			porcentAnt = getPorcentajePila(listaReporteCum.get(i - 1));
			porcentAct = getPorcentajePila(listaReporteCum.get(i));

			if (porcentAnt < porcentAct) {
				ant = listaReporteCum.get(i - 1);
				act = listaReporteCum.get(i);
				listaReporteCum.set(i - 1, act);
				listaReporteCum.set(i, ant);
				flag = true;
			}
			if (flag && (i + 1) == tam) {
				i = 0;
				flag = false;
			}
			i++;
		}
		for (int k = 0; k < listaReporteCum.size(); k++) {			
			for (int j = 0; j < listaReporteCum.get(k).size(); j++) {
				retorno.add((VistaCDTO) listaReporteCum.get(k).get(j));
			}
		}
		return retorno;
	}

	@SuppressWarnings("rawtypes")
	public double getPorcentajePila(List<VistaCDTO> lista) {
		double res = 0.0;
		Iterator it = lista.iterator();
		while (it.hasNext()) {
			VistaCDTO obj = (VistaCDTO) it.next();
				res += obj.getTotal();
		}

		return res/lista.size();
	}
	/*___________PILA________________*/
	
	public boolean CargaInicialHistorico(){
		boolean respuesta = false;
		try{
			respuesta = reporteDAO.CargaInicialHistorico();
		} 
		catch (Exception e) {
			logger.info("No fue posible cargar historico");
			
		}
							
		return respuesta;
	}
	
	public boolean CargaCecosHistorico(){
		boolean respuesta = false;
		try{
			respuesta = reporteDAO.CargaCecosHistorico();
		} 
		catch (Exception e) {
			logger.info("No fue posible cargar cecos");
			
		}
							
		return respuesta;
	}
	
	public boolean CargaGeografiaHistorico(){
		boolean respuesta = false;
		try{
			respuesta = reporteDAO.CargaGeografiaHistorico();
		} 
		catch (Exception e) {
			logger.info("No fue posible cargar geografia");
			
		}
							
		return respuesta;
	}

	public List<VistaCDTO>  ReporteChecklist(int idCeco, int idChecklist)
			throws Exception {
		try {
			listaCeco = reporteDAO.ReporteChecklist(idCeco, idChecklist);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaCeco;
	}

	public int ReporteCalificacion(int idBitacora, int bandera) {
		
		int respuesta = 0 ;
		
		try {
			respuesta = reporteDAO.ReporteCalificacion(idBitacora, bandera);
		} catch (Exception e) {
			logger.info("No fue posible consultar la calificacion");
		}
		
		return respuesta;
	}
}