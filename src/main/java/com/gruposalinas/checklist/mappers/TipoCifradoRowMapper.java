package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.TipoCifradoDTO;

public class TipoCifradoRowMapper implements RowMapper <TipoCifradoDTO> {

	public TipoCifradoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		TipoCifradoDTO tipoCifradoDTO = new TipoCifradoDTO();

		tipoCifradoDTO.setIdTipoCifrado(rs.getInt("FIIDTIPOCIFRADO"));
		tipoCifradoDTO.setNumEmpleado(rs.getInt("FIID_USUARIO"));
		tipoCifradoDTO.setTipoServicio(rs.getInt("FITIPOSERVICIO"));
		tipoCifradoDTO.setJson(rs.getString("FCJSON"));
		tipoCifradoDTO.setFecha(rs.getString("FDFECHA"));
		tipoCifradoDTO.setTipoApp(rs.getInt("FITIPOAPP"));

		return tipoCifradoDTO;
	}
}
