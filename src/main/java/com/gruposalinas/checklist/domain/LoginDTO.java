package com.gruposalinas.checklist.domain;

public class LoginDTO extends CecoDTO{
	
	private int noEmpleado;
	private String NombreEmpelado;
	private int idperfil;
	private String descPuesto;
	private int idPuesto;
	private int idPais;
	private int idTerritorio;
	private int zona;
	private int region;
	
	public int getNoEmpleado() {
		return noEmpleado;
	}
	public void setNoEmpleado(int noEmpleado) {
		this.noEmpleado = noEmpleado;
	}
	public String getNombreEmpelado() {
		return NombreEmpelado;
	}
	public void setNombreEmpelado(String nombreEmpelado) {
		NombreEmpelado = nombreEmpelado;
	}
	public int getIdperfil() {
		return idperfil;
	}
	public void setIdperfil(int idperfil) {
		this.idperfil = idperfil;
	}
	public String getDescPuesto() {
		return descPuesto;
	}
	public void setDescPuesto(String descPuesto) {
		this.descPuesto = descPuesto;
	}
	public int getIdPuesto() {
		return idPuesto;
	}
	public void setIdPuesto(int idPuesto) {
		this.idPuesto = idPuesto;
	}
	public int getIdPais() {
		return idPais;
	}
	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}
	public int getIdTerritorio() {
		return idTerritorio;
	}
	public void setIdTerritorio(int idTerritorio) {
		this.idTerritorio = idTerritorio;
	}
	public int getZona() {
		return zona;
	}
	public void setZona(int zona) {
		this.zona = zona;
	}
	public int getRegion() {
		return region;
	}
	public void setRegion(int region) {
		this.region = region;
	}
	
}
