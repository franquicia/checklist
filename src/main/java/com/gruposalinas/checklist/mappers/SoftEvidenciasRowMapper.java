package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;

public class SoftEvidenciasRowMapper implements RowMapper<ChecklistPreguntasComDTO> {

	public ChecklistPreguntasComDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistPreguntasComDTO com = new ChecklistPreguntasComDTO();

        com.setIdBita(rs.getInt("FIID_BITACORA"));
	    com.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    com.setIdArbol(rs.getInt("FIID_ARBOL_DES"));
	    com.setRuta(rs.getString("RUTA"));
	    com.setIdElem(rs.getInt("FIID_ELEM_PL"));
	    com.setTipoEvi(rs.getInt("FIID_TIPO"));
	   
	
		return com;
	}

}
