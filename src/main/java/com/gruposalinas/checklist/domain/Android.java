package com.gruposalinas.checklist.domain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gruposalinas.checklist.controller.LoginController;
import com.gruposalinas.checklist.util.UtilCryptoGS;
import com.gruposalinas.checklist.util.UtilFRQ;

public class Android {
	private static Logger logger = LogManager.getLogger(Android.class);
	
	private int id;
	private String name;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	// "http://localhost:8080/checklist/servicios/enviaRespuesta.json");
	public static void main(String[] args) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

		try {

			URL url = new URL(
			"http://localhost:8080/checklist/servicios/respuesta.json?/LrmW7p+t0tWWlzvO5N3vg==&token=");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/text");

			//String input = "Uh4D1aCk/i/+pjNGXM+AgNhCmS05nnARS/ JueMF0L0GseeiwsjOK7DPo0wdWpwmOm6HSL49nkrdVBE9Q4SCXoQ==&token=á";

			String inputJson="{\"idCheckUsua\":1,"
					+ "\"listaRespuestas\":["
						+ "{\"idRespuesta\":12,\"compromiso\":{\"descripcion\":\"hola\",\"fechaCompromiso\":\"20161212\"},\"evidencia\":{\"idTipo\":\"1\",\"ruta\":\"imagen\"}},"
						+ "{\"idRespuesta\":14,\"compromiso\":{\"descripcion\":\"hola\",\"fechaCompromiso\":\"20161212\"},\"evidencia\":{\"idTipo\":\"1\",\"ruta\":\"imagen\"}},"
						+ "{\"idRespuesta\":16,\"compromiso\":{\"descripcion\":\"hola\",\"fechaCompromiso\":\"20161212\"},\"evidencia\":{\"idTipo\":\"1\",\"ruta\":\"imagen\"}}],"
					+ "\"bitacora\":{\"idBitacora\":32,\"latitud\":19.987654,\"longitud\":-99.123456,\"fechaFin\":\"20161212\"}}";
			
			UtilCryptoGS cifra= new UtilCryptoGS();
			String input=cifra.encryptParams(inputJson);
			
			OutputStream os = conn.getOutputStream();
			try {
				os.write(input.getBytes());
				os.flush();
			} finally{
				os.close();
			}
			
			InputStream inputStrm = conn.getInputStream();
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						(inputStrm)));
				String output;
				//System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {

					//System.out.println(output);
				}
			} finally {
				inputStrm.close();
			}
			
			conn.disconnect();

		} catch (MalformedURLException e) {

			logger.info("Ocurrio algo...");
		} catch (IOException e) {

			logger.info("Ocurrio algo...");

		}
		
	}

}