package com.gruposalinas.checklist.domain;

public class AppPerfilDTO {
 private int idApp;
 private String descripcion;
 private int idAppPerfil;
 private int idUsuario;
 private int commit;
public int getIdApp() {
	return idApp;
}
public void setIdApp(int idApp) {
	this.idApp = idApp;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
public int getIdAppPerfil() {
	return idAppPerfil;
}
public void setIdAppPerfil(int idAppPerfil) {
	this.idAppPerfil = idAppPerfil;
}
public int getIdUsuario() {
	return idUsuario;
}
public void setIdUsuario(int idUsuario) {
	this.idUsuario = idUsuario;
}
public int getCommit() {
	return commit;
}
public void setCommit(int commit) {
	this.commit = commit;
}
 
 
}
