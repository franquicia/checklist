package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.GrupoDTO;


public interface GrupoDAO {
	
	public List<GrupoDTO> obtieneGrupo(String idGrupo) throws Exception;
	
	public int insertaGrupo(GrupoDTO bean) throws Exception;
	
	public boolean actualizaGrupo(GrupoDTO bean) throws Exception;
	
	public boolean eliminaGrupo(int idGrupo) throws Exception;
}
