<div id="content" class="carouselDiv"
	style="width: 653px; margin: 0 auto; padding: 10px">
	<!--  <img src="/checklist/images/checklist_espera.jpg"/> -->

	<!-- Carousel -->
	<div id="myCarousel" class="carousel slide" data-interval="false">
		<!-- Indicadores -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
			<li data-target="#myCarousel" data-slide-to="3"></li>
			<li data-target="#myCarousel" data-slide-to="4"></li>
			<li data-target="#myCarousel" data-slide-to="5"></li>
			<li data-target="#myCarousel" data-slide-to="6"></li>
			<li data-target="#myCarousel" data-slide-to="7"></li>
			<li data-target="#myCarousel" data-slide-to="8"></li>
			<li data-target="#myCarousel" data-slide-to="9"></li>
		</ol>
		<!-- Imagenes -->
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<img src="../images/ayudaAndroid/1.png"
					alt="imagen1">
			</div>

			<div class="item">
				<img src="../images/ayudaAndroid/2.png"
					alt="imagen2">
			</div>

			<div class="item">
				<img src="../images/ayudaAndroid/3.png"
					alt="imagen3">
			</div>

			<div class="item">
				<img src="../images/ayudaAndroid/4.png"
					alt="imagen4">
			</div>

			<div class="item">
				<img src="../images/ayudaAndroid/5.png"
					alt="imagen5">
			</div>
			
			<div class="item">
				<img src="../images/ayudaAndroid/6.png"
					alt="imagen6">
			</div>

			<div class="item">
				<img src="../images/ayudaAndroid/7.png"
					alt="imagen7">
			</div>

			<div class="item">
				<img src="../images/ayudaAndroid/8.png"
					alt="imagen8">
			</div>

			<div class="item">
				<img src="../images/ayudaAndroid/9.png"
					alt="imagen9">
			</div>
			<div class="item">
				<img src="../images/ayudaAndroid/10.png"
					alt="imagen10">
			</div>

			<!-- Controles Izquierda y Derecha -->
			<a class="left carousel-control" href="#myCarousel" role="button"data-slide="prev"> 
				<span class="flechas" id="fAnterior"> <img
					src="../images/carousel/slide/flechaizqover.png" alt="imagen2"
					width="40" height="55">
				</span>
			</a> 
			<a class="right carousel-control" href="#myCarousel" role="button"
				data-slide="next"> <!--Imagenes Flecha Derecha --> <span
				class="flechas" id="fSiguiente"> <img
					src="../images/carousel/slide/flechaderover.png" alt="imagen2"
					width="40" height="55">
			</span>
			</a>
		</div>
	</div>
</div>