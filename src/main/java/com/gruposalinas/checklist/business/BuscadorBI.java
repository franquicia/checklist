package com.gruposalinas.checklist.business;

public class BuscadorBI {
	
	/*
	private static Logger logger = LogManager.getLogger(BuscadorBI.class);
	@Autowired
	private ArchiveDAO archiveParser;
	@Autowired
	private SearchHistoryDAO searchHistoryDAO;
	@Autowired
	private GridFSDAO fileStorage;

	private String recurso;

	public List<Tag> getTags(String tagName, HttpServletRequest request) {
		try {
			List<DBObject> hsr = searchHistoryDAO.findHistory(tagName);
			List<Tag> result = new ArrayList<Tag>();
			Iterator<DBObject> it = hsr.iterator();
			while (it.hasNext()) {
				DBObject o = it.next();
				if (!o.get("searchText").toString().contains(tagName)) {
					result.add(new Tag(o.get("searchText").toString(), o.get("searchText").toString(), null));
				}
			}
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("Ocurrio algo...");
			return null;
		}
	}

	public List<Tag> getData(String termino) {
		List<Tag> result = new ArrayList<Tag>();
		Iterable<DBObject> rslt = archiveParser.groupSearchByArchive(termino);
		Iterator<DBObject> it1 = rslt.iterator();
		while (it1.hasNext()) {
			DBObject o = it1.next();
			BSONObject id = (BSONObject) o.get("_id");
			result.add(
					new Tag(id.get("title").toString(), o.get("pageText").toString(), id.get("fileName").toString()));
		}
		return result;
	}

	public List<String> muestraDoc(String recurso, String tagName,  HttpServletRequest request) {
		this.recurso = recurso;
		try {
			tagName = new String(tagName.getBytes("ISO-8859-1"), "UTF-8");
			List<String> np = new ArrayList<String>();
			List<DBObject> hsr = searchHistoryDAO.findHistory(tagName);
			Iterator<DBObject> it = hsr.iterator();
			if (!it.hasNext()) {
				SearchHistory sh = new SearchHistory(tagName, new Date());
				searchHistoryDAO.addSearchHistory(sh);
			}
			while (it.hasNext()) {
				DBObject o = it.next();
				if (!o.get("searchText").toString().contains(tagName)) {
					SearchHistory sh = new SearchHistory(tagName, new Date());
					searchHistoryDAO.addSearchHistory(sh);
				}
			}
			Iterable<DBObject> pageNums = archiveParser.getPageNumMatches(tagName, recurso);
			Iterator<DBObject> itr = pageNums.iterator();
			while (itr.hasNext()) {
				np.add(itr.next().get("pageNums").toString());
			}
			return np;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("Ocurrio algo...");
			return null;
		}
	}

	public InputStream reporte(String recurso) {
		return fileStorage.getByArchiveName(this.recurso);
	}

	public List<Tag> getResults(String tagName) {
		Iterable<DBObject> md = fileStorage.searchMetadata(tagName);
		List<Tag> results = new ArrayList<Tag>();
		
		Iterable<DBObject> spt = archiveParser.searchPageText(tagName);
		Iterator<DBObject> ispt = spt.iterator();
		while (ispt.hasNext()) {
			DBObject o = ispt.next();
			BSONObject id = (BSONObject) o.get("_id");
			results.add(
					new Tag(o.get("title").toString(), o.get("pageText").toString(), id.get("fileName").toString()));
			////System.out.println("ispt: " + o);
		}
		
		List<DBObject> uniqueMetadata = fileStorage.removeDupMetadata(md, spt);
		
		if ( uniqueMetadata.size() > 0 ) {
			
			for (DBObject obj : uniqueMetadata){
				BSONObject id = (BSONObject) obj.get("_id");
				results.add(
						new Tag(obj.get("title").toString(), obj.get("description").toString(), id.get("fileName").toString()));
			}
		}
		
		return results;
	}*/

}
