<!DOCTYPE HTML>
<html>
<head>
	<title>
		checklist - Error
	</title>
	<style type="text/css">
		.vertical-center {
			min-height: 100%;
			min-height: 100vh;
			display: flex;
			align-items: center;
		}
	</style>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap/bootstrap.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="css/header-style.css"> -->
	<script src="../js/jquery/jquery-2.2.4.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
</head>

<header>
<!-- 
<div class="checklists">
	
	<div class="checklists">
		<div class="imageheader">
			<img src="images/header/logo_baz.png" alt="logo"
				width="290" height="37">
		</div>
		<div class="textheader">
			</br>
			
		</div>
	</div>

	</div>
 -->
</header>

<body>
	
		<div class="container">
			<div class="row vertical-center">
				<div class="col-md-12">
					<img src="../images/error/algo_paso.png" class="img-responsive center-block" />
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<div class="center-block" style="max-width: 22em;">
						<a href="../">
							<img src="../images/error/boton_volver.png" class="img-responsive" />
						</a>
					</div>
				</div>
			</div>	
			<!--
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
					  <div class="panel-body text-center">
					  	<h1>
					    	<span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
					    	No se encontr� el recurso solicitado.
					    </h1>
					    <div class="btn-group" role="group" aria-label="Justified button group">
					    	<a href="../" id="go-back" class="btn btn-warning center-block" role="button" style="min-width: 10em; display: block;">
						       <span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span>
						       &nbsp;Regresar
						    </a>
						    <a href="../" class="btn btn-warning center-block" role="button" style="min-width: 10em; display: block;">
						       <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
						       &nbsp;Ir al inicio
						    </a>
					    </div>
					    <br/>
					    <br/>
					  </div>
					</div>
					
				</div>
			</div>
			-->
		</div>
	
	<script>
	$(document).ready(function(){
		$('#go-back').click(function(){
			parent.history.back();
			return false;
		});
	});
	</script>
</body>
</html>