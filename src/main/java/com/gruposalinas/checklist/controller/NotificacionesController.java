package com.gruposalinas.checklist.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.NotificacionesBI;

@Controller
public class NotificacionesController {

	@Autowired
	NotificacionesBI notificacionesBI;
	
	@RequestMapping(value={"tienda/enviarNotificacionAndroid.htm","central/enviarNotificacionAndroid.htm"},method=RequestMethod.GET)
	public ModelAndView menuChecklist(HttpServletRequest requqest,HttpServletResponse response,Model model)
			throws ServletException, IOException{

		ModelAndView mv = new ModelAndView("enviarNotificacionAndroid");
		return mv;
	}
	
	@RequestMapping(value={"tienda/enviarAccionAndroid.htm","central/enviarAccionAndroid.htm"},method=RequestMethod.POST)
	public ModelAndView asignacionChecklist(HttpServletRequest request,HttpServletResponse response,Model model,
			@RequestParam(value = "tokenUsuario", required = true, defaultValue = "null") String tokenUsuario,
			@RequestParam(value = "titulo", required = true, defaultValue = "false") String titulo,
			@RequestParam(value = "mensaje", required = true, defaultValue = "false") String mensaje)
	
			throws ServletException, IOException{

		ModelAndView mv = new ModelAndView("enviarAccionAndroid");
		boolean respuestaEnvio=false;
		//respuestaEnvio=notificacionesBI.enviarNotificacion(tokenUsuario, titulo, mensaje);
		//se obtiene el nombre del checklist
		mv.addObject("estatusEnvio", respuestaEnvio);		
		return mv;		
	}
}
