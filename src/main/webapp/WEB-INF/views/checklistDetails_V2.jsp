<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<head>
	<script type="text/javascript">
		var urlServer = '${urlServer}';
	</script>
</head>
<body onload="initTable(${idCheck})">
	<input type="hidden" id="idChecklist" name="idChecklist" value="${idCheck}" />
	<!-- contenedor para mostrar el formulario de la definicion del arbol de decision -->
	<div class="seccionArbol" id="contenedorArbol"	style=" background: #fff; z-index: 0; overflow-y: scroll;">
		<h1>Detalle de Checklist: ${nombreCheck}</h1>
		<!-- lista donde se mostran las preguntas de la vista previa -->
		<h1>Preguntas</h1>

		<table class="tabla" id="myTable">
			<thead id="myTableHead"> 
				<tr>
					<th>Respuesta</th>
					<th>Evidencia</th>
					<th>Tipo de evidencia</th>
					<th>Evidencia obligatoria</th>
					<th>Observaciones</th>
					<th>Acciones</th>
					<th>Pregunta siguiente</th>
					<th>Etiqueta Evidencia</th>
 				</tr>
			</thead>
			<tbody id="myTableBody">
			</tbody>
		</table>
		</div>
	
			<form name="formulario" method="POST" action="/checklist/central/resumenCheck.htm" id="formActivaAdmin">							
				<a onclick="resumen()"><button id="continuar" class="log-btn2" style="margin-left:50%;">Regresar</button></a>
				<input type="hidden" name="idUserAdmin" value="adminCheckchecklists">
			</form>	
</body>
	