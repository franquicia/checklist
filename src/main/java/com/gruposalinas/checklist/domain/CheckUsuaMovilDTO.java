/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.domain;

/**
 *
 * @author leodan1991
 */
public class CheckUsuaMovilDTO {
    
	private int idCheckUsuario;
	private int idChecklist;
	private int idUsuario;
     	private String fechaIni;
	private String fechaFin;
	private int idCeco;
	private String nombreCeco;
	private String nombreCheck;
	private String latitud;
	private String longitud;
	private String zona;

    public int getIdCheckUsuario() {
        return idCheckUsuario;
    }

    public void setIdCheckUsuario(int idCheckUsuario) {
        this.idCheckUsuario = idCheckUsuario;
    }

    public int getIdChecklist() {
        return idChecklist;
    }

    public void setIdChecklist(int idChecklist) {
        this.idChecklist = idChecklist;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(String fechaIni) {
        this.fechaIni = fechaIni;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public int getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(int idCeco) {
        this.idCeco = idCeco;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    

    public String getNombreCheck() {
        return nombreCheck;
    }

    public void setNombreCheck(String nombreCheck) {
        this.nombreCheck = nombreCheck;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }
 
        
    
	
    
}
