package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.ReporteHallazgosDTO;


public interface ReporteHallazgosDAO {
		
		public Map<String, Object> conteoGralHallazgos(String nego,String fechaInicio,String fechaFin) throws Exception; 
		
		public List<ReporteHallazgosDTO> conteoCecoHallazgos(String ceco,String fechaInicio,String fechaFin) throws Exception; 
		
		public List<ReporteHallazgosDTO> conteoFaseHallazgos(String ceco,String fechaInicio,String fechaFin) throws Exception; 
		
		public List<ReporteHallazgosDTO> obtieneCecosFechas(String negocio,String fechaInicio,String fechaFin) throws Exception;
		
		
		
	}