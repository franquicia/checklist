package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.Coment7SDTO;


public class Coments7sRowMapper implements RowMapper<Coment7SDTO>{


	public Coment7SDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		Coment7SDTO coment = new Coment7SDTO();
		
		coment.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		coment.setIdArbolDes(rs.getInt("FIID_ARBOL_DES"));
		coment.setIdBitacora(rs.getInt("FIID_BITACORA"));
		coment.setIdRespuesta(rs.getInt("FIID_RESPUESTA"));
		coment.setDescripcion(rs.getString("FCDESCRIPCION"));
		coment.setIdCheckUsua(rs.getInt("FIID_CHECK_USUA"));
		coment.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		coment.setCeco(rs.getString("FCID_CECO"));
		return coment;
	}
	

}
