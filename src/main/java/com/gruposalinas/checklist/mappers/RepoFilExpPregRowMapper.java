package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.RepoFilExpDTO;

public class RepoFilExpPregRowMapper implements RowMapper<RepoFilExpDTO> {

	public RepoFilExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		RepoFilExpDTO com = new RepoFilExpDTO();

	    
	    
		com.setCeco(rs.getString("FCID_CECO"));
		 com.setVersion(rs.getInt("FIVERSION"));
	    com.setIdCheck(rs.getInt("FIID_CHECKLIST"));
	    com.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    com.setPregunta(rs.getString("FCDESCRIPCION"));   
	    com.setClasif(rs.getString("FCCLASIFICA"));
	    com.setChecklist(rs.getString("CHECKLIST"));
	    com.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
	    

	
		return com;
	}

}
