package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.ChecklistHallazgosRepoDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.PreguntaCheckDTO;
import com.gruposalinas.checklist.mappers.PreguntaCheckRowMapper;

public interface ChecklistHallazgosRepoDAO {

	// obtiene HALLAZGOS por fechas
	public List<ChecklistHallazgosRepoDTO> obtieneDatosFecha(String fechaIni, String fechaFin) throws Exception;
		
	
	// obtiene cursores 4
	public Map<String, Object>  cursores(String bitgral) throws Exception;
	
	// obtiene cursores 3 nvo
		public Map<String, Object>  cursoresNvo(String bitgral) throws Exception;
		
		// obtiene negocio por bitacora
		public List<ChecklistHallazgosRepoDTO> obtieneDatosNego(String bita) throws Exception;
		
		// obtiene negocio por bitacora
		public List<ChecklistHallazgosRepoDTO> obtieneDatosNegoExp(String ceco,String fase,String proyecto) throws Exception;
		
		
		
}
