package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.BitacoraAdministradorDAO;
import com.gruposalinas.checklist.domain.BitacoraAdministradorDTO;


public class BitacoraAdministradorBI {
	
	private static Logger logger = LogManager.getLogger(BitacoraBI.class);
	
	@Autowired
	BitacoraAdministradorDAO bitacoraAdmDAO;
	
	List<BitacoraAdministradorDTO> listaBitacora = null;
	List<BitacoraAdministradorDTO> listaBitacoraAll = null ;

	public int insertaBitacora(BitacoraAdministradorDTO bean){
		
		int idBitacora = 0 ;
		
		try{
			
			idBitacora=bitacoraAdmDAO.insertaBitacora(bean);
			
		}catch(Exception e){
			logger.info("No fue posible insertar la Bitacora "+ e);
		}
		
		return idBitacora;
				
	}
	
	public boolean actualizaBitacora(BitacoraAdministradorDTO bean){
		
		boolean respuesta = false;
		
		try{
			respuesta = bitacoraAdmDAO.actualizaBitacora(bean);
		}catch(Exception e){
			logger.info("No fue posible actualizar la Bitacora "+e);
		}
		
		return respuesta;
		
	}
	
	public boolean eliminaBitacora(int idBitacora) {
	
		boolean respuesta = false;
		
		try{
			respuesta = bitacoraAdmDAO.eliminaBitacora(idBitacora);
		}catch(Exception e){
			logger.info("No fue posible eliminar la Bitacora "+e);
		}
		
		return respuesta;
		
	}
	
	public List<BitacoraAdministradorDTO> buscaBitacoraCompleto(BitacoraAdministradorDTO bean){
		
		try {
			listaBitacoraAll = bitacoraAdmDAO.buscaBitacoraCompleto(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar la Bitacora "+ e);
		}
		
		return listaBitacoraAll;
		
	}
	
	public List<BitacoraAdministradorDTO> buscaBitacorasPorCampo(int idCheckUsua, int idBitacora, String fechaInicio, String fechaFin){
		
		try {
			listaBitacora = bitacoraAdmDAO.buscaBitacorasPorCampo(idCheckUsua, idBitacora, fechaInicio, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar la Bitacora "+ e);
		}
		
		return listaBitacora;
		
	}
	
	
	
}
