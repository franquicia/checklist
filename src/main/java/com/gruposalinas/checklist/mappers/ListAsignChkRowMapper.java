package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ListaAsignChkDTO;


public class ListAsignChkRowMapper implements RowMapper<ListaAsignChkDTO>{


	public ListaAsignChkDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		ListaAsignChkDTO obj= new ListaAsignChkDTO();
		
                /*
                    private int idCheckUsuario;
                    private int idChecklist;
                    private int idUsuario;
                    private String fechaIni;
                    private String fechaFin;
                    private int idCeco;
                    private String nombreCeco;
                    private String nombreCheck;
                    private String latitud;
                    private String longitud;
                    private String zona;
                    private int version;
                */
                
                obj.setIdCheckUsuario(rs.getInt("FIID_CHECK_USUA"));
                obj.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
                obj.setIdUsuario(rs.getInt("FIID_USUARIO"));
                obj.setFechaIni(rs.getString("FDFECHA_INICIO"));
                //obj.setFechaFin(rs.getString(""));
                obj.setIdCeco(rs.getInt("FCID_CECO"));
                obj.setNombreCeco(rs.getString("FCNOMBRE"));
                //obj.setNombreCheck(rs.getString(""));
                obj.setLatitud(rs.getString("FCLATITUD"));
                obj.setLongitud(rs.getString("FCLONGITUD"));
                obj.setZona(rs.getString("FCZONA"));
                obj.setVersion(rs.getInt("FIVERSION"));
                
		
		return obj;
	}
	

}
