package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.DepuraBitacoraDTO;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class DepuraBitacoraDAOImpl extends DefaultDAO implements DepuraBitacoraDAO {

    private static Logger logger = LogManager.getLogger(DepuraBitacoraDAOImpl.class);

    DefaultJdbcCall jdbcDepuraBitacoras;

    public void init() {

        jdbcDepuraBitacoras = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADEPURACION")
                .withProcedureName("SPDEPBITACORAS");

    }

    @Override
    public List<DepuraBitacoraDTO> depuraBitacorasSem(int fecha) throws Exception {
        Map<String, Object> out = null;
        List<DepuraBitacoraDTO> respuesta = null;
        int result = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FITIEMPO", fecha);

        out = jdbcDepuraBitacoras.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADEPURACION.SPDEPBITACORAS");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        result = resultado.intValue();

        if (result == 0) {
            logger.info("Algo ocurrió al depurar bitacoras");
        } else {
            respuesta = new ArrayList<DepuraBitacoraDTO>();
            DepuraBitacoraDTO aux = new DepuraBitacoraDTO();
            BigDecimal respuestas = (BigDecimal) out.get("PA_RESELIMIN");
            BigDecimal Bitacoras = (BigDecimal) out.get("PA_ELIMINADAS");
            aux.setRespEliminadas(respuestas.intValue());
            aux.setBitaEliminadas(Bitacoras.intValue());
            aux.setEjecucion(result);
            respuesta.add(aux);
        }
        return respuesta;
    }

}
