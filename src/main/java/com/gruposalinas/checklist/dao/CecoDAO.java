package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.CecoIndicadoresDTO;
import java.util.List;

public interface CecoDAO {

    public boolean insertaCeco(CecoDTO bean) throws Exception;

    public boolean actualizaCeco(CecoDTO bean) throws Exception;

    public boolean eliminaCeco(int ceco) throws Exception;

    public List<CecoDTO> buscaCeco(int ceco) throws Exception;

    public List<CecoDTO> buscaCecos(int activo) throws Exception;

    public boolean cargaCecos() throws Exception;

    public boolean cargaCecos2() throws Exception;

    public boolean cargaGeografia() throws Exception;

    public boolean updateCecos() throws Exception;

    public List<CecoDTO> buscaCecoPaso(String ceco, String cecoPadre, String descripcion) throws Exception;

    public List<CecoDTO> buscaCecosPaso() throws Exception;

    public List<CecoDTO> buscaTerritorios(int idPais) throws Exception;

    public List<CecoDTO> buscaCecosPasoP(int idCecoPadre) throws Exception;

    public List<CecoIndicadoresDTO> buscaCecosIndicadores() throws Exception;

    public int eliminaCecosTrabajo() throws Exception;

    public int[] cargaCecosSFGuatemala() throws Exception;

    public int[] cargaCecosCOMGuatemala() throws Exception;

    public int[] cargaCecosSFPeru() throws Exception;

    public int[] cargaCecosCOMPeru() throws Exception;

    public int[] cargaCecosSFHonduras() throws Exception;

    public int[] cargaCecosCOMHonduras() throws Exception;

    public int[] cargaCecosSFPanama() throws Exception;

    public int[] cargaCecosSFSalvador() throws Exception;

    public boolean eliminaDuplicados() throws Exception;

    public boolean cargaCreditoCobranzaCecos() throws Exception;

    public boolean cargaCreditoCobranzaCecosPanama() throws Exception;

    public List<CecoDTO> buscaCecosGCC() throws Exception;

    public boolean cargaCreditoCobranza() throws Exception;

    public boolean cargaBazPasoTrabajo() throws Exception;

    public boolean cargaGCCPasoTrabajo() throws Exception;

    public boolean cargaPPRPasoTrabajo() throws Exception;

    public boolean cargaEKTPasoTrabajo() throws Exception;

    public boolean cargaCanalesTercerosPasoTrabajo() throws Exception;

    public boolean cargaMicronegocioPasoTrabajo() throws Exception;

}
