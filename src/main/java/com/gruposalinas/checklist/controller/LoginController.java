package com.gruposalinas.checklist.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.AppBI;
import com.gruposalinas.checklist.business.CanalBI;
import com.gruposalinas.checklist.business.LoginBI;
import com.gruposalinas.checklist.business.PerfilUsuarioBI;
import com.gruposalinas.checklist.domain.AppPerfilDTO;
import com.gruposalinas.checklist.domain.Login;
import com.gruposalinas.checklist.domain.LoginDTO;
import com.gruposalinas.checklist.domain.LoginTokenDTO;

@Controller
public class LoginController {
	private static Logger logger = LogManager.getLogger(LoginController.class);
	
	@Autowired
	LoginBI loginBI;
	
	@Autowired
	PerfilUsuarioBI perfilUsuarioBI;
	
	@Autowired
	AppBI perfilAPP;
	
	@RequestMapping(value ="/login.htm", method = RequestMethod.GET)
	public ModelAndView login(){
		Login login = new Login();
		return new ModelAndView("login", "command", login);
	}
	
	@InitBinder
    public void setAllowedFields(WebDataBinder dataBinder) {
        dataBinder.setAllowedFields("user", "pass");
    }
	
	@RequestMapping(value="/consultaLogin.htm", method = RequestMethod.POST)
	public ModelAndView consultaLogin(@ModelAttribute("login") Login login, BindingResult result, HttpServletRequest request) throws Exception{
		
		int user = login.getUser();
		String pass = login.getPass();
		String valida= request.getParameter("valida");
		ModelAndView salida = null;
		
		if(user>0 && valida.equals("true")){
			LoginDTO regEmpleado=null;
			try {
				regEmpleado = loginBI.consultaUsuario(user);
			} catch (Exception e) {
				logger.info("Ocurrio algo...");
			}
			if(regEmpleado!=null){		
				if( pass.equals(regEmpleado.getNombreEmpelado())){
					salida = new ModelAndView("redirect:/central/schedulerForm.htm");
					salida.addObject("valida",valida);
					
				} else {
					request.setAttribute("mensaje", "Password incorrecto");
					salida = new ModelAndView("login", "command", login);
					salida.addObject("invalido", "Password incorrecto");
				}
			}else{
				request.setAttribute("mensaje", "Usuario no registrado como administrador");
				salida = new ModelAndView("login", "command", login);	
				salida.addObject("invalido", "Usuario no registrado");
			}
		} else {
			request.setAttribute("mensaje", "Número de empleado o llave incorrecta, favor de verificar");
			salida = new ModelAndView("login", "command", login);
			salida.addObject("invalido", "Número de empleado incorrecto");
		}        
		return salida;
	}	
	
	@ExceptionHandler(Exception.class)
	public ModelAndView handleError(HttpServletRequest request, Exception ex) {
		logger.info("Ocurrio algo...");
		ModelAndView mv = new ModelAndView("exception");
	    return mv;
	}
	
	
	@RequestMapping(value = "central/verificaUsuarioTienda.json", method = RequestMethod.GET)
	public @ResponseBody boolean verificaUsuarioTienda(
			@RequestParam(value = "idUsuario", required = true, defaultValue = "") String idUsuario,
			@RequestParam(value = "token", required = true, defaultValue = "") String token, 
	        HttpServletRequest request, Exception ex) {
		
		boolean res  = false;
		
		res = perfilUsuarioBI.verificaPerfilTienda(idUsuario);
				
		return res;
		
	}
	
	@RequestMapping(value = "central/store.htm", method = RequestMethod.POST)
	public ModelAndView controllerStore(HttpServletRequest request, Exception ex, HttpServletResponse response, Model model,
			@RequestParam(value = "usuario", required = true, defaultValue = "vacio") String usuario) {
		
		ModelAndView mv = new ModelAndView("pruebaStore");
		List<AppPerfilDTO> perfiles=null;
		
		
		
		//usuario="196228";
		perfiles=perfilAPP.buscaAppPerfil(""+1, usuario.trim());
		
		if(perfiles.size()>0){
			mv.addObject("GM", "1");
		}
		else{
			mv.addObject("GM", "0");
		}
		perfiles=null;
		perfiles=perfilAPP.buscaAppPerfil(""+2, usuario.trim());
		
		if(perfiles.size()>0){
			mv.addObject("MG", "2");
		}
		else{
			mv.addObject("MG", "0");
		}
		perfiles=null;
		perfiles=perfilAPP.buscaAppPerfil(""+3, usuario.trim());
		
		if(perfiles.size()>0){
			mv.addObject("BD", "3");
		}
		else{
			mv.addObject("BD", "0");
		}
		perfiles=null;
		perfiles=perfilAPP.buscaAppPerfil(""+4, usuario.trim());
		
		if(perfiles.size()>0){
			mv.addObject("GZ", "4");
		}
		else{
			mv.addObject("GZ", "0");
		}
		perfiles=null;
		perfiles=perfilAPP.buscaAppPerfil(""+5, usuario.trim());
		if(perfiles.size()>0){
			mv.addObject("CU", "5");
		}
		else{
			mv.addObject("CU", "0");
		}
		
		perfiles=null;
		perfiles=perfilAPP.buscaAppPerfil(""+6, usuario.trim());
		if(perfiles.size()>0){
			mv.addObject("PM", "6");
		}
		else{
			mv.addObject("PM", "0");
		}
		
		return mv;
	}
	
	@RequestMapping(value = "central/storeIndex.htm", method = RequestMethod.GET)
	public ModelAndView storeIndex(HttpServletRequest request, Exception ex) {
		return new ModelAndView("storeIndex", "command", new LoginTokenDTO());
	}
	

	
}