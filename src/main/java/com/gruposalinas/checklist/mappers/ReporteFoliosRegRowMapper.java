package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.PreguntaDTO;

public class ReporteFoliosRegRowMapper implements RowMapper<PreguntaDTO> {

	public PreguntaDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		PreguntaDTO preguntaDTO = new PreguntaDTO();
		preguntaDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		preguntaDTO.setPregunta(rs.getString("PREGUNTA"));
		return preguntaDTO;
	}

}
