	$(document).ready(function() {
		$("#activo1").val("");
		$("#idCanal1").val("");
		$("#idCeco1").val("");
		$("#descCeco1").val("");
		$("#idCecoSuperior1").val("");
		$("#calle1").val("");
		$("#ciudad1").val("");
		$("#idEstado1").val("");
		$("#idPais1").val("");
		$("#cp1").val("");
		$("#idNegocio1").val("");
		$("#idNivel1").val("");
		$("#nombreContacto1").val("");
		$("#puestoContacto1").val("");
		$("#telefonoContacto1").val("");
		$("#telefonoContacto1").val("");
		$("#fechaModifico1").val("");
		$("#usuarioModifico1").val("");
		$("#activo1").focus();

		$("#fechaModifico1").datepicker({
			maxDate: '0d',
			dateFormat: 'yymmdd',
			showOtherMonths: true,
			selectOtherMonths: true,
			changeMonth: true,
			changeYear: true,
	        beforeShow: function (input, inst) {
	            var rect = input.getBoundingClientRect();
	            setTimeout(function () {
	    	        inst.dpDiv.css({ top: rect.top + 35, left: rect.left + 0 });
	            }, 0);
	        }
		});
	});
	
	function validaAlta() {
		var activo1 = document.getElementById("activo1").value;
		var calle1 = document.getElementById("calle1").value;
		var ciudad1 = document.getElementById("ciudad1").value;
		var cp1 = document.getElementById("cp1").value;
		var descCeco1 = document.getElementById("descCeco1").value;
		var faxContacto1 = document.getElementById("faxContacto1").value;
		var idCanal1 = document.getElementById("idCanal1").value;
		var idCeco1 = document.getElementById("idCeco1").value;
		var idCecoSuperior1 = document.getElementById("idCecoSuperior1").value;
		var idEstado1 = document.getElementById("idEstado1").value;
		var idNegocio1 = document.getElementById("idNegocio1").value;
		var idNivel1 = document.getElementById("idNivel1").value;
		var idPais1 = document.getElementById("idPais1").value;
		var nombreContacto1 = document.getElementById("nombreContacto1").value;
		var puestoContacto1 = document.getElementById("puestoContacto1").value;
		var telefonoContacto1 = document.getElementById("telefonoContacto1").value;
		var fechaModifico1 = document.getElementById("fechaModifico1").value;	
		var usuarioModifico1 = document.getElementById("usuarioModifico1").value;

		if ((activo1 == '' || activo1 == null) &&
				(calle1 == '' || calle1 == null) &&
				(ciudad1 == '' || ciudad1 == null) &&
				(cp1 == '' || cp1 == null) &&
				(descCeco1 == '' || descCeco1 == null) &&
				(faxContacto1 == '' || faxContacto1 == null) &&
				(idCanal1 == '' || idCanal1 == null) &&
				(idCeco1 == '' || idCeco1 == null) &&
				(idCecoSuperior1 == '' || idCecoSuperior1 == null) &&
				(idEstado1 == '' || idEstado1 == null) &&
				(idNegocio1 == '' || idNegocio1 == null) &&
				(idNivel1 == '' || idNivel1 == null) &&
				(idPais1 == '' || idPais1 == null) &&
				(nombreContacto1 == '' || nombreContacto1 == null) &&
				(puestoContacto1 == '' || puestoContacto1 == null) &&
				(telefonoContacto1 == '' || telefonoContacto1 == null) &&
				(fechaModifico1 == '' || fechaModifico1 == null) &&
				(usuarioModifico1 == '' || usuarioModifico1 == null)) {
			alert("Ningún campo puede ir vacío!");
			return false;
		}
		else {
			return true;
		}
	}
