package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.AltaGerenteDAO;
import com.gruposalinas.checklist.domain.AdmHandbookDTO;
import com.gruposalinas.checklist.domain.AltaGerenteDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class AltaGerenteBI {

	private static Logger logger = LogManager.getLogger(AltaGerenteBI.class);

	@Autowired
	AltaGerenteDAO altaGerenteDAO;
	
	
	public boolean insertaGerente(AltaGerenteDTO bean) {
		
		boolean respuesta = false;
		
		try {		
			respuesta = altaGerenteDAO.insertaGerente(bean);		
		} catch (Exception e) {
			logger.info("No fue posible insertar el gerente: "+e);
			
		}
		
		return respuesta;		
	}

	List<AltaGerenteDTO> lista1 = null;
	
	public List<AltaGerenteDTO> obtieneIdGerente(AltaGerenteDTO bean){
				
		try{
			lista1 = altaGerenteDAO.obtieneIdGerente(bean);
		}
		catch(Exception e){
			logger.info("No fue posible obtener el id del Gerente");
					
		}
				
		return lista1;
	}
	
	List<AltaGerenteDTO> lista2 = null;
	
	public List<AltaGerenteDTO> obtieneIdUsuarios(AltaGerenteDTO bean){
				
		try{
			lista2 = altaGerenteDAO.obtieneIdUsuarios(bean);
		}
		catch(Exception e){
			logger.info("No fue posible obtener el id de usuarios");
					
		}
				
		return lista2;
	}
	
	public boolean actualizaDisSup (AltaGerenteDTO bean){
		
		boolean respuesta = false;
				
		try {
			respuesta = altaGerenteDAO.actualizaDisSup(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar informacion");
			
		}
							
		return respuesta;
	}
	
public boolean eliminaDisSup(AltaGerenteDTO bean){
		
		boolean respuesta = false;
		
		try{
			respuesta = altaGerenteDAO.eliminaDisSup(bean);
		}catch(Exception e){
			logger.info("No fue posible borrar Informacion");
			
		}
		
		return respuesta;
	}

List<AltaGerenteDTO> lista3 = null;

public List<AltaGerenteDTO> obtienefullTable(String idUsuario, String idGerente){
			
	try{
		lista3 = altaGerenteDAO.obtienefullTable(idUsuario, idGerente);
	}
	catch(Exception e){
		logger.info("No fue posible obtener informacion");
				
	}
			
	return lista3;
}


}
