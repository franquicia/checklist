package com.gruposalinas.checklist.domain;

public class ConsultaCecoCheckDTO {
	

//consulta  el  cursor CU_CHECKL

		private int idUsuario;
		private int ceco;
		private int checklist;
		//private int totalceco;
		private  int totalcheckl;
		
	
		public int getIdUsuario() {
			return idUsuario;
		}
		public void setIdUsuario(int idUsuario) {
			this.idUsuario = idUsuario;
		}
		
		
		public int getCeco() {
			return ceco;
		}
		public void setCeco(int ceco) {
			this.ceco = ceco;
		}
		public int getChecklist() {
			return checklist;
		}
		public void setChecklist(int checklist) {
			this.checklist = checklist;
		}
		
		public int getTotalcheckl() {
			return totalcheckl;
		}
		public void setTotalcheckl(int totalcheckl) {
			this.totalcheckl = totalcheckl;
		}
		@Override
		public String toString() {
			return "ConsultaCecoCheckDTO [idUsuario=" + idUsuario + ", ceco=" + ceco + ", checklist=" + checklist
					+ " totalcheckl=" + totalcheckl + "]";
		}
		
		
		
		
		
	
	}