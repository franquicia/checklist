package com.gruposalinas.checklist.domain;

public class ProyectFaseTransfDTO {

	private int idProyecto;
	private int idStatus;
	private int fase;
	private String nomProy;
	private String nomFase;
	private int pregPadre;
	private String obsProy;
	private String obsFase;
	private String periodo;
	private String auxProy;
	private String auxFase;
	private int idFaseProy;
	private int idTipoProyecto;
	private int edoCargaIni;
	private int hallazgoEdoMax;
	
	public int getIdProyecto() {
		return idProyecto;
	}


	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}


	public int getIdStatus() {
		return idStatus;
	}


	public void setIdStatus(int idStatus) {
		this.idStatus = idStatus;
	}


	public int getFase() {
		return fase;
	}


	public void setFase(int fase) {
		this.fase = fase;
	}


	public String getNomProy() {
		return nomProy;
	}


	public void setNomProy(String nomProy) {
		this.nomProy = nomProy;
	}


	public String getNomFase() {
		return nomFase;
	}


	public void setNomFase(String nomFase) {
		this.nomFase = nomFase;
	}


	public int getPregPadre() {
		return pregPadre;
	}


	public void setPregPadre(int pregPadre) {
		this.pregPadre = pregPadre;
	}


	public String getObsProy() {
		return obsProy;
	}


	public void setObsProy(String obsProy) {
		this.obsProy = obsProy;
	}


	public String getObsFase() {
		return obsFase;
	}


	public void setObsFase(String obsFase) {
		this.obsFase = obsFase;
	}


	public String getPeriodo() {
		return periodo;
	}


	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}


	public String getAuxProy() {
		return auxProy;
	}


	public void setAuxProy(String auxProy) {
		this.auxProy = auxProy;
	}


	public String getAuxFase() {
		return auxFase;
	}


	public void setAuxFase(String auxFase) {
		this.auxFase = auxFase;
	}


	public int getIdFaseProy() {
		return idFaseProy;
	}


	public void setIdFaseProy(int idFaseProy) {
		this.idFaseProy = idFaseProy;
	}


	public int getIdTipoProyecto() {
		return idTipoProyecto;
	}


	public void setIdTipoProyecto(int idTipoProyecto) {
		this.idTipoProyecto = idTipoProyecto;
	}


	public int getEdoCargaIni() {
		return edoCargaIni;
	}


	public void setEdoCargaIni(int edoCargaIni) {
		this.edoCargaIni = edoCargaIni;
	}


	public int getHallazgoEdoMax() {
		return hallazgoEdoMax;
	}


	public void setHallazgoEdoMax(int hallazgoEdoMax) {
		this.hallazgoEdoMax = hallazgoEdoMax;
	}


	@Override
	public String toString() {
		return "ProyectFaseTransfDTO [idProyecto=" + idProyecto + ", idStatus=" + idStatus + ", fase=" + fase
				+ ", nomProy=" + nomProy + ", nomFase=" + nomFase + ", pregPadre=" + pregPadre + ", obsProy=" + obsProy
				+ ", obsFase=" + obsFase + ", periodo=" + periodo + ", auxProy=" + auxProy + ", auxFase=" + auxFase
				+ ", idFaseProy=" + idFaseProy + ", idTipoProyecto=" + idTipoProyecto + ", edoCargaIni=" + edoCargaIni
				+ ", hallazgoEdoMax=" + hallazgoEdoMax + "]";
	}
	

	
}