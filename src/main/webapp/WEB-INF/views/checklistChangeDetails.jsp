<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link rel="stylesheet" href="../css/checklistChangeDesign.css">

<style>
#container{display:inline-block; margin-left: 32%;}
#left{display:inline-block;}
#right{display:inline-block;}
#center{display:inline-block;}
</style>

<input type="hidden" id="idChecklist" name="idChecklist" 
	value="${idCheck}" />
<body onload="initComparaTable(${idCheck})">

<button id="continuar" class="log-btn2" onClick="changeView(this)" style="margin-left:48%;width:35px;height:35px"><img alt="image" src="../images/iconosAdmCheck/view.png" style="width:100%;height:80%"></button>
<!-- contenedor para mostrar el formulario de la definicion del arbol de decision -->

<div id="mainContent">

	<div class="seccionArbol" id="contenedorArbol1"	style="background: #fff; z-index: 0;">
		<h3>Detalle de Checklist Actual</h3>
		<div id="generales"><p  id="generalesData"></p></div>
		<!-- lista donde se mostran las preguntas de la vista previa -->
		<table class="tabla" id="myTable" style = "overflow-y:scroll;">
			<thead>
				<tr>
					<th>Respuesta</th>
					<th>Evidencia</th>
					<th>Tipo de evidencia</th>
					<th>Evidencia obligatoria</th>
					<th>Observaciones</th>
					<th>Acciones</th>
					<th>Pregunta siguiente</th>
					<th>Etiqueta Evidencia</th>
				</tr>

			</thead>
			<tbody id="myTableBody">
			</tbody>
		</table>
	</div>

	<div class="seccionArbol" id="contenedorArbol2"
		style="background: #fff; z-index: 0;">
		<h3>Detalle de Checklist Modificado</h3>
		<!-- lista donde se mostran las preguntas de la vista previa -->
	<div id="generalesNuevo"><p id="generalesDataNuevo"></p></div>

		<table class="tabla"  id="myTableNuevo">
			<thead>
				<tr class="encabezado">
					<th>Respuesta</th>
					<th>Evidencia</th>
					<th>Tipo de evidencia</th>
					<th>Evidencia obligatoria</th>
					<th>Observaciones</th>
					<th>Acciones</th>
					<th>Pregunta siguiente</th>
					<th>Etiqueta Evidencia</th>
					<th>Estatus</th>
				</tr>

			</thead>
			<tbody id="myTableBodyNuevo">
			</tbody>
		</table>
	</div>
</div>
</body>	
<div id="container">
	<div id="left">
		<form name="formulario" method="POST"
			action="../central/resumenCheck.htm" id="formActivaAdmin">
			<a onclick="resumen()"><button id="continuar" class="log-btn2"
					style="margin-left: 45%;">Regresar</button></a> <input type="hidden"
				name="idUserAdmin" value="adminCheckchecklists">
		</form>
	</div>
	<div id="right">
		<form name="formulario" method="POST" action="../central/autorizaCambiosCheck.htm" id="formActivaAdmin">
			<button id="continuar" class="log-btn2" style="margin-left: 45%;">Autorizar</button>
			 <input type="hidden" name="tipo" value="true">
			 <input type="hidden" name="idCheck" value="${idCheck}">
			 <input type="hidden" name="nombreCheck" value="${nombreCheck}">
		</form>	
	</div>
	<div id="center">
		<form name="formulario" method="POST" action="../central/autorizaCambiosCheck.htm" id="formActivaAdmin">
			<button id="continuar" class="log-btn2" style="margin-left: 45%;">Rechazar</button>
			 <input type="hidden" name="tipo" value="false">
			 <input type="hidden" name="idCheck" value="${idCheck}">
			  <input type="hidden" name="nombreCheck" value="${nombreCheck}">
		</form>
	</div>
</div>