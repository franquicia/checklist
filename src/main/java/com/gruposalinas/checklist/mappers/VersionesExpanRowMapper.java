package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.VersionExpanDTO;

public class VersionesExpanRowMapper implements RowMapper<VersionExpanDTO> {

	public VersionExpanDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		VersionExpanDTO VersionExpanDTO = new VersionExpanDTO();
		
		VersionExpanDTO.setIdTab(rs.getInt("FIID_VERSCHEC"));
		VersionExpanDTO.setIdCheck(rs.getInt("FIID_CHECKLIST"));
		VersionExpanDTO.setIdVers(rs.getInt("FIVERSION"));
		VersionExpanDTO.setIdactivo(rs.getInt("FIIDACTIVO"));
	    VersionExpanDTO.setAux(rs.getString("FCAUX"));
	    VersionExpanDTO.setObs(rs.getString("FCOBSERVACION"));
	    VersionExpanDTO.setPeriodo(rs.getString("FCPERIODO"));

		return VersionExpanDTO;
	}

}


