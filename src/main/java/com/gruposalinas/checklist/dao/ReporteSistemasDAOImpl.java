package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ReporteSistemasDTO;
import com.gruposalinas.checklist.mappers.ReporteCecosSistRowMapper;
import com.gruposalinas.checklist.mappers.ReporteCheckSistRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ReporteSistemasDAOImpl extends DefaultDAO implements ReporteSistemasDAO {

    private static final Logger logger = LogManager.getLogger(ReporteSistemasDAOImpl.class);

    private DefaultJdbcCall jdbcReporteChecks;
    private DefaultJdbcCall jdbcReporteCecos;

    public void init() {
        jdbcReporteChecks = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPSISTEMAS")
                .withProcedureName("SP_REPORTE_CONT_C")
                .returningResultSet("RCL_CHECK", new ReporteCheckSistRowMapper());

        jdbcReporteCecos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPSISTEMAS")
                .withProcedureName("SP_REPORTE_SUPER")
                .returningResultSet("RCL_PORCENTAJE", new ReporteCecosSistRowMapper());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReporteSistemasDTO> ReporteChecklistSistemas(int idCeco) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<ReporteSistemasDTO> listaChecklist = null;
        int respuesta = 0;
        int porcentaje = 0;
        int ceco = 0;
        int conteo = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", idCeco);

        out = jdbcReporteChecks.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAVISTAC.SP_REPORTE_VC}");

        listaChecklist = (List<ReporteSistemasDTO>) out.get("RCL_CHECK");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("El Reporte de Sistemas retorno error");
        }
        return listaChecklist;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReporteSistemasDTO> ReporteCecosSistemas(int idCeco, int idChecklist, String fecha) throws Exception {
        Map<String, Object> out = null;
        List<ReporteSistemasDTO> listaConteo = null;
        int respuesta = 0;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCECO", idCeco)
                .addValue("PA_IDCHECKLIST", idChecklist)
                .addValue("PA_FECHA", fecha);

        out = jdbcReporteCecos.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAVISTAC.SP_REPORTE_VC1}");

        listaConteo = (List<ReporteSistemasDTO>) out.get("RCL_PORCENTAJE");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("El Reporte de Cecos de Sistemas retorno Error");
        }

        return listaConteo;
    }
}
