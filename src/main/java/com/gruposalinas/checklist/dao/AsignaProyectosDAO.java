package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.AsignaTransfDTO;


public interface AsignaProyectosDAO {

	   public int insertaAsignacionProy(AsignaTransfDTO bean)throws Exception;
		
		public boolean desactivaAsignacionProy(String ceco,String usuario,String agrupa)throws Exception;		
	
		public List<AsignaTransfDTO> obtieneProyectosAdm(String ceco) throws Exception; 
		
		public List<AsignaTransfDTO> obtieneFasesAdm(String ceco,String proyecto) throws Exception; 
		
		public List<AsignaTransfDTO> obtieneAsignacionesProy(String ceco,String proyecto) throws Exception; 
		
		public boolean liberaFaseProy(AsignaTransfDTO bean)throws Exception;
		
		public List<AsignaTransfDTO> obtieneDatosLiberaFase(String ceco,String proyecto ) throws Exception; 
		
	}