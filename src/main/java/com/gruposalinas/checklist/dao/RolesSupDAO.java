package com.gruposalinas.checklist.dao;

import java.util.List;
import com.gruposalinas.checklist.domain.RolesSupDTO;

public interface RolesSupDAO {
	
	public int insertRoles(int idUsuario, int idPerfil, int idMenu, int activo) throws Exception;
	public int updateRoles(int idUsuario, int idPerfil, int idMenu, String activo) throws Exception;
	public int deleteRoles(int idUsuario, int idPerfil, int idMenu) throws Exception;
	public List<RolesSupDTO> getRoles(String idUsuario, String idPerfil, String idMenu) throws Exception;
	public int mergeRoles(int idUsuario, int idPerfil, int idMenu, int activo) throws Exception;

}
