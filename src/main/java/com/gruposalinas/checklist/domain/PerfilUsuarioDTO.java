package com.gruposalinas.checklist.domain;

public class PerfilUsuarioDTO {

	private int idUsuario;
	private int idPerfil;
	private String descripcion;
	private String idActor;
	private String bandNvoEsq;
	
	public int getIdUsuario() {
	return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
	this.idUsuario = idUsuario;
	}
	public int getIdPerfil() {
	return idPerfil;
	}
	public void setIdPerfil(int idPerfil) {
	this.idPerfil = idPerfil;
	}
	public String getDescripcion() {
	return descripcion;
	}
	public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
	}
	
	public String getIdActor() {
		return idActor;
	}
	public void setIdActor(String idActor) {
		this.idActor = idActor;
	}
	public String getBandNvoEsq() {
		return bandNvoEsq;
	}
	public void setBandNvoEsq(String bandNvoEsq) {
		this.bandNvoEsq = bandNvoEsq;
	}
	@Override
	public String toString() {
		return "PerfilUsuarioDTO [idUsuario=" + idUsuario + ", idPerfil=" + idPerfil + ", descripcion=" + descripcion
				+ ", idActor=" + idActor + ", bandNvoEsq=" + bandNvoEsq + "]";
	}

	
}

