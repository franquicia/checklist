<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html lang="es">
<head>
<title>Banco Azteca | Sistematización de actividades</title> 
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/vistaCumplimientoVisitas/estilo.css" />
<script type="text/javascript">
	var conteoL = ${conteo};
	var nivelPerfilL = ${nivelPerfil};
	var porcentajeG = ${porcentajeG};
	var idCecoPadreL = ${idCecoPadre};
	var nombreCecoL = '${nombreCeco}';
	var banderaCecoPadreL = ${banderaCecoPadre};	
	var seleccionAnoL = ${seleccionAno};
	var seleccionMesL = ${seleccionMes};
	var seleccionCanalL = ${seleccionCanal};
	var seleccionPaisL = ${seleccionPais};
	var url = '${url}';
</script>	

<link rel="stylesheet" media="screen" href="../css/vistaCumplimientoVisitas/modal.css">
	
<!-- jquery Start -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/vistaCumplimientoVisitas/jquery.js"></script>
	
<!-- dropdown -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/vistaCumplimientoVisitas/menu.js"></script>
	
<!-- dropdown -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/vistaCumplimientoVisitas/script-filtroCumplimientoVisitasReg.js"></script>
	
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- <link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/vistaCumplimientoVisitas/tooltipster.bundle.css" /> -->


	<style type="text/css">
			.nombreCheck {
			}
	</style>
	<% response.addHeader("X-Frame-Options", "SAMEORIGIN"); %>
</head>
<body onload="nobackbutton();">


	<input type="hidden" id="longitudRegiones" value="${(fn:length(listaSucursales))}"></input>	
	
	<div class="header" style="z-index: 10; top: -40px;">
		
		<div class="wrapperFixed" style="background:red; background:#f2f2f2;">
		
			<div class="goborder">
				<h2>
					<a id="idPais" style="color:#747474"></a><span class="subSeccion">/<a id="idOpcTerr" style="color:#747474"></a></span>
				</h2>
			</div>

			<table class="avance" style="max-width: 800px; min-width:560px">
				<thead>
					<tr>
						<th colspan="3">SISTEMATIZACIÓN DE ACTVIDADES <br>Información en tiempo real </th>
					</tr>
					<tr>
						<th colspan="3">Objetivo al d&iacute;a <a id="idDiaActual" style="color:#9e9e9e;">0</a></th>
					</tr>
					<tr>
						<th>Día 1 0%</th>
						<th class="w100">
							<div class="w3-light-grey w3-round">
								<div class="w3-containerN w3-blue" style="width: 1%" id="idBarPor">&nbsp;</div>
							</div>
						</th>
						<th>Día <a id="idTotalDias" style="color:#9e9e9e;">0</a> 100%</th>
					</tr>
				</thead>
			</table>

			<div class="titulos">
				<table class="titulosMargen">
					<tbody>
						<tr >
							<td class="primerTitulo">
							</td>
							<td class="segundoTitulo"> INDICADORES
							</td>
							<td class="tercerTitulo"> CHECKLIST
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<table class="bloquer" style="max-width: 800px; min-width:560px">
				<thead>
					<tr>
						<th>Zona</th>
						
						<th style="width: ${32/conteo}%;">Consumo</th>
						<th style="width: ${32/conteo}%;">Personales</th>
						
						<c:forEach items="${listaChecklist}" var="item">
							<c:if test="${item.idChecklist == 97 || item.idChecklist == 99}">
							    <th style="width: ${32/conteo}%;">${item.nombreCheck}</th>
							 </c:if>
						</c:forEach>
						
						
					</tr>
				</thead>
			</table>
		</div>
	</div>
	
	
	   
	
	<div class="wrapper">
		<div class="hspacer"></div>

		<div class="box" style="margin-top: 80px;">
		
		 <form id="formTerri" method="POST" action="vistaTerritCumplimientoVisitas.htm" >	
				<dl class="accordion3" id="idGeneralTablaTerritorios">
				
				<c:if test="${nivelPerfil==0}">
					<c:forEach var = "i" begin = "0" end = "${(fn:length(listaSucursales))-1}">
						<dt onclick="despliega(${listaSucursales[i][0].idCeco})">
							<table class="bloquer linea goshadow1">
								<tbody>
									<tr class="pointer">
										<td class="nombreCheck">${listaSucursales[i][0].descCeco} <span>${listaSucursales[i][0].numSuc} suc</span></td>
										
										<td class="nombreCheck" style="width: ${32/conte}%;" id="f0${i}"><div class="radio rojo"><b class="colorF"></b>%</div></td>
   										<td class="nombreCheck" style="width: ${32/conte}%;" id="f1${i}"><div class="radio rojo"><b class="colorF"></b>%</div></td>
								
								
										<c:forEach var="k" begin="0" end="${(fn:length(listaSucursales[i]))-1}">
												<c:if test="${listaSucursales[i][k].total<(porcentajeG-5)}">
													<td class="nombreCheck" style="width: ${32/conteo}%;"><div class="radio rojo">${listaSucursales[i][k].total}%</div></td>
												</c:if>
												<c:if test="${listaSucursales[i][k].total>=(porcentajeG-5) && listaSucursales[i][k].total<(porcentajeG)}">
													<td class="nombreCheck" style="width: ${32/conteo}%;"><div class="radio amarilloN" style="color:black;">${listaSucursales[i][k].total}%</div></td>
												</c:if>
												<c:if test="${listaSucursales[i][k].total>=porcentajeG}">
													<td class="nombreCheck" style="width: ${32/conteo}%;"><div class="radio verdeN">${listaSucursales[i][k].total}%</div></td>
												</c:if>
										</c:forEach>
										<input type="hidden" id="posicion${i}" value="${listaSucursales[i][0].idCeco}"></input>	
   										<c:set var = "idCeco" scope = "session" value = "${listaSucursales[i][0].idCeco}"/>
   										
   										
										<script>			
											pPersonales(${i},${listaSucursales[i][0].idCeco},'${listaSucursales[i][0].descCeco}',${(fn:length(listaSucursales))-1});	
										</script>
									</tr>
								</tbody>
							</table>
						</dt>
						
						
						<dd>
							<div class="inner" id="idLlenaZonas${idCeco}"></div>	
						</dd>						
					</c:forEach>
				</c:if>
				</dl>
			</form>
		</div>
		<div class="fspacer"></div>
	</div>
	

	<!-- progressbar -->
	<script>
		function move() {
			var elem = document.getElementById("myBar");
			var width = 1;
			var id = setInterval(frame, 10);
			function frame() {
				if (width >= 100) {
					clearInterval(id);
				} else {
					width++;
					elem.style.width = width + "%";
				}
			}
		}
	</script>


	<script type="text/javascript">
		jQuery(function() {
			$(".subSeccion").hide();

		  var allPanels = $('.accordion3 > dd').hide();

		  jQuery('.accordion3 > dt').on('click', function() {
		    $this = $(this);
		    //the target panel content
		    $target = $this.next();
			 
		    
		    jQuery('.accordion3 > dt').removeClass('accordion3-active');
		    if ($target.hasClass("in")) {
		      $this.removeClass('accordion3-active');
		      $target.slideUp();
		      $target.removeClass("in");
				$(".subSeccion").hide();
				
		    } else {
		      $this.addClass('accordion3-active');
		      jQuery('.accordion3 > dd').removeClass("in");
		      $target.addClass("in");
				$(".subSeccion").show();
				

		      jQuery('.accordion3 > dd').slideUp();
		      $target.slideDown();
		    }
		  })
		})
	</script>

	<div id="tipoModal" class="modal3"></div>
	
</body>
</html>
