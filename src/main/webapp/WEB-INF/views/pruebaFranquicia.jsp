<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta charset="UTF-8">
		<title>.:: checklist ::.</title>
	</head>

	<body>
		<div style="margin: 0; padding: 0" marginwidth="0" marginheight="0" bgcolor="#F1F1F1">
			<table bgcolor="#ffffff" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0">
				<tbody>
					
					<tr>
						<td width="100%" bgcolor="#ffffff">
							<table width="100%" border="0">
								<tbody>
									<tr>
										<td width="110%" align="center">
											<img class="CToWUd" width="96px" height="96px" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; max-width: 100%; margin: 0; padding: 0; border: none" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAe1ElEQVR4nOWdeZQdV3ngf7e6et/3ltyt7nZ3q7XZsoSNF2yPcBiMcbyQmER2Tmx8CHPiA8QYckLiYTKegIeBwxkcwIGTZWyYwDhMBm9DWOJFMcHGVmxLsvZd6pZard737tfv1Z0/arv3VtV71TJmck4+neqqust3v/tt97vLKwnywFf2vn67gNuEYBvQJRGADPIlgBDug3BT3FyhlJHKuwQh3DTp4fKypFLMfwxbCov7GTJoRi1lviW9y5D2IMPNyUhHryC0m/cigvLRcsJv4STIHRLx9MP9Vz5FAoi4xC/u2bnNQjyGoCupI4iYNBTGSOM9CQ8RkUYZn1g+rnYeev1EoSqKXjLjOEF5pWrMQ7QnAhShyjBVypMO3Pvl9VfviK2jwhd2vfZVBJ8y0/MLwFVNjdkyJg2JjJF5hO0KbqUbkbSwvAyb9MsYFhPilLFK4INpASJWRaMgIlLyrFuG7xLxyH/fcM0DWj315aE3f/EY8BE/R3ULeifCpLAxoafJmLRIXRm6MJUx+azLBKHU863OZJrn8eIZrwpEsCxzOvo0AhAqI3XXqgrAQ/b4n2+89l61NACfe+Plrwr4VN6OC/09IgCpMNajPhwDQmmaluM+xViAqgCkYWB+t7gyAQivC1J7jxDlv5mCMj2Rl+YlP/KNTdc/EBT743/5520IXgxrmk7BHFRJZIzWyZSaHH2Pd0du32MCgRgcekAgkVq9eBwAy4ELEq4DjbUANVG6A28KS9GtSb73W5ds22ED5KR8LCQkjrCYJzVqCZ7DDqqRThA9KVoh/UhCs6oY4at+PWhMei5S7XjUFbnC88cHTRwKDo1HZKU3Sglpdkl51iuJ0PQTBeHi0vjwGNAtHnjtn24H8WSSRsS5iMKRSdQSzDTTrYHPjKjW5XMxJi1S5WisBQqiTit0lVnpxLoUkfgSQZ8n2xWAYgkfsnNS3hYXGeS3g7hy0TTpqZCQItDrcC6hWIcMa6nGpLGpgNClWUHNl2p9qdYwniU5xVJMbCJINcNQ3b0JzSPo7kqtAfI2O+fIbVJEkUUZpTTku5YgyjGnaBpPXZMNmKB7bZVxmhUHzSQrh4xJ1NTFG31lDLNMrP6bKQCBQAoZeB2fI0Ltk+IWhQyDEZWTKigj6jY7h9MlDfqkj9BvUomhVXKljO8EfsOBj/XHhLCRAF+EcXozyX5dxNIQpSXKal33CfUqFp9UXL7UC2vW7Q8Nngv1BkYBOP5Y5ylsUA7RZeekHij6o5KWhsGgmPfYtICBvkAVxgfMiI/XpFFSLRMwDRkGAIa1S50r7oBt4jDLAo5v614jAolDuMAQUqi6VT/NfQ/4JkDIqGtUcdk5U4vNaM0gMCiX8K5qVLS+SrKXKTzhSAKXBo4iPD9y0NeZpCMN5kY7qrlBTfNVtxqSglQEENAiPEuWWp9UGkUQ6krlHfBoFP6objBSIE0BKINi4H9ND1YgTfi0qV4wDhRN1CwlzNatR2Woz/iQzihm3Q/rEVGcdfn9DUEY9BgSNfyjmS4D0oQVZqucE4DtSD0n8IHBTWo1pO+yhFrG+6sKWfq149mvkR76FJ0Gv01fGoEGqgzUBz01V7UgPUzQn1W0joJJKLXd9sP0OBqFSqNQwhgpXFekBnxeuGoHLIpasGGiErwBR7qUBvGslATRgJtkIXGIn4j53LK0TmhL1AhFkIbemhMxyx0IcVvUCQ8G7JBhoU2GCqOuqDvSpR4hkVZIo5AyoFHG0OhTHqiGVIIQS2LpJhiwI3BBcbrhK6YbcUk9XXswvYCjEILm58PyDma0HURfQndyOt1CSZQYHjQhMlPxqaYrEMKPZDwRhKM3MufRKEIMUnr0JdGolQ15EO2tm2A7hu0GUgzSZJjmJymaHOdgpFpOSC1i0Efo+ImY5RGvekepLaQIDVugHgYxwdTJcWN5PTVOpaQS/bgDsfRolBqNbo8iNFqqlgulrN6a736kBFvihCGarwQQNX0gnI24muKkmIipPtRdulE7LULfqbHBz/ba0akIFVjBgoFDZab0XQLgiTegWWeXGqMT+CeVRt/NmcFKXhq1fhM6Bt8CJKqW+ixNngcIvJBLxrglBUPgA4UwhCoCkem+U+m3BD8KC4bJ4OZK0+27sYGi0SOjGQbzg20LjymWwjjHc0nCSqbRjU4FQnhjnkqjbwWO4w3MhtOSAtsJnmWAWCni3rVeeSZpEBQFdZBS0jTsYZnW0nJ6qmrorqiir6qW9vJKwN0inMkuM5PNMLmcYSyzxPDiAsNLC5yanwUknRVVtJaW01pWTmNJGXXFJVTbxVTbJZRYFkLAwPwcR2anODE/w7HZaYaXFjRmmn1GghBeXx0Zug2iZUMFCQMaNyNUUEdpKAhekNhq2Bn4fmVc0BASVgQUrbpQCBEPL80zvDTPy2Pue5EQ9FTWsLm2kS117iVSNiiRHJqZ4oWRs+yeGuPY3DRmsJGavCCc9JhXAEdQViE1YL7wrdbnn8D2JzMBU5VZpeu/lFgTpUjQmxV1KYbceHCk5ODMJCfnZxheWmA+l2VTTQMllpUXY8Zx2Ds9zkujQ7wyPsxiLpdQMo0wQ2cVN5/RxtsEJQ3mFcIPZdWmJeK2F57VAxFV87WGlEyhpr0diGeCQHBZXQPvb2nn8vrmRKaHWhmPJ+M47JwY4afnB9g1OR7LxEIQKKCqpEpz0nhX80M3rVtEwEYBtjfWhegiS81hyIRmDG+X+XjthG+Vts1NrR18oLWD1tLyxFrjmSWePXeKnwwPAHBjawe3tHXSUFKqlSuxLN7T2Mp7GlsZXlrgx8MD/Gh4gLnsMqn9p5Q42gCe1JOE/qkFzCYliFteeCZ2QTfWvKKTubcHnlY1lJRy+6oubmrtoLzITiw+mlnkicFjPHf+DFk/+vGIsIXF+1ouYnt7D02lZYk4FnJZfjQ8yFNnTzKeWSpMo+W5EX/uA5rS+P1QSIlltBsF6fkCEDe/8EyEj8nSVDBrwrgwUVTbxdxx0cXcsqozr2+fXs7wvYGj/Hh4MGR8AtjC4gOt7dzV0UtNcUliuYzj8MzQKf7+zHFmssvJCP1+KgIwFdF0Lzo3pIIjLCi8Caq4+fmnNSuJQxqN8b27SChQAIqE4NdXdfI7a3qpzKPxjpQ8M3SK7w0e89xGAiExUGkXc1dHD7eu6sTKc7hnLpflu6eP8uy5U5hL80Co3YYANBJi/HtEOQURyxACxAcLCUDGm1bheUA8bKyp5xM9G+msqMpb7ujcNF87upejc9PJhVIQ0FtVwx/0bKK3qiZvuVPzs3z9+F72TU/oGcL7owYe+TRe5VHgvr09gSQBqNLS1vXV44VEEawESqwiPtK5lltXd+ZFI5E8MXCM/zVwLF4jY2oUgiIhuLOjl+0dPXnnEhJ45uxJvn3qMEuOG75KRQBBa+oeuslwI90vr1qKVuymQABSr6mM4NGVnpVBZ0U1D67fQoc3u02CscwSXzz4JvtNLfwlwYaaev5k3RYajWjJhIGFOb548E1Ozs2ETI0wOmYyKqLeImB8XBgKiA8891TkBJK+H/v2mP++lnY+3ruRUqsob7kD0xN84eAbTKSJTBRYKWX1JaV8bt1WNtTU5y235OT4xtF9PDcyGCYqwsgX30vjWRWiUPIEgqLeu7c/pB1w9Wqp5nQh7BfAvV39/F73emyRf/b6s9EhHtr/OnO57AW0tDJYzOV44fxZ1lRUsSbPOGQLi2saWyktKuLNyVF9sA14I/Rl8gJjg1DrS28MuPG5pySqxvsrETGuMq0gbGHx2f7NXNe0qmDZH547zTeO7ks9S20sKWNrfRO9lTXUea5kMrPE0blp3pgYZSyzmAqPQPCJ3o3c3LamYNmXRof40uHdZIn+dqDQ2KC6pEgwJsBWmR83wV2p9tvC4nPrt3JVQ0vBsi7z96Zqo6uymrvXrOXqxpbEgVQieWXsPN85fZgTczN58UkkXz+6F6CgEK5vWkWJVcTnD72ROA8Ryl/1oJvvwOMoFkBRz+9ufyg6eXBzpVGzEKME8OC6LVzT2FqgJPzz6Dm+cnhPKpx3dvTyYP8W1lRU5Y1iBIKOiio+2LYGiWTv9HhBOl4bH6GrojqvOwJoL69kTUUVL42di412dKvwR133EnFlvGIWKIyV4c2MANNo6ce613NtU1vBcodnp/jy4d0F3Y4APt13Kfd0rs07mTLBEoJ7Ovv5dN/m8KRCwiWl5MuHdnF4ZrIg3usa2/hY17rUdASdyLN+Y+nvxoYCxn6wvwkac723aRW/eVF3QXpms8s8fOANlnLZvPiQkrvXrOX9re0r67AC729t5+7O/oLllpwcDx98k/kUQcAdq7u5oXGVd+KtwIV3z7NiYLlMVrYXvT+REz3K5rx5tZVVcH/fJQWJB/jW8f0MLc7nU0oksLa6jjvX9KbCmQ/uXNNLf3VdwfaGFuf5i2P7UuG8v+cS2soq3jZtAJaDJ7CAGHeDOXiXEkdKHJIF8IneTXlXMX3YPTXGT4cHCzJDAh/tWvd2Jt4BCOCj3etStfnT4UH2TI0VxFleZPPJizelwunzVPsn/TtYjs9g78rhMtuR7ln5XJAHZllHSt5d38IV9c2pmPHXJw4q+JKv9vJKLqtrXAmf88Lm2kbayyuD/iRdWSn5yxMHU+G8vK6ZK+tbCvYl5J1yQcDPUAAK4/1Lmnkx110p3cSbk6PsnR7Pi8u/0oSwK4VrGltdzStw7Zsa583J0VQ472zvjdIvYvqkeZHQAhwkVlgIcoF0/MoYQtEl2VdVx8YCU3ofnh06HdWEhKu/uu7COZ0AfVW1qYTvIHl26FQqnBur6+mrrMMpNB4TuhzVAqTEPRUhEeGKXXBKwo+f3PeYAZzrGguHnOAK9OWxYYKDwAWgqSR5R+tCoam0LHX7Pq1pQt/rG9s4ODMRy58AvEjI/BG9BPdcUHisTxI3m/NxmL8O3FrfVJBAgCOz08xkM6nKAtgFTj5cCBQLKzixWghmshmOzE6lssQttU158XrcxD/YHKqyO0GLHM6NnBNUH412Cm2q+HBmYTa19gFMLacXVlqYXM7gpCeBMwtzqQTQWV4V6ZtqNxpffSUXYZLt+PF/wmQhH81VdnFBAgHmc7kVCeD43DRX/pIH4uPK4aw0MJtyZbbKLg76VnCH1l/2lyJY9LeCmN8YOPwB2J+QuQMz+pWyQ7YQ0bp5rp0TI6nwrgR2TpyPxuN5/hWnXPrwgxWZ0BcJwZFRN9JS51rS+4WMuv+ridF3T75b0hk+tDjPRQV2uQBWlVfgFDjNoMLLY+cYzyxFzvlcKIxnllYUBACsLivcL3B5IKU5CugH8s29SBGcvfLDUDXe157V8DBqAbtTzBoBeiprYAVWsOQ4PH7qUCrcaeDxU4dYcpzU7SMEPQU28X3YMz1m1BeeNaj9NfipvEdmwv4/LR0nMklzpOS582dSEVllF7O5tiHlrNG9vnv6KAdSrFAWggMzE3z39BEc6aS+Ntc2pB7fnh85o82RpNaPME0m8NbyI/1w0uVNyKSeFrcU8fz5M94R8cJw66quFQkg4+R4YPfLjCyl2+GKg9HMIg/sfoWM46SeBDoSbm7rTIX/9MIsL4ycCXgjjT7IIM0bD2Jm3pa/5qNXdLxnR2eMMWPMSoc/O/B6quj6llWddFRUpp6NOkgGFma5+19e4HRKIWvMmZ/l7p0vMLAwu6I2OyoquX11V0H8Evj8oddZVpYYVDzqu5QS6Uh0JXaFXdR414ce8hEmXWF+dEV0YGGW+VyW9xSYFVtCsKGmnqeHTuL/Oj/NNbmc4cmzJ6i2S9hQU19wdpqTkr8bPMYDe17m/NJC6nYk7gTwa5ddmyqw+MqRXTw7fMrgj7KQYDxjbt5L9y7WPvMdJd7RwUyL26j34Xc6+nhw3WUFT0A8f/4MD+x5JTj4tBLoKK/iNy7q5vqmNtZV1wVtZaXDwZlJXho9xw/OnGBgYeUWU2oV8dVLr+bXWi7KWy4rHb54eBffGzwSMFU/ahJ9Vg5RmA+I3me+I1VWq4ewViIAgA3V9fzp+nfxrrr8SxT7Zyb47N5XOfg2BtkiIaiyixEIZrKZFU2yTFhXXceXNl3Jhur8C4tvTI3yZwdf58DsBD5nTZ7GMV2omR6Zwc8OLn7624kC8GvF/TAhH1zT2MpdHX28t3l14oGsnJT88Nxpnhg8ys7x86nGkVi4wIoCuKKhhe0dvdzctoaiBNe25OTYMXKW7w0c4ZWJ4bByjAAiz/7d/7KXcURCAKL76cckaD+i9BgenVjHfXIyH1QU2VxR38yWuib6q2vprKimqbSMhuJSzZcfm5vmJ8MD/PjcAHvfoWOJPmysqeemtg5ubO1w5yceOFIyvrzE6NIip+ZnODQzxZuTo+ycGGHB8ZYlAsZ6rsc4HZEoAGHqrgjW3ETnU48FHJZGmSBN6mn/piCWsVEBxD4rAggSdNTYwY/IpOFmZIx1X7ibfcfgnSTJXzvT+Wa66Pz13ego+r0h/yyW7S8OBSD12nFW8a8J3sbYWxCU3+eFaQWeVfGE3w0KhwGzkO2vkUcG2piOvZOd/dcIplPQmZv87D/5n6gJV6FVrRb+ryRjzqf5Xim2iX9jcIFWrwY8UkvxwRWE97WUqAvStd2fB0cHkTQEKtMVvS3V3LyP9uSd6KZuy39SNDAvojA4L9hETAEnJyPZkXuwI0ZAkzsGGI4+mIJ547LybW3MPWG3qEqRMsvwKglh7IWagg1kINWbBuFmtukfk94VWuMM12BGSI7hDWLGgFjXHEnUtV2AftBZ2R+2kzaUg3WMhDg0bEIf2QMqgzA2nlmFtdJPVS0vMjePvOdnhZfmCzLJGpUYXG8/nsxCzjkSSCk1bFPjtO/um+iVGcN8Lst/3HA51zSvptSyqLRLeH74NN8/fZTta/q4qqkNS7hfQ3ly8Dj/e+Ao45lFZrIZNtQ28On+y+iqrOFvTx7iB4PHmMtmuKKhlf+y6UqKheD+XT9j1+QIVza28Zm1W/i/Qyf5zokD9NXU89Hu9fyHnk3UF5cymlnksRP7+ZsT+zm3OM9HuzdwXdNq/vzIbl4dP0eNXcLv92xia0MLf7rvF5QKmz9e9y5eGj3LXx/fR1WxzSd7N/OHa7dQLCx+PnaOLx16ncnMEp9dv5WfnDvNN4/vpbG4nLbycv6wbytrKqr59qkDPDt0AltYKYIT6X0rOxrRWuo+aLB0qt2VlT7CITvnSPprGlhfU8/+6XFeHB7krckxFp0sVze3UVtcwvNDg0wtZ/hvm6/m/v5LKRKC3OIC66rqubGtk23NF7F9zVoa7GIyuRxrKqvpq67l+paL+O2OPhpLyhESttQ30VxWBgK+cMlV/EHfZh498hbbdjzF4ycO8Jm1W/j8pquwhcWqskq21DdTXVzCsuNq78WVtWyta6bcsqmwbbbWt9BcWo4lBP9pw7t5cN3lPDFwhDt+8SMOzkxwXfNqSoqKuLyuhY7yKnK5HA4OraXl/G5nP91VNXyqbzMZJ4eM+yd1nobaGy3nfq5GKJ/kUoNVbT019MU+KksIppaX+atj+zk4PcGyk6OxtIy64jJ2T4zw9aO7aTtTyYbam2kuLcfJLtNYXcsda3oZmJ/l5yNn6a+uY31dE8fmZ9y1ESHYMzXGrau72TkxzNGZSRwJmZzDjW2d3NDczteP7OHz+1+jzLbZMzlCTXEJ72vt4OrGNhDuejvehgci3It2JCAlWSeHAK5qbOXfNa7mjYkRvnp4F3unx/inkTPkkPzG6p5A8XAcWkrLueOiXuZzWb52ZBf/ddPVrKup5+TcNEFoqai35vKDP35gEBZ0d8RkqNtBozGeTc2XQpJzJLXFxXysZwMPX3o11zavptSymcossam2kT9a9y7+5oobyDqS54YHmVheZHNdEz2VNTx95hj/89QhyuxiPri6k5qSEkAyk8nwV8f3Mba8yCd7N9Nf08DUcgYpJd2VNSzLHIdmJlh2JMVWEROL8xyfnaKuuJTmsnLlOKB7ORLlYxt+mqQIuLiyhobSMvZMjXFyfpqKouAXW2F9j29NpWVcXt/C3546yJNnjgPw2x19Ub4Eeu49RzyIz283zfI3idXzi1KG23ORYc7voAMSh7Iimy31LdzQ0k5HRTW2JVh0ctSXlPKbHT1sqW/mh2dP8OyZ45DLsbW+ha6qGsqLbKrtEhwpeU/TamrtMpZyDsWWxaGpCf7H8QNsrmviprZOpJTKcrP66czgSTN01dR9vcPruJ/g1hGBVkZ2sDTHLuiqqGZrfQtnF+a5oqGFgzOT3NneH/BIV15df02l8IWAlF4YqjWl1jZWMKTaUYEtLM4vLPD7O18MXFBDaTmtpRX84/lB/vNbr/KFS67iI93reW38PC8WnWFTnbvh/bkNVwRtDi3Oc21zG6WWjQNYTo7/c+ow93av597u9WQdh68d2cN8bpkau4T28mqQDktOjpaKajbXNTK8NMfR2UnWVtdRUmRTWVwSuIHKIhtLiOAMj8B9Prc4z0w2Q3dlDW2lFRyfm6bYtjRhOtKhuaKCm1d1A/DQxncHdC/kstzU1sk/DJ00+KcgEG5gExvB4/9KUonl9TOhvm9zv4kffqod1wTUkqqEBZRaFmNLC3z/9GE+1H4xVze2UVNcwlWNbTx6ZA8P79vJ+NwMt3b38/i7/z03r+rmF+PnyDg5SiyLmcwC3ziyh9XllfRW1SKE5MXhQV6fOM9n+jeTcbL8fHSI7Wv6+HBHH986tpefDR6nrqiYB/ou45M9l5DN5eirquOGlnZeGRtiaHGOjTWNFAn3P2p4ZfQsuydH+HB7H3/UfznfHzzMlvpmZpaXOTo3icA1so6yarY1t/PI0Tf59K6f01hSysWVtbz6a7/FB9s6+aEiAKk8BR8c9BU3yAyD46KSD//6Q6rsgk8SG/FuaOhuRg7JptpGFmWW584NMrW8hARK7CIurW3gyMwkeyZHOTU/S2WxjRCwqrySqeVF/n7wOCfmZlgGFp0clUXFlFhFHJ+dQiB4dWKY0/MzDC7OsaqsgrIim6fOHGf/5Ai7p8ZYVV7J/Wsv456u9XRV1iCRzGaX2T8/zWvj5zk8M8ktqy/mvp5LuLZ5NT84c5w/eesVBhdm6KqsYXV5Jfumx3l5fIhdk6OUFhVxT+d67ulaR3dlDT86d5LT87P0VdXx+uQw5xbnubKhlUePvcX5pQUsy2LZcSiy3D49OXhM09dwsPXmEDHT+2CGXPX9b8ZHseYnFrWqriAiaGMmKSv4cWMUjx+UxeHwZtiLuSwfaOvkW1u3MZJZ5L7XX2TX5Ai2sKIb+PnnU4mZqboQeG1l2uYTrydoT6Ly776ZsNGlLyOFyfEmYk4wVgyxazYxJAW3sHRO5qgtLqW+uIzx5UVms8to+7CmwiRMrFekLJow1RFXoTOiPVHJ2ub2o4ihTv/usToME0ktTHfSskJU3En8MMNkS1jet0WXlTKmXklVSfPg9VuO8cMaf+LqmVXi6utgayj08TgGTdityAJTYh2ThHSiiqzBGJFZurbi6UpDQRKbk3DGp0UZZNa3pZlqlvBM2FwaitUmRblTW4RpEJ5kTa0SahkDpNG2SYvaTl4GKvF2LM44vBqCKGEBnhgcQgYWYNSKxxWbn8cIDIgv5dZXsMh82pBMX3x7CtXep+r9sDodxGlSuvaT+GWWsWMVPkWTK/H/0fFDrx2nYGloyN+eWUOGspUJ9CsDtoni7dCVT9wRC0gjkNiGtQglrmzYMf3/aMzjI/OOR0qCOkURSX3QB1bfNWjWZ7Zg9slvR81PbC+EJCEJvK+l/Coh2RFdaN0LB7Eiu3pnaDQEENOITM6Kg8LFVtDpNEVTlEmKX1bqzWMh9pdXpo+TkWz/Fny+PuKnVTeYIAR9spzk6aUW6kizUNCeHoVHkAWPnivRXJjfozyb6tFQRxljpVEmgUaDAQkoY8uaswz/1Ypr98LBjJD0peI0UJiW5I6lr5Me8tEjlSttsyY+Czi5crJStvYrhvjo51fVrgHpyDhpATtUSSZdSW0kX8nWkI+2NLSshB7170rxFaqXv91U+HZYwNN5ygO/DBf1y9XKZHoSfHReWOmh+8KwAnxPC4DSJx49AXRFsHghcuGBLa5iXKgfznZFnqXiQpB3+u+ViLijpI0OlAW2FUoiCWXe9bgQTk7ddl+3H4bem9jIymjiwrQ9fZ309BTA6YV3/x+1/17woqCl7R/fATyiZa+Ej6nLymhM/A4wP91k752NngrAI1O33bcDDFpLn3j0MeAj/ntBU1dXBvMtG8QsvAhzGVQUQGam5qEr0TP6D772m3OJyCJdAVoMF5S/3QAen7rtvsDjaDPhpe0fvxfTEgpBgpKkC81+Vdp/Ie1egPYXrvKIynxIoLf0iUe3CXgM6LrQnke01SBOCCOxgHrHLsIVJiOa4R1+jbXuuG+KJW6oexbjBypSyzQLnwTu9d1OKnoByp549HYEtwHbMKOkPJhimRURQD5KREJWfnsvEBiF3i6pQqIAwna16Co4hh+L8ySwA3h66vb7nkoi6/8Be3jsWDsYqVsAAAAASUVORK5CYII=">
										</td>
										
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
						
					<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; margin: 0; padding: 0">
						<td bgcolor="#ffffff" style="color: #6d6f71; text-align: center; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 30px; margin: 0 0 0px; line-height: 1.3; padding-bottom: 10px;">
							<strong style="font-size: 23px;">@Titulo</strong>
						</td>
					</tr>	
					
					<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; margin: 0; padding: 0">
						<td bgcolor="#ffffff" style="color: black; text-align:justify; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-weight: 200; font-size: 20px; line-height: 1.3; margin: 0 0 0px; padding-left: 50px; padding-right: 50px; padding-top: 30px;">
							<strong style="font-size: 20px;">Fecha:</strong> @Fecha						
						</td>
					</tr>	
					<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; margin: 0; padding: 0">
						<td bgcolor="#ffffff" style="color: black; text-align:justify; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-weight: 200; font-size: 20px; line-height: 1.3; margin: 0 0 0px; padding-left: 50px; padding-right: 50px;">
							<strong style="font-size: 20px;">Nombre:</strong> @Nombre	
						</td>
					</tr>
					<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; margin: 0; padding: 0">
						<td bgcolor="#ffffff" style="color: black; text-align:justify; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-weight: 200; font-size: 20px; line-height: 1.3; margin: 0 0 0px; padding-left: 50px; padding-right: 50px;">
							<strong style="font-size: 20px;">No. Empleado:</strong> @NoEmpleado	
						</td>
					</tr>
					<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; margin: 0; padding: 0">
						<td bgcolor="#ffffff" style="color: black; text-align:justify; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-weight: 200; font-size: 20px; line-height: 1.3; margin: 0 0 0px; padding-left: 50px; padding-right: 50px;">
							<strong style="font-size: 20px;">Zona o Región o Sucursal:</strong> @Zona
						</td>
					</tr>
					<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; margin: 0; padding: 0">
						<td bgcolor="#ffffff" style="color: black; text-align:justify; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-weight: 200; font-size: 20px; line-height: 1.3; margin: 0 0 0px; padding-left: 50px; padding-right: 50px;">
							<strong style="font-size: 20px;">Teléfono de Contacto:</strong>  @Telefono
						</td>
					</tr>
					<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; margin: 0; padding: 0">
						<td bgcolor="#ffffff" style="color: black; text-align:justify; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-weight: 200; font-size: 20px; line-height: 1.3; margin: 0 0 0px; padding-left: 50px; padding-right: 50px;padding-top: 30px; padding-bottom: 10px;">
							<strong style="font-size: 20px;">Comentario:</strong>
						</td>
					</tr>	
						<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; margin: 0; padding: 0">
						<td bgcolor="#ffffff" style="color: black; text-align:justify; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-weight: 200; font-size: 20px; line-height: 1.3; margin: 0 0 0px; padding-left: 70px; padding-right: 50px;">
							@Comentario
						</td>
					</tr>								
					<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; margin: 0; padding: 0">
						<td bgcolor="#ffffff" style="color: #6d6f71; text-align: center; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-weight: 200; font-size: 18px; margin: 0 0 0px; padding-left: 50px; padding-right: 50px; padding-top: 40px; padding-bottom: 10px;">
							<p>
								Sistemas - checklist.
							</p>
						</td>
					</tr>
					
				</tbody>
			</table>
		</div>
	</body>
</html>