package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.PosiblesTipoPreguntaDAO;
import com.gruposalinas.checklist.domain.PosiblesTipoPreguntaDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class PosibleTipoPreguntaBI {

	
	private Logger logger = LogManager.getLogger(PosibleTipoPreguntaBI.class);
	
	@Autowired
	PosiblesTipoPreguntaDAO posibleTipoPreguntaDAO;
	
	public List<PosiblesTipoPreguntaDTO> obtienePosiblesRespuestas(String idTipoPregunta){
		
		List<PosiblesTipoPreguntaDTO> listaRes = null;
		
		try {
			listaRes = posibleTipoPreguntaDAO.obtienePosiblesRespuestas(idTipoPregunta);
		} catch (Exception e) {
			logger.info("No fue posible obtener las posibles respuestas para el tipo ");
			
		}
		
		return listaRes;
		
	}
	
	public int insertaPosibleTipoPregunta(int idPosible, int idTipoPregunta){
		
		int res= 0;
		
		try {
			res = posibleTipoPreguntaDAO.insertaPosibleTipoPregunta(idPosible, idTipoPregunta);
		} catch (Exception e) {
			logger.info("No fue posible insertar las posibles respuestas para el tipo ");
			
		}
		
		return res;
		
	}
	
	public boolean actualizaPosibleTipoPregunta(PosiblesTipoPreguntaDTO posibleTipoPreguntaDTO){
		
		boolean res = false;
		
		try {
			res = posibleTipoPreguntaDAO.actualizaPosibleTipoPregunta(posibleTipoPreguntaDTO);
		} catch (Exception e) {
			logger.info("No fue posible actualizar las posibles respuestas para el tipo ");
			
		}
		
		return res;
		
	}
	
	public boolean eliminaPosibleTipoPregunta(int idPosibleTipoPregunta){
		boolean res = false;
		
		try {
			res = posibleTipoPreguntaDAO.eliminaPosibleTipoPregunta(idPosibleTipoPregunta);
		} catch (Exception e) {
			logger.info("No fue posible eliminar las posibles respuestas para el tipo ");
			
		}
		
		return res;
	}

}
