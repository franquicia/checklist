package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.RecursoPerfilDTO;
import com.gruposalinas.checklist.mappers.RecursoPerfilRowMapper;
import com.gruposalinas.checklist.mappers.RecursoPerfilUnicoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class RecursoPerfilDAOImpl extends DefaultDAO implements RecursoPerfilDAO {

    private static Logger logger = LogManager.getLogger(RecursoDAOImpl.class);

    private DefaultJdbcCall jdbcBuscaRecursoP;
    private DefaultJdbcCall jdbcInsertaRecursoP;
    private DefaultJdbcCall jdbcActualizaRecursoP;
    private DefaultJdbcCall jdbcEliminaRecursoP;
    private DefaultJdbcCall jdbcObtienePermisos;

    public void init() {

        jdbcBuscaRecursoP = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILREC")
                .withProcedureName("SP_SEL_PR")
                .returningResultSet("RCL_PR", new RecursoPerfilRowMapper());

        jdbcInsertaRecursoP = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILREC")
                .withProcedureName("SP_INS_PR");

        jdbcActualizaRecursoP = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILREC")
                .withProcedureName("SP_ACT_PR");

        jdbcEliminaRecursoP = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILREC")
                .withProcedureName("SP_DEL_PR");

        jdbcObtienePermisos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILREC")
                .withProcedureName("SP_OBTIENE_PERMISOS")
                .returningResultSet("RCL_PERMISOS", new RecursoPerfilUnicoRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<RecursoPerfilDTO> buscaRecursoP(String idRecurso, String idPerfil) throws Exception {
        Map<String, Object> out = null;

        List<RecursoPerfilDTO> listaRecurso = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDRECURSO", idRecurso)
                .addValue("PA_FIIDPERFIL", idPerfil);

        out = jdbcBuscaRecursoP.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMRECURSO.SP_SEL_RECURSO}");

        listaRecurso = (List<RecursoPerfilDTO>) out.get("RCL_PR");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener el Perfil Recurso  con id: " + idRecurso);
        } else {
            return listaRecurso;
        }

        return null;
    }

    @Override
    public boolean insertaRecursoP(RecursoPerfilDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDRECURSO", bean.getIdRecurso())
                .addValue("PA_FIIDPERFIL", bean.getIdPerfil())
                .addValue("PA_FICONSULTA", bean.getConsulta())
                .addValue("PA_FIINSERTA", bean.getInserta())
                .addValue("PA_FIMODIFICA", bean.getModifica())
                .addValue("PA_FIELIMINA", bean.getElimina());

        out = jdbcInsertaRecursoP.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPERFILREC.SP_INS_PR}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar el Recurso Perfil");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaRecursoP(RecursoPerfilDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDRECURSO", bean.getIdRecurso())
                .addValue("PA_FIIDPERFIL", bean.getIdPerfil())
                .addValue("PA_FICONSULTA", bean.getConsulta())
                .addValue("PA_FIINSERTA", bean.getInserta())
                .addValue("PA_FIMODIFICA", bean.getModifica())
                .addValue("PA_FIELIMINA", bean.getElimina());

        out = jdbcActualizaRecursoP.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPERFILREC.SP_ACT_PR}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar Recurso Perfil");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaRecursoP(int idRecursoP, int idPerfil) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDRECURSO", idRecursoP).addValue("PA_FIIDPERFIL", idPerfil);

        out = jdbcEliminaRecursoP.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPERFILREC.SP_DEL_PR}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al eliminar Recurso Perfil");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<RecursoPerfilDTO> obtienePermisos(int idRecurso, int idUsuario) {
        Map<String, Object> out = null;

        List<RecursoPerfilDTO> listaRecurso = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDRECURSO", idRecurso)
                .addValue("PA_USUARIO", idUsuario);

        try {
        out = jdbcObtienePermisos.execute(in);
        } catch (Exception e) {
			e.printStackTrace();
		}

        logger.info("Funcion ejecutada: {checklist.PAADMRECURSO.SP_OBTIENE_PERMISOS}");

        listaRecurso = (List<RecursoPerfilDTO>) out.get("RCL_PERMISOS");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = errorReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener los permisos del recurso para el usuario : " + idUsuario);
        } else {
            return listaRecurso;
        }

        return null;
    }

}
