package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.AsignacionCheckDAO;
import com.gruposalinas.checklist.domain.AsignacionCheckDTO;

public class AsignacionCheckBI {
	
	@Autowired
	AsignacionCheckDAO asignacionCheckDAO;
	
	private Logger logger = LogManager.getLogger(AsignacionBI.class);
	
	
	public List<AsignacionCheckDTO> obtieneAsignaciones(String idChecklist, String idCeco, String idUsuario, String asigna){
		
		List<AsignacionCheckDTO > respuesta = null;
				
		try {
			respuesta = asignacionCheckDAO.obtieneAsignaciones(idChecklist, idCeco, idUsuario, asigna);
		} catch (Exception e) {
			logger.info("No fue posible consultar las asignaciones");
				
		}
		
		return respuesta;
	}
	

	public List<AsignacionCheckDTO> obtieneAsignacionC(int bunker, int pais) {
		
		List<AsignacionCheckDTO > respuesta = null;
				
		try {
			respuesta = asignacionCheckDAO.obtieneAsignacionC(bunker, pais);
		} catch (Exception e) {
			logger.info("No fue posible consultar las asignaciones");
				
		}
		
		return respuesta;
	}
	
	
	public boolean insertaAsignacion(AsignacionCheckDTO asignacion){
		
		boolean respuesta = false;
		
		try {
			respuesta = asignacionCheckDAO.insertaAsignacion(asignacion);
		} catch (Exception e) {
			logger.info("No fue posible insertar la asignacion");
				
		}
		
		return respuesta;
	}
	
	public boolean actualizaAsignacion(AsignacionCheckDTO asignacion){
		
		boolean respuesta = false;
		
		try {
			respuesta = asignacionCheckDAO.actualizaAsignacion(asignacion);
		} catch (Exception e) {
			logger.info("No fue posible actualizar la asignacion");
				
		}
		
		return respuesta;
	}
	
	public boolean eliminarAsignacion(AsignacionCheckDTO asignacion){
		
		boolean respuesta = false;
		
		try {
			respuesta = asignacionCheckDAO.eliminaAsignacion(asignacion);
		} catch (Exception e) {
			logger.info("No fue posible eliminar la asignacion");
				
		}
		
		return respuesta;
	}
	
	public boolean asignaCheclist(){
		
		boolean respuesta = false;
		
		try {
			respuesta = asignacionCheckDAO.asignaCheclist();
		} catch (Exception e) {
			logger.info("No fue posible eliminar la asignacion");
				
		}
		
		return respuesta;
	}
}
