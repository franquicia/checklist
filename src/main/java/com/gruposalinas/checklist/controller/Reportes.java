package com.gruposalinas.checklist.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.business.ReporteBI;
import com.gruposalinas.checklist.business.ReporteIndicadorBI;
import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.CecoIndicadoresDTO;
import com.gruposalinas.checklist.domain.ClienteDTO;
import com.gruposalinas.checklist.domain.NegocioDTO;
import com.gruposalinas.checklist.domain.RH;
import com.gruposalinas.checklist.domain.ReporteIndicadorDTO;
import com.gruposalinas.checklist.domain.VistaCDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.util.UtilCryptoGS;
import com.gruposalinas.checklist.util.UtilDate;

@Controller
@RequestMapping("/reportes")
public class Reportes {

	private static Logger logger = LogManager.getLogger(Reportes.class);

	@Autowired
	CecoBI cecoBI;
	
	@Autowired
	ReporteBI reporteBI;
	
	@Autowired
	ReporteIndicadorBI reporteIndicadorBI;
	
	// ************************************ INICIO REPORTES ************************************
	
	@RequestMapping(value = "/reporteEquipo.htm", method = RequestMethod.GET)
	public ModelAndView reporteEquipo() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException, JSONException {

		
		ModelAndView mv = new ModelAndView("vistaReporteEquipo");
		
		int cecos[] = {236737,236736,236738};
		
		//int cecos[] = {236737};
		
		List<CecoDTO> lista = null;
		
		List<CecoDTO> listaRetorno = new ArrayList<CecoDTO>();
		
		for (int h = 0; h < cecos.length; h++) {
			lista = cecoBI.buscaCecosPasoP(cecos[h]);
		
			for (int i = 0; i < lista.size(); i++) {
				
				CecoDTO cecoDTO = lista.get(i);
				if(cecoDTO.getDescCeco().contains("  "))
					cecoDTO.setDescCeco(cecoDTO.getDescCeco().replace("  ", " "));
				if(cecoDTO.getDescCeco().contains("   "))
					cecoDTO.setDescCeco(cecoDTO.getDescCeco().replace("   ", " "));
				
				logger.info("CECOS HIJOS: " + cecoDTO.getDescCeco());
				logger.info("CECOS HIJO Negocio: " + cecoDTO.getIdNegocio());
				
				listaRetorno.add(cecoDTO);
			}
		}
		
		logger.info("listaCecos.size(): " + listaRetorno.size());
		logger.info("listaCecos.size(): " + listaRetorno.get(0).toString());
		
		mv.addObject("porcentajeL", 80);
		mv.addObject("listaCecos", listaRetorno);
		
		return mv;

	}
	
	@RequestMapping(value = "/reporteCliente.htm", method = RequestMethod.GET)
	public ModelAndView reporteCliente() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException {

		ModelAndView mv = new ModelAndView("vistaReporteCliente");
		
		int cecos[] = {236737,236736,236738};
		//int cecos[] = {236737};
		
		List<CecoDTO> lista = null;
		List<CecoDTO> listaRetorno = new ArrayList<CecoDTO>();
		
		for (int h = 0; h < cecos.length; h++) {
			lista = cecoBI.buscaCecosPasoP(cecos[h]);
		
			
			for (int i = 0; i < lista.size(); i++) {
				CecoDTO cecoDTO = lista.get(i);
				logger.info("CECOS HIJOS: " + cecoDTO.getDescCeco());
				
				listaRetorno.add(cecoDTO);
			}
			
		}
		
		logger.info("listaCecos.size(): " + listaRetorno.size());
		logger.info("listaCecos.size(): " + listaRetorno.get(0).toString());
		
		mv.addObject("porcentajeL", 80);
		mv.addObject("listaCecos", listaRetorno);
	
		return mv;
	}
	
	@RequestMapping(value = "/reporteNegocio.htm", method = RequestMethod.GET)
	public ModelAndView reporteNegocio() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException {

		
		ModelAndView mv = new ModelAndView("vistaReporteNegocio");
		
		int cecos[] = {236737,236736,236738};
		//int cecos[] = {236737};
		
		List<CecoDTO> lista = null;
		
		List<CecoDTO> listaRetorno = new ArrayList<CecoDTO>();
		     
		for (int h = 0; h < cecos.length; h++) {
			lista = cecoBI.buscaCecosPasoP(cecos[h]);
		
			
			for (int i = 0; i < lista.size(); i++) {
				CecoDTO cecoDTO = lista.get(i);
				logger.info("CECOS HIJOS: " + cecoDTO.getDescCeco());
				
				listaRetorno.add(cecoDTO);
			}
			
		}
		
		logger.info("listaCecos.size(): " + listaRetorno.size());
		logger.info("listaCecos.size(): " + listaRetorno.get(0).toString());
		
		mv.addObject("porcentajeL", 80);
		mv.addObject("listaCecos", listaRetorno);
		
		return mv;

	}
	
	// ************************************ FIN REPORTES ************************************
	
	
	// ************************************ INICIO AJAX ************************************
	
	@RequestMapping(value = "/ajaxBuscaCecosPadres", method = RequestMethod.GET)
	public @ResponseBody List<CecoDTO> ajaxBuscaCecosPadres() {
				
		List<CecoDTO> lista = null;
		List<CecoDTO> listaRetorno = new ArrayList<CecoDTO>();
		
		int cecos[] = {236737,236736,236738};
		//int cecos[] = {236737};
				
		for (int h = 0; h < cecos.length; h++) {
			try {
				lista = cecoBI.buscaCecosPasoP(cecos[h]);
				
				for (int i = 0; i < lista.size(); i++) {
					
					CecoDTO cecoDTO = lista.get(i);
					if(cecoDTO.getDescCeco().contains("  "))
						cecoDTO.setDescCeco(cecoDTO.getDescCeco().replace("  ", " "));
					if(cecoDTO.getDescCeco().contains("   "))
						cecoDTO.setDescCeco(cecoDTO.getDescCeco().replace("   ", " "));
					/*
					logger.info("ID CECOS HIJOS: " + cecoDTO.getIdCeco());
					logger.info("CECOS HIJOS: " + cecoDTO.getDescCeco());
					logger.info("CECOS HIJO Negocio: " + cecoDTO.getIdNegocio());
					*/
					listaRetorno.add(cecoDTO);
				}
			} catch (Exception e) {
				logger.info("No hay: " + e.getMessage());
			}
		}
		
		//logger.info("listaCecos.size(): " + listaRetorno.size());
		
		return listaRetorno;
		
	}
	
	@RequestMapping(value = "/ajaxBuscaCecosPA", method = RequestMethod.GET)
	public @ResponseBody List<CecoDTO> ajaxBuscaCecosPA(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") int idCeco) {
				
		List<CecoDTO> lista = null;
		List<CecoDTO> listaRetorno = new ArrayList<CecoDTO>();
		
		try {
			lista = cecoBI.buscaCecosPasoP(idCeco);
			
			for (int i = 0; i < lista.size(); i++) {
				
				CecoDTO cecoDTO = lista.get(i);
				if(cecoDTO.getDescCeco().contains("  "))
					cecoDTO.setDescCeco(cecoDTO.getDescCeco().replace("  ", " "));
				if(cecoDTO.getDescCeco().contains("   "))
					cecoDTO.setDescCeco(cecoDTO.getDescCeco().replace("   ", " "));
				
				/*logger.info("ID CECOS HIJOS: " + cecoDTO.getIdCeco());
				logger.info("CECOS HIJOS: " + cecoDTO.getDescCeco());
				logger.info("CECOS HIJO Negocio: " + cecoDTO.getIdNegocio());
				*/
				listaRetorno.add(cecoDTO);
			}
		} catch (Exception e) {
			logger.info("No hay: " + e.getMessage());
		}

		//logger.info("listaCecos.size(): " + listaRetorno.size());
		//logger.info("listaCecos.size(): " + listaRetorno.get(0).toString());
		
		return lista;
		
	}
	
	//http://10.51.210.239:8080/checklist/reportes/ajaxRH.json
	@RequestMapping(value = "/ajaxRH", method = RequestMethod.GET)
	public @ResponseBody CecoDTO ajaxRH(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "nomCeco", required = true, defaultValue = "") String nomCeco) throws IOException, JSONException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		
		
		CecoDTO cecoDTO = new CecoDTO();
		
		cecoDTO.setIdCeco(idCeco);
		cecoDTO.setDescCeco(nomCeco);
		
		UtilDate fec = new UtilDate();
		@SuppressWarnings("static-access")
		String fecha = fec.getSysDate("ddMMyyyyHHmmss");
	
		URL url = new URL(FRQConstantes.getURLServer() + "/migestion/servicio/token.json?idapl=666&" + (new UtilCryptoGS().encryptParams("ip=10.51.219.216&fecha=" + fecha + "&uri=http://10.51.210.239:8080/migestion/servicios/getDatosRH")).replace("\n",""));
		BufferedReader in = null;
		String Text="";
		try {
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			Text = (((in.readLine().split(",")[0]).split(":")[1]).replace('"', ' ')).replace(" ", "");
			logger.info("+-+-+- Resultado:  " + Text);
		} catch (Throwable t) {
			logger.info("+-+-+- Ocurrio Algo 1 " + t);
		} finally{
			in.close();
		}
	
		logger.info(FRQConstantes.getURLServer() + "/migestion/servicios/getDatosRH.json?" + (new UtilCryptoGS().encryptParams("id=147247&strCC="+idCeco+"&strPeriodo=20180201")).replace("\n","") + "&token=" + Text);
	
		URL url1 = new URL(FRQConstantes.getURLServer() + "/migestion/servicios/getDatosRH.json?" + (new UtilCryptoGS().encryptParams("id=147247&strCC="+idCeco+"&strPeriodo=20180201")).replace("\n","") + "&token=" + Text);
		BufferedReader in1 = null;
		String json = "";
		try {
			in1 = new BufferedReader(new InputStreamReader(url1.openStream()));
			String linea;
			while((linea=in1.readLine())!=null){
				json+=linea;
			}
		} catch (Throwable t) {
			json=null;
			logger.info("+-+-+- Ocurrio algo 2 " + t);
		} finally{
			if(in1!=null){
				in1.close();
			}
		}
		
		logger.info("+-+-+- json" + json);
		
		//json="{'plantilla':90,'banderaPlantilla':false,'cubrimiento':10,'banderaCubrimiento':false,'rotacion':0,'banderaRotacion':true,'tendencia':0,'banderaTendencia':false,'certificacion':0,'banderaCertificacion':true,'garantiaTotal':0,'garantizados':0,'noGarantizados':0,'nivel':1,'valorTotal':37,'horario':'08:30','periodoRepet':5,'periodo':2,'reporte':4}";
		
		//logger.info("+-+-+- json modificado: " + json);
		
		String jsonTareas = metodoTareas(idCeco,"20");
		
		
		if(json!=null){
			JSONObject obj = new JSONObject(json);
			
			//logger.info("+-+-+- obj:  " + obj);
			if(!json.contains("cubrimiento")){
				logger.info("+-+-+- rh fue null");
				cecoDTO.setRh(null);
			}else{
				
				RH rh = new RH();
				
				rh.setPlantilla(obj.getInt("cubrimiento"));
				rh.setRotacion(obj.getInt("rotacion"));
				rh.setAsesores(obj.getInt("garantizados"));
				rh.setCertificados(obj.getInt("certificacion"));
				if(jsonTareas!=null)
					rh.setTareas(Integer.parseInt(jsonTareas));
				else
					rh.setTareas(0);
				
				cecoDTO.setRh(rh);
			}
			
		}else{
			logger.info("+-+-+- rh trono");
			cecoDTO.setRh(null);
		}
		
		logger.info("+-+-+- resultado de rh:   " + cecoDTO);
		
		return cecoDTO;
	}
	
	@RequestMapping(value = "/ajaxPorcentajeApertura", method = RequestMethod.GET)
	public @ResponseBody ClienteDTO ajaxPorcentajeApertura(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "nomCeco", required = true, defaultValue = "") String nomCeco) throws IOException, JSONException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		
		logger.info("/*/*/*/*/*/*/*/ ENTRE AL AJAX DE PORCENTAJE APERTURA");
		
		ClienteDTO cienteDTO = new ClienteDTO();
		
		cienteDTO.setIdCeco(idCeco);
		cienteDTO.setDescCeco(nomCeco);
		
		UtilDate fec = new UtilDate();
		@SuppressWarnings("static-access")
		String fecha = fec.getSysDate("ddMMyyyyHHmmss");
	
		URL url = new URL(FRQConstantes.getURLServer() + "/migestion/servicio/token.json?idapl=666&" + (new UtilCryptoGS().encryptParams("ip=10.51.219.216&fecha=" + fecha + "&uri=http://10.51.210.239:8080/migestion/servicios/getPorcentajeApertura")).replace("\n",""));
		BufferedReader in = null;
		String Text="";
		try {
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			Text = (((in.readLine().split(",")[0]).split(":")[1]).replace('"', ' ')).replace(" ", "");
			logger.info("+-+-+- Resultado:  " + Text);
		} catch (Throwable t) {
			logger.info("+-+-+- Ocurrio 1 " + t);
		} finally{
			in.close();
		}
	
		logger.info(FRQConstantes.getURLServer() + "/migestion/servicios/getPorcentajeApertura.json?" + (new UtilCryptoGS().encryptParams("id=147247&idCeco="+idCeco)).replace("\n","") + "&token=" + Text);
	
		URL url1 = new URL(FRQConstantes.getURLServer() + "/migestion/servicios/getPorcentajeApertura.json?" + (new UtilCryptoGS().encryptParams("id=147247&idCeco="+idCeco)).replace("\n","") + "&token=" + Text);
		BufferedReader in1 = null;
		String jsonAper = "";
		try {
			in1 = new BufferedReader(new InputStreamReader(url1.openStream()));
			String linea;
			while((linea=in1.readLine())!=null){
				jsonAper+=linea;
			}
		} catch (Throwable t) {
			jsonAper=null;
			logger.info("+-+-+- Ocurrio 2 " + t);
		} finally{
			in1.close();
		}
		
		logger.info("+-+-+- json getPorcentajeApertura:  " + jsonAper);
		
		JSONObject objAper = null;
		
		if(jsonAper!=null)
			objAper = new JSONObject(jsonAper);	
		
		if(objAper!=null){		
			JSONArray ar = objAper.getJSONArray("porcentajeApertura");
			
			logger.info("+-+-+- ar:  " + ar);
			
			if(ar.length()>0){
				
				JSONObject obj = new JSONObject(ar.getString(0));					
				logger.info("+-+-+- obj:  " + obj);
				cienteDTO.setApertura(obj.getString("conteo"));
									
			}else{
				cienteDTO.setApertura(null);
			}
		}else{
			logger.info("+-+-+- apertura trono");
			cienteDTO.setApertura(null);
		}
	
		
		logger.info("+-+-+- resultado de apertura:   " + cienteDTO);
		
		return cienteDTO;
	}
	
	@RequestMapping(value = "/ajaxPorcentajeCierre", method = RequestMethod.GET)
	public @ResponseBody ClienteDTO ajaxPorcentajeCierre(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "nomCeco", required = true, defaultValue = "") String nomCeco) throws IOException, JSONException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		
		
		logger.info("/*/*/*/*/*/*/*/ ENTRE AL AJAX DE PORCENTAJE CIERRE");
		
		
		ClienteDTO cienteDTO = new ClienteDTO();
		
		cienteDTO.setIdCeco(idCeco);
		cienteDTO.setDescCeco(nomCeco);
		
		UtilDate fec = new UtilDate();
		@SuppressWarnings("static-access")
		String fecha = fec.getSysDate("ddMMyyyyHHmmss");
	
		URL url = new URL(FRQConstantes.getURLServer() + "/migestion/servicio/token.json?idapl=666&" + (new UtilCryptoGS().encryptParams("ip=10.51.219.216&fecha=" + fecha + "&uri=http://10.51.210.239:8080/migestion/servicios/getPorcentajeApertura")).replace("\n",""));
		BufferedReader in = null;
		String Text="";
		try {
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			Text = (((in.readLine().split(",")[0]).split(":")[1]).replace('"', ' ')).replace(" ", "");
			logger.info("+-+-+- Resultado:  " + Text);
		} catch (Throwable t) {
			logger.info("+-+-+- Ocurrio algo... 1 " + t);
		} finally{
			in.close();
		}
	
		logger.info(FRQConstantes.getURLServer() + "/migestion/servicios/getPorcentajeCierre.json?" + (new UtilCryptoGS().encryptParams("id=147247&idCeco="+idCeco)).replace("\n","") + "&token=" + Text);
	
		URL url1 = new URL(FRQConstantes.getURLServer() + "/migestion/servicios/getPorcentajeCierre.json?" + (new UtilCryptoGS().encryptParams("id=147247&idCeco="+idCeco)).replace("\n","") + "&token=" + Text);
		BufferedReader in1 = null;
		String jsonAper = "";
		try {
			in1 = new BufferedReader(new InputStreamReader(url1.openStream()));
			String linea;
			while((linea=in1.readLine())!=null){
				jsonAper+=linea;
			}
		} catch (Throwable t) {
			jsonAper=null;
			logger.info("+-+-+- Ocurrio algo... 2 " + t);
		} finally{
			in1.close();
		}
		
		logger.info("+-+-+- json getPorcentajeCierre:  " + jsonAper);
		
		JSONObject objCierre = null;
		
		if(jsonAper!=null)
			objCierre = new JSONObject(jsonAper);	
		
		if(objCierre!=null){		
			JSONArray ar = objCierre.getJSONArray("porcentajeCierre");
			
			logger.info("+-+-+- ar:  " + ar);
			
			if(ar.length()>0){
				
				JSONObject obj = new JSONObject(ar.getString(0));					
				logger.info("+-+-+- obj:  " + obj);
				cienteDTO.setCierre(obj.getString("conteo"));
									
			}else{
				cienteDTO.setCierre(null);
			}
		}else{
			logger.info("+-+-+- cierre trono");
			cienteDTO.setCierre(null);
		}
	
		
		logger.info("+-+-+- resultado de cierre:   " + cienteDTO);
		
		return cienteDTO;
	}
	
	@RequestMapping(value = "/ajaxBuscaChecklist", method = RequestMethod.GET)
	public @ResponseBody VistaCDTO ajaxBuscaChecklist(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "idCheck", required = true, defaultValue = "0") int idCheck) throws IOException, JSONException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		
		
		logger.info("/*/*/*/*/*/*/*/ ENTRE AL AJAX DE BUSCA CHECK");

		logger.info("");
		logger.info("");
		logger.info("");
		
		List<VistaCDTO> lista =null;
		VistaCDTO obj =null;
		try {
			//lista = reporteBI.ReporteChecklist(232822, 156);
			lista = reporteBI.ReporteChecklist(Integer.parseInt(idCeco), idCheck);
			
			logger.info("lista: " + lista);
			if(lista==null){
				logger.info("lista: " + null);
			}else{
				logger.info("lista.size(): " + lista.size());
				if(lista.size()>0){
					logger.info("lista.get(0): " + lista.get(0));
					obj=lista.get(0);
					logger.info("obj de Checklist: " + obj.toString());
				}else{
					logger.info("lista Vacia: ");
				}
			}
			
			logger.info("");
			logger.info("");
			logger.info("");
		} catch (Exception e) {
			logger.info("Ocurrio algo: " + e);
		}
		
		return obj;
		
	}
	
	@SuppressWarnings("static-access")
	@RequestMapping(value = "/ajaxNegocio", method = RequestMethod.GET)
	public @ResponseBody NegocioDTO ajaxNegocio(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "nomCeco", required = true, defaultValue = "") String nomCeco) throws IOException, JSONException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		

		logger.info("/*/*/*/*/*/*/*/ ENTRE AL AJAX DE BUSCA CHECK" );
		
		//migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getIndicadoresThreads.json?idUsuario=147247&fecha=20180221&geografia=36276 - ZONA VILLAS  METRO NORTE&nivel=Z
		
		UtilDate fec = new UtilDate();
		
		String fecha = fec.getSysDate("ddMMyyyyHHmmss");
		String fechaSer = fec.getFechaAyer("yyyyMMdd");
	
		URL url = new URL(FRQConstantes.getURLServer() + "/migestion/servicio/token.json?idapl=666&" + (new UtilCryptoGS().encryptParams("ip=10.51.219.216&fecha=" + fecha + "&uri=http://10.51.210.239:8080/migestion/servicios/getIndicadoresThreads")).replace("\n",""));
		BufferedReader in = null;
		String Text="";
		try {
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			Text = (((in.readLine().split(",")[0]).split(":")[1]).replace('"', ' ')).replace(" ", "");
			logger.info("+-+-+- Resultado:  " + Text);
		} catch (Throwable t) {
			logger.info("+-+-+- Ocurrio algo  1... " + t);
		} finally{
			in.close();
		}
	
		logger.info("/*/*/*/*/*/*/*/ geografia:   " + idCeco + " - " + nomCeco);
		logger.info("/*/*/*/*/*/*/*/ fechaSer:   " + fechaSer);

		logger.info(FRQConstantes.getURLServer() + "/migestion/servicios/getIndicadoresThreads.json?" + (new UtilCryptoGS().encryptParams("idUsuario=147247&fecha="+fechaSer+"&geografia="+idCeco+" - "+nomCeco+"&nivel=Z")).replace("\n","") + "&token=" + Text);
	
		URL url1 = new URL(FRQConstantes.getURLServer() + "/migestion/servicios/getIndicadoresThreads.json?" + (new UtilCryptoGS().encryptParams("idUsuario=147247&fecha="+fechaSer+"&geografia="+idCeco+" - "+nomCeco+"&nivel=Z")).replace("\n","") + "&token=" + Text);
		
		logger.info("/*/*/*/*/*/*/*/ url1:   " + url1);

		
		BufferedReader in1 = null;
		String jsonAper = "";
		try {
			in1 = new BufferedReader(new InputStreamReader(url1.openStream()));
			String linea;
			while((linea=in1.readLine())!=null){
				jsonAper+=linea;
			}
		} catch (Throwable t) {
			jsonAper=null;
			logger.info("+-+-+- Ocurrio algo 2... " + t);
		} finally{
			in1.close();
		}
		
		logger.info("+-+-+- json getPorcentajeCierre:  " + jsonAper);
		
		NegocioDTO obj = new NegocioDTO();

		obj.setIdCeco(idCeco);
		obj.setNomCeco(nomCeco);
		
		logger.info("+-+-+- json getPorcentajeCierre:  " + jsonAper);
		
		JSONObject objCierre = null;
		
		if(jsonAper!=null)
			
			try {
			objCierre = new JSONObject(jsonAper);	
			} catch(Exception e) {
				
				logger.info("AP al convertir json en ajaxNegocio Reportes 617");
			}
		
		if(objCierre!=null){		
			JSONArray ar = objCierre.getJSONArray("indicadores");
			
			logger.info("+-+-+- ar:  " + ar);
			
			if(ar.length()>0){
				for (int i = 0; i < ar.length(); i++) {
					JSONObject objInd = ar.getJSONObject(i);	
					if(objInd.getString("idService").equals("1")){
						obj.setContribucion(objInd.getString("indicador"));
						obj.setColorContribucion(objInd.getString("color"));
					}else if(objInd.getString("idService").equals("15")){
						obj.setNormalidad(objInd.getString("indicador"));
						obj.setColorNormalidad(objInd.getString("color"));
					}else if(objInd.getString("idService").equals("16")){
						obj.setNuncaAbonadas(objInd.getString("indicador"));
						obj.setColorNuncaAbonadas(objInd.getString("color"));
					}else if(objInd.getString("idService").equals("8")){
						obj.setVista(objInd.getString("indicador"));
						obj.setColorVista(objInd.getString("color"));
					}else if(objInd.getString("idService").equals("9")){
						obj.setPlazo(objInd.getString("indicador"));
						obj.setColorPlazo(objInd.getString("color"));
					}
					
				}		
				logger.info("+-+-+- obj:  " + obj);
									
			}else{
				obj=null;
			}
		}else{
			logger.info("+-+-+- cierre trono");
			obj=null;
		}
	
		
		logger.info("+-+-+- resultado de cierre:   " + obj);
		
		return obj;
		
	}
	
	// ************************************ FIN AJAX ************************************
	
	
	@RequestMapping(value = "/pruebaIndicadoresRH", method = RequestMethod.GET)
	public @ResponseBody String pruebaIndicadoresRH(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "nomCeco", required = true, defaultValue = "") String nomCeco) throws IOException, JSONException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		
		String inserto = "";

		UtilDate fec = new UtilDate();
		@SuppressWarnings("static-access")
		String fecha = fec.getSysDate("ddMMyyyyHHmmss");
	
		URL url = new URL(FRQConstantes.getURLServer() + "/migestion/servicio/token.json?idapl=666&" + (new UtilCryptoGS().encryptParams("ip=10.51.219.216&fecha=" + fecha + "&uri=http://10.51.210.239:8080/migestion/servicios/getDatosRH")).replace("\n",""));
		BufferedReader in = null;
		String token="";
		try {
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			token = (((in.readLine().split(",")[0]).split(":")[1]).replace('"', ' ')).replace(" ", "");
			logger.info("+-+-+- Resultado:  " + token);
		} catch (Throwable t) {
			logger.info("+-+-+- 1 " + t);
		} finally{
			in.close();
		}
	
		if(token!=null){
			logger.info(FRQConstantes.getURLServer() + "/migestion/servicios/getDatosRH.json?" + (new UtilCryptoGS().encryptParams("id=147247&strCC=236276&strPeriodo=20180201")).replace("\n","") + "&token=" + token);
			
			URL url1 = new URL(FRQConstantes.getURLServer() + "/migestion/servicios/getDatosRH.json?" + (new UtilCryptoGS().encryptParams("id=147247&strCC=236276&strPeriodo=20180201")).replace("\n","") + "&token=" + token);
			BufferedReader in1 = null;
			String json = "";
			try {
				in1 = new BufferedReader(new InputStreamReader(url1.openStream()));
				String linea;
				while((linea=in1.readLine())!=null){
					json+=linea;
				}
			} catch (Throwable t) {
				json=null;
				logger.info("+-+-+- 2 " + t);
			} finally{
				if(in1!=null){
					in1.close();
				}
			}
			
			logger.info("+-+-+- json" + json);

			//String jsonTareas = metodoTareas(idCeco,"20");
			ReporteIndicadorDTO objPlantilla = null;
			ReporteIndicadorDTO objRotacion = null;
			ReporteIndicadorDTO objGarantizados = null;
			ReporteIndicadorDTO objCertificacion = null;
			ReporteIndicadorDTO objTareas = null;
			
			if(json!=null){
				JSONObject obj = null;
				
				try {
					obj = new JSONObject(json);

					} catch(Exception e) {
						
						logger.info("AP al convertir json en ajaxNegocio Reportes 617");
					}
				
				if(!json.contains("cubrimiento")){
					logger.info("RH fue null");
					inserto = "RH fue null";
				}else{

					objPlantilla = new ReporteIndicadorDTO();
					objRotacion = new ReporteIndicadorDTO();
					objGarantizados = new ReporteIndicadorDTO();
					objCertificacion = new ReporteIndicadorDTO();
					objTareas = new ReporteIndicadorDTO();
					
					
					objPlantilla.setIdCeco(idCeco);
					objPlantilla.setNombreCeco(nomCeco);
					objPlantilla.setIndicador(0);
					objPlantilla.setValor(""+obj.getInt("cubrimiento"));
					
					inserto += "Plantilla: " + reporteIndicadorBI.insertaIndicador(objPlantilla);
					
					objRotacion.setIdCeco(idCeco);
					objRotacion.setNombreCeco(nomCeco);
					objRotacion.setIndicador(1);
					objRotacion.setValor(""+obj.getInt("rotacion"));
					
					inserto += ", Rotacion: " + reporteIndicadorBI.insertaIndicador(objRotacion);
					
					objGarantizados.setIdCeco(idCeco);
					objGarantizados.setNombreCeco(nomCeco);
					objGarantizados.setIndicador(2);
					objGarantizados.setValor(""+obj.getInt("garantizados"));
					
					inserto += ", Garantizados: " + reporteIndicadorBI.insertaIndicador(objGarantizados);
					
					objCertificacion.setIdCeco(idCeco);
					objCertificacion.setNombreCeco(nomCeco);
					objCertificacion.setIndicador(3);
					objCertificacion.setValor(""+obj.getInt("certificacion"));
					
					inserto += ", Certificacion: " + reporteIndicadorBI.insertaIndicador(objCertificacion);
					
					objTareas.setIdCeco(idCeco);
					objTareas.setNombreCeco(nomCeco);
					objTareas.setIndicador(4);
					objTareas.setValor("0");
					
					inserto += ", Tareas: " + reporteIndicadorBI.insertaIndicador(objTareas);
					
				}
			}else{
				logger.info("El servicio de RH no responde");
				inserto = "El servicio de RH no responde";
			}
		}else{
			logger.info("El token de indicadores RH es null");
			inserto = "El token de indicadores RH es null";
		}
		return inserto;
	}

	
	// ************************************ INICIO PRUEBAS PARA CONSULTA DE RH ************************************
	
	@RequestMapping(value = "/reporteEquipoDos.htm", method = RequestMethod.GET)
	public ModelAndView reporteEquipoDos() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException, JSONException {

		ModelAndView mv = new ModelAndView("vistaReporteEquipo2");
			
		int cecos[] = {236737,236736,236738};
		
		//int cecos[] = {236738};
		
		List<CecoDTO> lista = null;
		List<CecoDTO> listaRetorno = new ArrayList<CecoDTO>();
		
		for (int h = 0; h < cecos.length; h++) {
			lista = cecoBI.buscaCecosPasoP(cecos[h]);
		
			for (int i = 0; i < lista.size(); i++) {
				
				CecoDTO cecoDTO = lista.get(i);
				if(cecoDTO.getDescCeco().contains("  "))
					cecoDTO.setDescCeco(cecoDTO.getDescCeco().replace("  ", " "));
				if(cecoDTO.getDescCeco().contains("   "))
					cecoDTO.setDescCeco(cecoDTO.getDescCeco().replace("   ", " "));
				
				logger.info("ID CECOS HIJOS: " + cecoDTO.getIdCeco());
				logger.info("CECOS HIJOS: " + cecoDTO.getDescCeco());
				logger.info("CECOS HIJO Negocio: " + cecoDTO.getIdNegocio());
				
				listaRetorno.add(cecoDTO);
			}
		}
		
		logger.info("listaCecos.size(): " + listaRetorno.size());
		logger.info("listaCecos.size(): " + listaRetorno.toString());
		
		mv.addObject("porcentajeL", 80);
		mv.addObject("listaCecos", listaRetorno);
		
		return mv;
	}
	
	public String pruebaInsertaIndicadoresRH(){

		String inserto = "";
		List<CecoIndicadoresDTO> listaCecos = null;
		String idCeco="";
		String nomCeco="";
		int a=0;
		
		try {
			listaCecos = cecoBI.buscaCecosIndicadores();
			logger.info("listaCecos.size(): " + listaCecos.size());

			for (CecoIndicadoresDTO objDTO : listaCecos) {
				
				inserto += "\n";
				
				idCeco = objDTO.getIdCeco();
				nomCeco = objDTO.getNombreCeco();
				
				//idCeco = "232822";
				//nomCeco = "ZONA LERMA METRO PONIENTE";
				
				//if(a==50000)
					//break;
				//logger.info("obj"+ a++ +": " + objDTO.getNombreCeco());
							
				
				UtilDate fec = new UtilDate();
				@SuppressWarnings("static-access")
				String fecha = fec.getSysDate("ddMMyyyyHHmmss");
				String fechaRH = fec.getSysDate("yyyyMMdd");
			
				URL url = new URL(FRQConstantes.getURLServer() + "/migestion/servicio/token.json?idapl=666&" + (new UtilCryptoGS().encryptParams("ip=10.51.219.216&fecha=" + fecha + "&uri=http://10.51.210.239:8080/migestion/servicios/getDatosRH")).replace("\n",""));
				BufferedReader in = null;
				String token="";
				try {
					in = new BufferedReader(new InputStreamReader(url.openStream()));
					token = (((in.readLine().split(",")[0]).split(":")[1]).replace('"', ' ')).replace(" ", "");
					logger.info("+-+-+- Resultado:  " + token);
				} catch (Throwable t) {
					logger.info("+-+-+- 1 " + t);
				} finally{
					in.close();
				}
			
				if(token!=null){
					logger.info(FRQConstantes.getURLServer() + "/migestion/servicios/getDatosRH.json?" + (new UtilCryptoGS().encryptParams("id=147247&strCC="+idCeco+"&strPeriodo="+fechaRH)).replace("\n","") + "&token=" + token);
					
					URL url1 = new URL(FRQConstantes.getURLServer() + "/migestion/servicios/getDatosRH.json?" + (new UtilCryptoGS().encryptParams("id=147247&strCC="+idCeco+"&strPeriodo="+fechaRH)).replace("\n","") + "&token=" + token);
					BufferedReader in1 = null;
					String json = "";
					try {
						in1 = new BufferedReader(new InputStreamReader(url1.openStream()));
						String linea;
						while((linea=in1.readLine())!=null){
							json+=linea;
						}
					} catch (Throwable t) {
						json=null;
						logger.info("+-+-+- 2 " + t);
					} finally{
						if(in1!=null){
							in1.close();
						}
					}
					
					logger.info("+-+-+- json" + json);
					
					//if(true){
					if(json!=null){
						//json="{'plantilla':33,'banderaPlantilla':false,'cubrimiento':89,'banderaCubrimiento':false,'rotacion':0,'banderaRotacion':true,'tendencia':0,'banderaTendencia':false,'certificacion':0,'banderaCertificacion':true,'garantiaTotal':0,'garantizados':0,'noGarantizados':0,'nivel':1,'valorTotal':37,'horario':'08:30','periodoRepet':5,'periodo':2,'reporte':4}";
						JSONObject obj= null;

						if(!json.contains("cubrimiento")){	
							logger.info("RH fue null");
							inserto = "RH fue null";
						}else{
							//logger.info("RH no fue null");	
							
							try {
								obj = new JSONObject(json);

								} catch(Exception e) {
									
									logger.info("AP al convertir json en ajaxNegocio Reportes 617");
								}

							ReporteIndicadorDTO objPlantilla = new ReporteIndicadorDTO();
							ReporteIndicadorDTO objRotacion = new ReporteIndicadorDTO();
							ReporteIndicadorDTO objGarantizados = new ReporteIndicadorDTO();
							ReporteIndicadorDTO objCertificacion = new ReporteIndicadorDTO();
												
							objPlantilla.setIdCeco(idCeco);
							objPlantilla.setNombreCeco(nomCeco);
							objPlantilla.setIndicador(0);
							objPlantilla.setValor(""+obj.getInt("cubrimiento"));
							
							inserto += "Plantilla: " + reporteIndicadorBI.insertaIndicador(objPlantilla);
							
							objRotacion.setIdCeco(idCeco);
							objRotacion.setNombreCeco(nomCeco);
							objRotacion.setIndicador(1);
							objRotacion.setValor(""+obj.getInt("rotacion"));
							
							inserto += ", Rotacion: " + reporteIndicadorBI.insertaIndicador(objRotacion);
							
							objGarantizados.setIdCeco(idCeco);
							objGarantizados.setNombreCeco(nomCeco);
							objGarantizados.setIndicador(2);
							objGarantizados.setValor(""+obj.getInt("garantizados"));
							
							inserto += ", Garantizados: " + reporteIndicadorBI.insertaIndicador(objGarantizados);
							
							objCertificacion.setIdCeco(idCeco);
							objCertificacion.setNombreCeco(nomCeco);
							objCertificacion.setIndicador(3);
							objCertificacion.setValor(""+obj.getInt("certificacion"));
							
							inserto += ", Certificacion: " + reporteIndicadorBI.insertaIndicador(objCertificacion);
														
						}
					}else{
						logger.info("El servicio de RH no responde");
					}
				}else{
					logger.info("El token de indicadores RH es null");
				}
			}//fin del for
		} catch (Exception e) {
			logger.info("No trajo lista de Cecos... " + e);
		}
		
		logger.info("inserto: " + inserto);
		return inserto;
	}

	public String pruebaInsertaTareas(){
		
		String inserto="";
		List<CecoIndicadoresDTO> listaCecos = null;

		try {
			listaCecos = cecoBI.buscaCecosIndicadores();
			
			if(listaCecos!=null){
				
				logger.info("listaCecos.size(): " + listaCecos.size());

				ReporteIndicadorDTO objTareas = null;
				String idCeco="";
				String nomCeco="";
				//int a=0;
				
				for (CecoIndicadoresDTO objDTO : listaCecos) {

					inserto += "\n";
					
					idCeco = objDTO.getIdCeco();
					nomCeco = objDTO.getNombreCeco();
					
					//idCeco = "232822";
					//nomCeco = "ZONA LERMA METRO PONIENTE";
					
					/*
					if(a==50000)
						break;
					logger.info("obj"+ a++ +": " + objDTO.getNombreCeco());
					*/
					
					String valorTareas="-";
					
					try {
						valorTareas = metodoTareas(objDTO.getIdCeco(),"" + objDTO.getNegocio());
						
						objTareas = new ReporteIndicadorDTO();
						
						objTareas.setIdCeco(idCeco);
						objTareas.setNombreCeco(nomCeco);
						objTareas.setIndicador(4);
						objTareas.setValor(valorTareas);

						inserto += "Tareas: " + reporteIndicadorBI.insertaIndicador(objTareas);
						
					} catch (Exception e) {
						logger.info("No respondio tareas " + e);
					}
				}
			}
		}catch(Exception e){
			logger.info("Ocurrio algo  " + e);
		}
				
		logger.info("inserto: " + inserto);

		return inserto;
	}
	
	public String updateRH(int numOpera){
		
		String aztualizo = "";
		List<CecoIndicadoresDTO> listaCecos = null;		
		int ejecuciones = 0;
		
		try {
			listaCecos = cecoBI.buscaCecosIndicadores();
			
			if(listaCecos!=null) {
				logger.info("listaCecos.size(): " + listaCecos.size());
				
				String idCeco="";
				String nomCeco="";
				int a=0;
				int contaError = 0;

				for (CecoIndicadoresDTO objDTO : listaCecos) {
					a++;
					aztualizo += "\n";
					
					idCeco = objDTO.getIdCeco();
					nomCeco = objDTO.getNombreCeco();
					
					//idCeco = "232822";
					//nomCeco = "ZONA LERMA METRO PONIENTE";
					
					/*if(a==1)
						break;
					logger.info("obj"+ a++ +": " + objDTO.getNombreCeco());*/
					
					String valorRH = "-";
					try {
						valorRH = metodoRH(objDTO.getIdCeco());
						
						//if(true){
						if(valorRH!=null){

							//json="{'plantilla':33,'banderaPlantilla':false,'cubrimiento':89,'banderaCubrimiento':false,'rotacion':0,'banderaRotacion':true,'tendencia':0,'banderaTendencia':false,'certificacion':100,'banderaCertificacion':true,'garantiaTotal':0,'garantizados':0,'noGarantizados':0,'nivel':1,'valorTotal':37,'horario':'08:30','periodoRepet':5,'periodo':2,'reporte':4}";
							
							if(!valorRH.contains("cubrimiento")){	
								logger.info("RH fue null");
								contaError++;
								aztualizo += a +" - "+ idCeco + " - Indicador - RH - false";
								
								/*reporteIndicadorBI.insertaPendientes(idCeco, 0);
								reporteIndicadorBI.insertaPendientes(idCeco, 1);
								reporteIndicadorBI.insertaPendientes(idCeco, 2);
								reporteIndicadorBI.insertaPendientes(idCeco, 3);*/
								
							}else{	
								
								JSONObject obj = null;
								logger.info("RH no fue null");	
								boolean respPan = false;
								
								try {
									
									obj= new JSONObject(valorRH);

									} catch(Exception e) {
										
										logger.info("AP al convertir json en ajaxNegocio Reportes 617");
									}

								ReporteIndicadorDTO objPlantilla = new ReporteIndicadorDTO();
								ReporteIndicadorDTO objRotacion = new ReporteIndicadorDTO();
								ReporteIndicadorDTO objGarantizados = new ReporteIndicadorDTO();
								ReporteIndicadorDTO objCertificacion = new ReporteIndicadorDTO();
													
								objPlantilla.setIdCeco(idCeco);
								objPlantilla.setNombreCeco(nomCeco);
								objPlantilla.setIndicador(0);
								objPlantilla.setValor(""+obj.getInt("cubrimiento"));
								
								respPan = reporteIndicadorBI.insertaIndicador(objPlantilla);
								
								/*if(!respPan)
									reporteIndicadorBI.insertaPendientes(idCeco, 0);
								*/
								aztualizo +=  a +" - "+ "idCeco - "+idCeco+" -  Plantilla: " + respPan;
								
								objRotacion.setIdCeco(idCeco);
								objRotacion.setNombreCeco(nomCeco);
								objRotacion.setIndicador(1);
								objRotacion.setValor(""+obj.getInt("rotacion"));
								
								respPan = reporteIndicadorBI.insertaIndicador(objRotacion);
								
								/*if(!respPan)
									reporteIndicadorBI.insertaPendientes(idCeco, 1);
								*/
								aztualizo += ", Rotacion: " + respPan;
								
								objGarantizados.setIdCeco(idCeco);
								objGarantizados.setNombreCeco(nomCeco);
								objGarantizados.setIndicador(2);
								objGarantizados.setValor(""+obj.getInt("garantizados"));
								
								respPan = reporteIndicadorBI.insertaIndicador(objGarantizados);
								
								/*if(!respPan)
									reporteIndicadorBI.insertaPendientes(idCeco, 2);
								*/
								aztualizo += ", Garantizados: " + respPan;
								
								objCertificacion.setIdCeco(idCeco);
								objCertificacion.setNombreCeco(nomCeco);
								objCertificacion.setIndicador(3);
								objCertificacion.setValor(""+obj.getInt("certificacion"));
								
								respPan = reporteIndicadorBI.insertaIndicador(objCertificacion);
								
								/*if(!respPan)
									reporteIndicadorBI.insertaPendientes(idCeco, 3);
								*/
								aztualizo += ", Certificacion: " + respPan;
										
								
								if(!respPan)
									contaError++;
							}
						}else{
							aztualizo += a +" - "+ idCeco + " - Indicador - RH - false.";
							contaError++;
							/*reporteIndicadorBI.insertaPendientes(idCeco, 0);
							reporteIndicadorBI.insertaPendientes(idCeco, 1);
							reporteIndicadorBI.insertaPendientes(idCeco, 2);
							reporteIndicadorBI.insertaPendientes(idCeco, 3);*/
						}
						
						if(contaError == numOpera) {
							logger.info("Se han Acumulado "+ numOpera +" errores en " + ejecuciones + " ejecuciones de RH");
							break;
						}
						
					} catch (Exception e) {
						aztualizo += a +" - "+ idCeco + " - Indicador - RH - false";
						logger.info("No respondio RH " + e);
					}
					
					aztualizo +="\n";
					ejecuciones++;
				}//fin del for
			}
		} catch (Exception e) {
			logger.info("No trajo lista de Cecos... " + e);
		}
		logger.info("aztualizo: " + aztualizo);
		return aztualizo;
	}

	public String updateTareas(int numOpera){
		
		String aztualizo="";
		List<CecoIndicadoresDTO> listaCecos = null;
		int ejecuciones = 0;

		try {
			listaCecos = cecoBI.buscaCecosIndicadores();
			
			if(listaCecos!=null){
				
				logger.info("listaCecos.size(): " + listaCecos.size());

				ReporteIndicadorDTO objTareas = null;
				String idCeco="";
				String nomCeco="";
				int a=0;
				int contaError = 0;
				
				for (CecoIndicadoresDTO objDTO : listaCecos) {

					a++;
					idCeco = objDTO.getIdCeco();
					nomCeco = objDTO.getNombreCeco();
					
					/*
					idCeco = "232822";
					nomCeco = "ZONA LERMA METRO PONIENTE";
										
					if(a==5)
						break;
					logger.info("obj"+ a++ +": " + objDTO.getNombreCeco());
					*/
					
					String valorTareas="-";
					
					try {
						valorTareas = metodoTareas(objDTO.getIdCeco(),"" + objDTO.getNegocio());
						
						if(valorTareas != null){
							
							objTareas = new ReporteIndicadorDTO();
							
							objTareas.setIdCeco(idCeco);
							objTareas.setNombreCeco(nomCeco);
							objTareas.setIndicador(4);
							objTareas.setValor(valorTareas);

							boolean respuestaActualizacion = reporteIndicadorBI.actualizaIndicador(objTareas);
							aztualizo += a +" - "+ idCeco + " - Indicador - 4 - " + respuestaActualizacion;
							
							if(!respuestaActualizacion)
								contaError++;
							
						}else{
							aztualizo += a +" - "+ idCeco + " - Indicador - 4 - false.";
							contaError++;
						}
						
						if(contaError == numOpera) {
							logger.info("Se han Acumulado "+ numOpera +" errores en " + ejecuciones + " ejecuciones de Tareas");
							break;
						}
						
					} catch (Exception e) {
						aztualizo += a +" - "+ idCeco + " - Indicador - 4 - false";
						logger.info("No respondio tareas " + e);
					}
					
					aztualizo += "\n";
					ejecuciones++;
				}
			}
		}catch(Exception e){
			logger.info("No trajo lista de Cecos...  " + e);
		}
		logger.info(aztualizo);
		return aztualizo;
	}
	
	@RequestMapping(value = "/pruebaInsertaIndRH", method = RequestMethod.GET)
	public @ResponseBody String pruebaInsertaIndRH() {
		
		String respuesta = "";
		respuesta += pruebaInsertaIndicadoresRH();
		
		return respuesta;
	}
	
	@RequestMapping(value = "/pruebaInsertaIndTar", method = RequestMethod.GET)
	public @ResponseBody String pruebaInsertaIndTar() {
		
		String respuesta = "";
		respuesta += pruebaInsertaTareas();
		
		return respuesta;
	}
	
	//10.51.210.239:8080/checklist/reportes/pruebaUpdateTareas.json?idCeco=522191&nomCeco=EKT MORELOS CUAUTLA REFORMA&idIndicador=1&valor=0&numOpera=10&opcion=1
	//10.51.210.239:8080/checklist/reportes/pruebaUpdateTareas.json?numOpera=10&opcion=0
	@RequestMapping(value = "/pruebaUpdateTareas", method = RequestMethod.GET)
	public @ResponseBody String pruebaUpdateTareas(
			@RequestParam(value = "idCeco", defaultValue = "") String idCeco,
			@RequestParam(value = "nomCeco", defaultValue = "") String nomCeco,
			@RequestParam(value = "idIndicador", defaultValue = "0") int idIndicador,
			@RequestParam(value = "valor", defaultValue = "") String valor,
			@RequestParam(value = "numOpera", required = true) int numOpera,
			@RequestParam(value = "opcion", required = true) int opcion) {
		
		String respuesta = "";

		if(opcion==1){
		
			ReporteIndicadorDTO obj = null;
			obj = new ReporteIndicadorDTO();
			
			obj.setIdCeco(idCeco);
			obj.setNombreCeco(nomCeco);
			obj.setIndicador(idIndicador);
			obj.setValor(valor);
			
			try {
				respuesta += "UPDATE: \n" + idCeco + " - Indicador - "+idIndicador+" - " + reporteIndicadorBI.actualizaIndicador(obj);
			} catch (Exception e) {
				respuesta += "UPDATE: \n" + idCeco + " - Indicador - "+idIndicador+" - false";
				logger.info("Ocurrio algo... " + e);
			}
		}else{
			respuesta += "UPDATE: \n" + updateTareas(numOpera);
		}
		
		return respuesta;
	}
	
	//10.51.210.239:8080/checklist/reportes/pruebaUpdateRH.json?idCeco=522191&nomCeco=EKT MORELOS CUAUTLA REFORMA&idIndicador=1&valor=0&numOpera=20&opcion=0
	@RequestMapping(value = "/pruebaUpdateRH", method = RequestMethod.GET)
	public @ResponseBody String pruebaUpdateRH(
			@RequestParam(value = "idCeco", required = true) String idCeco,
			@RequestParam(value = "nomCeco", required = true) String nomCeco,
			@RequestParam(value = "idIndicador", required = true) int idIndicador,
			@RequestParam(value = "valor", required = true) String valor,
			@RequestParam(value = "numOpera", required = true) int numOpera,
			@RequestParam(value = "opcion", required = true) int opcion) {
		
		String respuesta = "";

		if(opcion==1){
		
			ReporteIndicadorDTO obj = null;
			obj = new ReporteIndicadorDTO();
			
			obj.setIdCeco(idCeco);
			obj.setNombreCeco(nomCeco);
			obj.setIndicador(idIndicador);
			obj.setValor(valor);
			
			try {
				respuesta += "UPDATE: \n" + idCeco + " - Indicador - "+idIndicador+" - " + reporteIndicadorBI.actualizaIndicador(obj);
			} catch (Exception e) {
				respuesta += "UPDATE: \n" + idCeco + " - Indicador - "+idIndicador+" - false";
				logger.info("Ocurrio algo... " + e);
			}
		}else{
			respuesta += "UPDATE: \n" + updateRH(numOpera);
		}
		
		return respuesta;
	}
	
	//10.51.210.239:8080/checklist/reportes/pruebaConsultaRH.htm?idCeco=522191&nomCeco=EKT MORELOS CUAUTLA REFORMA
	@RequestMapping(value = "/pruebaConsultaRH", method = RequestMethod.GET)
	public @ResponseBody CecoDTO pruebaConsultaRH(
			@RequestParam(value = "idCeco", required = true) String idCeco,
			@RequestParam(value = "nomCeco", required = true) String nomCeco) {
				
		CecoDTO cecoDTO = new CecoDTO();
		RH rh = new RH();
		
		cecoDTO.setIdCeco(idCeco);
		cecoDTO.setDescCeco(nomCeco);

		try {
			rh.setPlantilla(Integer.parseInt(reporteIndicadorBI.consultaPorcentaje(idCeco, 0)));
			rh.setRotacion(Integer.parseInt(reporteIndicadorBI.consultaPorcentaje(idCeco, 1)));
			rh.setAsesores(Integer.parseInt(reporteIndicadorBI.consultaPorcentaje(idCeco, 2)));
			rh.setCertificados(Integer.parseInt(reporteIndicadorBI.consultaPorcentaje(idCeco, 3)));
			rh.setTareas(Integer.parseInt(reporteIndicadorBI.consultaPorcentaje(idCeco, 4)));
			
			cecoDTO.setRh(rh);
			
		} catch (Exception e) {
			logger.info("Ocurrio algo " + e);
		}
		
		logger.info("RH... " + cecoDTO.getRh().toString());
		return cecoDTO;
	}
	
	
	public String metodoRH(String idCeco) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException {
		
		UtilDate fec = new UtilDate();
		@SuppressWarnings("static-access")
		String fecha = fec.getSysDate("ddMMyyyyHHmmss");
		String fechaRH = fec.getSysDate("yyyyMMdd");
	
		URL url = new URL(FRQConstantes.getURLServer() + "/migestion/servicio/token.json?idapl=666&" + (new UtilCryptoGS().encryptParams("ip=10.51.219.216&fecha=" + fecha + "&uri=http://10.51.210.239:8080/migestion/servicios/getDatosRH")).replace("\n",""));
		BufferedReader in = null;
		String json="";
		String token="";
		try {
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			token = (((in.readLine().split(",")[0]).split(":")[1]).replace('"', ' ')).replace(" ", "");
			logger.info("+-+-+- Resultado:  " + token);
		} catch (Throwable t) {
			token=null;
			logger.info("+-+-+- 1 " + t);
		} finally{
			in.close();
		}
	
		if(token!=null){
			logger.info(FRQConstantes.getURLServer() + "/migestion/servicios/getDatosRH.json?" + (new UtilCryptoGS().encryptParams("id=147247&strCC="+idCeco+"&strPeriodo="+fechaRH)).replace("\n","") + "&token=" + token);
			
			URL url1 = new URL(FRQConstantes.getURLServer() + "/migestion/servicios/getDatosRH.json?" + (new UtilCryptoGS().encryptParams("id=147247&strCC="+idCeco+"&strPeriodo="+fechaRH)).replace("\n","") + "&token=" + token);
			BufferedReader in1 = null;
			try {
				in1 = new BufferedReader(new InputStreamReader(url1.openStream()));
				String linea;
				while((linea=in1.readLine())!=null){
					json+=linea;
				}
			} catch (Throwable t) {
				json=null;
				logger.info("+-+-+- RH tono " + t);
			} finally{
				if(in1!=null){
					in1.close();
				}
			}
		}else {
			logger.info("+-+-+- token trono");
			json=null;
		}
		logger.info("+-+-+- resultado de RH:   " + json);
		return json;
	}
	
	public String metodoTareas(String idCeco, String negocio) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException, JSONException{

		logger.info(FRQConstantes.getRutaTareas() + "/ServicioRest/services/admonIndicadorTarea/getTareasDetalleFrq?idCeco="+idCeco+"&negocio="+negocio+"&op=3");
	
		URL url1 = new URL(FRQConstantes.getRutaTareas() + "/ServicioRest/services/admonIndicadorTarea/getTareasDetalleFrq?idCeco="+idCeco+"&negocio="+negocio+"&op=3");
		BufferedReader in1 = null;
		String jsonTareas = "";
		try {
			in1 = new BufferedReader(new InputStreamReader(url1.openStream()));
			String linea;
			while((linea=in1.readLine())!=null){
				jsonTareas+=linea;
			}
		} catch (Throwable t) {
			jsonTareas=null;
			logger.info("+-+-+- Ocurrio algo 2 " + t);
		} finally{
			in1.close();
		}
		
		logger.info("+-+-+- jsonTareas" + jsonTareas);
		
		String json="";
		
		if(jsonTareas!=null){
			if(jsonTareas.contains("null")){
				logger.info("+-+-+- tareas fue null");
				json=null;
			}else{
				JSONArray ar = new JSONArray(jsonTareas);
				if(ar.length()>0){	
					JSONObject obj = new JSONObject(ar.getString(0));					
					logger.info("+-+-+- obj:  " + obj);
					json = obj.getString("FIPORCENTAJE");
				}else{
					json=null;
				}
			}
		}else{
			logger.info("+-+-+- tareas trono");
			json=null;
		}
		logger.info("+-+-+- resultado de tareas:   " + json);
		
		return json;
	}
	
	// ************************************ FIN PRUEBAS PARA CONSULTA DE RH ************************************
	
	
	
	// ************************************ INICIO TIEMPO REAL ************************************
	
	@RequestMapping(value = "/reporteTiempoReal.htm", method = RequestMethod.GET)
	public ModelAndView reporteTiempoReal() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException {

		return new ModelAndView("vistaReporteTiempoReal");
	}
	
	@RequestMapping(value = "/reporteTiempoRealHoy.htm", method = RequestMethod.GET)
	public ModelAndView reporteTiempoRealHoy() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException {

		return new ModelAndView("vistaReporteTiempoRealHoy");
	}
	// ************************************ FIN TIEMPOREAL ************************************
	
	
	
}
