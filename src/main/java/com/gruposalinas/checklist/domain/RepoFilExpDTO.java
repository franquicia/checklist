package com.gruposalinas.checklist.domain;

import java.util.ArrayList;
import java.util.List;

public class RepoFilExpDTO {
	
	private int idBita;
	private int idResp;
	private int idArbol;
	private String observ;
	private int idPreg;
	private int idCheck;
	private int idPosible;
	private int idObserv;
	private String posible;
	private String pregunta;
	private int idcritica;
	private int idcheckUsua;
	private int idUsu;
	private String checklist;
	private int idOrdenGru;
	private int idperiodicidad;
	private String respuestaAbierta;
	private int idRespAb;
	private int banderaRespAb;
	private String modulo;
	private double calif;
	private double precalif;
	private String clasif;
	private double calCheck;
	private int bitGral;
	private String fechaTermino;
	private String idRecorrido;
	private String ceco;
	private String nombreCeco;
	private int activoCeco;
	private String cecoSuperior;
	private String direccion;
	private String region;
	private String territorio;
	private String zona;
	private int aperturable;
	private String perido;
	private String aux;
	private int aux2;
	private int pregPadre;
	private int version;
	private String fechaIni;
	private String fechaFin;
	private String bita1;
	private String bita2;
	private String pregFilt;
	private String ruta;
	private String nomCeco;
	private String nomUsu;
	private String obs;
	private double pondTot;
	private double sumPreg;
	private double sum;
	private String fase;
	private String obsVersion;
	private String detalle;
	private String proyecto;
	private int idSoft;
	private String liderObra;
	private String area;
	private int sla;
	
	private List<ChecklistPreguntasComDTO> listaImperdonables;
	
	private List<SoftNvoDTO> listaCalculos;
	
	public List<SoftNvoDTO> getListaCalculos() {
		return listaCalculos;
	}
	public void setListaCalculos(List<SoftNvoDTO> listaCalculos) {
		this.listaCalculos = listaCalculos;
	}
	
	public List<ChecklistPreguntasComDTO> getListaImperdonables() {
		return listaImperdonables;
	}
	public void setListaImperdonables(List<ChecklistPreguntasComDTO> listaImperdonables) {
		this.listaImperdonables = listaImperdonables;
	}
	public int getIdBita() {
		return idBita;
	}
	public void setIdBita(int idBita) {
		this.idBita = idBita;
	}
	public int getIdResp() {
		return idResp;
	}
	public void setIdResp(int idResp) {
		this.idResp = idResp;
	}
	public int getIdArbol() {
		return idArbol;
	}
	public void setIdArbol(int idArbol) {
		this.idArbol = idArbol;
	}
	public String getObserv() {
		return observ;
	}
	public void setObserv(String observ) {
		this.observ = observ;
	}
	public int getIdPreg() {
		return idPreg;
	}
	public void setIdPreg(int idPreg) {
		this.idPreg = idPreg;
	}
	public int getIdCheck() {
		return idCheck;
	}
	public void setIdCheck(int idCheck) {
		this.idCheck = idCheck;
	}
	public int getIdPosible() {
		return idPosible;
	}
	public void setIdPosible(int idPosible) {
		this.idPosible = idPosible;
	}
	public int getIdObserv() {
		return idObserv;
	}
	public void setIdObserv(int idObserv) {
		this.idObserv = idObserv;
	}
	public String getPosible() {
		return posible;
	}
	public void setPosible(String posible) {
		this.posible = posible;
	}
	public String getPregunta() {
		return pregunta;
	}
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}
	public int getIdcritica() {
		return idcritica;
	}
	public void setIdcritica(int idcritica) {
		this.idcritica = idcritica;
	}
	public int getIdcheckUsua() {
		return idcheckUsua;
	}
	public void setIdcheckUsua(int idcheckUsua) {
		this.idcheckUsua = idcheckUsua;
	}
	public int getIdUsu() {
		return idUsu;
	}
	public void setIdUsu(int idUsu) {
		this.idUsu = idUsu;
	}
	public String getChecklist() {
		return checklist;
	}
	public void setChecklist(String checklist) {
		this.checklist = checklist;
	}
	public int getIdOrdenGru() {
		return idOrdenGru;
	}
	public void setIdOrdenGru(int idOrdenGru) {
		this.idOrdenGru = idOrdenGru;
	}
	public int getIdperiodicidad() {
		return idperiodicidad;
	}
	public void setIdperiodicidad(int idperiodicidad) {
		this.idperiodicidad = idperiodicidad;
	}
	public String getRespuestaAbierta() {
		return respuestaAbierta;
	}
	public void setRespuestaAbierta(String respuestaAbierta) {
		this.respuestaAbierta = respuestaAbierta;
	}
	public int getIdRespAb() {
		return idRespAb;
	}
	public void setIdRespAb(int idRespAb) {
		this.idRespAb = idRespAb;
	}
	public int getBanderaRespAb() {
		return banderaRespAb;
	}
	public void setBanderaRespAb(int banderaRespAb) {
		this.banderaRespAb = banderaRespAb;
	}
	public String getModulo() {
		return modulo;
	}
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
	public double getCalif() {
		return calif;
	}
	public void setCalif(double calif) {
		this.calif = calif;
	}
	public double getPrecalif() {
		return precalif;
	}
	public void setPrecalif(double precalif) {
		this.precalif = precalif;
	}
	public String getClasif() {
		return clasif;
	}
	public void setClasif(String clasif) {
		this.clasif = clasif;
	}
	public double getCalCheck() {
		return calCheck;
	}
	public void setCalCheck(double calCheck) {
		this.calCheck = calCheck;
	}
	public int getBitGral() {
		return bitGral;
	}
	public void setBitGral(int bitGral) {
		this.bitGral = bitGral;
	}
	public String getFechaTermino() {
		return fechaTermino;
	}
	public void setFechaTermino(String fechaTermino) {
		this.fechaTermino = fechaTermino;
	}
	public String getIdRecorrido() {
		return idRecorrido;
	}
	public void setIdRecorrido(String idRecorrido) {
		this.idRecorrido = idRecorrido;
	}
	public String getCeco() {
		return ceco;
	}
	public void setCeco(String ceco) {
		this.ceco = ceco;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	public int getActivoCeco() {
		return activoCeco;
	}
	public void setActivoCeco(int activoCeco) {
		this.activoCeco = activoCeco;
	}
	public String getCecoSuperior() {
		return cecoSuperior;
	}
	public void setCecoSuperior(String cecoSuperior) {
		this.cecoSuperior = cecoSuperior;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getTerritorio() {
		return territorio;
	}
	public void setTerritorio(String territorio) {
		this.territorio = territorio;
	}
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	public int getAperturable() {
		return aperturable;
	}
	public void setAperturable(int aperturable) {
		this.aperturable = aperturable;
	}
	public String getPerido() {
		return perido;
	}
	public void setPerido(String perido) {
		this.perido = perido;
	}
	public String getAux() {
		return aux;
	}
	public void setAux(String aux) {
		this.aux = aux;
	}
	public int getAux2() {
		return aux2;
	}
	public void setAux2(int aux2) {
		this.aux2 = aux2;
	}
	
	
	public int getPregPadre() {
		return pregPadre;
	}
	public void setPregPadre(int pregPadre) {
		this.pregPadre = pregPadre;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	
	public String getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	public String getBita1() {
		return bita1;
	}
	public void setBita1(String bita1) {
		this.bita1 = bita1;
	}
	public String getBita2() {
		return bita2;
	}
	public void setBita2(String bita2) {
		this.bita2 = bita2;
	}
	public String getPregFilt() {
		return pregFilt;
	}
	public void setPregFilt(String pregFilt) {
		this.pregFilt = pregFilt;
	}
	
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	

	public String getNomCeco() {
		return nomCeco;
	}
	public void setNomCeco(String nomCeco) {
		this.nomCeco = nomCeco;
	}
	public String getNomUsu() {
		return nomUsu;
	}
	public void setNomUsu(String nomUsu) {
		this.nomUsu = nomUsu;
	}
	public String getObs() {
		return obs;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}
	
	public double getPondTot() {
		return pondTot;
	}
	public void setPondTot(double pondTot) {
		this.pondTot = pondTot;
	}
	public double getSumPreg() {
		return sumPreg;
	}
	public void setSumPreg(double sumPreg) {
		this.sumPreg = sumPreg;
	}
	public double getSum() {
		return sum;
	}
	public void setSum(double sum) {
		this.sum = sum;
	}

	public String getFase() {
		return fase;
	}
	public void setFase(String fase) {
		this.fase = fase;
	}
	public String getProyecto() {
		return proyecto;
	}
	public void setProyecto(String proyecto) {
		this.proyecto = proyecto;
	}
	
	public int getIdSoft() {
		return idSoft;
	}
	public void setIdSoft(int idSoft) {
		this.idSoft = idSoft;
	}
	
	public String getObsVersion() {
		return obsVersion;
	}
	public void setObsVersion(String obsVersion) {
		this.obsVersion = obsVersion;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	
	public String getLiderObra() {
		return liderObra;
	}
	public void setLiderObra(String liderObra) {
		this.liderObra = liderObra;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	
	public int getSla() {
		return sla;
	}
	public void setSla(int sla) {
		this.sla = sla;
	}
	@Override
	public String toString() {
		return "RepoFilExpDTO [idBita=" + idBita + ", idResp=" + idResp + ", idArbol=" + idArbol + ", observ=" + observ
				+ ", idPreg=" + idPreg + ", idCheck=" + idCheck + ", idPosible=" + idPosible + ", idObserv=" + idObserv
				+ ", posible=" + posible + ", pregunta=" + pregunta + ", idcritica=" + idcritica + ", idcheckUsua="
				+ idcheckUsua + ", idUsu=" + idUsu + ", checklist=" + checklist + ", idOrdenGru=" + idOrdenGru
				+ ", idperiodicidad=" + idperiodicidad + ", respuestaAbierta=" + respuestaAbierta + ", idRespAb="
				+ idRespAb + ", banderaRespAb=" + banderaRespAb + ", modulo=" + modulo + ", calif=" + calif
				+ ", precalif=" + precalif + ", clasif=" + clasif + ", calCheck=" + calCheck + ", bitGral=" + bitGral
				+ ", fechaTermino=" + fechaTermino + ", idRecorrido=" + idRecorrido + ", ceco=" + ceco + ", nombreCeco="
				+ nombreCeco + ", activoCeco=" + activoCeco + ", cecoSuperior=" + cecoSuperior + ", direccion="
				+ direccion + ", region=" + region + ", territorio=" + territorio + ", zona=" + zona + ", aperturable="
				+ aperturable + ", perido=" + perido + ", aux=" + aux + ", aux2=" + aux2 + ", pregPadre=" + pregPadre
				+ ", version=" + version + ", fechaIni=" + fechaIni + ", fechaFin=" + fechaFin + ", bita1=" + bita1
				+ ", bita2=" + bita2 + ", pregFilt=" + pregFilt + ", ruta=" + ruta + ", nomCeco=" + nomCeco
				+ ", nomUsu=" + nomUsu + ", obs=" + obs + ", pondTot=" + pondTot + ", sumPreg=" + sumPreg + ", sum="
				+ sum + ", fase=" + fase + ", obsVersion=" + obsVersion + ", detalle=" + detalle + ", proyecto="
				+ proyecto + ", idSoft=" + idSoft + ", liderObra=" + liderObra + ", area=" + area + ", sla=" + sla
				+ ", listaImperdonables=" + listaImperdonables + ", listaCalculos=" + listaCalculos + "]";
	}
	
	
}
