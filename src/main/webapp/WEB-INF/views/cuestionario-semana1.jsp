<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>Reporte Supervisi&oacute;n</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervision/estilos.css">
  <!-- Tablas/Paginado -->
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervision/paginador.css">
  <script type="text/javascript">
    	

    		function fechas(foto){
        		 
    			 document.getElementById('fAntes').src = "http://10.53.33.82"+foto;
    			 //document.getElementById('fAntes').src = " http://10.51.219.179:80"+foto;
    			
    			 //document.getElementById('fAntes').src = foto;
    			 //document.getElementById('fAntes').src = "http://10.53.33.82/franquicia/imagenes/2019/95170_3447277_13795_ORDEN_1_2019092016213.jpg";
    			 //document.getElementById('fAntes').src = "http://10.53.33.82/franquicia/imagenes/2019/304675_2906132_7687_20190522113422.jpg";
				//alert("Ruta evidencia: "+ foto);
			        } 
    	</script>
</head>

<body ng-app="app" ng-controller="validarForm">

  <div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="${pageContext.request.contextPath}/img/supervision/btn_hamburguer.svg" alt="Men� principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="inicio.htm"><img src="${pageContext.request.contextPath}/img/supervision/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.htm" class="selMenu">
          <div>Salir</div>
          <div><img src="${pageContext.request.contextPath}/img/supervision/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="reporteCeco.htm">Reporte</a> 
		</div>
    	Detalle del protocolo
    </div>

	<!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervision/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Men� Principal<br>
          </div>
        </div>
        <%-- <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la p�gina">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> --%>
        <div id="menu">
	        <tiles:insertTemplate template="/WEB-INF/templates/templateMenuAseguramiento.jsp" flush="true">
					<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuAseguramiento.jsp" />
				</tiles:insertTemplate>
          <!-- <ul class="l-navegacion nivel1">
          		<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Asegurador</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>B�squeda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>B�squeda Asegurador</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galer�a de Evidencias</div></a>
				</li>
			</ul> -->
			
        </div>
      </div>
    </div>

    <!-- Contenido -->
    <div class="contSecc">
    
    	<div class="liga">
		<a href="#" onclick="javascript:history.go(-2)" class="liga">
		<img style="height: 15px; width: 15px;" src="${pageContext.request.contextPath}/img/supervision/arrowmleft.png">
		<span>P�GINA ANTERIOR</span>
		</a>
		</div>
		
		<br><br>
		
		<c:choose>
		<c:when test="${nomUser == 0}">
		
		<div class="titSec"></div>
		<div class="gris">	
		
			<div class="datProt">
				<div>
					<div><strong>Sucursal:</strong> ${nomCeco}</a></div>
					<table>
						<tbody>
							<table>
								<tr>
									<td><strong>Fecha: </strong>${fechaTermino}</td>
								</tr>
								<tr>
									<td><strong>Protocolo: </strong>${nomChecklist}</td>
								</tr>
							</table>
						</tbody>
					</table>
				</div>
				</div>
		</div>
		
		</c:when>
		
		<c:when test="${nomUser == 1}">
		
		<div class="titSec"></div>
		<div class="gris">	
		
			<div class="datProt">
				<div>
					<div><strong>Asegurador:</strong> ${nomUsuario}</a></div>
					<div><strong>N�mero de empleado:</strong> ${idUsuario}</a></div>
					<table>
						<tbody>
							<table>
								<tr>
									<td><strong>Protocolo: </strong>${nomChecklist}</td>
								</tr>
								<tr>
									<td><strong>Sucursal: </strong>${nomCeco}</td>
								</tr>
								<tr>
									<td><strong>Fecha: </strong>${fechaTermino}</td>
								</tr>
							</table>
						</tbody>
					</table>
				</div>
				</div>
		</div>
		
		</c:when>
		</c:choose>
		<br><br>
    
		
		
		<%-- <c:choose>
		<c:when test="${paso == 1}"> --%>
		<div class="gris">
			<table id="registro" class="tblPaginador tblGeneral ">
	            <thead>
	              <tr>
	                <th></th>
	                <th></th>
	                <th></th>
	                <th></th>
	              </tr>
	            </thead>
	            <tbody>
	               <c:set var="nFilters" value="${0}" scope="request"/>
						<c:forEach var="modulos" items="${modulos}">
						
							<tr>
				                <td class="tLeft" ><b><c:out value="${modulos.nombre}"/></b></td>
				                 <td></td>
					             <td></td>
					             <td></td>
					             <td></td>
				            </tr>
				            
				            <c:set var="nomMod" value="mods${nFilters}" scope="request"/>
										
										
										<c:forEach var="list" items="${lista}">
										
										
											<c:choose>
												<c:when test="${list.idModulo == modulos.idModulo}">
												
													<tr>
										                <td class="tLeft"><c:out value="${list.pregunta}" /></td>
										                <td>
										                		<c:choose>
																	<c:when test="${list.idPosible == '4'}">
																		<c:out value="${list.respuestaAbierta}" />
																	</c:when>
																	<c:otherwise>
																		<c:out value="${list.posible}" />
																	</c:otherwise>
																</c:choose>
										                </td>
                                                                                                <td><c:out value="${list.observ}" /></td>
										                <td><c:out value="${list.ponderacion}" />%</td>
										                <td>
										                		<c:choose>
																	<c:when test="${empty list.ruta}">
																		NO APLICA
																		
																	</c:when>
																	<c:otherwise>
																		<a href="#" class="modal01_view" class="modal01_view" onclick='fechas("${list.ruta}")'><img src="${pageContext.request.contextPath}/img/supervision/imgIco.svg"></a>
																	</c:otherwise>
																</c:choose>
										                
										                </td>
										             </tr>			
												</c:when>
											</c:choose>
											
										</c:forEach>
						
						
						</c:forEach> 
						
	            </tbody>
	        </table>
		</div>
		
		<%-- </c:when>
		</c:choose> --%>
		
	</div>	<!-- Fin conSecc -->
	
	<!-- Footer -->
	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualizaci�n Febrero 2019</div>
	</div>
</div><!-- Fin page -->
</body>
</html>

<!-- Modal -->
<div id="modal01" class="modal">
  <div class="cuadro cuadroG">
    <a href="#" class="btnCerrar simplemodal-close"><img  src="${pageContext.request.contextPath}/img/supervision/icoCerrar.svg"></a>
    <img id="fAntes" class="imgodal" src="${pageContext.request.contextPath}/img/supervision/Modex.png">
  </div>
  <div class="clear"></div><br>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/custom-file-input.js"></script><!--Unpload -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/funciones.js"></script>
<!-- Tablas/Paginado -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/paginador.js"></script> 
<script>
$(document).ready(function() {
    $('#registro').DataTable({searching: false, paging: true, info: true, ordering: false, pageLength : 15, lengthMenu: [[5], [5]]});
} );
</script>

<script type="text/javascript">



</script>

