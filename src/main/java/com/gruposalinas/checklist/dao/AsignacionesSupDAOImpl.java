package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AsignacionesSupDTO;
import com.gruposalinas.checklist.mappers.AsignacionesSupRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AsignacionesSupDAOImpl extends DefaultDAO implements AsignacionesSupDAO {

    private static Logger logger = LogManager.getLogger(AsignacionesSupDAOImpl.class);

    DefaultJdbcCall jdbcInsertAsignacion;
    DefaultJdbcCall jdbcUpdateAsignacion;
    DefaultJdbcCall jdbcDeleteAsignacion;
    DefaultJdbcCall jdbcGetAsignacion;
    DefaultJdbcCall jdbcUpdateStatus;
    DefaultJdbcCall jdbcMergeAsignacion;
    DefaultJdbcCall jdbcGetAsignacionByCeco;

    public void init() {

        jdbcInsertAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMASIGNACION")
                .withProcedureName("SPINSASIGNACION");

        jdbcUpdateAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMASIGNACION")
                .withProcedureName("SPACTASIGNACION");

        jdbcDeleteAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMASIGNACION")
                .withProcedureName("SPDELASIGNACION");

        jdbcGetAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMASIGNACION")
                .withProcedureName("SPGETASIGNACION")
                .returningResultSet("PA_CONSULTA", new AsignacionesSupRowMapper());

        jdbcUpdateStatus = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMASIGNACION")
                .withProcedureName("SPACTSTATUS");

        jdbcMergeAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMASIGNACION")
                .withProcedureName("SPMERGEASIGN");

        jdbcGetAsignacionByCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMASIGNACION")
                .withProcedureName("SPGETASIGN_CECO")
                .returningResultSet("PA_CONSULTA", new AsignacionesSupRowMapper());
    }

    @Override
    public int insertAsignacion(int idUsuario, int idPerfil, String idCeco, int idGerente, int activo,
            String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario)
                .addValue("PA_FIPERFIL_ID", idPerfil)
                .addValue("PA_FCID_CECO", idCeco)
                .addValue("PA_FIGERENTEID", idGerente)
                .addValue("PA_FIACTIVO", activo)
                .addValue("PA_FDINICIO_ASIGN", fechaInicio)
                .addValue("PA_FDFIN_ASIGN", fechaFin);

        out = jdbcInsertAsignacion.execute(in);
        logger.info("Función ejecutada: FRANQUICIA.PADMASIGNACION.SPINSASIGNACION");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al insertar la asignacion");
        }
        return respuesta;
    }

    @Override
    public int updateAsignacion(int idUsuario, int idPerfil, String idCeco, int idGerente, int activo,
            String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario)
                .addValue("PA_FIPERFIL_ID", idPerfil)
                .addValue("PA_FCID_CECO", idCeco)
                .addValue("PA_FIGERENTEID", idGerente)
                .addValue("PA_FIACTIVO", activo)
                .addValue("PA_FDINICIO_ASIGN", fechaInicio)
                .addValue("PA_FDFIN_ASIGN", fechaFin);

        out = jdbcUpdateAsignacion.execute(in);
        logger.info("Función ejecutada: FRANQUICIA.PADMASIGNACION.SPACTASIGNACION");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al actualizar la asignacion");
        }
        return respuesta;
    }

    @Override
    public int deleteAsignacion(int idUsuario, String idCeco) throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario)
                .addValue("PA_FCID_CECO", idCeco);

        out = jdbcDeleteAsignacion.execute(in);
        logger.info("Función ejecutada: FRANQUICIA.PADMASIGNACION.SPDELASIGNACION");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al eliminar la asignacion");
        }
        return respuesta;
    }

    @Override
    public List<AsignacionesSupDTO> getAsignaciones(String idUsuario, String idPerfil, String idCeco, String idGerente,
            String activo, String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        List<AsignacionesSupDTO> respuesta = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario)
                .addValue("PA_FIPERFIL_ID", idPerfil)
                .addValue("PA_FCID_CECO", idCeco)
                .addValue("PA_FIGERENTEID", idGerente)
                .addValue("PA_FIACTIVO", activo)
                .addValue("PA_FDINICIO_ASIGN", fechaInicio)
                .addValue("PA_FDFIN_ASIGN", fechaFin);

        out = jdbcGetAsignacion.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADMASIGNACION.SPGETASIGNACION");

        respuesta = (List<AsignacionesSupDTO>) out.get("PA_CONSULTA");

        if (respuesta.size() == 0) {
            logger.info("Algo ocurrió al obtener las asignaciones");
        }
        return respuesta;
    }

    @Override
    public int updateStatus() throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource();
        out = jdbcUpdateStatus.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADMASIGNACION.SPACTSTATUS");
        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al actualizar el status");
        }
        return respuesta;
    }

    @Override
    public int mergeAsignaciones(int idUsuario, int idPerfil, String idCeco, String idGerente, int activo,
            String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario)
                .addValue("PA_FIPERFIL_ID", idPerfil)
                .addValue("PA_FCID_CECO", idCeco)
                .addValue("PA_FIGERENTEID", idGerente)
                .addValue("PA_FIACTIVO", activo)
                .addValue("PA_FDINICIO_ASIGN", fechaInicio)
                .addValue("PA_FDFIN_ASIGN", fechaFin);
        out = jdbcUpdateStatus.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADMASIGNACION.SPMERGEASIGN");
        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al realizar el merge de la asignación");
        }
        return respuesta;
    }

    @Override
    public List<AsignacionesSupDTO> getAsignacionesByUsr(String idUsuario, String activo) throws Exception {
        Map<String, Object> out = null;
        List<AsignacionesSupDTO> respuesta = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_USUARIO", idUsuario)
                .addValue("PA_FIACTIVO", activo);

        out = jdbcGetAsignacionByCeco.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADMASIGNACION.SPGETASIGN_CECO");

        respuesta = (List<AsignacionesSupDTO>) out.get("PA_CONSULTA");

        if (respuesta.size() == 0) {
            logger.info("Algo ocurrió al obtener las asignaciones");
        }
        return respuesta;
    }

}
