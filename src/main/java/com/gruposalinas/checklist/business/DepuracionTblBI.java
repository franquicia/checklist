package com.gruposalinas.checklist.business;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.DepuracionTblDAO;
import java.util.ArrayList;


public class DepuracionTblBI {
		
	private static Logger logger = LogManager.getLogger(DepuracionTblBI.class);
	
	@Autowired
	DepuracionTblDAO depuracionTblDAO;
	
	
	public boolean depuraTabla(String nombreTabla) {

		boolean resultado = false;

		try {
			resultado = depuracionTblDAO.depuraTabla(nombreTabla);
		} catch (Exception e) {
			logger.info("No fue posible depurar la tabla "+nombreTabla);
			logger.info(""+e);
		}

		return resultado;
	}
        
        public ArrayList<Object> depuraBitacora(String fechaInicio,String fechaFin) {

		ArrayList<Object> resultado = null;

		try {
			resultado = depuracionTblDAO.depuraBitacora(fechaInicio,fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible depurar las bitacoras del rango "+fechaInicio+" - "+fechaFin);
			logger.info(""+e);
		}

		return resultado;
	}
        
          
        public ArrayList<Object> depuraErrores(String fechaInicio,String fechaFin) {

		ArrayList<Object> resultado = null;

		try {
			resultado = depuracionTblDAO.depuraErrores(fechaInicio,fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible depurar los errores del rango "+fechaInicio+" - "+fechaFin);
			logger.info(""+e);
		}

		return resultado;
	}
        
	
}