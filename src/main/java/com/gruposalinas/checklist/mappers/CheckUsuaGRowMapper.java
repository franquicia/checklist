package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistUsuarioDTO;

public class CheckUsuaGRowMapper implements RowMapper<ChecklistUsuarioDTO>  {

	public ChecklistUsuarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistUsuarioDTO checklistUsuarioDTO = new ChecklistUsuarioDTO();
		
		checklistUsuarioDTO.setIdCheckUsuario(rs.getInt("FIID_CHECK_USUA"));
		checklistUsuarioDTO.setNombreU(rs.getString("FCNOMBRE"));
		checklistUsuarioDTO.setIdCeco(rs.getInt("FCID_CECO"));
		checklistUsuarioDTO.setNombreCeco(rs.getString("NOMBRECC"));
		checklistUsuarioDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		checklistUsuarioDTO.setFechaIni(rs.getString("FECHAINI"));
		checklistUsuarioDTO.setFechaFin(rs.getString("FECHAFIN"));
		checklistUsuarioDTO.setFechaModificacion(rs.getString("FDFECHA_MOD"));
		checklistUsuarioDTO.setRespuestas(rs.getString("RESPUESTAS"));
		checklistUsuarioDTO.setCompromisos(rs.getString("COMPROMISOS"));
		checklistUsuarioDTO.setRespuestas_Ad(rs.getString("RESP_AD"));
		
		return checklistUsuarioDTO;
	}

}
