package com.gruposalinas.checklist.resources;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.domain.TokenDTO;
import com.gruposalinas.checklist.domain.Usuario_ADTO;
import com.gruposalinas.checklist.util.StrCipher;
import com.gruposalinas.checklist.util.UtilCryptoGS;
import com.gruposalinas.checklist.util.UtilDate;
import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.util.UtilString;

public class FRQAuthInterceptorServices implements HandlerInterceptor {
	
	private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);
	
	@SuppressWarnings({ "unchecked", "static-access", "rawtypes" })
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
	
		String urli = request.getQueryString();
		UtilCryptoGS desci = new UtilCryptoGS();
		StrCipher cifraIOS = new StrCipher();
		UtilDate fec = new UtilDate();
		String fecha = fec.getSysDate("ddMMyyyyHHmmss");
		boolean flag 			= true;
		long startTime 			= System.currentTimeMillis();
		request.setAttribute	("startTime", startTime);
		
		String uri = request.getRequestURI();
		boolean bandera = false;
		
		if( uri.contains("/checklist/servicios/") ){ //Aqui va la url que validara el token
			String url = request.getParameter("token");

			//logger.info("REQUEST PREHANDLE INTERCEPTOR_SERVICES " + request);
			//logger.info("QUERY STRING PREHANDLE INTERCEPTOR_SERVICES " + urli);
			//logger.info("TOKEN PREHANDLE INTERCEPTOR_SERVICES " + url);


			if (url != null && url != " "){
				if( url.contains(";")){
					String sp1 = url.split(";")[1];
						if(sp1.contains("idapl")){
							String sp2 = sp1.split("=")[1];
								if (Integer.parseInt(sp2) > 0){
									bandera = true;
								}
						}
				}
			}
			
			if (bandera) {
				logger.info("TOKEN CORRECTO!! LA APLICACION INICIA CON LA EJECUCION");
					int idLlave=Integer.parseInt((url.split(";")[1]).split("=")[1]);
					url = url.replace(" ", "+");
					url = url.replace("\n", "");
					String uriinf = urli.split("&")[0];
					String urides="";
					
					if(idLlave==7){
						urides = ((cifraIOS.decrypt(uriinf,FRQConstantes.getLlaveEncripcionLocalIOS())).split("&")[0]);
					}else if(idLlave==666){
						urides = ((desci.decryptParams(uriinf)).split("&")[0]);
					}
					
					int uriuser = Integer.parseInt(urides.split("=")[1].replace(" ", ""));
					
					List<Usuario_ADTO> regChecklist = new ArrayList<Usuario_ADTO>();
		
					Class<?> c1;
					try {
						Object object1 = FRQAppContextProvider.getApplicationContext().getBean("usuario_ABI");
						c1 = object1.getClass();     
						Method method1 = c1.getDeclaredMethod("obtieneUsuario", new Class[]{int.class});
						regChecklist =   (List<Usuario_ADTO>) method1.invoke(object1, uriuser);
					} catch (Exception e) {
						logger.info("Ocurrio algo...");
					}
			
					if(regChecklist.size()>0){
						String[] a = url.split(";");
						Map mapaDes = new HashMap();
				
						mapaDes.put(a[0].replace(" ", "+"), a[0].replace(" ", "+"));
						mapaDes.put(a[1].replace(" ", "+"), a[1].replace(" ", "+"));
						mapaDes.put(a[2].replace(" ", "+"), a[2].replace(" ", "+"));
						mapaDes.put(a[3].replace(" ", "+"), a[3].replace(" ", "+"));
				
						String desencriptafinal = desci.decryptMap2(mapaDes).toString();
						String[] vari = desencriptafinal.split(",");
						String[] valores = new String[4];
						valores[0] = vari[0].substring(0, (vari[0].length()) / 2);
						valores[1] = vari[1].substring(0, (vari[1].length()) / 2);
						valores[2] = vari[2].substring(0, (vari[2].length()) / 2);
						valores[3] = vari[3].substring(0, (vari[3].length()) / 2);
				
						String variipfinal = "", variidaplfinal = "", variurlfinal = "",varifechaoriginal="",variFechaClien="";
				
						for (String valor : valores) {
							if (valor.contains("uri")) {
								variurlfinal = "uri="+valor.split("=")[1];
							}
							if (valor.contains("idapl")) {
								variidaplfinal = "idapl="+valor.split("=")[1];
							}
							if (valor.contains("ip")) {
								variipfinal = "ip="+valor.split("=")[1];
							}
							if (valor.contains("fecha")) {
								variFechaClien=valor.split("=")[1];
								varifechaoriginal = "fecha="+valor.split("=")[1];
							}
						}
							
												
						logger.info("Hora del cliente..  "+ UtilString.cleanParameter(varifechaoriginal));
						logger.info("Hora del servidor.. fecha="+ fecha);
					
						if (fec.validaFecha(variFechaClien, fecha)) {	
							TokenDTO token = new TokenDTO();
							
							/*
							Class<?> c2;
							List llave=null;
							try {
								Object object2 = FRQAppContextProvider.getApplicationContext().getBean("llaveBI");
								c2 = object2.getClass();     
								Method method2 = c2.getDeclaredMethod("obtieneLlave", new Class[]{int.class});
								llave = (List) method2.invoke(object2, variidaplfinal);
							} catch ( NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
								e.printStackTrace();	
							}		
												
							if(llave.size()>0)
								token.setLlave(llave.get(0).toString());
							else{
								Class<?> c21;
								llave=null;
								try {
									Object object21 = FRQAppContextProvider.getApplicationContext().getBean("llaveBI");
									c21 = object21.getClass(); 
									//LlaveBI objLlave = (LlaveBI) object21;
									//objLlave.obtieneLlave(666);
									Method method21 = c21.getDeclaredMethod("obtieneLlave", new Class[]{int.class});
									llave = (List) method21.invoke(object21, 666);
								} catch ( NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
									e.printStackTrace();	
								}	
							}
							
							token.setLlave((llave.get(0)).toString());
							
							//System.out.println("Id apli.. " +variidaplfinal);
							//System.out.println("Llave.. "+token.getLlave());
							*/		
							
							token.setFechaHora(varifechaoriginal);
							token.setIpOrigen(variipfinal);
							token.setIdAplicacion(variidaplfinal);
							token.setUrlRedirect(variurlfinal);
							token.setBandera(true);
							token.setLlave(FRQConstantes.getLlaveEncripcionLocal());
							String str1 = new UtilCryptoGS().generaToken(token);
							str1 = str1.replace(" ", "+");
							str1 = str1.replace("\n", "");
							token.setToken(str1);
						
							Class<?> c3;
							boolean urlbase3 = false;
							try {
								Object object3 = FRQAppContextProvider.getApplicationContext().getBean("tokenBI");
								c3 = object3.getClass();  
								//TokenBI objLlave = (TokenBI) object3;
								//urlbase3=objLlave.verificaToken(url, uriuser);
								Method method3 = c3.getDeclaredMethod("verificaToken", new Class[]{String.class, Integer.class});
								urlbase3 =  (Boolean) method3.invoke(object3, str1, uriuser) ;							
							} catch ( Exception  e) {
								logger.info("Ocurrio algo...");
							}	
							if (urlbase3) {
								flag = true;
								String urlServer = FRQConstantes.getURLServer();
								request.getSession().setAttribute("urlServer", urlServer);
								//response.sendRedirect(  );
							}else{
								logger.info("Actualizacion de token en BD incorrecta..  ");
								response.sendRedirect( FRQConstantes.getURLServer() + "../servicioInfoResponse/infoResponseToken.json" );
								flag = false;
							}
						} else {
							logger.info("El token no esta en el rango de tiempo correcto..  ");
							response.sendRedirect( FRQConstantes.getURLServer() + "../servicioInfoResponse/infoResponseToken.json" );
							flag = false;
						}			
					}else{
						logger.info("Usuario o Contraseña incorrecta..  ");
						response.sendRedirect( FRQConstantes.getURLServer() + "../servicioInfoResponse/null.json" );
						flag = false;
					}
			}
			else{
				logger.info("TOKEN NO VALIDO EN LA LLAMADA AL SERVICIO");
				response.sendRedirect( FRQConstantes.getURLServer() + "../servicioInfoResponse/infoResponseToken.json" );
				flag = false;
			}
		}
		return flag;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		// Se ejecuta siempre y cuando el preHandle devuelve true
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		// TODO Auto-generated method stub	
	}
}
