<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html lang="es">
<head>
<title>Banco Azteca | Sistematización de actividades</title> 
<link rel="stylesheet"
	href="../css/vistaCumplimientoVisitasSistemas/estilo.css" />
<script type="text/javascript">
	var conteoL = ${conteo};
	var nivelPerfilL = ${nivelPerfil};
	var porcentajeG = ${porcentajeG};
	var idCecoPadreL = ${idCecoPadre};
	var nombreCecoL = '${nombreCeco}';
	var banderaCecoPadreL = ${banderaCecoPadre};	
	var seleccionAnoL = ${seleccionAno};
	var seleccionMesL = ${seleccionMes};
	var seleccionCanalL = ${seleccionCanal};
	var seleccionPaisL = ${seleccionPais};	
</script>	
					
<!-- jquery Start -->
<script type="text/javascript"
	src="../js/vistaCumplimientoVisitasSistemas/jquery.js"></script>

<!-- progressbar -->
<script>
	function move() {
		var elem = document.getElementById("myBar");
		var width = 1;
		var id = setInterval(frame, 10);
		function frame() {
			if (width >= 100) {
				clearInterval(id);
			} else {
				width++;
				elem.style.width = width + "%";
			}
		}
	}
</script>

<!-- dropdown -->
<script type="text/javascript"
	src="../js/vistaCumplimientoVisitasSistemas/menu.js"></script>
	
<!-- dropdown -->
<script type="text/javascript" src="../js/vistaCumplimientoVisitasSistemas/script-filtroCumplimientoVisitas.js"></script>


<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- <link rel="stylesheet" type="text/css"
	href="../css/vistaCumplimientoVisitasSistemas/tooltipster.bundle.css" /> -->
<link rel="stylesheet" media="screen"
	href="../css/vistaCumplimientoVisitasSistemas/modal.css">
</head>
<body onload="nobackbutton();">

	<div class="header" style="z-index: 10;">
		<div class="logo">
			<c:if test="${!opcionReporte}">
				<a href="/checklist/"><img src="../images/vistaCumplimientoVisitasSistemas/logo.svg"></a>
			</c:if>
			<c:if test="${opcionReporte}">
				<a><img src="../images/vistaCumplimientoVisitasSistemas/logo.svg"></a>
			</c:if>		
		</div>
		<div class="title">
			<div class="box tleft">
				<c:if test="${opcionReporte}">
					<a href="javascript:window.location.replace('vistaCumplimientoVisitasMovil.htm');" class="hom">
						<img src="../images/vistaCumplimientoVisitasSistemas/regresar.svg" class="imgBack">
					</a>
					<a href="../central/vistaCumplimientoVisitasMovil.htm" class="active">
						<img src="../images/vistaCumplimientoVisitasSistemas/rev01W.svg">
					</a> 
				</c:if>	
				<c:if test="${!opcionReporte}">
					<a href="javascript:window.location.replace('vistaCumplimientoVisitas.htm');" class="hom">
						<img src="../images/vistaCumplimientoVisitasSistemas/regresar.svg" class="imgBack">
					</a>
					<a href="../central/vistaCumplimientoVisitas.htm" class="active">
						<img src="../images/vistaCumplimientoVisitasSistemas/rev01W.svg">
					</a> 
				</c:if>	
				
				<!--<a href="#" class=" "><img
					src="../images/vistaCumplimientoVisitasSistemas/rev02W.svg"></a>
				<a href="#" class=" "><img
					src="../images/vistaCumplimientoVisitasSistemas/rev03W.svg"></a>
				<a href="#" class=" "><img
					src="../images/vistaCumplimientoVisitasSistemas/rev04W.svg"></a>
				<a href="#" class=" "><img
					src="../images/vistaCumplimientoVisitasSistemas/rev05W.svg"></a>-->
			</div>
		</div>
		
			<div class="wrapperFixed" style="background:red; background:#f2f2f2;">
					<div class="goborder">
						<h2>
							<a id="idPais" style="color:#747474"></a><span class="subSeccion">/<a id="idOpcTerr" style="color:#747474"></a></span>
						</h2>
					</div>
		
		
					<table class="avance" style="max-width: 800px;">
						<thead>
							<tr>
								<th colspan="3">Objetivo al d&iacute;a <a id="idDiaActual" style="color:#9e9e9e;">0</a></th>
							</tr>
							<tr>
								<th>Día 1 0%</th>
								<th class="w100">
									<div class="w3-light-grey w3-round">
										<div class="w3-container w3-blue" style="width: 1%" id="idBarPor">&nbsp;</div>
									</div>
								</th>
								<th>Día <a id="idTotalDias" style="color:#9e9e9e;">0</a> 100%</th>
							</tr>
						</thead>
					</table>
		
		
					<table class="bloquer" style="max-width: 800px;">
						<thead>
							<tr>
								<th>Territorio</th>
								
								<c:forEach items="${listaChecklist}" var="item">
								    <th style="width: ${64/conteo}%;">${item.nombreCheck}</th>
								</c:forEach>
								
						</thead>
					</table>
			</div>
		
	</div>
	
	
	
	
	<div class="wrapper">
		<div class="hspacer"></div>
		<div class="box" style="margin-top: 155px;">
		
		 <form id="formTerri" method="POST" action="vistaTerritCumplimientoVisitas.htm">	
				<dl class="accordion3" id="idGeneralTablaTerritorios">
				
				<c:if test="${nivelPerfil==0}">
					<c:forEach var = "i" begin = "0" end = "${(fn:length(listaSucursales))-1}">
				
						<dt onclick="cambiaNombre('${listaSucursales[i][0].nombreCeco}');">
							<table class="bloquer linea goshadow1">
								<tbody>
									<tr class="pointer" onclick="muestraZonas(${i},${listaSucursales[i][0].idCeco},${seleccionAno},${seleccionMes},${seleccionCanal},${seleccionPais});">
										<td>${listaSucursales[i][0].nombreCeco} <span>${listaSucursales[i][0].numSucursal} suc</span></td>
						
										<c:forEach var="k" begin="0" end="${(fn:length(listaSucursales[i]))-1}">
											<c:if test="${listaSucursales[i][k].porcentaje<(porcentajeG-5)}">
												<td style="width: ${64/conteo}%;"><div class="radio rojo">${listaSucursales[i][k].porcentaje}%</div></td>
											</c:if>
											<c:if test="${listaSucursales[i][k].porcentaje>=porcentajeG}">
												<td style="width: ${64/conteo}%;"><div class="radio verde">${listaSucursales[i][k].porcentaje}%</div></td>
											</c:if>		
											<c:if test="${listaSucursales[i][k].porcentaje>=(porcentajeG-5) && listaSucursales[i][k].porcentaje<(porcentajeG)}">
												<td style="width: ${64/conteo}%;"><div class="radio amarilloN" style="color:black;">${listaSucursales[i][k].porcentaje}%</div></td>
											</c:if>											
										</c:forEach>
									</tr>
								</tbody>
							</table>
						</dt>
						<dd>
							<div class="inner" id="idLlenaZonas${i}"></div>	
						</dd>						
					</c:forEach>
					<!-- accordion -->
					<script type="text/javascript">
						jQuery(function() {
							$(".subSeccion").hide();
					
						  var allPanels = $('.accordion3 > dd').hide();
					
						  jQuery('.accordion3 > dt').on('click', function() {
						    $this = $(this);
						    //the target panel content
						    $target = $this.next();
							 						    
						    jQuery('.accordion3 > dt').removeClass('accordion3-active');
						    if ($target.hasClass("in")) {
							    $this.removeClass('accordion3-active');
							    $target.slideUp();
							    $target.removeClass("in");
								$(".subSeccion").hide();
								
						    } else {
						      $this.addClass('accordion3-active');
						      jQuery('.accordion3 > dd').removeClass("in");
						      $target.addClass("in");
							  $(".subSeccion").show();
													
						      jQuery('.accordion3 > dd').slideUp();
						      $target.slideDown();
						    }
						  })				
						})
					</script> 
				</c:if>
				
				<c:if test="${nivelPerfil==1}">
					<c:forEach var = "i" begin = "0" end = "${(fn:length(listaSucursales))-1}">
						<dt> <!-- onclick="cambiaNombre('${listaSucursales[i][0].nombreCeco}');"> -->
							<table class="bloquer linea goshadow1">
								<tbody>
									<tr class="pointer"> <!-- onclick="muestraZonasUno(${i},${listaSucursales[i][0].idCeco},${seleccionAno},${seleccionMes},${seleccionCanal},${seleccionPais});" -->
										<td>${listaSucursales[i][0].nombreCeco} <span>${listaSucursales[i][0].numSucursal} suc</span></td>
									
										<c:forEach var="k" begin="0" end="${(fn:length(listaSucursales[i]))-1}">
											<c:if test="${listaSucursales[i][k].porcentaje<(porcentajeG-5)}">
												<td style="width: ${64/conteo}%;"><div class="radio rojo">${listaSucursales[i][k].porcentaje}%</div></td>
											</c:if>
											<c:if test="${listaSucursales[i][k].porcentaje>=porcentajeG}">
												<td style="width: ${64/conteo}%;"><div class="radio verde">${listaSucursales[i][k].porcentaje}%</div></td>
											</c:if>		
											<c:if test="${listaSucursales[i][k].porcentaje>=(porcentajeG-5) && listaSucursales[i][k].porcentaje<(porcentajeG)}">
												<td style="width: ${64/conteo}%;"><div class="radio amarilloN" style="color:black;">${listaSucursales[i][k].porcentaje}%</div></td>
											</c:if>	
										</c:forEach>
									</tr>
				
								</tbody>
							</table>
						</dt>
				
						<dd style="display: block;">
							<div class="inner" id="idLlenaZonas${i}"></div>		
							
							<script>
								cambiaNombre(""+'${listaSucursales[i][0].nombreCeco}'+"");
								muestraZonasUno(${i},${listaSucursales[i][0].idCeco},${seleccionAno},${seleccionMes},${seleccionCanal},${seleccionPais});								
							</script>
												
						</dd>	
						
						
					</c:forEach>
				</c:if>
				</dl>
			</form>
		</div>
		<div class="fspacer"></div>
	</div>

	<div class="modal"></div>
</body>
</html>