package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.OrdenGrupoDTO;
import com.gruposalinas.checklist.mappers.OrdenGrupoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class OrdenGrupoDAOImpl extends DefaultDAO implements OrdenGrupoDAO {

    private static Logger logger = LogManager.getLogger(OrdenGrupoDAOImpl.class);

    DefaultJdbcCall jdbcObtieneOrdenGrupo;
    DefaultJdbcCall jdbcInsertaOrdenGrupo;
    DefaultJdbcCall jdbcActualizaOrdenGrupo;
    DefaultJdbcCall jdbcEliminaOrdenGrupo;

    public void init() {

        jdbcObtieneOrdenGrupo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMORDEN_G")
                .withProcedureName("SP_SEL_ORDEN_GRUPO")
                .returningResultSet("RCL_GRUPO", new OrdenGrupoRowMapper());

        jdbcInsertaOrdenGrupo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMORDEN_G")
                .withProcedureName("SP_INS_ORDEN_GRUPO");

        jdbcActualizaOrdenGrupo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMORDEN_G")
                .withProcedureName("SP_ACT_ORDEN_GRUPO");

        jdbcEliminaOrdenGrupo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMORDEN_G")
                .withProcedureName("SP_DEL_ORDEN_GRUPO");
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OrdenGrupoDTO> obtieneOrdenGrupo(String idOrdenGrupo, String idChecklist, String idGrupo) throws Exception {
        Map<String, Object> out = null;
        List<OrdenGrupoDTO> listaGrupo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_OR_GRUPO", idOrdenGrupo)
                .addValue("PA_IDCHECK", idChecklist)
                .addValue("PA_IDGRUPO", idGrupo);

        out = jdbcObtieneOrdenGrupo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMORDEN_G.SP_SEL_ORDEN_GRUPO}");

        listaGrupo = (List<OrdenGrupoDTO>) out.get("RCL_GRUPO");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Orden del Grupo");
        } else {
            return listaGrupo;
        }

        return null;
    }

    @Override
    public int insertaOrdenGrupo(OrdenGrupoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idGrupo = 0;

        logger.info("VALORES ENTRADA: " + bean);

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_CHECK", bean.getIdChecklist())
                .addValue("PA_ORDEN", bean.getOrden())
                .addValue("PA_IDGRUPO", bean.getIdGrupo())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcInsertaOrdenGrupo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMORDEN_G.SP_INS_ORDEN_GRUPO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDORDEN_G");
        idGrupo = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Orden del Grupo");
        } else {
            return idGrupo;
        }

        return idGrupo;
    }

    @Override
    public boolean actualizaOrdenGrupo(OrdenGrupoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_CHECK", bean.getIdChecklist())
                .addValue("PA_ORDEN", bean.getOrden())
                .addValue("PA_IDGRUPO", bean.getIdGrupo())
                .addValue("PA_IDOR_GRUPO", bean.getIdOrdenGrupo())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcActualizaOrdenGrupo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMORDEN_G.SP_ACT_ORDEN_GRUPO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el Orden del Grupo");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaOrdenGrupo(int idOrdenGrupo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDOR_GRUPO", idOrdenGrupo);

        out = jdbcEliminaOrdenGrupo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMGRUPO.SP_DEL_GRUPO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el Orden del Grupo");
        } else {
            return true;
        }

        return false;
    }

}
