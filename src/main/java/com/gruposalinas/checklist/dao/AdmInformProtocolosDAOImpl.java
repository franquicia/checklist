package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AdmInformProtocolosDTO;
import com.gruposalinas.checklist.mappers.AdmInformProtocolosRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AdmInformProtocolosDAOImpl extends DefaultDAO implements AdmInformProtocolosDAO {

    private static Logger logger = LogManager.getLogger(AdmInformProtocolosDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcInsertaInformProtocolos;
    DefaultJdbcCall jdbcActualizaInformProtocolos;
    DefaultJdbcCall jdbcEliminaInformProtocolos;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINFPROTO")
                .withProcedureName("SPSELECPROT")
                .returningResultSet("PA_CONSULTA", new AdmInformProtocolosRowMapper());

        jdbcInsertaInformProtocolos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINFPROTO")
                .withProcedureName("SPINSPROTO");

        jdbcActualizaInformProtocolos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINFPROTO")
                .withProcedureName("SPACTPROTO");

        jdbcEliminaInformProtocolos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINFPROTO")
                .withProcedureName("SPDELPROTO");
    }

    @SuppressWarnings("unchecked")
    public List<AdmInformProtocolosDTO> obtieneTodos() throws Exception {

        Map<String, Object> out = null;
        List<AdmInformProtocolosDTO> lista = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funcion ejecutada: {checklist.PAADMINFPROTO.SPSELECPROT}");

        lista = (List<AdmInformProtocolosDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener detalle del informe");
        } else {
            return lista;
        }

        return lista;
    }

    public boolean insertaInformProtocolos(AdmInformProtocolosDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_CECO", bean.getCeco())
                .addValue("PA_FIID_USUARIO", bean.getUsuario())
                .addValue("PA_FDFECHA", bean.getFecha())
                .addValue("PA_FCRUTA", bean.getRuta())
                .addValue("PA_FCLIDER_7S", bean.getLider())
                .addValue("PA_FCFIRMA_LIDER", bean.getFirmaLider())
                .addValue("PA_FINUMLIDER", bean.getNumLider())
                .addValue("PA_FCFIRMAFRQ", bean.getFirmaFrq())
                .addValue("PA_RETRO", bean.getRetroAlimentacion())
                .addValue("PA_FCID_PROTOCOLO", bean.getIdProtocolo());

        out = jdbcInsertaInformProtocolos.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMINFPROTO.SPINSPROTO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar el Compromiso");
        } else {
            return true;
        }

        return false;
    }

    public boolean actualizaInformProtocolos(AdmInformProtocolosDTO bean) throws Exception {

        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_SEC", bean.getIdsec())
                .addValue("PA_FCID_CECO", bean.getCeco())
                .addValue("PA_FIID_USUARIO", bean.getUsuario())
                .addValue("PA_FDFECHA", bean.getFecha())
                .addValue("PA_FCRUTA", bean.getRuta())
                .addValue("PA_FCLIDER_7S", bean.getLider())
                .addValue("PA_FCFIRMA_LIDER", bean.getFirmaLider())
                .addValue("PA_FINUMLIDER", bean.getNumLider())
                .addValue("PA_FCFIRMAFRQ", bean.getFirmaFrq());

        out = jdbcActualizaInformProtocolos.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMINFPROTO.SPACTPROTO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al actualizar el detalle de informe");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaInformProtocolos(AdmInformProtocolosDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_SEC", bean.getIdsec());

        out = jdbcEliminaInformProtocolos.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMINFPROTO.SPDELPROTO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al borrar el Handbook");
        } else {
            return true;
        }

        return false;
    }

}
