package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.HallazgosTransfDTO;

public class HallazgosTransfRepoRowMapper implements RowMapper<HallazgosTransfDTO> {

	public HallazgosTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		HallazgosTransfDTO HallazgosTransfDTO = new HallazgosTransfDTO();
		
		HallazgosTransfDTO.setIdHallazgo(rs.getInt("FIID_HALLAZGO"));
	    HallazgosTransfDTO.setIdFolio(rs.getInt("FIID_FOLIO"));
	    HallazgosTransfDTO.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    HallazgosTransfDTO.setStatus(rs.getInt("FCSTATUS"));
	    HallazgosTransfDTO.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    HallazgosTransfDTO.setPreg(rs.getString("FCPREGUNTA"));
	    HallazgosTransfDTO.setRespuesta(rs.getString("FCRESPUESTA"));
	    HallazgosTransfDTO.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
	    HallazgosTransfDTO.setIdVersion(rs.getInt("IDVERSION"));
	    HallazgosTransfDTO.setObsAtencion(rs.getString("COMENTARIOATENC"));
	    HallazgosTransfDTO.setArea(rs.getString("FCAREA"));
	    HallazgosTransfDTO.setObs(rs.getString("FCOBSERVACION"));
	    HallazgosTransfDTO.setBitGral(rs.getInt("FIID_BITA"));
	    HallazgosTransfDTO.setArbol(rs.getString("IDARBOL"));
	    //RUTAEVI
	    HallazgosTransfDTO.setRuta(rs.getString("RUTA"));
	    HallazgosTransfDTO.setFechaIni(rs.getString("FDINICIO"));
	    HallazgosTransfDTO.setFechaFin(rs.getString("FDFIN"));
	    HallazgosTransfDTO.setSla(rs.getString("FCSLA"));
	    HallazgosTransfDTO.setFechaAutorizo(rs.getString("FDAUTORIZO"));
	    HallazgosTransfDTO.setPeriodo(rs.getString("FCPERIODO"));
	    HallazgosTransfDTO.setCeco(rs.getString("FCID_CECO"));
	    HallazgosTransfDTO.setIdcheck(rs.getInt("FIID_CHECKLIST"));
	    HallazgosTransfDTO.setNombChek(rs.getString("CHECKLIST"));
	    HallazgosTransfDTO.setZonaClasi(rs.getString("FCCLASIFICA"));
	    HallazgosTransfDTO.setMotivrechazo(rs.getString("MOTIVORECHAZO"));
	    HallazgosTransfDTO.setCecoNom(rs.getString("FCNOMBRE"));
	    HallazgosTransfDTO.setTipoEvi(rs.getInt("FCTIPOARCH"));
	    HallazgosTransfDTO.setNomUsu(rs.getString("NOMUSU"));
	    HallazgosTransfDTO.setModulo(rs.getString("MODULO"));
	    HallazgosTransfDTO.setPonderacion(rs.getDouble("PONDERACION"));
	    
		return HallazgosTransfDTO;
	}

}


