package com.gruposalinas.checklist.domain;



//@Document

public class Archive {

	private String id;
    private String fileName;
    private String title;
    private String pageText;
    private Integer pageNum;
    
    public Archive(){}
    public Archive(String fileName, String title, String pageText, Integer pageNum) {
		super();
		this.fileName = fileName;
		this.title = title;
		this.pageText = pageText;
		this.pageNum = pageNum;
	}
	
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
     
    @Override
    public String toString(){
        return id+"::"+fileName+"::";
    }
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPageText() {
		return pageText;
	}
	public void setPageText(String pageText) {
		this.pageText = pageText;
	}
	public Integer getPageNum() {
		return pageNum;
	}
	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}
}


