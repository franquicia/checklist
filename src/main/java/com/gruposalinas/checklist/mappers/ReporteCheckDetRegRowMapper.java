package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistDTO;

public class ReporteCheckDetRegRowMapper implements RowMapper<ChecklistDTO> {

	public ChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistDTO checklist = new ChecklistDTO();
		
		checklist.setIdCeco(rs.getString("FCID_CECO"));
		checklist.setNombresuc(rs.getString("FCNOMBRE"));
		checklist.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		checklist.setNombreCheck(rs.getString("NOMBRECHECKLIST"));
		checklist.setFecha_inicio(rs.getString("FECHAINICIO"));
		checklist.setHoraInicio(rs.getString("HORAINICIO"));
		checklist.setFecha_fin(rs.getString("FECHAFIN"));
		checklist.setHoraFin(rs.getString("HORAFIN"));
		checklist.setFechaEnvio(rs.getString("FECHAENVIO"));
		checklist.setModo(rs.getString("FIMODO"));
		
		return checklist;
	}

}
