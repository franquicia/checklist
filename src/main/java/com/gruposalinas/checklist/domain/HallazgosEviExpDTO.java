package com.gruposalinas.checklist.domain;

public class HallazgosEviExpDTO {
	


	private int idHallazgo;
	private int idEvi;
	private int idResp;
	private int status;
	private String ruta;
	private String nombre;
	private int totEvi;
	private String aux;
	private int bitaGral;
	private String periodo;
	private int tipoEvi;
	
	
	public int getIdHallazgo() {
		return idHallazgo;
	}
	public void setIdHallazgo(int idHallazgo) {
		this.idHallazgo = idHallazgo;
	}
	public int getIdEvi() {
		return idEvi;
	}
	public void setIdEvi(int idEvi) {
		this.idEvi = idEvi;
	}
	public int getIdResp() {
		return idResp;
	}
	public void setIdResp(int idResp) {
		this.idResp = idResp;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getTotEvi() {
		return totEvi;
	}
	public void setTotEvi(int totEvi) {
		this.totEvi = totEvi;
	}
	public String getAux() {
		return aux;
	}
	public void setAux(String aux) {
		this.aux = aux;
	}
	public int getBitaGral() {
		return bitaGral;
	}
	public void setBitaGral(int bitaGral) {
		this.bitaGral = bitaGral;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	
	
	public int getTipoEvi() {
		return tipoEvi;
	}
	public void setTipoEvi(int tipoEvi) {
		this.tipoEvi = tipoEvi;
	}
	@Override
	public String toString() {
		return "HallazgosEviExpDTO [idHallazgo=" + idHallazgo + ", idEvi=" + idEvi + ", idResp=" + idResp + ", status="
				+ status + ", ruta=" + ruta + ", nombre=" + nombre + ", totEvi=" + totEvi + ", aux=" + aux
				+ ", bitaGral=" + bitaGral + ", periodo=" + periodo + ", tipoEvi=" + tipoEvi + "]";
	}
	
	
		
	}