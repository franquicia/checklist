package com.gruposalinas.checklist.business;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
//import org.jboss.xnio.log.Logger;

import com.gruposalinas.checklist.dao.BitacoraDAO;
import com.gruposalinas.checklist.dao.CheckSoporteAdmDAO;
import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.CheckSoporteAdmDTO;
import com.gruposalinas.checklist.domain.ChecklistLayoutDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import com.gruposalinas.checklist.util.UtilFRQ;

public class CheckSoporteAdmBI {

	@Autowired
	CheckSoporteAdmDAO checkSoporteAdmDAO;
	private static Logger logger = LogManager.getLogger(BitacoraBI.class);
	List<CheckSoporteAdmDTO> checks = null;

	public List<CheckSoporteAdmDTO> checkAutorizacion(String idGrup) {

		try {
			checks = checkSoporteAdmDAO.checkAutorizacion(idGrup);
		} catch (Exception e) {
			logger.info("No fue posible consultar  " + e);
		}

		return checks;

	}
	public List<CheckSoporteAdmDTO> checkActivos(String idGrupo) {

		try {
			checks = checkSoporteAdmDAO.checkActivos( idGrupo);
		} catch (Exception e) {
			logger.info("No fue posible consultar " + e);
		}

		return checks;

	}
	public boolean actualiza(CheckSoporteAdmDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = checkSoporteAdmDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar"+e);
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	public List<CheckSoporteAdmDTO> checkVersParam(String param, String idGrupo) {

		try {
			checks = checkSoporteAdmDAO.checkVersParam(param,idGrupo);
		} catch (Exception e) {
			logger.info("No fue posible consultar  " + e);
		}

		return checks;

	}
	
	public List<CheckSoporteAdmDTO> checkVersParamProd(String param, String idGrupo) {

		try {
			checks = checkSoporteAdmDAO.checkVersParamProd(param,idGrupo);
		} catch (Exception e) {
			logger.info("No fue posible consultar  " + e);
		}

		return checks;

	}
}
