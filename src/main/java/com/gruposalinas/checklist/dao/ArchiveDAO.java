package com.gruposalinas.checklist.dao;

public class ArchiveDAO {

/*

	public static final String COLLECTION_NAME = "archive";
	public static final String JOIN_COLLECTION_NAME = "searchHistory";

	public void addArchive(Archive archive) {

		archive.setId(UUID.randomUUID().toString());
		mongoTemplate.insert(archive, COLLECTION_NAME);

	}

	public List<Archive> listArchive() {
		return mongoTemplate.findAll(Archive.class, COLLECTION_NAME);
	}

	public void deleteArchive(Archive archive) {
		mongoTemplate.remove(archive, COLLECTION_NAME);
	}

	public void updateArchive(Archive archive) {
		mongoTemplate.insert(archive, COLLECTION_NAME);
	}

	// Devuelve una lista de documentos que coinciden con la cadena de búsqueda
	public List<DBObject> findArchives(String find) {

		DBObject findCommand = new BasicDBObject("$text", new BasicDBObject("$search", find));

		DBObject projectCommand = new BasicDBObject("score", new BasicDBObject("$meta", "textScore"));

		DBObject sortCommand = new BasicDBObject("score", new BasicDBObject("$meta", "textScore"));
		DBCursor result = mongoTemplate.getCollection(COLLECTION_NAME).find(findCommand, projectCommand)
				.sort(sortCommand);

		return result.toArray();

	}

	public Iterable<DBObject> groupSearchByArchive(String find) {

		BasicDBObject $groupId = new BasicDBObject("_id",
				new BasicDBObject("title", "$title").append("fileName", "$fileName"));

		$groupId = $groupId.append("pageText", new BasicDBObject("$first", "$pageText"));

		$groupId = $groupId.append("description", new BasicDBObject("$first", "$description"));

		$groupId = $groupId.append("count", new BasicDBObject("$sum", 1));

		BasicDBObject $group = new BasicDBObject("$group", $groupId);

		Iterable<DBObject> results = (Iterable<DBObject>) mongoTemplate.getCollection(COLLECTION_NAME)
				.aggregate(Arrays.asList(
						(DBObject) new BasicDBObject("$match",
								new BasicDBObject("$text", new BasicDBObject("$search", find))),
						(DBObject) new BasicDBObject("$sort",
								new BasicDBObject("score", new BasicDBObject("$meta", "textScore"))),
						(DBObject) $group))
				.results();
		return results;
	}

	
	 * Obtiene los números de página en donde se encontraron coincidencias para
	 * los términos buscados.
	 * 
	 * @param find
	 *            cadena con los términos a buscar.
	 * @param fileName
	 *            cadena que representa el nombre del archivo en el que se
	 *            buscará.
	 * @return los números de página.
	 

	public Iterable<DBObject> getPageNumMatches(String find, String fileName) {

		BasicDBObject $project = new BasicDBObject("$project",

				new BasicDBObject("fileName", 1)

						.append("pageNum", 1)

						.append("score", new BasicDBObject("$meta", "textScore")));

		BasicDBObject $groupId = new BasicDBObject("_id",
				new BasicDBObject("fileName", "$fileName").append("pageNum", "$pageNum"));

		$groupId = $groupId.append("score", new BasicDBObject("$first", "$score"));

		BasicDBObject $group = new BasicDBObject("$group", $groupId);

		BasicDBObject $grpAndPush = new BasicDBObject("$group",

				new BasicDBObject("_id",

						new BasicDBObject("fileName", "$_id.fileName"))

								.append("pageNums", new BasicDBObject("$push", "$_id.pageNum")));

		Iterable<DBObject> output = (Iterable<DBObject>) mongoTemplate.getCollection(COLLECTION_NAME).aggregate(

				Arrays.asList(

						(DBObject) new BasicDBObject("$match",
								new BasicDBObject("$text", new BasicDBObject("$search", find)))

						, (DBObject) new BasicDBObject("$match", new BasicDBObject("fileName", fileName))

						, (DBObject) new BasicDBObject("$sort",
								new BasicDBObject("score", new BasicDBObject("$meta", "textScore")))

						, $project

						, (DBObject) $group

						, (DBObject) new BasicDBObject("$sort", new BasicDBObject("score", -1))

						, $grpAndPush

				)).results();

		return output;

	}

	/**
	 * Búsqueda en el campo pageText. Obtiene el título, texto de la página,
	 * nombre del archivo y cantidad de páginas en donde encontró coincidencias
	 * tras buscar texto en el campo pageText campos metadata.title y
	 * metadata.description. Los resultados se agrupan por nombre de archivo
	 * (filename).
	 * 
	 * @param find
	 *            cadena con los términos a buscar.
	 * @return título, texto de página, evaluación promedio, nombre de archivo y
	 *         cantidad de páginas.
	 
	public Iterable<DBObject> searchPageText(String find) {
		BasicDBObject $lookup = new BasicDBObject("$lookup", new BasicDBObject("from", JOIN_COLLECTION_NAME)
				.append("localField", "fileName").append("foreignField", "fileName").append("as", "scores"));

		BasicDBObject $project = new BasicDBObject("$project",
				new BasicDBObject("fileName", 1).append("pageText", 1).append("title", 1)
						.append("score", new BasicDBObject("$meta", "textScore")).append("average",
								new BasicDBObject("$avg", "$scores.score")));

		BasicDBObject $groupId = new BasicDBObject("_id", new BasicDBObject("fileName", "$fileName"));
		$groupId = $groupId.append("pageText", new BasicDBObject("$first", "$pageText"));
		$groupId = $groupId.append("title", new BasicDBObject("$first", "$title"));
		$groupId = $groupId.append("count", new BasicDBObject("$sum", 1));
		$groupId = $groupId.append("score", new BasicDBObject("$first", "$score"));
		$groupId = $groupId.append("average", new BasicDBObject("$first", "$average"));
		BasicDBObject $group = new BasicDBObject("$group", $groupId);

		Iterable<DBObject> output = (Iterable<DBObject>) mongoTemplate.getCollection(COLLECTION_NAME)
				.aggregate(Arrays.asList(
						(DBObject) new BasicDBObject("$match",
								new BasicDBObject("$text", new BasicDBObject("$search", find))),
						(DBObject) new BasicDBObject("$sort",
								new BasicDBObject("score", new BasicDBObject("$meta", "textScore"))),
						$lookup, $project, (DBObject) $group,
						(DBObject) new BasicDBObject("$sort", new BasicDBObject("score", -1))))
				.results();
		return output;
	}

	*/
	
	/*
	 * :::METODO SUSTITUIDO::: public CommandResult groupSearchByArchive(String
	 * find) {
	 * 
	 * CommandResult cmdResult = mongoTemplate.executeCommand(
	 * 
	 * "{aggregate: '" + COLLECTION_NAME + "', "
	 * 
	 * + "pipeline: [ "
	 * 
	 * + "{ $match: { $text: { $search: '"+ find +"' } } },"
	 * 
	 * + "{ $sort: { score: { $meta: 'textScore' } } },"
	 * 
	 * + "{ $group: { "
	 * 
	 * + "_id: '$title', "
	 * 
	 * + "pageText: { $first: '$pageText' }, "
	 * 
	 * + "description: { $first: '$description' }, "
	 * 
	 * + "count: { $sum: 1 } } }]}");
	 * 
	 * return cmdResult;
	 * 
	 * }
	 */

}
