package com.gruposalinas.checklist.servicios.servidor;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.AdmPregZonasBI;
import com.gruposalinas.checklist.business.AdmZonasBI;
import com.gruposalinas.checklist.business.ArbolRespAdiEviAdmBI;
import com.gruposalinas.checklist.business.CheckAutProtoBI;
import com.gruposalinas.checklist.business.PosiblesAdmBI;
import com.gruposalinas.checklist.business.PreguntaAdmBI;
import com.gruposalinas.checklist.business.VersionCheckAdmBI;
import com.gruposalinas.checklist.dao.BitacoraCheckUsuAdmDAO;
import com.gruposalinas.checklist.resources.FRQAuthInterceptor;

@Controller
@RequestMapping("/catalogosAdmService")

public class BajaChecklistAdminService {
	@Autowired
	PreguntaAdmBI preguntaAdmBI;
	@Autowired
	BitacoraCheckUsuAdmDAO  bitacoraCheckUsuAdmDAO;
	@Autowired
	ArbolRespAdiEviAdmBI arbolRespAdiEviAdmBI;
	@Autowired
	CheckAutProtoBI checkAutProtoBI;
	@Autowired
	PosiblesAdmBI posiblesAdmBI;
	@Autowired
	VersionCheckAdmBI versionCheckAdmBI;
	@Autowired
	AdmPregZonasBI admPregZonasBI;
	@Autowired
	AdmZonasBI admZonasBI;
	
	private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);
	
	// http://localhost:8080/checklist/catalogosAdmService/eliminaModulo.json?idModulo=<?>
		@RequestMapping(value = "/eliminaModulo", method = RequestMethod.GET)
		public ModelAndView eliminaModulo(HttpServletRequest request, HttpServletResponse response)
				throws UnsupportedEncodingException {
			String idModulo = request.getParameter("idModulo");

			boolean res = preguntaAdmBI.eliminaModulo(Integer.parseInt(idModulo));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "MODULO ELIMINADO");
			mv.addObject("res", res);
			return mv;
		}
		
		// http://localhost:8080/checklist/catalogosAdmService/eliminaCheck.json?idChecklist=<?>
		@RequestMapping(value = "/eliminaCheck", method = RequestMethod.GET)
		public ModelAndView eliminaCheck(HttpServletRequest request, HttpServletResponse response)
				throws UnsupportedEncodingException {
			try {
				int idChecklist = Integer.parseInt(request.getParameter("idChecklist"));

				boolean res = preguntaAdmBI.eliminaChecklist(idChecklist);

				ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
				mv.addObject("tipo", "CHECKLIST ELIMINADO");
				mv.addObject("res", res);
				return mv;
			} catch (Exception e) {
				logger.info(e);
				return null;
			}

		}

		// http://localhost:8080/checklist/catalogosAdmService/eliminaPreguntaService.json?idPregunta=<?>
		@RequestMapping(value = "/eliminaPreguntaService", method = RequestMethod.GET)
		public @ResponseBody boolean eliminaPreguntaService(HttpServletRequest request, HttpServletResponse response)
				throws UnsupportedEncodingException {
			try {
				String idPregunta = request.getParameter("idPregunta");

				boolean res = this.preguntaAdmBI.eliminaPregunta(Integer.parseInt(idPregunta));

				
				return res;
			} catch (Exception e) {
				logger.info(e);
				return false;
			}
		}

		
		// http://localhost:8080/checklist/catalogosAdmService/eliminaPregCheck.json?idChecklist=<?>&idPreg=<?>
		@RequestMapping(value = "/eliminaPregCheck", method = RequestMethod.GET)
		public ModelAndView eliminaPreguntaCheck(HttpServletRequest request, HttpServletResponse response) {
			try {
				String idCheck = request.getParameter("idChecklist");
				String idPreg = request.getParameter("idPreg");

				boolean res = preguntaAdmBI.eliminaChecklistPregunta(Integer.parseInt(idCheck),
						Integer.parseInt(idPreg));

				ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
				mv.addObject("tipo", "PREGUNTA CHECKLIST ELIMINADA");
				mv.addObject("res", res);
				return mv;
			} catch (Exception e) {
				logger.info(e);
				return null;
			}
		}
		
		// http://localhost:8080/checklist/catalogosAdmService/eliminaBitacora.json?idBitacora=<?>
		@RequestMapping(value = "/eliminaBitacora", method = RequestMethod.GET)
		public ModelAndView eliminaBitacora(HttpServletRequest request, HttpServletResponse response)
				throws UnsupportedEncodingException {
			try {
				String idBitacora = request.getParameter("idBitacora");

				boolean res = bitacoraCheckUsuAdmDAO.eliminaBitacora(Integer.parseInt(idBitacora));

				ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
				mv.addObject("tipo", "BITACORA ELIMINADA");
				mv.addObject("res", res);
				return mv;
			} catch (Exception e) {
				logger.info(e);
				return null;
			}
		}
		
		// http://localhost:8080/checklist/catalogosAdmService/desactivaCheckusua.json?idChecklist=<?>&ceco=&usu=
		@RequestMapping(value = "/desactivaCheckusua", method = RequestMethod.GET)
		public ModelAndView desactivaCheckusua(HttpServletRequest request, HttpServletResponse response) {
			try {
				int  idChecklist = Integer.parseInt(request.getParameter("idChecklist"));
				int ceco = Integer.parseInt(request.getParameter("ceco"));
				int usu = Integer.parseInt(request.getParameter("usu"));

				boolean res = bitacoraCheckUsuAdmDAO.desactivaCheckUsua(idChecklist,ceco,usu);

				ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
				mv.addObject("tipo", "ID CHECKLIST DESACTIVADO");
				mv.addObject("res", res);
				return mv;
			} catch (Exception e) {
				logger.info(e);
				return null;
			}
		}
		
		// http://localhost:8080/checklist/catalogosAdmService/eliminaArbolDecision.json?idArbolDecision=<?>
		@RequestMapping(value = "/eliminaArbolDecision", method = RequestMethod.GET)
		public ModelAndView eliminaArbolDecision(HttpServletRequest request, HttpServletResponse response)
				throws UnsupportedEncodingException {
			try {
				String idArbolDecision = request.getParameter("idArbolDecision");

				boolean res = arbolRespAdiEviAdmBI.eliminaArbolDecision(Integer.parseInt(idArbolDecision));

				ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
				mv.addObject("tipo", "ARBOL ELIMINADO CON EXITO--->");
				mv.addObject("res", res);
				return mv;
			} catch (Exception e) {
				logger.info(e);
				return null;
			}
		}
		
		// http://localhost:8080/checklist/catalogosAdmService/eliminaRespuesta.json?idRespuesta=<?>
		@RequestMapping(value = "/eliminaRespuesta", method = RequestMethod.GET)
		public ModelAndView eliminaRespuesta(HttpServletRequest request, HttpServletResponse response)
				throws UnsupportedEncodingException {
			try {
				String idRespuesta = request.getParameter("idRespuesta");

				boolean res = arbolRespAdiEviAdmBI.eliminaRespuesta(Integer.parseInt(idRespuesta));

				ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
				mv.addObject("tipo", "RESPUESTA ELIMINADA");
				mv.addObject("res", res);
				return mv;
			} catch (Exception e) {
				logger.info(e);
				return null;
			}
		}
		// http://localhost:8080/checklist/catalogosAdmService/eliminaRespAd.json?idRespAd=<?>
		@RequestMapping(value = "/eliminaRespAd", method = RequestMethod.GET)
		public ModelAndView eliminaRespA(HttpServletRequest request, HttpServletResponse response)
				throws UnsupportedEncodingException {
			try {
				String idRespAd = request.getParameter("idRespAd");

				boolean idRespA = arbolRespAdiEviAdmBI.eliminaRespAD(Integer.parseInt(idRespAd));

				ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
				mv.addObject("tipo", "RESPUESTA ADICIONAL ELIMINADA");
				mv.addObject("res", idRespA);
				return mv;
			} catch (Exception e) {
				logger.info(e);
				return null;
			}
		}
		
		// http://localhost:8080/checklist/catalogosAdmService/eliminaEvidencia.json?idEvidencia=<?>
		@RequestMapping(value = "/eliminaEvidencia", method = RequestMethod.GET)
		public ModelAndView eliminaEvidencia(HttpServletRequest request, HttpServletResponse response)
				throws UnsupportedEncodingException {
			try {
				String idEvidencia = request.getParameter("idEvidencia");

				boolean idEvi = arbolRespAdiEviAdmBI.eliminaEvidencia(Integer.parseInt(idEvidencia));

				ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
				mv.addObject("tipo", "EVIDENCIA ELIMNADA");
				mv.addObject("res", idEvi);
				return mv;
			} catch (Exception e) {
				logger.info(e);
				return null;
			}
		}
		
		// http://localhost:8080/checklist/catalogosAdmService/depuraTabs.json?
				@RequestMapping(value = "/depuraTabs", method = RequestMethod.GET)
				public ModelAndView depuraTabs(HttpServletRequest request, HttpServletResponse response)
						throws UnsupportedEncodingException {
					try {
	
						boolean idRespA = arbolRespAdiEviAdmBI.depuraTabs();

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Tablas Administrador Depuradas");
						mv.addObject("res", idRespA);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;
					}
				}
				
				// http://localhost:8080/checklist/catalogosAdmService/depuraTabParam.json?idCheck
				@RequestMapping(value = "/depuraTabParam", method = RequestMethod.GET)
				public ModelAndView depuraTabParam(HttpServletRequest request, HttpServletResponse response)
						throws UnsupportedEncodingException {
					try {
	
							int idCheck=Integer.parseInt(request.getParameter("idCheck"));
						boolean idRespA = arbolRespAdiEviAdmBI.depuraTabParam(idCheck);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Id Check --->  "+idCheck+" ELIMINADA");
						mv.addObject("res", idRespA);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;
					}
				}
				
				// http://localhost:8080/checklist/catalogosAdmService/eliminaAutoCheck.json?
				@RequestMapping(value = "/eliminaAutoCheck", method = RequestMethod.GET)
				public ModelAndView eliminaAutoCheck(HttpServletRequest request, HttpServletResponse response)
						throws UnsupportedEncodingException {
					try {
	
						int idCheck=Integer.parseInt(request.getParameter("idCheck"));
						boolean idRespA = checkAutProtoBI.eliminaAutoProto(idCheck);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Id Check --->  "+idCheck+"  ELIMINADA");
						mv.addObject("res", idRespA);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;
					}
				}
				// http://localhost:8080/checklist/catalogosAdmService/eliminaPosible.json?idPosible=<?>
				@RequestMapping(value = "/eliminaPosible", method = RequestMethod.GET)
				public ModelAndView eliminaPosible(HttpServletRequest request, HttpServletResponse response)
				{
					try {

						String idPosible = request.getParameter("idPosible");

						boolean res = posiblesAdmBI.eliminaPosible(Integer.parseInt(idPosible));

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Posible Respuesta  eliminado ");
						mv.addObject("res", res);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;  
					}
				}
				
				// http://localhost:8080/checklist/catalogosAdmService/eliminaVersionCheckAdm.json?idtab=<?>
				@RequestMapping(value = "/eliminaVersionCheckAdm", method = RequestMethod.GET)
				public ModelAndView eliminaVersionCheckAdm(HttpServletRequest request, HttpServletResponse response)
				{
					try {

						String idtab = request.getParameter("idtab");

						boolean res = versionCheckAdmBI.elimina(idtab);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "VersionCheckAdm  eliminado ");
						mv.addObject("res", res);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;  
					}
				}
				// http://localhost:8080/checklist/catalogosAdmService/eliminaVersionNegoAdm.json?idtab=<?>
				@RequestMapping(value = "/eliminaVersionNegoAdm", method = RequestMethod.GET)
				public ModelAndView eliminaVersionNegoAdm(HttpServletRequest request, HttpServletResponse response)
				{
					try {

						String idtab = request.getParameter("idtab");

						boolean res = versionCheckAdmBI.eliminaNegoVer(idtab);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "versionNegocio eliminado ");
						mv.addObject("res", res);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;  
					}
				}
}
