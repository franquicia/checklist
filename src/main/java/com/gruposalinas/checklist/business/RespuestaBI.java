package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.RespuestaDAO;
import com.gruposalinas.checklist.domain.EvidenciaDTO;
import com.gruposalinas.checklist.domain.RegistroRespuestaDTO;
import com.gruposalinas.checklist.domain.RespuestaDTO;
 
public class RespuestaBI {

	private static Logger logger = LogManager.getLogger(RespuestaBI.class);

	@Autowired
	RespuestaDAO respuestaDAO;
	
	
	List<RespuestaDTO> listaRespuestas = null;
	List<RespuestaDTO> listaRespuesta = null;
	
	public List<RespuestaDTO> obtieneRespuesta(){
		try{
			listaRespuestas = respuestaDAO.obtieneRespuesta();
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Respuesta");
					
		}
				
		return listaRespuestas;
	}
	
	public List<RespuestaDTO> obtieneRespuesta(String idArbol, String idRespuesta, String idBitacora){
		try{
			listaRespuesta = respuestaDAO.obtieneRespuesta(idArbol, idRespuesta, idBitacora);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Respuesta");
			
		}
				
		return listaRespuesta;
	}
	
	public int insertaRespuesta (RespuestaDTO respuesta){
		int idRespuesta = 0;
		try{
			idRespuesta = respuestaDAO.insertaRespuesta(respuesta);
		} 
		catch (Exception e) {
			logger.info("No fue posible insertar la Respuesta");
			
		}
		
		return idRespuesta;	
	}
	
	public boolean actualizaRespuesta(RespuestaDTO resp){
		boolean respuesta = false;
		try{
			respuesta = respuestaDAO.actualizaRespuesta(resp);
		}
		catch (Exception e) {
			logger.info("No fue posible actualizar la Respuesta");
			
		}
							
		return respuesta;
	}
	
	public boolean actualizaFechaResp(String idRespuesta, String fechaTermino, String commit){
		boolean respuesta = false;
		try{
			respuesta = respuestaDAO.actualizaFechaResp(idRespuesta, fechaTermino, commit);
		}
		catch (Exception e) {
			logger.info("No fue posible actualizar la fecha de la Respuesta");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaRespuesta(int idRespuesta){
		boolean respuesta = false;
		try{
			respuesta = respuestaDAO.eliminaRespuesta(idRespuesta);
		}
		catch(Exception e){
			logger.info("No fue posible borrar la Respuesta");
			
		}
		
		return respuesta;
	}
	
	public boolean eliminaRespuestasDuplicadas(){
		boolean respuesta = false;
		try{
			respuesta = respuestaDAO.eliminaRespuestasDuplicadas();
		}
		catch(Exception e){
			logger.info("No fue posible borrar la Respuesta");
			logger.info(e.getMessage());
		}
		
		return respuesta;
	}
	
	
	public boolean registraRespuestas(RegistroRespuestaDTO respuestasChecklist){

		int idCheckUsua;
		String idRespuestas="";
		String compromisos="";
		String evidencias= "";
		int cont = 0;
		
		boolean respuesta = false;
		
		try { 
			
			idCheckUsua = respuestasChecklist.getIdCheckUsua();

			for (RespuestaDTO respuestaDTO : respuestasChecklist.getListaRespuestas() ) {

				idRespuestas += String.valueOf(respuestaDTO.getIdRespuesta());
				idRespuestas +="!~OBJ~!";
				
				if(!respuestaDTO.getObservacion().equals("")){
					idRespuestas += respuestaDTO.getObservacion();
				}
				else
				{
					idRespuestas += " ";
			        
				}
				idRespuestas +="!~OBJ~!"; 

				if(respuestaDTO.getIdTipoRespuesta() == 2)
					idRespuestas += respuestaDTO.getRespuestaAbierta();
				else 
					idRespuestas += " ";
						

				if ( respuestaDTO.getCompromiso() != null  ){
					compromisos += respuestaDTO.getCompromiso().getDescripcion();
					compromisos +="!~OBJ~!";					
					compromisos += respuestaDTO.getCompromiso().getFechaCompromiso();
					compromisos +="!~OBJ~!";					
					compromisos += respuestaDTO.getCompromiso().getIdResponsable();
				}else{
					compromisos += " ";
				}
									
				if(respuestaDTO.getEvidencia() != null){
					evidencias +=respuestaDTO.getEvidencia().getIdTipo();
					evidencias +="!~OBJ~!";
					evidencias += respuestaDTO.getEvidencia().getRuta();
				}else{
					evidencias += " ";
				}
									
				if(cont < respuestasChecklist.getListaRespuestas().size() ){
					idRespuestas += "!<ATRRIB>!";
					compromisos += "!<ATRRIB>!";
					evidencias += "!<ATRRIB>!";
				}
									
				cont ++;
			}
			
			/*//System.out.println("id check: " + idCheckUsua );
			//System.out.println("id bitacora: " + respuestasChecklist.getBitacora() );
			//System.out.println("id respuesta: " + idRespuestas );
			//System.out.println("compromiso: " + compromisos );
			//System.out.println("evidencias: " + evidencias );*/
			
			respuesta = respuestaDAO.registraRespuestas(idCheckUsua, respuestasChecklist.getBitacora(), idRespuestas, compromisos, evidencias);
	
		}catch(Exception e){
			logger.info("No fue posible Registrar las respuestas del Usuario");
			
		}
		
		return respuesta;
	}
	
	public boolean insertaUnaRespuesta(RegistroRespuestaDTO registroUnaResp){

		int idCheckUsua;
		int idBitacora;
		int idArbol;
		int idPreg;
		String obs;
		String respAd;
		String compromisos="";
		String evidencias= "";
		
		boolean respuesta = false;
		
		
		try {
			logger.info("entro!!!");
			
			idCheckUsua = registroUnaResp.getIdCheckUsua();
			logger.info("entro 1!!!" + idCheckUsua);
			idBitacora = registroUnaResp.getBitacora().getIdBitacora();
			logger.info("entro 2!!!"+idBitacora);
			idArbol = registroUnaResp.getIdArbol();
			logger.info("entro 3!!!"+idArbol);
			idPreg = registroUnaResp.getIdPreg();
			logger.info("entro 4!!!"+idPreg);
			obs = registroUnaResp.getObservacion();
			logger.info("entro 5!!!"+obs);
			respAd = registroUnaResp.getRespAd();


			logger.info("entro por acá!!!"+ registroUnaResp.getListaRespuestas());
			
			for (RespuestaDTO respuestaDTO : registroUnaResp.getListaRespuestas() ) {

				if ( respuestaDTO.getCompromiso() != null  ){
					compromisos += respuestaDTO.getCompromiso().getDescripcion();
					compromisos +="!~OBJ~!";					
					compromisos += respuestaDTO.getCompromiso().getFechaCompromiso();
					compromisos +="!~OBJ~!";					
					compromisos += respuestaDTO.getCompromiso().getIdResponsable();
				}else{
					compromisos += " ";
				}
									
				if(respuestaDTO.getEvidencia() != null){
					evidencias +=respuestaDTO.getEvidencia().getIdTipo();
					evidencias +="!~OBJ~!";
					evidencias += respuestaDTO.getEvidencia().getRuta();
				}else{
					evidencias += " ";
				}

				logger.info("entro otra vez!!!");
				
			}
			
			//System.out.println("id check: " + idCheckUsua );
			//System.out.println("id bitacora: " + idBitacora);
			//System.out.println("idPreg: " + idPreg);
			//System.out.println("id Arbol: " + idArbol);
			//System.out.println("obs: " + obs);
			//System.out.println("respAd: " + respAd);
			//System.out.println("compromiso: " + compromisos );
			//System.out.println("evidencias: " + evidencias );
			
			respuesta = respuestaDAO.insertaUnaRespuesta(idCheckUsua, idBitacora, idPreg, idArbol, obs, compromisos, evidencias, respAd);
	
		}catch(Exception e){
			logger.info("No fue posible Registrar la respuesta del Usuario");
			
		}
		
		return respuesta;
	}


	public boolean eliminaRespuestas(RegistroRespuestaDTO registroUnaResp){
		boolean respuesta = false;
		String respuestas = "";
		
		try{
			for (RespuestaDTO respuestaDTO : registroUnaResp.getListaRespuestas() ) {

				
				if ( respuestaDTO.getIdPregunta() != 0  ){
					respuestas += respuestaDTO.getIdPregunta();
					respuestas +="!~OBJ~!";					
				}
				
				if ( respuestaDTO.getIdBitacora() != 0  ){
					respuestas += respuestaDTO.getIdBitacora();
					respuestas +="!~OBJ~!";					
				}
				
				if ( respuestaDTO.getIdCheckUsuario() != 0  ){
					respuestas += respuestaDTO.getIdCheckUsuario();
				}
				
				if (respuestas != null){
					respuestas += "!<ATRRIB>!";
				}
				logger.info("RESPUESTAS: "+respuestas);
			}
			

			respuesta = respuestaDAO.eliminaRespuestas(respuestas);
	
		}
		catch(Exception e){
			logger.info("No fue posible borrar la Respuesta");
			
		}
		
		return respuesta;
	}
	
	public boolean insertaNuevaRespuesta(RegistroRespuestaDTO registroUnaResp){

		int idCheckUsua;
		int idBitacora;
		int idArbol;
		int idPreg;
		String obs;
		String respAd;
		String compromisos="";
		String evidencias= "";
		
		boolean respuesta = false;
		
		
		try {
			logger.info("entro!!!");
			
			idCheckUsua = registroUnaResp.getIdCheckUsua();
			logger.info("entro 1!!!" + idCheckUsua);
			idBitacora = registroUnaResp.getBitacora().getIdBitacora();
			logger.info("entro 2!!!"+idBitacora);
			idArbol = registroUnaResp.getIdArbol();
			logger.info("entro 3!!!"+idArbol);
			idPreg = registroUnaResp.getIdPreg();
			logger.info("entro 4!!!"+idPreg);
			obs = registroUnaResp.getObservacion();
			logger.info("entro 5!!!"+obs);
			respAd = registroUnaResp.getRespAd();
			
			if(idArbol==4419 || idArbol==4437 || idArbol==4439 || idArbol==4443 || idArbol==4445 || idArbol==4447 ) {
				logger.info("entro ");
			}


			logger.info("entro por acá!!!"+ registroUnaResp.getListaRespuestas());
			
			for (RespuestaDTO respuestaDTO : registroUnaResp.getListaRespuestas() ) {

				if ( respuestaDTO.getCompromiso() != null  ){
					compromisos += respuestaDTO.getCompromiso().getDescripcion();
					compromisos +="!~OBJ~!";					
					compromisos += respuestaDTO.getCompromiso().getFechaCompromiso();
					compromisos +="!~OBJ~!";					
					compromisos += respuestaDTO.getCompromiso().getIdResponsable();
				}else{
					compromisos += " ";
				}
									
				for (EvidenciaDTO evidenciaDTO : registroUnaResp.getListaEvidencia() )
					{	
						if(evidenciaDTO.getRuta() != null)
						{			
					evidencias +=evidenciaDTO.getIdTipo();
					evidencias +="!~OBJ~!";
					evidencias += evidenciaDTO.getRuta();
					evidencias +="!~OBJ~!";
					evidencias += evidenciaDTO.getIdPlantilla();
					
					
						}
					evidencias += "!<ATRRIB>!";
				}

				logger.info("entro otra vez!!!");
				
			}
			logger.info("evidencias "+evidencias);
			logger.info("compromisos: "+compromisos);
			
			respuesta = respuestaDAO.insertaRespuestaNueva(idCheckUsua, idBitacora, idPreg, idArbol, obs, compromisos, evidencias, respAd);
	
		}catch(Exception e){
			logger.info("No fue posible Registrar la respuesta del Usuario");
			
		}
		
		return respuesta;
	}
	
	
}

