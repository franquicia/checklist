/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.util.plataformaFrq;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;


/**
 *
 * @author leodan1991
 */
public class TokenPlataforma {

    @JsonProperty("access_token")
    private String accessToken;
    @JsonProperty("token_type")
    private String tokenType;
    @JsonProperty("refresh_token")
    private String refreshToken;
    @JsonProperty("expires_in")
    private long expiresIn;
    @JsonProperty("scope")
    private String scope;
    @JsonProperty("ceco")
    private String ceco;
    @JsonProperty("empleado")
    private String empleado;
    @JsonProperty("nombre")
    private String nombre;
    @JsonProperty("jti")
    private String jti;

    @JsonCreator
    public TokenPlataforma(
            @JsonProperty("access_token") String accessToken,
            @JsonProperty("token_type") String tokenType,
            @JsonProperty("refresh_token") String refreshToken,
            @JsonProperty("expires_in") long expiresIn,
            @JsonProperty("scope") String scope,
            @JsonProperty("ceco") String ceco,
            @JsonProperty("empleado") String empleado,
            @JsonProperty("nombre") String nombre,
            @JsonProperty("jti") String jti) {

        this.accessToken = accessToken;
        this.tokenType = tokenType;
        this.refreshToken = refreshToken;
        this.expiresIn = expiresIn;
        this.scope = scope;
        this.ceco = ceco;
        this.empleado = empleado;
        this.nombre = nombre;
        this.jti = jti;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getEmpleado() {
        return empleado;
    }

    public void setEmpleado(String empleado) {
        this.empleado = empleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getJti() {
        return jti;
    }

    public void setJti(String jti) {
        this.jti = jti;
    }
    
    

    @Override
    public String toString() {
        return 
                "\n accessToken=" + accessToken + 
                "\n tokenType=" + tokenType + 
                "\n refreshToken=" + refreshToken + 
                "\n expiresIn=" + expiresIn + 
                "\n scope=" + scope + 
                "\n ceco=" + ceco + 
                "\n empleado=" + empleado + 
                "\n nombre=" + nombre + 
                "\n jti=" + jti ;
    }

}


