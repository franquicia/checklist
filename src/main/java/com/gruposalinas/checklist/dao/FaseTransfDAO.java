package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.FaseTransfDTO;


public interface FaseTransfDAO {

	  public int inserta(FaseTransfDTO bean)throws Exception;
		
		public boolean elimina(String idTab)throws Exception;
		
		public List<FaseTransfDTO> obtieneDatos(String fase) throws Exception; 
		
		
		public boolean actualiza(FaseTransfDTO bean)throws Exception;
		
//agrupa
		
	  public int insertaAgrupa(FaseTransfDTO bean)throws Exception;
		
	  public boolean eliminaAgrupa(String idTab)throws Exception;
		
	  public List<FaseTransfDTO> obtieneDatosAgrupa(String fase) throws Exception; 
		
		
	  public boolean actualizaAgrupa(FaseTransfDTO bean)throws Exception;
			
		
	}