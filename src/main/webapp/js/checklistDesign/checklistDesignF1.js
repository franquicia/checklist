var urlServer = $(location).attr('host');
var editarTipoCheck = false;
var periodicidad = "undefinied";

$(document).ready(function() {

	if ($("#periodicidad .seleccionado").text() == "Semanal")
		$("#fecha").show();
	else
		ocultarComponente("fecha");

	asignarEventosCombo();
	cargarCatalogoTipoChecklist();

});

function mostrarComponente(nombreComponente) {

	$("#" + nombreComponente).show();

	// return false;
}
function ocultarComponente(nombreComponente) {

	$("#" + nombreComponente).css("display", "none");

	// return false;
}

function asignarEventosCombo() {
	/*
	 * se asignan los eventos para los combos agregados en el div dinamico del
	 * arbol de deciciones del checklist
	 */

	$(".caja").off();

	$(".caja").hover(

	function(e) {

		asignarEventosCombo();

		var lista = $(this).find("ul");
		var triangulo = $(this).find("span:last-child");
		e.preventDefault();

		$(this).find("ul").toggle();
		if (lista.is(":hidden")) {
			triangulo.removeClass("triangulosup").addClass("trianguloinf");
			// lista.hide();

			triangulo.parent().css('z-index', 0);
		} else {
			// lista.hide();
			triangulo.removeClass("trianguloinf").addClass("triangulosup");
			triangulo.parent().css('z-index', 3000);
		}

	});

	$(".caja").on("click", "li", function(e) {

		var texto = $(this).text();
		var seleccionado = $(this).parent().prev();
		// var seleccionado = $(this).parent().find(".seleccionado");

		var valor = $(this).val();

		var lista = $(this).closest("ul");
		var triangulo = $(this).parent().next();
		// var lista = $(this).find("ul").toggle();

		// var lista = $(this).parents("li").find(".listaselect");
		// var triangulo = $(this).find("span:last-child");
		// var oculto = $(this).parents("li").find(".caja").attr("id");

		e.preventDefault();
		e.stopPropagation();
		seleccionado.text(texto);
		// alert("DIV: "+oculto);

		// alert("VALOR: "+valor);
		// seleccionado.val(valor);
		seleccionado.attr("id", valor);
		lista.hide();
		$(".caja").unbind('mouseleave');
		triangulo.removeClass("triangulosup").addClass("trianguloinf");

		triangulo.parent().css('z-index', 0);

		// condiciones para la gestion de los tipos de checklist
		var nuevaPreg = $("#tipoChecklist .seleccionado").attr("id");

		if (nuevaPreg != "-2") {
			ocultarComponente("agregarTipoChecklist");
			ocultarComponente("nuevoTipoChecklist");

		} else {
			mostrarComponente("agregarTipoChecklist");
			mostrarComponente("nuevoTipoChecklist");

		}
		// si cambia al combo se actualiza el estaus de la edicion
		editarTipoCheck = false;

	});

	$("#periodicidad").on("click", "li", function(e) {
		var texto = $(this).text();
		var seleccionado = $(this).parent().prev();
		var valor = $(this).val();
		e.preventDefault();
		e.stopPropagation();
		periodicidad = $(this).text();
		seleccionado.text(texto);
		seleccionado.attr("id", texto);

		if ($(this).text().localeCompare("Semanal") == 0) {
			$("#fecha").show();
			$("#fecha .seleccionado").text("Selecciona el dia");
			$("#fecha .seleccionado").attr("id", "-1");

		} else {
			ocultarComponente("fecha");
			$("#fecha .seleccionado").attr("id", "-1");
		}
	});

	$("#fecha").on("click", "li", function(e) {
		var texto = $(this).text();
		var seleccionado = $(this).parent().prev();
		var valor = $(this).val();

		e.preventDefault();
		e.stopPropagation();
		seleccionado.text(texto);
		seleccionado.attr("id", texto);
	});

}
/* se agregan los eventos del calendario */

$(function() {
	$("#fechaInicio").datepicker({

		minDate : new Date(),
		maxDate : "31/12/2050",

	});
});

$(function() {
	$("#fechaTermino").datepicker({

		minDate : new Date(),
		maxDate : "31/12/2050",
	});
});

function eventoHoraInicio() {
	$('#horaInicio').wickedpicker({
		now : '12:00',
		twentyFour : false,
		title : 'Hora de inicio',
		showSeconds : false
	});

	$("#wickedpicker__title").text("Hora de inicio");
	$(".wickedpicker").hide();

	return false;
}

function eventoHoraTermino() {
	$('#horaTermino').wickedpicker({
		now : '12:00',
		twentyFour : false,
		title : 'Hora de termino',
		showSeconds : false
	});

	$("#wickedpicker__title").text("Hora de termino");
	$(".wickedpicker").hide();

	return false;
}

/**
 * ************************************metodos para la gestion de los tipos de
 * checklist*******************************************
 */

function cargarCatalogoTipoChecklist() {
	$
			.ajax({
				type : "GET",
				url : encodeURI("http://"
						+ urlServer
						+ '../consultaChecklistService/getTiposCheckService.json'),
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				paramName : "tagName",
				delimiter : ",",
				success : function(json) {
					var jsonC = JSON.stringify(json);
					var i = 0;
					catalogoTipoChecklist = new Array();
					var contenido = '<li value="-1"><a href="#">Tipo de checklist</a></li>';
					contenido += '<li value="-2" ><a href="#">Agregar nuevo</a></li>';

					while (json[i] != null) {
						// document.forms.formulario.tipo.options[i+1] = new
						// Option(json[i].nombreCheck, json[i].idChecklist);
						catalogoTipoChecklist.push({
							"id" : json[i].idTipoCheck,
							"descripcion" : json[i].descTipo
						});
						contenido += '<li value="' + json[i].idTipoCheck
								+ '" ><a href="#">' + json[i].descTipo
								+ '</a></li>';

						i++;
						// alert("REG "+i+"--> id:
						// "+posiblesRespuestas[posiblesRespuestas.length-1].id+"idTipo:
						// "+posiblesRespuestas[posiblesRespuestas.length-1].idTipo+"
						// respuesta:
						// "+posiblesRespuestas[posiblesRespuestas.length-1].respuesta);
					}

					$("#tipoChecklist .listaselect").html(contenido);
					// alert("se ha cargado el catalogo!, total de regs:
					// "+posiblesRespuestas.length);
				},
				error : function() {
					alert('Algo paso al cargar los tipos de posibles respuestas');
				}
		 		,async: false
			});
}

function agregarTipoChecklistService() {
	var tipoChecklist = $("#nuevoTipoChecklist").val();
	// alert("http://"+urlServer +
	// '../checklistServices/altaTipoCheck.json?descTipoCheck='+tipoChecklist);
	$
			.ajax({
				type : "GET",
				url : encodeURI("http://"
						+ urlServer
						+ '../checklistServices/altaTipoCheck.json?descTipoCheck='
						+ tipoChecklist),
				// contentType : "application/json; charset=utf-8",
				// dataType : "json",
				// paramName : "tagName",
				// delimiter : ",",
				success : function() {
					// alert("Se agrego correctamente el tipo de checklist:
					// "+tipoChecklist);
					cargarCatalogoTipoChecklist();

				},
				error : function() {
					alert('Algo paso al agregar el tipo de checklist');

				}
		 		,async: false
			});

}

function modificarTipoChecklistService() {
	var tipoChecklist = $("#nuevoTipoChecklist").val();
	var idTipoCheck = $("#tipoChecklist .seleccionado").attr("id");
	// alert("http://"+urlServer +
	// '../checklistServices/updateTipoCheck.json?idTipoCheck='+idTipoCheck+'&descTipoCheck='+tipoChecklist);
	$
			.ajax({
				type : "GET",
				url : encodeURI("http://"
						+ urlServer
						+ '../checklistServices/updateTipoCheck.json?idTipoCheck='
						+ idTipoCheck + '&descTipoCheck=' + tipoChecklist),
				// contentType : "application/json; charset=utf-8",
				// dataType : "json",
				// paramName : "tagName",
				// delimiter : ",",
				success : function() {
					// alert("Se actualizo correctamente el tipo de checklist:
					// "+tipoChecklist);
					// se actualiza el catalogo de los tipos
					cargarCatalogoTipoChecklist();

				},
				error : function() {
					alert('Algo paso al modificar el tipo de checklist');

				}
		 		,async: false
			});

}

function eliminarTipoChecklistService() {
	var tipoChecklist = $("#tipoChecklist .seleccionado").text();
	var idTipoCheck = $("#tipoChecklist .seleccionado").attr("id");

	if (idTipoCheck == "-1" || idTipoCheck == "-2") {
		alert("No has seleccionado ninguna opcion");
		return false;
	} else {
		if (confirm("¿Deseas eliminar este registro[" + tipoChecklist + "]?\n")) {
			$
					.ajax({
						type : "GET",
						url : encodeURI("http://"
								+ urlServer
								+ '../checklistServices/eliminaTipoCheck.json?idTipoCheck='
								+ idTipoCheck),
						// contentType : "application/json; charset=utf-8",
						// dataType : "json",
						// paramName : "tagName",
						// delimiter : ",",
						success : function() {
							alert("Se elimino  correctamente el tipo de checklist: "
									+ tipoChecklist);
							// se actualiza el catalogo de los tipos de
							// checklist
							cargarCatalogoTipoChecklist();
							editarTipoCheck = false;

							ocultarComponente('agregarTipoChecklist');
							ocultarComponente('nuevoTipoChecklist');
							$("#nuevoTipoChecklist").val("");

							$("#tipoChecklist .seleccionado").text(
									"Tipo de checklist");
							$("#tipoChecklist .seleccionado").attr("id", -1);

						},
						error : function() {
							// alert('Se ha producido un error al eliminar el
							// tipo de checklist');
							alert('No es posible eliminar el tipo, hay Checklist activos relacionados');

						}
				 		,async: false
					});

		}
	}

	// alert("http://"+urlServer +
	// '../checklistServices/eliminaTipoCheck.json?idTipoCheck='+idTipoCheck);

	return false;

}

function eliminarTipoChecklist() {
	eliminarTipoChecklistService();
	return false;
}
function agregarNuevoTipoChecklist() {

	var nuevoTipo = $("#nuevoTipoChecklist").val();
	if (nuevoTipo.trim() == "") {

		alert("No has ingresado el nombre del tipo de checklist");
	} else {
		if (editarTipoCheck) {

			if (confirm("¿Desea modificar este registro ["
					+ $("#tipoChecklist .seleccionado").text() + "]?")) {
				// se ejecuta el servicio
				modificarTipoChecklistService();
			}

			editarTipoCheck = false;

		} else {
			agregarTipoChecklistService();
			editarTipoCheck = false;
		}

		$("#nuevoTipoChecklist").val("");
		ocultarComponente("agregarTipoChecklist");
		ocultarComponente("nuevoTipoChecklist");

		$("#tipoChecklist .seleccionado").attr("id", -1);
		$("#tipoChecklist .seleccionado").text("Tipo de checklist");
	}

	return false;
}

function activarEdicionTipoChecklist() {
	if ($("#tipoChecklist .seleccionado").attr("id") == "-1"
			|| $("#tipoChecklist .seleccionado").attr("id") == "-2") {
		alert("No has seleccionado ninguna opción");
	} else {
		mostrarComponente('agregarTipoChecklist');
		mostrarComponente('nuevoTipoChecklist');
		editarTipoCheck = true;

		$("#nuevoTipoChecklist").val($("#tipoChecklist .seleccionado").text());

	}

	return false;
}

function agregarChecklist() {

	// nombre de checklist
	var nombreChecklist = $("#nombreChecklist").val();
	// tipo de checklist
	var idTipoChecklist = $("#tipoChecklist .seleccionado").attr("id");
	// horario
	var idHorario = $("#horario .seleccionado").attr("id");
	// fechas de aplicacion del checklist
	var fechaInicioTmp = $("#fechaInicio").val().split("/");
	var fechaTerminoTmp = $("#fechaTermino").val().split("/");
	var fechaInicio = fechaInicioTmp[2] + "" + fechaInicioTmp[1] + ""
			+ fechaInicioTmp[0];
	var fechaTermino = "";

	/* Variables Periodicidad */
	var idUsuario = "0";
	var periodicidad = $("#periodicidad .seleccionado").attr("id");
	var dia = $("#fecha .seleccionado").attr("id");

	if ($("#fecha .seleccionado").text() == 'Selecciona el dia'
			|| $("#fecha .seleccionado").text() == 'undefinied')
		dia = "";

	if (fechaTerminoTmp.length > 1) {
		fechaTermino = fechaTerminoTmp[2] + "" + fechaTerminoTmp[1] + ""
				+ fechaTerminoTmp[0];
	}

	// alert("FECHA TERMINO: "+fechaTermino);

	// alert("http://"+urlServer +
	// '../checklistServices/altaTipoCheck.json?descTipoCheck='+tipoChecklist);
	/*
	 * alert("http://"+urlServer +
	 * '../checklistServices/altaCheckService.json?fechaIni='+fechaInicio+'&fechaFin='+fechaTermino+'&idEstado=21&idHorario='+idHorario+'&idTipoCheck='+idTipoChecklist+'&nombreCheck='+nombreChecklist+'&setVigente=1&commit=1'
	 *  );
	 */

	$.ajax({
				type : "GET",
				url : encodeURI("http://"
						+ urlServer
						+ '../checklistServices/altaCheckServiceTemp.json?idCheck=0&fechaIni='
						+ fechaInicio + '&fechaFin=' + fechaTermino
						+ '&idEstado=28&idHorario=' + idHorario
						+ '&idTipoCheck=' + idTipoChecklist + '&nombreCheck='
						+ nombreChecklist
						+ '&setVigente=1&commit=1&idUsuario=0&periodicidad='
						+ periodicidad + '&dia=' + dia+"&numVersion=0&tipoCambio=I"),

				contentType : "plain/text; charset=utf-8",
				dataType : "json",
				paramName : "tagName",
				delimiter : ",",
				success : function(json) {

					if (json != null) {
						// alert("Se agrego correctamente la información del
						// checklist: "+nombreChecklist);
						// se actualiza el valor del id del checklist
						$("#idChecklist").val(json.idChecklist);
						// document.getElementById("idChecklist").value=json.idChecklist;
						// document.getElementById("formulario").submit();

					}

				},
				error : function() {
					alert('Algo paso al guardar la información del checklist');

				},
				async : false
			});

}

function modificarChecklist() {

	var idChecklist = $("#idChecklist").val();
	// nombre de checklist
	var nombreChecklist = $("#nombreChecklist").val();
	// tipo de checklist
	var idTipoChecklist = $("#tipoChecklist .seleccionado").attr("id");
	// horario
	var idHorario = $("#horario .seleccionado").attr("id");
	// fechas de aplicacion del checklist
	var fechaInicioTmp = $("#fechaInicio").val().split("/");
	var fechaTerminoTmp = $("#fechaTermino").val().split("/");
	var fechaInicio = fechaInicioTmp[2] + "" + fechaInicioTmp[1] + ""
			+ fechaInicioTmp[0];
	var fechaTermino = "";

	/* Variables Periodicidad */
	var idUsuario = "0";
	var periodicidad = $("#periodicidad .seleccionado").attr("id");
	var dia = $("#fecha .seleccionado").attr("id");

	if ($("#fecha .seleccionado").attr("id") == "-1"
		|| $("#fecha .seleccionado").text() == 'Selecciona el dia')
			//|| $("#fecha .seleccionado").text() == 'undefinied'*/)
		dia = "";

	if (fechaTerminoTmp.length > 1) {
		fechaTermino = fechaTerminoTmp[2] + "" + fechaTerminoTmp[1] + ""
				+ fechaTerminoTmp[0];
	}

	// alert("FECHA TERMINO: "+fechaTermino);

	// http://localhost:8080../checklistServices/updateCheck.json?idChecklist=<?>&fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>

	$
			.ajax({
				type : "GET",
				url : encodeURI("http://"
						+ urlServer
						+ '../checklistServices/altaCheckServiceTemp.json?idCheck='+idChecklist+'&fechaIni='
						+ fechaInicio + '&fechaFin=' + fechaTermino
						+ '&idEstado=28&idHorario=' + idHorario
						+ '&idTipoCheck=' + idTipoChecklist + '&nombreCheck='
						+ nombreChecklist
						+ '&setVigente=1&commit=1&idUsuario=0&periodicidad='
						+ periodicidad + '&dia=' + dia+"&numVersion=0&tipoCambio=U"),


				contentType : "application/json; charset=utf-8",
				dataType : "json",
				paramName : "tagName",
				delimiter : ",",
				success : function(json) {
					if (json != null) {
						//alert("Se actualizó la información del checklist: "+nombreChecklist);

					} else {
						alert('Algo paso al guardar la información del checklist');
					}

				},
				error : function() {
					alert('Algo paso al guardar la información del checklist');

				},
				async : false
			});

}

$(document)
		.ready(
				function() {
					$("#continuar")
							.click(
									function() {
										var pendientes = "";
										var existePendiente = false;
										if ($("#tipoChecklist .seleccionado")
												.attr("id") == "-1"
												|| $(
														"#tipoChecklist .seleccionado")
														.attr("id") == "-2") {
											pendientes += "\n*Tipo de checklist";
											existePendiente = true;
										}
										if ($("#horario .seleccionado").attr(
												"id") == "-1") {
											pendientes += "\n*Horario";
											existePendiente = true;
										}
										if ($("#fechaInicio").val().trim() == "") {
											pendientes += "\n*Fecha de incio";
											existePendiente = true;
										}

										if ($("#periodicidad .seleccionado").attr("id") == '-1' 
													|| $("#periodicidad .seleccionado").attr("id") == "Selecciona periodicidad") {
											pendientes += "\n*Periodicidad";
											existePendiente = true;
										}
										if (periodicidad == 'Semanal') {
											if ($("#fecha .seleccionado").attr("id") == "-1"
													|| $("#fecha .seleccionado").attr("id") == "Selecciona el dia"
													/*|| $("#fecha .seleccionado")
															.text() == 'undefinied'*/) {
												pendientes += "\n*Día";
												existePendiente = true;
											}
										}
										if (existePendiente) {
											alert("No has llenado los siguientes campos: "
													+ pendientes);
										} else {
											// nuevo checklist
											if ($("#idChecklist").val() == 0) {
												// si no existe el checklist
												agregarChecklist();
												return true;

											} else {
												modificarChecklist();
												return true;

											}
											// $.post("");

										}

										return false;

									})
				});
