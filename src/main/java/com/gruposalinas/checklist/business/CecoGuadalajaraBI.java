package com.gruposalinas.checklist.business;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.CecoDAO;
import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.CecoIndicadoresDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class CecoGuadalajaraBI {
	
	private static Logger logger = LogManager.getLogger(CecoGuadalajaraBI.class);
	
	@Autowired
	CecoDAO cecoDAO;
	@Autowired
	CecoBI cecobi;
	
	
	public boolean actualizaCecoGuadalajara(){
		
		boolean resultado = false;
		
		CecoDTO ceco = new CecoDTO();
		ceco.setActivo(1);
		ceco.setCalle("CENTRO");
		ceco.setCiudad("GUADALAJARA");
		ceco.setCp("44100");
		ceco.setDescCeco("ZONA GUADALAJARA");
		ceco.setFaxContacto("55");
		ceco.setIdCanal(21);
		ceco.setIdCeco("237734");
		ceco.setIdCecoSuperior(6781);
		ceco.setIdEstado(14);
		ceco.setIdNegocio(41);
		ceco.setIdNivel(6);
		ceco.setIdPais(21);
		ceco.setNombreContacto("JUAN GERMAN IBARRA PARRA");
		ceco.setPuestoContacto("142439");
		ceco.setTelefonoContacto("55");
		ceco.setFechaModifico("");
		ceco.setUsuarioModifico("");
		
		try {
			boolean res = cecobi.actualizaCeco(ceco);
			
			boolean res2 = cecobi.cargaGeografia();
			
			if(res==true && res2==true){
				resultado=true;
			}

		} catch (Exception e) {
			logger.info("No fue posible actualizar el CECO ");
			
		}
		
		return resultado;
	}
	

}
