package com.gruposalinas.checklist.domain;

public class ChecklistCompletoDTO {

	private int idTipoPregunta;
	private String cvePregunta;	
	private String descripcionTipo;
	private int idPregunta;
	private int ordenPregunta;
	private int idArbolDesicion;
	private String pregunta;
	private String respuesta;
	private int idModulo;
	private String nombreModulo;
	private int idModuloPadre;
	private String nombrePadre;
	private int estatusEvidencia;
	private int ordenCheckRespuesta;
	private int reqAccion;
	private int obliga;
	private int reqObs;
	private String desEvidencia;


	public int getOrdenPregunta() {
		return ordenPregunta;
	}
	public void setOrdenPregunta(int ordenPregunta) {
		this.ordenPregunta = ordenPregunta;
	}
	public int getIdArbolDesicion() {
		return idArbolDesicion;
	}
	public void setIdArbolDesicion(int idArbolDesicion) {
		this.idArbolDesicion = idArbolDesicion;
	}
	
	public String getDescripcionTipo() {
		return descripcionTipo;
	}
	public void setDescripcionTipo(String descripcionTipo) {
		this.descripcionTipo = descripcionTipo;
	}
	
	public int getIdTipoPregunta() {
		return idTipoPregunta;
	}
	public void setIdTipoPregunta(int idTipoPregunta) {
		this.idTipoPregunta = idTipoPregunta;
	}
	public String getCvePregunta() {
		return cvePregunta;
	}
	public void setCvePregunta(String cvePregunta) {
		this.cvePregunta = cvePregunta;
	}
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public int getIdModulo() {
		return idModulo;
	}
	public void setIdModulo(int idModulo) {
		this.idModulo = idModulo;
	}
	public String getNombreModulo() {
		return nombreModulo;
	}
	public void setNombreModulo(String nombreModulo) {
		this.nombreModulo = nombreModulo;
	}
	public int getIdModuloPadre() {
		return idModuloPadre;
	}
	public void setIdModuloPadre(int idModuloPadre) {
		this.idModuloPadre = idModuloPadre;
	}
	public String getNombrePadre() {
		return nombrePadre;
	}
	public void setNombrePadre(String nombrePadre) {
		this.nombrePadre = nombrePadre;
	}
	public String getPregunta() {
		return pregunta;
	}
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public int getEstatusEvidencia() {
		return estatusEvidencia;
	}
	public void setEstatusEvidencia(int estatusEvidencia) {
		this.estatusEvidencia = estatusEvidencia;
	}
	public int getOrdenCheckRespuesta() {
		return ordenCheckRespuesta;
	}
	public void setOrdenCheckRespuesta(int ordenCheckRespuesta) {
		this.ordenCheckRespuesta = ordenCheckRespuesta;
	}
	public int getReqAccion() {
		return reqAccion;
	}
	public void setReqAccion(int reqAccion) {
		this.reqAccion = reqAccion;
	}

	public int getObliga() {
		return obliga;
	}
	public void setObliga(int obliga) {
		this.obliga = obliga;
	}

	public int getReqObs() {
		return reqObs;
	}
	public void setReqObs(int reqObs) {
		this.reqObs = reqObs;
	}
	public String getDesEvidencia() {
		return desEvidencia;
	}
	public void setDesEvidencia(String desEvidencia) {
		this.desEvidencia = desEvidencia;
	}
	
}
