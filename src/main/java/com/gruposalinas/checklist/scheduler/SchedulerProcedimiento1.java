package com.gruposalinas.checklist.scheduler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gruposalinas.checklist.business.SucursalBI;
import com.gruposalinas.checklist.business.TokenBI;
import com.gruposalinas.checklist.business.Usuario_ABI;

@Component
public class SchedulerProcedimiento1 {
	
	@SuppressWarnings("unused")
	private static Logger logger = LogManager.getLogger(SchedulerProcedimiento1.class);

	@Autowired
	TokenBI tokenBI;
	@Autowired
	Usuario_ABI usuarioABI;
	@Autowired
	SucursalBI sucursalBI;
	
	public void run() {
		/*try {
			//System.out.println("FECHA: "+ new Date().toString());
			//System.out.println("Funcion token: "+ tokenBI.eliminaTokenTodos());		
			//System.out.println("Carga usuarios: "+ usuarioABI.cargaUsuarios());
			//System.out.println("Carga sucursales: "+ sucursalBI.cargaSucursales());
			
		} catch (Exception e) {
			
			logger.info("Error al ejecutar el Scheduler 1");
		}*/
	}
}