package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.HallazgosExpDTO;

public class HallazgosTablaRowMapper implements RowMapper<HallazgosExpDTO> {

	public HallazgosExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		HallazgosExpDTO HallazgosExpDTO = new HallazgosExpDTO();
		
		HallazgosExpDTO.setIdHallazgo(rs.getInt("FIID_HALLAZGO"));
	    HallazgosExpDTO.setIdFolio(rs.getInt("FIID_FOLIO"));
	    HallazgosExpDTO.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    HallazgosExpDTO.setStatus(rs.getInt("FCSTATUS"));
	    HallazgosExpDTO.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    HallazgosExpDTO.setPreg(rs.getString("FCPREGUNTA"));
	    HallazgosExpDTO.setRespuesta(rs.getString("FCRESPUESTA"));
	    HallazgosExpDTO.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
	    HallazgosExpDTO.setResponsable(rs.getString("FCRESPONSABLE"));
	    HallazgosExpDTO.setObsNueva(rs.getString("FCDISPOSITIVO"));
	    HallazgosExpDTO.setObs(rs.getString("FCOBSERVACION"));
	    HallazgosExpDTO.setIdcheck(rs.getInt("FIID_CHECKLIST"));
	    HallazgosExpDTO.setRuta(rs.getString("FCRUTA"));
	   // HallazgosExpDTO.setArea(rs.getString("FCAREA"));

	    HallazgosExpDTO.setBitGral(rs.getInt("FIID_BITGRAL"));
	    HallazgosExpDTO.setFechaIni(rs.getString("FDINICIO"));
	    HallazgosExpDTO.setFechaFin(rs.getString("FDFIN"));
	    HallazgosExpDTO.setFechaAutorizo(rs.getString("FDAUTORIZO"));
	    HallazgosExpDTO.setCeco(rs.getString("FCID_CECO"));
	    HallazgosExpDTO.setRecorrido(rs.getInt("FCRECORRIDO"));
	    

		return HallazgosExpDTO;
	}

}


