<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>Formulario</title>
  <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>

<body ng-app="app" ng-controller="validarForm">

  <div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="img/btn_hamburguer.svg" alt="Menú principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="index.html"><img src="img/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        <div class="name"><strong>Coyolxauhqui</strong> Ramírez Romero<br> Administrador</div>
        <div class="pic" style="background-image: url('img/user.jpg');"></div>
        <div class="user-menu">
          <a href="#"><img src="img/btn_desp_menu.svg" class="imgShowMenuUser"></a>
        </div>
        <div class="fecha">
			<b>14/Feb/2019</b><br>
			<span>15:30Hrs.</span>
		</div>
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.html" class="selMenu">
          <div>Salir</div>
          <div><img src="img/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="index.html">Página Principal</a> /
			<a href="formulario.html">Formulario</a> 
		</div>
    	Formulario
    </div>

	<!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="img/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Menú Principal<br>
          </div>
        </div>
        <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la página">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div>
        <div id="menu">
          <ul class="l-navegacion nivel1">
				
				<li><a href="login.html"><div>Login</div></a></li>
				<li>
					<a><div>Estructuras</div><div class="flecha"></div></a>
					<ul class="nivel2">
						<li><a href="acordeon.html"><div>Acordeón</div></a></li>
						<li><a href="bread-crumb.html"><div>Bread Crumb</div></a></li>
						<li><a href="columnas.html"><div>Columnas</div></a></li>
						<li><a href="ligas.html"><div>Ligas</div></a></li>
						<li><a href="menu.html"><div>Menú</div></a></li>
						<li><a href="scroll.html"><div>Scroll</div></a></li>
						<li><a href="slider.html"><div>Slider</div></a></li>
						<li><a href="tablas.html"><div>Tablas</div></a></li>
						<li><a href="tabs.html"><div>Tabs</div></a></li>
						<li><a href="texto.html"><div>Texto</div></a></li>
						<li><a href="tree.html"><div>Tree</div></a></li>
						<li><a href="visualizador.html"><div>Visualizador</div></a></li>
					</ul>
				</li>
				<li>
					<a><div>Formularios</div><div class="flecha"></div></a>
					<ul class="nivel2">
						<li><a href="formulario.html"><div>_Demo Formulario</div></a></li>
						<li>
							<a><div>Botones</div><div class="flecha"></div></a>
							<ul class="nivel3">
								<li><a href="botones.html"><div>_Demo tipos de Botones</div></a></li>
								<li><a href="agregar.html"><div>Agregar/Eliminar</div></a></li>
							</ul>
						</li>
						<li><a href="checkbox.html"><div>Checkbox</div></a></li>
						<li><a href="chip.html"><div>Chip - Tags-it</div></a></li>
						<li><a href="radio.html"><div>Radios</div></a></li>
						<li><a href="switch.html"><div>Switch</div></a></li>
					</ul>
				</li>

				<li>
					<a><div>Eventos/Estados</div><div class="flecha"></div></a>
					<ul class="nivel2"> 
						<li><a href="desactivado.html"><div>Elementos desactivados</div></a></li>
						<li><a href="loading.html"><div>Loading</div></a></li>
						<li><a href="medidores-avance.html"><div>Medidores de avance</div></a></li>
						<li><a href="modales.html"><div>Modales</div></a></li>
						<li><a href="tooltip.html"><div>Tooltips</div></a></li>
						<li><a href="tracking.html"><div>Tracking</div></a></li>
					</ul>
				</li>
				<li>
					<a><div>Componentes</div><div class="flecha"></div></a>
					<ul class="nivel2"> 
						<li><a href="chat.html"><div>Chat</div></a></li>
						<li><a href="drag-and-drop.html"><div>Drag and drop</div></a></li>
						<li><a href="galeria.html"><div>Galería</div></a></li>
						<li><a href="graficas.html"><div>Gráficas</div></a></li>
						<li><a href="paginador.html"><div>Paginador</div></a></li>
					</ul>
				</li> 
			</ul>
        </div>
      </div>
    </div>

    <!-- Contenido -->
    <div class="contSecc">
		<div class="titSec">Formulario</div>
		<div class="gris">
			<div class="titCol">Por favor complete los siguientes campos</div>
			<div class="divCol3">
				<div class="col3">
					Select:<br>
					<select>
						  <option value="">Opción 1</option>
						  <option value="">Opción 2</option>
						  <option value="">Opción 3</option>
					</select>  
				</div>
				<div class="col3">
					<span class="obliga">*</span> Input:<br>
					<input type="text" placeholder="Nombres">
				</div>
				<div class="col3">
					Correo:<br>
					<input type="text" placeholder="crramirez@elektra.com">
				</div>
				<div class="col3">
					Teléfono:<br>
					<div class="divTel">
						<input type="text" placeholder="55">
						<input type="text" placeholder="1234 5678">
					</div>
				</div>
				<div class="col3">
					Password:<br>
					<input type="password" placeholder="****">
				</div>
				<div class="col3">
					<span class="obliga">*</span>  Radio<br>
					<div class="divCol2Input">
						<div><input type="radio" name="grupo1" id="radio1"/><label for="radio1">Texto prueba 1</label></div>
						<div><input type="radio" name="grupo1" id="radio2"/><label for="radio2">Texto prueba 1</label></div>
						<div><input type="radio" name="grupo1" id="radio3"/><label for="radio3">Texto prueba 1</label></div>
						<div><input type="radio" name="grupo1" id="radio4"/><label for="radio4">Texto prueba 1</label></div>
						<div><input type="radio" name="grupo1" id="radio5"/><label for="radio5">Texto prueba 1</label></div>
						<div><input type="radio" name="grupo1" id="radio6"/><label for="radio6">Texto prueba 1</label></div>
						<div><input type="radio" name="grupo1" id="radio7"/><label for="radio7">Texto prueba 1</label></div>
						<div><input type="radio" name="grupo1" id="radio8"/><label for="radio8">Texto prueba 1</label></div>
					</div>
				</div>
				<div class="col3">
					<span class="obliga">*</span> Checkbox:<br>
					<div class="divCol2Input">
						<div><input type="checkbox" name="grupo1" id="check1"/><label for="check1">Texto prueba 1</label></div>
						<div><input type="checkbox" name="grupo1" id="check2"/><label for="check2">Texto prueba 1</label></div>
						<div><input type="checkbox" name="grupo1" id="check3"/><label for="check3">Texto prueba 1</label></div>
						<div><input type="checkbox" name="grupo1" id="check4"/><label for="check4">Texto prueba 1</label></div>
					</div>
				</div>
				<div class="col3">
					Fecha:<br>
					<input type="text" placeholder="DD/MM/AAAA" class="datapicker1 date" id="datepicker" autocomplete="off"> 
				</div>
				<div class="col3">
					Buscar:<br>
					<form id="" name="" action="" method="get" onsubmit="return valida(this)">
						<div class="pRelative">
					  		<input type="text" id="busca">
						  	<input type="submit" class="buscar" value="">
						</div>
					</form>
				</div>
				<div class="col3">
					Textarea:<br>
					<textarea></textarea>
				</div>
				
				<div class="col3">
					Upload:<br>
					<input type="file" id="carga01" class="inputfile carga "/>
					<label for="carga01" class="btnAgregaFoto "><span>Adjuntar documento </span></label>
				</div>
				<div class="col3">
					Switch:<br>
					<div class="divSwitch">
						<input type="checkbox" /> 
						<div class="switch"></div> Texto ejemplo
					</div>
				</div>	
				<div class="col3">
					Editar:<br>
					<form id="" name="" action="" method="get">
						<div class="pRelative">
					  		<input type="text" id="busca">
						  	<input type="submit" class="editar" value="">
						</div>
					</form>
				</div>
				<div class="col3">
					Hora:<br>
					<input type="time" name="usr_time">
				</div>
				<div class="col3">
					Número:<br>
					<input type="number" placeholder="1234">
				</div>									
			</div>
			<div class="btnCenter">
				<a href="#" class="btn btnLimpiar"><div></div>Limpiar</a>
				<a href="#" class="btn btnEditar"><div></div>Editar</a>
				<a href="#" class="btn btnGuardar"><div></div>Guardar</a>
			</div>
		</div>
		
		
	</div>	<!-- Fin conSecc -->
	
	<!-- Footer -->
	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualización Febrero 2019</div>
	</div>
</div><!-- Fin page -->

</body>
</html>

<script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="js/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="js/content_height.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script src="js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="js/angular.min.js"></script>
<script src="js/custom-file-input.js"></script><!--Unpload -->
<script src="js/jquery.treeview.js"></script><!--Unpload -->
<script src="js/owl.carousel.js"></script><!-- Slider -->
<script type="text/javascript" src="js/funciones.js"></script>
