package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.HallazgosExpDTO;

public class HallazgosHistoricoDetRowMapper implements RowMapper<HallazgosExpDTO> {

	public HallazgosExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		HallazgosExpDTO HallazgosExpDTO = new HallazgosExpDTO();
		
		HallazgosExpDTO.setAux2(rs.getInt("REGISTRO"));
		HallazgosExpDTO.setIdHallazgo(rs.getInt("FIID_HALLAZGO"));
	    HallazgosExpDTO.setIdFolio(rs.getInt("FIID_FOLIO"));
	    HallazgosExpDTO.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    HallazgosExpDTO.setStatus(rs.getInt("FCSTATUS"));
	    HallazgosExpDTO.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    HallazgosExpDTO.setPreg(rs.getString("FCPREGUNTA"));
	    HallazgosExpDTO.setRespuesta(rs.getString("FCRESPUESTA"));
	    HallazgosExpDTO.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
	    HallazgosExpDTO.setResponsable(rs.getString("FCRESPONSABLE"));
	    HallazgosExpDTO.setDisposi(rs.getString("OBSNUEVA"));
	    HallazgosExpDTO.setArea(rs.getString("FCAREA"));
	    HallazgosExpDTO.setObs(rs.getString("FCOBSERVACION"));
	    HallazgosExpDTO.setBitGral(rs.getInt("FIID_BITGRAL"));
	    HallazgosExpDTO.setArbol(rs.getString("FIID_ARBOL"));
	    HallazgosExpDTO.setIdcheck(rs.getInt("FIID_CHECKLIST"));
	    //RUTAEVI
	    HallazgosExpDTO.setRuta(rs.getString("FCRUTA"));
	    HallazgosExpDTO.setFechaIni(rs.getString("FDINICIO"));
	    HallazgosExpDTO.setFechaFin(rs.getString("FDFIN"));
	    HallazgosExpDTO.setSla(rs.getString("FCSLA"));
	    HallazgosExpDTO.setUsuModif(rs.getString("FCUSUMODIF"));
	    HallazgosExpDTO.setFechaAutorizo(rs.getString("FDAUTORIZO"));
	    HallazgosExpDTO.setMotivrechazo(rs.getString("MOTIVORECHAZO"));
	    HallazgosExpDTO.setPeriodo(rs.getString("FCPERIODO"));
	    //HallazgosExpDTO.setCeco(rs.getString("FCID_CECO"));
	   // HallazgosExpDTO.setNombChek(rs.getString("CHECKLIST"));
	   // HallazgosExpDTO.setZonaClasi(rs.getString("FCCLASIFICA"));
	    //HallazgosExpDTO.setCecoNom(rs.getString("FCNOMBRE"));

	  
	  

		return HallazgosExpDTO;
	}

}


