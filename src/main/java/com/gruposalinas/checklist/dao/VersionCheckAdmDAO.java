package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.VersionExpanDTO;


public interface VersionCheckAdmDAO {

	  	public int inserta(VersionExpanDTO bean)throws Exception;
		
		public boolean elimina(String idTab)throws Exception;
		
		public List<VersionExpanDTO> obtieneDatos(String idVers) throws Exception; 
		
		public boolean actualiza(VersionExpanDTO bean)throws Exception;
		
		public boolean actualiza2(VersionExpanDTO bean)throws Exception;
		
		// RELACION NEGOCIO VERSION 
		public int insertaNegoVer(VersionExpanDTO bean)throws Exception;
		
		public boolean eliminaNegoVer(String idTab)throws Exception;
		
		public List<VersionExpanDTO> obtieneDatosNegoVer(String idVers) throws Exception; 
		
		public boolean actualizaNegoVer(VersionExpanDTO bean)throws Exception;
		
		public boolean cargaVesionesTab(String protocolo)throws Exception;
		
		public boolean cargaVesionesTabProd(String protocolo)throws Exception;
		
	}