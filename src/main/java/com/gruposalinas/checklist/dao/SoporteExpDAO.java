package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.SoporteExpDTO;


public interface SoporteExpDAO {

		
		public List<SoporteExpDTO> obtieneDatos(String ceco ,String status, String tipoSuc) throws Exception; 
		
		public boolean actualiza(SoporteExpDTO bean)throws Exception;
		
		public boolean actualizaTipoSuc(SoporteExpDTO bean)throws Exception;
		
		public boolean actualizaAbol(SoporteExpDTO bean)throws Exception;
		
		public boolean actualizaPreg(SoporteExpDTO bean)throws Exception;
		
		public boolean actualizaFechaSoftNvo(SoporteExpDTO bean)throws Exception;
	}