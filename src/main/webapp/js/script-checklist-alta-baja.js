	
		$( function() {
			$("#fechaIni1").datepicker({
				maxDate: '0d',
				dateFormat: 'yymmdd',
				showOtherMonths: true,
				selectOtherMonths: true,
				changeMonth: true,
				changeYear: true,
		        beforeShow: function (input, inst) {
		            var rect = input.getBoundingClientRect();
		            setTimeout(function () {
		    	        inst.dpDiv.css({ top: rect.top + 35, left: rect.left + 0 });
		            }, 0);
		        }
			});

			$("#fechaResp1").datepicker({
				maxDate: '0d',
				dateFormat: 'yymmdd',
				showOtherMonths: true,
				selectOtherMonths: true,
				changeMonth: true,
				changeYear: true,
		        beforeShow: function (input, inst) {
		            var rect = input.getBoundingClientRect();
		            setTimeout(function () {
		    	        inst.dpDiv.css({ top: rect.top + 35, left: rect.left + 0 });
		            }, 0);
		        }
			});
		});

		function validaAlta() {
			var activo = document.getElementById("activo1").value;
			var fechaini = document.getElementById("fechaIni1").value;
			var fechaResp = document.getElementById("fechaResp1").value;
			var idCeco = document.getElementById("idCeco1").value;
			var idChecklist = document.getElementById("idChecklist1").value;
			var idUsuario = document.getElementById("idUsuario1").value;

			if ((activo == '' || activo == null) &&
					(fechaini == '' |fechaini == null) &&
					(fechaResp == '' || fechaResp == null) &&
					(idCeco == '' || idCeco == null) &&
					(idChecklist == '' || idChecklist == null) &&
					(idUsuario == '' || idUsuario == null)) {
				alert("Ningún campo puede ir vacío!");
				return false;
			}
			else {
				return true;
			}

		}

		function validaBaja() {
			var idUsuario = document.getElementById("idCheckUsuario2").value;

			if (idUsuario == '' || idUsuario == null) {
				alert("El campo ID de Usuario no puede ir vacío!");
				return false;
			}
			else {
				return true;
			}
		}
