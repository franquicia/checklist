package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ConsultaDashBoardDTO;


public interface ConsultaDashBoardDAO {

	public  List<ConsultaDashBoardDTO> obtieneDashBoard(ConsultaDashBoardDTO bean ) throws Exception;
	}