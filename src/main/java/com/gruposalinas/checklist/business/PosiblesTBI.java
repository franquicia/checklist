package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.PosiblesTDAO;
import com.gruposalinas.checklist.domain.PosiblesDTO;

public class PosiblesTBI {
	private static Logger logger = LogManager.getLogger(PosiblesTBI.class);
	
	@Autowired
	PosiblesTDAO posiblesDAO;
	List<PosiblesDTO> listaPosible= null;
	
	public List<PosiblesDTO> buscaPosibleTemp() throws Exception {

		try {
		listaPosible = posiblesDAO.buscaPosibleTemp();
		} catch (Exception e) {
			logger.info("No fue posible consultar las Posibles respuestas de la tabla temporal");
		}
		return listaPosible;
	}
	
	public boolean insertaPosibleTemp(PosiblesDTO bean){
		
		boolean idPosible = false;
		
		try {
			idPosible = posiblesDAO.insertaPosibleTemp(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar la Posible respuesta en la tabla temporal");
		}
		
		return idPosible;
		
	}
	
	public boolean actualizaPosibleTemp(PosiblesDTO bean){
		
		boolean respuesta = false ;
		
		try {
			respuesta = posiblesDAO.actualizaPosibleTemp(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar la Posible respuesta en la tabla temporal");
		}
		
		return respuesta;
	}
	
	public boolean eliminaPosibleTemp(int idPosible){
		
		boolean respuesta = false ;
		
		try {
			respuesta = posiblesDAO.eliminaPosibleTemp(idPosible);
		} catch (Exception e) {
			logger.info("No fue posible eliminar la Posible Respuesta de la tabla temporal");
		}
		
		return respuesta;		
	}

}
