package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;

public class SucImagenExpRowMapper implements RowMapper<SucursalChecklistDTO> {

	public SucursalChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		SucursalChecklistDTO sim = new SucursalChecklistDTO();
		
		sim.setIdImag(rs.getInt("FIID_IMAG"));
		sim.setIdSucursal(rs.getInt("FCID_CECO"));
		sim.setNombre(rs.getString("FCNOMBRE"));
		sim.setRuta(rs.getString("FCRUTA"));
		sim.setObserv(rs.getString("FCOBSERVACION"));
		sim.setPeriodo(rs.getString("FCPERIODO"));
		
		return sim;
	}

}


