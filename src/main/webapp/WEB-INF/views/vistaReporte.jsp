<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="height: 100%; min-width: 1200px">
<head>
	<link href="../css/vistaReporte.css"
		rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		var urlServer = '${urlServer}';
	</script>
	<script>
		var idUsuario=${idUsuario};
		var rutaHostImagen='${rutaHostImagen}';
		var nivelAdmin=${nivelAdmin};
		var regionId='${regionId}';
		var zonaId='${zonaId}';
		var territorioId='${territorioId}';
		var paisId=${paisId};
		var negocioId=${negocioId};
		//var paisIdAux=${paisIdAux};
		var regionName='${regionName}';
		var zonaName='${zonaName}';
		var territorioName='${territorioName}';
		var paisName='${paisName}';
		var negocioName='${negocioName}';
		//var paisNameAux='${paisNameAux}';
	</script>
	<script	src="../css/jQuery/jQuery.js"></script>
	<script type="text/javascript" 	src="../js/script-reportesChecklist.js" async="async"></script>
	<% response.addHeader("X-Frame-Options", "SAMEORIGIN"); %>
</head>
<body style="height: 97%;">		
			
	<div id="idGeneral" style="background: white; margin: 24px 24px 24px 24px; min-width: 700px; min-height:950px; height: 95%; border-radius: 20px; border:3px solid #000000;" >
		<form:form name="formulario" method="POST"	action="reporteCumplimiento.htm" commandName="command" style="height:100%;" >
		<div style="height:15px;"></div>	
		<div style="background: white; margin: 0 0 0 0; height:94%;">
			<!-- TITULO GENERAL -->							
			<div onmouseover="muestraHijoTituloGen();" onmouseout="muestraHijoTituliGen2();" class="TituloGeneral" style="position:relative;">
				<img style="position:absolute; margin-left: 20px; margin-top: -2px;" width="50px" height="50px;" src="../images/despliegue01.png"/>						
				<div style="list-style: none; color: white; text-align: center; font-size:30px;	padding: 5px; font-family: Century-Regular; font-weight:bold;    "> 
					¿Cómo están supervisando regiones? 
				</div>	
			</div>	
			<!-- TABLA DE MENUS PRINCIPALES Y CALENDARIOS DE FECHAS -->
			<div style=" margin-top: 0px;">
				<!-- MUESTRA LOS 2 MENUS PRINCIPALES -->
				<div id="idHijoTituloGen" onmouseover="muestraHijoTituloGen();" onmouseout="muestraHijoTituliGen2();" style="width:23%; height:70px; position:relative; display:inline-block; box-shadow: 5px 5px 17px #a5a4a3;">
					<div id="idPadreVisita">Cumplimiento de visita</div>
					<div id="idPadreOperando">¿Cómo estamos operando?</div> 
				</div>
				<div id="idHijoTituloGenVacio" style="width:23%; height:70px; position:relative; display:inline-block;">
					<div id="idPadreVisitaVacio">.</div>
					<div id="idPadreOperandoVacio">.</div> 				
				</div>
				<!-- CALENDARIOS DE FECHAS -->
				<div style="width:73%; height:70px; float: right; ">
					<div id="idFechaRangos" style="width:100%; height:70px;">
						<div style="width:48%;height:67px;  display: inline-block;">
							<div style="color: white; text-align: center; height:69px; font-size:15px;">
								<div style="height:97%;">
									<div style="height:47%;">
									<div style="display: inline-block; width: 49%; height:70%; margin-top: 7px; font:16px 'Century-Regular'; color:black;">Año</div>
									<div style="display: inline-block; width: 49%; height:70%; margin-top: 7px; font:16px 'Century-Regular'; color:black;">Periodo</div>
									</div>
									<div style="height:47%;">
										<div style="width: 40%; margin-left:1%; display: inline-block;">
											<li class="li-stilovR" style="width: 100%;">
												<div class="cajaVisvR" id="periodoAVis">
													<input type="hidden" name="periodoA" value="Selecciona una Opcion" id="valorPeriodoA">
													<span class="seleccionadoVisvR " id="Selecciona una Opcion">Elije una opcion</span>
													<ul class="listaselectVisvR" id="periodoAVis2">	
														<li value="2016"><a href="">2016</a></li>
														<li value="2017"><a href="">2017</a></li>
													</ul>
													<span class="trianguloinfVisvR"></span>
												</div>
											</li>
										</div>
										<div  style="width: 40%; margin-left:10%;  margin-right:1%; display: inline-block;">
											<li class="li-stilovR" style="width: 100%;">
												<div class="cajaVisvR" id="periodoVis">
													<input type="hidden" name="periodo" value="Selecciona una Opcion" id="valorPeriodo">
													<span class="seleccionadoVisvR " id="Selecciona una Opcion">Elije una opcion</span>
													<ul class="listaselectVisvR" id="periodoVis2"></ul>
													<span class="trianguloinfVisvR"></span>
												</div>
											</li>
										</div>
									</div>								
								</div>									
							</div>
						</div>
						<div style="display: inline-block; width: 50%;float: right; ">
							<div style="width:49%;height:67px;  display: inline-block;">					
								<div style="color: white; text-align: center;  font-size:15px;"> 							
									<div class="li-stiloVr" style="font:16px 'Century-Regular'; color:black;"> del
									<!-- <img id="lupa" width="30px" height="30px;" style="vertical-align:middle; margin-left: 7px;"
											src="../images/icono_calendario.png"/> -->
										<input onchange="cambiaFecha(0);" class="textBoxInputB" type="text" id="fechaInicioVr" name="fecha" placeholder="Fecha Inicio" size="22" />
									</div>
								</div>
							</div>
							<div style="width:50%;height:67px;  display: inline-block;">
								<div style="color: white; text-align: center; font-size:15px;"> 
									<div class="li-stiloVr" style="font:16px 'Century-Regular'; color:black;">al
									<!-- <img id="lupa" width="30px" height="30px"  style="vertical-align:middle; margin-left: 7px;"
												src="../images/icono_calendario.png"/> -->
										<input onchange="cambiaFecha(1);" class="textBoxInputB" type="text" id="fechaFinVr" name="fecha" placeholder="Fecha Fin" size="22" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>		
			</div>	
			<input type="hidden" name="porcentajeL" value="0" id="idPorcentajeL">
			<!-- MENSAJE DE REPORTE DE LOS ULTIMOS 15 DIAS -->
			<div style=" height:45px; margin-left:40%; margin-right: 2%; margin-top: 10px; position: relative;">
				<a class="idPorc0">0%</a>
				<a id="idPorc1"></a>
				<a class="idPorc100">100%</a>
				<div id="barraPorcent" style="background:white; border:1px solid #285a43; height:35px; width: 98%;"></div>
				<a class="idDia0">1</a>
				<a id="idDia1-2"></a>
				<a id="idDia1">31</a>
			</div>	
			<!-- TABLA QUE MUESTRA LA VISTA PREVIA Y PAISES -->
			<div style=" height:10%;  margin-top: 5px; ">
				<!-- TABLA QUE MUESTRA LA VISTA PREVIA DE VISTA -->
				<div id="vistaPrevia" style="height:100%; width:31%;position:relative; display:inline-block ;">	
					<div style=" height: 100%; width:98%; margin-left:1%; margin-right:1%;">
						<table style=" height: 100%; width:100%; border-spacing:1px;">
							<tr style="background:#6c6f6d; color:white; height: 37%; font-size: 10px; font-family:Roboto-Thin">
								<th style="width: 50%; font-family:Century-Regular; font-weight:100;">
									<div id="idColumna0">SUCURSAL</div>
								</th>
								<th style="width: 10%; font-family:Century-Regular; font-weight:100;">	
									<div>Plan</div>
								</th>
								<th style="width: 13%; font-family:Century-Regular; font-weight:100;">
									<div>Visitas a sucursales</div>						
								</th>
								<th style="width: 13%; font-family:Century-Regular; font-weight:100;">
									<div>Avance</div>
								</th>
								<th style="width: 14%; font-family:Century-Regular; font-style:italic; font-weight:100;">
									<div>Check List</div>
								</th>
							</tr>
							<tr style="background:white; height: 3%;"></tr>
							<tr style="background:white; height: 59%; text-align: center; font-size: 11px;">			
								<td style="font-family:Calibri; font-weight:100; border: 1px solid #40795c; width: 50%;" id="idNombreCeco">Vacio</td>
								<td style="font-family:Calibri; font-weight:100; border: 1px solid #40795c; width: 10%;" id="idPlanCeco">1</td>
								<td style="font-family:Calibri; font-weight:100; border: 1px solid #40795c; width: 13%;" id="idRealCeco">1</td>
								<td style="font-family:Calibri; font-weight:100; border: 1px solid #40795c; width: 13%;" id="idAvanceCeco"></td>	
								<td style="font-family:Calibri; font-weight:100; border: 1px solid #40795c; width: 14%;" id="idDistintoCeco">1</td>					
							</tr>		
						</table>					
					</div>					
				</div>
				<!-- TABLA QUE MUESTRA LA VISTA PREVIA DE OPERANDO-->
				<div id="vistaPreviaReactivo" style="height:100%; width:31%;position:relative; display:inline-block ;">	
					<div style=" height: 90%; margin-left:1%; margin-right:1%; margin-top:-2%;">
						<div style="height: 60%; width:100%;  text-align: center;">
							<div style="height: 96%; width:90%; margin-left:5%; border-radius: 10px; border-color:blue; border: 1px solid;">
								<div id="conteoSucursales" style="margin-top:2%; width:100%; height:100%; font-size: 14px; font-family:Century-Bold; color: black;">									
									Sucursales
								</div>	
							</div>							
						</div>
						<div id="idReactivoVista" style="height: 40%; width:100%; ">
							<div style="margin-top:8.5%; margin-left:63%;  background:red; float:left;">
								<div class="logRegresa-btn3"  style="text-align:center; padding-top: 0.7%;">Reactivo:</div>
							</div>
						</div>
					</div>	
				</div>
				<!-- TABLA QUE MUESTRA EL FILTRADO DE PAISES -->
				<div style="height:100%; width:68%;  float:right; position:relative; display: inline-block; margin-right: 1%">
					<div style=" width:100%; height:30%;  float:right; margin-top: 1%; color:black;">
						<div class="li-stilovR quinto-nivelvR margen_combos" >
							<a class="tamanoLetraTituloCombos">Región</a> 
						</div>
						<div class="li-stilovR cuarto-nivelvR margen_combos" >
							<a class="tamanoLetraTituloCombos">Zona</a> 
						</div>
						<div class="li-stilovR tercer-nivelvR margen_combos" >
							<a class="tamanoLetraTituloCombos">Territorio</a> 
							<div class="logBuscar-btn" id="logBuscar-btn" onclick="logBuscarbtn();">Buscar / Actualizar</div>	
						</div>
						<div class="li-stilovR segundo-nivelvR margen_combos">
							<a class="tamanoLetraTituloCombos">Pais</a>
						</div>
						<div class="li-stilovR primer-nivelvR margen_combos" >
							<a class="tamanoLetraTituloCombos">Canal</a>
						</div>
						<div class="li-stilovR primer-nivelvR margen_combos">
							<a class="tamanoLetraTituloCombos">Checklist</a>
						</div>
					</div>	
					<div style="width:100%; min-width:640px; float:right;">
						<ul class="margenUlvR"  style="margin-left:0px; margin-top:5px; width:100%;">
							<div class="quinto-nivelvR margen_combos" >
								<li class="li-stilovR" style="width: 100%;">
									<div class="cajaVisvR" id="regionVis">
										<input type="hidden" name="region" value="Selecciona una Opcion" id="valorRegion">
										<span class="seleccionadoVisvR " id="Selecciona una Opcion">Elije una opcion</span>
											<ul class="listaselectVisvR" id="regionVis2"></ul>
										<span class="trianguloinfVisvR"></span>
									</div>
								</li> 
							</div>
							<div class="cuarto-nivelvR margen_combos" >
								<li class="li-stilovR" style="width: 100%;"> 
									<div class="cajaVisvR" id="zonaVis">
										<input type="hidden" name="zona" value="Selecciona una Opcion" id="valorZona">
										<span class="seleccionadoVisvR " id="Selecciona una Opcion">Elije una opcion</span>
											<ul class="listaselectVisvR" id="zonaVis2"></ul>
										<span class="trianguloinfVisvR"></span>
									</div>
								</li>
							</div>
							<div class="tercer-nivelvR margen_combos" >
								<li class="li-stilovR" style="width: 100%;">
									<div class="cajaVisvR" id="territorioVis">
										<input type="hidden" name="territorio" value="Selecciona una Opcion" id="valorTerritorio">
										<span class="seleccionadoVisvR " id="Selecciona una Opcion">Elije una opcion</span>
											<ul class="listaselectVisvR" id="territorioVis2"></ul>
										<span class="trianguloinfVisvR"></span>
									</div>
								</li>
							</div>
							<div class=" segundo-nivelvR margen_combos" >
								<li class="li-stilovR" style="width: 100%;">
									<div class="cajaVisvR" id="paisVis">
										<input type="hidden" name="pais" value="Selecciona una Opcion" id="valorPais">
										<span class="seleccionadoVisvR" id="Selecciona una Opcion">Elije una opcion</span>
											<ul class="listaselectVisvR" id="paisVis2"></ul>
										<span class="trianguloinfVisvR"></span>
									</div>
								</li>
							</div>
							<div class=" primer-nivelvR margen_combos" >
								<li class="li-stilovR" style="width: 100%;">
									<div class="cajaVisvR" id="canalVis">
										<input type="hidden" name="canal" value="Selecciona una Opcion" id="valorCanal">
										<span class="seleccionadoVisvR" id="Selecciona una Opcion">Elije una opcion</span>
											<ul class="listaselectVisvR" id="canalVis2"></ul>
										<span class="trianguloinfVisvR"></span>
									</div>
								</li>
							</div>
							<div class=" primer-nivelvR margen_combos">
								<li class="li-stilovR" style="width: 100%;">
									<div class="cajaVisvR" id="checklistVis">
										<input type="hidden" name="checklist" value="Selecciona una Opcion" id="valorChecklist">
										<span class="seleccionadoVisvR" id="Selecciona una Opcion">Elije una opcion</span>
											<ul class="listaselectVisvR" id="checklistVis2"></ul>
										<span class="trianguloinfVisvR"></span>
									</div>
								</li>
							</div>			
						</ul>
					</div>
				</div>
			</div>
			<div id="idHijoVisitaTabla" style="margin-top:45px; width:90%;  margin-left: 5%; margin-right: 5%; overflow-y: scroll;">
				<table style="border-spacing:1px; width:100%; height:40px; position: relative;">
					<tr style="background:#6c6f6d; color:white; height: 60px; width: 100%;  ">
						<th style="width: 32.5%; font-family:Century-Regular; font-weight:100;">
							<div id="idColumna1">SUCURSAL</div>
						</th>
						<th style="width: 17%; font-family:Century-Regular; font-weight:100;">	
							<div>PLAN</div>
						</th>
						<th style="width: 17%; font-family:Century-Regular; font-weight:100;">
							<div>Visitas a Sucursal</div>						
						</th>
						<th style="width: 17%; font-family:Century-Regular; font-weight:100; position:relative;  border-left: 7px solid black; border-top:7px solid black; border-right: 7px solid black;">
							<div>AVANCE</div>
						</th>
						<th style="width: 1.5%; background:white;"></th>
						<th style="width: 15%; font-family:Century-Regular; font-style:italic; font-weight:100;">
							<div>Check List</div>
						</th>
					</tr>
				</table>	
			</div>
			<!-- TABLA QUE MUESTRA LOS DATOS DEL MODO VISTA -->			
			<div id="idHijoVisita" style="width: 90%; margin-top:10px; height: 50%; overflow-y: scroll; margin-left: 5%; margin-right: 5%; max-height: 447px;">
				<div id="tablaVisita" style="display:block;  width:100%;">
					<table id="idTablaVistaPrincipal" style="  border-spacing:1px 3px; width:100%;"></table>
				</div>
			</div>
			<div id="tablaVisitaHijo" style="display: none; width: 90%; margin-top:60px; height: 50%; margin-left: 5%; margin-right: 5%; max-height: 447px;">			
				<div style="display:inline-block ; width:30%">	
					<div style="width:100%; height:50px;">
					<table style="width:100%; height:100%; border-spacing:2px; ">
						<tr style='background:#6c6f6d;  text-align: center; font-family:Century-Regular; font-size: 17px; color:white; font-weight: 100;'>							
							<th style="width:100%; ">SUCURSALES</th>							
						</tr>
					</table>
					</div>
					<div style="width:100%; height:396px; overflow-y: scroll;">
						<table id="idHijoVisitaCentroTabla" style="border-spacing:2px 1px; width:100%;"></table>
					</div>
				</div>
				<div style="display: inline-block; width:69%; float:right;">
				<table style="border-spacing:2px 1px;  width:100%; height:100%;">
						<tr style='background:#6c6f6d;  text-align: center; font-family:Century-Regular; font-size: 24px;font-weight: 100;'>							
							<th class='tableUno' id="idCal01">D</th>
							<th class='tableUno' id="idCal02">L</th>
							<th class='tableUno' id="idCal03">M</th>
							<th class='tableUno' id="idCal04">M</th>
							<th class='tableUno' id="idCal05">J</th>
							<th class='tableUno' id="idCal06">V</th>
							<th class='tableUno' id="idCal07">S</th>
						</tr>
						<tr style='border: 1px solid red; height:22px; text-align: center; font-family:Century-Regular; font-size: 13px'>							
							<td class='tableDos' id="idCal1"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal2"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal3"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal4"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal5"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal6"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal7"><table><tr><td class="tableTres"></td></tr></table></td>
						</tr>
						<tr style='border: 1px solid red; height:22px; text-align: center; font-family:Century-Regular; font-size: 13px'>							
							<td class='tableDos' id="idCal8"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal9"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal10"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal11"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal12"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal13"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal14"><table><tr><td class="tableTres"></td></tr></table></td>
						</tr>
						<tr style='border: 1px solid red; height:22px; text-align: center; font-family:Century-Regular; font-size: 13px'>							
							<td class='tableDos' id="idCal15"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal16"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal17"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal18"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal19"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal20"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal21"><table><tr><td class="tableTres"></td></tr></table></td>
						</tr>
						<tr style='border: 1px solid red; height:20px; text-align: center; font-family:Century-Regular; font-size: 13px'>							
							<td class='tableDos' id="idCal22"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal23"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal24"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal25"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal26"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal27"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal28"><table><tr><td class="tableTres"></td></tr></table></td>
						</tr>
						<tr style='border: 1px solid red; height:20px;  text-align: center; font-family:Century-Regular; font-size: 13px'>							
							<td class='tableDos' id="idCal29"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal30"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal31"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal32"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal33"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal34"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal35"><table><tr><td class="tableTres"></td></tr></table></td>
						</tr>
						<tr style='border: 1px solid red; height:20px; text-align: center; font-family:Century-Regular; font-size: 13px'>							
							<td class='tableDos' id="idCal36"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal37"><table><tr><td class="tableTres"></td></tr></table></td>
							<td class='tableDos' id="idCal38"></td>
							<td class='tableDos' id="idCal39"></td>
							<td class='tableDos' id="idCal40"></td>
							<td class='tableDos' id="idCal41"></td>
							<td class='tableDos' id="idCal42"></td>
						</tr>
					</table>	
				</div>	
			</div>	
			<div id="idHijoVisitaFinTabla" style="margin-top:10px; width:90%; margin-left: 5%; margin-right: 5%;">
				<table style="border-spacing:2px; width:100%; height:50px;">
					<tr style="background:white; text-align: center; font-size: 20px; font-family:Century-Regular; color:black; height: 40px;">	
						<td style="border: 1px solid #40795c; width: 30%;">Total</td>
						<td style="width: 1%;"></td>
						<td style="border: 1px solid white; width: 9%;" id="totalDia0"></td>
						<td style="border: 1px solid white; width: 9%;" id="totalDia1"></td>
						<td style="border: 1px solid white; width: 9%;" id="totalDia2"></td>
						<td style="border: 1px solid white; width: 9%;" id="totalDia3"></td>
						<td style="border: 1px solid white; width: 9%;" id="totalDia4"></td>
						<td style="border: 1px solid white; width: 9%;" id="totalDia5"></td>
						<td style="border: 1px solid white; width: 9%;" id="totalDia6"></td>
						<td style="border: 5px solid black; width: 7.9%; background:green; font-family:Century-Regular; font-weight:bold; font-size: 40px; color:white" id="idTotal">0</td>	
					</tr>				
				</table>
			</div>	
			<!-- TABLA QUE MUESTRA LA DATOS DEL MODO OPERANDO -->	
			<div id="idHijoOperando" style="width: 95%; height: 57%;  margin-top:50px; margin-left:5%">	
				<div id="tablaOperando" style="display:block; width:100%; height:100%; max-height: 630px;">
					<div style="width:70%;">
						<table style="border-spacing:1px; width:100%; ">							
							<tr style="background:#6c6f6d; color:white; height: 40px; width: 100%;">
								<th style="width: 59%; font-family:Century-Regular; font-weight:100;" id="idColumna3">Todos</th>
								<th style="width: 1%; background: white; font-family:Century-Regular; font-weight:100;"/>
								<th style="width: 12%; font-family:Century-Regular; font-weight:100;">Sí</th>
								<th style="width: 12%; font-family:Century-Regular; font-weight:100;">No</th>
								<th style="width: 16%; font-family:Century-Regular; font-weight:100; border-left: 7px solid black; border-top:7px solid black; border-right: 7px solid black;">Avance</th>
							</tr>
						</table>		
					</div>
					<div  style="width: 70%; height:570px; display:inline-block; margin-top:5px;  overflow-y: scroll;">		
						<table id="idTablaOperandoPrincipal" style="border-spacing:1px 3px; width:100%; "></table>
					</div>
					<div style="width: 29%;  height:93%; display:inline-block; ">
						<div style=" height:20%; width:100%; ">
							<div align="center" style="position:absolute; height:16%; min-height:160px; width:27.5%; min-width:320px;">
								<img width="50px" height="50px" style="margin-left:-200px; margin-top: 6%; margin-right : 10px"
									src="../images/icono_rojo.png"/>
								<a style="position:absolute; margin-top: 9%; font-family:Century-Regular; font-size:19px; color:black;">
								Corrección inmediata</a>
							</div>
						</div>
						<div style=" height:20%; width:100%;">
							<div align="center" style="position: absolute; height:12%; min-height:120px; width:27.5%; min-width:320px; ">
								<img width="50px"  height="50px" style=" margin-left:-200px; margin-top: 4%;  margin-right : 10px"
									src="../images/icono_amarillo.png"/>
								<a style="position:absolute;  margin-top: 4%; font-family:Century-Regular; font-size:19px; text-align:right; color:black; ">							
								¿En qué tenemos<BR> que trabajar?</a>
							</div>
						</div>
						<div style=" height:20%; width:100%; ">
							<div align="center" style="position: absolute; height:18%; min-height:180px; width:27.5%; min-width:320px;">
								<img  width="50px" height="50px" style="margin-left:-200px; margin-top: 2.5%; margin-right : 10px"
									src="../images/icono_verde.png"/>
								<a style="position:absolute; margin-top: 3.5%; font-family:Century-Regular; font-size:19px; text-align:right; color:black;">								
								¿Qué debemos <BR> de mantener?</a>
							</div>
						</div>
						<div style=" height:32.7%; width:100%; position: relative">						
							<div align="center" style=" position: absolute; height:90%; width:100%; float:right;">
								<div style="width:320px; height:70px; margin-left:0%; margin-top:14%; margin-right:0%; ">
									<div onclick="infoOpera();" id="idInfoPorcentaje" style="width:248px; height:70px; display: inline-block;  border-color: black; border: 1px solid; float:right;">
										<div style="width:100%;height:70px;">
											<div style="margin-top:7px; color: black; text-align:center; font-family:Century-Regular;  font-size:9.5px; "> 
												El porcentaje de la tabla está calculado de acuerdo
												<div>al resultado de cada pregunta obtenido en el</div>
												<div>CheckList de <a style="color:black" id="idConSelec">0</a> </div>
												<div><a style="color:black; font-family:Century-Regular;  font-size:10px;"  id="idNomSelec"> Sucursales</a></div>
											</div>
										</div>										
									</div>	
									<div onclick="infoOpera();" id="idInfoPorcentaje2" style="width:64px; height:70px; display:inline-block;  float:right; ">									
										<div style="width:63px; height:70px; text-align:center; border-color: black; border: 1px solid; ">											
											<img style="margin-top:5px;"id="lupa" width="30px" height="30px"
															src="../images/icono_i.png"/>
											<div style="color: black; text-align: center; margin-top:1px; font-family:Century-Regular;  font-size:10px; line-height:10px; "> 
												Información relevante
											</div>
										</div>
									</div>								
								</div>
							</div>
						</div>						
					</div>				
				</div>	
				<div id="tablaOperandoHijo" style="display:none; width: 100%; height:588px;">	
					<div style="width:121px;  height:100%; display:inline-block;">					
						<div style=" width:100%; height:200px;">							
							<div class="logRegresa-btn4" style="text-align:center; padding-top:5px; ">Reactivo:</div>
						</div>
					</div>		
					<div  style="width:83.5%; height:100%; display:inline-block;  margin-right: 5%">					
						<div style="background:#285a43; height: 35px;">
							<div id="idReactivoPregunta" style=" list-style: none; color: white; text-align: center; font-family:Century-Regular; font-size:17px; padding: 9px"> 
								¿Pregunta?
							</div>
						</div>		
						<div style=" width:100%; margin-top: 15px; overflow-y: scroll;">
							<table style="border-spacing:2px; width:100%; ">								
								<tr style="background:#6c6f6d; text-align: center; color:white; font-size: 20px; height:50px;">						
									<td id="idColumna4" style="width:25%; font-family:Century-Regular;">REGIÓN</td>
									<td style="width:25%; font-family:Century-Regular;">Sí</td>
									<td style="width:25%; font-family:Century-Regular;">No</td>
									<td style="width:25%; font-family:Century-Regular; border-left: 7px solid black; border-top:7px solid black; border-right: 7px solid black;">Avance</td>	
								</tr>
							</table>							
						</div>
						<div  style=" width: 100%; height:78.25%; margin-top:20px;  display:inline-block; overflow-y: scroll;">
							<table style=" border-spacing:2px; width:100%;" id="tablaOperandoHijoRelleno">													
							</table>
						</div>
					</div>
				</div>
			</div>		
			<!-- TABLA QUE MUESTRA EL REGRESO Y MENSAJE DE ADVERTENCIA -->	
			<div style=" margin-top:25px; margin-right:20px; height:56px;">
				<div style="margin-top:10px; margin-left:45%; height: 40px; float:left;">
					<div class="logRegresa-btn" id="logRegresa-btn" style="text-align: center; padding-top: 7px;">Regresar</div>
					<div class="logRegresa-btn2" id="logRegresa-btn2" style="margin-top:55px; text-align: center; padding-top: 7px;">Regresar</div>
				</div>
				<div onclick="infoVisita();" id="idInfoPiePagina2" style="width:300px; height:56px;  display: inline-block; float: right; border-color: black; border: 1px solid; margin-top: -5px;">
					<div style="width:240px;height:60px;  display: inline-block;">
						<div style="margin-top:3px; color: black; text-align: left; font-family:Century-Regular;  font-size:12px; "> 
							<div style="margin-left:3px;">Problemas con:</div>
							<div style="margin-left:20px;">- Visita + de una vez la sucursal</div>
							<div style="margin-left:20px;">- No ha visitado la sucursal</div>
						</div>
					</div>
					<div style="width:60px;height:56px;  display: inline-block; float:left; text-align:center; bottom:0px;">
						<img id="lupa" width="65px" height="50px;" style="margin-top:3px; margin-left: 10px;"
									src="../images/icono_problemas.png"/>
					</div>
				</div>
				<div onclick="infoVisita();" id="idInfoPiePagina1"  style="width:65px; height:56px; float:right;  margin-top: -5px;">
					<div style="width:63px; height:56px; display: inline-block; text-align:center; border-color: black; border: 1px solid;">
						<img id="lupa" width="30px" height="30px" style="margin-top:2px;"
										src="../images/icono_i.png"/>
						<div style="color: black; margin-top:0px; text-align: center; font-family:Century-Regular;  font-size:10px; line-height:9px; "> 
							Información relevante
						</div>
					</div>	
				</div>
			</div>	
		</div>
		</form:form>
	</div>
	
	<div id="idGeneralDetalle" class="deGeneralDeralle">
		<div class="deMargen1">
			<img onclick="nuevaVistaResumen();" class="deIm1-1" width="180px" height="80px" src="../images/reporteOnline/logoBanco.png">
		</div>
		<div class="deMargen2" >
			<div class="deDetalleGeneral1">
				<table class="tablaDatosGenerales">
					<tbody>
						<tr><td></td><td><div><h3 class="paraContainer">Para Regional</h3></div></td><td></td></tr>
						<tr><td></td><td><div><h1 id="sucursalContainer">Nombre Sucursal</h1></div></td><td></td></tr>
						<tr><td></td><td>
						<ul id="accordion">
							<li class="folios">
						      	<div class="infoWrapper">
							        <div class="tituloContainer">
										<h6 class="tituloTextFolios">Folios</h6>
									</div>
							        <div class="contenidoGeneralFolios">
							        <div class="resumen">RESUMEN</div>
										<div class="contentScroll">
							        		<table id="tablaFoliosTitulo">
							        			<thead>
							        				<tr>
								        				<th class="anchoTotalFolioIzqTitulo"><strong>Total Folios</strong></th>
								        				<th class="anchoTotalFolioDerTitulo"><strong id="idTotalFolios"></strong></th>
							        				</tr>
							        			</thead>
							        		</table>
							        		<div class="margenFolioContenido">
							        			<table id="tablaFolioContenido">								        		
								        			<tbody id="tablaFoliosBody">
									        			<tr>
									        				<td class="anchoTotalFolioIzq" id="fPregunta"></td>
										        			<td class="anchoTotalFolioDer" id="eLink"></td>
									        			</tr>
									        		</tbody>
							        			</table>
							        		</div>
							        	</div>
							        </div>
						    	</div>
						    </li>
						    <li class="evidencias">
					        	<div class="infoWrapper">
							        <div class="tituloContainer">
										<h3 class="tituloTextEvidencias">Evidencias</h3>
									</div>
									<div class="contenidoGeneralEvidencias">
							        <div class="resumen">RESUMEN</div>
							        	<div class="contentScroll">
							        		<table id="tablaEvidenciasTitulo">
							        			<thead>
							        				<tr>
								        				<th class="anchoTotalEvidenciaIzqTitulo" id="idTotalEvidencia"><strong>Total de Evidencias</strong></th>
								        				<th class="anchoTotalEvidenciaDerTitulo"><strong>Ver Evidencia</strong></th>
							        				</tr>
							        			</thead>
							        		</table>
							        		<div class="margenEvidenciaContenido">
							        			<table id="tablaEvidenciasContenido">								        		
								        			<tbody id="tablaEvidenciasBody">
									        			<tr>
									        				<td class="anchoTotalEvidenciaIzq" id="ePregunta"></td>
									        				<td class="anchoTotalEvidenciaDer" id="eLink"></td>
									        			</tr>
									        		</tbody>
							        			</table>
							        		</div>
							        	</div>							        	
							        </div>
					        	</div>
						    </li>
							<li class="fechaHora">
								<div class="infoWrapper">
									<div class="tituloContainer">
										<h3 class="tituloTextFecha">Fecha&nbsp;y&nbsp;Hora</h3>
									</div>
							        <div class="contenidoGeneralFechaHora">
							        	<div class="resumen">RESUMEN</div>
						        		<table id="tablaFechaHora">
						        			<thead><tr>
						        				<th class="margenTituloFecha"><strong>Fecha</strong></th>
						        				<th class="margenTituloFecha"><strong>Hora</strong></th>
						        			</tr></thead>
							        		<tbody>
							        			<tr><td id="fInicio">Inicio:</td><td id="hInicio">Inicio: Hrs</td></tr>
							        			<tr><td id="fFin">Fin:</td><td id="hFin">Fin:  Hrs</td></tr>
							        			<tr><td id="fEnvio">Env&iacute;o: </td><td id="hModo">Modo: </td></tr>
							        		</tbody>
						        		</table>
							        </div>
								</div>
						    </li>
					  	</ul>
						</td><td></td></tr>
					</tbody>
				</table>
			</div>
			<div class="deDetalleGeneral2">
				<div class="deMarco2-2">
					<div class="deMarco2-2-1">
						<div class="deMarcoDetalle-21">
							<div class="deMarcoDetalle-21-1"></div>
							<div class="deMarcoDetalle-21-2"><table class="afontdeMarcoDetalle-21-2"><tr><td class="tipoFuenteUno">DETALLE</td></tr></table></div>
							<div class="deMarcoDetalle-21-3"><table class="afontdeMarcoDetalle-21-3"><tr><td class="tipoFuenteUnoFolio"></td> <td id="tipoFuenteDosFolio2"></td></tr></table></div>
						</div>
						<div class="deMarcoDetalle-22">
							<div class="deMarcoDetalle-21-1"><table class="afontdeMarcoDetalle-21-222"><tr><td id="tipoFuenteUnoPregunta"></td></tr></table></div>
							<div class="deMarcoDetalle-21-2">
								<table class="afontdeMarcoDetalle-21-2Cuadrado">																		
									<tr>
										<td class="marcoTdCuadrado" align="center">
										    <table class="marcoUnoCuadrado"><tr><td id="tipoFuenteUnoCuadro" align="center"></td></tr></table>
										</td>							   
									</tr>
								</table>
							</div>
							<div class="deMarcoDetalle-21-3">
								<table class="afontdeMarcoDetalle-21-3">
									<tr><td class="tipoFuenteUnoFolio">Folio:</td> <td id="tipoFuenteDosFolio"></td> </tr>
								</table>	
								<table class="afontdeMarcoDetalle-21-3">
									<tr><td class="tipoFuenteUnoAccion" valign="top">Acción:</td> <td id="tipoFuenteDosAccion"></td> </tr>
								</table>						
							</div>
						</div>
						<div class="deMarcoDetalle-23"></div>
					</div>
					<div class="deMarco2-2-2">					
						<div class="margenCarruselUno">							
							<div class="margenCarruselUno-1"></div>
							<div class="margenCarruselUno-2">
								<img onclick="anteriorDetalle();" class="imagAnterior" src="../images/flecha_atras.png">	
							</div>							
						</div>
						<div class="margenCarruselDos">
							<div class="margenCarruselUno-2">
								<table class="afontdeMarcoDetalle-Evidencia"><tr><td class="tipoFuenteUnoEvidencia">Evidencia:</td></tr></table>
							</div>
							<div class="margenCarruselUno-1"></div>							
						</div>
						<div class="margenCarruselTres">							
							<div id="margenImagen">													
								<img class="tamaImagen" src="../images/reporteOnline/evidencia_no_requerida.png">
							</div>
							<div class="margenNumeros">
								<table class="celdaNumeros">
									<tr>
										<td id="celdaCirculos"></td>
									</tr>
								</table>								
							</div>							
						</div>
						<div class="margenCarruselCuatro">
							<div class="margenCarruselUno-1"></div>
							<div class="margenCarruselUno-2">
								<img onclick="siguienteDetalle();" class="imagPosterior" src="../images/flecha_adelante.png">	
							</div>
						</div>					
					</div>					
				</div>
			</div>		
		</div>			
		<div style="margin-top:1%; margin-left:45%; height: 40px; float:left;">
			<div onclick="regresaNuevaVistaResumen();" class="logRegresa-btn5" id="logRegresa-btn5" style="text-align: center; padding-top: 7px;">Regresar</div>
		</div>
	</div>	
	
	<div id="ventanaEmergente" class="modal-wrapper" onclick="oneClick(event, this);">
	    <div class="ventanaEmergente-contenedor">
	       <img id="idImagenEmergente" class="tamaImagenEmergente" src="../images/reporteOnline/evidencia_no_requerida.png">
	       <a class="ventanaEmergente-cerrar" onclick="ocultaPopup();">X</a>
	    </div>
	</div>
	
	<div class="modal"><!-- Place at bottom of page --></div>			
</body>
</html>