package com.gruposalinas.checklist.domain;

import java.io.Serializable;

public class UsuarioADN extends UsuarioDTO implements Serializable{
	
	private static final long serialVersionUID = 1946287931073886091L;
	
	private String numempleado; 
	private String ws; 
	private String apellidop; 
	private String apellidom; 
	private String sucursal; 
	private String nomsucursal; 
	private String puesto; 
	private String descpuesto;
	private String sap;
	private String pais;
	private String canal;
	private String servidor;
	private String puestobase;
	
	public String getNumempleado() {
		return numempleado;
	}
	public void setNumempleado(String numempleado) {
		this.numempleado = numempleado;
	}
	public String getWs() {
		return ws;
	}
	public void setWs(String ws) {
		this.ws = ws;
	}
	public String getApellidop() {
		return apellidop;
	}
	public void setApellidop(String apellidop) {
		this.apellidop = apellidop;
	}
	public String getApellidom() {
		return apellidom;
	}
	public void setApellidom(String apellidom) {
		this.apellidom = apellidom;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getNomsucursal() {
		return nomsucursal;
	}
	public void setNomsucursal(String nomsucursal) {
		this.nomsucursal = nomsucursal;
	}
	public String getPuesto() {
		return puesto;
	}
	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}
	public String getDescpuesto() {
		return descpuesto;
	}
	public void setDescpuesto(String descpuesto) {
		this.descpuesto = descpuesto;
	}
	public String getSap() {
		return sap;
	}
	public void setSap(String sap) {
		this.sap = sap;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getCanal() {
		return canal;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
	public String getServidor() {
		return servidor;
	}
	public void setServidor(String servidor) {
		this.servidor = servidor;
	}
	public String getPuestobase() {
		return puestobase;
	}
	public void setPuestobase(String puestobase) {
		this.puestobase = puestobase;
	}
	
}
