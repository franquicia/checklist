package com.gruposalinas.checklist.domain;

public class RH {

	private int plantilla;
	private int rotacion;
	private int asesores;
	private int certificados;
	private int tareas;
	
	public RH() {
		super();
	}
	public int getPlantilla() {
		return plantilla;
	}
	public void setPlantilla(int plantilla) {
		this.plantilla = plantilla;
	}
	public int getRotacion() {
		return rotacion;
	}
	public void setRotacion(int rotacion) {
		this.rotacion = rotacion;
	}
	public int getAsesores() {
		return asesores;
	}
	public void setAsesores(int asesores) {
		this.asesores = asesores;
	}
	public int getCertificados() {
		return certificados;
	}
	public void setCertificados(int certificados) {
		this.certificados = certificados;
	}
	public int getTareas() {
		return tareas;
	}
	public void setTareas(int tareas) {
		this.tareas = tareas;
	}
	
	@Override
	public String toString() {
		return "RH [plantilla=" + plantilla + ", rotacion=" + rotacion + ", asesores=" + asesores + ", certificados="
				+ certificados + ", tareas=" + tareas + "]";
	}	
}
