$body = $("body");
$(document).on({
    ajaxStart: function() {$body.addClass("loading");},
    ajaxStop: function() {$body.removeClass("loading");}    
});

$(document).on("click",function(e) {
    
    var container = $("#unicaTabla");
                       
       if (!container.is(e.target) && container.has(e.target).length === 0) { 
    	   menuTiempoRealDesaparece();
       }
});

var banTiempo = 0;
var borra =0;

var matrizAscNom = [];
var matrizDescNom = [];
var matrizAscPres = [];
var matrizDescPres = [];
var matrizAscCons = [];
var matrizDescCons = [];

var matrizAscNomHijo = [];
var matrizDescNomHijo = [];
var matrizAscPresHijo = [];
var matrizDescPresHijo = [];
var matrizAscConsHijo = [];
var matrizDescConsHijo = [];

var matrizAscNomNieto = [];
var matrizDescNomNieto = [];
var matrizAscPresNieto = [];
var matrizDescPresNieto = [];
var matrizAscConsNieto = [];
var matrizDescConsNieto = [];

var porcentajeL=0;
var validaCheck=0;
var intervaloZona=null;
var intervaloRegion=null;
var intervaloSucursal=null;

var activoRegion = 0;
var activoSucursal = 0;

var idCecoRegion = 0;
var nomCecoRegion = "";

var urlCache = "";

var sumFil=0;
var preC=0;
var preR=0;
var preP=0;
var conC=0;
var conR=0;
var conP=0;

var aa = 0;
var bb = 0;
var cc = 0;

var dd = 0;
var ee = 0;
var ff = 0;

var gg = 0;
var hh = 0;
var ii = 0;

var jj = 0;
var kk = 0;
var ll = 0;

var nomZona = "Zona";

$(window).load(function() {
	despliegaPadre();
});

function ordenaNombre(divActual,nivel){
	$("#presAscendente"+nivel).hide();
	$("#presDescendente"+nivel).hide();
	$("#consAscendente"+nivel).hide();
	$("#consDescendente"+nivel).hide();
	
	$("#nomVista"+nivel).hide();
	
	$("#presVista"+nivel).show();
	$("#consVista"+nivel).show();
	
	var 	$divs = $(divActual);
	
	if(nivel==""){
		document.getElementById("idGeneralTablaTerritorios").innerHTML = "";
		if(dd==0){
			if(aa==0){
				$("#nomAscendente"+nivel).hide();
				$("#nomDescendente"+nivel).show();
				pintaPadre(matrizDescNom);
			    aa=1;
			    dd=1;
			}else if(aa==1){
				$("#nomAscendente"+nivel).show();
				$("#nomDescendente"+nivel).hide();
				pintaPadre(matrizAscNom);
		    		aa=0;
		    }
		}else if(dd==1){
			$("#nomAscendente"+nivel).show();
			$("#nomDescendente"+nivel).hide();
			pintaPadre(matrizAscNom);
	    		dd=0;
	    		aa=0;
		}
		
	}else if(nivel=="Hijo"){
		document.getElementById("idGeneralTablaHijo").innerHTML = "";
		if(gg==0){
			if(aa==0){
				$("#nomAscendente"+nivel).hide();
				$("#nomDescendente"+nivel).show();
				pintaHijo(matrizDescNomHijo);
			    aa=1;
			    gg=1;
			}else if(aa==1){
				$("#nomAscendente"+nivel).show();
				$("#nomDescendente"+nivel).hide();
				pintaHijo(matrizAscNomHijo);
		    		aa=0;
		    }
		}else if(gg==1){
			$("#nomAscendente"+nivel).show();
			$("#nomDescendente"+nivel).hide();
			pintaHijo(matrizAscNomHijo);
	    		gg=0;
	    		aa=0;
		}	
		
	}else if(nivel=="Nieto"){
		document.getElementById("idGeneralTablaNieto").innerHTML = "";
		if(jj==0){
			if(aa==0){
				$("#nomAscendente"+nivel).hide();
				$("#nomDescendente"+nivel).show();
				pintaNieto(matrizDescNomNieto);
			    aa=1;
			    jj=1;
			}else if(aa==1){
				$("#nomAscendente"+nivel).show();
				$("#nomDescendente"+nivel).hide();
				pintaNieto(matrizAscNomNieto);
		    		aa=0;
		    }
		}else if(jj==1){
			$("#nomAscendente"+nivel).show();
			$("#nomDescendente"+nivel).hide();
			pintaNieto(matrizAscNomNieto);
	    		jj=0;
	    		aa=0;
		}	
	}
}

function ordenaAvancePrestamos(divActual , nivel){
	$("#nomAscendente"+nivel).hide();
	$("#nomDescendente"+nivel).hide();
	$("#consAscendente"+nivel).hide();
	$("#consDescendente"+nivel).hide();
	
	$("#presVista"+nivel).hide();
	
	$("#nomVista"+nivel).show();
	$("#consVista"+nivel).show();

	var 	$divs = $(divActual);
		
	if(nivel==""){
		document.getElementById("idGeneralTablaTerritorios").innerHTML = "";
		if(ee==0){
			if(bb==0){
				$("#presAscendente"+nivel).hide();
				$("#presDescendente"+nivel).show();
				pintaPadre(matrizDescPres);
			    bb=1;
			    ee=1;
			}else if(bb==1){
				$("#presAscendente"+nivel).show();
				$("#presDescendente"+nivel).hide();
				pintaPadre(matrizAscPres);
				bb=0;
		    }
		}else if(ee==1){
			$("#presAscendente"+nivel).show();
			$("#presDescendente"+nivel).hide();
			pintaPadre(matrizAscPres);
			ee=0;
			bb=0;
		}
	}else if(nivel=="Hijo"){
		document.getElementById("idGeneralTablaHijo").innerHTML = "";
		if(hh==0){
			if(bb==0){
				$("#presAscendente"+nivel).hide();
				$("#presDescendente"+nivel).show();
				pintaHijo(matrizDescPresHijo);
			    bb=1;
			    hh=1;
			}else if(bb==1){
				$("#presAscendente"+nivel).show();
				$("#presDescendente"+nivel).hide();
				pintaHijo(matrizAscPresHijo);
				bb=0;
		    }
		}else if(hh==1){
			$("#presAscendente"+nivel).show();
			$("#presDescendente"+nivel).hide();
			pintaHijo(matrizAscPresHijo);
			hh=0;
			bb=0;
		}
	}else if(nivel=="Nieto"){
		document.getElementById("idGeneralTablaNieto").innerHTML = "";
		if(kk==0){
			if(bb==0){
				$("#presAscendente"+nivel).hide();
				$("#presDescendente"+nivel).show();
				pintaNieto(matrizDescPresNieto);
			    bb=1;
			    kk=1;
			}else if(bb==1){
				$("#presAscendente"+nivel).show();
				$("#presDescendente"+nivel).hide();
				pintaNieto(matrizAscPresNieto);
				bb=0;
		    }
		}else if(kk==1){
			$("#presAscendente"+nivel).show();
			$("#presDescendente"+nivel).hide();
			pintaNieto(matrizAscPresNieto);
			kk=0;
			bb=0;
		}
	}
}

function ordenaAvanceConsumo(divActual,nivel){
	$("#nomAscendente"+nivel).hide();
	$("#nomDescendente"+nivel).hide();
	$("#presAscendente"+nivel).hide();
	$("#presDescendente"+nivel).hide();
	
	$("#consVista"+nivel).hide();
	
	$("#nomVista"+nivel).show();
	$("#presVista"+nivel).show();
	
	var 	$divs = $(divActual);	
	
	if(nivel==""){
		document.getElementById("idGeneralTablaTerritorios").innerHTML = "";
		if(ff==0){
			if(cc==0){
				$("#consAscendente"+nivel).hide();
				$("#consDescendente"+nivel).show();
				pintaPadre(matrizDescCons);
			    cc=1;
			    ff=1;
			}else if(cc==1){
				$("#consAscendente"+nivel).show();
				$("#consDescendente"+nivel).hide();
				pintaPadre(matrizAscCons);
				cc=0;
		    }
		}else if(ff==1){
			$("#consAscendente"+nivel).show();
			$("#consDescendente"+nivel).hide();
			pintaPadre(matrizAscCons);
			ff=0;
			cc=0;
		}
	}else if(nivel=="Hijo"){
		document.getElementById("idGeneralTablaHijo").innerHTML = "";
		if(ii==0){
			if(cc==0){
				$("#consAscendente"+nivel).hide();
				$("#consDescendente"+nivel).show();
				pintaHijo(matrizDescConsHijo);
			    cc=1;
			    ii=1;
			}else if(cc==1){
				$("#consAscendente"+nivel).show();
				$("#consDescendente"+nivel).hide();
				pintaHijo(matrizAscConsHijo);
				cc=0;
		    }
		}else if(ii==1){
			$("#consAscendente"+nivel).show();
			$("#consDescendente"+nivel).hide();
			pintaHijo(matrizAscConsHijo);
			ii=0;
			cc=0;
		}
	}else if(nivel=="Nieto"){
		document.getElementById("idGeneralTablaNieto").innerHTML = "";
		if(ll==0){
			if(cc==0){
				$("#consAscendente"+nivel).hide();
				$("#consDescendente"+nivel).show();
				pintaNieto(matrizDescConsNieto);
			    cc=1;
			    ll=1;
			}else if(cc==1){
				$("#consAscendente"+nivel).show();
				$("#consDescendente"+nivel).hide();
				pintaNieto(matrizAscConsNieto);
				cc=0;
		    }
		}else if(ll==1){
			$("#consAscendente"+nivel).show();
			$("#consDescendente"+nivel).hide();
			pintaNieto(matrizAscConsNieto);
			ll=0;
			cc=0;
		}
	}
}

function reporte(){
	modal = 1;
	document.getElementById("tipoModal").className="modal3";
	location.href = "/checklist/reportes/reporteTiempoRealHoy.htm";
}

function myFunction() {
	$body.removeClass("loading1");
	location.reload();
}

function nobackbutton(){
	window.location.hash="no-back-button";
	window.location.hash="Again-No-back-button" // chrome
	window.onhashchange=function(){window.location.hash="";}
}

function pPersonales(fila, idCeco, nomCeco, tam){

	document.getElementById("idGeneralTablaTerritorios").innerHtml="";
	$.ajax({
		type : "GET",
		url :"/migestion/controladores/getPorcentajesColocacionTiempoRealHoy.json?ceco="+idCeco+"&nombreCeco="+nomCeco+"&nivel=Z",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		// async:false,
		delimiter : ",",
		success : function(json) {

			if(json!=null){
				
				sumFil = sumFil + 1;
				
				if(json.colorPersonales != "-"){
								
					matrizAscNom.push(json);
					matrizAscPres.push(json);
					matrizAscCons.push(json);
					
					preC = preC + json.compromisoPersonales;
					preR = preR + json.avanceTiempoRealPersonales;
					preP = preP + json.valorPersonales;
					conC = conC + json.compromisoConsumo;
					conR = conR + json.avanceTiempoRealConsumo;
					conP = conP + json.valorConsumo;

					if(sumFil == (tam)){

						metOrdPadAscNom(matrizAscNom);
						var ar01 = metOrdPadAscPre(matrizAscPres);
						metOrdPadAscCon(matrizAscCons);
						
						pintaPadre(ar01);

						/*
						for (var j = 0; j < matrizAscPres.length; j++) {
							alert('preA ' + matrizAscPres[j].valorPersonales  + 'preDE ' + matrizDescPres[j].valorPersonales);
							//alert('consumA ' + matrizAscCons[j].valorConsumo  + 'consumDE ' + matrizDescCons[j].valorConsumo);
						}

						for (var j = 0; j < matrizAscCons.length; j++) {
							alert('preA ' + matrizAscCons[j].valorConsumo  + 'preDE ' + matrizDescCons[j].valorConsumo);
							//alert('consumA ' + matrizAscCons[j].valorConsumo  + 'consumDE ' + matrizDescCons[j].valorConsumo);
						}*/

						
						if(sumFil==tam){
							
							var prP = 0;
							var prC = 0;
							if(json.colorPersonales != "-"){
								document.getElementById("preCTotal").innerHTML = "";
								document.getElementById("preRTotal").innerHTML = "";
								document.getElementById("prePTotal").innerHTML = "";
								document.getElementById("conCTotal").innerHTML = "";
								document.getElementById("conRTotal").innerHTML = "";
								document.getElementById("conPTotal").innerHTML = "";
								
								prP=(redondeo((preP/tam),0));
								prC=(redondeo((conP/tam),0));
								
								$("#preCTotal").append("<p>$"+new Intl.NumberFormat('es-MX').format(preC)+"</p>");
								$("#preRTotal").append("<p>$"+new Intl.NumberFormat('es-MX').format(preR)+"</p>");
								$("#prePTotal").append("<div class='radio grisN'><b class='colorN'>"+prP+"%</b></div>");
	
								$("#conCTotal").append("<p>$"+new Intl.NumberFormat('es-MX').format(conC)+"</p>");
								$("#conRTotal").append("<p>$"+new Intl.NumberFormat('es-MX').format(conR)+"</p>");
								$("#conPTotal").append("<div class='radio grisN'><b class='colorN'>"+prC+"%</b></div>");
							}
							
							document.getElementById("tipoModal").className="modal1";
							sumFil=0;
							
							preC = 0;
							preR = 0;
							preP = 0;
							conC = 0;
							conR = 0;
							conP = 0;	
						}
					}
				}
			}
		},
		error : function() {
			
			$("#idGeneralTablaTerritorios").append("" +
					"<dl class='accordion3'>" +
						"<div class='divDT' style='background: #dbdbdb33;'>" +
							"<table class='bloquer linea goshadow1'>" +
								"<tbody>" +
									"<tr class='pointer'>" +
										"<td id='nom00' class='nombreCheck' style='background-image:none;'><ab>-</ab></td>" +
										
										"<td  style='width:1%; min-width: 15px;'></td>" +
										
										"<td id='preC00' class='nombreCheck' style='width: "+(70/6)+"%;'><p>-</p></td>" +
										"<td id='preR00' class='nombreCheck' style='width: "+(70/6)+"%;'><p>-</p></td>" +
										"<td id='preP00' class='nombreCheck' style='width: "+(70/6)+"%;'><div class='radio grisN'><bc class='colorN'>-</bc></div></td>" +
										
										"<td  style='width:1%; min-width: 15px;'></td>" +
										"<td  style='width:0.1%; background: #f2f2f2;'></td>" +
										
										"<td id='conC00' class='nombreCheck' style='width: "+(70/6)+"%;'><p>-</p></td>" +
										"<td id='conR00' class='nombreCheck' style='width: "+(70/6)+"%;'><p>-</p></td>" +
										"<td id='conP00' class='nombreCheck' style='width: "+(70/6)+"%;'><div class='radio grisN'><cb class='colorN'>-</cb></div></td>" +
										"<td  style='width:1%; min-width: 15px;'></td>" +
									"</tr>" +
								"</tbody>" +
							"</table>" +
						"</div>" +
					"</dl>");	

			document.getElementById("tipoModal").className="modal1";
		}
	});
}

function pPersonalesHijo(fila, idCeco, nomCeco, tam){

	if(borra==0){
		document.getElementById("preCTotalHijo").innerHTML = "";
		document.getElementById("preRTotalHijo").innerHTML = "";
		document.getElementById("prePTotalHijo").innerHTML = "";
		document.getElementById("conCTotalHijo").innerHTML = "";
		document.getElementById("conRTotalHijo").innerHTML = "";
		document.getElementById("conPTotalHijo").innerHTML = "";
		borra=1;
	}
	
	$.ajax({
		type : "GET",
		url :"/migestion/controladores/getPorcentajesColocacionTiempoRealHoy.json?ceco="+idCeco+"&nombreCeco="+nomCeco+"&nivel=R",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		// async:false,
		delimiter : ",",
		success : function(json) {

			if(json!=null){
				sumFil = sumFil + 1;
				
				if(json.colorPersonales != "-"){
					
					matrizAscNomHijo.push(json);
					matrizAscPresHijo.push(json);
					matrizAscConsHijo.push(json);
					
					preC = preC + json.compromisoPersonales;
					preR = preR + json.avanceTiempoRealPersonales;
					preP = preP + json.valorPersonales;
					conC = conC + json.compromisoConsumo;
					conR = conR + json.avanceTiempoRealConsumo;
					conP = conP + json.valorConsumo;	
					
					if(sumFil == (tam)){

						metOrdPadAscNomHijo(matrizAscNomHijo);
						var ar01 = metOrdPadAscPreHijo(matrizAscPresHijo);
						metOrdPadAscConHijo(matrizAscConsHijo);
						
						pintaHijo(ar01);

						if(sumFil==tam){
							var prP = 0;
							var prC = 0;
							if(json.colorPersonales != "-"){

								prP=(redondeo((preP/tam),0));
								prC=(redondeo((conP/tam),0));
								
								$("#preCTotalHijo").append("<p>$"+new Intl.NumberFormat('es-MX').format(preC)+"</p>");
								$("#preRTotalHijo").append("<p>$"+new Intl.NumberFormat('es-MX').format(preR)+"</p>");
								$("#prePTotalHijo").append("<div class='radio grisN'><b class='colorN'>"+prP+"%</b></div>");
								
								$("#conCTotalHijo").append("<p>$"+new Intl.NumberFormat('es-MX').format(conC)+"</p>");
								$("#conRTotalHijo").append("<p>$"+new Intl.NumberFormat('es-MX').format(conR)+"</p>");
								$("#conPTotalHijo").append("<div class='radio grisN'><b class='colorN'>"+prC+"%</b></div>");
			
							}
							
							document.getElementById("tipoModal").className="modal1";
							sumFil=0;
							
							preC = 0;
							preR = 0;
							preP = 0;
							conC = 0;
							conR = 0;
							conP = 0;
							
							borra=0;
						}
					}
				}	
			}
		},
		error : function() {
			
			$("#idGeneralTablaHijo").append("" +
					"<dl class='accordion3'>" +
						"<div class='divDTHijo' style='background: #dbdbdb33;'>" +
							"<table class='bloquer linea goshadow1'>" +
								"<tbody>" +
									"<tr class='pointer'>" +
										"<td id='nom10' class='nombreCheck' style='background-image:none;'><ab>-</ab></td>" +
										
										"<td  style='width:1%; min-width: 15px;'></td>" +
										
										"<td id='preC10' class='nombreCheck' style='width: "+(70/6)+"%;'><p>-</p></td>" +
										"<td id='preR10' class='nombreCheck' style='width: "+(70/6)+"%;'><p>-</p></td>" +
										"<td id='preP10' class='nombreCheck' style='width: "+(70/6)+"%;'><div class='radio grisN'><bc class='colorN'>-</bc></div></td>" +
										
										"<td  style='width:1%; min-width: 15px;'></td>" +
										"<td  style='width:0.1%; background: #f2f2f2;'></td>" +
										
										"<td id='conC10' class='nombreCheck' style='width: "+(70/6)+"%;'><p>-</p></td>" +
										"<td id='conR10' class='nombreCheck' style='width: "+(70/6)+"%;'><p>-</p></td>" +
										"<td id='conP10' class='nombreCheck' style='width: "+(70/6)+"%;'><div class='radio grisN'><cb class='colorN'>-</cb></div></td>" +
										"<td  style='width:1%; min-width: 15px;'></td>" +
									"</tr>" +
								"</tbody>" +
							"</table>" +
						"</div>" +
					"</dl>");	
			
			document.getElementById("tipoModal").className="modal1";
			
			if(sumFil==tam){
				borra=0;
			}
		}
	});
}

function pPersonalesNieto(fila, idCeco, nomCeco, tam){

	if(borra==0){
		document.getElementById("preCTotalNieto").innerHTML = "";
		document.getElementById("preRTotalNieto").innerHTML = "";
		document.getElementById("prePTotalNieto").innerHTML = "";
		document.getElementById("conCTotalNieto").innerHTML = "";
		document.getElementById("conRTotalNieto").innerHTML = "";
		document.getElementById("conPTotalNieto").innerHTML = "";
		borra=1;
	}
	
	$.ajax({
		type : "GET",
		url :"/migestion/controladores/getPorcentajesColocacionTiempoRealHoy.json?ceco="+idCeco+"&nombreCeco="+nomCeco+"&nivel=T",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		// async:false,
		delimiter : ",",
		success : function(json) {

			if(json!=null){
				sumFil = sumFil + 1;
				
				if(json.colorPersonales != "-"){
					
					matrizAscNomNieto.push(json);
					matrizAscPresNieto.push(json);
					matrizAscConsNieto.push(json);
					
					preC = preC + json.compromisoPersonales;
					preR = preR + json.avanceTiempoRealPersonales;
					preP = preP + json.valorPersonales;
					conC = conC + json.compromisoConsumo;
					conR = conR + json.avanceTiempoRealConsumo;
					conP = conP + json.valorConsumo;	
					
					
					if(sumFil == (tam)){

						metOrdPadAscNomNieto(matrizAscNomNieto);
						var ar01 = metOrdPadAscPreNieto(matrizAscPresNieto);
						metOrdPadAscConNieto(matrizAscConsNieto);
						
						pintaNieto(ar01);
						

						if(sumFil==tam){
							var prP = 0;
							var prC = 0;
							if(json.colorPersonales != "-"){
								
								
								prP=(redondeo((preP/tam),0));
								prC=(redondeo((conP/tam),0));
								
								$("#preCTotalNieto").append("<p>$"+new Intl.NumberFormat('es-MX').format(preC)+"</p>");
								$("#preRTotalNieto").append("<p>$"+new Intl.NumberFormat('es-MX').format(preR)+"</p>");
								$("#prePTotalNieto").append("<div class='radio grisN'><b class='colorN'>"+prP+"%</b></div>");
								
								$("#conCTotalNieto").append("<p>$"+new Intl.NumberFormat('es-MX').format(conC)+"</p>");
								$("#conRTotalNieto").append("<p>$"+new Intl.NumberFormat('es-MX').format(conR)+"</p>");
								$("#conPTotalNieto").append("<div class='radio grisN'><b class='colorN'>"+prC+"%</b></div>");
							}
							
							document.getElementById("tipoModal").className="modal1";
							sumFil=0;
							
							preC = 0;
							preR = 0;
							preP = 0;
							conC = 0;
							conR = 0;
							conP = 0;
							
							borra=0;
						}
					}
				}
			}
			
		},
		error : function() {
			$("#idGeneralTablaNieto").append("" +
					"<dl class='accordion3'>" +
						"<div class='divDTNieto' style='background: #dbdbdb33;'>" +
							"<table class='bloquer linea goshadow1'>" +
								"<tbody>" +
									"<tr class='pointer'>" +
										"<td id='nom20' class='nombreCheck' style='background-image:none;'><ab>-</ab></td>" +
										
										"<td  style='width:1%; min-width: 15px;'></td>" +
										
										"<td id='preC20' class='nombreCheck' style='width: "+(70/6)+"%;'><p>-</p></td>" +
										"<td id='preR20' class='nombreCheck' style='width: "+(70/6)+"%;'><p>-</p></td>" +
										"<td id='preP20' class='nombreCheck' style='width: "+(70/6)+"%;'><div class='radio grisN'><bc class='colorN'>-</bc></div></td>" +
										
										"<td  style='width:1%; min-width: 15px;'></td>" +
										"<td  style='width:0.1%; background: #f2f2f2;'></td>" +
										
										"<td id='conC20' class='nombreCheck' style='width: "+(70/6)+"%;'><p>-</p></td>" +
										"<td id='conR20' class='nombreCheck' style='width: "+(70/6)+"%;'><p>-</p></td>" +
										"<td id='conP20' class='nombreCheck' style='width: "+(70/6)+"%;'><div class='radio grisN'><cb class='colorN'>-</cb></div></td>" +
										"<td  style='width:1%; min-width: 15px;'></td>" +
									"</tr>" +
								"</tbody>" +
							"</table>" +
						"</div>" +
					"</dl>");	
			
			document.getElementById("tipoModal").className="modal1";
			
			if(sumFil==tam){
				borra=0;
			}
		}
	});
	
}

function redondeo(numero, decimales){
	var flotante = parseFloat(numero);
	var resultado = Math.round(flotante*Math.pow(10,decimales))/Math.pow(10,decimales);
	return resultado;
}

function menuTiempoReal(){
	
	if(banTiempo == 0){
		$("#idMenuTiempoReal").show();
		banTiempo = 1;
	}else{
		menuTiempoRealDesaparece();
	}
}

function menuTiempoRealDesaparece(){
	
	$("#idMenuTiempoReal").hide();
	banTiempo = 0;
	
}

function regresar(){
	
	clearInterval(intervaloRegion);
   	intervaloZona = setInterval("reporte()",600000);
   	activoRegion=0;
   	
	aa=0;
	bb=0;
	cc=0;

	$("#idGeneralTablaTerritorios").show();
	$("#idGeneralTablaHijo").hide();
	$("#idBtnReg").hide();
	$("#idGeneralTablaNombreTitulo").show();
	$("#idGeneralTablaNombreTituloHijo").hide();
}

function regresarNieto(){
	
	clearInterval(intervaloSucursal);
   	intervaloRegion = setInterval("despliegaHijo("+idCecoRegion+",'"+nomCecoRegion+"')",600000);
   	activoRegion=1;
   	
   	activoSucursal=0;
	
	aa=0;
	bb=0;
	cc=0;

	$("#idGeneralTablaHijo").show();
	$("#idGeneralTablaNieto").hide();
	$("#idBtnReg").show();
	$("#idBtnRegNieto").hide();
	$("#idGeneralTablaNombreTituloHijo").show();
	$("#idGeneralTablaNombreTituloNieto").hide();
	
}

function despliegaPadre(){
	
	intervaloZona = setInterval("reporte()",600000);
	
	$.ajax({
		type : "GET",
		url : '/checklist/reportes/ajaxBuscaCecosPadres.json',
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			if (json == null) {
				document.getElementById("tipoModal").className="modal1";
			}else{	
				if(Object.keys(json).length==0){
					//alert("No hay Zonas")
					document.getElementById("tipoModal").className="modal1";
				}else{
					for (var a = 0; a < Object.keys(json).length; a++){
						pPersonales(a,json[a].idCeco,json[a].descCeco,Object.keys(json).length);
					}
				}
			} 	
		},error : function() {
			//alert('Favor de actualizar la pagina');
			document.getElementById("tipoModal").className="modal1";
		}
	});
}

function despliegaHijo(idCeco, nomCeco){
	
	clearInterval(intervaloZona);
	if(activoRegion==0){
	   	intervaloRegion = setInterval("despliegaHijo("+idCeco+",'"+nomCeco+"')",600000);
	   	activoRegion=1;
    }
	
	idCecoRegion = idCeco;
	nomCecoRegion = nomCeco;
	 
	 
	//if(validaCheck!=idCeco){

		$("#nomAscendenteHijo").hide();
		$("#nomDescendenteHijo").hide();
		$("#presAscendenteHijo").show();
		$("#presDescendenteHijo").hide();
		$("#consAscendenteHijo").hide();
		$("#consDescendenteHijo").hide();
		
		$("#nomVistaHijo").show();
		$("#presVistaHijo").hide();
		$("#consVistaHijo").show();
		
		matrizAscNomHijo = [];
		matrizDescNomHijo = [];
		matrizAscPresHijo = [];
		matrizDescPresHijo = [];
		matrizAscConsHijo = [];
		matrizDescConsHijo = [];
		
		aa=0;
		bb=0;
		cc=0;
		gg=0;
		hh=0;
		ii=0;
		
		nomZona=nomCeco;
		
		document.getElementById("nomTotalHijo").innerHTML = "";
		$("#nomTotalHijo").append(""+nomCeco);
		// $("#idGeneralTablaTerritorios").hide();
		$("#idGeneralTablaHijo").show();
		$("#idGeneralTablaTerritorios").hide();
		// $("#idBtnRegNieto").hide();
		$("#idBtnReg").show();
		$("#idBtnRegNieto").hide();
		// $("#idGeneralTablaNombreTitulo").hide();
		$("#idGeneralTablaNombreTituloHijo").show();
		$("#idGeneralTablaNombreTitulo").hide();
		
		document.getElementById("tipoModal").className="modal3";
		

		validaCheck=idCeco;
		document.getElementById("idGeneralTablaHijo").innerHTML = "";

		$.ajax({
			type : "GET",
			url : '/checklist/reportes/ajaxBuscaCecosPA.json?idCeco='+idCeco,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {
				if (json == null) {
					document.getElementById("tipoModal").className="modal1";
				}else{	
					if(Object.keys(json).length==0){
						//alert("No hay Regiones")
						document.getElementById("tipoModal").className="modal1";
					}else{
						for (var a = 0; a < Object.keys(json).length; a++) {								
							pPersonalesHijo(a,json[a].idCeco,json[a].descCeco,Object.keys(json).length);							
						}
					}
				} 	
			},error : function() {
				//alert('Favor de actualizar la pagina');
				document.getElementById("tipoModal").className="modal1";
			}
		});
	/*}else{
		document.getElementById("nomTotalHijo").innerHTML = "";
		$("#nomTotalHijo").append(""+nomCeco);
		// $("#idGeneralTablaTerritorios").hide();
		$("#idGeneralTablaHijo").show();
		$("#idGeneralTablaTerritorios").hide();
		// $("#idBtnRegNieto").hide();
		$("#idBtnReg").show();
		$("#idBtnRegNieto").hide();
		// $("#idGeneralTablaNombreTitulo").hide();
		$("#idGeneralTablaNombreTituloHijo").show();
		$("#idGeneralTablaNombreTitulo").hide();
		if(document.getElementById("idGeneralTablaHijo").innerHTML=="")
			alert("No hay regiones")
	}*/
}

function despliegaNieto(idCeco, nomCeco){
	
	clearInterval(intervaloRegion);
   	if(activoSucursal==0){
   		intervaloSucursal = setInterval("despliegaNieto("+idCeco+",'"+nomCeco+"')",600000);
   	   	activoSucursal=1;
   	}
   	
	//if(validaCheck!=idCeco){ 
		
		$("#nomAscendenteNieto").hide();
		$("#nomDescendenteNieto").hide();
		$("#presAscendenteNieto").show();
		$("#presDescendenteNieto").hide();
		$("#consAscendenteNieto").hide();
		$("#consDescendenteNieto").hide();
		
		$("#nomVistaNieto").show();
		$("#presVistaNieto").hide();
		$("#consVistaNieto").show();
		
		matrizAscNomNieto = [];
		matrizDescNomNieto = [];
		matrizAscPresNieto = [];
		matrizDescPresNieto = [];
		matrizAscConsNieto = [];
		matrizDescConsNieto = [];
		
		aa=0;
		bb=0;
		cc=0;
		jj=0;
		kk=0;
		ll=0;
		
		document.getElementById("nomTotalNieto").innerHTML = "";
		$("#nomTotalNieto").append(""+nomCeco);
		// $("#idGeneralTablaHijo").hide();
		$("#idGeneralTablaNieto").show();
		$("#idGeneralTablaHijo").hide();
		// $("#idBtnReg").hide();
		$("#idBtnRegNieto").show();
		$("#idBtnReg").hide();
		// $("#idGeneralTablaNombreTituloHijo").hide();
		$("#idGeneralTablaNombreTituloNieto").show();
		$("#idGeneralTablaNombreTituloHijo").hide();
		
		document.getElementById("tipoModal").className="modal3";
		
		validaCheck=idCeco;
		document.getElementById("idGeneralTablaNieto").innerHTML = "";

		$.ajax({
			type : "GET",
			url : '/checklist/reportes/ajaxBuscaCecosPA.json?idCeco='+idCeco,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {
				
				if (json == null) {
					// alert('Sin Datos');
					// $("#idGeneralTablaHijo").append("<span class='Gris1'>Sin
					// Datos</span>");
					document.getElementById("tipoModal").className="modal1";
				}else{					

					if(Object.keys(json).length==0){
						
						document.getElementById("preCTotalNieto").innerHTML = "";
						document.getElementById("preRTotalNieto").innerHTML = "";
						document.getElementById("prePTotalNieto").innerHTML = "";
						document.getElementById("conCTotalNieto").innerHTML = "";
						document.getElementById("conRTotalNieto").innerHTML = "";
						document.getElementById("conPTotalNieto").innerHTML = "";
					
						$("#preCTotalNieto").append("<p>-</p>");
						$("#preRTotalNieto").append("<p>-</p>");
						$("#prePTotalNieto").append("<div class='radio grisN'><b class='colorB'>-</b></div>");
						
						$("#conCTotalNieto").append("<p>-</p>");
						$("#conRTotalNieto").append("<p>-</p>");
						$("#conPTotalNieto").append("<div class='radio grisN'><b class='colorB'>-</b></div>");
						
						//alert("No hay sucursales")
						document.getElementById("tipoModal").className="modal1";
						
					}else{
						for (var a = 0; a < Object.keys(json).length; a++) {	
							
							pPersonalesNieto(a,json[a].idCeco,json[a].descCeco,Object.keys(json).length);
							
						}
					}
				} 					
			},error : function() {
				//alert('Favor de actualizar la pagina');
				document.getElementById("tipoModal").className="modal1";
			}
		});
	/*}else{
		document.getElementById("nomTotalNieto").innerHTML = "";
		$("#nomTotalNieto").append(""+nomCeco);
		// $("#idGeneralTablaHijo").hide();
		$("#idGeneralTablaNieto").show();
		$("#idGeneralTablaHijo").hide();
		// $("#idBtnReg").hide();
		$("#idBtnRegNieto").show();
		$("#idBtnReg").hide();
		// $("#idGeneralTablaNombreTituloHijo").hide();
		$("#idGeneralTablaNombreTituloNieto").show();
		$("#idGeneralTablaNombreTituloHijo").hide();
		
		if(document.getElementById("idGeneralTablaNieto").innerHTML==""){
			
			document.getElementById("preCTotalNieto").innerHTML = "";
			document.getElementById("preRTotalNieto").innerHTML = "";
			document.getElementById("prePTotalNieto").innerHTML = "";
			document.getElementById("conCTotalNieto").innerHTML = "";
			document.getElementById("conRTotalNieto").innerHTML = "";
			document.getElementById("conPTotalNieto").innerHTML = "";
		
			$("#preCTotalNieto").append("<p>-</p>");
			$("#preRTotalNieto").append("<p>-</p>");
			$("#prePTotalNieto").append("<div class='radio grisN'><b class='colorB'>-</b></div>");
			
			$("#conCTotalNieto").append("<p>-</p>");
			$("#conRTotalNieto").append("<p>-</p>");
			$("#conPTotalNieto").append("<div class='radio grisN'><b class='colorB'>-</b></div>");
			
			alert("No hay sucursales")
		}	
	}*/
}


function metOrdPadAscNom(matriz){
	
	var flag = false;
	var l =1;
	var sumaAct;
	var sumaAnt;
	var act;
	var ant;
	
	while(l < matriz.length){

		sumaAnt=matriz[l-1].descCeco;
		sumaAct=matriz[l].descCeco;

		if(sumaAnt < sumaAct){
			ant=matriz[l-1];
			act=matriz[l];
			matriz[l - 1] = act;
			matriz[l] = ant;
			flag=true;
		}

		if (flag && (l + 1) == matriz.length) {
			l = 0;
			flag = false;
		}
		l++;
	}
	matrizAscNom=matriz;
	for (var i = matrizAscNom.length - 1; i >= 0 ; i--) {
		matrizDescNom.push(matrizAscNom[i]);
	}
	
	return matriz;
}

function metOrdPadAscPre(matriz){
	
	var flag = false;
	var l =1;
	var sumaAct;
	var sumaAnt;
	var act;
	var ant;
	
	while(l < matriz.length){

		sumaAnt=matriz[l-1].valorPersonales;
		sumaAct=matriz[l].valorPersonales;

		if(sumaAnt < sumaAct){
			ant=matriz[l-1];
			act=matriz[l];
			matriz[l - 1] = act;
			matriz[l] = ant;
			flag=true;
		}

		if (flag && (l + 1) == matriz.length) {
			l = 0;
			flag = false;
		}
		l++;
	}
	matrizAscPres=matriz;
	for (var i = matrizAscPres.length - 1; i >= 0 ; i--) {
		matrizDescPres.push(matrizAscPres[i]);
	}
	
	return matriz;
}

function metOrdPadAscCon(matriz){
	
	var flag = false;
	var l =1;
	var sumaAct;
	var sumaAnt;
	var act;
	var ant;
	
	while(l < matriz.length){

		sumaAnt=parseInt(matriz[l-1].valorConsumo);
		sumaAct=parseInt(matriz[l].valorConsumo);

		if(sumaAnt < sumaAct){
			ant=matriz[l-1];
			act=matriz[l];
			matriz[l - 1] = act;
			matriz[l] = ant;
			flag=true;
		}

		if (flag && (l + 1) == matriz.length) {
			l = 0;
			flag = false;
		}
		l++;
	}
	matrizAscCons=matriz;
	for (var i = matrizAscCons.length - 1; i >= 0 ; i--) {
		matrizDescCons.push(matrizAscCons[i]);
	}
	
	return matriz;
}


function metOrdPadAscNomHijo(matriz){
	
	var flag = false;
	var l =1;
	var sumaAct;
	var sumaAnt;
	var act;
	var ant;
	
	while(l < matriz.length){

		sumaAnt=matriz[l-1].descCeco;
		sumaAct=matriz[l].descCeco;

		if(sumaAnt < sumaAct){
			ant=matriz[l-1];
			act=matriz[l];
			matriz[l - 1] = act;
			matriz[l] = ant;
			flag=true;
		}

		if (flag && (l + 1) == matriz.length) {
			l = 0;
			flag = false;
		}
		l++;
	}
	matrizAscNomHijo=matriz;
	for (var i = matrizAscNomHijo.length - 1; i >= 0 ; i--) {
		matrizDescNomHijo.push(matrizAscNomHijo[i]);
	}
	
	return matriz;
}

function metOrdPadAscPreHijo(matriz){
	
	var flag = false;
	var l =1;
	var sumaAct;
	var sumaAnt;
	var act;
	var ant;
	
	while(l < matriz.length){

		sumaAnt=matriz[l-1].valorPersonales;
		sumaAct=matriz[l].valorPersonales;

		if(sumaAnt < sumaAct){
			ant=matriz[l-1];
			act=matriz[l];
			matriz[l - 1] = act;
			matriz[l] = ant;
			flag=true;
		}

		if (flag && (l + 1) == matriz.length) {
			l = 0;
			flag = false;
		}
		l++;
	}
	matrizAscPresHijo=matriz;
	for (var i = matrizAscPresHijo.length - 1; i >= 0 ; i--) {
		matrizDescPresHijo.push(matrizAscPresHijo[i]);
	}
	
	return matriz;
}

function metOrdPadAscConHijo(matriz){
	
	var flag = false;
	var l =1;
	var sumaAct;
	var sumaAnt;
	var act;
	var ant;
	
	while(l < matriz.length){

		sumaAnt=parseInt(matriz[l-1].valorConsumo);
		sumaAct=parseInt(matriz[l].valorConsumo);

		if(sumaAnt < sumaAct){
			ant=matriz[l-1];
			act=matriz[l];
			matriz[l - 1] = act;
			matriz[l] = ant;
			flag=true;
		}

		if (flag && (l + 1) == matriz.length) {
			l = 0;
			flag = false;
		}
		l++;
	}
	matrizAscConsHijo=matriz;
	for (var i = matrizAscConsHijo.length - 1; i >= 0 ; i--) {
		matrizDescConsHijo.push(matrizAscConsHijo[i]);
	}
	
	return matriz;
}


function metOrdPadAscNomNieto(matriz){
	
	var flag = false;
	var l =1;
	var sumaAct;
	var sumaAnt;
	var act;
	var ant;
	
	while(l < matriz.length){

		sumaAnt=matriz[l-1].descCeco;
		sumaAct=matriz[l].descCeco;

		if(sumaAnt < sumaAct){
			ant=matriz[l-1];
			act=matriz[l];
			matriz[l - 1] = act;
			matriz[l] = ant;
			flag=true;
		}

		if (flag && (l + 1) == matriz.length) {
			l = 0;
			flag = false;
		}
		l++;
	}
	matrizAscNomNieto=matriz;
	for (var i = matrizAscNomNieto.length - 1; i >= 0 ; i--) {
		matrizDescNomNieto.push(matrizAscNomNieto[i]);
	}
	
	return matriz;
}

function metOrdPadAscPreNieto(matriz){
	
	var flag = false;
	var l =1;
	var sumaAct;
	var sumaAnt;
	var act;
	var ant;
	
	while(l < matriz.length){

		sumaAnt=matriz[l-1].valorPersonales;
		sumaAct=matriz[l].valorPersonales;

		if(sumaAnt < sumaAct){
			ant=matriz[l-1];
			act=matriz[l];
			matriz[l - 1] = act;
			matriz[l] = ant;
			flag=true;
		}

		if (flag && (l + 1) == matriz.length) {
			l = 0;
			flag = false;
		}
		l++;
	}
	matrizAscPresNieto=matriz;
	for (var i = matrizAscPresNieto.length - 1; i >= 0 ; i--) {
		matrizDescPresNieto.push(matrizAscPresNieto[i]);
	}
	
	return matriz;
}

function metOrdPadAscConNieto(matriz){
	
	var flag = false;
	var l =1;
	var sumaAct;
	var sumaAnt;
	var act;
	var ant;
	
	while(l < matriz.length){

		sumaAnt=parseInt(matriz[l-1].valorConsumo);
		sumaAct=parseInt(matriz[l].valorConsumo);

		if(sumaAnt < sumaAct){
			ant=matriz[l-1];
			act=matriz[l];
			matriz[l - 1] = act;
			matriz[l] = ant;
			flag=true;
		}

		if (flag && (l + 1) == matriz.length) {
			l = 0;
			flag = false;
		}
		l++;
	}
	matrizAscConsNieto=matriz;
	for (var i = matrizAscConsNieto.length - 1; i >= 0 ; i--) {
		matrizDescConsNieto.push(matrizAscConsNieto[i]);
	}
	
	return matriz;
}


function pintaPadre(matriz){
	
	for (var a = 0; a < matriz.length; a++) {	
		
		$("#idGeneralTablaTerritorios").append("" +
			"<dl class='accordion3'>" +
				"<div class='divDT' onclick='despliegaHijo("+matriz[a].idCeco+",&apos;"+matriz[a].descCeco+"&apos;)' style='background: #dbdbdb33;'>" +
					"<table class='bloquer linea goshadow1'>" +
						"<tbody>" +
							"<tr class='pointer'>" +
								"<td id='nom0"+a+"' class='nombreCheck' style='background-image:none;'><ab>"+matriz[a].descCeco+"</ab></td>" +
								
								"<td  style='width:1%; min-width: 15px;'></td>" +
								
								"<td id='preC0"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><p>$"+new Intl.NumberFormat('es-MX').format(matriz[a].compromisoPersonales)+"</p></td>" +
								"<td id='preR0"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><p>$"+new Intl.NumberFormat('es-MX').format(matriz[a].avanceTiempoRealPersonales)+"</p></td>" +
								"<td id='preP0"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><div class='radio grisN'><bc class='colorN'>"+matriz[a].valorPersonales+"%</bc></div></td>" +
								
								"<td  style='width:1%; min-width: 15px;'></td>" +
								"<td  style='width:0.1%; background: #f2f2f2;'></td>" +
								
								"<td id='conC0"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><p>$"+new Intl.NumberFormat('es-MX').format(matriz[a].compromisoConsumo)+"</p></td>" +
								"<td id='conR0"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><p>$"+new Intl.NumberFormat('es-MX').format(matriz[a].avanceTiempoRealConsumo)+"</p></td>" +
								"<td id='conP0"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><div class='radio grisN'><cb class='colorN'>"+matriz[a].valorConsumo+"%</cb></div></td>" +
								"<td  style='width:1%; min-width: 15px;'></td>" +
							"</tr>" +
						"</tbody>" +
					"</table>" +
				"</div>" +
			"</dl>");							
	}
}

function pintaHijo(matriz){
	
	for (var a = 0; a < matriz.length; a++) {	
		
		$("#idGeneralTablaHijo").append("" +
			"<dl class='accordion3'>" +
				"<div class='divDTHijo' onclick='despliegaNieto("+matriz[a].idCeco+",&apos;"+matriz[a].descCeco+"&apos;)' style='background: #dbdbdb33;'>" +
					"<table class='bloquer linea goshadow1'>" +
						"<tbody>" +
							"<tr class='pointer'>" +
								"<td id='nom1"+a+"' class='nombreCheck' style='background-image:none;'><ab>"+matriz[a].descCeco+"</ab></td>" +
								
								"<td  style='width:1%; min-width: 15px;'></td>" +
								
								"<td id='preC1"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><p>$"+new Intl.NumberFormat('es-MX').format(matriz[a].compromisoPersonales)+"</p></td>" +
								"<td id='preR1"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><p>$"+new Intl.NumberFormat('es-MX').format(matriz[a].avanceTiempoRealPersonales)+"</p></td>" +
								"<td id='preP1"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><div class='radio grisN'><bc class='colorN'>"+matriz[a].valorPersonales+"%</bc></div></td>" +
								
								"<td  style='width:1%; min-width: 15px;'></td>" +
								"<td  style='width:0.1%; background: #f2f2f2;'></td>" +
								
								"<td id='conC1"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><p>$"+new Intl.NumberFormat('es-MX').format(matriz[a].compromisoConsumo)+"</p></td>" +
								"<td id='conR1"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><p>$"+new Intl.NumberFormat('es-MX').format(matriz[a].avanceTiempoRealConsumo)+"</p></td>" +
								"<td id='conP1"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><div class='radio grisN'><cb class='colorN'>"+matriz[a].valorConsumo+"%</cb></div></td>" +
								"<td  style='width:1%; min-width: 15px;'></td>" +
							"</tr>" +
						"</tbody>" +
					"</table>" +
				"</div>" +
			"</dl>");							
	}
}

function pintaNieto(matriz){
	
	for (var a = 0; a < matriz.length; a++) {	
		
		$("#idGeneralTablaNieto").append("" +
			"<dl class='accordion3'>" +
				"<div class='divDTNieto' style='background: #dbdbdb33;'>" +
					"<table class='bloquer linea goshadow1'>" +
						"<tbody>" +
							"<tr>" +
								"<td id='nom2"+a+"' class='nombreCheck' style='background-image:none;'><ab>"+matriz[a].descCeco+"</ab></td>" +
								
								"<td  style='width:1%; min-width: 15px;'></td>" +
								
								"<td id='preC2"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><p>$"+new Intl.NumberFormat('es-MX').format(matriz[a].compromisoPersonales)+"</p></td>" +
								"<td id='preR2"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><p>$"+new Intl.NumberFormat('es-MX').format(matriz[a].avanceTiempoRealPersonales)+"</p></td>" +
								"<td id='preP2"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><div class='radio grisN'><bc class='colorN'>"+matriz[a].valorPersonales+"%</bc></div></td>" +
								
								"<td  style='width:1%; min-width: 15px;'></td>" +
								"<td  style='width:0.1%; background: #f2f2f2;'></td>" +
								
								"<td id='conC2"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><p>$"+new Intl.NumberFormat('es-MX').format(matriz[a].compromisoConsumo)+"</p></td>" +
								"<td id='conR2"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><p>$"+new Intl.NumberFormat('es-MX').format(matriz[a].avanceTiempoRealConsumo)+"</p></td>" +
								"<td id='conP2"+a+"' class='nombreCheck' style='width: "+(70/6)+"%;'><div class='radio grisN'><cb class='colorN'>"+matriz[a].valorConsumo+"%</cb></div></td>" +
								"<td  style='width:1%; min-width: 15px;'></td>" +
							"</tr>" +
						"</tbody>" +
					"</table>" +
				"</div>" +
			"</dl>");							
	}
}