package com.gruposalinas.checklist.business;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.gruposalinas.checklist.domain.ParametroDTO;
import com.gruposalinas.checklist.domain.SchedulerDTO;
import com.gruposalinas.checklist.domain.TareaDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import com.gruposalinas.checklist.resources.FRQConstantes;

@Component
public class SchedulerBI implements Job {

    @Autowired
    TareasBI tareasBI;
    private static final Logger logger = LogManager.getLogger(SchedulerBI.class);
    private static SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
    private static Map<String, Scheduler> schedArray = new LinkedHashMap<String, Scheduler>();
    private static Map<String, JobDetail> jobDetailArray = new LinkedHashMap<String, JobDetail>();
    private static int aut = 0;

    public void execute(JobExecutionContext cntxt) throws JobExecutionException {

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("eliminaToken")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO eliminaToken... ");
            try {
                TokenBI tokenBI = (TokenBI) FRQAppContextProvider.getApplicationContext().getBean("tokenBI");
                logger.info("Token... " + tokenBI.eliminaTokenTodos());
            } catch (Exception e) {
                logger.info("Algo Ocurrió: execute");
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaCecos")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO cargaCecos... ");
            try {
                ParametroBI paramBI = (ParametroBI) FRQAppContextProvider.getApplicationContext()
                        .getBean("parametroBI");
                int paramTotal = Integer.parseInt(paramBI.obtieneParametros("numCargas").get(0).getValor());

                CecoBI cecoBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
                logger.info("Elimina Duplicados... " + cecoBI.eliminaDuplicados());

                for (int j = 1; j <= paramTotal; j++) {
                    ParametroDTO parametroDTO = new ParametroDTO();
                    parametroDTO.setClave("tipoCarga");
                    parametroDTO.setValor("" + j);
                    parametroDTO.setActivo(1);
                    ParametroBI parametroBI = (ParametroBI) FRQAppContextProvider.getApplicationContext()
                            .getBean("parametroBI");
                    logger.info("Actualiza Parametro... " + parametroBI.actualizaParametro(parametroDTO));

                    cecoBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");

                    logger.info("Carga Ceco... " + cecoBI.cargaCecos());
                    logger.info("Carga Ceco 2... " + cecoBI.cargaCecos2());
                }
                CecoBI cecoActBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
                logger.info("Actualiza Cecos... " + cecoActBI.updateCecos());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaUsuarios")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO cargaUsuarios... ");
            try {
                Usuario_ABI usuario_ABI = (Usuario_ABI) FRQAppContextProvider.getApplicationContext()
                        .getBean("usuario_ABI");
                logger.info("Carga Usuarios... " + usuario_ABI.cargaUsuarios());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaSucursales")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO cargaSucursales... ");
            try {
                SucursalBI sucursalBI = (SucursalBI) FRQAppContextProvider.getApplicationContext().getBean("sucursalBI");
                logger.info("Carga Sucursales... " + sucursalBI.cargaSucursales());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaCecosLAM")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO cargaCecosLAM... ");
            try {
                CecoBI cecoBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
                logger.info("Carga Cecos LAM... " + cecoBI.cargaCecosLAM());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("test")) {
            //logger.info("... EJECUTO SCHEDULER TEST ... ");
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("ejecutaAsignaciones")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO ejecutaAsignaciones... ");
            try {
                AsignacionNewBI asignacionNewBI = (AsignacionNewBI) FRQAppContextProvider.getApplicationContext().getBean("asignacionNewBI");
                logger.info("AsignacionBI... " + asignacionNewBI.ejecutaAsignaciones());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaGeografia")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO cargaGeografia... ");
            try {
                CecoBI cecoBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
                logger.info("Carga Geografia... " + cecoBI.cargaGeografia());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("notificacionRegionalAndroid")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO notificacionRegionalAndroid... ");
            try {
                NotificacionesBI notificacion = (NotificacionesBI) FRQAppContextProvider.getApplicationContext().getBean("notificacionesBI");
                logger.info("Enviando notificaciones... " + notificacion.enviarReporteAndroid());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("notificacionRegionalIOS")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO notificacionRegionalIOS... ");
            try {
                NotificacionesBI notificacion = (NotificacionesBI) FRQAppContextProvider.getApplicationContext().getBean("notificacionesBI");
                logger.info("Enviando notificaciones... " + notificacion.enviarReporteIOS());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("eliminaRespuestasDuplicadas08")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO eliminaRespuestasDuplicadas... ");
            try {
                RespuestaBI respuesta = (RespuestaBI) FRQAppContextProvider.getApplicationContext().getBean("respuestaBI");
                logger.info("Elimina Respuestas... " + respuesta.eliminaRespuestasDuplicadas());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("eliminaRespuestasDuplicadas15")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO eliminaRespuestasDuplicadas... ");
            try {
                RespuestaBI respuesta = (RespuestaBI) FRQAppContextProvider.getApplicationContext().getBean("respuestaBI");
                logger.info("Elimina Respuestas... " + respuesta.eliminaRespuestasDuplicadas());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("correoBunker")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO correoBunker... ");
            try {
                EnviaCorreoVisitasBI respuesta = (EnviaCorreoVisitasBI) FRQAppContextProvider.getApplicationContext().getBean("enviaCorreoVisitasBI");
                logger.info("Correo Bunker... " + respuesta.enviaCorre());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("actualizaCecoGuadalajara")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO actualizaCecoGuadalajara... ");
            try {
                CecoGuadalajaraBI cecoGuadalajaraBI = (CecoGuadalajaraBI) FRQAppContextProvider.getApplicationContext().getBean("cecoGuadalajaraBI");
                logger.info("Carga Cecos LAM... " + cecoGuadalajaraBI.actualizaCecoGuadalajara());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("asignacionPerfilesAuto")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO notificacionRegionalAndroid... ");
            try {
                PerfilesNuevoBI asignacion = (PerfilesNuevoBI) FRQAppContextProvider.getApplicationContext().getBean("perfilesNuevoBI");
                logger.info("EJECUCION DE LAS ASIGNACIONES... " + asignacion.asignacionAutomaticaPerfiles());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("asignacionPerfilesSistemas")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO notificacion perfilSistemas... ");
            try {
                PerfilesNuevoBI asignacion = (PerfilesNuevoBI) FRQAppContextProvider.getApplicationContext().getBean("perfilesNuevoBI");
                logger.info("EJECUCION DE LAS ASIGNACIONES... " + asignacion.altaPerfilSistemas());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("asignacionPerfilesScouting")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO notificacion PerfilScouting.... ");
            try {
                PerfilesNuevoBI asignacion = (PerfilesNuevoBI) FRQAppContextProvider.getApplicationContext().getBean("perfilesNuevoBI");
                logger.info("EJECUCION DE LAS ASIGNACIONES... " + asignacion.altaPerfilSCouting());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("asignacionPerfilesPrestaPrenda")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO notificacion perfil PrestaPrenda... ");
            try {
                PerfilesNuevoBI asignacion = (PerfilesNuevoBI) FRQAppContextProvider.getApplicationContext().getBean("perfilesNuevoBI");
                logger.info("EJECUCION DE LAS ASIGNACIONES... " + asignacion.altaPerfilPrestaPrenda());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        //---------------Ejecuta asignaciones de gerentes y subgerentes de carpeta maestra-----------------
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("altaCheckList")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO alta CheckList gerentes y subgerentes carpeta maestra... ");
            try {
                CargaCheckListBI cargaAsignacionBI = (CargaCheckListBI) FRQAppContextProvider.getApplicationContext().getBean("cargaCheckListBI");
                logger.info("Carga asignaciones gerentes... " + cargaAsignacionBI.insertaCheckList());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        //--------------------------------------------------------------------------------------------------
        //---------------Elimina asignaciones de gerentes y subgerentes de carpeta maestra cambio ceco-----------------
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("eliminaCheckList")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO baja de CheckList gerentes y subgerentes carpeta maestra por cambio de ceco... ");
            try {
                EliminaCheckListBI eliminaCheckListBI = (EliminaCheckListBI) FRQAppContextProvider.getApplicationContext().getBean("eliminaCheckListBI");
                logger.info("Desactiva asignaciones gerentes... " + eliminaCheckListBI.eliminaCheckList());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        //--------------------------------------------------------------------------------------------------
        //------------------------------------- Depura la tabla de token -----------------------------------
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("truncTableTokenBI")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO truncar tabla de tokens... ");
            try {
                TruncTableTokenBI truncTableTokenBI = (TruncTableTokenBI) FRQAppContextProvider.getApplicationContext().getBean("truncTableTokenBI");
                logger.info("Trunca tabla token... " + truncTableTokenBI.truncTable());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        //--------------------------------------------------------------------------------------------------
        //------------------------------------- Carga ubicacion de sucursales -----------------------------------
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("sucUbicacionBI")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO para cargar las ubicacione sucursales... ");
            try {
                SucUbicacionBI sucUbicacionBI = (SucUbicacionBI) FRQAppContextProvider.getApplicationContext().getBean("sucUbicacionBI");
                logger.info("Carga ubicaciones de sucursales... " + sucUbicacionBI.cargaSucUbic());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        //--------------------------------------------------------------------------------------------------
        //------------------------------------- Administrador de Protocolos -----------------------------------
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("checklistAdminBI")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO para cargar las ubicacione sucursales... ");
            try {
                ChecklistAdminBI checklistAdminBI = (ChecklistAdminBI) FRQAppContextProvider.getApplicationContext().getBean("checklistAdminBI");
                logger.info("Carga protocolos temporales a produccion... " + checklistAdminBI.ejecutaCargaProd());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        //--------------------------------------------------------------------------------------------------
        //------------------------------------------------------ Cargar Ceco General------------------------------
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("CargaGeneralCecos")) {
            //logger.info("...EJECUTO EL PROCEDIMIENTO cecoWSPaso... ");
            try {
                ObtieneCecos obtieneCecos = (ObtieneCecos) FRQAppContextProvider.getApplicationContext().getBean("obtieneCecos");
                logger.info("Carga cecos general... " + obtieneCecos.ejecutaProcesosCecosGeneral());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        //------------------------------------------------------ Cargar Ceco Tabla de paso a tabla de trabajo------------------------------
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaCecosRedUnica")) {
            try {
                ObtieneCecos obtieneCecos = (ObtieneCecos) FRQAppContextProvider.getApplicationContext().getBean("obtieneCecos");
                logger.info("cargaCecosRedUnica... " + obtieneCecos.cargaTablaPasoATablaTrabajoCeco());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... cargaCecosRedUnica ", e);
            }
        }
    }

    public void run() throws SchedulerException, UnknownHostException {
        String ip = InetAddress.getLocalHost().getHostAddress();
        logger.info("IP HOST...... " + ip);
        //if (aut == 0 && ip.equals("10.53.29.153")) {
        if (aut == 0 && ip.equals(FRQConstantes.getIpHost())) {
            CronTrigger trigger = null;
            List<TareaDTO> listaTareas = null;
            try {
                listaTareas = tareasBI.obtieneTareas();
                Scheduler sched = null;
                JobDetail jobDetail = new JobDetail();
                Iterator<TareaDTO> it = listaTareas.iterator();
                while (it.hasNext()) {
                    /* INICIA EL SCHEDULER */
                    TareaDTO o = it.next();
                    if (o.getActivo() == 1) {
                        sched = schedFact.getScheduler();
                        jobDetail = new JobDetail("Job" + o.getCveTarea(), "Ejemplo", SchedulerBI.class);
                        jobDetail.getJobDataMap().put("type", o.getCveTarea());
                        trigger = new CronTrigger("trigger" + o.getCveTarea(), "Trigger");
                        trigger.setCronExpression("*/10 * * * * ?");
                        sched.scheduleJob(jobDetail, trigger);
                        sched.start();
                        /* MODIFICA EL SCHEDULER CON LOS DATOS DE LA BD */
                        sched.unscheduleJob("trigger" + o.getCveTarea(), "Trigger");
                        sched = schedFact.getScheduler();
                        trigger = new CronTrigger("trigger" + o.getCveTarea(), "Trigger");
                        trigger.setCronExpression(o.getStrFechaTarea());
                        sched.scheduleJob(jobDetail, trigger);
                        sched.start();
                    } else if (o.getActivo() == 0) {
                        sched = schedFact.getScheduler();
                        jobDetail = new JobDetail("Job" + o.getCveTarea(), "Ejemplo", SchedulerBI.class);
                        jobDetail.getJobDataMap().put("type", o.getCveTarea());
                        trigger = new CronTrigger("trigger" + o.getCveTarea(), "Trigger");
                        trigger.setCronExpression("*/10 * * * * ?");
                        /* MODIFICA EL SCHEDULER CON LOS DATOS DE LA BD */
                        sched.unscheduleJob("trigger" + o.getCveTarea(), "Trigger");
                        sched = schedFact.getScheduler();
                        trigger = new CronTrigger("trigger" + o.getCveTarea(), "Trigger");
                        trigger.setCronExpression(o.getStrFechaTarea());
                    }
                    schedArray.put(o.getCveTarea(), sched);
                    jobDetailArray.put(o.getCveTarea(), jobDetail);
                }
            } catch (ParseException e) {
                logger.info("Algo Ocurrió run()");
            }
        }
        aut++;
    }

    // AGREGA UNA TAREA
    public boolean newTask(SchedulerDTO scheduler, HttpServletRequest request)
            throws ParseException, UnknownHostException {

        String ip = InetAddress.getLocalHost().getHostAddress();
        logger.info("IP HOST...... " + ip);
        //if (ip.equals("10.53.29.153")) {
        if (ip.equals(FRQConstantes.getIpHost())) {
            try {
                //
                String date = "";
                String h = scheduler.getHora(), m = scheduler.getMinutos(), s = scheduler.getSegundos(),
                        d = scheduler.getDia(), month = scheduler.getMes(), diaSemana = scheduler.getDiaSemana();

                if (request.getParameter("h") != null && request.getParameter("h").equals("on")) {
                    h = "*/" + scheduler.getHora();
                }
                if (request.getParameter("m") != null && request.getParameter("m").equals("on")) {
                    m = "*/" + scheduler.getMinutos();
                }
                if (request.getParameter("s") != null && request.getParameter("s").equals("on")) {
                    s = "*/" + scheduler.getSegundos();
                }
                if (request.getParameter("d") != null && request.getParameter("d").equals("on")) {
                    d = "*/" + scheduler.getDia();
                }
                if (request.getParameter("month") != null && request.getParameter("month").equals("on")) {
                    month = "*/" + scheduler.getMes();
                }
                if (request.getParameter("diaSemana") != null && request.getParameter("diaSemana").equals("on")) {
                    month = "*/" + scheduler.getDiaSemana();
                }
                if (!request.getParameter("diaSemana").equals("?")) {
                    d = "?";
                }

                date = s + " " + m + " " + h + " " + d + " " + month + " " + diaSemana;

                TareaDTO tarea = new TareaDTO();
                tarea.setActivo(Integer.parseInt(request.getParameter("nTareaActiva")));
                tarea.setCveTarea(request.getParameter("nombreTarea"));// nombre
                tarea.setStrFechaTarea(date);

                CronTrigger trigger = null;
                @SuppressWarnings("unused")
                List<TareaDTO> listaTareas = null;
                try {
                    listaTareas = tareasBI.obtieneTareas();
                    Scheduler sched = null;
                    JobDetail jobDetail = new JobDetail();
                    if (tarea.getActivo() == 1) {
                        sched = schedFact.getScheduler();
                        jobDetail = new JobDetail("Job" + tarea.getCveTarea(), "Ejemplo", SchedulerBI.class);
                        jobDetail.getJobDataMap().put("type", tarea.getCveTarea());
                        trigger = new CronTrigger("trigger" + tarea.getCveTarea(), "Trigger");
                        trigger.setCronExpression(tarea.getStrFechaTarea());
                        sched.scheduleJob(jobDetail, trigger);
                        sched.start();
                    } else if (tarea.getActivo() == 0) {
                        sched = schedFact.getScheduler();
                        jobDetail = new JobDetail("Job" + tarea.getCveTarea(), "Ejemplo", SchedulerBI.class);
                        jobDetail.getJobDataMap().put("type", tarea.getCveTarea());
                        trigger = new CronTrigger("trigger" + tarea.getCveTarea(), "Trigger");
                        trigger.setCronExpression(tarea.getStrFechaTarea());
                    }
                    schedArray.put(tarea.getCveTarea(), sched);
                    jobDetailArray.put(tarea.getCveTarea(), jobDetail);
                } catch (SchedulerException e) {
                    logger.info("Algo Ocurrió... ", e);
                    return false;
                }
                try {
                    tareasBI.insertaTarea(tarea);
                } catch (Exception e) {
                    logger.info("Algo Ocurrió... ", e);
                    return false;
                }
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
                return false;
            }
        }
        return true;
    }

    // MODIFICA UNA TAREA
    public boolean task(SchedulerDTO scheduler, HttpServletRequest request)
            throws ParseException, UnknownHostException {

        String ip = InetAddress.getLocalHost().getHostAddress();
        logger.info("IP HOST...... " + ip);
        // if (ip.equals("10.53.29.153") ) {
        if (ip.equals(FRQConstantes.getIpHost())) {
            try {
                String date = "";
                String identifier = scheduler.getId();
                String h = scheduler.getHora(), m = scheduler.getMinutos(), s = scheduler.getSegundos(),
                        d = scheduler.getDia(), month = scheduler.getMes(), diaSemana = scheduler.getDiaSemana();

                if (request.getParameter("h") != null && request.getParameter("h").equals("on")) {
                    h = "*/" + scheduler.getHora();
                }
                if (request.getParameter("m") != null && request.getParameter("m").equals("on")) {
                    m = "*/" + scheduler.getMinutos();
                }
                if (request.getParameter("s") != null && request.getParameter("s").equals("on")) {
                    s = "*/" + scheduler.getSegundos();
                }
                if (request.getParameter("d") != null && request.getParameter("d").equals("on")) {
                    d = "*/" + scheduler.getDia();
                }
                if (request.getParameter("month") != null && request.getParameter("month").equals("on")) {
                    month = "*/" + scheduler.getMes();
                }
                if (request.getParameter("diaSemana") != null && request.getParameter("diaSemana").equals("on")) {
                    month = "*/" + scheduler.getDiaSemana();
                }
                if (!request.getParameter("diaSemana").equals("?")) {
                    d = "?";
                }

                date = s + " " + m + " " + h + " " + d + " " + month + " " + diaSemana;

                TareaDTO tarea = new TareaDTO();
                tarea.setActivo(Integer.parseInt(request.getParameter("act" + identifier)));

                if (Integer.parseInt(request.getParameter("act" + identifier)) == 0) {
                    tarea.setStrFechaTarea(request.getParameter("fecha" + identifier));
                } else {
                    tarea.setStrFechaTarea(date);
                }

                tarea.setCveTarea(request.getParameter("idTarea" + identifier));
                tarea.setIdTarea(Integer.parseInt(scheduler.getId()));

                try {
                    tareasBI.actualizaTarea(tarea);
                } catch (Exception e) {
                    logger.info("Algo Ocurrió...", e);
                    return false;
                }

                CronTrigger trigger = null;
                List<TareaDTO> listaTareas = null;
                try {
                    listaTareas = tareasBI.obtieneTareas();
                    Iterator<TareaDTO> it = listaTareas.iterator();
                    @SuppressWarnings("unused")
                    Scheduler sched = null;
                    while (it.hasNext()) {
                        TareaDTO o = it.next();
                        if (o.getIdTarea() == tarea.getIdTarea() && o.getActivo() == 1) {
                            schedArray.get(tarea.getCveTarea()).unscheduleJob("trigger" + tarea.getCveTarea(),
                                    "Trigger");
                            sched = schedFact.getScheduler();
                            trigger = new CronTrigger("trigger" + tarea.getCveTarea(), "Trigger");
                            trigger.setCronExpression(tarea.getStrFechaTarea());
                            schedArray.get(tarea.getCveTarea()).scheduleJob(jobDetailArray.get(tarea.getCveTarea()),
                                    trigger);
                            schedArray.get(tarea.getCveTarea()).start();
                        } else if (o.getIdTarea() == tarea.getIdTarea() && o.getActivo() == 0) {
                            schedArray.get(tarea.getCveTarea()).unscheduleJob("trigger" + tarea.getCveTarea(),
                                    "Trigger");
                        }
                    }
                } catch (SchedulerException e) {
                    logger.info("Algo Ocurrió...", e);
                    return false;
                }

            } catch (Exception e) {
                logger.info("Algo Ocurrió...", e);
                return false;
            }
        }
        return true;
    }

    // ELIMINA UNA TAREA
    public boolean eliminaTask(int idTarea, String cveTarea) throws ParseException, UnknownHostException {
        String ip = InetAddress.getLocalHost().getHostAddress();
        logger.info("IP HOST...... " + ip);
        // if (ip.equals("10.53.29.153") ) {
        if (ip.equals(FRQConstantes.getIpHost())) {
            List<TareaDTO> listaTareas = null;
            try {
                listaTareas = tareasBI.obtieneTareas();
                Iterator<TareaDTO> it = listaTareas.iterator();
                while (it.hasNext()) {
                    TareaDTO o = it.next();
                    if (o.getIdTarea() == idTarea) {
                        schedArray.get(cveTarea).unscheduleJob("trigger" + cveTarea, "Trigger");
                        schedArray.get(cveTarea).deleteJob("Job" + cveTarea, "Ejemplo");
                        schedArray.remove(cveTarea);
                        jobDetailArray.remove(cveTarea);
                    }
                }
                try {
                    tareasBI.eliminaTarea(idTarea);
                } catch (Exception e) {
                    logger.info("Algo Ocurrió... ", e);
                    return false;
                }
            } catch (SchedulerException e) {
                logger.info("Algo Ocurrió... ", e);
                return false;
            }
        }
        return true;
    }
}
