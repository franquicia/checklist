package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.mappers.BitacoraRowMapper;
import com.gruposalinas.checklist.mappers.BitacorasCerradasRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class BitacoraDAOImpl extends DefaultDAO implements BitacoraDAO {

    private static Logger logger = LogManager.getLogger(BitacoraDAOImpl.class);

    private DefaultJdbcCall insertaBitacora;
    private DefaultJdbcCall actualizaBitacora;
    private DefaultJdbcCall eliminaBitacora;
    private DefaultJdbcCall buscaBitacora;
    private DefaultJdbcCall buscaBitacoras;
    private DefaultJdbcCall verificadIdBitacora;
    private DefaultJdbcCall cierraBitacora;
    private DefaultJdbcCall cierreBitacoras;
    private DefaultJdbcCall cierreBitacorasR;

    public void init() {
        insertaBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_BITACORA")
                .withProcedureName("SP_INS_BITACORA");

        actualizaBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_BITACORA")
                .withProcedureName("SP_ACT_BITACORA_S");

        eliminaBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_BITACORA")
                .withProcedureName("SP_DEL_BITACORA");

        buscaBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_BITACORA")
                .withProcedureName("SP_SEL_BITACORA")
                .returningResultSet("RCL_BITACORA", new BitacoraRowMapper());

        buscaBitacoras = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_BITACORA")
                .withProcedureName("SP_SEL_G_BIT")
                .returningResultSet("RCL_BITACORA", new BitacoraRowMapper());

        verificadIdBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_BITACORA")
                .withProcedureName("SP_OBTIENE_ID");

        cierraBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_BITACORA")
                .withProcedureName("SPCIERREBITACORAS");

        cierreBitacoras = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_BITACORA")
                .withProcedureName("SP_OBTIENE_CERRADAS")
                .returningResultSet("RCL_BITACORA", new BitacorasCerradasRowMapper());

        cierreBitacorasR = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_BITACORA")
                .withProcedureName("SP_OBTIENE_CERRADAS_R")
                .returningResultSet("RCL_BITACORA", new BitacorasCerradasRowMapper());

    }

    public int insertaBitacora(BitacoraDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idBitacora = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_CHECK_USA", bean.getIdCheckUsua())
                .addValue("PA_FIMODO", bean.getModo());
        out = insertaBitacora.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_ADM_CECO.SP_INS_CECO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDBITACORA");
        idBitacora = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar Bitacora");
        }

        return idBitacora;
    }

    public boolean actualizaBitacora(BitacoraDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_CHECK_USA", bean.getIdCheckUsua())
                .addValue("PA_FCLONGITUD", bean.getLongitud())
                .addValue("PA_FCLATITUD", bean.getLatitud())
                .addValue("PA_FIID_BITACORA", bean.getIdBitacora())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_FIMODO", bean.getModo())
                .addValue("PA_FECHA_TERMINO", bean.getFechaFin());

        out = actualizaBitacora.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_ADM_CECO.SP_ACT_BITACORA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Bitacora");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaBitacora(int idBitacora) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_BITACORA", idBitacora);

        out = eliminaBitacora.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_ADM_CECO.SP_DEL_BITACORA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Bitacora");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<BitacoraDTO> buscaBitacora() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<BitacoraDTO> listBitacora = null;

        out = buscaBitacoras.execute();

        logger.info("Funcion ejecutada:{checklist.PA_ADM_CECO.SP_SEL_G_BIT}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listBitacora = (List<BitacoraDTO>) out.get("RCL_BITACORA");

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Bitacora");
            return null;
        } else {
            return listBitacora;
        }
    }

    @SuppressWarnings("unchecked")
    public List<BitacoraDTO> buscaBitacora(String checkUsua, String idBitacora, String fechaI, String fechaF) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<BitacoraDTO> listBitacora = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FID_CHECK_USUA", checkUsua)
                .addValue("PA_IDBITACORA", idBitacora)
                .addValue("PA_FECHA_I", fechaI)
                .addValue("PA_FECHA_F", fechaF);

        out = buscaBitacora.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_ADM_CECO.SP_SEL_BITACORA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listBitacora = (List<BitacoraDTO>) out.get("RCL_BITACORA");

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Bitacora");
        } else {
            return listBitacora;
        }

        return null;
    }

    public int verificaIdBitacora(int checkUsua) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idBitacora = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCHECKUSA", checkUsua);

        out = verificadIdBitacora.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_BITACORA.SP_OBTIENE_ID}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDBITACORA");
        idBitacora = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Bitacora");
        }

        return idBitacora;

    }

    @Override
    public boolean cierraBitacora() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = cierraBitacora.execute();

        logger.info("Funcion ejecutada:{checklist.PA_BITACORA.SPCIERREBITACORAS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al terminar las Bitacoras");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<BitacoraDTO> buscaBitacoraCerradas(String idChecklist) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<BitacoraDTO> listBitacora = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", idChecklist);

        out = cierreBitacoras.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_BITACORA.SP_OBTIENE_CERRADAS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        listBitacora = (List<BitacoraDTO>) out.get("RCL_BITACORA");

        if (error == 1) {
            logger.info("Algo ocurrió al consultar la Bitacora");
        } else {
            return listBitacora;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<BitacoraDTO> buscaBitacoraCerradasR(String idChecklist) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<BitacoraDTO> listBitacora = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", idChecklist);

        out = cierreBitacorasR.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_BITACORA.SP_OBTIENE_CERRADAS_R}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        listBitacora = (List<BitacoraDTO>) out.get("RCL_BITACORA");

        if (error == 1) {
            logger.info("Algo ocurrió al consultar la Bitacora");
        } else {
            return listBitacora;
        }

        return null;
    }
}
