package com.gruposalinas.checklist.domain;

public class TipoCifradoDTO {

	private int idTipoCifrado;
	private int numEmpleado;
	private int tipoServicio;
	private String json = "";
	private String fecha = "";
	private int tipoApp;
	
	public int getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(int tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public int getTipoApp() {
		return tipoApp;
	}
	public void setTipoApp(int tipoApp) {
		this.tipoApp = tipoApp;
	}	
	public int getIdTipoCifrado() {
		return idTipoCifrado;
	}
	public void setIdTipoCifrado(int idTipoCifrado) {
		this.idTipoCifrado = idTipoCifrado;
	}
	public int getNumEmpleado() {
		return numEmpleado;
	}
	public void setNumEmpleado(int numEmpleado) {
		this.numEmpleado = numEmpleado;
	}

	public String getJson() {
		return json;
	}
	public void setJson(String json) {
		this.json = json;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
}
