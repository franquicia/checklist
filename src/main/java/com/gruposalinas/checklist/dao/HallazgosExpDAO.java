package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.HallazgosExpDTO;


public interface HallazgosExpDAO {

	  public int inserta(HallazgosExpDTO bean)throws Exception;
		
	  //elimina por respuesta
		public boolean eliminaResp(String idResp)throws Exception;
		
		//elimina por bitacoragral
		public boolean eliminaBita(String bitGral)throws Exception;
		
		public List<HallazgosExpDTO> obtieneDatos(int bitGral) throws Exception; 
		
		public List<HallazgosExpDTO> obtieneInfo() throws Exception; 
		
		// UPDATE WEB
		public boolean actualiza(HallazgosExpDTO bean)throws Exception;
		
		//UPDATE MOVIL
		public boolean actualizaMov(HallazgosExpDTO bean)throws Exception;
		
		public boolean actualizaResp(HallazgosExpDTO bean)throws Exception;
		
		public boolean cargaHallazgos(int bitGral)throws Exception;
		
		public boolean EliminacargaHallazgos()throws Exception;
		
		public boolean cargaHallazgosxBita(int bitGral)throws Exception;
		
		//HALLAZGOS HISTORICO
		
		public List<HallazgosExpDTO> obtieneHistFolio(int idFolio) throws Exception; 
		
		public List<HallazgosExpDTO> obtieneHistorico() throws Exception; 
		
		public List<HallazgosExpDTO> obtieneHistBita(int bitGral) throws Exception; 
		
		public boolean insertaHallazgosHist(int idHallazgo)throws Exception;
		
		public boolean cargaHallazgosHistorico()throws Exception;
		
		public boolean eliminaBitaHisto(String bitGral)throws Exception;
	}