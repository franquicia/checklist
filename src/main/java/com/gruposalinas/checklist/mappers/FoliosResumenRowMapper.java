package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.FoliosResumenDTO;

public class FoliosResumenRowMapper implements RowMapper<FoliosResumenDTO>{

	@Override
	public FoliosResumenDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		FoliosResumenDTO foliosResumenDTO = new FoliosResumenDTO();
		
		foliosResumenDTO.setIdPregunta(rs.getInt("IDPREGUNTA"));
		foliosResumenDTO.setAccion(rs.getString("ACCION"));
		foliosResumenDTO.setPregunta(rs.getString("PREGUNTA"));
		foliosResumenDTO.setRespuesta(rs.getString("RESPUESTA"));
		
		return foliosResumenDTO;
	}
	

}
