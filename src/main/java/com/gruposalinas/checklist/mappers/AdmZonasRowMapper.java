package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.AdminZonaDTO;


public class AdmZonasRowMapper implements RowMapper<AdminZonaDTO> {

	public AdminZonaDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AdminZonaDTO admZonasDTO = new AdminZonaDTO();
		
		admZonasDTO.setIdZona(String.valueOf(rs.getInt("FIIDZONA")));
		admZonasDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
		admZonasDTO.setIdStatus(String.valueOf(rs.getInt("FIIDESTATUS")));
		admZonasDTO.setIdTipo(String.valueOf(rs.getInt("FIIDITIPO")));		
		
		return admZonasDTO;
	}
}
