package com.gruposalinas.checklist.domain;

public class AdmPregZonaDTO {
	private String idPregZona;
	private String idPreg;
	private String idZona;
	public String getIdPregZona() {
		return idPregZona;
	}
	public void setIdPregZona(String idPregZona) {
		this.idPregZona = idPregZona;
	}
	public String getIdPreg() {
		return idPreg;
	}
	public void setIdPreg(String idPreg) {
		this.idPreg = idPreg;
	}
	public String getIdZona() {
		return idZona;
	}
	public void setIdZona(String idZona) {
		this.idZona = idZona;
	}
	
	@Override
	public String toString() {
		return "AdmPregZonaDTO [idPregZona=" + idPregZona + ", idPreg=" + idPreg + ", idZona=" + idZona + "]";
	}
	
	
}


