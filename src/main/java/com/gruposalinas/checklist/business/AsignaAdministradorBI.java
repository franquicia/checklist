package com.gruposalinas.checklist.business;


import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.AsignaProyectosDAO;
import com.gruposalinas.checklist.domain.AsignaTransfDTO;

public class AsignaAdministradorBI {

	private static Logger logger = LogManager.getLogger(AsignaAdministradorBI.class);

private List<AsignaTransfDTO> listafila;

	@Autowired
	AsignaProyectosDAO asignaProyectosDAO;
	
	
	public int insertaAsignacionProy(AsignaTransfDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = asignaProyectosDAO.insertaAsignacionProy(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean desactivaAsignacionProy(String ceco,String usuario,String agrupa){
		boolean respuesta = false;
		
		try {
			respuesta = asignaProyectosDAO.desactivaAsignacionProy(ceco,usuario,agrupa);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	
	public List<AsignaTransfDTO> obtieneProyectosAdm( String proyecto) {
		
		try {
			listafila =  asignaProyectosDAO.obtieneProyectosAdm(proyecto);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	

	public List<AsignaTransfDTO> obtieneFasesAdm(String ceco,String proyecto) {
		
		try {
			listafila =  asignaProyectosDAO.obtieneFasesAdm( ceco,proyecto);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<AsignaTransfDTO> obtieneAsignacionesProy(String ceco,String proyecto) {
		
		try {
			listafila =  asignaProyectosDAO.obtieneAsignacionesProy( ceco,proyecto);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public boolean liberaFaseProy(AsignaTransfDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = asignaProyectosDAO.liberaFaseProy(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	

		public List<AsignaTransfDTO> obtieneDatosLiberaFase(String ceco,String proyecto) {
			
			try {
				listafila =  asignaProyectosDAO.obtieneDatosLiberaFase( ceco,proyecto);
			} catch (Exception e) {
				logger.info("No fue posible obtener los datos");
				UtilFRQ.printErrorLog(null, e);	
			}
			
			return listafila;			
		}
	

/*
	//trae respuestas
		public String  obtieneDatosCursoresCeco(String ceco) {
				JSONObject respuesta = new JSONObject();
				
			try {
				Map<String, Object> listaSucursals = null;
				listaSucursals = asignaProyectosDAO.obtieneDatosCursoresCeco(ceco);
				
				
				@SuppressWarnings("unchecked")
				List<AsignaTransfDTO> listaProyecto = (List<AsignaTransfDTO>) listaSucursals.get("listaProyecto");
				@SuppressWarnings("unchecked")
				List<AsignaTransfDTO> listaFases = (List<AsignaTransfDTO>) listaSucursals.get("listaFases");
				@SuppressWarnings("unchecked")
				List<AsignaTransfDTO> listaAgrupador = (List<AsignaTransfDTO>) listaSucursals.get("listaAgrupador");
				@SuppressWarnings("unchecked")
				List<AsignaTransfDTO> listaConteoHallazgo = (List<AsignaTransfDTO>) listaSucursals.get("listaConteoHallazgo");
				
				List<HallazgosTransfDTO> listaMatriz = new ArrayList<>();
				
				
				JSONArray proyec=new JSONArray();
				JSONArray fase = new JSONArray();
				JSONArray agrupador=new JSONArray();
				JSONArray matrizHallazgo=new JSONArray();
				JSONArray conteoHallazgos=new JSONArray();
				
				for(int i=0; i < listaProyecto.size();i++) {
					             
					JSONObject obj =new  JSONObject();
					obj.put("idProyecto", listaProyecto.get(i).getProyecto());
					obj.put("NombreProyecto", listaProyecto.get(i).getNombProy());
					obj.put("idStatus", listaProyecto.get(i).getIdestatus());
					obj.put("ObsProyecto", listaProyecto.get(i).getObsProyecto());
					obj.put("periodo", listaProyecto.get(i).getPeriodo());
					obj.put("tipoProyecto", listaProyecto.get(i).getTipoProyecto());
					obj.put("cargaInicialHallazgos", listaProyecto.get(i).getEdoCargaHallazgo());
					obj.put("statusMaximo", listaProyecto.get(i).getStatusMaximo());
					proyec.put(obj);		
				
				} 
				
				for(int i=0; i < listaFases.size();i++) {
					
					JSONObject obj =new  JSONObject();
					
					obj.put("idFase", listaFases.get(i).getFase());
					obj.put("idProyecto", listaFases.get(i).getProyecto());
					obj.put("idVersion", listaFases.get(i).getVersion());
					obj.put("nombreFase", listaFases.get(i).getObsFase());
					obj.put("banderaUltimaFase", listaFases.get(i).getBanderaUltFase());
					
					fase.put(obj);
				
				} 
				for(int i=0; i < listaAgrupador.size();i++) {
					             
					JSONObject obj =new  JSONObject();
					
					
					obj.put("idAgrupador", listaAgrupador.get(i).getAgrupador());
					obj.put("idFase", listaAgrupador.get(i).getFase());
					obj.put("idOrdenFase", listaAgrupador.get(i).getIdOrdenFase());
					obj.put("estatus", listaAgrupador.get(i).getIdestatus());
					obj.put("observacion", listaAgrupador.get(i).getObs());

					
					agrupador.put(obj);		
				
				} 
				
				
				try {
					
				  listaMatriz = hallazgosMatrizDAO.obtieneHallazgoMatriz(null);
				  
				} catch (Exception e) {
					logger.info("AP con la matriz " + e);
					
				}

				
				for(int i=0; i < listaMatriz.size();i++) {
		             
					JSONObject obj =new  JSONObject();
					
					
					obj.put("idMatriz", listaMatriz.get(i).getIdMatriz());
					obj.put("tipoProyecto", listaMatriz.get(i).getTipoProy());
					obj.put("statusEnvio", listaMatriz.get(i).getStatusEnvio());
					obj.put("statusHallazgo", listaMatriz.get(i).getStatusHallazgo());
					obj.put("idPerfil", listaMatriz.get(i).getIdPerfil());
					obj.put("color", listaMatriz.get(i).getColor());
					obj.put("nombreStatus", listaMatriz.get(i).getNombreStatus());

					matrizHallazgo.put(obj);		
				
				} 
				for(int i=0; i < listaConteoHallazgo.size();i++) {
		             
					JSONObject obj =new  JSONObject();
					
					obj.put("ceco", listaConteoHallazgo.get(i).getCeco());
					obj.put("fase", listaConteoHallazgo.get(i).getFase());
					obj.put("proyecto", listaConteoHallazgo.get(i).getProyecto());
					obj.put("conteoHallazgos", listaConteoHallazgo.get(i).getConteoHallazgos());
					obj.put("nombreFase", listaConteoHallazgo.get(i).getNombFase());
					obj.put("detalleFase", listaConteoHallazgo.get(i).getObs());
					obj.put("version", listaConteoHallazgo.get(i).getVersion());

					conteoHallazgos.put(obj);		
				
				} 

				respuesta.accumulate("listaProyecto", proyec);
				respuesta.accumulate("listaFases", fase);
				respuesta.accumulate("listaAgrupador", agrupador);
				respuesta.accumulate("listaMatriz", matrizHallazgo);
				respuesta.accumulate("listaConteoHallazgo", conteoHallazgos);
		
		    	
			} catch (Exception e) {
				logger.info("No fue posible obtener los datos");
				UtilFRQ.printErrorLog(null, e);
			}

			return respuesta.toString();
		}
		
		public boolean actualizaN(AsignaTransfDTO bean){
			boolean respuesta = false;
			
			try {
				respuesta = asignaProyectosDAO.actualizaN(bean);
			} catch (Exception e) {
				logger.info("No fue posible actualizar");
				UtilFRQ.printErrorLog(null, e);	
			}
			
			return respuesta;			
		}
*/
		

		
}