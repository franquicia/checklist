package com.gruposalinas.checklist.business;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.PreguntaAdmDAO;
import com.gruposalinas.checklist.dao.PreguntaDAO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class PreguntaAdmBI {

	private static Logger logger = LogManager.getLogger(PreguntaAdmBI.class);
	
	@Autowired
	PreguntaAdmDAO preguntaAdmDAO;
	
	List<PreguntaDTO> listaPregunta = null;
	List<PreguntaDTO> listaPreguntas = null;
	List<ChecklistDTO> listaChecklist= null;
	Map<String, Object> listaPila = null;
	
	//MODULOS
	List<ModuloDTO> listaModulos = null;
	List<ModuloDTO> listaModulo = null;
	
	public List<ModuloDTO> obtieneModulo(){
		try{
			listaModulos = preguntaAdmDAO.obtieneModulo();
		}
		catch(Exception e)
		{
			logger.info("No fue posible obtener los datos de la tabla Modulo"+e);
			
		}
		
		return listaModulos;
	}
	
	public List<ModuloDTO> obtieneModulo(int idModulo){
		try{
			listaModulo = preguntaAdmDAO.obtieneModulo(idModulo);
		}
		catch(Exception e)
		{
			logger.info("No fue posible obtener datos de la tabla Modulo");
			
			
		}
		return listaModulo;
	}
	
	public int insertaModulo (ModuloDTO modulo){
		
		int idModulo =0;
		try{
			idModulo = preguntaAdmDAO.insertaModulo(modulo);
		}
		catch(Exception e)
		{
			logger.info("No fue posible insertar el Modulo"+e);
			
		}
		return idModulo;
	}
	
	public boolean actualizaModulo(ModuloDTO modulo){
		
		boolean respuesta = false;
		
		try{
			respuesta = preguntaAdmDAO.actualizaModulo(modulo);
		}
		catch(Exception e){
			logger.info("No fue posible actualizar la tabla Modulo");
			
			
		}
		
		return respuesta;
	}
	
	public boolean eliminaModulo(int idModulo)
	{
		boolean respuesta =false;
		
		try{
			respuesta = preguntaAdmDAO.eliminaModulo(idModulo);
		}
		catch(Exception e){
			logger.info("No fue posible borrar el Modulo");
			
			
		}
		return respuesta;
	}
	// CHECKLIST 
	public int insertaChecklist(ChecklistProtocoloDTO checklist){
		int idChecklist = 0;
		
		try{
			idChecklist = preguntaAdmDAO.insertaChecklist(checklist);
		}catch(Exception e){
			logger.info("No fue posible insertar el Checklist: "+e.getMessage());
			
		}
		
		return idChecklist;
	}
	
	public int insertaChecklistCom(ChecklistDTO bean){
		int idChecklist = 0;
		
		try{
			idChecklist = preguntaAdmDAO.insertaChecklistCom(bean);
		}catch(Exception e){
			logger.info("No fue posible insertar el Checklist");
			
		}
		
		return idChecklist;
	}
	
	//total pond
	public boolean actualizaChecklistCom(ChecklistDTO bean){
		boolean respuesta= false;
		
		try {
			respuesta = preguntaAdmDAO.actualizaChecklistCom(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Checklist");
			
		}
		
		return respuesta;
		
	}

public List<ChecklistDTO> buscaChecklist(int idChecklist){
		
		try{
			listaChecklist = preguntaAdmDAO.buscaChecklist(idChecklist);
		}catch(Exception e){
			logger.info("No fue posible obtener el Checklist");
			
		}
		
		return listaChecklist;
		
	}
	

	public List<ChecklistDTO> buscaChecklist(){
		
		try{
			listaChecklist = preguntaAdmDAO.buscaChecklist();
		}catch(Exception e){
			logger.info("No fue posible obtener los Checklist");
			
		}
		
		return listaChecklist;
		
	}
	
	public boolean eliminaChecklist(int idCheck)
	{
		boolean respuesta =false;
		
		try{
			respuesta = preguntaAdmDAO.eliminaChecklist(idCheck);
		}
		catch(Exception e){
			logger.info("No fue posible borrar el checklist");
			
			
		}
		return respuesta;
	}
	//PREGUNTAS
	public List<PreguntaDTO> obtienePregunta(){
		try{
			listaPreguntas = preguntaAdmDAO.obtienePregunta();
		}
		catch(Exception e)
		{
			logger.info("No fue posible obtener datos de la tabla Pregunta");
			
			
		}
		return listaPreguntas;
	}
	
	public List<PreguntaDTO> obtienePregunta(int idPreg){
		try{
			listaPregunta = preguntaAdmDAO.obtienePregunta(idPreg);
		}
		catch(Exception e)
		{
			logger.info("No fue posible obtener datos de la tabla Preguntas");
			
		}
		return listaPregunta;
	}
	
	public int insertaPreguntas(PreguntaDTO preguntas){
		int idPregunta = 0;
		try{
			idPregunta = preguntaAdmDAO.insertaPregunta(preguntas);
		}
		catch(Exception e)
		{
			logger.info("No fue posible insertar la Pregunta");
			
		}
		
		return idPregunta;
	}
	
	public int insertaPreguntaCom(PreguntaDTO preguntas){
		int idPregunta = 0;
		try{
			idPregunta = preguntaAdmDAO.insertaPreguntaCom(preguntas);
		}
		catch(Exception e)
		{
			logger.info("No fue posible insertar la Pregunta");
			
		}
		
		return idPregunta;
	}
	
	public boolean actualizaPregunta(PreguntaDTO pregunta){
		boolean respuesta = false;
		try
		{
			respuesta = preguntaAdmDAO.actualizaPregunta(pregunta);
		}
		catch(Exception e)
		{
			logger.info("No fue posible actualizar la Pregunta");
			
		}
		
		return respuesta;
	}
	
	
	//imperd
	
	public boolean actualizaPreguntaCom(PreguntaDTO pregunta){
		boolean respuesta = false;
		try
		{
			respuesta = preguntaAdmDAO.actualizaPreguntaCom(pregunta);
		}
		catch(Exception e)
		{
			logger.info("No fue posible actualizar la Pregunta imp");
			
		}
		
		return respuesta;
	}
	
	public boolean eliminaPregunta(int idPreg){
		boolean respuesta = false;
		
		try{
			respuesta = preguntaAdmDAO.eliminaPregunta(idPreg);
		}
		catch(Exception e)
		{
			logger.info("No fue posible eliminar la Pregunta");
			
		}
		
		return respuesta;
	}
	
//CHECK PREG
List<ChecklistPreguntaDTO> listaChecklistPregunta = null;
	
	public boolean insertaChecklistPregunta(ChecklistPreguntaDTO bean ){
	
		boolean respuesta = false;
		try{
			respuesta = preguntaAdmDAO.insertaChecklistPregunta(bean);
		}catch(Exception e){
			logger.info("No fue posible insertar Pregunta-CheckList");
			
		}		
		return respuesta;		
	}
	
	public boolean actualizaChecklistPregunta(ChecklistPreguntaDTO bean){
		
		boolean respuesta = false;
		try{
			respuesta = preguntaAdmDAO.actualizaChecklistPregunta(bean);
		}catch(Exception e){
			logger.info("No fue posible actualizar Pregunta-CheckList");
			
		}		
		return respuesta;
	}
	
	public boolean eliminaChecklistPregunta(int idChecklist, int idPregunta){
		
		boolean respuesta = false;
		try{
			respuesta = preguntaAdmDAO.eliminaChecklistPregunta(idChecklist,idPregunta);
		}catch(Exception e){
			logger.info("No fue posible eliminar Pregunta-CheckList");
			
		}		
		return respuesta;
	}
	
	
	public List<ChecklistPreguntaDTO> buscaPreguntas(int idChecklist){
		
		try{
			listaChecklistPregunta = preguntaAdmDAO.buscaPreguntas(idChecklist);
		}catch(Exception e){
			logger.info("No fue posible obtener Pregunta-CheckList");
			
		}
		
		return listaChecklistPregunta;
	}
	
	
	public List<ChecklistPreguntaDTO> obtienePregXcheck(int idChecklist){
		List<ChecklistPreguntaDTO> listaChecklistPregunta2=null;
		
		try {
			listaChecklistPregunta2 = preguntaAdmDAO.obtienePregXcheck(idChecklist);
						
		} catch (Exception e) {
			logger.info("No fue posible obtener las preguntas del Checklist: "+e);
			
		}

		return listaChecklistPregunta2;
	}
}
