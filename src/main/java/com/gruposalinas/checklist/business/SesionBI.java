package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.SesionDAO;
import com.gruposalinas.checklist.domain.AppPerfilDTO;
import com.gruposalinas.checklist.domain.SesionDTO;

public class SesionBI {

	private static Logger logger = LogManager.getLogger(SesionBI.class);

	List<SesionDTO> listaSesion = null;
	
	@Autowired
	SesionDAO sesionDAO;
	

	public int insertaSesion(SesionDTO bean)  {
		
		int idCanal = 0;
		
		try {
			idCanal = sesionDAO.insertaSesion(bean);	
		} catch (Exception e) {
			logger.info("No fue posible insertar");
		}
		
		return idCanal;
		
	}
		
	public boolean actualizaSesion(SesionDTO bean) {
		
		boolean respuesta = false ;
		
		try {
			respuesta = sesionDAO.actualizaSesion(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualiza");
		}
		
		return respuesta;
	}
	
	public boolean eliminaSesion(int idSesion) {
		
		boolean respuesta = false ;
		
		try {
			respuesta = sesionDAO.eliminaSesion(idSesion);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
		}
		
		return respuesta;		
	}
	
	public boolean eliminaSesiones(String fechaInicio, String FechaFin)  {
		
		boolean respuesta = false ;
		
		try {
			respuesta = sesionDAO.eliminaSesiones(fechaInicio, FechaFin);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
		}
		
		return respuesta;		
	}
	

	public List<SesionDTO> buscaSesion(String navegador, String idUsuario, String ip, String fechaLogueo, String fechaActualiza, String bandera){
		
		try {
			listaSesion = sesionDAO.buscaSesion(navegador, idUsuario, ip, fechaLogueo, fechaActualiza, bandera);
		} catch (Exception e) {
			logger.info("No fue posible consultar");
		}
				
		return listaSesion;		
	}
	
	public List<SesionDTO> buscaSesion(String idUsuario, String idSesion) {
		
		try {
			listaSesion = sesionDAO.buscaSesion(idUsuario, idSesion);
		} catch (Exception e) {
			logger.info("No fue posible consultar");
		}
				
		return listaSesion;		
	}
	

}
