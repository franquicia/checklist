package com.gruposalinas.checklist.business;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;

import sun.misc.BASE64Decoder;

public class FirmaExpansionCoreBI extends Thread{
	
	private static Logger logger = LogManager.getLogger(FirmaExpansionCoreBI.class);
	
		
	private String bitacora;
	private String ceco;
	private String puesto;
	private String responsable;
	private String correo;
	//Es la posicion
	private String nombre;
	private String ruta;
	private int idFase;
	private int idProyecto;

	
	private FirmaCheckBI firmaCheckBI;
	
	private int respuesta;

	public FirmaExpansionCoreBI(String bitacora, String ceco, String puesto, String responsable, String correo, String nombre, String ruta, int idFase, int idProyecto){
		this.ceco = ceco;
		this.bitacora = bitacora;
		this.puesto = puesto;
		this.responsable = responsable;
		this.correo = correo;
		this.nombre = nombre;
		this.ruta = ruta;
		this.idFase = idFase;
		this.idProyecto = idProyecto;
	}
	
	public int getRespuesta() {
		return respuesta;
	}

	@Override
	public void run(){		
		
		firmaCheckBI = (FirmaCheckBI) FRQAppContextProvider.getApplicationContext().getBean("firmaCheckBI");
        		
		
		//Validamos que la imagen corresponda a alguien que no asisitió al recorrido
		if (responsable.equalsIgnoreCase("n/a") && correo.equalsIgnoreCase("n/a")) {
			insertaRuta();
		}else {
			insertaFirma();
		}
		logger.info("Termino");
	}
	

	public void insertaFirma() {
		
		try {

	        File r2 = null;
	        String rootPath = File.listRoots()[0].getAbsolutePath();
	        String rutaFirmaBD = "";

	        String rutaFirma = "";
	        
	        rutaFirma = "/franquicia/firmaChecklist/firma/";
	        String rutaPATHFirma = rootPath + rutaFirma;
	        logger.info("ruta: " + rutaPATHFirma);
	        r2 = new File(rutaPATHFirma);

	        if (r2.mkdirs()) {
	            logger.info("SE HA CREADA LA CARPETA");
	        } else {
	            logger.info("EL DIRECTORIO YA EXISTE");
	        }

	        
	        
	        BASE64Decoder decoder = new BASE64Decoder();
	        byte[] firmaDecodificada = decoder.decodeBuffer(ruta);
	        Date fechaActual = new Date();

	        //Formateando la fecha:

	        DateFormat formatoHora = new SimpleDateFormat("HHmmss");
	        DateFormat formatoFecha = new SimpleDateFormat("ddMMyyyy");
	        String hora = formatoFecha.format(fechaActual);
	        String fecha = formatoHora.format(fechaActual);
	        String nomDecodificadoStr = "" + ceco + "" + fecha + "" + hora;
	        String extDecodificadaStr = "jpg";
	        String rutaTotalF = rutaPATHFirma + nomDecodificadoStr + "." + extDecodificadaStr;
	        FileOutputStream fileOutputF = new FileOutputStream(rutaTotalF);

	        try {
	            BufferedOutputStream bufferOutputF = new BufferedOutputStream(fileOutputF);
	            bufferOutputF.write(firmaDecodificada);
	            bufferOutputF.close();
	            rutaFirmaBD = rutaTotalF;
	        } finally {
	            fileOutputF.close();
	        }
	        
	        logger.info("rutaFirmaBD: " + rutaFirmaBD);
	        
	        FirmaCheckDTO firm = new FirmaCheckDTO();
	        firm.setBitacora(bitacora);
			firm.setCeco(ceco);
			firm.setPuesto(puesto);
			firm.setResponsable(responsable);
			firm.setCorreo(correo);
			firm.setNombre(nombre+"");
			firm.setRuta(rutaFirmaBD);
			firm.setIdFase(idFase);
			firm.setIdProyecto(idProyecto);

	    		int resp = 0;
	    			
	        for(int a=0; a<3; a++) {
	        		
					resp= firmaCheckBI.insertaN(firm);
					
		        if (resp != 0) {
		            resp = 1;
		            break;
		        }else {
		        		resp=0;
		        }
	        }
	        
	        
	        logger.info("rutaFirmaBD respuesta: " + resp);
	        
	        this.respuesta = resp;
		} catch (Exception e) {
			logger.info("rutaFirmaBD respuesta: " + 0);
			logger.info("rutaFirmaBD respuesta: " + e);
			this.respuesta = 0;
		}
		
	}
	
	public void insertaRuta() {
		
		try {

	        String rootPath = File.listRoots()[0].getAbsolutePath();
	        String rutaFirma = "";
	        
	        rutaFirma = "/franquicia/firmaChecklist/firma/99999511152517122019.jpg";
	        String rutaPATHFirma = rootPath + rutaFirma ;
	        logger.info("ruta: " + rutaPATHFirma);
	       
   
	        FirmaCheckDTO firm = new FirmaCheckDTO();
	        firm.setBitacora(bitacora);
			firm.setCeco(ceco);
			firm.setPuesto(puesto);
			firm.setResponsable(responsable);
			firm.setCorreo(correo);
			firm.setNombre(nombre+"");
			firm.setRuta(rutaPATHFirma);
			firm.setIdFase(idFase);
			firm.setIdProyecto(idProyecto);

	    		int resp = 0;
	    			
	        for(int a=0; a<3; a++) {
	        		
					resp= firmaCheckBI.insertaN(firm);
					
		        if (resp != 0) {
		            resp = 1;
		            break;
		        }else {
		        		resp=0;
		        }
	        }
	        
	        
	        logger.info("rutaFirmaBD respuesta: " + resp);
	        
	        this.respuesta = resp;
		} catch (Exception e) {
			logger.info("rutaFirmaBD respuesta: " + 0);
			this.respuesta = 0;
		}
		
	}
	
}
