package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.TareasDAOImpl;
import com.gruposalinas.checklist.domain.TareaDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class TareasBI {

	private static Logger logger = LogManager.getLogger(TareasBI.class);

	private List<TareaDTO> listaTareas;

	@Autowired
	TareasDAOImpl tareaDAO;

	public boolean insertaTarea(TareaDTO bean) {
		boolean respuesta = false;

		try {
			respuesta = tareaDAO.insertaTarea(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar el registro de la Tarea");
			
		}

		return respuesta;
	}

	public boolean actualizaTarea(TareaDTO bean) {
		boolean respuesta = false;

		try {
			respuesta = tareaDAO.actualizaTarea(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el registro de la Tarea");
			
		}

		return respuesta;
	}

	public boolean eliminaTarea(int idTarea) {
		boolean respuesta = false;

		try {
			respuesta = tareaDAO.eliminaTarea(idTarea);
		} catch (Exception e) {
			logger.info("No fue posible eliminar el registro de la Tarea");
			
		}

		return respuesta;
	}

	public List<TareaDTO> obtieneTareas() {

		try {
			listaTareas = tareaDAO.obtieneTareas();
		} catch (Exception e) {
			logger.info("No fue posible obtener el registro de la Tarea");
			
		}

		return listaTareas;
	}

	public List<TareaDTO> obtieneTareasActivas() {

		try {
			listaTareas = tareaDAO.obtieneActivas();
		} catch (Exception e) {
			logger.info("No fue posible obtener el registro de la Tarea");
			
		}

		return listaTareas;
	}
}
