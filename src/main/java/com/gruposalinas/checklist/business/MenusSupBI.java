package com.gruposalinas.checklist.business;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.MenusSupDAO;
import com.gruposalinas.checklist.domain.MenusSupDTO;

public class MenusSupBI {
		
	private static Logger logger = LogManager.getLogger(MenusSupBI.class);
	
	@Autowired
	MenusSupDAO menusSupDAO;
	List<MenusSupDTO> listaConsulta = null;
	int respuesta = 0;

	public int insertaMenu(int idMenu, String descMenu, int activo){	
		try {
			respuesta = menusSupDAO.insertMenu(idMenu, descMenu, activo);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}			
		return respuesta;
	}

	public int updateMenu(int idMenu, String descMenu, String activo){		
		try {
			respuesta = menusSupDAO.updateMenu(idMenu, descMenu, activo);
		} catch (Exception e) {
			logger.info(e);
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}			
		return respuesta;
	}

	public int deleteMenu(int idMenu){
		try {
			respuesta = menusSupDAO.deleteMenu(idMenu);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}
		return respuesta;
	}
	
	public List<MenusSupDTO> getMenus(String idMenu){		
		try {
			listaConsulta = menusSupDAO.getMenus(idMenu);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}
		return listaConsulta;
	}

}
