package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.Admin2SupDTO;
import com.gruposalinas.checklist.domain.AsignacionesSupDTO;

public class DetPersonalSupRowMapper implements RowMapper<Admin2SupDTO> {

	public Admin2SupDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		Admin2SupDTO asignaDTO = new Admin2SupDTO();
		
		asignaDTO.setSocio(rs.getString("Socio"));
		asignaDTO.setNumSocio(rs.getInt("No. de Socio"));
		asignaDTO.setSucursal(rs.getString("Sucursal"));
		asignaDTO.setCeco(rs.getString("Id Sucursal"));
		
		return asignaDTO;
		
		
	}

}
