package com.gruposalinas.checklist.domain;

public class ReporteIndicadorDTO {
	
	private int idIndicador;
	private String idCeco;
	private String nombreCeco;
	private int indicador;
	private String valor;
	
	public int getIdIndicador() {
		return idIndicador;
	}
	public void setIdIndicador(int idIndicador) {
		this.idIndicador = idIndicador;
	}
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	public int getIndicador() {
		return indicador;
	}
	public void setIndicador(int indicador) {
		this.indicador = indicador;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	@Override
	public String toString() {
		return "ReporteIndicadorDTO [idIndicador=" + idIndicador + ", idCeco=" + idCeco + ", nombreCeco=" + nombreCeco
				+ ", indicador=" + indicador + ", valor=" + valor + "]";
	}

	
	
}
