package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.RepoCheckPaperDTO;

public class RepoCheckPaperPreguntasRowMapper implements RowMapper<RepoCheckPaperDTO> {

	public RepoCheckPaperDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		RepoCheckPaperDTO rc = new RepoCheckPaperDTO();
		
		//Detalle Preguntas
		rc.setCali(rs.getDouble("FICALIFICACION"));
		rc.setBitacora(rs.getInt("FIID_BITACORA"));
		rc.setChekUsua(rs.getInt("FIID_CHECK_USUA"));
		rc.setIdCheclist(rs.getInt("FIID_CHECKLIST"));
		rc.setCeco(rs.getString("FCID_CECO"));
		rc.setNombreCheck(rs.getString("NOMBRECHECK"));
		rc.setVersion(rs.getInt("FIVERSION"));
		rc.setOrdenGrupo(rs.getInt("FIORDEN_GRUPO"));
		rc.setIdPreg(rs.getInt("FIID_PREGUNTA"));
		rc.setModulo(rs.getInt("FIID_MODULO"));
		rc.setTipoPreg(rs.getInt("FIID_TIPO_PREG"));
	    rc.setPregunta(rs.getString("PREGUNTA"));
	    rc.setNombreModulo(rs.getString("MODULO"));
	    rc.setIdmodPadre(rs.getInt("FIID_MOD_PADRE"));
	    rc.setClaveTipo(rs.getString("FCCLAVE_TIPO"));
	    rc.setArbol(rs.getInt("FIID_ARBOL_DES"));
	    rc.setRespAb(rs.getString("FCRESPUESTA"));
	    rc.setStatus(rs.getInt("FIESTATUS_E"));
	    rc.setPonderacion(rs.getFloat("FIPODENRACION"));
	    rc.setPlantilla(rs.getInt("FIID_PLANTILLA"));
	    rc.setPosResp(rs.getString("RESPUESTA"));
	    rc.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    rc.setRespAb(rs.getString("RESP_ABIERTA"));
	    rc.setIdRespAbierta(rs.getInt("FIID_RESPUESTA_AD"));
	    rc.setBandResp(rs.getInt("EVIDEN_AD"));
		rc.setFini(rs.getString("FDINICIO"));
		rc.setFfin(rs.getString("FDTERMINO"));

	    
		/*rc.setBandera(rs.getInt("BANDERA"));
		rc.setSemana(rs.getInt("SEMANA"));
		rc.setIdResp(rs.getInt("FIID_RESPUESTA"));
		rc.setRespAb(rs.getString("FCDESCRIPCION"));
		rc.setBandResp(rs.getInt("EVIDEN_AD"));
		rc.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
	    rc.setModulo(rs.getInt("FIID_MODULO"));
	    rc.setTipopregunta(rs.getString("TIPO_PREG"));
	   */
		
		return rc;
	}

}


