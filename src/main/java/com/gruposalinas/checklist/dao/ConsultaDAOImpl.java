package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ConsultaDTO;
import com.gruposalinas.checklist.mappers.ConsultaBitacoraRowMapper;
import com.gruposalinas.checklist.mappers.ConsultaCecoRowMapper;
import com.gruposalinas.checklist.mappers.ConsultaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ConsultaDAOImpl extends DefaultDAO implements ConsultaDAO {

    private static Logger logger = LogManager.getLogger(ConsultaDAOImpl.class);

    DefaultJdbcCall jdbcObtieneConsulta;
    DefaultJdbcCall jdbcObtieneConsulta2;
    DefaultJdbcCall jdbcObtieneConsulta3;

    public void init() {

        jdbcObtieneConsulta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAGETCHEKCECO")
                .withProcedureName("SPGETCHKCECO")
                .returningResultSet("PA_CONSULTA", new ConsultaCecoRowMapper());

        jdbcObtieneConsulta2 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAGETCHEKCECO")
                .withProcedureName("SPNOMCHECKID")
                .returningResultSet("PA_CONSULTA", new ConsultaRowMapper());

        jdbcObtieneConsulta3 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAGETCHEKCECO")
                .withProcedureName("SPGETBITACORA")
                .returningResultSet("PA_CONSULTA", new ConsultaBitacoraRowMapper());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaDTO> obtieneConsultaCeco(int idGrupo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaDTO> listaConsulta = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIORDENGPO", idGrupo);

        out = jdbcObtieneConsulta.execute(in);

        //logger.info("Función ejecutada: FRANQUICIA.PAGETCHEKCECO.SPGETCHKCECO");
        listaConsulta = (List<ConsultaDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al consultar el StoreProcedure");
        } else {
            return listaConsulta;
        }

        return listaConsulta;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ConsultaDTO> obtieneConsultaChecklist(int idGrupo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaDTO> listaConsulta2 = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIORDENGPO", idGrupo);

        out = jdbcObtieneConsulta2.execute(in);

        //logger.info("Función ejecutada: FRANQUICIA.PAGETCHEKCECO.SPNOMCHECKID");
        listaConsulta2 = (List<ConsultaDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al consultar el StoreProcedure");
        } else {
            return listaConsulta2;
        }

        return listaConsulta2;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ConsultaDTO> obtieneConsultaBitacora(int idCeco, int idChecklist) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaDTO> listaConsulta3 = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FCIDCECO", idCeco).addValue("PA_FIIDCHEKLST", idChecklist);

        out = jdbcObtieneConsulta3.execute(in);

        //logger.info("Función ejecutada: FRANQUICIA.PAGETCHEKCECO.SPGETBITACORA");
        listaConsulta3 = (List<ConsultaDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al consultar el StoreProcedure");
        } else {
            return listaConsulta3;
        }

        return listaConsulta3;
    }

}
