package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ConsultaDashBoardDAO;
import com.gruposalinas.checklist.domain.ConsultaDashBoardDTO;


public class ConsultaDashBoardBI {
		
	private static Logger logger = LogManager.getLogger(ConsultaDashBoardDTO.class);
	
	@Autowired
	ConsultaDashBoardDAO consultaDashBoardDAO;
	
	List<ConsultaDashBoardDTO> listaConsulta = null;

	public List<ConsultaDashBoardDTO> obtieneDashBoard(ConsultaDashBoardDTO bean){
	
		try {
			listaConsulta = consultaDashBoardDAO.obtieneDashBoard(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
			logger.info(""+e);
		}			
		
		return listaConsulta;
	
	}
	
	
}