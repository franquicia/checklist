$body = $("body");
$(document).on({
    ajaxStart: function() {$body.addClass("loading");},
    ajaxStop: function() {$body.removeClass("loading");}    
});

var porcentajeL=0;
var validaCheck=0;
var intervalo=null;
var intervaloDespliegue=null;
var urlCache = "";
var filaHijo=0;


$(window).load(function() {
	//document.getElementById("tipoModal").className="modal3";
	intervalo = setInterval("reporte()",300000);
});

function reporte(){
		
	modal = 1;
	document.getElementById("tipoModal").className="modal3";
	location.href = "/checklist/reportes/reporteNegocio.htm";
}

function myFunction() {
	$body.removeClass("loading1");
	location.reload();
}

function nobackbutton(){
	window.location.hash="no-back-button";
	window.location.hash="Again-No-back-button" //chrome
	window.onhashchange=function(){window.location.hash="";}
}

var arreglo = [];
var cuenta = 0;

function muestraZonas(fila, idCeco, nomCeco, tam , nivel){

	
	//document.getElementById("tipoModal").className="modal3";
	//alert(fila + ' - ' + idCeco + ' - ' + nomCeco + ' - ' + tam + ' - ' + nivel);
	
	var ban=false;
	$.ajax({
		type : "GET",
		//url : "/checklist/central/vistaChecklist.htm?idUsuario=147247&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2017&seleccionMes=7&seleccionCanal="+seleccionCanalL+"&seleccionPais=21",'
		url :'/checklist/reportes/ajaxNegocio.json?idCeco='+idCeco+'&nomCeco='+nomCeco,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async:true,
		delimiter : ",",
		success : function(json) {
			
			cuenta=cuenta+1;
			
			if (json == null) {
				ban=true;
				//alert('Sin Datos');
				//$("#idLlenaZonas"+idCeco).append("<span class='Gris1'>Sin Datos</span>");
			}else{
				ban=false;	
				
				document.getElementById("nom"+nivel+fila).innerHTML = "";
				document.getElementById("con"+nivel+fila).innerHTML = "";
				document.getElementById("nor"+nivel+fila).innerHTML = "";
				document.getElementById("nun"+nivel+fila).innerHTML = "";
				document.getElementById("vis"+nivel+fila).innerHTML = "";
				document.getElementById("pla"+nivel+fila).innerHTML = "";
				
				$("#nom"+nivel+""+fila).append(json.nomCeco+"<span>suc</span>");
				
				if(json.contribucion==null)
					$("#con"+nivel+""+fila).append("<div class='radio gris'>-</div>");
				else
					if(json.colorContribucion == "verde")
						$("#con"+nivel+""+fila).append("<div class='radio verdeN'>"+json.contribucion+"</div>");
					else
						$("#con"+nivel+""+fila).append("<div class='radio rojo'>"+json.contribucion+"</div>");
				
				if(json.normalidad==null)
					$("#nor"+nivel+""+fila).append("<div class='radio gris'>-</div>");
				else
					if(json.colorNormalidad == "verde")
						$("#nor"+nivel+""+fila).append("<div class='radio verdeN'>"+json.normalidad+"</div>");
					else
						$("#nor"+nivel+""+fila).append("<div class='radio rojo'>"+json.normalidad+"</div>");
				
				
				if(json.nuncaAbonadas==null)
					$("#nun"+nivel+""+fila).append("<div class='radio gris'>-</div>");
				else
					if(json.colorNuncaAbonadas)
						$("#nun"+nivel+""+fila).append("<div class='radio verdeN'>"+json.nuncaAbonadas+"</div>");
					else
						$("#nun"+nivel+""+fila).append("<div class='radio rojo'>"+json.nuncaAbonadas+"</div>");
				
				
				if(json.vista==null)
					$("#vis"+nivel+""+fila).append("<div class='radio gris'>-</div>");
				else
					$("#vis"+nivel+""+fila).append("<div class='radio rojo'>"+json.vista+"</div>");
				
				if(json.plazo==null)
					$("#pla"+nivel+""+fila).append("<div class='radio gris'>-</div>");
				else
					$("#pla"+nivel+""+fila).append("<div class='radio rojo'>"+json.plazo+"</div>");
		
			}
			if(cuenta == (tam)){
				document.getElementById("tipoModal").className="modal1";
				cuenta=0;
			}
			
			/*if(fila == (tam-1)){
				if(ban){
					alert('Sin Datos de Extraccion');
					document.getElementById("tipoModal").className="modal1";
				}
			}*/
				
		},
		error : function() {
			//alert('Favor de actualizar la pagina');
		}
		
		
	});	
}

function metodoOrdenaObjetos(matriz){
	
	var flag = false;
	var l =1;
	var sumaAct;
	var sumaAnt;
	var act;
	var ant;
	
	while(l < matriz.length){

		sumaAnt=matriz[l-1].rh.plantilla - matriz[l-1].rh.rotacion  - matriz[l-1].rh.asesores  + matriz[l-1].rh.certificados + matriz[l-1].rh.tareas;
		sumaAct=matriz[l].rh.plantilla - matriz[l].rh.rotacion - matriz[l].rh.asesores + matriz[l].rh.certificados + matriz[l].rh.tareas;

		if(sumaAnt < sumaAct){
			ant=matriz[l-1];
			act=matriz[l];
			matriz[l - 1] = act;
			matriz[l] = ant;
			flag=true;
		}

		if (flag && (l + 1) == matriz.length) {
			l = 0;
			flag = false;
		}
		l++;
	}
	
	return matriz;
}

function despliega(id, fila){
	
	despliegaCheck(id, fila);
	modal = 2;
	filaHijo=fila;
	//LLAMAR A AJAX PARA OBTENER LOS DATOS	
}



function despliegaCheck(idCeco, fila){
	
	if(validaCheck!=idCeco){ 
		
		document.getElementById("tipoModal").className="modal3";
		arreglo = [];

		validaCheck=idCeco;
		document.getElementById("idLlenaZonas"+idCeco).innerHTML = "";

		$.ajax({
			type : "GET",
			url : '/checklist/reportes/ajaxBuscaCecosPA.json?idCeco='+idCeco,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {
				
				if (json == null) {
					//alert('Sin Datos');
					$("#idLlenaZonas"+idCeco).append("<span class='Gris1'>Sin Datos</span>");
					document.getElementById("tipoModal").className="modal1";
				}else{					

					for (var a = 0; a < Object.keys(json).length; a++) {
						$("#idLlenaZonas"+idCeco).append("<table class='bloquer subnivel'><tbody><tr id='"+json[a].idCeco+"' class='pointer'><td id='nom"+(fila+1)+(a)+"' class='nombreCheck'>"+json[a].descCeco+"</td>");
			
						$("#"+json[a].idCeco).append("<td id='con"+(fila+1)+(a)+"'><div class='radio grisN'>-<div></td>");
						$("#"+json[a].idCeco).append("<td id='nor"+(fila+1)+(a)+"'><div class='radio grisN'>-<div></td>");
						$("#"+json[a].idCeco).append("<td id='nun"+(fila+1)+(a)+"'><div class='radio grisN'>-<div></td>");
						$("#"+json[a].idCeco).append("<td id='vis"+(fila+1)+(a)+"'><div class='radio grisN'>-<div></td>");
						$("#"+json[a].idCeco).append("<td id='pla"+(fila+1)+(a)+"'><div class='radio grisN'>-<div></td>");
					
						$("#idLlenaZonas"+idCeco).append("</tr><tbody></table>");
						
						muestraZonas(a,json[a].idCeco,json[a].descCeco,Object.keys(json).length,""+(fila+1));
					
					}
					//document.getElementById("tipoModal").className="modal1";
					
				} 	
				
			},error : function() {
				//alert('Favor de actualizar la pagina');
			}
		});
	}	
}


function despliegaCheckIntervalo(){

	document.getElementById("tipoModal").className="modal3";
	document.getElementById("idLlenaZonas"+validaCheck).innerHTML = "";
	arreglo = [];
	
	$.ajax({
		type : "GET",
		url : '/checklist/reportes/ajaxBuscaCecosPA.json?idCeco='+validaCheck,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			
			if (json == null) {
				//alert('Sin Datos');
				$("#idLlenaZonas"+validaCheck).append("<span class='Gris1'>Sin Datos</span>");
				document.getElementById("tipoModal").className="modal1";
			}else{					
				
				for (var a = 0; a < Object.keys(json).length; a++) {
					$("#idLlenaZonas"+validaCheck).append("<table class='bloquer subnivel'><tbody><tr id='"+json[a].idCeco+"' class='pointer'><td id='nom"+(filaHijo+1)+(a)+"' class='nombreCheck'>"+json[a].descCeco+"</td>");
		
					$("#"+json[a].idCeco).append("<td id='con"+(filaHijo+1)+(a)+"'><div class='radio grisN'>-<div></td>");
					$("#"+json[a].idCeco).append("<td id='nor"+(filaHijo+1)+(a)+"'><div class='radio grisN'>-<div></td>");
					$("#"+json[a].idCeco).append("<td id='nun"+(filaHijo+1)+(a)+"'><div class='radio grisN'>-<div></td>");
					$("#"+json[a].idCeco).append("<td id='vis"+(filaHijo+1)+(a)+"'><div class='radio grisN'>-<div></td>");
					$("#"+json[a].idCeco).append("<td id='pla"+(filaHijo+1)+(a)+"'><div class='radio grisN'>-<div></td>");
				
					$("#idLlenaZonas"+validaCheck).append("</tr><tbody></table>");
					
					muestraZonas(a,json[a].idCeco,json[a].descCeco,Object.keys(json).length,""+(filaHijo+1));
				
				}
			} 	
			
		},error : function() {
			//alert('Favor de actualizar la pagina');
		}
	});
}
