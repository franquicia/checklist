package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.VersionProtoCheckDAO;
import com.gruposalinas.checklist.domain.VersionProtoCheckDTO;

public class VersionProtoCheckBI {

	private static Logger logger = LogManager.getLogger(VersionProtoCheckBI.class);

private List<VersionProtoCheckDTO> listaVersiones;
	
	@Autowired
	VersionProtoCheckDAO versionProtoCheckDAO;
		
	
	public boolean insertaVersion(VersionProtoCheckDTO bean){
		boolean respuesta = true;
		
		try {
			respuesta = versionProtoCheckDAO.insertaVersion(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar la version ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaVersion(VersionProtoCheckDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = versionProtoCheckDAO.eliminaVersion(bean);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	
	
	public List<VersionProtoCheckDTO> obtieneVersiones(VersionProtoCheckDTO bean) {

		try {
			listaVersiones = versionProtoCheckDAO.consultaVersion(bean);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			//UtilFRQ.printErrorLog(null, e);	
			e.printStackTrace();
		}

		return listaVersiones;
	}
	
	


	
	public boolean actualiza(VersionProtoCheckDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = versionProtoCheckDAO.actualizaVersion(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	
	

}