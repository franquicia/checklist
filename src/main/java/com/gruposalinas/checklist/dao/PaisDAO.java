package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.PaisDTO;

public interface PaisDAO {

	public List<PaisDTO> obtienePais() throws Exception;
	
	public List<PaisDTO> obtienePaisVisualizador() throws Exception;
	
	public List<PaisDTO> obtienePais(int idPais) throws Exception;
	
	public int insertaPais(PaisDTO bean) throws Exception;
	
	public boolean actualizaPais(PaisDTO bean) throws Exception;
	
	public boolean eliminaPais(int idPais) throws Exception;
}
