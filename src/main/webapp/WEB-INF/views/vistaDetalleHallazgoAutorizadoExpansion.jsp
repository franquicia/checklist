<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">

<head>

<script src="../js/expancion/jquery-3.3.1.js"></script>
<script src="../js/expancion/jquery-ui-1.12.1.js"></script>

<script type="text/javascript">
var urlServer = '${urlServer}';

$(document).ready(
		function() {
			$('#owl-carousel2').on(
					'initialized.owl.carousel changed.owl.carousel',
					function(e) {
						if (!e.namespace) {
							return;
						}
						var carousel = e.relatedTarget;
						$('#txtMdl2').html(
								" Item "
										+ '<span class="imgNum">'
										+ (carousel.relative(carousel
												.current()) + 1)
										+ '</span> de '
										+ carousel.items().length);
					}).owlCarousel({
				loop : false,
				margin : 10,
				nav : true,
				items : 1,
				dots : false,
			});

			$('.owl-stage-outer').zoomify({

				  // animation duration
				  duration: 200,

				  // easing effect
				  easing:   'linear',

				  // zoom scale
				  // 1 = fullscreen
				  scale:    0.9

				});
		});


function aceptarHallazgo(idCeco,idResp,status,respuesta,ruta,fechaAutorizacion,fechaFin,comentario){

	alertify.confirm('AVISO', 'Deseas Dar Por Atendido el Hallazgo', 
			function(){
				updateHallazgo(idCeco,idResp,"3","SI",ruta,fechaAutorizacion,fechaFin,comentario);
		 		alertify.success('Ok');
		 	}, 
		 	function(){ 
			 	alertify.error('Cancelado');
			}
	);
/*
    var opcion = confirm("Deseas Dar Por Atendido el Hallazgo");
    if (opcion == true) {
        updateHallazgo(idCeco,idResp,"3","SI",ruta,fechaAutorizacion,fechaFin,comentario);
	} else {
        alert("Cancelado");
	}
*/
}

function rechazarHallazgo(idCeco,idResp,status,respuesta,ruta,fechaAutorizacion,fechaFin,comentario){

	alertify.confirm('AVISO', 'Deseas Rechazar el Hallazgo', 
			function(){
				updateHallazgo(idCeco,idResp,"1","NO","","",fechaFin,comentario);
		 		alertify.success('Ok');
		 	}, 
		 	function(){ 
			 	alertify.error('Cancelado');
			}
	);
/*
    var opcion = confirm("Deseas Rechazar el Hallazgo");
    if (opcion == true) {
        updateHallazgo(idCeco,idResp,"1","NO","","",fechaFin,comentario);
	} else {
        alert("Cancelado");
	}
*/
}

function updateHallazgo(idCeco,idResp,status,respuesta,ruta,fechaAutorizacion,fechaFin,comentario){
	$(location).attr("href","actualizaHallazgoDirecto.htm?ceco="+idCeco+"&idResp="+idResp+"&status="+status+"&respuesta="+respuesta+"&ruta="+ruta+"&fechaAutorizacion="+fechaAutorizacion+"&fechaFin="+fechaFin+"&comentario="+comentario);

}
function regresar() {
	alertify.confirm('AVISO', 'Deseas Regesar a la pantalla anterior', 
			function(){
				window.history.back();
		 		alertify.success('Ok');
		 	}, 
		 	function(){ 
			 	alertify.error('Cancelado');
			}
	);
}

</script>


<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />

<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/secciones.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/estiloVentana.css" media="screen">


<link rel="stylesheet" type="text/css" href="../css/expancion/modal.css">
<link rel="stylesheet" type="text/css" href="../css/expancion/index.css">

<!-- CARRUSEL -->
<link rel="stylesheet" type="text/css" href="../css/expancion/zoomify.css">
<link rel="stylesheet" href="../css/expancion/owl.theme.defaultHallazgos.css">
<link rel="stylesheet" href="../css/expancion/owl.carouselHallazgos.css">

<script src="../js/expancion/owl.carousel.js"></script>
<script src="../js/expancion/zoomify.js"></script>
<!-- CARRUSEL -->

<script src="../js/expancion/alertify.js"></script>
<link rel="stylesheet" href="../css/expancion/alertify.css">

<style type="text/css">

.divContainer{
margin-left: auto;
margin-right: auto;
display: block;
width:50%;
height:70%;
background-color:#E8E8E8;
}

.divContenido{
	border-style: solid 1px;
	margin-top:2px;
	margin-left:auto;
	margin-right: auto;
	display: block;
	width:99%;
	height:9.5%;
}

.divImg{
	margin-top:2px;
	margin-left:auto;
	margin-right: auto;
	display: block;
	width:99%;
	height:70%;
	background-color:white; 
}

.divAccion{
margin-top:10px;
margin-left: auto;
margin-right: auto;
display: block;
width:50%;
height:5%;
}

.divContenido{
	margin-top:2px;
	margin-left:auto;
	margin-right: auto;
	display: block;
	width:99%;
	height:9.5%;
	background-color:white; 
}

.textoItem{
	text-align:center;
	margin-top:2px;
	width:100%;
	height:100%;
}

.btnRespuesta{
	margin-top:2px;
	width: 250px;
    height: 50px;
	margin-left:auto;
	margin-right: auto;
	display: auto;	
}

#divSiBtn{
    margin-left: 0%;
    margin-right: auto;
	float:left;
  	width:45px;
  	height:45px;
  	background-color:green;
  	border-radius: 25px;
}

#divNoBtn{
	margin-left: auto;
    margin-right: 0%;
    float:right;
  	width:45px;
  	height:45px;
  	background-color:gray;
  	border-radius: 25px;
}

.btnDecision{
	margin-top:2px;
	/*width: 300px; erick*/
    height: 50px;
	margin-left:auto;
	margin-right: auto;
	display: auto;	
}

#decisionSi{
	 margin-left: 0%;
    margin-right: auto;
	float:left;
  	width:45px;
  	height:45px;
}

#decisionNo{
	margin-left: auto;
    margin-right: 0%;
    float:right;
  	width:45px;
  	height:45px;
}

#divSiBtn:hover,#divNoBtn:hover,#decisionSi:hover,#decisionNo:hover {
  transform: scale(1.1); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
}
</style>


</head>

<body style="height: auto;">

	<div class="header h90">
		<span class="titulagsBold spBold fz146">HALLAZGO</span>
	</div>
	
	<div class="divContainer">
		
		<div class="divContenido">
			
			<div class="textoItem">
				<label style="font-size:18px;">${pregunta}</label>
				
			</div>
		
		</div>
	
		<div class="divContenido">
				<div class="btnRespuesta">
						<div id="divSiBtn">
							<label style="color:white; text-align:center; margin-left:auto; margin-right: auto; display: block; font-size:20px;"><b>SI</b></label>
						</div>
					  	
					  	<div id="divNoBtn">
					  		<label style="color:white; text-align:center; margin-left:auto; margin-right: auto; display: block; font-size:20px;"><b>NO</b></label>
					  	</div>
				</div>
		</div>
			
		<div class="divImg">
							${carrusel}
		</div>
		<div class="divContenido" style="padding-bottom:1px;">
			<textarea style="width:99%; height:95%; resize: none;" readonly>${comentarios}</textarea>
					
		</div>
	
		<div class="divAccion">
			<div class="btnDecision">
						<!--  <div id="decisionSi">
					  		<button style="background-color:gray; color:white; text-align:center; margin-left:auto; margin-right: auto; display: block; font-size:20px;" onclick="aceptarHallazgo(${ceco},${idResp},3,'SI','${ruta}','${fechaAutorizacion}','${fechaFin}','${comentarios}');">Aceptar</button>
						</div>
					  	
					  	<div id="decisionNo">
					  		<button style="background-color:gray; color:white; text-align:center; margin-left:auto; margin-right: auto; display: block; font-size:20px;" onclick="rechazarHallazgo(${ceco},${idResp},1,'NO','','','${fechaFin}','${comentarios}');">Rechazar</button>
					  	</div> -->
					  	<button id="decisionNoBtn" style="background-color:gray; color:white; text-align:center; margin-left:auto; margin-right: auto; display: block; font-size:20px;" onclick="regresar();">Regresar</button>
				</div>
		</div>
		
	<div class="clear"></div>
</body>
</html>