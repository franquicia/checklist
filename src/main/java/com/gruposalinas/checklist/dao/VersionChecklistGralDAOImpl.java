package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.VersionChecklistGralDTO;
import com.gruposalinas.checklist.mappers.VersionesChecklistGralRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class VersionChecklistGralDAOImpl extends DefaultDAO implements VersionChecklistGralDAO {

    private static Logger logger = LogManager.getLogger(VersionChecklistGralDAOImpl.class);

    DefaultJdbcCall jdbcInsertaVers;
    DefaultJdbcCall jdbcEliminaVers;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneVers;
    DefaultJdbcCall jbdcActualizaVers;
    DefaultJdbcCall jbdcActualizaVers2;

    public void init() {

        jdbcInsertaVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSCHECK")
                .withProcedureName("SP_INS_VERS");

        jdbcEliminaVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSCHECK")
                .withProcedureName("SP_DEL_VERS");

        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSCHECK")
                .withProcedureName("SP_SEL_DETALLE")
                .returningResultSet("RCL_VERS", new VersionesChecklistGralRowMapper());

        jdbcObtieneVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSCHECK")
                .withProcedureName("SP_SEL_VERS")
                .returningResultSet("RCL_VERS", new VersionesChecklistGralRowMapper());

        jbdcActualizaVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSCHECK")
                .withProcedureName("SP_ACT_VERS");

        jbdcActualizaVers2 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSCHECK")
                .withProcedureName("SP_ACT_VERS2");

    }

    public int inserta(VersionChecklistGralDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", bean.getIdCheck())
                .addValue("PA_VERSION", bean.getIdVers())
                .addValue("PA_ACTIVO", bean.getIdactivo())
                .addValue("PA_OBS", bean.getObs())
                .addValue("PA_VERSANT", bean.getVersionAnt())
                .addValue("PA_NOMBRECHECK", bean.getNombrecheck());

        out = jdbcInsertaVers.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMVERSCHECK.SP_INS_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDTABLA");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el Empleado");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean elimina(String idTab) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDTABLA", idTab);

        out = jdbcEliminaVers.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMVERSCHECK.SP_DEL_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el dato(" + idTab + ")");
        } else {
            return true;
        }

        return false;

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<VersionChecklistGralDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<VersionChecklistGralDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneTodo.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMVERSCHECK.SP_SEL_DETALLE}");

        listaFijo = (List<VersionChecklistGralDTO>) out.get("RCL_VERS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los  datos");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //parametro

    @SuppressWarnings("unchecked")
    public List<VersionChecklistGralDTO> obtieneDatos(int idVers) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<VersionChecklistGralDTO> listaN = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDVERS", idVers);

        out = jdbcObtieneVers.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMVERSCHECK.SP_SEL_VERS}");
        //lleva el nombre del cursor del procedure
        listaN = (List<VersionChecklistGralDTO>) out.get("RCL_VERS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la version(" + idVers + ")");
        }
        return listaN;

    }

    public boolean actualiza(VersionChecklistGralDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDTABLA", bean.getIdTab())
                .addValue("PA_CHECKLIST", bean.getIdCheck())
                .addValue("PA_VERSION", bean.getIdVers())
                .addValue("PA_ACTIVO", bean.getIdactivo())
                .addValue("PA_OBS", bean.getObs())
                .addValue("PA_VERSANT", bean.getVersionAnt())
                .addValue("PA_NOMBRECHECK", bean.getNombrecheck());

        out = jbdcActualizaVers.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMVERSCHECK.SP_ACT_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar  del  id( " + bean.getIdTab() + ")");
        } else {
            return true;
        }
        return false;

    }

    public boolean actualiza2(VersionChecklistGralDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDTABLA", bean.getIdTab())
                .addValue("PA_CHECKLIST", bean.getVersionAnt())
                .addValue("PA_VERSION", bean.getIdVers());

        out = jbdcActualizaVers2.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMVERSCHECK.SP_ACT_VERS2}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdTab() + ")");
        } else {
            return true;
        }
        return false;

    }

}
