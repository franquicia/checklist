package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.AdmHandbookDTO;

public interface AdmHandbookDAO {

	
	public List<AdmHandbookDTO> obtieneTodos() throws Exception;
	
	public List<AdmHandbookDTO> obtieneIdBook(AdmHandbookDTO bean) throws Exception;
	
	public boolean insertaHandbook(AdmHandbookDTO bean) throws Exception;

	public boolean actualizaHandbook(AdmHandbookDTO bean) throws Exception;
	
	public boolean eliminaHandbook(AdmHandbookDTO bean) throws Exception;
}
