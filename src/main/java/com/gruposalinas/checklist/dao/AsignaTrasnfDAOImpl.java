package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AsignaTransfDTO;
import com.gruposalinas.checklist.mappers.AsigTransfDetaRowMapper;
import com.gruposalinas.checklist.mappers.ConteoHallazActaRowMapper;
import com.gruposalinas.checklist.mappers.FasesTransformacionDetaRowMapper;
import com.gruposalinas.checklist.mappers.FasesTransformacionRowMapper;
import com.gruposalinas.checklist.mappers.ProgramacionAsignacionRowMapper;
import com.gruposalinas.checklist.mappers.ProyectTransfRowMapper;
import com.gruposalinas.checklist.mappers.SucTransfRowMapper;
import com.gruposalinas.checklist.mappers.VersionesTransfRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AsignaTrasnfDAOImpl extends DefaultDAO implements AsignaTransfDAO {

    private static Logger logger = LogManager.getLogger(AsignaTrasnfDAOImpl.class);

    DefaultJdbcCall jdbcInsertaAsign;
    DefaultJdbcCall jdbcEliminaAsign;
    DefaultJdbcCall jdbcCargaNuevo;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneVersion;
    DefaultJdbcCall jbdcActualizaAsign;
    DefaultJdbcCall jbdcActualizaAsignNva;
    DefaultJdbcCall jbdcActualizaAsignCheckU;
    DefaultJdbcCall jdbcObtieneCheckusua;
    DefaultJdbcCall jdbcObtieneAsigna;
    DefaultJdbcCall jdbcCargaAsigna;
    DefaultJdbcCall jdbcObtieneCursores;
    DefaultJdbcCall jdbcObtieneCursoresCeco;
    DefaultJdbcCall jdbcInsertaAsignMtto;

    public void init() {

        jdbcInsertaAsign = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGTRANSF")
                .withProcedureName("SP_INS_ASIG");

        jdbcInsertaAsignMtto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAFLUJOMMTO")
                .withProcedureName("SP_INS_ASIG");

        jdbcEliminaAsign = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGTRANSF")
                .withProcedureName("SP_DEL_ASIGUSU");
        //carga cuando no existe en la tabla check_usua
        jdbcCargaNuevo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGTRANSF")
                .withProcedureName("SP_CARGAPROCESO");

        jdbcObtieneVersion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGTRANSF")
                .withProcedureName("SP_SEL_ASIG")
                .returningResultSet("RCL_ASIG", new AsigTransfDetaRowMapper());

        jbdcActualizaAsign = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGTRANSF")
                .withProcedureName("SP_ACT_ASIG");

        jbdcActualizaAsignNva = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGTRANSF")
                .withProcedureName("SP_ACT_NVO");

        jdbcCargaAsigna = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGTRANSF")
                .withProcedureName("SP_CARGAPROCESO");

        jdbcObtieneCursoresCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASOPTRANSFO")
                .withProcedureName("SP_SEL_CECO")
                .returningResultSet("RCL_PROYE", new ProyectTransfRowMapper())
                .returningResultSet("RCL_FASE", new FasesTransformacionRowMapper())
                .returningResultSet("RCL_AGRUPA", new FasesTransformacionDetaRowMapper())
                .returningResultSet("RCL_CONTHALLA", new ConteoHallazActaRowMapper());

        jdbcObtieneCursores = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASOPTRANSFO")
                .withProcedureName("SP_SEL_SUC")
                .returningResultSet("RCL_SUC", new SucTransfRowMapper())
                .returningResultSet("RCL_PROYE", new ProyectTransfRowMapper())
                .returningResultSet("RCL_FASE", new FasesTransformacionRowMapper())
                .returningResultSet("RCL_GENER", new FasesTransformacionDetaRowMapper())
                .returningResultSet("RCL_VERS", new VersionesTransfRowMapper())
                .returningResultSet("RCL_CONTHALLA", new ConteoHallazActaRowMapper())
                .returningResultSet("RCL_PROGRAASIST", new ProgramacionAsignacionRowMapper());

    }

    public int inserta(AsignaTransfDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_USU", bean.getUsuario())
                .addValue("PA_PROY", bean.getProyecto())
                .addValue("PA_USUASIG", bean.getUsuario_asig())
                .addValue("PA_OBSERV", bean.getObs())
                .addValue("PA_IDAGRUPA", bean.getAgrupador())
                .addValue("PA_EDOCARGA", bean.getEdoCargaHallazgo())
                .addValue("PA_BANDFOLIO", bean.getBanderaFolio());

        out = jdbcInsertaAsign.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAASIGTRANSF.SP_INS_ASIG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        //BigDecimal idTipoReturn = (BigDecimal) out.get("PA_FIIDESTADO");
        //idEmpFij = idTipoReturn.intValue();
        if (error != 1) {
            logger.info("Algo ocurrió al insertar el usuario");
            error = 0;
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public int insertaAsignacionMtto(AsignaTransfDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_USU", bean.getUsuario())
                .addValue("PA_PROY", bean.getProyecto())
                .addValue("PA_USUASIG", bean.getUsuario_asig())
                .addValue("PA_OBSERV", bean.getObs())
                .addValue("PA_IDAGRUPA", bean.getAgrupador())
                .addValue("PA_FECHAPROG", bean.getFechaProgramacion());
        //.addValue("PA_BANDFOLIO",bean.getBanderaFolio());

        out = jdbcInsertaAsignMtto.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAFLUJOMMTO.SP_INS_ASIG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        //BigDecimal idTipoReturn = (BigDecimal) out.get("PA_FIIDESTADO");
        //idEmpFij = idTipoReturn.intValue();
        if (error == 0) {
            logger.info("Algo ocurrió al insertar ASIGNACION");
            error = 0;
        } else {
            return error;
        }

        return idEmpFij;
    }

    public boolean elimina(String ceco, String usuario_asign) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_USUASIG", usuario_asign)
                .addValue("PA_CECO", ceco);

        out = jdbcEliminaAsign.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAASIGTRANSF.SP_DEL_ASIGUSU}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + usuario_asign + ")");
        } else {
            return true;
        }

        return false;

    }

    //parametro
    @SuppressWarnings("unchecked")
    public List<AsignaTransfDTO> obtieneDatos(String idproyecto) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AsignaTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPROY", idproyecto);

        out = jdbcObtieneVersion.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAASIGTRANSF.SP_SEL_ASIG}");
        //lleva el nombre del cursor del procedure
        lista = (List<AsignaTransfDTO>) out.get("RCL_ASIG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la version (" + idproyecto + ")");
        }
        return lista;

    }

    public boolean actualiza(AsignaTransfDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_PROY", bean.getProyecto())
                .addValue("PA_USU", bean.getUsuario())
                .addValue("PA_USUASIG", bean.getUsuario_asig())
                .addValue("PA_STATUS", bean.getIdestatus())
                .addValue("PA_OBSERV", bean.getObs())
                .addValue("PA_IDAGRUPA", bean.getAgrupador())
                .addValue("PA_ORDENFASE", bean.getIdOrden());

        out = jbdcActualizaAsign.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAASIGTRANSF.SP_ACT_ASIG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado de la version  id( " + bean.getProyecto() + ")");
        } else {
            return true;
        }
        return false;

    }

    public boolean actualizaN(AsignaTransfDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_PROY", bean.getProyecto())
                .addValue("PA_STATUS", bean.getIdestatus())
                .addValue("PA_OBSERV", bean.getObs())
                .addValue("PA_IDAGRUPA", bean.getAgrupador())
                .addValue("PA_ORDENFASE", bean.getIdOrden())
                .addValue("PA_DESBFASE", bean.getBanderaDesbloqueoFase());

        out = jbdcActualizaAsignNva.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAASIGTRANSF.SP_ACT_ASIG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado de la version  id( " + bean.getProyecto() + ")");
        } else {
            return true;
        }
        return false;

    }

    @SuppressWarnings("unchecked")
    public List<AsignaTransfDTO> obtieneDatosAsignaVer(String usuario) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AsignaTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_USUARIO", usuario);

        out = jdbcObtieneAsigna.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAASIGTRANSF.SP_SEL_ASIG}");
        //lleva el nombre del cursor del procedure
        lista = (List<AsignaTransfDTO>) out.get("RCL_ASIG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la version (" + usuario + ")");
        }
        return lista;

    }

    public boolean CargaAsig() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaAsigna.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAASIGTRANSF.SP_CARGAASIG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la carga de Asigna");
        } else {
            return true;
        }
        return false;

    }

    // obtiene respuesta NO IMPERDONABLE CONCATENADAS
    @SuppressWarnings("unchecked")
    public Map<String, Object> obtieneDatosCursores(String idusu) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<AsignaTransfDTO> listaSucursal = null;
        List<AsignaTransfDTO> listaProyecto = null;
        List<AsignaTransfDTO> listaFases = null;
        List<AsignaTransfDTO> listaGenerales = null;
        List<AsignaTransfDTO> listaVersiones = null;
        List<AsignaTransfDTO> listaConteoHallazgo = null;
        List<AsignaTransfDTO> listaProgramacionAsignaciones = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USU", idusu);

        out = jdbcObtieneCursores.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PASOPTRANSFO.SP_SEL_SUC}");

        listaSucursal = (List<AsignaTransfDTO>) out.get("RCL_SUC");
        listaProyecto = (List<AsignaTransfDTO>) out.get("RCL_PROYE");
        listaFases = (List<AsignaTransfDTO>) out.get("RCL_FASE");
        listaGenerales = (List<AsignaTransfDTO>) out.get("RCL_GENER");
        listaVersiones = (List<AsignaTransfDTO>) out.get("RCL_VERS");
        listaConteoHallazgo = (List<AsignaTransfDTO>) out.get("RCL_CONTHALLA");
        listaProgramacionAsignaciones = (List<AsignaTransfDTO>) out.get("RCL_PROGRAASIST");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion");
        } else {
            lista = new HashMap<String, Object>();
            lista.put("listaSucursal", listaSucursal);
            lista.put("listaProyecto", listaProyecto);
            lista.put("listaFases", listaFases);
            lista.put("listaGenerales", listaGenerales);
            lista.put("listaVersiones", listaVersiones);
            lista.put("listaConteoHallazgo", listaConteoHallazgo);
            lista.put("listaProgramacionAsignaciones", listaProgramacionAsignaciones);

            return lista;

        }
        return lista;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> obtieneDatosCursoresCeco(String ceco) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;

        List<AsignaTransfDTO> listaProyecto = null;
        List<AsignaTransfDTO> listaFases = null;
        List<AsignaTransfDTO> listaAgrupador = null;
        List<AsignaTransfDTO> listaConteoHallazgo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco);

        out = jdbcObtieneCursoresCeco.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PASOPTRANSFO.SP_SEL_SUC}");

        listaProyecto = (List<AsignaTransfDTO>) out.get("RCL_PROYE");
        listaFases = (List<AsignaTransfDTO>) out.get("RCL_FASE");
        listaAgrupador = (List<AsignaTransfDTO>) out.get("RCL_AGRUPA");
        listaConteoHallazgo = (List<AsignaTransfDTO>) out.get("RCL_CONTHALLA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion");
        } else {
            lista = new HashMap<String, Object>();

            lista.put("listaProyecto", listaProyecto);
            lista.put("listaFases", listaFases);
            lista.put("listaAgrupador", listaAgrupador);
            lista.put("listaConteoHallazgo", listaConteoHallazgo);

            return lista;

        }
        return lista;
    }

}
