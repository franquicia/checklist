package com.gruposalinas.checklist.business;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.SupervisorDAO;
import com.gruposalinas.checklist.domain.SupervisorDTO;

public class SupervisorBI {
		
	private static Logger logger = LogManager.getLogger(SupervisorBI.class);
	
	@Autowired
	SupervisorDAO supervisorDAO;
	
	List<SupervisorDTO> listaSupervisor = null;
	
	public List<SupervisorDTO> buscaSupervisor(SupervisorDTO bean){
	
		try {
			listaSupervisor = supervisorDAO.obtieneSupervisor(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar los supervisores: "+e);
		}			
		
		return listaSupervisor;
	
	}
}
