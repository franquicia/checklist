package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.DuplicadosDAO;
import com.gruposalinas.checklist.domain.AsignacionDTO;
import com.gruposalinas.checklist.domain.DuplicadosDTO;

public class DuplicadosBI {
	private static Logger logger = LogManager.getLogger(DuplicadosBI.class);
	
	@Autowired
	DuplicadosDAO duplicadosDAO;
	
	public boolean eliminaRespuestasD(){
		boolean respuesta = false;
		try{
			logger.info("Entro BI");
			respuesta = duplicadosDAO.eliminaRespuestasD();
			logger.info("Salio BI");
		}
		catch(Exception e){
			logger.info("No fue posible borrar la Respuesta");
			
		}
		
		return respuesta;
	}
	

	public List<DuplicadosDTO> consultaDuplicados(String idUsuario, String checkUsua, String idCheck, String idBitacora, String fechaInicio, String FechaFin, int bandera){
		
		List<DuplicadosDTO > respuesta = null;
				
		try {
			respuesta = duplicadosDAO.consultaDuplicados(idUsuario, checkUsua, idCheck, idBitacora, fechaInicio, FechaFin, bandera);
		} catch (Exception e) {
			logger.info("No fue posible consultar las respuestas duplicadas");
				
		}
		
		return respuesta;
	}
	
	

	public boolean eliminaDuplicadosResp (String idUsuario, String checkUsua, String idCheck, String idBitacora){
		
		boolean respuesta = false;
		
		try {
			respuesta = duplicadosDAO.eliminaDuplicadosResp(idUsuario, checkUsua, idCheck, idBitacora);
		} catch (Exception e) {
			logger.info("No fue posible eliminar las respuestas");
				
		}
		
		return respuesta;
	}
	
	

	public boolean eliminaDuplicadosNuevo (String idUsuario, String checkUsua, String idCheck, String idBitacora, String fechaInicio, String fechaFin){
		
		boolean respuesta = false;
		
		try {
			respuesta = duplicadosDAO.eliminaDuplicadosNuevo(idUsuario, checkUsua, idCheck, idBitacora, fechaInicio, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible eliminar las respuestas");
				
		}
		
		return respuesta;
	}
	
}
