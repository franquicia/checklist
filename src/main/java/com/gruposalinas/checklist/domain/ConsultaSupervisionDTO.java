package com.gruposalinas.checklist.domain;

public class ConsultaSupervisionDTO {

	private int idSucursal;
	private String nomSucursal;
	private String fechaInicio;
	private String fechaFin;
	private int idUsuario;
	private String nomUsuario;
	private int idProtocolo;
	private String nomProtocolo;
	private int idBitacora;
	
	public int getIdSucursal() {
		return idSucursal;
	}


	public void setIdSucursal(int idSucursal) {
		this.idSucursal = idSucursal;
	}


	public String getNomSucursal() {
		return nomSucursal;
	}


	public void setNomSucursal(String nomSucursal) {
		this.nomSucursal = nomSucursal;
	}



	public String getFechaInicio() {
		return fechaInicio;
	}


	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}


	public String getFechaFin() {
		return fechaFin;
	}


	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}


	public int getIdUsuario() {
		return idUsuario;
	}


	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}


	public String getNomUsuario() {
		return nomUsuario;
	}


	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}


	public int getIdProtocolo() {
		return idProtocolo;
	}


	public void setIdProtocolo(int idProtocolo) {
		this.idProtocolo = idProtocolo;
	}


	public String getNomProtocolo() {
		return nomProtocolo;
	}


	public void setNomProtocolo(String nomProtocolo) {
		this.nomProtocolo = nomProtocolo;
	}

	public int getIdBitacora() {
		return idBitacora;
	}



	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}



	@Override
	public String toString() {
		return "ConsultaSupervisionDTO [idSucursal=" + idSucursal
				+ ", nomSucursal=" + nomSucursal
				+ ", fechaInicio=" + fechaInicio
				+ ", fechaFin=" + fechaFin
				+ ", idUsuario=" + idUsuario
				+ ", nomUsuario=" + nomUsuario
				+ ", idProtocolo=" + idProtocolo
				+ ", nomProtocolo=" + nomProtocolo
				+ ", idBitacora=" + idBitacora+ "]";
	}
	
}
