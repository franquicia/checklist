package com.gruposalinas.checklist.servicios.servidor;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.gruposalinas.checklist.business.AdmPregZonasBI;
import com.gruposalinas.checklist.business.AdmTipoZonaBI;
import com.gruposalinas.checklist.business.AdmZonasBI;
import com.gruposalinas.checklist.business.AppBI;
import com.gruposalinas.checklist.business.ArbolDecisionBI;
import com.gruposalinas.checklist.business.AsignacionBI;
import com.gruposalinas.checklist.business.BitacoraBI;
import com.gruposalinas.checklist.business.ChecklistBI;
import com.gruposalinas.checklist.business.ChecklistPreguntaBI;
import com.gruposalinas.checklist.business.ChecklistUsuarioBI;
import com.gruposalinas.checklist.business.CompromisoBI;
import com.gruposalinas.checklist.business.DepuraBitacoraBI;
import com.gruposalinas.checklist.business.EdoChecklistBI;
import com.gruposalinas.checklist.business.EvidenciaBI;
import com.gruposalinas.checklist.business.GrupoBI;
import com.gruposalinas.checklist.business.ImagenesRespuestaBI;
import com.gruposalinas.checklist.business.ModuloBI;
import com.gruposalinas.checklist.business.OrdenGrupoBI;
import com.gruposalinas.checklist.business.PlantillaBI;
import com.gruposalinas.checklist.business.PreguntaBI;
import com.gruposalinas.checklist.business.PreguntaSupBI;
import com.gruposalinas.checklist.business.RespuestaAdBI;
import com.gruposalinas.checklist.business.RespuestaBI;
import com.gruposalinas.checklist.business.TipoChecklistBI;
import com.gruposalinas.checklist.business.TipoPreguntaBI;
import com.gruposalinas.checklist.domain.AdmPregZonaDTO;
import com.gruposalinas.checklist.domain.AdmTipoZonaDTO;
import com.gruposalinas.checklist.domain.AdminZonaDTO;
import com.gruposalinas.checklist.domain.AppPerfilDTO;
import com.gruposalinas.checklist.domain.AsignacionDTO;
import com.gruposalinas.checklist.domain.DepuraBitacoraDTO;
import com.gruposalinas.checklist.resources.FRQAuthInterceptor;
import com.gruposalinas.checklist.business.EmpFijoBI;

@Controller
@RequestMapping("/checklistServices")
public class BajaChecklistService {

	@Autowired
	ModuloBI modulobi;
	@Autowired
	TipoPreguntaBI tipPreguntabi;
	@Autowired
	PreguntaBI preguntabi;
	@Autowired
	TipoChecklistBI tipoChecklistbi;
	@Autowired
	ChecklistBI checklistbi;
	@Autowired
	ChecklistPreguntaBI checklistPreguntabi;
	@Autowired
	ChecklistUsuarioBI checklistUsuariobi;
	@Autowired
	EdoChecklistBI edoChecklistbi;
	@Autowired
	ArbolDecisionBI arbolDecisionbi;
	@Autowired
	BitacoraBI bitacorabi;
	@Autowired
	RespuestaBI respuestabi;
	@Autowired
	CompromisoBI compromisobi;
	@Autowired
	EvidenciaBI evidenciabi;
	@Autowired
	RespuestaAdBI respuestaAdbi;
	@Autowired
	ImagenesRespuestaBI imagenesRespuestaBI;
	@Autowired
	AsignacionBI asignacionBI;
	@Autowired
	GrupoBI grupoBI;
	@Autowired
	OrdenGrupoBI ordenBI;
	@Autowired
	AppBI appBI;
	@Autowired
	PlantillaBI plantillaBI;
	@Autowired
	EmpFijoBI empFijoBI;
	@Autowired
	PreguntaSupBI preguntaSupBI;
	@Autowired
	DepuraBitacoraBI depuraBitacoraBI;
	@Autowired
	AdmZonasBI admZonasBI;
	@Autowired
	AdmTipoZonaBI admTipoZonaBI;
	@Autowired
	AdmPregZonasBI admPregZonasBI;

	private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);

	// http://localhost:8080/checklist/checklistServices/eliminaModulo.json?idModulo=<?>
	@RequestMapping(value = "/eliminaModulo", method = RequestMethod.GET)
	public ModelAndView eliminaModulo(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String idModulo = request.getParameter("idModulo");

		boolean res = modulobi.eliminaModulo(Integer.parseInt(idModulo));

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "MODULO ELIMINADO");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/checklistServices/eliminaTipoPregunta.json?idTipoPreg=<?>
	@RequestMapping(value = "/eliminaTipoPregunta", method = RequestMethod.GET)
	public ModelAndView eliminaTipoPregunta(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idTipoPregunta = request.getParameter("idTipoPreg");

			boolean res = tipPreguntabi.eliminaTipoPregunta(Integer.parseInt(idTipoPregunta));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "TIPO_PREGUNTA ELIMINADO");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaPregunta.json?idPregunta=<?>
	@RequestMapping(value = "/eliminaPregunta", method = RequestMethod.GET)
	public ModelAndView eliminaPregunta(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idPregunta = request.getParameter("idPregunta");

			boolean res = this.preguntabi.eliminaPregunta(Integer.parseInt(idPregunta));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "PREGUNTA ELIMINADA");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaPregunta.json?idPregunta=<?>
	@RequestMapping(value = "/eliminaPreguntaService", method = RequestMethod.GET)
	public @ResponseBody boolean eliminaPreguntaService(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idPregunta = request.getParameter("idPregunta");

			boolean res = this.preguntabi.eliminaPregunta(Integer.parseInt(idPregunta));

			return res;
		} catch (Exception e) {
			logger.info(e);
			return false;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaTipoCheck.json?idTipoCheck=<?>
	@RequestMapping(value = "/eliminaTipoCheck", method = RequestMethod.GET)
	public ModelAndView eliminaTipoChecklist(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idTipoChck = request.getParameter("idTipoCheck");

			boolean res = tipoChecklistbi.eliminaTipoChecklist(Integer.parseInt(idTipoChck));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "TIPO CHECKLIST ELIMINADO");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}

	}

	// http://localhost:8080/checklist/checklistServices/eliminaEstadoChecklist.json?idEdoCheck=<?>
	@RequestMapping(value = "/eliminaEstadoChecklist", method = RequestMethod.GET)
	public ModelAndView eliminaEstadoChecklist(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idEdoChck = request.getParameter("idEdoCheck");

			boolean res = edoChecklistbi.eliminaEdoChecklist(Integer.parseInt(idEdoChck));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "ESTADO CHECKLIST ELIMINADO");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaCheck.json?idChecklist=<?>
	@RequestMapping(value = "/eliminaCheck", method = RequestMethod.GET)
	public ModelAndView eliminaCheck(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idChck = request.getParameter("idChecklist");

			boolean res = checklistbi.eliminaChecklist(Integer.parseInt(idChck));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "CHECKLIST ELIMINADO");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}

	}

	// http://localhost:8080/checklist/checklistServices/eliminaPregCheck.json?idChecklist=<?>&idPreg=<?>
	@RequestMapping(value = "/eliminaPregCheck", method = RequestMethod.GET)
	public ModelAndView eliminaPreguntaCheck(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idCheck = request.getParameter("idChecklist");
			String idPreg = request.getParameter("idPreg");

			boolean res = checklistPreguntabi.eliminaChecklistPregunta(Integer.parseInt(idCheck),
					Integer.parseInt(idPreg));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "PREGUNTA CHECKLIST ELIMINADA");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaPregCheck.json?idChecklist=<?>&idPreg=<?>
	@RequestMapping(value = "/eliminaPregCheckService", method = RequestMethod.GET)
	public @ResponseBody boolean eliminaPreguntaCheckService(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idCheck = request.getParameter("idChecklist");
			String idPreg = request.getParameter("idPreg");

			boolean res = checklistPreguntabi.eliminaChecklistPregunta(Integer.parseInt(idCheck),
					Integer.parseInt(idPreg));

			return res;
		} catch (Exception e) {
			logger.info(e);
			return false;
		}
	}

	/* AGREGUE IDCHECKUSUARIO (ID) */
	// http://localhost:8080/checklist/checklistServices/eliminaUsuCheck.json?idCheckUsuario=<?>
	@RequestMapping(value = "/eliminaUsuCheck", method = RequestMethod.GET)
	public ModelAndView eliminaUsuarioCheck(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idCheckUsuario = request.getParameter("idCheckUsuario");

			boolean res = checklistUsuariobi.eliminaChecklistUsuario(Integer.parseInt(idCheckUsuario));

			// ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

			mv.addObject("tipo", "ID CHECKLIST ELIMINADO");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/desactivaCheckusua.json?idChecklist=<?>
	@RequestMapping(value = "/desactivaCheckusua", method = RequestMethod.GET)
	public ModelAndView desactivaCheckusua(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idChecklist = request.getParameter("idChecklist");

			boolean res = checklistUsuariobi.desactivaChecklist(Integer.parseInt(idChecklist));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "ID CHECKLIST DESACTIVADO");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/desactivaCheckusuaBoolean.json?idChecklist=<?>
	@RequestMapping(value = "/desactivaCheckusuaBoolean", method = RequestMethod.GET)
	public @ResponseBody boolean desactivaCheckusuaBoolean(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idChecklist = request.getParameter("idChecklist");

			boolean res = checklistUsuariobi.desactivaChecklist(Integer.parseInt(idChecklist));

			return res;
		} catch (Exception e) {

			return false;
		}
	}

	/* AGREGUE IDSETARBOLDESICION (ID) */
	// http://localhost:8080/checklist/checklistServices/eliminaArbolDecision.json?idArbolDecision=<?>
	@RequestMapping(value = "/eliminaArbolDecision", method = RequestMethod.GET)
	public ModelAndView eliminaArbolDecision(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idArbolDecision = request.getParameter("idArbolDecision");

			boolean res = arbolDecisionbi.eliminaArbolDecision(Integer.parseInt(idArbolDecision));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "ARBOL ELIMINADO CON EXITO--->");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaBitacora.json?idBitacora=<?>
	@RequestMapping(value = "/eliminaBitacora", method = RequestMethod.GET)
	public ModelAndView eliminaBitacora(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idBitacora = request.getParameter("idBitacora");

			boolean res = bitacorabi.eliminaBitacora(Integer.parseInt(idBitacora));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "BITACORA ELIMINADA");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaRespuesta.json?idRespuesta=<?>
	@RequestMapping(value = "/eliminaRespuesta", method = RequestMethod.GET)
	public ModelAndView eliminaRespuesta(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idRespuesta = request.getParameter("idRespuesta");

			boolean res = respuestabi.eliminaRespuesta(Integer.parseInt(idRespuesta));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "RESPUESTA ELIMINADA");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaRespuestasDuplicadas.json
	@RequestMapping(value = "/eliminaRespuestasDuplicadas", method = RequestMethod.GET)
	public ModelAndView eliminaRespuestasDuplicadas(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {

			boolean res = respuestabi.eliminaRespuestasDuplicadas();

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "RESPUESTAS DUPLICADAS ELIMINADAS");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info("Ocurrio algo al eliminarRespuestas " + e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaCompromiso.json?idCompromiso=<?>
	@RequestMapping(value = "/eliminaCompromiso", method = RequestMethod.GET)
	public ModelAndView eliminaCompromiso(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idCompromiso = request.getParameter("idCompromiso");

			boolean res = compromisobi.eliminaCompromiso(Integer.parseInt(idCompromiso));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "COMPROMISO ELIMINADO");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaEvidencia.json?idEvidencia=<?>
	@RequestMapping(value = "/eliminaEvidencia", method = RequestMethod.GET)
	public ModelAndView eliminaEvidencia(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idEvidencia = request.getParameter("idEvidencia");

			boolean idEvi = evidenciabi.eliminaEvidencia(Integer.parseInt(idEvidencia));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "EVIDENCIA ELIMNADA");
			mv.addObject("res", idEvi);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaRespAd.json?idRespAd=<?>
	@RequestMapping(value = "/eliminaRespAd", method = RequestMethod.GET)
	public ModelAndView eliminaRespA(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idRespAd = request.getParameter("idRespAd");

			boolean idRespA = respuestaAdbi.eliminaRespAD(Integer.parseInt(idRespAd));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "RESPUESTA ADICIONAL ELIMINADA");
			mv.addObject("res", idRespA);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaImagenArbol.json?idImagen=<?>
	@RequestMapping(value = "/eliminaImagenArbol", method = RequestMethod.GET)
	public ModelAndView eliminaImagenArbol(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idImagen = request.getParameter("idImagen");

			boolean res = imagenesRespuestaBI.eliminaImagen(Integer.parseInt(idImagen));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "IMAGEN ARBOL ELIMINADA ");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaAsignaciones.json?idChecklist=<?>&idCeco=<?>&idPuesto=<?>
	@RequestMapping(value = "/eliminaAsignaciones", method = RequestMethod.GET)
	public ModelAndView eliminaAsignaciones(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idChecklist = request.getParameter("idChecklist");
			String idCeco = request.getParameter("idCeco");
			String idPuesto = request.getParameter("idPuesto");

			AsignacionDTO asignacionDTO = new AsignacionDTO();

			asignacionDTO.setIdChecklist(Integer.parseInt(idChecklist));
			asignacionDTO.setCeco(idCeco);
			asignacionDTO.setIdPuesto(Integer.parseInt(idPuesto));

			boolean res = asignacionBI.eliminarAsignacion(asignacionDTO);

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "ASIGNACION ELIMINADA ");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaGrupo.json?idGrupo=<?>
	@RequestMapping(value = "/eliminaGrupo", method = RequestMethod.GET)
	public ModelAndView eliminaGrupo(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idGrupo = request.getParameter("idGrupo");

			boolean res = grupoBI.eliminaGrupo(Integer.parseInt(idGrupo));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "GRUPO ELIMINADO ");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaOrdenGrupo.json?idOrdenGrupo=<?>
	@RequestMapping(value = "/eliminaOrdenGrupo", method = RequestMethod.GET)
	public ModelAndView eliminaOrdenGrupo(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idOrdenGrupo = request.getParameter("idOrdenGrupo");

			boolean res = ordenBI.eliminaOrdenGrupo(Integer.parseInt(idOrdenGrupo));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "ORDEN GRUPO ELIMINADO ");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaApp.json?idApp=<?>
	@RequestMapping(value = "/eliminaApp", method = RequestMethod.GET)
	public ModelAndView eliminaApp(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idApp = request.getParameter("idApp");

			boolean respuesta = appBI.eliminaApp(Integer.parseInt(idApp));

			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

			mv.addObject("res", respuesta);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaAppPerfil.json?idAppPerfil=<?>
	@RequestMapping(value = "/eliminaAppPerfil", method = RequestMethod.GET)
	public ModelAndView eliminaAppPerfil(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idAppPerfil = request.getParameter("idAppPerfil");

			boolean respuesta = appBI.eliminaAppPerfil(Integer.parseInt(idAppPerfil));

			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

			mv.addObject("res", respuesta);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaPlantilla.json?idPlantilla=<?>
	@RequestMapping(value = "/eliminaPlantilla", method = RequestMethod.GET)
	public ModelAndView eliminaPlantilla(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idPlantilla = request.getParameter("idPlantilla");

			boolean respuesta = plantillaBI.eliminaPlantilla(Integer.parseInt(idPlantilla));

			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

			mv.addObject("res", respuesta);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/eliminaElemento.json?idElemento=<?>
	@RequestMapping(value = "/eliminaElemento", method = RequestMethod.GET)
	public ModelAndView eliminaElemento(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idElemento = request.getParameter("idElemento");

			boolean respuesta = plantillaBI.eliminaElementoP(Integer.parseInt(idElemento));

			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

			mv.addObject("res", respuesta);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/eliminaEmpFijo.json?idEmpFijo=<?>
	@RequestMapping(value = "/eliminaEmpFijo", method = RequestMethod.GET)
	public ModelAndView eliminaEmpFijo(HttpServletRequest request, HttpServletResponse response) {
		try {

			String idEmpFijo = request.getParameter("idEmpFijo");

			boolean res = empFijoBI.elimina(idEmpFijo);

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Empleado Fijo Eliminado " + idEmpFijo);
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://10.51.218.144:8080/checklist/checklistServices/eliminaPreguntaSup.json?idPregunta=<?>
	@RequestMapping(value = "/eliminaPreguntaSup", method = RequestMethod.GET)
	public ModelAndView eliminaPreguntaSup(HttpServletRequest request, HttpServletResponse response) {
		try {

			String idPregunta = request.getParameter("idPregunta");

			boolean res = preguntaSupBI.eliminaPregunta(Integer.parseInt(idPregunta));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Pregunta Eliminada " + idPregunta);
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://10.51.218.140:8080/checklist/checklistServices/depuraBitacoras.json?fecha=<?>
	@RequestMapping(value = "/depuraBitacoras", method = RequestMethod.GET)
	public ModelAndView depuraBitacoras(HttpServletRequest request, HttpServletResponse response) {
		try {

			String fechaS = request.getParameter("fecha");

			int fecha = 0;
			if (fechaS != null) {
				fecha = Integer.parseInt(fechaS);
			}

			List<DepuraBitacoraDTO> respuesta = depuraBitacoraBI.depuraBitacorasSem(fecha);

			ModelAndView mv = new ModelAndView("muestraServicios");
			mv.addObject("tipo", "DEPURA_BITACORA");
			mv.addObject("res", respuesta);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	//http://10.51.218.72:8080/checklist/checklistServices/bajaZona.json?idZona=?
    @RequestMapping(value = "/bajaZona", method = RequestMethod.GET)
    public ModelAndView bajaZona(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        try {
            String idZona = request.getParameter("idZona");
            
            AdminZonaDTO zona = new AdminZonaDTO();
            zona.setIdZona(idZona);
            
            boolean respuesta = admZonasBI.eliminaZona(zona);
            mv.addObject("tipo", "Elimina Zona");
            mv.addObject("res", respuesta);

        } catch (Exception e) {
        	System.out.println("Catch bajaZona: "+e.getStackTrace());
        }
        return mv;
    }
    
  //http://10.51.218.72:8080/checklist/checklistServices/bajaTipoZona.json?idTipoZona=?
    @RequestMapping(value = "/bajaTipoZona", method = RequestMethod.GET)
    public ModelAndView bajaTipoZona(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        try {
            String idTipoZona = request.getParameter("idTipoZona");
            
            AdmTipoZonaDTO tipoZona = new AdmTipoZonaDTO();
            tipoZona.setIdTipoZona(idTipoZona);
            
            boolean respuesta = admTipoZonaBI.eliminaTipoZona(tipoZona);
            mv.addObject("tipo", "Elimina Tipo Zona");
            mv.addObject("res", respuesta);

        } catch (Exception e) {
        	System.out.println("Catch bajaTipoZona: "+e.getStackTrace());
        }
        return mv;
    }
    
  //http://10.51.218.72:8080/checklist/checklistServices/bajaPregZona.json?idPregZona=?
    @RequestMapping(value = "/bajaPregZona", method = RequestMethod.GET)
    public ModelAndView bajaPregZona(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        try {
            String idPregZona = request.getParameter("idPregZona");
            
            AdmPregZonaDTO pregZona = new AdmPregZonaDTO();
            pregZona.setIdPregZona(idPregZona);
            
            boolean respuesta = admPregZonasBI.eliminaPregZona(pregZona);
            mv.addObject("tipo", "Elimina Pregunta Zona");
            mv.addObject("res", respuesta);

        } catch (Exception e) {
        	System.out.println("Catch bajaPregZona: "+e.getStackTrace());
        }
        return mv;
    }
	
}
