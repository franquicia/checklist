package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.ConsultaAseguradorDTO;

public class ConsultaDetalleProRowMapper implements RowMapper<ConsultaAseguradorDTO> {

	public ConsultaAseguradorDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConsultaAseguradorDTO consultaDetalleDTO = new ConsultaAseguradorDTO();
		
		consultaDetalleDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		consultaDetalleDTO.setIdCeco(rs.getInt("FCID_CECO"));
		consultaDetalleDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		consultaDetalleDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
		consultaDetalleDTO.setFechaInicio(rs.getString("FDINICIO"));
		consultaDetalleDTO.setFechaTermino(rs.getString("FDTERMINO"));
		
		return consultaDetalleDTO;
		
		
	}

}
