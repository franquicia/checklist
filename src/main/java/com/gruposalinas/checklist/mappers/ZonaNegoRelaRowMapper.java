package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ZonaNegoExpDTO;

public class ZonaNegoRelaRowMapper implements RowMapper<ZonaNegoExpDTO> {

	public ZonaNegoExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		ZonaNegoExpDTO ne = new ZonaNegoExpDTO();
		
		ne.setIdTabNegocio(rs.getInt("FIID_TAB"));
	    ne.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
	    ne.setIdZona(rs.getInt("FIID_ZONA"));
	    ne.setIdActivo(rs.getInt("FIACTIVO"));
	    
		return ne;
	}

}


