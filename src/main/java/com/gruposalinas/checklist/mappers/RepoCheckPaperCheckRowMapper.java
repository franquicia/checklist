package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.RepoCheckPaperDTO;

public class RepoCheckPaperCheckRowMapper implements RowMapper<RepoCheckPaperDTO> {

	public RepoCheckPaperDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		RepoCheckPaperDTO rc = new RepoCheckPaperDTO();
		
		//Detalle Checklist
		rc.setCeco(rs.getString("FCID_CECO"));
		rc.setChekUsua(rs.getInt("FIID_CHECK_USUA"));
		rc.setIdCheclist(rs.getInt("FIID_CHECKLIST"));
		rc.setNombreCheck(rs.getString("CHECKLIST"));
		rc.setCali(rs.getDouble("FICALIFICACION"));
		rc.setFini(rs.getString("FDINICIO"));
		rc.setFfin(rs.getString("FDTERMINO"));
		rc.setBitacora(rs.getInt("FIID_BITACORA"));
		rc.setNombreCheck(rs.getString("NOMBRECHECK"));
		rc.setVersion(rs.getInt("FIVERSION"));
		rc.setOrdenGrupo(rs.getInt("FIORDEN_GRUPO"));
		rc.setIdPreg(rs.getInt("FIID_PREGUNTA"));
		rc.setModulo(rs.getInt("FIID_MODULO"));
	    rc.setTipoPreg(rs.getInt("FIID_TIPO_PREG"));
	    rc.setPregunta(rs.getString("PREGUNTA"));
	    rc.setNombreModulo(rs.getString("MODULO"));
	    rc.setIdmodPadre(rs.getInt("FIID_MOD_PADRE"));
	    
		/*rc.setBandera(rs.getInt("BANDERA"));
		rc.setSemana(rs.getInt("SEMANA"));
		rc.setIdResp(rs.getInt("FIID_RESPUESTA"));
		rc.setRespAb(rs.getString("FCDESCRIPCION"));
		rc.setBandResp(rs.getInt("EVIDEN_AD"));
		rc.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
	    rc.setModulo(rs.getInt("FIID_MODULO"));
	    rc.setTipopregunta(rs.getString("TIPO_PREG"));
	   */
		
		return rc;
	}

}


