package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;

public class ConteoSINOGralRowMapper implements RowMapper<ChecklistPreguntasComDTO> {

	public ChecklistPreguntasComDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistPreguntasComDTO con = new ChecklistPreguntasComDTO();

	    con.setClasif(rs.getString("FCCLASIFICA"));
	    con.setPregSi(rs.getInt("RESP_SI"));
	    con.setPregNo(rs.getInt("RESP_NO"));
		con.setPregNa(rs.getInt("RESP_NA"));
		con.setIdBitaGral(rs.getInt("FIID_BITGRAL"));
		con.setTotPreg(rs.getInt("TOT_PREGUNTAS"));
		con.setSumGrupo(rs.getInt("SUMA_GRUPO"));
		con.setPonTot(rs.getDouble("FCPONDTOTAL"));
		con.setPrecalif(rs.getDouble("PRECALIFICACION"));
		con.setCalif(rs.getDouble("CALIFICACION"));
		con.setAux2(rs.getInt("IMPERDONABLE"));
		//con.setTotGral(rs.getDouble("TOTALGRAL"));
		return con;
	}
	
}
