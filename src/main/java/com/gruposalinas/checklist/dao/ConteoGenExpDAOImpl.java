package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ConteoGenExpDTO;
import com.gruposalinas.checklist.mappers.ConteoGenExpCheckRowMapper;
import com.gruposalinas.checklist.mappers.ConteoGenExpPregRowMapper;
import com.gruposalinas.checklist.mappers.ConteoGenExpRespRowMapper;
import com.gruposalinas.checklist.mappers.FirmaCheckRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ConteoGenExpDAOImpl extends DefaultDAO implements ConteoGenExpDAO {

    private static Logger logger = LogManager.getLogger(ConteoGenExpDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtienedatos;
    DefaultJdbcCall jdbcObtieneRepoSemanal;

    DefaultJdbcCall jdbcCursoresCecoFaseProyecto;

    public void init() {

        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGCONTGEN")
                .withProcedureName("SPCONT_BITGEN")
                .returningResultSet("RCL_FIRMA", new FirmaCheckRowMapper());

        jdbcObtienedatos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGCONTGEN")
                .withProcedureName("SPCONT_BITGEN")
                .returningResultSet("RCL_PREG", new ConteoGenExpPregRowMapper())
                .returningResultSet("RCL_RESP", new ConteoGenExpRespRowMapper())
                .returningResultSet("RCL_CHECKS", new ConteoGenExpCheckRowMapper());

        jdbcObtieneRepoSemanal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOFECHAS")
                .withProcedureName("SP_PREG_RESP")
                .returningResultSet("RCL_PREG", new ConteoGenExpPregRowMapper())
                .returningResultSet("RCL_RESP", new ConteoGenExpRespRowMapper());

        jdbcCursoresCecoFaseProyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEXPNVOFLUJO")
                .withProcedureName("SPCONT_BITGEN")
                .returningResultSet("RCL_PREG", new ConteoGenExpPregRowMapper())
                .returningResultSet("RCL_RESP", new ConteoGenExpRespRowMapper())
                .returningResultSet("RCL_CHECKS", new ConteoGenExpCheckRowMapper());

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<ConteoGenExpDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<ConteoGenExpDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneTodo.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGCONTGEN.SPCONT_BITGEN}");

        listaFijo = (List<ConteoGenExpDTO>) out.get("RCL_FIRMA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las firmas");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //parametro

    @SuppressWarnings("unchecked")
    public Map<String, Object> obtieneDatos(String bita, String clasifica) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        int error = 0;
        List<ConteoGenExpDTO> preguntas = null;
        List<ConteoGenExpDTO> respuestas = null;
        List<ConteoGenExpDTO> checks = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bita)
                .addValue("PA_CLASIF", clasifica);

        out = jdbcObtienedatos.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGCONTGEN.SP_SEL_FIRMACE}");
        //lleva el nombre del cursor del procedure
        preguntas = (List<ConteoGenExpDTO>) out.get("RCL_PREG");
        respuestas = (List<ConteoGenExpDTO>) out.get("RCL_RESP");
        checks = (List<ConteoGenExpDTO>) out.get("RCL_CHECKS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion de padres e hijas");
        } else {
            lista = new HashMap<String, Object>();
            lista.put("preguntas", preguntas);
            lista.put("respuestas", respuestas);
            lista.put("checks", checks);

            return lista;

        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> obtieneDatosRepoSem(String bitaGral) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        int error = 0;
        List<ConteoGenExpDTO> preguntas = null;
        List<ConteoGenExpDTO> respuestas = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bitaGral);

        out = jdbcObtieneRepoSemanal.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPOFECHAS.SP_PREG_RESP}");
        //lleva el nombre del cursor del procedure
        preguntas = (List<ConteoGenExpDTO>) out.get("RCL_PREG");
        respuestas = (List<ConteoGenExpDTO>) out.get("RCL_RESP");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion de padres e hijas");
        } else {
            lista = new HashMap<String, Object>();
            lista.put("preguntas", preguntas);
            lista.put("respuestas", respuestas);

            return lista;

        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> obtieneDatosConteoGral(String ceco, String fase, String proyecto, String clasifica) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        int error = 0;
        List<ConteoGenExpDTO> preguntas = null;
        List<ConteoGenExpDTO> respuestas = null;
        List<ConteoGenExpDTO> checks = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto)
                .addValue("PA_CLASIF", clasifica);

        out = jdbcCursoresCecoFaseProyecto.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAEXPNVOFLUJO.SPCONT_BITGEN}");
        //lleva el nombre del cursor del procedure
        preguntas = (List<ConteoGenExpDTO>) out.get("RCL_PREG");
        respuestas = (List<ConteoGenExpDTO>) out.get("RCL_RESP");
        checks = (List<ConteoGenExpDTO>) out.get("RCL_CHECKS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion de padres e hijas");
        } else {
            lista = new HashMap<String, Object>();
            lista.put("preguntas", preguntas);
            lista.put("respuestas", respuestas);
            lista.put("checks", checks);

            return lista;

        }
        return lista;

    }

}
