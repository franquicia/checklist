<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link href="../css/menuCheck.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../css/v2/estilos.css" />
<link rel="stylesheet" type="text/css" href="../css/v2/dropkick.css" />
<script src="../js/script-menu.js"></script>

<div class="menu-taps">

	<div class="contenedor2">

		<div id="tab-container" class="tab-container"
			style="margin-top: 30px;">

			<ul class='etabs'>

				<li class='tab'><a href="#tabs-resumen"> <img
						src="../images/v2/btn-resumen.png"
						id="resumen-verde" /> <img
						src="../images/v2/btn-resumen_over.png"
						id="resumen-hover" style="display: none" /> Resumen

				</a></li>


				<li class='tab' onclick="diseñar()">
					<!--<form name="formulario" method="POST" action="../central/checklistDesignF1.htm" id="formEnvia">

<input type="hidden" name="idChecklist" value="0">

--> <a href="#tabs-diseniar">

						<form name="formulario" method="POST" action="../central/checklistDesignF1.htm" id="formEnvia">

							<input type="hidden" name="idChecklist" value="0"> <img
								src="../images/v2/iconDisenarOff.svg"
								id="diseniar-verde" /> <img
								src="../images/v2/btn-disenar_over.png"
								id="diseniar-hover" style="display: none" /> Diseñar

						</form>

				</a> <!--</form>-->

				</li>



				<li class='tab'><a href="#tabs-recopilar"> <img
						src="../images/v2/btn-recopilar.png"
						id="recopilar-verde" /> <img
						src="../images/v2/btn-recopilar-hover.png"
						id="recopilar-hover" style="display: none" /> Recopilar

				</a></li>


				<li class='tab'><a href="#tabs-visualizar"> <img
						src="../images/v2/btn-visualizar.png"
						id="visualizar-verde" /> <img
						src="../images/v2/btn-visualizar_over.png"
						id="visualizar-hover" style="display: none" /> Visualizar

				</a></li>



			</ul>

			<div id="sombra">
				<img src="../images/v2/sombra.png" />
			</div>

</div>
</div>
</div>


			<!-- Fin del control de tabs *************************************************************************** -->