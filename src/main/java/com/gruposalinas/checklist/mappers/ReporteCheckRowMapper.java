package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteDTO;

public class ReporteCheckRowMapper implements RowMapper<ReporteDTO>  {

	@Override
	public ReporteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		ReporteDTO reporteDTO = new ReporteDTO();
		
		reporteDTO.setIdCheckUsua(rs.getInt("FIID_CHECK_USUA"));
		reporteDTO.setIdTerritorio(rs.getString("FITERRITORIO"));
		reporteDTO.setTerritorio(rs.getString("FCTERRITORIO"));
		reporteDTO.setIdZona(rs.getString("FIZONA"));
		reporteDTO.setZona(rs.getString("FCZONA"));
		reporteDTO.setIdRegional(rs.getString("FIREGIONAL"));
		reporteDTO.setRegional(rs.getString("FCREGIONAL"));
		reporteDTO.setSucursal(rs.getString("FCSUCURSAL"));
		reporteDTO.setNombreSucursal(rs.getString("NOMBRESUC"));
		reporteDTO.setIdChecklist(rs.getString("FIID_CHECKLIST"));
		reporteDTO.setNombreChecklist(rs.getString("CKLNOMBRE"));
		reporteDTO.setIdUsuario(rs.getString("FIID_USUARIO"));
		reporteDTO.setNombreUsuario(rs.getString("FCNOMBRE"));
		reporteDTO.setIdPuesto(rs.getInt("FIID_PUESTO"));
		reporteDTO.setPuesto(rs.getString("FCDESCRIPCION"));
		reporteDTO.setIdCeco(rs.getString("FCID_CECO"));
		
		return reporteDTO;
	}

}
