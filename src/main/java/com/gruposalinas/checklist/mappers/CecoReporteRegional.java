package com.gruposalinas.checklist.mappers;

import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteDTO;


public class CecoReporteRegional implements RowMapper<ReporteDTO> {

	public ReporteDTO mapRow(java.sql.ResultSet rs, int rowNum) throws SQLException {
		
		ReporteDTO reporteDTO = new ReporteDTO();

		reporteDTO.setIdCeco(rs.getString("FCID_CECO"));
		reporteDTO.setNombreCeco(rs.getString("FCNOMBRE"));
		reporteDTO.setIdTerritorio(rs.getString("TERRITORIO"));
		reporteDTO.setTerritorio(rs.getString("TERRITORIO2"));
		reporteDTO.setIdZona(rs.getString("ZONA"));
		reporteDTO.setZona(rs.getString("ZONA2"));
		reporteDTO.setIdRegional(rs.getString("REGION"));
		reporteDTO.setRegional(rs.getString("REGION2"));
		
		return reporteDTO;
	}

}
