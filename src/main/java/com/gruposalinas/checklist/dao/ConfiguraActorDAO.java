package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.ConfiguraActorDTO;


public interface ConfiguraActorDAO {

	  public int insertaConfActor(ConfiguraActorDTO bean)throws Exception;
		
		public boolean eliminaConfActor(String idConfig)throws Exception;
		
		public List<ConfiguraActorDTO> obtieneDatosConfActor(String idConfig) throws Exception; 
		
		public boolean actualizaConfActor(ConfiguraActorDTO bean)throws Exception;
		
	}