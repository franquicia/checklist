package com.gruposalinas.checklist.domain;

public class TransformacionDTO {
	


		private int ceco;
		private int idUsuario;
		private int bitacora;
		private int recorrido;
		private String fechaini;
		private String fechafin;
		private String aux;
		private String periodo;
		
		public int getCeco() {
			return ceco;
		}
		public void setCeco(int ceco) {
			this.ceco = ceco;
		}
		public int getIdUsuario() {
			return idUsuario;
		}
		public void setIdUsuario(int idUsuario) {
			this.idUsuario = idUsuario;
		}
		public int getBitacora() {
			return bitacora;
		}
		public void setBitacora(int bitacora) {
			this.bitacora = bitacora;
		}
		public int getRecorrido() {
			return recorrido;
		}
		public void setRecorrido(int recorrido) {
			this.recorrido = recorrido;
		}
		public String getFechaini() {
			return fechaini;
		}
		public void setFechaini(String fechaini) {
			this.fechaini = fechaini;
		}
		public String getFechafin() {
			return fechafin;
		}
		public void setFechafin(String fechafin) {
			this.fechafin = fechafin;
		}
		public String getAux() {
			return aux;
		}
		public void setAux(String aux) {
			this.aux = aux;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		@Override
		public String toString() {
			return "TransformacionDTO [ceco=" + ceco + ", idUsuario=" + idUsuario + ", bitacora=" + bitacora
					+ ", recorrido=" + recorrido + ", fechaini=" + fechaini + ", fechafin=" + fechafin + ", aux=" + aux
					+ ", periodo=" + periodo + "]";
		}
		
		
	}

