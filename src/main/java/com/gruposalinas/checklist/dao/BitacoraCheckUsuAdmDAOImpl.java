package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.ChecklistUsuarioDTO;
import com.gruposalinas.checklist.mappers.BitacoraRowMapper;
import com.gruposalinas.checklist.mappers.BitacorasCerradasRowMapper;
import com.gruposalinas.checklist.mappers.CheckUsuaGRowMapper;
import com.gruposalinas.checklist.mappers.ChecklistUsuarioRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class BitacoraCheckUsuAdmDAOImpl extends DefaultDAO implements BitacoraCheckUsuAdmDAO {

    private static Logger logger = LogManager.getLogger(BitacoraCheckUsuAdmDAOImpl.class);

    private DefaultJdbcCall insertaBitacora;
    private DefaultJdbcCall actualizaBitacora;
    private DefaultJdbcCall eliminaBitacora;
    private DefaultJdbcCall buscaBitacora;
    private DefaultJdbcCall buscaBitacoras;
    private DefaultJdbcCall verificadIdBitacora;
    private DefaultJdbcCall cierraBitacora;
    private DefaultJdbcCall cierreBitacoras;
    private DefaultJdbcCall cierreBitacorasR;

    //CHECK USUA
    DefaultJdbcCall jdbcObtieneCheckUsuario;
    DefaultJdbcCall jdbcObtieneCheckU;
    DefaultJdbcCall jdbcObtieneCheckUsua;
    DefaultJdbcCall jdbcInsertaCheckUsuario;
    DefaultJdbcCall jdbcActualizaCheckUsuario;
    DefaultJdbcCall jdbcEliminaCheckUsuario;
    DefaultJdbcCall jdbcDesactivaChecklist;
    DefaultJdbcCall jdbcDesactivaChecklUsua;
    DefaultJdbcCall jdbcInsertaEspecial;

    public void init() {
        insertaBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_BITACORAAAMD")
                .withProcedureName("SP_INS_BITACORA");

        actualizaBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_BITACORAAAMD")
                .withProcedureName("SP_ACT_BITACORA_S");

        eliminaBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_BITACORAAAMD")
                .withProcedureName("SP_DEL_BITACORA");

        buscaBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_BITACORAAAMD")
                .withProcedureName("SP_SEL_BITACORA")
                .returningResultSet("RCL_BITACORA", new BitacoraRowMapper());

        buscaBitacoras = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_BITACORAAAMD")
                .withProcedureName("SP_SEL_G_BIT")
                .returningResultSet("RCL_BITACORA", new BitacoraRowMapper());

        verificadIdBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_BITACORA")
                .withProcedureName("SP_OBTIENE_ID");

        cierraBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_BITACORA")
                .withProcedureName("SPCIERREBITACORAS");

        cierreBitacoras = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_BITACORA")
                .withProcedureName("SP_OBTIENE_CERRADAS")
                .returningResultSet("RCL_BITACORA", new BitacorasCerradasRowMapper());

        cierreBitacorasR = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_BITACORA")
                .withProcedureName("SP_OBTIENE_CERRADAS_R")
                .returningResultSet("RCL_BITACORA", new BitacorasCerradasRowMapper());

        //CHECK USUA
        jdbcObtieneCheckUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECK_USUAADM")
                .withProcedureName("SP_SEL_CHECK_USUA")
                .returningResultSet("RCL_CHECK_USUA", new ChecklistUsuarioRowMapper());

        jdbcObtieneCheckU = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECK_USUAADM")
                .withProcedureName("SP_SEL_G_CKU")
                .returningResultSet("RCL_CHECK_USUA", new ChecklistUsuarioRowMapper());

        jdbcObtieneCheckUsua = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECK_USUAADM")
                .withProcedureName("SP_SEL_CK_USUA")
                .returningResultSet("RCL_CHECK_USUA", new CheckUsuaGRowMapper());

        jdbcInsertaCheckUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECK_USUAADM")
                .withProcedureName("SP_INS_CHECK_USUA");

        jdbcActualizaCheckUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECK_USUAADM")
                .withProcedureName("SP_ACT_CHECK_USUA");

        jdbcEliminaCheckUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECK_USUAADM")
                .withProcedureName("SP_DEL_CHECK_USUA");

        jdbcDesactivaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNAESP")
                .withProcedureName("SP_DESACTIVACK");

        jdbcDesactivaChecklUsua = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECK_USUAADM")
                .withProcedureName("SP_ACT_CHECK_ACT");

        jdbcInsertaEspecial = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNAESP")
                .withProcedureName("SP_INSERTACHECKUSUA");

    }

    public int insertaBitacora(BitacoraDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idBitacora = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_CHECK_USA", bean.getIdCheckUsua())
                .addValue("PA_FIMODO", bean.getModo());
        out = insertaBitacora.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_BITACORAAAMD.SP_INS_BITA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDBITACORA");
        idBitacora = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar Bitacora");
        }

        return idBitacora;
    }

    public boolean actualizaBitacora(BitacoraDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_CHECK_USA", bean.getIdCheckUsua())
                .addValue("PA_FCLONGITUD", bean.getLongitud())
                .addValue("PA_FCLATITUD", bean.getLatitud())
                .addValue("PA_FIID_BITACORA", bean.getIdBitacora())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_FIMODO", bean.getModo())
                .addValue("PA_FECHA_TERMINO", bean.getFechaFin());

        out = actualizaBitacora.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_BITACORAAAMD.SP_ACT_BITACORA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Bitacora");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaBitacora(int idBitacora) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_BITACORA", idBitacora);

        out = eliminaBitacora.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_BITACORAAAMD.SP_DEL_BITACORA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Bitacora");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<BitacoraDTO> buscaBitacora() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<BitacoraDTO> listBitacora = null;

        out = buscaBitacoras.execute();

        logger.info("Funcion ejecutada:{checklist.PA_BITACORAAAMD.SP_SEL_G_BIT}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listBitacora = (List<BitacoraDTO>) out.get("RCL_BITACORA");

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Bitacora");
            return null;
        } else {
            return listBitacora;
        }
    }

    @SuppressWarnings("unchecked")
    public List<BitacoraDTO> buscaBitacora(String checkUsua, String idBitacora, String fechaI, String fechaF) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<BitacoraDTO> listBitacora = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FID_CHECK_USUA", checkUsua)
                .addValue("PA_IDBITACORA", idBitacora)
                .addValue("PA_FECHA_I", fechaI)
                .addValue("PA_FECHA_F", fechaF);

        out = buscaBitacora.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_BITACORAAAMD.SP_SEL_BITACORA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listBitacora = (List<BitacoraDTO>) out.get("RCL_BITACORA");

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Bitacora");
        } else {
            return listBitacora;
        }

        return null;
    }

    public int verificaIdBitacora(int checkUsua) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idBitacora = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCHECKUSA", checkUsua);

        out = verificadIdBitacora.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_BITACORA.SP_OBTIENE_ID}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDBITACORA");
        idBitacora = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Bitacora");
        }

        return idBitacora;

    }

    @Override
    public boolean cierraBitacora() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = cierraBitacora.execute();

        logger.info("Funcion ejecutada:{checklist.PA_BITACORA.SPCIERREBITACORAS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al terminar las Bitacoras");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<BitacoraDTO> buscaBitacoraCerradas(String idChecklist) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<BitacoraDTO> listBitacora = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", idChecklist);

        out = cierreBitacoras.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_BITACORA.SP_OBTIENE_CERRADAS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        listBitacora = (List<BitacoraDTO>) out.get("RCL_BITACORA");

        if (error == 1) {
            logger.info("Algo ocurrió al consultar la Bitacora");
        } else {
            return listBitacora;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<BitacoraDTO> buscaBitacoraCerradasR(String idChecklist) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<BitacoraDTO> listBitacora = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", idChecklist);

        out = cierreBitacorasR.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_BITACORA.SP_OBTIENE_CERRADAS_R}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        listBitacora = (List<BitacoraDTO>) out.get("RCL_BITACORA");

        if (error == 1) {
            logger.info("Algo ocurrió al consultar la Bitacora");
        } else {
            return listBitacora;
        }

        return null;
    }
//CHECK USUA

    @SuppressWarnings("unchecked")
    public List<ChecklistUsuarioDTO> obtieneCheckUsuario(int idCheckU) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ChecklistUsuarioDTO> listaCheck = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FID_CHECK_USUA", idCheckU);

        out = jdbcObtieneCheckUsuario.execute(in);

        logger.info("Funcion ejecutada: {checklist.PACHECK_USUAADM.SP_SEL_CHECK_USUA}");

        listaCheck = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_USUA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Checklist Usuario con id(" + idCheckU + ")");
        } else {
            return listaCheck;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistUsuarioDTO> obtieneCheckU(String idUsuario) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ChecklistUsuarioDTO> listaCheck = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", idUsuario);

        out = jdbcObtieneCheckU.execute(in);

        logger.info("Funcion ejecutada: {checklist.PACHECK_USUAADM.SP_SEL_CHECK_USUA}");

        listaCheck = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_USUA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Checklist Usuario con id Usuario(" + idUsuario + ")");
        } else {
            return listaCheck;
        }

        return null;
    }

    public int insertaCheckUsuario(ChecklistUsuarioDTO bean) throws Exception {
        Map<String, Object> out = new HashMap<String, Object>();

        int error = 0;
        int idCheckUsua = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_CHECKLIST", bean.getIdChecklist())
                .addValue("PA_FIID_USUARIO", bean.getIdUsuario())
                .addValue("PA_FCID_CECO", bean.getIdCeco())
                .addValue("PA_FIACTIVO", bean.getActivo())
                .addValue("PA_FDFECHA_INICIO", bean.getFechaIni())
                .addValue("PA_FDFECHA_RES", bean.getFechaResp());

        out = jdbcInsertaCheckUsuario.execute(in);

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_FID_CHECK_USUA");
        idCheckUsua = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Checklist Usuario");
        } else {
            return idCheckUsua;
        }

        return idCheckUsua;
    }

    public boolean actualizaCheckUsuario(ChecklistUsuarioDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FID_CHECK_USUA", bean.getIdCheckUsuario())
                .addValue("PA_FIACTIVO", bean.getActivo());

        out = jdbcActualizaCheckUsuario.execute(in);

        logger.info("Funcion ejecutada: {checklist.PACHECK_USUAADM.SP_ACT_CHECK_USUA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el Checklist Usuario  id( " + bean.getIdCheckUsuario() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaCheckUsuario(int idCheckU) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FID_CHECK_USUA", idCheckU);

        out = jdbcEliminaCheckUsuario.execute(in);

        logger.info("Funcion ejecutada: {checklist.PACHECK_USUAADM.SP_DEL_CHECK_USUA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar Checklist Usuario id(" + idCheckU + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ChecklistUsuarioDTO> obtieneCheckUsua(String idUsuario, String ceco, String fechaInicio) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ChecklistUsuarioDTO> listaCheck = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USUARIO", idUsuario)
                .addValue("PA_CECO", ceco).addValue("PA_FECHA", fechaInicio);

        out = jdbcObtieneCheckUsua.execute(in);

        logger.info("Funcion ejecutada: {checklist.PACHECK_USUAADM.SP_SEL_CK_USUA}");

        listaCheck = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_USUA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Checklist Usuario con id Usuario(" + idUsuario + ")");
        } else {
            return listaCheck;
        }

        return null;
    }

    public boolean desactivaChecklist(int idChecklist) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECKLIST", idChecklist);

        out = jdbcDesactivaChecklist.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAASIGNAESP.SP_DESACTIVACK}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al descativar el checklist id(" + idChecklist + ")");
        } else {
            return true;
        }

        return false;

    }

    public boolean desactivaCheckUsua(int idCeco, int idChecklist, int idUsu) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FCID_CHECKLIST", idChecklist).addValue("PA_FCID_CECO", idCeco)
                .addValue("PA_USUARIO", idUsu);

        out = jdbcDesactivaChecklUsua.execute(in);

        logger.info("Funcion ejecutada: {checklist.PACHECK_USUAADM.SP_ACT_CHECK_ACT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        if (ejecucion == 1) {
            logger.info("Algo ocurrió al descativar los check usuarios");
        } else {
            return true;
        }

        return false;

    }

    public boolean insertaCheckusuaEspecial(int idUsuario, int idChecklist, String ceco) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECKLIST", idChecklist).addValue("PA_USUARIO", idUsuario).addValue("PA_CECO", ceco);

        out = jdbcInsertaEspecial.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAASIGNAESP.SP_INSERTACHECKUSUA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al insertar checklist-usuario idcheck: " + idChecklist + " idUsuario: " + idUsuario + " ceco: " + ceco);
        } else {
            return true;
        }

        return false;
    }

}
