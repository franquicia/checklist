<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="../js/jquery/jquery-1.8.0.min.js"></script>    

<script src="../js/script-storeIndex.js"></script>    
<link rel="stylesheet" type="text/css" href="../css/css-storeIndex.css"></link>   

<title>Store Index</title>

</head>
<body>

<div class="checklists">
		<div class="header">
			<div class="tblHeader">
				 <!--  
				 <div class="headerMenu">
				 	<a href="#"  id="hamburger">
						<img src="../images/btn_hamburguer.svg" alt="Menú principal" class="imgHamburgesa">
					</a>
				 </div>
				 -->
				 <div class="headerLogo">
						<a href="#">
							<img src="../images/logo-baz.png"  id="imgLogo">
						</a>
				</div>
			</div>
			<!--  
			 <div class="headerUser">
			 	<div class="name">	
					Empleado Ambiente Desarrollo</br>
					Central
				</div>
			</div>
			-->
		</div>
		
</div>

<br>
<br>
<br>

<div class="inputData">
	<form:form method="POST" id="sendForm" enctype="multipart/form-data"
			action="../central/store.htm">
			<table>
				<tr>
					<td><form:label path="usuario">Usuario:</form:label><br><br>
						<form:input path="usuario" id="usuario" style="height: 40px; width: 450px; text-align: center; 	font-size: 20px;
						"/></td>
				</tr>
				
				<tr>
					<td><form:label path="llave">Token:</form:label><br><br>
						<form:input path="llave" id="llave" type="password" style="height: 40px; width: 450px; text-align: center; 	font-size: 20px;"/></td>
				</tr>
				
				<tr>
				<br><br><br><br>
					<td colspan="2">
					<input style="height: 40px; width: 450px; text-align: center; font-size: 20px;" type="submit" value="Aceptar"  onClick="return valida();" /></td>
				</tr>
			</table>
		</form:form>
		<br><br>
			<div class="error" id="error">${response}</div>
		
</div>


<div class="footer">
		<table class="tblFooter">
		  <tbody>
			<tr>
			  <td>Banco Azteca S.A. Institución de Banca Múltiple</td>
			  <td>Derechos Reservados 2014 (Términos y Condiciones de uso del Portal.</td>
			</tr>
		  </tbody>
		</table>
	</div>

</body>
</html>