package com.gruposalinas.checklist.domain;

public class ConsultaAseguradorDTO {
	

	private int idUsuario;
	private int idBitacora;
	private int idCeco;
	private String nomCeco;
	private String nomUsuario;
	private String fechaInicio;
	private String fechaTermino;
	private int idChecklist;
	private String nomCheckList;
	
	
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	
	public String getNomCheckList() {
		return nomCheckList;
	}
	public void setNomCheckList(String nomCheckList) {
		this.nomCheckList = nomCheckList;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public String getNomUsuario() {
		return nomUsuario;
	}
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public int getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(int idCeco) {
		this.idCeco = idCeco;
	}
	public String getNomCeco() {
		return nomCeco;
	}
	public void setNomCeco(String nomCeco) {
		this.nomCeco = nomCeco;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaTermino() {
		return fechaTermino;
	}
	public void setFechaTermino(String fechaTermino) {
		this.fechaTermino = fechaTermino;
	}
	@Override
	public String toString() {
		return "ConsultaAseguradorDTO [idUsuario=" + idUsuario + ", idBitacora=" + idBitacora + ", idCeco=" + idCeco
				+ ", nomCeco=" + nomCeco + ", nomUsuario=" + nomUsuario + ", fechaInicio=" + fechaInicio
				+ ", fechaTermino=" + fechaTermino + ", idChecklist=" + idChecklist + ", nomCheckList=" + nomCheckList
				+ "]";
	}
	
}
	



