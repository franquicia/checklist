package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteDTO;

public class ReporteDetRegSupRowMapper implements RowMapper<ReporteDTO>    {
	@Override
	public ReporteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		ReporteDTO reporteDTO = new ReporteDTO();

		reporteDTO.setIdCeco(rs.getString("FCID_CECO"));
		reporteDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		reporteDTO.setNombreCeco(rs.getString("FCNOMBRE"));  
		reporteDTO.setNumero(rs.getInt("NUMERO"));
		reporteDTO.setDia(rs.getString("DIA"));
		reporteDTO.setSemana(rs.getString("DIAFECHA"));
		reporteDTO.setTotal(rs.getInt("CONTEO"));
		//reporteDTO.setSemana(rs.getString("SEMANA"));
		reporteDTO.setImagen(rs.getString("IMAGEN"));
		
		return reporteDTO;
	}
}
