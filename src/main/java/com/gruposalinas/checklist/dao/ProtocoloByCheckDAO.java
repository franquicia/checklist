package com.gruposalinas.checklist.dao;

import java.util.List;
import com.gruposalinas.checklist.domain.ProtocoloByCheckDTO;

public interface ProtocoloByCheckDAO {

	public List<ProtocoloByCheckDTO> obtieneProtocolo (int idChecklist) throws Exception;
	
}
