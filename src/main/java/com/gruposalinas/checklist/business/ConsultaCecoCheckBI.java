package com.gruposalinas.checklist.business;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.ConsultaCecoCheckDAO;
import com.gruposalinas.checklist.domain.ConsultaCecoCheckDTO;
import com.gruposalinas.checklist.domain.ConsultaCecoCheckDTO2;


public class ConsultaCecoCheckBI {

	private static Logger logger = LogManager.getLogger(ConsultaCecoCheckBI.class);

private List<ConsultaCecoCheckDTO> listafila;
private Map<String, Object> listaComp;
	
	@Autowired
	ConsultaCecoCheckDAO consultaCecoCheckDAO;
		

	//con parametro
	public List<ConsultaCecoCheckDTO> obtieneDatos( int idUsuario) {
		
		try {
			listafila =  consultaCecoCheckDAO.obtieneDatos(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<ConsultaCecoCheckDTO> obtieneInfo() {

		try {
			listafila = consultaCecoCheckDAO.obtieneInfo();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	public Map<String, Object> obtieneDato()	{
		try {
			listaComp=consultaCecoCheckDAO.obtieneDato();
		}catch(Exception e) {
		logger.info("No Fue posible Obtener la informacion");
		UtilFRQ.printErrorLog(null,e);
		}
		return  listaComp;
		}
		
	//con parametro
	
	public Map<String, Object> obtieneDat(int idUsuario) throws Exception{
		try {
			listaComp=consultaCecoCheckDAO.obtieneDat(idUsuario);
		}catch(Exception e) {
			logger.info("No fue posible Obtener informacion");
			UtilFRQ.printErrorLog(null, e);
		}
		return  listaComp;
	}
	

}