package com.gruposalinas.checklist.domain;

public class FaseFirmasDTO {
	


		private int idFirma;
		private String idSoft;
		private String ceco;
		private String idUsuario;
		private String puesto;
		private String responsable;
		private String correo;
		private String nombre;
		private String ruta;
		private String observ;
		private String periodo;
		private int idAgrupa;
		private int idFase;
		private int idProyecto;
		private int idOrdenFaFse;
		private String nombreFase;
		private String detalleFase;
		public int getIdFirma() {
			return idFirma;
		}
		public void setIdFirma(int idFirma) {
			this.idFirma = idFirma;
		}
		public String getIdSoft() {
			return idSoft;
		}
		public void setIdSoft(String idSoft) {
			this.idSoft = idSoft;
		}
		public String getCeco() {
			return ceco;
		}
		public void setCeco(String ceco) {
			this.ceco = ceco;
		}
		public String getIdUsuario() {
			return idUsuario;
		}
		public void setIdUsuario(String idUsuario) {
			this.idUsuario = idUsuario;
		}
		public String getPuesto() {
			return puesto;
		}
		public void setPuesto(String puesto) {
			this.puesto = puesto;
		}
		public String getResponsable() {
			return responsable;
		}
		public void setResponsable(String responsable) {
			this.responsable = responsable;
		}
		public String getCorreo() {
			return correo;
		}
		public void setCorreo(String correo) {
			this.correo = correo;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getRuta() {
			return ruta;
		}
		public void setRuta(String ruta) {
			this.ruta = ruta;
		}
		public String getObserv() {
			return observ;
		}
		public void setObserv(String observ) {
			this.observ = observ;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		public int getIdAgrupa() {
			return idAgrupa;
		}
		public void setIdAgrupa(int idAgrupa) {
			this.idAgrupa = idAgrupa;
		}
		public int getIdFase() {
			return idFase;
		}
		public void setIdFase(int idFase) {
			this.idFase = idFase;
		}
		public int getIdProyecto() {
			return idProyecto;
		}
		public void setIdProyecto(int idProyecto) {
			this.idProyecto = idProyecto;
		}
		public int getIdOrdenFaFse() {
			return idOrdenFaFse;
		}
		public void setIdOrdenFaFse(int idOrdenFaFse) {
			this.idOrdenFaFse = idOrdenFaFse;
		}
		public String getNombreFase() {
			return nombreFase;
		}
		public void setNombreFase(String nombreFase) {
			this.nombreFase = nombreFase;
		}
		public String getDetalleFase() {
			return detalleFase;
		}
		public void setDetalleFase(String detalleFase) {
			this.detalleFase = detalleFase;
		}
		@Override
		public String toString() {
			return "FaseFirmasDTO [idFirma=" + idFirma + ", idSoft=" + idSoft + ", ceco=" + ceco + ", idUsuario="
					+ idUsuario + ", puesto=" + puesto + ", responsable=" + responsable + ", correo=" + correo
					+ ", nombre=" + nombre + ", ruta=" + ruta + ", observ=" + observ + ", periodo=" + periodo
					+ ", idAgrupa=" + idAgrupa + ", idFase=" + idFase + ", idProyecto=" + idProyecto + ", idOrdenFaFse="
					+ idOrdenFaFse + ", nombreFase=" + nombreFase + ", detalleFase=" + detalleFase + "]";
		}
		
		
		
			
	}