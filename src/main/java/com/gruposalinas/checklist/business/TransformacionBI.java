package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.TransformacionDAO;
import com.gruposalinas.checklist.domain.TransformacionDTO;

public class TransformacionBI {

	private static Logger logger = LogManager.getLogger(TransformacionBI.class);

private List<TransformacionDTO> listafila;
	
	@Autowired
	TransformacionDAO transformacionDAO;
		
	
	public int inserta(TransformacionDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = transformacionDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	public int insertaMan(TransformacionDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = transformacionDAO.insertaMan(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean elimina(String ceco,String recorrido){
		boolean respuesta = false;
		
		try {
			respuesta = transformacionDAO.elimina(ceco,recorrido);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<TransformacionDTO> obtieneDatos( String ceco) {
		
		try {
			listafila =  transformacionDAO.obtieneDatos(ceco);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
		
	public boolean actualiza(TransformacionDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = transformacionDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	

}