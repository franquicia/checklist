package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.PosiblesTipoPreguntaDTO;

public class PosibleTipoPreguntaRowMapper implements RowMapper<PosiblesTipoPreguntaDTO>{

	@Override
	public PosiblesTipoPreguntaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		PosiblesTipoPreguntaDTO posibleTipoPregunta = new PosiblesTipoPreguntaDTO();
		
		posibleTipoPregunta.setIdPosibleTipoPregunta(rs.getInt("FIIDPOSIPREG"));
		posibleTipoPregunta.setIdTipoPregunta(rs.getInt("FIID_TIPO_PREG"));
		posibleTipoPregunta.setIdPosibleRespuesta(rs.getInt("FIIDPOSIBLE"));
		posibleTipoPregunta.setDescripcionPosible(rs.getString("FCDESCRIPCION"));
		
		return posibleTipoPregunta;
	}
	

}
