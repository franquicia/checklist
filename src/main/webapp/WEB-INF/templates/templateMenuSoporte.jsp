<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 

 
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<nav class="container-fluid">
			<div class="navbar-header">
				<c:url value="/soporte/inicio.htm" var="inicio" />
	  			<a class="navbar-brand" href="${inicio}">Soporte</a>
	  			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" >
	  			<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
	  		</div>
	  		
	  		
	  		
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<c:url value="/soporte/validaSucursal.htm" var="validaSucursalUrl" />
						<c:url value="/soporte/altaSucursal.htm" var="altaSucursalUrl" />
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sucursales<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="${validaSucursalUrl}">Validar sucursal</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="${altaSucursalUrl}">Dar de alta sucursal</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<c:url value="/soporte/altaUserChecklist.htm" var="altaUserChecklistUrl" />
						<c:url value="/soporte/bajaUserChecklist.htm" var="bajaUserChecklistUrl" />
						<c:url value="/soporte/moficaChecklistEstatus.htm" var="estatusChecklist" />
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Checklist<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="${altaUserChecklistUrl}">Dar de alta usuario</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="${bajaUserChecklistUrl}">Dar de baja usuario</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="${estatusChecklist}">Cambiar Estatus Checklist</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<c:url value="/soporte/altaCeco.htm" var="altaCecoUrl" />
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CECO<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="${altaCecoUrl}">Dar de alta CECO</a></li>
						</ul>
					</li>
					<c:url value="/soporte/fileCenter.htm" var="fileCenterUrl" />
					<li><a href="${fileCenterUrl}">File center</a></li>
					<li class="dropdown">
						<c:url value="/soporte/listSucursal.htm" var="listSucursalUrl" />
						<c:url value="/soporte/listSucursalGCC.htm" var="listSucursalGCCUrl" />
						<c:url value="/soporte/listCheckUser.htm" var="listCheckUserUrl" />
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Listas<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="${listSucursalUrl}">Lista de Sucursales</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="${listSucursalGCCUrl}">Lista de Sucursales GCC</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="${listCheckUserUrl}">Lista de Checklist Usuario</a></li>
						</ul>
					</li>
					<c:url value="/soporte/listURL.htm" var="listUrl" />
					<li><a href="${listUrl}">Revisar URLs</a></li>
					<li class="dropdown">
						<c:url value="/soporte/listVersions.htm" var="listVersionsUrl" />
						<c:url value="/soporte/insertVersion.htm" var="insertVersionUrl" />
						<c:url value="/soporte/updateVersion.htm" var="updateVersionUrl" />
						<c:url value="/soporte/deleteVersion.htm" var="deleteVersionUrl" />
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Versiones<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="${listVersionsUrl}">Lista de versiones</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="${insertVersionUrl}">Agregar versión</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="${updateVersionUrl}">Actualizar versión</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="${deleteVersionUrl}">Eliminar versión</a></li>
						</ul>
					</li>
				</ul>
			</div>	
		</nav>
	</nav>
	