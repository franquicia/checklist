package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.SoftOpeningDTO;
import com.gruposalinas.checklist.mappers.SoftOpeningRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class SoftOpeningDAOImpl extends DefaultDAO implements SoftOpeningDAO {

    private static Logger logger = LogManager.getLogger(SoftOpeningDAOImpl.class);

    DefaultJdbcCall jdbcInsertaSoftOpen;
    DefaultJdbcCall jdbcInsertaSoftOpen2;
    DefaultJdbcCall jdbcInsertaManSoftOpen;
    DefaultJdbcCall jdbcEliminaSoftOpen;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneSoftOpen;
    DefaultJdbcCall jbdcActualizaSoftOpen;

    public void init() {

        jdbcInsertaSoftOpen = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSOFT")
                .withProcedureName("SP_INS_SOFT");

        jdbcInsertaSoftOpen2 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSOFT")
                .withProcedureName("SP_INS_SOFT");

        jdbcInsertaSoftOpen = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSOFT")
                .withProcedureName("SP_INS_SOFT");
        //inserta manual
        jdbcInsertaManSoftOpen = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSOFT")
                .withProcedureName("SP_INS_SOFTMAN");

        jdbcEliminaSoftOpen = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSOFT")
                .withProcedureName("SP_DEL_SOFT");

        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSOFT")
                .withProcedureName("SP_SEL_G")
                .returningResultSet("RCL_SOFT", new SoftOpeningRowMapper());

        jdbcObtieneSoftOpen = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSOFT")
                .withProcedureName("SP_SEL_CECO")
                .returningResultSet("RCL_SOFT", new SoftOpeningRowMapper());

        jbdcActualizaSoftOpen = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSOFT")
                .withProcedureName("SP_ACT_SOFT");

    }

    public int inserta(SoftOpeningDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bean.getBitacora());

        out = jdbcInsertaSoftOpen.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMSOFT.SP_INS_SOFT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0 || error == 3) {
            logger.info("Algo ocurrió al insertar registro de soft");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    @Override
    public int inserta2(int bitacora) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bitacora);

        out = jdbcInsertaSoftOpen2.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMSOFT.SP_INS_SOFT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0 || error == 3) {
            logger.info("Algo ocurrió al insertar registro de soft");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public int insertaMan(SoftOpeningDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_USUA", bean.getIdUsuario())
                .addValue("PA_BITA", bean.getBitacora())
                .addValue("PA_RECORRIDO", bean.getRecorrido())
                .addValue("PA_FECHAINI", bean.getFechaini())
                .addValue("PA_FECHAFIN", bean.getFechafin())
                .addValue("PA_AUX", bean.getAux());

        out = jdbcInsertaSoftOpen.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMSOFT.SP_INS_SOFTMAN}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el soft manual");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean elimina(String ceco, String recorrido) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco)
                .addValue("PA_RECORRIDO", recorrido);

        out = jdbcEliminaSoftOpen.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMSOFT.SP_DEL_SOFT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el soft del ceco: (" + ceco + ")");
        } else {
            return true;
        }

        return false;

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<SoftOpeningDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<SoftOpeningDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneTodo.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMSOFT.SP_SEL_G}");

        listaFijo = (List<SoftOpeningDTO>) out.get("RCL_SOFT");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los Empleados Fijos");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //parametro

    @SuppressWarnings("unchecked")
    public List<SoftOpeningDTO> obtieneDatos(int ceco) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<SoftOpeningDTO> listaN = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco);

        out = jdbcObtieneSoftOpen.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMSOFT.SP_SEL_CECO}");
        //lleva el nombre del cursor del procedure
        listaN = (List<SoftOpeningDTO>) out.get("RCL_SOFT");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener lod datos del ceco: (" + ceco + ")");
        }
        return listaN;

    }

    public boolean actualiza(SoftOpeningDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_USUA", bean.getIdUsuario())
                .addValue("PA_BITA", bean.getBitacora())
                .addValue("PA_RECORRIDO", bean.getRecorrido())
                .addValue("PA_FECHAINI", bean.getFechaini())
                .addValue("PA_FECHAFIN", bean.getFechafin())
                .addValue("PA_AUX", bean.getAux());

        out = jbdcActualizaSoftOpen.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMSOFT.SP_ACT_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getCeco() + ")");
        } else {
            return true;
        }
        return false;

    }

}
