package com.gruposalinas.checklist.business;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.RolesSupDAO;
import com.gruposalinas.checklist.domain.RolesSupDTO;

public class RolesSupBI {
		
	private static Logger logger = LogManager.getLogger(RolesSupBI.class);
	
	@Autowired
	RolesSupDAO rolesSupDAO;
	List<RolesSupDTO> listaConsulta = null;
	int respuesta = 0;

	public int insertRol(int idUsuario, int idPerfil, int idMenu, int activo){	
		try {
			respuesta = rolesSupDAO.insertRoles(idUsuario, idPerfil, idMenu, activo);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}			
		return respuesta;
	}

	public int updateRol(int idUsuario, int idPerfil, int idMenu, String activo){		
		try {
			respuesta = rolesSupDAO.updateRoles(idUsuario, idPerfil, idMenu, activo);
		} catch (Exception e) {
			logger.info(e);
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}			
		return respuesta;
	}

	public int deleteRol(int idUsuario, int idPerfil, int idMenu){
		try {
			respuesta = rolesSupDAO.deleteRoles(idUsuario, idPerfil, idMenu);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}
		return respuesta;
	}
	
	public List<RolesSupDTO> getRoles(String idUsuario, String idPerfil, String idMenu){		
		try {
			listaConsulta = rolesSupDAO.getRoles(idUsuario, idPerfil, idMenu);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}
		return listaConsulta;
	}
	
	public int mergeRol(int idUsuario, int idPerfil, int idMenu, int activo){
		try {
			respuesta = rolesSupDAO.mergeRoles(idUsuario, idPerfil, idMenu, activo);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}
		return respuesta;
	}

}
