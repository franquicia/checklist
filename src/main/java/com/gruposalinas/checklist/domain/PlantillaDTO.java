package com.gruposalinas.checklist.domain;

public class PlantillaDTO {

	private int idPlantilla;
	private String descripcion;
	private int idElemento;
	private String etiqueta;
	private int orden;
	private int obligatorio;
	private int idArchivo;
	public int getIdPlantilla() {
		return idPlantilla;
	}
	public void setIdPlantilla(int idPlantilla) {
		this.idPlantilla = idPlantilla;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getEtiqueta() {
		return etiqueta;
	}
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	public int getObligatorio() {
		return obligatorio;
	}
	public void setObligatorio(int obligatorio) {
		this.obligatorio = obligatorio;
	}
	public int getIdArchivo() {
		return idArchivo;
	}
	public void setIdArchivo(int idArchivo) {
		this.idArchivo = idArchivo;
	}
	public int getIdElemento() {
		return idElemento;
	}
	public void setIdElemento(int idElemento) {
		this.idElemento = idElemento;
	}
}
