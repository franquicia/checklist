/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.domain;

/**
 *
 * @author kramireza
 */
public class EKTPresentacion {

    Integer id_GRUPOEMP_PA;
    Integer id_GRUPOEMP;
    String nom_GRUPOEMP;
    Integer idestructura;
    String nomestructura;
    String ficcid_PA;
    Integer ficonfid_PA;
    String nom_CC_PA;
    String nom_GRUPOEMP_PA;
    Integer id_DIVISION_PA;
    String nom_DIVISION_PA;
    Integer id_ENTIDAD_PA;
    String nom_ENTIDAD_PA;
    String ficcid;
    Integer ficonfid;

    public Integer getId_GRUPOEMP_PA() {
        return id_GRUPOEMP_PA;
    }

    public void setId_GRUPOEMP_PA(Integer id_GRUPOEMP_PA) {
        this.id_GRUPOEMP_PA = id_GRUPOEMP_PA;
    }

    public Integer getId_GRUPOEMP() {
        return id_GRUPOEMP;
    }

    public void setId_GRUPOEMP(Integer id_GRUPOEMP) {
        this.id_GRUPOEMP = id_GRUPOEMP;
    }

    public String getNom_GRUPOEMP() {
        return nom_GRUPOEMP;
    }

    public void setNom_GRUPOEMP(String nom_GRUPOEMP) {
        this.nom_GRUPOEMP = nom_GRUPOEMP;
    }

    public Integer getIdestructura() {
        return idestructura;
    }

    public void setIdestructura(Integer idestructura) {
        this.idestructura = idestructura;
    }

    public String getNomestructura() {
        return nomestructura;
    }

    public void setNomestructura(String nomestructura) {
        this.nomestructura = nomestructura;
    }

    public String getFiccid_PA() {
        return ficcid_PA;
    }

    public void setFiccid_PA(String ficcid_PA) {
        this.ficcid_PA = ficcid_PA;
    }

    public Integer getFiconfid_PA() {
        return ficonfid_PA;
    }

    public void setFiconfid_PA(Integer ficonfid_PA) {
        this.ficonfid_PA = ficonfid_PA;
    }

    public String getNom_CC_PA() {
        return nom_CC_PA;
    }

    public void setNom_CC_PA(String nom_CC_PA) {
        this.nom_CC_PA = nom_CC_PA;
    }

    public String getNom_GRUPOEMP_PA() {
        return nom_GRUPOEMP_PA;
    }

    public void setNom_GRUPOEMP_PA(String nom_GRUPOEMP_PA) {
        this.nom_GRUPOEMP_PA = nom_GRUPOEMP_PA;
    }

    public Integer getId_DIVISION_PA() {
        return id_DIVISION_PA;
    }

    public void setId_DIVISION_PA(Integer id_DIVISION_PA) {
        this.id_DIVISION_PA = id_DIVISION_PA;
    }

    public String getNom_DIVISION_PA() {
        return nom_DIVISION_PA;
    }

    public void setNom_DIVISION_PA(String nom_DIVISION_PA) {
        this.nom_DIVISION_PA = nom_DIVISION_PA;
    }

    public Integer getId_ENTIDAD_PA() {
        return id_ENTIDAD_PA;
    }

    public void setId_ENTIDAD_PA(Integer id_ENTIDAD_PA) {
        this.id_ENTIDAD_PA = id_ENTIDAD_PA;
    }

    public String getNom_ENTIDAD_PA() {
        return nom_ENTIDAD_PA;
    }

    public void setNom_ENTIDAD_PA(String nom_ENTIDAD_PA) {
        this.nom_ENTIDAD_PA = nom_ENTIDAD_PA;
    }

    public String getFiccid() {
        return ficcid;
    }

    public void setFiccid(String ficcid) {
        this.ficcid = ficcid;
    }

    public Integer getFiconfid() {
        return ficonfid;
    }

    public void setFiconfid(Integer ficonfid) {
        this.ficonfid = ficonfid;
    }

    @Override
    public String toString() {
        return "EKTPresentacion{" + "id_GRUPOEMP_PA=" + id_GRUPOEMP_PA + ", id_GRUPOEMP=" + id_GRUPOEMP + ", nom_GRUPOEMP=" + nom_GRUPOEMP + ", idestructura=" + idestructura + ", nomestructura=" + nomestructura + ", ficcid_PA=" + ficcid_PA + ", ficonfid_PA=" + ficonfid_PA + ", nom_CC_PA=" + nom_CC_PA + ", nom_GRUPOEMP_PA=" + nom_GRUPOEMP_PA + ", id_DIVISION_PA=" + id_DIVISION_PA + ", nom_DIVISION_PA=" + nom_DIVISION_PA + ", id_ENTIDAD_PA=" + id_ENTIDAD_PA + ", nom_ENTIDAD_PA=" + nom_ENTIDAD_PA + ", ficcid=" + ficcid + ", ficonfid=" + ficonfid + '}';
    }

}
