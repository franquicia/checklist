package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;

public class ConteoSINORowMapper implements RowMapper<ChecklistPreguntasComDTO> {

	public ChecklistPreguntasComDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistPreguntasComDTO con = new ChecklistPreguntasComDTO();

	    con.setIdBita(rs.getInt("FIID_BITACORA"));
	    con.setIdCheck(rs.getInt("FIID_CHECKLIST"));
	    con.setChecklist(rs.getString("CHECKLIST"));
	    con.setPregSi(rs.getInt("RESP_SI"));
	    con.setPregNo(rs.getInt("RESP_NO"));
		con.setPregNa(rs.getInt("RESP_NA"));
		con.setIdBitaGral(rs.getInt("FIID_BITGRAL"));
	    con.setClasif(rs.getString("FCCLASIFICA"));
		
		return con;
	}

}
