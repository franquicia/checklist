package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ArbolDecisionDTO;

public class ArbolDecisionRowMapper2 implements RowMapper<ArbolDecisionDTO>{


	public ArbolDecisionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		ArbolDecisionDTO arbolDecision = new ArbolDecisionDTO();
		
		arbolDecision.setIdArbolDesicion(rs.getInt("FIID_ARBOL_DES"));
		arbolDecision.setIdCheckList(rs.getInt("FIID_CHECKLIST"));
		arbolDecision.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		arbolDecision.setRespuesta(rs.getString("FCRESPUESTA"));
		arbolDecision.setEstatusEvidencia(rs.getInt("FIESTATUS_E"));
		arbolDecision.setOrdenCheckRespuesta(rs.getInt("FIORDEN_CHECK"));
		arbolDecision.setReqAccion(rs.getInt("FIREQACCION"));
		arbolDecision.setReqObservacion(rs.getInt("FIREQOBS"));
		arbolDecision.setReqOblig(rs.getInt("FIOBLIGA"));
		arbolDecision.setDescEvidencia(rs.getString("FCDESCRIPCION_E"));
		arbolDecision.setPonderacion(rs.getString("FIPODENRACION"));
		arbolDecision.setIdPlantilla(rs.getInt("FIID_PLANTILLA"));
		arbolDecision.setIdProtocolo(rs.getInt("FIID_PROTOCOLO"));
		return arbolDecision;
	}
	

}
