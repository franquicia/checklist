package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.DuplicadosDTO;


public class DuplicadosRowMapper implements RowMapper<DuplicadosDTO>  {

	public DuplicadosDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		DuplicadosDTO duplicadoDTO = new DuplicadosDTO();
		
		duplicadoDTO.setConteo(rs.getInt("CONTEO"));
		duplicadoDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		duplicadoDTO.setIdArbol(rs.getInt("FIID_ARBOL_DES"));
		
		return duplicadoDTO;
	}

}
