package com.gruposalinas.checklist.business;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;

import sun.misc.BASE64Decoder;

import com.gruposalinas.checklist.dao.SucursalChecklistDAO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;
import com.gruposalinas.checklist.domain.ZonaNegoExpDTO;

public class SucursalChecklistBI {

	private static Logger logger = LogManager.getLogger(SucursalChecklistBI.class);

private List<SucursalChecklistDTO> listafila;
private List<SucursalChecklistDTO> lista;


Map<String, Object> listas = null;
	
	@Autowired
	SucursalChecklistDAO sucursalChecklistDAO;
		

	public String obtieneInfo() {
	
		JSONObject respuesta = new JSONObject();
		try {
			Map<String, Object> mapalistas = null;
			

			mapalistas = sucursalChecklistDAO.obtieneInfo();
			
			@SuppressWarnings("unchecked")
			List<SucursalChecklistDTO> listaAper = (List<SucursalChecklistDTO>) mapalistas.get("listaAper");
			@SuppressWarnings("unchecked")
			List<SucursalChecklistDTO> listatrans = (List<SucursalChecklistDTO>) mapalistas.get("listatrans");
			@SuppressWarnings("unchecked")
			List<SucursalChecklistDTO> listcuartel = (List<SucursalChecklistDTO>) mapalistas.get("listcuartel");
			
		
			
			JSONArray respuestasArrAper = new JSONArray();
			JSONArray respTran=new JSONArray();
			JSONArray respCuartel=new JSONArray();
			String ruta = "";
			//lista apertura
			for(int i=0; i < listaAper.size();i++) {
			
				ruta = "";
				
				JSONObject obj =new  JSONObject();
				
				obj.put("idSucursal", listaAper.get(i).getIdSucursal());
				obj.put("idEco",listaAper.get(i).getIdEco());
				obj.put("nombre", listaAper.get(i).getNombre());
				obj.put("direccion;", listaAper.get(i).getDireccion());
				obj.put("idImag", listaAper.get(i).getIdImag());
				obj.put("nombreImg", listaAper.get(i).getNombreImg());				
				obj.put("ruta", listaAper.get(i).getRuta());
				obj.put("observ", listaAper.get(i).getObserv());
				obj.put("periodo", listaAper.get(i).getPeriodo());
				obj.put("region", listaAper.get(i).getRegion());
				obj.put("territorio", listaAper.get(i).getTerritorio());
				obj.put("zona",listaAper.get(i).getZona());
				obj.put("statusSuc", listaAper.get(i).getStatusSuc());
				obj.put("aperturable", listaAper.get(i).getRecorrido());
				obj.put("recorrido", listaAper.get(i).getApert());
				obj.put("version", listaAper.get(i).getAux());
				obj.put("Negocio", listaAper.get(i).getNegocio());
				//System.out.println(obj.get("ruta").toString());
				
				respuestasArrAper.put(obj);
				
			}
			//lista Transfotrmacion
			for(int i=0; i< listatrans.size();i++) {
			
				JSONObject obj =new  JSONObject();
				
				obj.put("idSucursal", listatrans.get(i).getIdSucursal());
				obj.put("idEco",listatrans.get(i).getIdEco());
				obj.put("nombre", listatrans.get(i).getNombre());
				obj.put("direccion;", listatrans.get(i).getDireccion());
				obj.put("idImag", listatrans.get(i).getIdImag());
				obj.put("nombreImg", listatrans.get(i).getNombreImg());
				obj.put("ruta", listatrans.get(i).getRuta());
				obj.put("observ", listatrans.get(i).getObserv());
				obj.put("periodo", listatrans.get(i).getPeriodo());
				obj.put("region", listatrans.get(i).getRegion());
				obj.put("territorio", listatrans.get(i).getTerritorio());
				obj.put("zona",listatrans.get(i).getZona());
				obj.put("statusSuc", listatrans.get(i).getStatusSuc());
				obj.put("aperturable", listatrans.get(i).getRecorrido());
				obj.put("recorrido", listatrans.get(i).getApert());
				obj.put("version", listatrans.get(i).getAux());
				obj.put("Negocio", listaAper.get(i).getNegocio());
				
				respTran.put(obj);
				
			}
			//lista cuartel
			for(int i=0; i< listcuartel.size();i++) {
			
				JSONObject obj =new  JSONObject();
				
				obj.put("idSucursal", listcuartel.get(i).getIdSucursal());
				obj.put("idEco",listcuartel.get(i).getIdEco());
				obj.put("nombre", listcuartel.get(i).getNombre());
				obj.put("direccion;", listcuartel.get(i).getDireccion());
				obj.put("idImag", listcuartel.get(i).getIdImag());
				obj.put("nombreImg", listcuartel.get(i).getNombreImg());
				obj.put("ruta", listcuartel.get(i).getRuta());
				obj.put("observ", listcuartel.get(i).getObserv());
				obj.put("periodo", listcuartel.get(i).getPeriodo());
				obj.put("region", listcuartel.get(i).getRegion());
				obj.put("territorio", listcuartel.get(i).getTerritorio());
				obj.put("zona",listcuartel.get(i).getZona());
				obj.put("statusSuc", listcuartel.get(i).getStatusSuc());
				obj.put("aperturable", listcuartel.get(i).getRecorrido());
				obj.put("recorrido", listcuartel.get(i).getApert());
				obj.put("version", listcuartel.get(i).getAux());
				obj.put("Negocio", listaAper.get(i).getNegocio());
				respCuartel.put(obj);
				
			}
			respuesta.append("listaAper", respuestasArrAper);
			respuesta.append("listatrans", respTran);
			respuesta.append("listcuartel", respCuartel);
			
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de las Sucursales ");
			UtilFRQ.printErrorLog(null, e);	
		}

		return respuesta.toString();
	}

	//Servicios nuevos para la imagen 
	
	public int inserta(SucursalChecklistDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = sucursalChecklistDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean elimina(String idImag){
		boolean respuesta = false;
		
		try {
			respuesta = sucursalChecklistDAO.elimina(idImag);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<SucursalChecklistDTO> obtieneDatos( int idSucursal) {
		
		try {
			listafila =  sucursalChecklistDAO.obtieneDatos(idSucursal);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<SucursalChecklistDTO> obtieneInfoG() {

		try {
			lista = sucursalChecklistDAO.obtieneInfoG();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de la tabla Imagen Sucursal");
			UtilFRQ.printErrorLog(null, e);	
		}

		return lista;
	}


	
	public boolean actualiza(SucursalChecklistDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = sucursalChecklistDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public String insertaHallazgoDirecto(int ceco , String base64) throws IOException {
		
		String response = "";

		File r2 = null;
         String rootPath = File.listRoots()[0].getAbsolutePath();

         // DEFINIR RUTA
         String rutaEvidencia = "";
         
         String anioFormat = "2019";
         
         try{
         Calendar fechaC = Calendar.getInstance();
         int anio = fechaC.get(Calendar.YEAR);
         anioFormat = anio+"";
         } catch (Exception e) {
        	 logger.info("AP en el año de insertaHallazgoDirecto() SucursalChecklistBI 234");
         }
         
         rutaEvidencia = "/franquicia/imagenes/"+anioFormat+"/";
         String rutaPATHEvidencia = rootPath + rutaEvidencia;
         r2 = new File(rutaPATHEvidencia);
         
         logger.info("rutaPATHFirma: " + rutaPATHEvidencia);

         if (r2.mkdirs()) {
             logger.info("SE HA CREADA LA CARPETA");
         } else {
             logger.info("EL DIRECTORIO YA EXISTE");
         }
         
         FileOutputStream fos = null;
         BASE64Decoder decoder = new BASE64Decoder();
         byte[] hallazgoDecodificado = decoder.decodeBuffer(base64);
         // byte[] rutaGerenteDecodificada = decoder.decodeBuffer(FGerente);
         // //System.out.println(ruta+new String(imgDecodificada)+"."+new
         // String(extDecodificada));
         Date fechaActual = new Date();

         //Formateando la fecha:

         DateFormat formatoHora = new SimpleDateFormat("HHmmss");
         DateFormat formatoFecha = new SimpleDateFormat("ddMMyyyy");
         String hora = formatoFecha.format(fechaActual);
         String fecha = formatoHora.format(fechaActual);
         String nomDecodificadoStr = "" + ceco + "" + fecha + "" + hora;
         String extDecodificadaStr = "jpg";
         String rutaTotalF = rutaPATHEvidencia + nomDecodificadoStr + "." + extDecodificadaStr;
         FileOutputStream fileOutputF = new FileOutputStream(rutaTotalF);

         try {
             BufferedOutputStream bufferOutputF = new BufferedOutputStream(fileOutputF);
             // se escribe el archivo decodificado
             bufferOutputF.write(hallazgoDecodificado);
             //   bufferOutputFJ.write(rutaGerenteDecodificada);
             bufferOutputF.close();
           
             response = rutaTotalF;
             
         } finally {
             fileOutputF.close();
             //fileOutputFJ.close();
            //response = "";

         }
         
         String nombreImg = nomDecodificadoStr + "." + extDecodificadaStr;
        
         SucursalChecklistDTO suci = new SucursalChecklistDTO();
         
         suci.setIdSucursal(ceco);
         suci.setNombre(nombreImg);
         suci.setObserv(nombreImg);
         suci.setRuta(rutaTotalF);
         
         int respuesta = 1;
 		
 		try {
 			respuesta = sucursalChecklistDAO.inserta(suci);
 			
 			if (respuesta != 0) {
 	 			return rutaEvidencia + nombreImg;
 			} else {
 	 			return "";
 			}
 			
 		} catch (Exception e) {
 			logger.info("No fue posible insertar ");
 			respuesta = 0;
 			UtilFRQ.printErrorLog(null, e);	
 			return "";
 		}
	}
public List<SucursalChecklistDTO> obtieneDatosSuc( String ceco) {
		
		try {
			listafila =  sucursalChecklistDAO.obtieneDatosSuc(ceco);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}

public List<SucursalChecklistDTO> obtieneDatosSucCecoFaseProyecto(String ceco,String fase,String proyecto) {
	
	try {
		listafila =  sucursalChecklistDAO.obtieneDatosSucCecoFaseProyecto(ceco,fase,proyecto);
	} catch (Exception e) {
		logger.info("No fue posible obtener los datos");
		UtilFRQ.printErrorLog(null, e);	
	}
	
	return listafila;			
}
}