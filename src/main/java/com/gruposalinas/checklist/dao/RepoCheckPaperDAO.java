package com.gruposalinas.checklist.dao;

import java.util.List;



import com.gruposalinas.checklist.domain.RepoCheckPaperDTO;


public interface RepoCheckPaperDAO {



		
		public List<RepoCheckPaperDTO> obtieneDatos(String ceco,int idCheclist) throws Exception; 
		
		public List<RepoCheckPaperDTO> obtieneInfo(int idUsuario,int idCheclist) throws Exception; 
		
		public List<RepoCheckPaperDTO> obtieneDatosPreguntas(String bitacora,int idCheclist) throws Exception; 
		
		public List<RepoCheckPaperDTO> obtieneInfoPreguntas(int bitacora,int idCheclist) throws Exception; 
		
	}