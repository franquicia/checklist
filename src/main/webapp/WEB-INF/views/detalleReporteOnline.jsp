<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="height: 100%; min-width: 1200px">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
<title>DETALLE REPORTE ONLINE</title>
</head>

<body style="height: 97%;" onload="init(${idTienda},${iArray},'${fecha}',${idCheck},${idModulo},'${tienda}','${rutaImg}');">
<div id="idGeneralDetalle" class="deGeneralDeralle">
	
		<div class="deMargen1">
			<img class="deIm1-1" width="180px" height="80px" src="../images/reporteOnline/logoBanco.png">
		</div>
			
		<div class="deMargen2" >
			<div class="deDetalleGeneral1">
				<table class="tablaDatosGenerales">
				
					<tbody>
						<tr><td></td><td><div><h3 class="paraContainer">Para Gerente</h3></div></td><td></td></tr>
						<tr><td></td><td><div><h1 class="sucursalContainer">Nombre Sucursal</h1></div></td><td></td></tr>
						<tr><td></td><td>
						<ul id="accordion">
						      <li class="folios">
						      	<div class="infoWrapper">

							        <div class="tituloContainer">
										<h6 class="tituloText">Folios</h6>
									</div>
	
							        <div class="contenidoGeneralFolios">
							        <div class="resumen">RESUMEN</div>
								
										<div class="contentScroll" >
								        	<table id="tablaFolios">
								        		<thead><tr><th><strong>Pregunta</strong></th><th><strong>Respuesta</strong></th><th><strong>Acci&oacute;n</strong></th></tr></thead>
								        		<tbody id="tablaFoliosBody">

									       		</tbody>
								        	</table>
								        </div>	

							        </div>
						        </div>
						      </li>
	
						      <li class="evidencias">
							      <div class="infoWrapper">
								        <div class="tituloContainer">
											<h3 class="tituloText">Evidencias</h3>
										</div>
		
								        <div class="contenidoGeneralEvidencias">
								        <div class="resumen">RESUMEN</div>
								        
								        	<div class="contentScroll" >
								        		<table id="tablaEvidencias">
								        			<thead><tr><th><strong>Evidencia</strong></th><th><strong>Detalle</strong></th></tr></thead>
								        		
								        			<tbody id="tablaEvidenciasBody">
									        		</tbody>
								        		</table>
								        	</div>
								        	
								        </div>
							        </div>
						      </li>
						      <li class="fechaHora">
						        <div class="infoWrapper">
								        <div class="tituloContainer">
											<h3 class="tituloText">Fecha&nbsp;y&nbsp;Hora</h3>
										</div>
		
								        <div class="contenidoGeneralFechaHora">
								        	<div class="resumen">RESUMEN</div>
								        		<table id="tablaFechaHora">
								        			<thead><tr><th><strong>FECHA</strong></th><th><strong>HORA</strong></th></tr></thead>
								        			
									        		<tbody>
									        			<tr><td><strong>Inicio: </strong> <span id="fInicio"></span></td><td><strong>Inicio: </strong><span id="hInicio"></span></td></tr>
									        			<tr><td><strong>Fin: </strong> <span id="fFin">Sin fecha fin</span></td><td><strong>Fin: </strong><span id="hFin">Sin hora fin</span></td></tr>
									        			<tr><td><strong>Env&iacute;o: </strong> <span id="fEnvio">No enviado</span></td><td><strong>Modo: </strong><span id="hModo"></span></td></tr>
									        		</tbody>
								        		</table>
								        </div>
	
							        </div>
						      </li>
						      <li class="alertas">
						        <div class="infoWrapper">
								        <div class="tituloContainer">
											<h3 class="tituloText">Alertas</h3>
										</div>
		
								        <div class="contenidoGeneralAlertas">
								        	<div class="resumen">RESUMEN</div>
												<div class="contentScroll" >	
												<table id="tablaAlertas">
													<tbody id="tablaAlertasBody">
									        										        			
													</tbody>
								        		</table>
								        	    </div><!-- FIN CONTENT ALERTAS -->
								         </div>
								  </div>
						      </li>

					  	</ul>
						</td><td></td></tr>
					</tbody>
				
				</table>
			</div>
			
			<div class="deDetalleGeneral2">
				<div class="deMarco2-2">
					
					<div class="deMarco2-2-1">
						<div class="deMarcoDetalle-21">
							<div class="deMarcoDetalle-21-1"></div>
							<div class="deMarcoDetalle-21-2"><table class="afontdeMarcoDetalle-21-2"><tr><td class="tipoFuenteUno">DETALLE</td></tr></table></div>
							<div class="deMarcoDetalle-21-3"><table class="afontdeMarcoDetalle-21-3"><tr><td class="tipoFuenteUnoFolio">Acción: </td> <td id="tipoFuenteDosFolio">No</td></tr></table></div>
						</div>
						<div class="deMarcoDetalle-22">
							<div class="deMarcoDetalle-21-1"><table class="afontdeMarcoDetalle-21-222"><tr><td id="tipoFuenteUnoPregunta">26.- ¿Acceso al portafolio?</td></tr></table></div>
							<div class="deMarcoDetalle-21-2">
							
								<table class="afontdeMarcoDetalle-21-2Cuadrado">																		
									<tr>
										<td class="marcoTdCuadrado" align="center">
										    <table class="marcoUnoCuadrado">
										      <tr>
										        <td id="tipoFuenteUnoCuadro" align="center">NO</td>
										      </tr>
										    </table>
										</td>							   
									</tr>
								</table>
							</div>
	
	
							<div class="deMarcoDetalle-21-3">
								<table class="afontdeMarcoDetalle-21-3">
									<tr><td class="tipoFuenteUnoAccion">Motivo:</td> <td id="tipoFuenteDosAccion">Cómo entrar</td> </tr>
								</table>	
								<table class="afontdeMarcoDetalle-21-3">
									<tr><td class="tipoFuenteUnoFecha">Fecha de Correción:</td> <td id="tipoFuenteDosFecha">13/01/2016</td> </tr>
								</table>						
							</div>
						</div>
						<div class="deMarcoDetalle-23"></div>
					</div>
					<div class="deMarco2-2-2">					
						<div class="margenCarruselUno">							
							<div class="margenCarruselUno-1">														
							</div>
							<div class="margenCarruselUno-2">
								<img onclick="anteriorDetalle();" class="imagAnterior" src="../images/flecha_atras.png">	
							</div>							
						</div>
						<div class="margenCarruselDos">
							
							<div class="margenCarruselUno-2">
								<table class="afontdeMarcoDetalle-Evidencia"><tr><td class="tipoFuenteUnoEvidencia">Evidencia:</td></tr></table>
							</div>
							<div class="margenCarruselUno-1">														
							</div>							
						</div>
						<div class="margenCarruselTres">
							<div class="margenImagen">
								<img id="imagenEvidencia" alt="image" src="../images/reporteOnline/evidencia_no_requerida.png">
							</div>
							<div class="margenNumeros">
								<table class="celdaNumeros">
									<tr>
										<td id="circulos">
									
										</td>
									</tr>
								</table>
								
							</div>
							
						</div>
						<div class="margenCarruselCuatro">
							<div class="margenCarruselUno-1">
														
							</div>
							<div class="margenCarruselUno-2">
								<img onclick="siguienteDetalle();" class="imagPosterior" src="../images/flecha_adelante.png">	
							</div>
						</div>
					
					</div>
					
				</div>
			</div>
		</div>
</div>
</body>
</html>					