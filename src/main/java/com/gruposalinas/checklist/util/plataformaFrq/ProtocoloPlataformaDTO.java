/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.util.plataformaFrq;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 *
 * @author leodan1991
 */
public class ProtocoloPlataformaDTO {
    
    
        
    @JsonProperty("id_protocolo")
    private int idProtocolo;

    public int getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(int idProtocolo) {
        this.idProtocolo = idProtocolo;
    }

    @Override
    public String toString() {
        return "ProtocoloPlataformaDTO{" + "idProtocolo=" + idProtocolo + '}';
    }
    
     
    
}
