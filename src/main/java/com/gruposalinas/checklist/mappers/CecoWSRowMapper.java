/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.mappers;

import com.gruposalinas.checklist.domain.CecoPasoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author kramireza
 */
public class CecoWSRowMapper implements RowMapper<CecoPasoDTO> {

    public CecoPasoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CecoPasoDTO cecoPaso = new CecoPasoDTO();
        cecoPaso.setFccid(rs.getString("FCCCID"));
        cecoPaso.setFientidadid(rs.getInt("FIENTIDADID"));
        cecoPaso.setFinum_economico(rs.getInt("FINUM_ECONOMICO"));
        cecoPaso.setFcnombrecc(rs.getString("FCNOMBRECC"));
        cecoPaso.setFcnombre_ent(rs.getString("FCNOMBRE_ENT"));
        cecoPaso.setFcccid_padre(rs.getString("FCCCID_PADRE"));
        cecoPaso.setFientdid_padre(rs.getInt("FIENTDID_PADRE"));
        cecoPaso.setFcnombrecc_pad(rs.getString("FCNOMBRECC_PAD"));
        cecoPaso.setFctipocanal(rs.getString("FCTIPOCANAL"));
        cecoPaso.setFcnombtipcanal(rs.getString("FCNOMBTIPCANAL"));
        cecoPaso.setFccanal(rs.getString("FCCANAL"));
        cecoPaso.setFistatusccid(rs.getInt("FISTATUSCCID"));
        cecoPaso.setFcstatuscc(rs.getString("FCSTATUSCC"));
        cecoPaso.setFitipocc(rs.getInt("FITIPOCC"));
        cecoPaso.setFiestadoid(rs.getInt("FIESTADOID"));
        cecoPaso.setFcmunicipio(rs.getString("FCMUNICIPIO"));
        cecoPaso.setFctipooperacion(rs.getString("FCTIPOOPERACION"));
        cecoPaso.setFctiposucursal(rs.getString("FCTIPOSUCURSAL"));
        cecoPaso.setFcnombretiposuc(rs.getString("FCNOMBRETIPOSUC"));
        cecoPaso.setFccalle(rs.getString("FCCALLE"));
        cecoPaso.setFiresponsableid(rs.getInt("FIRESPONSABLEID"));
        cecoPaso.setFcresponsable(rs.getString("FCRESPONSABLE"));
        cecoPaso.setFccp(rs.getString("FCCP"));
        cecoPaso.setFctelefonos(rs.getString("FCTELEFONOS"));
        cecoPaso.setFiclasif_sieid(rs.getString("FICLASIF_SIEID"));
        cecoPaso.setFcclasif_sie(rs.getString("FCCLASIF_SIE"));
        cecoPaso.setFigastoxnegid(rs.getInt("FIGASTOXNEGID"));
        cecoPaso.setFcgastoxnegocio(rs.getString("FCGASTOXNEGOCIO"));
        cecoPaso.setFdapertura(rs.getString("FDAPERTURA"));
        cecoPaso.setFdcierre(rs.getString("FDCIERRE"));
        cecoPaso.setFipaisid(rs.getInt("FIPAISID"));
        cecoPaso.setFcpais(rs.getString("FCPAIS"));
        cecoPaso.setFdcarga(rs.getString("FDCARGA"));
        cecoPaso.setFiidunnego(rs.getInt("FIIDUNNEGO"));
        cecoPaso.setFcdescunbnego(rs.getString("FCDESCUNBNEGO"));
        cecoPaso.setFiidsubnego(rs.getInt("FIIDSUBNEGO"));
        cecoPaso.setFcdescsubnego(rs.getString("FCDESCSUBNEGO"));
        cecoPaso.setFcdescsubnego(rs.getString("FCDESCSUBNEGO"));
        cecoPaso.setFcusuario_mod(rs.getString("FCUSUARIO_MOD"));
        cecoPaso.setFdfecha_mod(rs.getString("FDFECHA_MOD"));

        return cecoPaso;
    }

}
