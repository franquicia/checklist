package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.CheckinDTO;



public interface CheckinDAO {
	
	public List<CheckinDTO> obtieneCheckin(CheckinDTO bean) throws Exception;
	
	public int insertaCheckin(CheckinDTO bean)throws Exception;
	
	public boolean modificaCheckin(CheckinDTO bean)throws Exception;
	
	public boolean eliminaCheckin(int idBitacora)throws Exception;
	
	
}
