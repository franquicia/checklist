package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.AdminSupDTO;

public interface AdminSupDAO {
	
	public List<AdminSupDTO> getEmpleado(int idUsuario) throws Exception;
	public Map<String, Object> getDetalleEmpleado(int idUsuario) throws Exception;
	public Map<String, Object> getPersonalAsig(int idUsuario) throws Exception;
	public Map<String, Object> getCecosAsig(int idUsuario) throws Exception;
	public List<AdminSupDTO> busquedaCeco(String ceco) throws Exception;
	
}
