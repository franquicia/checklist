package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.BitacoraGralDTO;
import com.gruposalinas.checklist.domain.EmpFijoDTO;

public class BitacoraGralRowMapper implements RowMapper<BitacoraGralDTO> {

	public BitacoraGralDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		BitacoraGralDTO bg = new BitacoraGralDTO();
		
		bg.setIdBitaGral(rs.getInt("FIID_BITGRAL"));
		bg.setFinicio(rs.getString("FDINICIO"));
		bg.setFin(rs.getString("FDTERMINO"));
		bg.setStatus(rs.getInt("FCSTATUS"));
		bg.setPeriodo(rs.getString("FCPERIODO"));

		return bg;
	}

}


