package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.ConsultaVisitasDTO;


public interface ConsultaVisitasDAO {

	public List<ConsultaVisitasDTO> ObtieneVisitas(ConsultaVisitasDTO bean) throws Exception;
	public List<ConsultaVisitasDTO>  ObtienePromCalif(ConsultaVisitasDTO bean) throws Exception;

	



}
