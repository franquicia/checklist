package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.ArbolRespAdiEviAdmDAO;
import com.gruposalinas.checklist.dao.ChecklistModificacionesAdmDAO;
import com.gruposalinas.checklist.dao.ChecklistModificacionesDAO;
import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.domain.ChecklistActualDTO;
import com.gruposalinas.checklist.domain.ChecklistModificacionesDTO;
import com.gruposalinas.checklist.domain.EvidenciaDTO;
import com.gruposalinas.checklist.domain.RegistroRespuestaDTO;
import com.gruposalinas.checklist.domain.RespuestaAdDTO;
import com.gruposalinas.checklist.domain.RespuestaDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class ArbolRespAdiEviAdmBI {
	
	private static Logger logger = LogManager.getLogger(ArbolRespAdiEviAdmBI.class);

	List<ArbolDecisionDTO> listaArbolDecision = null;
	
	@Autowired
	ArbolRespAdiEviAdmDAO arbolRespAdiEviAdmDAO;
	@Autowired
	ChecklistModificacionesAdmDAO checklistModificacionesAdmDAO;
	
	public int insertaArbolDecision(ArbolDecisionDTO bean){
		
		int respuesta= 0;
		
		try {
			
			respuesta = arbolRespAdiEviAdmDAO.insertaArbolDecision(bean);
			
		}catch(Exception e){
			logger.info("No fue posible insertar el registro del Arbol de Decision: "+e.getMessage());
			
			
		}
		
		return respuesta;
	}
	
	public boolean actualizaArbolDecision(ArbolDecisionDTO bean){
		
		boolean respuesta = false;
		
		try{
			
			respuesta = arbolRespAdiEviAdmDAO.actualizaArbolDecision(bean);
			
		}catch(Exception e){
			logger.info("No fue posible actualizarel registro del Arbol de Decision");
						
		}
		
		return respuesta;
		
	}
	
	public boolean eliminaArbolDecision(int idArbolDecision){
		
		boolean respuesta = false;
		
		try{
			respuesta = arbolRespAdiEviAdmDAO.eliminaArbolDecision(idArbolDecision);
		}catch(Exception e){
			logger.info("No fue posible eliminar el registro del Arbol de Decision");
				
		}
	
		return respuesta;
		
	}
	
	public List<ArbolDecisionDTO> buscaArbolDecision(int idChecklist){
		
		try{
			listaArbolDecision = arbolRespAdiEviAdmDAO.buscaArbolDecision(idChecklist);
		}catch(Exception e){
			logger.info("No fue posible obtener el Arbol de Decision");
			
		}
		
		return listaArbolDecision;
		
		
	}
	
	@SuppressWarnings("unchecked")
	public List<ArbolDecisionDTO> buscaArbolModificaciones(int idChecklist){
		
		try{
			listaArbolDecision = new ArrayList<ArbolDecisionDTO>();
			Map<String , Object> respuesta = checklistModificacionesAdmDAO.obtieneChecklist(idChecklist);
			
			List<ChecklistModificacionesDTO>  checklistModificaciones = (List<ChecklistModificacionesDTO>) respuesta.get("checklistModificaciones");
			List<ChecklistActualDTO> checklistActual = (List<ChecklistActualDTO>) respuesta.get("checklistActual");
			
			if(checklistModificaciones.size()>0){
				//System.out.println("TENGO MODIFICACIONES!!!! " + checklistModificaciones.size());
			    int cont =0;
				
				while(cont < checklistModificaciones.size()){

					ArbolDecisionDTO arbol = new ArbolDecisionDTO();
					arbol.setIdArbolDesicion(checklistModificaciones.get(cont).getIdArbol());
					arbol.setIdCheckList(checklistModificaciones.get(cont).getIdChecklist());
					arbol.setIdPregunta(checklistModificaciones.get(cont).getIdPregunta());
					arbol.setRespuesta(String.valueOf(checklistModificaciones.get(cont).getIdPosible()));
					arbol.setEstatusEvidencia(checklistModificaciones.get(cont).getEstatusEvidencia());
					arbol.setOrdenCheckRespuesta(checklistModificaciones.get(cont).getSiguientePregunta());
					arbol.setReqAccion(checklistModificaciones.get(cont).getRequiereAccion());
					arbol.setReqObservacion(checklistModificaciones.get(cont).getRequiereObsv());
					arbol.setDescEvidencia(checklistModificaciones.get(cont).getEtiquetaEvidencia());
															
					listaArbolDecision.add(arbol);
					
					cont ++;
				}
				
			}else{
				
				 int cont =0;
					
					while(cont < checklistActual.size()){
						
						ArbolDecisionDTO arbol = new ArbolDecisionDTO();
						arbol.setIdArbolDesicion(checklistActual.get(cont).getIdArbol());
						arbol.setIdCheckList(checklistActual.get(cont).getIdChecklist());
						arbol.setIdPregunta(checklistActual.get(cont).getIdPregunta());
						arbol.setRespuesta(String.valueOf(checklistActual.get(cont).getIdPosible()));
						arbol.setEstatusEvidencia(checklistActual.get(cont).getEstatusEvidencia());
						arbol.setOrdenCheckRespuesta(checklistActual.get(cont).getSiguientePregunta());
						arbol.setReqAccion(checklistActual.get(cont).getRequiereAccion());
						arbol.setReqObservacion(checklistActual.get(cont).getRequiereObsv());
						arbol.setDescEvidencia(checklistActual.get(cont).getEtiquetaEvidencia());
						arbol.setPonderacion(checklistActual.get(cont).getPonderacion());
						arbol.setDescResp(checklistActual.get(cont).getPosibleRespuesta());
						arbol.setIdPlantilla(checklistActual.get(cont).getIdPlantilla());
						arbol.setReqOblig(checklistActual.get(cont).getEvidenciaObligatoria());
						arbol.setCommit(1);
						
						listaArbolDecision.add(arbol);
						cont ++;
					}
				
			}
			
		}catch(Exception e){
			logger.info("No fue posible obtener el Arbol de Decision");
			
		}
		
		return listaArbolDecision;
	}
	
	public boolean cambiaRespuestasPorId(){
		boolean res = false;
		
		try {
			res = arbolRespAdiEviAdmDAO.cambiaRespuestasPorId();
		} catch (Exception e) {
			logger.info("No fue posible cambiar las respuestas por Ids Posibles");

		}
		
		return res;
	}
	
	//RESPUESTAS
	
	List<RespuestaDTO> listaRespuestas = null;
	List<RespuestaDTO> listaRespuesta = null;
	
	public List<RespuestaDTO> obtieneRespuesta(){
		try{
			listaRespuestas = arbolRespAdiEviAdmDAO.obtieneRespuesta();
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Respuesta");
					
		}
				
		return listaRespuestas;
	}
	
	public List<RespuestaDTO> obtieneRespuesta(String idArbol, String idRespuesta, String idBitacora){
		try{
			listaRespuesta = arbolRespAdiEviAdmDAO.obtieneRespuesta(idArbol, idRespuesta, idBitacora);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Respuesta");
			
		}
				
		return listaRespuesta;
	}
	
	public int insertaRespuesta (RespuestaDTO respuesta){
		int idRespuesta = 0;
		try{
			idRespuesta = arbolRespAdiEviAdmDAO.insertaRespuesta(respuesta);
		} 
		catch (Exception e) {
			logger.info("No fue posible insertar la Respuesta");
			
		}
		
		return idRespuesta;	
	}
	
	public boolean actualizaRespuesta(RespuestaDTO resp){
		boolean respuesta = false;
		try{
			respuesta = arbolRespAdiEviAdmDAO.actualizaRespuesta(resp);
		}
		catch (Exception e) {
			logger.info("No fue posible actualizar la Respuesta");
			
		}
							
		return respuesta;
	}
	
	public boolean actualizaFechaResp(String idRespuesta, String fechaTermino, String commit){
		boolean respuesta = false;
		try{
			respuesta = arbolRespAdiEviAdmDAO.actualizaFechaResp(idRespuesta, fechaTermino, commit);
		}
		catch (Exception e) {
			logger.info("No fue posible actualizar la fecha de la Respuesta");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaRespuesta(int idRespuesta){
		boolean respuesta = false;
		try{
			respuesta = arbolRespAdiEviAdmDAO.eliminaRespuesta(idRespuesta);
		}
		catch(Exception e){
			logger.info("No fue posible borrar la Respuesta");
			
		}
		
		return respuesta;
	}
	
	public boolean eliminaRespuestasDuplicadas(){
		boolean respuesta = false;
		try{
			respuesta = arbolRespAdiEviAdmDAO.eliminaRespuestasDuplicadas();
		}
		catch(Exception e){
			logger.info("No fue posible borrar la Respuesta");
			logger.info(e.getMessage());
		}
		
		return respuesta;
	}
	
	
	public boolean registraRespuestas(RegistroRespuestaDTO respuestasChecklist){

		int idCheckUsua;
		String idRespuestas="";
		String compromisos="";
		String evidencias= "";
		int cont = 0;
		
		boolean respuesta = false;
		
		try { 
			
			idCheckUsua = respuestasChecklist.getIdCheckUsua();

			for (RespuestaDTO respuestaDTO : respuestasChecklist.getListaRespuestas() ) {

				idRespuestas += String.valueOf(respuestaDTO.getIdRespuesta());
				idRespuestas +="!~OBJ~!";
				
				if(!respuestaDTO.getObservacion().equals("")){
					idRespuestas += respuestaDTO.getObservacion();
				}
				else
				{
					idRespuestas += " ";
			        
				}
				idRespuestas +="!~OBJ~!"; 

				if(respuestaDTO.getIdTipoRespuesta() == 2)
					idRespuestas += respuestaDTO.getRespuestaAbierta();
				else 
					idRespuestas += " ";
						

				if ( respuestaDTO.getCompromiso() != null  ){
					compromisos += respuestaDTO.getCompromiso().getDescripcion();
					compromisos +="!~OBJ~!";					
					compromisos += respuestaDTO.getCompromiso().getFechaCompromiso();
					compromisos +="!~OBJ~!";					
					compromisos += respuestaDTO.getCompromiso().getIdResponsable();
				}else{
					compromisos += " ";
				}
									
				if(respuestaDTO.getEvidencia() != null){
					evidencias +=respuestaDTO.getEvidencia().getIdTipo();
					evidencias +="!~OBJ~!";
					evidencias += respuestaDTO.getEvidencia().getRuta();
				}else{
					evidencias += " ";
				}
									
				if(cont < respuestasChecklist.getListaRespuestas().size() ){
					idRespuestas += "!<ATRRIB>!";
					compromisos += "!<ATRRIB>!";
					evidencias += "!<ATRRIB>!";
				}
									
				cont ++;
			}
			
			/*//System.out.println("id check: " + idCheckUsua );
			//System.out.println("id bitacora: " + respuestasChecklist.getBitacora() );
			//System.out.println("id respuesta: " + idRespuestas );
			//System.out.println("compromiso: " + compromisos );
			//System.out.println("evidencias: " + evidencias );*/
			
			respuesta = arbolRespAdiEviAdmDAO.registraRespuestas(idCheckUsua, respuestasChecklist.getBitacora(), idRespuestas, compromisos, evidencias);
	
		}catch(Exception e){
			logger.info("No fue posible Registrar las respuestas del Usuario");
			
		}
		
		return respuesta;
	}
	
	public boolean insertaUnaRespuesta(RegistroRespuestaDTO registroUnaResp){

		int idCheckUsua;
		int idBitacora;
		int idArbol;
		int idPreg;
		String obs;
		String respAd;
		String compromisos="";
		String evidencias= "";
		
		boolean respuesta = false;
		
		
		try {
			logger.info("entro!!!");
			
			idCheckUsua = registroUnaResp.getIdCheckUsua();
			logger.info("entro 1!!!" + idCheckUsua);
			idBitacora = registroUnaResp.getBitacora().getIdBitacora();
			logger.info("entro 2!!!"+idBitacora);
			idArbol = registroUnaResp.getIdArbol();
			logger.info("entro 3!!!"+idArbol);
			idPreg = registroUnaResp.getIdPreg();
			logger.info("entro 4!!!"+idPreg);
			obs = registroUnaResp.getObservacion();
			logger.info("entro 5!!!"+obs);
			respAd = registroUnaResp.getRespAd();


			logger.info("entro por acá!!!"+ registroUnaResp.getListaRespuestas());
			
			for (RespuestaDTO respuestaDTO : registroUnaResp.getListaRespuestas() ) {

				if ( respuestaDTO.getCompromiso() != null  ){
					compromisos += respuestaDTO.getCompromiso().getDescripcion();
					compromisos +="!~OBJ~!";					
					compromisos += respuestaDTO.getCompromiso().getFechaCompromiso();
					compromisos +="!~OBJ~!";					
					compromisos += respuestaDTO.getCompromiso().getIdResponsable();
				}else{
					compromisos += " ";
				}
									
				if(respuestaDTO.getEvidencia() != null){
					evidencias +=respuestaDTO.getEvidencia().getIdTipo();
					evidencias +="!~OBJ~!";
					evidencias += respuestaDTO.getEvidencia().getRuta();
				}else{
					evidencias += " ";
				}

				logger.info("entro otra vez!!!");
				
			}
			
			//System.out.println("id check: " + idCheckUsua );
			//System.out.println("id bitacora: " + idBitacora);
			//System.out.println("idPreg: " + idPreg);
			//System.out.println("id Arbol: " + idArbol);
			//System.out.println("obs: " + obs);
			//System.out.println("respAd: " + respAd);
			//System.out.println("compromiso: " + compromisos );
			//System.out.println("evidencias: " + evidencias );
			
			respuesta = arbolRespAdiEviAdmDAO.insertaUnaRespuesta(idCheckUsua, idBitacora, idPreg, idArbol, obs, compromisos, evidencias, respAd);
	
		}catch(Exception e){
			logger.info("No fue posible Registrar la respuesta del Usuario");
			
		}
		
		return respuesta;
	}


	public boolean eliminaRespuestas(RegistroRespuestaDTO registroUnaResp){
		boolean respuesta = false;
		String respuestas = "";
		
		try{
			for (RespuestaDTO respuestaDTO : registroUnaResp.getListaRespuestas() ) {

				
				if ( respuestaDTO.getIdPregunta() != 0  ){
					respuestas += respuestaDTO.getIdPregunta();
					respuestas +="!~OBJ~!";					
				}
				
				if ( respuestaDTO.getIdBitacora() != 0  ){
					respuestas += respuestaDTO.getIdBitacora();
					respuestas +="!~OBJ~!";					
				}
				
				if ( respuestaDTO.getIdCheckUsuario() != 0  ){
					respuestas += respuestaDTO.getIdCheckUsuario();
				}
				
				if (respuestas != null){
					respuestas += "!<ATRRIB>!";
				}
				logger.info("RESPUESTAS: "+respuestas);
			}
			

			respuesta = arbolRespAdiEviAdmDAO.eliminaRespuestas(respuestas);
	
		}
		catch(Exception e){
			logger.info("No fue posible borrar la Respuesta");
			
		}
		
		return respuesta;
	}
	
	public boolean insertaNuevaRespuesta(RegistroRespuestaDTO registroUnaResp){

		int idCheckUsua;
		int idBitacora;
		int idArbol;
		int idPreg;
		String obs;
		String respAd;
		String compromisos="";
		String evidencias= "";
		
		boolean respuesta = false;
		
		
		try {
			logger.info("entro!!!");
			
			idCheckUsua = registroUnaResp.getIdCheckUsua();
			logger.info("entro 1!!!" + idCheckUsua);
			idBitacora = registroUnaResp.getBitacora().getIdBitacora();
			logger.info("entro 2!!!"+idBitacora);
			idArbol = registroUnaResp.getIdArbol();
			logger.info("entro 3!!!"+idArbol);
			idPreg = registroUnaResp.getIdPreg();
			logger.info("entro 4!!!"+idPreg);
			obs = registroUnaResp.getObservacion();
			logger.info("entro 5!!!"+obs);
			respAd = registroUnaResp.getRespAd();
			
			if(idArbol==4419 || idArbol==4437 || idArbol==4439 || idArbol==4443 || idArbol==4445 || idArbol==4447 ) {
				logger.info("entro ");
			}


			logger.info("entro por acá!!!"+ registroUnaResp.getListaRespuestas());
			
			for (RespuestaDTO respuestaDTO : registroUnaResp.getListaRespuestas() ) {

				if ( respuestaDTO.getCompromiso() != null  ){
					compromisos += respuestaDTO.getCompromiso().getDescripcion();
					compromisos +="!~OBJ~!";					
					compromisos += respuestaDTO.getCompromiso().getFechaCompromiso();
					compromisos +="!~OBJ~!";					
					compromisos += respuestaDTO.getCompromiso().getIdResponsable();
				}else{
					compromisos += " ";
				}
									
				for (EvidenciaDTO evidenciaDTO : registroUnaResp.getListaEvidencia() )
					{	
						if(evidenciaDTO.getRuta() != null)
						{			
					evidencias +=evidenciaDTO.getIdTipo();
					evidencias +="!~OBJ~!";
					evidencias += evidenciaDTO.getRuta();
					evidencias +="!~OBJ~!";
					evidencias += evidenciaDTO.getIdPlantilla();
					
					
						}
					evidencias += "!<ATRRIB>!";
				}

				logger.info("entro otra vez!!!");
				
			}
			logger.info("evidencias "+evidencias);
			logger.info("compromisos: "+compromisos);
			
			respuesta = arbolRespAdiEviAdmDAO.insertaRespuestaNueva(idCheckUsua, idBitacora, idPreg, idArbol, obs, compromisos, evidencias, respAd);
	
		}catch(Exception e){
			logger.info("No fue posible Registrar la respuesta del Usuario");
			
		}
		
		return respuesta;
	}
	//ADICIONALES
List<RespuestaAdDTO> listaRespuestasAD = null;
	
	public List<RespuestaAdDTO> buscaRespADP(String idRespAD, String idResp) {
		try{
			listaRespuestasAD = arbolRespAdiEviAdmDAO.buscaRespADP(idRespAD, idResp);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Respuesta Adicional");
					
		}
				
		return listaRespuestasAD;
	}

	public int insertaRespAD(RespuestaAdDTO bean) {
		int idRespuesta = 0;
		try{
			idRespuesta = arbolRespAdiEviAdmDAO.insertaRespAD(bean);
		} 
		catch (Exception e) {
			logger.info("No fue posible insertar la Respuesta Adicional");
			
		}
		
		return idRespuesta;	
	}
	
	public boolean actualizaRespAD(RespuestaAdDTO bean) {
		boolean respuesta = false;
		try{
			respuesta = arbolRespAdiEviAdmDAO.actualizaRespAD(bean);
		}
		catch (Exception e) {
			logger.info("No fue posible actualizar la Respuesta Adicional");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaRespAD(int idRespAD) {
		boolean respuesta = false;
		try{
			respuesta = arbolRespAdiEviAdmDAO.eliminaRespAD(idRespAD);
		}
		catch(Exception e){
			logger.info("No fue posible borrar la Respuesta Adicional");
			
		}
		
		return respuesta;
	}
	
	public boolean depuraTabs() {
		boolean respuesta = false;
		try{
			respuesta = arbolRespAdiEviAdmDAO.depuraTabs();
		}
		catch(Exception e){
			logger.info("No fue posible borrar la Respuesta Adicional");
			
		}
		
		return respuesta;
	}
	
	public boolean depuraTabParam(int idCheck) {
		boolean respuesta = false;
		try{
			respuesta = arbolRespAdiEviAdmDAO.depuraTabParam(idCheck);
		}
		catch(Exception e){
			logger.info("No fue posible borrar la Respuesta Adicional");
			
		}
		
		return respuesta;
	}
//EVIDENCIAS
	List<EvidenciaDTO> listaEvidencias = null;
	List<EvidenciaDTO> listaEvidencia = null;
	

	public List<EvidenciaDTO> obtieneEvidencia(){
				
		try{
			listaEvidencias = arbolRespAdiEviAdmDAO.obtieneEvidencia();	
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Evidencia");
					
		}
				
		return listaEvidencias;
	}
	
	public List<EvidenciaDTO> obtieneEvidencia(int idEvidencia){
				
		try{
			listaEvidencia = arbolRespAdiEviAdmDAO.obtieneEvidencia(idEvidencia);	
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Evidencia");
			
		}
				
		return listaEvidencia;
	}	
	
	public int insertaEvidencia( EvidenciaDTO evidencia ) {
		
		int idEvidencia = 0;
		
		try {		
			idEvidencia = arbolRespAdiEviAdmDAO.insertaEvidencia(evidencia);
		} catch (Exception e) {
			logger.info("No fue posible insertar la Evidencia");
			
		}
		
		return idEvidencia;		
	}
	

	public boolean actualizaEvidencia (EvidenciaDTO evidencia){
		
		boolean respuesta = false;
				
		try {
			respuesta = arbolRespAdiEviAdmDAO.actualizaEvidencia(evidencia);
		} catch (Exception e) {
			logger.info("No fue posible actualizar la Evidencia");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaEvidencia(int idEvidencia){
		
		boolean respuesta = false;
		
		try{
			respuesta = arbolRespAdiEviAdmDAO.eliminaEvidencia(idEvidencia);
		}catch(Exception e){
			logger.info("No fue posible borrar la Evidencia");
			
		}
		
		return respuesta;
	}
}
