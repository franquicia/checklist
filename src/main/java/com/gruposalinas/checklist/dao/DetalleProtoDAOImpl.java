package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.DetalleProtoDTO;
import com.gruposalinas.checklist.mappers.BitacoraCerradaRowMapper;
import com.gruposalinas.checklist.mappers.ControlAccesoRowMapper;
import com.gruposalinas.checklist.mappers.DetalleProtoRowMapper;
import com.gruposalinas.checklist.mappers.NombreCecoRowMapper;
import com.gruposalinas.checklist.mappers.NombreGrupoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class DetalleProtoDAOImpl extends DefaultDAO implements DetalleProtoDAO {

    private static Logger logger = LogManager.getLogger(ProtocoloDAOImpl.class);

    DefaultJdbcCall jdbcObtieneDetalleProto;
    DefaultJdbcCall jdbcObtieneNombreGrupo;
    DefaultJdbcCall jdbcObtieneNombreCeco;
    DefaultJdbcCall jdbcObtieneBitacoraCerrada;
    DefaultJdbcCall jdbcObtieneControlAcceso;

    public void init() {

        jdbcObtieneDetalleProto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADETPROTO")
                .withProcedureName("SPDETALLEPRO")
                .returningResultSet("PA_CONSULTA", new DetalleProtoRowMapper());

        jdbcObtieneNombreGrupo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADETPROTO")
                .withProcedureName("SPNOMCHKPRO")
                .returningResultSet("PA_CONSULTA", new NombreGrupoRowMapper());

        jdbcObtieneNombreCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADETPROTO")
                .withProcedureName("SPDETCCONOM")
                .returningResultSet("PA_CONSULTA", new NombreCecoRowMapper());

        jdbcObtieneBitacoraCerrada = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADETPROTO")
                .withProcedureName("SPBITACERR")
                .returningResultSet("PA_CONSULTA", new BitacoraCerradaRowMapper());

        jdbcObtieneControlAcceso = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADETPROTO")
                .withProcedureName("SPCONSCHECKIN")
                .returningResultSet("PA_CONSULTA", new ControlAccesoRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DetalleProtoDTO> ObtieneDetalleProto(DetalleProtoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<DetalleProtoDTO> listaProto = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_GPOCHK", bean.getIdOrdeGrupo());

        out = jdbcObtieneDetalleProto.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADETPROTO.SPDETALLEPRO");

        listaProto = (List<DetalleProtoDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió obtener el id de grupo");
        } else {
            return listaProto;
        }

        return listaProto;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DetalleProtoDTO> ObtieneNombreGrupo(DetalleProtoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<DetalleProtoDTO> listaProto = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_USR", bean.getNombreUsuario())
                .addValue("PA_GPOCHK", bean.getIdOrdeGrupo());

        out = jdbcObtieneNombreGrupo.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADETPROTO.SPNOMCHKPRO");

        listaProto = (List<DetalleProtoDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió obtener el id de grupo y checklist");
        } else {
            return listaProto;
        }

        return listaProto;

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DetalleProtoDTO> ObtieneNombreCeco(DetalleProtoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<DetalleProtoDTO> listaProto = null;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_USR", bean.getIdUsuario())
                .addValue("PA_FIORDEN_GPO", bean.getIdOrdeGrupo());

        out = jdbcObtieneNombreCeco.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADETPROTO.SPDETCCONOM");

        listaProto = (List<DetalleProtoDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió obtener el Nombre y Ceco");
        } else {
            return listaProto;
        }

        return listaProto;

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DetalleProtoDTO> ObtieneBitacoraCerrada(DetalleProtoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<DetalleProtoDTO> listaProto = null;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_USR", bean.getIdUsuario())
                .addValue("PA_CCO", bean.getIdCeco())
                .addValue("PA_CHKLST", bean.getIdCheklist());

        out = jdbcObtieneBitacoraCerrada.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADETPROTO.SPBITACERR");

        listaProto = (List<DetalleProtoDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió obtener la informacion de la bitacora");
        } else {
            return listaProto;
        }

        return listaProto;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DetalleProtoDTO> ObtieneControlAcceso(DetalleProtoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<DetalleProtoDTO> listaProto = null;

        String idUsuario = null;
        if (bean.getIdUsuario() != 0) {
            idUsuario = "" + bean.getIdUsuario();
        }

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_USR", idUsuario)
                .addValue("PA_CCO", bean.getIdCeco())
                .addValue("PA_FECHA_INICIO", bean.getFdInicio())
                .addValue("PA_FECHA_FIN", bean.getFdTermino());

        out = jdbcObtieneControlAcceso.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADETPROTO.SPCONSCHECKIN");

        listaProto = (List<DetalleProtoDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió obtener la informacion de la bitacora");
        } else {
            return listaProto;
        }

        return listaProto;

    }
}
