package com.gruposalinas.checklist.business;

import com.gruposalinas.checklist.dao.InformeGeneralDAO;
import com.gruposalinas.checklist.domain.InformeGeneralDTO;
import com.gruposalinas.checklist.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class InformeGeneralBI {

	private static Logger logger = LogManager.getLogger(InformeGeneralBI.class);
    private List<InformeGeneralDTO> listafila;
	
	@Autowired
	InformeGeneralDAO informeGeneralDAO;
		
	
    public boolean insertaInformeGeneral(String idCeco, String fecha, int idUsuario, String rutaInforme, int idEstatus, String estatus, int numeroBitacora) {
		boolean respuesta = false;


		try {
			respuesta = informeGeneralDAO.insertaInformeGeneral(idCeco, fecha, idUsuario, rutaInforme, idEstatus, estatus, numeroBitacora);
                } catch (Exception e) {

                    logger.info("No fue posible insertar el informe general " + e);
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaInformeGeneral(String idCeco, String fecha, int idUsuario){
		boolean respuesta = false;
		
		try {
			respuesta = informeGeneralDAO.eliminaInformeGeneral(idCeco,fecha,idUsuario);
		} catch (Exception e) {
                    logger.info("No fue posible eliminar el informe general");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<InformeGeneralDTO> obtieneDatosInformeGeneral( String idCeco, String fecha, int idUsuario, int estatus) {
		
		try {
			listafila =  informeGeneralDAO.obtieneDatosInformeGeneral(idCeco, fecha, idUsuario, estatus);
		} catch (Exception e) {
                    logger.info("No fue posible obtener los datos del informe general");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	
	
	public boolean actualizaInformeGeneral(String idCeco, String fecha, String idUsuario, String rutaInforme, String idEstatus, String estatus, String numeroBitacora){
		boolean respuesta = false;
		
		try {
			respuesta = informeGeneralDAO.actualizaInformeGeneral(idCeco, fecha, idUsuario, rutaInforme, idEstatus, estatus, numeroBitacora);
		} catch (Exception e) {
                    logger.info("No fue posible actualizar el informe general");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	
}