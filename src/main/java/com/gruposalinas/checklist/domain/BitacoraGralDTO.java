package com.gruposalinas.checklist.domain;

public class BitacoraGralDTO {
	

//gral
		private int idBitaGral;
		private int status;
		private String finicio;
		private String fin;
		private String periodo;
		private String bitacora;

//hijas
		private int idBita;

		public int getIdBitaGral() {
			return idBitaGral;
		}

		public void setIdBitaGral(int idBitaGral) {
			this.idBitaGral = idBitaGral;
		}

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public String getFinicio() {
			return finicio;
		}

		public void setFinicio(String finicio) {
			this.finicio = finicio;
		}

		public String getFin() {
			return fin;
		}

		public void setFin(String fin) {
			this.fin = fin;
		}

		public String getPeriodo() {
			return periodo;
		}

		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}

		public int getIdBita() {
			return idBita;
		}

		public void setIdBita(int idBita) {
			this.idBita = idBita;
		}
		
		

		public String getBitacora() {
			return bitacora;
		}

		public void setBitacora(String bitacora) {
			this.bitacora = bitacora;
		}

		@Override
		public String toString() {
			return "BitacoraGralDTO [idBitaGral=" + idBitaGral + ", status=" + status + ", finicio=" + finicio
					+ ", fin=" + fin + ", periodo=" + periodo + ", bitacora=" + bitacora + ", idBita=" + idBita + "]";
		}
		
		
	}