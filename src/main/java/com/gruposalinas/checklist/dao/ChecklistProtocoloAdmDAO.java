package com.gruposalinas.checklist.dao;

import java.util.List;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;


public interface ChecklistProtocoloAdmDAO {
	

	public List<ChecklistProtocoloDTO> getChecklistProtocolo(ChecklistProtocoloDTO bean) throws Exception;

	public int insertaChecklistCom(ChecklistProtocoloDTO bean) throws Exception;
	
	public boolean actualizaChecklistCom(ChecklistProtocoloDTO bean) throws Exception;

	public boolean eliminaChecklist(ChecklistProtocoloDTO bean) throws Exception;
	
}
