package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AdmHandbookDTO;
import com.gruposalinas.checklist.mappers.AdmHandbookRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AdmHandbookDAOImpl extends DefaultDAO implements AdmHandbookDAO {

    private static Logger logger = LogManager.getLogger(CompromisoDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodosBooks;
    DefaultJdbcCall jdbcObtieneIdBooks;
    DefaultJdbcCall jdbcInsertaHandbook;
    DefaultJdbcCall jdbcActualizaHandbook;
    DefaultJdbcCall jdbcEliminaHandbook;

    public void init() {

        jdbcObtieneTodosBooks = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMHANDBOOK")
                .withProcedureName("SPSELBOOK")
                .returningResultSet("PA_CONSULTA", new AdmHandbookRowMapper());

        jdbcObtieneIdBooks = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMHANDBOOK")
                .withProcedureName("SPGETBOOK")
                .returningResultSet("PA_CONSULTA", new AdmHandbookRowMapper());

        jdbcInsertaHandbook = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMHANDBOOK")
                .withProcedureName("SPINSBOOK");

        jdbcActualizaHandbook = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMHANDBOOK")
                .withProcedureName("SPACTBOOK");

        jdbcEliminaHandbook = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMHANDBOOK")
                .withProcedureName("SPDELBOOK");
    }

    @SuppressWarnings("unchecked")
    public List<AdmHandbookDTO> obtieneTodos() throws Exception {

        Map<String, Object> out = null;
        List<AdmHandbookDTO> listaBooks = null;
        int error = 0;

        out = jdbcObtieneTodosBooks.execute();

        logger.info("Funcion ejecutada: {checklist.PAADMHANDBOOK.SPSELBOOK}");

        listaBooks = (List<AdmHandbookDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener los books");
        } else {
            return listaBooks;
        }

        return listaBooks;
    }

    @SuppressWarnings("unchecked")
    public List<AdmHandbookDTO> obtieneIdBook(AdmHandbookDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AdmHandbookDTO> listaBook = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_BOOK", bean.getIdBook());

        out = jdbcObtieneIdBooks.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMHANDBOOK.SPGETBOOK}");

        listaBook = (List<AdmHandbookDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener el Handbook");
        } else {
            return listaBook;
        }

        return null;
    }

    public boolean insertaHandbook(AdmHandbookDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCDESCRIPCION", bean.getDescripcion())
                .addValue("PA_FCRUTA", bean.getRuta())
                .addValue("PA_FIACTIVO", bean.getActivo())
                .addValue("PA_FIPADRE", bean.getPadre())
                .addValue("PA_FCTIPO", bean.getTipo())
                .addValue("PA_FDFECHA", bean.getFecha());

        out = jdbcInsertaHandbook.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMHANDBOOK.SPINSBOOK}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar el Compromiso");
        } else {
            return true;
        }

        return false;
    }

    public boolean actualizaHandbook(AdmHandbookDTO bean) throws Exception {

        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_BOOK", bean.getIdBook())
                .addValue("PA_FCDESCRIPCION", bean.getDescripcion())
                .addValue("PA_FCRUTA", bean.getRuta())
                .addValue("PA_FIACTIVO", bean.getActivo())
                .addValue("PA_FIPADRE", bean.getPadre())
                .addValue("PA_FCTIPO", bean.getTipo());

        out = jdbcActualizaHandbook.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMHANDBOOK.SPACTBOOK}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al actualizar el idHandBook");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaHandbook(AdmHandbookDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_BOOK", bean.getIdBook());

        out = jdbcEliminaHandbook.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_COMP.SP_DEL_COMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al borrar el Handbook");
        } else {
            return true;
        }

        return false;
    }

}
