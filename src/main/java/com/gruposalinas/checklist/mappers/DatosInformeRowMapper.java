package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.DatosInformeDTO;
import com.gruposalinas.checklist.domain.EmpFijoDTO;

public class DatosInformeRowMapper implements RowMapper<DatosInformeDTO> {

	public DatosInformeDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		DatosInformeDTO dat = new DatosInformeDTO();
		
		dat.setIdCeco(rs.getInt("FCID_CECO"));
	    dat.setFecha(rs.getString("FDFECHA"));
	    dat.setIdUsuario(rs.getInt("FIID_USUARIO"));
	    dat.setIdInforme(rs.getInt("FIID_INFO"));
		dat.setIdProtocolo(rs.getInt("FIID_PROTOCOLO"));
	    dat.setImperdonables(rs.getInt("FCIMPERD"));
	    dat.setTotalRespuestas(rs.getInt("FCTOTRESP"));
	    dat.setCalificacion(rs.getDouble("FICALIF"));
	    dat.setPeriodo(rs.getString("FCPERIODO"));
	    
	    
		return dat;
	}

}


