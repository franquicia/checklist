package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.BitacoraGralDTO;
import com.gruposalinas.checklist.domain.DatosInformeDTO;



public interface DatosInformeDAO {
//bita gral
	  public int insertaDatosInforme(DatosInformeDTO bean)throws Exception;
		
		public boolean eliminaDatosInforme(String idInforme)throws Exception;
		
		public List<DatosInformeDTO> obtieneDatosDatosInforme(String ceco,String idInforme,String idUsuario) throws Exception; 
		
		public boolean actualizaDatosInforme(DatosInformeDTO bean)throws Exception;
		
		
			
	}