package com.gruposalinas.checklist.domain;

public class VersionExpanDTO {
	


		private int idCheck;
		private int idVers;
		private int idTab;
		private int idactivo;
		private String aux;
		private String obs;
		private String periodo;
		private String nego;
		private String fechaLibera;
		private String protocolo;
		
		public int getIdCheck() {
			return idCheck;
		}
		public void setIdCheck(int idCheck) {
			this.idCheck = idCheck;
		}
		public int getIdVers() {
			return idVers;
		}
		public void setIdVers(int idVers) {
			this.idVers = idVers;
		}
		public int getIdTab() {
			return idTab;
		}
		public void setIdTab(int idTab) {
			this.idTab = idTab;
		}
		public int getIdactivo() {
			return idactivo;
		}
		public void setIdactivo(int idactivo) {
			this.idactivo = idactivo;
		}
		public String getAux() {
			return aux;
		}
		public void setAux(String aux) {
			this.aux = aux;
		}
		public String getObs() {
			return obs;
		}
		public void setObs(String obs) {
			this.obs = obs;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		
		public String getNego() {
			return nego;
		}
		public void setNego(String nego) {
			this.nego = nego;
		}
		
		public String getFechaLibera() {
			return fechaLibera;
		}
		public void setFechaLibera(String fechaLibera) {
			this.fechaLibera = fechaLibera;
		}
		
		public String getProtocolo() {
			return protocolo;
		}
		public void setProtocolo(String protocolo) {
			this.protocolo = protocolo;
		}
		@Override
		public String toString() {
			return "VersionExpanDTO [idCheck=" + idCheck + ", idVers=" + idVers + ", idTab=" + idTab + ", idactivo="
					+ idactivo + ", aux=" + aux + ", obs=" + obs + ", periodo=" + periodo + ", nego=" + nego
					+ ", fechaLibera=" + fechaLibera + ", protocolo=" + protocolo + "]";
		}
		
		
	
	}