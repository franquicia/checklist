package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.TransformacionDTO;

public class TransformacionRowMapper implements RowMapper<TransformacionDTO> {

	public TransformacionDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		TransformacionDTO TransformacionDTO = new TransformacionDTO();
		
		TransformacionDTO.setCeco(rs.getInt("FCID_CECO"));
		TransformacionDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
	    TransformacionDTO.setBitacora(rs.getInt("FIID_BITACORA"));
	    TransformacionDTO.setRecorrido(rs.getInt("FCRECORRIDO"));
	    TransformacionDTO.setFechafin(rs.getString("FDFECHA_FIN"));
	    TransformacionDTO.setFechaini(rs.getString("FDFECHA_INICIO"));
	    TransformacionDTO.setAux(rs.getString("FCAUX"));
	    TransformacionDTO.setPeriodo(rs.getString("FCPERIODO"));
		
		return TransformacionDTO;
	}

}


