package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;

public class ConteoSINOGralExtraRowMapper implements RowMapper<ChecklistPreguntasComDTO> {

	public ChecklistPreguntasComDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistPreguntasComDTO con = new ChecklistPreguntasComDTO();

	    con.setClasif(rs.getString("FCCLASIFICA"));
		con.setIdBitaGral(rs.getInt("FIID_BITGRAL"));
		con.setTotGral(rs.getDouble("TOTALGRAL"));
		con.setSumGrupo(rs.getInt("SUMA_GRUPO"));
	//	con.setPonTot(rs.getDouble("FCPONDTOTAL"));
	
		return con;
	}

}
