package com.gruposalinas.checklist.domain;

public class CheckinDTO {
	
	private int idCheck;
	private int idBitacora;
	private int idUsuario;
	private String idCeco;
	private String fecha;
	private String latitud;
	private String longitud;
	private int bandera;
	private String plataforma;
	private int tiempoConexion;
	
	
	public int getIdCheck() {
		return idCheck;
	}
	public void setIdCheck(int idCheck) {
		this.idCheck = idCheck;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public int getBandera() {
		return bandera;
	}
	public void setBandera(int bandera) {
		this.bandera = bandera;
	}
	public String getPlataforma() {
		return plataforma;
	}
	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}
	public int getTiempoConexion() {
		return tiempoConexion;
	}
	public void setTiempoConexion(int tiempoConexion) {
		this.tiempoConexion = tiempoConexion;
	}

	

	@Override
	public String toString() {
		return "CheckinDTO [idCheck=" + idCheck + ", idBitacora=" + idBitacora + ", idUsuario=" + idUsuario
				+ ", idCeco=" + idCeco + ", fecha=" + fecha + ", latitud=" + latitud + ", longitud=" + longitud
				+ ", bandera=" + bandera + ", plataforma=" + plataforma + ", tiempoConexion=" + tiempoConexion + "]";

	
}
}
