/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.business;

import com.gruposalinas.checklist.domain.ActivarCheckListDTO;
import com.gruposalinas.checklist.domain.AsignacionesSupDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistUsuarioDTO;
import com.gruposalinas.checklist.util.plataformaFrq.PlataformaFrqUtil;
import com.gruposalinas.checklist.util.plataformaFrq.TokenPlataforma;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author leodan1991
 */
public class AsigPuntoContactoBI {

    @Autowired
    AsignacionesSupBI asignacionesSupBI;
    @Autowired
    ChecklistBI checklistBI;
    @Autowired
    ChecklistUsuarioBI checklistUsuariobi;
    @Autowired
    ActivarCheckListBI activarCheckListBI;

    public void asignaProtocolosUsr() throws IOException {

        PlataformaFrqUtil plataformaUtil = new PlataformaFrqUtil();
        TokenPlataforma token = plataformaUtil.getTokenPlataforma("196228");
        List<AsignacionesSupDTO> asignacionesUsr = asignacionesSupBI.getAsignaciones(null, null, null, null, null, null, null);

        for (AsignacionesSupDTO asgn : asignacionesUsr) {
            String ceco = asgn.getIdCeco();
            int idUsuario = asgn.getIdUsuario();
            
            
            
            
            
            ArrayList<Integer> protocolos = plataformaUtil.getProtocolosByCeco(token.getAccessToken(), ceco);
            if (protocolos != null && protocolos.size() > 0) {

                //desactivaAsignaciones(idUsuario, ceco);
                
            	ActivarCheckListDTO a1 = new ActivarCheckListDTO();

                a1.setIdUsuario(""+idUsuario);
                a1.setIdCeco(ceco);
                a1.setIdChecklist(null);
                a1.setActivo("0");
                
                boolean res = activarCheckListBI.actualizaCheckList(a1);

                for (Integer idProtocolo : protocolos) {

                    List<ChecklistDTO> chks = checklistBI.buscaChecklistByProtocolo(idProtocolo);
                    for (ChecklistDTO chk : chks) {
                        int idChecklist = chk.getIdChecklist();

                        System.out.println("CHECK USUA---->> idUsuario: " + idUsuario + " ceco: " + ceco + " idChecklist: " + idChecklist);
                        //se inserta la asignacion
                        checklistUsuariobi.insertaCheckusuaEspecial(idUsuario, idChecklist, ceco);

                    }

                }

            }

        }

    }

    private void desactivaAsignaciones(int idUsuario, String ceco) {

        List<ChecklistUsuarioDTO> cus = checklistUsuariobi.obtieneCheckU(idUsuario + "");
        if (cus != null && cus.size() > 0) {
            for (ChecklistUsuarioDTO cu : cus) {
                if (cu.getIdCeco() == Integer.parseInt(ceco)) {

                    cu.setActivo(0);
                    checklistUsuariobi.actualizaChecklistUsuario(cu);
                }

            }

        }

    }
    
    public int  asignaProtocolosCeco(String ceco) throws IOException{
        int cantidadUsuarios=0;
        PlataformaFrqUtil plataformaUtil = new PlataformaFrqUtil();
        TokenPlataforma token = plataformaUtil.getTokenPlataforma("196228");
        List<AsignacionesSupDTO> asignacionesUsr =getAsigUsrByCeco(ceco);
        ArrayList<Integer> protocolos = plataformaUtil.getProtocolosByCeco(token.getAccessToken(), ceco);
            
        for (AsignacionesSupDTO asgn : asignacionesUsr) {
            cantidadUsuarios++;
            int idUsuario = asgn.getIdUsuario();
            if (protocolos != null && protocolos.size() > 0) {

                //desactivaAsignaciones(idUsuario, ceco);
                
                ActivarCheckListDTO a1 = new ActivarCheckListDTO();

                a1.setIdUsuario(""+idUsuario);
                a1.setIdCeco(ceco);
                a1.setIdChecklist(null);
                a1.setActivo("0");
                
                boolean res = activarCheckListBI.actualizaCheckList(a1);

                for (Integer idProtocolo : protocolos) {

                    List<ChecklistDTO> chks = checklistBI.buscaChecklistByProtocolo(idProtocolo);
                    for (ChecklistDTO chk : chks) {
                        int idChecklist = chk.getIdChecklist();

                        System.out.println("CHECK USUA---->> idUsuario: " + idUsuario + " ceco: " + ceco + " idChecklist: " + idChecklist);
                        //se inserta la asignacion
                        checklistUsuariobi.insertaCheckusuaEspecial(idUsuario, idChecklist, ceco);

                    }

                }

            }

        }
        
        return cantidadUsuarios;
    }
    
    private ArrayList<AsignacionesSupDTO> getAsigUsrByCeco(String ceco){
                
        ArrayList<AsignacionesSupDTO> asigCeco=new ArrayList<>();
        List<AsignacionesSupDTO> asignacionesUsr = asignacionesSupBI.getAsignaciones(null, null, null, null, null, null, null);
        if(asignacionesUsr!=null){
            for(AsignacionesSupDTO asig:asignacionesUsr){
                if(asig.getIdCeco().equalsIgnoreCase(ceco)){
                    asigCeco.add(asig);
                    
                }
            }
                
            
        }

        return asigCeco;
    }

}
