package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.NegocioDTO;

public class FiltrosNegocioRowMapper implements RowMapper<NegocioDTO>{

	@Override
	public NegocioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
	    NegocioDTO negocioDTO = new NegocioDTO();
	    negocioDTO.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
	    negocioDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
		return negocioDTO;
	}

}
