package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.EstadosDTO;


public interface EstadosDAO {

	  public int inserta(EstadosDTO bean)throws Exception;
		
		public boolean elimina(String idEstado)throws Exception;
		
	
		public List<EstadosDTO> obtieneDatos(int idEstado) throws Exception; 	//parametro
		
		
		public List<EstadosDTO> obtieneInfo() throws Exception; //sin parametros
		
		public List<EstadosDTO> obtieneDetalle() throws Exception; //todos los campos
		
		public boolean actualiza(EstadosDTO bean)throws Exception;
		
	}