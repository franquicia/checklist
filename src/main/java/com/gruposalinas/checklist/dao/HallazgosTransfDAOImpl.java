package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.HallazgosTransfDTO;
import com.gruposalinas.checklist.mappers.HallazgosEviTransfRowMapper;
import com.gruposalinas.checklist.mappers.HallazgosExpFaseProyRowMapper;
import com.gruposalinas.checklist.mappers.HallazgosHistoricoDetRowMapper;
import com.gruposalinas.checklist.mappers.HallazgosTransfRepoRowMapper;
import com.gruposalinas.checklist.mappers.HallazgosTransfRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class HallazgosTransfDAOImpl extends DefaultDAO implements HallazgosTransfDAO {

    private static Logger logger = LogManager.getLogger(HallazgosTransfDAOImpl.class);

    DefaultJdbcCall jdbcInsertaHallazgo;
    DefaultJdbcCall jdbcEliminaHallazgo;
    DefaultJdbcCall jdbcEliminaHallazgoBita;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneHallazgo;
    DefaultJdbcCall jdbcObtieneHallazgoRepo;
    DefaultJdbcCall jbdcActualizaHallazgo;
    DefaultJdbcCall jbdcActualizaHallazgoMov;
    DefaultJdbcCall jbdcActualizaHallazgoResp;
    DefaultJdbcCall jbdcCargaHallazgo;
    DefaultJdbcCall jbdcDepuraHallazgo;
    DefaultJdbcCall jbdcCargaHallazgoxBita;
    DefaultJdbcCall jbdcActualizaAdicionalHallazgo;
    //HISTORICO
    DefaultJdbcCall jbdcCargaHistorico;
    DefaultJdbcCall jbdcInsertaHisto;
    DefaultJdbcCall jbdcEliminaHistbita;
    DefaultJdbcCall jbdcObtieneHistFolio;
    DefaultJdbcCall jbdcObtieneHistBita;
    DefaultJdbcCall jbdcObtieneDetaHist;
    DefaultJdbcCall jdbcObtieneHallazgoBita;
    DefaultJdbcCall jdbcObtieneHallaHistoFaseProyecto;
    DefaultJdbcCall jdbcObtieneHallaFaseProyecto;
    DefaultJdbcCall jdbcObtieneHallaFaseProyectoIniciales;
    DefaultJdbcCall jbdcCargaHallazgoNvoFlujo;
    //expansion
    DefaultJdbcCall jdbcObtieneHallazgoTransf;

    public void init() {

        jdbcInsertaHallazgo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLATRANSF")
                .withProcedureName("SP_INS_HALLAZ");

        jdbcEliminaHallazgoBita = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLATRANSF")
                .withProcedureName("SP_DE_HALLAZ");

        jdbcObtieneHallazgo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLATRANSF")
                .withProcedureName("SP_SEL_HALLAZ")
                .returningResultSet("RCL_HALLAZ", new HallazgosTransfRowMapper());

        //Reporte
        jdbcObtieneHallazgoRepo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLATRANSF")
                .withProcedureName("SP_SEL_HAREPO")
                .returningResultSet("RCL_HALLAZ", new HallazgosTransfRepoRowMapper());//HallazgosTransfRepoRowMapper
        //update  web
        jbdcActualizaHallazgo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLATRANSF")
                .withProcedureName("SP_ACT_HALLAZ");

        //update movil
        jbdcActualizaHallazgoMov = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLATRANSF")
                .withProcedureName("SP_ACT_RESPNVO");

        jbdcActualizaHallazgoResp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLATRANSF")
                .withProcedureName("SP_ACT_RESP");

        jbdcCargaHallazgo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLATRANSF")
                .withProcedureName("SP_CARGA_HALLAZ");

        jbdcDepuraHallazgo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLATRANSF")
                .withProcedureName("SP_DEPURA_HALLAZ");

        jbdcCargaHallazgoxBita = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLATRANSF")
                .withProcedureName("SP_CARGA_HALLAZ");

        //histoorico
        jbdcCargaHistorico = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHISHALLATRANS")
                .withProcedureName("SP_CARGA_HALLAZH");

        jbdcInsertaHisto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHISHALLATRANS")
                .withProcedureName("SP_INSHALLA");

        jbdcEliminaHistbita = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHISHALLATRANS")
                .withProcedureName("SP_DEL_HALLAZ");

        jbdcObtieneHistFolio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHISHALLATRANS")
                .withProcedureName("SP_DETA_FOLIO")
                .returningResultSet("RCL_HISTORICO", new HallazgosHistoricoDetRowMapper());

        jbdcObtieneHistBita = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHISHALLATRANS")
                .withProcedureName("SP_SEL_HISBITA")
                .returningResultSet("RCL_HISTORICO", new HallazgosHistoricoDetRowMapper());

        jbdcObtieneDetaHist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHISHALLATRANS")
                .withProcedureName("SP_DETA_HALLAZ")
                .returningResultSet("RCL_HISTORICO", new HallazgosHistoricoDetRowMapper());

        //BITACORA
        jdbcObtieneHallazgoBita = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLATRANSF")
                .withProcedureName("SP_SEL_EVIXBITA")
                .returningResultSet("RCL_EVIHALLAZ", new HallazgosEviTransfRowMapper());
        //tabla hallazgo historico ceco fase proyecto nvo flujo
        //jdbcObtieneHallaFaseProyecto
        jdbcObtieneHallaHistoFaseProyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADASHEXPNVO")
                .withProcedureName("SP_SEL_HALLAHIS")
                .returningResultSet("RCL_HALLAZ", new HallazgosExpFaseProyRowMapper());

        //tabla hallazgo  ceco fase proyecto nvo flujo
        //jdbcObtieneHallaHistoFaseProyecto
        jdbcObtieneHallaFaseProyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADASHEXPNVO2")
                .withProcedureName("SP_SEL_HALLAZ")//
                .returningResultSet("RCL_HALLAZ", new HallazgosExpFaseProyRowMapper());

        //tabla hallazgos Iniciales
        jdbcObtieneHallaFaseProyectoIniciales = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADASHEXPNVO2")
                .withProcedureName("SP_SEL_HALLAINI")
                .returningResultSet("RCL_HALLAZ", new HallazgosExpFaseProyRowMapper());

        jbdcCargaHallazgoNvoFlujo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACARGAHALLATRA")
                .withProcedureName("SP_CARGA_HALLAZ");

        jbdcActualizaAdicionalHallazgo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHISHALLATRANS")
                .withProcedureName("SP_ADICHALLA");
    }

    public int inserta(HallazgosTransfDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_USUARIO", bean.getUsuario())
                .addValue("PA_VERSION", bean.getIdVersion())
                .addValue("PA_PROYECTO", bean.getIdProyecto());

        out = jdbcInsertaHallazgo.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAHALLATRANSF.SP_INS_HALLAZ}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == -1) {
            logger.info("No existen Asignaciones");
            error = 0;
        }

        return error;
    }

    public boolean eliminaBita(String bitGral) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITAGRAL", bitGral);

        out = jdbcEliminaHallazgoBita.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAHALLATRANSF.SP_DE_HALLAZ}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + bitGral + ")");
        } else {
            return true;
        }

        return false;
    }

    //parametro
    @SuppressWarnings("unchecked")
    public List<HallazgosTransfDTO> obtieneDatos(String ceco, String proyecto, String fase) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_PROYECTO", proyecto)
                .addValue("PA_FASE", fase);

        out = jdbcObtieneHallazgo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAHALLATRANSF.SP_SEL_HALLAZ}");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosTransfDTO>) out.get("RCL_HALLAZ");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos del ceco(" + ceco + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;

    }

    //reporte
    @SuppressWarnings("unchecked")
    public List<HallazgosTransfDTO> obtieneDatosRepo(String ceco, String proyecto, String fase) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_PROYECTO", proyecto)
                .addValue("PA_FASE", fase);

        out = jdbcObtieneHallazgoRepo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAHALLATRANSF.SP_SEL_HAREPO}");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosTransfDTO>) out.get("RCL_HALLAZ");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos del ceco(" + ceco + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;

    }
    // web

    public boolean actualiza(HallazgosTransfDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDRESP", bean.getIdResp())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_POSIBLE", bean.getRespuesta())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_OBS", bean.getObs())
                .addValue("PA_OBSNUE", bean.getObsNueva())
                .addValue("PA_IDHALLA", bean.getIdHallazgo())
                .addValue("PA_FECHAAUTO", bean.getFechaAutorizo().replace("-", ""))
                .addValue("PA_FECHAFIN", bean.getFechaFin().replace("-", ""))
                .addValue("PA_USUMODIF", bean.getUsuModif());

        out = jbdcActualizaHallazgo.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAHALLATRANSF.SP_ACT_HALLAZ}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdHallazgo() + ")");
        } else {
            return true;
        }
        return false;

    }
    // mov

    public Map<String, Object> actualizaMov(HallazgosTransfDTO bean) throws Exception {

        Map<String, Object> out = null;
        Map<String, Object> response = new HashMap<String, Object>();
        int error = 0;
        String validaCompleto = "";

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDRESP", bean.getIdResp())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_POSIBLE", bean.getRespuesta())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_OBS", bean.getObs())
                .addValue("PA_OBSNUE", bean.getObsNueva())
                .addValue("PA_IDHALLA", bean.getIdHallazgo())
                .addValue("PA_FOLIO", bean.getIdFolio())
                .addValue("PA_FECHAAUTO", bean.getFechaAutorizo().replace("-", ""))
                .addValue("PA_FECHAFIN", bean.getFechaFin().replace("-", ""))
                .addValue("PA_USUMODIF", bean.getUsuModif())
                .addValue("PA_MOTIVRECHA", bean.getMotivrechazo())
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_USUARIO", bean.getUsuario())
                .addValue("PA_TIPOEVI", bean.getTipoEvi());

        out = jbdcActualizaHallazgoMov.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAHALLATRANSF.SP_ACT_RESPNVO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        String valida = (String) out.get("PA_VERIFICA");
        validaCompleto = valida.toString();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdHallazgo() + ")");
        } else {

            response.put("response", error);
            response.put("validaCompleto", validaCompleto);
            return response;
        }
        return response;
    }

    @Override
    public boolean actualizaResp(HallazgosTransfDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDRESP", bean.getIdResp())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_POSIBLE", bean.getRespuesta())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_OBS", bean.getObs())
                .addValue("PA_OBSNUE", bean.getObsNueva())
                .addValue("PA_IDHALLA", bean.getIdHallazgo())
                .addValue("PA_FECHAAUTO", bean.getFechaAutorizo().replace("-", ""))
                .addValue("PA_FECHAFIN", bean.getFechaFin().replace("-", ""))
                .addValue("PA_USUMODIF", bean.getUsuModif())
                .addValue("PA_MOTIVRECHA", bean.getMotivrechazo());

        out = jbdcActualizaHallazgoResp.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAHALLATRANSF.SP_ACT_RESP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdHallazgo() + ")");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean cargaHallazgosxBita(int bitGral, int cargaIni) throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITAGRAL", bitGral)
                .addValue("PA_CARGAINI", cargaIni);

        out = jbdcCargaHallazgoxBita.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAHALLATRANSF.SP_CARGA_HALLAZ}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener LA CARGA x bita");
        } else {
            return true;
        }

        return false;
    }

//HALLAZGOS HISTORICO
    @SuppressWarnings("unchecked")
    public List<HallazgosTransfDTO> obtieneHistFolio(int idFolio) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FOLIO", idFolio);

        out = jbdcObtieneHistFolio.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAHISHALLATRANS.SP_DETA_FOLIO}");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosTransfDTO>) out.get("RCL_HISTORICO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos de la bitagral(" + idFolio + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<HallazgosTransfDTO> obtieneHistBita(String bitGral) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bitGral);

        out = jbdcObtieneHistBita.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAHISHALLATRANS.SP_SEL_HISBITA}");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosTransfDTO>) out.get("RCL_HISTORICO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos de la bitagral(" + bitGral + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;

    }

    @Override
    public boolean insertaHallazgosHist(int idHallazgo) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDHALLAZ", idHallazgo);

        out = jbdcInsertaHisto.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAHISHALLATRANS.SP_INSHALLA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el usuario(" + idHallazgo + ")");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargaHallazgosHistorico() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jbdcCargaHistorico.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAHISHALLATRANS.SP_CARGA_HALLAZ}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener LA CARGA ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaBitaHisto(String bitGral) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", bitGral);

        out = jbdcEliminaHistbita.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAHISHALLATRANS.SP_DEL_HALLAZ}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + bitGral + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<HallazgosTransfDTO> obtieneDatosBita(int proyecto, int fase, int ceco) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROYECTO", proyecto)
                .addValue("PA_FASE", fase)
                .addValue("PA_CECO", ceco);

        out = jdbcObtieneHallazgoBita.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAEVIHALLAZ.SP_DETA_RESP}");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosTransfDTO>) out.get("RCL_EVIHALLAZ");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos del ceco(" + ceco + ")");
        }
        if (error == -1) {
            return null;
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<HallazgosTransfDTO> obtieneHistoricoHallazgoDash(String ceco, String proyecto, String fase) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_PROYECTO", proyecto)
                .addValue("PA_FASE", fase);

        out = jdbcObtieneHallaHistoFaseProyecto.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PADASHEXPNVO.SP_SEL_HALLAHIS}");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosTransfDTO>) out.get("RCL_HALLAZ");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos del ceco(" + ceco + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<HallazgosTransfDTO> obtieneHallazgosExpansion(String ceco, String proyecto, String fase) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_PROYECTO", proyecto)
                .addValue("PA_FASE", fase);

        out = jdbcObtieneHallaFaseProyecto.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PADASHEXPNVO2.SP_SEL_hallazg");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosTransfDTO>) out.get("RCL_HALLAZ");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos del ceco(" + ceco + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<HallazgosTransfDTO> obtieneHallazgosExpansionIni(String ceco, String proyecto, String fase) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_PROYECTO", proyecto)
                .addValue("PA_FASE", fase);

        out = jdbcObtieneHallaFaseProyectoIniciales.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PADASHEXPNVO2.SP_SEL_hallazgINI");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosTransfDTO>) out.get("RCL_HALLAZ");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos del ceco(" + ceco + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;

    }

    @Override
    public boolean cargaHallazgosxBitaNvo(int bitGral, int ceco, int fase, int proyecto) throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITAGRAL", bitGral)
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto);

        out = jbdcCargaHallazgoNvoFlujo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PACARGAHALLATRA.SP_CARGA_HALLAZNVO}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener LA CARGA x bita");
        } else {
            return true;
        }

        return false;
    }

    public boolean actualizaAdicionalHalla(HallazgosTransfDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDHALLA", bean.getIdHallazgo())
                .addValue("PA_POSIBLE", bean.getRespuesta())
                .addValue("PA_USUMODIF", bean.getUsuModif())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_OBS", bean.getObs());
        out = jbdcActualizaAdicionalHallazgo.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAHALLATRANSF.SP_ADICHALLA ADICIONAL A HALLAZGO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdHallazgo() + ")");
        } else {
            return true;
        }
        return false;
    }

}
