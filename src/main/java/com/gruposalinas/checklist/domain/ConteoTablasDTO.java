package com.gruposalinas.checklist.domain;

public class ConteoTablasDTO {
	


		private double checkUsua;
		private double bitacoras;
		private double cecos;
		private double usuarios;
		private double tokens;
		private double pasoCeco;
		private double bitNulas;
		private double bitNoNulas;
		private String esquema;
		private double aux;
		private String aux2;
		
		public double getCheckUsua() {
			return checkUsua;
		}
		public void setCheckUsua(double checkUsua) {
			this.checkUsua = checkUsua;
		}
		public double getBitacoras() {
			return bitacoras;
		}
		public void setBitacoras(double bitacoras) {
			this.bitacoras = bitacoras;
		}
		public double getCecos() {
			return cecos;
		}
		public void setCecos(double cecos) {
			this.cecos = cecos;
		}
		public double getUsuarios() {
			return usuarios;
		}
		public void setUsuarios(double usuarios) {
			this.usuarios = usuarios;
		}
		public double getTokens() {
			return tokens;
		}
		public void setTokens(double tokens) {
			this.tokens = tokens;
		}
		public double getPasoCeco() {
			return pasoCeco;
		}
		public void setPasoCeco(double pasoCeco) {
			this.pasoCeco = pasoCeco;
		}
		public double getBitNulas() {
			return bitNulas;
		}
		public void setBitNulas(double bitNulas) {
			this.bitNulas = bitNulas;
		}
		public double getBitNoNulas() {
			return bitNoNulas;
		}
		public void setBitNoNulas(double bitNoNulas) {
			this.bitNoNulas = bitNoNulas;
		}
		public String getEsquema() {
			return esquema;
		}
		public void setEsquema(String esquema) {
			this.esquema = esquema;
		}
		public double getAux() {
			return aux;
		}
		public void setAux(double aux) {
			this.aux = aux;
		}
		public String getAux2() {
			return aux2;
		}
		public void setAux2(String aux2) {
			this.aux2 = aux2;
		}
		@Override
		public String toString() {
			return "ConteoTablasDTO [checkUsua=" + checkUsua + ", bitacoras=" + bitacoras + ", cecos=" + cecos
					+ ", usuarios=" + usuarios + ", tokens=" + tokens + ", pasoCeco=" + pasoCeco + ", bitNulas="
					+ bitNulas + ", bitNoNulas=" + bitNoNulas + ", esquema=" + esquema + ", aux=" + aux + ", aux2="
					+ aux2 + "]";
		}
		
		

	}