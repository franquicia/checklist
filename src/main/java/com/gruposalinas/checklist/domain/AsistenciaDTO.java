package com.gruposalinas.checklist.domain;

public class AsistenciaDTO {
	private int dia;
	private String horaEntrada;
	private String horaSalita;
	private int numSuc;
	private String sucursales;
	public int getDia() {
		return dia;
	}
	public void setDia(int dia) {
		this.dia = dia;
	}
	public String getHoraEntrada() {
		return horaEntrada;
	}
	public void setHoraEntrada(String horaEntrada) {
		this.horaEntrada = horaEntrada;
	}
	public String getHoraSalita() {
		return horaSalita;
	}
	public void setHoraSalita(String horaSalita) {
		this.horaSalita = horaSalita;
	}
	public int getNumSuc() {
		return numSuc;
	}
	public void setNumSuc(int numSuc) {
		this.numSuc = numSuc;
	}
	@Override
	public String toString() {
		return "AsistenciaDTO [dia=" + dia + ", horaEntrada=" + horaEntrada + ", horaSalita=" + horaSalita + ", numSuc="
				+ numSuc + ", sucursales=" + sucursales + "]";
	}
	public String getSucursales() {
		return sucursales;
	}
	public void setSucursales(String sucursales) {
		this.sucursales = sucursales;
	}
}
