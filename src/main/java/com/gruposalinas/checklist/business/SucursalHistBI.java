package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.SucursalHistDAO;
import com.gruposalinas.checklist.domain.SucursalHistDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class SucursalHistBI {

	private static Logger logger = LogManager.getLogger(SucursalHistBI.class);
	
	@Autowired
	SucursalHistDAO sucursalHistDAO;
	
	List<SucursalHistDTO> listaSucursales = null;

	public List<SucursalHistDTO> obtieneSucursal(String idHistSuc, String numSuc){
		try{
			listaSucursales = sucursalHistDAO.obtieneSucursal(idHistSuc, numSuc);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Sucursal");
					
		}
				
		return listaSucursales;
	}
}
