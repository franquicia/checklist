package com.gruposalinas.checklist.business;

import com.gruposalinas.checklist.dao.IdICUTiempoRealDAOImpl;
import com.gruposalinas.checklist.domain.CRMDTO;
import com.gruposalinas.checklist.domain.IdICUTiempoRealDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class ICUTiempoRealBI {

    private static Logger logger = LogManager.getLogger(ICUTiempoRealBI.class);

    @Autowired
    IdICUTiempoRealDAOImpl idICUTiempoRealDAOImpl;

    public boolean insertaICUTiempoRealBI(IdICUTiempoRealDTO obj) {

        boolean res = false;

        try {

            res = idICUTiempoRealDAOImpl.inserta(obj);

        } catch (Exception e) {

            e.printStackTrace();

        }

        return res;
    }

    public String getTokenCRM() {

        String usuario = FRQConstantes.getUsuarioTokenWSCRM();
        String password = FRQConstantes.getPasswordTokenWSCRM();
        String salida = null;

        BufferedReader rd = null;
        OutputStreamWriter ou = null;
        InputStreamReader inputStream = null;
        try {

            String data = usuario + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(data.getBytes(StandardCharsets.UTF_8));
            URL url = new URL(FRQConstantes.getURLMensajesPersonalizadosTokenWiFI());
            //  URL url = new URL("http://10.54.36.125:8339/dev/oauth");
            String authHeaderValue = "Basic " + new String(encodedAuth);
            String body = URLEncoder.encode("grant_type", "UTF-8") + "=" + URLEncoder.encode("client_credentials", "UTF-8");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Charset", "UTF-8");
            conn.setRequestProperty("Authorization", authHeaderValue);
            conn.setUseCaches(false);
            conn.connect();
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(body);
            wr.flush();

            // Obtener el estado del recurso
            int statusCode = conn.getResponseCode();

            logger.info("statusCode getTokenCRM:" + statusCode);

            StringBuffer res = new StringBuffer();
            inputStream = new InputStreamReader(conn.getInputStream());
            rd = new BufferedReader(inputStream);

            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }
            salida = res.toString();
        } catch (Exception e) {
            salida = null;
            e.printStackTrace();
        } finally {
            try {

                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                //logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }

            try {
                if (ou != null) {
                    ou.close();
                }
            } catch (Exception e) {
                //logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }
            try {
                if (inputStream != null) {
                    inputStream.close();
                }

            } catch (Exception e) {
                //logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }
        }

        return salida;
    }

    public String setData(String token, CRMDTO data) throws IOException {
        String salida = null;
        BufferedReader rd = null;
        OutputStreamWriter ou = null;
        InputStreamReader inputStream = null;
        try {
            URL url = new URL(FRQConstantes.getURLMensajesPersonalizadosWiFI());

            // URL url = new URL("http://10.54.36.125:8339/dev/crm/v1/transaccional/wifi/registrar");
            //  logger.info("url:" + url);
            String authHeaderValue = "Bearer " + token;
            //logger.info("authHeaderValue " + authHeaderValue);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", authHeaderValue);

            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Charset", "UTF-8");
            conn.setUseCaches(false);
            conn.connect();

            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(data);
            System.out.println("ResultingJSONstring = " + json);
            byte[] out = json.getBytes(StandardCharsets.UTF_8);

            OutputStream stream = conn.getOutputStream();
            stream.write(out);

            // Obtener el estado del recurso
            int statusCode = conn.getResponseCode();

            logger.info("statusCode setCRM:" + statusCode);

            StringBuffer res = new StringBuffer();
            inputStream = new InputStreamReader(conn.getInputStream());
            rd = new BufferedReader(inputStream);

            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }
            salida = res.toString();
        } catch (Exception e) {
            salida = null;
            e.printStackTrace();
        } finally {
            try {

                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                //logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }

            try {
                if (ou != null) {
                    ou.close();
                }
            } catch (Exception e) {
                //logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }
            try {
                if (inputStream != null) {
                    inputStream.close();
                }

            } catch (Exception e) {
                //logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }
        }

        return salida;
    }

}
