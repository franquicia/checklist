package com.gruposalinas.checklist.business;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.checklist.dao.VisitaTiendaDAO;
import com.gruposalinas.checklist.domain.DatosCecoDTO;
import com.gruposalinas.checklist.domain.VisitaTiendaDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class VisitaTiendaBI {
	
	Logger logger = LogManager.getLogger(VisitaTiendaBI.class);
	
	@Autowired
	VisitaTiendaDAO visitaTiendaDAO;
	
	@SuppressWarnings("unchecked")
	public String obtieneVisitas(String idChecklist, String idUsuario, String nuMes, String anio){
		
		JsonObject principal = new JsonObject(); 
		String idCeco = "";
		String nombreCeco = "";
		String idCecoPadre = "";
		String nombreCecoPadre ="";

		try {	
			
			Map<String, Object> datosVisitas = visitaTiendaDAO.obtieneVisitas(idChecklist, idUsuario, nuMes, anio);

			List<VisitaTiendaDTO> visitas = (List<VisitaTiendaDTO>) datosVisitas.get("visitas");
			
			List<DatosCecoDTO> datosCeco = (List<DatosCecoDTO>) datosVisitas.get("datosCeco");
			
			if(datosCeco.size() > 0){
				idCeco = datosCeco.get(0).getIdCeco();
				nombreCeco = datosCeco.get(0).getNombreCeco();
				idCecoPadre = datosCeco.get(0).getIdCecoPadre();
			    nombreCecoPadre = datosCeco.get(0).getNombreCecoPadre();
			}
			
			principal.addProperty("zona", idCecoPadre );
			principal.addProperty("nombreZona", nombreCecoPadre);
			principal.addProperty("region",idCeco);
			principal.addProperty("nombreRegion", nombreCeco);
			
			int cont = 0;

			VisitaTiendaDTO visitaAnt = null;


			JsonArray tiendas = new JsonArray();

			while(cont < visitas.size()){


				JsonArray fechasArray = new JsonArray();
				int numVisitas = 0;
				visitaAnt = new VisitaTiendaDTO();

				visitaAnt = visitas.get(cont);


				while(cont < visitas.size() && visitaAnt.getIdCeco() == visitas.get(cont).getIdCeco() ){
					JsonObject fechasVisita = null;

					if( visitas.get(cont).getFechaEnvio() != null )
						numVisitas ++;

					if(visitas.get(cont).getFechaInicio() != null){
						fechasVisita =  new JsonObject();
						fechasVisita.addProperty("fechaInicio", visitas.get(cont).getFechaInicio());
						fechasVisita.addProperty("fechaFin", visitas.get(cont).getFechaFin());
						fechasVisita.addProperty("fechaEnvio", visitas.get(cont).getFechaEnvio());
						fechasArray.add(fechasVisita);
					}


					cont++;
				}

				JsonObject tienda = new JsonObject(); 
				tienda.addProperty("idceco", visitaAnt.getIdCeco());
				tienda.addProperty("nombreCeco", visitaAnt.getNombreCeco());
				tienda.addProperty("numVisitas", numVisitas);
				tienda.add("fechasVisitas", fechasArray);
				tienda.addProperty("ultimaVisita", visitaAnt.getUltimaVisita());
				tiendas.add(tienda);

			}

			principal.add("tiendas", tiendas);


		} catch (Exception e) {
			logger.info("Ocurrio un error al obtener las visitas para el usuario ("+idUsuario+") y checklist ("+idChecklist+")");
				
		}

		return principal.toString();
	}
	

}
