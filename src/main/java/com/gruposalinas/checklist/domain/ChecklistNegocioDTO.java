package com.gruposalinas.checklist.domain;

public class ChecklistNegocioDTO {
	
	private int checklist;
	private int negocio;
	
	public int getChecklist() {
		return checklist;
	}
	public void setChecklist(int checklist) {
		this.checklist = checklist;
	}
	public int getNegocio() {
		return negocio;
	}
	public void setNegocio(int negocio) {
		this.negocio = negocio;
	}
	
	
}
