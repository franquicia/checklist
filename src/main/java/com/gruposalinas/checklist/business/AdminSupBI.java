package com.gruposalinas.checklist.business;

import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.AdminSupDAO;
import com.gruposalinas.checklist.domain.AdminSupDTO;
import com.gruposalinas.checklist.domain.Admin2SupDTO;

public class AdminSupBI {
		
	private static Logger logger = LogManager.getLogger(AdminSupBI.class);
	
	@Autowired
	AdminSupDAO adminSupDAO;
	List<AdminSupDTO> listaConsulta = null;
	Map<String,Object> respuesta = null;

	public List<AdminSupDTO> busquedaEmpleado(int idUsuario){	
		try {
			listaConsulta = adminSupDAO.getEmpleado(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}			
		return listaConsulta;
	}

	public Map<String,Object> detalleMenu(int idUsuario){		
		try {
			respuesta = adminSupDAO.getDetalleEmpleado(idUsuario);
		} catch (Exception e) {
			logger.info(e);
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}			
		return respuesta;
	}

	public Map<String,Object> personalAsig(int idUsuario){
		try {
			respuesta = adminSupDAO.getPersonalAsig(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}
		return respuesta;
	}
	
	public Map<String,Object> cecosAsig(int idUsuario){		
		try {
			respuesta = adminSupDAO.getCecosAsig(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}
		return respuesta;
	}

	public List<AdminSupDTO> busquedaCeco(String ceco){	
		try {
			listaConsulta = adminSupDAO.busquedaCeco(ceco);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}
		return listaConsulta;
	}

}
