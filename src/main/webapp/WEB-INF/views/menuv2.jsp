<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!--menu principal -->
		<div class="menu-principal">
		    <div class="contenedor">
		        <nav>
		        	<ul>
		        	    <li><a href="#"><img src="../images/v2/iconTriunfo.svg" id="copa"><span id="triunfo">Triunfo</span></a>
		        	        <ul >
								<li><a href="#"><span class="cuadro"></span>¿Qué es?</a></li>
								<li><a href="#"><span class="cuadro"></span>¿Qué tengo que verificar?</a></li>
								<li><a href="#"><span class="cuadro"></span>¿Qué tengo que hacer?</a></li>
								<li><a href="#"><span class="cuadro"></span>Nuestras guías operativas</a></li>
								<li><a href="Index.html"><span class="cuadro"></span>Administrador checklist</a></li>
					        </ul>
		        	    </li>

		        	    <li><a href="#"><img src="../images/v2/icon7s.svg" id="reloj"><span id="siete">7S</span></a></li>
			            <li><a href=""><img src="../images/v2/iconAyuda.svg" id="interrogante"><span id="ayuda">Ayuda<span></a></li> 
		        	</ul>
		        </nav>
		    </div>
		</div>
