<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="es">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
<title>Supervisi&oacute;n Sucursales</title>
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/estilos.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/menu.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/secciones.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/dropkick.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/paginador.css" media="screen">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/calendario.css" media="screen">

</head>
<script type="text/javascript">
function porcGen(i, a, b, c){
	var aa = parseFloat(calculaPromedio('promP'+i, a, b));
	var bb = parseFloat(calculaPromedio('promR'+i, c, b));

	if(aa==0 && bb==0){
		document.getElementById("promR"+i).style.color = "#5fc297"; //#5fc297 verde
		$('#barraPor'+i).append("<div style='width:0%; background-color: #5fc297 !important; height: 7px; border-radius: 28px !important;'></div>");
	}else{
		if(bb<aa){
			document.getElementById("promR"+i).style.color = "#d0001b"; //#d0001b rojo
			$('#barraPor'+i).append("<div style='width: "+bb+"%; background-color: #d0001b !important; height: 7px; border-radius: 28px !important;'></div>");
		}else{
			document.getElementById("promR"+i).style.color = "#5fc297"; //#5fc297 verde
			$('#barraPor'+i).append("<div style='width: "+bb+"%; background-color: #5fc297 !important; height: 7px; border-radius: 28px !important;'></div>");
		}
	}
	
}

function calculaPromedio(i, a, b){
	if(a==0 || b==0){
		document.getElementById(i).innerHTML = '0%';
		return '0';
	}else{
		var c = (a*100)/b;  
		var d = c.toFixed(2);

		if(d.includes('.00')){
			document.getElementById(i).innerHTML = d.split('.')[0]+'%';
			return d.split('.')[0];
		}else{
			document.getElementById(i).innerHTML = d+'%';
			return d;
		}
	}
}
</script>

<body>

	<div class="page" style="height: 100%;">
		<div class="clear"></div>
		<div class="title" id="title">
			<div class="wrapper">
				<div class="h2">
					<a href="/checklist/central/vistaSupervisionSistemasV2.htm">P&aacute;gina Principal </a> / <a>Supervisi&oacute;n Sistemas</a>
				</div>
				<h1>Supervisi&oacute;n Sistemas</h1>
			</div>
		</div>

		<div class="clear"></div>

		<!-- Contenido -->
		<div class="contHome">

			<div class="titN" style="margin-top: 10px; margin-bottom: 10px;">
				<table style="width: 95%; margin: auto; font-family: 'Avenir_Next_Regular'; font-weight: 600; font-style: normal; font-size: 18px; color: #000;">
					<tbody>
						<tr>
							<td>Visita de Sucursales 2018</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="home" style="width: 95%;">
				
				<!-- Pagina 1 -->
				<div class="pagedemo _current" style="display: inline-block; width: 67%; float: left;">


					<table style="width: 100%; margin: auto; text-align: center; color: #000; font-size: 11px; height: 25px; ">
						<tbody>
							<tr style="vertical-align: text-top;">
								<th style="width: 30%;">Sistemas</th>
								<th style="width: 10%;">Sucursales Visitadas</th>
								<th style="width: 10%;">Recursos</th>
								<th style="width: 15%;">% Planeado</th>
								<th style="width: 20%;"></th>
								<th style="width: 15%;">% Real</th>
							</tr>
						</tbody>
					</table>
					
					<div style="overflow: scroll; height: 500px;">
						<table class="tblGeneral">
							<tbody>
								<c:if test="${fn:length(listaReporteSistemas) > 0}">
									<c:forEach var="i" begin="0" end="${(fn:length(listaReporteSistemas))-1}">
										<tr style='height: 40px;'>
									
											<td style="width: 30%; height:20px; color: #000;">${listaReporteSistemas[i].nombreCeco}</td>
											<td style="width: 10%; height:20px; color: #000;">${listaReporteSistemas[i].terminados}</td>
											<td style="width: 10%; height:20px; color: #000; text-align:center;">${listaReporteSistemas[i].recursos}</td>
											<td style="width: 15%; height:20px; color: #000;" id="promP${i}"></td>
											<td style="width: 20%; height:20px; color: #000;">
												
												<table style="max-width: 800px; width: 100%;">
													<thead>
														<tr>
															<td style="border-bottom: 0px solid #d0001b !important; height: 15px;">
																<div id="barraPor${i}" style="border-radius: 28px !important; overflow: hidden; color: #000 !important; background-color: #f1f1f1 !important; border: 0px solid #dddddd;">																	
																</div>
															</td>															
														</tr>
													</thead>
												</table>
												
											</td>
											<td style="width: 15%; height:20px; color: #000;" id="promR${i}"></td>
											
											<script>
												porcGen(${i},${listaReporteSistemas[i].planeado},${listaReporteSistemas[i].asignados},${listaReporteSistemas[i].terminados});
											</script>
											
										</tr>
									</c:forEach>									
								</c:if>
							</tbody>
						</table>
					</div>
					
				</div>
				
				
				<div class="pagedemo _current" style="display: inline-block; width: 30%; float: right; height: 525px;">
				
					<table style="width: 100%; margin: auto; text-align: left; color: #000; font-size: 11px; height: 25px; ">
						<tbody>
							<tr>
								<th style="width: 100%; padding-left: 10px;">Meta de Visitas</th>
							</tr>
						</tbody>
					</table>
					
					<table style="width: 100%; margin: auto; text-align: center; color: #000; font-size: 11px; border-top: 2px solid #4fd6b8 !important; background: #fff;">
						<tbody>
						
							<tr style='height: 25px;'>										
								<td style="width: 50%; font-size: 12px; font-weight: 600;">M�xico</td>
								<td style="width: 50%; font-size: 12px; font-weight: 600;">LAM</td>
							</tr>
						
							<tr style='height: 25px; border-style: hidden;'>										
								<td style="width: 50%; font-weight: 600;">${(totalMeta-listaReporteSistemasLAM[0].asignados)}</td>
								<td style="width: 50%; font-weight: 600;">${listaReporteSistemasLAM[0].asignados}</td>
							</tr>
	
						</tbody>
					</table>	
					
					<table style="width: 100%; margin: auto; text-align: left; color: #000; font-size: 11px; height: 25px; margin-top: 20px;">
						<tbody>
							<tr>
								<th style="width: 100%; padding-left: 10px;">Visitas hasta mes de ${fecha}</th>
							</tr>
						</tbody>
					</table>
					
					<table style="width: 100%; margin: auto; text-align: center; color: #000; font-size: 11px; border-top: 2px solid #4fd6b8 !important; background: #fff;">
						<tbody>
						
							<tr style='height: 25px;'>										
								<td style="width: 50%; font-size: 12px; font-weight: 600;">M�xico</td>
								<td style="width: 50%; font-size: 12px; font-weight: 600;">LAM</td>
							</tr>
						
							<tr style='height: 25px; border-style: hidden;'>										
								<td style="width: 50%; font-weight: 600;">${(totalVisitas-listaReporteSistemasLAM[0].terminados)}</td>
								<td style="width: 50%; font-weight: 600;">${listaReporteSistemasLAM[0].terminados}</td>
							</tr>
	
						</tbody>
					</table>	
					
					
					<table style="border-bottom: 2px solid #4fd6b8 !important; background: #fff; width: 90%; margin: auto; text-align: center; font-size: 12px; height: 45px; margin-top: 251px; float: right;">
						<tbody>
							<tr>
								<th style="width: 60%; text-align: justify; padding-left: 5px;">Sucursales Visitadas</th>
								<th style="width: 40%; text-align: right; padding-right: 5px;">${totalVisitas}</th>
							</tr>
						</tbody>
					</table>
					
					<table style="border-bottom: 2px solid #4fd6b8 !important; background: #fff;width: 90%; margin: auto; text-align: center;  font-size: 12px; height: 45px; margin-top: 10px; float: right;">
						<tbody>
							<tr>
								<th style="width: 60%; text-align: justify; padding-left: 5px;">Empleados Enviados</th>
								<th style="width: 40%; text-align: right; padding-right: 5px;">${(totalRecursos)}</th>
							</tr>
						</tbody>
					</table>
					
					
					
				</div>
							
			</div>

			<div class="clear"></div>

			
			
		</div>
		

		<div class="clear"></div>
		<div class="footer1">
			<table class="tblFooter1">
				<tbody>
					<tr>
						<td style="width: 200px;">Sistema Reportes</td>
						<td style="width: 200px;"><a href="#" class="btnRojo">Malas Pr&aacute;cticas</a></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="footer">
			<table class="tblFooter">
				<tbody>
					<tr>
						<td style="width: 200px;">Banco Azteca S.A. Instituci&oacute;n de Banca M&uacute;ltiple</td>
						<td style="width: 200px;">Derechos Reservados 2014 (T&eacute;rminos y Condiciones de uso del Portal.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>

<script type="text/javascript"
	src="../js/supervisionSistemas/script_reportes.js"></script>

<script type="text/javascript"
	src="../js/supervisionSistemas/jquery-1.12.4.min.js"></script>
<script type="text/javascript"
	src="../js/supervisionSistemas/jquery.dropkick.js"></script>
<script type="text/javascript"
	src="../js/supervisionSistemas/content_height.js"></script>
<script type="text/javascript"
	src="../js/supervisionSistemas/jquery-ui.js"></script>

<script type="text/javascript"
	src="../js/supervisionSistemas/jquery.paginate.js"></script>
<script type="text/javascript"
	src="../js/supervisionSistemas/funciones.js"></script>

<script type="text/javascript">
	$(function() {
		$("#demo5").paginate(
				{
					count : 3,
					start : 1,
					display : 3,
					border : false,
					border_color : false,
					text_color : '#8a8a8a',
					background_color : 'transparent',
					border_hover_color : 'transparent',
					text_hover_color : '#000',
					background_hover_color : 'transparent',
					images : true,
					mouse : 'press',
					onChange : function(page) {
						$('._current', '#paginationdemo').removeClass(
								'_current').hide();
						$('#p' + page).addClass('_current').show();
					}
				});
	});
</script>