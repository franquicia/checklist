package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.FaseTransfDTO;

public class FasesAdministradorRowMapper implements RowMapper<FaseTransfDTO> {

	public FaseTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		FaseTransfDTO FaseTransfDTO = new FaseTransfDTO();
		
		FaseTransfDTO.setIdTab(rs.getInt("IDTABAGRUPA"));
		FaseTransfDTO.setIdAgrupa(rs.getInt("FIID_AGRUPA"));
		FaseTransfDTO.setFase(rs.getInt("FIID_FASE"));
		FaseTransfDTO.setVersion(rs.getInt("FIVERSION"));
	    FaseTransfDTO.setObs(rs.getString("NOMBRE_FASE"));
	    FaseTransfDTO.setOrdenFase(rs.getInt("FIID_ORDENFASE"));
	    FaseTransfDTO.setProyecto(rs.getInt("FIID_PROYECTO"));
	    FaseTransfDTO.setNombreProyecto(rs.getString("NOMBRE_PROYECTO"));
	    FaseTransfDTO.setFaseActual(rs.getInt("FIID_FASEACT"));
	    FaseTransfDTO.setBanderaFaseActual(rs.getString("FASE_ACTUAL"));
	    FaseTransfDTO.setPeriodo(rs.getString("FCPERIODO"));
	 
	    
	
	return FaseTransfDTO;
	}

}


