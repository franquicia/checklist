package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.NegocioAdicionalDTO;


public interface NegocioAdicionalDAO {

	public List<NegocioAdicionalDTO> obtieneNegocioAdicional(String usuario, String negocio ) throws Exception;
	
	public boolean insertaNegocioAdicional(int usuario, int negocio)throws Exception;
	
	public boolean eliminaNegocioAdicional(int usuario, int negocio)throws Exception;
	
}
