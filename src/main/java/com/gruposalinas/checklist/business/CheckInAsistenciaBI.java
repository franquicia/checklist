package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.CheckInAsistenciaDAO;

import com.gruposalinas.checklist.domain.CheckInAsistenciaDTO;
import com.gruposalinas.checklist.domain.ProgramacionMttoDTO;

public class CheckInAsistenciaBI {

	private static Logger logger = LogManager.getLogger(CheckInAsistenciaBI.class);

private List<CheckInAsistenciaDTO> listafila;
private List<ProgramacionMttoDTO> listafilas;
	
	@Autowired
	CheckInAsistenciaDAO checkInAsistenciaDAO;
		
	
	public int insertaAsistencia(CheckInAsistenciaDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = checkInAsistenciaDAO.insertaAsistencia(bean);
		} catch (Exception e) {
			respuesta = 0;
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaAsistencia(String idEmpFijo){
		boolean respuesta = false;
		
		try {
			respuesta = checkInAsistenciaDAO.eliminaAsistencia(idEmpFijo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<CheckInAsistenciaDTO> obtieneDatosAsistencia( String fechaInicio,String fechaFin, String idUsuario) {
		
		try {
			listafila =  checkInAsistenciaDAO.obtieneDatosAsistencia(fechaInicio,fechaFin,idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	
	public boolean actualizaAsistencia(CheckInAsistenciaDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = checkInAsistenciaDAO.actualizaAsistencia(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public boolean actualizaProgramacion(ProgramacionMttoDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = checkInAsistenciaDAO.actualizaProgramacion(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<ProgramacionMttoDTO> obtieneProgramaciones(String idUsuario) {
		
		try {
			listafilas =  checkInAsistenciaDAO.obtieneProgramaciones(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafilas;			
	}
}