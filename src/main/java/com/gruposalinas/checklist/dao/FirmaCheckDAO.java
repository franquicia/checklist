package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.FirmaCheckDTO;


public interface FirmaCheckDAO {

	  public int inserta(FirmaCheckDTO bean)throws Exception;
	  
	  public int insertaN(FirmaCheckDTO bean)throws Exception;
		
		public boolean elimina(String idFirma)throws Exception;
		
		public List<FirmaCheckDTO> obtieneDatos(String ceco) throws Exception; 
		
		public List<FirmaCheckDTO> obtieneInfo() throws Exception; 
		
		public boolean actualiza(FirmaCheckDTO bean)throws Exception;
		
		public List<FirmaCheckDTO> obtieneDatosFirmaCecoFaseProyecto(String ceco,String fase,String proyecto) throws Exception; 
	}