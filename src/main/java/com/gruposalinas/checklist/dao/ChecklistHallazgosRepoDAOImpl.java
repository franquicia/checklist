package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ChecklistHallazgosRepoDTO;
import com.gruposalinas.checklist.domain.HallazgosExpDTO;
import com.gruposalinas.checklist.mappers.BitaNegoRowMapper;
import com.gruposalinas.checklist.mappers.HallazgosCopiaRowMapper;
import com.gruposalinas.checklist.mappers.HallazgosRepoRowMapper;
import com.gruposalinas.checklist.mappers.HallazgosTablaRowMapper;
import com.gruposalinas.checklist.mappers.HallazgosUltRecoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ChecklistHallazgosRepoDAOImpl extends DefaultDAO implements ChecklistHallazgosRepoDAO {

    private static Logger logger = LogManager.getLogger(ChecklistHallazgosRepoDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcCursores4;
    DefaultJdbcCall jdbcCursores3;
    DefaultJdbcCall jdbcBitaNego;
    DefaultJdbcCall jdbcObtieneCursor2;
    DefaultJdbcCall jdbcObtienePregGen;
    DefaultJdbcCall jdbcObtienePreg;
    DefaultJdbcCall jdbcObtieneConteoHij;
    DefaultJdbcCall jdbcObtienePregImp;
    DefaultJdbcCall jdbcObtienePregNoImp;
    DefaultJdbcCall jdbcObtienePregPadreHij;
    DefaultJdbcCall jdbcObtieneCampExtras;
    DefaultJdbcCall jdbcObtienePregConcatImp;
    DefaultJdbcCall jdbcObtienePregConcatImppru;
    DefaultJdbcCall jdbcObtieneNegoCecoFaseProy;

    public void init() {

        // trae hallazgos por filtros de fechas
        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOHALLAFIL")
                .withProcedureName("SP_HALLAZGOS")
                .returningResultSet("RCL_PREG", new HallazgosRepoRowMapper());

        // trae preguntas preguntas concatenadas imperdonables en una consulta
        jdbcCursores4 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA").withCatalogName("PAREVISAHALLAZ")
                .withProcedureName("SP_DETA_HALLAZ")
                //.returningResultSet("RCL_MINHALLA", new HallazgosRepoRowMapper())
                .returningResultSet("RCL_HALLAZTAB", new HallazgosTablaRowMapper())
                .returningResultSet("RCL_HALLCOPIA", new HallazgosCopiaRowMapper())
                .returningResultSet("RCL_HALLAULTREC", new HallazgosUltRecoRowMapper());

        // trae preguntas preguntas concatenadas imperdonables en una consulta
        jdbcCursores3 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA").withCatalogName("PAREVISAHALLAZ")
                .withProcedureName("SP_DETA_HALLAZ")
                //.returningResultSet("RCL_MINHALLA", new HallazgosRepoRowMapper())
                .returningResultSet("RCL_HALLAZTAB", new HallazgosTablaRowMapper())
                .returningResultSet("RCL_HALLCOPIA", new HallazgosCopiaRowMapper())
                .returningResultSet("RCL_HALLAULTREC", new HallazgosUltRecoRowMapper());
//negocio por bitacora
        jdbcBitaNego = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA").withCatalogName("PAREVISAHALLAZ")
                .withProcedureName("SP_BITANEGO")
                .returningResultSet("RCL_BITANEGO", new BitaNegoRowMapper());

        //negocio por CECO FASE PROYECTO
        jdbcObtieneNegoCecoFaseProy = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA").withCatalogName("PAEXPNVOFLUJO")
                .withProcedureName("SP_SUCU")
                .returningResultSet("RCL_SUC", new BitaNegoRowMapper());

    }

    // trae si y no conteo
    @SuppressWarnings("unchecked")
    public List<ChecklistHallazgosRepoDTO> obtieneDatosFecha(String fechaIni, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistHallazgosRepoDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FECHAINI", fechaIni).addValue("PA_FECHAFIN",
                fechaFin);

        out = jdbcObtieneTodo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPOHALLAFIL.SP_HALLAZGOS}");

        listaFijo = (List<ChecklistHallazgosRepoDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los Hallazgos");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> cursores(String bitgral) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<HallazgosExpDTO> listahallazgosini = null;
        List<HallazgosExpDTO> listahallazgotab = null;
        List<HallazgosExpDTO> listaReconue = null;
        List<HallazgosExpDTO> listahallazultimoreco = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bitgral);

        out = jdbcCursores4.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREVISAHALLAZ.SP_DETA_HALLAZ}");

        //listahallazgosini = (List<HallazgosExpDTO>) out.get("RCL_MINHALLA");
        listahallazgotab = (List<HallazgosExpDTO>) out.get("RCL_HALLAZTAB");
        listaReconue = (List<HallazgosExpDTO>) out.get("RCL_HALLCOPIA");
        listahallazultimoreco = (List<HallazgosExpDTO>) out.get("RCL_HALLAULTREC");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los Hallazgos");
        }
        lista = new HashMap<String, Object>();
        //lista.put("hallazgosiniciales",listahallazgosini);
        lista.put("hallazgostabla", listahallazgotab);
        lista.put("listaReconue", listaReconue);
        lista.put("hallazgosultreco", listahallazultimoreco);

        return lista;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> cursoresNvo(String bitgral) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<HallazgosExpDTO> listahallazgosini = null;
        List<HallazgosExpDTO> listahallazgotab = null;
        List<HallazgosExpDTO> listaReconue = null;
        List<HallazgosExpDTO> listahallazultimoreco = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bitgral);

        out = jdbcCursores3.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREVISAHALLAZ.SP_DETA_HALLAZ}");

        //listahallazgosini = (List<HallazgosExpDTO>) out.get("RCL_MINHALLA");
        listahallazgotab = (List<HallazgosExpDTO>) out.get("RCL_HALLAZTAB");
        listaReconue = (List<HallazgosExpDTO>) out.get("RCL_HALLCOPIA");
        listahallazultimoreco = (List<HallazgosExpDTO>) out.get("RCL_HALLAULTREC");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los Hallazgos");
        }
        lista = new HashMap<String, Object>();
        //lista.put("hallazgosiniciales",listahallazgosini);
        lista.put("hallazgostabla", listahallazgotab);
        lista.put("listaReconue", listaReconue);
        lista.put("hallazgosultreco", listahallazultimoreco);

        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistHallazgosRepoDTO> obtieneDatosNego(String bita) throws Exception {
        // TODO Auto-generated method stub
        Map<String, Object> out = null;
        List<ChecklistHallazgosRepoDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", bita);

        out = jdbcBitaNego.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPOHALLAFIL.SP_BITANEGO}");

        listaFijo = (List<ChecklistHallazgosRepoDTO>) out.get("RCL_BITANEGO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los Hallazgos");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    @SuppressWarnings("unchecked")
    public List<ChecklistHallazgosRepoDTO> obtieneDatosNegoExp(String ceco, String fase, String proyecto) throws Exception {
        // TODO Auto-generated method stub
        Map<String, Object> out = null;
        List<ChecklistHallazgosRepoDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto);

        out = jdbcObtieneNegoCecoFaseProy.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAEXPNVOFLUJO.SP_SUCU}");

        listaFijo = (List<ChecklistHallazgosRepoDTO>) out.get("RCL_SUC");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los negocio");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

}
