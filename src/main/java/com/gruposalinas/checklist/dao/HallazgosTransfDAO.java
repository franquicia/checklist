package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.HallazgosTransfDTO;


public interface HallazgosTransfDAO {

	  public int inserta(HallazgosTransfDTO bean)throws Exception;
		
		//elimina por bitacoragral
		public boolean eliminaBita(String bitGral)throws Exception;
		
		public List<HallazgosTransfDTO> obtieneDatos(String ceco,String proyecto,String fase) throws Exception; 
		
		//REPORTE
		public List<HallazgosTransfDTO> obtieneDatosRepo(String ceco,String proyecto,String fase) throws Exception; 
		
		// UPDATE WEB
		public boolean actualiza(HallazgosTransfDTO bean)throws Exception;
		
		//UPDATE MOVIL
		public Map<String,Object> actualizaMov(HallazgosTransfDTO bean)throws Exception;
		
		public boolean actualizaResp(HallazgosTransfDTO bean)throws Exception;
		
		public boolean cargaHallazgosxBita(int bitGral, int cargaIni)throws Exception;
		
		//HALLAZGOS HISTORICO
		
		public List<HallazgosTransfDTO> obtieneHistFolio(int idFolio) throws Exception; 
		
		
		public List<HallazgosTransfDTO> obtieneHistBita(String bitGral) throws Exception; 
		
		public boolean insertaHallazgosHist(int idHallazgo)throws Exception;
		
		public boolean cargaHallazgosHistorico()throws Exception;
		
		public boolean eliminaBitaHisto(String bitGral)throws Exception;
		
		//evidencias
		public List<HallazgosTransfDTO> obtieneDatosBita(int proyecto, int fase,int ceco) throws Exception; 
		
		//hallazgos historicos ceco fase y proyecto para dashboard
		public List<HallazgosTransfDTO> obtieneHistoricoHallazgoDash(String ceco,String proyecto,String fase) throws Exception; 
		
		//HALLAZGOS EXPANSION
		public List<HallazgosTransfDTO> obtieneHallazgosExpansion(String ceco,String proyecto,String fase) throws Exception; 
		
		//HALLAZGOS EXPANSION Carga inicial
		public List<HallazgosTransfDTO> obtieneHallazgosExpansionIni(String ceco,String proyecto,String fase) throws Exception; 
		
		public boolean cargaHallazgosxBitaNvo(int bitGral, int ceco,int fase, int proyecto)throws Exception;
		
		// UPDATE WEB
		public boolean actualizaAdicionalHalla(HallazgosTransfDTO bean)throws Exception;
	}