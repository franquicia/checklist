
/*variables globales*/
var checklist;
var preguntas = new Array();
var arbolDecision=new Array();

var pregSeleccionada=-1;
var seccion=0;
var arbolCompleto=true;
var posiblesRespuestas=new Array();
var urlServer =$(location).attr('host');
//variables para la edicion de los tipos de checklist
var editarTipoCheck=false;
var catalogoTipoChecklist=new Array();
//variables para la edicion de los modulos
var editarModulo=false;
var catalogoModulo=new Array();
var idModEditar=0;
var nuevoMod=false;


$(document).ready(
		function(){

			asignarEventosCombo();
			cargarPosiblesResp() ;
			//se carga el catalogo de tipos de checklist
			cargarCatalogoTipoChecklist();
			//se carga el catalogo de los modulos
			cargarCatalogoModulos();
			//linea de prueba
			//enviarChecklist();
			
		}
);


function mostrarTitulo() {
	document.getElementById("tituloChecklist").value = document.getElementById("nombreChecklist").value;
	document.getElementById("tituloChecklistArbol").value = document.getElementById("nombreChecklist").value;

}
function cambiarFondo(indice) {
	var elemento = "lista" + indice;

	document.getElementById("lista1").style = "background:#fff;";
	document.getElementById("lista2").style = "background:#fff;";
	document.getElementById("lista3").style = "background:#fff;";
	//document.getElementById("lista4").style = "background:#fff;";
	
	document.getElementById(elemento).style = "background:rgb(217, 240, 229);";

	return false;

}
function mostrarDiv(nombreDiv) {

	if (document.getElementById(nombreDiv).style.display == 'none') {

		document.getElementById(nombreDiv).style.display = 'block';
	} else {

		document.getElementById(nombreDiv).style.display = 'none';
	}


	return false;

}
function habilitarEvidencia(tipoResp,indice){

	if(document.getElementById("evidencia_"+tipoResp+"_"+indice).checked){

		document.getElementById("evidenciaObliga_"+tipoResp+"_"+indice).disabled=false;
		$("#tipoEvidencia_"+tipoResp+"_"+indice).parent().css('pointer-events', "auto");

	}else{

		$("#tipoEvidencia_"+tipoResp+"_"+indice+" .seleccionado").attr("id",-1);
		$("#tipoEvidencia_"+tipoResp+"_"+indice+" .seleccionado").text("Tipo de evidencia");

		document.getElementById("evidenciaObliga_"+tipoResp+"_"+indice).checked=false;

		document.getElementById("evidenciaObliga_"+tipoResp+"_"+indice).disabled=true;
		$("#tipoEvidencia_"+tipoResp+"_"+indice).parent().css('pointer-events', "none");



	}
	
	return false;
}
function quitarEventoCombo(tipoResp,indice){
	

	document.getElementById("evidenciaObliga_"+tipoResp+"_"+indice).disabled=true;

	$("#tipoEvidencia_"+tipoResp+"_"+indice).parent().css('pointer-events', "none");
}
function quitarEventosCombo(){
	
	for(var i=0;i<preguntas.length;i++){
		for(var j=0;j<posiblesRespuestas.length;j++){
			if(preguntas[i].idTipo==posiblesRespuestas[j].idTipo){

				quitarEventoCombo(posiblesRespuestas[j].idPosibleRespuesta,i);
			}
		}
		/*
		//alert("PREGUNTA "+preguntas[i].idTipo);
		if(preguntas[i].idTipo=="1"){
		
			quitarEventoCombo(1,i);
			quitarEventoCombo(2,i);
		}
		if(preguntas[i].idTipo=="2"){

			quitarEventoCombo(1,i);
			quitarEventoCombo(2,i);
			quitarEventoCombo(3,i);
		}
		if(preguntas[i].idTipo=="3"){

			quitarEventoCombo(4,i);
		}
		*/
		
	}
	
}
function generarOpcionesArbol(resp,indice,tipoResp) {

	var vistaPreviaRespuesta = "";
	/* si la respuesta es si */
	vistaPreviaRespuesta += "<tr>";
	vistaPreviaRespuesta += "<td>"+resp+"</td>";
	vistaPreviaRespuesta += "<td>"+"<input id='evidencia_"+tipoResp+"_"+indice+"'type='checkbox' onchange='return habilitarEvidencia("+tipoResp+","+indice+");' />"+"</td>";

	vistaPreviaRespuesta += 
			"<td style='width:190px;'>"
			+"<div id='tipoEvidencia_"+tipoResp+"_"+indice+"' class='caja'  >"
			+ "<span class='seleccionado' id='-1' >Tipo de evidencia</span>"
			+ "<ul class='listaselect'>"
			+ "<li value='-1'><a href='#'>Tipo de evidencia</a></li>"
			+ "<li value='21'><a href='#'>Imagen [JPG]</a></li>"
			+"</ul>"
			+ "<span class='trianguloinf'></span>" + "</div>"
			+"</td>";

	vistaPreviaRespuesta += "<td>"+"<input id='evidenciaObliga_"+tipoResp+"_"+indice+"' type='checkbox' disabled />"+"</td>";

	vistaPreviaRespuesta += "<td>"+"<input id='observacion_"+tipoResp+"_"+indice+"' type='checkbox' />"+"</td>";
	vistaPreviaRespuesta += "<td>"+"<input id='acciones_"+tipoResp+"_"+indice+"' type='checkbox' />"+"</td>";
	
	vistaPreviaRespuesta += 
			"<td style='width:190px;'>"
			+"<div id='pregSig_"+tipoResp+"_"+indice+"' class='caja' >"
			+ "<span class='seleccionado' id='-1'>Siguiente pregunta</span>"
			+ "<ul class='listaselect'>"
			+ "<li value='-1'><a href='#'>Siguiente pregunta</a></li>";
			/*
			+ "<li><a href='#'>Pregunta 1</a></li>"
			+ "<li><a href='#'>Pregunta 2</a></li>"
			+ "<li><a href='#'>Pregunta 3</a></li>" + "</ul>"
			+ "<span class='trianguloinf'></span>" + "</div>"
			+"</td>";*/
	
	for(var i=0;i<preguntas.length;i++){
		if(i>indice){
			vistaPreviaRespuesta+= "<li value='"+preguntas[i].id+"'><a href='#'>Pregunta "+(i+1)+"</a></li>";
		}
		
	}

	vistaPreviaRespuesta+= "<li value='-2'><a href='#'>Fin checklist</a></li>";
	vistaPreviaRespuesta+= 
		"</ul>"+ "<span class='trianguloinf'></span>" + "</div>"+"</td>";
		
	vistaPreviaRespuesta += "</tr>";

	return vistaPreviaRespuesta;

}

function botonContinuar(){
	//alert("BOTON DINAMICO!");
	if($("#continuar").html()=="Continuar"){

		//se reinicia el arbol
		arbolDecision=new Array();

		//alert("INCIO!");
		var tituloChecklist = $("#nombreChecklist").val();

		var idTipoChecklist=$("#tipoChecklist span").attr("id");
		var tipoChecklist = $("#tipoChecklist span").text();
		
		var fechaInicio = $("#fechaInicio").val();
		var fechaTermino = $("#fechaTermino").val();
		
		var idHorario=$("#horario span").attr("id");

		//alert("VARIABLES OK!");
		checklist={
				"nombre":tituloChecklist,
				"idTipo":idTipoChecklist,
				"tipo":tipoChecklist,
				"fechaInicio":fechaInicio,
				"fechaTermino":fechaTermino,
				"idHorario":idHorario
		};

		//alert("ASIGNACION!");
		var mensaje="";
		var incompleto=false;
		if(checklist.nombre.trim()==""){
			mensaje="*Titulo del checklist";
			incompleto=true;
		}
		if(checklist.idTipo=="-1" || checklist.idTipo=="-2"){
			mensaje+="\n*Tipo de checklist";
			incompleto=true;
		}
		if(checklist.fechaInicio.trim()==""){
			mensaje+="\n*Fecha de inicio";
			incompleto=true;
		}
		if(checklist.idHorario=="-1"){
			mensaje+="\n*Horario del checklist";
			incompleto=true;
		}
		//alert("DATOS!");
		if(incompleto){
			alert("Hace falta llenar los siguientes campos:\n "+mensaje);
			return false;	
		}
			
		//se valida si se han agregado preguntas al checklist
		if(preguntas.length!=0){
			alert("TOTAL PREGUNTAS: "+preguntas.length);
			
			$("#vistaPrevia").hide();
			$("#definicionPregunta").hide();
			$("#contenedorArbol").show();
			$("#regresar").css("display", "inline");
			var preguntaArbol = "";
			for (var i = 0; i < preguntas.length; i++){
				
	
				preguntaArbol += "<tr>";
				preguntaArbol += "<td colspan='7' class='tituloPregunta'>";
				preguntaArbol += ""+(i+1)+". "+ preguntas[i].descripcion + "";
				preguntaArbol += "</td>";
				preguntaArbol += "</tr>";
	
				var tipoPreg = preguntas[i].idTipo;
				var vPrevia = "";
				for(var j=0;j<posiblesRespuestas.length;j++){
					if(posiblesRespuestas[j].idTipo==preguntas[i].idTipo){
						vPrevia+=generarOpcionesArbol(posiblesRespuestas[j].respuesta,i,posiblesRespuestas[j].idPosibleRespuesta);
							
					}
				}
				/*
				switch(tipoPreg){

				case "1":
					vPrevia += generarOpcionesArbol("SI",i,1);
					vPrevia += generarOpcionesArbol("NO",i,2);
					break;
				case "2":
					vPrevia += generarOpcionesArbol("SI",i,1);
					vPrevia += generarOpcionesArbol("NO",i,2);
					vPrevia += generarOpcionesArbol("N/A",i,3);
					break;
				case "3":
					vPrevia += generarOpcionesArbol("Abierta",i,4);
					break;
				}
			
				*/	
				preguntaArbol += vPrevia;
			}
				//$elemento = $(preguntaArbol);
				//$("#preguntasArbol").append($elemento);

				document.getElementById("preguntasArbol").innerHTML=preguntaArbol;
				quitarEventosCombo();
	
			
			
				asignarEventosCombo();
				
				$("#continuar").html("Guardar");
				//se actualiza la seccion
				seccion=1;
				
				//alert("BOTON:"+$("#continuar").html());
				$("#preguntasArbol").animate({scrollTop:$("#preguntasArbol")[0].scrollHeight}, 1000);
		}else{
			alert("No has agregado ninguna pregunta al checklist");
		}
		return false;
	}
	if($("#continuar").html()=="Guardar"){
		//se reinicia el arbol
		arbolDecision=new Array();
		
		
		for(var i=0;i<preguntas.length;i++){

			for(var j=0;j<posiblesRespuestas.length;j++){

				if(posiblesRespuestas[j].idTipo==preguntas[i].idTipo){

					agregarOpcionArbol(posiblesRespuestas[j].idPosibleRespuesta,i);
				}
			}

			//alert("PREGUNTA "+preguntas[i].idTipo);
			/*
			if(preguntas[i].idTipo=="1"){
			
				agregarOpcionArbol(1,i);
				agregarOpcionArbol(2,i);
			}
			if(preguntas[i].idTipo=="2"){

				agregarOpcionArbol(1,i);
				agregarOpcionArbol(2,i);
				agregarOpcionArbol(3,i);
			}
			if(preguntas[i].idTipo=="3"){

				agregarOpcionArbol(4,i);
			}
			*/
				
			
		}
		if(!arbolCompleto){
			arbolCompleto=true;
			return false;
		}
		seccion=2;

		alert("TAMAÑO DEL ARBOL: "+arbolDecision.length);
		$("#vistaPrevia").hide();
		$("#definicionPregunta").hide();
		$("#contenedorArbol").hide();
		$("#contenedorAsignacion").show();

		$("#continuar").html("Asignar");
		
		//se hace el envio del checklist a la base de datos
		enviarChecklist();
		
		return false;
	}
	
	return false;

}

function agregarOpcionArbol(tipoRes,indice){
	

	var campos=false;
	var mensaje="";
	//alert("AGREGAR! "+indice);
	var evidencia = document.getElementById("evidencia_"+tipoRes+"_"+indice).checked;

	var idTipoEvidencia=$("#tipoEvidencia_"+tipoRes+"_"+indice+" span").attr("id");
	var evidenciaObliga =  document.getElementById("evidenciaObliga_"+tipoRes+"_"+indice).checked;
	var observaciones =  document.getElementById("observacion_"+tipoRes+"_"+indice).checked;
	var acciones =  document.getElementById("acciones_"+tipoRes+"_"+indice).checked;
	
	var idPregSig=$("#pregSig_"+tipoRes+"_"+indice+" span").attr("id");
	var res="";

	var idPosibleRes;
	//alert("OPCIONES! ");

	for(var j=0;j<posiblesRespuestas.length;j++){
		if(posiblesRespuestas[j].idPosibleRespuesta==tipoRes){
			res=posiblesRespuestas[j].respuesta;
			idPosibleRes=posiblesRespuestas[j].idPosibleRespuesta;
		}
		
	}
	/*
	if(tipoRes=="1"){
		res="SI";
	}
	if(tipoRes=="2"){
		res="NO";
	}
	if(tipoRes=="3"){
		res="N/A";
	}
	if(tipoRes=="4"){
		res="Abierta";
	}
	*/
	if(idPregSig=="-1"){
		mensaje="\n*Siguiente pregunta";
		campos=true;
		arbolCompleto=false;
	}
	if(evidencia==true){

		if(idTipoEvidencia=="-1"){
			mensaje+="\n*Tipo de evidencia";
			campos=true;
			arbolCompleto=false;
		}
	}
	if(campos){
		alert("Hacen falta llenar los siguientes campos de la pregunta ["+(indice+1)+"] con respuesta ["+res+"]: \n"+mensaje);
	}
	

	var arbol={
			"idPreg":preguntas[indice].id,
			"resp":res,
			"idPosibleRes":idPosibleRes,
			
			"evidencia":evidencia,
			"idTipoEvidencia":idTipoEvidencia,
			"evidenciaObliga":evidenciaObliga,
			"observaciones":observaciones,
			"acciones":acciones,
			"idPregSig":idPregSig
			
	};
	
	alert("SE AGREGO UNA OPCION:  \n"

			+"\nidPreg: "+arbol.idPreg
			+"\nresp: "+arbol.resp
			+"\nidPosibleRes: "+arbol.idPosibleRes
			+"\nevidencia: "+arbol.evidencia
			+"\nidTipoEvidencia: "+arbol.idTipoEvidencia
			+"\nevidenciaObliga: "+arbol.evidenciaObliga
			+"\nobservaciones: "+arbol.observaciones
			+"\nacciones: "+arbol.acciones
			+"\nidPregSig: "+arbol.idPregSig);
	
	arbolDecision.push(arbol);
	
	
}
/* evento para ocultar la definicion de las pregutas */

$(document).ready(function() {
	$("#continuar").click(function() {
				
		botonContinuar();
		return false;
		
	})
}

);

function vistaPrevia(idTipoPreg){
	

	var vistaPreviaRespuesta = "";
	vistaPreviaRespuesta = "<ul>";
	for(var i=0;i<posiblesRespuestas.length;i++){ 
		if(posiblesRespuestas[i].idTipo==idTipoPreg){
			vistaPreviaRespuesta += "<li>"+posiblesRespuestas[i].respuesta+"</li>";
		}
	}

	vistaPreviaRespuesta += "</ul>";
	/*
	switch(idTipoPreg){
	case "1":

		vistaPreviaRespuesta = "<ul>";
		vistaPreviaRespuesta += "<li>SI</li>";
		vistaPreviaRespuesta += "<li>NO</li>";
		vistaPreviaRespuesta += "</ul>";
		break;
	case "2":

		vistaPreviaRespuesta = "<ul>";
		vistaPreviaRespuesta += "<li>SI</li>";
		vistaPreviaRespuesta += "<li>NO</li>";
		vistaPreviaRespuesta += "<li>N/A</li>";
		vistaPreviaRespuesta += "</ul>";
		break;
	case "3":

		vistaPreviaRespuesta = "<ul>";
		vistaPreviaRespuesta += "<li>Abierta</li>";
		vistaPreviaRespuesta += "</ul><br>";
		break;
	
	}
	*/
	return vistaPreviaRespuesta;
	
}
$(document)
		.ready(
				function() {
					$("#agregarPregunta")
							.click(
									function() {

										var nombrePregunta = $("#preguntaText").val();
										var tipoPreg = $("#tipoPreg span").text();
										var modulo = $("#modulo span").text();
										
										var tipoPregId=$("#tipoPreg span").attr("id");
										var moduloId=$("#modulo span").attr("id");

										if(pregSeleccionada==-1){
											// alert("mensaje");
	
											
											//alert("TIPO PREGUNTA: "+tipoPregId);
	
											if (tipoPregId == -1) {
												alert("No has seleccionado el tipo de pregunta");
												return false;
											}
											if (moduloId == -1 || moduloId==-2) {
												alert("No has seleccionado alguna categoria de la pregunta");
												return false;
											}
											if (nombrePregunta.trim() == "") {
												alert("El nombre de la pregunta no puede ser un campo vacio");
												return false;
											}
	
											var idPreg=preguntas.length;
											var preg={
													"id":idPreg,
													"descripcion":nombrePregunta,
													"idTipo":tipoPregId,
													"tipo":tipoPreg,
													"moduloId":moduloId,
													"modulo":modulo
													};
											//se agrega un objeto de tipo pregunta
											preguntas.push(preg);
								
											var $myNewElement = $("<li id='preg"
													+ (preg.id ) + "'   >"
													+ preg.descripcion + " - "
													+ preg.modulo 

													+"<button onclick='return mostrarPregunta("+preg.id+");' class='botonEditar'></button>"
													+"<button onclick='return eliminarPregunta("+preg.id+");' class='botonEliminar'></button>"
													
													+"<button onclick='subirNivel("+preg.id+")' class='botonContraer'></button>"
													+"<button onclick='bajarNivel("+preg.id+")' class='botonDesplegar' ></button>"
													+ vistaPrevia(tipoPregId)
													+ "</li>");
											$("#preguntas").append($myNewElement);
	
											$("#vistaPrevia").animate({scrollTop:$("#vistaPrevia")[0].scrollHeight}, 500);
											
										}else{
											//si la pregunta existe se edita
											for(var i=0;i<preguntas.length;i++){
												if(preguntas[i].id==pregSeleccionada){
													//alert("NOMBRE VAR:1 ");
													preguntas[i].descripcion=nombrePregunta;
													preguntas[i].idTipo=tipoPregId;
													preguntas[i].tipo=tipoPreg;
													preguntas[i].moduloId=moduloId;
													preguntas[i].modulo=modulo;
													//alert("NOMBRE VAR: 2");
													var contenidoNuevo=
															preguntas[i].descripcion + " - "
															+ preguntas[i].modulo 

															+"<button onclick='return mostrarPregunta("+preguntas[i].id+");' class='botonEditar'></button>"
															+"<button onclick='return eliminarPregunta("+preguntas[i].id+");' class='botonEliminar'></button>"
															+"<button onclick='subirNivel("+preguntas[i].id+")' class='botonContraer'></button>"
															+"<button onclick='bajarNivel("+preguntas[i].id+")' class='botonDesplegar' ></button>"
															
															+ vistaPrevia(tipoPregId);
													//alert("NOMBRE VAR: 3"+"preg"+preguntas[i].id);
													//$("#preg"+preguntas[i].id).remove();
													//$("#preg"+preguntas[i].id).append($contenidoNuevo);
													
													document.getElementById("preg"+preguntas[i].id).innerHTML=contenidoNuevo;
													
													
												}
											}
											
										}
										
										limpiarCamposPregunta();
										
										return false;
										

									});
				}

		);
function limpiarCamposPregunta(){

	//se regresa el estatus de la pregunta
	pregSeleccionada=-1;
	

	//se agrega la informacion de la pregunta
	$("#preguntaText").val("");
	$("#modulo .seleccionado").attr("id",-1);
	$("#tipoPreg .seleccionado").attr("id",-1);
	
	$("#modulo .seleccionado").text("Categoria");
	$("#tipoPreg .seleccionado").text("Tipo de pregunta");
	
}
function subirNivel(idPregunta){

	limpiarCamposPregunta();
	
	for(var i=0;i<preguntas.length;i++){
		
		if(preguntas[i].id==idPregunta){
			if(i>0 && i<preguntas.length){
				var pregTmp=preguntas[i-1];
				var idTmp;
				preguntas[i-1]=preguntas[i];
				preguntas[i]=pregTmp;
				
				//se actualizan los id
				idTmp=preguntas[i].id;
				preguntas[i].id=preguntas[i-1].id;
				preguntas[i-1].id=idTmp;
				
				
			}
		}
	}

	var preguntaHtml="";
	for(var i=0;i<preguntas.length;i++){
		preguntaHtml+= "<li id='preg"
				+ (preguntas[i].id ) + "'  >"
				+ preguntas[i].descripcion + " - "
				
				+ preguntas[i].modulo 
				+"<button onclick='return mostrarPregunta("+preguntas[i].id+");' class='botonEditar'></button>"
				+"<button onclick='return eliminarPregunta("+preguntas[i].id+");' class='botonEliminar'></button>"
				+"<button onclick='subirNivel("+preguntas[i].id+")' class='botonContraer'></button>"
				+"<button onclick='bajarNivel("+preguntas[i].id+")' class='botonDesplegar' ></button>"
				
				+ vistaPrevia(preguntas[i].idTipo)
				
				+ "</li>";
		
		
	}

	alert(preguntaHtml);

	document.getElementById("preguntas").innerHTML=preguntaHtml;

	
	return false;
}
function bajarNivel(idPregunta){

	limpiarCamposPregunta();
	
	for(var i=0;i<preguntas.length;i++){
		
		if(preguntas[i].id==idPregunta){
			if(i>=0 && i<preguntas.length-1){
				var pregTmp=preguntas[i+1];
				var idTmp;
				preguntas[i+1]=preguntas[i];
				preguntas[i]=pregTmp;
				
				//se actualizan los id
				idTmp=preguntas[i].id;
				preguntas[i].id=preguntas[i+1].id;
				preguntas[i+1].id=idTmp;
				
				
			}
		}
	}

	var preguntaHtml="";
	for(var i=0;i<preguntas.length;i++){
		preguntaHtml+= "<li id='preg"
				+ (preguntas[i].id ) + "'  >"
				+ preguntas[i].descripcion + " - "
				
				+ preguntas[i].modulo 
				+"<button onclick='return mostrarPregunta("+preguntas[i].id+");' class='botonEditar'></button>"
				
				+"<button onclick='return eliminarPregunta("+preguntas[i].id+");' class='botonEliminar'></button>"
				+"<button onclick='subirNivel("+preguntas[i].id+")' class='botonContraer'></button>"
				+"<button onclick='bajarNivel("+preguntas[i].id+")' class='botonDesplegar' ></button>"
				
				+ vistaPrevia(preguntas[i].idTipo)
				
				+ "</li>";
		
		
		
	}

	document.getElementById("preguntas").innerHTML=preguntaHtml;
		
	
	
	return false;
}
function mostrarPregunta(idPregunta){
	for(var i=0;i<preguntas.length;i++){
		if(preguntas[i].id==idPregunta){
			//se agrega la informacion de la pregunta
			$("#preguntaText").val(preguntas[i].descripcion);
			$("#modulo .seleccionado").attr("id",preguntas[i].moduloId);
			$("#tipoPreg .seleccionado").attr("id",preguntas[i].idTipo);
			$("#modulo .seleccionado").text(preguntas[i].modulo);
			$("#tipoPreg .seleccionado").text(preguntas[i].tipo);
			//se asigna la pregunta seleccionada
			pregSeleccionada=idPregunta;
		}
	}
	
	return false;
}

function eliminarPregunta(idPregunta){
	
	limpiarCamposPregunta();
	
	var preguntasTmp=new Array();
	
	for(var i=0;i<preguntas.length;i++){
		
		if(preguntas[i].id!=idPregunta){
			preguntas[i].id=preguntasTmp.length;
			preguntasTmp.push(preguntas[i]);
		}
		
	}
	
	//se actualiza el array 
	preguntas=preguntasTmp;
	//se actualiza el contenido
	var preguntaHtml="";
	for(var i=0;i<preguntas.length;i++){
		preguntaHtml+= "<li id='preg"
				+ (preguntas[i].id ) + "'  >"
				+ preguntas[i].descripcion + " - "
				
				+ preguntas[i].modulo 
				+"<button onclick='return mostrarPregunta("+preguntas[i].id+");' class='botonEditar'></button>"
				+"<button onclick='return eliminarPregunta("+preguntas[i].id+");' class='botonEliminar'></button>"
				+"<button onclick='subirNivel("+preguntas[i].id+")' class='botonContraer'></button>"
				+"<button onclick='bajarNivel("+preguntas[i].id+")' class='botonDesplegar' ></button>"
				
				+ vistaPrevia(preguntas[i].idTipo)
				
				+ "</li>";
	}

	document.getElementById("preguntas").innerHTML=preguntaHtml;
	
	return false;
}



function asignarEventosCombo(){
	/*se asignan los eventos para los combos agregados en el div dinamico del arbol de deciciones del checklist*/
	$(".caja").off();

		
	$(".caja").hover(
			
			function(e){

				asignarEventosCombo(); 
				
				var lista = $(this).find("ul");
				var triangulo = $(this).find("span:last-child");
				e.preventDefault();
				
				$(this).find("ul").toggle();
				if (lista.is(":hidden") ) {
					triangulo.removeClass("triangulosup").addClass("trianguloinf");
					//lista.hide();

					triangulo.parent().css('z-index', 0);
				} else {
					//lista.hide();
					triangulo.removeClass("trianguloinf").addClass("triangulosup");
					triangulo.parent().css('z-index', 3000);
				}
				
			}
		);
		
		$(".caja").on("click","li",
				function(e){
					
					var texto = $(this).text();
					var seleccionado = $(this).parent().prev(); 
					var valor=$(this).val();
					
					var lista = $(this).closest("ul");
					var triangulo = $(this).parent().next();
					//var lista = $(this).find("ul").toggle();

					//var lista = $(this).parents("li").find(".listaselect");
					//var triangulo = $(this).find("span:last-child");
					
					e.preventDefault();
					e.stopPropagation();
					seleccionado.text(texto);
					//alert("VALOR: "+valor);
					//seleccionado.val(valor);
					seleccionado.attr("id",valor);
					lista.hide();
					$(".caja").unbind('mouseleave');
					triangulo.removeClass("triangulosup").addClass("trianguloinf");

					triangulo.parent().css('z-index', 0);

					//$(".caja").on('mouseenter mouseleave');
					//asignarEventosCombo();
					//alert("se oculto la lista ");
					//se cambia el estatus de la edicion
					//editarModulo=false;
					editarTipoCheck=false;
				
					var nuevaPreg=$("#tipoChecklist .seleccionado").attr("id");
					
					if(nuevaPreg!="-2"){
						ocultarComponente("agregarTipoChecklist");
						ocultarComponente("nuevoTipoChecklist");
						
					}else{
						mostrarComponente("agregarTipoChecklist");
						mostrarComponente("nuevoTipoChecklist");
						
					}
					
					var nuevoModulo=$("#modulo .seleccionado").attr("id");
					/*
					//modificacion de un modulo
					if(editarModulo){

						cargarCatalogoModulosPadre();
						$("#moduloPadre").css('display', "inline-block");
						mostrarComponente("agregarNuevoModulo");
						mostrarComponente("nuevoModulo");

					}*/
					//alert("ID: "+$(this).attr("id"));
					if(idModEditar!=nuevoModulo && nuevoModulo!="-2"){

						cargarCatalogoModulosPadre();
						$("#moduloPadre").css('display', "inline-block");
						mostrarComponente("agregarNuevoModulo");
						mostrarComponente("nuevoModulo");
						$("#nuevoModulo").val("");
						editarModulo=false;
						nuevoMod=false;
						
					}
					//si no se edita
					if(nuevoModulo!="-2" && !editarModulo) {
						ocultarComponente("nuevoModulo");
						ocultarComponente("agregarNuevoModulo");

						$("#moduloPadre").css('display', "none");

						
					}
					//para agregar una nueva pregunta
					if(nuevoModulo=="-2" && !nuevoMod){

						cargarCatalogoModulosPadre();
						$("#moduloPadre").css('display', "inline-block");
						mostrarComponente("agregarNuevoModulo");
						mostrarComponente("nuevoModulo");
						//$("#nuevoModulo").val("");
						editarModulo=false;

						$("#moduloPadre .seleccionado").attr("id",0);
						$("#moduloPadre .seleccionado").text("Sin modulo padre");
						nuevoMod=true;

					}

					
				}
		);
		
}
/* se agregan los eventos del calendario*/

$( function() {
    $( "#fechaInicio" ).datepicker({

        minDate: new Date(),
        maxDate: "31/12/2050",
    	
    });
 } );

$( function() {
    $( "#fechaTermino" ).datepicker({
    	

        minDate: new Date(),
        maxDate: "31/12/2050",
    });
  } );


function eventoHoraInicio(){
	$('#horaInicio').wickedpicker({now: '12:00', twentyFour: false, title:'Hora de inicio', showSeconds: false});

	$("#wickedpicker__title").text("Hora de inicio");
	$(".wickedpicker").hide();
	
	return false;
}

		
function eventoHoraTermino(){
	$('#horaTermino').wickedpicker({now: '12:00', twentyFour: false, title:'Hora de termino', showSeconds: false});

	$("#wickedpicker__title").text("Hora de termino");
	$(".wickedpicker").hide();
	
	return false;
}

/*
$(function(){
$('.horaInicio').wickedpicker({now: '12:00', twentyFour: false, title:
    'Hora de inicio', showSeconds: false});
}
);

$(function(){
	$('.horaTermino').wickedpicker({now: '1:00', twentyFour: false, title:
	    'Hora de termino', 

	    upArrow: 'wickedpicker__controls__control-up',
	    downArrow: 'wickedpicker__controls__control-down',
	    close: 'wickedpicker__close',
	    hoverState: 'hover-state',
	    
	    showSeconds: false});
	}
	);
*/


$(document)
		.ready(
				function() {
					$("#regresar")
							.click(
									function() {

										switch(seccion){

										case 1:
											$("#vistaPrevia").show();
											$("#definicionPregunta").show();
											$("#contenedorArbol").hide();

											$("#regresar").css("display", "none");
											
											$("#continuar").html("Continuar");
											seccion=0;
											break;
										case 2:
											$("#contenedorAsignacion").hide();

											$("#contenedorArbol").show();
											$("#continuar").html("Guardar");
											seccion=1;
											break;
											
									
										}
										
										return false;
										
										
										}
									)});

function mostrarComponente(nombreComponente){

	$("#"+nombreComponente).css('display', "inline");
	
	//return false;
}
function ocultarComponente(nombreComponente){

	$("#"+nombreComponente).css('display', "none");
	
	//return false;
}

function agregarNuevoMod(){

	var nuevoTipo = $("#nuevoModulo").val();
	if(nuevoTipo.trim()==""){

		alert("No has ingresado el nombre de la categoria");
	}else{
		if(editarModulo){
			//opcion para actualizar un modulo
			editarModulo=false;
			modificarModuloService();
			idModEditar=0;
			
			$("#nuevoModulo").val("");
			ocultarComponente("nuevoModulo");
			ocultarComponente("agregarNuevoModulo");

			$("#moduloPadre").css('display', "none");

			$("#moduloPadre .seleccionado").attr("id",0);
			$("#moduloPadre .seleccionado").text("Sin modulo padre");

			$("#modulo .seleccionado").attr("id",-1);
			$("#modulo .seleccionado").text("Categoria");
			
			
		}else{

			//opcion para agregar nuevo modulo
			agregarModuloService();
			
			$("#nuevoModulo").val("");
			ocultarComponente("nuevoModulo");
			ocultarComponente("agregarNuevoModulo");

			$("#moduloPadre").css('display', "none");

			$("#moduloPadre .seleccionado").attr("id",0);
			$("#moduloPadre .seleccionado").text("Sin modulo padre");

			$("#modulo .seleccionado").attr("id",-1);
			$("#modulo .seleccionado").text("Categoria");
		}
	}
	
	return false;
}

function agregarNuevoTipoChecklist(){

	var nuevoTipo = $("#nuevoTipoChecklist").val();
	if(nuevoTipo.trim()==""){

		alert("No has ingresado el nombre del tipo de checklist");
	}else{
		if(editarTipoCheck){

			
			if(confirm("¿Desea modificar este registro ["+$("#tipoChecklist .seleccionado").text()+"]?")){
				//se ejecuta el servicio
				modificarTipoChecklistService();	
			}
			
			editarTipoCheck=false;
			
		}else{
			agregarTipoChecklistService();
			editarTipoCheck=false;
		}
		
		$("#nuevoTipoChecklist").val("");
		ocultarComponente("agregarTipoChecklist");
		ocultarComponente("nuevoTipoChecklist");

		$("#tipoChecklist .seleccionado").attr("id",-1);
		$("#tipoChecklist .seleccionado").text("Tipo de checklist");
	}
	
	return false;
}
function activarEdicionTipoChecklist(){
	if($("#tipoChecklist .seleccionado").attr("id")=="-1" || $("#tipoChecklist .seleccionado").attr("id")=="-2"){
		alert("No has seleccionado ninguna opción");
	}else{
		mostrarComponente('agregarTipoChecklist'); 
		mostrarComponente('nuevoTipoChecklist');
		editarTipoCheck=true;

		$("#nuevoTipoChecklist").val(
				$("#tipoChecklist .seleccionado").text());
		
				
	}

	return false;
}
function cargarPosiblesResp() {
	$
	.ajax({
		type : "GET",
		url : "http://"+urlServer + '../ajaxTiposPregunta.json',
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var i = 0;
			while (json[i] != null) {
				//document.forms.formulario.tipo.options[i+1]  = new Option(json[i].nombreCheck, json[i].idChecklist);
				posiblesRespuestas.push(
					{
						"id":json[i].id,
						"idTipo":json[i].idTipoPreg,
						"idPosibleRespuesta":json[i].idPosibleResp,
						"respuesta":json[i].respuesta
					}
				)
				i++;
				//alert("REG "+i+"--> id: "+posiblesRespuestas[posiblesRespuestas.length-1].id+"idTipo: "+posiblesRespuestas[posiblesRespuestas.length-1].idTipo+" respuesta: "+posiblesRespuestas[posiblesRespuestas.length-1].respuesta);
			}
			//alert("se ha cargado el catalogo!, total de regs: "+posiblesRespuestas.length);
		},
		error : function() {
			alert('Algo paso al cargar los tipos de posibles respuestas');
		}
	});
}



function agregarTipoChecklistService() {
	var tipoChecklist=$("#nuevoTipoChecklist").val();
	alert("http://"+urlServer + '../checklistServices/altaTipoCheck.json?descTipoCheck='+tipoChecklist);
	$
	.ajax({
		type : "GET",
		url : "http://"+urlServer + '../checklistServices/altaTipoCheck.json?descTipoCheck='+tipoChecklist,
		//contentType : "application/json; charset=utf-8",
		//dataType : "json",
		//paramName : "tagName",
		//delimiter : ",",
		success : function() {
			alert("Se agrego correctamente el tipo de checklist: "+tipoChecklist);
			cargarCatalogoTipoChecklist();

		},
		error : function() {
			alert('Algo paso al agregar el tipo de checklist');
			
		}
	});
	
}
function modificarTipoChecklistService() {
	var tipoChecklist=$("#nuevoTipoChecklist").val();
	var idTipoCheck=$("#tipoChecklist .seleccionado").attr("id");
	alert("http://"+urlServer + '../checklistServices/updateTipoCheck.json?idTipoCheck='+idTipoCheck+'&descTipoCheck='+tipoChecklist);
	$
	.ajax({
		type : "GET",
		url : "http://"+urlServer + '../checklistServices/updateTipoCheck.json?idTipoCheck='+idTipoCheck+'&descTipoCheck='+tipoChecklist,
		//contentType : "application/json; charset=utf-8",
		//dataType : "json",
		//paramName : "tagName",
		//delimiter : ",",
		success : function() {
			alert("Se actualizo correctamente el tipo de checklist: "+tipoChecklist);
			//se actualiza el catalogo de los tipos
			cargarCatalogoTipoChecklist();

		},
		error : function() {
			alert('Algo paso al modificar el tipo de checklist');
			
		}
	});
	
}

function eliminarTipoChecklist(){
	eliminarTipoChecklistService();
	return false;
}

function eliminarTipoChecklistService() {
	var tipoChecklist=$("#tipoChecklist .seleccionado").text();
	var idTipoCheck=$("#tipoChecklist .seleccionado").attr("id");
	
	if(idTipoCheck=="-1" || idTipoCheck=="-2"){
		alert("No has seleccionado ninguna opcion");
		return false;
	}
	else{
		if(confirm("¿Deseas eliminar este registro["+tipoChecklist+"]?\n")){
			$
			.ajax({
				type : "GET",
				url : "http://"+urlServer + '../checklistServices/eliminaTipoCheck.json?idTipoCheck='+idTipoCheck,
				//contentType : "application/json; charset=utf-8",
				//dataType : "json",
				//paramName : "tagName",
				//delimiter : ",",
				success : function() {
					alert("Se elimino  correctamente el tipo de checklist: "+tipoChecklist);
					//se actualiza el catalogo de los tipos de checklist
					cargarCatalogoTipoChecklist();
					editarTipoCheck=false;

					ocultarComponente('agregarTipoChecklist'); 
					ocultarComponente('nuevoTipoChecklist');
					$("#nuevoTipoChecklist").val("");

					$("#tipoChecklist .seleccionado").text("Tipo de checklist");
					$("#tipoChecklist .seleccionado").attr("id",-1);
					
					

				},
				error : function() {
					alert('Algo paso al eliminar el tipo de checklist');
					
				}
			});
	
		}		
	}
	
	//alert("http://"+urlServer + '../checklistServices/eliminaTipoCheck.json?idTipoCheck='+idTipoCheck);
	
	return false;
	
}


function cargarCatalogoTipoChecklist() {
	$
	.ajax({
		type : "GET",
		url : "http://"+urlServer + '../consultaChecklistService/getTiposCheckService.json',
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var i = 0;
			catalogoTipoChecklist=new Array();
			var contenido='<li value="-1"><a href="#">Tipo de checklist</a></li>';
			contenido+='<li value="-2" ><a href="#">Agregar nuevo</a></li>';
			
			while (json[i] != null) {
				//document.forms.formulario.tipo.options[i+1]  = new Option(json[i].nombreCheck, json[i].idChecklist);
				catalogoTipoChecklist.push(
					{
						"id":json[i].idTipoCheck,
						"descripcion":json[i].descTipo
					}
				);
				contenido+='<li value="'+json[i].idTipoCheck+'" ><a href="#">'+json[i].descTipo+'</a></li>';
				
				i++;
				//alert("REG "+i+"--> id: "+posiblesRespuestas[posiblesRespuestas.length-1].id+"idTipo: "+posiblesRespuestas[posiblesRespuestas.length-1].idTipo+" respuesta: "+posiblesRespuestas[posiblesRespuestas.length-1].respuesta);
			}

			$("#tipoChecklist .listaselect").html(contenido);
			//alert("se ha cargado el catalogo!, total de regs: "+posiblesRespuestas.length);
		},
		error : function() {
			alert('Algo paso al cargar los tipos de posibles respuestas');
		}
	});
}

/************************************EDICION DE MODULOS****************************************************/

function cargarCatalogoModulos() {
	$
	.ajax({
		type : "GET",
		url : "http://"+urlServer + '../consultaChecklistService/getModulosService.json',
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var i = 0;
			catalogoModulo=new Array();
			var contenido='<li value="-1"><a href="#">Categoria</a></li>';
			contenido+='<li value="-2" ><a href="#">Agregar nuevo</a></li>';
			
			while (json[i] != null) {
				//document.forms.formulario.tipo.options[i+1]  = new Option(json[i].nombreCheck, json[i].idChecklist);
				catalogoModulo.push(
					{
						"id":json[i].idModulo,
						"nombre":json[i].nombre,
						"idModuloPadre":json[i].idModuloPadre,
						"nombreModuloPadre":json[i].nombrePadre
					}
				)
				contenido+='<li value="'+json[i].idModulo+'" style="text-align : left;" ><a href="#">'+json[i].nombre+'</a></li>';
				
				i++;
				
			}

			$("#modulo .listaselect").html(contenido);
			//alert("se ha cargado el catalogo!, total de regs: "+posiblesRespuestas.length);
		},
		error : function() {
			alert('Algo paso al cargar las categorias');
		}
	});
}


function cargarCatalogoModulosPadre() {
	$
	.ajax({
		type : "GET",
		url : "http://"+urlServer + '../consultaChecklistService/getModulosService.json',
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var i = 0;
			//catalogoTipoChecklist=new Array();
			var contenido='<li value="0"><a href="#">Sin modulo padre</a></li>';
			
			while (json[i] != null) {
				//document.forms.formulario.tipo.options[i+1]  = new Option(json[i].nombreCheck, json[i].idChecklist);
				
				contenido+='<li value="'+json[i].idModulo+'" ><a href="#">'+json[i].nombre+'</a></li>';
				
				i++;
				
			}

			$("#moduloPadre .listaselect").html(contenido);
			//alert("se ha cargado el catalogo!, total de regs: "+posiblesRespuestas.length);
		},
		error : function() {
			alert('Algo paso al cargar las categorias');
		}
	});
}

function agregarModuloService() {
	var nombreModulo=$("#nuevoModulo").val();
	var idModuloPadre=$("#moduloPadre .seleccionado").attr("id");
	
	alert("http://"+urlServer + '../checklistServices/altaModulo.json?idModuloPadre='+idModuloPadre+'&nombreMod='+nombreModulo);
	$
	.ajax({
		type : "GET",
		url : "http://"+urlServer + '../checklistServices/altaModulo.json?idModuloPadre='+idModuloPadre+'&nombreMod='+nombreModulo,
		//contentType : "application/json; charset=utf-8",
		//dataType : "json",
		//paramName : "tagName",
		//delimiter : ",",
		success : function() {
			alert("Se agrego correctamente la categoria: "+nombreModulo);
			cargarCatalogoModulos();

		},
		error : function() {
			alert('Algo paso al agregar la categoria');
			
		}
	});
	
}
function activarEdicionModulo(){
	if($("#modulo .seleccionado").attr("id")=="-1" || $("#modulo .seleccionado").attr("id")=="-2"){
		alert("No has seleccionado ninguna opción");
	}else{

		$("#moduloPadre").css('display', "inline-block");
		mostrarComponente("agregarNuevoModulo");
		mostrarComponente("nuevoModulo");

		cargarCatalogoModulosPadre();
		
		editarModulo=true;
		nuevoMod=false;
		idModEditar=$("#modulo .seleccionado").attr("id");

		//se muestra la descripcion del modulo
		$("#nuevoModulo").val(
				$("#modulo .seleccionado").text());
		
		for(var i=0;i<catalogoModulo.length;i++){

			if(catalogoModulo[i].id==$("#modulo .seleccionado").attr("id")){

				$("#moduloPadre .seleccionado").attr("id",catalogoModulo[i].idModuloPadre);
				if(catalogoModulo[i].idModuloPadre=="0"){
					$("#moduloPadre .seleccionado").text("Sin modulo padre");
				}else{

					$("#moduloPadre .seleccionado").text(catalogoModulo[i].nombreModuloPadre);
				}
			}
		}
		
				
	}

	return false;
}



function modificarModuloService() {
	var nombreModulo=$("#nuevoModulo").val();
	var idModulo=$("#modulo .seleccionado").attr("id");
	var idModuloPadre=$("#moduloPadre .seleccionado").attr("id");
	
	$
	.ajax({
		type : "GET",
		url : "http://"+urlServer + '../checklistServices/updateModulo.json?idModulo='+idModulo+'&idModuloPadre='+idModuloPadre+'&nombreMod='+nombreModulo,
		//contentType : "application/json; charset=utf-8",
		//dataType : "json",
		//paramName : "tagName",
		//delimiter : ",",
		success : function() {
			alert("Se actualizo correctamente la categoria: "+nombreModulo);
			//se actualiza el catalogo 
			cargarCatalogoModulos();

		},
		error : function() {
			alert('Algo paso al modificar la categoria');
			
		}
	});
	
}

function eliminarModulo(){
	eliminarModuloService();
	
	return false;
}

function eliminarModuloService() {
	var nombreModulo=$("#modulo .seleccionado").text();
	var idModulo=$("#modulo .seleccionado").attr("id");
	
	if(idModulo=="-1" || idModulo=="-2"){
		alert("No has seleccionado ninguna opcion");
		return false;
	}
	else{
		if(confirm("¿Deseas eliminar este registro["+nombreModulo+"]?\n")){
			$
			.ajax({
				type : "GET",
				url : "http://"+urlServer + '../checklistServices/eliminaModulo.json?idModulo='+idModulo,
				//contentType : "application/json; charset=utf-8",
				//dataType : "json",
				//paramName : "tagName",
				//delimiter : ",",
				success : function() {
					alert("Se elimino  correctamente la categoria: "+nombreModulo);
					//se actualiza el catalogo de los tipos de checklist
					cargarCatalogoModulos();
					editarModulo=false;

					cargarCatalogoModulosPadre();
					$("#moduloPadre").css('display', "none");
					ocultarComponente("agregarNuevoModulo");
					ocultarComponente("nuevoModulo");
					$("#nuevoModulo").val("");
					$("#moduloPadre .seleccionado").attr("id",0);
					$("#moduloPadre .seleccionado").text("Sin modulo padre");
					

					$("#modulo .seleccionado").attr("id",-1);
					$("#modulo .seleccionado").text("Categoria");

				},
				error : function() {
					alert('Algo paso al eliminar la categoria');
					
				}
			});
	
		}		
	}
	
	//alert("http://"+urlServer + '../checklistServices/eliminaTipoCheck.json?idTipoCheck='+idTipoCheck);
	
	return false;
	
}



function enviarChecklist() {
	var jsonGeneral='{';
	/*
	checklist={
			"nombre":"NUEVO CHECKLIST",
			"idTipo":21,
			"tipo":"VISITA",
			"fechaInicio":"27/11/2016",
			"fechaTermino":null,
			"idHorario":42
	};*/
	
	jsonGeneral+='"checklist":'+JSON.stringify(checklist)+",";

	jsonGeneral+='"preguntas":'+JSON.stringify(preguntas)+",";
	jsonGeneral+='"arbol":'+JSON.stringify(arbolDecision);
	
	jsonGeneral+='}'
		

	alert(jsonGeneral);
		

	/*
	$
	.ajax({
		type : "POST",
		url : "http://"+urlServer + '../ajaxInsertarChecklist.json',
		contentType : "application/json; charset=utf-8",
		traditional: true,
		//dataType : "json",
		//paramName : "tagName",
		//delimiter : ",",
		data:'{"objetos":'+JSON.stringify(objetosX)+'}',
		success : function(data) {
			alert('Envio satisfactorio!');
		},
		error : function(request, status, error) {
			alert('MENSAJE DE ERROR: '+error);
			
		}
	});*/
}

