package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.AdmZonasDAO;
import com.gruposalinas.checklist.domain.AdmHandbookDTO;
import com.gruposalinas.checklist.domain.AdminZonaDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class AdmZonasBI {

	private static Logger logger = LogManager.getLogger(AdmZonasBI.class);

	@Autowired
	AdmZonasDAO admZonasDAO;
	ArrayList<AdminZonaDTO> lista = null;	
	
	public ArrayList<AdminZonaDTO> obtieneZonaById(AdminZonaDTO bean){
		try{
			lista = admZonasDAO.obtieneZonaById(bean);	
		}
		catch(Exception e){
			logger.info("No fue posible obtener la zona: "+e.getMessage());
		}
				
		return lista;
	}	
	
	public boolean insertaZona(AdminZonaDTO bean) {
		
		boolean respuesta = false;		
		try {		
			respuesta = admZonasDAO.insertaZona(bean);		
		} catch (Exception e) {
			logger.info("No fue posible insertar la zona: "+e.getMessage());			
		}
		
		return respuesta;		
	}
	
	public boolean actualizaZona (AdminZonaDTO bean){
		
		boolean respuesta = false;				
		try {
			respuesta = admZonasDAO.actualizaZona(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar la zona: "+e.getMessage());
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaZona(AdminZonaDTO bean){
		
		boolean respuesta = false;		
		try{
			respuesta = admZonasDAO.eliminaZona(bean);
		}catch(Exception e){
			logger.info("No fue posible borrar la zona: "+e.getMessage());			
		}
		
		return respuesta;
	}

}
