package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ChecklistDAO;
import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.RegistroChecklistDTO;
import com.gruposalinas.checklist.domain.SucUbicacionDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class ChecklistBI {
	
	private static final Logger logger = LogManager.getLogger(ChecklistBI.class);
	
	@Autowired
	ChecklistDAO checklistDAO;
	@Autowired
	SucUbicacionBI sucUbicacionBI;
	
	List<ChecklistDTO> listaChecklist= null;
	List<CompromisoDTO> listaCompromiso= null;
	Map<String, Object> listaChecklistCompleto = null;
	Map<String, Object> listaPila = null;
	Map<String, Object> listaCompromisos = null;
	
	
	public List<ChecklistDTO> consultaChecklist(int noUsuario,   double latitud, double longitud) throws Exception{

		listaChecklist = checklistDAO.buscaChecklistActivos(noUsuario, latitud, longitud);

		if (listaChecklist.size() == 0) 
			return null;
		else 
			return listaChecklist;
	}
		
	public Map<String, Object> buscaChecklistCompleto (int idCheckUsua) throws Exception{
		
		listaChecklistCompleto = checklistDAO.buscaChecklistCompleto(idCheckUsua);
		
		if(listaChecklistCompleto.size() == 0) return null;
		else return listaChecklistCompleto;		
		
	}
	
	public List<CompromisoDTO> compromisosChecklist (int idChecklist, int idSucursal) throws Exception{
			listaCompromiso = checklistDAO.compromisosChecklist(idChecklist, idSucursal);
		if(listaCompromiso.size() == 0) return null;
		else return listaCompromiso;		
		
	}
	
	public List<ChecklistDTO> buscaChecklistCompletoPrueba(int noUsuario, int idCheck, String latitud, String longitud) throws Exception{
		

		listaChecklist = checklistDAO.buscaChecklistCompletoPrueba(noUsuario, idCheck, latitud, longitud);

		if (listaChecklist.size() == 0) 
			return null;
		else 
			return listaChecklist;
		
	}
	
	@SuppressWarnings("unused")
	public Map<String, Object>  buscaResumenCheck() throws Exception{

		Map<String, Object> listas = new HashMap<String, Object>();
		List<ChecklistDTO> activos = new ArrayList<ChecklistDTO>();
		List<ChecklistDTO> pendientes = new ArrayList<ChecklistDTO>();
		
		listaChecklist = checklistDAO.buscaResumenCheck();

		if (listaChecklist.size() == 0) 
			return null;
		else {
			Iterator<ChecklistDTO> it = listaChecklist.iterator();
			
			while(it.hasNext()){
				ChecklistDTO checkslist = it.next();
				
				if (checkslist.getIdEstado() == 21 )
					activos.add(checkslist);
				else
					pendientes.add(checkslist);
			}
						
			listas.put("activos", activos);
			listas.put("pendientes", pendientes);
			
			Iterator<ChecklistDTO> itActivos = activos.iterator();
			Iterator<ChecklistDTO> itPendientes = pendientes.iterator();
			
			while(itActivos.hasNext()){
				ChecklistDTO activo = itActivos.next();				
				int cont =0;
				boolean encontrado= false;
				
				//Busca Checklist si tiene pendientes
				while(cont < pendientes.size() && encontrado == false){
					if(pendientes.get(cont).getIdChecklist() == activo.getIdChecklist()){
						encontrado = true;
						if(pendientes.get(cont).getIdEstado() == 27)
							activo.setIsPendiente(true);
						else if(pendientes.get(cont).getIdEstado() == 25)
							activo.setRechazoCambios(true);
					}
					
					cont ++;
				}
				
			}
			
			
		}
		
			return listas;
	}

	public int insertaChecklist(ChecklistDTO bean){
		int idChecklist = 0;
		
		try{
			idChecklist = checklistDAO.insertaChecklist(bean);
		}catch(Exception e){
			logger.info("No fue posible insertar el Checklist: "+e.getMessage());
			
		}
		
		return idChecklist;
	}


	public List<CompromisoDTO> compromisosChecklistWeb(int idBitacora)   throws Exception{
		try{
			listaCompromiso = checklistDAO.compromisosChecklistWeb(idBitacora);

          	} catch (Exception e) {
		        logger.info("No fue posible consultar los compromisos por idBitacora");
		         
	        }

		if (listaCompromiso.size() == 0) 
			return null;
		else 
			return listaCompromiso;	
	}
	
	public boolean actualizaChecklist(ChecklistDTO bean){
		boolean respuesta= false;
		
		try {
			respuesta = checklistDAO.actualizaChecklist(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Checklist: "+e.getMessage());
			
		}
		
		return respuesta;
		
	}

	public boolean actualizaCheckVigente(int idCheck, int estatus){
		boolean respuesta= false;
		
		try {
			respuesta = checklistDAO.actualizaCheckVigente(idCheck, estatus);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el estatus del Checklist: "+e.getMessage());
			
		}
		
		return respuesta;
		
	}
	public boolean actualizaFechatermino(String fechaTermino, int idBitacora, String preCalificacion, String calificacion){
		boolean respuesta= false;
		logger.info("PRECALIFICACION: "+preCalificacion+" CALIFICACION FINAL: "+calificacion);
		
		try {
			respuesta = checklistDAO.actualizaFechatermino(fechaTermino, idBitacora, preCalificacion, calificacion); 
		} catch (Exception e) {
			logger.info("No fue posible actualizar la fecha termino de la bitacora: "+e.getMessage());
			
		}
		
		return respuesta;
		
	}
	
	public boolean eliminaChecklist(int idCheckList){
		boolean respuesta= false;
		
		try {
			respuesta = checklistDAO.eliminaChecklist(idCheckList);
		} catch (Exception e) {
			logger.info("No fue posible eliminar el Checklist: "+e.getMessage());
			
		}
		
		return respuesta;
		
	}
	
	public List<ChecklistDTO> buscaChecklist(int idChecklist){
		
		try{
			listaChecklist = checklistDAO.buscaChecklist(idChecklist);
		}catch(Exception e){
			logger.info("No fue posible obtener el Checklist: "+e.getMessage());
			
		}
		
		return listaChecklist;
		
	}
	
	public List<ChecklistDTO> buscaTChecklist(int idTipoCheck){
		
		try{
			listaChecklist = checklistDAO.buscaTChecklist(idTipoCheck);
		}catch(Exception e){
			logger.info("No fue posible obtener el Checklist: "+e.getMessage());
			
		}
		
		return listaChecklist;
		
	}
	
	public List<ChecklistDTO> buscaChecklist(){
		
		try{
			listaChecklist = checklistDAO.buscaChecklist();
		}catch(Exception e){
			logger.info("No fue posible obtener los Checklist: "+e.getMessage());
			
		}
		
		return listaChecklist;
		
	}
	
	
	public boolean asignaChecklist(String ceco, int puesto, int idChecklist){
		boolean respuesta = false ;
		try{
			respuesta = checklistDAO.asignaChecklist(ceco,puesto,idChecklist);
		}catch(Exception e ){
			logger.info("No fue posible asignar los checklist: "+e.getMessage());
			
		}
		return respuesta;
	}
	
	@SuppressWarnings("unused")
	public boolean registraChecklist (RegistroChecklistDTO registraCheck) throws Exception{
		
		String checklist = "";
		String preguntas = "";
		String arbolDesiciones = "";
		int numRespuesta = 0; 
		int cont = 1;
		List<ArbolDecisionDTO> arbolDesDTO = registraCheck.getListaArbol();
		boolean respuesta = false;
		int j = 0;
		try{
			
			checklist += registraCheck.getListaChecklist().getIdTipoChecklist().getIdTipoCheck();
			checklist +="!~OBJ~!";
			checklist += registraCheck.getListaChecklist().getNombreCheck();	
			checklist +="!~OBJ~!";	
			checklist += registraCheck.getListaChecklist().getIdHorario();	
			checklist +="!~OBJ~!";	
			checklist += registraCheck.getListaChecklist().getVigente();
			checklist +="!~OBJ~!";	
			checklist += registraCheck.getListaChecklist().getFecha_inicio();
			checklist +="!~OBJ~!";	
			checklist += registraCheck.getListaChecklist().getFecha_fin();
			checklist +="!~OBJ~!";	
			checklist += registraCheck.getListaChecklist().getIdEstado();
			checklist += "!<ATRRIB>!";
			
			
			for (PreguntaDTO preguntaDTO : registraCheck.getListaPregunta()){
					preguntas += preguntaDTO.getIdModulo();
					preguntas +="!~OBJ~!";	
					preguntas += preguntaDTO.getIdTipo();
					preguntas +="!~OBJ~!";
					preguntas += preguntaDTO.getEstatus();
					preguntas +="!~OBJ~!";
					preguntas += preguntaDTO.getPregunta();
					preguntas +="!~OBJ~!";
					preguntas += preguntaDTO.getNumRespuestas();
					preguntas += "!<ATRRIB>!";
					
					numRespuesta = Integer.parseInt(preguntaDTO.getNumRespuestas());
					if (numRespuesta == 0)
					{
						throw new Exception("La pregunta debe contener su posibles respuestas.");
					}
					
						for(int i = 0; i < numRespuesta; i++){
						arbolDesiciones += arbolDesDTO.get(j).getRespuesta();
						arbolDesiciones +="!~OBJ~!";	
						arbolDesiciones += arbolDesDTO.get(j).getEstatusEvidencia();
						arbolDesiciones +="!~OBJ~!";	
						arbolDesiciones += arbolDesDTO.get(j).getOrdenCheckRespuesta();
						arbolDesiciones +="!~OBJ~!";	
						arbolDesiciones += arbolDesDTO.get(j).getReqAccion();
						arbolDesiciones +="!~OBJ~!";	
						arbolDesiciones += arbolDesDTO.get(j).getReqObservacion();
						arbolDesiciones +="!~OBJ~!";	
						arbolDesiciones += arbolDesDTO.get(j).getReqOblig();
							
							if (i < (numRespuesta - 1))
							{
								arbolDesiciones +="!~ARB~!";	
							}
							else
							{
								if (numRespuesta == 1)
								{
									arbolDesiciones +="!~ARB~!";
								}
								else
								{
								arbolDesiciones +="";
								}
							}
							j++;
						}
						arbolDesiciones += "!<ATRRIB>!";
						
				cont++;
			}
					
			respuesta = checklistDAO.registraChecklist(checklist, preguntas, arbolDesiciones);
			
			}catch(Exception e){
				logger.info("No fue posible Registrar el Checklist: "+e.getMessage());
				
			}
	
		return respuesta;
	}
	
	//public List<ChecklistDTO> validaCheckUsua (int checkUsua,  double latitud, double longitud) {
	public List<ChecklistDTO> validaCheckUsua (int checkUsua,  String latitud, String longitud) {	
		List<ChecklistDTO> respuesta = null;
		
		try {
			respuesta = checklistDAO.validaCheckUsua(checkUsua, latitud, longitud);
		} catch (Exception e) {
			logger.info("No fue posible validar el Checkusua: "+e.getMessage());
			
		}
		
		return respuesta;
		
	}
	
	public Map<String, Object> ReportePila(int idUsuario) throws Exception{
		
		listaPila = checklistDAO.ReportePila(idUsuario);
		

		//List<SucUbicacionDTO> lista = sucUbicacionBI.obtieneDatos("21");
		
		if(listaPila.size() == 0) return null;
		else { 
			
			//listaPila.put("listaTodosCecos",lista);
			return listaPila;		
		}
		
	}
	
	public List<ChecklistDTO> buscaChecklistSoporte() {
		
		try{
			listaChecklist = checklistDAO.buscaChecklistSoporte();
		}catch(Exception e){
			logger.info("No fue posible obtener el Checklist: "+e.getMessage());
			
		}
		
		return listaChecklist;
		
	}
	
	//total pond
	public int insertaChecklistCom(ChecklistDTO bean){
		int idChecklist = 0;
		
		try{
			idChecklist = checklistDAO.insertaChecklistCom(bean);
		}catch(Exception e){
			logger.info("No fue posible insertar el Checklist: "+e.getMessage());
			
		}
		
		return idChecklist;
	}
	
	//total pond
	public boolean actualizaChecklistCom(ChecklistDTO bean){
		boolean respuesta= false;
		
		try {
			respuesta = checklistDAO.actualizaChecklistCom(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Checklist: "+e.getMessage());
			
		}
		
		return respuesta;
		
	}
        
        public List<ChecklistDTO> buscaChecklistByProtocolo(int idProtocolo){
		
		try{
			listaChecklist = checklistDAO.buscaChecklistByProtocolo(idProtocolo);
		}catch(Exception e){
			logger.info("No fue posible obtener el Checklist: "+e.getMessage());
			
		}
		
		return listaChecklist;
		
	}
	


}