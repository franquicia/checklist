package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.PerfilDTO;

public interface PerfilDAO {

	public List<PerfilDTO> obtienePerfil() throws Exception;
	
	public List<PerfilDTO> obtienePerfil(int idPerfil) throws Exception;
	
	public int insertaPerfil(PerfilDTO bean) throws Exception;
	
	public boolean actualizaPerfil(PerfilDTO bean) throws Exception;
	
	public boolean eliminaPerfil(int idPerfil) throws Exception;
	
	public int[] agregaNuevoPerfil() throws Exception;
}
