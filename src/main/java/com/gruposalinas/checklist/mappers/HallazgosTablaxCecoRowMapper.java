package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;

public class HallazgosTablaxCecoRowMapper implements RowMapper<ChecklistPreguntasComDTO> {

	public ChecklistPreguntasComDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistPreguntasComDTO com = new ChecklistPreguntasComDTO();
		
        com.setIdHalla(rs.getInt("FIID_HALLAZGO"));
	    com.setFolio(rs.getInt("FIID_FOLIO"));
	    com.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    com.setStatus(rs.getInt("FCSTATUS"));
	    com.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    com.setPregunta(rs.getString("FCPREGUNTA"));
	    com.setPosible(rs.getString("FCRESPUESTA"));
	    com.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
	    com.setResponsable(rs.getString("FCRESPONSABLE"));
	    com.setObsNueva(rs.getString("FCDISPOSITIVO"));
	    com.setObserv(rs.getString("FCOBSERVACION"));
	    com.setIdCheck(rs.getInt("FIID_CHECKLIST"));
	    com.setRuta(rs.getString("FCRUTA"));
	    com.setIdBitaGral(rs.getInt("FIID_BITGRAL"));
	    com.setFechaIni(rs.getString("FDINICIO"));
	    com.setFechaFin(rs.getString("FDFIN"));
	    com.setFechaAut(rs.getString("FDAUTORIZO"));
	    com.setCeco(rs.getString("FCID_CECO"));
	    com.setReco(rs.getInt("FCRECORRIDO"));   
	    com.setClasif(rs.getString("FCCLASIFICA"));
	    com.setNombCeco(rs.getString("FCNOMBRE"));
	    com.setLiderObra(rs.getString("LIDEROBRA"));
	    com.setRealizoReco(rs.getString("USUARIO"));
	    com.setEco(rs.getInt("ECO"));
	    
	  

	  //  ChecklistPreguntasComDTO.setResponsable(rs.getString("FCRESPONSABLE"));

	   
		return com;
	}

}


