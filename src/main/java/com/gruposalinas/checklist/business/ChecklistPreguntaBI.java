package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ChecklistModificacionesDAO;
import com.gruposalinas.checklist.dao.ChecklistPreguntaDAO;
import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class ChecklistPreguntaBI {
	
	private static Logger logger = LogManager.getLogger(ChecklistPreguntaBI.class);
	
	@Autowired
	ChecklistPreguntaDAO checklistPreguntaDAO;
	
	@Autowired
    ChecklistModificacionesDAO checklistModificacionesDAO;
	
	List<ChecklistPreguntaDTO> listaChecklistPregunta = null;
	
	public boolean insertaChecklistPregunta(ChecklistPreguntaDTO bean ){
	
		boolean respuesta = false;
		try{
			respuesta = checklistPreguntaDAO.insertaChecklistPregunta(bean);
		}catch(Exception e){
			logger.info("No fue posible insertar Pregunta-CheckList");
			
		}		
		return respuesta;		
	}
	
	public boolean actualizaChecklistPregunta(ChecklistPreguntaDTO bean){
		
		boolean respuesta = false;
		try{
			respuesta = checklistPreguntaDAO.actualizaChecklistPregunta(bean);
		}catch(Exception e){
			logger.info("No fue posible actualizar Pregunta-CheckList");
			
		}		
		return respuesta;
	}
	
	public boolean eliminaChecklistPregunta(int idChecklist, int idPregunta){
		
		boolean respuesta = false;
		try{
			respuesta = checklistPreguntaDAO.eliminaChecklistPregunta(idChecklist,idPregunta);
		}catch(Exception e){
			logger.info("No fue posible eliminar Pregunta-CheckList");
			
		}		
		return respuesta;
	}
	
	
	public List<ChecklistPreguntaDTO> buscaPreguntas(int idChecklist){
		
		try{
			listaChecklistPregunta = checklistPreguntaDAO.buscaPreguntas(idChecklist);
		}catch(Exception e){
			logger.info("No fue posible obtener Pregunta-CheckList");
			
		}
		
		return listaChecklistPregunta;
	}
	
	
	public List<ChecklistPreguntaDTO> obtienePregXcheck(int idChecklist){
		
		try {
			listaChecklistPregunta = checklistPreguntaDAO.obtienePregXcheck(idChecklist);
						
		} catch (Exception e) {
			logger.info("No fue posible obtener las preguntas del Checklist: "+e);
			
		}
		
		
		
		return listaChecklistPregunta;
	}
	
	public boolean cambiaEstatus(){
		boolean res = false;
		
		try {
			res = checklistPreguntaDAO.cambiaEstatus();
		} catch (Exception e) {
			logger.info("No fue posible cambiar estatus del check preg ");
			
		}
		
		return res;
	}

}
