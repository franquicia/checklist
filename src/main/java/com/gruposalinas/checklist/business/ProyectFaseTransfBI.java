package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.ProyectFaseTransfDAO;
import com.gruposalinas.checklist.domain.ProyectFaseTransfDTO;

public class ProyectFaseTransfBI {

	private static Logger logger = LogManager.getLogger(ProyectFaseTransfBI.class);

private List<ProyectFaseTransfDTO> listafila;
	
	@Autowired
	ProyectFaseTransfDAO proyectFaseTransfDAO;
		
	
	public int insertaProyec(ProyectFaseTransfDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = proyectFaseTransfDAO.insertaProyec(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaProyec(String proyec){
		boolean respuesta = false;
		
		try {
			respuesta = proyectFaseTransfDAO.eliminaProyec(proyec);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<ProyectFaseTransfDTO> obtieneDatosProyec( String proyect) {
		
		try {
			listafila =  proyectFaseTransfDAO.obtieneDatosProyec(proyect);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	

	
	public boolean actualizaProyec(ProyectFaseTransfDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = proyectFaseTransfDAO.actualizaProyec(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	public boolean actualizaProyecNvo(ProyectFaseTransfDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = proyectFaseTransfDAO.actualizaProyecNvo(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}


}