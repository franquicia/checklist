package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.CheckinDTO;
import com.gruposalinas.checklist.mappers.CheckinRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class CheckinDAOImpl extends DefaultDAO implements CheckinDAO {

    private static Logger logger = LogManager.getLogger(ProtocoloDAOImpl.class);

    DefaultJdbcCall jdbcObtieneCheckin;
    DefaultJdbcCall jdbcInsertaCheckin;
    DefaultJdbcCall jdbcActualizaCheckin;
    DefaultJdbcCall jdbcEliminaCheckin;
    DefaultJdbcCall jdbcObtieneArchiveroForm;

    public void init() {

        jdbcObtieneCheckin = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECK").withProcedureName("SPGETCHECKIN")
                .returningResultSet("PA_CONSULTA", new CheckinRowMapper());

        jdbcInsertaCheckin = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECK").withProcedureName("SPINSCHECKIN");

        jdbcActualizaCheckin = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECK").withProcedureName("SPACTCHECKIN");

        jdbcEliminaCheckin = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECK").withProcedureName("SPDELCHECKIN");

    }

    @SuppressWarnings("unchecked")
    /* @Override */
    public List<CheckinDTO> obtieneCheckin(CheckinDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<CheckinDTO> listaCheck = null;

        String idBitacora = null;
        if (bean.getIdBitacora() != 0) {
            idBitacora = "" + bean.getIdBitacora();
        }

        String idBandera = null;
        if (bean.getBandera() != 0) {
            idBandera = "" + bean.getBandera();
        }

        String idUsuarioS = null;
        if (bean.getIdUsuario() != 0) {
            idUsuarioS = "" + bean.getIdUsuario();
        }

        String tipoConexion = null;
        if (bean.getTiempoConexion() != 0) {
            tipoConexion = "" + bean.getTiempoConexion();
        }

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_BITACORA", idBitacora).addValue("PA_FCID_USUARIO", idUsuarioS)
                .addValue("PA_FCID_CECO", bean.getIdCeco()).addValue("PA_FDFECHA", bean.getFecha())
                .addValue("PA_FCLATITUD", bean.getLatitud()).addValue("PA_FCLONGITUD", bean.getLongitud())
                .addValue("PA_FCBANDERA", idBandera).addValue("PA_PLATAFORMA", bean.getPlataforma())
                .addValue("PA_CONEXION", tipoConexion);

        out = jdbcObtieneCheckin.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAADMCHECK.SPGETCHECKIN");

        listaCheck = (List<CheckinDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar el Checkin");
        } else {
            return listaCheck;
        }

        return listaCheck;
    }

    @Override
    public int insertaCheckin(CheckinDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        int idFiidid = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_ID", bean.getIdCheck())
                .addValue("PA_FIID_BITACORA", bean.getIdBitacora()).addValue("PA_FCID_USUARIO", bean.getIdUsuario())
                .addValue("PA_FCID_CECO", bean.getIdCeco()).addValue("PA_FDFECHA", bean.getFecha())
                .addValue("PA_FCLATITUD", bean.getLatitud()).addValue("PA_FCLONGITUD", bean.getLongitud())
                .addValue("PA_FCBANDERA", bean.getBandera()).addValue("PA_PLATAFORMA", bean.getPlataforma())
                .addValue("PA_CONEXION", bean.getTiempoConexion());

        out = jdbcInsertaCheckin.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMCHECK.SPINSCHECKIN}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar el protocolo");
        } else {
            BigDecimal idCheckin = (BigDecimal) out.get("PA_FID_ID");
            idFiidid = idCheckin.intValue();
            return idFiidid;
        }
        return idFiidid;
    }

    /* @Override */
    public boolean modificaCheckin(CheckinDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_ID", bean.getIdCheck())
                .addValue("PA_FIID_BITACORA", bean.getIdBitacora()).addValue("PA_FCID_USUARIO", bean.getIdUsuario())
                .addValue("PA_FCID_CECO", bean.getIdCeco()).addValue("PA_FDFECHA", bean.getFecha())
                .addValue("PA_FCLATITUD", bean.getLatitud()).addValue("PA_FCLONGITUD", bean.getLongitud())
                .addValue("PA_FCBANDERA", bean.getBandera()).addValue("PA_PLATAFORMA", bean.getPlataforma())
                .addValue("PA_CONEXION", bean.getTiempoConexion());

        out = jdbcActualizaCheckin.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMCHECK.SPACTCHECKIN}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar el protocolo");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean eliminaCheckin(int fiid_protocolo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_ID", fiid_protocolo);

        out = jdbcEliminaCheckin.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMCHECK.SPDELCHECKIN}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al eliminar el Checkin");
        } else {
            return true;
        }
        return false;
    }

}
