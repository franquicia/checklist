package com.gruposalinas.checklist.util;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;

public class UtilImage {
	public static String getFoto(int numEmp){
		String urlOk = "";
		//Obtenemos la foto del usuario
		try {
			URL url1 = new URL("http://10.64.204.130/fotos/EKT-BAZ/empleados/"+numEmp+".jpg");
			URL url2 = new URL("http://10.64.204.130/fotos/AZTECA/EMPLEADOS/"+numEmp+".jpg");
			URL url3 = new URL("http://10.58.20.238/FotografiasWEB/FotografiasSQL/ObtenerFoto.ashx?compania=EKT&nempleado="+numEmp);
			URL url4 = new URL("http://10.64.204.130/fotos/EKT-BAZ/externos/"+numEmp+".jpg");
			
			urlOk = tryFoto(url1);
			if(urlOk.equals("keepTrying")){
				urlOk = tryFoto(url2);
				if(urlOk.equals("keepTrying")){
					urlOk = tryFoto(url3);
					if(urlOk.equals("keepTrying")){
						urlOk = tryFoto(url4);
						if(urlOk.equals("keepTrying")){
							urlOk = "imagenes/fotografia.png";
						}
					}
				}
			}
			
		} catch (MalformedURLException e) {
			urlOk = "imagenes/fotografia.png";
			
		} 
		
		return urlOk;
	}
	
	private static String tryFoto(URL url){
		BufferedImage foto = null;
		String urlOk = "keepTrying";
		try{
			foto = ImageIO.read(url);
			if(foto != null){
				urlOk = "http://"+url.getAuthority()+url.getPath();
			} else {
				urlOk = "keepTrying";
			}
		} catch (Exception a){
			urlOk = "keepTrying";
		}
		
		return urlOk;
	}
	
	/*
	 * 
	 *Funcion para rotar una imagen 
	 * 
	 * 
	 */
	public static BufferedImage rotate(BufferedImage img, int rotation) {
		int w = img.getWidth();
		int h = img.getHeight();
		BufferedImage newImage = new BufferedImage(h, w, img.getType());
		Graphics2D g2 = newImage.createGraphics();
		g2.rotate(Math.toRadians(rotation), w/2, h/2);
		g2.drawImage(img,null,130,128);
		
		return newImage;
	}
	
	/*
	 * 
	 * Función para redimensionar imágenes
	 * img - imagen a redimensionar
	 * newW - Nuevo ancho
	 * newH - Nuevo alto
	 * 
	 */
	public static BufferedImage resize(BufferedImage img, int newW, int newH) { 
	    Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
	    BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

	    Graphics2D g2d = dimg.createGraphics();
	    g2d.drawImage(tmp, 0, 0, null);
	    g2d.dispose();

	    return dimg;
	}
	/*
	 * 
	 * Función para generar texto Centrado
	 * g - Instancia de Graphics para calculos
	 * text - el texto que se va a ingresar
	 * font - el tipo de fuente para calcular tamaño total
	 * w - width total de la imagen a centrar
	 * p - partes en las que se divide el width total (centrado a la derecha o a la izquierda o en el centro)
	 * r - reserva para ajustar con pixeles de más o de menos
	 */
	public static int centerHorizontal(Graphics g, String text, Font font, int w, int p, int r) {
	    // Obtiene el FontMetrics
	    FontMetrics metrics = g.getFontMetrics(font);
	    
	    // Determina la coordenada en X para el texto
	    return (w - metrics.stringWidth(text)) / p + r;
	}
}
