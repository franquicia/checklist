package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.TipoChecklistDTO;
import com.gruposalinas.checklist.mappers.TipoChecklistRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class TipoChecklistDAOImpl extends DefaultDAO implements TipoChecklistDAO {

    private static Logger logger = LogManager.getLogger(TipoChecklistDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneTipoChecklist;
    DefaultJdbcCall jdbcInsertaTipoChecklist;
    DefaultJdbcCall jdbcActualizaTipoChecklist;
    DefaultJdbcCall jdbcEliminaTipoChecklist;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TIPOCKL")
                .withProcedureName("SP_SEL_G_TIPOCKL")
                .returningResultSet("RCL_TIPOCKL", new TipoChecklistRowMapper());

        jdbcObtieneTipoChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TIPOCKL")
                .withProcedureName("SP_SEL_TIPOCKL")
                .returningResultSet("RCL_TIPOCKL", new TipoChecklistRowMapper());

        jdbcInsertaTipoChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TIPOCKL")
                .withProcedureName("SP_INS_TIPOCKL");

        jdbcActualizaTipoChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TIPOCKL")
                .withProcedureName("SP_ACT_TIPOCKL");

        jdbcEliminaTipoChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TIPOCKL")
                .withProcedureName("SP_DEL_TIPOCKL");

    }

    @SuppressWarnings("unchecked")
    public List<TipoChecklistDTO> obtieneTipoChecklist() throws Exception {
        Map<String, Object> out = null;
        List<TipoChecklistDTO> listaTipoChecklist = null;
        int error = 0;
        out = jdbcObtieneTodos.execute();
        logger.info("Funci�n ejecutada: {checklist.PA_ADM_TIPOCKL.SP_SEL_G_TIPOCKL}");
        listaTipoChecklist = (List<TipoChecklistDTO>) out.get("RCL_TIPOCKL");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener los Tipo de Checklist");
        } else {
            return listaTipoChecklist;
        }

        return listaTipoChecklist;

    }

    @SuppressWarnings("unchecked")
    public List<TipoChecklistDTO> obtieneTipoChecklist(int idTipoCK) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<TipoChecklistDTO> listaTipoChecklist = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_TIPOCKL", idTipoCK);

        out = jdbcObtieneTipoChecklist.execute(in);

        logger.info("Finci�n ejecutada: {checklistPA_ADM_TIPOCKL.SEP_SEL_TIPOCKL}");

        listaTipoChecklist = (List<TipoChecklistDTO>) out.get("RCL_TIPOCKL");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener los Tipo de Checklist");
        } else {
            return listaTipoChecklist;
        }

        return listaTipoChecklist;

    }

    public int insertaTipoChecklist(TipoChecklistDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idTipoCKL = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_DESCRIPCION", bean.getDescTipo());

        out = jdbcInsertaTipoChecklist.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_TIPOCKL.SP_INS_TIPOCKL}");

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_FIID_TIPOCKL");
        idTipoCKL = idTipoReturn.intValue();

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar el Tipo de Checklist");
        } else {
            return idTipoCKL;
        }

        return idTipoCKL;
    }

    public boolean actualizaTipoChecklist(TipoChecklistDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_TIPOCKL", bean.getIdTipoCheck())
                .addValue("PA_DESCRIPCION", bean.getDescTipo());

        out = jdbcActualizaTipoChecklist.execute(in);
        logger.info("Funcion ejecutada: {checklist.PA_ADM_TIPOCKL.SP_ACT_TIPOCKL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar Tipo Archivo  id( " + bean.getIdTipoCheck() + ")");
        } else {
            return true;
        }

        return false;

    }

    public boolean eliminaTipoChecklist(int idTipoCK) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_TIPOCKL", idTipoCK);

        out = jdbcEliminaTipoChecklist.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_TIPOCKL.SP_DEL_TIPOCKL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al borrar el Tipo archivo id(" + idTipoCK + ")");
        } else {
            return true;
        }

        return false;

    }
}
