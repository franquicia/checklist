package com.gruposalinas.checklist.domain;

public class PosiblesDTO {

	private int idPosible;
	private String descripcion;
	private String numeroRevision;
	private String tipoCambio;
	
	public int getIdPosible() {
		return idPosible;
	}
	public void setIdPosible(int idPosible) {
		this.idPosible = idPosible;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getNumeroRevision() {
		return numeroRevision;
	}
	public void setNumeroRevision(String numeroRevision) {
		this.numeroRevision = numeroRevision;
	}
	public String getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	
}
