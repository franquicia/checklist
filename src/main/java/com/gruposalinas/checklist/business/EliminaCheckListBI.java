package com.gruposalinas.checklist.business;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.EliminaCheckListDAO;



public class EliminaCheckListBI {
	
	private Logger logger = LogManager.getLogger(EliminaCheckListBI.class);
	
	@Autowired
	EliminaCheckListDAO eliminaCheckListDAO;
	
	
	public boolean eliminaCheckList(){
		boolean respuesta = false;
		
		try {
			respuesta = eliminaCheckListDAO.eliminaCheckList();
		} catch (Exception e) {
			logger.info("No fue posible eliminar informacion checklist en la tabla FRTACHECKUSUA eliminaCheckList()");
				
		}
		
		return respuesta;
	}
	

}
