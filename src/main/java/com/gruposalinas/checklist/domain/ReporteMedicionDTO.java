package com.gruposalinas.checklist.domain;

public class ReporteMedicionDTO {
		
	//Entrada
	private int idProtocolo;
	private String fechaInicio;
	private String fechaFin;
	//Primer Cursor
	private int idChecklist;
	private int idPregunta;
	private String descPregunta;
	private int tipoPreg;
	private int estatus;
	private int idModulo;
	private int idPregPadre;
	private int idCritica;
	//Segundo Cursor
	private String fecha;
	private int idUsuario;
	private String nomUsuario;
	private int idPuesto;
	private int idCanal;
	private String nomCanal;
	private String nomPais;
	private String nomPuesto;
	private String idCeco;
	private String nomCeco;
	private String territorio;
	private String zona;
	private String region;
	private String sucursal;
	private String calificacion;
	private int idBitacora;
	private int idRespuesta;
	private String descRespuesta;
	private double idPonderacion;
	public int getIdProtocolo() {
		return idProtocolo;
	}
	public void setIdProtocolo(int idProtocolo) {
		this.idProtocolo = idProtocolo;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public String getDescPregunta() {
		return descPregunta;
	}
	public void setDescPregunta(String descPregunta) {
		this.descPregunta = descPregunta;
	}
	public int getTipoPreg() {
		return tipoPreg;
	}
	public void setTipoPreg(int tipoPreg) {
		this.tipoPreg = tipoPreg;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public int getIdModulo() {
		return idModulo;
	}
	public void setIdModulo(int idModulo) {
		this.idModulo = idModulo;
	}
	public int getIdPregPadre() {
		return idPregPadre;
	}
	public void setIdPregPadre(int idPregPadre) {
		this.idPregPadre = idPregPadre;
	}
	public int getIdCritica() {
		return idCritica;
	}
	public void setIdCritica(int idCritica) {
		this.idCritica = idCritica;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNomUsuario() {
		return nomUsuario;
	}
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}
	public int getIdPuesto() {
		return idPuesto;
	}
	public void setIdPuesto(int idPuesto) {
		this.idPuesto = idPuesto;
	}
	public int getIdCanal() {
		return idCanal;
	}
	public void setIdCanal(int idCanal) {
		this.idCanal = idCanal;
	}
	public String getNomCanal() {
		return nomCanal;
	}
	public void setNomCanal(String nomCanal) {
		this.nomCanal = nomCanal;
	}
	public String getNomPais() {
		return nomPais;
	}
	public void setNomPais(String nomPais) {
		this.nomPais = nomPais;
	}
	public String getNomPuesto() {
		return nomPuesto;
	}
	public void setNomPuesto(String nomPuesto) {
		this.nomPuesto = nomPuesto;
	}
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getNomCeco() {
		return nomCeco;
	}
	public void setNomCeco(String nomCeco) {
		this.nomCeco = nomCeco;
	}
	public String getTerritorio() {
		return territorio;
	}
	public void setTerritorio(String territorio) {
		this.territorio = territorio;
	}
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public int getIdRespuesta() {
		return idRespuesta;
	}
	public void setIdRespuesta(int idRespuesta) {
		this.idRespuesta = idRespuesta;
	}
	public String getDescRespuesta() {
		return descRespuesta;
	}
	public void setDescRespuesta(String descRespuesta) {
		this.descRespuesta = descRespuesta;
	}
	public double getIdPonderacion() {
		return idPonderacion;
	}
	public void setIdPonderacion(double idPonderacion) {
		this.idPonderacion = idPonderacion;
	}
	
	
	@Override
	public String toString() {
		return "ReporteMedicionDTO [idProtocolo=" + idProtocolo + 
				", fechaInicio=" + fechaInicio + 
				", fechaFin=" + fechaFin + 
				", idChecklist=" + idChecklist + 
				", idPregunta=" + idPregunta + 
				", descPregunta=" + descPregunta + 
				", tipoPreg=" + tipoPreg + 
				", estatus=" + estatus + 
				", idModulo=" + idModulo + 
				", idPregPadre=" + idPregPadre +
				", idCritica=" + idCritica +
				", fecha=" + fecha + 
				", idUsuario=" + idUsuario + 
				", nomUsuario="	+ nomUsuario + 
				", idPuesto=" + idPuesto + 
				", idCanal=" + idCanal + 
				", nomCanal=" + nomCanal + 
				", nomPais=" + nomPais + 
				", nomPuesto=" + nomPuesto + 
				", idCeco=" + idCeco + 
				", nomCeco=" + nomCeco + 
				", territorio=" + territorio + 
				", zona=" + zona + 
				", region=" + region + 
				", sucursal=" + sucursal + 
				", calificacion=" + calificacion + 
				", idBitacora=" + idBitacora + 
				", idRespuesta=" + idRespuesta + 
				", descRespuesta=" + descRespuesta + 
				", idPonderacion=" + idPonderacion + "]";
	}
		
}
