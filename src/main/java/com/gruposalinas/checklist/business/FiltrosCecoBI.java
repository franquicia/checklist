package com.gruposalinas.checklist.business;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.FiltrosCecoDAO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.FiltrosCecoDTO;
import com.gruposalinas.checklist.domain.PaisDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class FiltrosCecoBI {
	
	private Logger logger = LogManager.getLogger(FiltrosCecoBI.class);
	
	@Autowired
	FiltrosCecoDAO filtrosCecoDAO;
	
	public Map<String , Object> obtieneFiltros(int idUsuario){
		Map<String , Object> filtros = new HashMap<String, Object>();
		
		try {
			filtros = filtrosCecoDAO.obtieneFiltros(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los filtros");
			
		}
		
		return filtros;
	 
	}
	
	public List<FiltrosCecoDTO> obtieneCecos(int idCeco, int tipoBusqueda){
		
		List<FiltrosCecoDTO> cecos = null;
		
		try {
			cecos = filtrosCecoDAO.obtieneCecos(idCeco, tipoBusqueda);
		} catch (Exception e) {
			logger.info("No fue posible obtener los cecos");
			
		}
		
		
		return cecos;
	}
	
	public List<ChecklistDTO> obtieneChecklist(int idUsuario){
		
		List<ChecklistDTO> cecos = null;
		
		try {
			cecos = filtrosCecoDAO.obtieneChecklist(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los checklist");
			
		}
		
		
		return cecos;
	}
	
	
	public List<PaisDTO> obtienePaises(int negocio){
		
		List<PaisDTO> paises = null;
		
		try {
			paises = filtrosCecoDAO.obtienePaises(negocio);
		} catch (Exception e) {
			logger.info("No fue posible obtener los paises");
			
		}
		
		return paises;
		
	}
	
	public List<PaisDTO> obtienePaises(int negocio, int idUsuario){
		
		List<PaisDTO> paises = null;
		
		try {
			paises = filtrosCecoDAO.obtienePaises(negocio, idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los paises");
			
		}
		
		return paises;
		
	}
	
	public List<FiltrosCecoDTO> obtieneTerritorios(int pais, int negocio){
		
		List<FiltrosCecoDTO> cecos = null;
		
		try {
			cecos = filtrosCecoDAO.obtieneTerritorios(pais, negocio);
		} catch (Exception e) {
			logger.info("No fue posible obtener los cecos");
			
		}
		
		
		return cecos;
	}
	

}
