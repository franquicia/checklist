package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.RepoCheckDTO;

public class RepoCheckRowMapper implements RowMapper<RepoCheckDTO> {

	public RepoCheckDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		//REPORTE PARA CONSULTA DE PROTOCOLOS DE CERFIFICACION 11 CHECKLIST
		RepoCheckDTO checklist = new RepoCheckDTO();
		
		checklist.setNombre(rs.getString("FCNOMBRE"));
		checklist.setCeco(rs.getString("FCID_CECO"));
		checklist.setIdcheckList(rs.getInt("FIID_CHECKLIST"));
		checklist.setNombCheck(rs.getString("FCNOMBRE"));
		checklist.setCalif(rs.getInt("FICALIFICACION"));
		checklist.setFechaInic("FDINICIO");
		checklist.setFechaFin(rs.getString("FDTERMINO"));
		checklist.setFechaFin(rs.getString("DIFERENCIA"));
		
	
		return checklist;
	}

}


