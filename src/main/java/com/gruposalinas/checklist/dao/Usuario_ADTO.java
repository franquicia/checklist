package com.gruposalinas.checklist.dao;

public class Usuario_ADTO {

	private int idUsuario;
	private int idPuesto;
	private int idPerfil;
	private String idCeco;
	private String nombre;
	private int activo;
	private String fecha;
	
	
	private int puestof;
	private String descPuesto;
	private String desPuestof;
	private String pais;
	private String fechaBaja;
	private int idEmpleadoRep;
	private int ccEmpleadoRep;
	private String nombreCeco;
	
	
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdPuesto() {
		return idPuesto;
	}
	public void setIdPuesto(int idPuesto) {
		this.idPuesto = idPuesto;
	}
	public int getIdPerfil() {
		return idPerfil;
	}
	public void setIdPerfil(int idPerfil) {
		this.idPerfil = idPerfil;
	}
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getPuestof() {
		return puestof;
	}
	public void setPuestof(int puestof) {
		this.puestof = puestof;
	}
	public String getDescPuesto() {
		return descPuesto;
	}
	public void setDescPuesto(String descPuesto) {
		this.descPuesto = descPuesto;
	}
	public String getDesPuestof() {
		return desPuestof;
	}
	public void setDesPuestof(String desPuestof) {
		this.desPuestof = desPuestof;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getFechaBaja() {
		return fechaBaja;
	}
	public void setFechaBaja(String fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	public int getIdEmpleadoRep() {
		return idEmpleadoRep;
	}
	public void setIdEmpleadoRep(int idEmpleadoRep) {
		this.idEmpleadoRep = idEmpleadoRep;
	}
	public int getCcEmpleadoRep() {
		return ccEmpleadoRep;
	}
	public void setCcEmpleadoRep(int ccEmpleadoRep) {
		this.ccEmpleadoRep = ccEmpleadoRep;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	
	
}
