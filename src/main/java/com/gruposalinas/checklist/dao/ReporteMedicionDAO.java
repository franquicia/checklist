package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.ReporteMedPregDTO;
import com.gruposalinas.checklist.domain.ReporteMedicionDTO;

public interface ReporteMedicionDAO {

	public Map<String, Object> ReporteMedicion (int idProtocolo, String fechaInicio, String fechaFin) throws Exception;
	public Map<String, Object> ReporteProtMedicion (int idProtocolo, String fechaInicio, String fechaFin) throws Exception;
	public List <ReporteMedPregDTO> getCursor(int idProtocolo, String fechaInicio, String fechaFin) throws Exception;
}
