package com.gruposalinas.checklist.business;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ReporteSupervisionSistemasDAO;
import com.gruposalinas.checklist.domain.ReporteSupervisionSistemasDTO;

public class ReporteSupervisionSistemasBI {
	private static final Logger logger = LogManager.getLogger(ReporteSupervisionSistemasBI.class);

	@Autowired
	ReporteSupervisionSistemasDAO reporteSupervisionSistemasDAO;

	List<ReporteSupervisionSistemasDTO> listaCheckSistemas = null;
	List<ReporteSupervisionSistemasDTO> listaCecosSistemas = null;
	Map<String, Object> listaChecklistCompleto = null;

	public Map<String, Object> reporteSistemas(int idCeco) throws Exception {
		listaChecklistCompleto = reporteSupervisionSistemasDAO.reporteSistemas(idCeco);
		if (listaChecklistCompleto.size() == 0)
			return null;
		else
			return listaChecklistCompleto;
	}
	
	public Map<String, Object> reporteSistemas2(int idCeco) throws Exception {
		listaChecklistCompleto = reporteSupervisionSistemasDAO.reporteSistemas2(idCeco);
		if (listaChecklistCompleto.size() == 0)
			return null;
		else
			return listaChecklistCompleto;
	}

	public List<ReporteSupervisionSistemasDTO> reporteSisCap(String inicio, String fin, String checklist,
			String sucursal, String area) {
		try {
			listaCheckSistemas = reporteSupervisionSistemasDAO.reporteSisCap(inicio, fin, checklist, sucursal, area);
		} catch (Exception e) {
			logger.info("No fue posible consultar los checklist" + e);
		}
		return listaCheckSistemas;
	}

	public List<ReporteSupervisionSistemasDTO> filtroCecos(int idReporte) throws Exception {
		try {
			listaCheckSistemas = reporteSupervisionSistemasDAO.filtroCheck(idReporte);
		} catch (Exception e) {
			logger.info("No fue posible consultar los checklist" + e);
		}
		return listaCheckSistemas;
	}

	public List<ReporteSupervisionSistemasDTO> filtroSucursal(String sucursal) {
		try {
			listaCheckSistemas = reporteSupervisionSistemasDAO.filtroSucursal(sucursal);
		} catch (Exception e) {
			logger.info("No fue posible consultar los checklist" + e);
		}
		return listaCheckSistemas;
	}
	
	public List<ReporteSupervisionSistemasDTO> filtroUsuario(String usuario) {
		try {
			listaCheckSistemas = reporteSupervisionSistemasDAO.filtroUsuario(usuario);
		} catch (Exception e) {
			logger.info("No fue posible consultar los checklist" + e);
		}
		return listaCheckSistemas;
	}

	public Map<String, Object> obtenerRespuestas(int idBitacora) throws Exception {
		// try {
		// listaCheckSistemas =
		// reporteSupervisionSistemasDAO.obtenerRespuestas(idBitacora);
		// } catch (Exception e) {
		// logger.info("No fue posible consultar los checklist" + e);
		// }
		// return listaCheckSistemas;

		listaChecklistCompleto = reporteSupervisionSistemasDAO.obtenerRespuestas(idBitacora);
		logger.info("Tamaño de lista checklista size:" + listaChecklistCompleto.size());
		if (listaChecklistCompleto.size() == 0)
			return null;
		else
			return listaChecklistCompleto;

	}

	public List<ReporteSupervisionSistemasDTO> obtenerEvidencia(int idBitacora) {
		List<ReporteSupervisionSistemasDTO> listaEvidencia = null;
		try {
			listaEvidencia = reporteSupervisionSistemasDAO.obtenerEvidencia(idBitacora);
		} catch (Exception e) {
			logger.info("No fue posible consultar los checklist" + e);
		}
		return listaEvidencia;
	}

}
