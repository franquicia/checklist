package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.mappers.ChecklistPreguntaTRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ChecklistPreguntaTDAOImpl extends DefaultDAO implements ChecklistPreguntaTDAO {

    private static Logger logger = LogManager.getLogger(ChecklistPreguntaTDAOImpl.class);

    private DefaultJdbcCall buscaChecklistPreguntaTemp;
    private DefaultJdbcCall insertaChecklistPreguntaTemp;
    private DefaultJdbcCall actualizaChecklistPreguntaTemp;
    private DefaultJdbcCall eliminaChecklistPreguntaTemp;

    public void init() {

        buscaChecklistPreguntaTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECKPTEMP")
                .withProcedureName("SP_SEL_CHECKP_TEMP")
                .returningResultSet("RCL_CHECK_PREG", new ChecklistPreguntaTRowMapper());

        insertaChecklistPreguntaTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECKPTEMP")
                .withProcedureName("SP_INS_CHECKP_TEMP");

        actualizaChecklistPreguntaTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECKPTEMP")
                .withProcedureName("SP_ACT_CHECKP_TEMP");

        eliminaChecklistPreguntaTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECKPTEMP")
                .withProcedureName("SP_DEL_CHECKP_TEMP");
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ChecklistPreguntaDTO> obtienePregCheckTemp(int idChecklist) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        List<ChecklistPreguntaDTO> listaChecklistPregunta = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_CHECKLIST", idChecklist);

        out = buscaChecklistPreguntaTemp.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_ADM_CHECK_PREG.SPOBTIENEPREGS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = errorReturn.intValue();

        listaChecklistPregunta = (List<ChecklistPreguntaDTO>) out.get("RCL_CHECK_PREG");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al obtener el ChecklistPregunta de la tabla temporal");
        } else {
            return listaChecklistPregunta;
        }

        return null;
    }

    @Override
    public boolean insertaChecklistPreguntaTemp(ChecklistPreguntaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECKLIST", bean.getIdChecklist())
                .addValue("PA_FIID_PREGUNTA", bean.getIdPregunta())
                .addValue("PA_FIORDEN_CHECK", bean.getOrdenPregunta())
                .addValue("PA_FIID_PREG_PADRE", bean.getPregPadre())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_NUM_REV", bean.getNumVersion())
                .addValue("PA_TIPO_MOD", bean.getTipoCambio());

        out = insertaChecklistPreguntaTemp.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMCHECKPTEMP.SP_INS_CHECKP_TEMP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar ChecklistPregunta en la tabla temporal");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean actualizaChecklistPreguntaTemp(ChecklistPreguntaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECKLIST", bean.getIdChecklist())
                .addValue("PA_FIID_PREGUNTA", bean.getIdPregunta())
                .addValue("PA_FIORDEN_CHECK", bean.getOrdenPregunta())
                .addValue("PA_FIID_PREG_PADRE", bean.getPregPadre())
                .addValue("PA_NUM_REV", bean.getNumVersion())
                .addValue("PA_TIPO_MOD", bean.getTipoCambio());

        out = actualizaChecklistPreguntaTemp.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMCHECKPTEMP.SP_ACT_CHECKP_TEMP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar ChecklistPregunta de la tabla temporal");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean eliminaChecklistPreguntaTemp(int idChecklist, int idPregunta) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECKLIST", idChecklist)
                .addValue("PA_FIID_PREGUNTA", idPregunta);

        out = eliminaChecklistPreguntaTemp.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMCHECKPTEMP.SP_DEL_CHECKP_TEMP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al eliminar ChecklistPregunta de la tabla temporal");
        } else {
            return true;
        }
        return false;
    }

}
