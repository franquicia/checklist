package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.VisitasxMesDAO;
import com.gruposalinas.checklist.domain.VisitasxMesDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class VisitasxMesBI {
	
	private Logger logger = LogManager.getLogger(VisitasxMesBI.class);
	
	@Autowired
	VisitasxMesDAO visitasxMesDAO;
	
	public List<VisitasxMesDTO> obtieneVisitas(int checklist, String fechaInicio, String fechaFin){
		
		List<VisitasxMesDTO> lista = null;
		
	    try {
			lista = visitasxMesDAO.obtieneVisitas(checklist, fechaInicio, fechaFin);
		} catch (Exception e) {
			
			logger.info("No fue posible obtener las visitas ");
			
		}
		
		return lista;
	}

}
