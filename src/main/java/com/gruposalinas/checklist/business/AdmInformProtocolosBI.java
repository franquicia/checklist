package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.AdmInformProtocolosDAO;
import com.gruposalinas.checklist.domain.AdmInformProtocolosDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class AdmInformProtocolosBI {

	private static Logger logger = LogManager.getLogger(AdmInformProtocolosBI.class);

	@Autowired
	AdmInformProtocolosDAO admInformProtocolosDAO;
	
	List<AdmInformProtocolosDTO> lista = null;

	

	public List<AdmInformProtocolosDTO> obtieneTodos(){
				
		try{
			lista = admInformProtocolosDAO.obtieneTodos();
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla FRTAINFPROT " + e);
					
		}
				
		return lista;
	}
	

	public boolean insertaInformProtocolos(AdmInformProtocolosDTO bean) {
		
		boolean respuesta = false;
		
		try {		
			respuesta = admInformProtocolosDAO.insertaInformProtocolos(bean);		
		} catch (Exception e) {
			logger.info("No fue posible insertar detalle del informe " + e);
			
		}
		
		return respuesta;		
	}
	
	
	public boolean actualizaInformProtocolos (AdmInformProtocolosDTO bean){
		
		boolean respuesta = false;
				
		try {
			respuesta = admInformProtocolosDAO.actualizaInformProtocolos(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar detalle de informe " + e);
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaHandbook(AdmInformProtocolosDTO bean){
		
		boolean respuesta = false;
		
		try{
			respuesta = admInformProtocolosDAO.eliminaInformProtocolos(bean);
		}catch(Exception e){
			logger.info("No fue posible borrar detalle de informe " + e);
			
		}
		
		return respuesta;
	}

}
