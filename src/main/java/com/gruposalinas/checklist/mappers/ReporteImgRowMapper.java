package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.ReporteImgDTO;

public class ReporteImgRowMapper implements RowMapper<ReporteImgDTO> {

	public ReporteImgDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteImgDTO consultaDTO = new ReporteImgDTO();
		consultaDTO.setCeco(rs.getString("CECO"));
		consultaDTO.setSucursal(rs.getString("SUCURSAL"));
		consultaDTO.setFecha(rs.getString("FECHAS DE COINCIDENCIA"));
		consultaDTO.setProtocolo(rs.getString("PROTOCOLO"));
		consultaDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		consultaDTO.setOrdenGpo(rs.getInt("FIORDEN_GRUPO"));
		consultaDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		
		return consultaDTO;
		
		
	}

}
