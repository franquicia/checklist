package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistUsuarioDTO;

public class CheckUsuaOfflineRowMapper  implements RowMapper<ChecklistUsuarioDTO>  {

	public ChecklistUsuarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistUsuarioDTO checklistUsuarioDTO = new ChecklistUsuarioDTO();
		
		checklistUsuarioDTO.setIdCheckUsuario(rs.getInt("FIID_CHECK_USUA"));
		checklistUsuarioDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		checklistUsuarioDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		checklistUsuarioDTO.setUltimaVisita(rs.getString("ULTVISITA"));
		
		return checklistUsuarioDTO;
	}

}
