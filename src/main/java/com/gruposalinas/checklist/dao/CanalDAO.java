package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.CanalDTO;

public interface CanalDAO {
	
	public int insertaCanal(CanalDTO bean) throws Exception;
	
	public boolean actualizaCanal(CanalDTO bean) throws Exception;
	
	public boolean eliminaCanal(int idCanal) throws Exception;
	
	public List<CanalDTO> buscaCanal(int idCanal) throws Exception;
	
	public List<CanalDTO> buscaCanales(int activo) throws Exception;

}
