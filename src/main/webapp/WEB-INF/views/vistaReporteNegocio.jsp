<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Banco Azteca | Reporte RH</title>
	
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/css/vistaReportes/estilo.css" />
	
	<link rel="stylesheet" media="screen"
		href="../css/vistaCumplimientoVisitas/modal.css">
	
	<!-- jquery Start -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/vistaCumplimientoVisitas/jquery.js"></script>
	
	<!-- dropdown -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/vistaCumplimientoVisitas/menu.js"></script>
	
	<!-- dropdown -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/vistaReportes/script-reporteNegocio.js"></script>
	
	<meta name="viewport"
		content="width=device-width, initial-scale=1, maximum-scale=1">
	<% response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate"); %>
	<% response.addHeader("Pragma", "no-cache"); %>
	<% response.addHeader("Expires", "0"); %>
</head>

<body onload="nobackbutton();">

<div id="tipoModal" class="modal3"></div>


	<input type="hidden" id="longitudRegiones" 	value="${fn:length(listaCecos)}"></input>

	<div class="header" style="z-index: 10; top: -40px;">

		<div class="wrapperFixed"
			style="background: red; background: #f2f2f2;">

			<div class="goborder">
				<h2>
					<a id="idPais" style="color: #747474"></a><span class="subSeccion">/<a
						id="idOpcTerr" style="color: #747474"></a></span>
				</h2>
			</div>

			<table class="avance"
				style="max-width: 800px; min-width: 320px; height: 100px; background: #a6b0b3; border-radius: 150px 150px 150px 150px;">
				<thead>
					<tr style="height: 50px;">
						<th rowspan="2" class="rayaBlanca"><div class="textoComp">Mi <BR> Compromiso </div>
						</th>

						<th style="width: ${64/5}%;"><p class="margenArriba">Contribución</p></th>
						<th style="width: ${64/5}%;"><p class="margenArriba">Normalidad</p></th>
						<th style="width: ${64/5}%;"><p class="margenArriba">Nunca Abonadas</p></th>
						<th style="width: ${64/5}%;"><p class="margenArriba">Vista</p></th>
						<th style="width: ${64/5}%;"><p class="margenArriba">Plazo</p></th>
					</tr>
					<tr style="height: 50px;">

						<th style="width: ${64/5}%;"><p class="margenAbajo">+5%</p></th>
						<th style="width: ${64/5}%;"><p class="margenAbajo">85%</p></th>
						<th style="width: ${64/5}%;"><p class="margenAbajo">85%</p></th>
						<th style="width: ${64/5}%;"><p class="margenAbajo">100%</p></th>
						<th style="width: ${64/5}%;"><p class="margenAbajo">100%</p></th>

						<!-- <th style="width: ${64/conteo}%;"><p class="margenAbajo"> &#2713; ${(fn:split(porcentajeG,'.'))[0]}%</p></th> -->
					</tr>
				</thead>
			</table>

			<div class="hspacerSin"></div>
		</div>
	</div>

	<div class="wrapper">
		<div class="hspacer"></div>

		<div class="box" style="margin-top: 37px;">

			<dl class="accordion3" id="idGeneralTablaTerritorios">

				<c:if test="${listaCecos!=null}">
			
					<c:forEach var="i" begin="0" end="${(fn:length(listaCecos))-1}">
						<dt onclick="despliega(${listaCecos[i].idCeco}, ${i})" style="background: #ffffff;">
							<table class="bloquer linea goshadow1">
								<tbody>									
									<tr class="pointer">
									
										<td id="nom0${i}" class="nombreCheck">${listaCecos[i].descCeco}
											<span> suc</span>
										</td>
										<td id="con0${i}" class="nombreCheck" style="width: ${64/5}%;">
											<div class="radio grisN">-</div>
										</td>
										<td id="nor0${i}" class="nombreCheck" style="width: ${64/5}%;">
											<div class="radio grisN">-</div>
										</td>
										<td id="nun0${i}" class="nombreCheck" style="width: ${64/5}%;">
											<div class="radio grisN">-</div>
										</td>
										<td id="vis0${i}" class="nombreCheck" style="width: ${64/5}%;">
											<div class="radio grisN">-</div>
										</td>
										<td id="pla0${i}" class="nombreCheck" style="width: ${64/5}%;">
											<div class="radio grisN">-</div>
										</td>
										
										<input type="hidden" id="posicion${i}"	value="${listaCecos[i].idCeco}"></input>
										<c:set var="idCeco" scope="session"	value="${listaCecos[i].idCeco}" />
									</tr>
								</tbody>
							</table>
						</dt>


						<dd>
							<div class="inner" id="idLlenaZonas${listaCecos[i].idCeco}"></div>
						</dd>
						
						<script>

							muestraZonas(${i},${listaCecos[i].idCeco},'${listaCecos[i].descCeco}',${(fn:length(listaCecos))},'0');
							
							//document.getElementById("tipoModal").className="modal1";
						</script>
						
					</c:forEach>
				</c:if>
				<c:if test="${listaCecos==null}">
					<script>
						document.getElementById("tipoModal").className="modal1";
					</script>
				</c:if>
				
			</dl>
		</div>
		<div class="fspacer"></div>
	</div>

	<script type="text/javascript">
		jQuery(function() {
			$(".subSeccion").hide();

		  var allPanels = $('.accordion3 > dd').hide();
		  var activo = 0;

		  jQuery('.accordion3 > dt').on('click', function() {
		    $this = $(this);
		    //the target panel content
		    $target = $this.next();
			 
		    jQuery('.accordion3 > dt').removeClass('accordion3-active');
		    if ($target.hasClass("in")) {
				$this.removeClass('accordion3-active');
				$target.slideUp();
				$target.removeClass("in");
				$(".subSeccion").hide();

				clearInterval(intervaloDespliegue);
				intervalo = setInterval("reporte()",300000);
				
				activo=0;
				
		    } else {
			      $this.addClass('accordion3-active');
			      jQuery('.accordion3 > dd').removeClass("in");
			      $target.addClass("in");
				  $(".subSeccion").show();
			      jQuery('.accordion3 > dd').slideUp();
			      $target.slideDown();
		
			      clearInterval(intervalo);
			      
			      if(activo==0){
			    	  intervaloDespliegue = setInterval("despliegaCheckIntervalo()",300000);
			    	  activo=1;
			      }
						
		    }
		  })
		})
	</script>



</body>

</html>