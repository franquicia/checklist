package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.EdoChecklistDAO;
import com.gruposalinas.checklist.domain.EdoChecklistDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class EdoChecklistBI {

	private static Logger logger = LogManager.getLogger(EdoChecklistBI.class);
	
	@Autowired
	
	EdoChecklistDAO edoChecklistDAO;
	
	List<EdoChecklistDTO> listaedoChecklist = null;
	
	List<EdoChecklistDTO> listaedosChecklist = null;
	
	public List<EdoChecklistDTO> obtieneEdoChecklist(){
		try{
			listaedosChecklist = edoChecklistDAO.obtieneEdoChecklist();
			}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Estado del Checklist");
						
		}
				
		return listaedosChecklist;
	}
	
	public List<EdoChecklistDTO> obtieneEdoChecklist(int idEdoCK){
		try{
			listaedoChecklist = edoChecklistDAO.obtieneEdoChecklist(idEdoCK);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Estado del Checklist");
						
		}
				
		return listaedoChecklist;
	}
	
	public int insertaEdoChecklist(EdoChecklistDTO edoCheck){
		
		int idEdoCK =0;
		try
		{
			idEdoCK = edoChecklistDAO.insertaEdoChecklist(edoCheck);
		}
		catch (Exception e) {
			logger.info("No fue posible insertar el Estado del Checklist");
			
		}
		
		return idEdoCK;	
	}
	
	public boolean actualizaEdoChecklist (EdoChecklistDTO edoCheck){
		boolean  respuesta = false;
		try{
			respuesta = edoChecklistDAO.actualizaEdoChecklist(edoCheck);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Estado del Checklist");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaEdoChecklist(int idEdoCK){
		boolean respuesta = false;
		
		try{
			respuesta = edoChecklistDAO.eliminaEdoCheckList(idEdoCK);
		}
		catch(Exception e){
			logger.info("No fue posible borrar el Estado del Checklist");
			
		}
		
		return respuesta;
	}
}
