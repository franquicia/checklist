package com.gruposalinas.checklist.servicios.servidor;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.checklist.business.ActorEdoHallaBI;
import com.gruposalinas.checklist.business.CheckInAsistenciaBI;
import com.gruposalinas.checklist.business.ConfiguraActorBI;
import com.gruposalinas.checklist.business.CorreoBI;
import com.gruposalinas.checklist.business.FirmaCheckBI;
import com.gruposalinas.checklist.business.FirmaExpansionCoreBI;
import com.gruposalinas.checklist.business.FirmasCatalogoBI;
import com.gruposalinas.checklist.business.FragmentMenuBI;
import com.gruposalinas.checklist.business.GeneraExcelDetalleExpansionBI;
import com.gruposalinas.checklist.business.HallazgosTransfBI;
import com.gruposalinas.checklist.business.ICUTiempoRealBI;
import com.gruposalinas.checklist.business.RepoFilExpBI;
import com.gruposalinas.checklist.business.ReporteChecksExpBI;
import com.gruposalinas.checklist.business.ReporteHallazgosBI;
import com.gruposalinas.checklist.business.ReporteSupervisionSistemasBI;
import com.gruposalinas.checklist.business.SoftNvoBI;
import com.gruposalinas.checklist.business.SoporteHallazgosTransfBI;
import com.gruposalinas.checklist.business.SucursalChecklistBI;
import com.gruposalinas.checklist.business.SucursalesCercanasBI;
import com.gruposalinas.checklist.domain.ActorEdoHallaDTO;
import com.gruposalinas.checklist.domain.AdmFirmasCatDTO;
import com.gruposalinas.checklist.domain.CRMDTO;
import com.gruposalinas.checklist.domain.ConfiguraActorDTO;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;
import com.gruposalinas.checklist.domain.FragmentMenuDTO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;
import com.gruposalinas.checklist.domain.IdICUTiempoRealDTO;
import com.gruposalinas.checklist.domain.ProgramacionMttoDTO;
import com.gruposalinas.checklist.domain.RepoFilExpDTO;
import com.gruposalinas.checklist.domain.ReporteChecksExpDTO;
import com.gruposalinas.checklist.domain.ReporteSupervisionSistemasDTO;
import com.gruposalinas.checklist.domain.SoftNvoDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.util.StrCipher;
import com.gruposalinas.checklist.util.UtilCryptoGS;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.misc.BASE64Decoder;

@Controller
@RequestMapping(value = {"/servicios", "/expansionCoreServices"})

public class ExpansionCoreServices {

    @Autowired
    FirmasCatalogoBI firmasCatalogoBI;
    @Autowired
    FragmentMenuBI fragmentMenuBI;
    @Autowired
    SoftNvoBI softNvoBI;
    @Autowired
    ActorEdoHallaBI actorEdoHallaBI;
    @Autowired
    ConfiguraActorBI configuraActorBI;
    @Autowired
    RepoFilExpBI repoFilExpBI;
    @Autowired
    HallazgosTransfBI hallazgosTransfBI;
    @Autowired
    SucursalChecklistBI sucursalChecklistBI;
    @Autowired
    FirmaCheckBI firmaCheckBI;
    @Autowired
    CorreoBI correoBI;
    @Autowired
    ReporteChecksExpBI reporteChecksBI;
    @Autowired
    SoporteHallazgosTransfBI soporteHallazgosTransfBI;
    @Autowired
    ReporteHallazgosBI reporteHallazgosBi;
    @Autowired
    ReporteHallazgosBI reporteHallazgosBI;
    @Autowired
    CheckInAsistenciaBI checkInAsistenciaBI;
    @Autowired
    SucursalesCercanasBI sucursalesCercanasBI;
    @Autowired
    ReporteSupervisionSistemasBI reporteSupervisionBI;
    @Autowired
    ICUTiempoRealBI iCUTiempoRealBI;

    private static Logger logger = LogManager.getLogger(ExpansionCoreServices.class);

    // ------------------------------------------------------------------------SERVICIOS
    // CORE
    // EXPANSION----------------------------------------------------------------------
    // ********* WAF *********
    // http://localhost:8080/checklist/servicios/getFirmasCatalogo.json?idUsuario=<?>
    // 10.51.218.154:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getFirmasCatalogo.json?idUsuario=370566
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getFirmasCatalogo", method = RequestMethod.GET)
    public @ResponseBody
    String getFirmasCatalogo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = "";
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String firmas = "[ ]";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            // idFirma = urides.split("&")[1].split("=")[1];

            // Obtiene todas las firmas
            try {

                // List<HallazgosExpDTO> listaHallazgos = hallazgosExpBI.obtieneDatos(idFirma);
                List<AdmFirmasCatDTO> listaFirmas = firmasCatalogoBI.obtieneDatosFirmaCat(null);
                // List<HallazgosEviExpDTO> listaEvidencias =
                // hallazgosEviExpBI.obtieneDatosBita(ceco);

                if (listaFirmas != null && listaFirmas.size() > 0) {

                    firmas = new Gson().toJson(listaFirmas);

                }

                res = "{";
                res += " \"Firmas\"  : " + firmas;
                res += "}";

            } catch (Exception e) {
                logger.info("AP Al obtener la lista de firmasCat");
                return "{}";
            }

        } catch (Exception e) {
            logger.info("AP con los parametros de obtener firmasCat");
            return "{}";
        }

        return res;

    }

    // ********* WAF *********
    // http://localhost:8080/checklist/servicios/getFirmasAgrupador.json?idUsuario=<?>&idFirma=
    // 10.51.218.154:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getFirmasAgrupador.json?idUsuario=370566&idFirma=
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getFirmasAgrupador", method = RequestMethod.GET)
    public @ResponseBody
    String getFirmasAgrupador(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = "";
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String idFirma = null;
        String AgrupadorFirmas = "[ ]";
        String listaEvidenciasResult = "[ ]";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            // 480100
            int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            idFirma = urides.split("&")[1].split("=")[1];

            // Obtiene los hallazgos por CECO
            try {

                List<AdmFirmasCatDTO> listaAgrupador = firmasCatalogoBI.obtieneDatosFirmaAgrupa(idFirma);

                if (listaAgrupador != null && listaAgrupador.size() > 0) {

                    AgrupadorFirmas = new Gson().toJson(listaAgrupador);
                }

                res = "{";
                res += " \"AgrupadorFirmas \"  : " + AgrupadorFirmas;
                res += "}";

            } catch (Exception e) {
                logger.info("AP Al obtener la lista de Agrupador Firmas");
                return "{}";
            }

        } catch (Exception e) {
            logger.info("AP con los parametros de obtener Agrupador Firmas");
            return "{}";
        }

        return res;

    }

    // ********* WAF *********
    // http://localhost:8080/checklist/servicios/getFragments.json?idUsuario=<?>
    // 10.51.218.154:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getFragments.json?idUsuario=370566
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getFragments", method = RequestMethod.GET)
    public @ResponseBody
    String getFragments(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = "";
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String fragments = "[ ]";
        String listaEvidenciasResult = "[ ]";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);

            try {

                List<FragmentMenuDTO> listaFragments = fragmentMenuBI.obtieneDatosFragment(null);

                if (listaFragments != null && listaFragments.size() > 0) {

                    fragments = new Gson().toJson(listaFragments);
                }

                res = "{";
                res += " \"Fragments\"  : " + fragments;
                res += "}";

            } catch (Exception e) {
                logger.info("AP Al obtener la lista de Fragments");
                return "{}";
            }

        } catch (Exception e) {
            logger.info("AP con los parametros de obtener firmasCat");
            return "{}";
        }

        return res;

    }

    // ********* WAF *********
    // http://localhost:8080/checklist/servicios/getOpcionesMenu.json?idUsuario=<?>
    // 10.51.218.154:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getOpcionesMenu.json?idUsuario=370566
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getOpcionesMenu", method = RequestMethod.GET)
    public @ResponseBody
    String getOpcionesMenu(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = "";
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String opcionesMenu = "[ ]";
        String listaEvidenciasResult = "[ ]";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);

            try {

                List<FragmentMenuDTO> listaMenu = fragmentMenuBI.obtieneDatosMenu(null);

                if (listaMenu != null && listaMenu.size() > 0) {

                    opcionesMenu = new Gson().toJson(listaMenu);
                }

                res = "{";
                res += " \"opcionesMenu\"  : " + opcionesMenu;
                res += "}";

            } catch (Exception e) {
                logger.info("AP Al obtener la lista de opcionesMenu");
                return "{}";
            }

        } catch (Exception e) {
            logger.info("AP con los parametros de obtener opcionesMenu");
            return "{}";
        }
        return res;
    }

    // ********* WAF *********
    // http://localhost:8080/checklist/servicios/getConfigProyecto.json?idUsuario=<?>
    // 10.51.218.154:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getConfigProyecto.json?idUsuario=370566
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getConfigProyecto", method = RequestMethod.GET)
    public @ResponseBody
    String getConfigProyecto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = "";
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String configuracionProyecto = "[ ]";
        String listaEvidenciasResult = "[ ]";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);

            try {

                List<FragmentMenuDTO> listaConfiguracion = fragmentMenuBI.obtieneDatosConf(null);

                if (listaConfiguracion != null && listaConfiguracion.size() > 0) {

                    configuracionProyecto = new Gson().toJson(listaConfiguracion);
                }

                res = "{";
                res += " \"configuracionProyecto\"  : " + configuracionProyecto;
                res += "}";

            } catch (Exception e) {
                logger.info("AP Al obtener la lista de configuracionProyecto");
                return "{}";
            }

        } catch (Exception e) {
            logger.info("AP con los parametros de obtener configuracionProyecto");
            return "{}";
        }
        return res;
    }

    // ********* WAF *********
    // http://localhost:8080/checklist/servicios/getActorHallazgo.json?idUsuario=<?>
    // 10.51.218.154:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getActorHallazgo.json?idUsuario=370566
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getActorHallazgoCore", method = RequestMethod.GET)
    public @ResponseBody
    String getActorHallazgo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = "";
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String actoresHallazgos = "[ ]";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);

            try {

                List<ActorEdoHallaDTO> listaActorHallazgo = actorEdoHallaBI.obtieneDatosActorHalla(null);

                if (listaActorHallazgo != null && listaActorHallazgo.size() > 0) {

                    actoresHallazgos = new Gson().toJson(listaActorHallazgo);
                }

                res = "{";
                res += " \"actoresHallazgos\"  : " + actoresHallazgos;
                res += "}";

            } catch (Exception e) {
                logger.info("AP Al obtener la lista de actoresHallazgos");
                return "{}";
            }

        } catch (Exception e) {
            logger.info("AP con los parametros de obtener actoresHallazgos");
            return "{}";
        }
        return res;
    }

    // ********* WAF *********
    // http://localhost:8080/checklist/servicios/getGpoEdoHallazgo.json?idUsuario=<?>
    // 10.51.218.154:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getGpoEdoHallazgo.json?idUsuario=370566&idConfig=
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getGpoEdoHallazgo", method = RequestMethod.GET)
    public @ResponseBody
    String getGpoEdoHallazgo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = "";
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String estadosHallazgo = "[ ]";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);

            String idConfig = null;
            idConfig = urides.split("&")[1].split("=")[1];

            try {

                List<ActorEdoHallaDTO> listaEdoHallazgo = actorEdoHallaBI.obtieneDatosConfEdoHalla(null);

                if (listaEdoHallazgo != null && listaEdoHallazgo.size() > 0) {

                    estadosHallazgo = new Gson().toJson(listaEdoHallazgo);
                }

                res = "{";
                res += " \"estadosHallazgo\"  : " + estadosHallazgo;
                res += "}";

            } catch (Exception e) {
                logger.info("AP Al obtener la lista de estadosHallazgo");
                return "{}";
            }

        } catch (Exception e) {
            logger.info("AP con los parametros de obtener estadosHallazgo");
            return "{}";
        }
        return res;
    }

    // ********* WAF *********
    // http://localhost:8080/checklist/servicios/getConfigActor.json?idUsuario=<?>
    // 10.51.218.154:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getConfigActor.json?idUsuario=370566&idActor
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getConfigActor", method = RequestMethod.GET)
    public @ResponseBody
    String getConfigActor(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String res = "";
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String configActor = "[ ]";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            String idActor = urides.split("&")[0].split("=")[1];

            try {

                List<ConfiguraActorDTO> listaConfigActor = configuraActorBI.obtieneDatosConfActor(null);

                if (listaConfigActor != null && listaConfigActor.size() > 0) {

                    configActor = new Gson().toJson(listaConfigActor);
                }

                res = "{";
                res += " \"configActor\"  : " + configActor;
                res += "}";

            } catch (Exception e) {
                logger.info("AP Al obtener la lista de configActor");
                return "{}";
            }

        } catch (Exception e) {
            logger.info("AP con los parametros de obtener configActor");
            return "{}";
        }
        return res;
    }

    // ********* WAF *********
    // getGpoEdoHallazgo -- MOMENTANEO
    // http://localhost:8080/checklist/servicios/postAltaFirmaCheck.json?idUsuario=<?>
    // http://localhost:8080/checklist/servicios/getGpoEdoHallazgo.json?idUsuario=<?>
    // postAltaFirmaCore -- ORIGINAL
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getGpoEdoHallazgo", method = RequestMethod.POST)
    public @ResponseBody
    String postAltaFirmaCore(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        String res = null;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // logger.info("URIDES IOS: " + urides);
                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                // logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());
                // logger.info("JSON IOS: " + contenido);

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }

            if (contenido != null) {

                contenido = contenido.replaceAll("\\n", "");
                contenido = contenido.replaceAll("\\r", "");

                JSONArray jsonArray = new JSONArray(contenido);

                // **************************************************************************
                FirmaExpansionCoreBI firmaHilos[] = new FirmaExpansionCoreBI[jsonArray.length()];

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject rec = jsonArray.getJSONObject(i);

                    FirmaCheckDTO firm = new FirmaCheckDTO();

                    firm.setBitacora(rec.getString("bitacora"));
                    firm.setCeco(rec.getString("ceco"));
                    firm.setPuesto(rec.getString("puesto"));
                    firm.setResponsable(rec.getString("responsable"));
                    firm.setCorreo(rec.getString("correo"));
                    firm.setNombre(i + "");
                    firm.setRuta(rec.getString("ruta"));
                    firm.setIdFase(rec.getInt("idFase"));
                    firm.setIdProyecto(rec.getInt("idProyecto"));

                    int idAgrupa = 0;

                    if (request.getParameter("idAgrupador") != null
                            && request.getParameter("idAgrupador").length() <= 0) {

                        idAgrupa = 0;

                    } else {

                        idAgrupa = rec.getInt("idAgrupador");
                    }

                    firm.setIdAgrupa(idAgrupa);

                    firmaHilos[i] = new FirmaExpansionCoreBI(firm.getBitacora(), firm.getCeco(), firm.getPuesto(),
                            firm.getResponsable(), firm.getCorreo(), firm.getNombre(), firm.getRuta(), firm.getIdFase(),
                            firm.getIdProyecto());

                }

                for (int j = 0; j < firmaHilos.length; j++) {
                    firmaHilos[j].start();
                    Thread.sleep(1000);
                }

                boolean terminaron = false;
                boolean fallo = false;

                long startTime1 = System.currentTimeMillis();
                while (!terminaron) {
                    for (int j = 0; j < firmaHilos.length; j++) {

                        logger.info("getRespuesta en Hilos: " + j);

                        if (firmaHilos[j].getRespuesta() == 1) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            fallo = true;
                            break;
                        }

                    }

                    for (int j = 0; j < firmaHilos.length; j++) {

                        logger.info("TIEMPO RESP en Hilos: " + j);

                        if (!firmaHilos[j].isAlive()) {
                            terminaron = true;
                        } else {
                            terminaron = false;
                            break;
                        }
                    }

                }

                long endTime1 = System.currentTimeMillis() - startTime1;
                logger.info("TIEMPO RESP en Hilos: " + endTime1);

                // **************************************************************************
                JsonObject envoltorioJsonObj = new JsonObject();
                if (fallo) {
                    envoltorioJsonObj.addProperty("response", false);
                } else {
                    envoltorioJsonObj.addProperty("response", true);
                }

                logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
                res = envoltorioJsonObj.toString();

            }

        } catch (Exception e) {
            logger.info("postAltaFirmaCheckArray.json e " + e);
            res = null;
        }

        if (res != null) {
            return res.toString();
        } else {
            JsonObject envoltorioJsonObj = new JsonObject();
            envoltorioJsonObj.addProperty("response", false);
            return envoltorioJsonObj.toString();
        }

    }

    // ********* WAF *********
    // http://localhost:8080/checklist/servicios/sendCorreoAperturaCore.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/sendCorreoAperturaCore", method = RequestMethod.POST)
    public @ResponseBody
    Boolean sendCorreoAperturaCore(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws UnsupportedEncodingException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }

            /*
			 * contenido = "{ " + "\"tipoVisita\":\"Primer Recorrido - Soft Opening \","+
			 * "\"fecha\":\"4 Juio 2019 \","+ "\"ceco\":\"9995 \","+
			 * "\"fase\":\"480100 \","+ "\"proyecto\":\"9995 \","+ "\"eco\":\"9995 \","+
			 * "\"sucursal\":\"MEGA AV BELEN QRO \"," +
			 * "\"territorio\":\"TERRITORIO SB CENTRO PONIENTE\"," + "\"tipo\":\"Nueva\"," +
			 * "\"zona\":\"ZONA SB QUERETARO BAJIO\"," +
			 * "\"region\":\"REGIONAL SB QUERETARO\","+ "\"imperdonable\":["+ "{"+
			 * "\"descripcion\":\"Esto es una Imperdonable nuevo 1 \""+ "}"+
			 *
			 * ","+
			 *
			 * "{"+ "\"descripcion\":\"Esto es una Imperdonable nuevo 2 \""+ "}"+
			 *
			 * ","+
			 *
			 * "{"+ "\"descripcion\":\"Esto es una Imperdonable nuevo 3 \""+ "}"+ "],"+
			 * "\"idBitacora\":"+ "20878" + ","+ "\"aperturbale\":\"false\""+ "}";
             */
            // Datos
            String economico = "";
            String sucursal = "";
            String tipo = "";
            String territorio = "";
            String zona = "";
            String aperturable = "";
            int idBitacora = 0;

            String tipoVisita = "";
            String fechaVisita = "";
            String tipoNegocio = "";

            int ceco = 0;
            int fase = 0;
            int proyecto = 0;

            // Json
            List<String> imperdonablesLista = new ArrayList<String>();

            String rutaImg = "/franquicia/firmaChecklist/firmalogo_fachada.png";

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);

                economico = rec.getString("eco");
                sucursal = rec.getString("sucursal");
                territorio = rec.getString("territorio");
                tipo = rec.getString("tipo");
                zona = rec.getString("zona");
                aperturable = rec.getString("aperturbale");
                idBitacora = rec.getInt("idBitacora");

                tipoVisita = rec.getString("tipoVisita");
                fechaVisita = rec.getString("fecha");
                tipoNegocio = rec.getString("tipoNegocio");

                ceco = rec.getInt("ceco");
                fase = rec.getInt("idFase");
                proyecto = rec.getInt("idProyecto");

                JSONArray results = rec.getJSONArray("imperdonable");

                for (int i = 0; i < results.length(); i++) {
                    JSONObject data = results.getJSONObject(i);
                    String result = data.getString("descripcion");
                    imperdonablesLista.add(result);
                }

                // Firmas ***NUEVO***
                List<FirmaCheckDTO> firmasB = new ArrayList<>();
                firmasB = firmaCheckBI.obtieneDatosFirmaCecoFaseProyecto(ceco + "", fase + "", proyecto + "");

                // Respuestas NO ***NUEVO***
                List<ReporteChecksExpDTO> incidencias = new ArrayList<>();
                incidencias = reporteChecksBI.obtienePregNoImpCecoFaseProyecto(ceco + "", fase + "", proyecto + "");

                if (firmasB.get(0).getCeco() != null && firmasB.get(0).getCeco() != "") {
                    try {
                        List<SucursalChecklistDTO> lista = sucursalChecklistBI
                                .obtieneDatos(Integer.parseInt(firmasB.get(0).getCeco()));

                        if (lista != null) {
                            for (SucursalChecklistDTO sucursalChecklistDTO : lista) {
                                if (sucursalChecklistDTO != null && sucursalChecklistDTO.getRuta() != null
                                        && !sucursalChecklistDTO.getRuta().equals("")) {
                                    rutaImg = sucursalChecklistDTO.getRuta();
                                    // break;
                                }
                            }
                        }

                    } catch (Exception e) {
                        logger.info("SIN IMAGEN DE FACHADA DE SUCURSALES ");
                    }

                }

                try {

                    // ***NUEVO***
                    correoBI.sendMailExpansionNuevoFormatoConteoCore(idBitacora + "", tipoVisita, fechaVisita,
                            economico, sucursal, tipo, territorio, zona, imperdonablesLista, firmasB, incidencias,
                            aperturable, rutaImg, tipoNegocio, ceco, fase, proyecto);

                } catch (Exception e) {
                    logger.info(e);
                    logger.info("AP AL ENVIAR EL EXPANSION ANTERIOR" + e.getStackTrace());
                    return false;
                }

            }
        } catch (Exception e) {
            logger.info(e);
            return false;
        }

        logger.info("SALI sendCorreoApertura");
        return true;
    }

    // NUEVO PROCESO INFRAESTRUCTURA
    // http://localhost:8080/checklist/servicios/getInfoRecorridos.json?idCeco=<?>&nombreCeco=<?>&tipoRecorrido=<?>&fechaInicio=<?>&fechaFin=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getInfoRecorridos", method = {RequestMethod.GET})
    public @ResponseBody
    List<RepoFilExpDTO> getInfoRecorridos(HttpServletRequest request,
            HttpServletResponse response) throws UnsupportedEncodingException {

        List<RepoFilExpDTO> res = new ArrayList<RepoFilExpDTO>();

        String idCeco = request.getParameter("idCeco");
        String nombreCeco = request.getParameter("nombreCeco");
        String tipoRecorrido = request.getParameter("tipoRecorrido");
        String fechaIni = request.getParameter("fechaInicio");
        String fechaFin = request.getParameter("fechaFin");

        String fechaInicioParam = "";
        String fechaFinParam = "";
        DecimalFormat formatDecimales = new DecimalFormat("###.##");
        try {

            // Formato Fecha (aaaammdd)
            res = repoFilExpBI.obtieneDatosFiltro1FaseResult(idCeco, tipoRecorrido, "", nombreCeco, tipoRecorrido,
                    fechaIni, fechaFin);
        } catch (Exception e) {
            logger.info("AP al obtener la información de los recorridos");
            logger.info("Mensaje " + e);
        }

        return res;
    }

    // http://10.51.218.72:8080/checklist/expansionCoreServices/getTipoRecorrido.json?ceco=
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getTipoRecorrido", method = RequestMethod.GET)
    public @ResponseBody
    List<SoftNvoDTO> getTipoRecorrido(HttpServletRequest request,
            HttpServletResponse response) throws UnsupportedEncodingException {

        List<SoftNvoDTO> lista = new ArrayList<SoftNvoDTO>();
        String ceco = null;
        try {
            ceco = request.getParameter("ceco");
            lista = softNvoBI.obtieneDatosSoftN(ceco);
        } catch (Exception e) {
            logger.info("Error: " + e.getMessage());
            lista = null;
        }
        return lista;
    }

    // http://10.51.218.72:8080/checklist/expansionCoreServices/generaHallazgos.json?ceco=480100&proyecto=3&fase=6
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/generaHallazgos", method = RequestMethod.GET)
    public Boolean generaHallazgos(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "ceco", required = true, defaultValue = "") String ceco,
            @RequestParam(value = "proyecto", required = true, defaultValue = "") String proyecto,
            @RequestParam(value = "fase", required = true, defaultValue = "") String fase)
            throws ServletException, IOException {

        try {
            List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();
            List<HallazgosTransfDTO> listaEvidencias = new ArrayList<HallazgosTransfDTO>();
            listaHallazgos.clear();
            listaEvidencias.clear();

            listaHallazgos = hallazgosTransfBI.obtieneHallazgosExpansionIni(ceco, proyecto, fase);

            GeneraExcelDetalleExpansionBI ge = new GeneraExcelDetalleExpansionBI();
            Workbook hallazgosWorkbook;
            hallazgosWorkbook = ge.generaHallazgosCore(fase, listaHallazgos, listaEvidencias, ceco,
                    sucursalChecklistBI);

            // Write the output to a file
            ByteArrayOutputStream arrayBytesOutStream = new ByteArrayOutputStream();
            hallazgosWorkbook.write(arrayBytesOutStream);

            ServletOutputStream sos = null;
            String archivo = "";

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition",
                    "attachment; filename=Hallazgos_Ceco_" + ceco + "_fase_" + fase + "_" + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();

            sos.write(arrayBytesOutStream.toByteArray());
            sos.flush();
            sos.close();

            // Closing the workbook
            hallazgosWorkbook.close();

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    // http://10.51.218.72:8080/checklist/expansionCoreServices/generaHistorialHallazgos.json?ceco=480100&proyecto=3&fase=6
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/generaHistorialHallazgos", method = RequestMethod.GET)
    public Boolean generaHistorialHallazgos(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "ceco", required = true, defaultValue = "") String ceco,
            @RequestParam(value = "proyecto", required = true, defaultValue = "") String proyecto,
            @RequestParam(value = "fase", required = true, defaultValue = "") String fase)
            throws ServletException, IOException {

        try {
            List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();
            List<HallazgosTransfDTO> listaHallazgos2 = new ArrayList<HallazgosTransfDTO>();
            listaHallazgos.clear();

            listaHallazgos = hallazgosTransfBI.obtieneHallazgosExpansionIni(ceco, proyecto, fase);
            listaHallazgos2 = hallazgosTransfBI.obtieneHallazgosExpansion(ceco, proyecto, fase);

            GeneraExcelDetalleExpansionBI ge = new GeneraExcelDetalleExpansionBI();
            Workbook hallazgosWorkbook;
            hallazgosWorkbook = ge.generaHistorialHallazgosCore(fase, listaHallazgos, listaHallazgos2, ceco,
                    sucursalChecklistBI);

            // Write the output to a file
            ByteArrayOutputStream arrayBytesOutStream = new ByteArrayOutputStream();
            hallazgosWorkbook.write(arrayBytesOutStream);

            ServletOutputStream sos = null;
            String archivo = "";

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition",
                    "attachment; filename=Hallazgos_Ceco_" + ceco + "_fase_" + fase + "_" + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();

            sos.write(arrayBytesOutStream.toByteArray());
            sos.flush();
            sos.close();

            // Closing the workbook
            hallazgosWorkbook.close();

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    // http://10.51.218.72:8080/checklist/expansionCoreServices/generaDashboard.json?ceco=480100&proyecto=3&fase=6
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/generaDashboard", method = RequestMethod.GET)
    public Boolean generaDashboard(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "ceco", required = true, defaultValue = "") String ceco,
            @RequestParam(value = "proyecto", required = true, defaultValue = "") String proyecto,
            @RequestParam(value = "fase", required = true, defaultValue = "") String fase)
            throws ServletException, IOException {

        try {
            List<RepoFilExpDTO> lista = null;
            lista = repoFilExpBI.obtienedashCecoProyecto(ceco, fase, proyecto);

            GeneraExcelDetalleExpansionBI ge = new GeneraExcelDetalleExpansionBI();
            Workbook hallazgosWorkbook;
            hallazgosWorkbook = ge.generaDashboard(fase, lista, ceco, sucursalChecklistBI);

            // Write the output to a file
            ByteArrayOutputStream arrayBytesOutStream = new ByteArrayOutputStream();
            hallazgosWorkbook.write(arrayBytesOutStream);

            ServletOutputStream sos = null;
            String archivo = "";

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition",
                    "attachment; filename=Dashboard_Ceco_" + ceco + "_fase_" + fase + "_" + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();

            sos.write(arrayBytesOutStream.toByteArray());
            sos.flush();
            sos.close();

            // Closing the workbook
            hallazgosWorkbook.close();

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    // http://10.51.218.72:8080/checklist/expansionCoreServices/getHallazgosExpansionCore.json?ceco=480100&proyecto=3&fase=6
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getHallazgosExpansionCore", method = RequestMethod.GET)
    public @ResponseBody
    List<HallazgosTransfDTO> getHallazgosExpansionCore(HttpServletRequest request,
            HttpServletResponse response, @RequestParam(value = "ceco", required = true, defaultValue = "") String ceco,
            @RequestParam(value = "proyecto", required = true, defaultValue = "") String proyecto,
            @RequestParam(value = "fase", required = true, defaultValue = "") String fase)
            throws ServletException, IOException {

        List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();

        try {
            listaHallazgos.clear();

            // listaHallazgos = hallazgosTransfBI.obtieneHallazgosExpansionIni(ceco,
            // proyecto, fase);
            listaHallazgos = hallazgosTransfBI.obtieneHallazgosExpansion(ceco, proyecto, fase);

            logger.info(listaHallazgos.size());

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return listaHallazgos;

        }
        return listaHallazgos;
    }

    // http://10.51.218.104:8080/checklist/expansionCoreServices/getDetalleHallazgoCecoFaseProyectoId.json?idCeco=480100&idProyecto=3&idFase=5&idHallazgo=2504
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDetalleHallazgoCecoFaseProyectoId", method = RequestMethod.GET)
    public @ResponseBody
    HashMap<String, Object> getDetalleHallazgoCecoFaseProyectoId(HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
            @RequestParam(value = "idProyecto", required = true, defaultValue = "") String idProyecto,
            @RequestParam(value = "idFase", required = true, defaultValue = "") String idFase,
            @RequestParam(value = "idHallazgo", required = true, defaultValue = "") String idHallazgo)
            throws ServletException, IOException {

        HashMap<String, Object> result = new HashMap<String, Object>();

        List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();
        List<HallazgosTransfDTO> listaHallazgosResult = new ArrayList<HallazgosTransfDTO>();
        List<HallazgosTransfDTO> listaEvidencias = new ArrayList<HallazgosTransfDTO>();

        HallazgosTransfDTO objPreg = new HallazgosTransfDTO();
        int tipoArchivo = 21;

        try {
            // Se filtra el tipo de proyecto internamente (Para expansión trae todo lo que
            // no sea idFolio == 0 y Transformación trae todos los NO)
            listaHallazgos = hallazgosTransfBI.obtieneDatos(idCeco, "" + idProyecto, "" + idFase);
            listaEvidencias = hallazgosTransfBI.obtieneDatosBita(Integer.parseInt(idProyecto), Integer.parseInt(idFase),
                    Integer.parseInt(idCeco));

            // Se obtiene el hallazgo
            objPreg = listaHallazgos.stream()
                    .filter(hallazgo -> (hallazgo.getIdHallazgo() == Integer.parseInt(idHallazgo))).findAny()
                    .orElse(null);

            listaHallazgosResult.add(objPreg);

            // se obtiene el tipo de evidencia
            listaEvidencias = listaEvidencias.stream()
                    .filter(evi -> (evi.getIdHallazgo() == Integer.parseInt(idHallazgo))).collect(Collectors.toList());

            HallazgosTransfDTO objEvidencia = listaEvidencias.stream()
                    .filter(hallazgo -> (hallazgo.getIdHallazgo() == Integer.parseInt(idHallazgo))).findAny()
                    .orElse(null);

            if (objEvidencia != null) {
                tipoArchivo = objEvidencia.getTipoEvi();
            }

            result.put("detalleHallazgo", listaHallazgosResult);
            result.put("detalleEvidencias", listaEvidencias);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.info("AP al obtener la información del hallazgo");
            return result;
        }

        return result;

    }

    // *** WAF ***
    // http://localhost:8080/checklist/servicios/sendCorreoExpansionCoreNotifica.json?idUsuario=<?>&idCeco=<?>&idFase=<?>&idProyecto=<?>&correo=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/sendCorreoExpansionCoreNotifica", method = {RequestMethod.POST})
    public @ResponseBody
    boolean sendCorreoExpansionCoreNotifica(@RequestBody String provider,
            HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();
        List<HallazgosTransfDTO> listaEvidencias = new ArrayList<HallazgosTransfDTO>();
        boolean banderaCompleto = false;

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uri.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        String contenido = "";
        boolean respuesta = false;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                // logger.info("URIDES IOS: " + urides);

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                // logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                // logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }

            // Json
            try {

                if (contenido != null) {

                    JSONObject rec = new JSONObject(contenido);
                    JSONObject results = rec.getJSONObject("data");

                    int idCeco = results.getInt("idCeco");
                    int idFase = results.getInt("idFase");
                    int idProyecto = results.getInt("idProyecto");

                    if (idCeco != 0 && idFase != 0 && idProyecto != 0) {

                        correoBI.sendMailExpansionCoreNotifica(idCeco, idFase, idProyecto);

                        banderaCompleto = true;

                    } else {
                        logger.info("AP ALAGUN VALOR == 0");

                        banderaCompleto = true;

                    }
                }
            } catch (Exception e) {
                logger.info("AP AL OBTENER EL JSON CORREO HALLAZGOS EXPANSION NOTIFICACION " + e);
                return false;
            }
        } catch (Exception e) {
            logger.info("AP AL OBTENER PARAMETROS DE REQUEST CORREO EXPANSION NOTIFICACION" + e);
            return false;
        }

        return banderaCompleto;
    }

    // NUEVO PROCESO INFRAESTRUCTURA
    // ------------------------------------------------------------------------ FIN
    // SERVICIOS CORE
    // EXPANSION----------------------------------------------------------------------
    // http://localhost:8080/checklist/expansionCoreServices/updateHallazgoExpansionDirecto.json?idUsuario=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/updateHallazgoExpansionDirecto", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    boolean updateHallazgoExpansionDirecto(@RequestBody String provider,
            HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {

        logger.info(provider);
        String contenido = provider;

        try {

            if (contenido != null) {

                JSONObject rec = new JSONObject(contenido);

                JSONArray results = rec.getJSONArray("hallazgos");

                boolean banderaCompleto = true;
                boolean banderaResponse = true;

                for (int i = 0; i < results.length(); i++) {

                    JSONObject data = results.getJSONObject(i);
                    HallazgosTransfDTO objHallazgo = new HallazgosTransfDTO();

                    int idHallazgo = 0;
                    int idFolio = 0;
                    int idResp = 0;
                    int status = 0;
                    String respuesta = "";
                    String ruta = "";
                    String obs = "";
                    String obsNueva = "";
                    String fechaAut = "";
                    String fechafin = "";
                    String usuModif = "";
                    String motiRecha = "";
                    int tipoEvidencia = 0;

                    int idCeco = 0;
                    int idProyecto = 0;
                    int idFase = 0;

                    idHallazgo = data.getInt("idHallazgo");
                    idFolio = data.getInt("idFolio");
                    idResp = data.getInt("idResp");
                    status = data.getInt("status");
                    respuesta = data.getString("respuesta");
                    ruta = data.getString("ruta");
                    obs = data.getString("obs");
                    obsNueva = data.getString("obsNueva");
                    fechaAut = data.getString("fechaAut");
                    fechafin = data.getString("fechaFin");
                    usuModif = data.getString("usuModif");
                    motiRecha = data.getString("motivoRechazo");
                    tipoEvidencia = data.getInt("tipoEvidencia");

                    idCeco = data.getInt("idCeco");
                    idProyecto = data.getInt("idProyecto");
                    idFase = data.getInt("idFase");

                    objHallazgo.setIdHallazgo(idHallazgo);
                    objHallazgo.setIdFolio(idFolio);
                    objHallazgo.setIdResp(idResp);
                    objHallazgo.setStatus(status);
                    objHallazgo.setRespuesta(respuesta);
                    objHallazgo.setRuta(ruta);
                    objHallazgo.setObs(obs);
                    objHallazgo.setObsNueva(obsNueva);
                    objHallazgo.setFechaAutorizo(fechaAut);
                    objHallazgo.setFechaFin(fechafin);
                    objHallazgo.setUsuModif(usuModif);
                    objHallazgo.setMotivrechazo(motiRecha);
                    objHallazgo.setTipoEvi(tipoEvidencia);

                    Map<String, Object> resp = hallazgosTransfBI.actualizaMov(objHallazgo);

                    int banderaRespuesta = (int) resp.get("response");
                    String banderaCierreHallazgos = (String) resp.get("validaCompleto");

                    if (banderaRespuesta == 0) {
                        logger.info("No fue posible ACTUALIZAR el HALLAZGO " + idHallazgo);
                        banderaResponse = false;
                        banderaCompleto = false;
                    } else {
                        logger.info("HALLAZGO: " + idHallazgo + " ACTUALIZADO ");
                        banderaResponse = true;
                    }

                    if (banderaRespuesta == 1 && banderaCierreHallazgos.compareToIgnoreCase("true") == 0) {
                        banderaCompleto = true;
                        // Se hace el envío del correo
                    }

                }

                if (banderaResponse) {
                    logger.info("ACTUALIZACION COMPLETA");
                    return banderaResponse;

                } else {
                    logger.info("ACTUALIZACION IN-COMPLETA");
                    return banderaResponse;
                }
            }
        } catch (Exception e) {
            logger.info("AP AL OBTENER EL JSON DE HALLAZGOS A ACTUALIZAR " + e);
            return false;
        }

        return false;
    }

    // http://localhost:8080/checklist/expansionCoreServices/actualizaHallazgoImagenCore.json?idUsuario=<?>
    @RequestMapping(value = {"/actualizaHallazgoImagenCore"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    boolean actualizaHallazgoImagenCore(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws UnsupportedEncodingException {

        boolean res = false;

        try {

            String nomArchivo = "";
            String archivo = "";
            String contenido = provider;

            if (contenido != null) {
                JSONObject rec = new JSONObject(contenido);
                nomArchivo = rec.getString("nomArchivo");
                archivo = rec.getString("archivo");
            }
            // System.out.println("-------------------Entro a guardar evidencia checklist
            // 3-------------------------- "+nomArchivo);

            Date fechaActual = new Date();
            // System.out.println(fechaActual);
            // System.out.println("-------------------Entro a guardar evidencia checklist
            // 4--------------------------");
            // Formateando la fecha:
            DateFormat formatoAnio = new SimpleDateFormat("yyyy");
            String anio = formatoAnio.format(fechaActual);
            File r2 = null;
            String rootPath = File.listRoots()[0].getAbsolutePath();
            String rutaArchivo = "/franquicia/imagenes/" + anio + "/";

            String rutaPATHArchivo = rootPath + rutaArchivo;
            r2 = new File(rutaPATHArchivo);

            if (r2.mkdirs()) {
                logger.info("SE HA CREADA LA CARPETA");
            } else {
                logger.info("EL DIRECTORIO YA EXISTE");
            }

            BASE64Decoder decoder = new BASE64Decoder();
            byte[] archivoDecodificada = decoder.decodeBuffer(archivo);

            String rutaTotalF = rutaPATHArchivo + nomArchivo;
            // System.out.println("-------------------Entro a guardar evidencia checklist
            // 5--------------------------" + rutaTotalF);
            FileOutputStream fileOutputF = new FileOutputStream(rutaTotalF);
            // System.out.println("-------------------Entro a guardar evidencia checklist
            // 6-------------------------- " + fileOutputF);
            int alta = 0;
            try {
                BufferedOutputStream bufferOutputF = new BufferedOutputStream(fileOutputF);
                bufferOutputF.write(archivoDecodificada);
                bufferOutputF.close();
                alta = 1;
                logger.info("SE HA GUARDADO LA IMAGEN EN: " + rutaTotalF);
            } catch (Exception ex) {
                alta = 0;
                logger.info("Ocurrio algo: ");
            } finally {
                fileOutputF.close();
            }

            JsonObject envoltorioJsonObj = new JsonObject();
            envoltorioJsonObj.addProperty("ALTA", alta);
            logger.info("JSON ID FOLIO: " + envoltorioJsonObj.toString());
            String result = envoltorioJsonObj.toString();
            logger.info(result);

            if (alta == 1) {
                logger.info("Entro a guardar evidencia checklist");
                // 7-------------------------- " + res);
                return true;

            } else {
                return false;
            }

        } catch (Exception e) {
            logger.info("AP al actualizar el hallazgo " + e);
        }
        return res;

    }

    // http://localhost:8080/checklist/expansionCoreServices/getHistorialHallazgoCore.json?idHallazgo=<?>
    @RequestMapping(value = {"/getHistorialHallazgoCore"}, method = {RequestMethod.GET, RequestMethod.GET})
    public @ResponseBody
    List<HallazgosTransfDTO> getHistorialHallazgoCore(
            HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "idHallazgo", required = true, defaultValue = "") String idHallazgo)
            throws UnsupportedEncodingException {

        List<HallazgosTransfDTO> lista = new ArrayList<HallazgosTransfDTO>();

        try {

            int idHallazgoLocal = Integer.parseInt(idHallazgo);
            lista = hallazgosTransfBI.obtieneHistFolio(idHallazgoLocal);

            if (lista != null && lista.size() > 0) {
                logger.info("La lista de historial de hallazgos es correcta");
                return lista;

            } else {
                logger.info("La lista de historial de hallazgos es 0 ó nula" + idHallazgoLocal);

            }

        } catch (Exception e) {

            logger.info("AP al obtener el historial del hallazgo " + e);
            return lista;
        }

        return lista;
    }

    // http://10.51.218.72:8080/checklist/expansionCoreServices/generaAdicionales.json?ceco=480100&proyecto=3&fase=6
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/generaAdicionales", method = RequestMethod.GET)
    public Boolean generaAdicionales(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "ceco", required = true, defaultValue = "") String ceco,
            @RequestParam(value = "proyecto", required = true, defaultValue = "") String proyecto,
            @RequestParam(value = "fase", required = true, defaultValue = "") String fase)
            throws ServletException, IOException {

        try {
            List<HallazgosTransfDTO> listaAdicionales = new ArrayList<HallazgosTransfDTO>();
            List<HallazgosTransfDTO> listaEvidencias = new ArrayList<HallazgosTransfDTO>();
            listaAdicionales.clear();
            listaEvidencias.clear();

            listaAdicionales = soporteHallazgosTransfBI.obtieneAdicionalesCecoFaseProyecto(ceco, fase, proyecto, 0);

            GeneraExcelDetalleExpansionBI ge = new GeneraExcelDetalleExpansionBI();
            Workbook hallazgosWorkbook;
            hallazgosWorkbook = ge.generaAdicionalesCore(fase, listaAdicionales, listaEvidencias, ceco,
                    sucursalChecklistBI);

            // Write the output to a file
            ByteArrayOutputStream arrayBytesOutStream = new ByteArrayOutputStream();
            hallazgosWorkbook.write(arrayBytesOutStream);

            ServletOutputStream sos = null;
            String archivo = "";

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition",
                    "attachment; filename=Adicionales_Ceco_" + ceco + "_fase_" + fase + "_" + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();

            sos.write(arrayBytesOutStream.toByteArray());
            sos.flush();
            sos.close();

            // Closing the workbook
            hallazgosWorkbook.close();

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    // http://10.51.218.72:8080/checklist/expansionCoreServices/getAdicionalesCore.json?ceco=480100&proyecto=3&fase=6
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getAdicionalesCore", method = RequestMethod.GET)
    public @ResponseBody
    List<HallazgosTransfDTO> getAdicionalesCore(HttpServletRequest request,
            HttpServletResponse response, @RequestParam(value = "ceco", required = true, defaultValue = "") String ceco,
            @RequestParam(value = "proyecto", required = true, defaultValue = "") String proyecto,
            @RequestParam(value = "fase", required = true, defaultValue = "") String fase)
            throws ServletException, IOException {

        List<HallazgosTransfDTO> listaAdicionales = new ArrayList<HallazgosTransfDTO>();

        try {
            listaAdicionales.clear();

            listaAdicionales = soporteHallazgosTransfBI.obtieneAdicionalesCecoFaseProyecto(ceco, fase, proyecto, 1);

            return listaAdicionales;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return listaAdicionales;
        }
    }

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getConteosCecos", method = RequestMethod.GET)
    public @ResponseBody
    String getConteosCecos(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "negocio", required = true, defaultValue = "") String negocio,
            @RequestParam(value = "fechaInicio", required = true, defaultValue = "") String fechaInicio,
            @RequestParam(value = "fechaFin", required = true, defaultValue = "") String fechaFin)
            throws ServletException, IOException {

        JsonObject jsonRespuesta = new JsonObject();
        JsonArray arrayConteosCecos = null;

        try {
            arrayConteosCecos = reporteHallazgosBi.obtieneConteosCeco(negocio, fechaInicio, fechaFin);

            jsonRespuesta.add("conteos_by_ceco", arrayConteosCecos);

        } catch (Exception e) {

            jsonRespuesta.add("conteos_by_ceco", null);
            e.printStackTrace();

        }

        return jsonRespuesta.toString();

    }

    @RequestMapping(value = "/getConteoHallazgosFase", method = RequestMethod.GET)
    public @ResponseBody
    String getConteoHallazgosFase(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "ceco", required = true, defaultValue = "") String ceco,
            @RequestParam(value = "fechaInicio", required = true, defaultValue = "") String fechaInicio,
            @RequestParam(value = "fechaFin", required = true, defaultValue = "") String fechaFin) {

        JsonObject jsonRespuesta = new JsonObject();
        JsonArray arrayConteosFases = null;

        try {

            arrayConteosFases = reporteHallazgosBi.obtieneConteoFases(ceco, fechaInicio, fechaFin);
            jsonRespuesta.add("conteos_by_fase", arrayConteosFases);

        } catch (Exception e) {

            jsonRespuesta.add("conteos_by_fase", null);
            e.printStackTrace();
        }

        return jsonRespuesta.toString();

    }

    // http://localhost:8080/checklist/consultaCatalogosService/getConteoHallazgosGral.json?nego=&fechaInicio=&fechaFin=AAAAMMDD
    @RequestMapping(value = "/getConteoHallazgosGral", method = RequestMethod.GET)
    public @ResponseBody
    String getConteoHallazgosGral(HttpServletRequest request, HttpServletResponse response) {
        try {

            String res = "";
            String nego = request.getParameter("nego");
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");

            try {

                res = reporteHallazgosBI.conteoGralHallazgos(nego, fechaInicio, fechaFin);

            } catch (Exception e) {

                e.printStackTrace();

                return null;
            }

            return res;

        } catch (Exception e) {
            logger.info("ConteoHallazgos GRAL");
            return null;
        }
    }

    // ************** WAF **************
    // **** INICIO PENDIENTE DE API ****
    // SE REUTILIZA WAF PARA SERVICIO DE SUPERVISIÓN MANTENIMIENTO (AUTOASIGNACIÓN)
    // http://localhost:8080/checklist/servicios/postAltaFirmaCheck.json?idUsuario=<?>&tipoServicio=<?>
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/postAltaFirmaCheck", method = RequestMethod.POST)
    public @ResponseBody
    String postAltaFirmaCheck(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        String json = "";
        String res = null;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        try {
            logger.info("PROVIDER");
            logger.info(provider);
            logger.info("REQUEST");
            logger.info(request);
            logger.info("REQUEST QUERY STRING");
            logger.info(request.getQueryString());
            logger.info("REQUEST TOKEN");
            logger.info(uriAp);

            if (idLlave == 7) {

                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

            } else if (idLlave == 666) {

                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
            }
            logger.info("***urides: " + urides + "****");
            logger.info("**contenido : " + contenido + "****");

            String idUsuario = (urides.split("&")[0]).split("=")[1];
            String tipo = (urides.split("&")[1]).split("=")[1];

            logger.info("***SERVICIO EJECUTADO TIPO : " + tipo + "****");
            logger.info("***DATA DESECRIPTADA " + urides + "****");

            String responseTipo = "";

            if (contenido != null) {

                switch (tipo) {
                    // Obtener programacion de usuario
                    case "1":

                        responseTipo = getProgramacion(idUsuario);

                        return responseTipo;

                    // Realizar reprogramacion
                    case "2":

                        responseTipo = setProgramacion(idUsuario, contenido);

                        return responseTipo;

                    case "3":

                        responseTipo = getPuntosCercanos(contenido);

                        return responseTipo;

                    case "4":

                        responseTipo = getSucursales(contenido);

                        return responseTipo;

                    case "5":

                        logger.info("***SERVICIO EJECUTADO****");

                        if (getInformacionDepositada(contenido)) {

                            return "{\"response\": true}";

                        } else {

                            return "{\"response\": false}";

                        }

                    default:
                        break;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("AP General " + e);
            return "{}";

        }

        return json;

    }

    public String getProgramacion(String idUsuario) {

        String respuesta = "";

        List<ProgramacionMttoDTO> lista = new ArrayList<>();

        try {

            lista = checkInAsistenciaBI.obtieneProgramaciones(idUsuario);

            JSONObject objetoResponse = new JSONObject();

            JSONArray dataArray = new JSONArray();

            for (ProgramacionMttoDTO programacionMttoDTO : lista) {

                JSONObject objeto = new JSONObject();

                objeto.put("idProgramacion", programacionMttoDTO.getIdProgramacion());
                objeto.put("idCeco", programacionMttoDTO.getIdCeco());
                objeto.put("idProyecto", programacionMttoDTO.getIdProyecto());
                objeto.put("idUsuario", programacionMttoDTO.getIdUsuario());
                objeto.put("status", programacionMttoDTO.getStatus());
                objeto.put("fechaProgramacion", programacionMttoDTO.getFechaProgramacion());
                objeto.put("fechaProgramacionInicial", programacionMttoDTO.getfechaProgramacionInicial());
                objeto.put("comentarioReprogramacion", programacionMttoDTO.getComentarioReprogramacion());
                objeto.put("negocio", programacionMttoDTO.getNegocio());
                objeto.put("tickets", programacionMttoDTO.getTickets());
                objeto.put("tipoVisita", programacionMttoDTO.getTipoVisita());
                objeto.put("cecoSuperior", programacionMttoDTO.getCecoSuperior());
                objeto.put("nombreCeco", programacionMttoDTO.getNombreCeco());
                objeto.put("nombreProyecto", programacionMttoDTO.getNombreProyecto());

                dataArray.put(objeto);
            }

            objetoResponse.put("lista", dataArray);
            respuesta = objetoResponse.toString();

            return respuesta;

        } catch (Exception e) {

            e.printStackTrace();
            logger.info("AP al Consultar Programacion" + e);
            return respuesta;

        }

    }

    public String setProgramacion(String usuario, String contenido) {

        List<ProgramacionMttoDTO> lista = new ArrayList<>();

        try {

            String fechaProgramacion = "";
            String idProgramacion = "";

            int idUsuario = Integer.parseInt(usuario);
            String motivo = "Visita Adelantada";

            if (contenido != null) {

                contenido = contenido.replaceAll("\\n", "");
                contenido = contenido.replaceAll("\\r", "");

                JSONObject rec = new JSONObject(contenido);

                // dia mes año ddmmyy
                fechaProgramacion = rec.getString("fechaProgramacion");
                idProgramacion = rec.getString("idProgramacion");

                ProgramacionMttoDTO asist = new ProgramacionMttoDTO();
                asist.setIdUsuario(idUsuario);
                asist.setComentarioReprogramacion(motivo);

                asist.setFechaProgramacion(fechaProgramacion);
                asist.setIdProgramacion(Integer.parseInt(idProgramacion));

                boolean respuesta = checkInAsistenciaBI.actualizaProgramacion(asist);

                if (respuesta) {

                    return "{\"response\": true}";

                } else {

                    return "{\"response\": false}";

                }

            } else {

                return "{\"response\": false}";

            }

        } catch (Exception e) {

            e.printStackTrace();
            logger.info("AP al Programar Asignacion" + e);
            return "{\"response\": false}";

        }

    }

    public String getPuntosCercanos(String contenido) {

        String respuesta = "";

        try {
            contenido = contenido.replaceAll("\\n", "");
            contenido = contenido.replaceAll("\\r", "");

            JSONObject rec = new JSONObject(contenido);

            String latitud = rec.getString("latitud");
            String longitud = rec.getString("longitud");

            respuesta = sucursalesCercanasBI.obtienePuntosCercanos(latitud, longitud);

        } catch (JSONException e) {
            return "{'data' : [],'estatus' : 'FAILURE'}";

        }

        return respuesta;
    }

    public String getSucursales(String contenido) {

        String respuesta = "";
        List<ReporteSupervisionSistemasDTO> listaFiltrosSucursales = new ArrayList<ReporteSupervisionSistemasDTO>();

        try {
            contenido = contenido.replaceAll("\\n", "");
            contenido = contenido.replaceAll("\\r", "");

            JSONObject rec = new JSONObject(contenido);

            String valorIngresado = rec.getString("valorIngresado");

            listaFiltrosSucursales = reporteSupervisionBI.filtroSucursal(valorIngresado);
            JSONObject objetoResponse = new JSONObject();
            JSONArray dataArray = new JSONArray();

            if (!listaFiltrosSucursales.isEmpty()) {

                for (ReporteSupervisionSistemasDTO objetoDTO : listaFiltrosSucursales) {

                    JSONObject objeto = new JSONObject();
                    objeto.put("infoCeco", objetoDTO.getSucursal());
                    dataArray.put(objeto);

                }

                objetoResponse.put("listaSucursales", dataArray);
                respuesta = objetoResponse.toString();

                return respuesta;

            } else {

                return "{'listaSucursales' : []}";

            }

        } catch (JSONException e) {
            return "{'listaSucursales' : []}";

        }

    }

    // http://10.51.218.104:8080/checklist/expansionCoreServices/convierteAdicionalHallazgo.json?idUsuario=191312&idHallazgo=1000
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/convierteAdicionalHallazgo", method = RequestMethod.GET)
    public @ResponseBody
    String convierteAdicionalHallazgo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "idUsuario", required = true, defaultValue = "") String idUsuario,
            @RequestParam(value = "idHallazgo", required = true, defaultValue = "") int idHallazgo) {

        try {

            HallazgosTransfDTO h = new HallazgosTransfDTO();

            h.setIdHallazgo(idHallazgo);

            h.setRespuesta("NO");
            h.setRuta(null);
            h.setObs("Adicional");
            h.setUsuModif(idUsuario);

            boolean res = hallazgosTransfBI.actualizaAdicionalHalla(h);

            if (res) {

                return "{\"response\": true}";

            } else {

                return "{\"response\": false}";

            }

        } catch (Exception e) {
            logger.info("AP al obtener la información del hallazgo");
            return "{\"response\": false}";

        }

    }

    // http://10.51.218.104:8080/checklist/expansionCoreServices/enviaCorreoAtencionHallazgos.json?idUsuario=191312&idCeco=1000&idFase=1000&idProyecto=1000
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/enviaCorreoAtencionHallazgos", method = RequestMethod.GET)
    public @ResponseBody
    String enviaCorreoAtencionHallazgos(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
            @RequestParam(value = "idFase", required = true, defaultValue = "") String idFase,
            @RequestParam(value = "idProyecto", required = true, defaultValue = "") String idProyecto,
            @RequestParam(value = "nombreFase", required = true, defaultValue = "") String nombreFase,
            @RequestParam(value = "territorio", required = true, defaultValue = "") String territorio,
            @RequestParam(value = "zona", required = true, defaultValue = "") String zona,
            @RequestParam(value = "region", required = true, defaultValue = "") String region,
            @RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco) {

        try {

            List<FirmaCheckDTO> firmasB = new ArrayList<FirmaCheckDTO>();

            try {

                firmasB = firmaCheckBI.obtieneDatosFirmaCecoFaseProyecto(idCeco, idFase, idProyecto);

            } catch (Exception e) {

                firmasB.clear();
            }

            boolean respuesta = correoBI.sendMailExpansionCeroHallazgos(idCeco, idFase, idProyecto, firmasB, nombreCeco,
                    nombreFase, territorio, zona, region);

            if (respuesta) {

                return "{\"response\": true}";

            } else {

                return "{\"response\": false}";

            }

        } catch (Exception e) {
            logger.info("AP al obtener la información del hallazgo");
            return "{\"response\": false}";

        }

    }

    // http://10.51.218.104:8080/checklist/expansionCoreServices/generaActaRecorrido.json?idUsuario=191312&idCeco=1000&idFase=1000&idProyecto=1000
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/generaActaRecorrido", method = RequestMethod.GET)
    public @ResponseBody
    Boolean generaActaRecorrido(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
            @RequestParam(value = "idFase", required = true, defaultValue = "") String idFase,
            @RequestParam(value = "idProyecto", required = true, defaultValue = "") String idProyecto,
            @RequestParam(value = "nombreSucursal", required = true, defaultValue = "") String nombreSucursal,
            @RequestParam(value = "zona", required = true, defaultValue = "") String zona,
            @RequestParam(value = "region", required = true, defaultValue = "") String region,
            @RequestParam(value = "fase", required = true, defaultValue = "") String fase,
            @RequestParam(value = "calificacion", required = true, defaultValue = "") String calificacion,
            @RequestParam(value = "fecha", required = true, defaultValue = "") String fecha)
            throws IOException, ParseException {

        ReporteChecksExpBI reporteChecksBI = new ReporteChecksExpBI(this.hallazgosTransfBI, this.firmaCheckBI,
                this.sucursalChecklistBI);
        HashMap<String, Object> responseMap = reporteChecksBI.generaActaRecorridoWeb(idCeco, idProyecto, idFase,
                nombreSucursal, zona, region, fase, calificacion, fecha);

        ServletOutputStream sos = null;

        response.setContentType("Content-type:   application/pdf; charset='UTF-8'; name='pdf';");
        response.setHeader("Content-Disposition", "attachment; filename=" + nombreSucursal + "-" + idFase + ".pdf");
        response.setHeader("Pragma: no-cache", "Expires: 0");
        sos = response.getOutputStream();

        byte[] bytes = Files.readAllBytes(Paths.get(((File) responseMap.get("arhivo")).getAbsolutePath()));

        sos.write(bytes);
        sos.flush();
        sos.close();

        ((File) responseMap.get("arhivo")).delete();
        ((File) responseMap.get("imagenSucursal")).delete();

        return null;

    }

    public boolean getInformacionDepositada(String contenido) {

        boolean response = false;

        try {
            String respuesta = "";
            logger.info("Inicio Informacion depositada antes replace");
            logger.info(contenido);
            logger.info("Fin Informacion depositada antes replace");
            contenido = contenido.replaceAll("\\n", "");
            contenido = contenido.replaceAll("\\r", "");
            logger.info("Inicio Informacion depositada");
            logger.info(contenido);
            respuesta = contenido;

            JSONObject rec = new JSONObject(respuesta);

            String userName = rec.getString("USERNAME");
            String macaddrs = rec.getString("MACADDRS");
            String clientid = rec.getString("CLIENTID");
            String calledid = rec.getString("CALLEDID");
            String fecha = rec.getString("FECHA");

            String macCrm = macaddrs;
            macaddrs = macaddrs.replaceAll(":", "");

            //para el mensaje de CRM el formato es asi 64-BC-58-41-29-E0
            macCrm = macCrm.replaceAll(":", "-");

            calledid = calledid.split("-")[1];
            calledid = (calledid.length() > 3) ? calledid : "0" + calledid;

            String horaString = fecha.split(" ")[1];
            fecha = fecha.split(" ")[0];
            fecha = fecha.split("-")[2] + "/" + fecha.split("-")[1] + "/" + fecha.split("-")[0];
            String fechaCrm = fecha;
            fecha = fecha + " " + horaString;

            String horaCrm = horaString;

            IdICUTiempoRealDTO obj = new IdICUTiempoRealDTO();
            obj.setUsername(userName);
            obj.setMac(macaddrs);
            obj.setIdentificadorCliente(clientid);
            obj.setTienda(calledid);
            obj.setFecha(fecha);
            obj.setFechaEvento(fecha);

            CRMDTO crmDatos = new CRMDTO();
            crmDatos.setIcu(clientid);
            crmDatos.setFecha(fechaCrm);
            crmDatos.setTiempo(horaCrm);

            crmDatos.setTienda(calledid);
            crmDatos.setMacAddress(macCrm);

            response = iCUTiempoRealBI.insertaICUTiempoRealBI(obj);
            if (response) {
                logger.info("*************Inicio envio CRM*************");
                logger.info("*************Inicio obtengo token CRM*************");
                String tokenResult = iCUTiempoRealBI.getTokenCRM();
                System.out.println("tokenResult::" + tokenResult);
                logger.info("*************Fin obtengo token CRM*************");
                if (tokenResult != null && !tokenResult.equals("")) {

                    JSONObject jsonObj = new JSONObject(tokenResult);
                    String message = jsonObj.getString("message");

                    if (message.equalsIgnoreCase("Successful Generate Token")) {

                        String token = jsonObj.getString("access_token");
//                        System.out.println("Token Recibido" + token);

                        String result = iCUTiempoRealBI.setData(token, crmDatos);
                        JSONObject jsonObjCRM = new JSONObject(result);
                        //                      System.out.println("result:" + result);
                        String messageInsertCRM = jsonObjCRM.getString("mensaje");
                        if (messageInsertCRM.equalsIgnoreCase("Operacion exitosa")) {
                            logger.info("*************Termina envio CRM*************");
                            logger.info("Fin Informacion depositada");
                            response = true;
                        } else {
                            logger.info("Error al insertar datos en CRM:: El servicio del Mando un error..." + result);
                            response = false;
                        }
                    } else {
                        String error = jsonObj.getString("error");
                        logger.info("Error Obtener Token CRM:: El servicio del Mando un error..." + message + " Error: " + error);
                        response = false;
                    }
                } else {
                    logger.info("Error Envio CRM:: El servicio del Token no responde...");
                    response = false;
                }

            }

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            return false;

        }

        return response;
    }
    /* FIN PENDIENTE API */

}
