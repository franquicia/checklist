package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ChecklistUsuarioDTO;
import com.gruposalinas.checklist.mappers.CheckUsuaGRowMapper;
import com.gruposalinas.checklist.mappers.ChecklistUsuarioRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ChecklistUsuarioDAOImpl extends DefaultDAO implements ChecklistUsuarioDAO {

    private static Logger logger = LogManager.getLogger(ChecklistUsuarioDAOImpl.class);

    DefaultJdbcCall jdbcObtieneCheckUsuario;
    DefaultJdbcCall jdbcObtieneCheckU;
    DefaultJdbcCall jdbcObtieneCheckUsua;
    DefaultJdbcCall jdbcInsertaCheckUsuario;
    DefaultJdbcCall jdbcActualizaCheckUsuario;
    DefaultJdbcCall jdbcEliminaCheckUsuario;
    DefaultJdbcCall jdbcDesactivaChecklist;
    DefaultJdbcCall jdbcDesactivaChecklUsua;
    DefaultJdbcCall jdbcInsertaEspecial;

    public void init() {

        jdbcObtieneCheckUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK_USUA")
                .withProcedureName("SP_SEL_CHECK_USUA")
                .returningResultSet("RCL_CHECK_USUA", new ChecklistUsuarioRowMapper());

        jdbcObtieneCheckU = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK_USUA")
                .withProcedureName("SP_SEL_G_CKU")
                .returningResultSet("RCL_CHECK_USUA", new ChecklistUsuarioRowMapper());

        jdbcObtieneCheckUsua = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK_USUA")
                .withProcedureName("SP_SEL_CK_USUA")
                .returningResultSet("RCL_CHECK_USUA", new CheckUsuaGRowMapper());

        jdbcInsertaCheckUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK_USUA")
                .withProcedureName("SP_INS_CHECK_USUA");

        jdbcActualizaCheckUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK_USUA")
                .withProcedureName("SP_ACT_CHECK_USUA");

        jdbcEliminaCheckUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK_USUA")
                .withProcedureName("SP_DEL_CHECK_USUA");

        jdbcDesactivaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNAESP")
                .withProcedureName("SP_DESACTIVACK");

        jdbcDesactivaChecklUsua = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK_USUA")
                .withProcedureName("SP_ACT_CHECK_ACT");

        jdbcInsertaEspecial = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNAESP")
                .withProcedureName("SP_INSERTACHECKUSUA");

    }

    @SuppressWarnings("unchecked")
    public List<ChecklistUsuarioDTO> obtieneCheckUsuario(int idCheckU) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ChecklistUsuarioDTO> listaCheck = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FID_CHECK_USUA", idCheckU);

        out = jdbcObtieneCheckUsuario.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_CHECK_USUA.SP_SEL_CHECK_USUA}");
        listaCheck = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_USUA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Checklist Usuario con id(" + idCheckU + ")");
        } else {
            return listaCheck;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistUsuarioDTO> obtieneCheckU(String idUsuario) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ChecklistUsuarioDTO> listaCheck = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", idUsuario);

        out = jdbcObtieneCheckU.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_CHECK_USUA.SP_SEL_CHECK_USUA}");
        listaCheck = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_USUA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Checklist Usuario con id Usuario(" + idUsuario + ")");
        } else {
            return listaCheck;
        }

        return null;
    }

    public int insertaCheckUsuario(ChecklistUsuarioDTO bean) throws Exception {
        Map<String, Object> out = new HashMap<String, Object>();

        int error = 0;
        int idCheckUsua = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_CHECKLIST", bean.getIdChecklist())
                .addValue("PA_FIID_USUARIO", bean.getIdUsuario())
                .addValue("PA_FCID_CECO", bean.getIdCeco())
                .addValue("PA_FIACTIVO", bean.getActivo())
                .addValue("PA_FDFECHA_INICIO", bean.getFechaIni())
                .addValue("PA_FDFECHA_RES", bean.getFechaResp());

        out = jdbcInsertaCheckUsuario.execute(in);

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_FID_CHECK_USUA");
        idCheckUsua = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Checklist Usuario");
        } else {
            return idCheckUsua;
        }

        return idCheckUsua;
    }

    public boolean actualizaCheckUsuario(ChecklistUsuarioDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FID_CHECK_USUA", bean.getIdCheckUsuario())
                .addValue("PA_FIID_CHECKLIST", bean.getIdChecklist())
                .addValue("PA_FIID_USUARIO", bean.getIdUsuario())
                .addValue("PA_FCID_CECO", bean.getIdCeco())
                .addValue("PA_FIACTIVO", bean.getActivo())
                .addValue("PA_FDFECHA_INICIO", bean.getFechaIni())
                .addValue("PA_FDFECHA_RES", bean.getFechaResp());

        out = jdbcActualizaCheckUsuario.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_CHECK_USUA.SP_ACT_CHECK_USUA}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el Checklist Usuario  id( " + bean.getIdCheckUsuario() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaCheckUsuario(int idCheckU) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FID_CHECK_USUA", idCheckU);

        out = jdbcEliminaCheckUsuario.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_CHECK_USUA.SP_DEL_CHECK_USUA}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar Checklist Usuario id(" + idCheckU + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ChecklistUsuarioDTO> obtieneCheckUsua(String idUsuario, String ceco, String fechaInicio) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ChecklistUsuarioDTO> listaCheck = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USUARIO", idUsuario)
                .addValue("PA_CECO", ceco).addValue("PA_FECHA", fechaInicio);

        out = jdbcObtieneCheckUsua.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_CHECK_USUA.SP_SEL_CK_USUA}");
        listaCheck = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_USUA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Checklist Usuario con id Usuario(" + idUsuario + ")");
        } else {
            return listaCheck;
        }

        return null;
    }

    public boolean desactivaChecklist(int idChecklist) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECKLIST", idChecklist);

        out = jdbcDesactivaChecklist.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PAASIGNAESP.SP_DESACTIVACK}");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al descativar el checklist id(" + idChecklist + ")");
        } else {
            return true;
        }

        return false;

    }

    public boolean desactivaCheckUsua(int idCeco, int idChecklist) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FCID_CHECKLIST", idChecklist).addValue("PA_FCID_CECO", idCeco);

        out = jdbcDesactivaChecklUsua.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_CHECK_USUA.SP_ACT_CHECK_ACT}");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        if (ejecucion == 1) {
            logger.info("Algo ocurrió al descativar los check usuarios");
        } else {
            return true;
        }

        return false;

    }

    public boolean insertaCheckusuaEspecial(int idUsuario, int idChecklist, String ceco) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECKLIST", idChecklist).addValue("PA_USUARIO", idUsuario).addValue("PA_CECO", ceco);

        out = jdbcInsertaEspecial.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PAASIGNAESP.SP_INSERTACHECKUSUA}");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al insertar checklist-usuario idcheck: " + idChecklist + " idUsuario: " + idUsuario + " ceco: " + ceco);
        } else {
            return true;
        }

        return false;
    }

}
