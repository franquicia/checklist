package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.InformeGeneralDTO;
import com.gruposalinas.checklist.mappers.InformeGeneralRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class InformeGeneralDAOImpl extends DefaultDAO implements InformeGeneralDAO {

    private static Logger logger = LogManager.getLogger(InformeGeneralDAOImpl.class);

    DefaultJdbcCall jdbcInsertaReporteGeneral;
    DefaultJdbcCall jdbcEliminaReporteGeneral;
    DefaultJdbcCall jdbcObtieneReporteGeneral;
    DefaultJdbcCall jbdcActualizaReporteGeneral;

    public void init() {

        jdbcInsertaReporteGeneral = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINFGRAL")
                .withProcedureName("SPINSINFGRAL");

        jdbcEliminaReporteGeneral = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINFGRAL")
                .withProcedureName("SPDELINFGRAL");

        jdbcObtieneReporteGeneral = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINFGRAL")
                .withProcedureName("SPGETINFGRAL")
                .returningResultSet("PA_CDATOS", new InformeGeneralRowMapper());

        jbdcActualizaReporteGeneral = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINFGRAL")
                .withProcedureName("SPACTINFGRAL");

    }

    public boolean insertaInformeGeneral(String idCeco, String fecha, int idUsuario, String rutaInforme, int idEstatus, String estatus, int numeroBitacora) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        boolean idReturn = false;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_CECO", idCeco)
                .addValue("PA_FDFECHA", fecha)
                .addValue("PA_FIID_USUARIO", idUsuario)
                .addValue("PA_FCRUTAINFORME", rutaInforme)
                .addValue("PA_FISTATUS", idEstatus)
                .addValue("PA_FCSTATUS", estatus)
                .addValue("PA_FINUMBITACORA", numeroBitacora);
        out = jdbcInsertaReporteGeneral.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMINFGRAL.SPINSINFGRAL}");

        //BigDecimal resultado =(BigDecimal) out.get("PA_ERROR");
        //	error = resultado.intValue();
        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_NRESEJECUCION");
        int respReturn = idTipoReturn.intValue();

        if (respReturn != 1) {
            logger.info("Algo ocurrió al insertar datos");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaInformeGeneral(String idCeco, String fecha, int idUsuario) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FCID_CECO", idCeco)
                .addValue("PA_FDFECHA", fecha)
                .addValue("PA_FIID_USUARIO", idUsuario);
        out = jdbcEliminaReporteGeneral.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMINFGRAL.SPDELINFGRAL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el reporte( Ceco" + idCeco + ", Fecha" + fecha + ", Usuario " + idUsuario + ")");
        } else {
            return true;
        }

        return false;

    }

    //parametro
    @SuppressWarnings("unchecked")
    public List<InformeGeneralDTO> obtieneDatosInformeGeneral(String idCeco, String fecha, int idUsuario, int estatus) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<InformeGeneralDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_CECO", idCeco)
                .addValue("PA_FDFECHA", fecha)
                .addValue("PA_FIID_USUARIO", idUsuario)
                .addValue("PA_FISTATUS", estatus);

        out = jdbcObtieneReporteGeneral.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMINFGRAL.SPGETINFGRAL}");
        //lleva el nombre del cursor del procedure
        lista = (List<InformeGeneralDTO>) out.get("PA_CDATOS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener datos del reporte(Ceco" + idCeco + ", Fecha " + fecha + ",Usuario " + idUsuario + ", Estatus " + estatus + ")");
        }
        return lista;

    }

    public boolean actualizaInformeGeneral(String idCeco, String fecha, String idUsuario, String rutaInforme, String idEstatus, String estatus, String numeroBitacora) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        Integer.parseInt(idUsuario);

        Integer.parseInt(idEstatus);
        Integer.parseInt(numeroBitacora);
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_CECO", idCeco)
                .addValue("PA_FDFECHA", fecha)
                .addValue("PA_FIID_USUARIO", idUsuario)
                .addValue("PA_FCRUTAINFORME", rutaInforme)
                .addValue("PA_FISTATUS", idEstatus)
                .addValue("PA_FCSTATUS", estatus)
                .addValue("PA_FINUMBITACORA", numeroBitacora);

        out = jbdcActualizaReporteGeneral.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMINFGRAL.SPACTINFGRAL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar");
        } else {
            return true;
        }
        return false;

    }

}
