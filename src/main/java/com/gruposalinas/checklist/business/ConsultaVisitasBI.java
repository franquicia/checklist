package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ConsultaVisitasDAO;
import com.gruposalinas.checklist.domain.ConsultaVisitasDTO;


public class ConsultaVisitasBI {
		
	private static Logger logger = LogManager.getLogger(ConsultaVisitasDTO.class);
	@Autowired
	ConsultaVisitasDAO consultaVisitasDAO;
	
	List<ConsultaVisitasDTO> listaConsulta = null;

	public List<ConsultaVisitasDTO> obtieneVisitas(ConsultaVisitasDTO bean){
	
		try {
			listaConsulta = consultaVisitasDAO.ObtieneVisitas(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
			logger.info(""+e);
		}			
		
		return listaConsulta;
	
	}
	
	public List<ConsultaVisitasDTO> ObtienePromCalif(ConsultaVisitasDTO bean){
		
		try {
			listaConsulta = consultaVisitasDAO.ObtienePromCalif(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
			logger.info(""+e);
		}			
		
		return listaConsulta;
	
	
}
	
}