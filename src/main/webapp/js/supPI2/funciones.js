angular.module('app', [])

.controller('validarForm', ['$scope', function($scope) {
    $scope.tab = 1;

    $scope.setTab = function(newTab){
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };
	
  }])
.controller('tabs', ['$scope', function($scope) {
    $scope.tab1 = 1;

    $scope.setTab1 = function(newTab1){
      $scope.tab1 = newTab1;
    };

    $scope.isSet1 = function(tabNum1){
      return $scope.tab1 === tabNum1;
    };
	
  }]);


//Tooltip
(function() {

  function getOffset(elem) {
    var offsetLeft = 0, offsetTop = 0;
    do {
      if ( !isNaN( elem.offsetLeft ) )
      {
        offsetLeft += elem.offsetLeft;
        offsetTop += elem.offsetTop;
      }
    } while( elem = elem.offsetParent );
    return {left: offsetLeft, top: offsetTop};
  }

  var targets = document.querySelectorAll( '[rel=tooltip]' ),
  target  = false,
  tooltip = false,
  title   = false,
  tip     = false;

  for(var i = 0; i < targets.length; i++) {
    targets[i].addEventListener("mouseenter", function() {
      target  = this;
      tip     = target.getAttribute("title");
      tooltip = document.createElement("div");
      tooltip.id = "tooltip";

      if(!tip || tip == "")
      return false;

      target.removeAttribute("title");
      tooltip.style.opacity = 0;
      tooltip.innerHTML = tip;
      document.body.appendChild(tooltip);

      var init_tooltip = function()
      {
        console.log(getOffset(target));
        // set width of tooltip to half of window width
        if(window.innerWidth < tooltip.offsetWidth * 1.5)
        tooltip.style.maxWidth = window.innerWidth / 2;
        else
        tooltip.style.maxWidth = 340;

        var pos_left = getOffset(target).left + (target.offsetWidth / 2) - (tooltip.offsetWidth / 2),
        pos_top  = getOffset(target).top - tooltip.offsetHeight - 10;
        console.log("top is", pos_top);
        if( pos_left < 0 )
        {
          pos_left = getOffset(target).left + target.offsetWidth / 2 - 20;
          tooltip.classList.add("left");
        }
        else
        tooltip.classList.remove("left");

        if( pos_left + tooltip.offsetWidth > window.innerWidth )
        {
          pos_left = getOffset(target).left - tooltip.offsetWidth + target.offsetWidth / 2 + 20;
          tooltip.classList.add("right");
        }
        else
        tooltip.classList.remove("right");

        if( pos_top < 0 )
        {
          var pos_top  = getOffset(target).top + target.offsetHeight + 15;
          tooltip.classList.add("top");
        }
        else
        tooltip.classList.remove("top");
        // adding "px" is very important
        tooltip.style.left = pos_left + "px";
        tooltip.style.top = pos_top + "px";
        tooltip.style.opacity  = 1;
      };

      init_tooltip();
      window.addEventListener("resize", init_tooltip);

      var remove_tooltip = function() {
        tooltip.style.opacity  = 0;
        document.querySelector("#tooltip") && document.body.removeChild(document.querySelector("#tooltip"));
        target.setAttribute("title", tip );
      };

      target.addEventListener("mouseleave", remove_tooltip );
      tooltip.addEventListener("click", remove_tooltip );
    });
  }

})();

//Calendario
	$.datepicker.regional['es'] = {
		 closeText: 'Cerrar',
		 prevText: '',
		 nextText: ' ',
		 currentText: 'Hoy',
		 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
		 weekHeader: 'Sm',
		 dateFormat: 'dd/mm/yy',
		 firstDay: 1,
		 isRTL: false,
		 showMonthAfterYear: false,
		 yearSuffix: ''
	 };
	$.datepicker.setDefaults($.datepicker.regional['es']);
	$(function() {
		$( "#datepicker" ).datepicker({
			firstDay: 1 
		});
		$( "#datepicker1" ).datepicker({
			firstDay: 1 
		});
		$( "#datepicker2" ).datepicker({
			firstDay: 1 
		});
	});

$(document).ready(function() {
	//Modal resolución
	$('.modalFinalizado_view').click(function (e) {
		$('#modalFinalizado').modal();
		return false;
	})
	
	$('#modalCarga_view').click(function(e) {
		setTimeout(function() {
			$('#modalCarga').modal({
			});
			return false;
		}, 1);
	})

	// Menu hambuergesa
	$("#effect").toggle(false);
	$("#hamburger").click(function (event) {
		event.stopPropagation();
		 $( "#effect" ).toggle( "slide"); 
	});

	$(document).click(function() {
		$("#effect").toggle(false);
	});
	$("#effect").click (function (event){
		event.stopPropagation();
	}); 

	/*Menu User*/
	$(".MenuUser, .MenuUser1").hide();
	$('.imgShowMenuUser').click(function() {
		$(".MenuUser, .MenuUser1").toggle("ver");
	});

	$(".divRC").show();
	$('#radioResCivil').on( "change", function() {
				$(".divRC").show();
			});
	$('#radioEmpre').on( "change", function() {
		$(".divRC").show();
	});
	
	// Menu seguimiento
	$("#seguimiento").toggle(true);
	
	$(".hamburgerSeg").click(function (event) {
		event.stopPropagation();
		 $( ".seguimiento" ).toggle( "slide"); 
	});

	$(document).click(function() {
		$(".seguimiento").toggle(false);
	});
	$(".seguimiento").click (function (event){
		event.stopPropagation();
	});
	/********Para selects*******************/
	$( "select").dropkick({
		mobile: true
	});	
	
	$('.switch').click(function(){
		$(this).toggleClass("switchOn");
	}); 
	
	$( ".cerrarChip" ).click(function() {
	   $(this).parent().parent().remove()
	});


});
//Validacion login
$(".txtRojo").show();
function validaLogin(f) {
	if(f.txtUsuario.value == "")  
  	{	
	  $(".txtRojo").html('Por favor ingrese su usuario');
	  return false;
  	}else
	if(f.txtLlave.value == "" )
  	{
	  $(".txtRojo").html('Por favor ingrese su contraseña');
	  return false;
  	}else
	{
		location.href="index.html";
		return false;}
}
/*Valida buscador del menu de hamburgesa*/
function valida(f) {
	if (f.busca.value == "")  {
		alert("Es necesario que introduzca un valor");
	}else { 
		return false;
	}
}
/*Detecta resolucion de pantalla*/
if (matchMedia) {
  const mq = window.matchMedia("(min-width: 780px)");
  mq.addListener(WidthChange);
  WidthChange(mq);
}
function WidthChange(mq) {
	if (mq.matches) {
	  	$("#menu ul").addClass("normal");
	  	$("#menu ul li").removeClass("in");
		$('ul.nivel1 >li > ul').slideUp();
		$('ul.nivel2 >li > ul').slideUp();
		$('ul.nivel1>li').off("click");
		$('ul.nivel2>li').off("click");
	} else {
	   $("#menu ul").removeClass("normal");

		$('ul.nivel1>li').on('click', function(event) {
			event.stopPropagation();
			
			$target = $(this).children();

			if ($(this).hasClass("in"))  {
			    $('ul.nivel2').slideUp();

				$(this).removeClass("in");
				$('.flecha').removeClass("rotar");
			}else {
			  	$('ul.nivel1 > li').removeClass("in");
				$('ul.nivel2').slideUp();
				$('ul.nivel3').slideUp();
				$('ul.nivel2>li').removeClass("in");
				$(this).addClass("in");
			  	$target.slideDown();
				$('ul.nivel1 > li > a .flecha').addClass("rotar");
				
			}
		});
		$('ul.nivel2>li').on('click', function(event) {
			event.stopPropagation();
		
			$target = $(this).children();

			if ($(this).hasClass("in"))  {
			    $('ul.nivel3').slideUp();
				$(this).removeClass("in");
				$('ul.nivel2 > li > a .flecha').removeClass("rotar");
			}else {
			  	$('ul.nivel2 > li').removeClass("in");
				$('ul.nivel3').slideUp();
				$(this).addClass("in");
			  	$target.slideDown();
				$('ul.nivel2 > li > a .flecha').addClass("rotar");
			}
		});
		$('ul.nivel3>li').on('click', function(event) {
			event.stopPropagation();
		});
	}
}
var allPanels = $('.accordion > dd').show().first().show();

	jQuery('.accordion > dt').on('click', function() {
		$this = $(this);
		//the target panel content
		$target = $this.next();

		jQuery('.accordion > dt').removeClass('accordion-active');
		if ($target.hasClass("in")) {
		  $this.removeClass('accordion-active');
		  $target.slideUp();
		  $target.removeClass("in");

		} else {
		  $this.addClass('accordion-active');
		  jQuery('.accordion > dd').removeClass("in");
		  $target.addClass("in");
			$(".subSeccion").show();

		  jQuery('.accordion > dd').slideUp();
		  $target.slideDown();
		}
	});
	
	var allPanels = $('.accordionM > dd').show();

	jQuery('.accordionM > dt').on('click', function() {
		$this = $(this);
		//the target panel content
		$target = $this.next();

		jQuery('.accordionM > dt').removeClass('accordion-active');
		if ($target.hasClass("in")) {
		  $this.removeClass('accordion-active');
		  $target.slideUp();
		  $target.removeClass("in");

		} else {
		  $this.addClass('accordion-active');
		  jQuery('.accordionM > dd').removeClass("in");
		  $target.addClass("in");
			$(".subSeccion").show();

		  jQuery('.accordionM > dd').slideUp();
		  $target.slideDown();
		}
	});
$(document).ready(function() {
	//Acordeon 
 	$(".menu2").accordion({
      collapsible: true,
      active: false,
      autoHeight: true,
      navigation: true,
      heightStyle: "content"
    });
	

});


//Show - Hide
$(".divResultado").show();
$(".btnResultado").click(function(){
	$(".divResultado").show();
});
	
$('.divHideV').show();
$('.divHideP').show();

$('.divHideS').show();
$('.divHideSV').show();

$('.btnBuscarP').click(function(){
	$('.divHideV').show();

});

var buscarP = false;

$('.btnBuscarS').click(function(){
	if(buscarP == false){
		buscarP = true;
		$('.divHideS').show();
	}else{
		$('.divHideSV').show();
		$('.divHideS').show();
	}
	

});

var buscarV = false;

$('.btnBuscarP').click(function(){
	if(buscarV == false){
		buscarV = true;
		$('.divHideV').show();
	}else{
		$('.divHideP').show();
		$('.divHideV').show();
	}
	

});

/*Asignación de Perfiles*/
var validacionBusqueda = [
	{nombre:'Buscar nombre o número socio', estatus:false}
];

$('.buscarNNSocio').change(function(){
	if(this == ""){
		validacionBusqueda[0].estatus=false;
	} else {
		validacionBusqueda[0].estatus=true;
	}
	myresultadosA();
});
$('.divResultadoAsegurador').show();
function myresultadosA(){
	var contador = 0;
	validacionBusqueda.forEach(function(element){
		if(element.estatus==true){
			contador++;
		}
	});
	if(contador==1){
		$('.divResultadoAsegurador').show();
	}
	else {$('.divResultadoAsegurador').show();}
}

function uncheckAll2() {
  var inputs = document.querySelectorAll('.checkPerfil');
  for(var i = 0; i < inputs.length; i++) {
    inputs[i].checked = false;
  }
}

/*Asignación de Personal*/
var validacionBusquedaP = [
	{nombre:'Buscar nombre o número socio', estatus:false}
];
$('.buscarNNSocio').change(function(){
	if(this == ""){
		validacionBusquedaP[0].estatus=false;
	} else {
		validacionBusquedaP[0].estatus=true;
	}
	myresultadosA();
});
$('.divResultadosPersonal').show();
function myresultadosP(){
	var contador = 0;
	validacionBusquedaP.forEach(function(element){
		if(element.estatus==true){
			contador++;
		}
	});
	if(contador==1){
		$('.divResultadosPersonal').show();
	}
	else {$('.divResultadosPersonal').show();}
}


var validacionAsigPersonal = [
	{nombre:'Escribe el Número de Ceco', estatus:false}
];

$('.buscarNombre').change(function(){
	if(this == ""){
		validacionAsigPersonal[0].estatus=false;
	} else {
		validacionAsigPersonal[0].estatus=true;
	}
	myconsultaPersonal();
});

$('.divResultadoBusquedaP').show();

function myconsultaPersonal(){
	var contador = 0;
	validacionAsigPersonal.forEach(function(element){
		if(element.estatus==true){
			contador++;
		}
	});
	if(contador==1){
		$('.divResultadoBusquedaP').show();
	}
	else {$('.divResultadoBusquedaP').show();}
}


var contPersonal=0;
$('.agregarPersonal').click(function (e) {
	contPersonal+=1;
	$(".tblAsigPersonal").append( $( "<tr><td>Samuel García Robles Becerril</td><td>456278</td> <td>Mega Naucalpan</td><td><img src='img/delete.svg' class='ico carga"+contPersonal+"'></td></tr>" ) );
	eliminarFoto1("carga"+contPersonal);
});

/*Eliminar*/
function eliminarFoto1(idElem1){
	$("."+idElem1).click(function (e) {
		$(this).parent().parent().remove();
	});	
} 

$('.btnDelete').on('click',function(){
	$(this).closest('tr').remove();
});

/*Asignacion de Ceco*/
var validacionBusquedaCeco = [
	{nombre:'Buscar nombre o número socio', estatus:false}
];

$('.buscarNNSocio').change(function(){
	if(this == ""){
		validacionBusquedaCeco[0].estatus=false;
		$('.btnAsignar').show();
	} else {
		validacionBusquedaCeco[0].estatus=true;
		$('.btnAsignar').css('display', 'none');
	}
	myresultadosC();
});

$('.divResultadosCeco').show();
function myresultadosC(){
	var contador = 0;
	validacionBusquedaCeco.forEach(function(element){
		if(element.estatus==true){
			contador++;
		}
	});
	if(contador==1){
		$('.divResultadosCeco').show();
	}
	else {
		$('.divResultadosCeco').css('display', 'none');
		
	}
}

var contPersonalCeco=0;
$('.agregarPersonalCeco').click(function (e) {
	contPersonalCeco+=1;
	$(".tblAsigCeco").append( $( "<tr><td>BA PRESTA PRENDA IXTLAN DEL RIO NAY</td><td>624729</td><td>IXTLAN</td><td><img src='img/delete.svg' class='ico carga"+contPersonalCeco+"'></td></tr>" ) );
	eliminarFoto1("carga"+contPersonalCeco);
});



var validacionAsigCeco = [
	{nombre:'Escribe el Número de Ceco', estatus:false}
];

$('.buscarNumCeco').change(function(){
	if(this == ""){
		validacionAsigCeco[0].estatus=false;
	} else {
		validacionAsigCeco[0].estatus=true;
	}
	myconsultaCeCo();
});

$('.divResultadoBusquedaP').show();

function myconsultaCeCo(){
	var contador = 0;
	validacionAsigCeco.forEach(function(element){
		if(element.estatus==true){
			contador++;
		}
	});
	if(contador==1){
		$('.divResultadoBusquedaP').show();
	}
	else {$('.divResultadoBusquedaP').show();}
}

