package com.gruposalinas.checklist.domain;

public class HallazgosExpDTO {

	private int idHallazgo;
	private int idFolio;
	private int idResp;
	private int status;
	private int idPreg;
	private String preg;
	private String respuesta;
	private int pregPadre;
	private String responsable;
	private String disposi;
	private String area;
	private String obs;
	private int bitGral;
	private String aux;
	private int aux2;
	private String fechaIni;
	private String fechaFin;
	private String sla;
	private String fechaAutorizo;
	private String periodo;
	private String ceco;
	private String cecoNom;
	private int idcheck;
	private String nombChek;
	private String zonaClasi;
	private String arbol;
	private String ruta;
	private String obsNueva;
	private int recorrido;
	private int bitacora;
	private String motivrechazo;
	private String usuModif;
	private int idVersion;
	private String obsAtencion;
	
	private int tipoEvi;
	private int idEvi;
	private String nombre;
	private int totEvi;
	private int bitaGral;

	private String liderObra;
	private String fechaTermino;
	private String usuarioFRQ;
	private String nombreFase;
	private String obsFase;
	private String fase;
	
	private int usuario;

	private String nomUsu;
	private String modulo;
	private double ponderacion;
	private int criticidad;

	public int getIdHallazgo() {
		return idHallazgo;
	}

	public void setIdHallazgo(int idHallazgo) {
		this.idHallazgo = idHallazgo;
	}

	public int getIdFolio() {
		return idFolio;
	}

	public void setIdFolio(int idFolio) {
		this.idFolio = idFolio;
	}

	public int getIdResp() {
		return idResp;
	}

	public void setIdResp(int idResp) {
		this.idResp = idResp;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getIdPreg() {
		return idPreg;
	}

	public void setIdPreg(int idPreg) {
		this.idPreg = idPreg;
	}

	public String getPreg() {
		return preg;
	}

	public void setPreg(String preg) {
		this.preg = preg;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public int getPregPadre() {
		return pregPadre;
	}

	public void setPregPadre(int pregPadre) {
		this.pregPadre = pregPadre;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public String getDisposi() {
		return disposi;
	}

	public void setDisposi(String disposi) {
		this.disposi = disposi;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public int getBitGral() {
		return bitGral;
	}

	public void setBitGral(int bitGral) {
		this.bitGral = bitGral;
	}

	public String getAux() {
		return aux;
	}

	public void setAux(String aux) {
		this.aux = aux;
	}

	public int getAux2() {
		return aux2;
	}

	public void setAux2(int aux2) {
		this.aux2 = aux2;
	}

	public String getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getSla() {
		return sla;
	}

	public void setSla(String sla) {
		this.sla = sla;
	}

	public String getFechaAutorizo() {
		return fechaAutorizo;
	}

	public void setFechaAutorizo(String fechaAutorizo) {
		this.fechaAutorizo = fechaAutorizo;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getCeco() {
		return ceco;
	}

	public void setCeco(String ceco) {
		this.ceco = ceco;
	}

	public String getCecoNom() {
		return cecoNom;
	}

	public void setCecoNom(String cecoNom) {
		this.cecoNom = cecoNom;
	}

	public int getIdcheck() {
		return idcheck;
	}

	public void setIdcheck(int idcheck) {
		this.idcheck = idcheck;
	}

	public String getNombChek() {
		return nombChek;
	}

	public void setNombChek(String nombChek) {
		this.nombChek = nombChek;
	}

	public String getZonaClasi() {
		return zonaClasi;
	}

	public void setZonaClasi(String zonaClasi) {
		this.zonaClasi = zonaClasi;
	}

	public String getArbol() {
		return arbol;
	}

	public void setArbol(String arbol) {
		this.arbol = arbol;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public String getObsNueva() {
		return obsNueva;
	}

	public void setObsNueva(String obsNueva) {
		this.obsNueva = obsNueva;
	}

	
	public int getRecorrido() {
		return recorrido;
	}

	public void setRecorrido(int recorrido) {
		this.recorrido = recorrido;
	}
	

	public int getBitacora() {
		return bitacora;
	}

	public void setBitacora(int bitacora) {
		this.bitacora = bitacora;
	}
	

	public String getMotivrechazo() {
		return motivrechazo;
	}

	public void setMotivrechazo(String motivrechazo) {
		this.motivrechazo = motivrechazo;
	}

	
	public String getUsuModif() {
		return usuModif;
	}

	public void setUsuModif(String usuModif) {
		this.usuModif = usuModif;
	}

	public int getIdVersion() {
		return idVersion;
	}

	public void setIdVersion(int idVersion) {
		this.idVersion = idVersion;
	}

	public String getObsAtencion() {
		return obsAtencion;
	}

	public void setObsAtencion(String obsAtencion) {
		this.obsAtencion = obsAtencion;
	}

	public int getTipoEvi() {
		return tipoEvi;
	}

	public void setTipoEvi(int tipoEvi) {
		this.tipoEvi = tipoEvi;
	}

	public int getIdEvi() {
		return idEvi;
	}

	public void setIdEvi(int idEvi) {
		this.idEvi = idEvi;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getTotEvi() {
		return totEvi;
	}

	public void setTotEvi(int totEvi) {
		this.totEvi = totEvi;
	}

	public int getBitaGral() {
		return bitaGral;
	}

	public void setBitaGral(int bitaGral) {
		this.bitaGral = bitaGral;
	}

	public String getLiderObra() {
		return liderObra;
	}

	public void setLiderObra(String liderObra) {
		this.liderObra = liderObra;
	}

	public String getFechaTermino() {
		return fechaTermino;
	}

	public void setFechaTermino(String fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

	public String getUsuarioFRQ() {
		return usuarioFRQ;
	}

	public void setUsuarioFRQ(String usuarioFRQ) {
		this.usuarioFRQ = usuarioFRQ;
	}

	public String getNombreFase() {
		return nombreFase;
	}

	public void setNombreFase(String nombreFase) {
		this.nombreFase = nombreFase;
	}

	public String getObsFase() {
		return obsFase;
	}

	public void setObsFase(String obsFase) {
		this.obsFase = obsFase;
	}

	public int getUsuario() {
		return usuario;
	}

	public void setUsuario(int usuario) {
		this.usuario = usuario;
	}

	public String getNomUsu() {
		return nomUsu;
	}

	public void setNomUsu(String nomUsu) {
		this.nomUsu = nomUsu;
	}

	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	public double getPonderacion() {
		return ponderacion;
	}

	public void setPonderacion(double ponderacion) {
		this.ponderacion = ponderacion;
	}

	public int getCriticidad() {
		return criticidad;
	}

	public void setCriticidad(int criticidad) {
		this.criticidad = criticidad;
	}

	public String getFase() {
		return fase;
	}

	public void setFase(String fase) {
		this.fase = fase;
	}

	@Override
	public String toString() {
		return "HallazgosExpDTO [idHallazgo=" + idHallazgo + ", idFolio=" + idFolio + ", idResp=" + idResp + ", status="
				+ status + ", idPreg=" + idPreg + ", preg=" + preg + ", respuesta=" + respuesta + ", pregPadre="
				+ pregPadre + ", responsable=" + responsable + ", disposi=" + disposi + ", area=" + area + ", obs="
				+ obs + ", bitGral=" + bitGral + ", aux=" + aux + ", aux2=" + aux2 + ", fechaIni=" + fechaIni
				+ ", fechaFin=" + fechaFin + ", sla=" + sla + ", fechaAutorizo=" + fechaAutorizo + ", periodo="
				+ periodo + ", ceco=" + ceco + ", cecoNom=" + cecoNom + ", idcheck=" + idcheck + ", nombChek="
				+ nombChek + ", zonaClasi=" + zonaClasi + ", arbol=" + arbol + ", ruta=" + ruta + ", obsNueva="
				+ obsNueva + ", recorrido=" + recorrido + ", bitacora=" + bitacora + ", motivrechazo=" + motivrechazo
				+ ", usuModif=" + usuModif + ", idVersion=" + idVersion + ", obsAtencion=" + obsAtencion + ", tipoEvi="
				+ tipoEvi + ", idEvi=" + idEvi + ", nombre=" + nombre + ", totEvi=" + totEvi + ", bitaGral=" + bitaGral
				+ ", liderObra=" + liderObra + ", fechaTermino=" + fechaTermino + ", usuarioFRQ=" + usuarioFRQ
				+ ", nombreFase=" + nombreFase + ", obsFase=" + obsFase + ", fase=" + fase + ", usuario=" + usuario
				+ ", nomUsu=" + nomUsu + ", modulo=" + modulo + ", ponderacion=" + ponderacion + ", criticidad="
				+ criticidad + "]";
	}

}