package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.FragmentMenuDTO;
import com.gruposalinas.checklist.mappers.FragmentConfigProyRowMapper;
import com.gruposalinas.checklist.mappers.FragmentMenuRowMapper;
import com.gruposalinas.checklist.mappers.FragmentRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class FragmentMenuDAOImpl extends DefaultDAO implements FragmentMenuDAO {

    private static Logger logger = LogManager.getLogger(FragmentMenuDAOImpl.class);

    DefaultJdbcCall jdbcInsertaFragment;
    DefaultJdbcCall jdbcEliminaFragment;
    DefaultJdbcCall jdbcObtieneFragment;
    DefaultJdbcCall jbdcActualizaFragment;
//MENU
    DefaultJdbcCall jdbcInsertaMenu;
    DefaultJdbcCall jdbcEliminaMenu;
    DefaultJdbcCall jdbcObtieneMenu;
    DefaultJdbcCall jbdcActualizaMenu;

    //CONFIGURACION PROYECTO
    DefaultJdbcCall jdbcInsertaConfig;
    DefaultJdbcCall jdbcEliminaConfig;
    DefaultJdbcCall jdbcObtieneConfig;
    DefaultJdbcCall jbdcActualizaConfig;

    public void init() {

        jdbcInsertaFragment = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFRAGMENU")
                .withProcedureName("SP_INS_CTFRAG");

        jdbcEliminaFragment = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFRAGMENU")
                .withProcedureName("SP_DEL_FRAG");

        jdbcObtieneFragment = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFRAGMENU")
                .withProcedureName("SP_SEL_FRAG")
                .returningResultSet("RCL_FRAG", new FragmentRowMapper());

        jbdcActualizaFragment = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFRAGMENU")
                .withProcedureName("SP_ACT_FRAG");
//menu
        jdbcInsertaMenu = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFRAGMENU")
                .withProcedureName("SP_INS_CTMENU");

        jdbcEliminaMenu = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFRAGMENU")
                .withProcedureName("SP_DEL_MENU");

        jdbcObtieneMenu = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFRAGMENU")
                .withProcedureName("SP_SEL_MENU")
                .returningResultSet("RCL_MENU", new FragmentMenuRowMapper());

        jbdcActualizaMenu = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFRAGMENU")
                .withProcedureName("SP_ACT_MENU");

//config proy
        jdbcInsertaConfig = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACONFIGPROYEC")
                .withProcedureName("SP_INS_CONFPROY");

        jdbcEliminaConfig = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACONFIGPROYEC")
                .withProcedureName("SP_DEL_CONFPROY");

        jdbcObtieneConfig = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACONFIGPROYEC")
                .withProcedureName("SP_SEL_CONFPROY")
                .returningResultSet("RCL_CONFPROY", new FragmentConfigProyRowMapper());

        jbdcActualizaConfig = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACONFIGPROYEC")
                .withProcedureName("SP_ACT_CONFPROY");
    }

    public int insertaFragment(FragmentMenuDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NOMBRE", bean.getNombreFragment())
                .addValue("PA_PARAMETRO", bean.getParametro());

        out = jdbcInsertaFragment.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADFRAGMENU.SP_INS_FRAG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_FRAG");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el Empleado");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean eliminaFragment(String idFragment) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FRAG", idFragment);

        out = jdbcEliminaFragment.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADFRAGMENU.SP_DEL_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el fragment(" + idFragment + ")");
        } else {
            return true;
        }

        return false;

    }

    //parametro
    @SuppressWarnings("unchecked")
    public List<FragmentMenuDTO> obtieneDatosFragment(String idFragment) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<FragmentMenuDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDFRAG", idFragment);

        out = jdbcObtieneFragment.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADFRAGMENU.SP_SEL_FRAG}");
        //lleva el nombre del cursor del procedure
        lista = (List<FragmentMenuDTO>) out.get("RCL_FRAG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el fragment(" + idFragment + ")");
        }
        return lista;

    }

    public boolean actualizaFragment(FragmentMenuDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FRAG", bean.getIdFragment())
                .addValue("PA_NOMBRE", bean.getNombreFragment())
                .addValue("PA_PARAMETRO", bean.getParametro())
                .addValue("PA_NUMFRAG", bean.getNumFragment());

        out = jbdcActualizaFragment.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADFRAGMENU.SP_ACT_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdFragment() + ")");
        } else {
            return true;
        }
        return false;

    }

    @Override
    public int insertaMenu(FragmentMenuDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_TITULO", bean.getTituloMenu())
                .addValue("PA_PERFIL", bean.getIdPerfil())
                .addValue("PA_ORDEN", bean.getOrdenMenu())
                .addValue("PA_AUX", bean.getAux());

        out = jdbcInsertaMenu.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADFRAGMENU.SP_INS_MENU}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_MENU");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el MENU");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    @Override
    public boolean eliminaMenu(String menu) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_MENU", menu);

        out = jdbcEliminaMenu.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADFRAGMENU.SP_DEL_MENU}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el MENU(" + menu + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<FragmentMenuDTO> obtieneDatosMenu(String menu) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<FragmentMenuDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDMENU", menu);

        out = jdbcObtieneMenu.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADFRAGMENU.SP_SEL_Menu}");
        //lleva el nombre del cursor del procedure
        lista = (List<FragmentMenuDTO>) out.get("RCL_MENU");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el fragment(" + menu + ")");
        }
        return lista;

    }

    @Override
    public boolean actualizaMenu(FragmentMenuDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_MENU", bean.getIdConfigMenu())
                .addValue("PA_TITULO", bean.getTituloMenu())
                .addValue("PA_PERFIL", bean.getIdPerfil())
                .addValue("PA_ORDEN", bean.getOrdenMenu())
                .addValue("PA_AUX", bean.getAux());

        out = jbdcActualizaMenu.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADFRAGMENU.SP_ACT_MENU}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdFragment() + ")");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public int insertaConf(FragmentMenuDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCONF", bean.getIdConfigMenu())
                .addValue("PA_IDFRAG", bean.getIdFragment())
                .addValue("PA_ORDENFRA", bean.getOrdenFragment())
                .addValue("PA_AGRUPAFIR", bean.getIdAgrupaFirma())
                .addValue("PA_PARAM", bean.getParametro())
                .addValue("PA_AUX", bean.getAux());

        out = jdbcInsertaConfig.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADFRAGMENU.SP_INS_CONFIG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_CONFPROY");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el MENU");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    @Override
    public boolean eliminaConf(String menu) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CONFPROY", menu);

        out = jdbcEliminaConfig.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADFRAGMENU.SP_DEL_CONFIG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el MENU(" + menu + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<FragmentMenuDTO> obtieneDatosConf(String menu) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        List<FragmentMenuDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCONFPROY", menu);

        out = jdbcObtieneConfig.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADFRAGMENU.SP_SEL_CONFIG}");
        //lleva el nombre del cursor del procedure
        lista = (List<FragmentMenuDTO>) out.get("RCL_CONFPROY");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el fragment(" + menu + ")");
        }
        return lista;
    }

    @Override
    public boolean actualizaConf(FragmentMenuDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CONFPROY", bean.getIdConfigProyecto())
                .addValue("PA_IDCONF", bean.getIdConfigMenu())
                .addValue("PA_IDFRAG", bean.getIdFragment())
                .addValue("PA_ORDENFRA", bean.getOrdenFragment())
                .addValue("PA_AGRUPAFIR", bean.getIdAgrupaFirma())
                .addValue("PA_PARAM", bean.getParametro())
                .addValue("PA_AUX", bean.getAux());

        out = jbdcActualizaConfig.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADFRAGMENU.SP_ACT_CONFIG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdFragment() + ")");
        } else {
            return true;
        }
        return false;
    }

}
