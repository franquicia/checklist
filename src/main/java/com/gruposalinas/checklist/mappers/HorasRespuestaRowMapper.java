package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.HorasRespuestaDTO;

public class HorasRespuestaRowMapper implements RowMapper<HorasRespuestaDTO>{

	@Override
	public HorasRespuestaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		HorasRespuestaDTO horasRespuestaDTO = new HorasRespuestaDTO();
		
		horasRespuestaDTO.setCeco(rs.getString("FCID_CECO"));
		horasRespuestaDTO.setHora(rs.getString("HORA"));
		
		return horasRespuestaDTO;
	}
	
	

}
