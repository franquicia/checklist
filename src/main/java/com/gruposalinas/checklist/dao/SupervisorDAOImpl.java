package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.SupervisorDTO;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class SupervisorDAOImpl extends DefaultDAO implements SupervisorDAO {

    private static Logger logger = LogManager.getLogger(ProtocoloDAOImpl.class);

    DefaultJdbcCall jdbcObtieneSupervisor;

    public void init() {

        jdbcObtieneSupervisor = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE_SUPRV").withProcedureName("SPGETSUPERVISOR");
    }

    @SuppressWarnings("unchecked")
    /* @Override */
    public List<SupervisorDTO> obtieneSupervisor(SupervisorDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<SupervisorDTO> listaSupervisor = new ArrayList<SupervisorDTO>();

        int idUsuarioS = 0;
        if (bean.getIdUsuario() != 0) {
            idUsuarioS = bean.getIdUsuario();
        }
        String nomUsuario = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_USUARIO", idUsuarioS);

        out = jdbcObtieneSupervisor.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAREPORTE_SUPRV.SPGETSUPERVISOR");

        nomUsuario = (String) out.get("PA_NOMBRESUPERV");

        if (nomUsuario != null) {
            bean.setNomSupervisor(nomUsuario);
            listaSupervisor.add(bean);

        } else {
            error = 1;
        }

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el supervisor");
        } else {
            return listaSupervisor;
        }

        return listaSupervisor;
    }

}
