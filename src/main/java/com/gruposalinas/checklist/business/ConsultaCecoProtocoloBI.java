package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ConsultaCecoProtocoloDAO;
import com.gruposalinas.checklist.domain.ConsultaCecoProtocoloDTO;
import com.gruposalinas.checklist.domain.ConsultaDTO;


public class ConsultaCecoProtocoloBI {
		
	private static Logger logger = LogManager.getLogger(ConsultaCecoProtocoloBI.class);
	
	@Autowired
	ConsultaCecoProtocoloDAO consultaCecoDAO;
	
	List<ConsultaCecoProtocoloDTO> listaConsulta = null;

	public List<ConsultaCecoProtocoloDTO> obtieneConsultaCeco(){
	
		try {
			listaConsulta = consultaCecoDAO.obtieneConsultaCeco();
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
		}			
		
		return listaConsulta;
	
	}
	
	List<ConsultaCecoProtocoloDTO> listaConsulta2 = null;
	
	public List<ConsultaCecoProtocoloDTO> obtieneConsultaBitacora(ConsultaCecoProtocoloDTO bean){
		
		try {
			listaConsulta2 = consultaCecoDAO.obtieneConsultaBitacora(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
		}			
		
		return listaConsulta2;
	
		}

	List<ConsultaCecoProtocoloDTO> listaConsulta3 = null;

	public List<ConsultaCecoProtocoloDTO> obtieneConsultaChecklist(ConsultaCecoProtocoloDTO bean){
		
		try {
			listaConsulta3 = consultaCecoDAO.obtieneConsultaChecklist(bean);
		} catch (Exception e) {
			logger.info(e);
			logger.info("No fue posible consultar el StoreProcedure");
		}			
		
		return listaConsulta3;
	
		}
	
	List<ConsultaCecoProtocoloDTO> listaConsulta4 = null;

	public List<ConsultaCecoProtocoloDTO> obtieneDetalle(ConsultaCecoProtocoloDTO bean){
		
		try {
			listaConsulta4 = consultaCecoDAO.obtieneDetalle(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
		}			
		
		return listaConsulta4;
	
		}
}
