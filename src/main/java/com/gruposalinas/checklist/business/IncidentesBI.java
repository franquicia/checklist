package com.gruposalinas.checklist.business;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.checklist.domain.AdjuntoDTO;
import com.gruposalinas.checklist.domain.IncidenteDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;


public class IncidentesBI {

	private Logger logger = LogManager.getLogger(IncidentesBI.class);

	private static List<String> motivosEstado;

	public IncidentesBI() {

		motivosEstado = new ArrayList<String>();
		motivosEstado.add("Abierto");
		motivosEstado.add("Cerrado");
		motivosEstado.add("Desarollo");
		motivosEstado.add("Rechazado");
		motivosEstado.add("Confirmar");
		motivosEstado.add("En Línea");
		motivosEstado.add("Revisar");
		motivosEstado.add("En Proceso");
		motivosEstado.add("Recurrencia");
		motivosEstado.add("En Validación");
		motivosEstado.add("En Atención");
		motivosEstado.add("Cancelado");
		motivosEstado.add("Revisado");
	}

	/**
	 * Metodo que sirve para obtener la lista de todas las plantillas precargadas para levantar un 
	 * incidente 
	 * 
	 * @return String 
	 */
	public String getTemplates() {

		String url = FRQConstantes.getURLIncidentes()+"?wsdl";
		JsonArray arrayTemplates = new JsonArray();
		String res = "";

		String xmlRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
				+"<soapenv:Header/>"
				+"<soapenv:Body>"
				+"<tem:Templates/>"
				+"</soapenv:Body>"
				+"</soapenv:Envelope>";

		List<Element> templates = null;
		Element resultElement = null;


		try {

			StringBuilder strBuilder = getResponse(url, xmlRequest,"Templates");

			if(strBuilder != null) {			
				//String xmlUTF8 = new  String(strBuilder.toString().getBytes("ISO-8859-1"),"UTF-8");
				//resultElement = getEtiquetaResult(xmlUTF8, "Templates");
				resultElement = getEtiquetaResult(strBuilder.toString(), "Templates");
			}

			if(resultElement != null )
				templates = resultElement.getChildren("ListaPlantillas",Namespace.getNamespace("http://Elektra.com"));

			if(templates != null) {				
				for(Element template : templates) {					
					JsonObject jsonTemplate = new JsonObject();

					jsonTemplate.addProperty("id", template.getChild("InstanceId",Namespace.getNamespace("http://Elektra.com")).getValue());
					jsonTemplate.addProperty("name_template", template.getChild("Template_Name",Namespace.getNamespace("http://Elektra.com")).getValue());
					arrayTemplates.add(jsonTemplate);
				}	
			}

			res = arrayTemplates.toString();
		}catch(Exception e) {
			//System.out.println("Ocurrio algo ");
			res = "[]";
		}

		return "{\"listaTemplates\":"+res+"}";
	}

	public String getDirectores() {

		String url = FRQConstantes.getURLIncidentes()+"?wsdl";
		JsonArray arrayDirectores = new JsonArray();
		String res = "";

		String xmlRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
				+"<soapenv:Header/>"
				+"<soapenv:Body>"
				+"<tem:ConsultaDirectores/>"  				
				+"</soapenv:Body>"
				+"</soapenv:Envelope>";

		List<Element> directores = null;
		Element resultElement = null;

		try {


			StringBuilder strBuilder = getResponse(url, xmlRequest,"ConsultaDirectores");

			if(strBuilder != null) {
				//String xmlUTF8 = new  String(strBuilder.toString().getBytes("ISO-8859-1"),"UTF-8");
				//resultElement = getEtiquetaResult(xmlUTF8,"ConsultaDirectores");
				resultElement = getEtiquetaResult(strBuilder.toString(),"ConsultaDirectores");
			}


			if(resultElement != null )
				directores = resultElement.getChildren("DetalleDirector",Namespace.getNamespace("http://schemas.datacontract.org/2004/07/BE.Common.MethodResults"));

			if(directores != null) {				
				for(Element director : directores) {					
					JsonObject jsonDirector = new JsonObject();
					jsonDirector.addProperty("id", "");
					jsonDirector.addProperty("nombre", director.getChild("Director",Namespace.getNamespace("http://schemas.datacontract.org/2004/07/BE.Common.MethodResults")).getValue());
					arrayDirectores.add(jsonDirector);
				}	
			}

			res = arrayDirectores.toString();
		}catch(Exception e) {
			//System.out.println("Ocurrio algo ");
			res = "[]";
		}

		return "{\"listaDirectores\":"+res+"}";
	}

	public String getInformes(String estatus, String motivo,String fechaInicio, String fechaFin,String director) {

		String url = FRQConstantes.getURLIncidentes()+"?wsdl";
		JsonArray arrayIncidentes = new JsonArray();
		String res = "";


		String xmlRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:elek=\"http://Elektra.com\">"
				+"<soapenv:Header/>"
				+"<soapenv:Body>"
				+   "<tem:ConsultaInformes>"     
				+ "<tem:info>"
				+   "<elek:Estatus>"+estatus+"</elek:Estatus>"
				+   "<elek:MotivoEstatus>"+motivo+"</elek:MotivoEstatus>"
				+   "<elek:FechaIni>"+fechaInicio+"</elek:FechaIni>"
				+   "<elek:FechaFin>"+fechaFin+"</elek:FechaFin>"
				+   "<elek:Director>"+director+"</elek:Director>"
				+"</tem:info>"
				+"</tem:ConsultaInformes>"
				+"</soapenv:Body>"
				+"</soapenv:Envelope>";

		List<Element> incidentes = null;
		Element resultElement = null;

		try {
			//String xmlUTF8 = new String(xmlRequest.getBytes("ISO-8859-1"), "UTF-8");

			//StringBuilder strBuilder = getResponse(url, xmlUTF8,"ConsultaInformes");

			StringBuilder strBuilder = getResponse(url, xmlRequest,"ConsultaInformes");

			if(strBuilder != null){
				//String xmlUTF8response = new  String(strBuilder.toString().getBytes("ISO-8859-1"),"UTF-8");
				//resultElement = getEtiquetaResult(xmlUTF8response,"ConsultaInformes");
				resultElement = getEtiquetaResult(strBuilder.toString(),"ConsultaInformes");
			}
			if(resultElement != null )
				incidentes = resultElement.getChildren("DetalleInforme",Namespace.getNamespace("http://schemas.datacontract.org/2004/07/BE.Common.MethodResults"));

			if(incidentes != null) {
				for(Element incidente: incidentes) {
					JsonObject jsonIncidente = new JsonObject();
					jsonIncidente.addProperty("estatus", incidente.getChild("Estatus",Namespace.getNamespace("http://schemas.datacontract.org/2004/07/BE.Common.MethodResults")).getValue());
					jsonIncidente.addProperty("fechaApertura", incidente.getChild("FechaApertura",Namespace.getNamespace("http://schemas.datacontract.org/2004/07/BE.Common.MethodResults")).getValue());
					jsonIncidente.addProperty("folio", incidente.getChild("Folio",Namespace.getNamespace("http://schemas.datacontract.org/2004/07/BE.Common.MethodResults")).getValue());
					jsonIncidente.addProperty("mensajeError", incidente.getChild("MensajeError",Namespace.getNamespace("http://schemas.datacontract.org/2004/07/BE.Common.MethodResults")).getValue());
					jsonIncidente.addProperty("motivoEstado", incidente.getChild("MotivoEstado",Namespace.getNamespace("http://schemas.datacontract.org/2004/07/BE.Common.MethodResults")).getValue());
					jsonIncidente.addProperty("programador", incidente.getChild("Programador",Namespace.getNamespace("http://schemas.datacontract.org/2004/07/BE.Common.MethodResults")).getValue());

					arrayIncidentes.add(jsonIncidente);
				}
			}
			res = arrayIncidentes.toString();			
		}catch(Exception e) {
			//System.out.println("Ocurrio algo ");
			res = "[]";
		}

		return  "{\"listaIncidentes\":"+res+"}";
	}

	public StringBuilder getResponse(String url, String xmlRequest , String metodo) {

		StringBuilder xmlString = new StringBuilder() ;

		OutputStreamWriter wr = null;
		BufferedReader rd = null;
		HttpURLConnection con = null;

		try {
			URL urlConnection = new URL(url);
			con = (HttpURLConnection) urlConnection.openConnection();

			con.setRequestMethod("POST");
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestProperty("Content-Type", "text/xml; charset=utf-8");		
			con.setRequestProperty("Content-Length", Integer.toString(xmlRequest.length()));
			con.setRequestProperty("Connection", "Keep-Alive");
			con.setRequestProperty("SOAPAction", "http://tempuri.org/IService1/"+metodo);

			con.connect();

			////System.out.println("Peticion al servicio: "+xmlRequest);

			OutputStream outputStream = con.getOutputStream();
			try {
				wr = new OutputStreamWriter(outputStream);
				wr.write(xmlRequest,0,xmlRequest.length());
				wr.flush();

			}finally {
				wr.close();
				wr = null;
			}

			try {
				rd = new BufferedReader(new InputStreamReader(con.getInputStream()));			
			}catch(Exception e) {
				rd = new BufferedReader(new InputStreamReader(con.getErrorStream()));
			}

			String line;
			while((line = rd.readLine()) != null) {
				xmlString.append(line);
			}


		}catch(Exception e) {
			xmlString = null;
		}


		if(xmlString != null) {
			////System.out.println("Respuesta del servicio SOA: \n"+xmlString.toString());
		}else{
			////System.out.println("Respuesta del servicio SOA: \n");
		}


		return xmlString;
	}

	public Element getEtiquetaResult(String respuestaXml,String metodo) {

		Element result = null;
		try {
			//String cadena = respuestaXml.toString();
			StringReader strReader = new StringReader(respuestaXml);

			SAXBuilder builder = new SAXBuilder();
			Document document = builder.build(strReader);

			Element rootNode = document.getRootElement();

			Element body = rootNode.getChild("Body",Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));

			Element response = body.getChild(metodo+"Response",Namespace.getNamespace("http://tempuri.org/"));

			result = response.getChild(metodo+"Result",Namespace.getNamespace("http://tempuri.org/"));

		}catch(Exception e) {
			//System.out.println("Ocurrio algo");
			result = null;
		}


		return result;
	}

	public String getIncidentesPorMotivo(String fechaInicio, String fechaFin,String motivoEstatus){

		String res = "";
		String colores [] = {"#E64C65","#0093FF","#049571","#FCB150","#FF00DF","#FFF300","#1CE9ED","#8A1F30","#11A8AB","#BFBFBF","#1E759B","#B19673","#4FC4F6"};

		try {
			JsonParser parser = new JsonParser();       
			JsonElement jsonTree = parser.parse(getInformes("", motivoEstatus, fechaInicio, fechaFin, ""));
			JsonObject informes = jsonTree.getAsJsonObject();

			JsonArray arrayIncidentes = informes.get("listaIncidentes").getAsJsonArray();

			int incidentes=0;
			int totalPorMotivo;
			int totalG=0;

			JsonArray arrayMotivos = new JsonArray();

			if(motivosEstado!=null) {			
				for (String motivo : motivosEstado) {
					totalPorMotivo = 0;

					for (JsonElement jsonElement : arrayIncidentes) {
						JsonObject incidente = jsonElement.getAsJsonObject();

						String motivoIncidente = incidente.get("motivoEstado").getAsString();

						if(motivoIncidente.equals(motivo)) 
							totalPorMotivo++;

					} 

					if(totalPorMotivo > 0 ) {						
						totalG=totalG+totalPorMotivo;

						JsonObject motivoJson = new JsonObject();
						motivoJson.addProperty("motivoEstado", motivo);
						motivoJson.addProperty("totalIncidentes", totalPorMotivo);

						motivoJson.addProperty("color", colores[incidentes]);
						arrayMotivos.add(motivoJson);
						incidentes++;
					}
				}


				float sumaPor=0f;
				for (int i=0; i<incidentes; i++) {
					JsonObject aux = new JsonObject();
					aux=(JsonObject) arrayMotivos.get(i);
					int total= Integer.parseInt(aux.get("totalIncidentes").toString());
					float calculo = (float) ((total * 100.0) / totalG);
					sumaPor = sumaPor +calculo;

					aux.addProperty("porcentaje", calculo);
					aux.addProperty("totalIncidentesGeneral", totalG);
					aux.addProperty("sumaPor", sumaPor);
				}

				res = arrayMotivos.toString();
			}
		}catch(Exception e) {
			res="[]";
		}
		//logger.info(res);

		return "{\"totalesMotivo\":"+res+"}";
	}

	public String getIncidentesByDirector(String fechaInicio, String fechaFin,String motivoEstatus) {
		String res = "";
		String colores [] = {"#E64C65","#0093FF","#049571","#FCB150","#FF00DF","#FFF300","#1CE9ED","#8A1F30","#11A8AB","#BFBFBF","#1E759B","#B19673","#4FC4F6"};


		
		//Comentario 
		
		try {
			JsonParser parser = new JsonParser();       
			JsonElement jsonTree = parser.parse(getInformes("", motivoEstatus, fechaInicio, fechaFin, ""));
			JsonObject informes = jsonTree.getAsJsonObject();

			JsonArray arrayIncidentes = informes.get("listaIncidentes").getAsJsonArray();

			int incidentes=0;
			int coloresCont =0;
			int totalPorGerente;
			int totalG=0;


			JsonArray arrayDirectores = new JsonArray();
			
			for(int i=0; i < arrayIncidentes.size(); i++) {	
				
				String directorStr = arrayIncidentes.get(i).getAsJsonObject().get("programador").getAsString();
				
				totalPorGerente =1;
				//Busca en directores acumulado
				
				boolean existeDirector = false;
				
				for(int k=0; k < arrayDirectores.size(); k++) {
					
					String directorAcumulado =  arrayDirectores.get(k).getAsJsonObject().get("motivoEstado").getAsString();
					if(directorAcumulado.equals(directorStr)) {
						existeDirector=true;
						break;
					}
						
				}
				
				//
				if (!existeDirector) {
					for(int j=i+1; j < arrayIncidentes.size(); j++) {
						
						String directorStr2= arrayIncidentes.get(j).getAsJsonObject().get("programador").getAsString();
						
						if(directorStr.equals(directorStr2))
							totalPorGerente++;
					}
					
					if(totalPorGerente > 0 ) {						
						totalG=totalG+totalPorGerente;
						
						JsonObject acumuladoDirector = new JsonObject();
						acumuladoDirector.addProperty("motivoEstado", directorStr);
						acumuladoDirector.addProperty("totalIncidentes", totalPorGerente);
						
						if(incidentes > colores.length ) {
							coloresCont=0;
						}
						
						acumuladoDirector.addProperty("color", colores[coloresCont]);
						
						arrayDirectores.add(acumuladoDirector);
						incidentes++;
						coloresCont++;
					}	
				}
				
			}			
		
			float sumaPor=0f;
			for (int i=0; i<incidentes; i++) {
				JsonObject aux = new JsonObject();
				aux=(JsonObject) arrayDirectores.get(i);
				int total= Integer.parseInt(aux.get("totalIncidentes").toString());
				float calculo = (float) ((total * 100.0) / totalG);
				sumaPor = sumaPor +calculo;

				aux.addProperty("porcentaje", calculo);
				aux.addProperty("totalIncidentesGeneral", totalG);
				aux.addProperty("sumaPor", sumaPor);
			}

			res = arrayDirectores.toString();

		}catch(Exception e) {
			res="[]";
		}
		//logger.info(res);

		return "{\"totalesMotivo\":"+res+"}";

	}

	public String createIncidente(IncidenteDTO incidenteIn,AdjuntoDTO adjunto){

		String url = FRQConstantes.getURLIncidentes()+"?wsdl";

		String xmlRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:elek=\"http://Elektra.com\">" + 
				"<soapenv:Header/>" + 
				"<soapenv:Body>" + 
				"<tem:CreateIncident>"+
				"<tem:parameter>"+ 						
				"<elek:EmployeeBySucursal>" + 
				//"<elek:Sucursal>2244</elek:Sucursal>" + 
				"<elek:Sucursal>"+incidenteIn.getSucursal()+"</elek:Sucursal>" +
				"<elek:Nombre></elek:Nombre>" + 
				"<elek:Apellidos></elek:Apellidos>" + 
				"<elek:GrupoAsignado></elek:GrupoAsignado>" + 
				"<elek:UsuarioAsignado></elek:UsuarioAsignado>" + 
				"<elek:CorporateID></elek:CorporateID>" + 
				"<elek:Company></elek:Company>" + 
				"<elek:LoginID></elek:LoginID>" + 
				"<elek:TemplateId>"+incidenteIn.getIdPlantilla()+"</elek:TemplateId>" + 
				"<elek:Director>"+incidenteIn.getDirector()+"</elek:Director>" + 
				"<elek:Gerente></elek:Gerente>"+
				"<elek:Programador></elek:Programador>"+
				"<elek:Gerencia></elek:Gerencia>"+
				"<elek:TipoDeError>"+incidenteIn.getTipoError()+"</elek:TipoDeError>"+
	            "<elek:Prioridad>"+incidenteIn.getPrioridad()+"</elek:Prioridad>"+
				"</elek:EmployeeBySucursal>" + 
				"<elek:Descripcion>"+incidenteIn.getDescripcion()+"</elek:Descripcion>" + 
				"<elek:Resumen>Incidente Franquicia</elek:Resumen>" + 
				"<elek:N1Operacion></elek:N1Operacion>" + 
				"<elek:N2Operacion></elek:N2Operacion>" + 
				"<elek:N3Operacion></elek:N3Operacion>" + 
				"<elek:N1Producto></elek:N1Producto>" + 
				"<elek:N2Producto></elek:N2Producto>" + 
				"<elek:N3Producto></elek:N3Producto>" + 
				"<elek:IncidentID></elek:IncidentID>" +  
			
				"</tem:parameter>" + 
				"</tem:CreateIncident>" + 
				"</soapenv:Body>" + 
				"</soapenv:Envelope>";

		Element resultElement = null;
		String succesfulOperation = "";

		Element incidente=null;

		String incidenteCreado = "";

		String res = "";
		boolean archivoAdjunto = false;

		try {

			//String xmlUTF8 = new String(xmlRequest.getBytes("UTF-8"), "ISO-8859-1");

			if(incidenteIn.getId().equals("")) {
				//StringBuilder respuestaXml = getResponse(url, xmlUTF8,"CreateIncident");
				StringBuilder respuestaXml = getResponse(url, xmlRequest,"CreateIncident");

				if(respuestaXml != null) {
					resultElement = getEtiquetaResult(respuestaXml.toString(), "CreateIncident");					
				}

				if(resultElement != null) {
					succesfulOperation = resultElement.getChild("Result",Namespace.getNamespace("http://Elektra.com")).getChild("SuccessfulOperation",Namespace.getNamespace("http://Elektra.com")).getValue();
					incidente = resultElement.getChild("Incident",Namespace.getNamespace("http://Elektra.com"));
				}

				if(succesfulOperation.equals("true")) {
					if(incidente != null) {
						incidenteCreado = incidente.getChild("IncidentID",Namespace.getNamespace("http://Elektra.com")).getValue();
						incidenteIn.setId(incidenteCreado);
					}
				}else {				
					incidenteCreado = "";
				}
			}else {
				succesfulOperation="true";				
			}

			//Adjunta Archivo

			if(succesfulOperation.equals("true")) {
				if(incidenteIn.isAdjunto()) {				
					//Elimina la siguiente linea es de prueba
					//incidenteIn.setId("INC000006301580");
					adjunto.setIdIncidente(incidenteIn.getId());
					archivoAdjunto = cargaAdjunto(adjunto);
				}
			}

			res="{\"succesful\":"+succesfulOperation
					+",\"idIncidente\":\""+incidenteIn.getId()+"\""
					+",\"adjunto\":"+archivoAdjunto
					+"}";


		}catch(Exception e) {
			res = "";
		}

		return res;
	}

	public boolean cargaAdjunto(AdjuntoDTO adjunto){

		String url = FRQConstantes.getURLIncidentes()+"?wsdl";

		boolean res= false;

		String xmlRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
				+"<soapenv:Header/>"
				+"<soapenv:Body>"
				+"<tem:Adjuntos>"
				+"<tem:NoIncidente>"+adjunto.getIdIncidente()+"</tem:NoIncidente>"
				+"<tem:NombreArchivo>"+adjunto.getNombreArchivo()+"</tem:NombreArchivo>"
				+"<tem:archivobytes>"+adjunto.getArchivoBytes()+"</tem:archivobytes>"
				+"<tem:numBytesRead>"+adjunto.getNumBytesRead()+"</tem:numBytesRead>"
				+"</tem:Adjuntos>"
				+"</soapenv:Body>"
				+"</soapenv:Envelope>";

		Element resultElement = null;

		try {
			StringBuilder xmlRespuesta = getResponse(url, xmlRequest, "Adjuntos");

			if(xmlRespuesta != null)
				resultElement = getEtiquetaResult(xmlRespuesta.toString(), "Adjuntos");

			if(resultElement != null) {
				String strResult = resultElement.getValue();
				res = true;
			}


		}catch(Exception e) {
			res = false;
		}

		return res;

	}

	public void main(String[]args ) {
		////System.out.println(getInformes("", "", "2018-05-11 00:00:00", "2018-05-11 23:59:59", ""));

		//IncidentesBI incidentes = new IncidentesBI();

		////System.out.println(incidentes.getIncidentesPorMotivo("2018-05-11 00:00:00", "2018-05-22 23:59:59"));


		IncidenteDTO incidenteDTO = new IncidenteDTO();
		incidenteDTO.setIdPlantilla("AGGFNIR6D9563ANG2PX4BJ0TPY8JCS");
		incidenteDTO.setDescripcion("Prueba incidente servicio - Franquicia");
		incidenteDTO.setDirector("REYES NAVA SOLANO");
		//incidenteDTO.setId("INC000006301580");
		incidenteDTO.setId("");
		incidenteDTO.setAdjunto(true);

		//Prueba convertir en Base64 

		String base="";

		AdjuntoDTO adjunto = new AdjuntoDTO();
		adjunto.setArchivoBytes(base);
		adjunto.setNombreArchivo("portabilidad.jpg");
		//adjunto.setIdIncidente("INC000006301580");
		adjunto.setNumBytesRead(12309);

		//System.out.println(createIncidente(incidenteDTO,adjunto));

	}
	
}


