package com.gruposalinas.checklist.resources;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class FRQAppContextProvider implements ApplicationContextAware{

	private static ApplicationContext applicationContext = null;
	
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}
	
	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		FRQAppContextProvider.applicationContext = ctx;
	}
	
}
