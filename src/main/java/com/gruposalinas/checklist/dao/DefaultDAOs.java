package com.gruposalinas.checklist.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class DefaultDAOs {

    public DefaultDAOs() {
        super();
    }

    @Autowired
    JdbcTemplate frqJdbcTemplate;

    public JdbcTemplate getFrqJdbcTemplate() {
        return frqJdbcTemplate;
    }

    public void setFrqJdbcTemplate(JdbcTemplate frqJdbcTemplate) {
        this.frqJdbcTemplate = frqJdbcTemplate;
    }

    public void close() {
        try {
            if (!frqJdbcTemplate.getDataSource().getConnection().isClosed()) {
                frqJdbcTemplate.getDataSource().getConnection().close();
            }
        } catch (Exception e) {

            //e.printStackTrace();
        }

    }

}
