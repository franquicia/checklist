package com.gruposalinas.checklist.servicios.servidor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gruposalinas.checklist.business.HallazgosEviExpBI;
import com.gruposalinas.checklist.business.HallazgosExpBI;
import com.gruposalinas.checklist.business.PerfilUsuarioBI;
import com.gruposalinas.checklist.business.SucursalChecklistBI;
import com.gruposalinas.checklist.domain.HallazgosEviExpDTO;
import com.gruposalinas.checklist.domain.HallazgosExpDTO;
import com.gruposalinas.checklist.domain.PerfilUsuarioDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;

@Controller
@RequestMapping("/consultaHallazgoService")
public class ConsultaHallazgosService {

	private static final Logger logger = LogManager.getLogger(ConsultaHallazgosService.class);

	@Autowired
	SucursalChecklistBI sucursalChecklistBI;
	
	@Autowired
	HallazgosExpBI hallazgosExpBI;

	@Autowired
	HallazgosEviExpBI hallazgosEviExpBI;

	@Autowired
	PerfilUsuarioBI perfilUsuarioBI;

	//http://localhost:8080/checklist/consultaHallazgoService/getListaHallazgos.json?idCeco=999995
	@RequestMapping(value = "/getListaHallazgos", method = RequestMethod.GET)
	public @ResponseBody List<HallazgosExpDTO> getListaHallazgos(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") int idCeco) throws ServletException, IOException {

		List<HallazgosExpDTO> listaHallazgos = null;
		listaHallazgos = hallazgosExpBI.obtieneDatos(idCeco);
		List<SucursalChecklistDTO> sucursal = sucursalChecklistBI.obtieneDatosSuc(""+idCeco);
	
		int version = 0;
		version = sucursal.get(0).getAux();
		if(version==1) {
			listaHallazgos = getHallazgosConcatenadas(listaHallazgos);
		}
		
		logger.info("getListaHallazgos-idCeco:"+idCeco+" result:"+listaHallazgos);
		return listaHallazgos;
	}
	//getHistorialHallazgo
	//http://localhost:8080/checklist/consultaHallazgoService/getListaHallazgosHistorico.json?idCeco=999995&idHallazgo=2921
	@RequestMapping(value = "/getListaHallazgosHistorico", method = RequestMethod.GET)
	public @ResponseBody Map<String, List> getListaHallazgosHistorico(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") int idCeco,
			@RequestParam(value = "idHallazgo", required = true, defaultValue = "") int idHallazgo
			) throws ServletException, IOException {
		List<List> listasCursor = new ArrayList<>();
		List<HallazgosExpDTO> listaHallazgos = new ArrayList<HallazgosExpDTO>();
		List<HallazgosEviExpDTO> listaEvidencias = new ArrayList<HallazgosEviExpDTO>();
		ArrayList<String> listaEvidenciasAnteriores = new ArrayList<String>();
		List<HallazgosExpDTO> lista = new ArrayList<>();

		String imagen = "";
		String ruta = "";
		

		int version = 0;
		List<SucursalChecklistDTO> sucursal = sucursalChecklistBI.obtieneDatosSuc(""+idCeco);
		version = sucursal.get(0).getAux();
		
		listaHallazgos = hallazgosExpBI.obtieneDatos(idCeco);
		if(version==1) {
			listaHallazgos = getHallazgosConcatenadas(listaHallazgos);
		}
		listaEvidencias = hallazgosEviExpBI.obtieneDatosBita(idCeco);
		lista = hallazgosExpBI.obtieneHistFolio(idHallazgo);
		
		// OBTENGO EVIDENCIA MULTIPLE PARA PRIMER HALLAZGO
		if(listaEvidencias!=null && listaEvidencias.size()>0) {
			for (HallazgosEviExpDTO objEvidencia : listaEvidencias) {
				if (objEvidencia.getIdHallazgo() == idHallazgo) {
					ruta = objEvidencia.getRuta();
					listaEvidenciasAnteriores.add(FRQConstantes.getRutaImagen()+ruta);
				}
			}
		}
		/*ArrayList<HallazgosExpDTO> objeto = new ArrayList<>();
		if(lista!=null && lista.size()>0) {
			objeto.add(lista.get(0));
			lista.remove(0);
		}*/
		List<String> rutas = new ArrayList<>();
		rutas.add(FRQConstantes.getRutaImagen());
		
		Map<String, List> hasmap = new HashMap<>();
		hasmap.put("listaEvidenciasAnteriores", listaEvidenciasAnteriores);
		hasmap.put("lista", lista);
		hasmap.put("ruta", rutas);
		
		
		logger.info("getListaHallazgosHistorico-idCeco:"+idCeco+"-idHallazgo:"+idHallazgo+" result:"+hasmap);
		return hasmap;
	}

	//http://localhost:8080/checklist/consultaHallazgoService/getHallazgoDetalle.json?idCeco=999995&idHallazgo=2921
	@RequestMapping(value = "/getHallazgoDetalle", method = RequestMethod.GET)
	public @ResponseBody Map<String, List> getHallazgoDetalle(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") int idCeco,
			@RequestParam(value = "idHallazgo", required = true, defaultValue = "") int idHallazgo
			) throws ServletException, IOException {

		List<HallazgosExpDTO> listaHallazgos = new ArrayList<HallazgosExpDTO>();
		List<HallazgosEviExpDTO> listaEvidencias = new ArrayList<HallazgosEviExpDTO>();
		List<HallazgosEviExpDTO> evidencias = new ArrayList<HallazgosEviExpDTO>();
		List<HallazgosExpDTO> hallazgo = new ArrayList<>();
		
		Map<String, List> retorno = new HashMap<>();
		
		int version = 0;
		List<SucursalChecklistDTO> sucursal = sucursalChecklistBI.obtieneDatosSuc(""+idCeco);
		version = sucursal.get(0).getAux();
		
		listaHallazgos = hallazgosExpBI.obtieneDatos(idCeco);
		listaEvidencias = hallazgosEviExpBI.obtieneDatosBita(idCeco);
		
		if(version==1) {
			listaHallazgos = getHallazgosConcatenadas(listaHallazgos);
		}
		if(listaHallazgos!=null && listaHallazgos.size()>0) {
			for (HallazgosExpDTO objPreg : listaHallazgos) {

				if (objPreg.getIdHallazgo() == idHallazgo) {
					hallazgo.add(objPreg);
					break;
				}

			}
		}
		if(listaEvidencias!=null && listaEvidencias.size()>0) {
			for (HallazgosEviExpDTO objEvidencia : listaEvidencias) {
	
				if (objEvidencia.getIdHallazgo() == idHallazgo) {
					evidencias.add(objEvidencia);
				}
	
			}
		}
		retorno.put("hallazgo", hallazgo);
		retorno.put("evidencias", evidencias);
		
		logger.info("getHallazgoDetalle-idCeco:"+idCeco+"-idHallazgo:"+idHallazgo+" result:"+retorno);
		return retorno;
	}
	

	
	
	//http://localhost:8080/checklist/consultaHallazgoService/obtieneDireccion.json
	@RequestMapping(value = "/obtieneDireccion", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> obtieneDireccion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Map<String, String> retorno = new HashMap<>();

		retorno.put("direccion", FRQConstantes.getRutaImagen());
		

		logger.info("obtieneDireccion: "+retorno);
		return retorno;
	}
	
	//http://localhost:8080/checklist/consultaHallazgoService/validaPerfilhallazgo.json?idUsuario=73610
	//retorno 0 = sin perfil, 1 = atencion, 2 = autorizador 
	@RequestMapping(value = "/validaPerfilhallazgo", method = RequestMethod.GET)
	public @ResponseBody  Map<String, Integer> validaPerfilhallazgo(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idUsuario", required = true, defaultValue = "0") String idUsuario) throws ServletException, IOException {
		Map<String, Integer> hasMapRetorno = new HashMap<>();
		List<PerfilUsuarioDTO> listaPerfiles=null;
		int retorno = 0;

		if (!idUsuario.equalsIgnoreCase("0")) {
			listaPerfiles = perfilUsuarioBI.obtienePerfiles(idUsuario, FRQConstantes.PERFIL_EXPANSION_AUTORIZACION);
			if(listaPerfiles!=null && listaPerfiles.size()>0) {
				retorno = 2;
				//perfil=FRQConstantes.PERFIL_EXPANSION_AUTORIZACION;
			}
			else {
				listaPerfiles = perfilUsuarioBI.obtienePerfiles(idUsuario, FRQConstantes.PERFIL_EXPANSION_ATIENDE);
				if(listaPerfiles!=null && listaPerfiles.size()>0) {
					//perfil=FRQConstantes.PERFIL_EXPANSION_ATIENDE;
					retorno = 1;
				}
				else {
					retorno = 0;
				}
			}
		}
		hasMapRetorno.put("estado", retorno);
		

		logger.info("validaPerfilhallazgo-idUsuario:"+idUsuario+" result:"+hasMapRetorno);
		return hasMapRetorno;
	}
	
	//===========================================================================
	public List<HallazgosExpDTO> getHallazgosConcatenadas(List<HallazgosExpDTO> lista) {

		List<HallazgosExpDTO> chklistPreguntas = new ArrayList<HallazgosExpDTO>();
		List<HallazgosExpDTO> listaResult = new ArrayList<HallazgosExpDTO>();

		listaResult.clear();

		chklistPreguntas = lista;

		for (int i = 0; i < chklistPreguntas.size(); i++) {

			if (chklistPreguntas.get(i).getPregPadre() == 0) {

				for (int j = 0; j < chklistPreguntas.size(); j++) {

					if (chklistPreguntas.get(i).getIdPreg() == chklistPreguntas.get(j).getPregPadre()) {

						HallazgosExpDTO data = chklistPreguntas.get(j);
						data.setPreg(chklistPreguntas.get(i).getPreg() + " : " + chklistPreguntas.get(j).getPreg());
						listaResult.add(data);
					}
				}
			}
		}

		return listaResult;

	}

}
