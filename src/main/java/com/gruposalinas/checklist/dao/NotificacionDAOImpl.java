package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ChecklistPilaDTO;
import com.gruposalinas.checklist.domain.MovilInfoDTO;
import com.gruposalinas.checklist.mappers.NotificacionPorcentajeRowMapper;
import com.gruposalinas.checklist.mappers.UsuarioNotificacionesRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class NotificacionDAOImpl extends DefaultDAO implements NotificacionDAO {

    private static final Logger logger = LogManager.getLogger(ChecklistDAOImpl.class);

    private DefaultJdbcCall jdbcNotificacionPorcentaje;
    private DefaultJdbcCall jdbcUsuarioNoti;

    public void init() {

        jdbcNotificacionPorcentaje = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PANOTIFICACION")
                .withProcedureName("SPREPORTEMOVIL")
                .returningResultSet("RCL_REPORTE", new NotificacionPorcentajeRowMapper());

        jdbcUsuarioNoti = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PANOTIFICACION")
                .withProcedureName("SPNOTIFICACION")
                .returningResultSet("RCL_USUARIOS", new UsuarioNotificacionesRowMapper());
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> notificacionPorcentaje(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<ChecklistPilaDTO> listaReporte = null;
        int respuesta = 0;
        int porcentaje = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUA", idUsuario);

        out = jdbcNotificacionPorcentaje.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAPRUEBA.SPREPORTEMOVIL}");

        listaReporte = (List<ChecklistPilaDTO>) out.get("RCL_REPORTE");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        BigDecimal idreturn = (BigDecimal) out.get("PA_PORCENTAJE");
        porcentaje = idreturn.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno ERRROR");
        }

        lista = new HashMap<String, Object>();
        lista.put("listaReporte", listaReporte);
        lista.put("porcentaje", porcentaje);
        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<MovilInfoDTO> obtieneUsuariosNoti(String so) throws Exception {

        Map<String, Object> out = null;
        List<MovilInfoDTO> listaUsuarios = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_SO", so);
        out = jdbcUsuarioNoti.execute(in);

        logger.info("Funcion ejecutada: {checklist.PANOTIFICACION.SPNOTIFICACION}");

        listaUsuarios = (List<MovilInfoDTO>) out.get("RCL_USUARIOS");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        logger.info("Lista: " + listaUsuarios);
        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Usuarios");
        } else {
            return listaUsuarios;
        }

        return listaUsuarios;
    }

}
