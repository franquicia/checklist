package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.EvidenciaDTO;
import com.gruposalinas.checklist.domain.RespuestaAdDTO;
import com.gruposalinas.checklist.domain.RespuestaDTO;
import com.gruposalinas.checklist.mappers.ArbolDecisionRowMapper2;
import com.gruposalinas.checklist.mappers.EvidenciaRowMapper;
import com.gruposalinas.checklist.mappers.RespuestaAdRowMapper;
import com.gruposalinas.checklist.mappers.RespuestaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ArbolRespAdiEviAdmDAOImpl extends DefaultDAO implements ArbolRespAdiEviAdmDAO {

    private static Logger logger = LogManager.getLogger(ArbolRespAdiEviAdmDAOImpl.class);

    private DefaultJdbcCall insertaArbolDecision;
    private DefaultJdbcCall actualizaArbolDecision;
    private DefaultJdbcCall eliminaArbolDecision;
    private DefaultJdbcCall buscaChecklist;
    private DefaultJdbcCall cambiaRespuestas;

    //RESPUESTAS
    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneRespuesta;
    DefaultJdbcCall jdbcInsertaRespuesta;
    DefaultJdbcCall jdbcInsertaUnaResp;
    DefaultJdbcCall jdbcInsertaNuevaResp;
    DefaultJdbcCall jdbcActualizaFechaResp;
    DefaultJdbcCall jdbcActualizaRespuesta;
    DefaultJdbcCall jdbcEliminaRespuesta;
    DefaultJdbcCall jdbcEliminaRespuestas;
    DefaultJdbcCall jdbcEliminaRespuestasD;
    DefaultJdbcCall jdbcRegistraRespuestas;
    //ADICIONALES
    private DefaultJdbcCall jdbcBuscaRespAD;
    private DefaultJdbcCall jdbcInsertaRespAD;
    private DefaultJdbcCall jdbcActualizaRespAD;
    private DefaultJdbcCall jdbcEliminaRespAD;
    //EVIDENCIAS
    DefaultJdbcCall jdbcObtieneEvidencias;
    DefaultJdbcCall jdbcObtieneEvidencia;
    DefaultJdbcCall jdbcInsertaEvidencia;
    DefaultJdbcCall jdbcActualizaEvidencia;
    DefaultJdbcCall jdbcEliminaEvidencia;
    //DEPURA TABLAS
    DefaultJdbcCall jdbcDepuraTabs;
    DefaultJdbcCall jdbcDepuraTabParam;

    public void init() {

        insertaArbolDecision = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ARBOL_DESADM")
                .withProcedureName("SP_INS_ARBOL_DES");

        actualizaArbolDecision = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ARBOL_DESADM")
                .withProcedureName("SP_ACT_ARBOL_DES");

        eliminaArbolDecision = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ARBOL_DESADM")
                .withProcedureName("SP_DEL_ARBOL_DES");

        buscaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ARBOL_DESADM")
                .withProcedureName("SP_SEL_G_ARBOL_DES")
                .returningResultSet("RCL_ARBOL_DES", new ArbolDecisionRowMapper2());

        cambiaRespuestas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPOSIBLESIDS")
                .withProcedureName("SPCAMBIARESPUESTAS");

        //RESPUESTAS
        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_RESP_ADM")
                .withProcedureName("SP_SEL_G_RESP")
                .returningResultSet("RCL_RESP", new RespuestaRowMapper());

        jdbcObtieneRespuesta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_RESP_ADM")
                .withProcedureName("SP_SEL_RESP")
                .returningResultSet("RCL_RESP", new RespuestaRowMapper());

        jdbcInsertaRespuesta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_RESP_ADM")
                .withProcedureName("SP_INS_RESP");

        jdbcInsertaUnaResp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPUESTA_ADM")
                .withProcedureName("SP_REGISTRA_PREGUNTA");

        jdbcInsertaNuevaResp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPUESTA_ADM")
                .withProcedureName("SP_NUEVA_RESP");

        jdbcActualizaRespuesta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_RESP_ADM")
                .withProcedureName("SP_ACT_RESP");

        jdbcActualizaFechaResp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_RESP_ADM")
                .withProcedureName("SP_ACT_F_RESP");

        jdbcEliminaRespuesta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_RESP_ADM")
                .withProcedureName("SP_DEL_RESP");

        jdbcEliminaRespuestas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPUESTA_ADM")
                .withProcedureName("SP_ELIMINA_RESP");

        jdbcEliminaRespuestasD = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPUESTA_ADM")
                .withProcedureName("SPELIMINADUPLICADOS");

        jdbcRegistraRespuestas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKLIST")
                .withProcedureName("SP_REGISTA_RPTAS");

        //ADICIONALES
        jdbcBuscaRespAD = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPAD_ADM")
                .withProcedureName("SP_SEL_RESPAD")
                .returningResultSet("RCL_RESP", new RespuestaAdRowMapper());

        jdbcInsertaRespAD = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPAD_ADM")
                .withProcedureName("SP_INS_RESPAD");

        jdbcActualizaRespAD = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPAD_ADM")
                .withProcedureName("SP_ACT_RESPAD");

        jdbcEliminaRespAD = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPAD_ADM")
                .withProcedureName("SP_DEL_RESPAD");
        //DEPURA TABLAS
        jdbcDepuraTabs = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPAD_ADM")
                .withProcedureName("SP_DEPURATAB");

        jdbcDepuraTabParam = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPAD_ADM")
                .withProcedureName("SP_DEPXCHECK");

        //EVIDENCIAS
        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_EVID_ADM")
                .withProcedureName("SP_SEL_G_EVID")
                .returningResultSet("RCL_EVID", new EvidenciaRowMapper());

        jdbcObtieneEvidencia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_EVID_ADM")
                .withProcedureName("SP_SEL_EVID")
                .returningResultSet("RCL_EVID", new EvidenciaRowMapper());

        jdbcInsertaEvidencia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_EVID_ADM")
                .withProcedureName("SP_INS_EVID_N");

        jdbcActualizaEvidencia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_EVID_ADM")
                .withProcedureName("SP_ACT_EVID");

        jdbcEliminaEvidencia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_EVID_ADM")
                .withProcedureName("SP_DEL_EVID");

    }

    public int insertaArbolDecision(ArbolDecisionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int id = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_CHECK", bean.getIdCheckList())
                .addValue("PA_ID_PREG", bean.getIdPregunta())
                .addValue("PA_RESPUESTA", bean.getRespuesta())
                .addValue("PA_ESTATUS_E", bean.getEstatusEvidencia())
                .addValue("PA_ORDENPREG", bean.getOrdenCheckRespuesta())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_REQACCION", bean.getReqAccion())
                .addValue("PA_REQOBS", bean.getReqObservacion())
                .addValue("PA_OBLIGA", bean.getReqOblig())
                .addValue("PA_DESC_E", bean.getDescEvidencia())
                .addValue("PA_PONDERACION", bean.getPonderacion())
                .addValue("PA_PLANTILLA", bean.getIdPlantilla())
                .addValue("PA_PROTOCOLO", bean.getIdProtocolo());

        //System.out.println(bean.getPonderacion());
        out = insertaArbolDecision.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PA_ARBOL_DESADM.SP_INS_ARBOL_DES}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal returnId = (BigDecimal) out.get("PA_FIID_ARBOL_DES");
        id = returnId.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar arbol de decision");
        } else {
            return id;
        }

        return 0;
    }

    public boolean actualizaArbolDecision(ArbolDecisionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_ARBOL_DES", bean.getIdArbolDesicion())
                .addValue("PA_ID_CHECK", bean.getIdCheckList())
                .addValue("PA_ID_PREG", bean.getIdPregunta())
                .addValue("PA_RESPUESTA", bean.getRespuesta())
                .addValue("PA_ESTATUS_E", bean.getEstatusEvidencia())
                .addValue("PA_ORDENPREG", bean.getOrdenCheckRespuesta())
                .addValue("PA_REQACCION", bean.getReqAccion())
                .addValue("PA_REQOBS", bean.getReqObservacion())
                .addValue("PA_OBLIGA", bean.getReqOblig())
                .addValue("PA_DESC_E", bean.getDescEvidencia())
                .addValue("PA_PONDERACION", bean.getPonderacion())
                .addValue("PA_PLANTILLA", bean.getIdPlantilla())
                .addValue("PA_PROTOCOLO", bean.getIdProtocolo());

        out = actualizaArbolDecision.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PA_ARBOL_DESADM.SP_ACT_ARBOL_DES}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar arbol de decision");
        } else {
            return true;
        }

        return false;

    }

    public boolean eliminaArbolDecision(int idArbolDecision) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_ARBOL_DES", idArbolDecision);

        out = eliminaArbolDecision.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PA_ARBOL_DESADM.SP_DEL_ARBOL_DES}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al eliminar arbol de decision");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<ArbolDecisionDTO> buscaArbolDecision(int idChecklist) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<ArbolDecisionDTO> listaArbolDecision = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_CHECKLIST", idChecklist);

        out = buscaChecklist.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PA_ARBOL_DESADM.SP_SEL_G_ARBOL_DES}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listaArbolDecision = (List<ArbolDecisionDTO>) out.get("RCL_ARBOL_DES");

        if (error == 1) {
            logger.info("Algo ocurrió al eliminar arbol de decision");
        } else {
            return listaArbolDecision;
        }

        return null;
    }

    @Override
    public boolean cambiaRespuestasPorId() throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        out = cambiaRespuestas.execute();

        logger.info("Funci�n ejecutada:{checklist.PAPOSIBLESIDS.SPCAMBIARESPUESTAS}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al cambiar las respuestas por id Posibles");
        } else {
            return true;
        }

        return false;
    }

    //RESPUESTAS
    @SuppressWarnings("unchecked")
    public List<RespuestaDTO> obtieneRespuesta() throws Exception {
        Map<String, Object> out = null;
        List<RespuestaDTO> listaRespuesta = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funcion ejecutada: {checklist.PA_RESP_ADM.SP_SEL_G_RESP}");

        listaRespuesta = (List<RespuestaDTO>) out.get("RCL_RESP");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener las Respuestas");
        } else {
            return listaRespuesta;
        }

        return listaRespuesta;

    }

    @SuppressWarnings("unchecked")
    public List<RespuestaDTO> obtieneRespuesta(String idArbol, String idRespuesta, String idBitacora) throws Exception {
        Map<String, Object> out = new HashMap<String, Object>();
        List<RespuestaDTO> listaRespuesta = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_ARBOL", idArbol)
                .addValue("PA_IDRESPUESTA", idRespuesta)
                .addValue("PA_IDBITACORA", idBitacora);

        logger.info(idArbol + " " + idRespuesta + " " + idBitacora + "  dd ");

        out = jdbcObtieneRespuesta.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_RESP_ADM.SP_SEL_RESP}");

        listaRespuesta = (List<RespuestaDTO>) out.get("RCL_RESP");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener las Respuestas");
        } else {
            return listaRespuesta;
        }

        return null;
    }

    public int insertaRespuesta(RespuestaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idRespuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_USUARIO", bean.getIdCheckUsuario())
                .addValue("PA_ID_BITACORA", bean.getIdBitacora())
                .addValue("PA_ID_ARBOL_DES", bean.getIdArboldecision())
                .addValue("PA_OBSERVACION", bean.getObservacion())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcInsertaRespuesta.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_RESP_ADM.SP_INS_RESP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDRESP");
        idRespuesta = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar la Respuesta");
        } else {
            return idRespuesta;
        }

        return idRespuesta;

    }

    public boolean actualizaRespuesta(RespuestaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_RESP", bean.getIdRespuesta())
                .addValue("PA_ID_USUARIO", bean.getIdCheckUsuario())
                .addValue("PA_ID_BITACORA", bean.getIdBitacora())
                .addValue("PA_ID_ARBOL_DES", bean.getIdArboldecision())
                .addValue("PA_OBSERVACION", bean.getObservacion());

        out = jdbcActualizaRespuesta.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_RESP_ADM.SP_ACT_RESP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar la Respuesta  id( " + bean.getIdRespuesta() + ")");
        } else {
            return true;
        }

        return false;

    }

    public boolean eliminaRespuesta(int idResp) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_RESP", idResp);

        out = jdbcEliminaRespuesta.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_RESP_ADM.SP_DEL_RESP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al borrar la Respuesta id(" + idResp + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean registraRespuestas(int idCheckUsua, BitacoraDTO bitacora, String idRespuestas, String compromisos, String evidencias) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDBITACORA", bitacora.getIdBitacora())
                .addValue("PA_LONGITUD", bitacora.getLongitud())
                .addValue("PA_LATITUD", bitacora.getLatitud())
                .addValue("PA_CHECKUSUA", idCheckUsua)
                .addValue("PA_IDRESPUESTAS", idRespuestas)
                .addValue("PA_COMPROMISOS", compromisos)
                .addValue("PA_EVIDENCIAS", evidencias);

        out = jdbcRegistraRespuestas.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_CHECKLIST.SP_REGISTA_RPTAS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar las Respuestas");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean insertaUnaRespuesta(int idCheckU, int idBitacora, int idPreg, int idArbol, String obsv, String compromiso, String evidencia, String respAd) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCHECKUSUA", idCheckU)
                .addValue("PA_IDBITACORA", idBitacora)
                .addValue("PA_IDPREGUNTA", idPreg)
                .addValue("PA_IDARBOL", idArbol)
                .addValue("PA_OBSV", obsv)
                .addValue("PA_COMPROMISO", compromiso)
                .addValue("PA_EVIDENCIA", evidencia)
                .addValue("PA_RESPUESTA_AD", respAd);

        out = jdbcInsertaUnaResp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PARESPUESTA_ADM.SP_REGISTRA_PREGUNTA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar la Respuesta con el idArbol: " + idArbol + " ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaRespuestas(String respuestas) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_RESP", respuestas);

        out = jdbcEliminaRespuestas.execute(in);

        logger.info("Funcion ejecutada: {checklist.PARESPUESTA_ADM.SP_ELIMINA_RESP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al borrar las Respuestas");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaFechaResp(String idRespuesta, String fechaTermino, String commit) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDRESPUESTA", idRespuesta)
                .addValue("PA_FECHA", fechaTermino).addValue("PA_COMMIT", commit);

        out = jdbcActualizaFechaResp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_RESP_ADM.SP_ACT_F_RESP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar la Respuesta  id( " + idRespuesta + ")");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaRespuestasDuplicadas() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcEliminaRespuestasD.execute();

        logger.info("Funcion ejecutada: {checklist.PARESPUESTA_ADM.SPELIMINADUPLICADOS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al borrar las Respuestas");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean insertaRespuestaNueva(int idCheckU, int idBitacora, int idPreg, int idArbol, String obsv,
            String compromiso, String evidencia, String respAd) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCHECKUSUA", idCheckU)
                .addValue("PA_IDBITACORA", idBitacora)
                .addValue("PA_IDPREGUNTA", idPreg)
                .addValue("PA_IDARBOL", idArbol)
                .addValue("PA_OBSV", obsv)
                .addValue("PA_COMPROMISO", compromiso)
                .addValue("PA_EVIDENCIA", evidencia)
                .addValue("PA_RESPUESTA_AD", respAd);

        out = jdbcInsertaNuevaResp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PARESPUESTA_ADM.SP_NUEVA_RESP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar la Respuesta con el idArbol: " + idArbol + " ");
        } else {
            return true;
        }

        return false;
    }
    //ADICIONALES

    @SuppressWarnings("unchecked")
    @Override
    public List<RespuestaAdDTO> buscaRespADP(String idRespAD, String idResp) throws Exception {
        Map<String, Object> out = null;

        List<RespuestaAdDTO> listaRespuesta = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDRES_AD", idRespAD)
                .addValue("PA_IDRESPUESTA", idResp);

        out = jdbcBuscaRespAD.execute(in);

        logger.info("Funcion ejecutada: {checklist.PARESPAD_ADM.SP_SEL_RESPAD}");

        listaRespuesta = (List<RespuestaAdDTO>) out.get("RCL_RESP");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener las Respuestas Adicionales");
        } else {
            return listaRespuesta;
        }

        return null;
    }

    @Override
    public int insertaRespAD(RespuestaAdDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idResp = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDRESPUESTA", bean.getIdRespuesta())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcInsertaRespAD.execute(in);

        logger.info("Funcion ejecutada: {checklist.PARESPAD_ADM.SP_INS_RESPAD}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDRESP");
        idResp = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar la Respuesta Adicional");
        } else {
            return idResp;
        }

        return idResp;
    }

    @Override
    public boolean actualizaRespAD(RespuestaAdDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDRES_AD", bean.getIdRespuestaAd())
                .addValue("PA_IDRESPUESTA", bean.getIdRespuesta())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcActualizaRespAD.execute(in);

        logger.info("Funcion ejecutada: {checklist.PARESPAD_ADM.SP_ACT_RESPAD}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar la Respuesta Adicional");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaRespAD(int idRespAD) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDRES_AD", idRespAD);

        out = jdbcEliminaRespAD.execute(in);

        logger.info("Funcion ejecutada: {checklist.PARESPAD_ADM.SP_DEL_RESPAD}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al eliminar la Respuesta Adicional");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean depuraTabs() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcDepuraTabs.execute();

        logger.info("Funcion ejecutada: {checklist.PARESPAD_ADM.SP_DEPURATAB}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al eliminar la las tablas De administrador Check");
        } else {
            return true;
        }

        return false;
    }

    public boolean depuraTabParam(int idCheck) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECK", idCheck);
        out = jdbcDepuraTabParam.execute(in);

        logger.info("Funcion ejecutada: {checklist.PARESPAD_ADM.SP_DEPURATABCHECK}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al eliminar la las tablas De administrador Check");
        } else {
            return true;
        }

        return false;
    }
//EVIDENCIAS

    @SuppressWarnings("unchecked")
    public List<EvidenciaDTO> obtieneEvidencia() throws Exception {
        Map<String, Object> out = null;
        List<EvidenciaDTO> listaEvidencia = null;
        int error = 0;

        out = jdbcObtieneEvidencias.execute();

        logger.info("Funcion ejecutada: {checklist.PA_EVID_ADM.SP_SEL_G_EVID}");

        listaEvidencia = (List<EvidenciaDTO>) out.get("RCL_EVID");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener las Evidencias");
        } else {
            return listaEvidencia;
        }

        return listaEvidencia;

    }

    @SuppressWarnings("unchecked")
    public List<EvidenciaDTO> obtieneEvidencia(int idTipo) throws Exception {
        Map<String, Object> out = null;
        List<EvidenciaDTO> listaEvidencia = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDTIPO", idTipo);

        out = jdbcObtieneEvidencia.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_EVID_ADM.SP_SEL_EVID}");

        listaEvidencia = (List<EvidenciaDTO>) out.get("RCL_EVID");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener la Evidencia con id(" + idTipo + ")");
        } else {
            return listaEvidencia;
        }

        return null;

    }

    public int insertaEvidencia(EvidenciaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEvidencia = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDRESP", bean.getIdRespuesta())
                .addValue("PA_IDTIPO", bean.getIdTipo())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_ELEMENTO", bean.getIdPlantilla());

        out = jdbcInsertaEvidencia.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_EVID_ADM.SP_INS_EVID}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_FIID_EVID");
        idEvidencia = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar la Evidencia");
        } else {
            return idEvidencia;
        }

        return idEvidencia;

    }

    public boolean actualizaEvidencia(EvidenciaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_EVID", bean.getIdEvidencia())
                .addValue("PA_IDRESP", bean.getIdRespuesta())
                .addValue("PA_IDTIPO", bean.getIdTipo())
                .addValue("PA_RUTA", bean.getRuta());

        out = jdbcActualizaEvidencia.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_EVID_ADM.SP_ACT_EVID}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar la Evidencia  id( " + bean.getIdEvidencia() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaEvidencia(int idEvidencia) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_EVID", idEvidencia);

        out = jdbcEliminaEvidencia.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_EVID_ADM.SP_DEL_EVID}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar la Evidencia id(" + idEvidencia + ")");
        } else {
            return true;
        }

        return false;
    }

}
