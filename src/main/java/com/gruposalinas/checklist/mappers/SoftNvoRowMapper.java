package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.SoftNvoDTO;

public class SoftNvoRowMapper implements RowMapper<SoftNvoDTO> {

	public SoftNvoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		SoftNvoDTO SoftNvoDTO = new SoftNvoDTO();
		
		SoftNvoDTO.setIdSoft(rs.getInt("FIID_SOFTN"));
	    SoftNvoDTO.setIdFase(rs.getInt("FIID_FASE"));
	    SoftNvoDTO.setAux(rs.getString("FCID_CECO"));
	    SoftNvoDTO.setIdProyecto(rs.getInt("FIID_PROYECTO"));
	    SoftNvoDTO.setBitacora(rs.getInt("FIID_BITACORA"));
	    SoftNvoDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
	    SoftNvoDTO.setCalificacion(rs.getDouble("FICALIF"));
	    SoftNvoDTO.setCeco(rs.getString("NOMCECO"));
	    SoftNvoDTO.setIdCanal(rs.getInt("FIID_CANAL"));
	    SoftNvoDTO.setStatus(rs.getInt("FISTATUS"));
	    SoftNvoDTO.setPeriodo(rs.getString("FCPERIODO"));
	    SoftNvoDTO.setIdRecorrido(rs.getInt("FCRECO"));
		return SoftNvoDTO;
	}

}


