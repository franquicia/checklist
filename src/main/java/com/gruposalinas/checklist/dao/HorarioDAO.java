package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.HorarioDTO;

public interface HorarioDAO {

	public List<HorarioDTO> obtieneHorario() throws Exception;
	
	public List<HorarioDTO> obtenerHorario(int idHorario) throws Exception;
	
	public int insertaHorario(HorarioDTO bean) throws Exception;
	
	public boolean actualizaHorario(HorarioDTO bean) throws Exception;
	
	public boolean eliminaHorario(int idHorario) throws Exception;
}
