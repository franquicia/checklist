package com.gruposalinas.checklist.servicios.servidor;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonObject;
import com.gruposalinas.checklist.business.LogBI;
import com.gruposalinas.checklist.domain.LogDTO;
import com.gruposalinas.checklist.util.UtilDate;

@Controller
@RequestMapping("/errores")

public class ServicioErrores {
	
	@Autowired
	LogBI logbi;

	private static Logger logger = LogManager.getLogger(ServicioErrores.class);

	@SuppressWarnings("static-access")
	@RequestMapping(value = "/errorToken", method = RequestMethod.GET)
	public @ResponseBody String getErrorToken(HttpServletRequest request, HttpServletResponse response) throws IOException{

		JsonObject object = new JsonObject();
		UtilDate fec = new UtilDate();

		object.addProperty("token", "ErrorToken");
		object.addProperty("fecha", fec.getSysDate("dd/MM/yyyy"));
		String json = object.toString();
		logger.info("Algo paso al validar el token");
		response.setContentType("application/json");
		response.getWriter().write(json);
		return null;
	}
	@SuppressWarnings("static-access")
	@RequestMapping(value = "/null", method = RequestMethod.GET)
	public @ResponseBody String getnull(HttpServletRequest request, HttpServletResponse response) throws IOException{

		JsonObject object = new JsonObject();
		UtilDate fec = new UtilDate();

		object.addProperty("nombre", "null");
		object.addProperty("fecha", fec.getSysDate("dd/MM/yyyy"));
		String json = object.toString();
		logger.info("Algo paso al validar el usuario");
		response.setContentType("application/json");
		response.getWriter().write(json);
		return null;
	}
	
	// http://localhost:8080/checklist/errores/getErrores.json?fecha=<?>
	@RequestMapping(value = "/getErrores", method = RequestMethod.GET)
	public ModelAndView getErrores(HttpServletRequest request, HttpServletResponse response) {
		String fecha = request.getParameter("fecha");

		List<LogDTO> lista = logbi.obtieneErrores(fecha);

		ModelAndView mv = new ModelAndView("muestraServicios");
		mv.addObject("tipo", "ERRORES");
		mv.addObject("res", lista);
		return mv;
	}

	// http://localhost:8080/checklist/errores/getErrores.json?fecha=<?>
	@RequestMapping(value = "/getErrorSesion", method = RequestMethod.GET)
	public ModelAndView getErrorSesion(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView mv = new ModelAndView("http404");
		return mv;
	}
	
	
}
