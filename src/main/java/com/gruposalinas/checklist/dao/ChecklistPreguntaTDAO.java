package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;

public interface ChecklistPreguntaTDAO {
	
	public List<ChecklistPreguntaDTO> obtienePregCheckTemp(int idChecklist) throws Exception;
	
	public boolean insertaChecklistPreguntaTemp(ChecklistPreguntaDTO bean)throws Exception;
	
	public boolean actualizaChecklistPreguntaTemp(ChecklistPreguntaDTO bean)throws Exception;
	
	public boolean eliminaChecklistPreguntaTemp(int idChecklist, int idPregunta)throws Exception;

}
