package com.gruposalinas.checklist.domain;

public class CompromisoCecoDTO {
	
	private String idCeco;
	private int total;
	private String descCeco;
	private String idChecklist;
	private String nombreChecklist;
	private String zona;
	private int numeroSuc;
	private int asignados;
	private int terminados;
	
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getDescCeco() {
		return descCeco;
	}
	public void setDescCeco(String descCeco) {
		this.descCeco = descCeco;
	}
	public String getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(String idChecklist) {
		this.idChecklist = idChecklist;
	}
	public String getNombreChecklist() {
		return nombreChecklist;
	}
	public void setNombreChecklist(String nombreChecklist) {
		this.nombreChecklist = nombreChecklist;
	}
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	public int getNumeroSuc() {
		return numeroSuc;
	}
	public void setNumeroSuc(int numeroSuc) {
		this.numeroSuc = numeroSuc;
	}
	public int getAsignados() {
		return asignados;
	}
	public void setAsignados(int asignados) {
		this.asignados = asignados;
	}
	public int getTerminados() {
		return terminados;
	}
	public void setTerminados(int terminados) {
		this.terminados = terminados;
	}

	@Override
	public String toString() {
		return "CompromisoCecoDTO [idCeco=" + idCeco + ", total=" + total + ", descCeco=" + descCeco + ", idChecklist="
				+ idChecklist + ", nombreChecklist=" + nombreChecklist + "]";
	}

}
