package com.gruposalinas.checklist.domain;

public class EvidenciaDTO {

	private int idEvidencia;
	private int idRespuesta;
	private int idPregunta;
	private int idTipo;
	private String ruta;
	private int commit;
	private String descPreg;
	private int idPlantilla;
	
	public int getIdEvidencia() {
		return idEvidencia;
	}
	public void setIdEvidencia(int idEvidencia) {
		this.idEvidencia = idEvidencia;
	}
	public int getIdRespuesta() {
		return idRespuesta;
	}
	public void setIdRespuesta(int idRespuesta) {
		this.idRespuesta = idRespuesta;
	}
	public int getIdTipo() {
		return idTipo;
	}
	public void setIdTipo(int idTipo) {
		this.idTipo = idTipo;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	
	public int getCommit() {
		return commit;
	}
	public void setCommit(int commit) {
		this.commit = commit;
	}
	public String getDescPreg() {
		return descPreg;
	}
	public void setDescPreg(String descPreg) {
		this.descPreg = descPreg;
	}
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}

	@Override
	public String toString() {
		return "EvidenciaDTO [idEvidencia=" + idEvidencia + ", idRespuesta=" + idRespuesta + ", idPregunta="
				+ idPregunta + ", idTipo=" + idTipo + ", ruta=" + ruta + ", commit=" + commit + ", descPreg=" + descPreg
				+ "]";
	}

	public int getIdPlantilla() {
		return idPlantilla;
	}
	public void setIdPlantilla(int idPlantilla) {
		this.idPlantilla = idPlantilla;
	}
	
	
	
	
}
