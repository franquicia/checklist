package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.util.UtilFRQ;
import com.google.gson.JsonObject;
import com.gruposalinas.checklist.dao.ChecklistPreguntasComDAO;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.PreguntaCheckDTO;

public class ChecklistPreguntasComBI {

	private static Logger logger = LogManager.getLogger(ChecklistPreguntasComBI.class);

	private List<ChecklistPreguntasComDTO> listafila;

	private List<PreguntaCheckDTO> lista;
	@Autowired
	ChecklistPreguntasComDAO checklistPreguntasComDAO;

	// trae respuestas
	public String obtieneDatos(String idBita) {
		JSONObject respuesta = new JSONObject();

		try {
			Map<String, Object> listaPregs = null;
			listaPregs = checklistPreguntasComDAO.obtieneDatos(idBita);

			@SuppressWarnings("unchecked")
			List<ChecklistPreguntasComDTO> listaPreg = (List<ChecklistPreguntasComDTO>) listaPregs.get("listaPregunta");
			@SuppressWarnings("unchecked")
			List<ChecklistPreguntasComDTO> listaevid = (List<ChecklistPreguntasComDTO>) listaPregs
					.get("listaRespuesta");

			JSONArray preguntas = new JSONArray();
			JSONArray evid = new JSONArray();

			// lista preguntas
			for (int i = 0; i < listaPreg.size(); i++) {

				JSONObject obj = new JSONObject();
				obj.put("idBita", listaPreg.get(i).getIdBita());
				obj.put("idResp", listaPreg.get(i).getIdResp());
				obj.put("idArbol", listaPreg.get(i).getIdArbol());
				obj.put("observ", listaPreg.get(i).getObserv());
				obj.put("idPreg", listaPreg.get(i).getIdPreg());
				obj.put("idCheck", listaPreg.get(i).getIdCheck());
				obj.put("idPosible", listaPreg.get(i).getIdPosible());
				obj.put("posibleRespuesta", listaPreg.get(i).getPosible());
				obj.put("pregunta", listaPreg.get(i).getPregunta());
				obj.put("idcritica", listaPreg.get(i).getIdcritica());
				obj.put("idcheckUsua", listaPreg.get(i).getIdcheckUsua());
				obj.put("idUsu", listaPreg.get(i).getIdUsu());
				obj.put("ceco", listaPreg.get(i).getCeco());
				obj.put("checklist", listaPreg.get(i).getChecklist());
				obj.put("idOrdenGru", listaPreg.get(i).getIdOrdenGru());
				obj.put("idperiodicidad", listaPreg.get(i).getIdperiodicidad());
				obj.put("respuestaAbierta", listaPreg.get(i).getRespuestaAbierta());
				obj.put("idRespAb", listaPreg.get(i).getIdRespAb());
				obj.put("banderaRespAb", listaPreg.get(i).getBanderaRespAb());
				obj.put("modulo", listaPreg.get(i).getModulo());
				obj.put("calif", listaPreg.get(i).getCalif());
				obj.put("precalif", listaPreg.get(i).getPrecalif());
				obj.put("clasif", listaPreg.get(i).getClasif());
				obj.put("calCheck", listaPreg.get(i).getCalCheck());
				preguntas.put(obj);

			}
			for (int i = 0; i < listaevid.size(); i++) {

				JSONObject obj = new JSONObject();
				obj.put("idBita", listaevid.get(i).getIdBita());
				obj.put("idResp", listaevid.get(i).getIdResp());
				obj.put("idArbol", listaevid.get(i).getIdArbol());
				obj.put("ruta", listaevid.get(i).getRuta());
				obj.put("idPlantilla", listaevid.get(i).getIdPlantilla());
				obj.put("idElem", listaevid.get(i).getIdElem());
				obj.put("idOrden", listaevid.get(i).getIdOrden());
				obj.put("tipoEvi", listaevid.get(i).getTipoEvi());
				evid.put(obj);

			}
			respuesta.append("listaPreg", preguntas);
			respuesta.append("listaevid", evid);

		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta.toString();
	}

	// Obtiene solo respuestas
	public List<ChecklistPreguntasComDTO> obtieneRespuestas(String idBita) {

		Map<String, Object> listaPregs = null;
		List<ChecklistPreguntasComDTO> response = new ArrayList<ChecklistPreguntasComDTO>();
		try {

			listaPregs = checklistPreguntasComDAO.obtieneDatos(idBita);

			@SuppressWarnings("unchecked")
			List<ChecklistPreguntasComDTO> listaPreg = (List<ChecklistPreguntasComDTO>) listaPregs.get("listaPregunta");
			response = listaPreg;

		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return response;
	}

	// trae respuestas generico
	public List<ChecklistPreguntasComDTO> obtieneDatosGen(String idBita) {

		try {
			listafila = checklistPreguntasComDAO.obtieneDatosGen(idBita);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// trae si y no conteo por checklist

	public List<ChecklistPreguntasComDTO> obtieneInfo(String idBita) {

		try {
			listafila = checklistPreguntasComDAO.obtieneInfo(idBita);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// trae si y no conteo por Grupo

	public List<ChecklistPreguntasComDTO> obtieneInfoCont(String idBita) {

		try {
			listafila = checklistPreguntasComDAO.obtieneInfoCont(idBita);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// trae si y no conteo por Grupo campos Extra

	public List<ChecklistPreguntasComDTO> obtieneInfoContExt(String idBita) {

		try {
			listafila = checklistPreguntasComDAO.obtieneInfoContExt(idBita);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// trae si y no conteo por Grupo conteo hijas

	public List<ChecklistPreguntasComDTO> obtieneInfoContHij(String idBita) {

		try {
			listafila = checklistPreguntasComDAO.obtieneInfoContHij(idBita);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}
	// trae preguntas del checklist y detalle

	public List<PreguntaCheckDTO> obtieneInfopreg(int idCheck) {

		try {
			lista = checklistPreguntasComDAO.obtieneInfopreg(idCheck);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return lista;
	}

	// trae preguntas imperdonables

	public List<ChecklistPreguntasComDTO> obtieneInfoImp(int idBita) {

		try {
			listafila = checklistPreguntasComDAO.obtieneInfoImp(idBita);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}
	// trae preguntas no imperdonables

	public List<ChecklistPreguntasComDTO> obtieneInfoNoImp(int idBita) {

		try {
			listafila = checklistPreguntasComDAO.obtieneInfoNoImp(idBita);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// trae preg padres e hijas
	public String obtieneInfoPadHij(String idBita) {
		JSONObject response = new JSONObject();

		try {
			Map<String, Object> listaPregs = null;
			listaPregs = checklistPreguntasComDAO.obtieneInfoPadHij(idBita);

			@SuppressWarnings("unchecked")
			List<ChecklistPreguntasComDTO> listapadres = (List<ChecklistPreguntasComDTO>) listaPregs.get("listapadres");
			@SuppressWarnings("unchecked")
			List<ChecklistPreguntasComDTO> listahijas = (List<ChecklistPreguntasComDTO>) listaPregs.get("listahijas");

			JSONArray respuestas = new JSONArray();

			for (ChecklistPreguntasComDTO aux : listapadres)

			{
				String papa = "";
				JSONObject respuesta = new JSONObject();

				// System.out.println("Padre: " + aux.getPregunta());
				// System.out.println("Padre: " + aux.getIdPreg());

				papa = aux.getPregunta();

				for (ChecklistPreguntasComDTO aux2 : listahijas) {
					// System.out.println("hija: " + aux2.getPregunta());
					// System.out.println("hija: " + aux2.getIdPreg());

					if (aux.getIdPreg() == aux2.getPregPadre()) {
						papa += "," + aux2.getPregunta() + " ";
					}

				}
				respuesta.append("Respuesta", papa);
				respuestas.put(respuesta);

			}

			response.append("ArrayRespuestas", respuestas);

		} catch (Exception e) {
			logger.info(e);
			return "{}";
		}

		if (response != null)
			return response.toString();

		else
			return "{}";
	}

	// trae preguntas no imperdonables

	public List<ChecklistPreguntasComDTO> obtieneDatosGenales(String idBita) {

		try {
			listafila = checklistPreguntasComDAO.obtieneDatosGenales(idBita);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// trae preg imperdonables concat
	public String obtieneDatosImpconc(String idBita) {
		JSONObject response = new JSONObject();

		try {
			Map<String, Object> listaPregs = null;
			listaPregs = checklistPreguntasComDAO.obtieneDatosImpconc(idBita);

			@SuppressWarnings("unchecked")
			List<ChecklistPreguntasComDTO> listapadres = (List<ChecklistPreguntasComDTO>) listaPregs.get("listapadres");
			@SuppressWarnings("unchecked")
			List<ChecklistPreguntasComDTO> listahijas = (List<ChecklistPreguntasComDTO>) listaPregs.get("listahijas");

			JSONArray respuestas = new JSONArray();

			for (ChecklistPreguntasComDTO aux : listahijas)
			// for (ChecklistPreguntasComDTO aux : listapadres)

			{
				String papa = "";
				JSONObject respuesta = new JSONObject();

				System.out.println("Padre: " + aux.getPregunta());
				System.out.println("Padre id preg: " + aux.getIdPreg() + "  " + "Padre preg: " + aux.getPregPadre());

				papa = aux.getPregunta() + " " + aux.getPosible();

				for (ChecklistPreguntasComDTO aux2 : listapadres)
				// for(ChecklistPreguntasComDTO aux2:listahijas)
				{
					System.out.println("hija: " + aux2.getPregunta());
					System.out.println("hija id: " + aux2.getIdPreg() + " " + "hija id pad: " + aux2.getPregPadre());
					if (aux.getPregPadre() == aux2.getPregPadre())

					{
						papa += "";

					}

					if (aux.getIdPreg() == aux2.getPregPadre())

					{
						papa += "," + aux2.getPregunta() + " " + " " + aux.getPosible() + " ";

					}

					else {
						papa += "";

					}

				}
				respuesta.append("Respuesta", papa);
				respuestas.put(respuesta);

			}

			response.append("ArrayRespuestas", respuestas);

		} catch (Exception e) {
			logger.info(e);
			return "{}";
		}

		if (response != null)
			return response.toString();

		else
			return "{}";
	}

	public String obtieneDatosImpconcpru(String idBita) {

		JSONObject respuesta = new JSONObject();

		try {
			Map<String, Object> listaPregs = null;
			listaPregs = checklistPreguntasComDAO.obtieneDatosImpconcPru(idBita);

			@SuppressWarnings("unchecked")
			List<ChecklistPreguntasComDTO> listaPreg = (List<ChecklistPreguntasComDTO>) listaPregs.get("listahijas");

			JSONArray preguntas = new JSONArray();

			String preg = "";
			// lista preguntas
			for (ChecklistPreguntasComDTO aux : listaPreg) {
				// for(int i=0; i < listaPreg.size();i++) {

				JSONObject obj = new JSONObject();
				preg = aux.getAux() + " " + aux.getPregunta() + " " + aux.getPosible();

				/*
				 * obj.put("posibleRespuesta", listaPreg.get(i).getPosible());
				 * obj.put("pregunta", listaPreg.get(i).getPregunta());
				 * 
				 * obj.put("pregpadreid",listaPreg.get(i).getAux2());
				 * obj.put("descripcionpadre",listaPreg.get(i).getAux());
				 */

				//
				preguntas.put(obj);

			}

			respuesta.append("listaPreg", preguntas);

		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta.toString();
	}

	// trae HALLAZGOS RECORRIDO

	public List<ChecklistPreguntasComDTO> obtieneInfoHallaz(int ceco) {

		try {
			listafila = checklistPreguntasComDAO.obtieneInfoHallaz(ceco);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// trae HALLAZGOS tabla

	public List<ChecklistPreguntasComDTO> obtieneInfoHallazTab(int ceco) {

		try {
			listafila = checklistPreguntasComDAO.obtieneInfoHallazTab(ceco);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// OCC

	// trae si y no conteo por GRUPO checklist OCC

	public List<ChecklistPreguntasComDTO> obtieneInfoOCC(String idBita) {

		try {
			listafila = checklistPreguntasComDAO.obtieneInfoOCC(idBita);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// trae si y no conteo por GRUPO checklist OCC
	// FCCLASIFICA,FIID_BITGRAL,TOTALGRAL,SUMA_GRUPO

	public List<ChecklistPreguntasComDTO> obtieneComplemOCC(String idBita) {

		try {
			listafila = checklistPreguntasComDAO.obtieneComplemOCC(idBita);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// trae si y no conteo por GRUPO checklist OCC
	// FCCLASIFICA,FIID_BITGRAL,tot_si,tot_na

	public List<ChecklistPreguntasComDTO> obtieneComplemSiNaOCC(String idBita) {

		try {
			listafila = checklistPreguntasComDAO.obtieneComplemSiNaOCC(idBita);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// trae HALLAZGOS RECORRIDO NVO

	public List<ChecklistPreguntasComDTO> obtieneInfoHallazNvo(int ceco) {

		try {
			listafila = checklistPreguntasComDAO.obtieneInfoHallaz(ceco);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// trae HALLAZGOS tabla NVO

	public List<ChecklistPreguntasComDTO> obtieneInfoHallazTabNvo(int ceco) {

		try {
			listafila = checklistPreguntasComDAO.obtieneInfoHallazTab(ceco);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// SOFT NUEVO CECO PROYECTO FASE

	public List<ChecklistPreguntasComDTO> obtieneSoftCecoFaseProyecto(String ceco, String fase, String proyecto,
			String checklist) {
		try {
			listafila = checklistPreguntasComDAO.obtieneSoftCecoFaseProyecto(ceco, fase, proyecto, checklist);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;

	}
	// SOFT NUEVO CECO PROYECTO FASE

	public String obtieneSoftCecoFaseProyectoCom(String ceco, String fase, String proyecto, String checklist) {

		JSONObject respuesta = new JSONObject();

		try {
			Map<String, Object> listaPregs = null;

			listaPregs = checklistPreguntasComDAO.obtieneSoftCecoFaseProyectoCom(ceco, fase, proyecto, checklist);

			@SuppressWarnings("unchecked")
			List<ChecklistPreguntasComDTO> listaPreg = (List<ChecklistPreguntasComDTO>) listaPregs
					.get("listaRespuesta");
			@SuppressWarnings("unchecked")
			List<ChecklistPreguntasComDTO> listaevid = (List<ChecklistPreguntasComDTO>) listaPregs
					.get("listaEvidencia");

			JSONArray preguntas = new JSONArray();
			JSONArray evid = new JSONArray();

			// lista preguntas
			for (int i = 0; i < listaPreg.size(); i++) {

				JSONObject obj = new JSONObject();
				obj.put("idBita", listaPreg.get(i).getIdBita());
				obj.put("idResp", listaPreg.get(i).getIdResp());
				obj.put("idArbol", listaPreg.get(i).getIdArbol());
				obj.put("observ", listaPreg.get(i).getObserv());
				obj.put("idPreg", listaPreg.get(i).getIdPreg());
				obj.put("idCheck", listaPreg.get(i).getIdCheck());
				obj.put("idPosible", listaPreg.get(i).getIdPosible());
				obj.put("posibleRespuesta", listaPreg.get(i).getPosible());
				obj.put("pregunta", listaPreg.get(i).getPregunta());
				obj.put("idcritica", listaPreg.get(i).getIdcritica());
				obj.put("idcheckUsua", listaPreg.get(i).getIdcheckUsua());
				obj.put("idUsu", listaPreg.get(i).getIdUsu());
				obj.put("ceco", listaPreg.get(i).getCeco());
				obj.put("checklist", listaPreg.get(i).getChecklist());
				obj.put("idOrdenGru", listaPreg.get(i).getIdOrdenGru());
				obj.put("idperiodicidad", listaPreg.get(i).getIdperiodicidad());
				obj.put("respuestaAbierta", listaPreg.get(i).getRespuestaAbierta());
				obj.put("idRespAb", listaPreg.get(i).getIdRespAb());
				obj.put("banderaRespAb", listaPreg.get(i).getBanderaRespAb());
				obj.put("modulo", listaPreg.get(i).getModulo());
				obj.put("calif", listaPreg.get(i).getCalif());
				obj.put("precalif", listaPreg.get(i).getPrecalif());
				obj.put("clasif", listaPreg.get(i).getClasif());
				obj.put("calCheck", listaPreg.get(i).getCalCheck());
				preguntas.put(obj);

			}
			for (int i = 0; i < listaevid.size(); i++) {

				JSONObject obj = new JSONObject();
				obj.put("idBita", listaevid.get(i).getIdBita());
				obj.put("idResp", listaevid.get(i).getIdResp());
				obj.put("idArbol", listaevid.get(i).getIdArbol());
				obj.put("ruta", listaevid.get(i).getRuta());
				obj.put("idPlantilla", listaevid.get(i).getIdPlantilla());
				obj.put("idElem", listaevid.get(i).getIdElem());
				obj.put("idOrden", listaevid.get(i).getIdOrden());
				obj.put("tipoEvi", listaevid.get(i).getTipoEvi());
				evid.put(obj);

			}
			respuesta.append("listaPreg", preguntas);
			respuesta.append("listaevid", evid);

		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta.toString();
	}
	// trae preguntas imperdonables CECO FASE PROYECTO

		public List<ChecklistPreguntasComDTO> obtienePregNoImpCecoFaseProyecto(String ceco,String fase,String proyecto) {

			try {
				listafila = checklistPreguntasComDAO.obtienePregNoImpCecoFaseProyecto(ceco, fase, proyecto);
			} catch (Exception e) {
				logger.info("No fue posible obtener datos ");
				UtilFRQ.printErrorLog(null, e);
			}

			return listafila;
		}
}
