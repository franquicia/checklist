package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.mappers.PreguntaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PreguntaDAOImpl extends DefaultDAO implements PreguntaDAO {

    private static Logger logger = LogManager.getLogger(PreguntaDTO.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtienePregunta;
    DefaultJdbcCall jdbcInsertaPregunta;
    DefaultJdbcCall jdbcInsertaPreguntaCom;
    DefaultJdbcCall jdbcActualizaPreguntaCom;
    DefaultJdbcCall jdbcActualizaPregunta;
    DefaultJdbcCall jdbcEliminaPregunta;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PREG")
                .withProcedureName("SP_SEL_G_PREG")
                .returningResultSet("RCL_PREG", new PreguntaRowMapper());

        jdbcObtienePregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PREG")
                .withProcedureName("SP_SEL_PREG")
                .returningResultSet("RCL_PREG", new PreguntaRowMapper());

        jdbcInsertaPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PREG")
                .withProcedureName("SP_INS_PREG");
        //preguntas Imperdonables
        jdbcInsertaPreguntaCom = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PREG")
                .withProcedureName("SP_INS_PREG_N");
        //act preg imp
        jdbcActualizaPreguntaCom = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PREG")
                .withProcedureName("SP_ACT_PREG_N");

        jdbcActualizaPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PREG")
                .withProcedureName("SP_ACT_PREG");

        jdbcEliminaPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PREG")
                .withProcedureName("SP_DEL_PREG");
    }

    @SuppressWarnings("unchecked")
    public List<PreguntaDTO> obtienePregunta() throws Exception {
        Map<String, Object> out = null;
        List<PreguntaDTO> listaPregunta = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_PREG.SP_SEL_G_PREG}");

        listaPregunta = (List<PreguntaDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener las Preguntas");
        } else {
            return listaPregunta;
        }

        return listaPregunta;
    }

    @SuppressWarnings("unchecked")
    public List<PreguntaDTO> obtienePregunta(int idPreg) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        List<PreguntaDTO> listaPregunta = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREG", idPreg);

        out = jdbcObtienePregunta.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_PREG.SP_SEL_PREG}");

        listaPregunta = (List<PreguntaDTO>) out.get("RCL_PREG");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener las Preguntas");
        } else {
            return listaPregunta;
        }

        return listaPregunta;

    }

    public int insertaPreguntaCom(PreguntaDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idPreg = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDMODULO", bean.getIdModulo())
                .addValue("PA_IDT_PREG", bean.getIdTipo())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_DESCRIPCION", bean.getPregunta())
                .addValue("PA_FCDETALLE", bean.getDetalle())
                .addValue("PA_CRITICA", bean.getCritica())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_SLA", bean.getSla())
                .addValue("PA_AREA", bean.getArea())
                .addValue("PA_NUMSERIE", bean.getCodigo())
                .addValue("PA_FCRESPONSABLE", bean.getResponsable())
                .addValue("PA_FCNEGOCIO", bean.getNegocio());

        out = jdbcInsertaPreguntaCom.execute(in);

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idPregReturn = (BigDecimal) out.get("PA_IDPREGUNTA");
        idPreg = idPregReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar la Pregunta IMP");
        } else {
            return idPreg;
        }

        return idPreg;

    }

    public int insertaPregunta(PreguntaDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idPreg = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDMODULO", bean.getIdModulo())
                .addValue("PA_IDT_PREG", bean.getIdTipo())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_DESCRIPCION", bean.getPregunta())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_FCRESPONSABLE", bean.getResponsable())
                .addValue("PA_FCNEGOCIO", bean.getNegocio());

        out = jdbcInsertaPregunta.execute(in);

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idPregReturn = (BigDecimal) out.get("PA_IDPREGUNTA");
        idPreg = idPregReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar la Pregunta");
        } else {
            return idPreg;
        }

        return idPreg;

    }

    public boolean actualizaPregunta(PreguntaDTO bean) throws Exception {

        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREG", bean.getIdPregunta())
                .addValue("PA_IDMODULO", bean.getIdModulo())
                .addValue("PA_IDT_PREG", bean.getIdTipo())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_DESCRIPCION", bean.getPregunta())
                .addValue("PA_FCRESPONSABLE", bean.getPregunta())
                .addValue("PA_FCNEGOCIO", bean.getNegocio());

        out = jdbcActualizaPregunta.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_PREG.SP_ACT_PREG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar la Pregunta id( " + bean.getIdPregunta() + ")");
        } else {
            return true;
        }

        return false;
    }

    //imperd
    public boolean actualizaPreguntaCom(PreguntaDTO bean) throws Exception {

        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREG", bean.getIdPregunta())
                .addValue("PA_IDMODULO", bean.getIdModulo())
                .addValue("PA_IDT_PREG", bean.getIdTipo())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_DESCRIPCION", bean.getPregunta())
                .addValue("PA_FCDETALLE", bean.getDetalle())
                .addValue("PA_CRITICA", bean.getCritica())
                .addValue("PA_SLA", bean.getSla())
                .addValue("PA_AREA", bean.getArea())
                .addValue("PA_FCRESPONSABLE", bean.getResponsable())
                .addValue("PA_FCNEGOCIO", bean.getNegocio());

        out = jdbcActualizaPreguntaCom.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_PREG.SP_ACT_PREG_N}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar la Pregunta id( " + bean.getIdPregunta() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaPregunta(int idPreg) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREG", idPreg);

        out = jdbcEliminaPregunta.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_PREG.SP_DEL_PREG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar la pregunta id(" + idPreg + ")");
        } else {
            return true;
        }

        return false;

    }

}
