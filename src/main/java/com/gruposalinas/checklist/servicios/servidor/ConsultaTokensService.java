package com.gruposalinas.checklist.servicios.servidor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.TokenBI;

import com.gruposalinas.checklist.domain.TokenDTO;

@Controller
@RequestMapping("/consultaTokensService")
public class ConsultaTokensService {

	@Autowired
	TokenBI tokenbi;
	
	// http://localhost:8080/checklist/consultaTokensService/getTokens.json?fecha=20160922
	@RequestMapping(value = "/getTokens", method = RequestMethod.GET)
	public ModelAndView getTokens(HttpServletRequest request, HttpServletResponse response) {
		String fecha = request.getParameter("fecha");

		List<TokenDTO> lista = tokenbi.buscaTokenF(fecha);

		ModelAndView mv = new ModelAndView("muestraServicios");
		mv.addObject("tipo", "LISTA TOKENS");
		mv.addObject("res", lista);
		return mv;
	}


}
