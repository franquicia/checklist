package com.gruposalinas.checklist.servicios.servidor;

import com.gruposalinas.checklist.business.AsignacionCheckBI;
import com.gruposalinas.checklist.business.AsistenciaSupervisorBI;
import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.business.CheckinBI;
import com.gruposalinas.checklist.business.ChecklistPreguntasComBI;
import com.gruposalinas.checklist.business.CorreoBI;
import com.gruposalinas.checklist.business.EnviaCorreoVisitasBI;
import com.gruposalinas.checklist.business.ProtocoloByCheckBI;
import com.gruposalinas.checklist.business.RepMedicionBI;
import com.gruposalinas.checklist.business.RepoCheckBI;
import com.gruposalinas.checklist.business.ReporteBI;
import com.gruposalinas.checklist.business.ReporteMedicionBI;
import com.gruposalinas.checklist.business.ServiciosMiGestionBI;
import com.gruposalinas.checklist.business.Usuario_ABI;
import com.gruposalinas.checklist.business.VisitasxMesBI;
import com.gruposalinas.checklist.domain.AsignacionCheckDTO;
import com.gruposalinas.checklist.domain.AsistenciaSupervisorDTO;
import com.gruposalinas.checklist.domain.BinaryOutputWrapper;
import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.CheckinDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.FoliosMtoDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.ProtocoloByCheckDTO;
import com.gruposalinas.checklist.domain.RepMedicionDTO;
import com.gruposalinas.checklist.domain.RepoCheckDTO;
import com.gruposalinas.checklist.domain.ReporteDTO;
import com.gruposalinas.checklist.domain.ReporteMedPregDTO;
import com.gruposalinas.checklist.domain.ReporteMedicionDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.domain.Usuario_ADTO;
import com.gruposalinas.checklist.domain.VisitasxMesDTO;
import com.gruposalinas.checklist.util.FileUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/reporteService")
public class GeneraReporteService {

    @Autowired
    ReporteBI reportebi;

    @Autowired
    VisitasxMesBI visitasxMesBI;

    @Autowired
    AsignacionCheckBI asignacionCheckBI;

    @Autowired
    CorreoBI correoBI;

    @Autowired
    Usuario_ABI usuario_ABI;

    @Autowired
    CecoBI cecoBI;

    @Autowired
    EnviaCorreoVisitasBI enviaCorreoVisitasBI;

    @Autowired
    RepoCheckBI repoCheckBI;

    @Autowired
    CheckinBI checkinBI;

    @Autowired
    AsistenciaSupervisorBI asistenciaSupervisorBI;

    @Autowired
    ServiciosMiGestionBI serviciosMiGestionBI;

    @Autowired
    ChecklistPreguntasComBI checklistPreguntasComBI;

    @Autowired
    ReporteMedicionBI reporteMedicionBI;
    @Autowired
    RepMedicionBI repMedicionBI;
    @Autowired
    ProtocoloByCheckBI protocoloByCheckBI;

    private static final Logger logger = LogManager.getLogger(GeneraReporteService.class);

    // http://localhost:8080/checklist/reporteService/getReporte.json?idCheck=<?>&idUsuario=<?>&nombreUsuario=<?>&nombreSucursal=<?>&fechaRespuesta=<?>&nombreCheck=<?>&metodoCheck=<?>
    @RequestMapping(value = "/getReporte", method = RequestMethod.GET)
    public ModelAndView getReporte(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        try {
            String idCheck = request.getParameter("idCheck");
            String idUsuario = request.getParameter("idUsuario");
            String nombreUsuario = request.getParameter("nombreUsuario");
            String nombreSucursal = request.getParameter("nombreSucursal");
            String fechaRespuesta = request.getParameter("fechaRespuesta");
            String nombreCheck = request.getParameter("nombreCheck");
            String metodoCheck = request.getParameter("metodoCheck");

            ServletOutputStream sos = null;
            String archivo = "";
            ReporteDTO rep = new ReporteDTO();
            rep.setIdChecklist(idCheck);
            rep.setIdUsuario(idUsuario);
            rep.setNombreUsuario(nombreUsuario);
            rep.setNombreSucursal(nombreSucursal);
            rep.setFechaRespuesta(fechaRespuesta);
            rep.setNombreChecklist(nombreCheck);
            rep.setMetodoChecklist(metodoCheck);

            List<ReporteDTO> lista = reportebi.buscaReporte(idCheck, idUsuario, nombreUsuario, nombreSucursal, fechaRespuesta, nombreCheck, metodoCheck);

            Iterator<ReporteDTO> it = lista.iterator();
            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'><tr>"
                    + "<thead>"
                    + "<tr>"
                    + "<th align='center'>ID CHECKLIST</th>"
                    + "<th align='center'>ID USUARIO</th>"
                    + "<th align='center'>NOMBRE USUARIO</th>"
                    + "<th align='center'>NUMERO SUCURSAL</th>"
                    + "<th align='center'>NOMBRE CECO</th>"
                    + "<th align='center'>NUMERO REGIONAL</th>"
                    + "<th align='center'>NOMBRE REGIONAL</th>"
                    + "<th align='center'>NUMERO ZONA</th>"
                    + "<th align='center'>NOMBRE ZONA</th>"
                    + "<th align='center'>NUMERO TERRITORIO</th>"
                    + "<th align='center'>NOMBRE TERRITORIO</th>"
                    + "<th align='center'>FECHA RESPUESTA</th>"
                    + "<th align='center'>SEMANA</th>"
                    + "<th align='center'>NOMBRE MODULO PADRE</th>"
                    + "<th align='center'>NOMBRE MODULO</th>"
                    + "<th align='center'>ID PREGUNTA</th>"
                    + "<th align='center'>DESCRIPCION PREGUNTA</th>"
                    + "<th align='center'>RESPUESTA</th>"
                    + "<th align='center'>NOMBRE CHECKLIST</th>"
                    + "<th align='center'>METODO CHECKLIST</th>"
                    + "<th align='center'>OBSERVACIONES</th>"
                    + "<th align='center'>COMPROMISO</th>"
                    + "<th align='center'>FECHA COMPROMISO</th>"
                    + "<th align='center'>RESPONSABLE</th>"
                    + "<tr></thead>";
            archivo += "<tbody>";
            while (it.hasNext()) {
                ReporteDTO o = it.next();
                archivo += "<tr>"
                        + "<td align='center'>" + o.getIdChecklist() + "</td>"
                        + "<td align='center'>" + o.getIdUsuario() + "</td>"
                        + "<td align='center'>" + o.getNombreUsuario() + "</td>"
                        + "<td align='center'>" + o.getNombreSucursal() + "</td>"
                        + "<td align='center'>" + o.getNombreCeco() + "</td>"
                        + "<td align='center'>" + o.getIdRegional() + "</td>"
                        + "<td align='center'>" + o.getRegional() + "</td>"
                        + "<td align='center'>" + o.getIdZona() + "</td>"
                        + "<td align='center'>" + o.getZona() + "</td>"
                        + "<td align='center'>" + o.getIdTerritorio() + "</td>"
                        + "<td align='center'>" + o.getTerritorio() + "</td>"
                        + "<td align='center'>" + o.getFechaRespuesta() + "</td>"
                        + "<td align='center'>" + o.getSemana() + "</td>"
                        + "<td align='center'>" + o.getNombreModuloP() + "</td>"
                        + "<td align='center'>" + o.getNombreMod() + "</td>"
                        + "<td align='center'>" + o.getIdPregunta() + "</td>"
                        + "<td align='center'>" + o.getDesPregunta() + "</td>"
                        + "<td align='center'>" + o.getRespuesta() + "</td>"
                        + "<td align='center'>" + o.getNombreChecklist() + "</td>"
                        + "<td align='center'>" + o.getMetodoChecklist() + "</td>"
                        + "<td align='center'>" + o.getObservaciones() + "</td>"
                        + "<td align='center'>" + o.getCompromisos() + "</td>"
                        + "<td align='center'>" + o.getFechaCompromiso() + "</td>"
                        + "<td align='center'>" + o.getIdResposable() + "</td>"
                        + "</tr>";
            }
            archivo += "</tbody><table>";
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
            response.setContentType("Content-type: application/xls; name='excel'");
            response.setHeader("Content-Disposition", "attachment; filename=reporte" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();
            //byte[] encode = archivo.getBytes("UTF-8");
            sos.write(archivo.getBytes());
        } catch (Exception e) {
            logger.info(e);
        }
        //ModelAndView mv = new ModelAndView("muestraServicios");
        //mv.addObject("res",lista);
        //mv.addObject("tipo", "REPORTE");

        return null;
    }

    //http://localhost:8080/checklist/reporteService/reporteRegional.json
    //http://localhost:8080/checklist/reporteService/reporteRegional.json?fechaIni=24/11/2016&fechaFin=28/11/2016
    @RequestMapping(value = "/reporteRegional", method = RequestMethod.GET)
    public ModelAndView reporteRegional(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String fechaIni = null;
        String fechaFin = null;

        if (request.getQueryString() != null) {
            fechaIni = request.getParameter("fechaIni");
            fechaFin = request.getParameter("fechaFin");
        }

        try {

            ServletOutputStream sos = null;
            String archivo = "";

            List<ReporteDTO> lista = reportebi.ReporteRegional(fechaIni, fechaFin);

            Iterator<ReporteDTO> it = lista.iterator();
            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                    + "<thead><tr>"
                    + "<th align='center'>Territorio Acción</th>"
                    + "<th align='center'>Zona Acci&oacute;n</th>"
                    + "<th align='center'>Regional</th>"
                    + "<th align='center'>Sucursal</th>"
                    + "<th align='center'>Fecha que se realiza</th>"
                    + "<th align='center'>Arqueo en caja principal-Diferencia</th>"
                    + "<th align='center'>Monto</th>"
                    + "<th align='center'>Motivo</th>"
                    + "<th align='center'>Fecha de cuadre</th>"
                    + "<th align='center'>Arqueo en Caja Anclada - Diferencia</th>"
                    + "<th align='center'>Monto</th>"
                    + "<th align='center'>Motivo</th>"
                    + "<th align='center'>Fecha de cuadre</th>"
                    + "</tr></thead>";
            archivo += "<tbody>";

            if (lista != null && lista.size() > 0) {
                while (it.hasNext()) {
                    ReporteDTO o = it.next();
                    archivo += "<tr><td align='center'>" + o.getTerritorio() + "</td>"
                            + "<td align='center'>" + o.getZona() + "</td>"
                            + "<td align='center'>" + o.getRegional() + "</td>"
                            + "<td align='center'>" + o.getSucursal() + "</td>"
                            + "<td align='center'>" + o.getFecha_r() + "</td>"
                            + "<td align='center'>" + o.getPregunta_cp() + "</td>"
                            + "<td align='center'>" + o.getMonto() + "</td>"
                            + "<td align='center'>" + o.getMotivo() + "</td>"
                            + "<td align='center'>" + o.getFecha_c() + "</td>"
                            + "<td align='center'>" + o.getPregunta_ca() + "</td>"
                            + "<td align='center'>" + o.getMonto_ca() + "</td>"
                            + "<td align='center'>" + o.getMotivo_ca() + "</td>"
                            + "<td align='center'>" + o.getFecha_ca() + "</td>"
                            + "</tr>";
                }
            } else {
                archivo += "<tr><td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "</tr>";
            }

            archivo += "</tbody><table>";
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition", "attachment; filename=reporteRegional" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();
            //byte[] encode = archivo.getBytes("UTF-8");
            sos.write(archivo.getBytes());
        } catch (Exception e) {
            logger.info(e);
        }
        return null;
    }

    //http://localhost:8080/checklist/reporteService/reporteGeneral.json?idChecklist=30&fechaInicio=12/12/2016&fechaFin=12/12/2016
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/reporteGeneral", method = RequestMethod.GET)
    public @ResponseBody
    Boolean reporteGeneral(HttpServletRequest request, HttpServletResponse response) throws Exception {

        ////System.out.println("Entre al controller");
        Map<String, Object> mapReporteGeneral = null;
        List<PreguntaDTO> listaPreguntas = null;
        List<ReporteDTO> listaRespuestas = null;
        List<ReporteDTO> listaRespuestasFinal = new ArrayList<ReporteDTO>();
        int idChecklist = 0;
        int conteo = 0;

        if (request.getQueryString() != null) {
            String uri = request.getQueryString();
            if (uri.contains("idChecklist")) {
                idChecklist = Integer.parseInt(request.getParameter("idChecklist"));
                String fechaInicio = null;
                String fechaFin = null;

                if (uri.contains("fechaInicio")) {
                    fechaInicio = request.getParameter("fechaInicio");
                }

                if (uri.contains("fechaFin")) {
                    fechaFin = request.getParameter("fechaFin");
                }

                try {
                    mapReporteGeneral = reportebi.ReporteGeneral(idChecklist, fechaInicio, fechaFin);
                    if (mapReporteGeneral != null) {
                        Iterator<Map.Entry<String, Object>> enMap = mapReporteGeneral.entrySet().iterator();
                        while (enMap.hasNext()) {
                            Map.Entry<String, Object> enMaps = enMap.next();
                            String nomH = enMaps.getKey();
                            if (nomH == "listaPregunta") {
                                listaPreguntas = (List<PreguntaDTO>) enMaps.getValue();
                            }
                            if (nomH == "listaRespuesta") {
                                listaRespuestas = (List<ReporteDTO>) enMaps.getValue();
                            }
                            if (nomH == "conteo") {
                                conteo = (Integer) enMaps.getValue();
                            }
                        }

                        ServletOutputStream sos = null;
                        String archivo = "";
                        String columnaTitle = "";

                        Iterator<PreguntaDTO> itPreguntaDTO = null;
                        if (listaPreguntas != null) {
                            itPreguntaDTO = listaPreguntas.iterator();
                        }

                        if (itPreguntaDTO != null) {
                            while (itPreguntaDTO.hasNext()) {
                                PreguntaDTO preguntaDTO = itPreguntaDTO.next();
                                columnaTitle += "<th align='center'>" + preguntaDTO.getPregunta() + "</th>";
                            }
                        }

                        if (listaRespuestas != null && listaPreguntas != null) {
                            if (listaRespuestas.size() > 0) {

                                int numBitacoras = 1;
                                for (int a = 0; a < listaRespuestas.size() - 1; a++) {
                                    if (listaRespuestas.get(a + 1) != null) {
                                        if (listaRespuestas.get(a).getIdBitacora() != listaRespuestas.get(a + 1).getIdBitacora()) {
                                            numBitacoras++;
                                        }
                                    }
                                }

                                int k = 0;
                                for (int i = 0; i < numBitacoras; i++) {
                                    for (int j = 0; j < conteo; j++) {
                                        //logger.info("...   No. Bita "+(i+1)+"   ...   bitacora "+listaRespuestas.get(k).getIdBitacora()+"   ....   variable Preguntas "+ listaPreguntas.get(j).getIdPregunta()+"   ...   variable Respuestas "+listaRespuestas.get(k).getIdPregunta()  +"   ...");

                                        if (listaPreguntas.get(j).getIdPregunta() == Integer.parseInt(listaRespuestas.get(k).getIdPregunta())) {
                                            listaRespuestasFinal.add(listaRespuestas.get(k));
                                            if (k < listaRespuestas.size() - 1) {
                                                k++;
                                            }
                                        } else {
                                            ReporteDTO repo = new ReporteDTO();
                                            repo.setDesPregunta("vacio");
                                            repo.setRespuesta("");
                                            listaRespuestasFinal.add(repo);
                                        }
                                    }
                                }

                                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                        + "<thead>"
                                        + "<tr>"
                                        + "<th align='center'>Fecha que se Realizo</th>"
                                        + "<th align='center'>Quien lo realizo</th>"
                                        + "<th align='center'>Número de Usuario</th>"
                                        + "<th align='center'>Puesto</th>"
                                        + "<th align='center'>País</th>"
                                        + "<th align='center'>Territorio</th>"
                                        + "<th align='center'>Zona</th>"
                                        + "<th align='center'>Regional</th>"
                                        + "<th align='center'>Sucursal</th>"
                                        + "<th align='center'>Canal</th>"
                                        + columnaTitle
                                        + "</tr>"
                                        + "</thead>";
                                archivo += "<tbody>";

                                int inc = 0;
                                for (int i = 0; i < numBitacoras; i++) {
                                    ReporteDTO reporteDTO = listaRespuestasFinal.get(inc);
                                    archivo += "<tr>"
                                            + "<td align='center'>" + reporteDTO.getFecha_r() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getNombreUsuario() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getIdUsuario() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getPuesto() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getPais() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getTerritorio() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getZona() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getRegional() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getSucursal() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getCanal() + "</td>";

                                    for (int j = 0; j < conteo; j++) {
                                        ReporteDTO reporteDTO2 = listaRespuestasFinal.get(inc);
                                        archivo += "<td align='center'>" + reporteDTO2.getRespuesta() + "</td>";
                                        inc++;
                                    }
                                    archivo += "</tr>";
                                }

                            } else {
                                logger.info("NO EXISTEN RESPUESTAS");

                                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                        + "<thead>"
                                        + "<tr>"
                                        + "<th align='center'>Fecha que se Realizo</th>"
                                        + "<th align='center'>Quien lo realizo</th>"
                                        + "<th align='center'>Número de Usuario</th>"
                                        + "<th align='center'>Puesto</th>"
                                        + "<th align='center'>País</th>"
                                        + "<th align='center'>Territorio</th>"
                                        + "<th align='center'>Zona</th>"
                                        + "<th align='center'>Regional</th>"
                                        + "<th align='center'>Sucursal</th>"
                                        + "<th align='center'>Canal</th>"
                                        + columnaTitle
                                        + "</tr>"
                                        + "</thead>";
                                archivo += "<tbody>";

                                archivo += "<tr>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>";

                                for (int j = 0; j < conteo; j++) {
                                    archivo += "<td align='center'>Sin Respuesta</td>";
                                }

                                archivo += "</tr>";
                            }
                        } else {
                            logger.info("NO EXISTEN RESPUESTAS");

                            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                    + "<thead>"
                                    + "<tr>"
                                    + "<th align='center'>Fecha que se Realizo</th>"
                                    + "<th align='center'>Quien lo realizo</th>"
                                    + "<th align='center'>Número de Usuario</th>"
                                    + "<th align='center'>Puesto</th>"
                                    + "<th align='center'>País</th>"
                                    + "<th align='center'>Territorio</th>"
                                    + "<th align='center'>Zona</th>"
                                    + "<th align='center'>Regional</th>"
                                    + "<th align='center'>Sucursal</th>"
                                    + "<th align='center'>Canal</th>"
                                    + columnaTitle
                                    + "</tr>"
                                    + "</thead>";
                            archivo += "<tbody>";

                            archivo += "<tr>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>";

                            for (int j = 0; j < conteo; j++) {
                                archivo += "<td align='center'>Sin Respuesta</td>";
                            }

                            archivo += "</tr>";
                        }

                        archivo += "</tbody><table>";
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Date date = new Date();
                        String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
                        response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
                        response.setHeader("Content-Disposition", "attachment; filename=reporteGeneral" + fecha + ".xls");
                        response.setHeader("Pragma: no-cache", "Expires: 0");
                        sos = response.getOutputStream();
                        //byte[] encode = archivo.getBytes("UTF-8");
                        sos.write(archivo.getBytes());
                    } else {
                        logger.info("LA CONSULTA DEL CHECKLIST ESTA VACIA");
                    }
                } catch (Exception e) {
                    logger.info(e);
                }
            } else {
                logger.info("NO SE INGRESO EL IDCKECKLIST EN LA URL");
            }

        } else {
            logger.info("NO SE INGRESO EL IDCKECKLIST EN LA URL");
        }
        return null;
    }

    //-----------------------------------------------------
    //http://localhost:8080/checklist/reporteService/reporteGeneralComp.json?idChecklist=54&fechaInicio=01/04/2017&fechaFin=07/04/2017
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/reporteGeneralComp", method = RequestMethod.GET)
    public @ResponseBody
    Boolean reporteGeneralComp(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> mapReporteGeneral = null;
        List<PreguntaDTO> listaPreguntas = null;
        List<ReporteDTO> listaRespuestas = null;
        List<ReporteDTO> listaRespuestasFinal = new ArrayList<ReporteDTO>();
        int idChecklist = 0;
        int conteo = 0;

        if (request.getQueryString() != null) {
            String uri = request.getQueryString();
            if (uri.contains("idChecklist")) {
                idChecklist = Integer.parseInt(request.getParameter("idChecklist"));
                String fechaInicio = null;
                String fechaFin = null;

                if (uri.contains("fechaInicio")) {
                    fechaInicio = request.getParameter("fechaInicio");
                }

                if (uri.contains("fechaFin")) {
                    fechaFin = request.getParameter("fechaFin");
                }

                try {
                    mapReporteGeneral = reportebi.ReporteGeneralCompromisos(idChecklist, fechaInicio, fechaFin);
                    if (mapReporteGeneral != null) {
                        Iterator<Map.Entry<String, Object>> enMap = mapReporteGeneral.entrySet().iterator();
                        while (enMap.hasNext()) {
                            Map.Entry<String, Object> enMaps = enMap.next();
                            String nomH = enMaps.getKey();
                            if (nomH == "listaPregunta") {
                                listaPreguntas = (List<PreguntaDTO>) enMaps.getValue();
                            }
                            if (nomH == "listaRespuesta") {
                                listaRespuestas = (List<ReporteDTO>) enMaps.getValue();
                            }
                            if (nomH == "conteo") {
                                conteo = (Integer) enMaps.getValue();
                            }
                        }

                        ServletOutputStream sos = null;
                        String archivo = "";
                        String columnaTitle = "";

                        if (listaPreguntas != null) {
                            Iterator<PreguntaDTO> itPreguntaDTO = listaPreguntas.iterator();
                            while (itPreguntaDTO.hasNext()) {
                                PreguntaDTO preguntaDTO = itPreguntaDTO.next();
                                columnaTitle += "<th align='center'>" + preguntaDTO.getPregunta() + "</th>";
                            }
                        }

                        if (listaRespuestas != null && listaPreguntas != null) {
                            if (listaRespuestas.size() > 0) {

                                int numBitacoras = 1;
                                for (int a = 0; a < listaRespuestas.size() - 1; a++) {
                                    if (listaRespuestas.get(a + 1) != null) {
                                        if (listaRespuestas.get(a).getIdBitacora() != listaRespuestas.get(a + 1).getIdBitacora()) {
                                            numBitacoras++;
                                        }
                                    }
                                }

                                int k = 0;
                                for (int i = 0; i < numBitacoras; i++) {
                                    for (int j = 0; j < conteo; j++) {
                                        //logger.info("...   No. Bita "+(i+1)+"   ...   bitacora "+listaRespuestas.get(k).getIdBitacora()+"   ....   variable Preguntas "+ listaPreguntas.get(j).getIdPregunta()+"   ...   variable Respuestas "+listaRespuestas.get(k).getIdPregunta()  +"   ...");

                                        if (listaPreguntas.get(j).getIdPregunta() == Integer.parseInt(listaRespuestas.get(k).getIdPregunta())) {
                                            listaRespuestasFinal.add(listaRespuestas.get(k));
                                            if (k < listaRespuestas.size() - 1) {
                                                k++;
                                            }
                                        } else {
                                            ReporteDTO repo = new ReporteDTO();
                                            repo.setDesPregunta("vacio");
                                            repo.setRespuesta("");
                                            listaRespuestasFinal.add(repo);
                                        }
                                    }
                                }

                                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                        + "<thead>"
                                        + "<tr>"
                                        + "<th align='center'>Fecha que se Realizo</th>"
                                        + "<th align='center'>Quien lo realizo</th>"
                                        + "<th align='center'>Número de Usuario</th>"
                                        + "<th align='center'>Puesto</th>"
                                        + "<th align='center'>País</th>"
                                        + "<th align='center'>Territorio</th>"
                                        + "<th align='center'>Zona</th>"
                                        + "<th align='center'>Regional</th>"
                                        + "<th align='center'>Sucursal</th>"
                                        + "<th align='center'>Canal</th>"
                                        + "<th align='center'>Ponderacion</th>"
                                        + columnaTitle
                                        + "</tr>"
                                        + "</thead>";
                                archivo += "<tbody>";

                                int inc = 0;
                                int auxInc = 0;
                                for (int i = 0; i < numBitacoras; i++) {
                                    auxInc = inc;
                                    for (int r = 0; r < conteo; r++) {
                                        ReporteDTO reporteDTO = listaRespuestasFinal.get(auxInc);
                                        if (reporteDTO.getFecha_r() != null) {
                                            archivo += "<tr>"
                                                    + "<td align='center'>" + reporteDTO.getFecha_r() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getNombreUsuario() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getIdUsuario() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getPuesto() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getPais() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getTerritorio() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getZona() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getRegional() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getSucursal() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getCanal() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getPonderacion() + "</td>";
                                            break;
                                        }
                                        auxInc++;
                                    }

                                    for (int j = 0; j < conteo; j++) {
                                        ReporteDTO reporteDTO2 = listaRespuestasFinal.get(inc);
                                        archivo += "<td align='center'>" + reporteDTO2.getRespuesta() + "</td>";
                                        inc++;
                                    }
                                    archivo += "</tr>";
                                }

                            } else {
                                logger.info("NO EXISTEN RESPUESTAS");

                                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                        + "<thead>"
                                        + "<tr>"
                                        + "<th align='center'>Fecha que se Realizo</th>"
                                        + "<th align='center'>Quien lo realizo</th>"
                                        + "<th align='center'>Número de Usuario</th>"
                                        + "<th align='center'>Puesto</th>"
                                        + "<th align='center'>País</th>"
                                        + "<th align='center'>Territorio</th>"
                                        + "<th align='center'>Zona</th>"
                                        + "<th align='center'>Regional</th>"
                                        + "<th align='center'>Sucursal</th>"
                                        + "<th align='center'>Canal</th>"
                                        + "<th align='center'>Ponderacion</th>"
                                        + columnaTitle
                                        + "</tr>"
                                        + "</thead>";
                                archivo += "<tbody>";

                                archivo += "<tr>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>";

                                for (int j = 0; j < conteo; j++) {
                                    archivo += "<td align='center'>Sin Respuesta</td>";
                                }

                                archivo += "</tr>";
                            }
                        } else {
                            logger.info("NO EXISTEN RESPUESTAS");

                            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                    + "<thead>"
                                    + "<tr>"
                                    + "<th align='center'>Fecha que se Realizo</th>"
                                    + "<th align='center'>Quien lo realizo</th>"
                                    + "<th align='center'>Número de Usuario</th>"
                                    + "<th align='center'>Puesto</th>"
                                    + "<th align='center'>País</th>"
                                    + "<th align='center'>Territorio</th>"
                                    + "<th align='center'>Zona</th>"
                                    + "<th align='center'>Regional</th>"
                                    + "<th align='center'>Sucursal</th>"
                                    + "<th align='center'>Canal</th>"
                                    + "<th align='center'>Ponderacion</th>"
                                    + columnaTitle
                                    + "</tr>"
                                    + "</thead>";
                            archivo += "<tbody>";

                            archivo += "<tr>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>";

                            for (int j = 0; j < conteo; j++) {
                                archivo += "<td align='center'>Sin Respuesta</td>";
                            }

                            archivo += "</tr>";
                        }

                        archivo += "</tbody><table>";
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Date date = new Date();
                        String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
                        response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
                        response.setHeader("Content-Disposition", "attachment; filename=reporteGeneralComp" + fecha + ".xls");
                        response.setHeader("Pragma: no-cache", "Expires: 0");
                        sos = response.getOutputStream();
                        //byte[] encode = archivo.getBytes("UTF-8");
                        sos.write(archivo.getBytes());
                    } else {
                        logger.info("LA CONSULTA DEL CHECKLIST ESTA VACIA");
                    }
                } catch (Exception e) {
                    logger.info(e);
                }
            } else {
                logger.info("NO SE INGRESO EL IDCKECKLIST EN LA URL");
            }

        } else {
            logger.info("NO SE INGRESO EL IDCKECKLIST EN LA URL");
        }
        return null;
    }

    //-----------------------------------------------------
    //http://localhost:8080/checklist/reporteService/reporteProtocolos.json?idChecklist=54&fechaInicio=01/04/2017&fechaFin=07/04/2017
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/reporteProtocolos", method = RequestMethod.GET)
    public @ResponseBody
    Boolean reporteProtocolos(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> mapReporteGeneral = null;
        List<PreguntaDTO> listaPreguntas = null;
        List<ReporteDTO> listaRespuestas = null;
        List<ReporteDTO> listaRespuestasFinal = new ArrayList<ReporteDTO>();
        int idChecklist = 0;
        int conteo = 0;

        if (request.getQueryString() != null) {
            String uri = request.getQueryString();
            if (uri.contains("idChecklist")) {
                idChecklist = Integer.parseInt(request.getParameter("idChecklist"));
                String fechaInicio = null;
                String fechaFin = null;

                if (uri.contains("fechaInicio")) {
                    fechaInicio = request.getParameter("fechaInicio");
                }

                if (uri.contains("fechaFin")) {
                    fechaFin = request.getParameter("fechaFin");
                }

                try {
                    mapReporteGeneral = reportebi.ReporteGeneralProtocolos(idChecklist, fechaInicio, fechaFin);
                    if (mapReporteGeneral != null) {
                        Iterator<Map.Entry<String, Object>> enMap = mapReporteGeneral.entrySet().iterator();
                        while (enMap.hasNext()) {
                            Map.Entry<String, Object> enMaps = enMap.next();
                            String nomH = enMaps.getKey();
                            if (nomH == "listaPregunta") {
                                listaPreguntas = (List<PreguntaDTO>) enMaps.getValue();
                            }
                            if (nomH == "listaRespuesta") {
                                listaRespuestas = (List<ReporteDTO>) enMaps.getValue();
                            }
                            if (nomH == "conteo") {
                                conteo = (Integer) enMaps.getValue();
                            }
                        }

                        ServletOutputStream sos = null;
                        String archivo = "";
                        String columnaTitle = "";

                        if (listaPreguntas != null) {
                            Iterator<PreguntaDTO> itPreguntaDTO = listaPreguntas.iterator();
                            while (itPreguntaDTO.hasNext()) {
                                PreguntaDTO preguntaDTO = itPreguntaDTO.next();
                                columnaTitle += "<th align='center'>" + preguntaDTO.getPregunta() + "</th>";
                            }
                        }

                        if (listaRespuestas != null && listaPreguntas != null) {
                            if (listaRespuestas.size() > 0) {

                                int numBitacoras = 1;
                                for (int a = 0; a < listaRespuestas.size() - 1; a++) {
                                    if (listaRespuestas.get(a + 1) != null) {
                                        if (listaRespuestas.get(a).getIdBitacora() != listaRespuestas.get(a + 1).getIdBitacora()) {
                                            numBitacoras++;
                                        }
                                    }
                                }

                                int k = 0;
                                for (int i = 0; i < numBitacoras; i++) {
                                    for (int j = 0; j < conteo; j++) {
                                        //logger.info("...   No. Bita "+(i+1)+"   ...   bitacora "+listaRespuestas.get(k).getIdBitacora()+"   ....   variable Preguntas "+ listaPreguntas.get(j).getIdPregunta()+"   ...   variable Respuestas "+listaRespuestas.get(k).getIdPregunta()  +"   ...");

                                        if (listaPreguntas.get(j).getIdPregunta() == Integer.parseInt(listaRespuestas.get(k).getIdPregunta())) {
                                            listaRespuestasFinal.add(listaRespuestas.get(k));
                                            if (k < listaRespuestas.size() - 1) {
                                                k++;
                                            }
                                        } else {
                                            ReporteDTO repo = new ReporteDTO();
                                            repo.setDesPregunta("vacio");
                                            repo.setRespuesta("");
                                            if (listaPreguntas.get(j).getIdPregunta() == 0) {
                                                repo.setRespuesta("-100");
                                            }
                                            if (listaPreguntas.get(j).getIdPregunta() == 1) {
                                                repo.setRespuesta("-101");
                                            }
                                            listaRespuestasFinal.add(repo);
                                        }
                                    }
                                }

                                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                        + "<thead>"
                                        + "<tr>"
                                        + "<th align='center'>Fecha que se Realizo</th>"
                                        + "<th align='center'>Quien lo realizo</th>"
                                        + "<th align='center'>Número de Usuario</th>"
                                        + "<th align='center'>Puesto</th>"
                                        + "<th align='center'>País</th>"
                                        + "<th align='center'>Territorio</th>"
                                        + "<th align='center'>Zona</th>"
                                        + "<th align='center'>Regional</th>"
                                        + "<th align='center'>Sucursal</th>"
                                        + "<th align='center'>No. Sucursal</th>"
                                        + "<th align='center'>Canal</th>"
                                        + columnaTitle
                                        + "</tr>"
                                        + "</thead>";
                                archivo += "<tbody>";

                                int inc = 0;
                                int auxInc = 0;
                                for (int i = 0; i < numBitacoras; i++) {
                                    auxInc = inc;
                                    for (int r = 0; r < conteo; r++) {
                                        ReporteDTO reporteDTO = listaRespuestasFinal.get(auxInc);
                                        if (reporteDTO.getFecha_r() != null) {
                                            archivo += "<tr>"
                                                    + "<td align='center'>" + reporteDTO.getFecha_r() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getNombreUsuario() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getIdUsuario() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getPuesto() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getPais() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getTerritorio() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getZona() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getRegional() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getNombreSucursal() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getSucursal() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getCanal() + "</td>";
                                            break;
                                        }
                                        auxInc++;
                                    }

                                    int idBitacora = listaRespuestasFinal.get(inc).getIdBitacora();
                                    for (int j = 0; j < conteo; j++) {
                                        ReporteDTO reporteDTO2 = listaRespuestasFinal.get(inc);
                                        if (reporteDTO2.getRespuesta() == "-100") {
                                            String precalif = reportebi.ReporteCalificacion(idBitacora, 1) + "";
                                            if (precalif.equals("1")) {
                                                precalif = "-";
                                            }
                                            archivo += "<td align='center'>" + precalif + "</td>";
                                        }
                                        if (reporteDTO2.getRespuesta() == "-101") {
                                            String calif = reportebi.ReporteCalificacion(idBitacora, 2) + "";
                                            if (calif.equals("1")) {
                                                calif = "-";
                                            }
                                            archivo += "<td align='center'>" + calif + "</td>";
                                        }
                                        if (reporteDTO2.getRespuesta() != "-101" && reporteDTO2.getRespuesta() != "-100") {
                                            archivo += "<td align='center'>" + reporteDTO2.getRespuesta() + "</td>";
                                        }
                                        inc++;
                                    }
                                    archivo += "</tr>";
                                }

                            } else {
                                logger.info("NO EXISTEN RESPUESTAS");

                                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                        + "<thead>"
                                        + "<tr>"
                                        + "<th align='center'>Fecha que se Realizo</th>"
                                        + "<th align='center'>Quien lo realizo</th>"
                                        + "<th align='center'>Número de Usuario</th>"
                                        + "<th align='center'>Puesto</th>"
                                        + "<th align='center'>País</th>"
                                        + "<th align='center'>Territorio</th>"
                                        + "<th align='center'>Zona</th>"
                                        + "<th align='center'>Regional</th>"
                                        + "<th align='center'>Sucursal</th>"
                                        + "<th align='center'>No. Sucursal</th>"
                                        + "<th align='center'>Canal</th>"
                                        + columnaTitle
                                        + "</tr>"
                                        + "</thead>";
                                archivo += "<tbody>";

                                archivo += "<tr>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>";

                                for (int j = 0; j < conteo; j++) {
                                    archivo += "<td align='center'>Sin Respuesta</td>";
                                }

                                archivo += "</tr>";
                            }
                        } else {
                            logger.info("NO EXISTEN RESPUESTAS");

                            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                    + "<thead>"
                                    + "<tr>"
                                    + "<th align='center'>Fecha que se Realizo</th>"
                                    + "<th align='center'>Quien lo realizo</th>"
                                    + "<th align='center'>Número de Usuario</th>"
                                    + "<th align='center'>Puesto</th>"
                                    + "<th align='center'>País</th>"
                                    + "<th align='center'>Territorio</th>"
                                    + "<th align='center'>Zona</th>"
                                    + "<th align='center'>Regional</th>"
                                    + "<th align='center'>Sucursal</th>"
                                    + "<th align='center'>Canal</th>"
                                    + "<th align='center'>Ponderacion</th>"
                                    + columnaTitle
                                    + "</tr>"
                                    + "</thead>";
                            archivo += "<tbody>";

                            archivo += "<tr>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>";

                            for (int j = 0; j < conteo; j++) {
                                archivo += "<td align='center'>Sin Respuesta</td>";
                            }

                            archivo += "</tr>";
                        }

                        archivo += "</tbody><table>";
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Date date = new Date();
                        String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
                        response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
                        response.setHeader("Content-Disposition", "attachment; filename=reporteGeneralComp" + fecha + ".xls");
                        response.setHeader("Pragma: no-cache", "Expires: 0");
                        sos = response.getOutputStream();
                        //byte[] encode = archivo.getBytes("UTF-8");
                        sos.write(archivo.getBytes());
                    } else {
                        logger.info("LA CONSULTA DEL CHECKLIST ESTA VACIA");
                    }
                } catch (Exception e) {
                    logger.info(e);
                }
            } else {
                logger.info("NO SE INGRESO EL IDCKECKLIST EN LA URL");
            }

        } else {
            logger.info("NO SE INGRESO EL IDCKECKLIST EN LA URL");
        }
        return null;
    }

    //-----------------------------------------------------
    //http://localhost:8080/checklist/reporteService/reporteCheckUsua.json
    //http://localhost:8080/checklist/reporteService/reporteCheckUsua.json?idChecklist=30&idPuesto=25
    @RequestMapping(value = "/reporteCheckUsua", method = RequestMethod.GET)
    public ModelAndView reporteCheckUsua(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String idChecklist = null;
        String idPuesto = null;

        if (request.getQueryString() != null) {
            idChecklist = request.getParameter("idChecklist");
            idPuesto = request.getParameter("idPuesto");
        }

        try {

            ServletOutputStream sos = null;
            String archivo = "";

            List<ReporteDTO> lista = reportebi.ReporteCheckUsua(idChecklist, idPuesto);

            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                    + "<thead>"
                    + "<tr>"
                    + "<th align='center'>Territorio</th>"
                    + "<th align='center'>Zona</th>"
                    + "<th align='center'>Regional</th>"
                    + "<th align='center'>Id Sucursal</th>"
                    + "<th align='center'>Nombre Sucursal</th>"
                    + "<th align='center'>Id Usuario</th>"
                    + "<th align='center'>Nombre</th>"
                    + "<th align='center'>Id Puesto</th>"
                    + "<th align='center'>Descripcion Puesto</th>"
                    + "<th align='center'>Id Checklist</th>"
                    + "<th align='center'>Nombre Checklist</th>"
                    + "</tr>"
                    + "</thead>";
            archivo += "<tbody>";

            if (lista != null && lista.size() > 0) {
                for (int i = 0; i < lista.size(); i++) {
                    ReporteDTO reporteDTO = lista.get(i);
                    archivo += "<tr>"
                            + "<td align='center'>" + reporteDTO.getTerritorio() + "</td>"
                            + "<td align='center'>" + reporteDTO.getZona() + "</td>"
                            + "<td align='center'>" + reporteDTO.getRegional() + "</td>"
                            + "<td align='center'>" + reporteDTO.getSucursal() + "</td>"
                            + "<td align='center'>" + reporteDTO.getNombreSucursal() + "</td>"
                            + "<td align='center'>" + reporteDTO.getIdUsuario() + "</td>"
                            + "<td align='center'>" + reporteDTO.getNombreUsuario() + "</td>"
                            + "<td align='center'>" + reporteDTO.getIdPuesto() + "</td>"
                            + "<td align='center'>" + reporteDTO.getPuesto() + "</td>"
                            + "<td align='center'>" + reporteDTO.getIdChecklist() + "</td>"
                            + "<td align='center'>" + reporteDTO.getNombreChecklist() + "</td>";
                    archivo += "</tr>";
                }
            } else {
                archivo += "<tr>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>";
                archivo += "</tr>";
            }

            archivo += "</tbody><table>";
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition", "attachment; filename=reporteCheckUsua" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();
            //byte[] encode = archivo.getBytes("UTF-8");
            sos.write(archivo.getBytes());
        } catch (Exception e) {
            logger.info(e);
        }
        return null;
    }

    //http://localhost:8080/checklist/reporteService/reporteRH.json?idChecklist=40&inicio=<?>&fin=<?>
    @RequestMapping(value = "/reporteRH", method = RequestMethod.GET)
    public ModelAndView reporteRH(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String inicio = request.getParameter("inicio");
        String fin = request.getParameter("fin");
        String checklist = request.getParameter("idChecklist");

        ServletOutputStream sos = null;
        String archivo = "";
        try {

            List<VisitasxMesDTO> lista = visitasxMesBI.obtieneVisitas(Integer.parseInt(checklist), inicio, fin);

            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                    + "<thead>"
                    + "<tr>"
                    + "<th align='center'>Visitas</th>"
                    + "<th align='center'>Usuario</th>"
                    + "<th align='center'>Ceco</th>"
                    + "<th align='center'>Nombre Ceco</th>"
                    + "<th align='center'>Mes</th>"
                    + "</tr>"
                    + "</thead>";
            archivo += "<tbody>";

            if (lista != null && lista.size() > 0) {
                for (int i = 0; i < lista.size(); i++) {
                    VisitasxMesDTO reporteDTO = (VisitasxMesDTO) lista.get(i);
                    archivo += "<tr>"
                            + "<td align='center'>" + reporteDTO.getVisitas() + "</td>"
                            + "<td align='center'>" + reporteDTO.getUsuario() + "</td>"
                            + "<td align='center'>" + reporteDTO.getCeco() + "</td>"
                            + "<td align='center'>" + reporteDTO.getNombreCeco() + "</td>"
                            + "<td align='center'>" + reporteDTO.getMes() + "</td>";
                    archivo += "</tr>";
                }

            } else {
                archivo += "<tr>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>";
                archivo += "</tr>";
            }

            archivo += "</tbody><table>";
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition", "attachment; filename=reporteVisitasRH" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();
            //byte[] encode = archivo.getBytes("UTF-8");
            sos.write(archivo.getBytes());
        } catch (Exception e) {
            logger.info(e);

        }
        return null;
    }

    //http://10.51.210.239:8080/checklist/reporteService/reporteAsignaVisitas2.json?bunker=1&pais=0
    @RequestMapping(value = "/reporteAsignaVisitas2", method = RequestMethod.GET)
    public @ResponseBody
    String reporteAsignaVisitas2(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String estado = "Fallo";
        int bunker = Integer.parseInt(request.getParameter("bunker"));
        int pais = Integer.parseInt(request.getParameter("pais"));

        List<AsignacionCheckDTO> asignaciones = null;
        Calendar calendar = Calendar.getInstance();
        int numeroSemana = calendar.get(Calendar.WEEK_OF_YEAR) + 1;

        //OutputStream sos = null;
        List<String> destinatarios = new ArrayList<>();
        List<String> copiados = new ArrayList<>();

        String archivo = "";
        try {
            logger.info("bunker " + bunker);
            asignaciones = asignacionCheckBI.obtieneAsignacionC(bunker, pais);
            logger.info("asignaciones " + asignaciones);

            if (asignaciones != null && asignaciones.size() > 0) {

                archivo = "<table style='width:80%; border-collapse:collapse;' cellpadding='30' cellspacing='0' border='1' bordercolor='#666666' id='Exportar_a_Excel'>"
                        + "<thead>"
                        + "<tr>"
                        + "<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Periodo</th>"
                        + "<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Área</th>"
                        + "<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Recurso</th>"
                        + "<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Día</th>"
                        + "<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Tienda</th>"
                        + "<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Responsable</th>"
                        + "<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Entra a Bunker</th>"
                        + "<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Horario en Bunker</th>"
                        + "<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Justificación</th>"
                        + "</tr>"
                        + "</thead>";
                archivo += "<tbody>";

                UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
                String direccion = obtenDireccion(Integer.parseInt(userSession.getIdUsuario()));

                for (int i = 0; i < asignaciones.size(); i++) {
                    AsignacionCheckDTO aux = asignaciones.get(i);
                    copiados.add(aux.getCorreoC());
                    if (i == 0) {
                        destinatarios.add(aux.getCorreoD());
                    }
                    archivo += "<tr>"
                            + "<td align='center' style='font:12px; white-space: nowrap;'>Semana " + numeroSemana + "</td>"
                            + "<td align='center' style='font:12px; white-space: nowrap;'>" + direccion + "</td>"
                            + "<td align='center' style='font:12px; white-space: nowrap;'>" + aux.getNombreUsuario() + "</td>"
                            + "<td align='center' style='font:12px; white-space: nowrap;'>" + aux.getFecha() + "</td>"
                            + "<td align='center' style='font:12px; white-space: nowrap;'>" + aux.getNombreSucursal() + "</td>"
                            + "<td align='center' style='font:12px; white-space: nowrap;'>" + userSession.getNombre() + "</td>"
                            + (aux.getBunker() != 1 ? "<td align='center' style='font:12px; white-space: nowrap;'>NO</td><td align='center' style='font:12px; white-space: nowrap;'>-</td>" : "<td align='center' style='font:12px; white-space: nowrap;'>SI</td><td align='center' style='font:12px; white-space: nowrap;'>" + aux.getHoraBunker() + "</td>")
                            + "<td align='center' style='font:12px; white-space: nowrap;'>" + aux.getJustificacion() + "</td>";
                    archivo += "</tr>";

                }

                archivo += "</tbody><table>";

                logger.info("destinatarios " + destinatarios.toString());
                logger.info("copiados " + copiados.toString());

                byte[] encode = archivo.getBytes("UTF-8");

                File temp = null;
                temp = File.createTempFile("reporteVisitas-", ".xls");
                //temp.renameTo( new File( temp.getParent(), "reporteVisitas2.xls"));
                try (FileOutputStream file = new FileOutputStream(temp)) {
                    file.write(encode);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //logger.info("uri "+ temp.getPath());
                //logger.info("uri "+ temp.length());
                estado = "" + correoBI.sendMailVisitas(destinatarios, copiados, temp, "", "");

                /*DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Date date = new Date();
				String fecha = dateFormat.format(date).replace(" ", "_").replace(":","_").replace("/","_");
				response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
				response.setHeader("Content-Disposition","attachment; filename=reporteVisitas"+fecha+".xls");
				response.setHeader("Pragma: no-cache", "Expires: 0");
				sos = response.getOutputStream();*/
                //logger.info("sos "+ sos);
                //escribirPDFHtml(archivo , sos);
                //sos.write(encode);
                //sos.flush();
                //sos.close();
            }

        } catch (Exception e) {
            logger.info(e);
        }
        logger.info("respuesta del envio: " + estado);
        return estado;
    }

    public String obtenDireccion(int idUsuario) {

        String ceco = "";

        List<Usuario_ADTO> listaUsuario = null;

        try {
            listaUsuario = usuario_ABI.obtieneUsuario(idUsuario);
            if (listaUsuario != null & listaUsuario.size() > 0) {
                Usuario_ADTO usuario = listaUsuario.get(0);
                List<CecoDTO> listaCeco = null;
                try {
                    listaCeco = cecoBI.buscaCecoPaso(usuario.getIdCeco(), "NULL", "NULL");
                    if (listaCeco != null & listaCeco.size() > 0) {
                        CecoDTO c = listaCeco.get(0);
                        ceco = c.getDescCeco();
                    }
                } catch (Exception e) {
                    logger.info("Ocurrio algo adentro: " + e);
                }
            }
        } catch (Exception e) {
            logger.info("Ocurrio algo afuera: " + e);
        }
        logger.info("ceco " + ceco);

        return ceco;
    }

    //http://10.51.210.239:8080/checklist/reporteService/reporteAsignaVisitas.json
    @RequestMapping(value = "/reporteAsignaVisitas", method = RequestMethod.GET)
    public @ResponseBody
    String reporteAsignaVisitas(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String estado = "Fallo";

        estado = enviaCorreoVisitasBI.enviaCorre();

        return estado;
    }

    // http://localhost:8080/checklist//reporteService/asignaChecklistSemanal.json
    @RequestMapping(value = "/asignaChecklistSemanal", method = RequestMethod.GET)
    public ModelAndView asignaChecklist(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean resp = asignacionCheckBI.asignaCheclist();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA ESTADO CHECKLIST");
            mv.addObject("res", resp);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    @RequestMapping(value = "/repocheck", method = RequestMethod.GET)
    public ModelAndView repocheck(HttpServletRequest request, HttpServletResponse response) throws Exception {

        int idUsuario = 0;

        if (request.getQueryString() != null) {
            idUsuario = Integer.parseInt(request.getParameter("idUsuario"));

        }

        try {

            ServletOutputStream sos = null;
            String archivo = "";

            List<RepoCheckDTO> lista = repoCheckBI.obtieneDatos(idUsuario);

            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                    + "<thead>"
                    + "<tr>"
                    + "<th align='center'>Empleado</th>"
                    + "<th align='center'>Nombre</th>"
                    + "<th align='center'>Sucursal</th>"
                    + "<th align='center'>Fecha</th>"
                    + "<th align='center'>Fecha Inicio</th>"
                    + "<th align='center'>Fecha Fin</th>"
                    + "<th align='center'>Tiempo Transcurrido</th>"
                    + "</tr>"
                    + "</thead>";
            archivo += "<tbody>";

            if (lista != null && lista.size() > 0) {
                for (int i = 0; i < lista.size(); i++) {
                    RepoCheckDTO repoCheckDTO = lista.get(i);
                    archivo += "<tr>"
                            + "<td align='center'>" + repoCheckDTO.getIdUsu() + "</td>"
                            + "<td align='center'>" + repoCheckDTO.getNombre() + "</td>"
                            + "<td align='center'>" + repoCheckDTO.getCeco() + "</td>"
                            + "<td align='center'>" + repoCheckDTO.getFecha() + "</td>"
                            + "<td align='center'>" + repoCheckDTO.getFechaInic() + "</td>"
                            + "<td align='center'>" + repoCheckDTO.getFechaFin() + "</td>"
                            + "<td align='center'>" + repoCheckDTO.getTiempo() + "</td>";
                    archivo += "</tr>";
                }
            } else {
                archivo += "<tr>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>";
                archivo += "</tr>";
            }

            archivo += "</tbody><table>";
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition", "attachment; filename=repocheck" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();
            //byte[] encode = archivo.getBytes("UTF-8");
            sos.write(archivo.getBytes());
        } catch (Exception e) {
            logger.info(e);
        }
        return null;
    }

    /**
     * ********************************************* REPORTE CHECK IN/OUT
     * *********************************************
     */
    //http://localhost:8080/checklist/reporteService/reporteCheckIn.json?idUsuario=<?>&idCeco=<?>
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/reporteCheckIn", method = RequestMethod.GET)
    public @ResponseBody
    Boolean reporteCheckIn(HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<CheckinDTO> listaReporte = null;
        int idUsuario = 0;
        int conteo = 0;

        if (request.getQueryString() != null) {
            String uri = request.getQueryString();
            if (uri.contains("idUsuario")) {
                idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
                String idCeco = null;
                String archivo = "";

                if (uri.contains("idCeco")) {
                    idCeco = request.getParameter("idCeco");
                }

                try {
                    CheckinDTO asistencia = new CheckinDTO();
                    asistencia.setIdBitacora(0);
                    asistencia.setIdUsuario(idUsuario);
                    asistencia.setIdCeco(idCeco);
                    asistencia.setFecha(null);
                    asistencia.setLatitud(null);
                    asistencia.setLongitud(null);
                    asistencia.setBandera(0);
                    listaReporte = checkinBI.buscaCheckin(asistencia);
                    logger.info("Lista Reporte: " + listaReporte.size());

                    if (listaReporte != null) {
                        ServletOutputStream sos = null;

                        archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                + "<thead>"
                                + "<tr>"
                                + "<th align='center'>Num</th>"
                                + "<th align='center'>Número Bitácora</th>"
                                + "<th align='center'>Número de Usuario</th>"
                                + "<th align='center'>Número de sucursal</th>"
                                + "<th align='center'>Fecha en que se realizo</th>"
                                + "<th align='center'>Latitud</th>"
                                + "<th align='center'>Longitud</th>"
                                + "<th align='center'>Bandera</th>"
                                + "</tr>"
                                + "</thead>";
                        archivo += "<tbody>";

                        for (int i = 0; i < listaReporte.size(); i++) {

                            CheckinDTO reporteDTO = listaReporte.get(i);

                            archivo += "<tr>"
                                    + "<td align='center'>" + i + 1 + "</td>"
                                    + "<td align='center'>" + reporteDTO.getIdBitacora() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getIdUsuario() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getIdCeco() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getFecha() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getLatitud() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getLongitud() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getBandera() + "</td>" + "</tr>";

                        }

                        archivo += "</tbody><table>";
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Date date = new Date();
                        String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
                        response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
                        response.setHeader("Content-Disposition", "attachment; filename=reporteGeneralComp" + fecha + ".xls");
                        response.setHeader("Pragma: no-cache", "Expires: 0");
                        sos = response.getOutputStream();
                        sos.write(archivo.getBytes());
                    } else {
                        logger.info("LA CONSULTA DEL CHECKIN/OUT ESTA VACIA");

                        archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                + "<thead>"
                                + "<tr>"
                                + "<th align='center'>Id</th>"
                                + "<th align='center'>Número Bitácora</th>"
                                + "<th align='center'>Número de Usuario</th>"
                                + "<th align='center'>Número de sucursal</th>"
                                + "<th align='center'>Fecha en que se realizo</th>"
                                + "<th align='center'>Latitud</th>"
                                + "<th align='center'>Longitud</th>"
                                + "<th align='center'>Bandera</th>"
                                + "</tr>"
                                + "</thead>";
                        archivo += "<tbody>";

                        archivo += "<tr>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>";

                        for (int j = 0; j < conteo; j++) {
                            archivo += "<td align='center'>Sin Respuesta</td>";
                        }

                        archivo += "</tr>";
                    }
                } catch (Exception e) {
                    logger.info(e);
                }
            } else { //else perteneciente a if(uri.contains("idUsuario"))
                logger.info("NO SE INGRESO EL IDUSUARIO EN LA URL");
            }

        } else {//else perteneciente a if(request.getQueryString()!=null)
            logger.info("NO SE INGRESO EL IDCKECKLIST EN LA URL");
        }
        return null;
    }

    /**
     * ********************************************* REPORTE CHECK IN/OUT
     * *********************************************
     */
    //http://localhost:8080/checklist/reporteService/asistenciaSupervision.json?idUsuario=<?>&fechaInicio=<?>&fechaFin=<?>
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/asistenciaSupervision", method = RequestMethod.GET)
    public @ResponseBody
    Boolean asistenciaSupervision(HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<AsistenciaSupervisorDTO> listaReporte = null;
        int idUsuario = 0;
        int conteo = 0;

        if (request.getQueryString() != null) {
            String uri = request.getQueryString();
            if (uri.contains("idUsuario")) {
                idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
                String fechaInicio = request.getParameter("fechaInicio");
                String fechaFin = request.getParameter("fechaFin");

                String archivo = "";

                try {
                    AsistenciaSupervisorDTO asistencia = new AsistenciaSupervisorDTO();
                    asistencia.setIdUsuario(idUsuario);
                    asistencia.setFechaInicio(fechaInicio);
                    asistencia.setFechaFin(fechaFin);
                    listaReporte = asistenciaSupervisorBI.buscaAsistencia(asistencia);
                    logger.info("Lista Reporte: " + listaReporte.size());

                    if (listaReporte.size() != 0) {
                        ServletOutputStream sos = null;

                        archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                + "<thead>"
                                + "<tr>"
                                + "<th align='center'>Socio</th>"
                                + "<th align='center'>Fecha</th>"
                                + "<th align='center'>Hora de Entrada</th>"
                                + "<th align='center'>Hora de Salida</th>"
                                + "<th align='center'>Sucursal</th>"
                                + "</tr>"
                                + "</thead>";
                        archivo += "<tbody>";

                        for (int i = 0; i < listaReporte.size(); i++) {

                            AsistenciaSupervisorDTO reporteDTO = listaReporte.get(i);

                            archivo += "<tr>"
                                    + "<td align='center'>" + reporteDTO.getNomUsuario() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getFecha() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getEntrada() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getSalida() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getNomSucursal() + "</td>";

                        }

                        archivo += "</tbody><table>";
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Date date = new Date();
                        String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
                        response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
                        response.setHeader("Content-Disposition", "attachment; filename=reporteGeneralComp" + fecha + ".xls");
                        response.setHeader("Pragma: no-cache", "Expires: 0");
                        sos = response.getOutputStream();
                        sos.write(archivo.getBytes());
                    } else {
                        logger.info("LA CONSULTA DE ASISTENCIA DEL USUARIO ESTA VACIA");

                        archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                + "<thead>"
                                + "<tr>"
                                + "<th align='center'>Socio</th>"
                                + "<th align='center'>Fecha</th>"
                                + "<th align='center'>Hora de Entrada</th>"
                                + "<th align='center'>Hora de Salida</th>"
                                + "<th align='center'>Sucursal</th>"
                                + "</tr>"
                                + "</thead>";
                        archivo += "<tbody>";

                        archivo += "<tr>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>";

                        for (int j = 0; j < conteo; j++) {
                            archivo += "<td align='center'>Sin Respuesta</td>";
                        }

                        archivo += "</tr>";
                    }
                } catch (Exception e) {
                    logger.info(e);
                }
            } else { //else perteneciente a if(uri.contains("idUsuario"))
                logger.info("NO SE INGRESO EL IDUSUARIO EN LA URL");
            }

        } else {//else perteneciente a if(request.getQueryString()!=null)
            logger.info("NO SE INGRESO EL IDCKECKLIST EN LA URL");
        }
        return null;
    }

    /**
     * ********************************************* REPORTE FOLIOS MNTTO
     * SUCURSAL*********************************************
     */
    //http://localhost:8080/checklist/reporteService/reporteFoliosMntto.json?idSucursal=<?>
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/reporteFoliosMntto", method = RequestMethod.GET)
    public @ResponseBody
    Boolean reporteFoliosMntto(HttpServletRequest request, HttpServletResponse response) throws Exception {

        int idSucursal = 0;
        int conteo = 0;

        if (request.getQueryString() != null) {
            String uri = request.getQueryString();
            if (uri.contains("idSucursal")) {
                idSucursal = Integer.parseInt(request.getParameter("idSucursal"));

                String archivo = "";

                try {

                    String json = serviciosMiGestionBI.obtieneFolios(String.valueOf(idSucursal));
                    List<FoliosMtoDTO> listaFolios = new ArrayList<FoliosMtoDTO>();
                    if (json != null) {
                        JSONObject rec = new JSONObject(json);
                        JSONArray aux = rec.getJSONArray("FOLIOS");

                        for (int i = 0; i < aux.length(); i++) {
                            JSONObject jObject = aux.getJSONObject(i);
                            FoliosMtoDTO folios = new FoliosMtoDTO();
                            folios.setIdFolio(jObject.getString("ID_INCIDENTE"));
                            if (!jObject.getString("NO_SUCURSAL").equals("null")) {
                                folios.setIdSucursal(jObject.getString("NO_SUCURSAL"));
                            } else {
                                folios.setIdSucursal("Sin número de sucursal encontrado");
                            }
                            if (!jObject.getString("SUCURSAL").equals("null")) {
                                folios.setNomSucursal(jObject.getString("SUCURSAL"));
                            } else {
                                folios.setNomSucursal("Sin nombre de sucursal encontrado");
                            }
                            if (jObject.getString("PROVEEDOR").equals("null")) {
                                folios.setNomProveedor("Sin proveedor por asignar");
                            } else {
                                folios.setNomProveedor(jObject.getString("PROVEEDOR"));
                            }
                            if (!jObject.getString("INCIDENCIA").equals("null")) {
                                folios.setNomIncidente(jObject.getString("INCIDENCIA"));
                            } else {
                                folios.setNomIncidente("Sin incidente encontrado");
                            }
                            if (!jObject.getString("FECHA_PROGRAMADA").equals("null")) {
                                folios.setFechaProgramada(jObject.getString("FECHA_PROGRAMADA"));
                            } else {
                                folios.setFechaProgramada("Sin fecha programada encontrada");
                            }
                            if (!jObject.getString("ESTADO").equals("null")) {
                                folios.setEstado(jObject.getString("ESTADO"));
                            } else {
                                folios.setEstado("Sin estado encontrado");
                            }
                            if (!jObject.getString("MOTIVO_ESTADO").equals("null")) {
                                folios.setMotivoEstado(jObject.getString("MOTIVO_ESTADO"));
                            } else {
                                folios.setMotivoEstado("Sin motivo estado encontrado");
                            }
                            if (!jObject.getString("FECHA_MODIFICACION").equals("null")) {
                                folios.setFechaModificacion(jObject.getString("FECHA_MODIFICACION"));
                            } else {
                                folios.setFechaModificacion("Sin fecha de modificación encontrada");
                            }
                            if (!jObject.getString("FECHA_CREACION").equals("null")) {
                                folios.setFechaCreacion(jObject.getString("FECHA_CREACION"));
                            } else {
                                folios.setFechaCreacion("Sin fecha de creación encontrada");
                            }

                            listaFolios.add(folios);
                        }

                    }

                    logger.info("Lista Reporte: " + listaFolios.size());

                    if (listaFolios.size() != 0) {
                        ServletOutputStream sos = null;

                        archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                + "<thead>"
                                + "<tr>"
                                + "<th align='center'>Número de Folio</th>"
                                + "<th align='center'>Número de Sucursal</th>"
                                + "<th align='center'>Nombre de Sucursal</th>"
                                + "<th align='center'>Incidente</th>"
                                + "<th align='center'>Fecha Creación</th>"
                                + "<th align='center'>Fecha Programada</th>"
                                + "<th align='center'>Proveedor</th>"
                                + "<th align='center'>Estado</th>"
                                + "<th align='center'>Motivo Estado</th>"
                                + "<th align='center'>Fecha Modificación</th>"
                                + "</tr>"
                                + "</thead>";
                        archivo += "<tbody>";

                        for (int i = 0; i < listaFolios.size(); i++) {

                            FoliosMtoDTO reporteDTO = listaFolios.get(i);

                            archivo += "<tr>"
                                    + "<td align='center'>" + reporteDTO.getIdFolio() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getIdSucursal() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getNomSucursal() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getNomIncidente() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getFechaCreacion() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getFechaProgramada() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getNomProveedor() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getEstado() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getMotivoEstado() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getFechaModificacion() + "</td>";

                        }

                        archivo += "</tbody><table>";
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Date date = new Date();
                        String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
                        response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
                        response.setHeader("Content-Disposition", "attachment; filename=reporteGeneralComp" + fecha + ".xls");
                        response.setHeader("Pragma: no-cache", "Expires: 0");
                        sos = response.getOutputStream();
                        sos.write(archivo.getBytes());
                    } else {
                        logger.info("LA CONSULTA DE ASISTENCIA DEL USUARIO ESTA VACIA");

                        archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                + "<thead>"
                                + "<tr>"
                                + "<th align='center'>Socio</th>"
                                + "<th align='center'>Fecha</th>"
                                + "<th align='center'>Hora de Entrada</th>"
                                + "<th align='center'>Hora de Salida</th>"
                                + "<th align='center'>Sucursal</th>"
                                + "</tr>"
                                + "</thead>";
                        archivo += "<tbody>";

                        archivo += "<tr>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>";

                        for (int j = 0; j < conteo; j++) {
                            archivo += "<td align='center'>Sin Respuesta</td>";
                        }

                        archivo += "</tr>";
                    }
                } catch (Exception e) {
                    logger.info(e);
                }
            } else { //else perteneciente a if(uri.contains("idUsuario"))
                logger.info("NO SE INGRESO EL IDUSUARIO EN LA URL");
            }

        } else {//else perteneciente a if(request.getQueryString()!=null)
            logger.info("NO SE INGRESO EL IDCKECKLIST EN LA URL");
        }
        return null;
    }

    /**
     * ********************************************* REPORTE CHECK IN/OUT
     * *********************************************
     */
    //http://localhost:8080/checklist/reporteService/asistenciaSupervision.json?idBitacora=<?>
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/reporteProtocolo", method = RequestMethod.GET)
    public @ResponseBody
    Boolean reporteProtocolo(HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<AsistenciaSupervisorDTO> listaReporte = null;
        String idBitacora = "";
        int conteo = 0;

        if (request.getQueryString() != null) {
            String uri = request.getQueryString();
            if (uri.contains("idUsuario")) {
                idBitacora = request.getParameter("idUsuario");

                String archivo = "";

                try {
                    List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneDatosGen(idBitacora);
                    logger.info("Lista Reporte: " + lista.size());

                    if (lista.size() != 0) {
                        ServletOutputStream sos = null;

                        archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                + "<thead>"
                                + "<tr>"
                                + "<th align='center'>Pregunta</th>"
                                + "<th align='center'>Respuesta</th>"
                                + "<th align='center'>Porcentaje</th>"
                                + "</tr>"
                                + "</thead>";
                        archivo += "<tbody>";

                        for (int i = 0; i < listaReporte.size(); i++) {

                            AsistenciaSupervisorDTO reporteDTO = listaReporte.get(i);

                            archivo += "<tr>"
                                    + "<td align='center'>" + reporteDTO.getNomUsuario() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getFecha() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getEntrada() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getSalida() + "</td>"
                                    + "<td align='center'>" + reporteDTO.getNomSucursal() + "</td>";

                        }

                        archivo += "</tbody><table>";
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Date date = new Date();
                        String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
                        response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
                        response.setHeader("Content-Disposition", "attachment; filename=reporteGeneralComp" + fecha + ".xls");
                        response.setHeader("Pragma: no-cache", "Expires: 0");
                        sos = response.getOutputStream();
                        sos.write(archivo.getBytes());
                    } else {
                        logger.info("LA CONSULTA DE ASISTENCIA DEL USUARIO ESTA VACIA");

                        archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                + "<thead>"
                                + "<tr>"
                                + "<th align='center'>Socio</th>"
                                + "<th align='center'>Fecha</th>"
                                + "<th align='center'>Hora de Entrada</th>"
                                + "<th align='center'>Hora de Salida</th>"
                                + "<th align='center'>Sucursal</th>"
                                + "</tr>"
                                + "</thead>";
                        archivo += "<tbody>";

                        archivo += "<tr>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>";

                        for (int j = 0; j < conteo; j++) {
                            archivo += "<td align='center'>Sin Respuesta</td>";
                        }

                        archivo += "</tr>";
                    }
                } catch (Exception e) {
                    logger.info(e);
                }
            } else { //else perteneciente a if(uri.contains("idUsuario"))
                logger.info("NO SE INGRESO EL IDUSUARIO EN LA URL");
            }

        } else {//else perteneciente a if(request.getQueryString()!=null)
            logger.info("NO SE INGRESO EL IDCKECKLIST EN LA URL");
        }
        return null;
    }

    /**
     * *************************************************************** REPORTE
     * MEDICION ****************************************************************
     */
    //http://localhost:8080/checklist/reporteService/reporteMedicion.json?idProtocolo=54&fechaInicio=01/04/2017&fechaFin=07/04/2017
    //http://10.51.218.144:8080/checklist/reporteService/reporteMedicion.json?idProtocolo=21&fechaInicio=01/10/2019&fechaFin=31/10/2019
    //http://10.53.33.83/checklist/reporteService/reporteMedicion.json?idProtocolo=20&fechaInicio=21/01/2020&fechaFin=21/01/2020
    //http://10.51.218.72:8080/checklist/reporteService/reporteMedicion.json?idProtocolo=21&fechaInicio=01/10/2019&fechaFin=31/10/2019
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/reporteMedicion", method = RequestMethod.GET)
    public @ResponseBody
    Boolean reporteMedicion(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> mapReporteGeneral = null;
        List<ReporteMedPregDTO> listaPreguntas = null;
        List<ReporteMedicionDTO> listaRespuestas = null;
        List<ReporteMedicionDTO> listaRespuestasFinal = new ArrayList<ReporteMedicionDTO>();
        HashMap<String, List<ReporteMedicionDTO>> hashmap = new HashMap<>();
        int idProtocolo = 0;

        if (request.getQueryString() != null) {
            String uri = request.getQueryString();
            if (uri.contains("idProtocolo")) {
                idProtocolo = Integer.parseInt(request.getParameter("idProtocolo"));
                String fechaInicio = null;
                String fechaFin = null;

                if (uri.contains("fechaInicio")) {
                    fechaInicio = request.getParameter("fechaInicio");
                }

                if (uri.contains("fechaFin")) {
                    fechaFin = request.getParameter("fechaFin");
                }

                try {
                    mapReporteGeneral = reporteMedicionBI.ReporteMedicion(idProtocolo, fechaInicio, fechaFin);
                    if (mapReporteGeneral != null) {
                        Iterator<Map.Entry<String, Object>> enMap = mapReporteGeneral.entrySet().iterator();
                        while (enMap.hasNext()) {
                            Map.Entry<String, Object> enMaps = enMap.next();
                            String nomH = enMaps.getKey();
                            if (nomH == "listaPregunta") {
                                listaPreguntas = (List<ReporteMedPregDTO>) enMaps.getValue();
                            }
                            if (nomH == "listaRespuesta") {
                                listaRespuestas = (List<ReporteMedicionDTO>) enMaps.getValue();
                            }
                        }

                        ServletOutputStream sos = null;
                        String archivo = "";
                        String columnaTitle = "";

                        if (listaPreguntas != null) {
                            for (ReporteMedPregDTO preguntas : listaPreguntas) {
                                columnaTitle += "<th align='center'>" + preguntas.getDescPregunta() + "</th>";
                            }
                        }

                        if (listaRespuestas != null && listaPreguntas != null) {
                            if (listaRespuestas.size() > 0) {

                                ArrayList<String> cecos = new ArrayList<>();
                                String ceco = "";
                                for (ReporteMedicionDTO aux : listaRespuestas) {
                                    ceco = aux.getIdCeco();
                                    if (hashmap.size() > 0) {
                                        if (hashmap.containsKey(ceco)) {
                                            hashmap.get(ceco).add(aux);
                                        } else {
                                            ArrayList<ReporteMedicionDTO> tmp = new ArrayList<>();
                                            tmp.add(aux);
                                            hashmap.put(aux.getIdCeco(), tmp);
                                            cecos.add(aux.getIdCeco());
                                        }
                                    } else {

                                        ArrayList<ReporteMedicionDTO> tmp = new ArrayList<>();
                                        tmp.add(aux);
                                        hashmap.put(aux.getIdCeco(), tmp);
                                        cecos.add(aux.getIdCeco());

                                    }
                                }
                                //	logger.info("CECOS: "+cecos);

                                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                        + "<thead>"
                                        + "<tr>"
                                        + "<th align='center'>Fecha que se Realizo</th>"
                                        + "<th align='center'>Quien lo realizo</th>"
                                        + "<th align='center'>Número de Usuario</th>"
                                        + "<th align='center'>Puesto</th>"
                                        + "<th align='center'>País</th>"
                                        + "<th align='center'>Territorio</th>"
                                        + "<th align='center'>Zona</th>"
                                        + "<th align='center'>Regional</th>"
                                        + "<th align='center'>Sucursal</th>"
                                        + "<th align='center'>Nombre Sucursal</th>"
                                        + "<th align='center'>Canal</th>"
                                        + "<th align='center'>Ponderacion</th>"
                                        + "<th align='center'>Calificación</th>"
                                        + "<th align='center'>Conteo de SI</th>"
                                        + "<th align='center'>Conteo de NO</th>"
                                        + "<th align='center'>Imperdonables</th>"
                                        + columnaTitle
                                        + "</tr>"
                                        + "</thead>";
                                archivo += "<tbody>";

                                for (String dto : cecos) {
                                    int contSI = 0;
                                    int contNO = 0;
                                    int contCritica = 0;
                                    double Calif = 0.0;

                                    List<ReporteMedicionDTO> reporte = hashmap.get(dto);
                                    ReporteMedicionDTO[] repLimpio = new ReporteMedicionDTO[listaPreguntas.size()];
                                    int indc = 0;
                                    for (ReporteMedPregDTO preg : listaPreguntas) {
                                        for (ReporteMedicionDTO aux : reporte) {
                                            if (preg.getIdPregunta() == aux.getIdPregunta()) {
                                                repLimpio[indc] = aux;
                                                break;
                                            }

                                        }

                                        if (repLimpio[indc] == null) {
                                            ReporteMedicionDTO varAux = new ReporteMedicionDTO();
                                            /*if(preg.getIdCritica()==1){
													varAux.setDescRespuesta("FALSO");
												}else{
													varAux.setDescRespuesta("NO");
												}*/
                                            varAux.setDescRespuesta("");
                                            repLimpio[indc] = varAux;
                                        }

                                        indc++;
                                    }

                                    boolean flag = false;
                                    for (ReporteMedicionDTO repDTO : repLimpio) {
                                        if (repDTO != null) {
                                            if (repDTO.getDescRespuesta().trim().contains("SI")) {
                                                contSI++;
                                                Calif = Calif + repDTO.getIdPonderacion();
                                                //logger.info("SI: "+contSI);
                                            } else if (repDTO.getDescRespuesta().trim().contains("NO")) {
                                                contNO++;
                                                //logger.info("NO: "+contNO);
                                            }
                                            //logger.info("CRI: "+repDTO.getDescRespuesta().toLowerCase().trim());
                                            if (repDTO.getDescRespuesta().toLowerCase().trim().contains("verdadero")) {
                                                contCritica++;
                                                //logger.info("CRITICA: "+contCritica);
                                            }

                                            if (!flag && repDTO.getIdCeco() != null) {
                                                archivo += "<tr>"
                                                        + "<td align='center'>" + repDTO.getFecha() + "</td>"
                                                        + "<td align='center'>" + repDTO.getNomUsuario() + "</td>"
                                                        + "<td align='center'>" + repDTO.getIdUsuario() + "</td>"
                                                        + "<td align='center'>" + repDTO.getNomPuesto() + "</td>"
                                                        + "<td align='center'>" + repDTO.getNomPais() + "</td>"
                                                        + "<td align='center'>" + repDTO.getTerritorio() + "</td>"
                                                        + "<td align='center'>" + repDTO.getZona() + "</td>"
                                                        + "<td align='center'>" + repDTO.getRegion() + "</td>"
                                                        + "<td align='center'>" + repDTO.getSucursal() + "</td>"
                                                        + "<td align='center'>" + repDTO.getNomCeco() + "</td>"
                                                        + "<td align='center'>" + repDTO.getNomCanal() + "</td>";
                                                flag = true;
                                            }
                                        }

                                    }
                                    double calificacion = 0.0;
                                    if (contCritica == 0) {
                                        calificacion = Calif;
                                    }
                                    archivo
                                            += "<td align='center'>" + Calif + "</td>"
                                            + "<td align='center'>" + calificacion + "</td>"
                                            + "<td align='center'>" + contSI + "</td>"
                                            + "<td align='center'>" + contNO + "</td>"
                                            + "<td align='center'>" + contCritica + "</td>";

                                    for (ReporteMedicionDTO repDTO : repLimpio) {
                                        if (repDTO != null) {
                                            archivo += "<td align='center'>" + repDTO.getDescRespuesta() + "</td>";
                                        } else {
                                            archivo += "<td align='center'></td>";
                                        }
                                    }

                                    archivo += "</tr>";

                                }

                                int inc = 0;
                                int auxInc = 0;

                            } else {
                                logger.info("NO EXISTEN RESPUESTAS");

                                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                        + "<thead>"
                                        + "<tr>"
                                        + "<th align='center'>Fecha que se Realizo</th>"
                                        + "<th align='center'>Quien lo realizo</th>"
                                        + "<th align='center'>Número de Usuario</th>"
                                        + "<th align='center'>Puesto</th>"
                                        + "<th align='center'>País</th>"
                                        + "<th align='center'>Territorio</th>"
                                        + "<th align='center'>Zona</th>"
                                        + "<th align='center'>Regional</th>"
                                        + "<th align='center'>Sucursal</th>"
                                        + "<th align='center'>Nombre Sucursal</th>"
                                        + "<th align='center'>Canal</th>"
                                        + "<th align='center'>Ponderacion</th>"
                                        + "<th align='center'>Calificación</th>"
                                        + "<th align='center'>Conteo de SI</th>"
                                        + "<th align='center'>Conteo de NO</th>"
                                        + "<th align='center'>Imperdonables</th>"
                                        + columnaTitle
                                        + "</tr>"
                                        + "</thead>";
                                archivo += "<tbody>";

                                archivo += "<tr>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>";

                                for (int j = 0; j < listaPreguntas.size(); j++) {
                                    archivo += "<td align='center'>Sin Respuesta</td>";
                                }

                                archivo += "</tr>";
                            }
                        } else {
                            logger.info("NO EXISTEN RESPUESTAS");

                            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                    + "<thead>"
                                    + "<tr>"
                                    + "<th align='center'>Fecha que se Realizo</th>"
                                    + "<th align='center'>Quien lo realizo</th>"
                                    + "<th align='center'>Número de Usuario</th>"
                                    + "<th align='center'>Puesto</th>"
                                    + "<th align='center'>País</th>"
                                    + "<th align='center'>Territorio</th>"
                                    + "<th align='center'>Zona</th>"
                                    + "<th align='center'>Regional</th>"
                                    + "<th align='center'>Sucursal</th>"
                                    + "<th align='center'>Nombre Sucursal</th>"
                                    + "<th align='center'>Canal</th>"
                                    + "<th align='center'>Ponderacion</th>"
                                    + "<th align='center'>Calificación</th>"
                                    + "<th align='center'>Conteo de SI</th>"
                                    + "<th align='center'>Conteo de NO</th>"
                                    + "<th align='center'>Imperdonables</th>"
                                    + columnaTitle
                                    + "</tr>"
                                    + "</thead>";
                            archivo += "<tbody>";

                            archivo += "<tr>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>";

                            for (int j = 0; j < listaPreguntas.size(); j++) {
                                archivo += "<td align='center'>Sin Respuesta</td>";
                            }
                            archivo += "<td align='center'>Sin Respuesta</td>";

                            archivo += "</tr>";
                        }

                        archivo += "</tbody><table>";
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Date date = new Date();
                        String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
                        response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
                        response.setHeader("Content-Disposition", "attachment; filename=reporteGeneralZonas" + fecha + ".xls");
                        response.setHeader("Pragma: no-cache", "Expires: 0");
                        sos = response.getOutputStream();
                        sos.write(archivo.getBytes());
                        try {
                            sos.flush();
                        } finally {
                            sos.close();
                        }
                    } else { //CIERRA if(mapReporteGeneral!=null)
                        logger.info("LA CONSULTA DEL PROTOCOLO ESTA VACIA");
                    }
                } catch (Exception e) {
                    logger.info(e);
                    e.printStackTrace();
                }
            } else {
                logger.info("NO SE INGRESO EL IDPROTOCOLO EN LA URL");
            }

        } else {
            logger.info("NO SE INGRESO EL IDPROTOCOLO EN LA URL");
        }
        return null;
    }

    //http://localhost:8080/checklist/reporteService/reporteMedProtocolo.json?idChecklist=26&fechaInicio=01/10/2019&fechaFin=30/10/2019
    //http://10.51.218.144:8080/checklist/reporteService/reporteMedProtocolo.json?idChecklist=26&fechaInicio=01/10/2019&fechaFin=30/10/2019
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/reporteMedProtocolo", method = RequestMethod.GET)
    public @ResponseBody
    Boolean reporteMedProtocolo(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> mapReporteGeneral = null;
        List<ReporteMedPregDTO> listaPreguntas = null;
        List<ReporteMedicionDTO> listaRespuestas = null;
        List<ReporteMedicionDTO> listaRespuestasFinal = new ArrayList<ReporteMedicionDTO>();
        HashMap<String, List<ReporteMedicionDTO>> hashmap = new HashMap<>();
        int idChecklist = 0;

        if (request.getQueryString() != null) {
            String uri = request.getQueryString();
            if (uri.contains("idChecklist")) {
                idChecklist = Integer.parseInt(request.getParameter("idChecklist"));
                String fechaInicio = null;
                String fechaFin = null;

                if (uri.contains("fechaInicio")) {
                    fechaInicio = request.getParameter("fechaInicio");
                }

                if (uri.contains("fechaFin")) {
                    fechaFin = request.getParameter("fechaFin");
                }

                try {
                    mapReporteGeneral = reporteMedicionBI.ReporteProtMedicion(idChecklist, fechaInicio, fechaFin);
                    if (mapReporteGeneral != null) {
                        Iterator<Map.Entry<String, Object>> enMap = mapReporteGeneral.entrySet().iterator();
                        while (enMap.hasNext()) {
                            Map.Entry<String, Object> enMaps = enMap.next();
                            String nomH = enMaps.getKey();
                            if (nomH == "listaPregunta") {
                                listaPreguntas = (List<ReporteMedPregDTO>) enMaps.getValue();
                            }
                            if (nomH == "listaRespuesta") {
                                listaRespuestas = (List<ReporteMedicionDTO>) enMaps.getValue();
                            }
                        }

                        ServletOutputStream sos = null;
                        String archivo = "";
                        String columnaTitle = "";

                        if (listaPreguntas != null) {
                            for (ReporteMedPregDTO preguntas : listaPreguntas) {
                                columnaTitle += "<th align='center'>" + preguntas.getDescPregunta() + "</th>";
                            }
                        }

                        if (listaRespuestas != null && listaPreguntas != null) {
                            if (listaRespuestas.size() > 0) {

                                ArrayList<String> cecos = new ArrayList<>();
                                String ceco = "";
                                for (ReporteMedicionDTO aux : listaRespuestas) {
                                    ceco = aux.getIdCeco();
                                    if (hashmap.size() > 0) {
                                        if (hashmap.containsKey(ceco)) {
                                            hashmap.get(ceco).add(aux);
                                        } else {
                                            ArrayList<ReporteMedicionDTO> tmp = new ArrayList<>();
                                            tmp.add(aux);
                                            hashmap.put(aux.getIdCeco(), tmp);
                                            cecos.add(aux.getIdCeco());
                                        }
                                    } else {

                                        ArrayList<ReporteMedicionDTO> tmp = new ArrayList<>();
                                        tmp.add(aux);
                                        hashmap.put(aux.getIdCeco(), tmp);
                                        cecos.add(aux.getIdCeco());

                                    }
                                }
                                logger.info("CECOS: " + cecos);

                                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                        + "<thead>"
                                        + "<tr>"
                                        + "<th align='center'>Fecha que se Realizo</th>"
                                        + "<th align='center'>Quien lo realizo</th>"
                                        + "<th align='center'>Número de Usuario</th>"
                                        + "<th align='center'>Puesto</th>"
                                        + "<th align='center'>País</th>"
                                        + "<th align='center'>Territorio</th>"
                                        + "<th align='center'>Zona</th>"
                                        + "<th align='center'>Regional</th>"
                                        + "<th align='center'>Sucursal</th>"
                                        + "<th align='center'>Nombre Sucursal</th>"
                                        + "<th align='center'>Canal</th>"
                                        + "<th align='center'>Ponderacion</th>"
                                        + "<th align='center'>Calificación</th>"
                                        + "<th align='center'>Conteo de SI</th>"
                                        + "<th align='center'>Conteo de NO</th>"
                                        + "<th align='center'>Imperdonables</th>"
                                        + columnaTitle
                                        + "</tr>"
                                        + "</thead>";
                                archivo += "<tbody>";

                                for (String dto : cecos) {
                                    int contSI = 0;
                                    int contNO = 0;
                                    int contCritica = 0;
                                    double Calif = 0.0;

                                    List<ReporteMedicionDTO> reporte = hashmap.get(dto);

                                    ReporteMedicionDTO[] repLimpio = new ReporteMedicionDTO[listaPreguntas.size()];
                                    int indc = 0;
                                    for (ReporteMedPregDTO preg : listaPreguntas) {

                                        for (int i = reporte.size() - 1; i >= 0; i--) {
                                            if (preg.getIdPregunta() == reporte.get(i).getIdPregunta()) {
                                                repLimpio[indc] = reporte.get(i);
                                                break;
                                            }
                                        }

                                        if (repLimpio[indc] == null) {
                                            ReporteMedicionDTO varAux = new ReporteMedicionDTO();
                                            /*if(preg.getIdCritica()==1){
													varAux.setDescRespuesta("FALSO");
												}else{
													varAux.setDescRespuesta("");
												}*/
                                            varAux.setDescRespuesta("");
                                            repLimpio[indc] = varAux;
                                        }

                                        indc++;
                                    }

                                    boolean flag = false;
                                    for (ReporteMedicionDTO repDTO : repLimpio) {
                                        if (repDTO != null) {
                                            if (repDTO.getDescRespuesta().trim().contains("SI")) {
                                                contSI++;
                                                Calif = Calif + repDTO.getIdPonderacion();
                                                //logger.info("SI: "+contSI);
                                            } else if (repDTO.getDescRespuesta().trim().contains("NO")) {
                                                contNO++;
                                                //logger.info("NO: "+contNO);
                                            }

                                            //logger.info("CRI: "+repDTO.getDescRespuesta().toLowerCase().trim());
                                            if (repDTO.getDescRespuesta().toLowerCase().trim().contains("verdadero")) {
                                                contCritica++;
                                                //logger.info("CRITICA: "+contCritica);
                                            }

                                        }

                                    }

                                    archivo += "<tr>"
                                            + "<td align='center'>" + reporte.get(reporte.size() - 1).getFecha() + "</td>"
                                            + "<td align='center'>" + reporte.get(reporte.size() - 1).getNomUsuario() + "</td>"
                                            + "<td align='center'>" + reporte.get(reporte.size() - 1).getIdUsuario() + "</td>"
                                            + "<td align='center'>" + reporte.get(reporte.size() - 1).getNomPuesto() + "</td>"
                                            + "<td align='center'>" + reporte.get(reporte.size() - 1).getNomPais() + "</td>"
                                            + "<td align='center'>" + reporte.get(reporte.size() - 1).getTerritorio() + "</td>"
                                            + "<td align='center'>" + reporte.get(reporte.size() - 1).getZona() + "</td>"
                                            + "<td align='center'>" + reporte.get(reporte.size() - 1).getRegion() + "</td>"
                                            + "<td align='center'>" + reporte.get(reporte.size() - 1).getSucursal() + "</td>"
                                            + "<td align='center'>" + reporte.get(reporte.size() - 1).getNomCeco() + "</td>"
                                            + "<td align='center'>" + reporte.get(reporte.size() - 1).getNomCanal() + "</td>";

                                    double calificacion = 0.0;
                                    if (contCritica == 0) {
                                        calificacion = Calif;
                                    }
                                    archivo
                                            += "<td align='center'>" + Calif + "</td>"
                                            + "<td align='center'>" + calificacion + "</td>"
                                            + "<td align='center'>" + contSI + "</td>"
                                            + "<td align='center'>" + contNO + "</td>"
                                            + "<td align='center'>" + contCritica + "</td>";

                                    for (ReporteMedicionDTO repDTO : repLimpio) {
                                        if (repDTO != null) {
                                            archivo += "<td align='center'>" + repDTO.getDescRespuesta() + "</td>";
                                        } else {
                                            archivo += "<td align='center'></td>";
                                        }
                                    }

                                    archivo += "</tr>";

                                }

                                int inc = 0;
                                int auxInc = 0;

                            } else {
                                logger.info("NO EXISTEN RESPUESTAS");

                                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                        + "<thead>"
                                        + "<tr>"
                                        + "<th align='center'>Fecha que se Realizo</th>"
                                        + "<th align='center'>Quien lo realizo</th>"
                                        + "<th align='center'>Número de Usuario</th>"
                                        + "<th align='center'>Puesto</th>"
                                        + "<th align='center'>País</th>"
                                        + "<th align='center'>Territorio</th>"
                                        + "<th align='center'>Zona</th>"
                                        + "<th align='center'>Regional</th>"
                                        + "<th align='center'>Sucursal</th>"
                                        + "<th align='center'>Nombre Sucursal</th>"
                                        + "<th align='center'>Canal</th>"
                                        + "<th align='center'>Ponderacion</th>"
                                        + "<th align='center'>Calificación</th>"
                                        + "<th align='center'>Conteo de SI</th>"
                                        + "<th align='center'>Conteo de NO</th>"
                                        + "<th align='center'>Imperdonables</th>"
                                        + columnaTitle
                                        + "</tr>"
                                        + "</thead>";
                                archivo += "<tbody>";

                                archivo += "<tr>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>";

                                for (int j = 0; j < listaPreguntas.size(); j++) {
                                    archivo += "<td align='center'>Sin Respuesta</td>";
                                }

                                archivo += "</tr>";
                            }
                        } else {
                            logger.info("NO EXISTEN RESPUESTAS");

                            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                    + "<thead>"
                                    + "<tr>"
                                    + "<th align='center'>Fecha que se Realizo</th>"
                                    + "<th align='center'>Quien lo realizo</th>"
                                    + "<th align='center'>Número de Usuario</th>"
                                    + "<th align='center'>Puesto</th>"
                                    + "<th align='center'>País</th>"
                                    + "<th align='center'>Territorio</th>"
                                    + "<th align='center'>Zona</th>"
                                    + "<th align='center'>Regional</th>"
                                    + "<th align='center'>Sucursal</th>"
                                    + "<th align='center'>Nombre Sucursal</th>"
                                    + "<th align='center'>Canal</th>"
                                    + "<th align='center'>Ponderacion</th>"
                                    + "<th align='center'>Calificación</th>"
                                    + "<th align='center'>Conteo de SI</th>"
                                    + "<th align='center'>Conteo de NO</th>"
                                    + "<th align='center'>Imperdonables</th>"
                                    + columnaTitle
                                    + "</tr>"
                                    + "</thead>";
                            archivo += "<tbody>";

                            archivo += "<tr>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>";

                            for (int j = 0; j < listaPreguntas.size(); j++) {
                                archivo += "<td align='center'>Sin Respuesta</td>";
                            }
                            archivo += "<td align='center'>Sin Respuesta</td>";

                            archivo += "</tr>";
                        }

                        archivo += "</tbody><table>";
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Date date = new Date();
                        String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
                        response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
                        response.setHeader("Content-Disposition", "attachment; filename=reporteGeneralProtocolo" + fecha + ".xls");
                        response.setHeader("Pragma: no-cache", "Expires: 0");
                        sos = response.getOutputStream();
                        sos.write(archivo.getBytes());
                    } else { //CIERRA if(mapReporteGeneral!=null)
                        logger.info("LA CONSULTA DEL PROTOCOLO ESTA VACIA");
                    }
                } catch (Exception e) {
                    logger.info(e);
                    e.printStackTrace();
                }
            } else {
                logger.info("NO SE INGRESO EL IDPROTOCOLO EN LA URL");
            }

        } else {
            logger.info("NO SE INGRESO EL IDPROTOCOLO EN LA URL");
        }
        return null;
    }

    //http://10.51.218.144:8080/checklist/reporteService/downloadFile.json
    //http://10.51.218.144:8080/checklist/reporteService/postGenerateZIP.json?json={"nomArchivoRetorno":"480100","archivos":[{"nomArchivo":"/franquicia/Handbook/Manual 2.0 Banco y Elektra 2018.pdf"},{"nomArchivo":"/franquicia/Handbook/Manual_EKT-DAZ-3.0_EKT_DAZ-240619_SB.pdf"}]}
    @RequestMapping(value = "/postGenerateZIP", method = RequestMethod.POST)
    public ResponseEntity<?> postGenerateZIP(HttpServletRequest request, HttpServletResponse response) throws Exception {
        BinaryOutputWrapper output = new BinaryOutputWrapper();
        FileUtil fileUtil = new FileUtil();
        try {
            //String inputFile = "/franquicia/Handbook/Manual 2.0 Banco y Elektra 2018.pdf";
            //output = fileUtil.prepDownloadAsPDF(inputFile);

            //---------------Recibe por get
            String json = "";
            String uri = request.getQueryString();
            //if(uri.contains("contenido"))
            json = request.getParameter("contenido");

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");

            String nomarchivo = "_" + fecha + ".zip";
            List<String> archivos = new ArrayList<String>();
            if (json != null) {
                JSONObject rec = new JSONObject(json);

                JSONArray ja = rec.getJSONArray("archivos");
                nomarchivo = rec.getString("nomArchivoRetorno") + nomarchivo;
                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jo = ja.getJSONObject(i);
                    archivos.add(jo.getString("nomArchivo"));
                }

            }

            output = fileUtil.prepDownloadAsZIP(archivos, nomarchivo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(output.getData(), output.getHeaders(), HttpStatus.OK);
    }

    //http://10.51.218.144:8080/checklist/reporteService/downloadFile.json
    //http://10.51.218.144:8080/checklist/reporteService/getGenerateZIP.json?json={"nomArchivoRetorno":"480100","archivos":[{"nomArchivo":"/franquicia/Handbook/Manual 2.0 Banco y Elektra 2018.pdf"},{"nomArchivo":"/franquicia/Handbook/Manual_EKT-DAZ-3.0_EKT_DAZ-240619_SB.pdf"}]}
    @RequestMapping(value = "/getGenerateZIP", method = RequestMethod.GET)
    public ResponseEntity<?> getGenerateZIP(HttpServletRequest request, HttpServletResponse response) throws Exception {
        BinaryOutputWrapper output = new BinaryOutputWrapper();
        FileUtil fileUtil = new FileUtil();
        try {
            //String inputFile = "/franquicia/Handbook/Manual 2.0 Banco y Elektra 2018.pdf";
            //output = fileUtil.prepDownloadAsPDF(inputFile);

            //---------------Recibe por get
            String json = "";
            String uri = request.getQueryString();
            if (uri.contains("contenido")) {
                json = request.getParameter("contenido");
            }

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");

            String nomarchivo = "_" + fecha + ".zip";
            List<String> archivos = new ArrayList<String>();
            if (json != null) {
                JSONObject rec = new JSONObject(json);

                JSONArray ja = rec.getJSONArray("archivos");
                nomarchivo = rec.getString("nomArchivoRetorno") + nomarchivo;
                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jo = ja.getJSONObject(i);
                    archivos.add(jo.getString("nomArchivo"));
                }

            }

            output = fileUtil.prepDownloadAsZIP(archivos, nomarchivo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(output.getData(), output.getHeaders(), HttpStatus.OK);
    }

    // http://10.51.218.72:8080/checklist/reporteService/protocolosMedicion.json?idChecklist=21&fechaInicio=01/11/2019&fechaFin=30/11/2019
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/protocolosMedicion", method = RequestMethod.GET)
    public @ResponseBody
    Boolean protocolosMedicion(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        List<RepMedicionDTO> listaPreguntas = null;
        List<RepMedicionDTO> listaDatos = null;
        List<String> listaRespuestasFinal = new ArrayList<String>();
        // int idChecklist = 62;
        int idProtocolo = 0;
        String idChecklist = request.getParameter("idChecklist");
        String fechaInicio = request.getParameter("fechaInicio");
        String fechaFin = request.getParameter("fechaFin");
        List<ProtocoloByCheckDTO> respuesta = protocoloByCheckBI.obtieneProtocolo(Integer.parseInt(idChecklist));
        for (ProtocoloByCheckDTO auxi : respuesta) {
            idProtocolo = auxi.getIdProtocolo();
        }
        try {
            listaDatos = repMedicionBI.ReporteMedZonas(idProtocolo, fechaInicio, fechaFin);
            logger.info("RESPUESTA: " + listaDatos);
            RepMedicionDTO r = new RepMedicionDTO();
            r.setIdProtocolo(idProtocolo);
            listaPreguntas = repMedicionBI.ReportePreguntas(r);
            logger.info("PREGUNTAS: " + listaPreguntas);

            String[] arr = new String[listaPreguntas.size()];
            ServletOutputStream sos = null;
            String archivo = "";
            String columnaTitle = "";

            /*for (RepMedicionDTO reporte : listaDatos) {
				arr = new String[listaPreguntas.size()];
				arr = reporte.getDescRespuesta().split("#####");
				listaRespuestasFinal.add(Arrays.toString(arr));
			}*/
            //Charly
            List<String[]> res = new ArrayList<String[]>();
            if (listaDatos != null) {
                for (RepMedicionDTO reporte : listaDatos) {
                    arr = new String[listaPreguntas.size()];

                    for (int i = 0; i < arr.length; i++) {
                        arr[i] = "";
                    }

                    int apuntador = 0;
                    for (RepMedicionDTO aux : listaPreguntas) {
                        String[] arrTemp = reporte.getDescRespuesta().split("#####");

                        for (int i = 0; i < arrTemp.length; i++) {
                            String[] resp = arrTemp[i].split("====");
                            int idPreg = Integer.parseInt(resp[0]);
                            if (idPreg == aux.getIdPregunta()) {
                                arr[apuntador] = resp[1];

                            }
                        }
                        apuntador++;
                    }
                    res.add(arr);
                    listaRespuestasFinal.add(Arrays.toString(arr));
                }
            }
            if (listaPreguntas != null) {
                for (RepMedicionDTO preguntas : listaPreguntas) {
                    columnaTitle += "<th align='center'>" + preguntas.getPregunta() + "</th>";
                }
            }
            if (listaDatos != null && listaPreguntas != null) {
                if (listaDatos.size() > 0) {
                    archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                            + "<thead>" + "<tr>"
                            + "<th align='center'>Fecha que se Realizo</th>"
                            + "<th align='center'>Quien lo realizo</th>"
                            + "<th align='center'>Número de Usuario</th>"
                            + "<th align='center'>Puesto</th>"
                            + "<th align='center'>País</th>"
                            + "<th align='center'>Territorio</th>"
                            + "<th align='center'>Zona</th>"
                            + "<th align='center'>Regional</th>"
                            + "<th align='center'>Sucursal</th>"
                            + "<th align='center'>Nombre Sucursal</th>"
                            + "<th align='center'>Canal</th>"
                            + "<th align='center'>Ponderación</th>"
                            + "<th align='center'>Calificación</th>"
                            + "<th align='center'>Conteo de SI</th>"
                            + "<th align='center'>Conteo de NO</th>"
                            + "<th align='center'>Imperdonables</th>"
                            + columnaTitle + "</tr>" + "</thead>";
                    archivo += "<tbody>";

                    int contador = 0;
                    for (int i = 0; i < listaDatos.size(); i++) {
                        //logger.info("SIZE: "+listaDatos.size());
                        archivo += "<tr>";
                        archivo += "<td align='center'>" + listaDatos.get(i).getFecha() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getEmpleado() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getIdEmpleado() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getPuesto() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getPais() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getTerritorio() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getZona() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getRegional() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getSucursal() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getNomSucursal() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getCanal() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getPonderacion() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getCalificacion() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getContSi() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getContNo() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getContImp() + "</td>";

                        int cont = 0;
                        logger.info("LIST: " + listaRespuestasFinal);
                        for (int j = 0; j < listaPreguntas.size(); j++) {

                            archivo += "<td align='center'>" + res.get(contador)[j] + "</td>";
                            /*
							if (j == contador) {
								archivo += "<td align='center'>" + listaRespuestasFinal.get(contador) + "</td>";

							}else{
								archivo += "<td align='center'>" + "S/D" + "</td>";
							}
                             */

 /*for(int k=0; k<listaPreguntas.size(); k++){
								if (cont == contador) {
									archivo += "<td align='center'>" + listaRespuestasFinal.get(j) + "</td>";
								}else{
									archivo += "<td align='center'>" + "S/D" + "</td>";
								}
							}*/
                            cont++;
                        }
                        /*for (String repo : listaRespuestasFinal) {
							for (String respSplit : listaRespuestasFinal) {
								if (cont == contador) {
									archivo += "<td align='center'>" + respSplit + "</td>";
								}else{
									archivo += "<td align='center'></td>";
								}

							}//Cierra 3er for
							cont++;
						}//Cierra 2do for*/
                        archivo += "</tr>";
                        contador++;
                    }//Cierra 1er for
                } else {
                    logger.info("NO EXISTEN RESPUESTAS");

                    archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                            + "<thead>" + "<tr>" + "<th align='center'>Fecha que se Realizo</th>"
                            + "<th align='center'>Quien lo realizo</th>" + "<th align='center'>Numero de Usuario</th>"
                            + "<th align='center'>Puesto</th>" + "<th align='center'>Pais</th>"
                            + "<th align='center'>Territorio</th>" + "<th align='center'>Zona</th>"
                            + "<th align='center'>Regional</th>" + "<th align='center'>Sucursal</th>"
                            + "<th align='center'>Nombre Sucursal</th>" + "<th align='center'>Canal</th>"
                            + "<th align='center'>Ponderacion</th>" + "<th align='center'>Calificacion</th>"
                            + "<th align='center'>Conteo de SI</th>" + "<th align='center'>Conteo de NO</th>"
                            + "<th align='center'>Imperdonables</th>" + columnaTitle + "</tr>" + "</thead>";
                    archivo += "<tbody>";

                    archivo += "<tr>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>";

                    for (int j = 0; j < listaPreguntas.size(); j++) {
                        archivo += "<td align='center'>Sin Respuesta</td>";
                    }

                    archivo += "</tr>";
                }
            } else {
                logger.info("NO EXISTEN RESPUESTAS");

                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                        + "<thead>" + "<tr>" + "<th align='center'>Fecha que se Realizo</th>"
                        + "<th align='center'>Quien lo realizo</th>" + "<th align='center'>Numero de Usuario</th>"
                        + "<th align='center'>Puesto</th>" + "<th align='center'>Pais</th>"
                        + "<th align='center'>Territorio</th>" + "<th align='center'>Zona</th>"
                        + "<th align='center'>Regional</th>" + "<th align='center'>Sucursal</th>"
                        + "<th align='center'>Nombre Sucursal</th>" + "<th align='center'>Canal</th>"
                        + "<th align='center'>Ponderacion</th>" + "<th align='center'>Calificacion</th>"
                        + "<th align='center'>Conteo de SI</th>" + "<th align='center'>Conteo de NO</th>"
                        + "<th align='center'>Imperdonables</th>" + columnaTitle + "</tr>" + "</thead>";
                archivo += "<tbody>";

                archivo += "<tr>" + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                        + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                        + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                        + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                        + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                        + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                        + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                        + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>";

                for (int j = 0; j < listaPreguntas.size(); j++) {
                    archivo += "<td align='center'>Sin Respuesta</td>";
                }
                archivo += "<td align='center'>Sin Respuesta</td>";

                archivo += "</tr>";
            }

            archivo += "</tbody><table>";
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition", "attachment; filename=reporteGeneralProtocolos" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();
            sos.write(archivo.getBytes());
            sos.flush();
            sos.close();

        } catch (Exception e) {
            e.getStackTrace();
        }
        return null;
    }

}
