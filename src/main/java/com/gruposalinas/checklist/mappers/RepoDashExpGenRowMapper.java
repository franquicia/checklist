package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.RepoFilExpDTO;

public class RepoDashExpGenRowMapper implements RowMapper<RepoFilExpDTO> {

	public RepoFilExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		RepoFilExpDTO com = new RepoFilExpDTO();

	    com.setCeco(rs.getString("FCID_CECO"));
	    com.setIdUsu(rs.getInt("FIID_USUARIO"));
	    com.setBitGral(rs.getInt("FIID_BITGRAL"));
	    com.setIdRecorrido(rs.getString("FCRECORRIDO"));
	    com.setIdBita(rs.getInt("FIID_BITACORA"));
	    com.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    com.setObserv(rs.getString("FCOBSERVACIONES"));
	    com.setIdArbol(rs.getInt("FIID_ARBOL_DES"));
	    com.setIdPosible(rs.getInt("FCRESPUESTA"));
	    com.setPosible(rs.getString("POSIBLE"));
	    com.setIdCheck(rs.getInt("FIID_CHECKLIST"));
	    com.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    com.setCalCheck(rs.getDouble("FIPODENRACION"));
	    com.setPregunta(rs.getString("PREGUNTA"));  
	    com.setIdcritica(rs.getInt("FICRITICA"));
	    com.setAux(rs.getString("SLA"));
	    com.setModulo(rs.getString("FIID_MODULO"));
	    com.setRuta(rs.getString("FCRUTA"));
	    com.setChecklist(rs.getString("NOMCHECKLIST"));
	    com.setNombreCeco(rs.getString("NOMCECO"));
	    com.setClasif(rs.getString("CLASIFICA"));
	    com.setFechaTermino(rs.getString("FECHATERMINO"));
	  
	   
		return com;
	}

}
