package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.VersionProtoCheckDTO;

public class VersioneProtoCheckRowMapper implements RowMapper<VersionProtoCheckDTO> {

	public VersionProtoCheckDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		VersionProtoCheckDTO VersionDTO = new VersionProtoCheckDTO();
		
		VersionDTO.setIdProtocolo(""+rs.getInt("FIID_PROTOCOLO"));
		VersionDTO.setIdChecklist(""+rs.getInt("FIID_CHECKLIST"));
		VersionDTO.setFecha(rs.getString("FDFECHAINICIO"));
		VersionDTO.setVigente(""+rs.getInt("FDFECHAINICIO"));
		
	   
	 
		return VersionDTO;
	}

}


