	$(document).ready(function() {
		$("#numSucursal0").val("");
		$("#numSucursal0").focus();

		$("#coord1").val("");
		$("#coord1").focus();
	});

	function validateForm() {
		var coord1 = document.forms["form-2"]["coord1"].value;
		var coord2 = document.forms["form-confirma"]["coordenadas-2"].value;

		if (coord1 == "" || coord1 == null) {
			alert("El campo de las coordenadas no puede ir vacio!");
			return false;
		}
		else {
			return true;
		}

		if (coord2 == "" || coord2 == null) {
			alert("El campo de las coordenadas no puede ir vacio!");
			return false;
		}
		else {
			return true;
		}
	}

	function validateForm0() {
		var numSucursal = document.forms["form0"]["numSucursal0"].value;
		if (numSucursal == "" || numSucursal == null) {
			alert("El campo no puede ir vacío!");
			return false;
		}
		else {
			return true;
		}
	}
	
	function validateForm1() {
		var numSucursal = document.forms["form1"]["numSucursal-1"].value;

		if (numSucursal == "" || numSucursal == null) {
			alert("El campo no puede ir vacío!");
			return false;
		}
		else {
			return true;
		}
	}
	
	function validateForm2() {
		var numSucursal = document.forms["form2"]["idSucursal"].value;
		var coord1 = document.forms["form2"]["coord1"].value;
		
		if (numSucursal == "" || numSucursal == null, coord1 == "" || coord1 == null) {
			alert("El campo no puede ir vacío!");
			return false;
		}
		else {
			return true;
		}
	}
	
	function validateForm3() {
		var numSucursal = document.forms["form3"]["idSucursal3"].value;

		if (numSucursal == "" || numSucursal == null) {
			alert("El campo no puede ir vacío!");
			return false;
		}
		else {
			return true;
		}
	}

	function getConfirmation() {
		var retVal = confirm("¿Desea confirmar las coordenadas?");
		
		if ( retVal == true ) {
			return true;
		}
		else {
			return false;
		}
	}

	function getCancel() {
		var popup = confirm("¿Desea cancelar?");
			
		if (popup == true){
			return true;
		}
		else {
			return false;
		}
	}
	
	function fieldFormat(event) {
	    var e = event || window.event,
	        charCode = e.keyCode || e.which,
	        parts = e.target.value.split('.');
	    if (charCode > 31 && (charCode != 46 && (charCode < 48 || charCode > 57)) || (parts.length > 1 && charCode == 46))
	        return false;
	    return true;
	}
