package com.gruposalinas.checklist.domain;

public class PerfilesSupDTO {
	
	private int idPerfil;
	private String descPerfil;
	private String activo;
	private String usuaModificado;
	private String fechaModificacion;
	
	public int getIdPerfil() {
		return idPerfil;
	}
	public void setIdPerfil(int idPerfil) {
		this.idPerfil = idPerfil;
	}
	public String getDescPerfil() {
		return descPerfil;
	}
	public void setDescPerfil(String descPerfil) {
		this.descPerfil = descPerfil;
	}
	public String getActivo() {
		return activo;
	}
	public void setActivo(String activo) {
		this.activo = activo;
	}
	public String getUsuaModificado() {
		return usuaModificado;
	}
	public void setUsuaModificado(String usuaModificado) {
		this.usuaModificado = usuaModificado;
	}
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	@Override
	public String toString() {
		return "PerfilesSupDTO [idPerfil=" + idPerfil + ", descPerfil=" + descPerfil + ", activo=" + activo
				+ ", usuaModificado=" + usuaModificado + ", fechaModificacion=" + fechaModificacion + "]";
	}
	
}


