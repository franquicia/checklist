package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.Usuario_ADTO;

public class UsuarioSucursalRowMapper implements RowMapper<Usuario_ADTO> {
	
	public Usuario_ADTO mapRow(ResultSet rs,int rowNum) throws SQLException{
		Usuario_ADTO usuarioADTO = new Usuario_ADTO();
		
		usuarioADTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
		usuarioADTO.setNombre(rs.getString("FCNOMBRE"));
		
		return usuarioADTO;
	}

}
