package com.gruposalinas.checklist.dao;

import java.util.List;
import com.gruposalinas.checklist.domain.ConsultaSupervisionDTO;



public interface ConsultaSupervisionDAO {
	
	public List<ConsultaSupervisionDTO> obtieneSucursal(ConsultaSupervisionDTO bean) throws Exception;
	
	
}
