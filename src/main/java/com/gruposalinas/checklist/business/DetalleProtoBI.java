package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.DetalleProtoDAO;
import com.gruposalinas.checklist.domain.DetalleProtoDTO;

public class DetalleProtoBI {
		
	private static Logger logger = LogManager.getLogger(DetalleProtoBI.class);
	
	@Autowired
	DetalleProtoDAO protoDAO;
	
	List<DetalleProtoDTO> listaConsulta = null;

	public List<DetalleProtoDTO> ObtieneDetalleProto(DetalleProtoDTO bean){
	
		try {
			listaConsulta = protoDAO.ObtieneDetalleProto(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
			logger.info(""+e);
		}			
		
		return listaConsulta;
	
	}
	
	List<DetalleProtoDTO> listaConsulta2 = null;

	public List<DetalleProtoDTO> ObtieneNombreGrupo(DetalleProtoDTO bean){
		
		try {
			listaConsulta2 = protoDAO.ObtieneNombreGrupo(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
			logger.info(""+e);
		}			
		
		return listaConsulta2;
	
		}
	
	List<DetalleProtoDTO> listaConsulta3 = null;

	public List<DetalleProtoDTO> ObtieneNombreCeco(DetalleProtoDTO bean){
		
		try {
			listaConsulta3 = protoDAO.ObtieneNombreCeco(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
			logger.info(""+e);
		}			
		
		return listaConsulta3;
	
	}
	List<DetalleProtoDTO> listaConsulta4 = null;

	public List<DetalleProtoDTO> ObtieneBitacoraCerrada(DetalleProtoDTO bean){
		
		try {
			listaConsulta4 = protoDAO.ObtieneBitacoraCerrada(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
			logger.info(""+e);
		}			
		
		return listaConsulta4;
	
		}
	
	List<DetalleProtoDTO> listaConsulta5 = null;

	public List<DetalleProtoDTO> ObtieneControlAcceso(DetalleProtoDTO bean){
		
		try {
			listaConsulta5 = protoDAO.ObtieneControlAcceso(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
			logger.info(""+e);
		}			
		
		return listaConsulta5;

	}
}