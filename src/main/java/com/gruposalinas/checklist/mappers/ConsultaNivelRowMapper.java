package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ConsultaDistribucionDTO;

public class ConsultaNivelRowMapper implements RowMapper<ConsultaDistribucionDTO> {

	public ConsultaDistribucionDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConsultaDistribucionDTO a1 = new ConsultaDistribucionDTO();
		a1.setIdCeco(rs.getString("FCID_CECO"));
		a1.setNombreCeco(rs.getString("FCNOMBRE"));
	  
	return a1;
	}
	

		

}






