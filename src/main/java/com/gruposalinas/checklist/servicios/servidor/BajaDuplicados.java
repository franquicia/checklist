package com.gruposalinas.checklist.servicios.servidor;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.gruposalinas.checklist.business.DuplicadosBI;
import com.gruposalinas.checklist.domain.DuplicadosDTO;
import com.gruposalinas.checklist.resources.FRQAuthInterceptor;

@Controller
@RequestMapping("/checklistDuplicados")
public class BajaDuplicados {

	@Autowired
	DuplicadosBI duplicadosBI;
	private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);
	
	
	// http://localhost:8080/checklist/checklistDuplicados/eliminaRespuestasD.json
	@RequestMapping(value = "/eliminaRespuestasD", method = RequestMethod.GET)
	public ModelAndView eliminaRespuestasDuplicadas(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {

			logger.info("Entro Servicio");
			boolean res = duplicadosBI.eliminaRespuestasD();

			logger.info("sali Servicio");
			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "RESPUESTAS DUPLICADAS ELIMINADAS");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}
	


	// http://localhost:8080/checklist/checklistDuplicados/consultaRespuestasDup.json?idUsuario=<?>&idCheckUsu=<?>&idCheck=<?>&idBitacora=<?>&fechaI=<?>&fechaF=<?>&bandera=<?>
	@RequestMapping(value = "/consultaRespuestasDup", method = RequestMethod.GET)
	public ModelAndView consultaRespuestasDup(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {

			String idUsuario = request.getParameter("idUsuario");
			String idCheckUsu = request.getParameter("idCheckUsu");
			String idCheck = request.getParameter("idCheck");
			String idBitacora = request.getParameter("idBitacora");
			String fechaI = request.getParameter("fechaI");
			String fechaF = request.getParameter("fechaF");
			String bandera = request.getParameter("bandera");
			
			List<DuplicadosDTO> res = duplicadosBI.consultaDuplicados(idUsuario, idCheckUsu, idCheck, idBitacora, fechaI, fechaF, Integer.parseInt(bandera));

			logger.info("sali Servicio");
			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
			mv.addObject("tipo", "RESPUESTAS DUPLICADAS ELIMINADAS");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistDuplicados/eliminaRespuestasDupli.json?idUsuario=<?>&idCheckUsu=<?>&idCheck=<?>&idBitacora=<?>
	@RequestMapping(value = "/eliminaRespuestasDupli", method = RequestMethod.GET)
	public ModelAndView eliminaRespuestasDupli(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {

			String idUsuario = request.getParameter("idUsuario");
			String idCheckUsu = request.getParameter("idCheckUsu");
			String idCheck = request.getParameter("idCheck");
			String idBitacora = request.getParameter("idBitacora");
			
			boolean res = duplicadosBI.eliminaDuplicadosResp(idUsuario, idCheckUsu, idCheck, idBitacora);

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "RESPUESTAS DUPLICADAS ELIMINADAS");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}
	
	

	// http://localhost:8080/checklist/checklistDuplicados/eliminaDuplicadasRespuestas.json?idUsuario=<?>&idCheckUsu=<?>&idCheck=<?>&idBitacora=<?>&fechaI=<?>&fechaF=<?>
	@RequestMapping(value = "/eliminaDuplicadasRespuestas", method = RequestMethod.GET)
	public ModelAndView eliminaDuplicadasRespuestas(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {

			String idUsuario = request.getParameter("idUsuario");
			String idCheckUsu = request.getParameter("idCheckUsu");
			String idCheck = request.getParameter("idCheck");
			String idBitacora = request.getParameter("idBitacora");
			String fechaI = request.getParameter("fechaI");
			String fechaF = request.getParameter("fechaF");
			
			boolean res = duplicadosBI.eliminaDuplicadosNuevo(idUsuario, idCheckUsu, idCheck, idBitacora, fechaI, fechaF);

			logger.info("sali Servicio");
			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "RESPUESTAS DUPLICADAS ELIMINADAS");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}
	
	
	
}
