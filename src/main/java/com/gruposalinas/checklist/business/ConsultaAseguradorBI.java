package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ConsultaAseguradorDAO;
import com.gruposalinas.checklist.domain.ConsultaAseguradorDTO;

public class ConsultaAseguradorBI {
		
	private static Logger logger = LogManager.getLogger(ConsultaAseguradorBI.class);
	
	@Autowired
	ConsultaAseguradorDAO consultaAseguradorDAO;
	
	List<ConsultaAseguradorDTO> listaConsulta = null;

	public List<ConsultaAseguradorDTO> obtieneConsultaAsegurador(){
	
		try {
			listaConsulta = consultaAseguradorDAO.obtieneConsultaAsegurador();
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
		}			
		
		return listaConsulta;
	
	}
	
	List<ConsultaAseguradorDTO> listaConsulta2 = null;
	
	public List<ConsultaAseguradorDTO> obtieneConsultaSucursal(ConsultaAseguradorDTO bean){
		
		try {
			listaConsulta2 = consultaAseguradorDAO.obtieneConsultaSucursal(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
		}			
		
		return listaConsulta2;
	
		}

	List<ConsultaAseguradorDTO> listaConsulta3 = null;

	public List<ConsultaAseguradorDTO> obtieneConsultaBitaCerr(ConsultaAseguradorDTO bean){
		
		try {
			listaConsulta3 = consultaAseguradorDAO.obtieneConsultaBitaCerr(bean);
		} catch (Exception e) {
			logger.info(e);
			logger.info("No fue posible consultar el StoreProcedure");
		}			
		
		return listaConsulta3;
	
		}
	
	List<ConsultaAseguradorDTO> listaConsulta4 = null;

	public List<ConsultaAseguradorDTO> obtieneProtocolo(ConsultaAseguradorDTO bean){
		
		try {
			listaConsulta4 = consultaAseguradorDAO.obtieneProtocolo(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
		}			
		
		return listaConsulta4;
	
		}
	
	List<ConsultaAseguradorDTO> listaConsulta5 = null;

	public List<ConsultaAseguradorDTO> obtieneDetalleBitacora(ConsultaAseguradorDTO bean){
		
		try {
			listaConsulta5 = consultaAseguradorDAO.obtieneDetalleBitacora(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
		}			
		
		return listaConsulta5;
	
		}
}
