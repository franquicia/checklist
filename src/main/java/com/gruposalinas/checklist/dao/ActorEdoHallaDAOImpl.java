package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ActorEdoHallaDTO;
import com.gruposalinas.checklist.mappers.ActorEdoGpoHallazRowMapper;
import com.gruposalinas.checklist.mappers.ActorHallazgoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ActorEdoHallaDAOImpl extends DefaultDAO implements ActorEdoHallaDAO {

    private static Logger logger = LogManager.getLogger(ActorEdoHallaDAOImpl.class);

    DefaultJdbcCall jdbcInsertaActorHalla;
    DefaultJdbcCall jdbcEliminaActorHalla;
    DefaultJdbcCall jdbcObtieneActorHalla;
    DefaultJdbcCall jbdcActualizaActorHalla;
    DefaultJdbcCall jdbcInsertaConfEdoHalla;
    DefaultJdbcCall jdbcEliminaConfEdoHalla;
    DefaultJdbcCall jdbcObtieneConfEdoHalla;
    DefaultJdbcCall jbdcActualizaConfEdoHalla;

    public void init() {

        jdbcInsertaActorHalla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ACTOR")
                .withProcedureName("SP_INS_ACTOR");

        jdbcEliminaActorHalla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ACTOR")
                .withProcedureName("SP_DEL_ACTOR");

        jdbcObtieneActorHalla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ACTOR")
                .withProcedureName("SP_SEL_ACTOR")
                .returningResultSet("RCL_ACT", new ActorHallazgoRowMapper());

        jbdcActualizaActorHalla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ACTOR")
                .withProcedureName("SP_ACT_ACTOR");

        //CONFIG EDO HALLAZGO
        jdbcInsertaConfEdoHalla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ACTOR")
                .withProcedureName("SP_INS_CONFEDO");

        jdbcEliminaConfEdoHalla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ACTOR")
                .withProcedureName("SP_DEL_CONFEDO");

        jdbcObtieneConfEdoHalla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ACTOR")
                .withProcedureName("SP_SEL_CONFEDO")
                .returningResultSet("RCL_CONF", new ActorEdoGpoHallazRowMapper());

        jbdcActualizaConfEdoHalla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ACTOR")
                .withProcedureName("SP_ACT_CONFEDO");

    }

    public int insertaActorHalla(ActorEdoHallaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NOMBRE", bean.getNombreActor())
                .addValue("PA_OBS", bean.getObservacion())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_IDCONFIG", bean.getIdConfiguracion())
                .addValue("PA_IDPERFIL", bean.getIdPerfil())
                .addValue("PA_AUX", bean.getAux());

        out = jdbcInsertaActorHalla.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADM_ACTOR.SP_INS_ACTOR}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDACT");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el Empleado");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean eliminaActorHalla(int idFirma) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDACT", idFirma);

        out = jdbcEliminaActorHalla.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADM_ACTOR.SP_DEL_ACTOR}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + idFirma + ")");
        } else {
            return true;
        }

        return false;

    }

    //parametro
    @SuppressWarnings("unchecked")
    public List<ActorEdoHallaDTO> obtieneDatosActorHalla(String idFirma) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ActorEdoHallaDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDACT", idFirma);

        out = jdbcObtieneActorHalla.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADM_ACTOR.SP_SEL_ACTOR}");
        //lleva el nombre del cursor del procedure
        lista = (List<ActorEdoHallaDTO>) out.get("RCL_ACT");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el empleado(" + idFirma + ")");
        }
        return lista;

    }

    public boolean actualizaActorHalla(ActorEdoHallaDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDACT", bean.getIdActorConfig())
                .addValue("PA_NOMBRE", bean.getNombreActor())
                .addValue("PA_OBS", bean.getObservacion())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_IDCONFIG", bean.getIdConfiguracion())
                .addValue("PA_IDPERFIL", bean.getIdPerfil())
                .addValue("PA_AUX", bean.getAux());

        out = jbdcActualizaActorHalla.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADM_ACTOR.SP_ACT_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id ACTOR( " + bean.getIdActorConfig() + ")");
        } else {
            return true;
        }
        return false;

    }

    @Override
    public int insertaConfEdoHalla(ActorEdoHallaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NOMBRE", bean.getNombStatus())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_IDCONF", bean.getIdConfiguracion())
                .addValue("PA_OBS", bean.getObservacion())
                .addValue("PA_AUX", bean.getAux())
                .addValue("PA_COLOR", bean.getColor());

        out = jdbcInsertaConfEdoHalla.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADM_ACTOR.SP_INS_CONFEDO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_EDO");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el Empleado");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    @Override
    public boolean eliminaConfEdoHalla(int idFirma) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_EDO", idFirma);

        out = jdbcEliminaConfEdoHalla.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADM_ACTOR.SP_DEL_AGRUPA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + idFirma + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<ActorEdoHallaDTO> obtieneDatosConfEdoHalla(String idFirma) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ActorEdoHallaDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_EDO", idFirma);

        out = jdbcObtieneConfEdoHalla.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADM_ACTOR.SP_SEL_ConfEdoHalla}");
        //lleva el nombre del cursor del procedure
        lista = (List<ActorEdoHallaDTO>) out.get("RCL_CONF");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el empleado(" + idFirma + ")");
        }
        return lista;

    }

    @Override
    public boolean actualizaConfEdoHalla(ActorEdoHallaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NOMBRE", bean.getNombStatus())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_IDCONF", bean.getIdConfiguracion())
                .addValue("PA_OBS", bean.getObservacion())
                .addValue("PA_AUX", bean.getAux())
                .addValue("PA_COLOR", bean.getColor())
                .addValue("PA_EDO", bean.getIdEdoHallazgo());

        out = jbdcActualizaConfEdoHalla.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADM_ACTOR.SP_ACT_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar el estadohallazgo  id( " + bean.getIdEdoHallazgo() + ")");
        } else {
            return true;
        }
        return false;

    }

}
