package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.ReporteMedPregDTO;

public class ReporteMedPregRowMapper implements RowMapper<ReporteMedPregDTO> {

	public ReporteMedPregDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteMedPregDTO reporteDTO = new ReporteMedPregDTO();
		
		reporteDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		reporteDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		reporteDTO.setDescPregunta(rs.getString("FCDESCRIPCION"));
		reporteDTO.setTipoPreg(rs.getInt("FIID_TIPO_PREG"));
		reporteDTO.setEstatus(rs.getInt("FIESTATUS"));
		reporteDTO.setIdModulo(rs.getInt("FIID_MODULO"));
		reporteDTO.setIdPregPadre(rs.getInt("FIID_PREG_PADRE"));
		reporteDTO.setIdCritica(rs.getInt("FICRITICA"));
		reporteDTO.setNumSerie(rs.getInt("FINUMSERIE"));
		
		return reporteDTO;
		
	}
}
