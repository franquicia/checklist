package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.mappers.ChecklistPreguntaRowMapper;
import com.gruposalinas.checklist.mappers.PreguntasXCheklistRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ChecklistPreguntaDAOImpl extends DefaultDAO implements ChecklistPreguntaDAO {

    private static Logger logger = LogManager.getLogger(ChecklistPreguntaDAOImpl.class);

    private DefaultJdbcCall insertaChecklistPregunta;
    private DefaultJdbcCall actualizaChecklistPregunta;
    private DefaultJdbcCall eliminaChecklistPregunta;
    private DefaultJdbcCall buscaChecklistPregunta;
    private DefaultJdbcCall obtienePreguntas;
    private DefaultJdbcCall cambiaestatus;

    public void init() {

        insertaChecklistPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK_PREG")
                .withProcedureName("SP_INS_CHECK_PREG");

        actualizaChecklistPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK_PREG")
                .withProcedureName("SP_ACT_CHECK_PREG");

        eliminaChecklistPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK_PREG")
                .withProcedureName("SP_DEL_CHECK_PREG");

        buscaChecklistPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK_PREG")
                .withProcedureName("SP_SEL_CHECK_PREG")
                .returningResultSet("RCL_CHECK_PREG", new ChecklistPreguntaRowMapper());

        obtienePreguntas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK_PREG")
                .withProcedureName("SPOBTIENEPREGS")
                .returningResultSet("RCL_PREGS", new PreguntasXCheklistRowMapper());

        cambiaestatus = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK_PREG")
                .withProcedureName("SPCAMBIAESTATUS");
    }

    public boolean insertaChecklistPregunta(ChecklistPreguntaDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECKLIST", bean.getIdChecklist())
                .addValue("PA_FIID_PREGUNTA", bean.getIdPregunta())
                .addValue("PA_FIORDEN_CHECK", bean.getOrdenPregunta())
                .addValue("PA_FIID_PREG_PADRE", bean.getPregPadre())
                .addValue("PA_COMMIT", bean.getCommit());

        out = insertaChecklistPregunta.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_ADM_CHECK_PREG.SP_INS_CHECK_PREG}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar ChecklistPregunta");
        } else {
            return true;
        }
        return false;
    }

    public boolean actualizaChecklistPregunta(ChecklistPreguntaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECKLIST", bean.getIdChecklist())
                .addValue("PA_FIID_PREGUNTA", bean.getIdPregunta())
                .addValue("PA_FIORDEN_CHECK", bean.getOrdenPregunta())
                .addValue("PA_FIID_PREG_PADRE", bean.getPregPadre());

        out = actualizaChecklistPregunta.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_ADM_CHECK_PREG.SP_ACT_CHECK_PREG}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar ChecklistPregunta");
        } else {
            return true;
        }
        return false;

    }

    public boolean eliminaChecklistPregunta(int idChecklist, int idPregunta) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECKLIST", idChecklist)
                .addValue("PA_FIID_PREGUNTA", idPregunta);

        out = eliminaChecklistPregunta.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_ADM_CHECK_PREG.SP_DEL_CHECK_PREG}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al eliminar ChecklistPregunta");
        } else {
            return true;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntaDTO> buscaPreguntas(int idChecklist) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        List<ChecklistPreguntaDTO> listaChecklistPregunta = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECKLIST", idChecklist);

        out = buscaChecklistPregunta.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_ADM_CHECK_PREG.SP_SEL_CHECK_PREG}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listaChecklistPregunta = (List<ChecklistPreguntaDTO>) out.get("RCL_CHECK_PREG");

        if (error == 1) {
            logger.info("Algo ocurrió al obtener ChecklistPregunta");
        } else {
            return listaChecklistPregunta;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntaDTO> obtienePregXcheck(int idChecklist) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;

        List<ChecklistPreguntaDTO> listaChecklistPregunta = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_CHECKLIST", idChecklist);

        out = obtienePreguntas.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_ADM_CHECK_PREG.SPOBTIENEPREGS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = errorReturn.intValue();

        listaChecklistPregunta = (List<ChecklistPreguntaDTO>) out.get("RCL_PREGS");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al obtener preguntas por checklist");
        } else {
            return listaChecklistPregunta;
        }

        return null;
    }

    @Override
    public boolean cambiaEstatus() throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;
        int afectados = 0;

        out = cambiaestatus.execute();

        logger.info("Funcion ejecutada:{checklist.PA_ADM_CHECK_PREG.SPCAMBIAESTATUS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = errorReturn.intValue();

        BigDecimal afectadoReturn = (BigDecimal) out.get("PA_AFECTADOS");
        afectados = afectadoReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al obtener Actualizar estatus");
        } else {
            logger.info("Se afectaron " + afectados);
            return true;
        }

        return false;
    }

}
