package com.gruposalinas.checklist.domain;

public class ZonaNegoExpDTO {
	

		private int idNegocio;
		private int idRela;
		private int idZona;
		private int idTabZona;
		private int idActivo;
		private int idTabNegocio;
		private String clasifica;
		private String  negocio;
		private String  zona;
		private String  descZona;
		private String  descNego;
		private String  obs;
		private String  aux;
		private int  aux2;
		public int getIdNegocio() {
			return idNegocio;
		}
		public void setIdNegocio(int idNegocio) {
			this.idNegocio = idNegocio;
		}
		public int getIdRela() {
			return idRela;
		}
		public void setIdRela(int idRela) {
			this.idRela = idRela;
		}
		public int getIdZona() {
			return idZona;
		}
		public void setIdZona(int idZona) {
			this.idZona = idZona;
		}
		public int getIdTabZona() {
			return idTabZona;
		}
		public void setIdTabZona(int idTabZona) {
			this.idTabZona = idTabZona;
		}
		public int getIdActivo() {
			return idActivo;
		}
		public void setIdActivo(int idActivo) {
			this.idActivo = idActivo;
		}
		public int getIdTabNegocio() {
			return idTabNegocio;
		}
		public void setIdTabNegocio(int idTabNegocio) {
			this.idTabNegocio = idTabNegocio;
		}
		public String getClasifica() {
			return clasifica;
		}
		public void setClasifica(String clasifica) {
			this.clasifica = clasifica;
		}
		public String getNegocio() {
			return negocio;
		}
		public void setNegocio(String negocio) {
			this.negocio = negocio;
		}
		public String getZona() {
			return zona;
		}
		public void setZona(String zona) {
			this.zona = zona;
		}
		public String getDescZona() {
			return descZona;
		}
		public void setDescZona(String descZona) {
			this.descZona = descZona;
		}
		public String getDescNego() {
			return descNego;
		}
		public void setDescNego(String descNego) {
			this.descNego = descNego;
		}
		public String getObs() {
			return obs;
		}
		public void setObs(String obs) {
			this.obs = obs;
		}
		public String getAux() {
			return aux;
		}
		public void setAux(String aux) {
			this.aux = aux;
		}
		public int getAux2() {
			return aux2;
		}
		public void setAux2(int aux2) {
			this.aux2 = aux2;
		}
		@Override
		public String toString() {
			return "ZonaNegoExpDTO [idNegocio=" + idNegocio + ", idRela=" + idRela + ", idZona=" + idZona
					+ ", idTabZona=" + idTabZona + ", idActivo=" + idActivo + ", idTabNegocio=" + idTabNegocio
					+ ", clasifica=" + clasifica + ", negocio=" + negocio + ", zona=" + zona + ", descZona=" + descZona
					+ ", descNego=" + descNego + ", obs=" + obs + ", aux=" + aux + ", aux2=" + aux2 + "]";
		}
		
		
		
		
	}