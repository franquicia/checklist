<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html lang="es">
<head>
<title>Banco Azteca | Sistematización de actividades</title> 
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/vistaCumplimientoVisitas/estilo2.css" />
<script type="text/javascript">
	var conteoL = ${conteo};
	var nivelPerfilL = ${nivelPerfil};
	var porcentajeG = ${porcentajeG};
	var idCecoPadreL = ${idCecoPadre};
	var nombreCecoL = '${nombreCeco}';
	var banderaCecoPadreL = ${banderaCecoPadre};	
	var seleccionAnoL = ${seleccionAno};
	var seleccionMesL = ${seleccionMes};
	var seleccionCanalL = ${seleccionCanal};
	var seleccionPaisL = ${seleccionPais};
	var url = '${url}';
</script>	

<link rel="stylesheet" media="screen" href="../css/vistaCumplimientoVisitas/modal.css">
	
<!-- jquery Start -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/vistaCumplimientoVisitas/jquery.js"></script>
	
<!-- dropdown -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/vistaCumplimientoVisitas/menu.js"></script>
	
<!-- dropdown -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/vistaCumplimientoVisitas/script-vistaTiempoReal.js"></script>
	
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- <link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/vistaCumplimientoVisitas/tooltipster.bundle.css" /> -->


	<style type="text/css">
			.nombreCheck {
			}
	</style>
	<% response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate"); %>
	<% response.addHeader("Pragma", "no-cache"); %>
	<% response.addHeader("Expires", "0"); %>
</head>
<body onload="nobackbutton();">

<div id="tipoModal" class="modal3"></div>

	<input type="hidden" id="longitudRegiones" value="${(fn:length(listaSucursales))}"></input>	
	
	<div class="header" style="z-index: 10; top: -40px;">
		
		<div class="wrapperFixed" style="background:red; background:#f2f2f2;">
		
			<div class="goborder">
				<h2>
					<a id="idPais" style="color:#747474"></a><span class="subSeccion">/<a id="idOpcTerr" style="color:#747474"></a></span>
				</h2>
			</div>

			<table class="avance" style="max-width: 800px; min-width:320px; height: 100px; background: #a6b0b3;
			border-radius: 150px 150px 150px 150px;	">
				<thead>
					<tr style="height: 50px;">
						<th rowspan="2" class="rayaBlanca" style="font-size: 2em; color: white;">Mi <br>compromiso</th>
						
						<th style="width: ${64/4}%;"><p class="margenArriba">Consumo</p></th>
						<th style="width: ${64/4}%;"><p class="margenArriba">Préstamos<br>Personales</p></th>
						<th style="width: ${64/4}%;"><p class="margenArriba">Cajeros</p></th>
						<th style="width: ${64/4}%;"><p class="margenArriba">Filas</p></th>
				
					</tr>
					<tr style="height: 50px;">
						<th style="width: ${64/4}%;"><p class="margenAbajo">100%</p></th>
						<th style="width: ${64/4}%;"><p class="margenAbajo">100%</p></th>
						<th style="width: ${64/4}%;"><p class="margenAbajo"></p></th>
						<th style="width: ${64/4}%;"><p class="margenAbajo"></p></th>
					</tr>
				</thead>
			</table>

			<div class="hspacerSin"></div>

			
		</div>
	</div>
	
	
	<div class="wrapper">
		<div class="hspacer"></div>

		<div class="box" style="margin-top: 37px;">
		
		 <form id="formTerri" method="POST" action="vistaTerritCumplimientoVisitas.htm" >	
				<dl class="accordion3" id="idGeneralTablaTerritorios">
				
				<c:if test="${nivelPerfil==0}">
					<c:forEach var = "i" begin = "0" end = "${(fn:length(listaSucursales))-1}">
						<dt onclick="despliega(${listaSucursales[i][0].idCeco})"  style="background: #ffffff;">
							<table class="bloquer linea goshadow1">
								<tbody>
									<tr class="pointer">
										<td class="nombreCheck">${listaSucursales[i][0].descCeco} <span>${listaSucursales[i][0].numSuc} suc</span></td>
										
										<td class="nombreCheck" style="width: ${64/4}%;" id="f0${i}"><div class="radio grisN"><b class="colorF"></b></div></td>
   										<td class="nombreCheck" style="width: ${64/4}%;" id="f1${i}"><div class="radio grisN"><b class="colorF"></b></div></td>									
										<td class="nombreCheck" style="width: ${64/4}%;"><div class="radio grisN"></div></td>
										<td class="nombreCheck" style="width: ${64/4}%;"><div class="radio grisN"></div></td>
										
										<input type="hidden" id="posicion${i}" value="${listaSucursales[i][0].idCeco}"></input>	
   										<c:set var = "idCeco" scope = "session" value = "${listaSucursales[i][0].idCeco}"/>
   										
   										<script>			
											pPersonales(${i},${listaSucursales[i][0].idCeco},'${listaSucursales[i][0].descCeco}',${(fn:length(listaSucursales))-1});	
										</script>
   					
									</tr>
								</tbody>
							</table>
						</dt>
						
						
						<dd>
							<div class="inner" id="idLlenaZonas${idCeco}"></div>	
						</dd>						
					</c:forEach>
				</c:if>
				</dl>
			</form>
		</div>
		<div class="fspacer"></div>
	</div>
	

	<!-- progressbar -->
	<script>
		function move() {
			var elem = document.getElementById("myBar");
			var width = 1;
			var id = setInterval(frame, 10);
			function frame() {
				if (width >= 100) {
					clearInterval(id);
				} else {
					width++;
					elem.style.width = width + "%";
				}
			}
		}
	</script>


	<script type="text/javascript">
		jQuery(function() {
			$(".subSeccion").hide();

		  var allPanels = $('.accordion3 > dd').hide();
		  var activo = 0;

		  jQuery('.accordion3 > dt').on('click', function() {
		    $this = $(this);
		    //the target panel content
		    $target = $this.next();
		    
		    jQuery('.accordion3 > dt').removeClass('accordion3-active');
		    if ($target.hasClass("in")) {
		        $this.removeClass('accordion3-active');
		        $target.slideUp();
		        $target.removeClass("in");
				$(".subSeccion").hide();
				
				clearInterval(intervaloDespliegue);
				intervalo = setInterval("reporte()",300000);
				
				activo=0;
				
		    } else {
			    $this.addClass('accordion3-active');
			    jQuery('.accordion3 > dd').removeClass("in");
			    $target.addClass("in");
				$(".subSeccion").show();
			    jQuery('.accordion3 > dd').slideUp();
			    $target.slideDown();
			      
			    clearInterval(intervalo);
			    //intervaloDespliegue = setInterval("despliegapPersonalesIntervalo()",60000);
		    	
			    if(activo==0){
			    	intervaloDespliegue = setInterval("despliegapPersonalesIntervalo()",300000);
			    	activo=1;
			    }
		    }
		  })
		});
	</script>
	
</body>
</html>
