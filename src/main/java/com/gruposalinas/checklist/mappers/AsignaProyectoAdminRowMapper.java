package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.AsignaTransfDTO;

public class AsignaProyectoAdminRowMapper implements RowMapper<AsignaTransfDTO> {

	public AsignaTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AsignaTransfDTO AsignaTransfDTO = new AsignaTransfDTO();
		
		AsignaTransfDTO.setProyecto(rs.getInt("FIID_PROYECTO"));
		AsignaTransfDTO.setCeco(rs.getInt("FCID_CECO"));
		AsignaTransfDTO.setNombProy(rs.getString("NOMBRE_PROYECTO"));
		AsignaTransfDTO.setObsProyecto(rs.getString("NEGOCIO"));
	    AsignaTransfDTO.setTipoProyecto(rs.getInt("TIPOPROY"));
	    AsignaTransfDTO.setEdoCargaInicial(rs.getInt("FIID_EDOCARGAINI"));
	    AsignaTransfDTO.setStatusMaximo(rs.getInt("FCEDOHALLAFIN"));
	    AsignaTransfDTO.setCeco(rs.getInt("FCID_CECO"));
	    AsignaTransfDTO.setFaseActual(rs.getInt("FIID_FASEACT"));
		AsignaTransfDTO.setIdestatus(rs.getInt("FCSTATUS"));
	    AsignaTransfDTO.setAgrupador(rs.getInt("FIID_AGRUPA"));
	    AsignaTransfDTO.setUsuario_asig(rs.getInt("RESPONSABLE_ASIGNO"));
	    AsignaTransfDTO.setUsuario(rs.getInt("FIID_USUARIO"));
	    AsignaTransfDTO.setNombreUsuario(rs.getString("NOMBRE_USUARIO"));
	    AsignaTransfDTO.setObs(rs.getString("OBS_ASIGNA"));
	    AsignaTransfDTO.setFase(rs.getInt("FIID_FASE"));
	    AsignaTransfDTO.setIdOrdenFase(rs.getInt("FIID_ORDENFASE"));
	    AsignaTransfDTO.setNombreFase(rs.getString("NOMBRE_FASE"));
	 
		return AsignaTransfDTO;
	}

}


