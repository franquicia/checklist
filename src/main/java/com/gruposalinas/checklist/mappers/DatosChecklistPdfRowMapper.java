package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.DatosChecklistPdfDTO;

public class DatosChecklistPdfRowMapper implements RowMapper<DatosChecklistPdfDTO> {

	@Override
	public DatosChecklistPdfDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		DatosChecklistPdfDTO datosChecklistPdfDTO = new DatosChecklistPdfDTO();
		
		datosChecklistPdfDTO.setNombreCheck(rs.getString("NOMBRE_CHECK"));
		datosChecklistPdfDTO.setCeco(rs.getString("FCID_CECO"));
		datosChecklistPdfDTO.setNombreCeco(rs.getString("FCNOMBRE"));
		datosChecklistPdfDTO.setGerente(rs.getString("GERENTE"));
		datosChecklistPdfDTO.setRegional(rs.getString("REGIONAL"));
		datosChecklistPdfDTO.setFechaInicio(rs.getString("FDINICIO"));
		datosChecklistPdfDTO.setFechaFin(rs.getString("FDTERMINO"));
		
		return datosChecklistPdfDTO;
	}
	
	
	

}
