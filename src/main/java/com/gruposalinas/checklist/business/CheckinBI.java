package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.CheckinDAO;
import com.gruposalinas.checklist.domain.CheckinDTO;

public class CheckinBI {
		
	private static Logger logger = LogManager.getLogger(CheckinBI.class);
	
	@Autowired
	CheckinDAO checkinDAO;
	
	List<CheckinDTO> listaCheckin = null;
	
	public int insertaCheckin(CheckinDTO bean){
		
		int idCheckin = 0;
		
		try {
			idCheckin = checkinDAO.insertaCheckin(bean);			
		} catch (Exception e) {
			logger.info(e);
			logger.info("No fue posible insertar el Checkin");
		}
		
		return idCheckin;
		
	}
		
	public boolean modificaCheckin(CheckinDTO bean){
		
		boolean respuesta = false ;
		
		try {
			respuesta = checkinDAO.modificaCheckin(bean);
		} catch (Exception e) {
			logger.info("No fue posible modificar el Checkin");
		}
		
		return respuesta;
	}
	
	public boolean eliminaCheckin(int idCheckin){
		
		boolean respuesta = false ;
		
		try {
			respuesta = checkinDAO.eliminaCheckin(idCheckin);
		} catch (Exception e) {
			logger.info("No fue posible eliminar el checkin");
		}
		
		return respuesta;		
	}
	
	

	public List<CheckinDTO> buscaCheckin(CheckinDTO bean){
	
		try {
			listaCheckin = checkinDAO.obtieneCheckin(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar los checkin: "+e);
		}			
		
		return listaCheckin;
	
	}
}
