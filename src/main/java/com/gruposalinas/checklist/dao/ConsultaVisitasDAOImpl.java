package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ConsultaVisitasDTO;
import com.gruposalinas.checklist.mappers.ConsultaPromedioCalifRowMapper;
import com.gruposalinas.checklist.mappers.ConsultaVisitasRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ConsultaVisitasDAOImpl extends DefaultDAO implements ConsultaVisitasDAO {

    private static Logger logger = LogManager.getLogger(ProtocoloDAOImpl.class);

    DefaultJdbcCall jdbcObtieneVisitas;
    DefaultJdbcCall jdbcObtienePromCalif;

    public void init() {

        jdbcObtieneVisitas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE_SUPRV")
                .withProcedureName("SPGETVISITAS")
                .returningResultSet("PA_CONSULTA", new ConsultaVisitasRowMapper());

        jdbcObtienePromCalif = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE_SUPRV")
                .withProcedureName("SPGETPROMCALIF")
                .returningResultSet("PA_CONSULTA", new ConsultaPromedioCalifRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaVisitasDTO> ObtieneVisitas(ConsultaVisitasDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaVisitasDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_CECO", bean.getIdCeco())
                .addValue("PA_FIPROTOCOLO", bean.getProtocolo());

        out = jdbcObtieneVisitas.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAREPORTE_SUPRV.SPGETVISITAS");

        lista = (List<ConsultaVisitasDTO>) out.get("PA_CONSULTA");

        if (error != 1) {
            logger.info("Algo ocurrió obtener el territorio");
        } else {

            return lista;

        }

        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaVisitasDTO> ObtienePromCalif(ConsultaVisitasDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaVisitasDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_CECO", bean.getIdCeco())
                .addValue("PA_FIPROTOCOLO", bean.getProtocolo());

        out = jdbcObtienePromCalif.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAREPORTE_SUPRV.SPGETPROMCALIF");

        lista = (List<ConsultaVisitasDTO>) out.get("PA_CONSULTA");

        if (error != 1) {
            logger.info("Algo ocurrió obtener el territorio");
        } else {

            return lista;

        }
        return lista;
    }

}
