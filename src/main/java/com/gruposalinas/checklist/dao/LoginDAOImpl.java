package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.LoginDTO;
import com.gruposalinas.checklist.domain.PerfilUsuarioDTO;
import com.gruposalinas.checklist.mappers.ChecklistComboRowMapper;
import com.gruposalinas.checklist.mappers.LoginRowMapper;
import com.gruposalinas.checklist.mappers.LoginRowMapperN;
import com.gruposalinas.checklist.mappers.PerfilUsuarioLoginRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class LoginDAOImpl extends DefaultDAO implements LoginDAO {

    private static final Logger logger = LogManager.getLogger(LoginDAOImpl.class);
    private DefaultJdbcCall jdbcBuscaUsuario;
    private DefaultJdbcCall jdbcBuscaUsuarioCheck;

    public void init() {
        jdbcBuscaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_LOGIN")
                .withProcedureName("SP_BUSCA_USUARIO")
                .returningResultSet("PA_CUR_DATOS", new LoginRowMapper());

        jdbcBuscaUsuarioCheck = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_LOGIN")
                .withProcedureName("SP_BUSCA_USUARIO_N")
                .returningResultSet("PA_CUR_DATOS", new LoginRowMapperN())
                .returningResultSet("PA_CUR_CHECK", new ChecklistComboRowMapper())
                .returningResultSet("PA_CUR_PERFILES", new PerfilUsuarioLoginRowMapper());
    }

    @SuppressWarnings("unchecked")
    public List<LoginDTO> buscaUsuario(int usuario) throws Exception {
        Map<String, Object> out = null;
        List<LoginDTO> datosUsuario = null;
        int respuesta = 0;

        try {

            SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_USUARIO", usuario);

            out = jdbcBuscaUsuario.execute(in);

            logger.info("Funci�n ejecutada:{checklist.PA_LOGIN.SP_BUSCA_USUARIO}");

            datosUsuario = (List<LoginDTO>) out.get("PA_CUR_DATOS");

            BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
            respuesta = respuestaEjec.intValue();

            if (respuesta == 1) {
                throw new Exception("Algo ocurrió en el SP");
            }

        } catch (Exception e) {
            logger.info(e.getMessage());
            ////System.out.println("try de dao implemen" + e.getMessage());
            //throw new Exception();
        }

        return datosUsuario;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> buscaUsuarioCheck(int noEmpleado) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<LoginDTO> datosUsuario = null;
        List<ChecklistDTO> listaChecklist = null;
        List<PerfilUsuarioDTO> listaPerfiles = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_USUARIO", noEmpleado);
        out = jdbcBuscaUsuarioCheck.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAPRUEBA2.SP_BUSCA_USUARIO}");

        datosUsuario = (List<LoginDTO>) out.get("PA_CUR_DATOS");
        listaChecklist = (List<ChecklistDTO>) out.get("PA_CUR_CHECK");
        listaPerfiles = (List<PerfilUsuarioDTO>) out.get("PA_CUR_PERFILES");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("Algo ocurrió en el SP");
        }

        lista = new HashMap<String, Object>();
        lista.put("listaUsuarios", datosUsuario);
        lista.put("listaChecklist", listaChecklist);
        lista.put("listaPerfiles", listaPerfiles);

        return lista;
    }
}
