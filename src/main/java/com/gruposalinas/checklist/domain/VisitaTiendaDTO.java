package com.gruposalinas.checklist.domain;

public class VisitaTiendaDTO {
	
	private int idBitacora;
	private int idCheckusa;
	private int idCeco;
	private String nombreCeco;
	private String fechaInicio;
	private String fechaFin;
	private String fechaEnvio;
	private String ultimaVisita;
	
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public int getIdCheckusa() {
		return idCheckusa;
	}
	public void setIdCheckusa(int idCheckusa) {
		this.idCheckusa = idCheckusa;
	}
	public int getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(int idCeco) {
		this.idCeco = idCeco;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getFechaEnvio() {
		return fechaEnvio;
	}
	public void setFechaEnvio(String fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}
	public String getUltimaVisita() {
		return ultimaVisita;
	}
	public void setUltimaVisita(String ultimaVisita) {
		this.ultimaVisita = ultimaVisita;
	}

}
