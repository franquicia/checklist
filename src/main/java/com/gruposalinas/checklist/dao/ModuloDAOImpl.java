package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.mappers.ModuloRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ModuloDAOImpl extends DefaultDAO implements ModuloDAO {

    private static Logger logger = LogManager.getLogger(ModuloDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneModulo;
    DefaultJdbcCall jdbcInsertaModulo;
    DefaultJdbcCall jdbcActualizaModulo;
    DefaultJdbcCall jdbcEliminaModulo;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_MODULO")
                .withProcedureName("SP_SEL_G_MODULO")
                .returningResultSet("RCL_MODULO", new ModuloRowMapper());

        jdbcObtieneModulo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_MODULO")
                .withProcedureName("SP_SEL_MODULO")
                .returningResultSet("RCL_MODULO", new ModuloRowMapper());

        jdbcInsertaModulo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_MODULO")
                .withProcedureName("SP_INS_MODULO");

        jdbcActualizaModulo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_MODULO")
                .withProcedureName("SP_ACT_MODULO");

        jdbcEliminaModulo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_MODULO")
                .withProcedureName("SP_DEL_MODULO");

    }

    @SuppressWarnings("unchecked")
    public List<ModuloDTO> obtieneModulo() throws Exception {
        Map<String, Object> out = null;
        List<ModuloDTO> listaModulo = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_MODULO.SP_SEL_G_MODULO}");

        listaModulo = (List<ModuloDTO>) out.get("RCL_MODULO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Modulos");
        } else {
            return listaModulo;
        }

        return listaModulo;

    }

    @SuppressWarnings("unchecked")
    public List<ModuloDTO> obtieneModulo(int idModulo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ModuloDTO> listaModulo = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_MODULO", idModulo);

        out = jdbcObtieneModulo.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_MODULO,SP_SEL_MODULO}");

        listaModulo = (List<ModuloDTO>) out.get("RCL_MODULO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Modulos");
        } else {
            return listaModulo;
        }

        return listaModulo;

    }

    public int insertaModulo(ModuloDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idModulo = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_MOD_PADRE", bean.getIdModuloPadre());

        out = jdbcInsertaModulo.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_MODULO.SP_INS_MODULO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idModuloReturn = (BigDecimal) out.get("PA_IDMODULO");
        idModulo = idModuloReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Modulo");
        } else {
            return idModulo;
        }

        return idModulo;
    }

    public boolean actualizaModulo(ModuloDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDMODULO", bean.getIdModulo())
                .addValue("PA_MOD_PADRE", bean.getIdModuloPadre())
                .addValue("PA_NOMBRE", bean.getNombre());

        out = jdbcActualizaModulo.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_MODULO, SP_ACT_MODULO}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Modulo  id( " + bean.getIdModulo() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaModulo(int idModulo) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDMODULO", idModulo);

        out = jdbcEliminaModulo.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_MODULO.SP_DEL_MODULO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el Modulo id(" + idModulo + ")");
        } else {
            return true;
        }

        return false;
    }

}
