angular.module('app', [])

.controller('validarForm', ['$scope', function($scope) {
    $scope.tab = 1;

    $scope.setTab = function(newTab){
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };
	
  }])
.controller('tabs', ['$scope', function($scope) {
    $scope.tab1 = 1;

    $scope.setTab1 = function(newTab1){
      $scope.tab1 = newTab1;
    };

    $scope.isSet1 = function(tabNum1){
      return $scope.tab1 === tabNum1;
    };
	
  }]);


//Tooltip
(function() {

  function getOffset(elem) {
    var offsetLeft = 0, offsetTop = 0;
    do {
      if ( !isNaN( elem.offsetLeft ) )
      {
        offsetLeft += elem.offsetLeft;
        offsetTop += elem.offsetTop;
      }
    } while( elem = elem.offsetParent );
    return {left: offsetLeft, top: offsetTop};
  }

  var targets = document.querySelectorAll( '[rel=tooltip]' ),
  target  = false,
  tooltip = false,
  title   = false,
  tip     = false;

  for(var i = 0; i < targets.length; i++) {
    targets[i].addEventListener("mouseenter", function() {
      target  = this;
      tip     = target.getAttribute("title");
      tooltip = document.createElement("div");
      tooltip.id = "tooltip";

      if(!tip || tip == "")
      return false;

      target.removeAttribute("title");
      tooltip.style.opacity = 0;
      tooltip.innerHTML = tip;
      document.body.appendChild(tooltip);

      var init_tooltip = function()
      {
        console.log(getOffset(target));
        // set width of tooltip to half of window width
        if(window.innerWidth < tooltip.offsetWidth * 1.5)
        tooltip.style.maxWidth = window.innerWidth / 2;
        else
        tooltip.style.maxWidth = 340;

        var pos_left = getOffset(target).left + (target.offsetWidth / 2) - (tooltip.offsetWidth / 2),
        pos_top  = getOffset(target).top - tooltip.offsetHeight - 10;
        console.log("top is", pos_top);
        if( pos_left < 0 )
        {
          pos_left = getOffset(target).left + target.offsetWidth / 2 - 20;
          tooltip.classList.add("left");
        }
        else
        tooltip.classList.remove("left");

        if( pos_left + tooltip.offsetWidth > window.innerWidth )
        {
          pos_left = getOffset(target).left - tooltip.offsetWidth + target.offsetWidth / 2 + 20;
          tooltip.classList.add("right");
        }
        else
        tooltip.classList.remove("right");

        if( pos_top < 0 )
        {
          var pos_top  = getOffset(target).top + target.offsetHeight + 15;
          tooltip.classList.add("top");
        }
        else
        tooltip.classList.remove("top");
        // adding "px" is very important
        tooltip.style.left = pos_left + "px";
        tooltip.style.top = pos_top + "px";
        tooltip.style.opacity  = 1;
      };

      init_tooltip();
      window.addEventListener("resize", init_tooltip);

      var remove_tooltip = function() {
        tooltip.style.opacity  = 0;
        document.querySelector("#tooltip") && document.body.removeChild(document.querySelector("#tooltip"));
        target.setAttribute("title", tip );
      };

      target.addEventListener("mouseleave", remove_tooltip );
      tooltip.addEventListener("click", remove_tooltip );
    });
  }

})();

//Calendario
	$.datepicker.regional['es'] = {
		 closeText: 'Cerrar',
		 prevText: '',
		 nextText: ' ',
		 currentText: 'Hoy',
		 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
		 weekHeader: 'Sm',
		 dateFormat: 'dd/mm/yy',
		 firstDay: 1,
		 isRTL: false,
		 showMonthAfterYear: false,
		 yearSuffix: ''
	 };
	$.datepicker.setDefaults($.datepicker.regional['es']);
	$(function() {
		$( "#datepicker" ).datepicker({
			firstDay: 1 
		});
	});

$(document).ready(function() {
	//Modal resolución
	$('.modal01_view').click(function (e) {
		$('#modal01').modal();
		return false;
	})
	
	$('.modal02_view').click(function (e) {
		$('#modal02').modal();
		return false;
	})
	
	$('.modalEjemplo_view').click(function (e) {
		$('#modalEjemplo').modal();
		return false;
	})
	
	$('#modalCarga_view').click(function(e) {
		setTimeout(function() {
			$('#modalCarga').modal({
			});
			return false;
		}, 1);
	})

	// Menu hambuergesa
	$("#effect").toggle(false);
	$("#hamburger").click(function (event) {
		event.stopPropagation();
		 $( "#effect" ).toggle( "slide"); 
	});

	$(document).click(function() {
		$("#effect").toggle(false);
	});
	$("#effect").click (function (event){
		event.stopPropagation();
	}); 

	/*Menu User*/
	$(".MenuUser, .MenuUser1").hide();
	$('.imgShowMenuUser').click(function() {
		$(".MenuUser, .MenuUser1").toggle("ver");
	});

	$(".divRC").hide();
	$('#radioResCivil').on( "change", function() {
				$(".divRC").show();
			});
	$('#radioEmpre').on( "change", function() {
		$(".divRC").hide();
	});
	
	// Menu seguimiento
	$("#seguimiento").toggle(true);
	
	$(".hamburgerSeg").click(function (event) {
		event.stopPropagation();
		 $( ".seguimiento" ).toggle( "slide"); 
	});

	$(document).click(function() {
		$(".seguimiento").toggle(false);
	});
	$(".seguimiento").click (function (event){
		event.stopPropagation();
	});
	/********Para selects*******************/
	$( "select").dropkick({
		mobile: true
	});	
	
	$('.switch').click(function(){
		$(this).toggleClass("switchOn");
	}); 
	
	$( ".cerrarChip" ).click(function() {
	   $(this).parent().parent().remove()
	});

	/*Galeria*/
	colorboxConf = {
			maxWidth : '80%',
			maxHeight : '80%',
			opacity : 0.8,
			transition : 'elastic',
			current : ''
		};

        $('#galeria01').justifiedGallery({
            sizeRangeSuffixes: {
                'lt100': '_t',
                'lt240': '_m',
                'lt320': '_n',
                'lt500': '',
                'lt640': '_z',
                'lt1024': '_b'
            }
        }).on('jg.complete', function () {
            $(this).find('a').colorbox(colorboxConf);
        });

});
//Validacion login
$(".txtRojo").hide();
function validaLogin(f) {
	if(f.txtUsuario.value == "")  
  	{	
	  $(".txtRojo").html('Por favor ingrese su usuario');
	  return false;
  	}else
	if(f.txtLlave.value == "" )
  	{
	  $(".txtRojo").html('Por favor ingrese su contraseña');
	  return false;
  	}else
	{
		location.href="index.html";
		return false;}
}
/*Valida buscador del menu de hamburgesa*/
function valida(f) {
	if (f.busca.value == "")  {
		alert("Es necesario que introduzca un valor");
	}else { 
		return false;
	}
}
/*Detecta resolucion de pantalla*/
if (matchMedia) {
  const mq = window.matchMedia("(min-width: 780px)");
  mq.addListener(WidthChange);
  WidthChange(mq);
}
function WidthChange(mq) {
	if (mq.matches) {
	  	$("#menu ul").addClass("normal");
	  	$("#menu ul li").removeClass("in");
		$('ul.nivel1 >li > ul').slideUp();
		$('ul.nivel2 >li > ul').slideUp();
		$('ul.nivel1>li').off("click");
		$('ul.nivel2>li').off("click");
	} else {
	   $("#menu ul").removeClass("normal");

		$('ul.nivel1>li').on('click', function(event) {
			event.stopPropagation();
			
			$target = $(this).children();

			if ($(this).hasClass("in"))  {
			    $('ul.nivel2').slideUp();

				$(this).removeClass("in");
				$('.flecha').removeClass("rotar");
			}else {
			  	$('ul.nivel1 > li').removeClass("in");
				$('ul.nivel2').slideUp();
				$('ul.nivel3').slideUp();
				$('ul.nivel2>li').removeClass("in");
				$(this).addClass("in");
			  	$target.slideDown();
				$('ul.nivel1 > li > a .flecha').addClass("rotar");
				
			}
		});
		$('ul.nivel2>li').on('click', function(event) {
			event.stopPropagation();
		
			$target = $(this).children();

			if ($(this).hasClass("in"))  {
			    $('ul.nivel3').slideUp();
				$(this).removeClass("in");
				$('ul.nivel2 > li > a .flecha').removeClass("rotar");
			}else {
			  	$('ul.nivel2 > li').removeClass("in");
				$('ul.nivel3').slideUp();
				$(this).addClass("in");
			  	$target.slideDown();
				$('ul.nivel2 > li > a .flecha').addClass("rotar");
			}
		});
		$('ul.nivel3>li').on('click', function(event) {
			event.stopPropagation();
		});
	}
}
var allPanels = $('.accordion > dd').hide();

	jQuery('.accordion > dt').on('click', function() {
		$this = $(this);
		//the target panel content
		$target = $this.next();

		jQuery('.accordion > dt').removeClass('accordion-active');
		if ($target.hasClass("in")) {
		  $this.removeClass('accordion-active');
		  $target.slideUp();
		  $target.removeClass("in");

		} else {
		  $this.addClass('accordion-active');
		  jQuery('.accordion > dd').removeClass("in");
		  $target.addClass("in");
			$(".subSeccion").show();

		  jQuery('.accordion > dd').slideUp();
		  $target.slideDown();
		}
	});
	
	var allPanels = $('.accordionM > dd').hide();

	jQuery('.accordionM > dt').on('click', function() {
		$this = $(this);
		//the target panel content
		$target = $this.next();

		jQuery('.accordionM > dt').removeClass('accordion-active');
		if ($target.hasClass("in")) {
		  $this.removeClass('accordion-active');
		  $target.slideUp();
		  $target.removeClass("in");

		} else {
		  $this.addClass('accordion-active');
		  jQuery('.accordionM > dd').removeClass("in");
		  $target.addClass("in");
			$(".subSeccion").show();

		  jQuery('.accordionM > dd').slideUp();
		  $target.slideDown();
		}
	});
$(document).ready(function() {
	//Acordeon 
 	$(".menu2").accordion({
      collapsible: true,
      active: false,
      autoHeight: true,
      navigation: true,
      heightStyle: "content"
    });
	
	//Slider
	$('#owl-carousel1').owlCarousel({
		loop:true,
		margin:10,
		nav:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:3
			},
			1000:{
				items:4
			}
		}
	});

	$('#owl-carousel2').owlCarousel({
		center: true,
		loop:true,
		margin:10,
		nav:true,
		items: 5,
		smartSpeed: 750,
		navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
		responsive : {
			0 : {
				items: 1
			},
			850	 : {
				items: 3
			}
		}
	});
	
	 $('#owl-carousel3').owlCarousel({
		margin: 10,
		nav: true,
		loop: true,
		responsive: {
		  0: {
			items: 1
		  },
		  600: {
			items: 1
		  },
		  1000: {
			items: 1
		  }
		}
	  });
});

//Agregar
var contAdjunto1=0;
	$('.agregarRenglon').click(function (e) {
		contAdjunto1+=1;
		$(".divAdjuntar").append( $( "<div class='adjunto'><div><input type='text' placeholder='Nombre'></div><div><input type='file' id='carga"+contAdjunto1+"' class='inputfile carga'/><label for='carga"+contAdjunto1+"' class='btnAgregaFoto'><span>Adjuntar documento </span></label></div><div><img src='img/delete.svg' class='ico carga"+contAdjunto1+"'></div></div>" ) );
		declararEventoAdjuntarFileInputs("carga"+contAdjunto1);
		eliminarFoto("carga"+contAdjunto1);
	});	
					
	/*Eliminar*/
	function eliminarFoto(idElem){
		$("."+idElem).click(function (e) {
			$(this).parent().parent().remove();
		});	
	} 
//Agregar tr a tabla
var contAdjuntotabla=0;
	$('.agregarTr').click(function (e) {
		contAdjuntotabla+=1;
		$(".tblGeneral").append( $( "<tr><td><input type='text'></td><td><input type='text'></td><td><input type='text'></td><td><img src='img/icoEditar.svg'></td><td><img src='img/delete.svg' class='cargatbl"+contAdjuntotabla+"'></td></tr>" ) );
		eliminarFoto1("cargatbl"+contAdjuntotabla);
	});	
			
	/*Eliminar*/
	function eliminarFoto1(idElem1){alert
		$("."+idElem1).click(function (e) {
			$(this).parent().parent().remove();
		});	
	} 

// Agregar / Elimina Divs
var contDiv=1;
$('.agregarDiv').click(function(e) {
  contDiv+=1;
  $(".agregaDiv").append( $( "<div class='col4'><a  href='#' class='btnCerrar1 eliminaDiv"+contDiv+"' ></a>"+contDiv+" </div>" ) );
  eliminarFoto2("eliminaDiv"+contDiv);
});

function eliminarFoto2(idElem2){
   $("."+idElem2).click(function (e) {
     $(this).parent().remove();
   });
}

/* Seleccionar Checkbox*/
$("#checktodos").change(function () {$("input:checkbox.checkM").prop('checked', $(this).prop("checked"));});

// CHIP
$(".chip input[type='checkbox']").change(function(){
  if($(this).is(":checked")){
      $(this).parent().addClass("chipActive"); 
  }else{
      $(this).parent().removeClass("chipActive");  
  }
});
//Tag-it
$( ".chipClose .closebtn" ).click(function() {
  this.parentElement.style.display='none';
});
	
//Tree
	// Estructuras Asignados
	$("#EstructuraAsignada").treeview({
		animated: "fast",
		collapsed: true,
		control: "#treecontrol"
	});

	// Parametros Asignados
	$("#paramAsignados").treeview({
		animated: "fast",
		collapsed: true,
		control: "#treecontrol"
	});

	// Valores Asignados
	$("#ValoresAsignados").treeview({
		animated: "fast",
		collapsed: false,
		control: "#treecontrol"
	});

	$(' #EstructuraAsignada.treeview ul li span').click(function() {
		if ($(this).hasClass("in"))  {
			$(this).removeClass("seleccionado");
			$(this).removeClass("in");
		}else {
			$(this).addClass("seleccionado");
			$(this).addClass("in");	
		}
	});

	$(' #paramAsignados.treeview ul li span').click(function() {
		if ($(this).hasClass("in"))  {
			$(this).removeClass("seleccionado");
			$(this).removeClass("in");
		}else {
			$(this).addClass("seleccionado");
			$(this).addClass("in");	
		}
	});

	$(' #ValoresAsignados.treeview ul li span').click(function() {
		if ($(this).hasClass("in"))  {
			$(this).removeClass("seleccionado");
			$(this).removeClass("in");
		}else {
			$(this).addClass("seleccionado");
			$(this).addClass("in");	
		}
	});

  	$('#EstructuraAsignada ul.nivel3 li span').on('click', function(event) {
		$(this).removeClass("seleccionado");
	})

	$('#ValoresAsignados ul.nivel3 li span').on('click', function(event) {
		$(this).removeClass("seleccionado");
	});


