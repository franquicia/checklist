package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.EvidenciaDTO;
import com.gruposalinas.checklist.mappers.EvidenciaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class EvidenciaDAOImpl extends DefaultDAO implements EvidenciaDAO {

    private static Logger logger = LogManager.getLogger(EvidenciaDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneEvidencia;
    DefaultJdbcCall jdbcInsertaEvidencia;
    DefaultJdbcCall jdbcActualizaEvidencia;
    DefaultJdbcCall jdbcEliminaEvidencia;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_EVID")
                .withProcedureName("SP_SEL_G_EVID")
                .returningResultSet("RCL_EVID", new EvidenciaRowMapper());

        jdbcObtieneEvidencia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_EVID")
                .withProcedureName("SP_SEL_EVID")
                .returningResultSet("RCL_EVID", new EvidenciaRowMapper());

        jdbcInsertaEvidencia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_EVID")
                .withProcedureName("SP_INS_EVID_N");

        jdbcActualizaEvidencia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_EVID")
                .withProcedureName("SP_ACT_EVID");

        jdbcEliminaEvidencia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_EVID")
                .withProcedureName("SP_DEL_EVID");
    }

    @SuppressWarnings("unchecked")
    public List<EvidenciaDTO> obtieneEvidencia() throws Exception {
        Map<String, Object> out = null;
        List<EvidenciaDTO> listaEvidencia = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funcion ejecutada: {checklist.PA_ADM_EVID.SP_SEL_G_EVID}");

        listaEvidencia = (List<EvidenciaDTO>) out.get("RCL_EVID");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener las Evidencias");
        } else {
            return listaEvidencia;
        }

        return listaEvidencia;

    }

    @SuppressWarnings("unchecked")
    public List<EvidenciaDTO> obtieneEvidencia(int idTipo) throws Exception {
        Map<String, Object> out = null;
        List<EvidenciaDTO> listaEvidencia = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDTIPO", idTipo);

        out = jdbcObtieneEvidencia.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_EVID.SP_SEL_EVID}");

        listaEvidencia = (List<EvidenciaDTO>) out.get("RCL_EVID");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener la Evidencia con id(" + idTipo + ")");
        } else {
            return listaEvidencia;
        }

        return null;

    }

    public int insertaEvidencia(EvidenciaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEvidencia = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDRESP", bean.getIdRespuesta())
                .addValue("PA_IDTIPO", bean.getIdTipo())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_ELEMENTO", bean.getIdPlantilla());

        out = jdbcInsertaEvidencia.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_EVID.SP_INS_EVID}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_FIID_EVID");
        idEvidencia = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar la Evidencia");
        } else {
            return idEvidencia;
        }

        return idEvidencia;

    }

    public boolean actualizaEvidencia(EvidenciaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_EVID", bean.getIdEvidencia())
                .addValue("PA_IDRESP", bean.getIdRespuesta())
                .addValue("PA_IDTIPO", bean.getIdTipo())
                .addValue("PA_RUTA", bean.getRuta());

        out = jdbcActualizaEvidencia.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_EVID.SP_ACT_EVID}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar la Evidencia  id( " + bean.getIdEvidencia() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaEvidencia(int idEvidencia) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_EVID", idEvidencia);

        out = jdbcEliminaEvidencia.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_EVID.SP_DEL_EVID}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar la Evidencia id(" + idEvidencia + ")");
        } else {
            return true;
        }

        return false;
    }

}
