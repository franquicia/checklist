package com.gruposalinas.checklist.dao;

import java.util.List;
import com.gruposalinas.checklist.domain.AsistenciaSupervisorDTO;



public interface AsistenciaSupervisorDAO {
	
	public List<AsistenciaSupervisorDTO> obtieneAsistenciaSupervisor(AsistenciaSupervisorDTO bean) throws Exception;
	
	
}
