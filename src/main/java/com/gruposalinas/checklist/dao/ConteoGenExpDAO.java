package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.ConteoGenExpDTO;



public interface ConteoGenExpDAO {

		
		public Map<String, Object> obtieneDatos(String bita,String clasifica) throws Exception; 
		
		public List<ConteoGenExpDTO> obtieneInfo() throws Exception; 
		
		public Map<String, Object> obtieneDatosRepoSem(String bitaGral) throws Exception; 
		
		public Map<String, Object> obtieneDatosConteoGral(String ceco,String fase,String proyecto,String clasifica) throws Exception; 
	}