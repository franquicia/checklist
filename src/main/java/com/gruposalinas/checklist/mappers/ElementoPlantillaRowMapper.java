package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.PlantillaDTO;

public class ElementoPlantillaRowMapper implements RowMapper<PlantillaDTO>{

	public PlantillaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
	    
		PlantillaDTO plantillaDTO = new PlantillaDTO();
		
		plantillaDTO.setIdElemento(rs.getInt("FIID_ELEM_PL"));
		plantillaDTO.setEtiqueta(rs.getString("FCETIQUETA"));
		plantillaDTO.setOrden(rs.getInt("FIORDEN"));
		plantillaDTO.setObligatorio(rs.getInt("FIOBLIGA"));
		plantillaDTO.setIdPlantilla(rs.getInt("FIID_PLANTILLA"));
		plantillaDTO.setIdArchivo(rs.getInt("FIID_TIPO"));
		
		return plantillaDTO;
}

}
