package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecksOfflineCompletosDTO;

public class ChecksOfflineCompletosRowMapper implements RowMapper<ChecksOfflineCompletosDTO>{

	@Override
	public ChecksOfflineCompletosDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ChecksOfflineCompletosDTO checksOfflineCompletosDTO = new ChecksOfflineCompletosDTO();
		
		checksOfflineCompletosDTO.setIdCheckUsua(rs.getInt("FIID_CHECK_USUA"));
		checksOfflineCompletosDTO.setIdTipoPreg(rs.getInt("FIID_TIPO_PREG"));
		checksOfflineCompletosDTO.setCveTipoPregunta(rs.getString("FCCLAVE_TIPO"));
		checksOfflineCompletosDTO.setDescripcionTipo(rs.getString("FCDESC_TIPO"));
		checksOfflineCompletosDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		checksOfflineCompletosDTO.setOrdepregunta(rs.getInt("FIORDEN_CHECK"));
		checksOfflineCompletosDTO.setPreguntaPadre(rs.getInt("FIID_PREG_PADRE"));
		checksOfflineCompletosDTO.setIdArbolDes(rs.getInt("FIID_ARBOL_DES"));
		checksOfflineCompletosDTO.setPregunta(rs.getString("FCDESCRIPCION"));
		checksOfflineCompletosDTO.setRespuesta(rs.getString("FCRESPUESTA"));
		checksOfflineCompletosDTO.setIdModulo(rs.getInt("FIID_MODULO"));
		checksOfflineCompletosDTO.setNombreModulo(rs.getString("FCNOMBRE"));
		checksOfflineCompletosDTO.setIdModuloPadre(rs.getInt("FIID_MOD_PADRE"));
		checksOfflineCompletosDTO.setModuloPadre(rs.getString("FCMODPADRE"));
		checksOfflineCompletosDTO.setEstatusEvidencia(rs.getInt("FIESTATUS_E"));
		checksOfflineCompletosDTO.setSiguienteOrden(rs.getInt("FCORDEN_RESP"));
		checksOfflineCompletosDTO.setReqAccion(rs.getInt("FIREQACCION"));
		checksOfflineCompletosDTO.setObliga(rs.getInt("FIOBLIGA"));
		checksOfflineCompletosDTO.setReqObs(rs.getInt("FIREQOBS"));
		checksOfflineCompletosDTO.setEtiqueta(rs.getString("FCDESCRIPCION_E"));
                try{
                checksOfflineCompletosDTO.setIdProtocolo(rs.getInt("FIID_PROTOCOLO"));
                }catch(Exception e){
                    
                }
                
		
		return checksOfflineCompletosDTO;
	}
	
	
	
}
