package com.gruposalinas.checklist.business;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.ConsultaSupervisionDAO;
import com.gruposalinas.checklist.domain.ConsultaSupervisionDTO;

public class ConsultaSupervisionBI {
		
	private static Logger logger = LogManager.getLogger(ConsultaSupervisionBI.class);
	
	@Autowired
	ConsultaSupervisionDAO consultaSupervisionDAO;
	
	List<ConsultaSupervisionDTO> listaSucursal = null;
	
	public List<ConsultaSupervisionDTO> buscaSucursal(ConsultaSupervisionDTO bean){
	
		try {
			listaSucursal = consultaSupervisionDAO.obtieneSucursal(bean);
		} catch (Exception e) {
			logger.info("No fue posible obtener la consulta de sucursal: "+e);
		}			
		
		return listaSucursal;
	
	}
}
