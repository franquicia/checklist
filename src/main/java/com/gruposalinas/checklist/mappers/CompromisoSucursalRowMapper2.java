package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.CompromisoDTO;

public class CompromisoSucursalRowMapper2  implements RowMapper<CompromisoDTO>  {

	public CompromisoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		CompromisoDTO compromisoDTO = new CompromisoDTO();
		
		compromisoDTO.setIdCompromiso(rs.getInt("FIID_COMPROMISO"));
		compromisoDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		compromisoDTO.setIdUsuario(rs.getString("FIID_USUARIO"));
		compromisoDTO.setNombreChecklist(rs.getString("NOMBRECHECK"));
		compromisoDTO.setIdCeco(rs.getInt("FCID_CECO"));
		compromisoDTO.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
		compromisoDTO.setEconomico(rs.getString("ECONOMICO"));
		compromisoDTO.setEstatus(rs.getInt("FIESTATUS"));
		compromisoDTO.setIdTipoPreg(rs.getInt("FIID_TIPO_PREG"));
		compromisoDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		compromisoDTO.setDesPregunta(rs.getString("PREGUNTA"));
		compromisoDTO.setIdResponsable(rs.getInt("FIID_RESPONSABLE"));
		compromisoDTO.setNombreUsuario(rs.getString("FCNOMBRE"));
		compromisoDTO.setIdRespuesta(rs.getInt("FIID_RESPUESTA"));
		compromisoDTO.setIdArbol(rs.getInt("FIID_ARBOL_DES"));
		compromisoDTO.setDesRespuesta(rs.getString("FCRESPUESTA"));
		compromisoDTO.setDescripcion(rs.getString("COMPROMISO"));
		compromisoDTO.setFechaInicio(rs.getString("FECHAINI"));
		compromisoDTO.setFechaCompromiso(rs.getString("FECHAFIN"));

		return compromisoDTO;
	}
}
