package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.RecursoPerfilDAO;
import com.gruposalinas.checklist.domain.RecursoPerfilDTO;
import com.gruposalinas.checklist.util.UtilFRQ;


public class RecursoPerfilBI {

	private static Logger logger = LogManager.getLogger(RecursoPerfilBI.class);

	@Autowired
	RecursoPerfilDAO recursoPerfilDAO;
	
	List<RecursoPerfilDTO> listaRecursoPerfil = null;
	

	public List<RecursoPerfilDTO> buscaRecursoP(String idRecurso, String idPerfil) throws Exception {

		if (idRecurso.equals("NULL"))
			idRecurso = null;
		if (idPerfil.equals("NULL"))
			idPerfil = null;

		listaRecursoPerfil = recursoPerfilDAO.buscaRecursoP(idRecurso, idPerfil);

		return listaRecursoPerfil;
	}
	
	public boolean insertaRecursoP(RecursoPerfilDTO bean){
		
		boolean res = false;
		
		try {
			res = recursoPerfilDAO.insertaRecursoP(bean);	
		} catch (Exception e) {
			logger.info("No fue posible insertar el Recurso Perfil");
		}
		
		return res;
		
	}
	
	public boolean actualizaRecursoP(RecursoPerfilDTO bean) {
		
		boolean respuesta = false ;
		
		try {
			respuesta = recursoPerfilDAO.actualizaRecursoP(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar el Recurso");
		}
		
		return respuesta;
	}
	
	public boolean eliminaRecursoP(int idRecursoP, int idPerfil) {
		
		boolean respuesta = false ;
		
		try {
			respuesta = recursoPerfilDAO.eliminaRecursoP(idRecursoP, idPerfil);
		} catch (Exception e) {
			logger.info("No fue posible insertar el Recurso");
		}
		
		return respuesta;		
	}
	
	public RecursoPerfilDTO obtienePermisos(int idRecurso, int idUsuario){
		
		RecursoPerfilDTO recursoPerfilDTO = null;		
		try {
			if(recursoPerfilDAO.obtienePermisos(idRecurso, idUsuario).size()>0)
				recursoPerfilDTO=recursoPerfilDAO.obtienePermisos(idRecurso, idUsuario).get(0);
			else
				recursoPerfilDTO=null;
		} catch (Exception e) {
			logger.info("No fue posible obtener los permisos del usuario");
					
		}
		return recursoPerfilDTO;	
	}
}
