package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;

public class ChecklistPreguntaTRowMapper implements RowMapper<ChecklistPreguntaDTO>  {

	public ChecklistPreguntaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ChecklistPreguntaDTO checklistPreguntaDTO = new ChecklistPreguntaDTO();
		
		checklistPreguntaDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		checklistPreguntaDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		checklistPreguntaDTO.setOrdenPregunta(rs.getInt("FIORDEN_CHECK"));
		checklistPreguntaDTO.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
		checklistPreguntaDTO.setNumVersion(rs.getString("FINUMREVISION"));
		checklistPreguntaDTO.setTipoCambio(rs.getString("FCTIPOMODIF"));
		
		return checklistPreguntaDTO;
	}
	
}
