package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistNegocioDTO;

public class ChecklistNegocioRowMapper implements RowMapper<ChecklistNegocioDTO> {

	@Override
	public ChecklistNegocioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
	
		ChecklistNegocioDTO checklistNegocioDTO = new ChecklistNegocioDTO();
		
		checklistNegocioDTO.setChecklist(rs.getInt("FIID_CHECKLIST"));
		checklistNegocioDTO.setNegocio(rs.getInt("FIID_NEGOCIO"));
		
		return checklistNegocioDTO;
	}

}
