<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="../js/jquery/jquery-2.2.4.min.js"></script>
<script src="../css/jQuery/jQuery.js"></script>
<script src="../js/adminBD/checkpreg.js"></script>
<script src="../js/adminBD/arbolDes.js"></script>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Response</title>

<style>
table, th, td {
	
	border: 1px solid black;

}

th {
    background-color: #4CAF50;
    color: white;
}
</style>

</head>
<body>
	<!-- MODULO -->
	<c:choose>
	
		<c:when test="${tipo == 'LISTA CANALES'}">
			<table>
				<thead> 
				<tr> 
				   <th>ID CANAL</th>
				   <th>DESCRIPCION</th>
				   <th>ACTIVO</th>
				</tr>				 
				</thead>
				<tbody>
			    	<c:forEach items="${res}" var="item">				
						<tr>
				   			<td>${item.idCanal}</td>
				   			<td>${item.descrpicion}</td>
				   			<td>${item.activo}</td>
						</tr>
					</c:forEach>
				</tbody>			
			</table>			
		</c:when>
		
		<c:when test="${tipo == 'VERSION-CHECKLIST-PROTOCOLO'}">
			<table>
				<thead> 
				<tr> 
				   <th>ID PROTOCOLO</th>
				   <th>ID CHECKLIST</th>
				   <th>FECHA INICIO</th>
				   <th>VIGENTE</th>
				</tr>				 
				</thead>
				<tbody>
			    	<c:forEach items="${res}" var="item">				
						<tr>
				   			<td>${item.idProtocolo}</td>
				   			<td>${item.idChecklist}</td>
				   			<td>${item.fecha}</td>
				   			<td>${item.vigente}</td>
						</tr>
					</c:forEach>
				</tbody>			
			</table>			
		</c:when>
		
		<c:when test="${tipo == 'LISTA PERFILES'}">			
			<table>
				<thead>
					<tr>
					  <th>ID PERFIL</th>
					  <th>DESCRIPCION</th>
					</tr>			
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idPerfil} </td>
							<td>${item.descripcion}</td>
						</tr>
	  				</c:forEach>
				</tbody>
			</table>			
		</c:when>
		<c:when test="${tipo == 'PUESTOS'}">
		
			<table>
				<thead>
					<tr> 
						<th>ID PUESTO</th>
						<th>ID NIVEL</th>
						<th>ID NEGOCIO</th>
						<th>ID TIPO PUESTO</th>
						<th>ID CANAL</th>
						<th>CODIGO</th>
						<th>DESCRIPCION</th>
						<th>ID SUB NEGOCIO</th>
						<th>ACTIVO</th>					
					</tr>
				</thead>	
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idPuesto}</td>
							<td>${item.idNivel}</td>
							<td>${item.idNegocio}</td>
							<td>${item.idTipoPuesto}</td>
							<td>${item.idCanal}</td>
							<td>${item.codigo}</td>
							<td>${item.descripcion}</td>
							<td>${item.idSubnegocio}</td>
							<td>${item.activo}</td>
							<td></td>
						</tr>
					</c:forEach>				
				</tbody>
			</table>
			
		</c:when>
		<c:when test="${tipo == 'LISTA TIPOS ARCHIVO'}">
			<c:forEach items="${res}" var="item">
				ID TIPO ARCHIVO:${item.idTipoArchivo} <br />
				NOMBRE TIPO: ${item.nombreTipo}<br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA CECOS'}">
			<c:forEach items="${res}" var="item">
				ID CECO: ${item.idCeco} <br />
				IP PAIS: ${item.idPais} <br />
				ID NEGOCIO: ${item.idNegocio} <br />
				ID CANAL: ${item.idCanal} <br />
				ID ESTADO: ${item.idEstado} <br />
				DESC CECO: ${item.descCeco} <br />
				ID CECO SUPERIOR${item.idCecoSuperior} <br />
				ACTIVO: ${item.activo} <br />
				ID NIVEL: ${item.idNivel} <br />
				CALLE: ${item.calle} <br />
				CIUDAD: ${item.ciudad} <br />
				CP: ${item.cp} <br />
				NOMBRE CONTACTO: ${item.nombreContacto} <br />
				PUESTO CONTACTO: ${item.puestoContacto} <br />
				TELEFONO CONTACTO: ${item.telefonoContacto} <br />
				FAX CONTACTO: ${item.faxContacto}	<br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA SUCURSALES'}">
			<c:forEach items="${res}" var="item">
				ID SUCURSAL: ${item.idSucursal} <br />
				ID PAIS: ${item.idPais} <br />
				ID CANAL: ${item.idCanal} <br />
				NU SUCURSAL: ${item.nuSucursal} <br />
				NOMBRE SUC: ${item.nombresuc} <br />
				LATITUD: ${item.latitud} <br />
				LONGITUD: ${item.longitud} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA USUARIOS A'}">
			<c:forEach items="${res}" var="item">
				ID USUARIO: ${item.idUsuario} <br />
				ID PUESTO: ${item.idPuesto} <br />
				ID CECO: ${item.idCeco} <br />
				ID NOMBRE: ${item.nombre} <br />
				ID ACTIVO: ${item.activo} <br />
				ID FECHA: ${item.fecha} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA PAISES'}">
			<c:forEach items="${res}" var="item">
				ID PAIS: ${item.idPais} <br />
				NOMBRE: ${item.nombre} <br />
				ACTIVO: ${item.activo} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA HORARIOS'}">

			<c:forEach items="${res}" var="item">
				ID HORARIO: ${item.idHorario} <br />
				CVE HORARIO: ${item.cveHorario} <br />
				VALOR FIN: ${item.valorFin} <br />
				VALOR INI: ${item.valorIni} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA PARAMETROS'}">
			<c:forEach items="${res}" var="item">
				CLAVE: ${item.clave} <br />
				VALOR: ${item.valor} <br />
				ACTIVO: ${item.activo} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>


		<c:when test="${tipo == 'LISTA ESTADO CHECKLIST'}">
			<c:forEach items="${res}" var="item">
				ID EDO CHECKLIST: ${item.idEdochecklist} <br />
				DESCRIPCION: ${item.descripcion} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA TIPOS CHECKLIST'}">
			<c:forEach items="${res}" var="item">
				ID TIPO CHECKLIST: ${item.idTipoCheck} <br />
				DESCRIPCION TIPO: ${item.descTipo} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA TIPOS PREGUNTA'}">
			<c:forEach items="${res}" var="item">
				ID TIPO PREGUNTA: ${item.idTipoPregunta} <br />
				CVE PREGUNTA: ${item.cvePregunta} <br />
				DESCRICPION: ${item.descripcion} <br />
				ACTIVO: ${item.activo} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA NEGOCIOS'}">
			<c:forEach items="${res}" var="item">
				ID NEGOCIO: ${item.idNegocio} <br />
				DESCRIPCION: ${item.descripcion} <br />
				ACTIVO: ${item.activo} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA BITACORA'}">
			<c:forEach items="${res}" var="item">
				ID BITACORA: ${item.idBitacora} <br />
				ID CHECK USUA: ${item.idCheckUsua} <br />
				LONGITUD: ${item.longitud} <br />
				LATITUD: ${item.latitud} <br />
				FECHA INICIO: ${item.fechaInicio} <br />
				FECHA FIN: ${item.fechaFin} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA RESPUESTAS'}">
			<c:forEach items="${res}" var="item">
				ID RESPUESTA: ${item.idRespuesta} <br />
				ID BITACORA: ${item.idBitacora}<br />
				ID ARBOL: ${item.idArboldecision} <br />
				OBSERVACIONES: ${item.observacion} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA COMPROMISOS'}">
			<c:forEach items="${res}" var="item">
				ID COMPROMISO: ${item.idCompromiso} <br />
				ID RESPUESTA: ${item.idRespuesta} <br />
				DESRCRICPION: ${item.descripcion}<br />
				ESTATUS: ${item.estatus} <br />
				FECHA COMPROMISO: ${item.fechaCompromiso} <br />
				ID RESPONSABLE: ${item.idResponsable} <br />
				COMMIT: ${item.commit} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA EVIDENCIAS'}">
			<c:forEach items="${res}" var="item">
				ID EVIDENCIA: ${item.idEvidencia} <br />
				ID RESPUESTA: ${item.idRespuesta} <br />
				ID TIPO: ${item.idTipo}<br />
				RUTA: ${item.ruta} <br />
				COMMIT: ${item.commit} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA NIVELES'}">
			<c:forEach items="${res}" var="item">
				ID NIVEL: ${item.idNivel} <br />
				ID NEGOCIO: ${item.idNegocio} <br />
				CODIGO: ${item.codigo}<br />
				DESCRIPCION: ${item.descripcion} <br />
				ACTIVO: ${item.activo} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'ARBOL DESICION'}">
		
		<table  style="position:relative;  width:50%; ">
		  <thead>
		     <tr>
		        <th>ID ARBOL DESICION</th>
		        <th>ID CHECKLIST</th>
		        <th>ID PREGUNTA</th>
		        <th>RESPUESTA</th>
		        <th>EVIDENCIA</th>
		        <th>SIGUIENTE PREGUNTA</th>
		        <th>ACCION</th>
		        <th>OBSERVACIONES</th>
		        <th>EVIDENCIA OBLIGATORIA</th>
		        <th>ETIQUETA EVIDENCIA</th>
		        <th>Editar</th>
		     </tr>
		  </thead>
		  <tbody>
		     <c:forEach items="${res}" var="item">
				<tr align="center">				
				  <td>${item.idArbolDesicion}</td>
				  <td>${item.idCheckList} </td>
				  <td>${item.idPregunta}</td>
				  <td>${item.respuesta} </td>
				  <td>${item.estatusEvidencia} </td>
				  <td>${item.ordenCheckRespuesta} </td>				  
				  <td>${item.reqAccion}</td>
				  <td>${item.reqObservacion}</td>
				  <td>${item.reqOblig}</td>
				  <td>${item.descEvidencia} </td>
				  <td align="center"><input type="checkbox" id="${item.idArbolDesicion}" value="Editar" onclick="transformarEnEditable(this,${item.idArbolDesicion})" /> </td>
				
				</tr>
			</c:forEach>		  
		        
		  </tbody>		   
		</table>

	    <div id="contenedorForm" style="position:relative; display:inline-block; margin-top: 30px" > </div>
		</c:when>
		<c:when test="${tipo == 'GET CHECKLIST PREGUNTA'}">
			<table>
				<thead>
					<tr>
						<th>ID CHECKLIST</th>
						<th>ID PREGUNTA</th>
						<th>ORDEN PREGUNTA</th>
						<th>PREGUNTA PADRE</th>
						<th>EDITAR</th>
						<th>ID ZONA</th>
					</tr>					
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idChecklist}</td>
							<td>${item.idPregunta}</td>
							<td>${item.ordenPregunta}</td>
							<td> ${item.pregPadre}</td>
							 <td align="center"><input type="checkbox" id="${item.idPregunta}" value="Editar" onclick="transformarEnEditableCP(this,${item.idPregunta})" /> </td>
							 
							<td> ${item.idZona}</td>
						</tr>			
					</c:forEach>
				</tbody>			
			</table>
		</c:when>
		
		<c:when test="${tipo == 'GET CHECK COMPLETO'}">
			<table  style="position:relative; width: 80%;">
				<thead>
					<tr>
						<th>ID CHECK USUARIO</th>
						<th>NOMBRE USUARIO</th>
						<th>ID BITACORA</th>
						<th>FECHA INICIO</th>
						<th>FECHA FIN</th>
						<th>FECHA MODIFICACION</th>
						<th>ID CECO</th>
						<th>NOMBRE CECO</th>
						<th>ID RESPUESTAS</th>
						<th>ID COMPROMISOS</th>
						<th>ID RESPUESTAS AD</th>
					</tr>					
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idCheckUsuario}</td>
							<td>${item.nombreU}</td>
							<td>${item.idBitacora}</td>
							<td>${item.fechaIni}</td>
							<td>${item.fechaFin}</td>
							<td>${item.fechaModificacion}</td>
							<td>${item.idCeco}</td>
							<td> ${item.nombreCeco}</td>
							<td> ${item.respuestas}</td>
							<td> ${item.compromisos}</td>
							<td> ${item.respuestas_Ad}</td>
						</tr>			
					</c:forEach>
				</tbody>			
			</table>
		</c:when>
		
		
		<c:when test="${tipo == 'LISTA GEO'}">
			<table  style="position:relative; width: 80%;">
				<thead>
					<tr>
						<th>ID CECO</th>
						<th>ID REGION</th>
						<th>REGION</th>
						<th>ID ZONA</th>
						<th>ZONA</th>
						<th>ID TERRITORIO</th>
						<th>TERRITORIO</th>
					</tr>					
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idCeco}</td>
							<td>${item.idRegion}</td>
							<td>${item.region}</td>
							<td>${item.idZona}</td>
							<td>${item.zona}</td>
							<td>${item.idTerritorio}</td>
							<td>${item.territorio}</td>
						</tr>			
					</c:forEach>
				</tbody>			
			</table>
		</c:when>
		
		<c:when test="${tipo == 'BITACORA CERRADA'}">
			<table  style="position:relative; width: 80%;">
				<thead>
					<tr>
						<th>ID CHECK USUARIO</th>
						<th>ID USUARIO</th>
						<th>NOMBRE USUARIO</th>
						<th>ID BITACORA</th>
						<th>FECHA INICIO</th>
						<th>FECHA FIN</th>
					</tr>					
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idCheckUsua}</td>
							<td>${item.idUsuario}</td>
							<td>${item.usuario}</td>
							<td>${item.idBitacora}</td>
							<td>${item.fechaInicio}</td>
							<td>${item.fechaFin}</td>
						</tr>			
					</c:forEach>
				</tbody>			
			</table>
		</c:when>
		
		<c:when test="${tipo == 'GET CHECKLIST USUARIO'}">
			<c:forEach items="${res}" var="item">
				ID CHECKLIST USUARIO: ${item.idCheckUsuario} <br />
				ID CHECKLIST: ${item.idChecklist} <br />
				ID USUARIO: ${item.idUsuario} <br />
				ID CECO: ${item.idCeco} <br />
				ID ACTIVO: ${item.activo} <br />
				ID FECHA INI: ${item.fechaIni} <br />
				ID FECHA RESP: ${item.fechaResp} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'GET CHECKLIST'}">
			<c:forEach items="${res}" var="item">
				ID TIPO CHECKLIST: ${item.idTipoChecklist.idTipoCheck} <br />
				ID CHECKLIST: ${item.idChecklist} <br />
				NOMBRE CHECK: ${item.nombreCheck} <br />
				FECHA INI: ${item.fecha_inicio} <br />
				FECHA FIN: ${item.fecha_fin} <br />
				ID HORARIO: ${item.idHorario} <br />
				VIGENTE: ${item.vigente} <br />
				ID ESTADO: ${item.idEstado} <br />
				ID USUARIO: ${item.idUsuario} <br />
				DIA: ${item.dia} <br />
				PERIODO: ${item.periodicidad} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'GET MODULOS'}">
			<c:forEach items="${res}" var="item">
				ID MODULO: ${item.idModulo} <br />
				NOMBRE: ${item.nombre} <br />
				ID MODULO PADRE: ${item.idModuloPadre} <br />
				NOMBRE PADRE: ${item.nombrePadre} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'GET PREGUNTAS'}">
			<c:forEach items="${res}" var="item">
				ID PREGUNTA: ${item.idPregunta} <br />
				ID MODULO: ${item.idModulo} <br />
				ID TIPO: ${item.idTipo} <br />
				ESTATUS: ${item.estatus} <br />
				PREGUNTA: ${item.pregunta} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'REPORTE'}">
			<table id="reporte" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID CHECKLIST</th>
						<th>ID USUARIO</th>
						<th>NOMBRE USUARIO</th>
						<th>NOMBRE SUCURSAL</th>
						<th>NOMBRE CECO</th>
						<th>FECHA RESPUESTA</th>
						<th>SEMANA</th>
						<th>NOMBRE MODULO PADRE</th>
						<th>NOMBRE MODULO</th>
						<th>ID PREGUNTA</th>
						<th>DESCRIPCION PREGUNTA</th>
						<th>RESPUESTA</th>
						<th>NOMBRE CHECKLIST</th>
						<th>METODO CHECKLIST</th>
					</tr>
				</thead>

				</tbody>
				<c:forEach items="${res}" var="item">
					<tr>
						<td>${item.idChecklist}</td>
						<td>${item.idUsuario}</td>
						<td>${item.nombreUsuario}</td>
						<td>${item.nombreSucursal}</td>
						<td>${item.nombreCeco}<br />
						<td>${item.fechaRespuesta}<br />
						<td>${item.semana}</td>
						<td>${item.nombreModuloP}</td>
						<td>${item.nombreMod}</td>
						<td>${item.idPregunta}</td>
						<td>${item.desPregunta}</td>
						<td>${item.respuesta}</td>
						<td>${item.nombreChecklist}</td>
						<td>${item.metodoChecklist}</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</c:when>
		<c:when test="${tipo == 'LISTA TOKENS'}">
			<c:forEach items="${res}" var="item">
				ID TOKEN: ${item.idToken} <br />
				ID USUARIO: ${item.usuario} <br />
				TOKEN: ${item.token} <br />
				ESTATUS: ${item.estatus}<br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA LLAVES'}">
			<c:forEach items="${res}" var="item">
				ID LLAVE: ${item.idLlave} <br />
				LLAVE: ${item.llave} <br />
				DESCRIPCION: ${item.descripcion} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LLAVE'}">
			<c:forEach items="${res}" var="item">
				ID LLAVE: ${item.idLlave} <br />
				LLAVE: ${item.llave} <br />
				DESCRIPCION: ${item.descripcion} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'GET CHECKLIST-USUARIO POR ID USUARIO'}">
			<c:forEach items="${res}" var="item">
				ID CHECKLIST USUARIO: ${item.idCheckUsuario} <br />
				ID CHECKLIST: ${item.idChecklist} <br />
				ID USUARIO: ${item.idUsuario} <br />
				ID CECO: ${item.idCeco} <br />
				ID ACTIVO: ${item.activo} <br />
				ID FECHA INI: ${item.fechaIni} <br />
				ID FECHA RESP: ${item.fechaResp} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'SUCURSAL'}">
			<c:forEach items="${res}" var="item">
				ID SUCURSAL: ${item.idSucursal} <br />
				ID PAIS: ${item.idPais} <br />
				ID CANAL: ${item.idCanal} <br />
				NU SUCURSAL: ${item.nuSucursal} <br />
				NOMBRE SUC: ${item.nombresuc} <br />
				LATITUD: ${item.latitud} <br />
				LONGITUD: ${item.longitud} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		
		<c:when test="${tipo == 'CECO'}">
			<c:forEach items="${res}" var="item">
				ID CECO: ${item.idCeco} <br />
				IP PAIS: ${item.idPais} <br />
				ID NEGOCIO: ${item.idNegocio} <br />
				ID CANAL: ${item.idCanal} <br />
				ID ESTADO: ${item.idEstado} <br />
				DESC CECO: ${item.descCeco} <br />
				ID CECO SUPERIOR${item.idCecoSuperior} <br />
				ACTIVO: ${item.activo} <br />
				ID NIVEL: ${item.idNivel} <br />
				CALLE: ${item.calle} <br />
				CIUDAD: ${item.ciudad} <br />
				CP: ${item.cp} <br />
				NOMBRE CONTACTO: ${item.nombreContacto} <br />
				PUESTO CONTACTO: ${item.puestoContacto} <br />
				TELEFONO CONTACTO: ${item.telefonoContacto} <br />
				FAX CONTACTO: ${item.faxContacto}	<br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'USUARIO'}">
			<c:forEach items="${res}" var="item">
				ID USUARIO: ${item.idUsuario} <br />
				ID PUESTO: ${item.idPuesto} <br />
				ID PERFIL: ${item.idPerfil} <br />
				ID CECO: ${item.idCeco} <br />
				ID NOMBRE: ${item.nombre} <br />
				ID ACTIVO: ${item.activo} <br />
				ID FECHA: ${item.fecha} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		
		<c:when test="${tipo == 'ERRORES'}">
			<c:forEach items="${res}" var="item">
				ID LOG: ${item.idLog} <br />
				FECHA ERROR: ${item.fechaError} <br />
				CODIGO ERROR: ${item.codigoError} <br />
				MENSAJE ERROR: ${item.mensajeError} <br />
				ORIGEN ERROR: ${item.origenError}<br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		
		<c:when test="${tipo == 'USUARIOS PASO'}">
			<table>
				<thead>
					<tr>
						<th>ID USUARIO</th>
						<th>ID PUESTO</th>
						<th>ID PERFIL</th>
						<th>ID CECO</th>
						<th>ID NOMBRE</th>
						<th>ID ACTIVO</th>
						<th>ID FECHA</th>
						<th>PUESTO F</th>
						<th>DESC PUESTO</th>
						<th>DESC PUESTO F</th>
						<th>PAIS</th>
						<th>FECHA BAJA</th>
						<th>ID EMPLEADO REP</th>
						<th>CC EMPLEADO REP</th>
					</tr>
				</thead>
				<c:forEach items="${res}" var="item">
					<tr>
						<td>${item.idUsuario}</td>
						<td>${item.idPuesto}</td>
						<td>${item.idPerfil}</td>
						<td>${item.idCeco}</td>
						<td>${item.nombre}</td>
						<td>${item.activo}</td>
						<td>${item.fecha}</td>
						<td>${item.puestof}</td>
						<td>${item.descPuesto}</td>
						<td>${item.desPuestof}</td>
						<td>PAIS: ${item.pais}</td>
						<td>${item.fechaBaja}</td>
						<td>${item.idEmpleadoRep}</td>
						<td>${item.ccEmpleadoRep}</td>
					</tr>
				</c:forEach>
			</table>
		</c:when>
		
		
		
		<c:when test="${tipo == 'LISTA MOVIL'}">
			<table>
				<thead>
					<tr>
						<th>ID MOVIL</th>
						<th>ID USUARIO</th>
						<th>FECHA</th>
						<th>SO</th>
						<th>VERSION</th>
						<th>VERSION APP</th>
						<th>MODELO</th>
						<th>FABRICANTE</th>
						<th>NUMERO MOVIL</th>
						<th>TIPO CONEXION</th>
						<th>IDENTIFICADOR</th>
						<th>TOKEN</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idMovil}</td>
							<td>${item.idUsuario}</td>
							<td>${item.fecha}</td>
							<td>${item.so}</td>
							<td>${item.version}</td>
							<td>${item.versionApp}</td>
							<td>${item.modelo}</td>
							<td>${item.fabricante}</td>
							<td>${item.numMovil}</td>
							<td>${item.tipoCon}</td>
							<td>${item.identificador}</td>
							<td>${item.token}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:when>
		

		<c:when test="${tipo == 'USUARIOS PASO PARAMS'}">
			<table>
				<thead>
					<tr>
						<th>ID USUARIO</th>
						<th>ID PUESTO</th>
						<th>ID PERFIL</th>
						<th>ID CECO</th>
						<th>ID NOMBRE</th>
						<th>ID ACTIVO</th>
						<th>ID FECHA</th>
						<th>PUESTO F</th>
						<th>DESC PUESTO</th>
						<th>DESC PUESTO F</th>
						<th>PAIS</th>
						<th>FECHA BAJA</th>
						<th>ID EMPLEADO REP</th>
						<th>CC EMPLEADO REP</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idUsuario}</td>
							<td>${item.idPuesto}</td>
							<td>${item.idPerfil}</td>
							<td>${item.idCeco}</td>
							<td>${item.nombre}</td>
							<td>${item.activo}</td>
							<td>${item.fecha}</td>
							<td>${item.puestof}</td>
							<td>${item.descPuesto}</td>
							<td>${item.desPuestof}</td>
							<td>PAIS: ${item.pais}</td>
							<td>${item.fechaBaja}</td>
							<td>${item.idEmpleadoRep}</td>
							<td>${item.ccEmpleadoRep}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:when>

		<c:when test="${tipo == 'CECO PASO'}">
			<table>
				<thead>
					<tr>
						<th>ID CECO</th>
						<th>ID PAIS</th>
						<th>ID NEGOCIO</th>
						<th>ID CANAL</th>
						<th>ID ESTADO</th>
						<th>DESC CECO</th>
						<th>ID CECO SUPERIOR</th>
						<th>ACTIVO</th>
						<th>ID NIVEL</th>
						<th>CALLE</th>
						<th>CIUDAD</th>
						<th>CP</th>
						<th>NOMBRE CONTACTO</th>
						<th>TELEFONO CONTACTO</th>
						<th>ID ENTIDAD</th>
						<th>NUMERO ECONOMICO</th>
						<th>NOMBRE ENTIDAD</th>
						<th>ENTIDAD PADRE</th>
						<th>NOMBRE CECO PADRE</th>
						<th>TIPO CANAL</th>
						<th>NOMBRE TIPO CANAL</th>
						<th>ACTIVO</th>
						<th>TIPO CECO</th>
						<th>TIPO OPERACION</th>
						<th>TIPO SUCURSAL</th>
						<th>NOMBRE TIPO SUCURSAL</th>
						<th>ID RESPONSABLE</th>
						<th>ID SIE</th>
						<th>CLASIFICACION SIE</th>
						<th>GASTO NEGOCIO</th>
						<th>GASTO X NEGOCIO</th>
						<th>FECHA APERTURA</th>
						<th>FECHA CIERRE</th>
						<th>PAIS</th>
						<th>FECHA CARGA</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idCeco}</td>
							<td>${item.idPais}</td>
							<td>${item.idNegocio}</td>
							<td>${item.idCanal}</td>
							<td>${item.idEstado}</td>
							<td>${item.descCeco}</td>
							<td>${item.idCCSuperior}</td>
							<td>${item.activo}</td>
							<td>${item.idNivel}</td>
							<td>${item.calle}</td>
							<td>${item.ciudad}</td>
							<td>${item.cp}</td>
							<td>${item.nombreContacto}</td>
							<td>${item.telefonoContacto}</td>
							<td>${item.entidadid}</td>
							<td>${item.numEconomico}</td>
							<td>${item.nombreEnt}</td>
							<td>${item.entidadPadre}</td>
							<td>${item.nombrePadre}</td>
							<td>${item.desCanal}</td>
							<td>${item.nomTipoCanal}</td>
							<td>${item.estatusCC}</td>
							<td>${item.tipoCC}</td>
							<td>${item.tipoOperacion}</td>
							<td>${item.tipoSucursal}</td>
							<td>${item.nomTipoSucursal}</td>
							<td>${item.idResponsable}</td>
							<td>${item.idSie}</td>
							<td>${item.clasfSie}</td>
							<td>${item.gastoNegocio}</td>
							<td>${item.gastoxNegocio}</td>
							<td>${item.fechaApertura}</td>
							<td>${item.fechaCierre}</td>
							<td>${item.desPais}</td>
							<td>${item.fechaCarga}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:when>

		<c:when test="${tipo == 'CECO PASO PARAMS'}">
			<table>
				<thead>
					<tr>
						<th>ID CECO</th>
						<th>ID PAIS</th>
						<th>ID NEGOCIO</th>
						<th>ID CANAL</th>
						<th>ID ESTADO</th>
						<th>DESC CECO</th>
						<th>ID CECO SUPERIOR</th>
						<th>ACTIVO</th>
						<th>ID NIVEL</th>
						<th>CALLE</th>
						<th>CIUDAD</th>
						<th>CP</th>
						<th>NOMBRE CONTACTO</th>
						<th>TELEFONO CONTACTO</th>
						<th>ID ENTIDAD</th>
						<th>NUMERO ECONOMICO</th>
						<th>NOMBRE ENTIDAD</th>
						<th>ENTIDAD PADRE</th>
						<th>NOMBRE CECO PADRE</th>
						<th>TIPO CANAL</th>
						<th>NOMBRE TIPO CANAL</th>
						<th>ACTIVO</th>
						<th>TIPO CECO</th>
						<th>TIPO OPERACION</th>
						<th>TIPO SUCURSAL</th>
						<th>NOMBRE TIPO SUCURSAL</th>
						<th>ID RESPONSABLE</th>
						<th>ID SIE</th>
						<th>CLASIFICACION SIE</th>
						<th>GASTO NEGOCIO</th>
						<th>GASTO X NEGOCIO</th>
						<th>FECHA APERTURA</th>
						<th>FECHA CIERRE</th>
						<th>PAIS</th>
						<th>FECHA CARGA</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach items="${res}" var="item">
					<tr>
						<td>${item.idCeco}</td>
							<td>${item.idPais}</td>
							<td>${item.idNegocio}</td>
							<td>${item.idCanal}</td>
							<td>${item.idEstado}</td>
							<td>${item.descCeco}</td>
							<td>${item.idCCSuperior}</td>
							<td>${item.activo}</td>
							<td>${item.idNivel}</td>
							<td>${item.calle}</td>
							<td>${item.ciudad}</td>
							<td>${item.cp}</td>
							<td>${item.nombreContacto}</td>
							<td>${item.telefonoContacto}</td>
							<td>${item.entidadid}</td>
							<td>${item.numEconomico}</td>
							<td>${item.nombreEnt}</td>
							<td>${item.entidadPadre}</td>
							<td>${item.nombrePadre}</td>
							<td>${item.desCanal}</td>
							<td>${item.nomTipoCanal}</td>
							<td>${item.estatusCC}</td>
							<td>${item.tipoCC}</td>
							<td>${item.tipoOperacion}</td>
							<td>${item.tipoSucursal}</td>
							<td>${item.nomTipoSucursal}</td>
							<td>${item.idResponsable}</td>
							<td>${item.idSie}</td>
							<td>${item.clasfSie}</td>
							<td>${item.gastoNegocio}</td>
							<td>${item.gastoxNegocio}</td>
							<td>${item.fechaApertura}</td>
							<td>${item.fechaCierre}</td>
							<td>${item.desPais}</td>
							<td>${item.fechaCarga}</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</c:when>
		
		<c:when test="${tipo == 'CARGA CECOS GUATEMALA'}">
				EJECUCION: ${res[0]} <br />
				REGISTROS AFECTADOS SF: ${res[1]} <br />
				EJECUCION: ${res[2]} <br />
				REGISTROS AFECTADOS COMERCIO: ${res[3]} <br />
		</c:when>
		
		<c:when test="${tipo == 'CARGA CECOS PERU'}">
				EJECUCION: ${res[0]} <br />
				REGISTROS AFECTADOS SF: ${res[1]} <br />
				EJECUCION: ${res[2]} <br />
				REGISTROS AFECTADOS COMERCIO: ${res[3]} <br />
		</c:when>
		
		<c:when test="${tipo == 'CARGA CECOS HONDURAS'}">
				EJECUCION: ${res[0]} <br />
				REGISTROS AFECTADOS SF: ${res[1]} <br />
				EJECUCION: ${res[2]} <br />
				REGISTROS AFECTADOS COMERCIO: ${res[3]} <br />
		</c:when>
		
		<c:when test="${tipo == 'CARGA CECOS PANAMA'}">
				EJECUCION: ${res[0]} <br />
				REGISTROS AFECTADOS SF: ${res[1]} <br />
				
		</c:when>
		
		<c:when test="${tipo == 'CARGA CECOS SALVADOR'}">
				EJECUCION: ${res[0]} <br />
				REGISTROS AFECTADOS SF: ${res[1]} <br />
				
		</c:when>
		
		<c:when test="${tipo == 'CARGA CECOS LAM'}">
				PAIS: GUATEMALA <br />
				EJECUCION SF: ${res.get("ejecucionSFGuatemala")} <br />
				REGISTROS AFECTADOS SF: ${res.get("registrosSFGuatemala")} <br />
				EJECUCION COMERCIO: ${res.get("ejecucionCOMGuatemala")} <br />
				REGISTROS AFECTADOS COMERCIO: ${res.get("registrosCOMGuatemala")} <br />
				<br />
				PAIS: PERU <br />
				EJECUCION SF: ${res.get("ejecucionSFPeru")} <br />
				REGISTROS AFECTADOS SF: ${res.get("registrosSFPeru")} <br />
				EJECUCION COMERCIO: ${res.get("ejecucionCOMPeru")} <br />
				REGISTROS AFECTADOS COMERCIO: ${res.get("registrosCOMPeru")} <br />
				<br />
				PAIS: HONDURAS <br />
				EJECUCION SF: ${res.get("ejecucionSFHonduras")} <br />
				REGISTROS AFECTADOS SF: ${res.get("registrosSFHonduras")} <br />
				EJECUCION COMERCIO: ${res.get("ejecucionCOMHonduras")} <br />
				REGISTROS AFECTADOS COMERCIO: ${res.get("registrosCOMHonduras")} <br />
				<br />
				PAIS: PANAMA <br />
				EJECUCION SF: ${res.get("ejecucionSFPanama")} <br />
				REGISTROS AFECTADOS SF: ${res.get("registrosSFPanama")} <br />
				<br />
				PAIS: EL SALVADOR <br />
				EJECUCION SF: ${res.get("ejecucionSFSalvador")} <br />
				REGISTROS AFECTADOS SF: ${res.get("registrosSFSalvador")} <br />
		</c:when>
		
		<c:when test="${tipo == 'CARGA PERFILES'}">
				EJECUCION: ${res[0]} <br />
				REGISTROS AFECTADOS : ${res[1]} <br />				
		</c:when>		
		
		<c:when test="${tipo == 'CECOS TRABAJO'}">
				COLUMNAS AFECTADAS: ${res} <br />
		</c:when>
		
		<c:when test="${tipo == 'PERFILES USUARIO'}">
			<table align="left">
				<thead>
					<tr>
						<th>ID USUARIO</th>
						<th>ID PERFIL</th>												
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idUsuario}</td>
							<td>${item.idPerfil}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:when>
		
		<c:when test="${tipo == 'RECURSOS'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID RECURSO</th>
						<th>RECURSO</th>												
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idRecurso}</td>
							<td>${item.nombreRecurso}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'POSIBLES TIPO PREGUNTA'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID</th>
						<th>ID TIPO PREGUNTA</th>
						<th>ID POSIBLE</th>
						<th>RESPUESTA</th>																		
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idPosibleTipoPregunta}</td>
							<td>${item.idTipoPregunta}</td>
							<td>${item.idPosibleRespuesta}</td>
							<td>${item.descripcionPosible}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'POSIBLES '}">
           <table align="left">
				<thead>
					<tr>
						<th>ID</th>
						<th>RESPUESTA</th>																		
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idPosible}</td>
							<td>${item.descripcion}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'GET RESPUESTAS ADICIONALES'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID RESPUESTA</th>
						<th>ID RESPUESTA ADICIONAL</th>
						<th>RESPUESTA</th>																		
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idRespuestaAd}</td>
							<td>${item.idRespuesta}</td>
							<td>${item.descripcion}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'GET RECURSOS PERFIL'}">
			<table>
				<thead>
					<tr>
						<th>ID RECURSO</th>
						<th>ID PERFIL</th>
						<th>CONSULTA</th>
						<th>INSERTA</th>
						<th>MODIFICA</th>
						<th>ELIMINA</th>
					</tr>					
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idRecurso}</td>
							<td>${item.idPerfil}</td>
							<td>${item.consulta}</td>
							<td> ${item.inserta}</td>
							<td> ${item.modifica}</td>
							<td> ${item.elimina}</td>							
<%-- 							 <td align="center"><input type="checkbox" id="${item.idPregunta}" value="Editar" onclick="transformarEnEditableCP(this,${item.idPregunta})" /> </td> --%>
						</tr>			
					</c:forEach>
				</tbody>			
			</table>
		</c:when>
		<c:when test="${tipo == 'LISTA CHECKLIST NEGOCIO'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID CHECKLIST</th>
						<th>ID NEGOCIO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.checklist}</td>
							<td>${item.negocio}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'LISTA NEGOCIO ADICIONAL'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID USUARIO</th>
						<th>ID NEGOCIO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.usuario}</td>
							<td>${item.negocio}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'LISTA PAIS NEGOCIO'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID NEGOCIO</th>
						<th>ID PAIS</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idNegocio}</td>
							<td>${item.idPais}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'GET IMAGENES ARBOL'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID IMAGEN</th>
						<th>ID ARBOL</th>
						<th>URL IMG</th>
						<th>DESCRIPCION</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idImagen}</td>
							<td>${item.idArbol}</td>
							<td>${item.urlImg}</td>
							<td>${item.descripcion}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		
		
		<c:when test="${tipo == 'LISTA GRUPO'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID GRUPO</th>
						<th>DESCRIPCION</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idGrupo}</td>
							<td>${item.descripcion}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		
		
		<c:when test="${tipo == 'LISTA ORDEN GRUPO'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID ORDEN GRUPO</th>
						<th>ID CHECKLIST</th>
						<th>ID GRUPO</th>
						<th>ORDEN</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idOrdenGrupo}</td>
							<td>${item.idChecklist}</td>
							<td>${item.idGrupo}</td>
							<td>${item.orden}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		
		
		<c:when test="${tipo == 'LISTA PERIODO'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID GRUPO</th>
						<th>DESCRIPCION</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idGrupo}</td>
							<td>${item.descripcion}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'LISTA NOTIFICACION'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID MOVIL</th>
						<th>ID USUARIO</th>
						<th>NOMBRE USUARIO</th>
						<th>ID PUESTO</th>
						<th>FECHA</th>
						<th>SO</th>
						<th>VERSION</th>
						<th>VERSION APP</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idMovil}</td>
							<td>${item.idUsuario}</td>
							<td>${item.nombreU}</td>
							<td>${item.idPuesto}</td>
							<td>${item.fecha}</td>
							<td>${item.so}</td>
							<td>${item.version}</td>
							<td>${item.versionApp}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		
		
		<c:when test="${tipo == 'GET ASIGNACIONES'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID CHEKLIST</th>
						<th>ID CECO</th>
						<th>ID PUESTO</th>
						<th>ACTIVO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idChecklist}</td>
							<td>${item.ceco}</td>
							<td>${item.idPuesto}</td>
							<td>${item.activo}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'EJECUTE ASIGNACIONES' }">
		   <c:out value="${res['respuestaString']} "></c:out>
		   <br>
		   <c:out value="${res['respuestaBoolean']} "></c:out>
		</c:when>
	   <c:when test="${tipo == 'EJECUTE ASIGNACIONES_NEW' }">
		   Ejecucion :<c:out value="${res['respuestaString']} " />
		   <br>
		   Errores :<c:out value="${res['respuestaBoolean']} "></c:out>
		</c:when>
		<c:when test="${tipo == 'SUCURSAL GCC'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID SUCURSAL</th>
						<th>ID PAIS</th>
						<th>ID CANAL</th>
						<th>NO. SUCURSAL</th>
						<th>NOMBRE</th>
						<th>LONGITUD</th>
						<th>LATITUD</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idSucursal}</td>
							<td>${item.idPais}</td>
							<td>${item.idCanal}</td>
							<td>${item.nuSucursal}</td>
							<td>${item.nombresuc}</td>
							<td>${item.longitud}</td>
							<td>${item.latitud}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'NOTIFICACION REGIONAL' }">
		   <c:out value="${res} "></c:out>
		</c:when>
		
		
		<c:when test="${tipo == 'LISTA DE TAREAS'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID TAREA</th>
						<th>CLAVE</th>
						<th>DATE</th>
						<th>FECHA</th>
						<th>ACTIVO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idTarea}</td>
							<td>${item.cveTarea}</td>
							<td>${item.fechaTarea}</td>
							<td>${item.strFechaTarea}</td>
							<td>${item.activo}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
			
		<c:when test="${tipo == 'HISTORICO SUCURSAL'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIID_SUCURSALH</th>
						<th>FIID_PAIS</th>
						<th>FIIDNU_SUCURSAL</th>
						<th>FCNOMBRECC</th>
						<th>FCLONGITUD</th>
						<th>FCLATITUD</th>
						<th>FDFECHA_MOD</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idHistoricoSuc}</td>
							<td>${item.idPais}</td>
							<td>${item.numSucursal}</td>
							<td>${item.nombreCC}</td>
							<td>${item.longitud}</td>
							<td>${item.latitud}</td>
							<td>${item.fecha}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
			
			<c:when test="${tipo == 'Fijo'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID EMPLEADO FIJO</th>
						<th>ID USUARIO</th>
						<th>ACTIVO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idEmpFijo}</td>
							<td>${item.idUsuario}</td>
							<td>${item.idActivo}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'Estados'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID PAIS</th>
						<th>ID ESTADO</th>
						<th>DESCRIPCION</th>
						<th>ULTIMA MODIF</th>
						<th>CONSECUTIVO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idPais}</td>
							<td>${item.idEstado}</td>
							<td>${item.descripcion}</td>
							<td>${item.abreviatura}</td>
							<td>${item.modificacion}</td>
							<td>${item.idConsec}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'TOTALCECOS'}">
           <table align="left">
				<thead>
					<tr>
						<th>TOTAL CECO</th>
						<th>ID CHECKLIST</th>
						<th>ID USUARIO</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.totalcheckl}</td>
							<td>${item.checklist}</td>
							<td>${item.idUsuario}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'TOTALCHECK'}">
           <table align="left">
				<thead>
					<tr>
						<th>TOTAL CHECKLIST</th>
						<th>CECO</th>
						<th>ID USUARIO</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.totalceco}</td>
							<td>${item.ceco}</td>
							<td>${item.idUsuario}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'PAPERLESSCHECK'}">
           <table align="left">
				<thead>
					<tr>
						<th>CECO</th>
						<th>ID_CHECKLIST</th>
						<th>CHECK_USUA</th>
						<th>NOMBRE_CHECKLIST</th>
						<th>CALIFICACION</th>
						<th>FECHA_INICIO</th>
						<th>FECHA_FIN</th>
						<th>BITACORA</th>
						<th>BANDERA</th>
						<th>SEMANA</th>
						<th>ID_RESPUESTA</th>
						<th>RESP_ABIERTA</th>
						<th>BANDERA_RESP</th>
						<th>ID_PREGUNTA</th>
						<th>PREG_PADRE</th>
						<th>MODULO</th>
						<th>TIPOPREG</th>
						<th>PREGUNTA</th>
						<th>TIPO_PRE</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.ceco}</td>
							<td>${item.idCheclist}</td>
							<td>${item.chekUsua}</td>
							<td>${item.nombreCheck}</td>
							<td>${item.cali}</td>
							<td>${item.fini}</td>
							<td>${item.ffin}</td>
							<td>${item.bitacora}</td>
							<td>${item.bandera}</td>
							<td>${item.semana}</td>
							<td>${item.idResp}</td>
							<td>${item.respAb}</td>
							<td>${item.bandResp}</td>
							<td>${item.idPreg}</td>
							<td>${item.pregPadre}</td>
							<td>${item.modulo}</td>
							<td>${item.tipoPreg}</td>
							<td>${item.pregunta}</td>
							<td>${item.tipopregunta}</td>
							
		
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
				<c:when test="${tipo == 'BITAGRAL'}">
           <table align="left">
				<thead>
					<tr>
						<th>BITAGRAL</th>
						<th>BITACORA</th>
						<th>STATUS</th>
						<th>FECHA_INI</th>
						<th>FECHA_FIN</th>
						<th>PERIODO</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idBitaGral}</td>
							<td>${item.idBita}</td>
							<td>${item.status}</td>
							<td>${item.finicio}</td>
							<td>${item.fin}</td>
							<td>${item.periodo}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
						<c:when test="${tipo == 'SUCCHEK'}">
           <table align="left">
				<thead>
					<tr>
						<th>CECO</th>
						<th>ECO</th>
						<th>NOMBRE</th>
						<th>REGION</th>
						<th>ZONA</th>
						<th>TERRITORIO</th>
						<th>NEGOCIO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idSucursal}</td>
							<td>${item.idEco}</td>
							<td>${item.nombre}</td>
							<td>${item.region}</td>
							<td>${item.zona}</td>
							<td>${item.territorio}</td>
							<td>${item.negocio}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
			<c:when test="${tipo == 'FIRMACHECK'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID_FIRMA</th>
						<th>CECO</th>
						<th>BITACORA</th>
						<th>AGRUPADORFIRMA</th>
						<th>PUESTO</th>
						<th>RESPONSABLE</th>
						<th>CORREO</th>
						<th>NOMBRE</th>
						<th>RUTA</th>
						<th>OBSERVACION</th>
						<th>PERIODO</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idFirma}</td>
							<td>${item.ceco}</td>
							<td>${item.bitacora}</td>
							<td>${item.idUsuario}</td>
							<td>${item.puesto}</td>
							<td>${item.responsable}</td>
							<td>${item.correo}</td>
							<td>${item.nombre}</td>
							<td>${item.ruta}</td>
							<td>${item.observ}</td>
							<td>${item.periodo}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'REPOCHEKSEXP'}">
           <table align="left">
				<thead>
					<tr>
						<th>BITACORA</th>
						<th>CHECK_USUA</th>
						<th>USUARIO</th>
						<th>CECO</th>
						<th>CHECKLIST</th>
						<th>IDARBOL</th>
						<th>IDPREGUNTA</th>
						<th>PREGUNTA</th>
						<th>RESP</th>
						<th>RESPUESTA</th>
						<th>IDRESP</th>
						<th>OBSERV</th>
						<th>IDOBS</th>
					    <th>IMPERDONABLE</th>
					    <th>FECHA</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.bitacora}</td>
							<td>${item.checkusua}</td>
							<td>${item.usuario}</td>
							<td>${item.ceco}</td>
							<td>${item.idcheck}</td>
							<td>${item.idArbol}</td>
							<td>${item.idPreg}</td>
							<td>${item.pregunta}</td>
							<td>${item.respuesta}</td>
							<td>${item.posible}</td>
							<td>${item.idRespuesta}</td>
							<td>${item.observ}</td>
							<td>${item.idobserv}</td>
							<td>${item.imperdon}</td>
							<td>${item.fecha}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'IMAGSUCK'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID_IMAG</th>
						<th>CECO</th>
						<th>NOMBRE</th>
						<th>RUTA</th>
						<th>OBSERVACIONES</th>
						<th>PERIODO</th>
						
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idImag}</td>
							<td>${item.idSucursal}</td>
							<td>${item.nombre}</td>
							<td>${item.ruta}</td>
							<td>${item.observ}</td>
							<td>${item.periodo}</td>
							
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'SUCUBICA'}">
           <table align="left">
				<thead>
					<tr>
						<th>IDCECO</th>
						<th>CALLE</th>
						<th>CIUDAD</th>
						<th>CP</th>
						<th>SUCURSAL</th>
						<th>NOM_CECO</th>
					    <th>IDCANAL</th>
					    <th>CANAL</th>
					    <th>IDPAIS</th>
					    <th>PAIS</th>
					    <th>LONGITUD</th>
					    <th>LATITUD</th>
						
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.ceco}</td>
							<td>${item.calles}</td>
							<td>${item.ciudad}</td>
							<td>${item.codigoPos}</td>
							<td>${item.numSuc}</td>
							<td>${item.nombCeco}</td>
							<td>${item.idcanal}</td>
							<td>${item.canal}</td>
							<td>${item.idpais}</td>
							<td>${item.pais}</td>
							<td>${item.longitud}</td>
							<td>${item.latitud}</td>

							
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'RESPUESTAS'}">
           <table align="left">
				<thead>
					<tr>
						<th>BITACORA</th>
						<th>IDRESPUESTA</th>
						<th>IDARBOL</th>
						<th>OBSERVACIONES</th>
						<th>IDPREGUNTA</th>
						<th>IDCHECK</th>
						<th>IDRESP</th>
						<th>IDOBS</th>
						<th>POSIBLE</th>
						<th>PREGUNTA</th>
						<th>IDIMPERD</th>
						<th>RUTA</th>
						<th>CHEK_USUA</th>
						<th>IDUSU</th>
						<th>CECO</th>
						<th>CHECKLIST</th>
						<th>ORDENGRUPO</th>
						<th>PERIODICIDAD</th>
						<th>RESP_ABIERTA</th>
						<th>IDRESPAB</th>
						<th>BANDERARESP</th>
						<th>MODULO</th>
						<th>PRECALIF</th>
						<th>CALIFICA</th>
						<th>CLASIFICACION</th>
						<th>TOTALCHECK</th>
						<th>PREG_PADRE</th>
						<th>BITGRAL</th>
						<th>PONDERACION</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						<td>${item.idBita}</td>
						<td>${item.idResp}</td>
						<td>${item.idArbol}</td>
						<td>${item.observ}</td>
						<td>${item.idPreg}</td>
						<td>${item.idCheck}</td>
						<td>${item.idPosible}</td>
						<td>${item.idObserv}</td>
						<td>${item.posible}</td>
						<td>${item.pregunta}</td>
						<td>${item.idcritica}</td>
						<td>${item.ruta}</td>
						<td>${item.idcheckUsua}</td>
						<td>${item.idUsu}</td>
						<td>${item.ceco}</td>
						<td>${item.checklist}</td>
						<td>${item.idOrdenGru}</td>
						<td>${item.idperiodicidad}</td>
						<td>${item.respuestaAbierta}</td>
						<td>${item.idRespAb}</td>
						<td>${item.banderaRespAb}</td>
						<td>${item.modulo}</td>
						<td>${item.precalif}</td>
						<td>${item.calif}</td>
						<td>${item.calCheck}</td>
						<td>${item.clasif}</td>
						<td>${item.pregPadre}</td>
						<td>${item.idBitaGral}</td>
						<td>${item.ponderacion}</td>
					
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
			<c:when test="${tipo == 'TOTPREG'}">
           <table align="left">
				<thead>
					<tr>
						<th>BITACORA_GENERAL</th>
						<th>BITACORA</th>
						<th>IDCHECKLIST</th>
						<th>CHECKLIST</th>
						<th>TOTAL_SI</th>
						<th>TOTAL_NO</th>
						<th>TOTAL_ABIERTAS</th>
						<th>CLASIFICACION</th>
						<th>TOTAL_PREGUNTAS</th>
						<th>SUMA_GRUPO</th>
						<th>TOTAL_PONDERACION</th>
						<th>PRECALIFICACION</th>
						<th>CALIFICACION</th>
						<th>IMPERDONABLES</th>
						<th>TOTALGRAL</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idBitaGral}</td>
							<td>${item.idBita}</td>
							<td>${item.idCheck}</td>
							<td>${item.checklist}</td>
							<td>${item.pregSi}</td>
							<td>${item.pregNo}</td>
							<td>${item.pregNa}</td>
							<td>${item.clasif}</td>
							<td>${item.totPreg}</td>
							<td>${item.sumGrupo}</td>
							<td>${item.ponTot}</td>
							<td>${item.precalif}</td>
							<td>${item.calif}</td>
							<td>${item.aux2}</td>
							<td>${item.totGral}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'EVIDENCIAS_BIT'}">
           <table align="left">
				<thead>
					<tr>
						<th>BITACORA_GENERAL</th>
						<th>ID_RESPUESTA</th>
						<th>ID_ARBOL</th>
						<th>RUTA</th>
						<th>ID_PLANTILLA</th>
						<th>ELEMENTO</th>
						<th>ORDEN</th>
						<th>TIPO_EVID</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idBita}</td>
							<td>${item.idResp}</td>
							<td>${item.idArbol}</td>
							<td>${item.ruta}</td>
							<td>${item.idPlantilla}</td>
							<td>${item.idElem}</td>
							<td>${item.idOrden}</td>
							<td>${item.tipoEvi}</td>
							
							
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'PREG_CHEC'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID_PREG</th>
						<th>MODULO</th>
						<th>TIPOPREG</th>
						<th>ESTATUS</th>
						<th>DESCRIPCION</th>
						<th>DETALLE</th>
						<th>IMPERDONABLE</th>
						<th>SLA</th>
						<th>TIPO_CHEC</th>
						<th>NOMBRE</th>
						<th>VERSION</th>
						<th>ORDEN_GRUP</th>
						<th>PONDERACION</th>
						<th>CLASIFICACION</th>
				
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idPregunta}</td>
							<td>${item.idModulo}</td>
							<td>${item.idTipo}</td>
							<td>${item.estatus}</td>
							<td>${item.desc}</td>
							<td>${item.deta}</td>
							<td>${item.critica}</td>
							<td>${item.sla}</td>
							<td>${item.tipocheck}</td>
							<td>${item.nombChec}</td>
							<td>${item.version}</td>
							<td>${item.ordenG}</td>
							<td>${item.pond}</td>
							<td>${item.clasific}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
			<c:when test="${tipo == 'SOFTOPEN'}">
           <table align="left">
				<thead>
					<tr>
						<th>CECO</th>
						<th>USUARIO</th>
						<th>BITACORA_GRAL</th>
						<th>RECORRIDO</th>
						<th>FECHA INICIO</th>
						<th>FECHA FIN</th>
						<th>APERTURABLE</th>
						<th>PERIODO</th>
					
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.ceco}</td>
							<td>${item.idUsuario}</td>
							<td>${item.bitacora}</td>
							<td>${item.recorrido}</td>
							<td>${item.fechaini}</td>
							<td>${item.fechafin}</td>
							<td>${item.aux}</td>
							<td>${item.periodo}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		
		<c:when test="${tipo == 'CHECKLIST_GPO'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID_CHECKLIST</th>
						<th>NOM_CHECKLIST</th>

				
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idChecklist}</td>
							<td>${item.nomChecklist}</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		
		<c:when test="${tipo == 'CHECK_CECO'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID_CECO</th>
						<th>ID_USUARIO</th>
						<th>ID_CHECKLIST</th>
						<th>NOMBRE_CECO</th>
				
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idCeco}</td>
							<td>${item.idUsuario}</td>	
							<td>${item.idChecklist}</td>						
							<td>${item.nomCeco}</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		
		<c:when test="${tipo == 'BITACORA'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID_BITACORA</th>
						<th>ID_CECO</th>
						<th>ID_CHECKLIST</th>
						<th>ID_USUARIO</th>
						<th>FECHA_INICIO</th>
						<th>FECHA_TERMINO</th>
				
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idBitacora}</td>
							<td>${item.idCeco}</td>
							<td>${item.idChecklist}</td>
							<td>${item.idUsuario}</td>							
							<td>${item.fechaInicio}</td>
							<td>${item.fechaTermino}</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		
		<c:when test="${tipo == 'CHECKIN'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIID_BITACORA</th>
						<th>FCID_USUARIO</th>
						<th>FCID_CECO</th>
						<th>FDFECHA</th>
						<th>FCLATITUD</th>
						<th>FCLONGITUD</th>
						<th>FCBANDERA</th>
						<th>PLATAFORMA</th>
						<th>TIEMPO_CONEXION</th>

				
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idBitacora}</td>
							<td>${item.idUsuario}</td>
							<td>${item.idCeco}</td>
							<td>${item.fecha}</td>
							<td>${item.latitud}</td>
							<td>${item.longitud}</td>
							<td>${item.bandera}</td>
							<td>${item.plataforma}</td>
							<td>${item.tiempoConexion}</td>
							

						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'RESPUESTASIMP'}">
           <table align="left">
				<thead>
					<tr>
						<th>BITACORA</th>
						<th>IDRESPUESTA</th>
						<th>IDARBOL</th>
						<th>OBSERVACIONES</th>
						<th>IDPREGUNTA</th>
						<th>IDCHECK</th>
						<th>IDRESP</th>
						<th>IDOBS</th>
						<th>POSIBLE</th>
						<th>PREGUNTA</th>
						<th>IDIMPERD</th>
						<th>CLASIFICACION</th>
						<th>SLA</th>
						<th>RUTA</th>	
						<th>PREG_PADRE</th>
						<th>BITAGRAL</th>
						<th>LIDER_OBRA</th>
						<th>FECHAINI</th>
						<th>FECHAFIN</th>
						<th>IDUSU</th>
						<th>REALIZORECO</th>
						<th>NOMCECO</th>
						<th>ECO</th>
						<th>CECO</th>
		
		 
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						<td>${item.idBita}</td>
						<td>${item.idResp}</td>
						<td>${item.idArbol}</td>
						<td>${item.observ}</td>
						<td>${item.idPreg}</td>
						<td>${item.idCheck}</td>
						<td>${item.idPosible}</td>
						<td>${item.idObserv}</td>
						<td>${item.posible}</td>
						<td>${item.pregunta}</td>
						<td>${item.idcritica}</td>
						<td>${item.clasif}</td>
						<td>${item.sla}</td>
						<td>${item.ruta}</td>
						<td>${item.pregPadre}</td>
						<td>${item.idBitaGral}</td>
						<td>${item.liderObra}</td>
						<td>${item.fechaIni}</td>
						<td>${item.fechaFin}</td>
						<td>${item.idUsu}</td>
						<td>${item.realizoReco}</td>
						<td>${item.nombCeco}</td>
						<td>${item.eco}</td>
						<td>${item.ceco}</td>

	
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		
		
		<c:when test="${tipo == 'VERSIONEXP'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID_TABLA</th>
						<th>IDCHECKLIST</th>
						<th>VERSION</th>
						<th>ACTIVO</th>
						<th>AUX</th>
						<th>OBSERVACION</th>
						<th>PERIODO</th>
						<th>NEGOCIO</th>
						<th>FECHALIBERACION</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						<td>${item.idTab}</td>
						<td>${item.idCheck}</td>
						<td>${item.idVers}</td>
						<td>${item.idactivo}</td>
						<td>${item.aux}</td>
						<td>${item.obs}</td>
						<td>${item.periodo}</td>
						<td>${item.nego}</td>
						<td>${item.fechaLibera}</td>
						
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'ASIGNAEXP'}">
           <table align="left">
				<thead>
					<tr>
						<th>CECO</th>
						<th>VERSION</th>
						<th>USUARIO ASIGNO</th>
						<th>USUARIO ASIGNACION</th>
						<th>STATUS</th>
						<th>OBSERVACION</th>
						<th>PERIODO</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						<td>${item.ceco}</td>
						<td>${item.version}</td>
						<td>${item.usuario}</td>
						<td>${item.usuario_asig}</td>
						<td>${item.idestatus}</td>
						<td>${item.obs}</td>
						<td>${item.periodo}</td>
					
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo =='UsuarioProtocolo'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID_USUARIO</th>
						<th>NOMBRE_USUARIO</th>
						
				
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idUsuario}</td>
							<td>${item.nombreUsuario}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
			</c:when>
			
		<c:when test="${tipo =='idNombreChek'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIID_CHECKLIST</th>
						<th>FCNOMBRE</th>
						
				
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idUsuario}</td>
							<td>${item.nomCheklist}</td>
						
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
			<c:when test="${tipo =='idNombreCeco'}">
           <table align="left">
				<thead>
					<tr>
						<th>FCID_CECO</th>
						<th>FCNOMBRE</th>
						
				
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idCeco}</td>
							<td>${item.nomCeco}</td>
						
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
			<c:when test="${tipo =='bitacoraCerrada'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIID_BITACORA</th>
						<th>FDINICIO</th>
						<th>FDTERMINO</th>
						
				
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idBitacora}</td>
							<td>${item.fdInicio}</td>
							<td>${item.fdTermino}</td>
						
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo =='controlAcceso'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIID_BITACORA</th>
						<th>FCID_USUARIO</th>
						<th>FCID_CECO</th>
						<th>FCNOMBRE</th>
						<th>FDFECHA</th>
						<th>FCLATITUD</th>
						<th>FCLONGITUD</th>
						<th>FCBANDERA</th>
						<th>PLATAFORMA</th>
						<th>TIEMPO_CONEXION</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idBitacora}</td>
							<td>${item.idUsuario}</td>
							<td>${item.idCeco}</td>
							<td>${item.nomCeco}</td>
							<td>${item.fdfecha}</td>
							<td>${item.fcLatitud}</td>
							<td>${item.fcLongitud}</td>
							<td>${item.bandera}</td>
							<td>${item.plataforma}</td>
							<td>${item.tiempoConexion}</td>
						
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'VERSIONGRAL'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID_TABLA</th>
						<th>IDCHECKLIST</th>
						<th>VERSION</th>
						<th>VERSION_ANTERIOR</th>
						<th>ACTIVO</th>
						<th>NOMBRECHECKLIST</th>
						<th>OBSERVACION</th>
						<th>PERIODO</th>

					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						<td>${item.idTab}</td>
						<td>${item.idCheck}</td>
						<td>${item.idVers}</td>
						<td>${item.versionAnt}</td>
						<td>${item.idactivo}</td>
						<td>${item.nombrecheck}</td>
						<td>${item.obs}</td>
						<td>${item.periodo}</td>
	
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'SOPEXP'}">
           <table align="left">
				<thead>
					<tr>
						<th>CECO</th>
						<th>NOMBRE</th>
						<th>CECO_SUPERIOR</th>
						<th>ESTATUS</th>
						<th>TIPOSUCURSAL</th>
						<th>PAIS</th>
						<th>NEGOCIO</th>
						<th>USUARIO</th>
						<th>FECHA</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						<td>${item.ceco}</td>
						<td>${item.nombCeco}</td>
						<td>${item.cecoSup}</td>
						<td>${item.status}</td>
						<td>${item.tipoSuc}</td>
						<td>${item.pais}</td>
						<td>${item.negocio}</td>
						<td>${item.usuario}</td>
						<td>${item.fecha}</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'SOPASIGN'}">
           <table align="left">
				<thead>
					<tr>
						<th>CHECK_USUA</th>
						<th>CHECKLIST</th>
						<th>USUARIO</th>
						<th>CECO</th>
						<th>STATUS</th>
						<th>VERSION</th>
						<th>BITACORA</th>
						<th>FECHA_INICIO</th>
						<th>FECHA TERMINO</th>
					
						
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						<td>${item.usuario_asig}</td>
						<td>${item.aux}</td>
						<td>${item.usuario}</td>
						<td>${item.ceco}</td>
						<td>${item.obs}</td>
						<td>${item.version}</td>
						<td>${item.aux2}</td>
						<td>${item.cec}</td>
						<td>${item.periodo}</td>
					
	
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'filtro1'}">
           <table align="left">
				<thead>
					<tr>
						<th>CECO</th>
						<th>NOMBRECECO</th>
						<th>ACTIVO</th>
						<th>CECO_SUP</th>
						<th>DIRECCION</th>
						<th>REGION</th>
						<th>TERRITORIO</th>
						<th>ZONA</th>
						<th>RECORRIDO</th>
						<th>BITGRAL</th>
						<th>APERTURABLE</th>
						<th>PERIODO</th>
						<th>CALIFICACION</th>
						<th>PRECALIFICACION</th>
						<th>RESPONSABLEOBRA</th>
						<th>USUARIORECO</th>
						<th>SI-NA PONDERACION</th>
						
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">

						<tr>
						<td>${item.ceco}</td>
						<td>${item.nombreCeco}</td>
						<td>${item.activoCeco}</td>
						<td>${item.cecoSuperior}</td>
						<td>${item.direccion}</td>
						<td>${item.region}</td>
						<td>${item.territorio}</td>
						<td>${item.zona}</td>
						<td>${item.idRecorrido}</td>
						<td>${item.bitGral}</td>
						<td>${item.aperturable}</td>
						<td>${item.perido}</td>
						<td>${item.calif}</td>
						<td>${item.precalif}</td>
						<td>${item.nomUsu}</td>
						<td>${item.aux}</td>
						<td>${item.pondTot}</td>
					
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'filtro2'}">
           <table align="left">
				<thead>
					<tr>
						<th>BITACORA</th>
						<th>IDRESPUESTA</th>
						<th>IDARBOL</th>
						<th>OBSERVACIONES</th>
						<th>IDPREGUNTA</th>
						<th>IDCHECK</th>
						<th>IDRESP</th>
						<th>IDOBS</th>
						<th>POSIBLE</th>
						<th>PREGUNTA</th>
						<th>IDIMPERD</th>
						<th>CHEK_USUA</th>
						<th>IDUSU</th>
						<th>CECO</th>
						<th>CHECKLIST</th>
						<th>ORDENGRUPO</th>
						<th>PERIODICIDAD</th>
						<th>RESP_ABIERTA</th>
						<th>IDRESPAB</th>
						<th>BANDERARESP</th>
						<th>MODULO</th>
						<th>PRECALIF</th>
						<th>CALIFICA</th>
						<th>CLASIFICACION</th>
						<th>PONDERACIONCHECK</th>
						<th>BITAGRAL</th>
						<th>FECHATERMINO</th>
						<th>PREGPADRE</th>
						<th>RECORRIDO</th>
						<th>VERSION</th>
						<th>RUTA</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						<td>${item.idBita}</td>
						<td>${item.idResp}</td>
						<td>${item.idArbol}</td>
						<td>${item.observ}</td>
						<td>${item.idPreg}</td>
						<td>${item.idCheck}</td>
						<td>${item.idPosible}</td>
						<td>${item.idObserv}</td>
						<td>${item.posible}</td>
						<td>${item.pregunta}</td>
						<td>${item.idcritica}</td>
						<td>${item.idcheckUsua}</td>
						<td>${item.idUsu}</td>
						<td>${item.ceco}</td>
						<td>${item.checklist}</td>
						<td>${item.idOrdenGru}</td>
						<td>${item.idperiodicidad}</td>
						<td>${item.respuestaAbierta}</td>
						<td>${item.idRespAb}</td>
						<td>${item.banderaRespAb}</td>
						<td>${item.modulo}</td>
						<td>${item.precalif}</td>
						<td>${item.calif}</td>
						<td>${item.clasif}</td>
						<td>${item.calCheck}</td>
						<td>${item.bitGral}</td>
						<td>${item.fechaTermino}</td>
						<td>${item.pregPadre}</td>
						<td>${item.idRecorrido}</td>
						<td>${item.version}</td>
						<td>${item.ruta}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
			<c:when test="${tipo == 'PREGUN'}">
           <table align="left">
				<thead>
					<tr>
					
						<th>IDPREGUNTA</th>
						<th>IDCHECK</th>
						<th>PREGUNTA</th>
						<th>PREGUNTA_PADRE</th>
						<th>CECO</th>
						<th>CHECKLIST</th>
						<th>CLASIFICACION</th>
						<th>VERSION</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						
						<td>${item.idPreg}</td>
						<td>${item.idCheck}</td>
						<td>${item.pregunta}</td>
						<td>${item.pregPadre}</td>
						<td>${item.ceco}</td>
						<td>${item.checklist}</td>
						<td>${item.clasif}</td>
						<td>${item.version}</td>
					
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'DASHEXP'}">
           <table align="left">
				<thead>
					<tr>
						<th>IDBITACORA</th>
						<th>IDRESPUESTA</th>
						<th>ARBOL_DES</th>
						<th> OBSERVACIONES</th>
						<th>IDPREGUN</th>
						<th>PREGPADRE</th>
						<th>IDCHECK</th>
						<th>RESPUESTA</th>
						<th>REQOBS</th>
						<th>RESP</th>
						<th>PREGUNTA</th>
						<th>IMPERD</th>
						<th>CHECK_USUA</th>
						<th>IDUSU</th>
						<th>CECO</th>
						<th>CHECKLIST</th>
						<th>ORDEN GRUP</th>
						<th>PERIODO</th>
						<th>MODULO</th>
						<th>PRECALIF</th>
						<th>CALIF</th>
						<th>FECHA_TERMINO</th>
						<th>CLASIFICACION</th>
						<th>PONDERACION</th>
						<th>BITGRAL</th>
						<th>RECORRIDO</th>
						<th>NOMCECO</th>
						<th>ECO</th>
						<th>USUARIO</th>
						<th>RUTA</th>
						<th>FASE</th>
						<th>VERSION</th>
						<th>SLA</th>
						<th>AREA</th>
						<th>LIDEROBRA</th>
												
				</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idBita}</td>
							<td>${item.idResp}</td>
							<td>${item.idArbol}</td>
							<td>${item.observ}</td>
							<td>${item.idPreg}</td>
							<td>${item.pregPadre}</td>
							<td>${item.idCheck}</td>
							<td>${item.idPosible}</td>
							<td>${item.idObserv}</td>
							<td>${item.posible}</td>
							<td>${item.pregunta}</td>
							<td>${item.idcritica}</td>
							<td>${item.idcheckUsua}</td>
							<td>${item.idUsu}</td>
							<td>${item.ceco}</td>
							<td>${item.checklist}</td>
							<td>${item.idOrdenGru}</td>
							<td>${item.idperiodicidad}</td>
							<td>${item.checklist}</td>
							<td>${item.precalif}</td>
							<td>${item.calif}</td>
							<td>${item.fechaTermino}</td>
							<td>${item.clasif}</td>
							<td>${item.calCheck}</td>
							<td>${item.bitGral}</td>
							<td>${item.idRecorrido}</td>
							<td>${item.nombreCeco}</td>
							<td>${item.aux2}</td>
							<td>${item.nomUsu}</td>
							<td>${item.ruta}</td>
							<td>${item.fase}</td>
							<td>${item.version}</td>
							<td>${item.sla}</td>
							<td>${item.area}</td>
							<td>${item.liderObra}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'CONTACTOEXT'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID_TABLA</th>
						<th>CECO</th>
						<th>BITGRAL</th>
						<th> IDUSU</th>
						<th>DOMICILIO</th>
						<th>NOMBRE</th>
						<th>TELEFONO</th>
						<th>CONTACTO</th>
						<th>AREA</th>
						<th>COMPAÑIA</th>
						<th>AUX</th>
						<th>AUX2</th>
						<th>PERIODO</th>
												
				</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idTab}</td>
							<td>${item.ceco}</td>
							<td>${item.bitGral}</td>
							<td>${item.idUsu}</td>
							<td>${item.domicilio}</td>
							<td>${item.nombre}</td>
							<td>${item.telefono}</td>
							<td>${item.contacto}</td>
							<td>${item.area}</td>
							<td>${item.compania}</td>
							<td>${item.aux}</td>
							<td>${item.aux2}</td>
							<td>${item.periodo}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'GETDASHBOARD'}">
           <table align="left">
				<thead>
					<tr>
						<th>FCID_USUARIO</th>
						<th>FCNOMBRE</th>
						<th>FDFECHA</th>
						<th>FISUC_VISITADAS</th>
						<th>FISUCXVISITAR</th>
						<th>SUCURSAL_ASIGNADA</th>
						<th>SUCURSAL_VISITADA</th>
												
				</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idUsuario}</td>
							<td>${item.nomCeco}</td>
							<td>${item.fecha}</td>
							<td>${item.sucVisitadas}</td>
							<td>${item.sucxVisitar}</td>
							<td>${item.sucAsignada}</td>
							<td>${item.sucVisitada}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
			<c:when test="${tipo == 'CalculoAsistencia'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIMES</th>
						<th>FIDIAS_HABILES</th>
						<th>FIASISTENCIAS</th>
						<th>FIPORCENTAJE</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.fimes}</td>
							<td>${item.diasHabiles}</td>
							<td>${item.asistencias}</td>
							<td>${item.porcentaje}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
	
	
	<c:when test="${tipo == 'ConsultaFecha'}">
           <table align="left">
				<thead>
					<tr>
						<th>FDFECHA</th>
						<th>FIANIO</th>
						<th>FIMES</th>
						<th>FITRIMESTRE</th>
						<th>FISEMANA</th>
						<th>FINUMDIASEM</th>
						<th>FINUMDIA</th>
						<th>FIDIAFESTIVO</th>
						<th>FCUSUARIO_MOD</th>
						<th>FDFECHA_MOD</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.fecha}</td>
							<td>${item.anio}</td>
							<td>${item.mes}</td>
							<td>${item.trimestre}</td>
							<td>${item.semana}</td>
							<td>${item.numeroDiaSemana}</td>
							<td>${item.numDia}</td>
							<td>${item.diaFestivo}</td>
							<td>${item.usuarioMod}</td>
							<td>${item.fechaMod}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>		
		<c:when test="${tipo == 'SUPERVISOR'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID SUPERVISOR</th>
						<th>NOMBRE SUPERVISOR</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${lista}" var="item">
						<tr>
							<td>${item.idUsuario}</td>
							<td>${item.nomSupervisor}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'ASISTENCIASUP'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID USUARIO</th>
						<th>NOMBRE USUARIO</th>
						<th>FECHA</th>
						<th>HORA ENTRADA</th>
						<th>HORA SALIDA</th>
						<th>ID SUCURSAL</th>
						<th>NOMBRE SUCURSAL</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${lista}" var="item">
						<tr>
							<td>${item.idUsuario}</td>
							<td>${item.nomUsuario}</td>
							<td>${item.fecha}</td>
							<td>${item.entrada}</td>
							<td>${item.salida}</td>
							<td>${item.idSucursal}</td>
							<td>${item.nomSucursal}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
						

	
	<c:when test="${tipo == 'REPOFECHAEXP'}">
           <table align="left">
				<thead>
					<tr>
						<th>CECO</th>
						<th>NOMBRECECO</th>
						<th>DIRECCION</th>
						<th>FECHATERMINO</th>
						<th>BITGRAL</th>
						<th>RECORRIDO</th>
						<th>APERTURABLE</th>
						<th>CALIFICACION</th>
						<th>PRECALIFICACION</th>
						<th>USUARIO</th>
						<th>PUESTO</th>
						<th>RESPONSABLE</th>
						<th>TOTAL_IMPERD</th>
						<th>TOTAL_NO</th>
						<th>SUMA_SI</th>
						<th>SI-NA PONDERA</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">

						<tr>
						<td>${item.ceco}</td>
						<td>${item.nombreCeco}</td>
						<td>${item.direccion}</td>
						<td>${item.fechaTermino}</td>
						<td>${item.bitGral}</td>	
						<td>${item.idRecorrido}</td>
						<td>${item.aperturable}</td>
						<td>${item.calif}</td>
						<td>${item.precalif}</td>
						<td>${item.nomUsu}</td>
						<td>${item.aux}</td>
						<td>${item.obs}</td>
						<td>${item.idcritica}</td>
						<td>${item.aux2}</td>
						<td>${item.pondTot}</td>
						<td>${item.sumPreg}</td>
	  
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
	<c:when test="${tipo == 'Obtiene Territorio'}">
           <table align="left">
				<thead>
					<tr>
						<th>FCID_CECO</th>
						<th>FCNOMBRE</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idCeco}</td>
							<td>${item.nombreCeco}</td>
						
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'Obtiene Zonas'}">
           <table align="left">
				<thead>
					<tr>
						<th>FCID_CECO</th>
						<th>FCNOMBRE</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idCeco}</td>
							<td>${item.nombreCeco}</td>
						
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'Obtiene Estructura'}">
           <table align="left">
				<thead>
					<tr>
						<th>FCNOMBRETERR</th>
						<th>FCNOMBREZONA</th>
						<th>FCNOMBREREGI</th>
						<th>FCNOMBREGERE</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.territorio}</td>
							<td>${item.zona}</td>
							<td>${item.region}</td>
							<td>${item.gerente}</td>
						
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'CON_SUCURSAL'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID SUCURSAL</th>
						<th>NOMBRE SUCURSAL</th>
						<th>FECHA INICIO</th>
						<th>FECHA FIN</th>
						<th>ID USUARIO</th>
						<th>NOMBRE USUARIO</th>
						<th>ID PROTOCOLO</th>
						<th>NOMBRE PROTOCOLO</th>
						<th>ID BITACORA</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${lista}" var="item">
						<tr>
							<td>${item.idSucursal}</td>
							<td>${item.nomSucursal}</td>
							<td>${item.fechaInicio}</td>
							<td>${item.fechaFin}</td>
							<td>${item.idUsuario}</td>
							<td>${item.nomUsuario}</td>
							<td>${item.idProtocolo}</td>
							<td>${item.nomProtocolo}</td>
							<td>${item.idBitacora}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
			
		
		<c:when test="${tipo == 'Obtiene Visitas'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIMES</th>
						<th>FIVISITAMES</th>

					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.fimes}</td>
							<td>${item.ficonteo}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'promedioCalif'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIMES</th>
						<th>FIPROMEDIOCALF</th>

					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.fimes}</td>
							<td>${item.promedioCalif}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'HALLAZGOSEXP'}">
           <table align="left">
				<thead>
					<tr>
						<th>REGISTRO</th>
						<th>IDHALLAZGO</th>
						<th>FOLIO</th>
						<th>IDRESP</th>
						<th>STATUS</th>
						<th>IDPREG</th>
						<th>PREGUNTA</th>
						<th>RESPUESTA</th>
						<th>PREGPADRE</th>
						<th>RESPONSABLE</th>
						<th>DISPOSITIVO</th>
						<th>AREA</th>
						<th>OBSERVACION</th>
						<th>BITGRAL</th>
						<th>ARBOL</th>
						<th>RUTA</th>
						<th>FDINICIO</th>
						<th>FDFIN</th>
						<th>SLA</th>
						<th>FDAUTORIZO</th>
						<th>PERIODO</th>
						<th>CECO</th>
						<th>IDCHECK</th>
						<th>NOMBRECHECK</th>
						<th>ZONA</th>
						<th>MOTIVO RECHAZO</th>
						<th>USU_MODIFICO</th>
						<th>AREA</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.aux2}</td>
							<td>${item.idHallazgo}</td>
							<td>${item.idFolio}</td>
							<td>${item.idResp}</td>
							<td>${item.status}</td>
							<td>${item.idPreg}</td>
							<td>${item.preg}</td>
							<td>${item.respuesta}</td>
							<td>${item.pregPadre}</td>
							<td>${item.responsable}</td>
							<td>${item.disposi}</td>
							<td>${item.area}</td>
							<td>${item.obs}</td>
							<td>${item.bitGral}</td>
							<td>${item.arbol}</td>
							<td>${item.ruta}</td>
							<td>${item.fechaIni}</td>
							<td>${item.fechaFin}</td>
							<td>${item.sla}</td>
							<td>${item.fechaAutorizo}</td>
							<td>${item.periodo}</td>
							<td>${item.ceco}</td>
							<td>${item.idcheck}</td>
							<td>${item.nombChek}</td>
							<td>${item.zonaClasi}</td>
							<td>${item.motivrechazo}</td>
							<td>${item.usuModif}</td>
							<td>${item.area}</td>
	
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'HALLAZGOSEVIEXP'}">
           <table align="left">
				<thead>
					<tr>
						<th>IDEVIDENCIA</th>
						<th>IDHALLAZGO</th>
						<th>IDRESP</th>
						<th>RUTA</th>
						<th>NOMBRE</th>
						<th>TIPOEVI</th>
						<th>NUMEVI</th>
						<th>VERSION</th>
						<th>STATUS</th>
						<th>PERIODO</th>
						<th>BITGRAL</th>
						
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idEvi}</td>
							<td>${item.idHallazgo}</td>
							<td>${item.idResp}</td>
							<td>${item.ruta}</td>
							<td>${item.nombre}</td>
							<td>${item.tipoEvi}</td>
							<td>${item.totEvi}</td>
							<td>${item.idVersion}</td>
							<td>${item.status}</td>
							<td>${item.periodo}</td>
							<td>${item.bitaGral}</td>
						
			
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>

		
			<c:when test="${tipo == 'LISTADO BITACORA'}">
			<c:forEach items="${res}" var="item">
				ID BITACORA: ${item.idBitacora} <br />
				ID CHECK USUA: ${item.idCheckUsua} <br />
				LONGITUD: ${item.longitud} <br />
				LATITUD: ${item.latitud} <br />
				FECHA INICIO: ${item.fechaInicio} <br />
				FECHA FIN: ${item.fechaFin} <br />
				CECO : ${item.ceco}
				<br />
				<br />
			</c:forEach>
		</c:when>
	<c:when test="${tipo == 'PARAMETRO'}">
           <table align="left">
				<thead>
					<tr>
						<th>FCVALOR</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.valorParametro}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'DISTRIBCECOS'}">
           <table align="left">
				<thead>
					<tr>
						<th>FCID_CECO</th>
						<th>FCNOMBRE</th>
						<th>FCID_TERR</th>
						<th>FCID_ZONA</th>
						<th>FCID_REGI</th>
						<th>FCID_GERE</th>
						<th>FCUSUARIO_MOD</th>
						<th>FCUSUARIO_MOD</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.ceco}</td>
							<td>${item.nombre}</td>
							<td>${item.territorio}</td>
							<td>${item.zona}</td>
							<td>${item.region}</td>
							<td>${item.gerente}</td>
							<td>${item.usuario}</td>
							<td>${item.fecha}</td>
						
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
			<c:when test="${tipo == 'BOOKS'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIID_BOOK</th>
						<th>FCDESCRIPCION</th>
						<th>FCRUTA</th>
						<th>FIACTIVO</th>
						<th>FIPADRE</th>
						<th>FCTIPO</th>
						<th>FCUSUARIO_MOD</th>
						<th>FDFECHA_MOD</th>
						<th>FDFECHA</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idBook}</td>
							<td>${item.descripcion}</td>
							<td>${item.ruta}</td>
							<td>${item.activo}</td>
							<td>${item.padre}</td>
							<td>${item.tipo}</td>
							<td>${item.usuarioMod}</td>
							<td>${item.fechaMod}</td>
							<td>${item.fecha}</td>
						
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'CONTEOTAB'}">
           <table align="left">
				<thead>
					<tr>
						<th>CHECK_USUA</th>
						<th>BITACORAS</th>
						<th>CECOS</th>
						<th>USUARIOS</th>
						<th>TOKENS</th>
						<th>PASOCECO</th>
						<th>BITA_NULAS</th>
						<th>BITA_NO_NULAS</th>
						<th>ESQUEMA</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.checkUsua}</td>
							<td>${item.bitacoras}</td>
							<td>${item.cecos}</td>
							<td>${item.usuarios}</td>
							<td>${item.tokens}</td>
							<td>${item.pasoCeco}</td>
							<td>${item.bitNulas}</td>
							<td>${item.bitNoNulas}</td>
							<td>${item.esquema}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'hallaceco'}">
           <table align="left">
				<thead>
					<tr>
						<th>IDRESP</th>
						<th>BITACORA</th>
						<th>OBSERV</th>
						<th>CHECKLIST</th>
						
						<th>POSIBLE</th>
						<th>ID_PREG</th>
						<th>PREG_PADRE</th>
						<th>DESC_PREG</th>
						<th>BITGRAL</th>
						<th>RUTA</th>
						<th>CECO</th>
						<th>IMPERD</th>
						<th>ID_HALLAZ</th>
						<th>FOLIO</th>
						<th>RESPONSABLE</th>
						<th>OBS_NUEVA</th>
						<th>FECHAINI</th>
						<th>FECHAFIN</th>
						<th>FECHAAUTORIZO</th>
						<th>RECORRIDO</th>
	
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idResp}</td>
							<td>${item.idBita}</td>
							<td>${item.observ}</td>
							<td>${item.idCheck}</td>
							
							<td>${item.posible}</td>
							<td>${item.idPreg}</td>
							<td>${item.pregPadre}</td>
							<td>${item.pregunta}</td>
							<td>${item.idBitaGral}</td>
							<td>${item.ruta}</td>
							<td>${item.ceco}</td>
							<td>${item.idcritica}</td>
							<td>${item.idHalla}</td>
							<td>${item.folio}</td>
							<td>${item.responsable}</td>
							<td>${item.obsNueva}</td>
							<td>${item.fechaIni}</td>
							<td>${item.fechaFin}</td>
							<td>${item.fechaAut}</td>
							<td>${item.reco}</td>
							
							
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'CHECKLISTPROTOCOLO'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIID_CHECKLIST</th>
						<th>FIID_TIPO_CHECK</th>
						<th>FCNOMBRE</th>
						<th>FIID_HORARIO</th>
						<th>FIVIGENTE</th>
						<th>FDFECHA_INICIO</th>
						<th>FDFECHA_FIN</th>
						<th>FIID_ESTADO</th>
						<th>FCDIA</th>
						<th>FCPERIODO</th>
						<th>FIVERSION</th>
						<th>FIORDEN_GRUPO</th>
						<th>FCPONDTOTAL</th>
						<th>FCCLASIFICA</th>
						<th>FIID_PROTOCOLO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idChecklist}</td>
							<td>${item.idTipoChecklist}</td>
							<td>${item.nombreCheck}</td>
							<td>${item.idHorario}</td>
							<td>${item.vigente}</td>
							<td>${item.fecha_inicio}</td>
							<td>${item.fecha_fin}</td>
							<td>${item.idEstado}</td>
							<td>${item.dia}</td>
							<td>${item.periodo}</td>
							<td>${item.version}</td>
							<td>${item.ordenGrupo}</td>
							<td>${item.ponderacionTot}</td>
							<td>${item.clasifica}</td>
							<td>${item.idProtocolo}</td>
						
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'PREGUNTA_SUPERVISION'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID_PREGUNTA</th>
						<th>ID_MODULO</th>
						<th>ID_TIPO</th>
						<th>ESTATUS</th>
						<th>DESCIPCION_PREGUNTA</th>
						<th>AYUDA</th>
						<th>CRITICA</th>
						<th>SLA</th>
						<th>AREA</th>
						<th>NUM_SERIE</th>
						<th>ID ZONA</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idPregunta}</td>
							<td>${item.idModulo}</td>
							<td>${item.idTipo}</td>
							<td>${item.estatus}</td>
							<td>${item.descPregunta}</td>
							<td>${item.detalle}</td>
							<td>${item.critica}</td>
							<td>${item.sla}</td>
							<td>${item.area}</td>
							<td>${item.numSerie}</td>
							<td>${item.idZona}</td>
						
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'GETCOMPLETO'}">
			<c:forEach items="${res}" var="item">
				ID BITACORA: ${item.idBitacora} <br />
				ID CHECK USUA: ${item.idCheckUsua} <br />
				LONGITUD: ${item.longitud} <br />
				LATITUD: ${item.latitud} <br />
				FECHA INICIO: ${item.fechaInicio} <br />
				FECHA FIN: ${item.fechaFin} <br />
				CECO : ${item.ceco}
				<br />
				<br />
			</c:forEach>
		</c:when>
		
		<c:when test="${tipo == 'NegoZona'}">
           <table align="left">
				<thead>
					<tr>
						<th>TAB_ZONA</th>
						<th>ID_ZONA</th>
						<th>CLASIFICA</th>
						<th>DESCRIPCIONZONA</th>
						<th>TAB_NEGO</th>
						<th>ID_NEGO</th>
						<th>NEGO</th>
						<th>NEGOCIO</th>
						<th>ACTIVO</th>
						<th>OBS</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idTabZona}</td>
							<td>${item.idZona}</td>
							<td>${item.clasifica}</td>
							<td>${item.descZona}</td>
							<td>${item.idTabNegocio}</td>
							<td>${item.idNegocio}</td>
							<td>${item.negocio}</td>
							<td>${item.descNego}</td>
							<td>${item.idActivo}</td>
			
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'GETNOMSUCU'}">
			<table  style="position:relative; width: 80%;">
				<thead>
					<tr>
						<th>FCID_CECO</th>
						<th>FCNOMBRE</th>
					</tr>					
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idCeco}</td>
							<td>${item.nomCeco}</td>
						</tr>			
					</c:forEach>
				</tbody>			
			</table>
		</c:when>
		
		
				<c:when test="${tipo == 'GETCECOBITACORA'}">
			<table  style="position:relative; width: 80%;">
				<thead>
					<tr>
						<th>FCID_CECO</th>
						<th>FDINICIO</th>
						<th>FDTERMINO</th>
					</tr>					
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idCeco}</td>
							<td>${item.fechaInicio}</td>
							<td>${item.fechaTermino}</td>
						</tr>			
					</c:forEach>
				</tbody>			
			</table>
		</c:when>
		
					<c:when test="${tipo == 'GETNOMCHECK'}">
			<table  style="position:relative; width: 80%;">
				<thead>
					<tr>
					     <th>FDTERMINO</th>
						<th>FCID_CECO</th>
						<th>FIID_CHECKLIST</th>
						<th>FCNOMBRE</th>
						<th>FIID_BITACORA</th>
					</tr>					
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.fechaTermino}</td>
							<td>${item.idCeco}</td>
							<td>${item.idChecklist}</td>
							<td>${item.nomChecklist}</td>
							<td>${item.idBitacora}</td>
						</tr>			
					</c:forEach>
				</tbody>			
			</table>
		</c:when>
		
		<c:when test="${tipo == 'GETDETALLE'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIID_BITACORA</th>
						<th>FCID_CECO</th>
						<th>FIID_CHECKLIST</th>
						<th>FIID_USUARIO</th>
						<th>FDINICIO</th>
						<th>FDTERMINO</th>
				
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idBitacora}</td>
							<td>${item.idCeco}</td>
							<td>${item.idChecklist}</td>
							<td>${item.idUsuario}</td>							
							<td>${item.fechaInicio}</td>
							<td>${item.fechaTermino}</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
			<c:when test="${tipo == 'GETASEGURADOR'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIID_USUARIO</th>
						<th>FCNOMBRE</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idUsuario}</td>
								<td>${item.nomUsuario}</td>								
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'GETASEGURADORSUCURSAL'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIID_USUARIO</th>
						<th>FCID_CECO</th>
						<th>FCNOMBRE</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idUsuario}</td>
								<td>${item.idCeco}</td>
								<td>${item.nomCeco}</td>									
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
			<c:when test="${tipo == 'GETASEGURADORBITACORA'}">
           <table align="left">
				<thead>
					<tr>
						<th>FDTERMINO</th>
						<th>FCID_CECO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
								<td>${item.fechaTermino}</td>
								<td>${item.idCeco}</td>									
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'GETASEGPROTO'}">
           <table align="left">
				<thead>
					<tr>
						<th>FDTERMINO</th>
						<th>FCID_CECO</th>
						<th>FIID_CHECKLIST</th>
						<th>FCNOMBRE</th>
						<th>FIID_BITACORA</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.fechaTermino}</td>
								<td>${item.idCeco}</td>
								<td>${item.idChecklist}</td>
								<td>${item.nomCheckList}</td>
								<td>${item.idBitacora}</td>											
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'GETDETALLEBITACORA'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIID_BITACORA</th>
						<th>FCID_CECO</th>
						<th>FIID_CHECKLIST</th>
						<th>FIID_USUARIO</th>
						<th>FDINICIO</th>
						<th>FDTERMINO</th>
				
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idBitacora}</td>
							<td>${item.idCeco}</td>
							<td>${item.idChecklist}</td>
							<td>${item.idUsuario}</td>							
							<td>${item.fechaInicio}</td>
							<td>${item.fechaTermino}</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'GETIDGERENTE'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIGERENTEID</th>
						<th>FCNOMGERENTE</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
								<td>${item.idGerente}</td>
								<td>${item.nomGerente}</td>								
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'GETIDUSUARIOS'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIUSUARIO</th>
						<th>FCNOMUSUARIO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
								<td>${item.idUsuario}</td>
								<td>${item.nomUsuario}</td>								
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
				<c:when test="${tipo == 'GETINFORMACION'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIGERENTEID</th>
						<th>FCNOMGERENTE</th>
						<th>FIUSUARIO</th>
						<th>FCNOMUSUARIO</th>
						<th>ZONA</th>
						<th>REGION</th>
						<th>FIID_PERFIL</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
								<td>${item.idGerente}</td>
								<td>${item.nomGerente}</td>
								<td>${item.idUsuario}</td>	
								<td>${item.nomUsuario}</td>	
								<td>${item.zona}</td>	
								<td>${item.region}</td>	
								<td>${item.idPerfil}</td>								
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'RepMedProtocolos'}">
           <table align="left">
				<thead>
					<tr>
						<th>Fecha que se Realizo</th>
						<th>Quien lo realizo</th>
						<th>Número de Usuario</th>
						<th>Puesto</th>
						<th>País</th>
						<th>Territorio</th>
						<th>Zona</th>
						<th>Regional</th>
						<th>Sucursal</th>		
						<th>Nombre Sucursal</th>
						<th>Canal</th>	
						<th>Ponderacion</th>		
						<th>Calificación</th>
						<th>Conteo de SI</th>	
						<th>Conteo de NO</th>		
						<th>Imperdonables</th>		
						<c:forEach items="${preg}" var="preg">
									<th>${preg.pregunta}</th>			
						</c:forEach>				
					</tr>
				</thead>
				<tbody>
					<c:set var="contador" value="${0}" />
					<c:forEach items="${resp}" var="resp">
					<tr>
						<td>${resp.fecha}</td>
						<td>${resp.empleado}</td>
						<td>${resp.idEmpleado}</td>
						<td>${resp.puesto}</td>
						<td>${resp.pais}</td>
						<td>${resp.territorio}</td>
						<td>${resp.zona}</td>
						<td>${resp.regional}</td>
						<td>${resp.sucursal}</td>		
						<td>${resp.nomSucursal}</td>
						<td>${resp.canal}</td>	
						<td>${resp.ponderacion}</td>		
						<td>${resp.calificacion}</td>
						<td>${resp.contSi}</td>	
						<td>${resp.contNo}</td>		
						<td>${resp.contImp}</td>	
						
						<c:set var="cont" value="${0}" />
						<c:forEach items="${respSplit}" var="respSplit2">						
							<c:forEach items="${respSplit2}" var="tmp">
							<c:if test="${cont==contador}">
								<td>${tmp}</td>							
							</c:if>
							</c:forEach>
						<c:set var="cont" value="${cont+1}" />
						</c:forEach>
					</tr>
					<c:set var="contador" value="${contador+1}" />
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'ADICION'}">
           <table align="left">
				<thead>
					<tr>
						<th>BITAGRAL</th>
						<th>RECORRIDO</th>
						<th>CECO</th>
						<th>IDRESP</th>
						<th>ID_PREG</th>
						<th>IDCHECK</th>
						<th>POSIBLE</th>
						<th>RUTA</th>
						<th>OBS</th>
						<th>PREG</th>
						<th>CRITICA</th>
						<th>CLASIFICA</th>
						<th>PREG_PADRE</th>
						<th>STATUS</th>
						<th>USUMODIF</th>
						<th>USU</th>
						<th>FDAUTORIZO</th>
						<th>FECHAFIN</th>
						<th>FECHAINI</th>
						<th>AUX</th>
						<th>AUX2</th>
						<th>NOMCECO</th>
						<th>ECO</th>
						<th>LIDEROBRA</th>
						<th>USURECO</th>
						<th>AREA</th>
						<th>NOMCHECK</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.bitGral}</td>
							<td>${item.recorrido}</td>
							<td>${item.ceco}</td>
							<td>${item.idRespuesta}</td>
							<td>${item.idPreg}</td>							
							<td>${item.idChecklist}</td>
							<td>${item.posible}</td>
							<td>${item.ruta}</td>
							<td>${item.obs}</td>
							<td>${item.pregunta}</td>
							<td>${item.critica}</td>	
							<td>${item.clasifica}</td>							
							<td>${item.pregPadre}</td>
							<td>${item.status}</td>
							<td>${item.usuModif}</td>
							<td>${item.usu}</td>
							<td>${item.fechaAuto}</td>
							<td>${item.fechaFin}</td>							
							<td>${item.fechaIni}</td>
							<td>${item.aux}</td>
							<td>${item.aux2}</td>
							<td>${item.nomCeco}</td>
							<td>${item.eco}</td>
							<td>${item.liderObra}</td>
							<td>${item.usuReco}</td>
							<td>${item.clasifica}</td>
							<td>${item.nomCheck}</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'NEGOBIT'}">
           <table align="left">
				<thead>
					<tr>
						<th>CECO</th>
						<th>NEGOCIO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.ceco}</td>
							<td>${item.negocio}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo =='CHECKSOPO'}">
           <table align="left">
				<thead>
					<tr>
						<th>CHECKLIST</th>
						<th>TIPOCHECK</th>
						<th>NOMBRE</th>
						<th>HORARIO</th>
						<th>VIGENTE</th>
						<th>FECHAINI</th>
						<th>ESTADO</th>
						<th>USUARIO</th>
						<th>DIA</th>
						<th>PERIODICIDAD</th>
						<th>VERSION</th>
						<th>ORDENGRUPO</th>
						<th>PONDTOTAL</th>
						<th>CLASIFICA</th>
						<th>PROTOCOLO</th>
						<th>FECHA</th>
						<th>NEGOCIO</th>
						<th>NOMUSU</th>
				
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idChecklist}</td>
							<td>${item.tipo}</td>
							<td>${item.nombreCheck}</td>
							<td>${item.idHorario}</td>
							<td>${item.vigente}</td>
							<td>${item.fecha_inicio}</td>
							<td>${item.idEstado}</td>
							<td>${item.idUsuario}</td>
							<td>${item.dia}</td>
							<td>${item.periodicidad}</td>
							<td>${item.version}</td>
							<td>${item.ordeGrupo}</td>
							<td>${item.ponderacionTot}</td>
							<td>${item.clasifica}</td>
							<td>${item.idProtocolo}</td>
							<td>${item.fecha}</td>
							<td>${item.nego}</td>
							<td>${item.nomusu}</td>
			
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo =='AUTOCHECK'}">
           <table align="left">
				<thead>
					<tr>
						<th>IDTAB</th>
						<th>VERSION</th>
						<th>IDUSU</th>
						<th>NOMBRE</th>
						<th>DETALLE</th>
						<th>STATUS</th>
						<th>TIPOUSU</th>
						<th>FECHA_LIBERACION</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idTab}</td>
							<td>${item.fiversion}</td>
							<td>${item.idUsu}</td>
							<td>${item.nomUsu}</td>
							<td>${item.detalle}</td>
							<td>${item.status}</td>
							<td>${item.usuTipo}</td>
							<td>${item.fechaLiberacion}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo =='VERCHECKSOP'}">
           <table align="left">
				<thead>
					<tr>
						<th>IDTAB</th>
						<th>VERSION</th>
						<th>VIGENTE</th>
						<th>NEGOCIO</th>
						<th>OBS</th>
					   <th>CHECKLIST</th>
					   <th>ORDENGRUPO</th>
					   <th>NOMBRECHECK</th>
						<th>USUARIO</th>
						<th>NOMUSU</th>
						<th>FECHAMODIF</th>
						<th>FECHAINICIO</th>
						<th>FECHALIBERA</th>

					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idtab}</td>
							<td>${item.version}</td>
							<td>${item.vigente}</td>
							<td>${item.nego}</td>
							<td>${item.obs}</td>
							<td>${item.idChecklist}</td>
							<td>${item.ordeGrupo}</td>
							<td>${item.nombreCheck}</td>
							<td>${item.idUsuario}</td>
							<td>${item.nomusu}</td>
							<td>${item.fechaModificacion}</td>		
							<td>${item.fecha_inicio}</td>					
							<td>${item.fechaLibera}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'DEPURA_BITACORA'}">
           <table align="left">
				<thead>
					<tr>
						<th>EJECUCION</th>
						<th>RESPUESTAS</th>
						<th>BITACORAS</th>

					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.ejecucion}</td>
							<td>${item.respEliminadas}</td>
							<td>${item.bitaEliminadas}</td>
							
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'HALLAZGOSTRANSF'}">
           <table align="left">
				<thead>
					<tr>
						<th>REGISTRO</th>
						<th>IDHALLAZGO</th>
						<th>FOLIO</th>
						<th>IDRESP</th>
						<th>STATUS</th>
						<th>IDPREG</th>
						<th>PREGUNTA</th>
						<th>RESPUESTA</th>
						<th>PREGPADRE</th>
						<th>VERSION</th>
						<th>DISPOSITIVO</th>
						<th>AREA</th>
						<th>OBSERVACION</th>
						<th>BITGRAL</th>
						<th>ARBOL</th>
						<th>RUTA</th>
						<th>FDINICIO</th>
						<th>FDFIN</th>
						<th>SLA</th>
						<th>FDAUTORIZO</th>
						<th>PERIODO</th>
						<th>CECO</th>
						<th>IDCHECK</th>
						<th>NOMBRECHECK</th>
						<th>ZONA</th>
						<th>MOTIVO RECHAZO</th>
						<th>USU_MODIFICO</th>
						<th>OBSATENC</th>
						<th>TIPOARCH</th>
						<th>PLANTILLA</th>
						<th>FECHATERMINO</th>
						<th>LIDEROBRA</th>
						<th>CRITICIDAD</th>
						<th>PONDERACION</th>
						<th>FRANQUICIA</th>
						<th>FASE</th>
						<th>OBSFASE</th>
						<th>IDFASE</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.aux2}</td>
							<td>${item.idHallazgo}</td>
							<td>${item.idFolio}</td>
							<td>${item.idResp}</td>
							<td>${item.status}</td>
							<td>${item.idPreg}</td>
							<td>${item.preg}</td>
							<td>${item.respuesta}</td>
							<td>${item.pregPadre}</td>
							<td>${item.idVersion}</td>
							<td>${item.disposi}</td>
							<td>${item.area}</td>
							<td>${item.obs}</td>
							<td>${item.bitGral}</td>
							<td>${item.arbol}</td>
							<td>${item.ruta}</td>
							<td>${item.fechaIni}</td>
							<td>${item.fechaFin}</td>
							<td>${item.sla}</td>
							<td>${item.fechaAutorizo}</td>
							<td>${item.periodo}</td>
							<td>${item.ceco}</td>
							<td>${item.idcheck}</td>
							<td>${item.nombChek}</td>
							<td>${item.zonaClasi}</td>
							<td>${item.motivrechazo}</td>
							<td>${item.usuModif}</td>
							<td>${item.obsAtencion}</td>
							<td>${item.tipoEvi}</td>
							<td>${item.totEvi}</td>
							<td>${item.fechaTermino}</td>
							<td>${item.liderObra}</td>
							<td>${item.criticidad}</td>
							<td>${item.ponderacion}</td>
							<td>${item.nomUsu}</td>
							<td>${item.nombreFase}</td>
							<td>${item.obsFase}</td>
							<td>${item.fase}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
			<c:when test="${tipo == 'INFORME'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIID_SEC</th>
						<th>FCID_CECO</th>
						<th>FIID_USUARIO</th>
						<th>FDFECHA</th>
						<th>FCRUTAINFORME</th>
						<th>FCLIDER_7S</th>
						<th>FCFIRMA_LIDER</th>
						<th>FINUMLIDER</th>
						<th>FCFIRMAFRQ</th>
						<th>FCUSUARIO_MOD</th>
						<th>FDFECHA_MOD</th>
						<th>FRCTRETRO</th>
						<th>FCID_PROTOCOLO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idsec}</td>
							<td>${item.ceco}</td>
							<td>${item.usuario}</td>
							<td>${item.fecha}</td>
							<td>${item.ruta}</td>
							<td>${item.lider}</td>
							<td>${item.firmaLider}</td>
							<td>${item.numLider}</td>
							<td>${item.firmaFrq}</td>
							<td>${item.usuarioMod}</td>
							<td>${item.fechaMod}</td>
							<td>${item.retroAlimentacion}</td>
							<td>${item.idProtocolo}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'FASEAGRU'}">
           <table align="left">
				<thead>
					<tr>
						<th>IDTAB</th>
						<th>IDPROYECTO</th>
						<th>FASE</th>
						<th>AUX</th>
						<th>OBSERVACION</th>
						<th>PERIODO</th>
						<th>AGRUPADOR</th>
						<th>STATUS</th>
						<th>ORDENFASE</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idTab}</td>
							<td>${item.proyecto}</td>
							<td>${item.fase}</td>
							<td>${item.aux}</td>
							<td>${item.obs}</td>
							<td>${item.periodo}</td>
							<td>${item.idAgrupa}</td>
							<td>${item.idactivo}</td>
							<td>${item.ordenFase}</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
				<c:when test="${tipo == 'ValidaActualizaHAlla'}">
				
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item}</td>

						</tr>
					</c:forEach>
				</tbody>	
		</c:when>
		
		<c:when test="${tipo == 'ASIGNTRANSF'}">
           <table align="left">
				<thead>
					<tr>
						<th>CECO</th>
						<th>IDPROYECTO</th>
						<th>USU</th>
						<th>USUARIOASIGN</th>
						<th>FASEACTUAL</th>
						<th>STATUS</th>
						<th>IDAGRUPA</th>
						<th>OBSERV</th>
						<th>PERIODO</th>
						<th>NOMBCECO</th>

					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.ceco}</td>
							<td>${item.proyecto}</td>
							<td>${item.usuario}</td>
							<td>${item.usuario_asig}</td>
							<td>${item.faseActual}</td>
							<td>${item.idestatus}</td>
							<td>${item.agrupador}</td>
							<td>${item.obs}</td>
							<td>${item.periodo}</td>
							<td>${item.nomCeco}</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'HALLAZGOSTRANSREPO'}">
           <table align="left">
				<thead>
					<tr>
						<th>REGISTRO</th>
						<th>IDHALLAZGO</th>
						<th>FOLIO</th>
						<th>IDRESP</th>
						<th>STATUS</th>
						<th>IDPREG</th>
						<th>PREGUNTA</th>
						<th>RESPUESTA</th>
						<th>PREGPADRE</th>
						<th>VERSION</th>
						<th>DISPOSITIVO</th>
						<th>AREA</th>
						<th>OBSERVACION</th>
						<th>BITGRAL</th>
						<th>ARBOL</th>
						<th>RUTA</th>
						<th>FDINICIO</th>
						<th>FDFIN</th>
						<th>SLA</th>
						<th>FDAUTORIZO</th>
						<th>PERIODO</th>
						<th>CECO</th>
						<th>IDCHECK</th>
						<th>NOMBRECHECK</th>
						<th>ZONA</th>
						<th>MOTIVO RECHAZO</th>
						<th>USU_MODIFICO</th>
						<th>AREA</th>
						<th>OBSATENC</th>
						<th>TIPOARCH</th>
						<th>PLANTILLA</th>
						<th>MODULO</th>
						<th>NOMUSU</th>
						<th>PONDERACION</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.aux2}</td>
							<td>${item.idHallazgo}</td>
							<td>${item.idFolio}</td>
							<td>${item.idResp}</td>
							<td>${item.status}</td>
							<td>${item.idPreg}</td>
							<td>${item.preg}</td>
							<td>${item.respuesta}</td>
							<td>${item.pregPadre}</td>
							<td>${item.idVersion}</td>
							<td>${item.disposi}</td>
							<td>${item.area}</td>
							<td>${item.obs}</td>
							<td>${item.bitGral}</td>
							<td>${item.arbol}</td>
							<td>${item.ruta}</td>
							<td>${item.fechaIni}</td>
							<td>${item.fechaFin}</td>
							<td>${item.sla}</td>
							<td>${item.fechaAutorizo}</td>
							<td>${item.periodo}</td>
							<td>${item.ceco}</td>
							<td>${item.idcheck}</td>
							<td>${item.nombChek}</td>
							<td>${item.zonaClasi}</td>
							<td>${item.motivrechazo}</td>
							<td>${item.usuModif}</td>
							<td>${item.area}</td>
							<td>${item.obsAtencion}</td>
							<td>${item.tipoEvi}</td>
							<td>${item.totEvi}</td>
							<td>${item.modulo}</td>
							<td>${item.nomUsu}</td>
							<td>${item.ponderacion}</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'PROYECTO'}">
           <table align="left">
				<thead>
					<tr>
						<th>IDPROYECTO</th>
						<th>ACTIVO</th>
						<th>NOMBRE</th>
						<th>OBS</th>
						<th>PERIODO</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idProyecto}</td>
							<td>${item.idStatus}</td>
							<td>${item.nomProy}</td>
							<td>${item.obsProy}</td>
							<td>${item.periodo}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'DASHEXPREPONVO'}">
           <table align="left">
				<thead>
					<tr>
						<th>CECO</th>
						<th>IDUSU</th>
						<th>BITAGRAL</th>
						<th>RECORRIDO</th>
						<th>FIID_BITACORA</th>
						<th>FIID_RESPUESTA</th>
						<th>OBS</th>
						<th>IDARBOL</th>
						<th>IDPOSIBLE</th>
						<th>POSIBLE</th>
						<th>IDCHECK</th>
						<th>IDPREG</th>
						<th>PONDERACION</th>
						<th>PREGUNTA</th>
						<th>CRITICA</th>
						<th>SLA</th>
						<th>IDMODULO</th>
						<th>RUTA</th>
						<th>NOMCECO</th>
						<th>NOMCHECK</th>
						<th>CLASIFICA</th>
						<th>FECHATERMINO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.ceco}</td>
							<td>${item.idUsu}</td>
							<td>${item.bitGral}</td>
							<td>${item.idRecorrido}</td>																				
							<td>${item.idBita}</td>
							<td>${item.idResp}</td>
							<td>${item.observ}</td>							
							<td>${item.idArbol}</td>
							<td>${item.idPosible}</td>
							<td>${item.posible}</td>
							<td>${item.idCheck}</td>
							<td>${item.idPreg}</td>
							<td>${item.calCheck}</td>
							<td>${item.pregunta}</td>
							<td>${item.idcritica}</td>
							<td>${item.aux}</td>	
							<td>${item.modulo}</td>
							<td>${item.ruta}</td>
							<td>${item.nombreCeco}</td>							
							<td>${item.checklist}</td>
						    <td>${item.clasif}</td>
						    <td>${item.fechaTermino}</td>
						 </tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>

		  <c:when test="${tipo == 'FIRMASCATALO'}">
                        <table align="left">
                            <thead>
                                <tr>
                                      <th>ID FIRMACAT</th>
                                    <th>NOMBREFIRMCAT</th>
                                     <th>TIPOPROY</th>
                                     <th>AGRUPAFIRMA</th>
                                     <th>CARGO</th>
                                     <th>OBLIGATORIO</th>
                                     <th>ORDEN</th>
                                     <th>PERIODO</th>
                                      <th>AUX</th>		

                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${res}" var="item">
                            <tr>
                                <td>${item.idFirmaCatalogo}</td>
                                        <td>${item.nombreFirmaCatalogo}</td>
                                         <td>${item.tipoProyecto}</td>
                                         <td>${item.idAgrupaFirma}</td>
 										<td>${item.cargo}</td>
 										<td>${item.obligatorio}</td>
 										<td>${item.orden}</td>
 										<td>${item.periodo}</td>
 										<td>${item.aux2}</td>


                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>
            
            
            
            <c:when test="${tipo == 'FRAGMENU'}">
                        <table align="left">
                            <thead>
                                <tr>
                                    <th>IDFRAGMENT</th>
                                    <th>NOMBREFRAG</th>
                                     <th>NUMFRAG</th>
                                     <th>PARAMETRO</th>
                                     <th>IDCONFIGMENU</th>
                                     <th>TITULO</th>
                                     <th>ORDENMENU</th>
                                     <th>PERFIL</th>
                                     <th>AUX</th>
                                     <th>PERIODO</th>
                                     <th>IDCONFPROY</th>
                                     <th>AGRUPAFIRM</th>
                                     <th>ORDENFRAGM</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${res}" var="item">
                                    <tr>
                                        <td>${item.idFragment}</td>
                                        <td>${item.nombreFragment}</td>
                                         <td>${item.numFragment}</td>
                                         <td>${item.parametro}</td>
 										<td>${item.idConfigMenu}</td>
 										<td>${item.tituloMenu}</td>
 										<td>${item.ordenMenu}</td>
 										<td>${item.idPerfil}</td>
 										<td>${item.aux}</td>
 										<td>${item.periodo}</td>
			 							<td>${item.idConfigProyecto}</td>
			 							<td>${item.idAgrupaFirma}</td>
			 							<td>${item.ordenFragment}</td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>
            <c:when test="${tipo == 'filtro1CecoFase'}">
           <table align="left">
				<thead>
					<tr>
						<th>CECO</th>
						<th>NOMBRECECO</th>
						<th>ACTIVO</th>
						<th>CECO_SUP</th>
						<th>DIRECCION</th>
						<th>REGION</th>
						<th>TERRITORIO</th>
						<th>ZONA</th>
						<th>FASE</th>
						<th>BITACORA</th>
						<th>PROYECTO</th>
						<th>PERIODO</th>
						<th>IDSOFT</th>
						<th>PRECALIFICACION</th>
						<th>RESPONSABLEOBRA</th>
						<th>USUARIORECO</th>
						<th>SI-NA PONDERACION</th>
						<th>RECORRIDO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">

						<tr>
						<td>${item.ceco}</td>
						<td>${item.nombreCeco}</td>
						<td>${item.activoCeco}</td>
						<td>${item.cecoSuperior}</td>
						<td>${item.direccion}</td>
						<td>${item.region}</td>
						<td>${item.territorio}</td>
						<td>${item.zona}</td>
						<td>${item.fase}</td>
						<td>${item.bitGral}</td>
						<td>${item.proyecto}</td>
						<td>${item.perido}</td>
						<td>${item.idSoft}</td>
						<td>${item.precalif}</td>
						<td>${item.nomUsu}</td>
						<td>${item.aux}</td>
						<td>${item.pondTot}</td>
					    <td>${item.idRecorrido}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
			<c:when test="${tipo == 'SOFTOPNVO'}">
           <table align="left">
				<thead>
					<tr>
						<th>IDSOFT</th>
						<th>FASE</th>
						<th>CECO</th>
						<th>PROYECTO</th>
						<th>BITACORA</th>
						<th>USUARIO</th>
						<th>CALIF</th>
						<th>CECO</th>
						<th>CANAL</th>
						<th>FASE ACTUAL</th>
						<th>RECORRIDO</th>
						<th>PERIODO</th>
	
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idSoft}</td>
							<td>${item.idFase}</td>
							<td>${item.aux}</td>
							<td>${item.idProyecto}</td>
							<td>${item.bitacora}</td>
							<td>${item.idUsuario}</td>
							<td>${item.calificacion}</td>
							<td>${item.ceco}</td>
							<td>${item.idCanal}</td>
							<td>${item.status}</td>
							<td>${item.idRecorrido}</td>
							<td>${item.periodo}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'CALCULOSSOFT'}">
           <table align="left">
				<thead>
					<tr>
					<th>idSoft</th>
			        <th>idCalculo</th>
					<th>clasificacion</th>
					<th>itemSi</th>
					<th>itemNo</th>
					<th>itemNa</th>
					<th>itemrevisados</th>
					<th>itemTotales</th>
					<th>itemImperd</th>
					<th>porcentajeReSi</th>
					<th>porcentajeReNo</th>
					<th>porcentajeToSi</th>
					<th>porcentajeToNo</th>
					<th>porcentajeToNA</th>
					<th>ponderacionNA</th>
					<th>ponderacionMaximaReal</th>
					<th>ponderacionObtenidaReal</th>
					<th>ponderacionMaximaCalculada</th>
					<th>ponderacionObtenidaCalculada</th>
					<th>calificacionReal</th>
					<th>calificacionCalculada</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
								<td>${item.idSoft}</td>
								<td>${item.idCalculo}</td>
								<td>${item.clasificacion}</td>
								<td>${item.itemSi}</td>
								<td>${item.itemNo}</td>
								<td>${item.itemNa}</td>
								<td>${item.itemrevisados}</td>
								<td>${item.itemTotales}</td>
								<td>${item.itemImperd}</td>
								<td>${item.porcentajeReSi}</td>
								<td>${item.porcentajeReNo}</td>
								<td>${item.porcentajeToSi}</td>
								<td>${item.porcentajeToNo}</td>
								<td>${item.porcentajeToNA}</td>
								<td>${item.ponderacionNA}</td>
								<td>${item.ponderacionMaximaReal}</td>
								<td>${item.ponderacionObtenidaReal}</td>
								<td>${item.ponderacionMaximaCalculada}</td>
								<td>${item.ponderacionObtenidaCalculada}</td>
								<td>${item.calificacionReal}</td>
								<td>${item.calificacionCalculada}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'ACTOREDOHALLA'}">
           <table align="left">
				<thead>
					<tr>
						<th>IDCONFIGACTOR</th>
						<th>IDPERFIL</th>
						<th>NOMBREACTOR</th>
						<th>OBSERVACION</th>
						<th>STATUS</th>
						<th>IDEDOHALLAZGO</th>
						<th>NOMSTATUS</th>
						<th>IDCONFIGURACION</th>
						<th>AUX</th>
						<th>COLOR</th>
						<th>PERIODO</th>
	
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idActorConfig}</td>
							<td>${item.idPerfil}</td>
							<td>${item.nombreActor}</td>
							<td>${item.observacion}</td>
							<td>${item.status}</td>
							<td>${item.idEdoHallazgo}</td>
							<td>${item.nombStatus}</td>
							<td>${item.idConfiguracion}</td>
							<td>${item.aux}</td>
							<td>${item.color}</td>
							<td>${item.periodo}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'CONFACTORHALLA'}">
           <table align="left">
				<thead>
					<tr>
						<th>IDHHALLACONFIG</th>
						<th>IDCONFACTOR</th>
						<th>STATUSACTUACION</th>
						<th>ACCION</th>
						<th>STATUSACCION</th>
						<th>BANDATENDER</th>
						<th>OBS</th>
						<th>IDCONFIG</th>
						<th>AUX</th>
						<th>PERIODO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idHallazgoConf}</td>
							<td>${item.idConfigActor}</td>
							<td>${item.statusActuacion}</td>
							<td>${item.accion}</td>
							<td>${item.statusAccion}</td>
							<td>${item.banderaAtencion}</td>
							<td>${item.obs}</td>
							<td>${item.idConfig}</td>
							<td>${item.aux}</td>
							<td>${item.periodo}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'RESPUESTASSOFT'}">
           <table align="left">
				<thead>
					<tr>
						<th>NUM</th>
						<th>BITACORA</th>
						<th>IDRESPUESTA</th>
						<th>IDARBOL</th>
						<th>OBSERVACIONES</th>
						<th>IDPREG</th>
						<th>IDCHECK</th>
						<th>IDPOSIBLE</th>
						<th>REQOBS</th>
						<th>POSIBLE</th>
						<th>PREGUNTA</th>
						<th>CRITICA</th>
						<th>RUTA</th>
						<th>CHECK_USUA</th>
						<th>ID_USU</th>
						<th>CECO</th>
						<th>NOMCHECK</th>
						<th>ORDENGRUP</th>
						<th>MODULO</th>
						<th>PRECALIF</th>
						<th>CALIFICA</th>
						<th>PONTTOTAL</th>
						<th>CLASIFICA</th>
						<th>PONDERACION</th>
						<th>IDMODULO</th>
					
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						<td>${item.aux2}</td>
						<td>${item.idBita}</td>
						<td>${item.idResp}</td>
						<td>${item.idArbol}</td>
						<td>${item.observ}</td>
						<td>${item.idPreg}</td>
						<td>${item.idCheck}</td>
						<td>${item.idPosible}</td>
						<td>${item.idObserv}</td>
						<td>${item.posible}</td>
						<td>${item.pregunta}</td>
						<td>${item.idcritica}</td>
						<td>${item.ruta}</td>
						<td>${item.idcheckUsua}</td>
						<td>${item.idUsu}</td>
						<td>${item.ceco}</td>
						<td>${item.checklist}</td>
						<td>${item.idOrdenGru}</td>
						<td>${item.modulo}</td>
						<td>${item.precalif}</td>
						<td>${item.calif}</td>
						<td>${item.ponTot}</td>
						<td>${item.clasif}</td>
						<td>${item.ponderacion}</td>
						<td>${item.idModulo}</td>
						
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'ActaCecoFaseProy'}">
           <table align="left">
				<thead>
					<tr>
						<th>COLUMNA</th>
						<th>BITACORA</th>
						<th>IDRESP</th>
						<th>IDARBOL</th>
						<th>OBSERVACIONES</th>
						<th>IDPREG</th>
						<th>IDCHECK</th>
						<th>IDPOSIBLE</th>
						<th>POSIBLE</th>
						<th>PREGUNTA</th>
						<th>IDIMPERD</th>
						<th>CLASIFICA</th>
						<th>SLA</th>
						<th>RUTA</th>
						<th>LIDEROBRA</th>
						<th>FECHAINI</th>
						<th>FDTERMINO</th>
						<th>NOMBREREALIZO</th>
						<th>IDUSUARIO</th>
						<th>NOMBCECO</th>
						<th>ECO</th>
						<th>CECO</th>
						
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						<td>${item.aux2}</td>
						<td>${item.idBita}</td>
						<td>${item.idResp}</td>
						<td>${item.idArbol}</td>
						<td>${item.observ}</td>
						<td>${item.idPreg}</td>
						<td>${item.idCheck}</td>
						<td>${item.idPosible}</td>
						<td>${item.posible}</td>
						<td>${item.pregunta}</td>
						<td>${item.idcritica}</td>
						<td>${item.clasif}</td>
						<td>${item.sla}</td>
						<td>${item.ruta}</td>
						<td>${item.liderObra}</td>
						<td>${item.fechaIni}</td>
						<td>${item.fechaFin}</td>
						<td>${item.realizoReco}</td>
						<td>${item.idUsu}</td>
						<td>${item.nombCeco}</td>
						<td>${item.eco}</td>
						<td>${item.ceco}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'MatrizHallazgos'}">
           <table align="left">
				<thead>
					<tr>
						<th>IDMATRIZ</th>
						<th>TIPOPROY</th>
						<th>STATUSENVIO</th>
						<th>STATUSHALLAZGO</th>
						<th>IDPERFIL</th>
						<th>COLOR</th>
						<th>NOMBRESTATUS</th>
						<th>PERIODO</th>
					
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						<td>${item.idMatriz}</td>
						<td>${item.tipoProy}</td>
						<td>${item.statusEnvio}</td>
						<td>${item.statusHallazgo}</td>
						<td>${item.idPerfil}</td>
						<td>${item.color}</td>
						<td>${item.nombreStatus}</td>
						<td>${item.periodo}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'ConteoHallazgos'}">
           <table align="left">
				<thead>
					<tr>
						<th>CONTEOGRAL</th>
						<th>CONTEO_PORATENDER</th>
						<th>CONTEO_PORAUTORIZAR</th>
						<th>CONTEO_ATENDIDO</th>
						<th>CONTEO_RECHAZADO</th>
						<th>NEGOCIO</th>
						<th>CECO</th>
						<th>NOMCECO</th>
						<th>FASE</th>
						<th>NOMFASE</th>
						<th>PROYECTO</th>
						<th>NOMPROY</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						<td>${item.conteoTotal}</td>
						<td>${item.conteoPorAtender}</td>
						<td>${item.conteoPorAutorizar}</td>
						<td>${item.conteoAtendido}</td>
						<td>${item.conteoRechazado}</td>
						<td>${item.negocio}</td>
						<td>${item.ceco}</td>
						<td>${item.nombreCeco}</td>
						<td>${item.fase}</td>
						<td>${item.nombreFase}</td>
						<td>${item.proyecto}</td>
						<td>${item.nombreProyecto}</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'CheckInAsistencia'}">
           <table align="left">
				<thead>
					<tr>
						<th>IDCHECKASIST</th>
						<th>IDUSUARIO</th>
						<th>LATITUD</th>
						<th>LONGITUD</th>
						<th>FECHA</th>
						<th>OBS</th>
						<th>DESCUENTO</th>
						<th>RUTA</th>
						<th>LUGAR</th>
						<th>TIPOPROYECTO</th>
						<th>TIPOCHECKIN</th>
						<th>TIPOEASISTENCIA</th>
						<th>PERIODO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						<td>${item.idAsistencia}</td>
						<td>${item.idUsuario}</td>
						<td>${item.latitud}</td>
						<td>${item.longitud}</td>
						<td>${item.fecha}</td>
						<td>${item.observaciones}</td>
						<td>${item.idDescuento}</td>
						<td>${item.ruta}</td>
						<td>${item.lugar}</td>
						<td>${item.idTipoProyecto}</td>
						<td>${item.tipoCheckIn}</td>
						<td>${item.tipoAsistencia}</td>
						<td>${item.periodo}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'ProgramacionesMtto'}">
           <table align="left">
				<thead>
					<tr>
						<th>FIID_HISTORICO</th>
						<th>FCID_CECO</th>
						<th>NOMBRECECO</th>
						<th>FIID_PROYECTO</th>
						<th>NOMBREPROYECTO</th>
						<th>FIID_USUARIO</th>
						<th>FCOBSERVACION</th>
						<th>PROGRAMACION</th>
						<th>FECHAINICIAL</th>
						<th>CECOSUP</th>
						<th>FCNEGOCIO</th>
		
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						<td>${item.idProgramacion}</td>
						<td>${item.idCeco}</td>
						<td>${item.nombreCeco}</td>
						<td>${item.idProyecto}</td>
						<td>${item.nombreProyecto}</td>
						<td>${item.idUsuario}</td>
						<td>${item.comentarioReprogramacion}</td>
						<td>${item.fechaProgramacion}</td>
						<td>${item.fechaProgramacionInicial}</td>
						<td>${item.cecoSuperior}</td>
						<td>${item.negocio}</td>
						
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
				<c:when test="${tipo == 'DatosInforme'}">
           <table align="left">
				<thead>
					<tr>
						<th>CECO</th>
						<th>FECHA</th>
						<th>USUARIO</th>
						<th>IDINFORME</th>
						<th>PROTOCOLO</th>
						<th>TOTAL_IMPERD</th>
						<th>TOTAL_RESPUESTA</th>
						<th>CALIFICACION</th>
						<th>PERIODO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
						<td>${item.idCeco}</td>
						<td>${item.fecha}</td>
						<td>${item.idUsuario}</td>
						<td>${item.idInforme}</td>
						<td>${item.idProtocolo}</td>
						<td>${item.imperdonables}</td>
						<td>${item.totalRespuestas}</td>
						<td>${item.calificacion}</td>
						<td>${item.periodo}</td>
				
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:otherwise>
		<p>ERROR!!!!</p>
		</c:otherwise>
	</c:choose>
</body>
</html>
