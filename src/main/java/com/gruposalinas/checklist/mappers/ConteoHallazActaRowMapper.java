package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.AsignaTransfDTO;

public class ConteoHallazActaRowMapper implements RowMapper<AsignaTransfDTO> {

	public AsignaTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AsignaTransfDTO AsignaTransfDTO = new AsignaTransfDTO();
		
		AsignaTransfDTO.setCeco(rs.getInt("FCID_CECO"));;
		AsignaTransfDTO.setProyecto(rs.getInt("FIID_PROYECTO"));
		AsignaTransfDTO.setFase(rs.getInt("FIID_FASE"));
		AsignaTransfDTO.setConteoHallazgos(rs.getInt("HALLAZGOS"));;
	    AsignaTransfDTO.setVersion(rs.getInt("IDVERSION"));
	    AsignaTransfDTO.setObsFase(rs.getString("NOMBREFASE"));
	    AsignaTransfDTO.setObs(rs.getString("OBSERVACION"));;
	   

		return AsignaTransfDTO;
	}

}


