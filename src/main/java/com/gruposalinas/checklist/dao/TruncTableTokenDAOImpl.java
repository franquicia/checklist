package com.gruposalinas.checklist.dao;

import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TruncTableTokenDAOImpl extends DefaultDAO implements TruncTableTokenDAO {

    private Logger logger = LogManager.getLogger(TruncTableTokenDAOImpl.class);

    private DefaultJdbcCall jdbcTruncTableToken;

    public void init() {

        jdbcTruncTableToken = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_TRUNCTABLE")
                .withProcedureName("SPTRUNCTABLE");

    }

    @Override
    public boolean truncTable() throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        out = jdbcTruncTableToken.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_TRUNCTABLE.SPTRUNCTABLE}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al truncar la tabla");
        } else {
            return true;

        }

        return false;
    }
}
