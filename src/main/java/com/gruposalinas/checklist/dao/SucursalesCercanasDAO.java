package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.NegociosDentroDTO;
import com.gruposalinas.checklist.domain.PuntosCercanosDTO;
import com.gruposalinas.checklist.domain.SucursalesCercanasDTO;

public interface SucursalesCercanasDAO {
		
	public List<SucursalesCercanasDTO> obtieneSucursales(String latitud, String longitud) throws Exception;
	
	public List<PuntosCercanosDTO> obtieneSucursalesCercanas(String latitud, String longitud) throws Exception;

	public List<NegociosDentroDTO> obtieneNegociosDentro(String economico) throws Exception;
	
}
