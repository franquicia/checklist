package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;

public interface PreguntaAdmDAO {
	
	//MODULOS
	public List<ModuloDTO> obtieneModulo() throws Exception;
	
	public List<ModuloDTO> obtieneModulo(int idModulo) throws Exception;
	
	public int insertaModulo(ModuloDTO bean) throws Exception;
	
	public boolean actualizaModulo (ModuloDTO bean) throws Exception;
	
	public boolean eliminaModulo(int idModulo) throws Exception;
	
	//CHECKLIST 

	public int insertaChecklistCom(ChecklistDTO bean) throws Exception;
	
	public int insertaChecklist(ChecklistProtocoloDTO checklist) throws Exception;
	
	public boolean actualizaChecklistCom(ChecklistDTO bean) throws Exception;
	
	public boolean eliminaChecklist(int idCheckList) throws Exception;
	
	public List<ChecklistDTO> buscaChecklist() throws Exception;
	
	public List<ChecklistDTO> buscaChecklist(int idChecklist) throws Exception;
	
	//PREGUNTAS

	public List<PreguntaDTO> obtienePregunta() throws Exception;
	
	public List<PreguntaDTO> obtienePregunta(int idPregunta) throws Exception;
	
	public int insertaPregunta(PreguntaDTO bean) throws Exception;
	
	//imperdonables
	public int insertaPreguntaCom(PreguntaDTO bean) throws Exception;
	
	//imperdonables
	public boolean actualizaPreguntaCom(PreguntaDTO bean) throws Exception;
	
	public boolean actualizaPregunta(PreguntaDTO bean) throws Exception;
	
	public boolean eliminaPregunta(int idPregunta) throws Exception;
	
	//CHECK_PREG

	public boolean insertaChecklistPregunta(ChecklistPreguntaDTO bean)throws Exception;
	
	public boolean actualizaChecklistPregunta(ChecklistPreguntaDTO bean)throws Exception;
	
	public boolean eliminaChecklistPregunta(int idChecklist, int idPregunta)throws Exception;
	
	public List<ChecklistPreguntaDTO> buscaPreguntas(int idChecklist)throws Exception;
	
	public List<ChecklistPreguntaDTO> obtienePregXcheck(int idChecklist) throws Exception;
	
}
