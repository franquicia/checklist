package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.PerfilDTO;
import com.gruposalinas.checklist.mappers.PerfilRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PerfilDAOImpl extends DefaultDAO implements PerfilDAO {

    private static Logger logger = LogManager.getLogger(PerfilDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtienePerfil;
    DefaultJdbcCall jdbcInsertaPerfil;
    DefaultJdbcCall jdbcActualizaPerfil;
    DefaultJdbcCall jdbcEliminaPerfil;
    DefaultJdbcCall jdbcNuevoPerfil;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PERFIL")
                .withProcedureName("SP_SEL_G_PERFIL")
                .returningResultSet("RCL_PERFIL", new PerfilRowMapper());

        jdbcObtienePerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PERFIL")
                .withProcedureName("SP_SEL_PERFIL")
                .returningResultSet("RCL_PERFIL", new PerfilRowMapper());

        jdbcInsertaPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PERFIL")
                .withProcedureName("SP_INS_PERFIL");

        jdbcActualizaPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PERFIL")
                .withProcedureName("SP_ACT_PERFIL");

        jdbcEliminaPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PERFIL")
                .withProcedureName("SP_DEL_PERFIL");

        jdbcNuevoPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNAPERFIL")
                .withProcedureName("SP_INSERTPERF");
    }

    @SuppressWarnings("unchecked")
    public List<PerfilDTO> obtienePerfil() throws Exception {
        Map<String, Object> out = null;
        List<PerfilDTO> listaPerfil = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_PERFIL.SP_SEL_G_PERFIL}");
        listaPerfil = (List<PerfilDTO>) out.get("RCL_PERFIL");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Perfiles");
        } else {
            return listaPerfil;
        }

        return listaPerfil;
    }

    @SuppressWarnings("unchecked")
    public List<PerfilDTO> obtienePerfil(int idPerfil) throws Exception {
        Map<String, Object> out = null;
        List<PerfilDTO> listaPerfil = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_PERFIL", idPerfil);

        out = jdbcObtienePerfil.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_PERFIL.SP_SEL_PERFIL}");
        listaPerfil = (List<PerfilDTO>) out.get("RCL_PERFIL");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Perfil con id(" + idPerfil + ")");
        } else {
            return listaPerfil;
        }

        return null;

    }

    public int insertaPerfil(PerfilDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idPerfil = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_DESCRIPCION", bean.getDescripcion());

        out = jdbcInsertaPerfil.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_PERFIL.SP_INS_PERFIL}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idPerfilReturn = (BigDecimal) out.get("PA_IDPERFIL");
        idPerfil = idPerfilReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Perfil");
        } else {
            return idPerfil;
        }

        return idPerfil;

    }

    public boolean actualizaPerfil(PerfilDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDPERFIL", bean.getIdPerfil())
                .addValue("PA_DESCRIPCION", bean.getDescripcion());

        out = jdbcActualizaPerfil.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_PERFIL.SP_ACT_PERFIL}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el Perfil  id( " + bean.getIdPerfil() + ")");
        } else {
            return true;
        }

        return false;

    }

    public boolean eliminaPerfil(int idPerfil) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDPERFIL", idPerfil);

        out = jdbcEliminaPerfil.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_PERFIL.SP_DEL_PERFIL}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el Perfil id(" + idPerfil + ")");
        } else {
            return true;
        }

        return false;
    }

    public int[] agregaNuevoPerfil() throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;
        int afectados = 0;

        int[] respuesta = new int[2];

        out = jdbcNuevoPerfil.execute();

        //logger.info("Funcion ejecutada: {checklist.PAASIGNAPERFIL.SP_INSERTPERF}");
        BigDecimal returnEjec = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjec.intValue();

        BigDecimal returnAfectados = (BigDecimal) out.get("PA_REGISTROS");
        afectados = returnAfectados.intValue();

        respuesta[0] = ejecucion;
        respuesta[1] = afectados;

        return respuesta;
    }
}
