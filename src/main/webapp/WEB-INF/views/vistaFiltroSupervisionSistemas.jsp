<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="es">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
	<title>Supervisi&oacute;n Sucursales</title>
	<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/estilos.css">
	<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/menu.css">
	<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/secciones.css">
	<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/dropkick.css">
	<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/paginador.css" media="screen">
	<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/calendario.css" media="screen">

	<style type="text/css">
	#tags{
	width: 100%;
	border: none;
	border-bottom: 1px solid #d7d7d7;
	}
	</style>
	
</head>

<body>

<div class="page">
	<div class="header">
		<div class="tblHeader">
		 <div class="headerMenu">
		 	<a href="#"  id="hamburger">
				<img src="../images/supervisionSistemas/btn_hamburguer.svg" alt="Men� principal" class="imgHamburgesa">
			</a>
		 </div>
		 <div class="headerLogo">
				<a href="index.html">
					<img src="../images/supervisionSistemas/logo-baz.svg"  id="imgLogo">
				</a>
		</div>
		   <div class="headerUser">
				<div class="name">
					<b>${user.nombre}</b><br>
					<c:if test="${user.admin == true}">Administrador</c:if>
					<c:if test="${user.admin == false}">Usuario</c:if>
				</div>
				<div class="pic-name">
					<div class="pic" style="background-image: url('https://portal.socio.gs/foto/elektra/empleados/${user.idUsuario}.jpg'), url('../images/supervisionSistemas/person-icon.jpg');"></div>
					<span class="tCenter">0</span>
				</div>
				<div class="user-menu">
					<a href="#"><img src="../images/supervisionSistemas/btn_desp_menu.jpg" class="configuraciones"></a>
				</div>
				<div class="fecha">
					<c:set var = "now" value = "<%= new java.util.Date()%>" />
					<b><fmt:formatDate type="date" dateStyle = "short" value = "${now}" /></b><br>
					<span><fmt:formatDate type="time" timeStyle="short" value = "${now}" />Hrs. | CDMX</span>
				</div>
			</div>
			<div class="contConfig">
				<div class="divConfig">
					<a href="#" class="divConfig1 uno">
						<div>Cambiar contrase�a</div>
						<div><img src="../images/supervisionSistemas/ico1.png" class="imgConfig imgIco1"></div>
					</a>
					
					<a href="#" class="divConfig1 dos">
						<div>Cerrar sesi�n</div>
						<div><img src="../images/supervisionSistemas/ico2.svg" class="imgConfig imgIco2"></div>
					</a>
				</div>	
			</div>
		</div>
	</div>
	<div class="clear"></div>

	<div class="title" id="title">
		<div class="wrapper">
			<div class="h2">
			<a href="#">P�gina Principal </a> /
			<a href="#"><c:out value="${nombreCeco}"/></a>
			</div>
			<h1><c:out value="${nombreCeco}"/> </h1>
		</div>
		<div class="divlupa">
			<img src="../images/supervisionSistemas/interrogacion.png" class="lupa">
		</div>
	</div>
   
   <div class="clear"></div>    
	
	<!-- Menu -->
	<div id="effect" class="mismoalto ui-widget-content ui-corner-all">
		<div id="menuPrincipal">
			<div class="header-usuario">
				<div id=foto><img src="../images/supervisionSistemas/logo-baz2.svg" class="imgLogo"></div>
				<div id="inf-usuario">
					<span>Cuadre de inventario f�sico</span>
					<p>Men� Principal<br>
				</div>
				<div class="w87"><input class="buscar" type="text"></div>
			</div>
			<div class="c-mright" id="menu">
				<ul class="l-navegacion nivel1">
					<li class="has-sub">
						<a href="#">
							<p>Nivel1</p>
							<div class="flecha"></div>
						</a>
						<ul class="nivel2">
							<li class="has-sub">
								<a title="" href="#">
									<p>Nivel2</p>
									<div class="flecha"></div>
								</a>
								<ul class="nivel3">
									<li class="has-sub liSimple">
										<a title="" href="#">
											<p>Nivel3</p>
										</a>
									</li>
									
								</ul>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Contenido -->
	<div class="contHome">
	<form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
		<table class="tblBusquedas">
		  <tbody>
			<tr>
			  <td>Per�odo:</td>
			  <td><input type="text" placeholder="--/--/--" class="datapicker1 date"  id="datepicker2" ></td>
			  <td><input type="text" placeholder="--/--/--" class="datapicker1 date"  id="datepicker3" ></td>
			  <td>
				<select class="normal_select" id="selCheck" >
				<option value="">Seleccionar checklist</option>
					<c:forEach var="k" begin="0" end="${(fn:length(listaFiltros))-1}">
						<option value="${listaFiltros[k].idCecos}">${listaFiltros[k].nombreCeco}</option>
					</c:forEach>
				</select>
			  </td>
			  <td>
					<div class="ui-widget buscar1">
  						<input id="tags" class="buscar1">
					</div>
			  </td>
			  <td rowspan="2">
			  <input type="submit" value="" class="btnbuscar"/>
			  </td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td>Desde</td>
			  <td>Hasta</td>
			  <td>Cheklist</td>
			  <td>Sucursales</td>
			</tr>
		  </tbody>
		</table>
	</form>
		<div class="titulo titFecha">Resultados de la b�squeda por Periodo:<span id="periodoBusqueda"> 15/02/2018  - 16/02/2018</span></div>
		<div class="titulo titChecklist">Resultados de la b�squeda por Cheklist - Caja</div>
		<div class="titulo titSucursal">
			<div>Sucursal:<span> BA San Miguel Allende</span></div>
			<div>Evaluado por:<span id="nombreEvaluador"> Alberto Antonio Ch�vez Cedillo</span></div>
			<div>Fecha:<span id="fechaEvaluacion"> 19/02/2018</span></div>
		</div>
		<div id="paginationdemo" class="demo">
			<!-- Pagina 1 -->
			<div id="p1" class="pagedemo _current" style="display: block;">
				
				<table class="tblGeneral" id="busqueda">
				  <tbody>
				  <c:if test="${fn:length(listaReportesSicap) > 0}">
					  	<c:forEach var="i" begin="0" end="${(fn:length(listaReportesSicap))-1}">
							<tr style='cursor: pointer;' onclick='detalleBitacora("${listaReportesSicap[i].idBitacora}")' ><td>${listaReportesSicap[i].finicio}</td><td>${listaReportesSicap[i].evaluado}</td><td>${listaReportesSicap[i].sucursal}</td><td>${listaReportesSicap[i].region}</td><td>${listaReportesSicap[i].checklist}</td>
						</c:forEach>
					</c:if>
				<c:if test="${fn:length(listaReportesSicap)== 0}">
				</c:if>
				  </tbody>
				</table>
			</div>
			<div id="p2" class="pagedemo">Page 2</div>
			<div id="p3" class="pagedemo">Page 3</div>
			<div id="demo5" class="jPaginate" style="padding-left: 76px;">
				<div class="jPag-control-back">
					<a class="jPag-first">First</a>
					<span class="jPag-sprevious">�</span>
				</div>
				<div style="overflow: hidden; width: 186px;">
					<ul class="jPag-pages" style="width: 273px;">
						<li><span class="jPag-current">1</span></li>
						<li><a>2</a></li>
						<li><a>3</a></li>
			   </div>
			   <div class="jPag-control-front" style="left: 266px;">
					<span class="jPag-snext">�</span>
					<a class="jPag-last">Last</a>
				</div>
				
			</div>
			<a href="#" class="btnG">Exportar </a>
		</div>
		<div class="clear"></div>
		<div class="divSucursal"> 
			<div class="colS1">
				<table class="tblGeneral p4" id=tablaPreguntas>
				  <tbody>
				</table>
				<div class="clear"></div><br>
				<textarea placeholder="Comentarios adicionales"></textarea>
			</div>
			<div class="colS2" id="contenedorEvidencia">
				<table class="tblGeneral" >
				  <tbody>
					<tr class="rdSecc">
					  <td class="titTabla">Evidencias</td>
					</tr>
				</table>
			</div>
		</div>
		
		<div class="clear"></div>
			<div class="aviso"> </div>			
<!-- 			implementando el boton regresar -->
			<div class="GridBotonRegresar" style="position:fixed;bottom:80px;right:10px"> <button onclick="javascript:window.history.back()" style="background-color: #006341;padding: 5px;line-height: 15px;display: block;float: right;border: 0px; color:white;">Regresar</button></div>
	</div>
	
	<div class="clear"></div>
	<div class="footer1">
		<table class="tblFooter1">
		  <tbody>
			<tr>
			  <td>Sistema Reportes</td>
			  <td><a href="#" class="btnRojo">Malas Pr�cticas</a></td>
			</tr>
		  </tbody>
		</table>
	</div>
	<div class="footer">
		<table class="tblFooter">
		  <tbody>
			<tr>
			  <td>Banco Azteca S.A. Instituci�n de Banca M�ltiple</td>
			  <td>Derechos Reservados 2014 (T�rminos y Condiciones de uso del Portal.</td>
			</tr>
		  </tbody>
		</table>
	</div>
		
</div>
</body>
</html>

<script type="text/javascript" src="../js/supervisionSistemas/jquery-1.12.4.min.js"></script> 
<script type="text/javascript" src="../js/supervisionSistemas/jquery.dropkick.js"></script>
<script type="text/javascript" src="../js/supervisionSistemas/content_height.js"></script>
<script type="text/javascript" src="../js/supervisionSistemas/jquery-ui.js"></script> 

<script type="text/javascript" src="../js/supervisionSistemas/jquery.paginate.js"></script>
<script type="text/javascript" src="../js/supervisionSistemas/funciones.js"></script> 

<script type="text/javascript">
$(function() {
	$("#demo5").paginate({
		count 		: 3,
		start 		: 1,
		display     : 3,
		border					: false,
		border_color			: false,
		text_color  			: '#8a8a8a',
		background_color    	: 'transparent',	
		border_hover_color		: 'transparent',
		text_hover_color  		: '#000',
		background_hover_color	: 'transparent', 
		images					: true,
		mouse					: 'press',
		onChange     			: function(page){
									$('._current','#paginationdemo').removeClass('_current').hide();
									$('#p'+page).addClass('_current').show();
								  }
	});
	
	<c:if test="${fn:length(listaReportesSicap) > 0}">
		carga();	
		$(".titFecha").show();
		$("#paginationdemo").show();
	</c:if>
	<c:if test="${fn:length(listaReportesSicap)== 0}">
		$(".aviso").text('No existen resultados para esa directiva');
	</c:if>
	
	$( "#tags" ).autocomplete({
	      source: '../ajaxFiltroAutocompletaSucursal.json'
	    });
	
});
</script>