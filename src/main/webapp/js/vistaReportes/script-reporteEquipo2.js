$body = $("body");
$(document).on({
    ajaxStart: function() {$body.addClass("loading");},
    ajaxStop: function() {$body.removeClass("loading");}    
});

var arreglo = [];
var porcentajeL=0;
var validaCheck=0;
var intervalo=null;
var intervaloDespliegue=null;
var urlCache = "";
var filaHijo=0;

$(window).load(function() {
	arreglo = [];
	//document.getElementById("tipoModal").className="modal1";
	intervalo = setInterval("reporte()",300000);
});

function reporte(){
	modal = 1;
	document.getElementById("tipoModal").className="modal3";
	location.href = "/checklist/reportes/reporteEquipoDos.htm";
}

function myFunction() {
	$body.removeClass("loading1");
	location.reload();
}

function nobackbutton(){
	window.location.hash="no-back-button";
	window.location.hash="Again-No-back-button" //chrome
	window.onhashchange=function(){window.location.hash="";}
}

function muestraZonas(fila, idCeco, nomCeco, tam , nivel){

	//alert(fila + ' - ' + idCeco + ' - ' + nomCeco + ' - ' + tam + ' - ' + nivel);
	
	var ban=false;
	$.ajax({
		type : "GET",
		url :'/checklist/reportes/pruebaConsultaRH.json?idCeco='+idCeco+'&nomCeco='+nomCeco,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async:false,
		delimiter : ",",
		success : function(json) {
			if (json.rh == null) {
				ban=true;
				//alert('Sin Datos');
				//$("#idLlenaZonas"+idCeco).append("<span class='Gris1'>Sin Datos</span>");
			}else{
				ban=false;	
				//alert('a llenar ' + arreglo);
				arreglo.push(json);
				//alert(arreglo[arreglo.length-1].idCeco +" - " +arreglo[arreglo.length-1].descCeco)
				
				if(arreglo.length == (tam)){
					//alert ('entro a pintar');
					//var matriz = metodoOrdenaObjetos(arreglo);
					var matriz=arreglo;

					//setTimeout(function(){
						
						for(var i=0; i<tam; i++){
						
							document.getElementById("nom"+nivel+i).innerHTML = "";
							document.getElementById("pla"+nivel+i).innerHTML = "";
							document.getElementById("rot"+nivel+i).innerHTML = "";
							document.getElementById("ase"+nivel+i).innerHTML = "";
							document.getElementById("cer"+nivel+i).innerHTML = "";
							document.getElementById("tar"+nivel+i).innerHTML = "";
							
							//alert (matriz[i].idCeco + "-" +matriz[i].descCeco + "   Plantilla: " + matriz[i].rh.plantilla);
							
								
							$("#nom"+nivel+""+i).append(matriz[i].descCeco+"<span>suc</span>");
							
							if(matriz[i].rh.plantilla>=100){
								$("#pla"+nivel+""+i).append("<div class='radio verdeN'>"+matriz[i].rh.plantilla+"%</div>");
							}else if(matriz[i].rh.plantilla>=90 && matriz[i].rh.plantilla<100){
								$("#pla"+nivel+""+i).append("<div class='radio amarilloN' style='color:black;'>"+matriz[i].rh.plantilla+"%</div>");
							}else if(matriz[i].rh.plantilla < 90){
								$("#pla"+nivel+""+i).append("<div class='radio rojo'>"+matriz[i].rh.plantilla+"%</div>");
							}
							
							
							if(matriz[i].rh.rotacion <= 40){
								$("#rot"+nivel+i).append("<div class='radio verdeN'>"+matriz[i].rh.rotacion+"%</div>");
							}else if(matriz[i].rh.rotacion > 40){
								$("#rot"+nivel+i).append("<div class='radio amarilloN' style='color:black;'>"+matriz[i].rh.rotacion+"%</div>");
							}
							
							
							if(matriz[i].rh.asesores == 0){
								$("#ase"+nivel+i).append("<div Class='radio verdeN'>"+matriz[i].rh.asesores+"</div>");
							/*}else if(matriz[i].rh.asesores > 0 && json.rh.asesores < 11){
								$("#ase"+nivel+i).append("<div Class='radio amarilloN'>"+matriz[i].rh.asesores+"</div>");*/
							}else if(matriz[i].rh.asesores > 0){
								$("#ase"+nivel+i).append("<div Class='radio rojo'>"+matriz[i].rh.asesores+"</div>");
							}
							
							
							if(matriz[i].rh.certificados >= 100){
								$("#cer"+nivel+i).append("<div Class='radio verdeN'>"+matriz[i].rh.certificados+"%</div>");
							}else if(matriz[i].rh.certificados >=90 && matriz[i].rh.certificados < 100){
								$("#cer"+nivel+i).append("<div Class='radio amarilloN' style='color:black;'>"+matriz[i].rh.certificados+"%</div>");
							}else if(matriz[i].rh.certificados < 90){
								$("#cer"+nivel+i).append("<div Class='radio rojo'>"+matriz[i].rh.certificados+"%</div>");
							}

							
							$("#tar"+nivel+i).append("<div Class='radio rojo'>"+matriz[i].rh.tareas+"%</div>");
						
							if(i == (tam-1)){
								document.getElementById("tipoModal").className="modal1";
							}
						
						}
						
					//},5000);
							
				}
			}
			
			if(fila == (tam-1)){
				if(ban){
					//alert('Sin Datos de RH');
					document.getElementById("tipoModal").className="modal1";
				}
			}		
		},
		error : function() {
			//alert('Favor de actualizar la pagina');
			if(fila == (tam-1)){
				//alert('Sin Datos de RH');
				document.getElementById("tipoModal").className="modal1";
			}
		}
	});	
}

function metodoOrdenaObjetos(matriz){
	
	var flag = false;
	var l =1;
	var sumaAct;
	var sumaAnt;
	var act;
	var ant;
	
	while(l < matriz.length){

		sumaAnt = (matriz[l-1].rh.plantilla) - (matriz[l-1].rh.rotacion)  - (matriz[l-1].rh.asesores)  + (matriz[l-1].rh.certificados) + (matriz[l-1].rh.tareas);
		sumaAct = (matriz[l].rh.plantilla) - (matriz[l].rh.rotacion) - (matriz[l].rh.asesores) + (matriz[l].rh.certificados) + (matriz[l].rh.tareas);

		if(sumaAnt < sumaAct){
			ant=matriz[l-1];
			act=matriz[l];
			matriz[l - 1] = act;
			matriz[l] = ant;
			flag=true;
		}

		if (flag && (l + 1) == matriz.length) {
			l = 0;
			flag = false;
		}
		l++;
	}
	
	return matriz;
}

function despliega(id, fila){
	despliegaCheck(id, fila);
	modal = 2;
	filaHijo=fila;
	//LLAMAR A AJAX PARA OBTENER LOS DATOS	
}

function despliegaCheck(idCeco, fila){
	
	if(validaCheck!=idCeco){ 
		
		document.getElementById("tipoModal").className="modal3";
		
		arreglo = [];

		validaCheck=idCeco;
		document.getElementById("idLlenaZonas"+idCeco).innerHTML = "";

		$.ajax({
			type : "GET",
			url : '/checklist/reportes/ajaxBuscaCecosPA.json?idCeco='+idCeco,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {
				
				if (json == null) {
					//alert('Sin Datos');
					$("#idLlenaZonas"+idCeco).append("<span class='Gris1'>Sin Datos</span>");
					document.getElementById("tipoModal").className="modal1";
				}else{					

					for (var a = 0; a < Object.keys(json).length; a++) {	
						
						$("#idLlenaZonas"+idCeco).append("<table class='bloquer subnivel'><tbody><tr id='"+json[a].idCeco+"' class='pointer'><td id='nom"+(fila+1)+(fila+1)+(a)+"' class='nombreCheck'>"+json[a].descCeco+"</td>");
			
						$("#"+json[a].idCeco).append("<td id='pla"+(fila+1)+(fila+1)+(a)+"'><div class='radio grisN'>-<div></td>");
						$("#"+json[a].idCeco).append("<td id='rot"+(fila+1)+(fila+1)+(a)+"'><div class='radio grisN'>-<div></td>");
						$("#"+json[a].idCeco).append("<td id='ase"+(fila+1)+(fila+1)+(a)+"'><div class='radio grisN'>-<div></td>");
						$("#"+json[a].idCeco).append("<td id='cer"+(fila+1)+(fila+1)+(a)+"'><div class='radio grisN'>-<div></td>");
						$("#"+json[a].idCeco).append("<td id='tar"+(fila+1)+(fila+1)+(a)+"'><div class='radio grisN'>-<div></td>");
					
						$("#idLlenaZonas"+idCeco).append("</tr><tbody></table>");
						
						muestraZonas(a,json[a].idCeco,json[a].descCeco,Object.keys(json).length,""+(fila+1)+(fila+1));
						
					}
				} 	
				
			},error : function() {
				//alert('Favor de actualizar la pagina');
			}
		});
	}	
}


function despliegaCheckIntervalo(){

	document.getElementById("tipoModal").className="modal3";
	document.getElementById("idLlenaZonas"+validaCheck).innerHTML = "";
	arreglo = [];
	
	$.ajax({
		type : "GET",
		url : '/checklist/reportes/ajaxBuscaCecosPA.json?idCeco='+validaCheck,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			
			if (json == null) {
				//alert('Sin Datos');
				$("#idLlenaZonas"+validaCheck).append("<span class='Gris1'>Sin Datos</span>");
				document.getElementById("tipoModal").className="modal1";
			}else{					
				
				for (var a = 0; a < Object.keys(json).length; a++) {
					$("#idLlenaZonas"+validaCheck).append("<table class='bloquer subnivel'><tbody><tr id='"+json[a].idCeco+"' class='pointer'><td id='nom"+(filaHijo+1)+(filaHijo+1)+(a)+"' class='nombreCheck'>"+json[a].descCeco+"</td>");
		
					$("#"+json[a].idCeco).append("<td id='pla"+(filaHijo+1)+(filaHijo+1)+(a)+"'><div class='radio grisN'>-<div></td>");
					$("#"+json[a].idCeco).append("<td id='rot"+(filaHijo+1)+(filaHijo+1)+(a)+"'><div class='radio grisN'>-<div></td>");
					$("#"+json[a].idCeco).append("<td id='ase"+(filaHijo+1)+(filaHijo+1)+(a)+"'><div class='radio grisN'>-<div></td>");
					$("#"+json[a].idCeco).append("<td id='cer"+(filaHijo+1)+(filaHijo+1)+(a)+"'><div class='radio grisN'>-<div></td>");
					$("#"+json[a].idCeco).append("<td id='tar"+(filaHijo+1)+(filaHijo+1)+(a)+"'><div class='radio grisN'>-<div></td>");
				
					$("#idLlenaZonas"+validaCheck).append("</tr><tbody></table>");
					
					muestraZonas(a,json[a].idCeco,json[a].descCeco,Object.keys(json).length,""+(filaHijo+1)+(filaHijo+1));
				
				}
			} 	
			
		},error : function() {
			//alert('Favor de actualizar la pagina');
		}
	});
}
