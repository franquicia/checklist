package com.gruposalinas.checklist.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.gruposalinas.checklist.business.ChecklistBI;
import com.gruposalinas.checklist.business.FiltrosCecoBI;
import com.gruposalinas.checklist.business.ReporteBI;
import com.gruposalinas.checklist.business.ReporteRegBI;
import com.gruposalinas.checklist.business.ReporteSistemasBI;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.CompromisoCecoDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.domain.EvidenciaDTO;
import com.gruposalinas.checklist.domain.PaisDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.ReporteSistemasDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.domain.VistaCDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.util.UtilDate;

@Controller
public class VistaCumplimientoVisitasSistemas {
	
	private static Logger logger = LogManager.getLogger(VistaCumplimientoVisitasSistemas.class);
	private String nombreCecoAux;
	private boolean opcionReporte;
	private Map<String, Object> mapGlobal;
	
	@Autowired
	FiltrosCecoBI filtrosCecoBI;
	
	@Autowired
	ReporteBI reporteBI;
	
	@Autowired
	ReporteSistemasBI reporteSistemasBI;
	
	@Autowired
	ReporteRegBI reporteRegBI;
	
	@Autowired
	ChecklistBI checklistBI;
	
	
	
	//Modificado solo revisar si se va a modificar el filtroCecoBI, por otro,
	//Ademas revisar si el reporteBI; se va aquitar definitivamente o solo va a convivir con el nuevo
	//ReportesSistemasBI
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"central/vistaCumplimientoVisitasSistemas.htm","central/vistaCumplimientoVisitasSistemasMovil.htm"}, method = RequestMethod.GET)
	public ModelAndView vistaCumplimientoVisitasSistemas(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaCumplimientoVisitasSistemas...");

		String uri= request.getRequestURI();
		if(uri.contains("central/vistaCumplimientoVisitasSistemasMovil.htm"))
			this.opcionReporte=true;
		else
			this.opcionReporte=false;
		logger.info("this.opcionReporte "+this.opcionReporte);
		
		String paso=request.getParameter("paso");
		logger.info("Paso: "+paso);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitasSistemas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
		
			if(idUsuario>0){
				mv = new ModelAndView("vistaCumplimientoVisitasSistemas");
				
				if(paso != null){
					mv.addObject("paso", new String("1"));
				}
				
				mv.addObject("idUsuario", idUsuario);
				mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
				mv.addObject("opcionReporte", this.opcionReporte);
				//Fragmento para seleccionar el filtrado de tiendas
				
				List<CompromisoCecoDTO> listaValorPerfil = new ArrayList<CompromisoCecoDTO>();
				int totalPerfil = -1;
				String idCecoPerfil = "";
				String nomCecoPerfil = "";
				logger.info("datos antes del mapa... ");
	
				try {
					listaValorPerfil = reporteBI.CecoInfo(idUsuario);
					logger.info("listaValorPerfil... " + listaValorPerfil);
					if(!listaValorPerfil.isEmpty()){
						totalPerfil = listaValorPerfil.get(0).getTotal();
						idCecoPerfil = "230034";//listaValorPerfil.get(0).getIdCeco();
						nomCecoPerfil = listaValorPerfil.get(0).getDescCeco();
					}
					logger.info("nivelPerfil... " + totalPerfil);
					logger.info("idCeco... " + idCecoPerfil);
					logger.info("nombreCeco... " + nomCecoPerfil);
				} catch (Exception e) {
					logger.info("Ocurrio algo... "+e);
				}
				
				logger.info("nivelPerfil... " + totalPerfil);
				logger.info("idCeco... " + idCecoPerfil);
				logger.info("nombreCeco... " + nomCecoPerfil);
				mv.addObject("nivelPerfil", totalPerfil);
				mv.addObject("idCeco", idCecoPerfil);
				mv.addObject("nombreCeco", nomCecoPerfil);
				
				Map<String, Object> map= new HashMap<String, Object>();	
				//List<NegocioDTO> negocio=null;
				List<PaisDTO> pais=null;
				logger.info("datos antes del mapa 2... ");
	
				try {
					map= filtrosCecoBI.obtieneFiltros(idUsuario);
					logger.info("map... " + map);
					//negocio = (List<NegocioDTO>) map.get("negocio");
					pais = (List<PaisDTO>) map.get("pais");
					//logger.info("negocio... " + negocio.get(0).getDescripcion());
					logger.info("pais... " + pais.get(0).getNombre());
				} catch (Exception e) {
					logger.info("Ocurrio algo... "+e);
				}
				//int negocioId=0;
				//if(!negocio.isEmpty()){
					try {
						pais=filtrosCecoBI.obtienePaises(1,idUsuario);
					} catch (Exception e) {
						logger.info("Ocurrio algo... "+e);
					}
					//if(negocio.get(0).getDescripcion()!=null)
						//negocioId=negocio.get(0).getIdNegocio();
						
				//}
				//mv.addObject("negocio", negocio);
				logger.info("paises " + pais);
					
				mv.addObject("pais", pais);
				mv.addObject("negocioId", 1);
							
			}else{
				logger.info("...SALE DEL CONTROLLER vistaCumplimientoVisitasSistemas...");
				mv = new ModelAndView("http404");
			}
		}
		
		logger.info("...SALE DEL CONTROLLER vistaCumplimientoVisitasSistemas...");
		return mv;
	}
	
	
	//POR REVISAR SI DEBO MODIFICAR PATHS Y VALORES
	

	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	@RequestMapping(value = "central/vistaFiltroCumplimientoVisitasSistemas.htm", method = RequestMethod.POST)
	public ModelAndView vistaFiltroCumplimientoVisitasSistemas(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
			@RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,		
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,		
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,			
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaFiltroCumplimientoVisitasSistemas...");
		
		logger.info("this.opcionReporte "+this.opcionReporte);
		
		if(nivelPerfil==1)
			banderaCecoPadre=1;
		
		logger.info("banderaCecoPadre... "+banderaCecoPadre);		
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("porcentajeG... "+porcentajeG);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("nombreCeco... "+nombreCeco);		
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		
		this.nombreCecoAux=nombreCeco.split("/")[0];
		logger.info("nombreCecoAux "+ this.nombreCecoAux);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitasSistemas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
		
			List<ReporteSistemasDTO> listaChecklist = null;
			List<VistaCDTO> listaSucursales = null;
			List<List> listaReporteCum= new ArrayList<List>();
			int porcentaje = 0, conteo=0;
			
			Map<String, Object> map= new HashMap<String, Object>();
			List<List> listRetorno= new ArrayList<List>();
			String fechaReporte=""+seleccionMes+"/"+seleccionAno;
			logger.info("datos antes del mapa... ");
			
			try {
				map=reporteBI.ReporteVistaCumplimiento(idUsuario,null, seleccionPais, seleccionCanal,fechaReporte,banderaCecoPadre);
				logger.info("map... " + map);
				
			} catch (Exception e) {
				logger.info("Ocurrio algo... "+e);
				logger.info("No existe informaci�n");
			}
			
			if(map!=null){
				porcentaje= 10;//(Integer) map.get("porcentaje");
				logger.info("porcentaje... "+porcentaje);
				listaChecklist=(List<ReporteSistemasDTO>) reporteSistemasBI.ReporteChecklistSistemas(idCecoPadre); //map.get("listaChecklist");
				//listaChecklist=new ArrayList<ChecklistDTO>();
				logger.info("listaChecklist... "+listaChecklist);
				
				//METODO PARA MOSTRAR LA INFORMACION QUE ESTA DENTRO DEL ARRAYLIST CHECKLISTASISTEMAS
				
				List<List> listaAuxSistemas= new ArrayList<List>();
				logger.info("Valor del listaChecklist.size()"+listaChecklist.size());
				for(int i = 0; i < listaChecklist.size();i++){
					int idChecklist = listaChecklist.get(i).getIdChecklist();
					logger.info("CheckList ID"+idChecklist);
					logger.info("Porcentaje General:"+listaChecklist.get(i).getPorcentaje());
					List<ReporteSistemasDTO> reporte = reporteSistemasBI.ReporteCecosSistemas(idCecoPadre, idChecklist, "02/2018");
					logger.info("tamaño del reporte"+reporte.size());
					listaAuxSistemas.add(reporte);
					for(int j = 0; j<reporte.size();j++){
						/*
						logger.info("Tamaño Reportes size:"+reporte.size()+"Vuelta numero:"+i);
						logger.info("DATOS DEL REPORTE CECOS SISTEMAS Asignados:"+reporte.get(j).getAsignados());
						logger.info("DATOS DEL REPORTE CECOS SISTEMAS id ceco:"+reporte.get(j).getIdCeco());
						logger.info("DATOS DEL REPORTE CECOS SISTEMAS id checklist:"+reporte.get(j).getIdChecklist());
						logger.info("DATOS DEL REPORTE CECOS SISTEMAS id_grupo:"+reporte.get(j).getIdGrupo());
						logger.info("DATOS DEL REPORTE CECOS SISTEMAS Nombre ceco:"+reporte.get(j).getNombreCeco());
						logger.info("DATOS DEL REPORTE CECOS SISTEMAS nombre check:"+reporte.get(j).getNombreCheck());
						logger.info("DATOS DEL REPORTE CECOS SISTEMAS Num sucursal:"+reporte.get(j).getNumSucursal());
						logger.info("DATOS DEL REPORTE CECOS SISTEMAS Orden grupo:"+reporte.get(j).getOrdenGrupo());
						logger.info("DATOS DEL REPORTE CECOS SISTEMAS porcentaje:"+reporte.get(j).getPorcentaje());
						logger.info("DATOS DEL REPORTE CECOS SISTEMAS Terminados:"+reporte.get(j).getTerminados());
						logger.info("-----------------------------------------------------------------------");*/
					}
				}
				
				//El conteo es igual al tamaño del checklist
				conteo = listaChecklist.size();
				
				
				//FIN DEL METODO PARA MOSTRAR LA INFORMACION DEL ARRAYLIST CHECKLISTSISTEMAS
				
				listaReporteCum= (List<List>) map.get("listaSucursales");
				logger.info("listaSucursales... "+listaReporteCum);
				logger.info("listaSucursales.size()... "+listaReporteCum.size());
				conteo = (Integer) map.get("conteo");
				logger.info("conteo... "+conteo);
				
				//reporteSistemasBI.ReporteCecosSistemas(idCecoPadre, 1, "02/2018");
				
				
							
				List<ReporteSistemasDTO> listaAux=new ArrayList<ReporteSistemasDTO>();
				
				if(conteo==0)
					conteo++;
				
//				for (int i = 0; i < listaReporteCum.size(); i++) {			
//					listaAux.add((VistaCDTO) listaReporteCum.get(i));
//		
//					if((i+1)%conteo==0){
//						listRetorno.add(listaAux);				
//						listaAux=new ArrayList<VistaCDTO>();
//					}
//				}
				//OBTENER EL TAMAÑO DEL CICLO MAS INTERNO
				int cicliInterno = listaAuxSistemas.get(1).size();
				for(int j = 0; j<cicliInterno ;j++){
					for(int i = 0;i<listaAuxSistemas.size();i++){
						listaAux.add((ReporteSistemasDTO) listaAuxSistemas.get(i).get(j));
					}
					listRetorno.add(listaAux);				
					listaAux=new ArrayList<ReporteSistemasDTO>();
				}
				logger.info("listRetorno.size() "+listRetorno.size());
				logger.info("listRetorno "+listRetorno);
				if(listRetorno.size()>0)
					logger.info("listRetorno 0 size() "+listRetorno.get(0).size());
				
				if(idUsuario>0){
					mv = new ModelAndView("vistaFiltroCumplimientoVisitasSistemas");
					
					mv.addObject("opcionReporte", this.opcionReporte);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("porcentajeG", porcentajeG);
					mv.addObject("porcentaje", porcentaje);
					
					mv.addObject("idCecoPadre", idCecoPadre);
					mv.addObject("nombreCeco", this.nombreCecoAux);
					mv.addObject("banderaCecoPadre", 0);
					
					mv.addObject("seleccionAno", seleccionAno);
					mv.addObject("seleccionMes", seleccionMes);
					mv.addObject("seleccionCanal", seleccionCanal);
					mv.addObject("seleccionPais", seleccionPais);
					
					mv.addObject("conteo", conteo);
					mv.addObject("listaChecklist", listaChecklist);
					mv.addObject("listaSucursales", listRetorno);
					mv.addObject("url", FRQConstantes.getURLServer());
				}else{
					logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitasSistemas http404...");
					mv = new ModelAndView("http404");
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitasSistemas redirect...");
				logger.info("No existe informaci�n");
				mv = new ModelAndView("redirect:vistaCumplimientoVisitas.htm");
				mv.addObject("paso", new String("1"));
				
				
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitasSistemas...");
		return mv;
	}
	
	/*SIN USO, SI TIENE ALGUN USO DESCOMENTARLO, FUE COPIADO DE VistaCumplimientoVisitas
	
	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	@RequestMapping(value = "central/vistaFiltroCumplimientoVisitasDes.htm", method = RequestMethod.POST)
	public ModelAndView vistaFiltroCumplimientoVisitasDes(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
			@RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,		
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,		
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,			
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaFiltroCumplimientoVisitasDes...");
		
		logger.info("this.opcionReporte "+this.opcionReporte);
		
		if(nivelPerfil==1)
			banderaCecoPadre=1;
		
		logger.info("banderaCecoPadre... "+banderaCecoPadre);		
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("porcentajeG... "+porcentajeG);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("nombreCeco... "+nombreCeco);		
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		
		this.nombreCecoAux=nombreCeco.split("/")[0];
		logger.info("nombreCecoAux "+ this.nombreCecoAux);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
		
			List<ChecklistDTO> listaChecklist = null;
			List<VistaCDTO> listaSucursales = null;
			List<List> listaReporteCum= new ArrayList<List>();
			int porcentaje = 0, conteo=0;
			
			Map<String, Object> map= new HashMap<String, Object>();
			List<List> listRetorno= new ArrayList<List>();
			String fechaReporte=""+seleccionMes+"/"+seleccionAno;
			logger.info("datos antes del mapa... ");
			
			try {
				map=reporteRegBI.ReporteVistaCumplimiento(idUsuario,null, seleccionPais, seleccionCanal,fechaReporte,banderaCecoPadre);
				logger.info("map... " + map);			
			} catch (Exception e) {
				logger.info("Ocurrio algo... "+e);
			}
			
			if(map!=null){
				porcentaje= (Integer) map.get("porcentaje");
				logger.info("porcentaje... "+porcentaje);
				listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
				//listaChecklist=new ArrayList<ChecklistDTO>();
				logger.info("listaChecklist... "+listaChecklist);
				
				listaReporteCum= (List<List>) map.get("listaSucursales");
				logger.info("listaSucursales... "+listaReporteCum);
				logger.info("listaSucursales.size()... "+listaReporteCum.size());
				conteo = (Integer) map.get("conteo");
				logger.info("conteo... "+conteo);
							
				List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
				
				if(conteo==0)
					conteo++;
				
				for (int i = 0; i < listaReporteCum.size(); i++) {			
					listaAux.add((VistaCDTO) listaReporteCum.get(i));
		
					if((i+1)%conteo==0){
						listRetorno.add(listaAux);				
						listaAux=new ArrayList<VistaCDTO>();
					}
				}
				logger.info("listRetorno.size() "+listRetorno.size());
				logger.info("listRetorno "+listRetorno);
				if(listRetorno.size()>0)
					logger.info("listRetorno 0 size() "+listRetorno.get(0).size());
				
				if(idUsuario>0){
					mv = new ModelAndView("vistaFiltroCumplimientoVisitasDes");
					
					mv.addObject("opcionReporte", this.opcionReporte);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("porcentajeG", porcentajeG);
					mv.addObject("porcentaje", porcentaje);
					
					mv.addObject("idCecoPadre", idCecoPadre);
					mv.addObject("nombreCeco", this.nombreCecoAux);
					mv.addObject("banderaCecoPadre", 0);
					
					mv.addObject("seleccionAno", seleccionAno);
					mv.addObject("seleccionMes", seleccionMes);
					mv.addObject("seleccionCanal", seleccionCanal);
					mv.addObject("seleccionPais", seleccionPais);
					
					mv.addObject("conteo", conteo);
					mv.addObject("listaChecklist", listaChecklist);
					mv.addObject("listaSucursales", listRetorno);
					mv.addObject("url", FRQConstantes.getURLServer());
				}else{
					logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitas http404...");
					mv = new ModelAndView("http404");
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitas redirect...");
				mv = new ModelAndView("redirect:vistaCumplimientoVisitas.htm");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitas...");
		return mv;
	}
	*/
	
	
	//MODIFICADO
	
	/*CONTROLLER VISTA DETALLE NUEVO CONTROLLER*/
	//-DESARROLLO-central/vistaFiltroCumplimientoVisitasRegSistemas.htm?idUsuario=189870&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2017&seleccionMes=7&seleccionCanal=1&seleccionPais=21
	//-PRODUCCION-central/vistaFiltroCumplimientoVisitasRegSistemas.htm?idUsuario=147247&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2017&seleccionMes=7&seleccionCanal=1&seleccionPais=21

	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	@RequestMapping(value = "central/vistaFiltroCumplimientoVisitasRegSistemas.htm", method = RequestMethod.GET)
	public ModelAndView vistaFiltroCumplimientoVisitasRegSistemas(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idUsuario", required = true, defaultValue = "") String idUser,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
			@RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,		
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,		
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,			
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaFiltroCumplimientoVisitasRegSistemas...");
		
		logger.info("this.opcionReporte "+this.opcionReporte);
		
		if(nivelPerfil==1)
			banderaCecoPadre=1;
		
		logger.info("banderaCecoPadre... "+banderaCecoPadre);		
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("porcentajeG... "+porcentajeG);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("nombreCeco... "+nombreCeco);		
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		
		this.nombreCecoAux=nombreCeco.split("/")[0];
		logger.info("nombreCecoAux "+ this.nombreCecoAux);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
			
		
			List<ChecklistDTO> listaChecklist = null;
			List<VistaCDTO> listaSucursales = null;
			List<List> listaReporteCum= new ArrayList<List>();
			int porcentaje = 0, conteo=0;
			
			Map<String, Object> map= new HashMap<String, Object>();
			List<List> listRetorno= new ArrayList<List>();
			logger.info("datos antes del mapa... ");
			
			try {
				Date dtf = new Date();
				String date = new SimpleDateFormat("dd-MM-yyyy").format(dtf).toString(); // 18/07/2017
				String dia = date.substring(0, 2);
				String mes = date.substring(3, 5);
				String ano = date.substring(6, 10);
				String fechaReporte=""+mes+"/"+ano;
	
				map=reporteRegBI.ReporteVistaCumplimiento(idUsuario,null, seleccionPais, seleccionCanal,fechaReporte,banderaCecoPadre);
				logger.info("map... " + map);			
			} catch (Exception e) {
				logger.info("Ocurrio algo... "+e);
			}
			
			if(map!=null){
				porcentaje= (Integer) map.get("porcentaje");
				logger.info("porcentaje... "+porcentaje);
				listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
				//listaChecklist=new ArrayList<ChecklistDTO>();
				logger.info("listaChecklist... "+listaChecklist);
				
				listaReporteCum= (List<List>) map.get("listaSucursales");
				logger.info("listaSucursales... "+listaReporteCum);
				logger.info("listaSucursales.size()... "+listaReporteCum.size());
				//conteo = (Integer) map.get("conteo");
				conteo=2;
				logger.info("conteo... "+conteo);
							
				List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
				
				if(conteo==0)
					conteo++;
				
				for (int i = 0; i < listaReporteCum.size(); i++) {		
					
					listaAux.add((VistaCDTO) listaReporteCum.get(i));
		
					if((i+1)%conteo==0){
						listRetorno.add(listaAux);				
						listaAux=new ArrayList<VistaCDTO>();
					}
				}
				//logger.info("listRetorno.size() "+listRetorno.size());
				logger.info("listRetorno "+listRetorno);
				
				List<VistaCDTO> listaAuxSuma=new ArrayList<VistaCDTO>();
				
				if(listRetorno.size()>0)
					logger.info("listRetorno 0 size() "+listRetorno.get(0).size());
				
				if(idUsuario>0){
					mv = new ModelAndView("vistaFiltroCumplimientoVisitasRegSistemas");
					
					mv.addObject("opcionReporte", this.opcionReporte);
				
					Date dtf = new Date();
					String date = new SimpleDateFormat("dd-MM-yyyy").format(dtf).toString(); // 18/07/2017
					String dia = date.substring(0, 2);
					String mes = date.substring(3, 5);
					String ano = date.substring(6, 10);
					
					int diasMes = new UtilDate().calculaDiasMes(Integer.parseInt(ano) , Integer.parseInt(mes));
					double porcentajeC = ((Integer.parseInt(dia)-1)*100)/diasMes;
	
					mv.addObject("porcentajeG", porcentajeC);
					mv.addObject("porcentaje", porcentaje);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("banderaCecoPadre", banderaCecoPadre);
					mv.addObject("seleccionAno", ano);
					mv.addObject("seleccionMes", mes);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("idCecoPadre", idCecoPadre);
					mv.addObject("nombreCeco", this.nombreCecoAux);
					mv.addObject("seleccionCanal", seleccionCanal);
					mv.addObject("seleccionPais", seleccionPais);
					mv.addObject("conteo", conteo);
					mv.addObject("listaChecklist", listaChecklist);
					mv.addObject("listaSucursales", listRetorno);
					mv.addObject("url", FRQConstantes.getURLServer());
	
				}else{
					logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitas http404...");
					mv = new ModelAndView("http404");
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitas redirect...");
				mv = new ModelAndView("redirect:vistaCumplimientoVisitas.htm");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitasRegSistemas...");
		return mv;
	}
	
	
	/*CONTROLLER VISTA DETALLE NUEVO CONTROLLER*/
	//-DESARROLLO-central/vistaChecklist.htm?idUsuario=189870&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2017&seleccionMes=7&seleccionCanal=1&seleccionPais=21
	//-PRODUCCION-central/vistaChecklist.htm?idUsuario=147247&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2017&seleccionMes=7&seleccionCanal=1&seleccionPais=21

	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	@RequestMapping(value = "central/vistaChecklistSistemas.htm", method = RequestMethod.GET)
	public ModelAndView vistaChecklist(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idUsuario", required = true, defaultValue = "") String idUser,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
			@RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,		
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,		
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,			
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaChecklist...");
		
		logger.info("this.opcionReporte "+this.opcionReporte);
		
		if(nivelPerfil==1)
			banderaCecoPadre=1;
		
		logger.info("banderaCecoPadre... "+banderaCecoPadre);		
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("porcentajeG... "+porcentajeG);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("nombreCeco... "+nombreCeco);		
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		
		this.nombreCecoAux=nombreCeco.split("/")[0];
		logger.info("nombreCecoAux "+ this.nombreCecoAux);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
		
			List<ChecklistDTO> listaChecklist = null;
			List<VistaCDTO> listaSucursales = null;
			List<List> listaReporteCum= new ArrayList<List>();
			int porcentaje = 0, conteo=0;
			
			Map<String, Object> map= new HashMap<String, Object>();
			List<List> listRetorno= new ArrayList<List>();
			logger.info("datos antes del mapa... ");
			
			try {
				Date dtf = new Date();
				String date = new SimpleDateFormat("dd-MM-yyyy").format(dtf).toString(); // 18/07/2017
				String dia = date.substring(0, 2);
				String mes = date.substring(3, 5);
				String ano = date.substring(6, 10);
				String fechaReporte=""+mes+"/"+ano;
	
				map=reporteRegBI.ReporteVistaCumplimientoTres(idUsuario,null, seleccionPais, seleccionCanal,fechaReporte,banderaCecoPadre);
				logger.info("map... " + map);			
			} catch (Exception e) {
				logger.info("Ocurrio algo... "+e);
			}
			
			if(map!=null){
				porcentaje= (Integer) map.get("porcentaje");
				logger.info("porcentaje... "+porcentaje);
				listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
				//listaChecklist=new ArrayList<ChecklistDTO>();
				logger.info("listaChecklist... "+listaChecklist);
				
				listaReporteCum= (List<List>) map.get("listaSucursales");
				logger.info("listaSucursales... "+listaReporteCum);
				logger.info("listaSucursales.size()... "+listaReporteCum.size());
				conteo = (Integer) map.get("conteo");
				logger.info("conteo checks... "+conteo);
				conteo=3;
				logger.info("conteo... "+conteo);
							
				List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
				
				if(conteo==0)
					conteo++;
				
				for (int i = 0; i < listaReporteCum.size(); i++) {		
					
					listaAux.add((VistaCDTO) listaReporteCum.get(i));
		
					if((i+1)%conteo==0){
						listRetorno.add(listaAux);				
						listaAux=new ArrayList<VistaCDTO>();
					}
				}
				//logger.info("listRetorno.size() "+listRetorno.size());
				logger.info("listRetorno "+listRetorno);
				
				double totalCaja=0.0;
				double totalPiso=0.0;
				double totalS=0.0;
				
				int proCaja=0;
				int proPiso=0;
				int proS=0;
				
				List<VistaCDTO> listaAuxSuma=new ArrayList<VistaCDTO>();
				
				if(listRetorno.size()>0){
					logger.info("listRetorno 0 size() "+listRetorno.get(0).size());
					for (int k = 0; k < listRetorno.size(); k++) {
						listaAuxSuma = listRetorno.get(k);
						totalCaja = totalCaja + listaAuxSuma.get(0).getTotal();
						totalPiso = totalPiso + listaAuxSuma.get(1).getTotal();
						//totalS = totalS + listaAuxSuma.get(2).getTotal();
					}
					proCaja = (int) Math.round(totalCaja/listRetorno.size());
					proPiso = (int) Math.round(totalPiso/listRetorno.size());
					//proS = (int) Math.round(totalS/listRetorno.size());
				}else{
					proCaja=0;
					proPiso=0;
					proS=0;
				}
				
				if(idUsuario>0){
					mv = new ModelAndView("vistaChecklist");
					
					mv.addObject("opcionReporte", this.opcionReporte);
				
					Date dtf = new Date();
					String date = new SimpleDateFormat("dd-MM-yyyy").format(dtf).toString(); // 18/07/2017
					String dia = date.substring(0, 2);
					String mes = date.substring(3, 5);
					String ano = date.substring(6, 10);
					
					int diasMes = new UtilDate().calculaDiasMes(Integer.parseInt(ano) , Integer.parseInt(mes));
					double porcentajeC = ((Integer.parseInt(dia)-1)*100)/diasMes;
	
					mv.addObject("porcentajeG", porcentajeC);
					mv.addObject("porcentaje", porcentaje);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("banderaCecoPadre", banderaCecoPadre);
					mv.addObject("seleccionAno", ano);
					mv.addObject("seleccionMes", mes);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("idCecoPadre", idCecoPadre);
					mv.addObject("nombreCeco", this.nombreCecoAux);
					mv.addObject("seleccionCanal", seleccionCanal);
					mv.addObject("seleccionPais", seleccionPais);
					mv.addObject("conteo", conteo);
					mv.addObject("listaChecklist", listaChecklist);
					mv.addObject("listaSucursales", listRetorno);
					mv.addObject("url", FRQConstantes.getURLServer());
					
					mv.addObject("proCaja",proCaja);
					mv.addObject("proPiso",proPiso);
					mv.addObject("proS",proS);
	
				}else{
					logger.info("...SALE DEL CONTROLLER vistaChecklist http404...");
					mv = new ModelAndView("http404");
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaChecklist redirect...");
				mv = new ModelAndView("redirect:vistaCumplimientoVisitas.htm");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaChecklist...");
		return mv;
	}
	
	
	/*CONTROLLER VISTA DETALLE NUEVO CONTROLLER*/
	//-DESARROLLO-central/vistaTiempoReal.htm?idUsuario=189870&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2017&seleccionMes=7&seleccionCanal=1&seleccionPais=21
	//-PRODUCCION-central/vistaTiempoReal.htm?idUsuario=147247&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2017&seleccionMes=7&seleccionCanal=1&seleccionPais=21
	
	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	@RequestMapping(value = "central/vistaTiempoRealSistemas.htm", method = RequestMethod.GET)
	public ModelAndView vistaTiempoReal(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idUsuario", required = true, defaultValue = "") String idUser,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
			@RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,		
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,		
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,			
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaTiempoReal...");
		
		logger.info("this.opcionReporte "+this.opcionReporte);
		
		if(nivelPerfil==1)
			banderaCecoPadre=1;
		
		logger.info("banderaCecoPadre... "+banderaCecoPadre);		
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("porcentajeG... "+porcentajeG);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("nombreCeco... "+nombreCeco);		
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		
		this.nombreCecoAux=nombreCeco.split("/")[0];
		logger.info("nombreCecoAux "+ this.nombreCecoAux);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
			
		
			List<ChecklistDTO> listaChecklist = null;
			List<VistaCDTO> listaSucursales = null;
			List<List> listaReporteCum= new ArrayList<List>();
			int porcentaje = 0, conteo=0;
			
			Map<String, Object> map= new HashMap<String, Object>();
			List<List> listRetorno= new ArrayList<List>();
			logger.info("datos antes del mapa... ");
			
			try {
				Date dtf = new Date();
				String date = new SimpleDateFormat("dd-MM-yyyy").format(dtf).toString(); // 18/07/2017
				String dia = date.substring(0, 2);
				String mes = date.substring(3, 5);
				String ano = date.substring(6, 10);
				String fechaReporte=""+mes+"/"+ano;
	
				map=reporteRegBI.ReporteVistaCumplimientoTres(idUsuario,null, seleccionPais, seleccionCanal,fechaReporte,banderaCecoPadre);
				logger.info("map... " + map);			
			} catch (Exception e) {
				logger.info("Ocurrio algo... "+e);
			}
			
			if(map!=null){
				porcentaje= (Integer) map.get("porcentaje");
				logger.info("porcentaje... "+porcentaje);
				listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
				//listaChecklist=new ArrayList<ChecklistDTO>();
				logger.info("listaChecklist... "+listaChecklist);
				
				listaReporteCum= (List<List>) map.get("listaSucursales");
				logger.info("listaSucursales... "+listaReporteCum);
				logger.info("listaSucursales.size()... "+listaReporteCum.size());
				conteo = (Integer) map.get("conteo");
				logger.info("conteo checks... "+conteo);
				conteo=2;
				logger.info("conteo... "+conteo);
							
				List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
				
				if(conteo==0)
					conteo++;
				
				for (int i = 0; i < listaReporteCum.size(); i++) {		
					
					listaAux.add((VistaCDTO) listaReporteCum.get(i));
		
					if((i+1)%conteo==0){
						listRetorno.add(listaAux);				
						listaAux=new ArrayList<VistaCDTO>();
					}
				}
				logger.info("listRetorno.size() "+listRetorno.size());
				logger.info("listRetorno "+listRetorno);
				
				if(listRetorno.size()>0)
					logger.info("listRetorno 0 size() "+listRetorno.get(0).size());
				
				if(idUsuario>0){
					mv = new ModelAndView("vistaTiempoReal");
					
					mv.addObject("opcionReporte", this.opcionReporte);
				
					Date dtf = new Date();
					String date = new SimpleDateFormat("dd-MM-yyyy").format(dtf).toString(); // 18/07/2017
					String dia = date.substring(0, 2);
					String mes = date.substring(3, 5);
					String ano = date.substring(6, 10);
					
					int diasMes = new UtilDate().calculaDiasMes(Integer.parseInt(ano) , Integer.parseInt(mes));
					double porcentajeC = ((Integer.parseInt(dia)-1)*100)/diasMes;
	
					mv.addObject("porcentajeG", porcentajeC);
					mv.addObject("porcentaje", porcentaje);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("banderaCecoPadre", banderaCecoPadre);
					mv.addObject("seleccionAno", ano);
					mv.addObject("seleccionMes", mes);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("idCecoPadre", idCecoPadre);
					mv.addObject("nombreCeco", this.nombreCecoAux);
					mv.addObject("seleccionCanal", seleccionCanal);
					mv.addObject("seleccionPais", seleccionPais);
					mv.addObject("conteo", conteo);
					mv.addObject("listaChecklist", listaChecklist);
					mv.addObject("listaSucursales", listRetorno);
					mv.addObject("url", FRQConstantes.getURLServer());
	
				}else{
					logger.info("...SALE DEL CONTROLLER vistaTiempoReal http404...");
					mv = new ModelAndView("http404");
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaTiempoReal redirect...");
				mv = new ModelAndView("redirect:vistaCumplimientoVisitas.htm");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaTiempoReal...");
		return mv;
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "central/vistaTerritCumplimientoVisitasSistemas.htm", method = RequestMethod.POST)
	public ModelAndView vistaTerritCumplimientoVisitas(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,
			
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception{
			
		logger.info("...ENTRO AL CONTROLLER vistaTerritCumplimientoVisitas...");
		
		logger.info("this.opcionReporte "+this.opcionReporte);
		logger.info("porcentajeG... "+porcentajeG);
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("idCeco... "+idCeco);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("nombreCeco... "+nombreCeco);
		logger.info("banderaCecoPadre... "+banderaCecoPadre);
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		
		String nombreAux []=nombreCeco.split(",");
		int nombreAuxSize= nombreAux.length;		
		this.nombreCecoAux=nombreAux[nombreAuxSize-1];
		logger.info("nombreCecoAux "+ nombreCecoAux);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);

			List<ChecklistDTO> listaChecklist = null;
			List<List> listaReporteCum= new ArrayList<List>();
			int porcentaje = 0, conteo=0;
			
			Map<String, Object> map= new HashMap<String, Object>();
			String fechaReporte=""+seleccionMes+"/"+seleccionAno;
			logger.info("datos antes del mapa... ");

			try {
				if(nivelPerfil==0 || nivelPerfil==1)
					map=reporteBI.ReporteVistaCumplimientoSucursal(idUsuario,idCeco, seleccionPais, seleccionCanal,fechaReporte,banderaCecoPadre);
				else
					map=reporteBI.ReporteVistaCumplimiento(idUsuario , null, seleccionPais, seleccionCanal,fechaReporte,banderaCecoPadre);
				logger.info("map... " + map);
			} catch (Exception e) {
				logger.info("Ocurrio algo... "+e);
			}
			
			if(map!=null){
				porcentaje= (Integer) map.get("porcentaje");
				logger.info("porcentaje... "+porcentaje);
				listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
				//listaChecklist=new ArrayList<ChecklistDTO>();
				logger.info("listaChecklist... "+listaChecklist);
				listaReporteCum= (List<List>) map.get("listaSucursales");
				logger.info("listaSucursales... "+listaReporteCum);
				conteo = (Integer) map.get("conteo");
				logger.info("conteo... "+conteo);
		
				if(conteo==0)
					conteo++;
					
				if(idUsuario>0){
					mv = new ModelAndView("vistaTerritCumplimientoVisitas");
					
					mv.addObject("opcionReporte", this.opcionReporte);
					
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("porcentajeG", porcentajeG);
					mv.addObject("idCeco", idCeco);
					mv.addObject("idCecoPadre", idCecoPadre);
					mv.addObject("nombreCeco", this.nombreCecoAux);
					mv.addObject("banderaCecoPadre", banderaCecoPadre);
					mv.addObject("seleccionAno", seleccionAno);
					mv.addObject("seleccionMes", seleccionMes);
					mv.addObject("seleccionCanal", seleccionCanal);
					mv.addObject("seleccionPais", seleccionPais);
					
					mv.addObject("porcentaje", porcentaje);
					mv.addObject("conteo", conteo);
					mv.addObject("listaChecklist", listaChecklist);
					mv.addObject("listaSucursales", listaReporteCum);
					mv.addObject("listaSucursalesSize", listaReporteCum.size());
				}else{
					logger.info("...SALE DEL CONTROLLER vistaTerritCumplimientoVisitas...");
					mv = new ModelAndView("http404");
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaTerritCumplimientoVisitas...");
				mv = new ModelAndView("redirect:vistaCumplimientoVisitas.htm");
			}
		}
		
		
		return mv;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "central/vistaDetalleCumplimientoVisitasSistemas.htm", method = RequestMethod.POST)
	public ModelAndView vistaDetalleCumplimientoVisitas(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "porcentajeG", required = true, defaultValue = "") int porcentajeG,
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "") int idCecoPadre,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") int idCeco,
			@RequestParam(value = "idCecoHijo", required = true, defaultValue = "") int idCecoHijo,
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,			
			@RequestParam(value = "idCheck", required = true, defaultValue = "") int idCheck,
			@RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "") int banderaCecoPadre,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "") int seleccionPais) throws Exception{

		logger.info("...ENTRO AL CONTROLLER vistaDetalleCumplimientoVisitas...");
		
		logger.info("this.opcionReporte "+this.opcionReporte);
		logger.info("porcentajeG... "+porcentajeG);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("idCeco... "+idCeco);
		logger.info("idCecoHijo... "+idCecoHijo);
		logger.info("nombreCeco... "+nombreCeco);		
		logger.info("idCheck... "+idCheck);
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("banderaCecoPadre... "+banderaCecoPadre);
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
		
			List <ChecklistDTO> listaChecklist = null;
			List <EvidenciaDTO> listaEvidencias = null;
			List <PreguntaDTO> listaFolios = null;
			List<CompromisoDTO> preguntas = null;
			String json = "";
			int totalE = 0;
			int totalF = 0;
			int idBit = 0;
			
	
			Map<String, Object> map= new HashMap<String, Object>();
			String fechaC = seleccionMes+"/"+seleccionAno;
			logger.info("fecha... " + fechaC);
			//String fechaReporte=""+seleccionMes+"/"+seleccionAno;
			logger.info("datos antes del mapa... ");
			
			try {
				logger.info("ID CECO " + idCecoHijo);
				logger.info("ID CHECK " + idCheck);
				logger.info("ID USUARIO " + idUsuario);
				logger.info("ID FECHA " + fechaC);
	
				map=reporteBI.ReporteDetVC(idCecoHijo, idCheck, idUsuario, fechaC);
	
				listaChecklist = (List<ChecklistDTO>) map.get("listaChecklist");
				listaEvidencias = (List<EvidenciaDTO>) map.get("listaEvidencias");
				listaFolios = (List<PreguntaDTO>) map.get("listaFolios");
				totalE = (Integer) map.get("totalEvidencias");
				totalF = (Integer) map.get("totalFolios");
				idBit =  (Integer) map.get("idBitacora");
	
				
				//System.out.println("***************ID BITACORA***************" + idBit);
				preguntas = reporteBI.ReporteDetPreguntasRegional(idBit);
	
				json = new Gson().toJson(preguntas);
				//System.out.println("***************JSON***************" + json);
	
				
				//map=reporteBI.ReporteDetVC(idCecoHijo, idCheck, idUsuario, fechaC);
				map=this.mapGlobal;
				logger.info("map... " + map);
				
				if(map!=null){
					listaChecklist = (List<ChecklistDTO>) map.get("listaChecklist");
					listaEvidencias = (List<EvidenciaDTO>) map.get("listaEvidencias");
					listaFolios = (List<PreguntaDTO>) map.get("listaFolios");
					totalE = (Integer) map.get("totalEvidencias");
					totalF = (Integer) map.get("totalFolios");
					idBit =  (Integer) map.get("idBitacora");
					
					preguntas = reporteBI.ReporteDetPreguntasRegional(idBit);
					json = new Gson().toJson(preguntas);
				}			
			} catch (Exception e) {
				logger.info("Ocurrio algo... "+e);
			}
			String rutaImagen;
			if(this.opcionReporte)
				rutaImagen= FRQConstantes.getRutaImagenMovil();
			else
				rutaImagen= FRQConstantes.getRutaImagen();
			logger.info("rutaImagen "+rutaImagen);
			
			if(map!=null){
				if(Integer.parseInt(userSession.getIdUsuario())>0){
					mv = new ModelAndView("vistaDetalleCumplimientoVisitas");
					
					mv.addObject("opcionReporte", this.opcionReporte);
					
					mv.addObject("listaCheck", listaChecklist);
					mv.addObject("listaEvidencias", listaEvidencias);
					mv.addObject("listaFolios", listaFolios);
					mv.addObject("totalE", totalE);
					mv.addObject("totalF", totalF);
					mv.addObject("listaPreguntas", preguntas);
					mv.addObject("json", json);
					mv.addObject("rutaHostImagen", rutaImagen);
					
					mv.addObject("porcentajeG", porcentajeG);
					mv.addObject("idCecoPadre", idCecoPadre);
					mv.addObject("idCeco", idCeco);
					mv.addObject("nombreCeco", nombreCeco);				
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("banderaCecoPadre", banderaCecoPadre);
					mv.addObject("seleccionAno", seleccionAno);
					mv.addObject("seleccionMes", seleccionMes);
					mv.addObject("seleccionCanal", seleccionCanal);
					mv.addObject("seleccionPais", seleccionPais);
					
					logger.info(listaChecklist.size());
					logger.info(listaFolios.size());
					logger.info(listaEvidencias.size());
					logger.info(totalE);
					logger.info(totalF);
					logger.info(preguntas.size());
				}else{
					logger.info("...SALE DEL CONTROLLER vistaDetalleCumplimientoVisitas...");
					mv = new ModelAndView("http404");
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaDetalleCumplimientoVisitas...");
				//mv = new ModelAndView("redirect:vistaCumplimientoVisitas.htm");
				mv=null;
			}
		}
		return mv;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/ajaxFiltroSucursalSistemas", method = RequestMethod.GET)
	public @ResponseBody List<List> ajaxFiltroSucursal(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais,
			@RequestParam(value = "valorPila", required = true, defaultValue = "0") int valorPila, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxFiltroSucursal...");
		
		logger.info("idCeco... "+idCeco);
		logger.info("banderaCecoPadre... "+banderaCecoPadre);
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		logger.info("valorPila... "+valorPila);
		
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());
		logger.info("USUARIO "+ idUsuario);
		
		List<ChecklistDTO> listaChecklist = null;
		List<List> listaReporteCum= new ArrayList<List>();
		List<VistaCDTO> listaReporteCumPila= new ArrayList<VistaCDTO>();
		
		int porcentaje = 0, conteo=0;
		Map<String, Object> map= new HashMap<String, Object>();
		Map<String, Object> mapPila= new HashMap<String, Object>();
		List<List> listRetorno= new ArrayList<List>();
		String fechaReporte=""+seleccionMes+"/"+seleccionAno;
		logger.info("datos antes de los mapas... ");
		
		try {
			map=reporteBI.ReporteVistaCumplimientoSucursal(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,0);
			logger.info("map... " + map);
			if(valorPila==1){
				mapPila=reporteBI.ReporteVistaCumplimientoSucursal(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,2);
				logger.info("mapPila... " + mapPila);
			}
				
		} catch (Exception e) {
			logger.info("Ocurrio algo... "+e);
		}
		if(map!=null){
			porcentaje= (Integer) map.get("porcentaje");
			logger.info("porcentaje... "+porcentaje);
			listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
			logger.info("listaChecklist... "+listaChecklist);
			listRetorno.add(listaChecklist);
			
			listaReporteCum= (List<List>) map.get("listaSucursales");
			logger.info("listaSucursales... "+listaReporteCum);
			conteo = (Integer) map.get("conteo");
			logger.info("conteo... "+conteo);
		}			
		
		if(valorPila==1){
			if(mapPila!=null){
				listaReporteCumPila= (List<VistaCDTO>) mapPila.get("listaSucursales");
				logger.info("listaReporteCumPila... "+listaReporteCumPila);
				
				// NUEVA VALIDACION
				if(listaReporteCumPila.size()>1){					
					boolean flag = false;
					int l =1;
					int sumaAnt = 0;
					int sumaAct = 0;
					VistaCDTO act = null;
					VistaCDTO ant = null;
					while(l < listaReporteCumPila.size()){	
						sumaAnt= listaReporteCumPila.get(l - 1).getAsignados()+listaReporteCumPila.get(l - 1).getTerminados();
						sumaAct= listaReporteCumPila.get(l).getAsignados()+listaReporteCumPila.get(l).getTerminados();						
						if(sumaAnt < sumaAct){
							ant=listaReporteCumPila.get(l - 1);
							act=listaReporteCumPila.get(l);							
							listaReporteCumPila.set(l - 1, act);
							listaReporteCumPila.set(l, ant);
							flag=true;
						}
						if (flag && (l + 1) == listaReporteCumPila.size()) {
							l = 0;
							flag = false;
						}
						l++;
					}				
				}
				// NUEVA VALIDACION				
			}			
			for (int i = 0; i < listaReporteCumPila.size(); i++) {
				VistaCDTO listaVista = new VistaCDTO();
				listaVista=(VistaCDTO) listaReporteCum.get(i);
				
				VistaCDTO listaVistaPila = new VistaCDTO();
				listaVistaPila=(VistaCDTO) listaReporteCumPila.get(i);

				listaVista.setAsignados(listaVistaPila.getAsignados());
				listaVista.setTerminados(listaVistaPila.getTerminados());
			}
		}
		
		if(conteo==0)
			conteo++;
		
		List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
		
		logger.info("listaReporteCum.size() "+ listaReporteCum.size());
		for (int i = 0; i < listaReporteCum.size(); i++) {							
			listaAux.add((VistaCDTO) listaReporteCum.get(i));			
			if((i+1)%conteo==0 ){
				listRetorno.add(listaAux);				
				listaAux=new ArrayList<VistaCDTO>();
			}
		}
		
		logger.info("listRetorno.size() "+listRetorno.size());
		logger.info("listRetorno "+listRetorno);

		logger.info("...SALE DEL AJAX ajaxFiltroSucursal...");
		return listRetorno;
	}
	
	/*::::::::::::::::::::AJAX FILTRO SUCURSAL PILA::::::::::::::::::::*/
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/ajaxFiltroSucursalPilaSistemas", method = RequestMethod.GET)
	public @ResponseBody List<List> ajaxFiltroSucursalPila(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais,
			@RequestParam(value = "valorPila", required = true, defaultValue = "0") int valorPila, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxFiltroSucursal...");
		
		logger.info("idCeco... "+idCeco);
		logger.info("banderaCecoPadre... "+banderaCecoPadre);
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		logger.info("valorPila... "+valorPila);
		
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());
		logger.info("USUARIO "+ idUsuario);
		
		List<ChecklistDTO> listaChecklist = null;
		List<List> listaReporteCum= new ArrayList<List>();
		List<VistaCDTO> listaReporteCumPila= new ArrayList<VistaCDTO>();
		
		int porcentaje = 0, conteo=0;
		Map<String, Object> map= new HashMap<String, Object>();
		Map<String, Object> mapPila= new HashMap<String, Object>();
		List<List> listRetorno= new ArrayList<List>();
		String fechaReporte=""+seleccionMes+"/"+seleccionAno;
		logger.info("datos antes de los mapas... ");
		
		try {
			map=reporteBI.ReporteVistaCumplimientoSucursalPila(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,0);
			logger.info("map... " + map);
			if(valorPila==1){
				mapPila=reporteBI.ReporteVistaCumplimientoSucursalPila(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,2);
				logger.info("mapPila... " + mapPila);
			}
				
		} catch (Exception e) {
			logger.info("Ocurrio algo... "+e);
		}
		if(map!=null){
			porcentaje= (Integer) map.get("porcentaje");
			logger.info("porcentaje... "+porcentaje);
			listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
			logger.info("listaChecklist... "+listaChecklist);
			listRetorno.add(listaChecklist);
			
			listaReporteCum= (List<List>) map.get("listaSucursales");
			logger.info("listaSucursales... "+listaReporteCum);
			conteo = (Integer) map.get("conteo");
			logger.info("conteo... "+conteo);
		}			
		
		if(valorPila==1){
			if(mapPila!=null){
				listaReporteCumPila= (List<VistaCDTO>) mapPila.get("listaSucursales");
				logger.info("listaReporteCumPila... "+listaReporteCumPila);
				
				// NUEVA VALIDACION
				if(listaReporteCumPila.size()>1){					
					boolean flag = false;
					int l =1;
					int sumaAnt = 0;
					int sumaAct = 0;
					VistaCDTO act = null;
					VistaCDTO ant = null;
					while(l < listaReporteCumPila.size()){	
						sumaAnt= listaReporteCumPila.get(l - 1).getAsignados()+listaReporteCumPila.get(l - 1).getTerminados();
						sumaAct= listaReporteCumPila.get(l).getAsignados()+listaReporteCumPila.get(l).getTerminados();						
						if(sumaAnt < sumaAct){
							ant=listaReporteCumPila.get(l - 1);
							act=listaReporteCumPila.get(l);							
							listaReporteCumPila.set(l - 1, act);
							listaReporteCumPila.set(l, ant);
							flag=true;
						}
						if (flag && (l + 1) == listaReporteCumPila.size()) {
							l = 0;
							flag = false;
						}
						l++;
					}				
				}
				// NUEVA VALIDACION				
			}			
			for (int i = 0; i < listaReporteCumPila.size(); i++) {
				VistaCDTO listaVista = new VistaCDTO();
				listaVista=(VistaCDTO) listaReporteCum.get(i);
				
				VistaCDTO listaVistaPila = new VistaCDTO();
				listaVistaPila=(VistaCDTO) listaReporteCumPila.get(i);

				listaVista.setAsignados(listaVistaPila.getAsignados());
				listaVista.setTerminados(listaVistaPila.getTerminados());
			}
		}
		
		if(conteo==0)
			conteo++;
		
		List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
		
		logger.info("listaReporteCum.size() "+ listaReporteCum.size());
		for (int i = 0; i < listaReporteCum.size(); i++) {							
			listaAux.add((VistaCDTO) listaReporteCum.get(i));			
			if((i+1)%conteo==0 ){
				listRetorno.add(listaAux);				
				listaAux=new ArrayList<VistaCDTO>();
			}
		}
		
		logger.info("listRetorno.size() "+listRetorno.size());
		logger.info("listRetorno "+listRetorno);

		logger.info("...SALE DEL AJAX ajaxFiltroSucursal...");
		return listRetorno;
	}
	/*::::::::::::::::::::FIN AJAX FILTRO SUCURSAL PILA::::::::::::::::::::*/

	/*::::::::::::::::::::AJAX FILTRO SUCURSAL POR CAJA::::::::::::::::::::*/
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/ajaxFiltroSucursalCajaSistemas", method = RequestMethod.GET)
	public @ResponseBody List<List> ajaxFiltroSucursalCaja(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais,
			@RequestParam(value = "valorPila", required = true, defaultValue = "0") int valorPila, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxFiltroSucursal CAJA!!!!!...");
		
		logger.info("idCeco... "+idCeco);
		logger.info("banderaCecoPadre... "+banderaCecoPadre);
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		logger.info("valorPila... "+valorPila);
		
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());
		logger.info("USUARIO "+ idUsuario);
		
		List<ChecklistDTO> listaChecklist = null;
		List<List> listaReporteCum= new ArrayList<List>();
		List<VistaCDTO> listaReporteCumPila= new ArrayList<VistaCDTO>();
		
		int porcentaje = 0, conteo=0;
		Map<String, Object> map= new HashMap<String, Object>();
		Map<String, Object> mapPila= new HashMap<String, Object>();
		List<List> listRetorno= new ArrayList<List>();
		logger.info("datos antes de los mapas... ");
		
		try {
			Date dtf = new Date();
			String date = new SimpleDateFormat("dd-MM-yyyy").format(dtf).toString(); // 18/07/2017
			String mes = date.substring(3, 5);
			String ano = date.substring(6, 10);
			String fechaReporte=""+mes+"/"+ano;
			map=reporteRegBI.ReporteVistaCumplimientoSucursal(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,0);
			logger.info("map... " + map);
			if(valorPila==1){
				mapPila=reporteRegBI.ReporteVistaCumplimientoSucursal(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,2);
				logger.info("mapPila... " + mapPila);
			}
				
		} catch (Exception e) {
			logger.info("Ocurrio algo... "+e);
		}
		if(map!=null){
			porcentaje= (Integer) map.get("porcentaje");
			logger.info("porcentaje... "+porcentaje);
			listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
			logger.info("listaChecklist... "+listaChecklist);
			listRetorno.add(listaChecklist);
			
			listaReporteCum= (List<List>) map.get("listaSucursales");
			logger.info("listaSucursales... "+listaReporteCum);
			conteo = (Integer) map.get("conteo");
			logger.info("conteo... "+conteo);
		}			
		
		if(valorPila==1){
			if(mapPila!=null){
				listaReporteCumPila= (List<VistaCDTO>) mapPila.get("listaSucursales");
				logger.info("listaReporteCumPila... "+listaReporteCumPila);
				
				// NUEVA VALIDACION
				if(listaReporteCumPila.size()>1){					
					boolean flag = false;
					int l =1;
					int sumaAnt = 0;
					int sumaAct = 0;
					VistaCDTO act = null;
					VistaCDTO ant = null;
					while(l < listaReporteCumPila.size()){	
						sumaAnt= listaReporteCumPila.get(l - 1).getAsignados()+listaReporteCumPila.get(l - 1).getTerminados();
						sumaAct= listaReporteCumPila.get(l).getAsignados()+listaReporteCumPila.get(l).getTerminados();						
						if(sumaAnt < sumaAct){
							ant=listaReporteCumPila.get(l - 1);
							act=listaReporteCumPila.get(l);							
							listaReporteCumPila.set(l - 1, act);
							listaReporteCumPila.set(l, ant);
							flag=true;
						}
						if (flag && (l + 1) == listaReporteCumPila.size()) {
							l = 0;
							flag = false;
						}
						l++;
					}				
				}
				// NUEVA VALIDACION				
			}			
			for (int i = 0; i < listaReporteCumPila.size(); i++) {
				VistaCDTO listaVista = new VistaCDTO();
				listaVista=(VistaCDTO) listaReporteCum.get(i);
				
				VistaCDTO listaVistaPila = new VistaCDTO();
				listaVistaPila=(VistaCDTO) listaReporteCumPila.get(i);

				listaVista.setAsignados(listaVistaPila.getAsignados());
				listaVista.setTerminados(listaVistaPila.getTerminados());
			}
		}
		
		if(conteo==0)
			conteo++;
		
		List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
		
		logger.info("listaReporteCum.size() "+ listaReporteCum.size());
		for (int i = 0; i < listaReporteCum.size(); i++) {							
			listaAux.add((VistaCDTO) listaReporteCum.get(i));			
			if((i+1)%conteo==0 ){
				listRetorno.add(listaAux);				
				listaAux=new ArrayList<VistaCDTO>();
			}
		}
		
		logger.info("listRetorno.size() "+listRetorno.size());
		logger.info("listRetorno "+listRetorno);

		logger.info("...SALE DEL AJAX ajaxFiltroSucursal CAJA...");
		return listRetorno;
	}
	/*::::::::::::::::::::FIN AJAX FILTRO SUCURSAL POR CAJA::::::::::::::::::::*/

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/ajaxFiltroSucursalCheckSistemas", method = RequestMethod.GET)
	public @ResponseBody List<List> ajaxFiltroSucursalCheck(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxFiltroSucursalCheck...");
		
		logger.info("idCeco... "+idCeco);
		logger.info("banderaCecoPadre... "+banderaCecoPadre);
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);

		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());
		logger.info("USUARIO "+ idUsuario);
		
		List<ChecklistDTO> listaChecklist = null;
		List<List> listaReporteCum= new ArrayList<List>();
		
		int porcentaje = 0, conteo=0;
		Map<String, Object> map= new HashMap<String, Object>();
		List<List> listRetorno= new ArrayList<List>();
		String fechaReporte=""+seleccionMes+"/"+seleccionAno;
		logger.info("datos antes de los mapas... ");
		
		try {		
			map=reporteBI.ReporteVistaCumplimientoSucursal(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,1);
			logger.info("map... " + map);			
		} catch (Exception e) {
			logger.info("Ocurrio algo... "+e);
		}
		
		if(map!=null){
			porcentaje= (Integer) map.get("porcentaje");
			logger.info("porcentaje... "+porcentaje);
			listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
			logger.info("listaChecklist... "+listaChecklist);
			listRetorno.add(listaChecklist);
			
			listaReporteCum= (List<List>) map.get("listaSucursales");
			logger.info("listaSucursales... "+listaReporteCum);
			conteo = (Integer) map.get("conteo");
			logger.info("conteo... "+conteo);
		}			
			
		List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
		
		logger.info("listaReporteCum.size() "+ listaReporteCum.size());
		if(listaReporteCum.size()==0){
			
		}else{
			for (int i = 0; i < conteo; i++) {					
				listaAux.add((VistaCDTO) listaReporteCum.get(i));				
				if((i+1)%conteo==0){
					listRetorno.add(listaAux);				
					listaAux=new ArrayList<VistaCDTO>();
				}
			}
		}
		
		List<VistaCDTO> listaConteo=new ArrayList<VistaCDTO>();
		VistaCDTO conteoVista= new VistaCDTO();
		conteoVista.setTotal(conteo);
		listaConteo.add(conteoVista);
		listRetorno.add(listaConteo);
		logger.info("listRetorno.size() "+listRetorno.size());
		logger.info("listRetorno "+listRetorno);

		logger.info("...SALE DEL AJAX ajaxFiltroSucursalCheck...");
		return listRetorno;
	}
	
	/*AJAX FILTRO SUCURSAL CHECK PILA */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/ajaxFiltroSucursalCheckPilaSistemas", method = RequestMethod.GET)
	public @ResponseBody List<List> ajaxFiltroSucursalCheckPila(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxFiltroSucursalCheckPILA...");
		
		logger.info("idCeco... "+idCeco);
		logger.info("banderaCecoPadre... "+banderaCecoPadre);
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);

		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());
		logger.info("USUARIO "+ idUsuario);
		
		List<ChecklistDTO> listaChecklist = null;
		List<List> listaReporteCum= new ArrayList<List>();
		
		int porcentaje = 0, conteo=0;
		Map<String, Object> map= new HashMap<String, Object>();
		List<List> listRetorno= new ArrayList<List>();
		String fechaReporte=""+seleccionMes+"/"+seleccionAno;
		logger.info("datos antes de los mapas... ");
		
		try {		
			map=reporteBI.ReporteVistaCumplimientoSucursalPila(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,1);
			logger.info("map... " + map);			
		} catch (Exception e) {
			logger.info("Ocurrio algo... "+e);
		}
		
		if(map!=null){
			porcentaje= (Integer) map.get("porcentaje");
			logger.info("porcentaje... "+porcentaje);
			listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
			logger.info("listaChecklist... "+listaChecklist);
			listRetorno.add(listaChecklist);
			
			listaReporteCum= (List<List>) map.get("listaSucursales");
			logger.info("listaSucursales... "+listaReporteCum);
			conteo = (Integer) map.get("conteo");
			logger.info("conteo... "+conteo);
		}			
			
		List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
		
		logger.info("listaReporteCum.size() "+ listaReporteCum.size());
		if(listaReporteCum.size()==0){
			
		}else{
			for (int i = 0; i < conteo; i++) {					
				listaAux.add((VistaCDTO) listaReporteCum.get(i));				
				if((i+1)%conteo==0){
					listRetorno.add(listaAux);				
					listaAux=new ArrayList<VistaCDTO>();
				}
			}
		}
		
		List<VistaCDTO> listaConteo=new ArrayList<VistaCDTO>();
		VistaCDTO conteoVista= new VistaCDTO();
		conteoVista.setTotal(conteo);
		listaConteo.add(conteoVista);
		listRetorno.add(listaConteo);
		logger.info("listRetorno.size() "+listRetorno.size());
		logger.info("listRetorno "+listRetorno);

		logger.info("...SALE DEL AJAX ajaxFiltroSucursalCheckPILA...");
		return listRetorno;
	}
	/*FIN AJAX FILTRO SUCURSAL CHECK PILA*/
	
	@RequestMapping(value = "/ajaxValidaDetalleSistemas", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> ajaxValidaDetalle(
			@RequestParam(value = "idCecoHijo", required = true, defaultValue = "0") int idCecoHijo,
			@RequestParam(value = "idCheck", required = true, defaultValue = "0") int idCheck,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxValidaDetalle...");

		Map<String, Object> map= null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());		
		String fechaC = seleccionMes+"/"+seleccionAno;
		
		logger.info("ID CECO " + idCecoHijo);
		logger.info("ID CHECK " + idCheck);
		logger.info("ID USUARIO " + idUsuario);
		logger.info("ID FECHA " + fechaC);
		
		logger.info("datos antes del mapa... ");
		
		try {
			map=reporteBI.ReporteDetVC(idCecoHijo, idCheck, idUsuario, fechaC);
			
			if(map!=null){
				if(!map.isEmpty()){
					this.mapGlobal=map;
				}else{
					map=null;
					this.mapGlobal=null;
				}					
			}					
		} catch (Exception e) {
			logger.info("Ocurrio algo... "+e);
		}
		logger.info("map... " + map);		
		logger.info("...SALE DEL AJAX ajaxValidaDetalle...");
		
		return map;
	}
	
	
	
	
}