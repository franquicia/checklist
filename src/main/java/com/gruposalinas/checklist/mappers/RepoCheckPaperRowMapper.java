package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.RepoCheckPaperDTO;

public class RepoCheckPaperRowMapper implements RowMapper<RepoCheckPaperDTO> {

	public RepoCheckPaperDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		RepoCheckPaperDTO rc = new RepoCheckPaperDTO();
		
		rc.setCeco(rs.getString("FCID_CECO"));
		rc.setIdCheclist(rs.getInt("FIID_CHECKLIST"));
		rc.setNombreCheck(rs.getString("CHECKLIST"));
		rc.setCali(rs.getDouble("FICALIFICACION"));
		rc.setFini(rs.getString("FDINICIO"));
		rc.setFfin(rs.getString("FDTERMINO"));
		rc.setBitacora(rs.getInt("FIID_BITACORA"));
		rc.setBandera(rs.getInt("BANDERA"));
		rc.setSemana(rs.getInt("SEMANA"));
		
		
		return rc;
	}

}


