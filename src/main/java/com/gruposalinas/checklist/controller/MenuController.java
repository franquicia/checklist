package com.gruposalinas.checklist.controller;

import com.gruposalinas.checklist.business.ActivarCheckListBI;
import com.gruposalinas.checklist.business.AdmPregZonasBI;
import com.gruposalinas.checklist.business.AdmTipoZonaBI;
import com.gruposalinas.checklist.business.AdmZonasBI;
import com.gruposalinas.checklist.business.AdminSupBI;
import com.gruposalinas.checklist.business.AltaGerenteBI;
import com.gruposalinas.checklist.business.AsignacionesSupBI;
import com.gruposalinas.checklist.business.AsistenciaSupervisorBI;
import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.business.CheckinBI;
import com.gruposalinas.checklist.business.ChecklistAdminBI;
import com.gruposalinas.checklist.business.ChecklistAdminValidaBI;
import com.gruposalinas.checklist.business.ChecklistPreguntasComBI;
import com.gruposalinas.checklist.business.ChecklistUsuarioBI;
import com.gruposalinas.checklist.business.ChecklistUsuarioGpoBI;
import com.gruposalinas.checklist.business.ConsultaAseguradorBI;
import com.gruposalinas.checklist.business.ConsultaAsistenciaBI;
import com.gruposalinas.checklist.business.ConsultaBI;
import com.gruposalinas.checklist.business.ConsultaCecoProtocoloBI;
import com.gruposalinas.checklist.business.ConsultaDashBoardBI;
import com.gruposalinas.checklist.business.ConsultaDistribucionBI;
import com.gruposalinas.checklist.business.ConsultaSupervisionBI;
import com.gruposalinas.checklist.business.ConsultaVisitasBI;
import com.gruposalinas.checklist.business.DetalleProtoBI;
import com.gruposalinas.checklist.business.LeerExcelBI;
import com.gruposalinas.checklist.business.MenuBI;
import com.gruposalinas.checklist.business.MenusSupBI;
import com.gruposalinas.checklist.business.OrdenGrupoBI;
import com.gruposalinas.checklist.business.ParametroBI;
import com.gruposalinas.checklist.business.PerfilUsuarioBI;
import com.gruposalinas.checklist.business.PerfilesNuevoBI;
import com.gruposalinas.checklist.business.PerfilesSupBI;
import com.gruposalinas.checklist.business.ProtocoloBI;
import com.gruposalinas.checklist.business.ProtocoloByCheckBI;
import com.gruposalinas.checklist.business.RepMedicionBI;
import com.gruposalinas.checklist.business.ReporteImgBI;
import com.gruposalinas.checklist.business.ReporteMedicionBI;
import com.gruposalinas.checklist.business.RolesSupBI;
import com.gruposalinas.checklist.business.ServiciosMiGestionBI;
import com.gruposalinas.checklist.business.SupervisorBI;
import com.gruposalinas.checklist.business.VersionCheckAdmBI;
import com.gruposalinas.checklist.domain.ActivarCheckListDTO;
import com.gruposalinas.checklist.domain.AdmPregZonaDTO;
import com.gruposalinas.checklist.domain.AdmTipoZonaDTO;
import com.gruposalinas.checklist.domain.AdminSupDTO;
import com.gruposalinas.checklist.domain.AdminZonaDTO;
import com.gruposalinas.checklist.domain.AltaGerenteDTO;
import com.gruposalinas.checklist.domain.AsigCecosMasivaSup;
import com.gruposalinas.checklist.domain.AsistenciaDTO;
import com.gruposalinas.checklist.domain.AsistenciaSupervisorDTO;
import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.CheckinDTO;
import com.gruposalinas.checklist.domain.ChecklistLayoutDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.domain.ChecklistUsuarioDTO;
import com.gruposalinas.checklist.domain.ConsultaAseguradorDTO;
import com.gruposalinas.checklist.domain.ConsultaAsistenciaDTO;
import com.gruposalinas.checklist.domain.ConsultaCecoProtocoloDTO;
import com.gruposalinas.checklist.domain.ConsultaDTO;
import com.gruposalinas.checklist.domain.ConsultaDashBoardDTO;
import com.gruposalinas.checklist.domain.ConsultaDistribucionDTO;
import com.gruposalinas.checklist.domain.ConsultaSupervisionDTO;
import com.gruposalinas.checklist.domain.ConsultaVisitasDTO;
import com.gruposalinas.checklist.domain.DetalleProtoDTO;
import com.gruposalinas.checklist.domain.FoliosMtoDTO;
import com.gruposalinas.checklist.domain.MenusSupDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.OrdenGrupoDTO;
import com.gruposalinas.checklist.domain.ParametroDTO;
import com.gruposalinas.checklist.domain.PerfilUsuarioDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.ProtocoloByCheckDTO;
import com.gruposalinas.checklist.domain.RepMedicionDTO;
import com.gruposalinas.checklist.domain.ReporteImgDTO;
import com.gruposalinas.checklist.domain.ReporteMedPregDTO;
import com.gruposalinas.checklist.domain.ReporteMedicionDTO;
import com.gruposalinas.checklist.domain.SupervisorDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MenuController {

    private static final Logger logger = LogManager.getLogger(MenuController.class);
    private static final int MAX_FILE_SIZE = 1024 * 1024 * 100;// 100MB

    @Autowired
    private MenuBI menuBI;

    @Autowired
    private ConsultaBI consultaBI;

    @Autowired
    ChecklistPreguntasComBI checklistPreguntasComBI;
    @Autowired
    DetalleProtoBI detalleProtoBI;
    @Autowired
    CheckinBI checkinBI;
    @Autowired
    CecoBI cecobi;
    @Autowired
    SupervisorBI supervisorBI;
    @Autowired
    AsistenciaSupervisorBI asistenciaSupervisorBI;
    @Autowired
    ConsultaDashBoardBI consultaDashBoardBI;
    @Autowired
    ConsultaAsistenciaBI consultaAsistenciaBI;
    @Autowired
    ConsultaDistribucionBI consultaDistribucionBI;
    @Autowired
    ConsultaSupervisionBI consultaSupervisionBI;
    @Autowired
    ConsultaVisitasBI consultaVisitasBI;
    @Autowired
    ServiciosMiGestionBI serviciosMiGestionBI;
    @Autowired
    ChecklistUsuarioGpoBI checkListUsuarioGpoBI;
    @Autowired
    PerfilUsuarioBI perfilusuariobi;
    @Autowired
    PerfilesNuevoBI perfilesNuevoBI;
    @Autowired
    ReporteMedicionBI reporteMedicionBI;
    @Autowired
    ProtocoloByCheckBI protocoloByCheckBI;
    @Autowired
    ReporteImgBI reporteImgBI;
    @Autowired
    ConsultaCecoProtocoloBI consultaCecoProtocoloBI;
    @Autowired
    ConsultaAseguradorBI consultaAseguradorBI;
    @Autowired
    AsignacionesSupBI asignacionesSupBI;
    @Autowired
    PerfilesSupBI perfilesSupBI;
    @Autowired
    MenusSupBI menusSupBI;
    @Autowired
    RolesSupBI rolesSupBI;
    @Autowired
    AdminSupBI adminSupBI;
    @Autowired
    AltaGerenteBI altaGerenteBI;
    @Autowired
    RepMedicionBI repMedicionBI;
    @Autowired
    LeerExcelBI leerExcelBI;
    @Autowired
    ActivarCheckListBI activarCheckListBI;
    @Autowired
    ChecklistAdminBI checklistAdminBI;
    @Autowired
    ChecklistAdminValidaBI checklistValidaAdminBI;
    @Autowired
    ChecklistUsuarioBI checklistUsuariobi;
    @Autowired
    AdmTipoZonaBI admTipoZonaBI;
    @Autowired
    AdmZonasBI admZonasBI;
    @Autowired
    AdmPregZonasBI admPregZonasBI;
    @Autowired
    ProtocoloBI protocoloBI;
    @Autowired
    VersionCheckAdmBI versionCheckAdmBI;
    @Autowired
    OrdenGrupoBI ordenGrupoBI;
    @Autowired
    ParametroBI parametrobi;
    @Autowired
    ParametroBI parametroBI;

    @RequestMapping(value = "/irBuscador.htm", method = RequestMethod.GET)
    public ModelAndView masBuscados(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return null;
        		//menuBI.masBuscados();
    }

    @RequestMapping(value = {"tienda/queEs.htm", "central/queEs.htm"}, method = RequestMethod.GET)
    public ModelAndView queEs(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("queEs");
    }

    @RequestMapping(value = {"tienda/queVerificar.htm", "central/queVerificar.htm"}, method = RequestMethod.GET)
    public ModelAndView queVerificar(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("queVerificar");
    }

    @RequestMapping(value = {"tienda/queHacer.htm", "central/queHacer.htm"}, method = RequestMethod.GET)
    public ModelAndView queHacer(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("queHacer");
    }

    @RequestMapping(value = {"tienda/guiasOperativas.htm", "central/guiasOperativas.htm"}, method = RequestMethod.GET)
    public ModelAndView guiasOperativas(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("guiasOperativas");
    }

    @RequestMapping(value = {"tienda/miApertura.htm", "central/miApertura.htm"}, method = RequestMethod.GET)
    public ModelAndView miApertura(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("miApertura");
    }

    @RequestMapping(value = {"tienda/juntaInicioDia.htm", "central/juntaInicioDia.htm"}, method = RequestMethod.GET)
    public ModelAndView juntaInicioDia(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("juntaInicioDia");
    }

    @RequestMapping(value = {"tienda/preparandoCierre.htm",
        "central/preparandoCierre.htm"}, method = RequestMethod.GET)
    public ModelAndView preparandoCierre(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("preparandoCierre");
    }

    @RequestMapping(value = {"tienda/cobroResultados.htm", "central/cobroResultados.htm"}, method = RequestMethod.GET)
    public ModelAndView cobroResultados(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("cobroResultados");
    }

    @RequestMapping(value = {"tienda/ayuda.htm", "central/ayuda.htm"}, method = RequestMethod.GET)
    public ModelAndView ayuda(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        return new ModelAndView("ayudaAndroid");
    }

    @RequestMapping(value = {"tienda/carrusel7s.htm", "central/carrusel7s.htm"}, method = RequestMethod.GET)
    public ModelAndView carrusel7s(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        return new ModelAndView("carrusel7s");
    }

    /**
     * *************************************************
     * Reporte Supervisión
     * ************************************************************
     */
    @RequestMapping(value = {"tienda/reporteCeco.htm", "central/reporteCeco.htm"}, method = RequestMethod.GET)
    public ModelAndView reporteCeco(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        //logger.info("Entra a Reporte Ceco");
        String salida = "reporteCeco";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            List<ConsultaCecoProtocoloDTO> listaCecos = consultaCecoProtocoloBI.obtieneConsultaCeco();

            if (listaCecos.size() > 0) {
                mv.addObject("datos", 1);
                for (int i = 0; i < listaCecos.size(); i++) {
                    if (listaCecos.get(i).getIdCeco() == 996783 || listaCecos.get(i).getIdCeco() == 999995) {
                        listaCecos.remove(i);
                    }
                }
            } else {
                mv.addObject("datos", 0);
            }
            mv.addObject("lista", listaCecos);
            /*
			 * List<ConsultaDTO> lista = new ArrayList<ConsultaDTO>();
			 * lista=consultaBI.obtieneConsultaCeco(323);
			 *
			 * List<ConsultaDTO> lista2 = new ArrayList<ConsultaDTO>();
			 * lista2=consultaBI.obtieneConsultaCeco(324);
			 *
			 * if(lista.size()>0) { mv.addObject("datos", 1);
			 *
			 * for(int i=0; i<lista.size(); i++){
			 * if(lista.get(i).getIdCeco()==996783){ lista.remove(i); } }
			 *
			 *
			 * }else { mv.addObject("datos", 0); } mv.addObject("lista", lista);
             */
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/reporteFecha.htm", "central/reporteFecha.htm"}, method = RequestMethod.POST)
    public ModelAndView reporteFecha(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idCeco", required = true, defaultValue = "") int idCeco,
            @RequestParam(value = "nomCeco", required = false, defaultValue = "") String nomCeco)
            throws ServletException, IOException {

        //logger.info("Entra a Reporte Fecha " + idCeco);
        String salida = "reporteFecha";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            List<ConsultaCecoProtocoloDTO> listaDatos = new ArrayList<>();
            SimpleDateFormat parseador = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");

            ConsultaCecoProtocoloDTO consultaCeco = new ConsultaCecoProtocoloDTO();
            consultaCeco.setIdCeco(idCeco);
            List<ConsultaCecoProtocoloDTO> listaFechas = consultaCecoProtocoloBI.obtieneConsultaBitacora(consultaCeco);
            if (listaFechas.size() > 0) {
                mv.addObject("datos", 1);
                for (ConsultaCecoProtocoloDTO rep : listaFechas) {
                    Date date = parseador.parse(rep.getFechaInicio());
                    rep.setFechaInicio(formateador.format(date));
                    Date date2 = parseador.parse(rep.getFechaTermino());
                    rep.setFechaTermino(formateador.format(date2));
                    listaDatos.add(rep);
                }
                logger.info("listaDatos: " + listaDatos);

            } else {
                mv.addObject("datos", 0);
            }
            mv.addObject("lista", listaDatos);
            mv.addObject("idCeco", idCeco);
            mv.addObject("nomCeco", nomCeco);
            /*
			 * List<ConsultaDTO> lista = new ArrayList<ConsultaDTO>();
			 * lista=consultaBI.obtieneConsultaBitacora(idCeco, idChecklist);
			 * if(lista.size()>0) { mv.addObject("datos", 1); }else {
			 * mv.addObject("datos", 0); } mv.addObject("lista", lista);
			 * mv.addObject("idCeco", idCeco); mv.addObject("nomCeco", nomCeco);
			 * mv.addObject("idChecklist", idChecklist);
			 * mv.addObject("nomChecklist", nomChecklist);
             */
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/reporteProtocolo.htm",
        "central/reporteProtocolo.htm"}, method = RequestMethod.POST)
    public ModelAndView reporteProtocolo(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idCeco", required = true, defaultValue = "") int idCeco,
            @RequestParam(value = "nomCeco", required = false, defaultValue = "") String nomCeco,
            @RequestParam(value = "fechaTermino", required = false, defaultValue = "") String fechaTermino)
            throws ServletException, IOException {

        //logger.info("Entra a Reporte Protocolo " + idCeco + nomCeco);
        String salida = "reporteProtocolo";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            ConsultaCecoProtocoloDTO consultaCeco = new ConsultaCecoProtocoloDTO();
            consultaCeco.setIdCeco(idCeco);
            consultaCeco.setFechaTermino(fechaTermino);
            List<ConsultaCecoProtocoloDTO> listaProtocolos = consultaCecoProtocoloBI
                    .obtieneConsultaChecklist(consultaCeco);
            if (listaProtocolos.size() > 0) {
                mv.addObject("datos", 1);
            } else {
                mv.addObject("datos", 0);
            }
            System.out.println("Paso");
            System.out.println(request.getSession().getAttribute("nombre").toString());
            System.out.println(request.getSession().getAttribute("perfilSupervision").toString());
            if (request.getSession().getAttribute("perfilSupervision").toString().equalsIgnoreCase("3") || request.getSession().getAttribute("perfilSupervision").toString().equalsIgnoreCase("5")) {
                List<ParametroDTO> validaGuardaRep = parametroBI.obtieneParametros("checklistPedestal");
                String[] listaChecksPedestal = null;
                System.out.println("Llego a obtener checklist");
                if (validaGuardaRep != null) {
                    for (ParametroDTO param : validaGuardaRep) {
                        try {

                            if (param.getActivo() == 1) {
                                listaChecksPedestal = param.getValor().split(",");
                                System.out.println(listaChecksPedestal.toString());
                            }
                        } catch (Exception e) {
                            listaChecksPedestal = null;
                        }
                    }
                }
                //Crea lista checklist
                List<Integer> listaChecks = new ArrayList<Integer>();

                //int cont=0;
                for (String auxPedestal : listaChecksPedestal) {
                    //listaChecks[cont]=Integer.parseInt(auxPedestal);
                    listaChecks.add(Integer.parseInt(auxPedestal));
                    //cont++;
                }
                listaProtocolos.removeIf(obj -> !listaChecks.contains(obj.getIdChecklist()));
            }

            mv.addObject("lista", listaProtocolos);
            mv.addObject("idCeco", idCeco);
            mv.addObject("nomCeco", nomCeco);
            mv.addObject("fechaTermino", fechaTermino);
            /*
			 * List<ConsultaDTO> lista = new ArrayList<ConsultaDTO>();
			 * lista=consultaBI.obtieneConsultaChecklist((323));
			 * List<ConsultaDTO> listaZonas = new ArrayList<ConsultaDTO>();
			 * listaZonas=consultaBI.obtieneConsultaChecklist((324));
			 * if(lista.size()>0 || listaZonas.size()>0) { mv.addObject("datos",
			 * 1); }else { mv.addObject("datos", 0); } mv.addObject("lista",
			 * lista); mv.addObject("listaZonas", listaZonas);
			 * mv.addObject("idCeco", idCeco); mv.addObject("nomCeco", nomCeco);
             */
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    // http://192.168.2.39:8080/checklist/central/reporteDetalleProtocolo.htm?idBitacora=26289
    @RequestMapping(value = {"tienda/reporteDetalleProtocolo.htm",
        "central/reporteDetalleProtocolo.htm"}, method = RequestMethod.POST)
    public ModelAndView reporteDetalleProtocolo(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idBitacora", required = false, defaultValue = "") String idBitacora,
            @RequestParam(value = "fechaTermino", required = false, defaultValue = "") String fechaTermino,
            @RequestParam(value = "nomChecklist", required = false, defaultValue = "") String nomChecklist,
            @RequestParam(value = "nomCeco", required = false, defaultValue = "") String nomCeco,
            @RequestParam(value = "idUsuario", required = false, defaultValue = "") String idUsuario,
            @RequestParam(value = "nomUsuario", required = false, defaultValue = "") String nomUsuario)
            throws ServletException, IOException {

        //logger.info("Entra a Reporte Detalle Protocolo " + idBitacora);
        String salida = "cuestionario-semana1";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneDatosGen(idBitacora);
            if (lista.size() > 0) {
                mv.addObject("datos", 1);
            } else {
                mv.addObject("datos", 0);
            }
            /*List<String> listaRutas = new ArrayList<>();
			FRQConstantes consFRQ = new FRQConstantes();
			if (lista.size() > 0) {
				mv.addObject("datos", 1);
				for (int j = 0; j < lista.size(); j++) {
					listaRutas.add(lista.get(j).getRuta());
					String rutaX = consFRQ.getURLServerImagenes() + lista.get(j).getRuta();
					if(!rutaX.contains("null")){
						lista.get(j).setRuta(rutaX);
					}
				}
			} else {
				mv.addObject("datos", 0);
			}*/
            //logger.info("lista: "+lista);
            List<ModuloDTO> listaMod = new ArrayList<ModuloDTO>();
            HashMap<String, List<ChecklistPreguntasComDTO>> listaMods = new HashMap<String, List<ChecklistPreguntasComDTO>>();
            int cont = 0;
            for (ChecklistPreguntasComDTO aux : lista) {
                String modulo = aux.getModulo();
                if (listaMods.containsKey(modulo)) {
                    listaMods.get(modulo).add(aux);
                    cont++;
                } else {
                    listaMods.put(modulo, new ArrayList<ChecklistPreguntasComDTO>());
                    listaMods.get(modulo).add(aux);
                    ModuloDTO modAux = new ModuloDTO();
                    modAux.setIdModulo(aux.getIdModulo());
                    modAux.setNombre(modulo);
                    listaMod.add(modAux);
                    cont++;
                }

            }

            if (nomUsuario != null && !nomUsuario.equals("")) {
                mv.addObject("nomUser", 1);
            } else {
                mv.addObject("nomUser", 0);
            }

            mv.addObject("modulos", listaMod);
            mv.addObject("lista", lista);
            mv.addObject("nomCeco", nomCeco);
            mv.addObject("nomChecklist", nomChecklist);
            mv.addObject("fechaTermino", fechaTermino);
            mv.addObject("nomUsuario", nomUsuario);
            mv.addObject("idUsuario", idUsuario);

        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;

    }

    @RequestMapping(value = {"tienda/reporteUsuario.htm", "central/reporteUsuario.htm"}, method = RequestMethod.GET)
    public ModelAndView reporteUsuario(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        //logger.info("Entra a Reporte Usuario");
        String salida = "reporteUsuario";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            List<ConsultaAseguradorDTO> lista = consultaAseguradorBI.obtieneConsultaAsegurador();

            if (lista.size() > 0) {
                mv.addObject("datos", 1);
            } else {
                mv.addObject("datos", 0);
            }
            mv.addObject("lista", lista);
            /*
			 * DetalleProtoDTO detalle = new DetalleProtoDTO();
			 * detalle.setIdOrdeGrupo(323);
			 *
			 * List<DetalleProtoDTO> lista =
			 * detalleProtoBI.ObtieneDetalleProto(detalle);
			 *
			 * if(lista.size()>0) { mv.addObject("datos", 1); }else {
			 * mv.addObject("datos", 0); }
			 *
			 *
			 *
			 * mv.addObject("lista", lista);
             */
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/reporteUsuarioCeco.htm",
        "central/reporteUsuarioCeco.htm"}, method = RequestMethod.POST)
    public ModelAndView reporteUsuarioCeco(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idUsuario", required = true, defaultValue = "") int idUsuario,
            @RequestParam(value = "nomUsuario", required = false, defaultValue = "") String nomUsuario)
            throws ServletException, IOException {

        //logger.info("Entra a Reporte Usuario Ceco");
        String salida = "reporteUsuCeco";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            ConsultaAseguradorDTO consultaAseguradorSucursal = new ConsultaAseguradorDTO();
            consultaAseguradorSucursal.setIdUsuario(idUsuario);
            List<ConsultaAseguradorDTO> lista = consultaAseguradorBI
                    .obtieneConsultaSucursal(consultaAseguradorSucursal);
            if (lista.size() > 0) {
                mv.addObject("datos", 1);

                for (int i = 0; i < lista.size(); i++) {
                    if (lista.get(i).getIdCeco() == 996783 || lista.get(i).getIdCeco() == 999995) {
                        lista.remove(i);
                    }
                }

            } else {
                mv.addObject("datos", 0);
            }
            mv.addObject("lista", lista);
            mv.addObject("nomUsuario", nomUsuario);
            mv.addObject("idUsuario", idUsuario);
            /*
			 * DetalleProtoDTO detalle = new DetalleProtoDTO();
			 * detalle.setIdUsuario(idUsuario); detalle.setIdOrdeGrupo(323);
			 *
			 * List<DetalleProtoDTO> lista =
			 * detalleProtoBI.ObtieneNombreCeco(detalle);
			 *
			 * mv.addObject("idUsuario", idUsuario); mv.addObject("idProtocolo",
			 * idProtocolo); if(lista.size()>0) { mv.addObject("datos", 1);
			 *
			 * for(int i=0; i<lista.size(); i++){
			 * if(lista.get(i).getIdCeco().equals("996783")){ lista.remove(i); }
			 * }
			 *
			 * }else { mv.addObject("datos", 0); } mv.addObject("lista", lista);
			 * mv.addObject("nomChecklist", nomChecklist);
			 * mv.addObject("nomUsuario", nomUsuario);
             */
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/reporteUsuBitacora.htm",
        "central/reporteUsuBitacora.htm"}, method = RequestMethod.POST)
    public ModelAndView reporteUsuBitacora(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
            @RequestParam(value = "nomCeco", required = false, defaultValue = "") String nomCeco,
            @RequestParam(value = "idUsuario", required = true, defaultValue = "") String idUsuario,
            @RequestParam(value = "nomUsuario", required = false, defaultValue = "") String nomUsuario)
            throws ServletException, IOException {

        //logger.info("Entra a Reporte Usuario Bitacora");
        String salida = "reporteUsuBitacora";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            SimpleDateFormat parseador = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formateador = new SimpleDateFormat("yyyy/MM/dd");
            ConsultaAseguradorDTO consultaAseguradorBitacora = new ConsultaAseguradorDTO();
            consultaAseguradorBitacora.setIdUsuario(Integer.parseInt(idUsuario));
            consultaAseguradorBitacora.setIdCeco(Integer.parseInt(idCeco));
            List<ConsultaAseguradorDTO> lista = consultaAseguradorBI
                    .obtieneConsultaBitaCerr(consultaAseguradorBitacora);
            List<ConsultaAseguradorDTO> listaFechas = new ArrayList<>();
            if (lista.size() > 0) {
                mv.addObject("datos", 1);
                for (ConsultaAseguradorDTO rep : lista) {
                    Date date2 = parseador.parse(rep.getFechaTermino());
                    rep.setFechaTermino(formateador.format(date2));
                    listaFechas.add(rep);
                }
                logger.info("listaFechas: " + listaFechas);
            } else {
                mv.addObject("datos", 0);
            }
            mv.addObject("idUsuario", idUsuario);
            mv.addObject("idCeco", idCeco);
            mv.addObject("lista", listaFechas);
            mv.addObject("nomCeco", nomCeco);
            mv.addObject("nomUsuario", nomUsuario);
            /*
			 * DetalleProtoDTO detalle = new DetalleProtoDTO();
			 * detalle.setIdUsuario(idUsuario); detalle.setIdCeco(idCeco);
			 * detalle.setIdCheklist(idProtocolo);
			 *
			 * List<DetalleProtoDTO> lista =
			 * detalleProtoBI.ObtieneBitacoraCerrada(detalle);
			 *
			 * mv.addObject("idUsuario", idUsuario); mv.addObject("idProtocolo",
			 * idProtocolo); mv.addObject("idCeco", idCeco); if(lista.size()>0)
			 * { mv.addObject("datos", 1); }else { mv.addObject("datos", 0); }
			 * mv.addObject("lista", lista); mv.addObject("nomCeco", nomCeco);
			 * mv.addObject("nomChecklist", nomChecklist);
			 * mv.addObject("nomUsuario", nomUsuario);
             */
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/reporteUsuProtocolo.htm",
        "central/reporteUsuProtocolo.htm"}, method = RequestMethod.POST)
    public ModelAndView reporteUsuarioProtocolo(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "fechaTermino", required = false, defaultValue = "") String fechaTermino,
            @RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
            @RequestParam(value = "nomCeco", required = false, defaultValue = "") String nomCeco,
            @RequestParam(value = "idUsuario", required = true, defaultValue = "") String idUsuario,
            @RequestParam(value = "nomUsuario", required = false, defaultValue = "") String nomUsuario)
            throws ServletException, IOException {

        //logger.info("Entra a Reporte Usuario Protocolo");
        String salida = "reporteUsuProtocolo";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            ConsultaAseguradorDTO consultaAsegProto = new ConsultaAseguradorDTO();
            consultaAsegProto.setFechaTermino(fechaTermino);
            consultaAsegProto.setIdCeco(Integer.parseInt(idCeco));

            List<ConsultaAseguradorDTO> lista = consultaAseguradorBI.obtieneProtocolo(consultaAsegProto);

            if (lista.size() > 0) {
                mv.addObject("datos", 1);
            } else {
                mv.addObject("datos", 0);
            }
            mv.addObject("lista", lista);
            mv.addObject("nomUsuario", nomUsuario);
            mv.addObject("nomCeco", nomCeco);
            mv.addObject("idUsuario", idUsuario);
            mv.addObject("fechaTermino", fechaTermino);
            /*
			 * DetalleProtoDTO detalle = new DetalleProtoDTO();
			 * detalle.setIdUsuario(idUsuario); detalle.setIdOrdeGrupo(323);
			 * List<DetalleProtoDTO> lista =
			 * detalleProtoBI.ObtieneNombreGrupo(detalle);
			 *
			 * DetalleProtoDTO detalleZona = new DetalleProtoDTO();
			 * detalleZona.setIdUsuario(idUsuario);
			 * detalleZona.setIdOrdeGrupo(324); List<DetalleProtoDTO> listaZona
			 * = detalleProtoBI.ObtieneNombreGrupo(detalleZona);
			 *
			 * mv.addObject("idUsuario", idUsuario);
			 *
			 * if(lista.size()>0 || listaZona.size()>0) { mv.addObject("datos",
			 * 1); }else { mv.addObject("datos", 0); } mv.addObject("lista",
			 * lista); mv.addObject("listaZona", listaZona);
			 * mv.addObject("nomUsuario", nomUsuario);
             */
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/reporteAsistencia.htm",
        "central/reporteAsistencia.htm"}, method = RequestMethod.POST)
    public ModelAndView repAsistencia(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idUsuario", required = false, defaultValue = "") String idUsuario,
            @RequestParam(value = "nomUsuario", required = false, defaultValue = "") String nomUsuario,
            @RequestParam(value = "idProtocolo", required = false, defaultValue = "") String idProtocolo,
            @RequestParam(value = "anio", required = false, defaultValue = "") String anio,
            @RequestParam(value = "mes", required = false, defaultValue = "") String mes,
            @RequestParam(value = "nomMes", required = false, defaultValue = "") String nomMes,
            @RequestParam(value = "idCeco", required = false, defaultValue = "") String idCeco)
            throws ServletException, IOException {

        //logger.info("Entra a Reporte Ceco");
        String salida = "repAsistencia";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            DetalleProtoDTO detalle = new DetalleProtoDTO();
            detalle.setIdOrdeGrupo(323);

            List<DetalleProtoDTO> lista = detalleProtoBI.ObtieneDetalleProto(detalle);

            List<ConsultaDTO> lista2 = new ArrayList<ConsultaDTO>();
            lista2 = consultaBI.obtieneConsultaChecklist((323));

            mv.addObject("idUsuario", idUsuario);

            int anioI = Integer.parseInt(anio.trim());
            int mesI = Integer.parseInt(mes.trim());
            int idUsuarioI = Integer.parseInt(idUsuario);
            idCeco = "999995";
            mv.addObject("nomUsuario", nomUsuario);
            mv.addObject("nomMes", nomMes);

            CheckinDTO asistencia = new CheckinDTO();
            asistencia.setIdBitacora(0);
            asistencia.setIdUsuario(idUsuarioI);
            asistencia.setIdCeco(null);
            asistencia.setFecha(null);
            asistencia.setLatitud(null);
            asistencia.setLongitud(null);
            asistencia.setBandera(0);

            List<CheckinDTO> lista3 = checkinBI.buscaCheckin(asistencia);

            Calendar fecha2 = new GregorianCalendar(anioI, mesI - 1, 1);

            // Get the number of days in that month
            int days = fecha2.getActualMaximum(Calendar.DAY_OF_MONTH);

            List<AsistenciaDTO> Asistencia = new ArrayList<AsistenciaDTO>();

            for (int i = 1; i < days + 1; i++) {
                List<Calendar> diaX = new ArrayList<Calendar>();
                List<CheckinDTO> CheckDia = new ArrayList<CheckinDTO>();
                String sucursalesVis = "";
                ;
                List<String> Sucursales = new ArrayList<String>();
                for (CheckinDTO aux : lista3) {

                    String fecha = aux.getFecha();

                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("es", "ES"));
                    try {
                        if (fecha != null && fecha != "null") {

                            cal.setTime(sdf.parse(fecha));
                            int anioX = cal.get(Calendar.YEAR);
                            int mesX = cal.get(Calendar.MONTH);

                            int diaMES = cal.get(Calendar.DAY_OF_MONTH);
                            if (anioX == anioI && mesX == (mesI - 1) && i == diaMES) {
                                diaX.add(cal);
                                CheckDia.add(aux);
                                if (Sucursales.isEmpty()) {
                                    Sucursales.add(aux.getIdCeco());
                                    List<CecoDTO> listaCeco = cecobi.buscaCeco(Integer.parseInt(aux.getIdCeco()));
                                    for (CecoDTO auxCC : listaCeco) {
                                        sucursalesVis = sucursalesVis + "\n" + aux.getIdCeco() + " - "
                                                + auxCC.getDescCeco();
                                    }
                                } else {
                                    boolean flag = false;
                                    for (String Suc : Sucursales) {
                                        if (aux.getIdCeco().trim().equals(Suc.trim())) {
                                            flag = true;
                                        }
                                    }
                                    if (flag == false) {
                                        Sucursales.add(aux.getIdCeco());
                                        List<CecoDTO> listaCeco = cecobi.buscaCeco(Integer.parseInt(aux.getIdCeco()));
                                        for (CecoDTO auxCC : listaCeco) {
                                            sucursalesVis = sucursalesVis + "\n" + aux.getIdCeco() + " - "
                                                    + auxCC.getDescCeco();
                                        }
                                    }

                                }

                            }

                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if (!diaX.isEmpty()) {
                    int ultE = CheckDia.size() - 1;
                    SimpleDateFormat formato = new SimpleDateFormat("HH:mm:ss", new Locale("es", "ES"));
                    String fechaIni = CheckDia.get(0).getFecha();
                    String fechaFin = CheckDia.get(ultE).getFecha();
                    // System.out.println("Dia: "+i);
                    // System.out.println(fechaIni.substring(11,fechaIni.length()));
                    // System.out.println(fechaFin.substring(11,fechaFin.length()));

                    AsistenciaDTO diaAsist = new AsistenciaDTO();
                    diaAsist.setDia(i);
                    diaAsist.setHoraEntrada(fechaIni.substring(11, fechaIni.length()));
                    diaAsist.setHoraSalita(fechaFin.substring(11, fechaFin.length()));
                    diaAsist.setSucursales(sucursalesVis);
                    Asistencia.add(diaAsist);
                } else {
                    // System.out.println("Dia: "+i);
                    // System.out.println("-");
                    // System.out.println("-");
                    AsistenciaDTO diaAsist = new AsistenciaDTO();
                    diaAsist.setDia(i);
                    diaAsist.setHoraEntrada("-");
                    diaAsist.setHoraSalita("-");

                    Asistencia.add(diaAsist);
                }
            }

            if (lista.size() > 0) {
                mv.addObject("datos", 1);
            } else {
                mv.addObject("datos", 0);
            }
            mv.addObject("listaUsu", lista);
            mv.addObject("listaProto", lista2);
            mv.addObject("asistencia", Asistencia);

            String[] strMonths = new String[]{"Enero", "Febebro", "Marzo", "Abril", "Mayo", "Junio", "Julio",
                "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
            List<String> meses = new ArrayList<String>();

            for (String mesS : strMonths) {
                meses.add(mesS);
            }

            mv.addObject("meses", meses);

            List<String> anios = new ArrayList<String>();
            anios.add("2019");
            anios.add("2020");
            anios.add("2021");
            mv.addObject("anios", anios);
        } catch (Exception e) {
            e.getStackTrace();
            e.printStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/reporteAsistencia.htm",
        "central/reporteAsistencia.htm"}, method = RequestMethod.GET)
    public ModelAndView elemplo(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idUsuario", required = true, defaultValue = "") String idUsuario,
            @RequestParam(value = "idProtocolo", required = true, defaultValue = "") String idProtocolo,
            @RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco)
            throws ServletException, IOException {

        //logger.info("Entra a Reporte Ceco");
        String salida = "repAsistencia";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            DetalleProtoDTO detalle = new DetalleProtoDTO();
            detalle.setIdOrdeGrupo(323);

            List<DetalleProtoDTO> lista = detalleProtoBI.ObtieneDetalleProto(detalle);

            List<ConsultaDTO> lista2 = new ArrayList<ConsultaDTO>();
            lista2 = consultaBI.obtieneConsultaChecklist((323));

            mv.addObject("idUsuario", "331952");

            String[] strMonths = new String[]{"Enero", "Febebro", "Marzo", "Abril", "Mayo", "Junio", "Julio",
                "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
            List<String> meses = new ArrayList<String>();

            for (String mes : strMonths) {
                meses.add(mes);
            }

            mv.addObject("meses", meses);

            List<String> anios = new ArrayList<String>();
            anios.add("2019");
            anios.add("2020");
            anios.add("2021");
            mv.addObject("anios", anios);

            if (lista.size() > 0) {
                mv.addObject("datos", 1);
            } else {
                mv.addObject("datos", 0);
            }
            mv.addObject("listaUsu", lista);

        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"central/seleccionFiltroExpansionPrincipal.htm"}, method = RequestMethod.GET)
    public ModelAndView seleccionFiltroExpansion(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "busqueda", required = true, defaultValue = "0") Integer busqueda,
            @RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
            @RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,
            @RequestParam(value = "tipoRecorrido", required = true, defaultValue = "") String tipoRecorrido,
            @RequestParam(value = "estatusVisita", required = true, defaultValue = "") String estatusVisita,
            @RequestParam(value = "fechaInicio", required = true, defaultValue = "") String fechaIni,
            @RequestParam(value = "fechaFin", required = true, defaultValue = "") String fechaFin,
            @RequestParam(value = "fechaInicioParam", required = true, defaultValue = "") String fechaIniParam,
            @RequestParam(value = "fechaFinParam", required = true, defaultValue = "") String fechaFinParam)
            throws ServletException, IOException {

        ModelAndView mv = new ModelAndView("seleccionFiltroExpansion");

        return mv;
    }

    /*
	 * @RequestMapping(value={"tienda/checklistDesign.htm",
	 * "central/checklistDesign.htm"},method=RequestMethod.GET) public
	 * ModelAndView checkListDesign(HttpServletRequest
	 * requqest,HttpServletResponse response,Model model) throws
	 * ServletException, IOException{
	 *
	 *
	 * return new ModelAndView("checklistDesign");
	 *
	 * }
     */
    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest request, Exception ex) {
        ModelAndView mv = new ModelAndView("exception");
        return mv;
    }

    /**
     * *************************************************
     * Reportería Supervisión 2.0
     * ************************************************************
     */
    @RequestMapping(value = {"tienda/indexSupervision.htm",
        "central/indexSupervision.htm"}, method = RequestMethod.GET)
    public ModelAndView getindexSupervision(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        //logger.info("Entra a Index Supervision");
        String salida = "indexSupervision";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            List<ConsultaDistribucionDTO> lista = consultaDistribucionBI.ObtieneTerritorio();
            logger.info("Territorios: " + lista);
            ConsultaDistribucionDTO zonaDef = new ConsultaDistribucionDTO();
            zonaDef.setIdCeco("0");
            zonaDef.setNombreCeco("Selecciona la Zona");
            List<ConsultaDistribucionDTO> listaZonas = new ArrayList<>();
            listaZonas.add(zonaDef);
            logger.info("Zonas: " + listaZonas);

            ConsultaDistribucionDTO regionDef = new ConsultaDistribucionDTO();
            regionDef.setIdCeco("0");
            regionDef.setNombreCeco("Selecciona la Región");
            List<ConsultaDistribucionDTO> listaRegiones = new ArrayList<>();
            listaRegiones.add(regionDef);
            logger.info("Regiones: " + listaRegiones);

            ConsultaDistribucionDTO sucursalnDef = new ConsultaDistribucionDTO();
            sucursalnDef.setIdCeco("0");
            sucursalnDef.setNombreCeco("Selecciona la Sucursal");
            List<ConsultaDistribucionDTO> listaSucursales = new ArrayList<>();
            listaSucursales.add(sucursalnDef);
            logger.info("Sucursales: " + listaSucursales);

            List<ConsultaDTO> listaProtocolos = new ArrayList<>();
            List<ConsultaDTO> lista2 = new ArrayList<>();
            ConsultaDTO consultaDTO = new ConsultaDTO();
            consultaDTO.setIdChecklist(0);
            consultaDTO.setNomChecklist("Selecciona el Protocolo");
            lista2 = consultaBI.obtieneConsultaChecklist(323);
            listaProtocolos.add(consultaDTO);

            for (int i = 0; i < lista2.size(); i++) {
                listaProtocolos.add(lista2.get(i));
            }
            // listaProtocolos=consultaBI.obtieneConsultaChecklist(323);
            logger.info("Protocolos: " + listaProtocolos);

            mv.addObject("lista", lista);
            mv.addObject("listaZonas", listaZonas);
            mv.addObject("listaRegiones", listaRegiones);
            mv.addObject("listaSucursales", listaSucursales);
            mv.addObject("listaProtocolos", listaProtocolos);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/indexSupervisionZona.htm",
        "central/indexSupervisionZona.htm"}, method = RequestMethod.POST)
    public ModelAndView postindexSupervisionZona(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "terri", required = false, defaultValue = "") String idTerritorio,
            @RequestParam(value = "cal1", required = false, defaultValue = "") String calInicio,
            @RequestParam(value = "cal2", required = false, defaultValue = "") String calFin,
            @RequestParam(value = "pro", required = false, defaultValue = "") String proto)
            throws ServletException, IOException {

        logger.info("Entra a Index Supervision Zona " + idTerritorio);
        String salida = "indexSupervision";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            List<ConsultaDistribucionDTO> lista = consultaDistribucionBI.ObtieneTerritorio();

            ConsultaDistribucionDTO consultaTerritorio = new ConsultaDistribucionDTO();
            consultaTerritorio.setTerritorio(idTerritorio);
            List<ConsultaDistribucionDTO> listaZonas = consultaDistribucionBI.ObtieneNivelZona(consultaTerritorio);
            logger.info("Lista Zonas: " + listaZonas);

            if (listaZonas.size() == 0) {
                listaZonas.clear();
                ConsultaDistribucionDTO zonaDef = new ConsultaDistribucionDTO();
                zonaDef.setIdCeco("0");
                zonaDef.setNombreCeco("Sin zonas que mostrar");
                listaZonas.add(zonaDef);
            }

            ConsultaDistribucionDTO regionDef = new ConsultaDistribucionDTO();
            regionDef.setIdCeco("0");
            regionDef.setNombreCeco("Selecciona la Región");
            List<ConsultaDistribucionDTO> listaRegiones = new ArrayList<>();
            listaRegiones.add(regionDef);

            ConsultaDistribucionDTO sucursalnDef = new ConsultaDistribucionDTO();
            sucursalnDef.setIdCeco("0");
            sucursalnDef.setNombreCeco("Selecciona la Sucursal");
            List<ConsultaDistribucionDTO> listaSucursales = new ArrayList<>();
            listaSucursales.add(sucursalnDef);

            List<ConsultaDTO> listaProtocolos = new ArrayList<>();
            List<ConsultaDTO> lista2 = new ArrayList<>();
            ConsultaDTO consultaDTO = new ConsultaDTO();
            consultaDTO.setIdChecklist(0);
            consultaDTO.setNomChecklist("Selecciona el Protocolo");
            lista2 = consultaBI.obtieneConsultaChecklist(323);
            listaProtocolos.add(consultaDTO);

            for (int i = 0; i < lista2.size(); i++) {
                listaProtocolos.add(lista2.get(i));
            }
            logger.info("Protocolos: " + listaProtocolos);

            mv.addObject("lista", ordenaLista(lista, idTerritorio));
            mv.addObject("listaZonas", listaZonas);
            mv.addObject("listaRegiones", listaRegiones);
            mv.addObject("listaSucursales", listaSucursales);
            mv.addObject("listaProtocolos", ordenaListaProtocolos(listaProtocolos, proto));
            mv.addObject("terri", idTerritorio);
            mv.addObject("cal1", calInicio);
            mv.addObject("cal2", calFin);
            mv.addObject("protocolo", proto);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/indexSupervisionRegion.htm",
        "central/indexSupervisionRegion.htm"}, method = RequestMethod.POST)
    public ModelAndView postindexSupervisionRegion(HttpServletRequest request, HttpServletResponse response,
            Model model, @RequestParam(value = "idTerri", required = false, defaultValue = "") String idTerritorio,
            @RequestParam(value = "zona", required = false, defaultValue = "") String idZona,
            @RequestParam(value = "calen1", required = false, defaultValue = "") String calInicio,
            @RequestParam(value = "calen2", required = false, defaultValue = "") String calFin,
            @RequestParam(value = "proto", required = false, defaultValue = "") String proto)
            throws ServletException, IOException {

        logger.info("Entra a Index Supervision Regiones " + idTerritorio + ", " + idZona);
        String salida = "indexSupervision";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            List<ConsultaDistribucionDTO> lista = consultaDistribucionBI.ObtieneTerritorio();

            ConsultaDistribucionDTO consultaTerritorio = new ConsultaDistribucionDTO();
            consultaTerritorio.setTerritorio(idTerritorio);
            List<ConsultaDistribucionDTO> listaZonas = consultaDistribucionBI.ObtieneNivelZona(consultaTerritorio);

            ConsultaDistribucionDTO regionDef = new ConsultaDistribucionDTO();
            regionDef.setTerritorio(idTerritorio);
            regionDef.setZona(idZona);
            List<ConsultaDistribucionDTO> listaRegiones = consultaDistribucionBI.ObtieneNivelRegion(regionDef);
            logger.info("Regiones 3: " + listaRegiones);

            List<ConsultaDistribucionDTO> listaSucursales = new ArrayList<>();

            if (listaRegiones.size() == 0) {
                listaRegiones.clear();
                ConsultaDistribucionDTO aux = new ConsultaDistribucionDTO();
                aux.setIdCeco("0");
                aux.setNombreCeco("Sin regiones que mostrar");
                listaRegiones.add(aux);

                ConsultaDistribucionDTO auxSuc = new ConsultaDistribucionDTO();
                auxSuc.setIdCeco("0");
                auxSuc.setNombreCeco("Sin sucursales que mostrar");
                listaSucursales.add(auxSuc);
            } else {
                ConsultaDistribucionDTO sucursalnDef = new ConsultaDistribucionDTO();
                sucursalnDef.setIdCeco("0");
                sucursalnDef.setNombreCeco("Selecciona la Sucursal");
                listaSucursales.add(sucursalnDef);
            }

            List<ConsultaDTO> listaProtocolos = new ArrayList<>();
            List<ConsultaDTO> lista2 = new ArrayList<>();
            ConsultaDTO consultaDTO = new ConsultaDTO();
            consultaDTO.setIdChecklist(0);
            consultaDTO.setNomChecklist("Selecciona el Protocolo");
            lista2 = consultaBI.obtieneConsultaChecklist(323);
            listaProtocolos.add(consultaDTO);

            for (int i = 0; i < lista2.size(); i++) {
                listaProtocolos.add(lista2.get(i));
            }
            logger.info("Protocolos: " + listaProtocolos);

            mv.addObject("lista", ordenaLista(lista, idTerritorio));
            mv.addObject("listaZonas", ordenaLista(listaZonas, idZona));
            mv.addObject("listaRegiones", listaRegiones);
            mv.addObject("listaSucursales", listaSucursales);
            mv.addObject("listaProtocolos", ordenaListaProtocolos(listaProtocolos, proto));
            mv.addObject("terri", idTerritorio);
            mv.addObject("zona", idZona);
            mv.addObject("cal1", calInicio);
            mv.addObject("cal2", calFin);
            mv.addObject("protocolo", proto);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/indexSupervisionSucursal.htm",
        "central/indexSupervisionSucursal.htm"}, method = RequestMethod.POST)
    public ModelAndView postindexSupervisionSucursal(HttpServletRequest request, HttpServletResponse response,
            Model model, @RequestParam(value = "idTerritorio", required = false, defaultValue = "") String idTerritorio,
            @RequestParam(value = "idZona", required = false, defaultValue = "") String idZona,
            @RequestParam(value = "region", required = false, defaultValue = "") String idRegion,
            @RequestParam(value = "calendario1", required = false, defaultValue = "") String calInicio,
            @RequestParam(value = "calendario2", required = false, defaultValue = "") String calFin,
            @RequestParam(value = "protoco", required = false, defaultValue = "") String proto)
            throws ServletException, IOException {

        logger.info("Entra a Index Supervision Sucursal " + idTerritorio + ", " + idZona + ", " + idRegion);
        String salida = "indexSupervision";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            List<ConsultaDistribucionDTO> lista = consultaDistribucionBI.ObtieneTerritorio();

            ConsultaDistribucionDTO consultaTerritorio = new ConsultaDistribucionDTO();
            consultaTerritorio.setTerritorio(idTerritorio);
            List<ConsultaDistribucionDTO> listaZonas = consultaDistribucionBI.ObtieneNivelZona(consultaTerritorio);

            ConsultaDistribucionDTO regionDef = new ConsultaDistribucionDTO();
            regionDef.setTerritorio(idTerritorio);
            regionDef.setZona(idZona);
            List<ConsultaDistribucionDTO> listaRegiones = consultaDistribucionBI.ObtieneNivelRegion(regionDef);

            if (listaRegiones.size() == 0) {
                listaRegiones.clear();
                ConsultaDistribucionDTO aux = new ConsultaDistribucionDTO();
                aux.setIdCeco("0");
                aux.setNombreCeco("Sin regiones que mostrar");
                listaRegiones.add(aux);
            }

            ConsultaDistribucionDTO sucursalDef = new ConsultaDistribucionDTO();
            sucursalDef.setTerritorio(idTerritorio);
            sucursalDef.setZona(idZona);
            sucursalDef.setRegion(idRegion);
            List<ConsultaDistribucionDTO> listaSucursales = consultaDistribucionBI.ObtieneNivelGerente(sucursalDef);
            logger.info("Sucursales 4: " + listaSucursales.size());

            if (listaSucursales.size() == 0) {
                listaSucursales.clear();
                ConsultaDistribucionDTO aux = new ConsultaDistribucionDTO();
                aux.setIdCeco("0");
                aux.setNombreCeco("Sin sucursales que mostrar");
                listaSucursales.add(aux);
            }

            List<ConsultaDTO> listaProtocolos = new ArrayList<>();
            List<ConsultaDTO> lista2 = new ArrayList<>();
            ConsultaDTO consultaDTO = new ConsultaDTO();
            consultaDTO.setIdChecklist(0);
            consultaDTO.setNomChecklist("Selecciona el Protocolo");
            lista2 = consultaBI.obtieneConsultaChecklist(323);
            listaProtocolos.add(consultaDTO);

            for (int i = 0; i < lista2.size(); i++) {
                listaProtocolos.add(lista2.get(i));
            }
            logger.info("Protocolos: " + listaProtocolos);

            mv.addObject("lista", ordenaLista(lista, idTerritorio));
            mv.addObject("listaZonas", ordenaLista(listaZonas, idZona));
            mv.addObject("listaRegiones", ordenaLista(listaRegiones, idRegion));
            mv.addObject("listaSucursales", listaSucursales);
            mv.addObject("listaProtocolos", ordenaListaProtocolos(listaProtocolos, proto));
            mv.addObject("terri", idTerritorio);
            mv.addObject("zona", idZona);
            mv.addObject("region", idRegion);
            mv.addObject("cal1", calInicio);
            mv.addObject("cal2", calFin);
            mv.addObject("protocolo", proto);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/indexSupervisionProtocolo.htm",
        "central/indexSupervisionProtocolo.htm"}, method = RequestMethod.POST)
    public ModelAndView postindexSupervisionProtocolo(HttpServletRequest request, HttpServletResponse response,
            Model model,
            @RequestParam(value = "idTerritorios", required = false, defaultValue = "") String idTerritorios,
            @RequestParam(value = "idZonas", required = false, defaultValue = "") String idZonas,
            @RequestParam(value = "idRegiones", required = false, defaultValue = "") String idRegiones,
            @RequestParam(value = "sucursal", required = false, defaultValue = "") String idSucursales,
            @RequestParam(value = "calIni", required = false, defaultValue = "") String calInicio,
            @RequestParam(value = "calFin", required = false, defaultValue = "") String calFin,
            @RequestParam(value = "protocolo", required = false, defaultValue = "") String proto)
            throws ServletException, IOException {

        logger.info("Entra a Index Supervision Protocolo " + idTerritorios + ", " + idZonas + ", " + idRegiones + ", "
                + idSucursales);
        String salida = "indexSupervision";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            List<ConsultaDistribucionDTO> lista = consultaDistribucionBI.ObtieneTerritorio();

            ConsultaDistribucionDTO consultaTerritorio = new ConsultaDistribucionDTO();
            consultaTerritorio.setTerritorio(idTerritorios);
            List<ConsultaDistribucionDTO> listaZonas = consultaDistribucionBI.ObtieneNivelZona(consultaTerritorio);

            ConsultaDistribucionDTO regionDef = new ConsultaDistribucionDTO();
            regionDef.setTerritorio(idTerritorios);
            regionDef.setZona(idZonas);
            List<ConsultaDistribucionDTO> listaRegiones = consultaDistribucionBI.ObtieneNivelRegion(regionDef);

            if (listaRegiones.size() == 0) {
                listaRegiones.clear();
                ConsultaDistribucionDTO aux = new ConsultaDistribucionDTO();
                aux.setIdCeco("0");
                aux.setNombreCeco("Sin regiones que mostrar");
                listaRegiones.add(aux);
            }

            ConsultaDistribucionDTO sucursalDef = new ConsultaDistribucionDTO();
            sucursalDef.setTerritorio(idTerritorios);
            sucursalDef.setZona(idZonas);
            sucursalDef.setRegion(idRegiones);
            sucursalDef.setGerente(idSucursales);
            List<ConsultaDistribucionDTO> listaSucursales = consultaDistribucionBI.ObtieneNivelGerente(sucursalDef);

            if (listaSucursales.size() == 0) {
                listaSucursales.clear();
                ConsultaDistribucionDTO aux = new ConsultaDistribucionDTO();
                aux.setIdCeco("0");
                aux.setNombreCeco("Sin sucursales que mostrar");
                listaSucursales.add(aux);
            }

            List<ConsultaDTO> listaProtocolos = new ArrayList<>();
            List<ConsultaDTO> lista2 = new ArrayList<>();
            ConsultaDTO consultaDTO = new ConsultaDTO();
            consultaDTO.setIdChecklist(0);
            consultaDTO.setNomChecklist("Selecciona el Protocolo");
            lista2 = consultaBI.obtieneConsultaChecklist(323);
            listaProtocolos.add(consultaDTO);

            for (int i = 0; i < lista2.size(); i++) {
                listaProtocolos.add(lista2.get(i));
            }
            logger.info("Protocolos 5: " + listaProtocolos);

            mv.addObject("lista", ordenaLista(lista, idTerritorios));
            mv.addObject("listaZonas", ordenaLista(listaZonas, idZonas));
            mv.addObject("listaRegiones", ordenaLista(listaRegiones, idRegiones));
            mv.addObject("listaSucursales", ordenaLista(listaSucursales, idSucursales));
            mv.addObject("listaProtocolos", ordenaListaProtocolos(listaProtocolos, proto));
            mv.addObject("terri", idTerritorios);
            mv.addObject("zona", idZonas);
            mv.addObject("region", idRegiones);
            mv.addObject("sucursal", idSucursales);
            mv.addObject("cal1", calInicio);
            mv.addObject("cal2", calFin);
            mv.addObject("protocolo", proto);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/indexSupervisionSeleccionados.htm",
        "central/indexSupervisionSeleccionados.htm"}, method = RequestMethod.POST)
    public ModelAndView postindexSupervisionSeleccionados(HttpServletRequest request, HttpServletResponse response,
            Model model,
            @RequestParam(value = "TerritorioSel", required = false, defaultValue = "") String TerritorioSel,
            @RequestParam(value = "ZonaSel", required = false, defaultValue = "") String ZonaSel,
            @RequestParam(value = "RegionSel", required = false, defaultValue = "") String RegionSel,
            @RequestParam(value = "SucursalSel", required = false, defaultValue = "") String SucursalSel,
            @RequestParam(value = "ProtocoloSel", required = false, defaultValue = "") String ProtocoloSel,
            @RequestParam(value = "calendarioIni", required = false, defaultValue = "") String calInicio,
            @RequestParam(value = "calendarioFin", required = false, defaultValue = "") String calFin)
            throws ServletException, IOException {

        logger.info("Entra a Index Supervision Seleccionados " + TerritorioSel + ", " + ZonaSel + ", " + RegionSel
                + ", " + SucursalSel + ", " + ProtocoloSel);
        String salida = "indexSupervision";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            List<ConsultaDistribucionDTO> lista = consultaDistribucionBI.ObtieneTerritorio();

            ConsultaDistribucionDTO consultaTerritorio = new ConsultaDistribucionDTO();
            consultaTerritorio.setTerritorio(TerritorioSel);
            List<ConsultaDistribucionDTO> listaZonas = consultaDistribucionBI.ObtieneNivelZona(consultaTerritorio);

            ConsultaDistribucionDTO regionDef = new ConsultaDistribucionDTO();
            regionDef.setTerritorio(TerritorioSel);
            regionDef.setZona(ZonaSel);
            List<ConsultaDistribucionDTO> listaRegiones = consultaDistribucionBI.ObtieneNivelRegion(regionDef);

            if (listaRegiones.size() == 0) {
                listaRegiones.clear();
                ConsultaDistribucionDTO aux = new ConsultaDistribucionDTO();
                aux.setIdCeco("0");
                aux.setNombreCeco("Sin regiones que mostrar");
                listaRegiones.add(aux);
            }

            ConsultaDistribucionDTO sucursalDef = new ConsultaDistribucionDTO();
            sucursalDef.setTerritorio(TerritorioSel);
            sucursalDef.setZona(ZonaSel);
            sucursalDef.setRegion(RegionSel);
            sucursalDef.setGerente(SucursalSel);
            List<ConsultaDistribucionDTO> listaSucursales = consultaDistribucionBI.ObtieneNivelGerente(sucursalDef);

            if (listaSucursales.size() == 0) {
                listaSucursales.clear();
                ConsultaDistribucionDTO aux = new ConsultaDistribucionDTO();
                aux.setIdCeco("0");
                aux.setNombreCeco("Sin sucursales que mostrar");
                listaSucursales.add(aux);
            }

            List<ConsultaDTO> listaProtocolos = new ArrayList<>();
            List<ConsultaDTO> lista2 = new ArrayList<>();
            ConsultaDTO consultaDTO = new ConsultaDTO();
            consultaDTO.setIdChecklist(0);
            consultaDTO.setNomChecklist("Selecciona el Protocolo");
            lista2 = consultaBI.obtieneConsultaChecklist(323);
            listaProtocolos.add(consultaDTO);

            for (int i = 0; i < lista2.size(); i++) {
                listaProtocolos.add(lista2.get(i));
            }

            logger.info("Datos Seleccionados: " + TerritorioSel + ", " + ZonaSel + ", " + RegionSel + ", " + SucursalSel
                    + ", " + ProtocoloSel + "/");

            mv.addObject("lista", ordenaLista(lista, TerritorioSel));
            mv.addObject("listaZonas", ordenaLista(listaZonas, ZonaSel));
            mv.addObject("listaRegiones", ordenaLista(listaRegiones, RegionSel));
            mv.addObject("listaSucursales", ordenaLista(listaSucursales, SucursalSel));
            mv.addObject("listaProtocolos", ordenaListaProtocolos(listaProtocolos, ProtocoloSel));
            mv.addObject("terri", TerritorioSel);
            mv.addObject("zona", ZonaSel);
            mv.addObject("region", RegionSel);
            mv.addObject("sucursal", SucursalSel);
            mv.addObject("protocolo", ProtocoloSel);
            mv.addObject("cal1", calInicio);
            mv.addObject("cal2", calFin);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/indexSupervision.htm",
        "central/indexSupervision.htm"}, method = RequestMethod.POST)
    public ModelAndView postindexSupervision(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "TerritorioSel", required = false, defaultValue = "") String TerritorioSel,
            @RequestParam(value = "ZonaSel", required = false, defaultValue = "") String ZonaSel,
            @RequestParam(value = "RegionSel", required = false, defaultValue = "") String RegionSel,
            @RequestParam(value = "fechaInicio", required = false, defaultValue = "") String fechaDeInicio,
            @RequestParam(value = "fechaFin", required = false, defaultValue = "") String fechaDeFin,
            @RequestParam(value = "idCeco", required = false, defaultValue = "") String idCeco,
            @RequestParam(value = "idChecklist", required = false, defaultValue = "") String idChecklist)
            throws ServletException, IOException {

        logger.info(
                "Entra a Index Supervision " + fechaDeInicio + ", " + fechaDeFin + ", " + idCeco + ", " + idChecklist);
        String salida = "indexSupervision";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            List<ConsultaDistribucionDTO> listaTerritorios = consultaDistribucionBI.ObtieneTerritorio();

            ConsultaDistribucionDTO consultaTerritorio = new ConsultaDistribucionDTO();
            consultaTerritorio.setTerritorio(TerritorioSel);
            List<ConsultaDistribucionDTO> listaZonas = consultaDistribucionBI.ObtieneNivelZona(consultaTerritorio);

            ConsultaDistribucionDTO regionDef = new ConsultaDistribucionDTO();
            regionDef.setTerritorio(TerritorioSel);
            regionDef.setZona(ZonaSel);
            List<ConsultaDistribucionDTO> listaRegiones = consultaDistribucionBI.ObtieneNivelRegion(regionDef);

            if (listaRegiones.size() == 0) {
                listaRegiones.clear();
                ConsultaDistribucionDTO aux = new ConsultaDistribucionDTO();
                aux.setIdCeco("0");
                aux.setNombreCeco("Sin regiones que mostrar");
                listaRegiones.add(aux);
            }

            ConsultaDistribucionDTO sucursalDef = new ConsultaDistribucionDTO();
            sucursalDef.setTerritorio(TerritorioSel);
            sucursalDef.setZona(ZonaSel);
            sucursalDef.setRegion(RegionSel);
            sucursalDef.setGerente(idCeco);
            List<ConsultaDistribucionDTO> listaSucursales = consultaDistribucionBI.ObtieneNivelGerente(sucursalDef);

            if (listaSucursales.size() == 0) {
                listaSucursales.clear();
                ConsultaDistribucionDTO aux = new ConsultaDistribucionDTO();
                aux.setIdCeco("0");
                aux.setNombreCeco("Sin sucursales que mostrar");
                listaSucursales.add(aux);
            }

            List<ConsultaDTO> listaProtocolos = new ArrayList<>();
            List<ConsultaDTO> lista2 = new ArrayList<>();
            ConsultaDTO consultaDTO = new ConsultaDTO();
            consultaDTO.setIdChecklist(0);
            consultaDTO.setNomChecklist("Selecciona el Protocolo");
            lista2 = consultaBI.obtieneConsultaChecklist(323);
            listaProtocolos.add(consultaDTO);

            for (int i = 0; i < lista2.size(); i++) {
                listaProtocolos.add(lista2.get(i));
            }

            ConsultaSupervisionDTO consulta = new ConsultaSupervisionDTO();
            consulta.setIdSucursal(Integer.parseInt(idCeco));
            consulta.setFechaInicio(fechaDeInicio);
            consulta.setFechaFin(fechaDeFin);
            int idCheck = 0;
            String paso = null;
            if (!idChecklist.equals("0") && !idChecklist.equals("")) {
                idCheck = Integer.parseInt(idChecklist);
                paso = "1";
            } else {
                idCheck = 0;
                paso = "2";
            }
            consulta.setIdProtocolo(idCheck);

            List<ConsultaSupervisionDTO> lista = consultaSupervisionBI.buscaSucursal(consulta);
            logger.info("Lista Tabla: " + lista);
            if (lista.size() == 0) {
                paso = "3";
            }

            logger.info("Paso: " + paso);

            mv.addObject("listaTabla", lista);
            mv.addObject("lista", ordenaLista(listaTerritorios, TerritorioSel));
            mv.addObject("listaZonas", ordenaLista(listaZonas, ZonaSel));
            mv.addObject("listaRegiones", ordenaLista(listaRegiones, RegionSel));
            mv.addObject("listaSucursales", ordenaLista(listaSucursales, idCeco));
            mv.addObject("listaProtocolos", ordenaListaProtocolos(listaProtocolos, idChecklist));

            mv.addObject("paso", paso);
            mv.addObject("terri", TerritorioSel);
            mv.addObject("zona", ZonaSel);
            mv.addObject("region", RegionSel);
            mv.addObject("sucursal", idCeco);
            mv.addObject("protocolo", idChecklist);
            mv.addObject("cal1", fechaDeInicio);
            mv.addObject("cal2", fechaDeFin);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/busquedaSucursalSeleccionados.htm",
        "central/busquedaSucursalSeleccionados.htm"}, method = RequestMethod.POST)
    public ModelAndView postbusquedaSucursalSeleccionados(HttpServletRequest request, HttpServletResponse response,
            Model model, @RequestParam(value = "nomSucursal", required = false, defaultValue = "") String nomSucursal,
            @RequestParam(value = "nomZona", required = false, defaultValue = "") String nomZona,
            @RequestParam(value = "nomRegion", required = false, defaultValue = "") String nomRegion,
            @RequestParam(value = "nomTerritorio", required = false, defaultValue = "") String nomTerritorio,
            @RequestParam(value = "ProtocoloSel", required = false, defaultValue = "") String idChecklist,
            @RequestParam(value = "calendarioIni", required = false, defaultValue = "") String calInicio,
            @RequestParam(value = "calendarioFin", required = false, defaultValue = "") String calFin,
            @RequestParam(value = "TerritorioSel", required = false, defaultValue = "") String idTerritorio,
            @RequestParam(value = "ZonaSel", required = false, defaultValue = "") String idZona,
            @RequestParam(value = "RegionSel", required = false, defaultValue = "") String idRegion,
            @RequestParam(value = "SucursalSel", required = false, defaultValue = "") String idSucursal)
            throws ServletException, IOException {

        logger.info("Entra a Busqueda Sucursal Seleccionados " + nomSucursal + ", " + nomZona + ", " + nomRegion + ", "
                + nomTerritorio + ", " + idChecklist);
        String salida = "busquedaSucursalSupervision";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            List<ConsultaDTO> listaProtocolos = new ArrayList<>();
            List<ConsultaDTO> lista2 = new ArrayList<>();
            ConsultaDTO consultaDTO = new ConsultaDTO();
            consultaDTO.setIdChecklist(0);
            consultaDTO.setNomChecklist("Selecciona el Protocolo");
            lista2 = consultaBI.obtieneConsultaChecklist(323);
            listaProtocolos.add(consultaDTO);

            for (int i = 0; i < lista2.size(); i++) {
                listaProtocolos.add(lista2.get(i));
            }

            ConsultaVisitasDTO a1 = new ConsultaVisitasDTO();
            a1.setIdCeco(idSucursal);
            a1.setProtocolo(idChecklist);

            List<ConsultaVisitasDTO> listaCalif = consultaVisitasBI.ObtienePromCalif(a1);

            ConsultaVisitasDTO b1 = new ConsultaVisitasDTO();
            b1.setIdCeco(idSucursal);
            b1.setProtocolo(idChecklist);

            List<ConsultaVisitasDTO> listaVis = consultaVisitasBI.obtieneVisitas(a1);

            int cont = 1;
            for (ConsultaVisitasDTO aux : listaCalif) {
                mv.addObject("mesCalif" + cont, aux.getPromedioCalif());
                cont++;
            }

            cont = 1;
            for (ConsultaVisitasDTO aux : listaVis) {
                mv.addObject("mesVis" + cont, aux.getFiconteo());
                cont++;
            }

            mv.addObject("listaProtocolos", ordenaListaProtocolos(listaProtocolos, idChecklist));
            mv.addObject("cal1", calInicio);
            mv.addObject("cal2", calFin);
            mv.addObject("nomSucursal", nomSucursal);
            mv.addObject("nomZona", nomZona);
            mv.addObject("nomRegion", nomRegion);
            mv.addObject("nomTerritorio", nomTerritorio);
            mv.addObject("idTerritorio", idTerritorio);
            mv.addObject("idZona", idZona);
            mv.addObject("idRegion", idRegion);
            mv.addObject("idSucursal", idSucursal);
            mv.addObject("idProtocolo", idChecklist);

        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/busquedaSucursal.htm",
        "central/busquedaSucursal.htm"}, method = RequestMethod.POST)
    public ModelAndView postbusquedaSucursal(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idSucursal", required = false, defaultValue = "") String idSucursal,
            @RequestParam(value = "nomSucursal", required = false, defaultValue = "") String nomSucursal,
            @RequestParam(value = "nomZona", required = false, defaultValue = "") String nomZona,
            @RequestParam(value = "nomRegion", required = false, defaultValue = "") String nomRegion,
            @RequestParam(value = "nomTerritorio", required = false, defaultValue = "") String nomTerritorio,
            @RequestParam(value = "idProtocolo", required = false, defaultValue = "") String idChecklist,
            @RequestParam(value = "cal1", required = false, defaultValue = "") String calInicio,
            @RequestParam(value = "cal2", required = false, defaultValue = "") String calFin,
            @RequestParam(value = "idTerritorio", required = false, defaultValue = "") String idTerritorio,
            @RequestParam(value = "idZona", required = false, defaultValue = "") String idZona,
            @RequestParam(value = "idRegion", required = false, defaultValue = "") String idRegion)
            throws ServletException, IOException {

        logger.info("Entra a Busqueda Sucursal " + idSucursal + ", " + nomSucursal + ", " + nomZona + ", " + nomRegion
                + ", " + nomTerritorio + ", " + idChecklist);
        String salida = "busquedaSucursalSupervision";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            ConsultaVisitasDTO a1 = new ConsultaVisitasDTO();
            a1.setIdCeco(idSucursal);
            a1.setProtocolo(idChecklist);

            List<ConsultaVisitasDTO> listaCalif = consultaVisitasBI.ObtienePromCalif(a1);

            ConsultaVisitasDTO b1 = new ConsultaVisitasDTO();
            b1.setIdCeco(idSucursal);
            b1.setProtocolo(idChecklist);

            List<ConsultaVisitasDTO> listaVis = consultaVisitasBI.obtieneVisitas(b1);

            List<ConsultaDTO> listaProtocolos = new ArrayList<>();
            List<ConsultaDTO> lista2 = new ArrayList<>();
            ConsultaDTO consultaDTO = new ConsultaDTO();
            consultaDTO.setIdChecklist(0);
            consultaDTO.setNomChecklist("Selecciona el Protocolo");
            lista2 = consultaBI.obtieneConsultaChecklist(323);
            listaProtocolos.add(consultaDTO);

            for (int i = 0; i < lista2.size(); i++) {
                listaProtocolos.add(lista2.get(i));
            }

            int cont = 1;
            for (ConsultaVisitasDTO aux : listaCalif) {
                mv.addObject("mesCalif" + cont, aux.getPromedioCalif());
                cont++;
            }

            cont = 1;
            for (ConsultaVisitasDTO aux : listaVis) {
                mv.addObject("mesVis" + cont, aux.getFiconteo());
                cont++;
            }

            mv.addObject("listaProtocolos", listaProtocolos);
            mv.addObject("nomSucursal", nomSucursal);
            mv.addObject("nomZona", nomZona);
            mv.addObject("nomRegion", nomRegion);
            mv.addObject("nomTerritorio", nomTerritorio);
            mv.addObject("idProtocolo", idChecklist);
            mv.addObject("fechaInicio", calInicio);
            mv.addObject("fechaFin", calFin);
            mv.addObject("idTerritorio", idTerritorio);
            mv.addObject("idZona", idZona);
            mv.addObject("idRegion", idRegion);
            mv.addObject("idSucursal", idSucursal);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/detalleProtocoloSupervision.htm",
        "central/detalleProtocoloSupervision.htm"}, method = RequestMethod.POST)
    public ModelAndView detalleProtocolo(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idBitacora", required = false, defaultValue = "") String idBitacora,
            @RequestParam(value = "sucursalEnv", required = false, defaultValue = "") String idSucursal,
            @RequestParam(value = "zonaEnv", required = false, defaultValue = "") String idZona,
            @RequestParam(value = "regionEnv", required = false, defaultValue = "") String idRegion,
            @RequestParam(value = "terrEnv", required = false, defaultValue = "") String idTerritorio,
            @RequestParam(value = "proEnv", required = false, defaultValue = "") String nomChecklist,
            @RequestParam(value = "idProEnv", required = false, defaultValue = "") String idChecklist)
            throws ServletException, IOException {

        logger.info("Entra a Reporte Detalle Protocolo " + idBitacora + ", " + idSucursal + ", " + idZona + ", "
                + idRegion + ", " + idTerritorio + ", " + nomChecklist + ", " + idChecklist);
        String salida = "detalleProtocoloSupervision";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneDatosGen(idBitacora);
            if (lista.size() > 0) {
                mv.addObject("datos", 1);
            } else {
                mv.addObject("datos", 0);
            }

            List<ModuloDTO> listaMod = new ArrayList<ModuloDTO>();
            HashMap<String, List<ChecklistPreguntasComDTO>> listaMods = new HashMap<String, List<ChecklistPreguntasComDTO>>();
            int cont = 0;
            for (ChecklistPreguntasComDTO aux : lista) {
                String modulo = aux.getModulo();
                if (listaMods.containsKey(modulo)) {
                    listaMods.get(modulo).add(aux);
                    cont++;
                } else {
                    listaMods.put(modulo, new ArrayList<ChecklistPreguntasComDTO>());
                    listaMods.get(modulo).add(aux);
                    ModuloDTO modAux = new ModuloDTO();
                    modAux.setIdModulo(aux.getIdModulo());
                    modAux.setNombre(modulo);
                    listaMod.add(modAux);
                    cont++;
                }

            }

            List<ConsultaDistribucionDTO> listaTerritorios = consultaDistribucionBI.ObtieneTerritorio();
            String nomTerritorio = "";
            for (int i = 0; i < listaTerritorios.size(); i++) {
                if (listaTerritorios.get(i).getIdCeco().equals(idTerritorio)) {
                    nomTerritorio = listaTerritorios.get(i).getNombreCeco();
                }
            }

            ConsultaDistribucionDTO consultaTerritorio = new ConsultaDistribucionDTO();
            consultaTerritorio.setTerritorio(idTerritorio);
            List<ConsultaDistribucionDTO> listaZonas = consultaDistribucionBI.ObtieneNivelZona(consultaTerritorio);
            String nomZona = "";
            for (int i = 0; i < listaZonas.size(); i++) {
                if (listaZonas.get(i).getIdCeco().equals(idZona)) {
                    nomZona = listaZonas.get(i).getNombreCeco();
                }
            }

            ConsultaDistribucionDTO regionDef = new ConsultaDistribucionDTO();
            regionDef.setTerritorio(idTerritorio);
            regionDef.setZona(idZona);
            List<ConsultaDistribucionDTO> listaRegiones = consultaDistribucionBI.ObtieneNivelRegion(regionDef);
            String nomRegion = "";
            for (int i = 0; i < listaRegiones.size(); i++) {
                if (listaRegiones.get(i).getIdCeco().equals(idRegion)) {
                    nomRegion = listaRegiones.get(i).getNombreCeco();
                }
            }

            if (listaRegiones.size() == 0) {
                listaRegiones.clear();
                ConsultaDistribucionDTO aux = new ConsultaDistribucionDTO();
                aux.setIdCeco("0");
                aux.setNombreCeco("Sin regiones que mostrar");
                listaRegiones.add(aux);
                nomRegion = "Sin Región";
            }

            ConsultaDistribucionDTO sucursalDef = new ConsultaDistribucionDTO();
            sucursalDef.setTerritorio(idTerritorio);
            sucursalDef.setZona(idZona);
            sucursalDef.setRegion(idRegion);
            sucursalDef.setGerente(idSucursal);
            List<ConsultaDistribucionDTO> listaSucursales = consultaDistribucionBI.ObtieneNivelGerente(sucursalDef);
            String nomSucursal = "";
            for (int i = 0; i < listaSucursales.size(); i++) {
                if (listaSucursales.get(i).getIdCeco().equals(idSucursal)) {
                    nomSucursal = listaSucursales.get(i).getNombreCeco();
                }
            }

            if (listaSucursales.size() == 0) {
                listaSucursales.clear();
                ConsultaDistribucionDTO aux = new ConsultaDistribucionDTO();
                aux.setIdCeco("0");
                aux.setNombreCeco("Sin sucursales que mostrar");
                listaSucursales.add(aux);
                nomSucursal = "Sin Sucursal";
            }

            mv.addObject("modulos", listaMod);
            mv.addObject("lista", lista);
            mv.addObject("territorio", nomTerritorio);
            mv.addObject("zona", nomZona);
            mv.addObject("region", nomRegion);
            mv.addObject("sucursal", nomSucursal);
            mv.addObject("idSucursal", idSucursal);
            mv.addObject("nomProtocolo", nomChecklist);
            mv.addObject("idProtocolo", idChecklist);
            mv.addObject("idTerritorio", idTerritorio);
            mv.addObject("idZona", idZona);
            mv.addObject("idRegion", idRegion);

        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/busquedaSucursalDatos.htm",
        "central/busquedaSucursalDatos.htm"}, method = RequestMethod.POST)
    public ModelAndView postbusquedaSucursalDatos(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "nomSucursal", required = false, defaultValue = "") String nomSucursal,
            @RequestParam(value = "nomZona", required = false, defaultValue = "") String nomZona,
            @RequestParam(value = "nomRegion", required = false, defaultValue = "") String nomRegion,
            @RequestParam(value = "nomTerritorio", required = false, defaultValue = "") String nomTerritorio,
            @RequestParam(value = "idProtocolo", required = false, defaultValue = "") String idChecklist,
            @RequestParam(value = "fechaInicio", required = false, defaultValue = "") String calInicio,
            @RequestParam(value = "fechaFin", required = false, defaultValue = "") String calFin,
            @RequestParam(value = "idTerritorio", required = false, defaultValue = "") String idTerritorio,
            @RequestParam(value = "idZona", required = false, defaultValue = "") String idZona,
            @RequestParam(value = "idRegion", required = false, defaultValue = "") String idRegion,
            @RequestParam(value = "idSucursal", required = false, defaultValue = "") String idSucursal)
            throws ServletException, IOException {

        logger.info("Entra a Busqueda Sucursal Datos" + nomSucursal + ", " + nomZona + ", " + nomRegion + ", "
                + nomTerritorio + ", " + idChecklist + ", " + idSucursal);
        String salida = "busquedaSucursalSupervision";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {

            ConsultaVisitasDTO a1 = new ConsultaVisitasDTO();
            a1.setIdCeco(idSucursal);
            a1.setProtocolo(idChecklist);

            List<ConsultaVisitasDTO> listaCalif = consultaVisitasBI.ObtienePromCalif(a1);

            ConsultaVisitasDTO b1 = new ConsultaVisitasDTO();
            b1.setIdCeco(idSucursal);
            b1.setProtocolo(idChecklist);

            List<ConsultaVisitasDTO> listaVis = consultaVisitasBI.obtieneVisitas(b1);

            int cont = 1;
            for (ConsultaVisitasDTO aux : listaCalif) {
                mv.addObject("mesCalif" + cont, aux.getPromedioCalif());
                cont++;
            }

            cont = 1;
            for (ConsultaVisitasDTO aux : listaVis) {
                mv.addObject("mesVis" + cont, aux.getFiconteo());
                cont++;
            }

            List<ConsultaDTO> listaProtocolos = new ArrayList<>();
            List<ConsultaDTO> lista2 = new ArrayList<>();
            ConsultaDTO consultaDTO = new ConsultaDTO();
            consultaDTO.setIdChecklist(0);
            consultaDTO.setNomChecklist("Selecciona el Protocolo");
            lista2 = consultaBI.obtieneConsultaChecklist(323);
            listaProtocolos.add(consultaDTO);

            for (int i = 0; i < lista2.size(); i++) {
                listaProtocolos.add(lista2.get(i));
            }

            ConsultaSupervisionDTO consulta = new ConsultaSupervisionDTO();
            consulta.setIdSucursal(Integer.parseInt(idSucursal));
            consulta.setFechaInicio(calInicio);
            consulta.setFechaFin(calFin);
            int idCheck = 0;
            String paso = null;
            if (!idChecklist.equals("0") && !idChecklist.equals("")) {
                idCheck = Integer.parseInt(idChecklist);
                paso = "1";
            } else {
                idCheck = 0;
                paso = "2";
            }
            consulta.setIdProtocolo(idCheck);

            List<ConsultaSupervisionDTO> lista = consultaSupervisionBI.buscaSucursal(consulta);
            logger.info("listaDatos: " + lista);
            if (lista.size() == 0) {
                paso = "3";
            }

            logger.info("Paso: " + paso);

            mv.addObject("listaDatos", lista);
            mv.addObject("listaProtocolos", ordenaListaProtocolos(listaProtocolos, idChecklist));
            mv.addObject("cal1", calInicio);
            mv.addObject("cal2", calFin);
            mv.addObject("nomSucursal", nomSucursal);
            mv.addObject("nomZona", nomZona);
            mv.addObject("nomRegion", nomRegion);
            mv.addObject("nomTerritorio", nomTerritorio);
            mv.addObject("idTerritorio", idTerritorio);
            mv.addObject("idZona", idZona);
            mv.addObject("idRegion", idRegion);
            mv.addObject("idSucursal", idSucursal);
            mv.addObject("idProtocolo", idChecklist);
            mv.addObject("paso", paso);

        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/busquedaSupervisorSupervision.htm",
        "central/busquedaSupervisorSupervision.htm"}, method = RequestMethod.GET)
    public ModelAndView getbusquedaSupervisor(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        //logger.info("Entra a Busqueda Supervisor");
        String salida = "busquedaSupervisorSupervision";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            mv.addObject("paso", new String("0"));
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"central/busquedaSupervisorSupervision.htm"}, method = RequestMethod.POST)
    public ModelAndView postbusquedaSupervisor(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "numEmpleado", required = false, defaultValue = "") int idUsuario)
            throws ServletException, IOException {

        logger.info("Entra a Busqueda Supervisor " + idUsuario);
        String salida = "busquedaSupervisorSupervision";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            String paso = "1";
            SupervisorDTO supervisorDTO = new SupervisorDTO();
            supervisorDTO.setIdUsuario(idUsuario);
            List<SupervisorDTO> lista = new ArrayList<SupervisorDTO>();
            lista = supervisorBI.buscaSupervisor(supervisorDTO);

            if (lista.toString().contains("NO SE ENCONTRARON RESULTADOS PARA ESTE EMPLEADO")) {
                paso = "2";
            }

            logger.info("LISTA: " + lista.toString());
            mv.addObject("lista", lista);
            mv.addObject("idUsuario", idUsuario);
            mv.addObject("paso", paso);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/dashboardSupervision.htm",
        "central/dashboardSupervision.htm"}, method = RequestMethod.POST)
    public ModelAndView dashboardSupervision(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idUser", required = true, defaultValue = "") String idUsuario)
            throws ServletException, IOException {

        //logger.info("Entra a Dashboard");
        String salida = "dashboardSupervision";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        // String idUsuario="73433";
        // String idUsuario=request.getParameter("idUsuario");
        try {
            ConsultaDashBoardDTO a1 = new ConsultaDashBoardDTO();
            a1.setIdUsuario(idUsuario);

            List<ConsultaDashBoardDTO> lista = consultaDashBoardBI.obtieneDashBoard(a1);

            ConsultaAsistenciaDTO a2 = new ConsultaAsistenciaDTO();
            a2.setIdUsuario(idUsuario);

            List<ConsultaAsistenciaDTO> lista3 = consultaAsistenciaBI.obtieneAsistencia(a2);

            if (lista.size() > 0) {
                mv.addObject("datos", 1);
                for (ConsultaDashBoardDTO aux : lista) {
                    mv.addObject("idUsuario", aux.getIdUsuario());
                    mv.addObject("fecha", aux.getFecha());
                    mv.addObject("nombre", aux.getNomCeco());
                    mv.addObject("visitadas", aux.getSucVisitadas());
                    mv.addObject("visitar", aux.getSucxVisitar());
                    mv.addObject("sucAsignada", aux.getSucAsignada());
                    mv.addObject("sucVisitada", aux.getSucVisitada());
                    int sucAsignada = 0;
                    int sucVisitada = 0;
                    if (aux.getSucVisitada() != null) {
                        sucVisitada = Integer.parseInt(aux.getSucVisitada());
                    }
                    if (aux.getSucAsignada() != null) {
                        sucAsignada = Integer.parseInt(aux.getSucAsignada());
                    }
                    mv.addObject("sucFaltantes", sucAsignada - sucVisitada);
                }

            } else {
                mv.addObject("datos", 0);
            }

            if (lista3.size() > 0) {
                mv.addObject("datos", 1);
                for (ConsultaAsistenciaDTO aux : lista3) {
                    mv.addObject("mes" + aux.getFimes().trim(), aux.getPorcentaje());
                }

            } else {
                mv.addObject("datos", 0);
            }

            // mv.addObject("lista", lista);
            // mv.addObject("lista3", lista3);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/asistenciaSupervision.htm",
        "central/asistenciaSupervision.htm"}, method = RequestMethod.POST)
    public ModelAndView asistenciaSupervision(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idUsuario", required = true, defaultValue = "") String idUsuario,
            @RequestParam(value = "fechaInicio", required = true, defaultValue = "") String fechaInicio,
            @RequestParam(value = "fechaFin", required = true, defaultValue = "") String fechaFin)
            throws ServletException, IOException {

        //logger.info("Entra a Asistencia");
        String salida = "asistenciaSupervision";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            AsistenciaSupervisorDTO asistencia = new AsistenciaSupervisorDTO();
            asistencia.setIdUsuario(Integer.parseInt(idUsuario));
            asistencia.setFechaInicio(fechaInicio);
            asistencia.setFechaFin(fechaFin);

            List<AsistenciaSupervisorDTO> lista = new ArrayList<AsistenciaSupervisorDTO>();
            lista = asistenciaSupervisorBI.buscaAsistencia(asistencia);
            logger.info("Asistencia: " + lista.toString());

            mv.addObject("lista", lista);
            mv.addObject("idUsuario", idUsuario);
            mv.addObject("fechaInicio", fechaInicio);
            mv.addObject("fechaFin", fechaFin);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/listaSucursales.htm", "central/listaSucursales.htm"}, method = RequestMethod.GET)
    public ModelAndView listaSucursales(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        //logger.info("Entra a Lista Cecos");
        String salida = "listaSucursales";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            List<ConsultaDTO> listaCecos = consultaBI.obtieneConsultaCeco(323);
            logger.info(listaCecos);

            for (int i = 0; i < listaCecos.size(); i++) {
                if (listaCecos.get(i).getIdCeco() == 996783) {
                    listaCecos.remove(i);
                }
            }

            mv.addObject("listaCecos", listaCecos);

        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/foliosMantenimiento.htm",
        "central/foliosMantenimiento.htm"}, method = RequestMethod.POST)
    public ModelAndView foliosMantenimiento(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idSucursal", required = false, defaultValue = "") int idSucursal,
            @RequestParam(value = "nomSucursal", required = false, defaultValue = "") String nomSucursal)
            throws ServletException, IOException {

        //logger.info("Entra a Folios Mantenimiento ");
        String salida = "foliosMantenimiento";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            String json = serviciosMiGestionBI.obtieneFolios(String.valueOf(idSucursal));
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                JSONArray aux = rec.getJSONArray("FOLIOS");

                List<FoliosMtoDTO> listaFolios = new ArrayList<FoliosMtoDTO>();
                for (int i = 0; i < aux.length(); i++) {
                    JSONObject jObject = aux.getJSONObject(i);
                    FoliosMtoDTO folios = new FoliosMtoDTO();
                    folios.setIdFolio(jObject.getString("ID_INCIDENTE"));
                    if (!jObject.getString("NO_SUCURSAL").equals("null")) {
                        folios.setIdSucursal(jObject.getString("NO_SUCURSAL"));
                    } else {
                        folios.setIdSucursal("Sin número de sucursal encontrado");
                    }
                    if (!jObject.getString("SUCURSAL").equals("null")) {
                        folios.setNomSucursal(jObject.getString("SUCURSAL"));
                    } else {
                        folios.setNomSucursal("Sin nombre de sucursal encontrado");
                    }
                    if (jObject.getString("PROVEEDOR").equals("null")) {
                        folios.setNomProveedor("Sin proveedor por asignar");
                    } else {
                        folios.setNomProveedor(jObject.getString("PROVEEDOR"));
                    }
                    if (!jObject.getString("INCIDENCIA").equals("null")) {
                        folios.setNomIncidente(jObject.getString("INCIDENCIA"));
                    } else {
                        folios.setNomIncidente("Sin incidente encontrado");
                    }
                    if (!jObject.getString("FECHA_PROGRAMADA").equals("null")) {
                        folios.setFechaProgramada(jObject.getString("FECHA_PROGRAMADA"));
                    } else {
                        folios.setFechaProgramada("Sin fecha programada encontrada");
                    }
                    if (!jObject.getString("ESTADO").equals("null")) {
                        folios.setEstado(jObject.getString("ESTADO"));
                    } else {
                        folios.setEstado("Sin estado encontrado");
                    }
                    if (!jObject.getString("MOTIVO_ESTADO").equals("null")) {
                        folios.setMotivoEstado(jObject.getString("MOTIVO_ESTADO"));
                    } else {
                        folios.setMotivoEstado("Sin motivo estado encontrado");
                    }
                    if (!jObject.getString("FECHA_MODIFICACION").equals("null")) {
                        folios.setFechaModificacion(jObject.getString("FECHA_MODIFICACION"));
                    } else {
                        folios.setFechaModificacion("Sin fecha de modificación encontrada");
                    }
                    if (!jObject.getString("FECHA_CREACION").equals("null")) {
                        folios.setFechaCreacion(jObject.getString("FECHA_CREACION"));
                    } else {
                        folios.setFechaCreacion("Sin fecha de creación encontrada");
                    }

                    listaFolios.add(folios);
                }

                logger.info("Lista: " + listaFolios.size());

                if (listaFolios.isEmpty()) {
                    mv.addObject("paso", "0");
                } else {
                    mv.addObject("paso", "1");
                }

                mv.addObject("listaFolios", listaFolios);
                mv.addObject("idSucursal", idSucursal);
                mv.addObject("nomCeco", nomSucursal);
            }

        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/getPerfilSuperGenerica.htm",
        "central/getPerfilSuperGenerica.htm"}, method = RequestMethod.GET)
    public ModelAndView getPerfilSuperGenerica(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        //logger.info("Entra a Busqueda Supervisor");
        String salida = "perfilSupervisorGenerica";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            mv.addObject("paso", new String("0"));
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"central/postPerfilSuperGenerica.htm"}, method = RequestMethod.POST)
    public ModelAndView postPerfilSuperGenerica(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "numEmpleado", required = false, defaultValue = "") String idUsuario,
            @RequestParam(value = "nomEmpleado", required = false, defaultValue = "") String nomUsuario,
            @RequestParam(value = "agrego", required = false, defaultValue = "") String agrego,
            @RequestParam(value = "numEmpleado2", required = false, defaultValue = "") String idUsuario2)
            throws ServletException, IOException {

        logger.info("Entra a Busqueda Supervisor " + idUsuario);
        String salida = "perfilSupervisorGenerica";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        try {
            if (agrego.equals("")) {
                String paso = "1";
                SupervisorDTO supervisorDTO = new SupervisorDTO();
                supervisorDTO.setIdUsuario(Integer.parseInt(idUsuario));
                List<SupervisorDTO> lista = new ArrayList<SupervisorDTO>();
                lista = supervisorBI.buscaSupervisor(supervisorDTO);

                if (lista.toString().contains("NO SE ENCONTRARON RESULTADOS PARA ESTE EMPLEADO")) {
                    paso = "2";
                }

                logger.info("LISTA: " + lista.toString());
                mv.addObject("lista", lista);
                mv.addObject("idUsuario", idUsuario);
                mv.addObject("paso", paso);
            } else {

                boolean repuesta = checkListUsuarioGpoBI.insertaCheck(idUsuario2, "996783", "323");
                repuesta = checkListUsuarioGpoBI.insertaCheck(idUsuario2, "996783", "324");

                PerfilUsuarioDTO perfilUsuarioDTO = new PerfilUsuarioDTO();
                perfilUsuarioDTO.setIdPerfil(234);
                perfilUsuarioDTO.setIdUsuario(Integer.parseInt(idUsuario2));

                boolean res = perfilusuariobi.insertaPerfilUsuario(perfilUsuarioDTO);

                boolean actualizacionGestion = perfilesNuevoBI.asignacionPerfiles();
                boolean asignacion = false;
                logger.info("ACTUALIZACION GESTION: " + actualizacionGestion);

                if (actualizacionGestion) {
                    asignacion = perfilesNuevoBI.actualizacionPerfilesGestion();
                }
                logger.info("ACTUALIZACION PERFILES: " + asignacion);
                actualizacionGestion = perfilesNuevoBI.asignacionPerfiles();
                asignacion = false;
                logger.info("ACTUALIZACION GESTION: " + actualizacionGestion);

                if (actualizacionGestion) {
                    asignacion = perfilesNuevoBI.actualizacionPerfilesGestion();
                }
                logger.info("ACTUALIZACION PERFILES: " + asignacion);
                asignacion = false;
                asignacion = perfilesNuevoBI.actualizacionPerfilesGestion();

                mv.addObject("numEmpleado", idUsuario2);
                mv.addObject("nomEmpleado", nomUsuario);
                mv.addObject("paso", 3);
            }

        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/descargaBase.htm", "central/descargaBase.htm"}, method = RequestMethod.GET)
    public ModelAndView descargaBase(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        //logger.info("Entra a Descarga Base");
        String salida = "reporteMedicion";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            List<ConsultaDTO> listaProtocolos = consultaBI.obtieneConsultaChecklist(323);

            mv.addObject("listaProtocolos", listaProtocolos);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    /*
	 * @RequestMapping(value = {"tienda/descargaBase.htm",
	 * "central/descargaBase.htm"}, method = RequestMethod.POST) public
	 * ModelAndView postdescargaBase(HttpServletRequest request,
	 * HttpServletResponse response, Model model,
	 *
	 * @RequestParam(value = "protocolo", required = false, defaultValue = "")
	 * int idChecklist,
	 *
	 * @RequestParam(value = "calendarioIni", required = false, defaultValue =
	 * "") String calendarioIni,
	 *
	 * @RequestParam(value = "calendarioFin", required = false, defaultValue =
	 * "") String calendarioFin) throws ServletException, IOException {
	 *
	 * logger.info("Entra a Descarga Base Post"); String salida=
	 * "reporteMedicion"; ModelAndView mv = new ModelAndView(salida,"command",
	 * null); try {
	 *
	 *
	 *
	 *
	 *
	 * List<ConsultaDTO> listaProtocolos =
	 * consultaBI.obtieneConsultaChecklist(323); Map<String, Object>
	 * mapReporteGeneral = null; Map<String, Object> mapReporteGeneralProtocolo
	 * = null; List<ReporteMedPregDTO> listaPreguntas = null;
	 * List<ReporteMedicionDTO> listaRespuestas = null; List<ReporteMedPregDTO>
	 * listaPreguntas2 = null; List<ReporteMedicionDTO> listaRespuestas2 = null;
	 * HashMap<String, List<ReporteMedicionDTO>> hashmap = new HashMap<>();
	 * HashMap<String, List<ReporteMedicionDTO>> hashmap2 = new HashMap<>();
	 *
	 *
	 * int idProtocolo=0; List<ProtocoloByCheckDTO> respuesta =
	 * protocoloByCheckBI.obtieneProtocolo(idChecklist);
	 * logger.info("RESP: "+respuesta); for(ProtocoloByCheckDTO auxi:respuesta){
	 * idProtocolo=auxi.getIdProtocolo(); }
	 *
	 *
	 * mapReporteGeneral = reporteMedicionBI.ReporteMedicion(idProtocolo,
	 * calendarioIni, calendarioFin); if(mapReporteGeneral!=null){
	 * Iterator<Map.Entry<String, Object>> enMap =
	 * mapReporteGeneral.entrySet().iterator(); while (enMap.hasNext()) {
	 * Map.Entry<String, Object> enMaps = enMap.next(); String nomH =
	 * enMaps.getKey(); if (nomH == "listaPregunta") { listaPreguntas =
	 * (List<ReporteMedPregDTO>) enMaps.getValue(); } if (nomH ==
	 * "listaRespuesta") { listaRespuestas = (List<ReporteMedicionDTO>)
	 * enMaps.getValue(); } }
	 *
	 * ServletOutputStream sos = null; String archivo = ""; String
	 * columnaTitle="";
	 *
	 * if(listaPreguntas!=null){ for (ReporteMedPregDTO
	 * preguntas:listaPreguntas){
	 * //logger.info("Antes: "+preguntas.getDescPregunta());
	 * preguntas.setDescPregunta(cadenaLimpia(preguntas.getDescPregunta()));
	 * //logger.info("Despues: "+preguntas.getDescPregunta());
	 * columnaTitle+="<th align='center'>"+preguntas.getDescPregunta()+"</th>";
	 * } }
	 *
	 * if(listaRespuestas!=null && listaPreguntas!=null){
	 * if(listaRespuestas.size()>0){
	 *
	 * ArrayList<String> cecos=new ArrayList<>(); String ceco="";
	 * for(ReporteMedicionDTO aux: listaRespuestas) { ceco=aux.getIdCeco();
	 * if(hashmap.size()>0){ if(hashmap.containsKey(ceco)){
	 * hashmap.get(ceco).add(aux); }else{ ArrayList<ReporteMedicionDTO> tmp=new
	 * ArrayList<>(); tmp.add(aux); hashmap.put(aux.getIdCeco(), tmp);
	 * cecos.add(aux.getIdCeco()); } } else {
	 *
	 * ArrayList<ReporteMedicionDTO> tmp=new ArrayList<>(); tmp.add(aux);
	 * hashmap.put(aux.getIdCeco(), tmp); cecos.add(aux.getIdCeco());
	 *
	 * } } logger.info("CECOS: "+cecos);
	 *
	 *
	 *
	 * archivo =
	 * "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
	 * + "<thead>" + "<tr>" + "<th align='center'>Fecha que se Realizo</th>" +
	 * "<th align='center'>Quien lo realizo</th>" +
	 * "<th align='center'>Numero de Usuario</th>" +
	 * "<th align='center'>Puesto</th>" + "<th align='center'>Pais</th>" +
	 * "<th align='center'>Territorio</th>" + "<th align='center'>Zona</th>" +
	 * "<th align='center'>Regional</th>" + "<th align='center'>Sucursal</th>" +
	 * "<th align='center'>Nombre Sucursal</th>" +
	 * "<th align='center'>Canal</th>" + "<th align='center'>Ponderacion</th>" +
	 * "<th align='center'>Calificacion</th>" +
	 * "<th align='center'>Conteo de SI</th>" +
	 * "<th align='center'>Conteo de NO</th>" +
	 * "<th align='center'>Imperdonables</th>" + columnaTitle + "</tr>" +
	 * "</thead>"; archivo += "<tbody>";
	 *
	 *
	 *
	 *
	 * for(String dto:cecos){ int contSI=0; int contNO=0; int contCritica=0;
	 * double Calif=0.0;
	 *
	 * List<ReporteMedicionDTO> reporte=hashmap.get(dto); ReporteMedicionDTO[]
	 * repLimpio=new ReporteMedicionDTO[listaPreguntas.size()]; int indc=0;
	 * for(ReporteMedPregDTO preg:listaPreguntas){ for(ReporteMedicionDTO
	 * aux:reporte){ if(preg.getIdPregunta()==aux.getIdPregunta()){
	 * repLimpio[indc]=aux; break; }
	 *
	 * }
	 *
	 * if(repLimpio[indc]==null){ ReporteMedicionDTO varAux=new
	 * ReporteMedicionDTO(); if(preg.getIdCritica()==1){
	 * varAux.setDescRespuesta("FALSO"); }else{ varAux.setDescRespuesta("NO"); }
	 * repLimpio[indc]=varAux; }
	 *
	 * indc++; }
	 *
	 * boolean flag=false; for(ReporteMedicionDTO repDTO:repLimpio){
	 * if(repDTO!=null){ if (repDTO.getDescRespuesta().trim().contains("SI")){
	 * contSI++; Calif=Calif+repDTO.getIdPonderacion(); } else if
	 * (repDTO.getDescRespuesta().trim().contains("NO")){ contNO++; } if
	 * (repDTO.getDescRespuesta().toLowerCase().trim().contains("verdadero") &&
	 * repDTO.getDescPregunta()!=null){ contCritica++; }
	 *
	 * if(!flag && repDTO.getIdCeco()!=null){ archivo += "<tr>" +
	 * "<td align='center'>"+ repDTO.getFecha() +"</td>" +
	 * "<td align='center'>"+ repDTO.getNomUsuario() +"</td>" +
	 * "<td align='center'>"+ repDTO.getIdUsuario() +"</td>" +
	 * "<td align='center'>"+ repDTO.getNomPuesto() +"</td>" +
	 * "<td align='center'>"+ repDTO.getNomPais() +"</td>" +
	 * "<td align='center'>"+ repDTO.getTerritorio() +"</td>" +
	 * "<td align='center'>"+ repDTO.getZona() +"</td>" + "<td align='center'>"+
	 * repDTO.getRegion() +"</td>" + "<td align='center'>"+ repDTO.getSucursal()
	 * +"</td>" + "<td align='center'>"+ repDTO.getNomCeco() +"</td>" +
	 * "<td align='center'>"+ repDTO.getNomCanal() +"</td>" ; flag=true; } }
	 *
	 * } double calificacion=0.0; if(contCritica==0){ calificacion=Calif; }
	 * archivo+= "<td align='center'>"+ Calif +"</td>" + "<td align='center'>"+
	 * calificacion+"</td>" +"<td align='center'>"+ contSI+"</td>" +
	 * "<td align='center'>"+ contNO +"</td>" + "<td align='center'>"+
	 * contCritica +"</td>";
	 *
	 * for(ReporteMedicionDTO repDTO:repLimpio){ if(repDTO!=null){ archivo +=
	 * "<td align='center'>"+ repDTO.getDescRespuesta() +"</td>"; } else{
	 * archivo += "<td align='center'></td>"; } }
	 *
	 * archivo +="</tr>";
	 *
	 * }
	 *
	 *
	 * int inc=0; int auxInc=0;
	 *
	 *
	 * }else{ logger.info("NO EXISTEN RESPUESTAS");
	 *
	 * archivo =
	 * "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
	 * + "<thead>" + "<tr>" + "<th align='center'>Fecha que se Realizo</th>" +
	 * "<th align='center'>Quien lo realizo</th>" +
	 * "<th align='center'>Numero de Usuario</th>" +
	 * "<th align='center'>Puesto</th>" + "<th align='center'>Pais</th>" +
	 * "<th align='center'>Territorio</th>" + "<th align='center'>Zona</th>" +
	 * "<th align='center'>Regional</th>" + "<th align='center'>Sucursal</th>" +
	 * "<th align='center'>Nombre Sucursal</th>" +
	 * "<th align='center'>Canal</th>" + "<th align='center'>Ponderacion</th>" +
	 * "<th align='center'>Calificacion</th>" +
	 * "<th align='center'>Conteo de SI</th>" +
	 * "<th align='center'>Conteo de NO</th>" +
	 * "<th align='center'>Imperdonables</th>" + columnaTitle + "</tr>" +
	 * "</thead>"; archivo += "<tbody>";
	 *
	 * archivo += "<tr>" + "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>";
	 *
	 * for (int j = 0; j < listaPreguntas.size(); j++) { archivo +=
	 * "<td align='center'>Sin Respuesta</td>"; }
	 *
	 * archivo +="</tr>"; } }else{ logger.info("NO EXISTEN RESPUESTAS");
	 *
	 * archivo =
	 * "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
	 * + "<thead>" + "<tr>" + "<th align='center'>Fecha que se Realizo</th>" +
	 * "<th align='center'>Quien lo realizo</th>" +
	 * "<th align='center'>Numero de Usuario</th>" +
	 * "<th align='center'>Puesto</th>" + "<th align='center'>Pais</th>" +
	 * "<th align='center'>Territorio</th>" + "<th align='center'>Zona</th>" +
	 * "<th align='center'>Regional</th>" + "<th align='center'>Sucursal</th>" +
	 * "<th align='center'>Nombre Sucursal</th>" +
	 * "<th align='center'>Canal</th>" + "<th align='center'>Ponderacion</th>" +
	 * "<th align='center'>Calificacion</th>" +
	 * "<th align='center'>Conteo de SI</th>" +
	 * "<th align='center'>Conteo de NO</th>" +
	 * "<th align='center'>Imperdonables</th>" + columnaTitle + "</tr>" +
	 * "</thead>"; archivo += "<tbody>";
	 *
	 * archivo += "<tr>" + "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>" +
	 * "<td align='center'>Sin Respuesta</td>";
	 *
	 * for (int j = 0; j < listaPreguntas.size(); j++) { archivo +=
	 * "<td align='center'>Sin Respuesta</td>"; } archivo+=
	 * "<td align='center'>Sin Respuesta</td>";
	 *
	 * archivo +="</tr>"; }
	 *
	 * archivo += "</tbody><table>"; DateFormat dateFormat = new
	 * SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); Date date = new Date(); String
	 * fecha = dateFormat.format(date).replace(" ",
	 * "_").replace(":","_").replace("/","_"); response.
	 * setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';"
	 * ); response.setHeader(
	 * "Content-Disposition","attachment; filename=reporteGeneralZonas"+fecha+
	 * ".xls"); response.setHeader("Pragma: no-cache", "Expires: 0"); sos =
	 * response.getOutputStream(); sos.write(archivo.getBytes()); sos.flush();
	 * sos.close(); mv.addObject("datos", "1"); }else{ //CIERRA
	 * if(mapReporteGeneral!=null)
	 * logger.info("LA CONSULTA DEL PROTOCOLO ESTA VACIA");
	 * mv.addObject("listaProtocolos", ordenaListaProtocolos(listaProtocolos,
	 * String.valueOf(idChecklist))); mv.addObject("datos", "0"); }
	 *
	 * } catch (Exception e) { e.getStackTrace(); } return mv; }
     */
    @RequestMapping(value = {"tienda/descargaBaseProtocolos.htm",
        "central/descargaBaseProtocolos.htm"}, method = RequestMethod.POST)
    public ModelAndView postdescargaBaseProtocolos(HttpServletRequest request, HttpServletResponse response,
            Model model, @RequestParam(value = "protocolo", required = false, defaultValue = "") int idChecklist,
            @RequestParam(value = "calendarioIni", required = false, defaultValue = "") String calendarioIni,
            @RequestParam(value = "calendarioFin", required = false, defaultValue = "") String calendarioFin)
            throws ServletException, IOException {

        //logger.info("Entra a Descarga Base Protocolos");
        String salida = "reporteMedicion";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            List<ConsultaDTO> listaProtocolos = consultaBI.obtieneConsultaChecklist(323);
            Map<String, Object> mapReporteGeneral = null;
            List<ReporteMedPregDTO> listaPreguntas = null;
            List<ReporteMedicionDTO> listaRespuestas = null;
            HashMap<String, List<ReporteMedicionDTO>> hashmap = new HashMap<>();

            mapReporteGeneral = reporteMedicionBI.ReporteProtMedicion(idChecklist, calendarioIni, calendarioFin);
            if (mapReporteGeneral != null) {
                Iterator<Map.Entry<String, Object>> enMap = mapReporteGeneral.entrySet().iterator();
                while (enMap.hasNext()) {
                    Map.Entry<String, Object> enMaps = enMap.next();
                    String nomH = enMaps.getKey();
                    if (nomH == "listaPregunta") {
                        listaPreguntas = (List<ReporteMedPregDTO>) enMaps.getValue();
                    }
                    if (nomH == "listaRespuesta") {
                        listaRespuestas = (List<ReporteMedicionDTO>) enMaps.getValue();
                    }
                }

                ServletOutputStream sos = null;
                String archivo = "";
                String columnaTitle = "";

                if (listaPreguntas != null) {
                    for (ReporteMedPregDTO preguntas : listaPreguntas) {
                        columnaTitle += "<th align='center'>" + preguntas.getDescPregunta() + "</th>";
                    }
                }

                if (listaRespuestas != null && listaPreguntas != null) {
                    if (listaRespuestas.size() > 0) {

                        ArrayList<String> cecos = new ArrayList<>();
                        String ceco = "";
                        for (ReporteMedicionDTO aux : listaRespuestas) {
                            ceco = aux.getIdCeco();
                            if (hashmap.size() > 0) {
                                if (hashmap.containsKey(ceco)) {
                                    hashmap.get(ceco).add(aux);
                                } else {
                                    ArrayList<ReporteMedicionDTO> tmp = new ArrayList<>();
                                    tmp.add(aux);
                                    hashmap.put(aux.getIdCeco(), tmp);
                                    cecos.add(aux.getIdCeco());
                                }
                            } else {

                                ArrayList<ReporteMedicionDTO> tmp = new ArrayList<>();
                                tmp.add(aux);
                                hashmap.put(aux.getIdCeco(), tmp);
                                cecos.add(aux.getIdCeco());

                            }
                        }
                        logger.info("CECOS: " + cecos);

                        archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                + "<thead>" + "<tr>" + "<th align='center'>Fecha que se Realizo</th>"
                                + "<th align='center'>Quien lo realizo</th>"
                                + "<th align='center'>Número de Usuario</th>" + "<th align='center'>Puesto</th>"
                                + "<th align='center'>País</th>" + "<th align='center'>Territorio</th>"
                                + "<th align='center'>Zona</th>" + "<th align='center'>Regional</th>"
                                + "<th align='center'>Sucursal</th>" + "<th align='center'>Nombre Sucursal</th>"
                                + "<th align='center'>Canal</th>" + "<th align='center'>Ponderacion</th>"
                                + "<th align='center'>Calificación</th>" + "<th align='center'>Conteo de SI</th>"
                                + "<th align='center'>Conteo de NO</th>" + "<th align='center'>Imperdonables</th>"
                                + columnaTitle + "</tr>" + "</thead>";
                        archivo += "<tbody>";

                        for (String dto : cecos) {
                            int contSI = 0;
                            int contNO = 0;
                            int contCritica = 0;
                            double Calif = 0.0;

                            List<ReporteMedicionDTO> reporte = hashmap.get(dto);
                            ReporteMedicionDTO[] repLimpio = new ReporteMedicionDTO[listaPreguntas.size()];
                            int indc = 0;
                            for (ReporteMedPregDTO preg : listaPreguntas) {
                                for (ReporteMedicionDTO aux : reporte) {
                                    if (preg.getIdPregunta() == aux.getIdPregunta()) {
                                        repLimpio[indc] = aux;
                                        break;
                                    }

                                }

                                if (repLimpio[indc] == null) {
                                    ReporteMedicionDTO varAux = new ReporteMedicionDTO();
                                    if (preg.getIdCritica() == 1) {
                                        varAux.setDescRespuesta("FALSO");
                                    } else {
                                        varAux.setDescRespuesta("NO");
                                    }
                                    repLimpio[indc] = varAux;
                                }

                                indc++;
                            }

                            boolean flag = false;
                            for (ReporteMedicionDTO repDTO : repLimpio) {
                                if (repDTO != null) {
                                    if (repDTO.getDescRespuesta().trim().contains("SI")) {
                                        contSI++;
                                        Calif = Calif + repDTO.getIdPonderacion();
                                    } else if (repDTO.getDescRespuesta().trim().contains("NO")) {
                                        contNO++;
                                    }
                                    if (repDTO.getDescRespuesta().toLowerCase().trim().contains("verdadero")
                                            && repDTO.getDescPregunta() != null) {
                                        contCritica++;
                                    }

                                    if (!flag && repDTO.getIdCeco() != null) {
                                        archivo += "<tr>" + "<td align='center'>" + repDTO.getFecha() + "</td>"
                                                + "<td align='center'>" + repDTO.getNomUsuario() + "</td>"
                                                + "<td align='center'>" + repDTO.getIdUsuario() + "</td>"
                                                + "<td align='center'>" + repDTO.getNomPuesto() + "</td>"
                                                + "<td align='center'>" + repDTO.getNomPais() + "</td>"
                                                + "<td align='center'>" + repDTO.getTerritorio() + "</td>"
                                                + "<td align='center'>" + repDTO.getZona() + "</td>"
                                                + "<td align='center'>" + repDTO.getRegion() + "</td>"
                                                + "<td align='center'>" + repDTO.getSucursal() + "</td>"
                                                + "<td align='center'>" + repDTO.getNomCeco() + "</td>"
                                                + "<td align='center'>" + repDTO.getNomCanal() + "</td>";
                                        flag = true;
                                    }
                                }

                            }
                            double calificacion = 0.0;
                            if (contCritica == 0) {
                                calificacion = Calif;
                            }
                            archivo += "<td align='center'>" + Calif + "</td>" + "<td align='center'>" + calificacion
                                    + "</td>" + "<td align='center'>" + contSI + "</td>" + "<td align='center'>"
                                    + contNO + "</td>" + "<td align='center'>" + contCritica + "</td>";

                            for (ReporteMedicionDTO repDTO : repLimpio) {
                                if (repDTO != null) {
                                    archivo += "<td align='center'>" + repDTO.getDescRespuesta() + "</td>";
                                } else {
                                    archivo += "<td align='center'></td>";
                                }
                            }

                            archivo += "</tr>";

                        }

                        int inc = 0;
                        int auxInc = 0;

                    } else {
                        logger.info("NO EXISTEN RESPUESTAS postdescargaBaseProtocolos()");

                        archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                + "<thead>" + "<tr>" + "<th align='center'>Fecha que se Realizo</th>"
                                + "<th align='center'>Quien lo realizo</th>"
                                + "<th align='center'>Numero de Usuario</th>" + "<th align='center'>Puesto</th>"
                                + "<th align='center'>Pais</th>" + "<th align='center'>Territorio</th>"
                                + "<th align='center'>Zona</th>" + "<th align='center'>Regional</th>"
                                + "<th align='center'>Sucursal</th>" + "<th align='center'>Nombre Sucursal</th>"
                                + "<th align='center'>Canal</th>" + "<th align='center'>Ponderacion</th>"
                                + "<th align='center'>Calificacion</th>" + "<th align='center'>Conteo de SI</th>"
                                + "<th align='center'>Conteo de NO</th>" + "<th align='center'>Imperdonables</th>"
                                + columnaTitle + "</tr>" + "</thead>";
                        archivo += "<tbody>";

                        archivo += "<tr>" + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                                + "<td align='center'>Sin Respuesta</td>";

                        for (int j = 0; j < listaPreguntas.size(); j++) {
                            archivo += "<td align='center'>Sin Respuesta</td>";
                        }

                        archivo += "</tr>";
                    }
                } else {
                    logger.info("NO EXISTEN RESPUESTAS postdescargaBaseProtocolos()");

                    archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                            + "<thead>" + "<tr>" + "<th align='center'>Fecha que se Realizo</th>"
                            + "<th align='center'>Quien lo realizo</th>" + "<th align='center'>Numero de Usuario</th>"
                            + "<th align='center'>Puesto</th>" + "<th align='center'>Pais</th>"
                            + "<th align='center'>Territorio</th>" + "<th align='center'>Zona</th>"
                            + "<th align='center'>Regional</th>" + "<th align='center'>Sucursal</th>"
                            + "<th align='center'>Nombre Sucursal</th>" + "<th align='center'>Canal</th>"
                            + "<th align='center'>Ponderacion</th>" + "<th align='center'>Calificacion</th>"
                            + "<th align='center'>Conteo de SI</th>" + "<th align='center'>Conteo de NO</th>"
                            + "<th align='center'>Imperdonables</th>" + columnaTitle + "</tr>" + "</thead>";
                    archivo += "<tbody>";

                    archivo += "<tr>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>";

                    for (int j = 0; j < listaPreguntas.size(); j++) {
                        archivo += "<td align='center'>Sin Respuesta</td>";
                    }
                    archivo += "<td align='center'>Sin Respuesta</td>";

                    archivo += "</tr>";
                }

                archivo += "</tbody><table>";
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
                response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
                response.setHeader("Content-Disposition",
                        "attachment; filename=reporteGeneralProtocolos" + fecha + ".xls");
                response.setHeader("Pragma: no-cache", "Expires: 0");
                sos = response.getOutputStream();
                sos.write(archivo.getBytes());
                sos.flush();
                sos.close();
                mv.addObject("datosP", "1");
            } else { // CIERRA if(mapReporteGeneral!=null)
                logger.info("LA CONSULTA DEL PROTOCOLO ESTA VACIA postdescargaBaseProtocolos()");
                mv.addObject("listaProtocolos", ordenaListaProtocolos(listaProtocolos, String.valueOf(idChecklist)));
                mv.addObject("datosP", "0");
            }

        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    /**
     * ****************************************************************
     * Galeria de Evidencias
     * *************************************************************************
     */
    @RequestMapping(value = {"tienda/indexGaleria.htm", "central/indexGaleria.htm"}, method = RequestMethod.GET)
    public ModelAndView indexGaleria(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        //logger.info("Entra a Index Galeria");
        String salida = "indexGaleria";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {

            int perfilSup = (int) request.getSession().getAttribute("perfilSupervision");
            logger.info("Perfil: " + perfilSup);
            String menu = "";
            if (perfilSup == 3) {
                menu = "1";
            } else {
                menu = "0";
            }

            List<ReporteImgDTO> listaSucursales = reporteImgBI.obtieneSucursales();

            List<ReporteImgDTO> listaChecklist = new ArrayList<>();
            List<ReporteImgDTO> lista2 = new ArrayList<>();
            ReporteImgDTO consultaDTO = new ReporteImgDTO();
            consultaDTO.setIdChecklist(0);
            consultaDTO.setProtocolo("Selecciona el Protocolo");
            lista2 = reporteImgBI.obtieneProtocolosZonas(323);
            listaChecklist.add(consultaDTO);

            for (int i = 0; i < lista2.size(); i++) {
                listaChecklist.add(lista2.get(i));
            }

            List<ReporteImgDTO> listaProtocolos = new ArrayList<>();
            List<ReporteImgDTO> lista3 = new ArrayList<>();
            ReporteImgDTO consultaDTO2 = new ReporteImgDTO();
            consultaDTO2.setIdChecklist(0);
            consultaDTO2.setProtocolo("Selecciona la Zona");
            lista3 = reporteImgBI.obtieneProtocolosZonas(324);
            listaProtocolos.add(consultaDTO2);

            for (int i = 0; i < lista3.size(); i++) {
                listaProtocolos.add(lista3.get(i));
            }

            List<String> listaCecos = new ArrayList<>();

            for (int i = 0; i < listaSucursales.size(); i++) {
                listaCecos.add(listaSucursales.get(i).getCeco() + " - " + listaSucursales.get(i).getSucursal());
            }

            logger.info("listaCecos: " + listaCecos);
            mv.addObject("listaCecos", listaCecos);
            mv.addObject("listaChecklist", listaChecklist);
            mv.addObject("listaProtocolos", listaProtocolos);
            mv.addObject("listaSucursales", listaSucursales);
            mv.addObject("menu", menu);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/indexGaleriaCeco.htm",
        "central/indexGaleriaCeco.htm"}, method = RequestMethod.POST)
    public ModelAndView indexGaleriaCeco(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idCeco", required = false, defaultValue = "") String idCeco,
            @RequestParam(value = "idCeco2", required = false, defaultValue = "") String idCeco2,
            @RequestParam(value = "idCeco3", required = false, defaultValue = "") String idCeco3,
            @RequestParam(value = "fechaInicio", required = false, defaultValue = "") String fechaInicio,
            @RequestParam(value = "fechaFin", required = false, defaultValue = "") String fechaFin,
            @RequestParam(value = "idChecklist", required = false, defaultValue = "") String idChecklist,
            @RequestParam(value = "idProtocolo", required = false, defaultValue = "") String idProtocolo,
            @RequestParam(value = "idSucursal", required = false, defaultValue = "") String idSucursal)
            throws ServletException, IOException {

        logger.info("Entra a Index Galeria Ceco " + idCeco + "/" + idCeco2 + "/" + idCeco3);
        String salida = "indexGaleria";
        String paso = "";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            int perfilSup = (int) request.getSession().getAttribute("perfilSupervision");
            logger.info("Perfil: " + perfilSup);
            String menu = "";
            if (perfilSup == 3) {
                menu = "1";
            } else {
                menu = "0";
            }
            List<ReporteImgDTO> listaSucursales = reporteImgBI.obtieneSucursales();
            if (idChecklist.equals("0")) {
                idChecklist = null;
            } else if (idChecklist.contains("P")) {
                paso = "2"; // Protocolo seleccionado
                idChecklist = idChecklist.replaceAll("P", "");
                logger.info("Check P: " + idChecklist);
            } else if (idChecklist.contains("C")) {
                paso = "3"; // Checklist seleccionado
                idChecklist = idChecklist.replaceAll("C", "");
                logger.info("Check C: " + idChecklist);
            }

            List<ReporteImgDTO> listaChecklist = new ArrayList<>();
            List<ReporteImgDTO> lista2 = new ArrayList<>();
            ReporteImgDTO consultaDTO = new ReporteImgDTO();
            consultaDTO.setIdChecklist(0);
            consultaDTO.setProtocolo("Selecciona el Protocolo");
            lista2 = reporteImgBI.obtieneProtocolosZonas(323);
            listaChecklist.add(consultaDTO);

            for (int i = 0; i < lista2.size(); i++) {
                listaChecklist.add(lista2.get(i));
            }

            List<ReporteImgDTO> listaProtocolos = new ArrayList<>();
            List<ReporteImgDTO> lista3 = new ArrayList<>();
            ReporteImgDTO consultaDTO2 = new ReporteImgDTO();
            consultaDTO2.setIdChecklist(0);
            consultaDTO2.setProtocolo("Selecciona la Zona");
            lista3 = reporteImgBI.obtieneProtocolosZonas(324);
            listaProtocolos.add(consultaDTO2);

            for (int i = 0; i < lista3.size(); i++) {
                listaProtocolos.add(lista3.get(i));
            }

            List<ReporteImgDTO> respuesta = reporteImgBI.obtieneDetalleCeco(idCeco, fechaInicio, fechaFin, idChecklist);
            if (respuesta.size() == 0) {
                paso = "0";
            } else if (idChecklist == null) {
                paso = "1"; // solo filtros obligatorios
            }
            /*
				 * else if(idChecklist!=null){ if(idChecklist.contains("P")){
				 * paso="2"; //protocolo seleccionado
				 * idChecklist=idChecklist.replaceAll("P","");
				 * logger.info("Check P: "+idChecklist); } else
				 * if(idChecklist.contains("C")){ paso="3"; //Checklist
				 * seleccionado idChecklist=idChecklist.replaceAll("C","");
				 * logger.info("Check C: "+idChecklist); } }
             */
            List<ReporteImgDTO> listaDatos = new ArrayList<>();
            for (ReporteImgDTO rep : respuesta) {
                SimpleDateFormat parseador = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");

                Date date = parseador.parse(rep.getFecha());
                rep.setFecha(formateador.format(date));
                listaDatos.add(rep);
            }
            logger.info("listaDatos: " + listaDatos);
            List<String> listaCecos = new ArrayList<>();

            for (int i = 0; i < listaSucursales.size(); i++) {
                listaCecos.add(listaSucursales.get(i).getCeco() + " - " + listaSucursales.get(i).getSucursal());
            }

            logger.info("listaCecos: " + listaCecos);
            mv.addObject("listaChecklist", ordenaListaImg(listaChecklist, idChecklist));
            mv.addObject("listaProtocolos", ordenaListaImg(listaProtocolos, idChecklist));
            mv.addObject("listaSucursales", listaCecos);
            mv.addObject("paso", paso);
            mv.addObject("listaDatos", listaDatos);
            mv.addObject("idCeco", idCeco);
            mv.addObject("idSucursal", idSucursal);
            mv.addObject("fechaInicio", fechaInicio);
            mv.addObject("fechaFin", fechaFin);
            mv.addObject("idChecklist", idChecklist);
            mv.addObject("listaCecos", listaCecos);
            mv.addObject("menu", menu);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/visitaSeleccionada.htm",
        "central/visitaSeleccionada.htm"}, method = RequestMethod.POST)
    public ModelAndView visitaSeleccionada(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idBitacora", required = false, defaultValue = "") String idBitacora,
            @RequestParam(value = "idCeco1", required = false, defaultValue = "") String idCeco1,
            @RequestParam(value = "idCeco2", required = false, defaultValue = "") String idCeco2,
            @RequestParam(value = "idCeco3", required = false, defaultValue = "") String idCeco3,
            @RequestParam(value = "fecha", required = false, defaultValue = "") String fecha,
            @RequestParam(value = "nomSucursal", required = false, defaultValue = "") String nomSucursal,
            @RequestParam(value = "nomChecklist", required = false, defaultValue = "") String nomChecklist,
            @RequestParam(value = "idCheck", required = false, defaultValue = "") String idCheck)
            throws ServletException, IOException {

        //logger.info("Entra a Visita Seleccionada");
        String salida = "visitaSeleccionada";
        String idSucursal = "";
        String paso = "";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            int perfilSup = (int) request.getSession().getAttribute("perfilSupervision");
            logger.info("Perfil: " + perfilSup);
            String menu = "";
            if (perfilSup == 3) {
                menu = "1";
            } else {
                menu = "0";
            }
            List<String> listaRutas = new ArrayList<>();
            List<ReporteImgDTO> listaEvidencias = reporteImgBI.getAcervo(idBitacora, idCheck);
            logger.info("listaEvidencias: " + listaEvidencias);
            if (!idCeco1.equals("")) {
                idSucursal = idCeco1;
            } else if (!idCeco2.equals("")) {
                idSucursal = idCeco2;
            } else if (!idCeco3.equals("")) {
                idSucursal = idCeco3;
            }

            FRQConstantes consFRQ = new FRQConstantes();
            if (listaEvidencias.size() > 0) {
                paso = "1";
                for (int j = 0; j < listaEvidencias.size(); j++) {
                    listaRutas.add(listaEvidencias.get(j).getRuta());
                    String rutaX = consFRQ.getURLServerImagenes() + listaEvidencias.get(j).getRuta();
                    listaEvidencias.get(j).setRuta(rutaX);
                }
            } else {
                paso = "0";
            }

            logger.info("listaRutas: " + listaRutas);
            mv.addObject("listaRutas", listaRutas);
            mv.addObject("listaEvidencias", listaEvidencias);
            mv.addObject("idCeco", idSucursal);
            mv.addObject("idBitacora", idBitacora);
            mv.addObject("nomSucursal", nomSucursal);
            mv.addObject("nomChecklist", nomChecklist);
            mv.addObject("fecha", fecha);
            mv.addObject("paso", paso);
            mv.addObject("menu", menu);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/imagenSeleccionada.htm",
        "central/imagenSeleccionada.htm"}, method = RequestMethod.POST)
    public ModelAndView imagenSeleccionada(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "ruta", required = false, defaultValue = "") String ruta)
            throws ServletException, IOException {

        //logger.info("Entra a Imagen Seleccionada");
        String salida = "imagenSeleccionada";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            int perfilSup = (int) request.getSession().getAttribute("perfilSupervision");
            logger.info("Perfil: " + perfilSup);
            String menu = "";
            if (perfilSup == 3) {
                menu = "1";
            } else {
                menu = "0";
            }
            mv.addObject("ruta", ruta);
            mv.addObject("menu", menu);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    /**
     * **********************************************************
     * ADMINISTRADOR PERFILES
     * *****************************************************************************
     */
    @RequestMapping(value = {"tienda/asignaPerfiles.htm", "central/asignaPerfiles.htm"}, method = RequestMethod.GET)
    public ModelAndView asignaPerfiles(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        //logger.info("Entra a Asignaciones");
        String salida = "asignaPerfiles";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {

        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/asignaPerfiles.htm", "central/asignaPerfiles.htm"}, method = RequestMethod.POST)
    public ModelAndView asignaPerfilesMenu(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "numSocio", required = false, defaultValue = "") String numSocio)
            throws ServletException, IOException {

        //logger.info("Entra a POST Asignaciones");
        String salida = "asignaPerfiles";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        String paso = "0";
        try {
            Map<String, Object> respuesta = adminSupBI.detalleMenu(Integer.parseInt(numSocio));
            List<AdminSupDTO> detalle = null;
            List<AdminSupDTO> menus = null;
            Iterator<Map.Entry<String, Object>> enMap = respuesta.entrySet().iterator();
            while (enMap.hasNext()) {
                Map.Entry<String, Object> enMaps = enMap.next();
                String nomH = enMaps.getKey();
                if (nomH == "detalle") {
                    detalle = (List<AdminSupDTO>) enMaps.getValue();
                }
                if (nomH == "menus") {
                    menus = (List<AdminSupDTO>) enMaps.getValue();
                }
            }

            List<MenusSupDTO> listaMuenu = menusSupBI.getMenus(null);
            int indice = 0;
            for (MenusSupDTO aux : listaMuenu) {
                aux.getActivo();
                if (aux.getActivo() == 0) {
                    listaMuenu.remove(indice);
                }
                indice++;
            }

            if (detalle.size() != 0) {
                paso = "1";
            }
            logger.info("listaDetalle: " + detalle);
            logger.info("listaMenus: " + menus);
            logger.info("paso: " + paso);

            mv.addObject("menus", listaMuenu);
            mv.addObject("listaDetalle", detalle);
            mv.addObject("listaMenus", menus);
            mv.addObject("numSocio", numSocio);
            mv.addObject("paso", paso);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/postAsignaPerfilesMenu.htm",
        "central/postAsignaPerfilesMenu.htm"}, method = RequestMethod.POST)
    public ModelAndView postAsignaPerfilesMenu(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "numEmpleado", required = false, defaultValue = "") String numEmpleado,
            @RequestParam(value = "checklist", required = false, defaultValue = "") String checklist)
            throws ServletException, IOException {

        //logger.info("Entra a POST Asignaciones");
        String salida = "asignaPerfiles";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        String paso = "0";
        try {
            Map<String, Object> respuesta = adminSupBI.detalleMenu(Integer.parseInt(numEmpleado));
            List<AdminSupDTO> detalle = null;
            List<AdminSupDTO> menus = null;
            Iterator<Map.Entry<String, Object>> enMap = respuesta.entrySet().iterator();
            while (enMap.hasNext()) {
                Map.Entry<String, Object> enMaps = enMap.next();
                String nomH = enMaps.getKey();
                if (nomH == "detalle") {
                    detalle = (List<AdminSupDTO>) enMaps.getValue();
                }
                if (nomH == "menus") {
                    menus = (List<AdminSupDTO>) enMaps.getValue();
                }
            }

            List<MenusSupDTO> listaMuenu = menusSupBI.getMenus(null);
            int indice = 0;
            for (MenusSupDTO aux : listaMuenu) {
                aux.getActivo();
                if (aux.getActivo() == 0) {
                    listaMuenu.remove(indice);
                }
                indice++;
            }

            // ----------- Asigna y desasigna Perfiles -----------------
            if (checklist.length() != 0) {
                String vistas = checklist.substring(0, checklist.length() - 1);
                String[] arrVistas = vistas.split(",");

                for (AdminSupDTO auxMenu : menus) {
                    boolean flag = false;
                    int idVista = 0;
                    int auxIdMenu = auxMenu.getIdMenu();
                    for (int i = 0; i < arrVistas.length; i++) {
                        idVista = Integer.parseInt(arrVistas[i]);
                        if (auxIdMenu == idVista) {
                            flag = true;
                            break;
                        }
                    }
                    if (flag == false) {
                        int respuesta2 = rolesSupBI.deleteRol(Integer.parseInt(numEmpleado), 1, auxIdMenu);
                    }
                }

                for (int i = 0; i < arrVistas.length; i++) {
                    boolean flag = false;
                    int idVista = 0;
                    idVista = Integer.parseInt(arrVistas[i]);
                    for (AdminSupDTO auxMenu : menus) {
                        if (auxMenu.getIdMenu() == idVista) {
                            flag = true;
                            break;
                        }
                    }
                    if (flag == false) {
                        int respuesta2 = rolesSupBI.insertRol(Integer.parseInt(numEmpleado), 1, idVista, 1);
                    }
                }
            } else {
                for (AdminSupDTO auxMenu : menus) {
                    int auxIdMenu = auxMenu.getIdMenu();
                    int respuesta2 = rolesSupBI.deleteRol(Integer.parseInt(numEmpleado), 1, auxIdMenu);
                }
            }

            // **********************************************
            respuesta = adminSupBI.detalleMenu(Integer.parseInt(numEmpleado));
            detalle = null;
            menus = null;
            enMap = respuesta.entrySet().iterator();
            while (enMap.hasNext()) {
                Map.Entry<String, Object> enMaps = enMap.next();
                String nomH = enMaps.getKey();
                if (nomH == "detalle") {
                    detalle = (List<AdminSupDTO>) enMaps.getValue();
                }
                if (nomH == "menus") {
                    menus = (List<AdminSupDTO>) enMaps.getValue();
                }
            }

            if (detalle.size() != 0) {
                paso = "1";
            }
            logger.info("listaDetalle: " + detalle);
            logger.info("listaMenus: " + menus);
            logger.info("paso: " + paso);

            mv.addObject("menus", listaMuenu);
            mv.addObject("listaDetalle", detalle);
            mv.addObject("listaMenus", menus);
            mv.addObject("numSocio", numEmpleado);
            mv.addObject("paso", paso);
            mv.addObject("pasoPopup", "1");
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/asignaPersonal.htm", "central/asignaPersonal.htm"}, method = RequestMethod.GET)
    public ModelAndView asignaPersonal(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Personal");
        String salida = "asignaPersonal";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {

        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/asignaPersonal.htm", "central/asignaPersonal.htm"}, method = RequestMethod.POST)
    public ModelAndView asignaPersonalPost(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "numSocio", required = false, defaultValue = "") String numSocio)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Personal Post");
        String salida = "asignaPersonal";
        String paso = "0";
        String resultado = "2";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            AltaGerenteDTO getIdGerentes = new AltaGerenteDTO();
            getIdGerentes.setIdGerente(Integer.parseInt(numSocio));
            List<AltaGerenteDTO> lista = altaGerenteBI.obtieneIdUsuarios(getIdGerentes);
            if (lista.size() > 0) {
                paso = "1";
            }
            Map<String, Object> respuesta = adminSupBI.personalAsig(Integer.parseInt(numSocio));
            List<AdminSupDTO> detalle = null;
            Iterator<Map.Entry<String, Object>> enMap = respuesta.entrySet().iterator();
            while (enMap.hasNext()) {
                Map.Entry<String, Object> enMaps = enMap.next();
                String nomH = enMaps.getKey();
                if (nomH == "detalle") {
                    detalle = (List<AdminSupDTO>) enMaps.getValue();
                }
            }
            /*
			 * Map<String,Object> respuesta =
			 * adminSupBI.personalAsig(Integer.parseInt(numSocio));
			 * List<AdminSupDTO> detalle = null; List<AdminSupDTO> lineaDirecta
			 * = null; Iterator<Map.Entry<String, Object>> enMap =
			 * respuesta.entrySet().iterator(); while (enMap.hasNext()) {
			 * Map.Entry<String, Object> enMaps = enMap.next(); String nomH =
			 * enMaps.getKey(); if (nomH == "detalle") { detalle =
			 * (List<AdminSupDTO>) enMaps.getValue(); } if (nomH == "personal")
			 * { lineaDirecta = (List<AdminSupDTO>) enMaps.getValue(); } }
             */

 /*
			 * if(detalle.size()>0){ paso="1"; }
             */
            logger.info("listaDetalle: " + detalle);
            logger.info("lineaDirecta: " + lista);
            logger.info("paso: " + paso);
            logger.info("busqueda: " + resultado);
            mv.addObject("listaDetalle", detalle);
            mv.addObject("lineaDirecta", lista);
            mv.addObject("numSocio", numSocio);
            mv.addObject("paso", paso);
            mv.addObject("resultado", resultado);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/asignaPersonalBusqueda.htm",
        "central/asignaPersonalBusqueda.htm"}, method = RequestMethod.POST)
    public ModelAndView asignaPersonalBusqueda(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "nSocio", required = false, defaultValue = "") String numSocio1,
            @RequestParam(value = "idUsuario", required = false, defaultValue = "") String idUsuario1,
            @RequestParam(value = "nSocio2", required = false, defaultValue = "") String numSocio2,
            @RequestParam(value = "idUsuario2", required = false, defaultValue = "") String idUsuario2)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Personal Busqueda");
        String salida = "asignaPersonal";
        String paso = "0";
        String resultado = "0";
        String numSocio = "";
        String idUsuario = "";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            if (!numSocio1.equals("")) {
                numSocio = numSocio1;
                idUsuario = idUsuario1;
            } else {
                numSocio = numSocio2;
                idUsuario = idUsuario2;
            }
            AltaGerenteDTO getIdGerentes = new AltaGerenteDTO();
            getIdGerentes.setIdGerente(Integer.parseInt(numSocio));
            List<AltaGerenteDTO> lista = altaGerenteBI.obtieneIdUsuarios(getIdGerentes);
            if (lista.size() > 0) {
                paso = "1";
            }

            Map<String, Object> respuesta = adminSupBI.personalAsig(Integer.parseInt(numSocio));
            List<AdminSupDTO> detalle = null;
            Iterator<Map.Entry<String, Object>> enMap = respuesta.entrySet().iterator();
            while (enMap.hasNext()) {
                Map.Entry<String, Object> enMaps = enMap.next();
                String nomH = enMaps.getKey();
                if (nomH == "detalle") {
                    detalle = (List<AdminSupDTO>) enMaps.getValue();
                }
            }

            List<AdminSupDTO> empleado = adminSupBI.busquedaEmpleado(Integer.parseInt(idUsuario));
            if (empleado.size() > 0) {
                resultado = "1";
            }

            logger.info("listaDetalle: " + detalle);
            logger.info("lineaDirecta: " + lista);
            logger.info("paso: " + paso);
            logger.info("resultado: " + resultado);
            logger.info("empleado: " + empleado);
            mv.addObject("listaDetalle", detalle);
            mv.addObject("lineaDirecta", lista);
            mv.addObject("numSocio", numSocio);
            mv.addObject("idUsuario", idUsuario);
            mv.addObject("empleado", empleado);
            mv.addObject("paso", paso);
            mv.addObject("resultado", resultado);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/asignaPersonalElimina.htm",
        "central/asignaPersonalElimina.htm"}, method = RequestMethod.POST)
    public ModelAndView asignaPersonalElimina(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idGte", required = false, defaultValue = "") String numSocio,
            @RequestParam(value = "idUsrEliminado", required = false, defaultValue = "") String idEliminado)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Personal Elimina");
        String salida = "asignaPersonal";
        String paso = "0";
        String idPerfil = "";
        List<AdminSupDTO> detalle = null;
        List<AltaGerenteDTO> lista = null;
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            AltaGerenteDTO eliminaInf = new AltaGerenteDTO();
            eliminaInf.setIdUsuario(Integer.parseInt(idEliminado));
            boolean elimina = altaGerenteBI.eliminaDisSup(eliminaInf);
            if (elimina) {
                AltaGerenteDTO getIdGerentes = new AltaGerenteDTO();
                getIdGerentes.setIdGerente(Integer.parseInt(numSocio));
                lista = altaGerenteBI.obtieneIdUsuarios(getIdGerentes);
                if (lista.size() > 0) {
                    paso = "1";
                }
                Map<String, Object> respuesta = adminSupBI.personalAsig(Integer.parseInt(numSocio));
                Iterator<Map.Entry<String, Object>> enMap = respuesta.entrySet().iterator();
                while (enMap.hasNext()) {
                    Map.Entry<String, Object> enMaps = enMap.next();
                    String nomH = enMaps.getKey();
                    if (nomH == "detalle") {
                        detalle = (List<AdminSupDTO>) enMaps.getValue();
                    }
                }

                if (detalle.size() > 0) {
                    paso = "1";
                }
            } else {
                AltaGerenteDTO getIdGerentes = new AltaGerenteDTO();
                getIdGerentes.setIdGerente(Integer.parseInt(numSocio));
                lista = altaGerenteBI.obtieneIdUsuarios(getIdGerentes);
                if (lista.size() > 0) {
                    paso = "1";
                }
                Map<String, Object> respuesta = adminSupBI.personalAsig(Integer.parseInt(numSocio));
                Iterator<Map.Entry<String, Object>> enMap = respuesta.entrySet().iterator();
                while (enMap.hasNext()) {
                    Map.Entry<String, Object> enMaps = enMap.next();
                    String nomH = enMaps.getKey();
                    if (nomH == "detalle") {
                        detalle = (List<AdminSupDTO>) enMaps.getValue();
                    }
                }

                if (detalle.size() > 0) {
                    paso = "1";
                }
            }
            logger.info("listaDetalle: " + detalle);
            logger.info("lineaDirecta: " + lista);
            logger.info("paso: " + paso);
            mv.addObject("listaDetalle", detalle);
            mv.addObject("lineaDirecta", lista);
            mv.addObject("numSocio", numSocio);
            mv.addObject("paso", paso);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/asignaPersonalAgrega.htm",
        "central/asignaPersonalAgrega.htm"}, method = RequestMethod.POST)
    public ModelAndView asignaPersonalAgrega(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idGerente", required = false, defaultValue = "") String idGerente1,
            @RequestParam(value = "idSocio", required = false, defaultValue = "") String idUsuario1,
            @RequestParam(value = "nomSocio", required = false, defaultValue = "") String nomSocio1,
            @RequestParam(value = "idGerente2", required = false, defaultValue = "") String idGerente2,
            @RequestParam(value = "idSocio2", required = false, defaultValue = "") String idUsuario2,
            @RequestParam(value = "nomSocio2", required = false, defaultValue = "") String nomSocio2)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Personal Agrega");
        String salida = "asignaPersonal";
        String paso = "0";
        String idGerente = "";
        String idUsuario = "";
        String nomSocio = "";
        List<AdminSupDTO> detalle = null;
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            if (!idGerente1.equals("")) {
                idGerente = idGerente1;
                idUsuario = idUsuario1;
                nomSocio = nomSocio1;
            } else {
                idGerente = idGerente2;
                idUsuario = idUsuario2;
                nomSocio = nomSocio2;
            }
            List<AdminSupDTO> gte = adminSupBI.busquedaEmpleado(Integer.parseInt(idGerente));
            String nomGerente = gte.get(0).getNomUsuario();

            AltaGerenteDTO altaGerente = new AltaGerenteDTO();
            altaGerente.setIdGerente(Integer.parseInt(idGerente));
            altaGerente.setNomGerente(nomGerente);
            altaGerente.setIdUsuario(Integer.parseInt(idUsuario));
            altaGerente.setNomUsuario(nomSocio);

            if (idUsuario.equals("272445") || idUsuario.equals("81145")) { // Caso
                // de
                // los
                // masters(Mayte
                // y
                // Monica)
                altaGerente.setIdPerfil(1);
            } else if (idUsuario.equals("89421") || idUsuario.equals("89025") || idUsuario.equals("88919")
                    || idUsuario.equals("89018") || idUsuario.equals("88971") || idUsuario.equals("88975")
                    || idUsuario.equals("88998") || idUsuario.equals("88968") || idUsuario.equals("156824")
                    || idUsuario.equals("89067") || idUsuario.equals("331952")) { // Caso
                // de
                // los
                // gerentes
                altaGerente.setIdPerfil(2);
            } else { // Caso de los aseguradores
                altaGerente.setIdPerfil(4);
            }

            boolean alta = altaGerenteBI.insertaGerente(altaGerente);
            if (alta) {
                Map<String, Object> respuesta = adminSupBI.personalAsig(Integer.parseInt(idGerente));
                Iterator<Map.Entry<String, Object>> enMap = respuesta.entrySet().iterator();
                while (enMap.hasNext()) {
                    Map.Entry<String, Object> enMaps = enMap.next();
                    String nomH = enMaps.getKey();
                    if (nomH == "detalle") {
                        detalle = (List<AdminSupDTO>) enMaps.getValue();
                    }
                }

                if (detalle.size() > 0) {
                    paso = "1";
                }
            } else {
                Map<String, Object> respuesta = adminSupBI.personalAsig(Integer.parseInt(idGerente));
                Iterator<Map.Entry<String, Object>> enMap = respuesta.entrySet().iterator();
                while (enMap.hasNext()) {
                    Map.Entry<String, Object> enMaps = enMap.next();
                    String nomH = enMaps.getKey();
                    if (nomH == "detalle") {
                        detalle = (List<AdminSupDTO>) enMaps.getValue();
                    }
                }

                if (detalle.size() > 0) {
                    paso = "1";
                }
            }
            AltaGerenteDTO getIdGerentes = new AltaGerenteDTO();
            getIdGerentes.setIdGerente(Integer.parseInt(idGerente));
            List<AltaGerenteDTO> lista = altaGerenteBI.obtieneIdUsuarios(getIdGerentes);
            if (lista.size() > 0) {
                paso = "1";
            }

            logger.info("listaDetalle: " + detalle);
            logger.info("lineaDirecta: " + lista);
            logger.info("paso: " + paso);
            mv.addObject("listaDetalle", detalle);
            mv.addObject("lineaDirecta", lista);
            mv.addObject("numSocio", idGerente);
            mv.addObject("paso", paso);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/asignaCecos.htm", "central/asignaCecos.htm"}, method = RequestMethod.GET)
    public ModelAndView asignaCecos(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Cecos");
        String salida = "asignaCecos";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {

        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/asignaCecos.htm", "central/asignaCecos.htm"}, method = RequestMethod.POST)
    public ModelAndView asignaCecosPost(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "numSocio", required = false, defaultValue = "") String numSocio)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Cecos Post");
        String salida = "asignaCecos";
        String paso = "0";
        String resultado = "2";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            Map<String, Object> respuesta = adminSupBI.cecosAsig(Integer.parseInt(numSocio));
            List<AdminSupDTO> detalle = null;
            List<AdminSupDTO> cecos = null;
            Iterator<Map.Entry<String, Object>> enMap = respuesta.entrySet().iterator();
            while (enMap.hasNext()) {
                Map.Entry<String, Object> enMaps = enMap.next();
                String nomH = enMaps.getKey();
                if (nomH == "detalle") {
                    detalle = (List<AdminSupDTO>) enMaps.getValue();
                }
                if (nomH == "cecos") {
                    cecos = (List<AdminSupDTO>) enMaps.getValue();
                }
            }

            if (detalle.size() > 0) {
                paso = "1";
            }
            logger.info("listaDetalle: " + detalle);
            logger.info("listaCecos: " + cecos);
            logger.info("paso: " + paso);
            logger.info("busqueda: " + resultado);
            mv.addObject("listaDetalle", detalle);
            mv.addObject("listaCecos", cecos);
            mv.addObject("numSocio", numSocio);
            mv.addObject("paso", paso);
            mv.addObject("resultado", resultado);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/asignaCecosBusqueda.htm",
        "central/asignaCecosBusqueda.htm"}, method = RequestMethod.POST)
    public ModelAndView asignaCecosBusqueda(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idSucu", required = false, defaultValue = "") String idSucu1,
            @RequestParam(value = "nSocio", required = false, defaultValue = "") String nSocio1,
            @RequestParam(value = "idSucu2", required = false, defaultValue = "") String idSucu2,
            @RequestParam(value = "nSocio2", required = false, defaultValue = "") String nSocio2)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Cecos Busqueda");
        String salida = "asignaCecos";
        String paso = "0";
        String resultado = "0";
        String idSucu = "";
        String nSocio = "";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            if (!idSucu1.equals("")) {
                idSucu = idSucu1;
                nSocio = nSocio1;
            } else {
                idSucu = idSucu2;
                nSocio = nSocio2;
            }
            Map<String, Object> resp = adminSupBI.cecosAsig(Integer.parseInt(nSocio));
            List<AdminSupDTO> detalle = null;
            List<AdminSupDTO> cecos = null;
            Iterator<Map.Entry<String, Object>> enMap = resp.entrySet().iterator();
            while (enMap.hasNext()) {
                Map.Entry<String, Object> enMaps = enMap.next();
                String nomH = enMaps.getKey();
                if (nomH == "detalle") {
                    detalle = (List<AdminSupDTO>) enMaps.getValue();
                }
                if (nomH == "cecos") {
                    cecos = (List<AdminSupDTO>) enMaps.getValue();
                }
            }

            if (detalle.size() > 0) {
                paso = "1";
            }

            List<AdminSupDTO> respuesta = adminSupBI.busquedaCeco(idSucu);
            if (respuesta.size() > 0) {
                resultado = "1";
            }

            logger.info("listaDetalle: " + detalle);
            logger.info("listaCecos: " + cecos);
            logger.info("paso: " + paso);
            logger.info("busqueda: " + resultado);
            logger.info("sucursal: " + respuesta);
            mv.addObject("listaDetalle", detalle);
            mv.addObject("listaCecos", cecos);
            mv.addObject("numSocio", nSocio);
            mv.addObject("paso", paso);
            mv.addObject("resultado", resultado);
            mv.addObject("sucursal", respuesta);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/asignaCecosAgregar.htm",
        "central/asignaCecosAgregar.htm"}, method = RequestMethod.POST)
    public ModelAndView asignaCecosAgregar(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idSucursal", required = false, defaultValue = "") String idSucursal1,
            @RequestParam(value = "idUsuario", required = false, defaultValue = "") String idUsuario1,
            @RequestParam(value = "idSucursal2", required = false, defaultValue = "") String idSucursal2,
            @RequestParam(value = "idUsuario2", required = false, defaultValue = "") String idUsuario2)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Cecos Agregar");
        String salida = "asignaCecos";
        String paso = "0";
        String resultado = "2";
        String idSucursal = "";
        String idUsuario = "";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            if (!idSucursal1.equals("")) {
                idSucursal = idSucursal1;
                idUsuario = idUsuario1;
            } else {
                idSucursal = idSucursal2;
                idUsuario = idUsuario2;
            }
            String idPerfil = "";
            String idGerente = null;
            String fechaInicio = null;
            String fechaFin = null;
            List<AdminSupDTO> detalle = null;
            List<AdminSupDTO> cecos = null;

            AltaGerenteDTO getIdGerentes = new AltaGerenteDTO();
            getIdGerentes.setIdUsuario(Integer.parseInt(idUsuario));
            List<AltaGerenteDTO> lista = altaGerenteBI.obtieneIdUsuarios(getIdGerentes);
            if (lista.size() > 0) {
                paso = "1";
            }
            List<AltaGerenteDTO> getDatos = altaGerenteBI.obtienefullTable(idUsuario, idGerente);
            logger.info("Datos: " + getDatos);
            idPerfil = String.valueOf(getDatos.get(0).getIdPerfil());
            idGerente = String.valueOf(getDatos.get(0).getIdGerente());

            int alta = asignacionesSupBI.insertaAsignacion(Integer.parseInt(idUsuario), Integer.parseInt(idPerfil),
                    idSucursal, Integer.parseInt(idGerente), 1, fechaInicio, fechaFin);
            if (alta == 1) {
                Map<String, Object> respuesta = adminSupBI.cecosAsig(Integer.parseInt(idUsuario));
                Iterator<Map.Entry<String, Object>> enMap = respuesta.entrySet().iterator();
                while (enMap.hasNext()) {
                    Map.Entry<String, Object> enMaps = enMap.next();
                    String nomH = enMaps.getKey();
                    if (nomH == "detalle") {
                        detalle = (List<AdminSupDTO>) enMaps.getValue();
                    }
                    if (nomH == "cecos") {
                        cecos = (List<AdminSupDTO>) enMaps.getValue();
                    }
                }

                if (detalle.size() > 0) {
                    paso = "1";
                }
            } else {
                Map<String, Object> respuesta = adminSupBI.cecosAsig(Integer.parseInt(idUsuario));
                Iterator<Map.Entry<String, Object>> enMap = respuesta.entrySet().iterator();
                while (enMap.hasNext()) {
                    Map.Entry<String, Object> enMaps = enMap.next();
                    String nomH = enMaps.getKey();
                    if (nomH == "detalle") {
                        detalle = (List<AdminSupDTO>) enMaps.getValue();
                    }
                    if (nomH == "cecos") {
                        cecos = (List<AdminSupDTO>) enMaps.getValue();
                    }
                }

                if (detalle.size() > 0) {
                    paso = "1";
                }
            }
            logger.info("listaDetalle: " + detalle);
            logger.info("listaCecos: " + cecos);
            logger.info("paso: " + paso);
            logger.info("busqueda: " + resultado);
            mv.addObject("listaDetalle", detalle);
            mv.addObject("listaCecos", cecos);
            mv.addObject("numSocio", idUsuario);
            mv.addObject("paso", paso);
            mv.addObject("resultado", resultado);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/asignaCecosEliminar.htm",
        "central/asignaCecosEliminar.htm"}, method = RequestMethod.POST)
    public ModelAndView asignaCecosEliminar(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idCeco", required = false, defaultValue = "") String idCeco,
            @RequestParam(value = "idSocio", required = false, defaultValue = "") String idSocio)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Cecos Eliminar");
        String salida = "asignaCecos";
        String paso = "0";
        String resultado = "2";
        List<AdminSupDTO> detalle = null;
        List<AdminSupDTO> cecos = null;
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            int exito = asignacionesSupBI.deleteAsignacion(Integer.parseInt(idSocio), idCeco);
            if (exito == 1) {
                Map<String, Object> respuesta = adminSupBI.cecosAsig(Integer.parseInt(idSocio));
                Iterator<Map.Entry<String, Object>> enMap = respuesta.entrySet().iterator();
                while (enMap.hasNext()) {
                    Map.Entry<String, Object> enMaps = enMap.next();
                    String nomH = enMaps.getKey();
                    if (nomH == "detalle") {
                        detalle = (List<AdminSupDTO>) enMaps.getValue();
                    }
                    if (nomH == "cecos") {
                        cecos = (List<AdminSupDTO>) enMaps.getValue();
                    }
                }
                if (detalle.size() > 0) {
                    paso = "1";
                }
            } else {
                Map<String, Object> respuesta = adminSupBI.cecosAsig(Integer.parseInt(idSocio));
                Iterator<Map.Entry<String, Object>> enMap = respuesta.entrySet().iterator();
                while (enMap.hasNext()) {
                    Map.Entry<String, Object> enMaps = enMap.next();
                    String nomH = enMaps.getKey();
                    if (nomH == "detalle") {
                        detalle = (List<AdminSupDTO>) enMaps.getValue();
                    }
                    if (nomH == "cecos") {
                        cecos = (List<AdminSupDTO>) enMaps.getValue();
                    }
                }
                if (detalle.size() > 0) {
                    paso = "1";
                }
            }
            logger.info("listaDetalle: " + detalle);
            logger.info("listaCecos: " + cecos);
            logger.info("paso: " + paso);
            logger.info("busqueda: " + resultado);
            mv.addObject("listaDetalle", detalle);
            mv.addObject("listaCecos", cecos);
            mv.addObject("numSocio", idSocio);
            mv.addObject("paso", paso);
            mv.addObject("resultado", resultado);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/asignaCecosMasiva.htm",
        "central/asignaCecosMasiva.htm"}, method = RequestMethod.GET)
    public ModelAndView asignaCecosMasiva(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Cecos Masiva");
        String salida = "asignaCecosMasiva";
        String exito = "9";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            mv.addObject("exito", exito);
            mv.addObject("archivo", "na");
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/asignaCecosMasiva.htm", "central/asignaCecosMasiva.htm"}, method = RequestMethod.POST)
    public ModelAndView postasignaCecosMasiva(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "uploadedFile", required = false, defaultValue = "") MultipartFile uploadedFile)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Cecos Masiva Post");
        String salida = "asignaCecosMasiva";
        String exito = "";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        DataFormatter formatter = new DataFormatter();
        String format = "";
        AsigCecosMasivaSup asignacion = new AsigCecosMasivaSup();
        String gpoProtocolos = "323";
        String gpoZonas = "324";
        int cont = 0;
        try {
            String fileName = uploadedFile.getOriginalFilename();
            logger.info("Name: " + fileName);
            File tmp = new File(fileName);
            if (!tmp.exists()) {
                uploadedFile.transferTo(tmp);
            } else if (tmp.getName().equalsIgnoreCase(fileName)) {
                boolean elimino = tmp.delete();
                if (elimino) {
                    logger.info("Archivo eliminado");
                    uploadedFile.transferTo(tmp);
                    logger.info("Archivo creado");
                } else {
                    logger.info("Archivo NO eliminado");
                }
            }
            logger.info("tmp: " + tmp.getAbsolutePath());

            if (tmp.exists()) {
                if (uploadedFile.getSize() < MAX_FILE_SIZE) {
                    // leer archivo excel
                    XSSFWorkbook worbook = new XSSFWorkbook(tmp);
                    // obtener la hoja que se va leer
                    XSSFSheet sheet = worbook.getSheetAt(0);
                    for (Row row : sheet) {
                        for (Cell cell : row) {
                            // obtiene el valor y se le da formato
                            switch (cell.getCellTypeEnum()) {
                                case STRING:
                                    format = formatter.formatCellValue(cell);
                                    if (cell.getColumnIndex() == 4) {
                                        //logger.info("SUC: " + format);
                                        asignacion.setNomSucursal(format);
                                    } else if (cell.getColumnIndex() == 6) {
                                        //logger.info("Emp: " + format);
                                        asignacion.setNomEmpleado(format);
                                    } else if (cell.getColumnIndex() == 10) {
                                        //logger.info("Ger: " + format);
                                        asignacion.setNomGerente(format);
                                    } else if (cell.getColumnIndex() == 1) {
                                        asignacion.setNomGerente(format);
                                        if (format.length() == 3) {
                                            format = 480 + format;
                                            asignacion.setNumSucursal(Integer.parseInt(format));
                                            //logger.info("Ceco: " + format);
                                        } else if (format.length() == 4) {
                                            format = 48 + format;
                                            asignacion.setNumSucursal(Integer.parseInt(format));
                                            //logger.info("Ceco: " + format);
                                        }
                                    } else if (cell.getColumnIndex() == 9 && format.length() >= 5 && format.length() <= 8) {
                                        //logger.info("#Ger: " + format);
                                        asignacion.setIdGerente(Integer.parseInt(format));
                                        List<ChecklistUsuarioDTO> lista = checklistUsuariobi.obtieneCheckU(format);
                                        Calendar fecha = new GregorianCalendar();
                                        String fechaActual = Integer.toString(fecha.get(Calendar.YEAR)) + "-"
                                                + Integer.toString(fecha.get(Calendar.MONTH) + 1) + "-"
                                                + Integer.toString(fecha.get(Calendar.DAY_OF_MONTH));
                                        if (lista.size() > 0) {
                                            for (ChecklistUsuarioDTO checkUsu : lista) {
                                                if (checkUsu.getActivo() == 1 && !checkUsu.getFechaIni().contains(fechaActual)) {
                                                    // se hace un barrido a las asignaciones
                                                    ActivarCheckListDTO desasigna = new ActivarCheckListDTO();
                                                    desasigna.setIdUsuario(String.valueOf(asignacion.getIdGerente()));
                                                    desasigna.setActivo("0");
                                                    boolean res = activarCheckListBI.actualizaCheckList(desasigna);
                                                }
                                                break;
                                            }
                                        }
                                    } else if (cell.getColumnIndex() == 5 && format.length() >= 5 && format.length() <= 8) {
                                        //logger.info("#Emp: " + format);
                                        if (!format.equals("0")) {
                                            asignacion.setIdEmpleado(Integer.parseInt(format));
                                            List<ChecklistUsuarioDTO> lista = checklistUsuariobi.obtieneCheckU(format);
                                            Calendar fecha = new GregorianCalendar();
                                            String fechaActual = Integer.toString(fecha.get(Calendar.YEAR)) + "-"
                                                    + Integer.toString(fecha.get(Calendar.MONTH) + 1) + "-"
                                                    + Integer.toString(fecha.get(Calendar.DAY_OF_MONTH));
                                            if (lista.size() > 0) {
                                                for (ChecklistUsuarioDTO checkUsu : lista) {
                                                    if (checkUsu.getActivo() == 1 && !checkUsu.getFechaIni().contains(fechaActual)) {
                                                        // se hace un barrido a las asignaciones
                                                        ActivarCheckListDTO desasigna = new ActivarCheckListDTO();
                                                        desasigna.setIdUsuario(String.valueOf(asignacion.getIdEmpleado()));
                                                        desasigna.setActivo("0");
                                                        boolean res = activarCheckListBI.actualizaCheckList(desasigna);
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case NUMERIC:
                                    if (DateUtil.isCellDateFormatted(cell)) {
                                        format = formatter.formatCellValue(cell);
                                        //logger.info(format);
                                    } else {
                                        format = formatter.formatCellValue(cell);
                                        if (format.length() == 3) {
                                            format = 480 + format;
                                            asignacion.setNumSucursal(Integer.parseInt(format));
                                            //logger.info("Ceco: " + format);
                                        } else if (format.length() == 4) {
                                            format = 48 + format;
                                            asignacion.setNumSucursal(Integer.parseInt(format));
                                            //logger.info("Ceco: " + format);
                                        } else if (cell.getColumnIndex() == 5) {
                                            //logger.info("#Emp: " + format);
                                            if (!format.equals("0")) {
                                                asignacion.setIdEmpleado(Integer.parseInt(format));
                                                List<ChecklistUsuarioDTO> lista = checklistUsuariobi.obtieneCheckU(format);
                                                Calendar fecha = new GregorianCalendar();
                                                String fechaActual = Integer.toString(fecha.get(Calendar.YEAR)) + "-"
                                                        + Integer.toString(fecha.get(Calendar.MONTH) + 1) + "-"
                                                        + Integer.toString(fecha.get(Calendar.DAY_OF_MONTH));
                                                if (lista.size() > 0) {
                                                    for (ChecklistUsuarioDTO checkUsu : lista) {
                                                        if (checkUsu.getActivo() == 1 && !checkUsu.getFechaIni().contains(fechaActual)) {
                                                            // se hace un barrido a las asignaciones
                                                            ActivarCheckListDTO desasigna = new ActivarCheckListDTO();
                                                            desasigna.setIdUsuario(
                                                                    String.valueOf(asignacion.getIdEmpleado()));
                                                            desasigna.setActivo("0");
                                                            boolean res = activarCheckListBI.actualizaCheckList(desasigna);
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        } else if (cell.getColumnIndex() == 9) {
                                            //logger.info("#Ger: " + format);
                                            asignacion.setIdGerente(Integer.parseInt(format));
                                            List<ChecklistUsuarioDTO> lista = checklistUsuariobi.obtieneCheckU(format);
                                            Calendar fecha = new GregorianCalendar();
                                            String fechaActual = Integer.toString(fecha.get(Calendar.YEAR)) + "-"
                                                    + Integer.toString(fecha.get(Calendar.MONTH) + 1) + "-"
                                                    + Integer.toString(fecha.get(Calendar.DAY_OF_MONTH));
                                            if (lista.size() > 0) {
                                                for (ChecklistUsuarioDTO checkUsu : lista) {
                                                    if (checkUsu.getActivo() == 1 && !checkUsu.getFechaIni().contains(fechaActual)) {
                                                        // se hace un barrido a las asignaciones
                                                        ActivarCheckListDTO desasigna = new ActivarCheckListDTO();
                                                        desasigna.setIdUsuario(String.valueOf(asignacion.getIdGerente()));
                                                        desasigna.setActivo("0");
                                                        boolean res = activarCheckListBI.actualizaCheckList(desasigna);
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    break;

                                default:
                                    break;
                            } // Cierra switch

                        } // Cierra for cell

                        // se realiza la asignación
                        if (asignacion.getNumSucursal() != 0) {
                            if (cont >= 1) {
                                boolean respProtocolos = checkListUsuarioGpoBI.insertaCheck(
                                        String.valueOf(asignacion.getIdEmpleado()),
                                        String.valueOf(asignacion.getNumSucursal()), gpoProtocolos);
                                if (respProtocolos) {
                                    exito = "2";
                                    boolean respZonas = checkListUsuarioGpoBI.insertaCheck(
                                            String.valueOf(asignacion.getIdEmpleado()),
                                            String.valueOf(asignacion.getNumSucursal()), gpoZonas);
                                    if (respZonas) {
                                        exito = "1";
                                        boolean protoGerente = checkListUsuarioGpoBI.insertaCheck(
                                                String.valueOf(asignacion.getIdGerente()),
                                                String.valueOf(asignacion.getNumSucursal()), gpoProtocolos);
                                        boolean zonaGerente = checkListUsuarioGpoBI.insertaCheck(
                                                String.valueOf(asignacion.getIdGerente()),
                                                String.valueOf(asignacion.getNumSucursal()), gpoZonas);
                                    }
                                } else {
                                    exito = "0";
                                } // Cierra if respProtocolos
                            } // Cierra if cont
                        } // Cierra if asignacion
                        cont++;
                        mv.addObject("exito", exito);
                    } // Cierra for row

                    worbook.close();
                    boolean del = tmp.delete();
                    logger.info("¿Eliminado? " + del);
                    logger.info("Exito: " + exito);
                } else {
                    mv.addObject("archivo", "size");
                    logger.info("El archivo" + uploadedFile.getOriginalFilename() + " es mayor a 100MB");
                }

            } else {
                mv.addObject("archivo", "reintentar");
                logger.info("El directorio " + uploadedFile.getOriginalFilename() + " no existe");
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/descargaBase.htm", "central/descargaBase.htm"}, method = RequestMethod.POST)
    public ModelAndView postdescargaBase(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "protocolo", required = false, defaultValue = "") int idChecklist,
            @RequestParam(value = "calendarioIni", required = false, defaultValue = "") String fechaInicio,
            @RequestParam(value = "calendarioFin", required = false, defaultValue = "") String fechaFin)
            throws ServletException, IOException {

        //logger.info("Entra a Descarga Base Post");
        List<RepMedicionDTO> listaPreguntas = null;
        List<RepMedicionDTO> listaDatos = null;
        List<String> listaRespuestasFinal = new ArrayList<String>();
        int idProtocolo = 0;
        String salida = "reporteMedicion";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            List<ProtocoloByCheckDTO> respuesta = protocoloByCheckBI.obtieneProtocolo(idChecklist);
            for (ProtocoloByCheckDTO auxi : respuesta) {
                idProtocolo = auxi.getIdProtocolo();
            }
            listaDatos = repMedicionBI.ReporteMedZonas(idProtocolo, fechaInicio, fechaFin);
            logger.info("RESPUESTA: " + listaDatos);
            RepMedicionDTO r = new RepMedicionDTO();
            r.setIdProtocolo(idProtocolo);
            listaPreguntas = repMedicionBI.ReportePreguntas(r);
            logger.info("PREGUNTAS: " + listaPreguntas);

            String[] arr = new String[listaPreguntas.size()];
            ServletOutputStream sos = null;
            String archivo = "";
            String columnaTitle = "";
            // Charly
            List<String[]> res = new ArrayList<String[]>();
            if (listaDatos != null) {
                for (RepMedicionDTO reporte : listaDatos) {
                    arr = new String[listaPreguntas.size()];

                    for (int i = 0; i < arr.length; i++) {
                        arr[i] = "";
                    }

                    int apuntador = 0;
                    for (RepMedicionDTO aux : listaPreguntas) {
                        String[] arrTemp = reporte.getDescRespuesta().split("#####");

                        for (int i = 0; i < arrTemp.length; i++) {
                            String[] resp = arrTemp[i].split("====");
                            int idPreg = Integer.parseInt(resp[0]);
                            if (idPreg == aux.getIdPregunta()) {
                                arr[apuntador] = resp[1];

                            }
                        }
                        apuntador++;
                    }
                    res.add(arr);
                    listaRespuestasFinal.add(Arrays.toString(arr));
                }
            }

            if (listaPreguntas != null) {
                for (RepMedicionDTO preguntas : listaPreguntas) {
                    columnaTitle += "<th align='center'>" + preguntas.getPregunta() + "</th>";
                }
            }
            if (listaDatos != null && listaPreguntas != null) {
                if (listaDatos.size() > 0) {
                    archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                            + "<thead>" + "<tr>" + "<th align='center'>Fecha que se Realizo</th>"
                            + "<th align='center'>Quien lo realizo</th>" + "<th align='center'>Número de Usuario</th>"
                            + "<th align='center'>Puesto</th>" + "<th align='center'>País</th>"
                            + "<th align='center'>Territorio</th>" + "<th align='center'>Zona</th>"
                            + "<th align='center'>Regional</th>" + "<th align='center'>Sucursal</th>"
                            + "<th align='center'>Nombre Sucursal</th>" + "<th align='center'>Canal</th>"
                            + "<th align='center'>Ponderación</th>" + "<th align='center'>Calificación</th>"
                            + "<th align='center'>Conteo de SI</th>" + "<th align='center'>Conteo de NO</th>"
                            + "<th align='center'>Imperdonables</th>" + columnaTitle + "</tr>" + "</thead>";
                    archivo += "<tbody>";

                    int contador = 0;
                    for (int i = 0; i < listaDatos.size(); i++) {
                        archivo += "<tr>";
                        archivo += "<td align='center'>" + listaDatos.get(i).getFecha() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getEmpleado() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getIdEmpleado() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getPuesto() + "</td>"
                                + "<td align='center'>" + listaDatos.get(i).getPais() + "</td>" + "<td align='center'>"
                                + listaDatos.get(i).getTerritorio() + "</td>" + "<td align='center'>"
                                + listaDatos.get(i).getZona() + "</td>" + "<td align='center'>"
                                + listaDatos.get(i).getRegional() + "</td>" + "<td align='center'>"
                                + listaDatos.get(i).getSucursal() + "</td>" + "<td align='center'>"
                                + listaDatos.get(i).getNomSucursal() + "</td>" + "<td align='center'>"
                                + listaDatos.get(i).getCanal() + "</td>" + "<td align='center'>"
                                + listaDatos.get(i).getPonderacion() + "</td>" + "<td align='center'>"
                                + listaDatos.get(i).getCalificacion() + "</td>" + "<td align='center'>"
                                + listaDatos.get(i).getContSi() + "</td>" + "<td align='center'>"
                                + listaDatos.get(i).getContNo() + "</td>" + "<td align='center'>"
                                + listaDatos.get(i).getContImp() + "</td>";

                        int cont = 0;
                        logger.info("LIST: " + listaRespuestasFinal);
                        for (int j = 0; j < listaPreguntas.size(); j++) {

                            archivo += "<td align='center'>" + res.get(contador)[j] + "</td>";
                            cont++;
                        } // Cierra 2do for
                        archivo += "</tr>";
                        contador++;
                    } // Cierra 1er for
                } else {
                    logger.info("NO EXISTEN RESPUESTAS postdescargaBase()");

                    archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                            + "<thead>" + "<tr>" + "<th align='center'>Fecha que se Realizo</th>"
                            + "<th align='center'>Quien lo realizo</th>" + "<th align='center'>Numero de Usuario</th>"
                            + "<th align='center'>Puesto</th>" + "<th align='center'>Pais</th>"
                            + "<th align='center'>Territorio</th>" + "<th align='center'>Zona</th>"
                            + "<th align='center'>Regional</th>" + "<th align='center'>Sucursal</th>"
                            + "<th align='center'>Nombre Sucursal</th>" + "<th align='center'>Canal</th>"
                            + "<th align='center'>Ponderacion</th>" + "<th align='center'>Calificacion</th>"
                            + "<th align='center'>Conteo de SI</th>" + "<th align='center'>Conteo de NO</th>"
                            + "<th align='center'>Imperdonables</th>" + columnaTitle + "</tr>" + "</thead>";
                    archivo += "<tbody>";

                    archivo += "<tr>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                            + "<td align='center'>Sin Respuesta</td>";

                    for (int j = 0; j < listaPreguntas.size(); j++) {
                        archivo += "<td align='center'>Sin Respuesta</td>";
                    }

                    archivo += "</tr>";
                }
            } else {
                logger.info("NO EXISTEN RESPUESTAS postdescargaBase()");

                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                        + "<thead>" + "<tr>" + "<th align='center'>Fecha que se Realizo</th>"
                        + "<th align='center'>Quien lo realizo</th>" + "<th align='center'>Numero de Usuario</th>"
                        + "<th align='center'>Puesto</th>" + "<th align='center'>Pais</th>"
                        + "<th align='center'>Territorio</th>" + "<th align='center'>Zona</th>"
                        + "<th align='center'>Regional</th>" + "<th align='center'>Sucursal</th>"
                        + "<th align='center'>Nombre Sucursal</th>" + "<th align='center'>Canal</th>"
                        + "<th align='center'>Ponderacion</th>" + "<th align='center'>Calificacion</th>"
                        + "<th align='center'>Conteo de SI</th>" + "<th align='center'>Conteo de NO</th>"
                        + "<th align='center'>Imperdonables</th>" + columnaTitle + "</tr>" + "</thead>";
                archivo += "<tbody>";

                archivo += "<tr>" + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                        + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                        + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                        + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                        + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                        + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                        + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>"
                        + "<td align='center'>Sin Respuesta</td>" + "<td align='center'>Sin Respuesta</td>";

                for (int j = 0; j < listaPreguntas.size(); j++) {
                    archivo += "<td align='center'>Sin Respuesta</td>";
                }
                archivo += "<td align='center'>Sin Respuesta</td>";

                archivo += "</tr>";
            }

            archivo += "</tbody><table>";
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition", "attachment; filename=reporteGeneralProtocolos" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();
            sos.write(archivo.getBytes());
            sos.flush();
            sos.close();

        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/adminChecklist.htm", "central/adminChecklist.htm"}, method = RequestMethod.GET)
    public ModelAndView adminChecklist(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Cecos Masiva");
        String salida = "adminChecklist";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            String fileRoute = "/franquicia/imagenes";
            /*
			 * String idusu=""+request.getSession().getAttribute("idUsuario");
			 * String rootPath=File.listRoots()[0].getAbsolutePath(); String
			 * ruta=rootPath+fileRoute; File route=new File(ruta); String[]
			 * lista =route.list();
			 *
			 * if(new File(fileRoute).isDirectory()){
			 * logger.info("El directorio"+fileRoute+", si existe");
			 * mv.addObject(salida, new File(fileRoute).list()); for(String
			 * arch:lista){ if(arch.contains(idusu)){
			 * logger.info("Si se encontro"); mv.addObject("OKARCH","OK");
			 * mv.addObject("NombreArchivo",arch); break; }else {
			 * mv.addObject("OKARCH","NO"); logger.info("No se encontro"); } }
			 *
			 * }else { logger.info("El directorio no existe");
			 * mv.addObject("directorio","noExiste"); }
			 * mv.addObject("fileAction","subirArchivo");
             */
            mv.addObject("ruta", fileRoute);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    //http://10.51.219.179:8080/checklist/central/adminChecklist.htm
    @RequestMapping(value = {"tienda/adminChecklist.htm", "central/adminChecklist.htm"}, method = RequestMethod.POST)
    public ModelAndView adminChecklist(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "uploadedFile", required = false, defaultValue = "") MultipartFile uploadedFile,
            @RequestParam(value = "fileAction", required = false, defaultValue = "") String fileAction,
            @RequestParam(value = "fileRoute", required = false, defaultValue = "") String fileRoute,
            @RequestParam(value = "numero", required = false, defaultValue = "") String numero)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Cecos Masiva Post");
        String salida = "adminChecklist";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        int tipoCarga = 1;

        if (!numero.equals("")) {
            tipoCarga = Integer.parseInt(numero);
        }

        try {
            String fileName = uploadedFile.getOriginalFilename();
            //logger.info("Name: " + fileName);
            File tmp = new File(fileName);
            if (!tmp.exists()) {
                uploadedFile.transferTo(tmp);
            }
            String rootPath = File.listRoots()[0].getAbsolutePath();
            //logger.info("rootPath: " + rootPath);
            // fileRoute="/franquicia/imagenes";

            File file = new File(fileName);
            //logger.info("filePath: " + file.getAbsolutePath());

            if (file.exists()) {
                if (uploadedFile.getSize() < MAX_FILE_SIZE) {

                    boolean respuesta = false;

                    ArrayList<PreguntaDTO> listaPreguntas = new ArrayList<PreguntaDTO>();
                    ArrayList<ChecklistProtocoloDTO> listaProtocolos = new ArrayList<ChecklistProtocoloDTO>();
                    // leer archivo excel
                    // leer archivo excel
                    String parametroResult = "0";

                    try {

                        parametroResult = parametrobi.obtieneParametros("tipoLecturaExcel").get(0).getValor();

                    } catch (Exception e) {

                        logger.info("AP con el parámetro tipoLecturaExcel");
                        parametroResult = "0";

                    }

                    ArrayList<ChecklistLayoutDTO> listaEntrante = new ArrayList<ChecklistLayoutDTO>();

                    if (parametroResult.compareToIgnoreCase("0") == 0) {

                        listaEntrante = checklistAdminBI.leerExcell(file);

                    } else {

                        listaEntrante = checklistAdminBI.leerExcellMejorado(file);
                    }

                    //logger.info("El archivo: "+listaEntrante.toString());
                    // MÓDULOS
                    ArrayList<ModuloDTO> listaModulos = new ArrayList<ModuloDTO>();
                    listaModulos = checklistAdminBI.getListaModulos(listaEntrante);

                    ArrayList<ArrayList<ModuloDTO>> responseInsertaModulos = checklistAdminBI.insertaModulos(listaModulos);

                    ArrayList<ModuloDTO> listaModulosInsertados = responseInsertaModulos.get(1);
                    ArrayList<ModuloDTO> listaModulosNoInsertados = responseInsertaModulos.get(0);

                    // Se insertaron correctamente todos los módulos
                    if (listaModulosNoInsertados.size() == 0 && (listaModulosInsertados.size() == listaModulos.size())) {

                        // PREGUNTAS
                        listaPreguntas = checklistAdminBI.getListaPreguntas(listaEntrante, listaModulosInsertados, tipoCarga);

                        ArrayList<ArrayList<PreguntaDTO>> responseInsertaPreguntas = checklistAdminBI
                                .insertaPreguntas(listaPreguntas, tipoCarga);
                        ArrayList<PreguntaDTO> listaPreguntasInsertados = responseInsertaPreguntas.get(1);
                        ArrayList<PreguntaDTO> listaPreguntasNoInsertados = responseInsertaPreguntas.get(0);

                        if (listaPreguntasNoInsertados.size() == 0
                                && (listaPreguntasInsertados.size() == listaPreguntas.size())) {

                            respuesta = true;

                        } else {

                            respuesta = false;
                        }

                        // No se insertaron todos los módulos correctamente
                    } else {
                        respuesta = false;

                    }
                    ArrayList<ChecklistProtocoloDTO> listaProtocolosInsertados = null;
                    //INSERCIÓN DE PROTOCOLOS
                    if (respuesta) {

                        listaProtocolos = checklistAdminBI.getListaProtocolos(listaEntrante, tipoCarga);

                        ArrayList<ArrayList<ChecklistProtocoloDTO>> responseInsertaProtocolos = checklistAdminBI.insertaInfoProtocolos(listaProtocolos, request.getParameter("user"));

                        listaProtocolosInsertados = responseInsertaProtocolos.get(1);
                        ArrayList<ChecklistProtocoloDTO> listaProtocolosNoInsertados = responseInsertaProtocolos.get(0);

                        if (listaProtocolosNoInsertados.size() == 0 && (listaProtocolosInsertados.size() == listaProtocolos.size())) {

                            respuesta = true;

                        } else {

                            respuesta = false;
                        }

                    } else {
                        logger.info("NO SE INSERTA INFORMACIÓN DE PROTOCOLOS adminChecklist()");
                    }

                    //INSERCIÓN DE CHECKUSUA
                    if (respuesta) {

                        ArrayList<ChecklistLayoutDTO> infoLayout = checklistAdminBI.setInfoLayout(listaEntrante, listaPreguntas, listaProtocolos, tipoCarga);

                        //logger.info("infoLayout" + infoLayout.size());
                        ArrayList<ArrayList<ChecklistLayoutDTO>> responseInsertaCheckusua = checklistAdminBI.insertaCheckusua(infoLayout);

                        ArrayList<ChecklistLayoutDTO> listaCheckusuaInsertados = responseInsertaCheckusua.get(1);
                        ArrayList<ChecklistLayoutDTO> listaCheckusuaNoinsertados = responseInsertaCheckusua.get(0);

                        // Se insertaron correctamente todos los módulos
                        if (listaCheckusuaNoinsertados.size() == 0 && (listaCheckusuaInsertados.size() == infoLayout.size())) {
                            respuesta = true;
                            listaEntrante = responseInsertaCheckusua.get(1);
                            // No se insertaron todos los módulos correctamente
                        } else {
                            respuesta = false;
                        }

                    } else {
                        logger.info("NO SE INSERTA INFORMACIÓN DE CHECKUSUA adminChecklist()");

                    }

                    //---------ALTA ARBOL DE DESICION-------------
                    if (respuesta) {

                        ArrayList<ChecklistLayoutDTO> listaArbol = checklistAdminBI.getListaArbol(listaEntrante, tipoCarga);

                        //ArrayList<ChecklistLayoutDTO> listaArbolSalida = checklistAdminBI.getListaArbol(listaEntrante, 2);
                        ArrayList<ChecklistLayoutDTO> listaArbolSalida = checklistAdminBI.altaArbol(listaArbol, tipoCarga);

                        String preguntasError = "";
                        for (ChecklistLayoutDTO layout : listaArbolSalida) {
                            if (layout.getFlagErrorArbol() != 0) {
                                preguntasError = preguntasError + layout.getIdConsecutivo() + ", ";
                            }

                        }
                        if (preguntasError.equals("")) {

                            respuesta = true;

                        } else {
                            logger.info("PREGUNTAS CON ERROR AL CREAR ARBOL: " + preguntasError);
                            respuesta = false;
                        }

                    } else {
                        logger.info("NO SE INSERTA LOS ÁRBOLES ");
                    }

                    //código para agregar las zonas
                    if (respuesta) {

                        try {
                            //PreguntaBI preguntabi = (PreguntaBI) FRQAppContextProvider.getApplicationContext().getBean("preguntaBI");

                            for (int i = 0; i < listaPreguntas.size(); i++) {
                                int idPregunta = listaPreguntas.get(i).getIdPregunta();
                                System.out.println("ID_PREGUNTA_TEMP: " + idPregunta);

                                //listaPreguntasCorrectos.add(listaPreguntas.get(i));
                                //Se agrega el id pregunta e id zona en la tabla de Preg-Zona
                                AdmTipoZonaDTO tipoZona = new AdmTipoZonaDTO();
                                tipoZona.setIdTipoZona(tipoCarga + "");
                                ArrayList<AdmTipoZonaDTO> tipoZonaComp = admTipoZonaBI.obtieneTipoZonaById(tipoZona);
                                if (tipoZonaComp.get(0).getDescripcion().toUpperCase().contains("ASEGURAMIENTO")) {
                                    //Se obtiene el catalogo de zonas para aseguramiento
                                    AdminZonaDTO zona = new AdminZonaDTO();
                                    zona.setIdTipo(tipoCarga + "");
                                    ArrayList<AdminZonaDTO> zonaComp = admZonasBI.obtieneZonaById(zona);

                                    //Se recorre el catalogo de zonas de aseguramiento
                                    for (AdminZonaDTO zonaDTO : zonaComp) {
                                        System.out.println("ZONA DEL LAYOUT: " + listaPreguntas.get(i).getArea());
                                        System.out.println("ZONA DE CATALOGO: " + zonaDTO.getDescripcion());
                                        if (listaPreguntas.get(i).getArea().toUpperCase().contains(zonaDTO.getDescripcion().toUpperCase())) {
                                            //logger.info("Preg: " + listaPreguntas.get(i).getArea());
                                            AdmPregZonaDTO pregZona = new AdmPregZonaDTO();
                                            pregZona.setIdPreg(idPregunta + "");
                                            pregZona.setIdZona(zonaDTO.getIdZona());
                                            respuesta = admPregZonasBI.insertaPregZona(pregZona);
                                            if (respuesta) {
                                                break;
                                            }
                                        } else {
                                            logger.info("La zona no se encuentra en el catalogo de zonas");
                                        }
                                    }
                                } else if (tipoZonaComp.get(0).getDescripcion().toUpperCase().contains("INFRAESTRUCTURA")) {
                                    //Se obtiene el catalogo de zonas para infraestructura
                                    AdminZonaDTO zona = new AdminZonaDTO();
                                    zona.setIdTipo(tipoCarga + "");
                                    ArrayList<AdminZonaDTO> zonaComp = admZonasBI.obtieneZonaById(zona);

                                    //Se recorre el catalogo de zonas de infraestructura
                                    for (AdminZonaDTO zonaDTO : zonaComp) {
                                        if (listaPreguntas.get(i).getArea().toUpperCase().contains(zonaDTO.getDescripcion().toUpperCase())) {
                                            //logger.info("Preg: " + listaPreguntas.get(i).getArea());
                                            AdmPregZonaDTO pregZona = new AdmPregZonaDTO();
                                            pregZona.setIdPreg(idPregunta + "");
                                            pregZona.setIdZona(zonaDTO.getIdZona());
                                            respuesta = admPregZonasBI.insertaPregZona(pregZona);
                                            if (respuesta) {
                                                break;
                                            }
                                        } else {
                                            logger.info("La zona no se encuentra en el catalogo de zonas");
                                        }
                                    }
                                } else if (tipoZonaComp.get(0).getDescripcion().toUpperCase().contains("TRANSFORMACIÓN")) {
                                    //Se obtiene el catalogo de zonas para transformacion
                                    AdminZonaDTO zona = new AdminZonaDTO();
                                    zona.setIdTipo(tipoCarga + "");
                                    ArrayList<AdminZonaDTO> zonaComp = admZonasBI.obtieneZonaById(zona);

                                    //Se recorre el catalogo de zonas de transformaciones
                                    for (AdminZonaDTO zonaDTO : zonaComp) {
                                        if (listaPreguntas.get(i).getArea().toUpperCase().contains(zonaDTO.getDescripcion().toUpperCase())) {
                                            //logger.info("Preg: " + listaPreguntas.get(i).getArea());
                                            AdmPregZonaDTO pregZona = new AdmPregZonaDTO();
                                            pregZona.setIdPreg(idPregunta + "");
                                            pregZona.setIdZona(zonaDTO.getIdZona());
                                            respuesta = admPregZonasBI.insertaPregZona(pregZona);
                                            if (respuesta) {
                                                break;
                                            }
                                        } else {
                                            logger.info("La zona no se encuentra en el catalogo de zonas");
                                        }
                                    }
                                }
                            }

                            //response.add(0, listaPreguntasIncorrectos);
                            //response.add(1, listaPreguntasCorrectos);
                        } catch (Exception e) {
                            logger.info("Ocurrio algo en AP al insertar PREGUNTA: " + e.getMessage());
                            //System.out.println(e.getMessage());
                            //System.out.println("AP :" + e);
                        }

                    }

                    if (respuesta) {
                        //res = true;
                        String nomProtocolo = listaEntrante.get(0).getNombreProtocolo();
                        boolean resp = versionCheckAdmBI.cargaVesionesTabProd(nomProtocolo);
                        for (ChecklistProtocoloDTO aux : listaProtocolosInsertados) {
                            OrdenGrupoDTO grupoOrden = new OrdenGrupoDTO();
                            grupoOrden.setIdGrupo(0);
                            if (tipoCarga == 1) {
                                grupoOrden.setIdGrupo(321);
                            } else {
                                grupoOrden.setIdGrupo(323);
                            }

                            grupoOrden.setIdChecklist(aux.getIdChecklist());
                            grupoOrden.setOrden(1);
                            grupoOrden.setCommit(1);

                            int result = ordenGrupoBI.insertaGrupo(grupoOrden);
                        }
                    }

                } else {
                    mv.addObject("archivoSubido", "size");
                    logger.info("El archivo" + uploadedFile.getOriginalFilename() + " es mayor a 100MB");
                }

            } else {
                mv.addObject("archivoSubido", "no");
                logger.info("El directorio " + uploadedFile.getOriginalFilename() + " no existe");
            }
            mv.addObject("ruta", fileRoute);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = {"tienda/adminChecklistTemp.htm", "central/adminChecklistTemp.htm"}, method = RequestMethod.GET)
    public ModelAndView getAdminChecklistTemp(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Cecos Masiva");
        String salida = "adminChecklistValidador";
        ModelAndView mv = new ModelAndView(salida, "command" , null);
        try {
            String fileRoute = "/franquicia/imagenes";
            /*
			 * String idusu=""+request.getSession().getAttribute("idUsuario");
			 * String rootPath=File.listRoots()[0].getAbsolutePath(); String
			 * ruta=rootPath+fileRoute; File route=new File(ruta); String[]
			 * lista =route.list();
			 *
			 * if(new File(fileRoute).isDirectory()){
			 * logger.info("El directorio"+fileRoute+", si existe");
			 * mv.addObject(salida, new File(fileRoute).list()); for(String
			 * arch:lista){ if(arch.contains(idusu)){
			 * logger.info("Si se encontro"); mv.addObject("OKARCH","OK");
			 * mv.addObject("NombreArchivo",arch); break; }else {
			 * mv.addObject("OKARCH","NO"); logger.info("No se encontro"); } }
			 *
			 * }else { logger.info("El directorio no existe");
			 * mv.addObject("directorio","noExiste"); }
			 * mv.addObject("fileAction","subirArchivo");
             */
            mv.addObject("ruta", fileRoute);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return mv;
    }

    //http://10.51.219.179:8080/checklist/central/adminChecklistTemp.htm
    @RequestMapping(value = {"tienda/adminChecklistTemp.htm", "central/adminChecklistTemp.htm"}, method = RequestMethod.POST)
    public ModelAndView adminChecklistTemp(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "uploadedFile", required = false, defaultValue = "") MultipartFile uploadedFile,
            @RequestParam(value = "fileAction", required = false, defaultValue = "") String fileAction,
            @RequestParam(value = "fileRoute", required = false, defaultValue = "") String fileRoute,
            @RequestParam(value = "numero", required = false, defaultValue = "") String numero)
            throws ServletException, IOException {

        //logger.info("Entra a Asigna Cecos Masiva Post");
        String salida = "adminChecklistValidador";
        ModelAndView mv = new ModelAndView(salida, "command" , null);

        int tipoCarga = 1;

        if (!numero.equals("")) {
            tipoCarga = Integer.parseInt(numero);
        }

        try {
            String fileName = uploadedFile.getOriginalFilename();
            logger.info("Name: " + fileName);
            File tmp = new File(fileName);
            if (!tmp.exists()) {
                uploadedFile.transferTo(tmp);
            }
            String rootPath = File.listRoots()[0].getAbsolutePath();
            logger.info("rootPath: " + rootPath);
            // fileRoute="/franquicia/imagenes";

            File file = new File(fileName);
            logger.info("filePath: " + file.getAbsolutePath());

            if (file.exists()) {
                if (uploadedFile.getSize() < MAX_FILE_SIZE) {

                    boolean respuesta = false;

                    ArrayList<PreguntaDTO> listaPreguntas = new ArrayList<PreguntaDTO>();
                    ArrayList<ChecklistProtocoloDTO> listaProtocolos = new ArrayList<ChecklistProtocoloDTO>();
                    // leer archivo excel
                    String parametroResult = "0";

                    try {

                        parametroResult = parametrobi.obtieneParametros("tipoLecturaExcel").get(0).getValor();

                    } catch (Exception e) {

                        logger.info("AP con el parámetro tipoLecturaExcel");
                        parametroResult = "0";

                    }

                    ArrayList<ChecklistLayoutDTO> listaEntrante = new ArrayList<ChecklistLayoutDTO>();

                    if (parametroResult.compareToIgnoreCase("0") == 0) {

                        listaEntrante = checklistAdminBI.leerExcell(file);

                    } else {

                        listaEntrante = checklistAdminBI.leerExcellMejorado(file);
                    }
                    logger.info("El archivo: " + listaEntrante.toString());

                    // MÓDULOS
                    ArrayList<ModuloDTO> listaModulos = new ArrayList<ModuloDTO>();
                    listaModulos = checklistValidaAdminBI.getListaModulos(listaEntrante);

                    ArrayList<ArrayList<ModuloDTO>> responseInsertaModulos = checklistValidaAdminBI.insertaModulos(listaModulos);

                    ArrayList<ModuloDTO> listaModulosInsertados = responseInsertaModulos.get(1);
                    ArrayList<ModuloDTO> listaModulosNoInsertados = responseInsertaModulos.get(0);

                    // Se insertaron correctamente todos los módulos
                    if (listaModulosNoInsertados.size() == 0 && (listaModulosInsertados.size() == listaModulos.size())) {

                        // PREGUNTAS
                        listaPreguntas = checklistValidaAdminBI.getListaPreguntas(listaEntrante, listaModulosInsertados, tipoCarga);

                        ArrayList<ArrayList<PreguntaDTO>> responseInsertaPreguntas = checklistValidaAdminBI
                                .insertaPreguntas(listaPreguntas, tipoCarga);
                        ArrayList<PreguntaDTO> listaPreguntasInsertados = responseInsertaPreguntas.get(1);
                        ArrayList<PreguntaDTO> listaPreguntasNoInsertados = responseInsertaPreguntas.get(0);

                        if (listaPreguntasNoInsertados.size() == 0
                                && (listaPreguntasInsertados.size() == listaPreguntas.size())) {

                            respuesta = true;

                        } else {

                            respuesta = false;
                        }

                        // No se insertaron todos los módulos correctamente
                    } else {
                        respuesta = false;

                    }

                    //INSERCIÓN DE PROTOCOLOS
                    if (respuesta) {

                        listaProtocolos = checklistValidaAdminBI.getListaProtocolos(listaEntrante, tipoCarga);

                        ArrayList<ArrayList<ChecklistProtocoloDTO>> responseInsertaProtocolos = checklistValidaAdminBI.insertaInfoProtocolos(listaProtocolos, request.getParameter("user"));

                        ArrayList<ChecklistProtocoloDTO> listaProtocolosInsertados = responseInsertaProtocolos.get(1);
                        ArrayList<ChecklistProtocoloDTO> listaProtocolosNoInsertados = responseInsertaProtocolos.get(0);

                        if (listaProtocolosNoInsertados.size() == 0 && (listaProtocolosInsertados.size() == listaProtocolos.size())) {

                            respuesta = true;

                        } else {

                            respuesta = false;
                        }

                    } else {
                        logger.info("NO SE INSERTA INFORMACIÓN DE PROTOCOLOS adminChecklistTemp()");
                    }

                    //INSERCIÓN DE CHECKUSUA
                    if (respuesta) {

                        ArrayList<ChecklistLayoutDTO> infoLayout = checklistValidaAdminBI.setInfoLayout(listaEntrante, listaPreguntas, listaProtocolos, tipoCarga);

                        logger.info("infoLayout" + infoLayout.size());

                        ArrayList<ArrayList<ChecklistLayoutDTO>> responseInsertaCheckusua = checklistValidaAdminBI.insertaCheckusua(infoLayout);

                        ArrayList<ChecklistLayoutDTO> listaCheckusuaInsertados = responseInsertaCheckusua.get(1);
                        ArrayList<ChecklistLayoutDTO> listaCheckusuaNoinsertados = responseInsertaCheckusua.get(0);

                        // Se insertaron correctamente todos los módulos
                        if (listaCheckusuaNoinsertados.size() == 0 && (listaCheckusuaInsertados.size() == infoLayout.size())) {
                            respuesta = true;
                            listaEntrante = responseInsertaCheckusua.get(1);
                            // No se insertaron todos los módulos correctamente
                        } else {
                            respuesta = false;
                        }

                    } else {
                        logger.info("NO SE INSERTA INFORMACIÓN DE CHECKUSUA adminChecklistTemp()");

                    }

                    //---------ALTA ARBOL DE DESICION-------------
                    if (respuesta) {

                        ArrayList<ChecklistLayoutDTO> listaArbol = checklistValidaAdminBI.getListaArbol(listaEntrante, tipoCarga);

                        //ArrayList<ChecklistLayoutDTO> listaArbolSalida = checklistValidaAdminBI.getListaArbol(listaEntrante, 2);
                        ArrayList<ChecklistLayoutDTO> listaArbolSalida = checklistValidaAdminBI.altaArbol(listaArbol, tipoCarga);

                        String preguntasError = "";
                        for (ChecklistLayoutDTO layout : listaArbolSalida) {
                            if (layout.getFlagErrorArbol() != 0) {
                                preguntasError = preguntasError + layout.getIdConsecutivo() + ", ";
                            }

                        }
                        if (preguntasError.equals("")) {

                            respuesta = true;

                        } else {
                            logger.info("PREGUNTAS CON ERROR AL CREAR ARBOL: " + preguntasError);
                            respuesta = false;
                        }

                    } else {
                        logger.info("NO SE INSERTA LOS ÁRBOLES adminChecklistTemp()");
                    }

                } else {
                    mv.addObject("archivoSubido", "size");
                    logger.info("El archivo" + uploadedFile.getOriginalFilename() + " es mayor a 100MB");
                }

            } else {
                mv.addObject("archivoSubido", "no");
                logger.info("El directorio " + uploadedFile.getOriginalFilename() + " no existe");
            }
            mv.addObject("ruta", fileRoute);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }

    /**
     * *****************************************************************************************************************************************************************
     */
    public String cadenaLimpia(String cadena) {
        String cadenaLimpia = "";

        char[] ca = {'\u0061', '\u0301'};
        char[] ce = {'\u0065', '\u0301'};
        char[] ci = {'\u0069', '\u0301'};
        char[] co = {'\u006F', '\u0301'};
        char[] cu = {'\u0075', '\u0301'};
        // mayusculas
        char[] c1 = {'\u0041', '\u0301'};
        char[] c2 = {'\u0045', '\u0301'};
        char[] c3 = {'\u0049', '\u0301'};
        char[] c4 = {'\u004F', '\u0301'};
        char[] c5 = {'\u0055', '\u0301'};
        char[] c6 = {'\u006E', '\u0303'};

        if (cadena.contains(String.valueOf(ca)) || cadena.contains(String.valueOf(ce))
                || cadena.contains(String.valueOf(ci)) || cadena.contains(String.valueOf(co))
                || cadena.contains(String.valueOf(cu)) || cadena.contains(String.valueOf(c1))
                || cadena.contains(String.valueOf(c2)) || cadena.contains(String.valueOf(c3))
                || cadena.contains(String.valueOf(c4)) || cadena.contains(String.valueOf(c5))
                || cadena.contains(String.valueOf(c6))) {

            String rr = cadena.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
                    .replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
                    .replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
                    .replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
                    .replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
                    .replaceAll(String.valueOf(c6), "ñ");

            cadenaLimpia = rr;
            rr = null;
        } else {
            cadenaLimpia = cadena;
        }

        return cadenaLimpia;
    }

    public List<ConsultaDistribucionDTO> ordenaLista(List<ConsultaDistribucionDTO> arrDes, String entrada) {
        List<ConsultaDistribucionDTO> arrOrdenado = new ArrayList<ConsultaDistribucionDTO>();
        for (ConsultaDistribucionDTO aux : arrDes) {
            if (aux.getIdCeco().trim().equals(entrada.trim())) {
                arrOrdenado.add(aux);
                break;
            }
        }
        for (ConsultaDistribucionDTO aux : arrDes) {
            if (!aux.getIdCeco().trim().equals(entrada.trim())) {
                arrOrdenado.add(aux);
            }
        }

        return arrOrdenado;
    }

    public List<ConsultaDTO> ordenaListaProtocolos(List<ConsultaDTO> arrDes, String entrada) {
        List<ConsultaDTO> arrOrdenado = new ArrayList<ConsultaDTO>();
        for (ConsultaDTO aux : arrDes) {
            if (String.valueOf(aux.getIdChecklist()).trim().equals(entrada)) {
                arrOrdenado.add(aux);
                break;
            }
        }
        for (ConsultaDTO aux : arrDes) {
            if (!String.valueOf(aux.getIdChecklist()).trim().equals(entrada)) {
                arrOrdenado.add(aux);
            }
        }

        return arrOrdenado;
    }

    public List<ReporteImgDTO> ordenaListaImg(List<ReporteImgDTO> arrDes, String entrada) {
        List<ReporteImgDTO> arrOrdenado = new ArrayList<ReporteImgDTO>();
        for (ReporteImgDTO aux : arrDes) {
            if (String.valueOf(aux.getIdChecklist()).trim().equals(entrada)) {
                arrOrdenado.add(aux);
                break;
            }
        }
        for (ReporteImgDTO aux : arrDes) {
            if (!String.valueOf(aux.getIdChecklist()).trim().equals(entrada)) {
                arrOrdenado.add(aux);
            }
        }

        return arrOrdenado;
    }

    @SuppressWarnings("unused")
    private class Modulo {

        private int idModulo;
        private String nomModulo;

        public Modulo(int idModulo, String nomModulo) {
            this.idModulo = idModulo;
            this.nomModulo = nomModulo;
        }

        public int getIdModulo() {
            return idModulo;
        }

        public void setIdModulo(int idModulo) {
            this.idModulo = idModulo;
        }

        public String getNomModulo() {
            return nomModulo;
        }

        public void setNomModulo(String nomModulo) {
            this.nomModulo = nomModulo;
        }

        @Override
        public String toString() {
            return "Modulos [idModulo=" + idModulo + ", nomModulo=" + nomModulo + "]";
        }

    }

    @SuppressWarnings("unused")
    private class Dia {

        private Calendar X;

        public Dia(Calendar X) {
            this.X = X;
        }

        public Calendar getX() {
            return X;
        }

        public void setX(Calendar x) {
            X = x;
        }

        @Override
        public String toString() {
            return "Dia [X=" + X + "]";
        }

    }
}
