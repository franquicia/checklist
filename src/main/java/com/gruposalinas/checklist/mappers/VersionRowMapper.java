package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.VersionDTO;

public class VersionRowMapper implements RowMapper <VersionDTO> {

	public VersionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		VersionDTO versionDTO = new VersionDTO();

		versionDTO.setIdVersion(rs.getInt("FIID_VERSION"));
		versionDTO.setDescripcion(rs.getString("FC_DESCRIPCION"));
		versionDTO.setVersion(rs.getString("FC_VERSION"));
		versionDTO.setSistema(rs.getString("FC_SISTEMA"));
		versionDTO.setSo(rs.getString("FC_SO"));
		versionDTO.setFecha(rs.getString("FDFECHA_MOD"));

		return versionDTO;
	}

}
