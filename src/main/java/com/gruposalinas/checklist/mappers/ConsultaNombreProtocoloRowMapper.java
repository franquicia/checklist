package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ConsultaCecoProtocoloDTO;

public class ConsultaNombreProtocoloRowMapper implements RowMapper<ConsultaCecoProtocoloDTO> {

	public ConsultaCecoProtocoloDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConsultaCecoProtocoloDTO consultasDTO = new ConsultaCecoProtocoloDTO();
		consultasDTO.setFechaTermino(rs.getString("FDTERMINO"));
		consultasDTO.setIdCeco(rs.getInt("FCID_CECO"));
		consultasDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		consultasDTO.setNomChecklist(rs.getString("FCNOMBRE"));
		consultasDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		return consultasDTO;
		
		
	}

}
