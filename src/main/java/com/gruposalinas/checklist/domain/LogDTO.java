package com.gruposalinas.checklist.domain;

public class LogDTO {

	private int idLog;
	private String fechaError;
	private String codigoError;
	private String mensajeError;
	private String origenError;
	
	public int getIdLog() {
		return idLog;
	}
	public void setIdLog(int idLog) {
		this.idLog = idLog;
	}
	public String getFechaError() {
		return fechaError;
	}
	public void setFechaError(String fechaError) {
		this.fechaError = fechaError;
	}
	public String getCodigoError() {
		return codigoError;
	}
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public String getOrigenError() {
		return origenError;
	}
	public void setOrigenError(String origenError) {
		this.origenError = origenError;
	}

}
