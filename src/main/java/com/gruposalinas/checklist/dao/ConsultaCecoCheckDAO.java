package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.ConsultaCecoCheckDTO;



public interface ConsultaCecoCheckDAO {

	 
		public Map<String, Object> obtieneDato() throws Exception;
		
		public Map<String, Object> obtieneDat(int idUsuario) throws Exception;
		
		public List<ConsultaCecoCheckDTO> obtieneDatos(int idUsuario) throws Exception; 
		
		public List<ConsultaCecoCheckDTO> obtieneInfo() throws Exception; 
		
	}