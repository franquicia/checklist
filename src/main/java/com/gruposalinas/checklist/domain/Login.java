package com.gruposalinas.checklist.domain;

public class Login {
	int user;
	String pass = "";
	String valida="";
		
	public String getValida() {
		return valida;
	}
	public void setValida(String valida) {
		this.valida = valida;
	}
	public int getUser() {
		return user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	public void setUser(int user) {
		this.user = user;
	}
	public Login(){
		
	}
}
