package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.PreguntaPosibleTDTO;
import com.gruposalinas.checklist.mappers.PreguntaPosibleTRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PreguntaPosibleTDAOImpl extends DefaultDAO implements PreguntaPosibleTDAO {

    private Logger logger = LogManager.getLogger(PreguntaPosibleTDAOImpl.class);

    private DefaultJdbcCall jdbcObtienePregPosTemp;
    private DefaultJdbcCall jdbcInsertaPregPosTemp;
    private DefaultJdbcCall jdbcEliminaPregPosTemp;

    public void init() {

        jdbcObtienePregPosTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPREPOSTEMP")
                .withProcedureName("SP_SEL_PP_TEMP")
                .returningResultSet("RCL_PP", new PreguntaPosibleTRowMapper());

        jdbcInsertaPregPosTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPREPOSTEMP")
                .withProcedureName("SP_INS_PP_TEMP");

        jdbcEliminaPregPosTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPREPOSTEMP")
                .withProcedureName("SP_DEL_PP_TEMP");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PreguntaPosibleTDTO> obtienePreguntasPosibleTemp(String idPregunta) throws Exception {
        Map<String, Object> out = null;
        List<PreguntaPosibleTDTO> lista = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREG", idPregunta);

        out = jdbcObtienePregPosTemp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPREPOSTEMP.SP_SEL_PP_TEMP}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_ERROR");
        ejecucion = returnEjecucion.intValue();

        lista = (List<PreguntaPosibleTDTO>) out.get("RCL_PP");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al consultar la tabla pregunta-posibles");
        } else {
            return lista;
        }

        return null;
    }

    @Override
    public boolean insertaPreguntaPosibleTemp(PreguntaPosibleTDTO bean) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPOSIPREG", bean.getIdPosiblePreg())
                .addValue("PA_IDPREGUNTA", bean.getIdPregunta())
                .addValue("PA_NUM_REV", bean.getNumeroRevision())
                .addValue("PA_TIPO_MOD", bean.getTipoCambio())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcInsertaPregPosTemp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPREPOSTEMP.SP_INS_PP_TEMP}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_ERROR");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al insertar en la tabla pregunta-posibles");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaPreguntaPosibleTemp(int idPregPos, int idPregunta) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPOSIPREG", idPregPos).addValue("PA_IDPREGUNTA", idPregunta);

        out = jdbcEliminaPregPosTemp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPREPOSTEMP.SP_DEL_PP_TEMP}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_ERROR");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al eliminar en  la tabla pregunta-posibles");
        } else {
            return true;
        }

        return false;
    }

}
