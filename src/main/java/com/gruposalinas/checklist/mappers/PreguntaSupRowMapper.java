package com.gruposalinas.checklist.mappers;

import com.gruposalinas.checklist.domain.PreguntaSupDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PreguntaSupRowMapper implements RowMapper<PreguntaSupDTO> {

    public PreguntaSupDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        PreguntaSupDTO preguntaDTO = new PreguntaSupDTO();

        preguntaDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
        preguntaDTO.setIdModulo(rs.getString("FIID_MODULO"));
        preguntaDTO.setIdTipo(rs.getString("FIID_TIPO_PREG"));
        preguntaDTO.setEstatus(rs.getString("FIESTATUS"));
        preguntaDTO.setDescPregunta(rs.getString("FCDESCRIPCION"));
        preguntaDTO.setDetalle(rs.getString("FCDETALLE"));
        preguntaDTO.setCritica(rs.getString("FICRITICA"));
        preguntaDTO.setSla(rs.getString("FCSLA"));
        preguntaDTO.setArea(rs.getString("FCAREA"));
        preguntaDTO.setNumSerie(rs.getString("FINUMSERIE"));
        preguntaDTO.setIdZona(rs.getInt("FIIDZONA"));
        preguntaDTO.setResponsable(rs.getString("FCRESPONSABLE"));
        preguntaDTO.setNegocio(rs.getString("FCNEGOCIO"));
        return preguntaDTO;
    }
}
