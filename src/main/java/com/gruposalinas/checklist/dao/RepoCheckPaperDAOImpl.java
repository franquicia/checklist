package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.RepoCheckPaperDTO;
import com.gruposalinas.checklist.mappers.RepoCheckPaperCheckRowMapper;
import com.gruposalinas.checklist.mappers.RepoCheckPaperPreguntasRowMapper;
import com.gruposalinas.checklist.mappers.RepoCheckPaperRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class RepoCheckPaperDAOImpl extends DefaultDAO implements RepoCheckPaperDAO {

    private static Logger logger = LogManager.getLogger(RepoCheckPaperDAOImpl.class);

    DefaultJdbcCall jdbcObtieneCeco;
    DefaultJdbcCall jdbcObtieneUsuario;
    DefaultJdbcCall jdbcObtieneCecoPreg;
    DefaultJdbcCall jdbcObtieneUsuarioPreg;

    public void init() {

        jdbcObtieneCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTEPAPER")
                .withProcedureName("SP_REPORTE_7SCE")
                .returningResultSet("RCL_CHECK", new RepoCheckPaperRowMapper());

        jdbcObtieneUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTEPAPER")
                .withProcedureName("SP_REPORTE_7SUS")
                .returningResultSet("RCL_CHECK", new RepoCheckPaperRowMapper());

        jdbcObtieneCecoPreg = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTEPAPER")
                .withProcedureName("SP_REP_PREG")
                .returningResultSet("RCL_CHECK", new RepoCheckPaperPreguntasRowMapper());

        jdbcObtieneUsuarioPreg = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTEPAPER")
                .withProcedureName("SP_REP_DETCHECK")
                .returningResultSet("RCL_CHECK", new RepoCheckPaperCheckRowMapper());

    }

    //va por ceco
    @SuppressWarnings("unchecked")
    public List<RepoCheckPaperDTO> obtieneDatos(String ceco, int idCheclist) throws Exception {
        Map<String, Object> out = null;
        List<RepoCheckPaperDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_CHECKL", idCheclist);

        out = jdbcObtieneCeco.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPORTEPAPER.SP_REPORTE_7SCE}");

        listaFijo = (List<RepoCheckPaperDTO>) out.get("RCL_CHECK");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la lista de Checklist del  empleado(" + ceco + ")");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //va por usuario

    @SuppressWarnings("unchecked")
    public List<RepoCheckPaperDTO> obtieneInfo(int idUsuario, int idCheclist) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<RepoCheckPaperDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_USU", idUsuario)
                .addValue("PA_CHECKL", idCheclist);

        out = jdbcObtieneUsuario.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPORTEPAPER.SP_REPORTE_7SUS}");
        //lleva el nombre del cursor del procedure
        lista = (List<RepoCheckPaperDTO>) out.get("RCL_CHECK");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la lista de Checklist del  empleado(" + idUsuario + ")");
        }
        return lista;

    }

    //va por DETALLE DE PREGUNTAS
    @SuppressWarnings("unchecked")
    public List<RepoCheckPaperDTO> obtieneDatosPreguntas(String bitacora, int idCheclist) throws Exception {
        Map<String, Object> out = null;
        List<RepoCheckPaperDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bitacora)
                .addValue("PA_CHECKL", idCheclist);

        out = jdbcObtieneCecoPreg.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPORTEPAPER.SP_REP_PREG}");

        listaFijo = (List<RepoCheckPaperDTO>) out.get("RCL_CHECK");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la lista de BITACORA del  (" + bitacora + ")");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //va por DETALLE CHECKLIST

    @SuppressWarnings("unchecked")
    public List<RepoCheckPaperDTO> obtieneInfoPreguntas(int bitacora, int idCheclist) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<RepoCheckPaperDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bitacora)
                .addValue("PA_CHECKL", idCheclist);

        out = jdbcObtieneUsuarioPreg.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPORTEPAPER.SP_REP_DETCHECK}");
        //lleva el nombre del cursor del procedure
        lista = (List<RepoCheckPaperDTO>) out.get("RCL_CHECK");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la lista de BITACORA (" + bitacora + ")");
        }
        return lista;

    }

}
