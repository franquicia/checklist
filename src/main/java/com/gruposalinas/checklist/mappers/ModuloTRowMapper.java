package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ModuloDTO;

public class ModuloTRowMapper implements RowMapper<ModuloDTO>  {
	
	public ModuloDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ModuloDTO moduloDTO = new ModuloDTO();
		
		
		moduloDTO.setIdModulo(rs.getInt("FIID_MODULO"));
		moduloDTO.setNombre(rs.getString("FCNOMBRE"));
		moduloDTO.setIdModuloPadre(rs.getInt("FIID_MOD_PADRE"));
		moduloDTO.setNombrePadre(rs.getString("FCNOMBRE_PADRE"));
		moduloDTO.setNumVersion(rs.getString("FINUMREVISION"));
		moduloDTO.setTipoCambio(rs.getString("FCTIPOMODIF"));
		
		return moduloDTO;
		
	}
}
