package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.SoftNvoDTO;
import com.gruposalinas.checklist.mappers.SoftCalulosRowMapper;
import com.gruposalinas.checklist.mappers.SoftNvoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class SoftNvoDAOImpl extends DefaultDAO implements SoftNvoDAO {

    private static Logger logger = LogManager.getLogger(SoftNvoDAOImpl.class);

    DefaultJdbcCall jdbcInsertaSoftN;
    DefaultJdbcCall jdbcEliminaSoftN;
    DefaultJdbcCall jdbcObtieneSoftN;
    DefaultJdbcCall jdbcObtieneSoftNDeta;
    DefaultJdbcCall jbdcActualizaSoftN;
    DefaultJdbcCall jdbcInsertaCalculosActa;
    DefaultJdbcCall jdbcEliminaCalculosActa;
    DefaultJdbcCall jdbcObtieneCalculosActa;
    DefaultJdbcCall jbdcActualizaCalculosActa;
    DefaultJdbcCall jdbcInsertaSoftNuevoFlujo;
    DefaultJdbcCall jdbcInsertaSoftNMtto;

    public void init() {

        jdbcInsertaSoftN = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASOFTNVOFLUJO")
                .withProcedureName("SP_INS_SOFT");

        jdbcEliminaSoftN = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASOFTNVOFLUJO")
                .withProcedureName("SP_DEL_SOFT");

        jdbcObtieneSoftNDeta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASOFTNVOFLUJO")
                .withProcedureName("SP_SEL_DETA")
                .returningResultSet("RCL_SOFT", new SoftNvoRowMapper());

        jdbcObtieneSoftN = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASOFTNVOFLUJO")
                .withProcedureName("SP_SEL_CECO")
                .returningResultSet("RCL_SOFT", new SoftNvoRowMapper());

        jbdcActualizaSoftN = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASOFTNVOFLUJO")
                .withProcedureName("SP_ACT_SOFT");

        ///calculos
        jdbcInsertaCalculosActa = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASOFTNVOFLUJO")
                .withProcedureName("SP_INS_CAL");

        jdbcEliminaCalculosActa = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASOFTNVOFLUJO")
                .withProcedureName("SP_DEL_CAL");

        jdbcObtieneCalculosActa = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASOFTNVOFLUJO")
                .withProcedureName("SP_SEL_CAL")
                .returningResultSet("RCL_SOFT", new SoftCalulosRowMapper());

        jbdcActualizaCalculosActa = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASOFTNVOFLUJO")
                .withProcedureName("SP_ACT_CAL");

        jdbcInsertaSoftNuevoFlujo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACARGAHALLATRA")
                .withProcedureName("SP_INS_SOFTNUE");

        jdbcInsertaSoftNMtto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_FLUJOSUPMTTO")
                .withProcedureName("SP_INS_SOFT");

    }

    public int insertaSoftN(SoftNvoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_FASE", bean.getIdFase())
                .addValue("PA_PROYECTO", bean.getIdProyecto())
                .addValue("PA_BITA", bean.getBitacora())
                .addValue("PA_USUARIO", bean.getIdUsuario());

        out = jdbcInsertaSoftN.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PASOFTNVOFLUJO.SP_INS_SOFTN}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_SOFT");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el SOFTN EXPANSION");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public int insertaSoftNuevoFlujoMtto(SoftNvoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_PROYECTO", bean.getIdProyecto())
                .addValue("PA_IDPROGROG", bean.getIdProgramacion())
                .addValue("PA_USUARIO", bean.getIdUsuario())
                .addValue("PA_BITACORA", bean.getBitacora());

        out = jdbcInsertaSoftNMtto.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PASOFTNVOFLUJOMTTO.SP_INS_SOFTN}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_SOFT");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el SOFTOPENING MTTO");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean eliminaSoftN(int idFirma) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_SOFT", idFirma);

        out = jdbcEliminaSoftN.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PASOFTNVOFLUJO.SP_DEL_SOFTN}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + idFirma + ")");
        } else {
            return true;
        }

        return false;

    }

    //parametro
    @SuppressWarnings("unchecked")
    public List<SoftNvoDTO> obtieneDatosSoftN(String idFirma) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<SoftNvoDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", idFirma);

        out = jdbcObtieneSoftN.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PASOFTNVOFLUJO.SP_SEL_SOFTN");
        //lleva el nombre del cursor del procedure
        lista = (List<SoftNvoDTO>) out.get("RCL_SOFT");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el empleado(" + idFirma + ")");
        }
        return lista;

    }
    //parametro web directo

    @SuppressWarnings("unchecked")
    public List<SoftNvoDTO> obtieneDatosSoftNDeta(String idFirma) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<SoftNvoDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", idFirma);

        out = jdbcObtieneSoftNDeta.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PASOFTNVOFLUJO.SP_SEL_SOFTNDetalle");
        //lleva el nombre del cursor del procedure
        lista = (List<SoftNvoDTO>) out.get("RCL_SOFT");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el empleado(" + idFirma + ")");
        }
        return lista;

    }

    public boolean actualizaSoftN(SoftNvoDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_FASE", bean.getIdFase())
                .addValue("PA_PROYECTO", bean.getIdProyecto())
                .addValue("PA_BITA", bean.getBitacora())
                .addValue("PA_USUARIO", bean.getIdUsuario())
                .addValue("PA_SOFT", bean.getIdSoft());

        out = jbdcActualizaSoftN.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PASOFTNVOFLUJO.SP_ACT_SOFTN}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  soft id: ( " + bean.getIdSoft() + ")");
        } else {
            return true;
        }
        return false;

    }

    @Override
    public int insertaCalculosActa(SoftNvoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_FASE", bean.getIdFase())
                .addValue("PA_PROYECTO", bean.getIdProyecto())
                .addValue("PA_SOFT", bean.getIdSoft())
                .addValue("PA_CLASIF", bean.getClasificacion())
                .addValue("PA_SI", bean.getItemSi())
                .addValue("PA_NO", bean.getItemNo())
                .addValue("PA_NA", bean.getItemNa())
                .addValue("PA_REVISA", bean.getItemrevisados())
                .addValue("PA_TOTITEM", bean.getItemTotales())
                .addValue("PA_TOTIMP", bean.getItemImperd())
                .addValue("PA_PORCRESPSI", bean.getPorcentajeReSi())
                .addValue("PA_PORCRESPNO", bean.getPorcentajeReNo())
                .addValue("PA_PORCSI", bean.getPorcentajeToSi())
                .addValue("PA_PORCNO", bean.getPorcentajeToNo())
                .addValue("PA_PORCNA", bean.getPorcentajeToNA())
                .addValue("PA_PONDMAXR", bean.getPonderacionMaximaReal())
                .addValue("PA_PONDOBTR", bean.getPonderacionObtenidaReal())
                .addValue("PA_PONDMAXCA", bean.getPonderacionMaximaCalculada())
                .addValue("PA_PONDOBTCA", bean.getPonderacionObtenidaCalculada())
                .addValue("PA_CALREAL", bean.getCalificacionReal())
                .addValue("PA_CALCALCU", bean.getCalificacionCalculada());

        out = jdbcInsertaCalculosActa.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PASOFTNVOFLUJO.SP_INS_CALC}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_CAL");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el Empleado");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    @Override
    public boolean eliminaCalculosActa(int idFirma) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CAL", idFirma);

        out = jdbcEliminaCalculosActa.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PASOFTNVOFLUJO.SP_DEL_AGRUPA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + idFirma + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<SoftNvoDTO> obtieneDatosCalculosActa(String idFirma) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<SoftNvoDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_SOFT", idFirma);

        out = jdbcObtieneCalculosActa.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PASOFTNVOFLUJO.SP_SEL_CalculosActa}");
        //lleva el nombre del cursor del procedure
        lista = (List<SoftNvoDTO>) out.get("RCL_SOFT");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el empleado(" + idFirma + ")");
        }
        return lista;

    }

    @Override
    public boolean actualizaCalculosActa(SoftNvoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_SOFT", bean.getIdSoft())
                .addValue("PA_CLASIF", bean.getClasificacion())
                .addValue("PA_SI", bean.getItemSi())
                .addValue("PA_NO", bean.getItemNo())
                .addValue("PA_NA", bean.getItemNa())
                .addValue("PA_REVISA", bean.getItemrevisados())
                .addValue("PA_TOTITEM", bean.getItemTotales())
                .addValue("PA_TOTIMP", bean.getItemImperd())
                .addValue("PA_PORCRESPSI", bean.getPorcentajeReSi())
                .addValue("PA_PORCRESPNO", bean.getPorcentajeReNo())
                .addValue("PA_PORCSI", bean.getPorcentajeToSi())
                .addValue("PA_PORCNO", bean.getPorcentajeToNo())
                .addValue("PA_PORCNA", bean.getPorcentajeToNA())
                .addValue("PA_PONDMAXR", bean.getPonderacionMaximaReal())
                .addValue("PA_PONDOBTR", bean.getPonderacionObtenidaReal())
                .addValue("PA_PONDOBTCA", bean.getPonderacionObtenidaCalculada())
                .addValue("PA_PONDMAXCA", bean.getPonderacionMaximaCalculada())
                .addValue("PA_CALREAL", bean.getCalificacionReal())
                .addValue("PA_CALCALCU", bean.getCalificacionCalculada())
                .addValue("PA_CAL", bean.getIdCalculo());

        out = jbdcActualizaCalculosActa.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PASOFTNVOFLUJO.SP_ACT_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  idCALCULo( " + bean.getIdCalculo() + ")");
        } else {
            return true;
        }
        return false;

    }

    public int insertaSoftNuevoFlujo(SoftNvoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_FASE", bean.getIdFase())
                .addValue("PA_PROYECTO", bean.getIdProyecto())
                .addValue("PA_BITA", bean.getBitacora())
                .addValue("PA_USUARIO", bean.getIdUsuario());

        out = jdbcInsertaSoftNuevoFlujo.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PACARGAHALLATRA.SP_INS_SOFTNUEVOFLUJO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_SOFT");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el SOFT");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }
}
