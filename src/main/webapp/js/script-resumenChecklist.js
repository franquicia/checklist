function resumenEdita(id,pendiente){
	if(pendiente == true){	
		alert('Existe un cambio sin autorizar');
	}else{
		document.getElementById("formResumenEdita"+id).submit();
	}
	
	return true;
}
function resumenElimina(elimina){
	alert('Elimina informacion ')
}

function resumenClona(clona){
	alert('Clona informacion ')
}

/*FUNCION PARA VER LOS DETALLES DE UN CHECKLIST*/
function resumenVisualiza(id){
	document.getElementById("formDetail"+id).submit();
	//document.getElementById("formDetail"+id).submit();
	return true;
}

/*FUNCION PARA VER LOS DETALLES DE CAMBIOS UN CHECKLIST*/
function resumenChangeDetail(id){
	document.getElementById("formChangeDetail"+id).submit();
	return true;
}

function resumenAsigna(id){
	document.getElementById("formAsigna"+id).submit();
	return true;
}

function resumenAlerta(alerta){
	alert('Alerta informacion ')
}

function resumenEstado(estado){
	var x = document.getElementsByClassName("tamImagenResumenActivo imaEstado"+estado);
	
	if(x[0].id==1){
		var activo=0;
		$.ajax({
			type : "GET",
			url :  urlServer+'../ajaxResumenEstado.json?varEstado='+estado+ '&varActivo=' + activo,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {
				x[0].src="../images/iconosAdmCheck/iconoDesactivado.png";
				x[0].setAttribute("id", activo);				
			},
			error : function() {
				alert('Algo paso');
			}
		});
	}
		
	else if(x[0].id==0){
		var activo=1;
		$.ajax({
			type : "GET",
			url :  urlServer+'../ajaxResumenEstado.json?varEstado='+estado+ '&varActivo=' + activo,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {
				x[0].src="../images/iconosAdmCheck/iconoActivado.png";
				x[0].setAttribute("id", activo);
			},
			error : function() {
				alert('Algo paso');
			}
		});
	}
}

/*FUNCION PARA MODIFICAR LA VISTA EN LA COMPARACION DE LOS CHECKLIST*/
function changeView(){
	
	var main = document.getElementById("mainContent");
	var up = document.getElementById("contenedorArbol1");
	var down = document.getElementById("contenedorArbol2");

	var table1 = document.getElementById("myTable");
	var table2 = document.getElementById("myTableNuevo");
	
	main.style.minWidth = "900px"

	
	if (main.style.height == "600px"){
		up.style.width = "95%";
		down.style.width = "95%";
		up.style.top = "1px"
		down.style.top = "1px"

		main.style.height = "1150px";
		//main.style.width = "100%";

		up.style.left = "49%";
		down.style.left = "49%";
		
		//table1.style.fontSize = "20px";
		//table2.style.fontSize = "20px";
		
	} else{
		up.style.width = "45%";
		down.style.width = "46%";
		down.style.top = "1px";
		up.style.top = "1px";
		up.style.float = "left";
		up.style.position = "relative";
		down.style.float = "left";
		down.style.position = "relative;"
		up.style.left = "24%";
		down.style.display = "inline-block";
		down.style.left = "24%";
			
		main.style.height = "600px";
		//main.style.width = "100%";
		table1.style.fontSize = "14px";
		table2.style.fontSize = "14px";
	}
}

/*FUNCION PARA MOSTRAR LA TABLA DE LOS DETALLES DE CHECKLIST*/
function initTable (idCheck){
	$.ajax({
		type : "GET",

		url : '../consultaChecklistService/getChecklistDetails.json?idCheck='+idCheck,

		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			var i = 1;
			var j = 0;
			var cell = -1;
			//alert (JSON.stringify(json));
			//Obtiene la cantidad de objetos que tiene el JSON
			var objectsLength = Object.keys(json["data"]).length;
			var x = document.getElementById("myTableBody");
			var y = document.getElementById("myTable");
			var resLen = json.data;
						
			for (i ; i<=objectsLength; i++){
				// Create an empty <tr> element and add it to the last position of the table:
			     var row = x.insertRow(cell);
			 	row.style.backgroundColor = "#29C19A";

				 // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
				 //var cellPregunta = row.insertCell(cell);
				 // Add some text to the new cells:
				 row.innerHTML = "<td id='pregunta'>"+i+".-	"+resLen["respuesta"+i].pregunta+"</td>";
				//Obtiene la cantidad de respuestas que tiene la pregunta en i
				var respuestas = resLen["respuesta"+i].respuestas;
				 	j = 0;
					for (j ; j<respuestas.length;j++){
						 var row1 = x.insertRow(cell);
						 var cell1 = row1.insertCell(0);
						 var cell2 = row1.insertCell(1);
						 var cell3 = row1.insertCell(2);
						 var cell4 = row1.insertCell(3);
						 var cell5 = row1.insertCell(4);
						 var cell6 = row1.insertCell(5);
						 var cell7 = row1.insertCell(6);
						 var cell8 = row1.insertCell(7);
						 
						 cell1.innerHTML = respuestas[j].Respuesta;
						 
						 if (respuestas[j].Evidencia == 0)
						 cell2.innerHTML = "NO";
						 else
							 cell2.innerHTML = "SI";
	
							 cell3.innerHTML = "Imagen";
						 
						 if (respuestas[j].EvidenciaObligatoria == 0)
							 cell4.innerHTML = "NO";
						 else
							 cell4.innerHTML = "SI";
						 
						 if (respuestas[j].Observaciones == 0)
							 cell5.innerHTML = "NO";
						 else
							 cell5.innerHTML = "SI";
						 
						 if (respuestas[j].Accion == 0)
							 cell6.innerHTML = "NO";
						 else
							 cell6.innerHTML = "SI";

						 cell7.innerHTML = respuestas[j].PreguntaSiguiente;
						 cell8.innerHTML = respuestas[j].EtiquetaEvidencia;

					}
			}
			},
		error : function() {
			alert('Algo paso al cargar los tipos de posibles respuestas');
		},
		async: false
	});
}

function initComparaTable(idCheck){
		$.ajax({
			type : "GET",

			url : '../consultaChecklistService/getChecklistCompareDetails.json?idCheck='+idCheck,

			contentType : "application/json; charset=utf-8",
			dataType : "json",
			paramName : "tagName",
			delimiter : ",",
			success : function(json) {
				//CHECKLIST ACTUAL
				var i = 1;
				var j = 0;
				var cell = -1;
				//alert (JSON.stringify(json));
				//Obtiene la cantidad de objetos que tiene el JSON
				var objectsLength = Object.keys(json["anterior"]).length;
				var x = document.getElementById("myTableBody");
				var y = document.getElementById("myTable");
				var resLen = json.anterior;
						
				for (i ; i<=objectsLength; i++){
					// Create an empty <tr> element and add it to the last position of the table:
				     var row = x.insertRow(cell);
				 	row.style.backgroundColor = "#29C19A";

					 // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
					 //var cell0 = row.insertCell(cell);
					 // Add some text to the new cells:
					 row.innerHTML = "<td id='pregunta'>"+ resLen["respuesta"+i].ordenCheckPreg+".-"+resLen["respuesta"+i].pregunta+"</td>";
					//Obtiene la cantidad de respuestas que tiene la pregunta en i
					var respuestas = resLen["respuesta"+i].respuestas;
					 	j = 0;
						for (j ; j<respuestas.length;j++){
							 var row1 = x.insertRow(cell);
							 var cell1 = row1.insertCell(0);
							 var cell2 = row1.insertCell(1);
							 var cell3 = row1.insertCell(2);
							 var cell4 = row1.insertCell(3);
							 var cell5 = row1.insertCell(4);
							 var cell6 = row1.insertCell(5);
							 var cell7 = row1.insertCell(6);
							 var cell8 = row1.insertCell(7);
							 
							 cell1.innerHTML = respuestas[j].Respuesta;
							 
							 if (respuestas[j].Evidencia == 0)
							 cell2.innerHTML = "NO";
							 else
								 cell2.innerHTML = "SI";
		
								 cell3.innerHTML = "Imagen";
							 
							 if (respuestas[j].EvidenciaObligatoria == 0)
								 cell4.innerHTML = "NO";
							 else
								 cell4.innerHTML = "SI";
							 
							 if (respuestas[j].Observaciones == 0)
								 cell5.innerHTML = "NO";
							 else
								 cell5.innerHTML = "SI";
							 
							 if (respuestas[j].Accion == 0)
								 cell6.innerHTML = "NO";
							 else
								 cell6.innerHTML = "SI";

							 cell7.innerHTML = respuestas[j].PreguntaSiguiente;
							 cell8.innerHTML = respuestas[j].EtiquetaEvidencia;
						}
					}
				
				//CHECKLIST CON LOS CAMBIOS
				var i = 1;
				var j = 0;
				var cell = -1;
				//Obtiene la cantidad de objetos que tiene el JSON
				var objectsLength = Object.keys(json["nuevo"]).length;
				var x = document.getElementById("myTableBodyNuevo");
				var y = document.getElementById("myTableNuevo");
				var resLen = json.nuevo;
							
				for (i ; i<=objectsLength; i++){
					// Create an empty <tr> element and add it to the last position of the table:
				     var row = x.insertRow(cell);
				     var cellPregunta = row.insertCell(0);
				     cellPregunta.style.backgroundColor = "#29C19A";
				     cellPregunta.style.color = "white";
				     cellPregunta.style.textAlign = "left";
					 var cellEstatus = row.insertCell(1);
					 cellPregunta.colSpan = "8";
					 // Add some text to the new cells:
					 cellPregunta.innerHTML = "<td id='pregunta'>"+resLen["respuesta"+i].ordenCheckPreg+".-"+resLen["respuesta"+i].pregunta+"</td>";
					 // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
					 //var cell0 = row.insertCell(cell);
					 // Add some text to the new cells:
					 //row.innerHTML = i +".-"+resLen["respuesta"+i].pregunta;
					//Obtiene la cantidad de respuestas que tiene la pregunta en i
					var respuestas = resLen["respuesta"+i].respuestas;
					 	j = 0;
						for (j ; j<respuestas.length;j++){
							var cambio =	"";

							 var row1 = x.insertRow(cell);
							 var cell1 = row1.insertCell(0);
							 var cell2 = row1.insertCell(1);
							 var cell3 = row1.insertCell(2);
							 var cell4 = row1.insertCell(3);
							 var cell5 = row1.insertCell(4);
							 var cell6 = row1.insertCell(5);
							 var cell7 = row1.insertCell(6);
							 var cell8 = row1.insertCell(7);
							 var cell9 = row1.insertCell(8);

							 cell1.innerHTML = respuestas[j].Respuesta;
							 
							 if (respuestas[j].Evidencia == 0){
							 cell2.innerHTML = "NO";
							 //row1.style.backgroundColor = "yellow"; 
							 }
							 else
								 cell2.innerHTML = "SI";
								 cell3.innerHTML = "Imagen";
							 
							 if (respuestas[j].EvidenciaObligatoria == 0)
								 cell4.innerHTML = "NO";
							 else
								 cell4.innerHTML = "SI";
							 
							 if (respuestas[j].Observaciones == 0)
								 cell5.innerHTML = "NO";
							 else
								 cell5.innerHTML = "SI";
							 
							 if (respuestas[j].Accion == 0)
								 cell6.innerHTML = "NO";
							 else
								 cell6.innerHTML = "SI";

							 cell7.innerHTML = respuestas[j].PreguntaSiguiente;
							 cell8.innerHTML = respuestas[j].EtiquetaEvidencia;
							 
								 m = respuestas[j].ModificacionCheckPreg;
								 cambio +="CheckPreg-"+respuestas[j].ModificacionCheckPreg;
								 	if (respuestas[j].ModificacionCheckPreg != "D"){
							 
												 if (respuestas[j].ModificacionCheck != "S/C"){
													 m = respuestas[j].ModificacionCheck;
													 if (m == "I")
															document.getElementById("generalesDataNuevo").style.backgroundColor = "#b3e6b3";
													 	else if (m == "D")
															document.getElementById("generalesDataNuevo").style.backgroundColor = "#ff8080";
													 	else if (m == "U"){
															document.getElementById("generalesDataNuevo").style.backgroundColor = "#ffff80";
													 	}
															
												 } else{
													 //cambio += "Modificacion Check:"+respuestas[j].ModificacionCheck+"\n";
												 }
												 if (respuestas[j].ModificacionPreg != "S/C"){
													 m = respuestas[j].ModificacionPreg;
													 	if (m == "I"){
													 		cellEstatus.style.backgroundColor = "#b3e6b3";
													 		//cellPregunta.style.backgroundColor = "#b3e6b3";
													 		cellEstatus.innerHTML= "Agregado";
													 	}
													 	else if (m == "D"){
													 		cellEstatus.style.backgroundColor = "#ff8080";
													 		//cellPregunta.style.backgroundColor = "#ff8080";
													 		cellEstatus.innerHTML= "Eliminado";
													 	}
													 	else if (m == "U"){
													 		cellEstatus.style.backgroundColor = "#ffff80";
													 		//cellPregunta.style.backgroundColor = "#ffff80";
													 		cellEstatus.innerHTML= "Actualizado";
													 	}
												 } else{
											 		cellEstatus.style.backgroundColor = "white";
											 		//cambio += "Modificacion Preg:"+respuestas[j].ModificacionPreg+"\n";
												 }
												 if (respuestas[j].ModificacionModulo != "S/C"){
													 m = respuestas[j].ModificacionModulo;
													 //cambio +="Modificacion Modulo:"+respuestas[j].ModificacionModulo+"\n";
												 }
												if (respuestas[j].ModificacionArbol != "S/C"){
													 m = respuestas[j].ModificacionArbol;
													 if (m == "I"){
													 		//row1.style.backgroundColor = "#b3e6b3";
													 		cell9.style.backgroundColor = "#b3e6b3";
													 		cell9.innerHTML= "Agregado";
						
													 	}
													 	else if (m == "D"){
													 		cell9.style.backgroundColor = "#ff8080";
													 		//row1.style.backgroundColor = "#ff8080";
													 		cell9.innerHTML= "Eliminado";
													 	}
													 	else if (m == "U"){
													 		cell9.style.backgroundColor = "#ffff80";
													 		//row1.style.backgroundColor = "#ffff80";
													 		cell9.innerHTML= "Actualizado";
													 	}
												 }
								 	}
								 	else {
								 		cellEstatus.style.backgroundColor = "#ff8080";
								 		//cellPregunta.style.backgroundColor = "#ff8080";
								 		cellEstatus.innerHTML= "Eliminado";
								 	}
						}
					}
				
				var obj = json["datosGeneralesActual"];
				var fecha = "";
				if (obj.fechaFin != null)
					fecha = "<br> Fecha Fin: " + obj.fechaFin;
					else
					fecha = "<br> Fecha Fin: No Definida";
				var data = "Nombre:	"+obj.nombreCheck;
				//+ "<br> Tipo Check: " + obj.idTipoChecklist + "<br> Horario: " + obj.idHorario + "<br> Fecha Inicio: " + obj.fechaInicio + fecha;
				document.getElementById("generalesData").innerHTML = data;
				
				var obj1 = json["datosGeneralesNuevo"];
				var fecha = "";
				if (obj1.fechaFin != null)
					fecha = "<br> Fecha Fin: " + obj1.fechaFin;
					else
					fecha = "<br> Fecha Fin: No Definida";
				var data = "Nombre:	"+obj1.nombreCheck;
				//+ "<br> Tipo Check: " + obj1.idTipoChecklist + "<br> Horario: " + obj1.idHorario + "<br> Fecha Inicio: " + obj.fechaInicio + fecha;
				document.getElementById("generalesDataNuevo").innerHTML = data;
				
				
				},
			error : function() {
				alert('Algo paso al cargar los tipos de posibles respuestas');
			},
			async: false
		});
}