package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.ReporteImgDTO;

public class ReporteGetSucursalesRowMapper implements RowMapper<ReporteImgDTO> {

	public ReporteImgDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteImgDTO consultaDTO = new ReporteImgDTO();
		consultaDTO.setCeco(rs.getString("CECO"));
		consultaDTO.setSucursal(rs.getString("SUCURSAL"));
		
		return consultaDTO;
		
		
	}

}
