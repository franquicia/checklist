package com.gruposalinas.checklist.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.jfree.data.general.DefaultPieDataset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.AdicionalesBI;
import com.gruposalinas.checklist.business.CalculaDatosExpansion;
import com.gruposalinas.checklist.business.ChecklistPreguntasComBI;
import com.gruposalinas.checklist.business.ConteoGenExpBI;
import com.gruposalinas.checklist.business.CorreoBI;
import com.gruposalinas.checklist.business.ExpansionActa2BI;
import com.gruposalinas.checklist.business.ExpansionActaBI;
import com.gruposalinas.checklist.business.FirmaCheckBI;
import com.gruposalinas.checklist.business.GeneraExcelDetalleExpansionBI;
import com.gruposalinas.checklist.business.HallazgosEviExpBI;
import com.gruposalinas.checklist.business.HallazgosExpBI;
import com.gruposalinas.checklist.business.HallazgosRepoBI;
import com.gruposalinas.checklist.business.HallazgosTransfBI;
import com.gruposalinas.checklist.business.PerfilUsuarioBI;
import com.gruposalinas.checklist.business.RepoFilExpBI;
import com.gruposalinas.checklist.business.ReporteChecksExpBI;
import com.gruposalinas.checklist.business.SucursalChecklistBI;
import com.gruposalinas.checklist.domain.AdicionalesDTO;
import com.gruposalinas.checklist.domain.ChecklistHallazgosRepoDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.ConteoGenExpDTO;
import com.gruposalinas.checklist.domain.ExpansionContadorDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.util.StrCipher;
import com.gruposalinas.checklist.util.UtilCryptoGS;
import com.gruposalinas.checklist.util.UtilObject;
import com.gruposalinas.checklist.util.UtilResource;
import com.itextpdf.text.DocumentException;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;
import com.gruposalinas.checklist.domain.HallazgosEviExpDTO;
import com.gruposalinas.checklist.domain.HallazgosExpDTO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;
import com.gruposalinas.checklist.domain.PerfilUsuarioDTO;
import com.gruposalinas.checklist.domain.RepoFilExpDTO;
import com.gruposalinas.checklist.domain.ReporteChecksExpDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.domain.ZonaNegoExpDTO;

@Controller
public class ExpansionController {
	private static final Logger logger = LogManager.getLogger(ExpansionController.class);

	@Autowired
	ChecklistPreguntasComBI checklistPreguntasComBI;

	@Autowired
	FirmaCheckBI firmaCheckBI;

	@Autowired
	SucursalChecklistBI sucursalChecklistBI;

	@Autowired
	RepoFilExpBI repoFilExpBI;

	@Autowired
	HallazgosExpBI hallazgosExpBI;

	@Autowired
	AdicionalesBI adicionalesExpBI;

	@Autowired
	HallazgosEviExpBI hallazgosEviExpBI;

	@Autowired
	HallazgosRepoBI hallazgosRepoBI;

	@Autowired
	CorreoBI correobi;

	@Autowired
	ConteoGenExpBI conteoGenExpBI;

	@Autowired
	PerfilUsuarioBI perfilUsuarioBI;
	
	@Autowired
	HallazgosTransfBI hallazgosTransfBI;
	
	@Autowired
	ReporteChecksExpBI reporteChecksBI;

	@RequestMapping(value = { "central/detalleChecklistExpancionGeneral.htm" }, method = RequestMethod.GET)
	public ModelAndView detalleChecklistExpancion(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tipo", required = true, defaultValue = "0") String tipo,
			@RequestParam(value = "idBitacora", required = true, defaultValue = "0") Integer idBitacora, Model model)
			throws ServletException, IOException {

		ModelAndView mv = new ModelAndView("detalleChecklistExpancion");
		String clasif = "";
		String imagenDetalle = "";
		String titulo = "";

		switch (tipo) {
		case "AREAS":
			imagenDetalle = "zonaAreasComunes";
			titulo = "Áreas Comunes";
			clasif = "AREAS";
			break;
		case "BAZ":
			imagenDetalle = "zonaBAZ";
			titulo = "Banco Azteca";
			clasif = "BAZ";
			break;
		case "EKT":
			imagenDetalle = "zonaElektra";
			titulo = "Elektra";
			clasif = "EKT";
			break;
		case "GENERALES":
			imagenDetalle = "zonaGenerales";
			titulo = "Generales";
			clasif = "GENERALES";
			break;
		case "PP":
			imagenDetalle = "zonaPrestaPrenda";
			titulo = "Presta Prenda";
			clasif = "PP";
			break;
		case "CYC":
			imagenDetalle = "zonaBAZ";
			titulo = "Credito y Cobranza";
			clasif = "CYC";
			break;

		default:
			imagenDetalle = "";// totales
			titulo = "Total";
			clasif = "";
			break;
		}

		List<ChecklistPreguntasComDTO> chklistPreguntasImp = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> chklistPreguntasZonas = new ArrayList<ChecklistPreguntasComDTO>();
		chklistPreguntasImp.clear();
		chklistPreguntasImp = checklistPreguntasComBI.obtieneInfoNoImp(idBitacora);

		// Concatenado
		List<ChecklistPreguntasComDTO> listaImperdonablesConcatenadas = getHallazgosConcatenados(chklistPreguntasImp);

		if (chklistPreguntasImp != null) {
			for (int i = 0; i < chklistPreguntasImp.size(); i++) {
				if (chklistPreguntasImp.size() == 1) {
					if (chklistPreguntasImp.get(i).getPregunta() == null) {
						chklistPreguntasImp.clear();
						chklistPreguntasImp.clear();
						break;
					}
				}

				if (imagenDetalle == "") {
					chklistPreguntasImp.add(chklistPreguntasImp.get(i));

				} else {
					if (chklistPreguntasImp.get(i).getClasif().equals(clasif)) {
						chklistPreguntasZonas.add(chklistPreguntasImp.get(i));
					}
				}

			}
		}

		mv.addObject("chklistPreguntasImp", chklistPreguntasZonas);
		mv.addObject("titulo", titulo);
		mv.addObject("imagenDetalle", imagenDetalle);

		return mv;
	}

	@RequestMapping(value = { "central/detalleChecklistExpancion.htm" }, method = RequestMethod.GET)
	public ModelAndView detalleChecklistExpancion(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tipo", required = true, defaultValue = "0") Integer tipo,
			@RequestParam(value = "idBitacora", required = true, defaultValue = "0") Integer idBitacora, Model model)
			throws ServletException, IOException {

		ModelAndView mv = new ModelAndView("detalleChecklistExpancion");
		String clasif = "";
		String imagenDetalle = "";
		String titulo = "";

		switch (tipo) {
		case 1:
			imagenDetalle = "zonaAreasComunes";
			titulo = "Áreas Comunes";
			clasif = "AREAS";
			break;
		case 2:
			imagenDetalle = "zonaBAZ";
			titulo = "Banco Azteca";
			clasif = "BAZ";
			break;
		case 3:
			imagenDetalle = "zonaElektra";
			titulo = "Elektra";
			clasif = "EKT";
			break;
		case 4:
			imagenDetalle = "zonaGenerales";
			titulo = "Generales";
			clasif = "GENERALES";
			break;
		case 5:
			imagenDetalle = "zonaPrestaPrenda";
			titulo = "Presta Prenda";
			clasif = "PP";
			break;
		case 6:
			imagenDetalle = "zonaBAZ";
			titulo = "Credito y Cobranza";
			clasif = "CYC";
			break;

		default:
			imagenDetalle = "";// totales
			titulo = "Total";
			clasif = "";
			break;
		}

		List<ChecklistPreguntasComDTO> chklistPreguntasImp = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> chklistPreguntasZonas = new ArrayList<ChecklistPreguntasComDTO>();
		chklistPreguntasImp.clear();
		chklistPreguntasImp = checklistPreguntasComBI.obtieneInfoNoImp(idBitacora);

		// Concatenado
		List<ChecklistPreguntasComDTO> listaImperdonablesConcatenadas = getHallazgosConcatenados(chklistPreguntasImp);

		if (listaImperdonablesConcatenadas != null) {
			for (int i = 0; i < listaImperdonablesConcatenadas.size(); i++) {
				if (listaImperdonablesConcatenadas.size() == 1) {
					if (listaImperdonablesConcatenadas.get(i).getPregunta() == null) {
						listaImperdonablesConcatenadas.clear();
						chklistPreguntasImp.clear();
						break;
					}
				}

				if (imagenDetalle == "") {
					chklistPreguntasImp.add(listaImperdonablesConcatenadas.get(i));

				} else {
					if (listaImperdonablesConcatenadas.get(i).getClasif().equals(clasif)) {
						chklistPreguntasZonas.add(listaImperdonablesConcatenadas.get(i));
					}
				}

			}
		}

		mv.addObject("chklistPreguntasImp", chklistPreguntasZonas);
		mv.addObject("titulo", titulo);
		mv.addObject("imagenDetalle", imagenDetalle);

		return mv;
	}

	@RequestMapping(value = { "central/detalleChecklistExpancionImperdonable.htm" }, method = RequestMethod.GET)
	public ModelAndView detalleChecklistExpancionImperdonable(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idBitacora", required = true, defaultValue = "0") Integer idBitacora, Model model)
			throws ServletException, IOException {

		List<ChecklistPreguntasComDTO> chklistPreguntasImp = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> listaResult = new ArrayList<ChecklistPreguntasComDTO>();

		listaResult.clear();
		chklistPreguntasImp = checklistPreguntasComBI.obtieneInfoImp(idBitacora);

		// Concatenado
		for (int i = 0; i < chklistPreguntasImp.size(); i++) {

			if (chklistPreguntasImp.get(i).getPregPadre() == 0 && chklistPreguntasImp.get(i).getIdcritica() == 1) {

				for (int j = 0; j < chklistPreguntasImp.size(); j++) {

					if (chklistPreguntasImp.get(i).getIdPreg() == chklistPreguntasImp.get(j).getPregPadre()) {

						ChecklistPreguntasComDTO data = chklistPreguntasImp.get(j);
						data.setPregunta(chklistPreguntasImp.get(i).getPregunta() + " : "
								+ chklistPreguntasImp.get(j).getPregunta());
						listaResult.add(data);
					}
				}
			}
		}

		ModelAndView mv = new ModelAndView("detalleChecklistExpancionImperdonable");

		mv.addObject("chklistPreguntasImp", listaResult);
		mv.addObject("numImperdonables", listaResult.size());

		return mv;
	}

	@RequestMapping(value = { "central/detalleChecklistExpancionImperdonableGeneral.htm" }, method = RequestMethod.GET)
	public ModelAndView detalleChecklistExpancionImperdonableGeneral(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "idBitacora", required = true, defaultValue = "0") Integer idBitacora, Model model)
			throws ServletException, IOException {

		List<ChecklistPreguntasComDTO> chklistPreguntasImp = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> listaResult = new ArrayList<ChecklistPreguntasComDTO>();

		listaResult.clear();
		chklistPreguntasImp = checklistPreguntasComBI.obtieneInfoImp(idBitacora);

		// Concatenado
		for (int i = 0; i < chklistPreguntasImp.size(); i++) {
			if (chklistPreguntasImp.get(i).getPregPadre() == 0 && chklistPreguntasImp.get(i).getIdcritica() == 1) {
				ChecklistPreguntasComDTO data = chklistPreguntasImp.get(i);
				listaResult.add(data);
			}
		}

		ModelAndView mv = new ModelAndView("detalleChecklistExpancionImperdonable");

		mv.addObject("chklistPreguntasImp", listaResult);
		mv.addObject("numImperdonables", listaResult.size());

		return mv;
	}

	@RequestMapping(value = { "central/menuActa.htm" }, method = RequestMethod.GET)
	public ModelAndView menuActa(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idBitacora", required = true, defaultValue = "0") Integer idBitacora, Model model)
			throws ServletException, IOException {

		List<String> listaAreas = new ArrayList<String>();
		listaAreas.add("ELEKTRA");
		listaAreas.add("BANCO AZTECA");
		listaAreas.add("PRESTA PRENDA");
		listaAreas.add("ÁREAS COMUNES");
		listaAreas.add("GENERALES");
		listaAreas.add("CREDITO Y COBRANZA");

		ModelAndView mv = new ModelAndView("menuActa");
		mv.addObject("titulo", "Da click consultar los detalles de cada zona.");
		mv.addObject("idBitacora", idBitacora);
		mv.addObject("listaAreas", listaAreas);

		return mv;
	}

	@RequestMapping(value = { "central/detalleGrupoChecklist.htm" }, method = RequestMethod.GET)
	public ModelAndView detalleGrupoChecklist(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idUsuario", required = true, defaultValue = "0") Integer usuario,
			@RequestParam(value = "opcion", required = true, defaultValue = "0") Integer opcion,
			@RequestParam(value = "opcion2", required = true, defaultValue = "0") Integer opcion2,
			@RequestParam(value = "idBitacora", required = true, defaultValue = "0") Integer idBitacora, Model model)
			throws ServletException, IOException {

		String clasif = "";
		String imagenDetalle = "";
		String titulo = "";

		switch (opcion) {
		case 4:
			imagenDetalle = "zonaAreasComunes";
			titulo = "Áreas Comunes";
			clasif = "AREAS";
			break;
		case 2:
			imagenDetalle = "zonaBAZ";
			titulo = "Banco Azteca";
			clasif = "BAZ";
			break;
		case 1:
			imagenDetalle = "zonaElektra";
			titulo = "Elektra";
			clasif = "EKT";
			break;
		case 5:
			imagenDetalle = "zonaGenerales";
			titulo = "Generales";
			clasif = "GENERALES";
			break;
		case 6:
			imagenDetalle = "zonaBAZ";
			titulo = "Credito y Cobranza";
			clasif = "CYC";
			break;
		case 3:
			imagenDetalle = "zonaPrestaPrenda";
			titulo = "Presta Prenda";
			clasif = "PP";
			break;

		default:
			imagenDetalle = "zonaElektra";
			titulo = "Elektra";
			clasif = "EKT";
			break;
		}
		ModelAndView mv = new ModelAndView("detalleGrupoChecklist");

		List<ChecklistPreguntasComDTO> listaPreguntas = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> lista = new ArrayList<ChecklistPreguntasComDTO>();

		double calif = 0.0;
		listaPreguntas.clear();
		lista.clear();

		listaPreguntas = checklistPreguntasComBI.obtieneDatosGenales(idBitacora + "");

		if (listaPreguntas != null) {

			for (int i = 0; i < listaPreguntas.size(); i++) {
				if (listaPreguntas.size() == 1) {
					if (listaPreguntas.get(i).getPregunta() == null) {
						listaPreguntas.clear();
						break;
					}
				}

				if (listaPreguntas.get(i).getClasif().equals(clasif)) {
					logger.info(listaPreguntas.get(i).getPregPadre());
					logger.info(listaPreguntas.get(i).getCalif());
					lista.add(listaPreguntas.get(i));
					calif = listaPreguntas.get(i).getPrecalif();
				}

			}
		}

		DecimalFormat formatDecimales = new DecimalFormat("###.##");
		String calificacionReal = formatDecimales.format(calif / 100);

		mv.addObject("listaPreguntas", lista);
		mv.addObject("calificacion", calificacionReal);
		mv.addObject("imagenDetalle", imagenDetalle);
		mv.addObject("titulo", titulo);
		mv.addObject("idBitacora", idBitacora);
		return mv;
	}

	@RequestMapping(value = { "central/detalleChecklistExpancionFirmas.htm" }, method = RequestMethod.GET)
	public ModelAndView detalleChecklistExpancionFirmas(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idBitacora", required = true, defaultValue = "0") Integer idBitacora, Model model)
			throws ServletException, IOException {

		List<FirmaCheckDTO> firmasB = new ArrayList<FirmaCheckDTO>();

		firmasB.clear();
		firmasB = firmaCheckBI.obtieneDatos(idBitacora + "");
		if (firmasB != null) {
			if (firmasB.size() == 1) {
				if (firmasB.get(0).getBitacora() == null) {
					firmasB.clear();
				}
			}
		}

		String firmasHtml = "";
		int contadorReiniciador = 0;
		// Lleno la tabla de Firmas
		if (firmasB != null) {
			File r = null;
			String rootPath = File.listRoots()[0].getAbsolutePath();
			firmasHtml += "<table id=\"tabla\" class=\"display\" cellspacing=\"0\" width=\"50%\">" + "<tbody>";
			for (int i = 0; i < firmasB.size(); i++) {
				FirmaCheckDTO obj = firmasB.get(i);
				if (contadorReiniciador == 0 || contadorReiniciador == 3) {
					if (contadorReiniciador == 3) {
						firmasHtml += "</tr>";
						contadorReiniciador = 0;
					}
					firmasHtml += "<tr style=\"background-color: white;\">";
				}
				String encodedA = null;
				if (obj.getRuta() != null) {
					Path pdfPath = Paths.get(obj.getRuta());
					try {
						byte[] firma = Files.readAllBytes(pdfPath);

						encodedA = new String(Base64.encodeBase64(firma), "UTF-8");
					} catch (NoSuchFileException e) {
						logger.info("No se encontro Firma" + obj.getRuta());
					}
				}

				// firmasHtml += "<td style='padding: 5%; border: none;'> <b>" + obj.getPuesto()
				// + "</b><br></br>" + "Nombre: " + obj.getResponsable()
				// + "<br></br>" + "<br></br>" + "<img src='data:image/png;base64," + encodedA +
				// "' width='200' height='100'/>" + "</td>";

				firmasHtml += "<td style=\"text-align: center; border: none; padding-top: 50px; width: 30%;\"> <strong>"
						+ obj.getPuesto() + "</strong>" + "<br>" + "<img src='data:image/png;base64," + encodedA
						+ "' style=\"width: 30%;height: 30%;\"/>" + "<hr> " + "<label style=\"font-size: 100%;\">"
						+ obj.getResponsable() + "</label>" + "</td>";
				contadorReiniciador++;
			}
			firmasHtml += "</tbody>" + "</table>";
		}

		ModelAndView mv = new ModelAndView("detalleChecklistExpancionFirmas");
		mv.addObject("firmasHtml", firmasHtml);
		// mv.addObject("firmasB", firmasB);

		return mv;
	}

	@RequestMapping(value = { "central/filtrosPadresExpansion.htm" }, method = RequestMethod.GET)
	public ModelAndView filtrosPadresExpansion(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idUsuario", required = true, defaultValue = "0") Integer usuario,
			@RequestParam(value = "zona", required = true, defaultValue = "0") Integer zona,
			@RequestParam(value = "idRecorrido", required = true, defaultValue = "0") Integer idRecorrido,
			@RequestParam(value = "datos", required = true) List<String> datos,
			@RequestParam(value = "ceco", required = true, defaultValue = "0") Integer ceco, Model model)
			throws ServletException, IOException {

		List<String> dato = new ArrayList<>();
		dato.clear();
		String urlBack = "";
		String[] urlBuild = { "busqueda=", "idCeco=", "nombreCeco=", "tipoRecorrido=", "estatusVisita=", "fechaInicio=",
				"fechaFin=", "fechaInicioParam=", "fechaFinParam=" };
		String[] zonas = { "", "", "", "", "", "" };
		String clasif = "";
		String[] sucursal = new String[8];

		// Variables para armar la tabla
		int[] recorrido = { 0, 0 };
		String[] respuestas = { "", "" };
		int[] idBit = { 0, 0 };
		String tabla = "";
		String fechaRecorrido1 = "";
		String fechaRecorrido2 = "";
		List<SucursalChecklistDTO> sucursal2 = sucursalChecklistBI.obtieneDatosSuc(ceco.toString());
		
		
		String tipoNegocio = sucursal2.get(0).getNegocio();
		urlBack = "seleccionFiltroExpansion.htm?";
		for (int i = 0; i < datos.size(); i++) {
			if (i == 0) {
				urlBack += urlBuild[i] + datos.get(i).replace("[", "").replace("]", "");
			} else {
				urlBack += "&" + urlBuild[i] + datos.get(i).replace("[", "").replace("]", "");
			}
			dato.add(datos.get(i).replace("[", "").replace("]", ""));

		}

		switch (zona) {
		case 1: // ekt
			zonas[zona - 1] = "class=\"tab-active\"";
			clasif = "EKT";
			break;
		case 2: // baz
			zonas[zona - 1] = "class=\"tab-active\"";
			clasif = "BAZ";
			break;
		case 3: // pp
			zonas[zona - 1] = "class=\"tab-active\"";
			clasif = "PP";
			break;
		case 4: // areas comunes
			zonas[zona - 1] = "class=\"tab-active\"";
			clasif = "AREAS";
			break;
		case 5: // generales
			zonas[zona - 1] = "class=\"tab-active\"";
			clasif = "GENERALES";
			break;
		case 6: // CYC
			zonas[zona - 1] = "class=\"tab-active\"";
			clasif = "CYC";
			break;
		default: // ekt
			zonas[0] = "class=\"tab-active\"";
			clasif = "EKT";
			break;
		}

		// Se realiza la consulta de las preguntas y respuestas
		List<RepoFilExpDTO> chklistPreguntas = new ArrayList<RepoFilExpDTO>();
		List<RepoFilExpDTO> chklistrespuestas = new ArrayList<RepoFilExpDTO>();
		List<RepoFilExpDTO> sucursales = new ArrayList<RepoFilExpDTO>();
		chklistPreguntas.clear();
		chklistrespuestas.clear();

		if (ceco != 0 && idRecorrido != 0) {
			chklistPreguntas = repoFilExpBI.obtienePreguntas("" + ceco);
			chklistrespuestas = repoFilExpBI.obtieneInfo("" + ceco, "" + idRecorrido);
			sucursales = repoFilExpBI.obtieneDatosGen("" + ceco, "", "", "", "" + idRecorrido);

			// Se arma la tabla si existen preguntas
			if (chklistPreguntas != null) {
				String bodytable = "";
				int index = 0;
				for (int i = 0; i < chklistPreguntas.size(); i++) {
					if (chklistPreguntas.get(i).getClasif().equals(clasif)) {
						if (chklistrespuestas != null) {
							for (int j = 0; j < chklistrespuestas.size(); j++) {
								if (chklistrespuestas.get(j).getIdPreg() == chklistPreguntas.get(i).getIdPreg()) {
									if (idRecorrido == 1) {
										if (chklistrespuestas.get(j).getIdRecorrido().equals("1")) {
											if (fechaRecorrido1 == "")
												fechaRecorrido1 = chklistrespuestas.get(j).getFechaTermino();
											recorrido[0] = 1;
											respuestas[0] = chklistrespuestas.get(j).getPosible();
											idBit[0] = chklistrespuestas.get(j).getBitGral();
										}
									}
									if (idRecorrido > 1) {
										if (chklistrespuestas.get(j).getIdRecorrido().equals("1")) {
											if (fechaRecorrido1 == "")
												fechaRecorrido1 = chklistrespuestas.get(j).getFechaTermino();
											recorrido[0] = 1;
											respuestas[0] = chklistrespuestas.get(j).getPosible();
											idBit[0] = chklistrespuestas.get(j).getBitGral();
										}
										if (chklistrespuestas.get(j).getIdRecorrido().equals("2")) {
											if (fechaRecorrido2 == "")
												fechaRecorrido2 = chklistrespuestas.get(j).getFechaTermino();
											recorrido[1] = 1;
											respuestas[1] = chklistrespuestas.get(j).getPosible();
											idBit[1] = chklistrespuestas.get(j).getBitGral();

										}
									}

								}
							}

						}
						if (recorrido[0] == 0) {
							respuestas[0] = "S/R";
						}
						if (recorrido[1] == 0) {
							respuestas[1] = "S/R";
						}
						bodytable += "                            	<tr>" + "								<td>"
								+ (++index) + ". " + chklistPreguntas.get(i).getPregunta() + "</td>";
						if (idRecorrido > 1) {
							bodytable += "								<td><b>" + respuestas[0] + "</b></td>"
									+ "								<td><b>" + respuestas[1] + "</b></td>"
									+ "                                <td>";
						}
						if (idRecorrido == 1) {
							bodytable += "								<td><b>" + respuestas[0] + "</b></td>"
									+ "                                <td>";
						}

						if (recorrido[1] != 0 || recorrido[0] != 0) {
							bodytable += "								   <form name=\"formulario\" method=\"POST\" action=\"../central/filtrosHijasExpansion.htm\"  style=\"height: 18px;\">"
									+ "										<input type=\"hidden\" value = \""
									+ idBit[0] + "\" name=\"idBit1\" />"
									+ "										<input type=\"hidden\" value = \""
									+ idBit[1] + "\" name=\"idBit2\" />"
									+ "										<input type=\"hidden\" value = \""
									+ chklistPreguntas.get(i).getIdPreg() + "\" name=\"pregFilt\" />"
									+ "										<input type=\"hidden\" value = \"" + (index)
									+ "\" name=\"numPregFilt\" />"
									+ "										<input type=\"hidden\" value = \"" + usuario
									+ "\" name=\"idUsuario\" />"
									+ "										<input type=\"hidden\" value = \"" + zona
									+ "\" name=\"zona\" />"
									+ "										<input type=\"hidden\" value = \""
									+ idRecorrido + "\" name=\"idRecorrido\" />"
									+ "										<input type=\"hidden\" value = \""
									+ chklistPreguntas.get(i).getPregunta() + "\" name=\"detallePregunta\" />"
									+ "										<input type=\"hidden\" value = \""
									+ respuestas[0] + "\" name=\"detalleResPregunta1\" />"
									+ "										<input type=\"hidden\" value = \""
									+ respuestas[1] + "\" name=\"detalleResPregunta2\" />"
									+ "										<input type=\"hidden\" value = \""
									+ fechaRecorrido1 + "\" name=\"fechaRecorrido1\" />"
									+ "										<input type=\"hidden\" value = \""
									+ fechaRecorrido2 + "\" name=\"fechaRecorrido2\" />"
									+ "										<input type=\"hidden\" value = \"" + ceco
									+ "\" name=\"ceco\" />"
									+ "										<input type=\"image\" src=\"../images/expancion/imgticket.png\" class=\"btnhija\">"
									+ "									</form>";

						} else {
							bodytable += "								<input type=\"image\" src=\"../images/expancion/imgticket.png\" class=\"btnhija\">";
						}
						bodytable += "								</td>" + "							</tr>";
					}

					recorrido[0] = 0;
					recorrido[1] = 0;
					idBit[0] = 0;
					idBit[1] = 0;
					respuestas[0] = "";
					respuestas[1] = "";
				}
				if (idRecorrido == 1) {
					tabla = "<table id=\"tabla\" class=\"tablaChecklist\">" + "						<thead>"
							+ "							<tr>" + "								<th><b>Items</b></th>"
							+ "								<th><b>Visita 1 " + fechaRecorrido1 + "</b></th>"
							+ "								<th><b>Evidencias</b></th>"
							+ "							</tr>" + "						</thead>"
							+ "						<tbody>" + bodytable + "                        </tbody>"
							+ "</table>";
				}
				if (idRecorrido > 1) {
					tabla = "<table id=\"tabla\" class=\"tablaChecklist\">" + "						<thead>"
							+ "							<tr>" + "								<th><b>Items</b></th>"
							+ "								<th><b>Visita 1 " + fechaRecorrido1 + "</b></th>"
							+ "								<th><b>Visita 2 " + fechaRecorrido2 + "</b></th>"
							+ "								<th><b>Evidencias</b></th>"
							+ "							</tr>" + "						</thead>"
							+ "						<tbody>" + bodytable + "                        </tbody>"
							+ "</table>";
				}
			}

			// Se agregan datos para mostrar la informacion de la sucursal
			if (sucursales != null && !sucursales.isEmpty()) {
				DecimalFormat formatDecimales = new DecimalFormat("###.##");

				sucursal[0] = sucursales.get(0).getNombreCeco(); // NOMBRE SUCURSAL
				sucursal[1] = "" + ceco; // CECO
				sucursal[2] = "" + sucursales.get(0).getZona(); // ZONA
				sucursal[3] = "" + sucursales.get(0).getRegion(); // REGION
				sucursal[4] = (idRecorrido == 1) ? "Primer Recorrido"
						: (idRecorrido == 2) ? "Soft Opening" : "Tercer Recorrido"; // TIPO VISITA
				sucursal[5] = "" + sucursales.get(0).getPerido(); // FECHA
				String tot = (sucursales.get(0).getPondTot() > 0 ? formatDecimales
						.format(((sucursales.get(0).getPrecalif() * 10000) / sucursales.get(0).getPondTot())) : "0");
				sucursal[6] = "" + tot;
				// CALIFICACION
			}
		}
		// Se genera url
		String url = "idUsuario=" + usuario + "&ceco=" + ceco + "&idRecorrido=" + idRecorrido + "&datos=" + dato;

		ModelAndView mv = new ModelAndView("filtrosPadresExpansion");
		mv.addObject("zonas", zonas);
		mv.addObject("tabla", tabla);
		mv.addObject("url", url);
		mv.addObject("urlBack", urlBack);
		mv.addObject("sucursal", sucursal);
		mv.addObject("tipoNegocio", tipoNegocio);
		return mv;
	}

	@RequestMapping(value = { "central/filtrosHijasExpansion.htm" }, method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView filtrosHijasExpansion(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idUsuario", required = true, defaultValue = "0") Integer idUsuario,
			@RequestParam(value = "idBit1", required = true, defaultValue = "0") Integer idBit1,
			@RequestParam(value = "idBit2", required = true, defaultValue = "0") Integer idBit2,
			@RequestParam(value = "pregFilt", required = true, defaultValue = "0") Integer pregFilt,
			@RequestParam(value = "numPregFilt", required = true, defaultValue = "0") Integer numPregFilt,
			@RequestParam(value = "zona", required = true, defaultValue = "0") Integer zona,
			@RequestParam(value = "idRecorrido", required = true, defaultValue = "0") Integer idRecorrido,
			@RequestParam(value = "detallePregunta", required = true, defaultValue = "") String detallePregunta,
			@RequestParam(value = "detalleResPregunta1", required = true, defaultValue = "") String detalleResPregunta1,
			@RequestParam(value = "detalleResPregunta2", required = true, defaultValue = "") String detalleResPregunta2,
			@RequestParam(value = "fechaRecorrido1", required = true, defaultValue = "") String fechaRecorrido1,
			@RequestParam(value = "fechaRecorrido2", required = true, defaultValue = "") String fechaRecorrido2,
			@RequestParam(value = "ceco", required = true, defaultValue = "0") Integer ceco, Model model)
			throws ServletException, IOException {

		int[] idBitGeneral = { idBit1, idBit2 };
		String[] fechaRecorrido = { fechaRecorrido1, fechaRecorrido2 };
		String[] detalleResPregunta = { detalleResPregunta1, detalleResPregunta2 };
		String[] sucursal = new String[7];
		String carrusel[] = { "", "" };
		String tablahijas[] = { "", "" };
		List<RepoFilExpDTO> sucursales = new ArrayList<RepoFilExpDTO>();
		List<RepoFilExpDTO> respuestasEvi = new ArrayList<RepoFilExpDTO>();
		List<RepoFilExpDTO> preguntasHijas = new ArrayList<RepoFilExpDTO>();
		respuestasEvi.clear();
		sucursales.clear();

		if (ceco != 0 && idRecorrido != 0) {
			int version = 0;
			List<SucursalChecklistDTO> sucursal2 = sucursalChecklistBI.obtieneDatosSuc(ceco.toString());
			version = sucursal2.get(0).getAux();
			//String tipoNegocio = sucursal2.get(0).getNegocio();
			
			sucursales = repoFilExpBI.obtieneDatosGen("" + ceco, "", "", "", "" + idRecorrido);
			// Se agregan datos para mostrar la informacion de la sucursal
			if (sucursales != null && !sucursales.isEmpty()) {
				sucursal[0] = sucursales.get(0).getNombreCeco(); // NOMBRE SUCURSAL
				sucursal[1] = "" + ceco; // CECO
				sucursal[2] = "" + sucursales.get(0).getZona(); // ZONA
				sucursal[3] = "" + sucursales.get(0).getRegion(); // REGION
				sucursal[4] = (idRecorrido == 1) ? "Primer Recorrido"
						: (idRecorrido == 2) ? "Soft Opening" : "Tercer Recorrido"; // TIPO VISITA
				sucursal[5] = "" + sucursales.get(0).getPerido(); // FECHA
				sucursal[6] = "" + sucursales.get(0).getCalif(); // CALIFICACION
			}

			if (pregFilt != 0) {
				respuestasEvi = repoFilExpBI.obtieneInfofiltro("" + idBit1, "" + idBit2, "" + pregFilt);
				preguntasHijas = repoFilExpBI.obtienePreguntasHijas("" + ceco);
				if (respuestasEvi != null) {
					int hayEvidencia[] = { 0, 0 };
					// Genero Carrusel =====================================================
					carrusel[0] += "							<div class=\"cuadrobigUnic\"> "
							+ "								<div id=\"txtMdl1\" class=\"txtMdl spBold\">"
							+ "									Item <span class=\"imgNum\">1</span> /2"
							+ "								</div>"
							+ "								<div class=\"pad-slider\">"
							+ "									<div id=\"owl-carousel1\" class=\"owl-carousel owl-theme\">";
					carrusel[1] += "							<div class=\"cuadrobigUnic\">"
							+ "								<div id=\"txtMdl2\" class=\"txtMdl spBold\">"
							+ "									Item <span class=\"imgNum\">1</span> /2"
							+ "								</div>"
							+ "								<div class=\"pad-slider\">"
							+ "									<div id=\"owl-carousel2\" class=\"owl-carousel owl-theme\">";
					for (int i = 0; i < respuestasEvi.size(); i++) {
						if (respuestasEvi.get(i).getRuta() != null && respuestasEvi.get(i).getRuta() != ""
								&& respuestasEvi.get(i).getRuta() != "null") {
							Path pdfPath = Paths.get(respuestasEvi.get(i).getRuta());
							if (idBit1 == respuestasEvi.get(i).getBitGral()) {
								String encodedA = "";
								try {
									byte[] evidencia = Files.readAllBytes(pdfPath);
									encodedA = new String(Base64.encodeBase64(evidencia), "UTF-8");
								} catch (NoSuchFileException e) {
									logger.info(
											"No se encontro Evidencia en el servidor" + respuestasEvi.get(i).getRuta());
									encodedA = "";
								}
								String imagen = "<img src=\"../images/expancion/fotoBig.svg\" class=\"itemCometarios\">";
								if (encodedA != "") {
									imagen = "<img src='data:image/png;base64," + encodedA
											+ "'class=\"itemCometarios\">";
								}
								hayEvidencia[0] = 1;
								carrusel[0] += "										<div class=\"item\">" + imagen
										+ "										</div>";
							}
							if (idBit2 == respuestasEvi.get(i).getBitGral()) {
								String encodedA = "";
								try {
									byte[] evidencia = Files.readAllBytes(pdfPath);
									encodedA = new String(Base64.encodeBase64(evidencia), "UTF-8");
								} catch (NoSuchFileException e) {
									logger.info(
											"No se encontro Evidencia en el servidor" + respuestasEvi.get(i).getRuta());
									encodedA = "";
								}
								String imagen = "<img src=\"../images/expancion/fotoBig.svg\" class=\"itemCometarios\">";
								if (encodedA != "") {
									imagen = "<img src='data:image/png;base64," + encodedA
											+ "'class=\"itemCometarios\">";
								}
								hayEvidencia[1] = 1;
								carrusel[1] += "										<div class=\"item\">" + imagen
										+ "										</div>";
							}
						}
					}

					carrusel[0] += "									</div>" + "								</div>"
							+ "							</div>";
					carrusel[1] += "									</div>" + "								</div>"
							+ "							</div>";
					if (hayEvidencia[0] == 0) {
						carrusel[0] = "";
					}
					if (hayEvidencia[1] == 0) {
						carrusel[1] = "";
					}
					hayEvidencia[1] = 0;
					hayEvidencia[0] = 0;
					// FIN Genero Carrusel =================================================

					// Genero tablas hijas =================================================
					String tablabody[] = { "", "" };
					tablahijas[0] += "<table id=\"tabla1\" class=\"tablaChecklist\">";
					tablahijas[1] += "<table id=\"tabla2\" class=\"tablaChecklist\">";

					tablahijas[0] += "								<thead>"
							+ "									<tr>"
							+ "										<th>Afectacion</th>"
							+ "										<th>Respuesta</th>"
							+ "										<th>Observacion</th>"
							+ "									</tr>" + "								</thead>"
							+ "								<tbody>";
					tablahijas[1] += "								<thead>"
							+ "									<tr>"
							+ "										<th>Afectacion</th>"
							+ "										<th>Respuesta</th>"
							+ "										<th>Observacion</th>"
							+ "									</tr>" + "								</thead>"
							+ "								<tbody>";

					if (preguntasHijas != null) {
						for (int i = 0; i < preguntasHijas.size(); i++) {
							int resp1 = 0, resp2 = 0;
							if (preguntasHijas.get(i).getPregPadre() == pregFilt) {
								for (int j = 0; j < respuestasEvi.size(); j++) {
									if (respuestasEvi.get(j).getIdPreg() == preguntasHijas.get(i).getIdPreg()) {
										if (idBit1 == respuestasEvi.get(j).getBitGral() && resp1 == 0) {
											resp1 = 1;
											tablahijas[0] += "									<tr>"
													+ "										<td>"
													+ preguntasHijas.get(i).getPregunta() + "</td>"
													+ "										<td>"
													+ respuestasEvi.get(j).getPosible() + "</td>"
													+ "										<td>"
													+ respuestasEvi.get(j).getObserv() + "</td>"
													+ "									</tr>";
										}
										if (idBit2 == respuestasEvi.get(j).getBitGral() && resp2 == 0) {
											resp2 = 1;
											tablahijas[1] += "									<tr>"
													+ "										<td>"
													+ preguntasHijas.get(i).getPregunta() + "</td>"
													+ "										<td>"
													+ respuestasEvi.get(j).getPosible() + "</td>"
													+ "										<td>"
													+ respuestasEvi.get(j).getObserv() + "</td>"
													+ "									</tr>";
										}
									}
								}
								if (resp1 == 0) {
									tablahijas[0] += "									<tr>"
											+ "										<td>"
											+ preguntasHijas.get(i).getPregunta() + "</td>"
											+ "										<td>"
											//+ ((tipoNegocio.equalsIgnoreCase("EKT")) ? "SI" : "S/R") + "</td>"
											+ ((version==1) ? "SI" : "S/R") + "</td>"
											+ "										<td></td>"
											+ "									</tr>";
								}
								if (resp2 == 0) {
									tablahijas[1] += "									<tr>"
											+ "										<td>"
											+ preguntasHijas.get(i).getPregunta() + "</td>"
											+ "										<td>"
											//+ ((tipoNegocio.equalsIgnoreCase("EKT")) ? "SI" : "S/R") + "</td>"
											+ ((version==1) ? "SI" : "S/R") + "</td>"
											+ "										<td></td>"
											+ "									</tr>";
								}
								resp1 = 0;
								resp2 = 0;
							}
						}
					}
					tablahijas[0] += "								</tbody>" + "							</table>";
					tablahijas[1] += "								</tbody>" + "							</table>";
					hayEvidencia[1] = 0;
					hayEvidencia[0] = 0;
					// FIN Genero tablas hijas =============================================
				}
			}

		}

		ModelAndView mv = new ModelAndView("filtrosHijasExpansion");
		mv.addObject("idBitGeneral", idBitGeneral);
		mv.addObject("fechaRecorrido", fechaRecorrido);
		mv.addObject("detallePregunta", detallePregunta);
		mv.addObject("sucursal", sucursal);
		mv.addObject("carrusel", carrusel);
		mv.addObject("tablahijas", tablahijas);
		mv.addObject("detalleResPregunta", detalleResPregunta);
		mv.addObject("numPregFilt", numPregFilt);

		return mv;
	}

	@RequestMapping(value = "/getSucursalesExpansion", method = RequestMethod.GET)
	public @ResponseBody List<RepoFilExpDTO> ajaxConsultaAsignacion(
			@RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,
			@RequestParam(value = "tipoRecorrido", required = true, defaultValue = "") String tipoRecorrido,
			@RequestParam(value = "estatusVisita", required = true, defaultValue = "") String estatusVisita,
			@RequestParam(value = "fechaInicio", required = true, defaultValue = "") String fechaIni,
			@RequestParam(value = "fechaFin", required = true, defaultValue = "") String fechaFin,
			HttpServletRequest request) throws Exception {

		logger.info("...ENTRO POR SUCURSALES...");

		List<RepoFilExpDTO> res = null;

		try {

			// String ceco,String nombreCeco,String fechaIni,String fechaFin,String
			// idRecorrido
			res = repoFilExpBI.obtieneDatosGen(idCeco, nombreCeco, fechaIni, fechaFin, tipoRecorrido);

		} catch (Exception e) {
			logger.info("Ocurrio algo al consultar sucursales expansion.. ");
		}

		logger.info("consulta " + res);

		logger.info("...SALE DEL AJAX ajaxConsultaAsignacion...");

		return res;
	}

	@RequestMapping(value = { "central/seleccionFiltroExpansion.htm" }, method = RequestMethod.GET)
	public ModelAndView seleccionFiltroExpansion(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "busqueda", required = true, defaultValue = "0") Integer busqueda,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,
			@RequestParam(value = "tipoRecorrido", required = true, defaultValue = "") String tipoRecorrido,
			@RequestParam(value = "estatusVisita", required = true, defaultValue = "") String estatusVisita,
			@RequestParam(value = "fechaInicio", required = true, defaultValue = "") String fechaIni,
			@RequestParam(value = "fechaFin", required = true, defaultValue = "") String fechaFin,
			@RequestParam(value = "fechaInicioParam", required = true, defaultValue = "") String fechaIniParam,
			@RequestParam(value = "fechaFinParam", required = true, defaultValue = "") String fechaFinParam)
			throws ServletException, IOException {

		String tabla = "";
		String bodytable = "";
		String recorrido = "";

		List<String> datos = new ArrayList<>();
		datos.add("" + busqueda);
		datos.add("" + idCeco);
		datos.add("" + nombreCeco);
		datos.add("" + tipoRecorrido);
		datos.add("" + estatusVisita);
		datos.add("" + fechaIni);
		datos.add("" + fechaFin);
		datos.add("" + fechaIniParam);
		datos.add("" + fechaFinParam);

		if (busqueda != 0) {

			List<RepoFilExpDTO> res = null;
			DecimalFormat formatDecimales = new DecimalFormat("###.##");
			try {

				// String ceco,String nombreCeco,String fechaIni,String fechaFin,String
				// idRecorrido
				res = repoFilExpBI.obtieneDatosGen(idCeco, nombreCeco, fechaIni, fechaFin, tipoRecorrido);

				for (int i = 0; i < res.size(); i++) {

					String calif = formatDecimales.format((res.get(i).getPrecalif() * 10000) / res.get(i).getPondTot());
					// String calif = formatDecimales.format(res.get(i).getPrecalif());

					bodytable += "<tr>" +

							"<td align='center'> <a href='filtrosPadresExpansion.htm?ceco=" + res.get(i).getCeco()
							+ "&datos=" + datos + "&idRecorrido=" + res.get(i).getIdRecorrido() + "&nombreCeco="
							+ res.get(i).getNombreCeco() + "'>" + res.get(i).getNombreCeco() + "</a></td>"
							+ "<td align='center'><b>" + res.get(i).getCeco() + "</b></td>" + "<td align='center'><b>"
							+ res.get(i).getZona() + "</b></td>" + "<td align='center'><b>" + res.get(i).getRegion()
							+ "</b></td>" + "<td align='center'><b>" + getNombreRecorrido(res.get(i).getIdRecorrido())
							+ "</b></td>" + "<td align='center'><b>" + res.get(i).getPerido() + "</b></td>"
							+ "<td align='center'><b>" + calif + "</b></td>" +
							// "<td align='center'> <a href='#.htm?ceco="+ res.get(i).getCeco()
							// +"&tipoRecorrido="+res.get(i).getIdRecorrido()+"</a></td>" +
							"<td align='center'> <a target='_blank' href='generaDashboard.htm?idCeco="
							+ res.get(i).getCeco() + "&tipoRecorrido=" + res.get(i).getIdRecorrido()
							+ "&tipoReporte=0'>Descargar</a></td>"
							+ "<td align='center'> <a target='_blank' href='generaHallazgos.htm?idCeco="
							+ res.get(i).getCeco() + "&tipoRecorrido=" + res.get(i).getIdRecorrido() + "&bitacoraPapa="
							+ res.get(i).getBitGral() + "&tipoReporte=1'>Descargar</a></td>"
							+ "<td align='center'> <a target='_blank' href='generaHallazgosAtendidos.htm?idCeco="
							+ res.get(i).getCeco() + "&bitacoraPapa=" + res.get(i).getBitGral() + "'>Descargar</a></td>"
							+

							"</tr>";
				}

				tabla = "<table id=\"tabla\" class=\"tablaChecklist\">" + "						<thead>"
						+ "							<tr>";
				tabla += "								<th><b>Nombre de Sucursal</b></th>\n"
						+ "								<th><b>CECO</b></th>\n"
						+ "								<th><b>Zona</b></th>\n"
						+ "								<th><b>Región</b></th>\n"
						+ "								<th><b>Tipo de Visita</b></th>\n"
						+ "								<th><b>Fecha</b></th>\n"
						+ "								<th><b>Calificación</b></th>"
						+ "								<th><b> Dashboard</b></th>"
						+ "								<th><b> Hallazgos </b></th>"
						+ "								<th><b> Atención de Hallazgos </b></th>" +

						"							</tr>" + "						</thead>"
						+ "						<tbody>";
				tabla += bodytable;
				tabla += "                        </tbody>" + "</table>";

			} catch (Exception e) {
				logger.info("Ocurrio algo al consultar sucursales expansion.. ");
			}
		}

		ModelAndView mv = new ModelAndView("seleccionFiltroExpansion");

		mv.addObject("tabla", tabla);
		mv.addObject("busqueda", busqueda);
		mv.addObject("idCeco", idCeco);
		mv.addObject("nombreCeco", nombreCeco);
		mv.addObject("tipoRecorrido", tipoRecorrido);
		mv.addObject("estatusVisita", estatusVisita);
		mv.addObject("fechaInicio", fechaIni);
		mv.addObject("fechaFin", fechaFin);
		mv.addObject("fechaInicioParam", fechaIniParam);
		mv.addObject("fechaFinParam", fechaFinParam);
		return mv;
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = { "central/generaDashboard.htm" }, method = RequestMethod.GET)
	public ModelAndView generaDashboard(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
			@RequestParam(value = "tipoRecorrido", required = true, defaultValue = "") String tipoRecorrido)
			throws ServletException, IOException {

		GeneraExcelDetalleExpansionBI ge = new GeneraExcelDetalleExpansionBI(1, idCeco, repoFilExpBI);
		Workbook workbook = ge.generar();

		// Write the output to a ByteArrayOutputStream
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		workbook.write(baos);

		ServletOutputStream sos = null;

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
		response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
		response.setHeader("Content-Disposition",
				"attachment; filename=dashboard_Ceco_" + idCeco + "_recorrido_" + tipoRecorrido + "_" + ".xls");
		response.setHeader("Pragma: no-cache", "Expires: 0");
		sos = response.getOutputStream();

		sos.write(baos.toByteArray());

		// Closing the workbook
		workbook.close();

		return null;

	}

	@SuppressWarnings("unused")
	@RequestMapping(value = { "central/generaHallazgos.htm" }, method = RequestMethod.GET)
	public ModelAndView generaHallazgos(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
			@RequestParam(value = "bitacoraPapa", required = true, defaultValue = "") String bitacoraPapa,
			@RequestParam(value = "tipoRecorrido", required = true, defaultValue = "") String tipoRecorrido)
			throws ServletException, IOException {

		List<ChecklistPreguntasComDTO> chklistPreguntas1 = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> chklistPreguntasImp = new ArrayList<ChecklistPreguntasComDTO>();
		chklistPreguntasImp.clear();
		chklistPreguntas1.clear();
		chklistPreguntas1 = checklistPreguntasComBI.obtieneInfoNoImp(Integer.parseInt(bitacoraPapa));

		GeneraExcelDetalleExpansionBI ge = new GeneraExcelDetalleExpansionBI();
		Workbook hallazgosWorkbook = ge.generaHallazgos(tipoRecorrido, chklistPreguntas1, idCeco, sucursalChecklistBI);

		// Write the output to a file
		ByteArrayOutputStream arrayBytesOutStream = new ByteArrayOutputStream();
		hallazgosWorkbook.write(arrayBytesOutStream);

		ServletOutputStream sos = null;
		String archivo = "";

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
		response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
		response.setHeader("Content-Disposition",
				"attachment; filename=hallazgos_Ceco_" + idCeco + "_recorrido_" + tipoRecorrido + "_" + ".xls");
		response.setHeader("Pragma: no-cache", "Expires: 0");
		sos = response.getOutputStream();

		sos.write(arrayBytesOutStream.toByteArray());

		// Closing the workbook
		hallazgosWorkbook.close();

		return null;
	}

	class Employee {
		private String name;
		private String email;
		private Date dateOfBirth;
		private double salary;

		public Employee(String name, String email, Date dateOfBirth, double salary) {
			this.name = name;
			this.email = email;
			this.dateOfBirth = dateOfBirth;
			this.salary = salary;
		}

	}

	@SuppressWarnings("unused")
	@RequestMapping(value = { "central/generaExcel.htm" }, method = RequestMethod.GET)
	public ModelAndView generaExcel(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
			@RequestParam(value = "tipoRecorrido", required = true, defaultValue = "") String tipoRecorrido)
			throws ServletException, IOException {

		String[] columns = { "Name", "Email", "Date Of Birth", "Salary" };
		List<Employee> employees = new ArrayList<>();

		Calendar dateOfBirth = Calendar.getInstance();
		dateOfBirth.set(1992, 7, 21);
		employees.add(new Employee("Rajeev Singh", "rajeev@example.com", dateOfBirth.getTime(), 1200000.0));

		dateOfBirth.set(1965, 10, 15);
		employees.add(new Employee("Thomas cook", "thomas@example.com", dateOfBirth.getTime(), 1500000.0));

		dateOfBirth.set(1987, 4, 18);
		employees.add(new Employee("Steve Maiden", "steve@example.com", dateOfBirth.getTime(), 1800000.0));

		// Create a Workbook
		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

		/*
		 * CreationHelper helps us create instances of various things like DataFormat,
		 * Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way
		 */
		CreationHelper createHelper = workbook.getCreationHelper();

		// Create a Sheet
		Sheet sheet = workbook.createSheet("Employee");

		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 14);
		headerFont.setColor(IndexedColors.RED.getIndex());

		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		// Create a Row
		Row headerRow = sheet.createRow(0);

		// Create cells
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		// Create Cell Style for formatting Date
		CellStyle dateCellStyle = workbook.createCellStyle();
		dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

		// Create Other rows and cells with employees data
		int rowNum = 1;
		for (Employee employee : employees) {
			Row row = sheet.createRow(rowNum++);

			row.createCell(0).setCellValue("AAAAA" + rowNum);

			row.createCell(1).setCellValue("BBBB" + rowNum);

			Cell dateOfBirthCell = row.createCell(2);
			dateOfBirthCell.setCellValue("CCCC" + rowNum);
			dateOfBirthCell.setCellStyle(dateCellStyle);

			row.createCell(3).setCellValue("DDDD" + rowNum);

			/* IMAGEN */
			// FileInputStream obtains input bytes from the image file
			InputStream inputStream = new FileInputStream(
					"/franquicia/imagenes/2019/189871_2188942_12429_13082019151009_ELEM_PLAN1.jpg");
			// Get the contents of an InputStream as a byte[].
			byte[] bytes = IOUtils.toByteArray(inputStream);
			// Adds a picture to the workbook
			int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
			// close the input stream
			inputStream.close();
			// Returns an object that handles instantiating concrete classes
			CreationHelper helper = workbook.getCreationHelper();
			// Creates the top-level drawing patriarch.
			Drawing drawing = sheet.createDrawingPatriarch();

			// Create an anchor that is attached to the worksheet
			ClientAnchor anchor = helper.createClientAnchor();

			// create an anchor with upper left cell _and_ bottom right cell
			anchor.setCol1(4); // Column B
			anchor.setRow1(rowNum - 1); // Row 3
			anchor.setCol2(5); // Column C
			anchor.setRow2(rowNum); // Row 4

			// Creates a picture
			Picture pict = drawing.createPicture(anchor, pictureIdx);

			/* IMAGEN */
		}

		// Resize all columns to fit the content size
		for (int i = 0; i < columns.length; i++) {
			sheet.autoSizeColumn(i);
		}

		File temp = null;
		temp = File.createTempFile("Acta_Entrega", ".xls");
		System.out.println(temp.getAbsolutePath());

		// Write the output to a file
		FileOutputStream fileOut = new FileOutputStream(temp);
		workbook.write(fileOut);

		ServletOutputStream sos = null;
		String archivo = "";

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
		response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
		response.setHeader("Content-Disposition",
				"attachment; filename=hallazgos_Ceco_" + idCeco + "_recorrido_" + tipoRecorrido + "_" + ".xls");
		response.setHeader("Pragma: no-cache", "Expires: 0");
		sos = response.getOutputStream();

		// byte[] encode = archivo.getBytes("UTF-8");
		sos.write(temp.getAbsolutePath().getBytes());

		fileOut.close();

		// Closing the workbook
		workbook.close();

		return null;

	}

	// "20190815", "20190815"
	@RequestMapping(value = { "central/formResumenEntregaSemanal.htm" }, method = RequestMethod.GET)
	public ModelAndView formResumenEntregaSemanal(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {

		ModelAndView mv = new ModelAndView("formResumenEntregaSemanal");

		return mv;

	}
	/*
	 * @SuppressWarnings("unused")
	 * 
	 * @RequestMapping(value = {"central/resumenEntregaSemanal.htm"}, method =
	 * RequestMethod.GET) public ModelAndView
	 * resumenEntregaSemanal(HttpServletRequest request, HttpServletResponse
	 * response,
	 * 
	 * @RequestParam(value = "fechaInicio", required = true, defaultValue = "")
	 * String fechaInicio,
	 * 
	 * @RequestParam(value = "fechaFin", required = true, defaultValue = "") String
	 * fechaFin) throws ServletException, IOException {
	 * 
	 * 
	 * List<RepoFilExpDTO> listaDatosGeneral = repoFilExpBI.repoFechas(fechaInicio,
	 * fechaFin); List<List<RepoFilExpDTO>> listaDatos = new
	 * ArrayList<List<RepoFilExpDTO>>();
	 * 
	 * int bitacoraGeneral = 0; int contadorAgregados = 0; boolean banderaAgregados
	 * = false;
	 * 
	 * for (int i = 0; i < listaDatosGeneral.size(); i++) { bitacoraGeneral =
	 * listaDatosGeneral.get(i).getBitGral(); List<RepoFilExpDTO> listaData = new
	 * ArrayList<RepoFilExpDTO>();
	 * 
	 * for (int j = 0; j < listaDatosGeneral.size(); j++) { if (
	 * listaDatosGeneral.get(j).getBitGral() == bitacoraGeneral ) {
	 * listaData.add(listaDatosGeneral.get(j)); } }
	 * 
	 * listaDatos.add(listaData);
	 * 
	 * for (int k = 0; k < listaDatosGeneral.size(); k++) { if (
	 * listaDatosGeneral.get(k).getBitGral() == bitacoraGeneral ) {
	 * listaDatosGeneral.remove(k); k = 0; } } }
	 * 
	 * for (int i = 0 ; i < listaDatos.size() ; i++) {
	 * 
	 * int idBitacora = listaDatos.get(i).get(0).getBitGral();
	 * 
	 * List<ChecklistPreguntasComDTO> listaImperdonables = new
	 * ArrayList<ChecklistPreguntasComDTO>();
	 * 
	 * if (checklistPreguntasComBI.obtieneInfoNoImp(idBitacora) != null &&
	 * checklistPreguntasComBI.obtieneInfoNoImp(idBitacora).size() != 0) {
	 * listaImperdonables =
	 * getImperdonablesConcatenadas(checklistPreguntasComBI.obtieneInfoNoImp(
	 * idBitacora));
	 * 
	 * 
	 * }
	 * 
	 * listaDatos.get(i).get(0).setListaImperdonables(listaImperdonables);
	 * 
	 * }
	 * 
	 * String tabla = ""; String bodytable = "";
	 * 
	 * List<String> rowsTable = new ArrayList<>();
	 * 
	 * 
	 * for (int i = 0; i < listaDatos.size(); i++) {
	 * 
	 * for (int j = 0; j < 1; j++) { DecimalFormat formatDecimales = new
	 * DecimalFormat("###.##"); String calif =
	 * formatDecimales.format((listaDatos.get(i).get(j).getPrecalif()*10000)/
	 * listaDatos.get(i).get(j).getSumPreg());
	 * 
	 * bodytable +="<tr>" + "<td align='center'><b>" +
	 * listaDatos.get(i).get(j).getFechaTermino() + "</b></td>" +
	 * "<td align='center'><b>" + listaDatos.get(i).get(j).getCeco() + "</b></td>" +
	 * "<td align='center'><b>" + listaDatos.get(i).get(j).getNombreCeco() +
	 * "</b></td>" + "<td align='center'><b>" + calif + "</b></td>" +
	 * //"<td align='center'><b>" + listaDatos.get(i).get(j).getPrecalif() +
	 * "</b></td>" + erick "<td align='center'><b>" +
	 * getNombreRecorrido(listaDatos.get(i).get(j).getIdRecorrido()) + "</b></td>" +
	 * "<td align='center'><b>" + listaDatos.get(i).get(j).getAux2() + "</b></td>" +
	 * "<td align='center'><b>" + (listaDatos.get(i).get(j).getListaImperdonables()
	 * != null ? listaDatos.get(i).get(j).getListaImperdonables().size() : 0 ) +
	 * "</b></td>" + //"<td align='center'><b>" +
	 * getNombreRecorrido(listaDatos.get(i).get(j).getIdRecorrido()) + "</b></td>" +
	 * ""; //"</tr>";
	 * 
	 * 
	 * if(listaDatos.get(i).get(j).getListaImperdonables() != null &&
	 * listaDatos.get(i).get(j).getListaImperdonables().size() > 0) {
	 * bodytable+="<td align='center'><b>" +
	 * listaDatos.get(i).get(j).getListaImperdonables().get(0).getPregunta() +
	 * "</b></td>" + "<td align='center'><b>" + listaDatos.get(i).get(0).getAux() +
	 * " : " +listaDatos.get(i).get(0).getObs() + "</b></td>"+ "</tr>"; if
	 * (listaDatos.get(i).get(j).getListaImperdonables().size()>1) {
	 * 
	 * for (int k = 1; k < listaDatos.get(i).get(j).getListaImperdonables().size();
	 * k++) { bodytable+="<tr>"; bodytable += "<td align='center'><b>" +
	 * "			"+ "</b></td>" + "<td align='center'><b>" + "			"+
	 * "</b></td>" + "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" +
	 * listaDatos.get(i).get(j).getListaImperdonables().get(k).getPregunta() +
	 * "</b></td>";
	 * 
	 * if(k<listaDatos.get(i).size()) bodytable += "<td align='center'><b>" +
	 * listaDatos.get(i).get(k).getAux() + " : " +listaDatos.get(i).get(k).getObs()
	 * + "</b></td>";
	 * 
	 * bodytable+="</tr>"; }
	 * if(listaDatos.get(i).get(j).getListaImperdonables().size()<listaDatos.get(i).
	 * size()) { int diferencia =
	 * listaDatos.get(i).size()-listaDatos.get(i).get(j).getListaImperdonables().
	 * size(); for (int k= diferencia; k < listaDatos.get(i).size(); k++) {
	 * bodytable += "<tr>" + "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + listaDatos.get(i).get(k).getAux() + " : "
	 * +listaDatos.get(i).get(k).getObs() + "</b></td>" + "</tr>"; } } } else { for
	 * (int k = 1; k < listaDatos.get(i).size(); k++) { bodytable += "<tr>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + listaDatos.get(i).get(k).getAux() + " : "
	 * +listaDatos.get(i).get(k).getObs() + "</b></td>" + "</tr>"; } } } else {
	 * bodytable+="<td align='center'><b>" +"			"+ "</b></td>" +
	 * "<td align='center'><b>" + listaDatos.get(i).get(0).getAux() + " : "
	 * +listaDatos.get(i).get(0).getObs() + "</b></td>"+ "</tr>"; for (int k = 1; k
	 * < listaDatos.get(i).size(); k++) {
	 * 
	 * bodytable += "<tr>" +
	 * 
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + "			"+ "</b></td>" +
	 * "<td align='center'><b>" + listaDatos.get(i).get(k).getAux() + " : "
	 * +listaDatos.get(i).get(k).getObs() + "</b></td>" +
	 * 
	 * "</tr>";
	 * 
	 * } }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * tabla= "<table id=\"tabla\" class=\"tablaChecklist\">" +
	 * "						<thead>" + "							<tr>"; tabla
	 * += "								<th><b>FECHA</b></th>\n" +
	 * "								<th><b>CECO</b></th>\n" +
	 * "								<th><b>SUCURSAL</b></th>\n" +
	 * "								<th><b>CALIFICACIÓN</b></th>\n" +
	 * //CALIFICACION "								<th><b>RECORRIDO</b></th>\n" +
	 * //PRECALIFICACION "								<th><b>HALLAZGOS</b></th>\n"
	 * + //PRECALIFICACION
	 * "								<th><b>CANTIDAD IMPERDONABLES</b></th>\n" +
	 * "								<th><b>IMPERDONABLES</b></th>\n" +
	 * "								<th><b>PARTICIPANTES</b></th>\n" +
	 * //RECORRIDO
	 * 
	 * "							</tr>" + "						</thead>" +
	 * "						<tbody>"; tabla+= bodytable; tabla+=
	 * "                        </tbody>" + "</table>";
	 * 
	 * 
	 * ServletOutputStream sos = null; String archivo = tabla;
	 * 
	 * DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd"); Date date = new
	 * Date(); String fecha = dateFormat.format(date).replace(" ",
	 * "_").replace(":","_").replace("/","_");
	 * response.setContentType("Content-type: application/xls; name='excel'");
	 * response.setHeader(
	 * "Content-Disposition","attachment; filename=hallazgosSemanal"+fecha+".xls");
	 * response.setHeader("Pragma: no-cache", "Expires: 0"); sos =
	 * response.getOutputStream(); //byte[] encode = archivo.getBytes("UTF-8");
	 * sos.write(archivo.getBytes()); return null; }
	 */

	@SuppressWarnings("unused")
	@RequestMapping(value = { "central/resumenEntregaSemanal2.htm" }, method = RequestMethod.GET)
	public ModelAndView resumenEntregaSemanal2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "fechaInicio", required = true, defaultValue = "") String fechaInicio,
			@RequestParam(value = "fechaFin", required = true, defaultValue = "") String fechaFin)
			throws ServletException, IOException {

		List<RepoFilExpDTO> listaDatosGeneral = repoFilExpBI.repoFechasNvo(fechaInicio, fechaFin); // obtieneDatosRepoSem
		List<List<RepoFilExpDTO>> listaDatos = new ArrayList<List<RepoFilExpDTO>>();

		int bitacoraGeneral = 0;
		int contadorAgregados = 0;
		boolean banderaAgregados = false;

		for (int i = 0; i < listaDatosGeneral.size(); i++) {
			bitacoraGeneral = listaDatosGeneral.get(i).getBitGral();
			List<RepoFilExpDTO> listaData = new ArrayList<RepoFilExpDTO>();

			for (int j = 0; j < listaDatosGeneral.size(); j++) {
				if (listaDatosGeneral.get(j).getBitGral() == bitacoraGeneral) {
					listaData.add(listaDatosGeneral.get(j));
				}
			}

			listaDatos.add(listaData);

			for (int k = 0; k < listaDatosGeneral.size(); k++) {
				if (listaDatosGeneral.get(k).getBitGral() == bitacoraGeneral) {
					listaDatosGeneral.remove(k);
					k = 0;
				}
			}
		}

		for (int i = 0; i < listaDatos.size(); i++) {

			int idBitacora = listaDatos.get(i).get(0).getBitGral();

			List<ChecklistPreguntasComDTO> listaImperdonables = new ArrayList<ChecklistPreguntasComDTO>();
			List<ChecklistPreguntasComDTO> listaImperdonables2 = new ArrayList<ChecklistPreguntasComDTO>();

			if (checklistPreguntasComBI.obtieneInfoNoImp(idBitacora) != null
					&& checklistPreguntasComBI.obtieneInfoNoImp(idBitacora).size() != 0) {
				listaImperdonables2 = checklistPreguntasComBI.obtieneInfoNoImp(idBitacora);
				if (listaImperdonables2 != null && listaImperdonables2.size() > 0) {
					for (int h = 0; h < listaImperdonables2.size(); h++) {
						if (listaImperdonables2.get(h).getIdcritica() == 1) {
							listaImperdonables.add(listaImperdonables2.get(h));
						}
					}
				}
			}

			listaDatos.get(i).get(0).setListaImperdonables(listaImperdonables);

		}

		String tabla = "";
		String bodytable = "";
		List<String> rowsTable = new ArrayList<>();

		for (int i = 0; i < listaDatos.size(); i++) {

			for (int j = 0; j < 1; j++) {
				double datos[] = obtieneCalificacion(listaDatos.get(i).get(j).getBitGral(),
						listaDatos.get(i).get(j).getPondTot());
				DecimalFormat formatDecimales = new DecimalFormat("###.##");
				// String calif =
				// formatDecimales.format((listaDatos.get(i).get(j).getPrecalif()*10000)/listaDatos.get(i).get(j).getSumPreg());
				DecimalFormat formatSinDecimales = new DecimalFormat("###");
				String calif = formatDecimales.format(datos[0]);
				String numHallazgos = formatSinDecimales.format(datos[1]);

				bodytable += "<tr>" + "<td align='center'><b>" + listaDatos.get(i).get(j).getFechaTermino()
						+ "</b></td>" + "<td align='center'><b>" + listaDatos.get(i).get(j).getCeco() + "</b></td>"
						+ "<td align='center'><b>" + listaDatos.get(i).get(j).getNombreCeco() + "</b></td>"
						+ "<td align='center'><b>" + calif + "</b></td>" +
						// "<td align='center'><b>" + listaDatos.get(i).get(j).getPrecalif() +
						// "</b></td>" + erick
						"<td align='center'><b>" + getNombreRecorrido(listaDatos.get(i).get(j).getIdRecorrido())
						+ "</b></td>" + "<td align='center'><b>" + numHallazgos + "</b></td>" +
						// "<td align='center'><b>" + listaDatos.get(i).get(j).getAux2() + "</b></td>" +
						"<td align='center'><b>"
						+ (listaDatos.get(i).get(j).getListaImperdonables() != null
								? listaDatos.get(i).get(j).getListaImperdonables().size()
								: 0)
						+ "</b></td>" +
						// "<td align='center'><b>" +
						// getNombreRecorrido(listaDatos.get(i).get(j).getIdRecorrido()) + "</b></td>" +
						"";
				// "</tr>";

				if (listaDatos.get(i).get(j).getListaImperdonables() != null
						&& listaDatos.get(i).get(j).getListaImperdonables().size() > 0) {
					bodytable += "<td align='center'><b>"
							+ listaDatos.get(i).get(j).getListaImperdonables().get(0).getPregunta() + "</b></td>"
							+ "<td align='center'><b>" + listaDatos.get(i).get(0).getAux() + " : "
							+ listaDatos.get(i).get(0).getObs() + "</b></td>" + "</tr>";
					if (listaDatos.get(i).get(j).getListaImperdonables().size() > 1) {

						for (int k = 1; k < listaDatos.get(i).get(j).getListaImperdonables().size(); k++) {
							bodytable += "<tr>";
							bodytable += "<td align='center'><b>" + "			" + "</b></td>"
									+ "<td align='center'><b>" + "			" + "</b></td>" + "<td align='center'><b>"
									+ "			" + "</b></td>" + "<td align='center'><b>" + "			" + "</b></td>"
									+ "<td align='center'><b>" + "			" + "</b></td>" + "<td align='center'><b>"
									+ "			" + "</b></td>" + "<td align='center'><b>" + "			" + "</b></td>"
									+ "<td align='center'><b>"
									+ listaDatos.get(i).get(j).getListaImperdonables().get(k).getPregunta()
									+ "</b></td>";

							if (k < listaDatos.get(i).size())
								bodytable += "<td align='center'><b>" + listaDatos.get(i).get(k).getAux() + " : "
										+ listaDatos.get(i).get(k).getObs() + "</b></td>";

							bodytable += "</tr>";
						}
						if (listaDatos.get(i).get(j).getListaImperdonables().size() < listaDatos.get(i).size()) {
							int diferencia = listaDatos.get(i).size()
									- listaDatos.get(i).get(j).getListaImperdonables().size();
							for (int k = listaDatos.get(i).get(j).getListaImperdonables().size(); k < listaDatos.get(i)
									.size(); k++) {
								bodytable += "<tr>" + "<td align='center'><b>" + "			" + "</b></td>"
										+ "<td align='center'><b>" + "			" + "</b></td>"
										+ "<td align='center'><b>" + "			" + "</b></td>"
										+ "<td align='center'><b>" + "			" + "</b></td>"
										+ "<td align='center'><b>" + "			" + "</b></td>"
										+ "<td align='center'><b>" + "			" + "</b></td>"
										+ "<td align='center'><b>" + "			" + "</b></td>"
										+ "<td align='center'><b>" + "			" + "</b></td>"
										+ "<td align='center'><b>" + listaDatos.get(i).get(k).getAux() + " : "
										+ listaDatos.get(i).get(k).getObs() + "</b></td>" + "</tr>";
							}
						}
					} else {
						for (int k = 1; k < listaDatos.get(i).size(); k++) {
							bodytable += "<tr>" + "<td align='center'><b>" + "			" + "</b></td>"
									+ "<td align='center'><b>" + "			" + "</b></td>" + "<td align='center'><b>"
									+ "			" + "</b></td>" + "<td align='center'><b>" + "			" + "</b></td>"
									+ "<td align='center'><b>" + "			" + "</b></td>" + "<td align='center'><b>"
									+ "			" + "</b></td>" + "<td align='center'><b>" + "			" + "</b></td>"
									+ "<td align='center'><b>" + "			" + "</b></td>" + "<td align='center'><b>"
									+ listaDatos.get(i).get(k).getAux() + " : " + listaDatos.get(i).get(k).getObs()
									+ "</b></td>" + "</tr>";
						}
					}
				} else {
					bodytable += "<td align='center'><b>" + "			" + "</b></td>" + "<td align='center'><b>"
							+ listaDatos.get(i).get(0).getAux() + " : " + listaDatos.get(i).get(0).getObs()
							+ "</b></td>" + "</tr>";
					for (int k = 1; k < listaDatos.get(i).size(); k++) {

						bodytable += "<tr>" +

								"<td align='center'><b>" + "			" + "</b></td>" + "<td align='center'><b>"
								+ "			" + "</b></td>" + "<td align='center'><b>" + "			" + "</b></td>"
								+ "<td align='center'><b>" + "			" + "</b></td>" + "<td align='center'><b>"
								+ "			" + "</b></td>" + "<td align='center'><b>" + "			" + "</b></td>"
								+ "<td align='center'><b>" + "			" + "</b></td>" + "<td align='center'><b>"
								+ "			" + "</b></td>" + "<td align='center'><b>"
								+ listaDatos.get(i).get(k).getAux() + " : " + listaDatos.get(i).get(k).getObs()
								+ "</b></td>" +

								"</tr>";

					}
				}

			}
		}

		tabla = "<table id=\"tabla\" class=\"tablaChecklist\">" + "						<thead>"
				+ "							<tr>";
		tabla += "								<th><b>FECHA</b></th>\n"
				+ "								<th><b>CECO</b></th>\n"
				+ "								<th><b>SUCURSAL</b></th>\n"
				+ "								<th><b>CALIFICACIÓN</b></th>\n" + /* CALIFICACION */
				"								<th><b>RECORRIDO</b></th>\n" + /* PRECALIFICACION */
				"								<th><b>HALLAZGOS</b></th>\n" + /* PRECALIFICACION */
				"								<th><b>CANTIDAD IMPERDONABLES</b></th>\n"
				+ "								<th><b>IMPERDONABLES</b></th>\n"
				+ "								<th><b>PARTICIPANTES</b></th>\n" + /* RECORRIDO */

				"							</tr>" + "						</thead>"
				+ "						<tbody>";
		tabla += bodytable;
		tabla += "                        </tbody>" + "</table>";

		ServletOutputStream sos = null;
		String archivo = tabla;

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
		response.setContentType("Content-type: application/xls; name='excel'");
		response.setHeader("Content-Disposition", "attachment; filename=hallazgosSemanal" + fecha + ".xls");
		response.setHeader("Pragma: no-cache", "Expires: 0");
		sos = response.getOutputStream();
		// byte[] encode = archivo.getBytes("UTF-8");
		sos.write(archivo.getBytes());

		return null;

	}

	private double[] obtieneCalificacion(int bitGral, double pondTotal) {
		double[] resultado = new double[2]; // 0 = Calificacion 1 = Hallazgos
		double ponderacionSI = 0;
		double ponderacionNA = 0;
		boolean versionAntigua = true;

		Map<Integer, Double> ponderacion = new HashMap<>();
		Map<Integer, Boolean> descativado = new HashMap<>();

		int error = 0;

		JSONObject json;
		JSONArray preguntas = null;
		JSONArray respuestas = null;

		String res = conteoGenExpBI.obtieneDatosRepoSem(bitGral + "");
		try {
			json = new JSONObject(res.toString());
			respuestas = json.getJSONArray("listaRespuestas").getJSONArray(0);
			preguntas = json.getJSONArray("listaPreg").getJSONArray(0);
		} catch (Exception e) {
			error = -1;
			logger.info("Algo ocurrió al parsear json conteo... " + e.getMessage());
		}
		if (error != -1) {

			if (respuestas != null && respuestas.length() > 0) {
				try {
					List<ChecklistHallazgosRepoDTO> lista = hallazgosRepoBI.obtieneDatosNego(
							Integer.toString(Integer.parseInt(respuestas.getJSONObject(0).getString("bita"))));
					//String tipoNegocio = lista.get(0).getNegocio();
					int version = 0;
					List<SucursalChecklistDTO> sucursales = sucursalChecklistBI.obtieneDatosSuc(lista.get(0).getCeco());
					version = sucursales.get(0).getAux();
					
					//if (tipoNegocio == "EKT") {
					if (version==1) {
						versionAntigua = true;
					} else {
						versionAntigua = false;
					}

				} catch (Exception e) {
					error = -1;
					logger.info("Algo ocurrió al generar al calcular calif y conteo... " + e.getMessage());
				}
			}

			try {
				if (preguntas != null && preguntas.length() > 0) {
					for (int j = 0; j < preguntas.length(); j++) {
						ConteoGenExpDTO pregunta = new ConteoGenExpDTO();
						pregunta.setIdPreg(Integer.parseInt(preguntas.getJSONObject(j).getString("idpreg")));
						pregunta.setIdModulo(Integer.parseInt(preguntas.getJSONObject(j).getString("idmodulo")));
						pregunta.setPregunta(preguntas.getJSONObject(j).getString("pregunta"));
						pregunta.setCritica(Integer.parseInt(preguntas.getJSONObject(j).getString("critica")));
						pregunta.setArea(preguntas.getJSONObject(j).getString("area"));
						pregunta.setTipoPreg(Integer.parseInt(preguntas.getJSONObject(j).getString("tipopreg")));
						pregunta.setPregPadre(Integer.parseInt(preguntas.getJSONObject(j).getString("pregpadre")));
						pregunta.setIdCheck(Integer.parseInt(preguntas.getJSONObject(j).getString("idcheck")));
						pregunta.setNomCheck(preguntas.getJSONObject(j).getString("nomcheck"));
						pregunta.setClasifica(preguntas.getJSONObject(j).getString("clasifica"));
						pregunta.setPondTotal(Double.parseDouble(preguntas.getJSONObject(j).getString("pontot")));
						pregunta.setVersion(Integer.parseInt(preguntas.getJSONObject(j).getString("version")));

						if (ponderacion.get(pregunta.getIdCheck()) == null) {
							ponderacion.put(pregunta.getIdCheck(), pregunta.getPondTotal());
						}

						if (respuestas != null && respuestas.length() > 0) {
							for (int i = 0; i < respuestas.length(); i++) {
								ConteoGenExpDTO respuesta = new ConteoGenExpDTO();
								respuesta.setBitaGral(
										Integer.parseInt(respuestas.getJSONObject(i).getString("bitagral")));
								respuesta.setBitacora(Integer.parseInt(respuestas.getJSONObject(i).getString("bita")));
								respuesta.setIdResp(Integer.parseInt(respuestas.getJSONObject(i).getString("idresp")));
								respuesta
										.setIdCheck(Integer.parseInt(respuestas.getJSONObject(i).getString("idcheck")));
								respuesta.setIdPreg(Integer.parseInt(respuestas.getJSONObject(i).getString("idpreg")));
								respuesta
										.setCritica(Integer.parseInt(respuestas.getJSONObject(i).getString("critica")));
								respuesta.setPonderacion(
										Double.parseDouble(respuestas.getJSONObject(i).getString("ponderacion")));
								respuesta.setObs(respuestas.getJSONObject(i).getString("obs"));
								respuesta.setPosible(respuestas.getJSONObject(i).getString("posible"));
								respuesta.setPregunta(respuestas.getJSONObject(i).getString("pregunta"));
								respuesta.setNomCheck(respuestas.getJSONObject(i).getString("nomcheck"));
								respuesta.setClasifica(respuestas.getJSONObject(i).getString("clasifica"));
								if (pregunta.getIdPreg() == respuesta.getIdPreg()) {
									ponderacion.put(pregunta.getIdCheck(), 0.0);
									if (respuesta.getPosible().equalsIgnoreCase("SI")) {
										ponderacionSI += respuesta.getPonderacion();
									}
									if (respuesta.getPosible().equalsIgnoreCase("NO")) {
										if (versionAntigua) {
											if (pregunta.getPregPadre() != 0) {
												resultado[1] += 1;
											}
										} else {
											resultado[1] += 1;
										}
									}
									if (respuesta.getPosible().equalsIgnoreCase("N/A")) {
										ponderacionNA += respuesta.getPonderacion();
									}
									break;
								}
							}
						}
					}

				}

			} catch (Exception e) {
				error = -1;
				logger.info("Algo ocurrió al generar al calcular calif y conteo... " + e.getMessage());
			}
		}
		Double ponCheckDesactivados = 0.0;

		for (Map.Entry<Integer, Double> entry : ponderacion.entrySet()) {
			// System.out.println("clave=" + entry.getKey() + ", valor=" +
			// entry.getValue());
			ponCheckDesactivados += entry.getValue();
		}
		resultado[0] = ((pondTotal - ponderacionNA - ponCheckDesactivados <= 0 || ponderacionSI <= 0) ? 0
				: ponderacionSI * 100 / (pondTotal - ponderacionNA - ponCheckDesactivados));

		return resultado;
	}

	// Recibe Una lista Completa de NO (obtieneInfoNoImp , obtieneInfoImp)
	// Concatenado
	public List<ChecklistPreguntasComDTO> getImperdonablesConcatenadas(List<ChecklistPreguntasComDTO> lista) {

		List<ChecklistPreguntasComDTO> chklistPreguntasImp = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> listaResult = new ArrayList<ChecklistPreguntasComDTO>();

		listaResult.clear();

		chklistPreguntasImp = lista;

		for (int i = 0; i < chklistPreguntasImp.size(); i++) {

			if (chklistPreguntasImp.get(i).getPregPadre() == 0 && chklistPreguntasImp.get(i).getIdcritica() == 1) {

				for (int j = 0; j < chklistPreguntasImp.size(); j++) {

					if (chklistPreguntasImp.get(i).getIdPreg() == chklistPreguntasImp.get(j).getPregPadre()) {

						ChecklistPreguntasComDTO data = chklistPreguntasImp.get(j);
						data.setPregunta(chklistPreguntasImp.get(i).getPregunta() + " : "
								+ chklistPreguntasImp.get(j).getPregunta());
						listaResult.add(data);
					}
				}
			}
		}

		return listaResult;

	}

	// Recibe Una lista Completa de NO (obtieneInfoNoImp , obtieneInfoImp)
	public List<ChecklistPreguntasComDTO> getHallazgosConcatenados(List<ChecklistPreguntasComDTO> lista) {

		List<ChecklistPreguntasComDTO> chklistPreguntasImp = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> listaResult = new ArrayList<ChecklistPreguntasComDTO>();

		listaResult.clear();

		chklistPreguntasImp = lista;

		for (int i = 0; i < chklistPreguntasImp.size(); i++) {

			if (chklistPreguntasImp.get(i).getPregPadre() == 0) {

				for (int j = 0; j < chklistPreguntasImp.size(); j++) {

					if (chklistPreguntasImp.get(i).getIdPreg() == chklistPreguntasImp.get(j).getPregPadre()) {

						ChecklistPreguntasComDTO data = chklistPreguntasImp.get(j);
						data.setPregunta(chklistPreguntasImp.get(i).getPregunta() + " : "
								+ chklistPreguntasImp.get(j).getPregunta());
						listaResult.add(data);
					}
				}
			}
		}

		return listaResult;

	}

	public String getNombreRecorrido(String recorrido) {

		try {

			int numRecorrido = Integer.parseInt(recorrido);

			if (numRecorrido > 3) {

				return "Recorrido especial " + numRecorrido;

			} else {

				switch (numRecorrido) {

				case 1:
					return "Caminata";

				case 2:
					return "Soft Opening";

				case 3:
					return "Grand Opening";

				default:
					return "Pre Caminata";
				}

			}

		} catch (Exception e) {

			logger.info("AP al obtener el numero de recorrido Expansion");
			return "Caminata " + recorrido;

		}

	}

	@RequestMapping(value = { "central/seleccionSucursalHallazgos.htm" }, method = RequestMethod.GET)
	public ModelAndView seleccionSucursalHallazgos(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ModelAndView mv = new ModelAndView("seleccionSucursalHallazgos");
		return mv;
	}

	@RequestMapping(value = { "central/getHallazgos.htm" }, method = { RequestMethod.POST, RequestMethod.GET })

	public ModelAndView getHallazgos(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco)
			throws ServletException, IOException {

		List<HallazgosExpDTO> listaHallazgos = new ArrayList<HallazgosExpDTO>();
		List<HallazgosEviExpDTO> listaEvidencias = new ArrayList<HallazgosEviExpDTO>();
		
		UsuarioDTO userSession = null;
		userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		
		try {
			int version = 0;
			List<SucursalChecklistDTO> sucursal = sucursalChecklistBI.obtieneDatosSuc(idCeco);
			//String tipoNegocio = sucursal.get(0).getNegocio();
			version = sucursal.get(0).getAux();
			
			listaHallazgos = hallazgosExpBI.obtieneDatos(Integer.parseInt(idCeco));
			//if (tipoNegocio.equalsIgnoreCase("EKT")) {
			if(version==1) {
				listaHallazgos = getHallazgosConcatenadas(listaHallazgos);
			}
			listaEvidencias = hallazgosEviExpBI.obtieneDatosBita(Integer.parseInt(idCeco));

		} catch (Exception e) {

			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos y hallazgosEviExpBI.obtieneDatosBita del CECO: "
							+ idCeco);

		}

		ModelAndView mv = new ModelAndView("hallazgosSucursalExpansion");

		String tabla = "";
		String bodytable = "";
		String nombreCeco = "";
		String status = "";
		int hallazgosTotales = 0;
		if (listaHallazgos != null && listaHallazgos.size() > 0) {
			nombreCeco = listaHallazgos.get(0).getCecoNom();
			hallazgosTotales = listaHallazgos.size();
		}

		int hallazgosPorAtender = 0;
		int hallazgosPorAutorizar = 0;
		int hallazgosAtendidos = 0;
		int hallazgosRechazadosPorAtender = 0;

		if (listaHallazgos != null) {

			for (int i = 0; i < listaHallazgos.size(); i++) {

				switch (listaHallazgos.get(i).getStatus()) {
				case 0:
					hallazgosRechazadosPorAtender++;
					break;
				case 1:
					hallazgosPorAtender++;
					break;
				case 2:
					hallazgosPorAutorizar++;
					break;
				case 3:
					hallazgosAtendidos++;
					break;
				default:
					break;
				}

				bodytable += "<tr>" +

						"<td align='center'>" + listaHallazgos.get(i).getIdHallazgo() + "</a></td>" +

						"<td align='center'>" + listaHallazgos.get(i).getZonaClasi() + "</a></td>" +

						"<td align='center'><b>" + listaHallazgos.get(i).getNombChek() + "</b></td>" +

						getLigaDetalle(userSession,listaHallazgos.get(i).getStatus(), listaHallazgos.get(i).getIdHallazgo(), idCeco,
								listaHallazgos.get(i).getPreg())
						+

						"<td align='center'><b>" + listaHallazgos.get(i).getRespuesta() + "</b></td>" +

						"<td align='center' style='color:" + getColorStatus(listaHallazgos.get(i).getStatus())
						+ ";'><b>" + getStatus(listaHallazgos.get(i).getStatus()) + "</b></td>" +

						"<td align='center'><b> <a href='getHistorialHallazgo.htm?idCeco=" + idCeco + "&idHallazgo="
						+ listaHallazgos.get(i).getIdHallazgo() + "'> Ver Detalle</a></b></td>" +

						// "<td align='center'> <a target='_blank'
						// href='generaDashboard.htm?idCeco="+res.get(i).getCeco()+"&tipoRecorrido="+res.get(i).getIdRecorrido()+"&tipoReporte=0'>Descargar</a></td>"
						// +

						"</tr>";

			}

			tabla = "<table id=\"tabla\" class=\"tablaChecklist\">" +

					" <thead>" +

					" <tr>";

			tabla += " <th><b>Id</b></th>\n" + " <th><b>Zona</b></th>\n" + " <th><b>Checkslit</b></th>\n"
					+ " <th><b>Item</b></th>\n" + " <th><b>Respuesta</b></th>\n" + " <th><b>Estatus</b></th>\n"
					+ " <th><b>Historial</b></th>\n" + " </tr>" +

					" </thead>" + " <tbody>";

			tabla += bodytable;

			tabla += "                        </tbody>" +

					"</table>";

		}

		String tablaConteoHallazgos = "";

		tablaConteoHallazgos += "<div style=\"float:right;\">" +

				"<table style=\"margin-top: 4%; width: 100%; float:right;\" >" + "<tbody>" + "<tr>"
				+ "<td>Total Hallazgos reportados: " + hallazgosTotales + "</td>" + "</tr>" + "<tr>" + "<td>Atendidos: "
				+ hallazgosAtendidos + "</td>" + "</tr>" + "<tr>" + "<td>Pendientes Por Autorizar: "
				+ hallazgosPorAutorizar + "</td>" + "</tr>" + "<tr>" + "<td>Rechazados Por Atender: "
				+ hallazgosRechazadosPorAtender + " </td>" + "</tr>" + "<tr>" + "<td>Por Atender: "
				+ hallazgosPorAtender + "</td>" + "</tr>" + "</tbody>" + "</table>" + "</div>";

		mv.addObject("tabla", tabla);
		mv.addObject("idCeco", idCeco);
		mv.addObject("nombreCeco", nombreCeco);
		mv.addObject("listaHallazgos", listaHallazgos);
		mv.addObject("listaEvidencias", listaEvidencias);
		mv.addObject("tablaConteoHallazgos", tablaConteoHallazgos);

		return mv;

	}

	@RequestMapping(value = { "central/getAdicionales.htm" }, method = { RequestMethod.POST, RequestMethod.GET })

	public ModelAndView getAdicionales(HttpServletRequest request, HttpServletResponse response,

			@RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco)
			throws ServletException, IOException {

		List<AdicionalesDTO> listaHallazgos = new ArrayList<AdicionalesDTO>();
		// List<HallazgosEviExpDTO> listaEvidencias = new
		// ArrayList<HallazgosEviExpDTO>();

		try {

			listaHallazgos = adicionalesExpBI.obtieneDatos(idCeco);
			// listaHallazgos = getHallazgosConcatenadas(listaHallazgos);
			// listaEvidencias =
			// hallazgosEviExpBI.obtieneDatosBita(Integer.parseInt(idCeco));

		} catch (Exception e) {

			logger.info(
					"AP al obtener los Adicionales adicionelasExpBI.obtieneDatos y adicionalesExpBI.obtieneDatosBita del CECO: "
							+ idCeco);

		}

		ModelAndView mv = new ModelAndView("adicionalesSucursalExpansion");

		String tabla = "";
		String bodytable = "";
		String nombreCeco = "";
		String status = "";
		int hallazgosTotales = 0;
		if (listaHallazgos != null && listaHallazgos.size() > 0) {
			nombreCeco = listaHallazgos.get(0).getNomCeco();
			hallazgosTotales = listaHallazgos.size();
		}

		int hallazgosPorAtender = 0;
		int hallazgosPorAutorizar = 0;
		int hallazgosAtendidos = 0;
		int hallazgosRechazadosPorAtender = 0;

		if (listaHallazgos != null) {

			for (int i = 0; i < listaHallazgos.size(); i++) {

				switch (listaHallazgos.get(i).getStatus()) {
				case 0:
					hallazgosRechazadosPorAtender++;
					break;
				case 1:
					hallazgosPorAtender++;
					break;
				case 2:
					hallazgosPorAutorizar++;
					break;
				case 3:
					hallazgosAtendidos++;
					break;
				default:
					break;
				}

				bodytable += "<tr>" +

						"<td align='center'>" + listaHallazgos.get(i).getIdPreg() + "</a></td>" +

						"<td align='center'>" + listaHallazgos.get(i).getClasifica() + "</a></td>" +

						"<td align='center'><b>" + listaHallazgos.get(i).getNomCheck() + "</b></td>" +

						"<td align='center'><b> <a href='vistaAtiendeAdicionalExpansion.htm?bitaPadre="
						+ listaHallazgos.get(i).getBitGral() + "&idPreg=" + listaHallazgos.get(i).getIdPreg() + "'>"
						+ listaHallazgos.get(i).getPregunta() + "</a></b></td>" +

						"<td align='center'><b>" + listaHallazgos.get(i).getPosible() + "</b></td>" +

						"<td align='center' style='color:" + getColorStatus(listaHallazgos.get(i).getStatus())
						+ ";'><b>" + getStatus(listaHallazgos.get(i).getStatus()) + "</b></td>" +

						// "<td align='center'><b> <a href='getHistorialHallazgo.htm?idCeco=" + idCeco +
						// "&idHallazgo="+ listaHallazgos.get(i).getIdPreg() + "'> Ver
						// Detalle</a></b></td>" +

						// "<td align='center'> <a target='_blank'
						// href='generaDashboard.htm?idCeco="+res.get(i).getCeco()+"&tipoRecorrido="+res.get(i).getIdRecorrido()+"&tipoReporte=0'>Descargar</a></td>"
						// +

						"</tr>";

			}

			tabla = "<table id=\"tabla\" class=\"tablaChecklist\">" +

					" <thead>" +

					" <tr>";

			tabla += " <th><b>Id</b></th>\n" + " <th><b>Zona</b></th>\n" + " <th><b>Checkslit</b></th>\n"
					+ " <th><b>Item</b></th>\n" + " <th><b>Respuesta</b></th>\n" + " <th><b>Estatus</b></th>\n" +
					// " <th><b>Historial</b></th>\n" +
					" </tr>" +

					" </thead>" + " <tbody>";

			tabla += bodytable;

			tabla += "                        </tbody>" +

					"</table>";

		}

		String tablaConteoHallazgos = "";

		tablaConteoHallazgos += "<div style=\"float:right;\">" +

				"<table style=\"margin-top: 4%; width: 100%; float:right;\" >" + "<tbody>" + "<tr>"
				+ "<td>Total Adicionales reportados: " + hallazgosTotales + "</td>" + "</tr>" + "<tr>"
				+ "<td>Atendidos: " + hallazgosAtendidos + "</td>" + "</tr>" + "<tr>" + "<td>Pendientes Por Autorizar: "
				+ hallazgosPorAutorizar + "</td>" + "</tr>" + "<tr>" + "<td>Rechazados Por Atender: "
				+ hallazgosRechazadosPorAtender + " </td>" + "</tr>" + "<tr>" + "<td>Por Atender: "
				+ hallazgosPorAtender + "</td>" + "</tr>" + "</tbody>" + "</table>" + "</div>";

		mv.addObject("tabla", tabla);
		mv.addObject("idCeco", idCeco);
		mv.addObject("nombreCeco", nombreCeco);
		mv.addObject("listaHallazgos", listaHallazgos);
		mv.addObject("tablaConteoHallazgos", tablaConteoHallazgos);

		return mv;

	}

	public String getLigaDetalle(UsuarioDTO userSession, int status, int idHallazgo, String ceco, String pregunta) {
		List<PerfilUsuarioDTO> listaPerfiles=null;
		String perfil="";

		if (userSession != null) {
			listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(), FRQConstantes.PERFIL_EXPANSION_AUTORIZACION);
			if(listaPerfiles!=null && listaPerfiles.size()>0) {
				perfil=FRQConstantes.PERFIL_EXPANSION_AUTORIZACION;
			}
			else {
				listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(), FRQConstantes.PERFIL_EXPANSION_ATIENDE);
				if(listaPerfiles!=null && listaPerfiles.size()>0) {
					perfil=FRQConstantes.PERFIL_EXPANSION_ATIENDE;
				}
			}
		}

		if(FRQConstantes.PERFIL_EXPANSION_AUTORIZACION.equalsIgnoreCase(perfil)) {
			switch (status) {
			case 0:
				return "<td align='center'><b> <a href='vistaAtiendeHallazgoExpansion.htm?idCeco=" + ceco + "&idHallazgo="
						+ idHallazgo + "'>" + pregunta + "</a></b></td>";
			case 1:
				return "<td align='center'><b> <a href='vistaAtiendeHallazgoExpansion.htm?idCeco=" + ceco + "&idHallazgo="
						+ idHallazgo + "'>" + pregunta + "</a></b></td>";
			case 2:
				return "<td align='center'><b> <a href='vistaDetalleHallazgoExpansion.htm?idCeco=" + ceco + "&idHallazgo="
						+ idHallazgo + "'>" + pregunta + "</a></b></td>";
			case 3:
				return "<td align='center'><b> <a href='vistaDetalleHallazgoAutorizadoExpansion.htm?idCeco=" + ceco
						+ "&idHallazgo=" + idHallazgo + "'>" + pregunta + "</a></b></td>";
			default:
				return "Pendiente Por Atender";
	
			}
		}
		else {
			switch (status) {
			case 0:
				return "<td align='center'><b> <a href='vistaAtiendeHallazgoExpansion.htm?idCeco=" + ceco + "&idHallazgo="
				+ idHallazgo + "'>" + pregunta + "</a></b></td>";
			case 1:
				return "<td align='center'><b> <a href='vistaAtiendeHallazgoExpansion.htm?idCeco=" + ceco + "&idHallazgo="
				+ idHallazgo + "'>" + pregunta + "</a></b></td>";
			case 2:
				return "<td align='center'><b> <a href='vistaDetalleHallazgoAutorizadoExpansion.htm?idCeco=" + ceco
						+ "&idHallazgo=" + idHallazgo + "'>" + pregunta + "</a></b></td>";
			case 3:
				return "<td align='center'><b> <a href='vistaDetalleHallazgoAutorizadoExpansion.htm?idCeco=" + ceco
						+ "&idHallazgo=" + idHallazgo + "'>" + pregunta + "</a></b></td>";
			default:
				return "Pendiente Por Atender";
	
			}
		}
	}

	public List<HallazgosExpDTO> getHallazgosConcatenadas(List<HallazgosExpDTO> lista) {

		List<HallazgosExpDTO> chklistPreguntas = new ArrayList<HallazgosExpDTO>();
		List<HallazgosExpDTO> listaResult = new ArrayList<HallazgosExpDTO>();

		listaResult.clear();

		chklistPreguntas = lista;

		for (int i = 0; i < chklistPreguntas.size(); i++) {

			if (chklistPreguntas.get(i).getPregPadre() == 0) {

				for (int j = 0; j < chklistPreguntas.size(); j++) {

					if (chklistPreguntas.get(i).getIdPreg() == chklistPreguntas.get(j).getPregPadre()) {

						HallazgosExpDTO data = chklistPreguntas.get(j);
						data.setPreg(chklistPreguntas.get(i).getPreg() + " : " + chklistPreguntas.get(j).getPreg());
						listaResult.add(data);
					}
				}
			}
		}

		return listaResult;

	}

	@RequestMapping(value = { "central/vistaDetalleHallazgoExpansion.htm" }, method = RequestMethod.GET)
	public ModelAndView vistaDetalleHallazgoExpansion(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
			@RequestParam(value = "idHallazgo", required = true, defaultValue = "") String idHallazgo)
			throws ServletException, IOException {
		List<HallazgosExpDTO> listaHallazgos = new ArrayList<HallazgosExpDTO>();
		List<HallazgosEviExpDTO> listaEvidencias = new ArrayList<HallazgosEviExpDTO>();
		
		List<PerfilUsuarioDTO> listaPerfiles=null;
		boolean permiso=false;

		UsuarioDTO userSession = null;
		userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		
		if (userSession != null) {
			listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(), FRQConstantes.PERFIL_EXPANSION_AUTORIZACION);
			if(listaPerfiles!=null && listaPerfiles.size()>0) {
				permiso=true;
			}
			else {
				permiso=false;
			}
		}

		String pregunta = "";
		String imagen = "";
		String comentarios = "";
		String comentariosNuevos = "";

		int idResp = 0;
		int status = 0;
		String respuesta = "";
		String ruta = "";
		String rutaAtencion = "";
		String fechaAutorizacion = "";
		String fechaFin = "";

		ArrayList<String> listaEvidenciasAnteriores = new ArrayList<String>();

		try {
			int version = 0;
			List<SucursalChecklistDTO> sucursal = sucursalChecklistBI.obtieneDatosSuc(idCeco);
			//String tipoNegocio = sucursal.get(0).getNegocio();
			version = sucursal.get(0).getAux();
			
			listaHallazgos = hallazgosExpBI.obtieneDatos(Integer.parseInt(idCeco));
			
			//if (tipoNegocio.equalsIgnoreCase("EKT")) {
			if(version==1) {
				listaHallazgos = getHallazgosConcatenadas(listaHallazgos);
			}
			listaEvidencias = hallazgosEviExpBI.obtieneDatosBita(Integer.parseInt(idCeco));

			for (HallazgosExpDTO objPreg : listaHallazgos) {

				if (objPreg.getIdHallazgo() == Integer.parseInt(idHallazgo)) {
					pregunta = objPreg.getPreg();
					comentarios = objPreg.getObs();
					idResp = objPreg.getIdResp();
					status = objPreg.getStatus();
					respuesta = objPreg.getRespuesta();
					comentariosNuevos = objPreg.getDisposi();

					ruta = objPreg.getRuta();
					rutaAtencion = objPreg.getRuta();
					imagen = ruta;
					Path imgPath = Paths.get(imagen);

					String encodedA = "";
					try {
						byte[] evidencia = Files.readAllBytes(imgPath); // erick
						// byte[] evidencia =
						// Files.readAllBytes(Paths.get("/franquicia/imagenes/2019/200106_3269016_9874_ORDEN_1_20190822143423.jpg"));
						// //erick
						encodedA = new String(Base64.encodeBase64(evidencia), "UTF-8");
					} catch (NoSuchFileException e) {
						logger.info("No se encontro Evidencia en el servidor" + imagen);
						encodedA = "";
					}

					imagen = encodedA;

					SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
					fechaAutorizacion = f.format(new Date());
					fechaFin = objPreg.getFechaFin();
				}

			}

		} catch (Exception e) {
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos y hallazgosEviExpBI.obtieneDatosBita del CECO: "
							+ idCeco);
		}

		ModelAndView mv = new ModelAndView("vistaDetalleHallazgoExpansion");

		String carrusel = "";

		carrusel = "<div class=\"cuadrobigUnic\">" + "<div id=\"txtMdl2\" class=\"txtMdl spBold\">"
				+ "Item <span class=\"imgNum\">1</span> /2" + "</div>" + "<label align='center'> ATENDIDO </label>"
				+ "<div class=\"pad-slider\">" + "<div id=\"owl-carousel2\" class=\"owl-carousel owl-theme\">";

		carrusel += "<div class=\"item\">";

		if (imagen != null && imagen.length() > 0) {
			carrusel += "<img src='data:image/png;base64," + imagen + "'>";
		} else {
			carrusel += "<img src=\"../images/expancion/fotoBig.svg\">";
		}
		carrusel += "</div>";
		carrusel += "</div> </div> </div>";

		carrusel += "<div class='divContenido' style='padding-bottom:1px;'>";
		carrusel += "<textarea style='width:99%; height:95%; resize: none;' readonly>" + comentariosNuevos
				+ "</textarea>";

		// Carrusel 2
		for (HallazgosEviExpDTO objEvidencia : listaEvidencias) {

			if (objEvidencia.getIdHallazgo() == Integer.parseInt(idHallazgo)) {

				ruta = objEvidencia.getRuta();
				imagen = ruta;
				Path imgPath = Paths.get(imagen);

				String encodedA = "";
				try {
					byte[] evidencia = Files.readAllBytes(imgPath); // erick
					// byte[] evidencia =
					// Files.readAllBytes(Paths.get("/franquicia/imagenes/2019/200106_3269016_9874_ORDEN_1_20190822143423.jpg"));
					// //erick
					encodedA = new String(Base64.encodeBase64(evidencia), "UTF-8");
				} catch (NoSuchFileException e) {
					logger.info("No se encontro Evidencia en el servidor" + imagen);
					encodedA = "";
				}

				imagen = encodedA;
				listaEvidenciasAnteriores.add(encodedA);

			}

		}

		carrusel += "<div class=\"cuadrobigUnic\" id=\"cuadro1\">" + "<div id=\"txtMdl1\" class=\"txtMdl spBold\">"
				+ "Item <span class=\"imgNum\">1</span> /2" + "</div>" + "<label> ANTES </label>"
				+ "<div class=\"pad-slider\">" + "<div id=\"owl-carousel1\" class=\"owl-carousel owl-theme\">";

		for (int i = 0; i < listaEvidenciasAnteriores.size(); i++) {

			carrusel += "<div class=\"item\">";

			if (listaEvidenciasAnteriores.get(i) != null && listaEvidenciasAnteriores.get(i).length() > 0) {
				carrusel += "<img src='data:image/png;base64," + listaEvidenciasAnteriores.get(i) + "'>";

			} else {
				carrusel += "<img src=\"../images/expancion/fotoBig.svg\">";
			}

			carrusel += "</div>";

		}

		carrusel += "</div> </div> </div>";

		carrusel += "<div class='divContenido' style='padding-bottom:1px;'>";
		carrusel += "<textarea style='width:99%; height:95%; resize: none;' readonly>" + comentarios + "</textarea>";

		mv.addObject("urlServer", FRQConstantes.getURLServer());
		mv.addObject("carrusel", carrusel);
		mv.addObject("imagen", imagen);
		mv.addObject("pregunta", pregunta);
		mv.addObject("comentarios", comentariosNuevos);
		mv.addObject("comentarioInicio", comentarios);
		mv.addObject("permiso", permiso);

		mv.addObject("ceco", idCeco);
		mv.addObject("idResp", idResp);
		mv.addObject("status", status);
		mv.addObject("respuesta", respuesta);
		mv.addObject("ruta", rutaAtencion);
		mv.addObject("fechaAutorizacion", fechaAutorizacion);
		mv.addObject("fechaFin", fechaFin);

		mv.addObject("idHallazgo", idHallazgo);

		return mv;
	}

	/* DIRECTOS */
	@RequestMapping(value = { "central/actualizaHallazgoDirecto.htm" }, method = RequestMethod.GET)
	public String actualizaHallazgoDirecto(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "ceco", required = true, defaultValue = "") String ceco,
			@RequestParam(value = "idResp", required = true, defaultValue = "") String idResp,
			@RequestParam(value = "status", required = true, defaultValue = "") String status,
			@RequestParam(value = "respuesta", required = true, defaultValue = "") String respuesta,
			@RequestParam(value = "ruta", required = true, defaultValue = "") String ruta,
			@RequestParam(value = "fechaAutorizacion", required = true, defaultValue = "") String fechaAutorizacion,
			@RequestParam(value = "fechaFin", required = true, defaultValue = "") String fechaFin,
			@RequestParam(value = "idHallazgo", required = true, defaultValue = "") String idHallazgo,
			@RequestParam(value = "comentario", required = true, defaultValue = "") String comentario,
			@RequestParam(value = "comentarioInicio", required = true, defaultValue = "") String comentarioInicio,
			@RequestParam(value = "motivoRechazo", required = true, defaultValue = "") String motivoRechazo)
			throws ServletException, IOException {

		int idRespLocal = 0;
		int statusLocal = 0;
		String respuestaLocal = "";
		String rutaLocal = "";
		String fechaAutorizacionLocal = "";
		String fechaFinLocal = "";
		String comentarioLocal = "";
		String motivoRechazoLocal = "";
		String usuModif = "";
		UsuarioDTO userSession = null;

		userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if (userSession != null)
			usuModif = userSession.getIdUsuario() + "-"
					+ ((userSession.getNombre() != null && !userSession.getNombre().equalsIgnoreCase("null"))
							? userSession.getNombre()
							: "DESDEWEB");
		else
			usuModif = "DESDEWEB";
		HallazgosExpDTO bean = new HallazgosExpDTO();

		idRespLocal = Integer.parseInt(idResp);
		statusLocal = Integer.parseInt(status);
		respuestaLocal = respuesta;
		rutaLocal = ruta;
		fechaAutorizacionLocal = fechaAutorizacion;
		fechaFinLocal = fechaFin;
		comentarioLocal = comentario;
		motivoRechazoLocal = motivoRechazo;

		bean.setIdResp(idRespLocal);
		bean.setStatus(statusLocal);
		bean.setRespuesta(respuestaLocal);
		bean.setRuta(rutaLocal);
		bean.setFechaAutorizo(fechaAutorizacionLocal.replaceAll("-", ""));
		bean.setFechaFin(fechaFinLocal.replaceAll("-", ""));

		// SE ACTUALIZA CON ID RESPUESTA, NUEVO ES CON ID HALLAZGO
		bean.setIdHallazgo(Integer.parseInt(idHallazgo));
		bean.setIdResp(Integer.parseInt(idResp));
		// SE ACTUALIZA MOTIVO RECGAZO Y USUARIO MODIF
		bean.setMotivrechazo(motivoRechazo);
		bean.setUsuModif(usuModif);

		if (statusLocal == 3) {
			bean.setObs(comentarioInicio);
			bean.setObsNueva(comentarioLocal);
		} else if (statusLocal == 1 || statusLocal == 0) {
			bean.setObs(comentarioInicio);
			bean.setObsNueva(comentarioLocal);
		}

		bean.setMotivrechazo(motivoRechazoLocal);

		boolean actualizaResponse = hallazgosExpBI.actualizaResp(bean);

		logger.info(actualizaResponse);

		List<HallazgosExpDTO> listaHallazgos = new ArrayList<HallazgosExpDTO>();
		List<HallazgosEviExpDTO> listaEvidencias = new ArrayList<HallazgosEviExpDTO>();

		try {

			listaHallazgos = hallazgosExpBI.obtieneDatos(Integer.parseInt(ceco));

			listaHallazgos = getHallazgosConcatenadas(listaHallazgos);

			listaEvidencias = hallazgosEviExpBI.obtieneDatosBita(Integer.parseInt(ceco));

		} catch (Exception e) {
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos y hallazgosEviExpBI.obtieneDatosBita del CECO: "
							+ ceco);
		}

		ModelAndView mv = new ModelAndView("hallazgosSucursalExpansion");

		String tabla = "";
		String bodytable = "";

		String nombreCeco = "";

		if (listaHallazgos.size() > 0) {
			nombreCeco = listaHallazgos.get(0).getCecoNom();

		}

		for (int i = 0; i < listaHallazgos.size(); i++) {

			bodytable += "<tr>" +

					"<td align='center'>" + listaHallazgos.get(i).getZonaClasi() + "</a></td>"
					+ "<td align='center'><b>" + listaHallazgos.get(i).getNombChek() + "</b></td>"
					+ getLigaDetalle(userSession,listaHallazgos.get(i).getStatus(), listaHallazgos.get(i).getIdHallazgo(), ceco,
							listaHallazgos.get(i).getPreg())
					+ "<td align='center'><b>" + listaHallazgos.get(i).getRespuesta() + "</b></td>"
					+ "<td align='center' style='color:" + getColorStatus(listaHallazgos.get(i).getStatus()) + ";'><b>"
					+ getStatus(listaHallazgos.get(i).getStatus()) + "</b></td>" +
					// "<td align='center'> <a target='_blank'
					// href='generaDashboard.htm?idCeco="+res.get(i).getCeco()+"&tipoRecorrido="+res.get(i).getIdRecorrido()+"&tipoReporte=0'>Descargar</a></td>"
					// +

					"</tr>";
		}

		tabla = "<table id=\"tabla\" class=\"tablaChecklist\">" + "						<thead>"
				+ "							<tr>";
		tabla += "							<th><b>Zona</b></th>\n"
				+ "								<th><b>Checkslit</b></th>\n"
				+ "								<th><b>Item</b></th>\n"
				+ "								<th><b>Respuesta</b></th>\n"
				+ "								<th><b>Estatus</b></th>\n" + "							</tr>"
				+ "						</thead>" + "						<tbody>";
		tabla += bodytable;
		tabla += "                        </tbody>" + "</table>";

		mv.addObject("tabla", tabla);
		mv.addObject("idCeco", ceco);
		mv.addObject("nombreCeco", nombreCeco);
		mv.addObject("listaHallazgos", listaHallazgos);
		mv.addObject("listaEvidencias", listaEvidencias);

		return "redirect:/central/getHallazgos.htm?idCeco=" + ceco;

		// return mv;

	}

	@SuppressWarnings("unused")
	@RequestMapping(value = { "central/generaHallazgosSemanal.htm" }, method = RequestMethod.GET)
	public ModelAndView generaHallazgosSemanal(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "fechaInicio", required = true, defaultValue = "") String fechaInicio,
			@RequestParam(value = "fechaFin", required = true, defaultValue = "") String fechaFin)

			throws ServletException, IOException {

		List<ChecklistHallazgosRepoDTO> chklistPreguntas1 = new ArrayList<ChecklistHallazgosRepoDTO>();
		List<ChecklistHallazgosRepoDTO> chklistPreguntasImp = new ArrayList<ChecklistHallazgosRepoDTO>();

		chklistPreguntasImp.clear();
		chklistPreguntas1.clear();
		chklistPreguntas1 = hallazgosRepoBI.obtieneDatosFecha(fechaInicio, fechaFin);

		// checklistPreguntasComBI.obtieneInfoNoImp(Integer.parseInt(bitacoraPapa));

		GeneraExcelDetalleExpansionBI ge = new GeneraExcelDetalleExpansionBI();
		Workbook hallazgosWorkbook = ge.generaHallazgosSemana("2", chklistPreguntas1);

		// Write the output to a file
		ByteArrayOutputStream arrayBytesOutStream = new ByteArrayOutputStream();
		hallazgosWorkbook.write(arrayBytesOutStream);

		ServletOutputStream sos = null;
		String archivo = "";

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
		response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
		response.setHeader("Content-Disposition", "attachment; filename=Hallazgos_Semana_" + fecha + ".xls");
		response.setHeader("Pragma: no-cache", "Expires: 0");
		sos = response.getOutputStream();

		sos.write(arrayBytesOutStream.toByteArray());

		// Closing the workbook
		hallazgosWorkbook.close();

		return null;
	}

	@RequestMapping(value = { "central/vistaDetalleHallazgoAutorizadoExpansion.htm" }, method = RequestMethod.GET)
	public ModelAndView vistaDetalleHallazgoAutorizadoExpansion(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
			@RequestParam(value = "idHallazgo", required = true, defaultValue = "") String idHallazgo)
			throws ServletException, IOException {
		List<HallazgosExpDTO> listaHallazgos = new ArrayList<HallazgosExpDTO>();
		List<HallazgosEviExpDTO> listaEvidencias = new ArrayList<HallazgosEviExpDTO>();

		String pregunta = "";
		String imagen = "";
		String comentarios = "";

		int idResp = 0;
		int status = 0;
		String respuesta = "";
		String ruta = "";
		String fechaAutorizacion = "";
		String fechaFin = "";

		try {
			int version = 0;
			List<SucursalChecklistDTO> sucursal = sucursalChecklistBI.obtieneDatosSuc(idCeco);
			//String tipoNegocio = sucursal.get(0).getNegocio();
			version = sucursal.get(0).getAux();
			
			listaHallazgos = hallazgosExpBI.obtieneDatos(Integer.parseInt(idCeco));
			
			//if (tipoNegocio.equalsIgnoreCase("EKT")) {
			if(version==1) {
				listaHallazgos = getHallazgosConcatenadas(listaHallazgos);
			}
			listaEvidencias = hallazgosEviExpBI.obtieneDatosBita(Integer.parseInt(idCeco));

			for (HallazgosExpDTO objPreg : listaHallazgos) {

				if (objPreg.getIdHallazgo() == Integer.parseInt(idHallazgo)) {
					pregunta = objPreg.getPreg();
					comentarios = objPreg.getDisposi();
					idResp = objPreg.getIdResp();
					status = objPreg.getStatus();
					respuesta = objPreg.getRespuesta();

					ruta = objPreg.getRuta();
					imagen = ruta;
					Path imgPath = Paths.get(imagen);

					String encodedA = "";
					try {
						byte[] evidencia = Files.readAllBytes(imgPath); // erick
						// byte[] evidencia =
						// Files.readAllBytes(Paths.get("/franquicia/imagenes/2019/200106_3269016_9874_ORDEN_1_20190822143423.jpg"));
						// //erick
						encodedA = new String(Base64.encodeBase64(evidencia), "UTF-8");
					} catch (NoSuchFileException e) {
						logger.info("No se encontro Evidencia en el servidor" + imagen);
						encodedA = "";
					}

					imagen = encodedA;

					SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
					fechaAutorizacion = f.format(new Date());
					fechaFin = objPreg.getFechaFin();
				}

			}

		} catch (Exception e) {
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos y hallazgosEviExpBI.obtieneDatosBita del CECO: "
							+ idCeco);
		}

		ModelAndView mv = new ModelAndView("vistaDetalleHallazgoAutorizadoExpansion");

		String carrusel = "";

		carrusel = "<div class=\"cuadrobigUnic\">" + "<div id=\"txtMdl2\" class=\"txtMdl spBold\">"
				+ "Item <span class=\"imgNum\">1</span> /2" + "</div>" + "<div class=\"pad-slider\">"
				+ "<div id=\"owl-carousel2\" class=\"owl-carousel owl-theme\">";

		for (int i = 0; i < 1; i++) {

			carrusel += "<div class=\"item\">";

			if (imagen != null && imagen.length() > 0) {
				carrusel += "<img src='data:image/png;base64," + imagen + "'>";

			} else {
				carrusel += "<img src=\"../images/expancion/fotoBig.svg\">";
			}

			carrusel += "</div>";

		}

		carrusel += "</div> </div> </div>";

		mv.addObject("urlServer", FRQConstantes.getURLServer());
		mv.addObject("carrusel", carrusel);
		mv.addObject("imagen", imagen);
		mv.addObject("pregunta", pregunta);
		mv.addObject("comentarios", comentarios);

		mv.addObject("ceco", idCeco);
		mv.addObject("idResp", idResp);
		mv.addObject("status", status);
		mv.addObject("respuesta", respuesta);
		mv.addObject("ruta", ruta);
		mv.addObject("fechaAutorizacion", fechaAutorizacion);
		mv.addObject("fechaFin", fechaFin);

		return mv;
	}

	@RequestMapping(value = { "central/vistaAtiendeHallazgoExpansion.htm" }, method = RequestMethod.GET)
	public ModelAndView vistaAtiendeHallazgoExpansion(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
			@RequestParam(value = "idHallazgo", required = true, defaultValue = "") String idHallazgo)
			throws ServletException, IOException {
		List<HallazgosExpDTO> listaHallazgos = new ArrayList<HallazgosExpDTO>();
		List<HallazgosEviExpDTO> listaEvidencias = new ArrayList<HallazgosEviExpDTO>();

		UsuarioDTO userSession = null;
		userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		
		String pregunta = "";
		String imagen = "";
		String comentarios = "";

		int idResp = 0;
		int status = 0;
		String respuesta = "";
		String ruta = "";
		String fechaAutorizacion = "";
		String fechaFin = "";
		String rutaRechazado = "";
		String imagenRechazado64 = "";
		HallazgosExpDTO hallazgo = null;
		ArrayList<String> listaEvidenciasAnteriores = new ArrayList<String>();
		
		List<PerfilUsuarioDTO> listaPerfiles=null;
		int estado=2;

		if (userSession != null) {
			listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(), FRQConstantes.PERFIL_EXPANSION_AUTORIZACION);
			if(listaPerfiles!=null && listaPerfiles.size()>0) {
				estado=3;
			}
			else {
				listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(), FRQConstantes.PERFIL_EXPANSION_ATIENDE);
				if(listaPerfiles!=null && listaPerfiles.size()>0) {
					estado=2;
				}
			}
		}


		if (idCeco != null && !idCeco.isEmpty()) {
			try {

				int version = 0;
				List<SucursalChecklistDTO> sucursal = sucursalChecklistBI.obtieneDatosSuc(idCeco);
				//String tipoNegocio = sucursal.get(0).getNegocio();
				version = sucursal.get(0).getAux();
				
				listaHallazgos = hallazgosExpBI.obtieneDatos(Integer.parseInt(idCeco));
				//if (tipoNegocio.equalsIgnoreCase("EKT")) {
				if(version==1) {
					listaHallazgos = getHallazgosConcatenadas(listaHallazgos);
				}

				listaEvidencias = hallazgosEviExpBI.obtieneDatosBita(Integer.parseInt(idCeco));

				for (HallazgosExpDTO objPreg : listaHallazgos) {

					if (objPreg.getIdHallazgo() == Integer.parseInt(idHallazgo)) {
						hallazgo = objPreg;
						pregunta = objPreg.getPreg();
						comentarios = objPreg.getObs();
						idResp = objPreg.getIdResp();
						status = objPreg.getStatus();
						respuesta = objPreg.getRespuesta();
						if (status == 0) {
							rutaRechazado = objPreg.getRuta();
							try {
								byte[] evidencia = Files.readAllBytes(Paths.get(rutaRechazado)); // erick
								imagenRechazado64 = new String(Base64.encodeBase64(evidencia), "UTF-8");
							} catch (NoSuchFileException e) {
								logger.info("No se encontro Evidencia en el servidor" + rutaRechazado);
								imagenRechazado64 = "";
							}
						}

						SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
						fechaAutorizacion = f.format(new Date());
						fechaFin = objPreg.getFechaFin();
					}

				}

				for (HallazgosEviExpDTO objEvidencia : listaEvidencias) {

					if (objEvidencia.getIdHallazgo() == Integer.parseInt(idHallazgo)) {

						ruta = objEvidencia.getRuta();
						imagen = ruta;
						Path imgPath = Paths.get(imagen);

						String encodedA = "";
						try {
							byte[] evidencia = Files.readAllBytes(imgPath); // erick
							// byte[] evidencia =
							// Files.readAllBytes(Paths.get("/franquicia/imagenes/2019/200106_3269016_9874_ORDEN_1_20190822143423.jpg"));
							// //erick
							encodedA = new String(Base64.encodeBase64(evidencia), "UTF-8");
						} catch (NoSuchFileException e) {
							logger.info("No se encontro Evidencia en el servidor" + imagen);
							encodedA = "";
						}

						imagen = encodedA;
						listaEvidenciasAnteriores.add(encodedA);

					}

				}

			} catch (Exception e) {
				logger.info(
						"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos y hallazgosEviExpBI.obtieneDatosBita del CECO: "
								+ idCeco);
			}
		}

		ModelAndView mv = new ModelAndView("vistaAtiendeHallazgoExpansion");

		String carrusel = "";

		carrusel = "<div class=\"cuadrobigUnic\" id=\"cuadro1\">" + "<div id=\"txtMdl1\" class=\"txtMdl spBold\">"
				+ "Item <span class=\"imgNum\">1</span> /2" + "</div>" + "<div class=\"pad-slider\">"
				+ "<div id=\"owl-carousel1\" class=\"owl-carousel owl-theme\">";

		for (int i = 0; i < listaEvidenciasAnteriores.size(); i++) {

			carrusel += "<div class=\"item\">";

			if (listaEvidenciasAnteriores.get(i) != null && listaEvidenciasAnteriores.get(i).length() > 0) {
				carrusel += "<img src='data:image/png;base64," + listaEvidenciasAnteriores.get(i) + "'>";

			} else {
				carrusel += "<img src=\"../images/expancion/fotoBig.svg\">";
			}

			carrusel += "</div>";

		}

		carrusel += "</div> </div> </div>";

		// Carrusel 2
		carrusel += "<div class=\"cuadrobigUnic\" id=\"cuadro2\">" + "<div id=\"txtMdl2\" class=\"txtMdl spBold\">"
				+ "Item <span class=\"imgNum\">1</span> /2" + "</div>" + "<div class=\"pad-slider\">";

		carrusel += "<div id=\"owl-carousel2\" class=\"owl-carousel owl-theme\">";
		carrusel += "<div class=\"item\">";
		if (rutaRechazado != "")
			carrusel += "<img id='evidenciaCargada' src='data:image/png;base64," + imagenRechazado64 + "'>"; // erick
																												// imagenRechazado64
		else
			carrusel += "<img id='evidenciaCargada' src=\"../images/expancion/fotoBig.svg\">"; // erick
		carrusel += "</div>";
		carrusel += "</div> </div>";
		if (hallazgo != null && hallazgo.getMotivrechazo() != null && !hallazgo.getMotivrechazo().isEmpty()
				&& hallazgo.getMotivrechazo() != "") {
			carrusel += "<div id=\"rechazo\">\n" + "    \n"
					+ "<label style=\"font-size: 1.2em; color: #000;\">Se rechazo el hallazgo por el siguiente motivo:</label>\n"
					+ "<br>\n" + "\n" + "    <label style=\"font-size:18px;color: red;\">" + hallazgo.getMotivrechazo()
					+ "</label>\n" + "</div>";
		}
		carrusel += "</div>";

		mv.addObject("hallazgo", hallazgo);
		mv.addObject("urlServer", FRQConstantes.getURLServer());
		mv.addObject("carrusel", carrusel);
		mv.addObject("imagen", imagen);
		mv.addObject("pregunta", pregunta);
		mv.addObject("comentarios", comentarios);
		mv.addObject("estado", estado);//estado al que se actualizara de acuerdo al operfil
		

		mv.addObject("ceco", idCeco);
		mv.addObject("idResp", idResp);
		mv.addObject("status", status);
		mv.addObject("respuesta", respuesta);
		mv.addObject("ruta", ruta);
		mv.addObject("fechaAutorizacion", fechaAutorizacion);
		mv.addObject("fechaFin", fechaFin);

		mv.addObject("idHallazgo", idHallazgo);

		return mv;
	}

	/* DIRECTOS */
	@RequestMapping(value = { "central/actualizaHallazgoImagen.htm" }, method = { RequestMethod.GET,
			RequestMethod.POST })
	public String actualizaHallazgoImagen(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "ceco", required = true, defaultValue = "") String ceco,
			@RequestParam(value = "observacionNueva", required = true, defaultValue = "") String observacionNueva,
			@RequestParam(value = "img64", required = true, defaultValue = "") String img64,
			@RequestParam(value = "respuesta", required = true, defaultValue = "") String respuesta,
			@RequestParam(value = "status", required = true, defaultValue = "") String status,
			@RequestParam(value = "comentarioAnt", required = true, defaultValue = "") String comentarioAnt,
			@RequestParam(value = "idHallazgo", required = true, defaultValue = "") String idHallazgo,
			@RequestParam(value = "fechaFin", required = true, defaultValue = "") String fechaFin,
			@RequestParam(value = "idResp", required = true, defaultValue = "") String idResp)
			throws ServletException, IOException {

		// Es mi base64, aquí guardo la imagen, la tengo que enviar
		String base64 = img64;

		String observaciones = observacionNueva;
		String rutaImg = "";
		String res = respuesta;
		String statusRes = status;
		String comentarioAnterior = comentarioAnt;
		String idHallazgoN = idHallazgo;
		String fechaFinLocal = fechaFin;

		String fechaAutorizacionLocal = "";
		SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		fechaAutorizacionLocal = f.format(new Date());

		base64 = base64.replaceAll(" ", "+");
		base64 = base64.split(",")[1];
		logger.info(base64);

		String motivoRechazoLocal = "";// ERICK333
		String usuModif = "";
		UsuarioDTO userSession = null;

		userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if (userSession != null)
			usuModif = userSession.getIdUsuario() + "-"
					+ ((userSession.getNombre() != null && !userSession.getNombre().equalsIgnoreCase("null"))
							? userSession.getNombre()
							: "DESDEWEB");
		else
			usuModif = "DESDEWEB";

		// GUARDA IMAGEN
		String rutaResponse = sucursalChecklistBI.insertaHallazgoDirecto(Integer.parseInt(ceco), base64);

		// GUARDAR IMAGEN
		HallazgosExpDTO bean = new HallazgosExpDTO();

		bean.setObsNueva(observaciones);
		bean.setRuta(rutaResponse);
		bean.setRespuesta(res);
		bean.setStatus(Integer.parseInt(statusRes));
		bean.setObs(comentarioAnterior);
		// SE ACTUALIZA CON ID RESPUESTA, NUEVO ES CON ID HALLAZGO
		bean.setIdHallazgo(Integer.parseInt(idHallazgoN));
		bean.setIdResp(Integer.parseInt(idResp));
		// SE ACTUALIZA CON ID RESPUESTA, NUEVO ES CON ID HALLAZGO
		bean.setFechaFin(fechaFinLocal);
		bean.setFechaAutorizo(fechaAutorizacionLocal);
		// SE ACTUALIZA EL USUARIO QUE MODIFICA Y MOTIVO RECHAZO VACIO
		bean.setUsuModif(usuModif);
		bean.setMotivrechazo(motivoRechazoLocal);

		if (fechaFinLocal.length() <= 0) {
			bean.setFechaFin(fechaAutorizacionLocal);
		}

		boolean actualizaResponse = hallazgosExpBI.actualizaResp(bean);

		logger.info(actualizaResponse);

		// SE RECARGAN LAS LISTA PARA REDIRIGIR A LA PRIMER PÁGINA

		List<HallazgosExpDTO> listaHallazgos = new ArrayList<HallazgosExpDTO>();
		List<HallazgosEviExpDTO> listaEvidencias = new ArrayList<HallazgosEviExpDTO>();

		try {

			listaHallazgos = hallazgosExpBI.obtieneDatos(Integer.parseInt(ceco));

			listaHallazgos = getHallazgosConcatenadas(listaHallazgos);

			listaEvidencias = hallazgosEviExpBI.obtieneDatosBita(Integer.parseInt(ceco));

		} catch (Exception e) {
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos y hallazgosEviExpBI.obtieneDatosBita del CECO: "
							+ ceco);
		}

		ModelAndView mv = new ModelAndView("hallazgosSucursalExpansion");

		String tabla = "";
		String bodytable = "";

		String nombreCeco = "";

		if (listaHallazgos.size() > 0) {
			nombreCeco = listaHallazgos.get(0).getCecoNom();

		}

		for (int i = 0; i < listaHallazgos.size(); i++) {

			bodytable += "<tr>" +

					"<td align='center'>" + listaHallazgos.get(i).getZonaClasi() + "</a></td>"
					+ "<td align='center'><b>" + listaHallazgos.get(i).getNombChek() + "</b></td>"
					+ getLigaDetalle(userSession,listaHallazgos.get(i).getStatus(), listaHallazgos.get(i).getIdHallazgo(), ceco,
							listaHallazgos.get(i).getPreg())
					+ "<td align='center'><b>" + listaHallazgos.get(i).getRespuesta() + "</b></td>"
					+ "<td align='center' style='color:" + getColorStatus(listaHallazgos.get(i).getStatus()) + ";'><b>"
					+ getStatus(listaHallazgos.get(i).getStatus()) + "</b></td>" +
					// "<td align='center'> <a target='_blank'
					// href='generaDashboard.htm?idCeco="+res.get(i).getCeco()+"&tipoRecorrido="+res.get(i).getIdRecorrido()+"&tipoReporte=0'>Descargar</a></td>"
					// +

					"</tr>";
		}

		tabla = "<table id=\"tabla\" class=\"tablaChecklist\">" + "						<thead>"
				+ "							<tr>";
		tabla += "							<th><b>Zona</b></th>\n"
				+ "								<th><b>Checkslit</b></th>\n"
				+ "								<th><b>Item</b></th>\n"
				+ "								<th><b>Respuesta</b></th>\n"
				+ "								<th><b>Estatus</b></th>\n" + "							</tr>"
				+ "						</thead>" + "						<tbody>";
		tabla += bodytable;
		tabla += "                        </tbody>" + "</table>";

		mv.addObject("tabla", tabla);
		mv.addObject("idCeco", ceco);
		mv.addObject("nombreCeco", nombreCeco);
		mv.addObject("listaHallazgos", listaHallazgos);
		mv.addObject("listaEvidencias", listaEvidencias);

		return "redirect:/central/getHallazgos.htm?idCeco=" + ceco;

		// return mv;
	}

	/* DIRECTOS */
	@RequestMapping(value = { "central/getActualizaHallazgos.htm" }, method = { RequestMethod.GET })
	public ModelAndView getActualizaHallazgos(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idBitacoraHermana", required = true, defaultValue = "") String idBitacoraHermana)
			throws ServletException, IOException {

		// Es mi base64, aquí guardo la imagen, la tengo que enviar
		boolean res = false;

		try {
			List<ChecklistHallazgosRepoDTO> lista = hallazgosRepoBI.obtieneDatosNego(idBitacoraHermana);
			// boolean cargaHallazgos = hallazgosExpBI.cargaHallazgos(idBitacora);
			//String tipoNegocio = lista.get(0).getNegocio();
			int version = 0;
			List<SucursalChecklistDTO> sucursales = sucursalChecklistBI.obtieneDatosSuc(lista.get(0).getCeco());
			version = sucursales.get(0).getAux();
			
			int intIdBitacoraHermana = Integer.parseInt(idBitacoraHermana);
			//if (tipoNegocio.equalsIgnoreCase("EKT")) {
			if (version==1){
				res = hallazgosExpBI.cargaHallazgosUpdate(intIdBitacoraHermana);
			} else {
				res = hallazgosExpBI.cargaHallazgosUpdateNuevo(intIdBitacoraHermana);
			}

		} catch (NumberFormatException nfe) {
			res = false;
			logger.info("AP al convertir el bitacora a Int");
		} catch (Exception e) {
			res = false;
			logger.info("AP en General");
		}

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "ACTUALZACIÓN DE HALLAZGOS ");
		mv.addObject("res", res);

		return mv;

	}

	// @SuppressWarnings("unused")
	@RequestMapping(value = { "central/generaHallazgosAtendidos.htm" }, method = RequestMethod.GET)
	public ModelAndView generaHallazgosAtendidos(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
			@RequestParam(value = "bitacoraPapa", required = true, defaultValue = "") String bitacoraPapa)
			throws ServletException, IOException {

		List<ChecklistPreguntasComDTO> chklistPreguntasIniciales = new ArrayList<ChecklistPreguntasComDTO>();
		chklistPreguntasIniciales.clear();
		chklistPreguntasIniciales = checklistPreguntasComBI.obtieneInfoHallaz(Integer.parseInt(idCeco));

		List<ChecklistPreguntasComDTO> chklistPreguntasTabla = new ArrayList<ChecklistPreguntasComDTO>();
		chklistPreguntasTabla.clear();
		chklistPreguntasTabla = checklistPreguntasComBI.obtieneInfoHallazTab(Integer.parseInt(idCeco));

		GeneraExcelDetalleExpansionBI ge = new GeneraExcelDetalleExpansionBI();
		Workbook hallazgosWorkbook = ge.generaHallazgosAtendidos(chklistPreguntasIniciales, chklistPreguntasTabla,
				idCeco);

		// Write the output to a file
		ByteArrayOutputStream arrayBytesOutStream = new ByteArrayOutputStream();
		hallazgosWorkbook.write(arrayBytesOutStream);

		ServletOutputStream sos = null;
		String archivo = "";

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
		response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
		response.setHeader("Content-Disposition",
				"attachment; filename=hallazgos_atendidos_Ceco_" + idCeco + "_recorrido" + ".xls");
		response.setHeader("Pragma: no-cache", "Expires: 0");
		sos = response.getOutputStream();

		sos.write(arrayBytesOutStream.toByteArray());

		// Closing the workbook
		hallazgosWorkbook.close();

		return null;
	}

	@RequestMapping(value = { "central/getHistorialHallazgo.htm" }, method = RequestMethod.GET)
	public ModelAndView getHistorialHallazgo(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
			@RequestParam(value = "idHallazgo", required = true, defaultValue = "") String idHallazgo)
			throws ServletException, IOException {

		//
		// String idCeco = "488361";
		// String idHallazgo = "1155";

		List<HallazgosExpDTO> listaHallazgos = new ArrayList<HallazgosExpDTO>();
		List<HallazgosEviExpDTO> listaEvidencias = new ArrayList<HallazgosEviExpDTO>();

		String imagen = "";
		String ruta = "";

		ArrayList<String> listaEvidenciasAnteriores = new ArrayList<String>();

		try {

			int version = 0;
			List<SucursalChecklistDTO> sucursal = sucursalChecklistBI.obtieneDatosSuc(idCeco);
			//String tipoNegocio = sucursal.get(0).getNegocio();
			version = sucursal.get(0).getAux();
			
			listaHallazgos = hallazgosExpBI.obtieneDatos(Integer.parseInt(idCeco));
			//if (tipoNegocio.equalsIgnoreCase("EKT")) {
			if(version==1) {
				listaHallazgos = getHallazgosConcatenadas(listaHallazgos);
			}
			listaEvidencias = hallazgosEviExpBI.obtieneDatosBita(Integer.parseInt(idCeco));

		} catch (Exception e) {
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos y hallazgosEviExpBI.obtieneDatosBita del CECO: "
							+ idCeco);
		}

		ModelAndView mv = new ModelAndView("historialHallazgoExpansion");

		// OBTENGO EVIDENCIA MULTIPLE PARA PRIMER HALLAZGO
		for (HallazgosEviExpDTO objEvidencia : listaEvidencias) {
			if (objEvidencia.getIdHallazgo() == Integer.parseInt(idHallazgo)) {
				ruta = objEvidencia.getRuta();
				imagen = ruta;
				Path imgPath = Paths.get(imagen);
				String encodedA = "";
				try {
					byte[] evidencia = Files.readAllBytes(imgPath); // erick
					// byte[] evidencia =
					// Files.readAllBytes(Paths.get("/franquicia/imagenes/2019/200106_3269016_9874_ORDEN_1_20190822143423.jpg"));
					// //erick
					encodedA = new String(Base64.encodeBase64(evidencia), "UTF-8");
				} catch (NoSuchFileException e) {
					logger.info("No se encontro Evidencia en el servidor" + imagen);
					encodedA = "";
				}
				imagen = encodedA;
				listaEvidenciasAnteriores.add(encodedA);
			}
		}
		// OBTENGO EVIDENCIA MULTIPLE PARA PRIMER HALLAZGO

		// OBTENGO MI HISTORIAL DE HALLAZGOS
		List<HallazgosExpDTO> lista = hallazgosExpBI.obtieneHistFolio(Integer.parseInt(idHallazgo));

		if (lista != null && lista.size() == 0) {

			return null;

		} else {
			// CABECERA PARA PRIMER PUNTO
			String cabeceraHallazgoInicial = "";
			cabeceraHallazgoInicial += "<div id=\"cabecera\">"
					+ "<p> <b style=\"color:black;\">Persona que levantó el hallazgo:</b>" + lista.get(0).getUsuModif()
					+ "</p>" + "<p> <b style=\"color:black;\">Comentario:</b>" + lista.get(0).getDisposi() + "</p> "
					+ "<p> <b style=\"color:black;\">Estatus:</b> <b style=\"color:"
					+ getColorStatus(lista.get(0).getStatus()) + ";\">" + getStatus(lista.get(0).getStatus())
					+ "</b></p> <label style=\"margin-top: -10px; float: right;\">Fecha: " + lista.get(0).getFechaIni()
					+ "</label>" + "</div>";

			cabeceraHallazgoInicial += "<div id=\"linea\">" + "<div id=\"respuesta\">" + "<button id=\"respuestaBtn\">"
					+ lista.get(0).getRespuesta() + "</button>" + "<div id=\"respuestaBtnDiv\"></div>" + "</div>"
					+ "</div>";
			// CABECERA PARA PRIMER PUNTO

			// IMAGEN OBTENIDA DESDE INICIO
			String carrusel = "";
			carrusel += "<div class=\"cuadrobigUnic\" id=\"cuadro1\">" + "<div id=\"txtMdl1\" class=\"txtMdl spBold\">"
					+ "Item <span class=\"imgNum\">1</span> /2" + "</div>" + "<label> ANTES </label>"
					+ "<div class=\"pad-slider\">" + "<div id=\"owl-carousel1\" class=\"owl-carousel owl-theme\">";

			for (int i = 0; i < listaEvidenciasAnteriores.size(); i++) {

				carrusel += "<div class=\"item\">";

				if (listaEvidenciasAnteriores.get(i) != null && listaEvidenciasAnteriores.get(i).length() > 0) {
					carrusel += "<img src='data:image/png;base64," + listaEvidenciasAnteriores.get(i) + "'>";

				} else {
					carrusel += "<img src=\"../images/expancion/fotoBig.svg\">";
				}

				carrusel += "</div>";

			}

			carrusel += "</div> </div> </div>";
			// IMAGEN OBTENIDA DESDE INICIO

			String historialCadena = "";

			// HISTORIAL DE HALLAZGOS
			if (lista.size() > 1) {

				lista.remove(0);

				int i = 0;

				if (lista != null && lista.size() > 0) {

					for (HallazgosExpDTO obj : lista) {

						String rutaLocal = obj.getRuta();
						imagen = rutaLocal;
						Path imgPath = Paths.get(imagen);
						String encodedA = "";
						try {
							byte[] evidencia = Files.readAllBytes(imgPath); // erick
							// byte[] evidencia =
							// Files.readAllBytes(Paths.get("/franquicia/imagenes/2019/200106_3269016_9874_ORDEN_1_20190822143423.jpg"));
							// //erick
							encodedA = new String(Base64.encodeBase64(evidencia), "UTF-8");
						} catch (NoSuchFileException e) {
							logger.info("No se encontro Evidencia en el servidor" + imagen);
							encodedA = "";
						}
						imagen = encodedA;

						historialCadena += "<div id=\"contenedorTracking\" style=\"margin-top:0px;\">";

						historialCadena += "<div id=\"cabecera\">"
								+ "<p><b style=\"color:black;\">Persona que levantó el hallazgo:</b>"
								+ obj.getUsuModif() + "</p>";

						if (obj.getStatus() == 2 || obj.getStatus() == 3) {
							historialCadena += "<p><b style=\"color:black;\">Comentario:</b>" + obj.getDisposi()
									+ "</p>";
						} else if (obj.getStatus() == 0) {
							historialCadena += "<p><b style=\"color:black;\">Comentario:</b>" + obj.getMotivrechazo()
									+ "</p>";
						}

						historialCadena += "<p><b style=\"color:black;\">Estatus:</b> <b style=\"color:"
								+ getColorStatus(obj.getStatus()) + ";\">" + getStatus(obj.getStatus())
								+ "</b></p> <label style=\"margin-top: -10px; float: right;\">Fecha:" + obj.getPeriodo()
								+ "</label>" + "</div>";

						historialCadena += "<div id=\"linea\">" +

								"<div id=\"respuesta\">" + "<div id=\"respuestaBtnDivSup\"></div>"
								+ "<button id=\"respuestaBtn2\">" + obj.getRespuesta() + "</button>";

						if (i == lista.size() - 1) {
							historialCadena += "<div style=\"background-color:white;\" id=\"respuestaBtnDiv\"></div>";
						} else {
							historialCadena += "<div id=\"respuestaBtnDiv\"></div>";

						}
						historialCadena += "</div>" + "</div>";

						historialCadena += "<div id=\"contenidoImg\">"
								+ "<img id=\"imgEvidencia\" class=\"owl-stage-outer zoomify\" alt=\"evidencia\" src=\"data:image/png;base64,"
								+ imagen + "\">" + "</div>";

						historialCadena += "</div>";

						i++;
					}
				}
			}
			// HISTORIAL DE HALLAZGOS

			mv.addObject("urlServer", FRQConstantes.getURLServer());
			mv.addObject("cabeceraHallazgoInicial", cabeceraHallazgoInicial);
			mv.addObject("carrusel", carrusel);
			mv.addObject("historialCadena", historialCadena);

			return mv;
		}
	}

	public String getStatus(int status) {

		switch (status) {
		case 0:
			return "Rechazado/Pendiente Por Atender";
		case 1:
			return "Pendiente Por Atender";
		case 2:
			return "Pendiente Por Autorizar";
		case 3:
			return "Autorizado";
		default:
			return "Pendiente Por Atender";

		}

	}

	public String getColorStatus(int status) {

		switch (status) {
		case 1:
			return "red";
		case 2:
			return "orange";
		case 3:
			return "green";
		default:
			return "orange";

		}
	}

	// http://10.53.33.83/checklist/generaActaPDF.htm?idBitacora=323&tipoVisita=Primer
	// Recorrido&fechaVisita=20191028&economico=0100&sucursal=MEGA AV BELEN
	// QRO&tipo=Nueva&territorio=TERRITORIO SB CENTRO PONIENTE&zona=ZONA SB TOLUCA
	// MEXICO PONIENTE&rutaImagen=//franquicia/ImagSucursal/ej_suc2.PNG
	@RequestMapping(value = { "central/generaActaPDF.htm" }, method = RequestMethod.GET)
	public ModelAndView generaActaPDF(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idBitacora", required = true, defaultValue = "0") int idBitacora,
			@RequestParam(value = "tipoVisita", required = true, defaultValue = "") String tipoVisita,
			@RequestParam(value = "fechaVisita", required = true, defaultValue = "") String fechaVisita,
			@RequestParam(value = "economico", required = true, defaultValue = "") String economico,
			@RequestParam(value = "sucursal", required = true, defaultValue = "") String sucursal,
			@RequestParam(value = "tipo", required = true, defaultValue = "") String tipo,
			@RequestParam(value = "territorio", required = true, defaultValue = "") String territorio,
			@RequestParam(value = "zona", required = true, defaultValue = "") String zona,
			@RequestParam(value = "rutaImagen", required = true, defaultValue = "") String rutaImagen,
			@RequestParam(value = "tipoNegocio", required = true, defaultValue = "") String tipoNegocio)

			throws ServletException, IOException {
		List<ChecklistHallazgosRepoDTO> lista = null;
		List<SucursalChecklistDTO> sucursal3 = null;
		int version = 0;
		try {
			 lista = hallazgosRepoBI.obtieneDatosNego(""+idBitacora);
			 if(lista!=null && lista.size()>0) {
				 sucursal3 = sucursalChecklistBI.obtieneDatosSuc(lista.get(0).getCeco());
				 
			 }
		} catch (Exception e) {
		}
		version = sucursal3.get(0).getAux();

		List<FirmaCheckDTO> firmas = new ArrayList<FirmaCheckDTO>();

		firmas.clear();
		firmas = firmaCheckBI.obtieneDatos(idBitacora + "");
		if (firmas != null) {
			if (firmas.size() == 1) {
				if (firmas.get(0).getBitacora() == null) {
					firmas.clear();
				}
			}
		}

		CorreoBI c = new CorreoBI();
		ServletOutputStream sos = null;

		String pdfNuevo = "";
		//if (tipoNegocio.equalsIgnoreCase("EKT")) {
		if(version==1) {
			pdfNuevo = c.generaActa(idBitacora + "", tipoVisita, fechaVisita, economico, sucursal, tipo, territorio,
					zona, firmas, rutaImagen, tipoNegocio);
		} else {
			pdfNuevo = c.generaActaGeneral(idBitacora + "", tipoVisita, fechaVisita, economico, sucursal, tipo,
					territorio, zona, firmas, rutaImagen, tipoNegocio);
		}

		response.setContentType("Content-type: application/pdf; ");
		response.setHeader("Content-Disposition", "attachment; filename=ActaSucursalExpansion.pdf");
		response.setHeader("Pragma: no-cache", "Expires: 0");
		sos = response.getOutputStream();

		Path pdfPath = Paths.get(pdfNuevo);
		byte[] pdfNuevoBytes;
		try {
			pdfNuevoBytes = Files.readAllBytes(pdfPath);
			sos.write(pdfNuevoBytes);
		} catch (NoSuchFileException e) {
			logger.info("No se encontro Firma " + pdfNuevo);
		}

		// c.generaActa("311","1", "23/10/2019", "9321", "MEGA DURANGO 20 Nov", "0",
		// "Norte", "Centro", imperdonables, firmas, "No aperturable", "fachada");

		return null;
	}

	//
	@RequestMapping(value = { "central/vistaAtiendeAdicionalExpansion.htm" }, method = RequestMethod.GET)
	public ModelAndView vistaAtiendeAdicionalExpansion(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "bitaPadre", required = true, defaultValue = "") String bitaPadre,
			@RequestParam(value = "idPreg", required = true, defaultValue = "") String idPreg)
			throws ServletException, IOException {

		String rutaAdicional = "";
		String imagenAntesAdicional64 = "";
		String carrusel = "";
		String comentarios = "";
		String pregunta = "";

		List<AdicionalesDTO> lista = null;
		AdicionalesDTO adicionalDetalle = null;

		if (bitaPadre != null && !bitaPadre.isEmpty() && idPreg != null && !idPreg.isEmpty()) {

			try {
				lista = adicionalesExpBI.obtieneDatosBita(bitaPadre, idPreg);
			} catch (Exception e) {
				logger.info("error al obtener el detalle adicional");
			}

			if (lista != null && lista.size() > 0) {
				for (AdicionalesDTO a : lista) {
					adicionalDetalle = a;
				}

				comentarios = (adicionalDetalle.getObs() != null && !adicionalDetalle.getObs().isEmpty())
						? adicionalDetalle.getObs()
						: "";
				pregunta = (adicionalDetalle.getPregunta() != null && !adicionalDetalle.getPregunta().isEmpty())
						? adicionalDetalle.getPregunta()
						: "";

				if (adicionalDetalle.getRuta() != null && !adicionalDetalle.getRuta().isEmpty()) {
					try {
						rutaAdicional = adicionalDetalle.getRuta();
						byte[] evidencia = Files.readAllBytes(Paths.get(rutaAdicional));
						imagenAntesAdicional64 = new String(Base64.encodeBase64(evidencia), "UTF-8");
					} catch (NoSuchFileException e) {
						logger.info("No se encontro Evidencia en el servidor" + rutaAdicional);
						imagenAntesAdicional64 = "";
					}
				}
			}
		}

		carrusel += "<div class=\"cuadrobigUnic\" id=\"cuadro1\">" + "<div id=\"txtMdl1\" class=\"txtMdl spBold\">"
				+ "Item <span class=\"imgNum\">1</span> /2" + "</div>" + "<div class=\"pad-slider\">"
				+ "<div id=\"owl-carousel1\" class=\"owl-carousel owl-theme\">" + "<div class=\"item\">";
		if (imagenAntesAdicional64 != null && !imagenAntesAdicional64.isEmpty()) {
			carrusel += "                     <img src=\"data:image/png;base64," + imagenAntesAdicional64 + "\">";
		} else {
			carrusel += "                     <img src=\"../images/expancion/fotoBig.svg\">";
		}
		carrusel += "</div> </div> </div> </div>";

		// data:image/png;base64," + encodedA + "'

		ModelAndView mv = new ModelAndView("vistaAtiendeAdicionalExpansion");

		mv.addObject("carrusel", carrusel);
		mv.addObject("comentarios", comentarios);
		mv.addObject("pregunta", pregunta);

		/*
		 * mv.addObject("hallazgo",hallazgo); mv.addObject("urlServer",
		 * FRQConstantes.getURLServer());
		 * 
		 * mv.addObject("imagen",imagen); mv.addObject("pregunta",pregunta);
		 * mv.addObject("comentarios",comentarios);
		 * 
		 * mv.addObject("ceco",idCeco); mv.addObject("idResp",idResp);
		 * mv.addObject("status",status); mv.addObject("respuesta",respuesta);
		 * mv.addObject("ruta",ruta);
		 * mv.addObject("fechaAutorizacion",fechaAutorizacion);
		 * mv.addObject("fechaFin",fechaFin);
		 */
		return mv;
	}
	
	@SuppressWarnings("unused")
	@RequestMapping(value = { "central/generaHallazgosCore.htm" }, method = RequestMethod.GET)
	public ModelAndView generaHallazgosCore(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "ceco", required = true, defaultValue = "") String ceco,
			@RequestParam(value = "fase", required = true, defaultValue = "") String fase,
			@RequestParam(value = "proyecto", required = true, defaultValue = "") String proyecto)
			throws ServletException, IOException, ParseException {

		List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();   
		List<HallazgosTransfDTO> listaEvidencias = new ArrayList<HallazgosTransfDTO>();    
		
      	listaHallazgos.clear();
      	listaEvidencias.clear();
		
		listaHallazgos = hallazgosTransfBI.obtieneDatos(ceco, fase, proyecto);
      	listaEvidencias = hallazgosTransfBI.obtieneDatosBita(Integer.parseInt(proyecto),Integer.parseInt(fase),Integer.parseInt(ceco));
      	 
		GeneraExcelDetalleExpansionBI ge = new GeneraExcelDetalleExpansionBI();
		Workbook hallazgosWorkbook = ge.generaHallazgosCore(fase, listaHallazgos, listaEvidencias, ceco, sucursalChecklistBI);

		// Write the output to a file
		ByteArrayOutputStream arrayBytesOutStream = new ByteArrayOutputStream();
		hallazgosWorkbook.write(arrayBytesOutStream);

		ServletOutputStream sos = null;
		String archivo = "";

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
		response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
		response.setHeader("Content-Disposition",
				"attachment; filename=hallazgos_Ceco_" + ceco + "_fase_" + fase + "_" + ".xls");
		response.setHeader("Pragma: no-cache", "Expires: 0");
		sos = response.getOutputStream();

		sos.write(arrayBytesOutStream.toByteArray());

		// Closing the workbook
		hallazgosWorkbook.close();

		return null;
	}
	
	// http://localhost:8080/checklist/central/sendCorreoAperturaCore.json?idUsuario=<?>
	@SuppressWarnings("unused")
		@RequestMapping(value = "central/sendCorreoAperturaCoreDirecto", method = RequestMethod.GET)
		public ModelAndView sendCorreoAperturaCoreDirecto(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "tipoVisita", required = true, defaultValue = "0") String tipoVisita,
			@RequestParam(value = "fechaVisita", required = true, defaultValue = "0") String fechaVisita,
			@RequestParam(value = "economico", required = true, defaultValue = "0") String economico,
			@RequestParam(value = "sucursal", required = true, defaultValue = "0") String sucursal,
			@RequestParam(value = "tipo", required = true, defaultValue = "0") String tipo,
			@RequestParam(value = "territorio", required = true, defaultValue = "0") String territorio,
			@RequestParam(value = "zona", required = true, defaultValue = "0") String zona,
			@RequestParam(value = "aperturable", required = true, defaultValue = "0") String aperturable,
			@RequestParam(value = "tipoNegocio", required = true, defaultValue = "0") String tipoNegocio,			
			@RequestParam(value = "ceco", required = true, defaultValue = "0") int ceco,
			@RequestParam(value = "fase", required = true, defaultValue = "0") int fase,
			@RequestParam(value = "proyecto", required = true, defaultValue = "0") int proyecto
				)throws ServletException, IOException {

			
			String rutaImg = "/franquicia/firmaChecklist/firmalogo_fachada.png";
			List<String> imperdonablesLista = new ArrayList<String>();
			try {
				
				// Firmas ***NUEVO***
				List<FirmaCheckDTO> firmasB = new ArrayList<>();
				firmasB = firmaCheckBI.obtieneDatosFirmaCecoFaseProyecto(ceco + "", fase + "", proyecto + "");

				// Respuestas NO ***NUEVO***
				List<ReporteChecksExpDTO> incidencias = new ArrayList<>();
				incidencias = reporteChecksBI.obtienePregNoImpCecoFaseProyecto(ceco + "", fase + "", proyecto + "");

				if (firmasB.get(0).getCeco() != null && firmasB.get(0).getCeco() != "") {
					try {
						List<SucursalChecklistDTO> lista = sucursalChecklistBI
								.obtieneDatos(Integer.parseInt(firmasB.get(0).getCeco()));

						if (lista != null) {
							for (SucursalChecklistDTO sucursalChecklistDTO : lista) {
								if (sucursalChecklistDTO != null && sucursalChecklistDTO.getRuta() != null
										&& !sucursalChecklistDTO.getRuta().equals("")) {
									rutaImg = sucursalChecklistDTO.getRuta();
									// break;
								}
							}
						}

					} catch (Exception e) {
						logger.info("SIN IMAGEN DE FACHADA DE SUCURSALES ");
					}

				}

				try {

					// ***NUEVO***
					correobi.sendMailExpansionNuevoFormatoConteoCore(0 + "", tipoVisita, fechaVisita,
							economico, sucursal, tipo, territorio, zona, imperdonablesLista, firmasB, incidencias,
							aperturable, rutaImg, tipoNegocio, ceco, fase, proyecto);

				} catch (Exception e) {
					logger.info(e);
					logger.info("AP AL ENVIAR EL EXPANSION ANTERIOR" + e);
					return null;
				}

		} catch (Exception e) {
			logger.info(e);
			return null;
		}

		logger.info("SALI sendCorreoAperturaCOREDIRECTO");
		return null;
	}

}
