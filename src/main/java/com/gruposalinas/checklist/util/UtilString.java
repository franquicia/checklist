package com.gruposalinas.checklist.util;

public class UtilString {
	public static String cleanParameter(String parameter){
		if(parameter != null){
			if (parameter.matches("\\d{4}/\\d{2}/\\d{2}") || parameter.matches("\\d{2}/\\d{2}/\\d{4}")){
			parameter = parameter.replaceAll(":", "").replaceAll("\\\\", "").replaceAll("\\.", "");
			parameter = parameter.replaceAll("%2e", "").replaceAll("%2E", "").replaceAll("%2f", "").replaceAll("%2F", "").replaceAll("%5c", "").replaceAll("%5C", "");
			} else {
			parameter = parameter.replaceAll(":", "").replaceAll("/", "").replaceAll("\\\\", "").replaceAll("\\.", "");
			parameter = parameter.replaceAll("%2e", "").replaceAll("%2E", "").replaceAll("%2f", "").replaceAll("%2F", "").replaceAll("%5c", "").replaceAll("%5C", "");
			}
		}

		return parameter;
	}
	
	public static String cleanFilename(String filename){
		if(filename != null){
			filename = filename.replaceAll(":", "").replaceAll("/", "").replaceAll("\\\\", "").replaceAll("\\.", "");
			filename = filename.replaceAll("%2e", "").replaceAll("%2E", "").replaceAll("%2f", "").replaceAll("%2F", "").replaceAll("%5c", "").replaceAll("%5C", "");
			filename = filename.replace( '\n', '_' ).replace( '\r', '_' );
		}

		return filename;
	}
	
	public static String normalize(String input) {
		String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
		String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
		String output = input;
		for (int i=0; i<original.length(); i++) {
			output = output.replace(original.charAt(i), ascii.charAt(i));
		}
		return output;
	}
	
	public static String getFileExtension(String fileName) {
		String extensionArchivo;
		int dotPos = fileName.lastIndexOf(".");
		extensionArchivo = fileName.substring(dotPos);
		return extensionArchivo;
	}
}
