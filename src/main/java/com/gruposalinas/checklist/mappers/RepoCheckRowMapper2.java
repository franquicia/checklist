package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.RepoCheckDTO;

public class RepoCheckRowMapper2 implements RowMapper<RepoCheckDTO> {

	public RepoCheckDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		//REPORTE PARA CONSULTA DE USUARIOS
		RepoCheckDTO checklist = new RepoCheckDTO();
		
		checklist.setIdUsu(rs.getInt("FIID_USUARIO"));
		checklist.setNombre(rs.getString("FCNOMBRE"));
		checklist.setCeco(rs.getString("FCID_CECO"));
		checklist.setFechaFin(rs.getString("DIFERENCIA"));
		
	
		return checklist;
	}

}


