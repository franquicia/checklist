<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="../css/bootstrap/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap-table/bootstrap-table.min.css">
	<link rel="stylesheet" type="text/css" href="../css/jQuery/jquery-ui.css">

	<script	src="../js/jquery/jquery-2.2.4.min.js"></script>
	<script	src="../js/bootstrap.min.js"></script>
	<script	src="../js/bootstrap-table.min.js"></script>
	<script	src="../js/jquery/jquery-ui.js"></script>

	<script	src="../js/script-list-url.js"></script>

	<style>
		.no-close .ui-dialog-titlebar-close {
			display: none;
		}
	</style>

	<title>Soporte - Analiza lista de URLs</title>
</head>
<body>

	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuSoporte.jsp" flush="true">
		<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuSoporte.jsp" />
	</tiles:insertTemplate>

	<div id="loadingDialog" title="loading..." style="display:none;">
		<p>Análisando las URLs insertadas, por favor espere...</p>
	</div>

	<div id="container">
		<div id="container">
			<p class="text-center lead"><strong><br> <small>Bienvenido</small></strong></p>	
		</div>
		
		<div class="container">
			<textarea id="urls" name="urls" class="form-control" rows="6" placeholder="Pega las URL, una por línea"></textarea>
			<br>
			<form class="form-inline">
				<input type="button" id="test" title="test" name="test" value="Ejecutar" class="btn btn-primary" onclick="return checkExecute();" > &thinsp;
				<input type="checkbox" id="checkbox" title="checkbox" name="checkbox" checked> Detener análisis si ocurre un error
			</form>

			<div class="container" id="tableURL">
				<div class="container">
					<table	id="table"
							class="table table-striped table-bordered table-hover table-condensed" 
							data-toggle="table" 
							data-sort-name="url" 
							data-sort-order="asc"
							data-pagination="true"
							data-pagination-loop="true"
							data-page-size="25"
							data-height="480"
							data-smart-display="true"
							data-pagination-v-align="top"
							data-search="true"
							data-show-refresh="true"
							data-show-toggle="true"
							data-show-columns="true"
							data-show-pagination-switch="true"
							data-row-style="rowStyle">
						<thead>
							<tr>
								<th data-field="cUrl" data-sortable="true" data-searchable="true">URL</th>
								<th data-field="cStatus" data-sortable="true" data-searchable="true">Status</th>
								<th data-field="cContentType" data-sortable="true" data-searchable="true">Content-Type</th>
								<th data-field="cResponseBody" data-sortable="true" data-searchable="true">Response Body</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>