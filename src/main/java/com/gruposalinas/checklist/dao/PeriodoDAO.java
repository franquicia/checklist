package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.PeriodoDTO;


public interface PeriodoDAO {
	public List<PeriodoDTO> obtienePeriodo(String idPeriodo) throws Exception;
	
	public int insertaPeriodo(PeriodoDTO bean) throws Exception;
	
	public boolean actualizaPeriodo(PeriodoDTO bean) throws Exception;
	
	public boolean eliminaPeriodo(int idPeriodo) throws Exception;

}
