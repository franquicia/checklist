package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.TokenDTO;
import com.gruposalinas.checklist.mappers.TokenRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class TokenDAOImpl extends DefaultDAO implements TokenDAO {

    private static Logger logger = LogManager.getLogger(TokenDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaToken;
    private DefaultJdbcCall jdbcActualizaToken;
    private DefaultJdbcCall jdbcEliminaToken;
    private DefaultJdbcCall jdbcEliminaTodosToken;
    private DefaultJdbcCall jdbcBuscaToken;
    private DefaultJdbcCall jdbcBuscaTokenG;
    private DefaultJdbcCall jdbcBuscaTokens;

    public void init() {

        jdbcInsertaToken = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TOKEN")
                .withProcedureName("SP_INS_TOKEN");

        jdbcActualizaToken = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TOKEN")
                .withProcedureName("SP_ACT_TOKEN");

        jdbcEliminaToken = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TOKEN")
                .withProcedureName("SP_DEL_TOKEN");

        jdbcEliminaTodosToken = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TOKEN")
                .withProcedureName("SP_DEL_ALL_TOKEN");

        jdbcBuscaTokenG = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TOKEN")
                .withProcedureName("SP_SELDATE_TOKEN")
                .returningResultSet("RCL_TOKEN", new TokenRowMapper());

        jdbcBuscaToken = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TOKEN")
                .withProcedureName("SP_SEL_TOKEN")
                .returningResultSet("RCL_TOKEN", new TokenRowMapper());

        jdbcBuscaTokens = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TOKEN")
                .withProcedureName("SP_SEL_G_TOKEN")
                .returningResultSet("RCL_TOKEN", new TokenRowMapper());
    }

    public int insertaToken(TokenDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        int idToken = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDUSUARIO", bean.getUsuario())
                .addValue("PA_FCTOKEN", bean.getToken())
                .addValue("PA_FIESTATUS", bean.getEstatus());

        out = jdbcInsertaToken.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_TOKEN.SP_INS_TOKEN}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal intReturn = (BigDecimal) out.get("PA_IDTOKEN");
        idToken = intReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar el Token : " + bean.getToken());
        } else {
            return idToken;
        }

        return idToken;
    }

    public boolean actualizaToken(TokenDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_TOKEN", bean.getIdToken())
                .addValue("PA_IDUSUARIO", bean.getUsuario())
                .addValue("PA_FCTOKEN", bean.getToken())
                .addValue("PA_FIESTATUS", bean.getEstatus());

        out = jdbcActualizaToken.execute(in);

        logger.info("Funcion ejecutada: Actualiza Token");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("AP UPDATE TOKEN");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaTokenTodos() throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        out = jdbcEliminaTodosToken.execute();

        logger.info("Funcion ejecutada: {checklist.PA_ADM_TOKEN.SP_DEL_ALL_TOKEN}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al elimnar el Token ");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaToken(int idToken) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_TOKEN", idToken);

        out = jdbcEliminaToken.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_TOKEN.SP_DEL_TOKEN}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al elimnar el Token ");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<TokenDTO> buscaToken(String token) throws Exception {
        Map<String, Object> out = null;
        List<TokenDTO> listaToken = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FCTOKEN", token);

        out = jdbcBuscaToken.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_TOKEN.SP_SEL_TOKEN}");

        listaToken = (List<TokenDTO>) out.get("RCL_TOKEN");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {

            String clean = token.replace('\n', '_').replace('\r', '_');

            if (!token.equals(clean)) {
                clean += " (PRECAUCIÓN AL LEER EL LOG)";
            }

            logger.info("Algo paso al consultar el Token : " + clean);
        } else {
            return listaToken;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<TokenDTO> buscaTokenF(String fecha) throws Exception {
        Map<String, Object> out = null;
        List<TokenDTO> listaToken = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FECHA", fecha);

        out = jdbcBuscaTokenG.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_TOKEN.SP_SEL_TOKEN}");

        listaToken = (List<TokenDTO>) out.get("RCL_TOKEN");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al consultar los Token ");
        } else {
            return listaToken;
        }

        return null;

    }

    @SuppressWarnings("unchecked")
    public List<TokenDTO> buscaToken() throws Exception {

        Map<String, Object> out = null;
        List<TokenDTO> listaToken = null;
        int error = 0;

        out = jdbcBuscaTokens.execute();

        logger.info("Funcion ejecutada: {checklist.PA_ADM_TOKEN.SP_SEL_G_TOKEN}");

        listaToken = (List<TokenDTO>) out.get("RCL_TOKEN");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al consultar los Token ");
        } else {
            return listaToken;
        }

        return null;
    }

}
