var urlServer =$(location).attr('host');

var editando=false; 
  function transformarEnEditable(nodo,idArbolVista){ 	   
	  
	  if(editando == false){
		  
		  var nodoTd = nodo.parentNode;
		  var nodoTr = nodoTd.parentNode;
		  var nodoContenedorForm = document.getElementById('contenedorForm'); 

		  var nodoEnTr = nodoTr.getElementsByTagName('td');
		  var idArbol = nodoEnTr[0].textContent;
		  var idChecklist = nodoEnTr[1].textContent.trim();
		  var idPregunta = nodoEnTr[2].textContent;
		  var respuesta = nodoEnTr[3].textContent;
		  var evidencia = nodoEnTr[4].textContent;
		  var siguientePregunta = nodoEnTr[5].textContent;
		  var accion = nodoEnTr[6].textContent;
		  var observaciones = nodoEnTr[7].textContent;
		  var evidenciaObli = nodoEnTr[8].textContent;
		  var etiquetaEvi = nodoEnTr[9].textContent;
		  var opcEditar = nodoEnTr[10].textContent;
		
		  var nuevoHTML = '<td><input type="text" name="idArbol" id="idArbol" value="'+idArbol+'"size="10"></td> '+
		                  '<td><input type="text" name="idChecklist" id="idChecklist" value="'+idChecklist+'"size="10"></td> '+
		                  '<td><input type="text" name="idPregunta" id="idPregunta" value="'+idPregunta+'"size="10"></td> '+
		                  '<td><input type="text" name="respuesta" id="respuesta" value="'+respuesta+'"size="10"></td> '+
		                  '<td><input type="text" name="evidencia"  id="evidencia"value="'+evidencia+'"size="10"></td> '+
		                  '<td><input type="text" name="siguientePregunta" id="siguientePregunta" value="'+siguientePregunta+'"size="10"></td> '+
		                  '<td><input type="text" name="accion" id="accion" value="'+accion+'"size="10"></td> '+
		                  '<td><input type="text" name="observaciones" id="observaciones" value="'+observaciones+'"size="10"></td> '+
		                  '<td><input type="text" name="evidenciaObli" id="evidenciaObli" value="'+evidenciaObli+'"size="10"></td> '+
		                  '<td><input type="text" name="etiquetaEvi" id="etiquetaEvi" value="'+etiquetaEvi+'"size="10"> </td> '+		                  
		                  '<td align="center"><input type="checkbox"  id="btnActualizar" checked="checked" onclick="myClick()"> Editando </td>'
		                  ;
		  nodoTr.innerHTML = nuevoHTML;

// 		  nodoContenedorForm.innerHTML = 'Pulse Aceptar para guardar los cambios o cancelar para anularlos '+
// 		  '<form name = "formulario" method="get" onsubmit="return capturarEnvio();" '+
// 		  'onreset="anular()"> '+
// 		  '<input class="boton" type = "submit" value="Aceptar"> <input class="boton" type="reset" value="Cancelar">' 
// 		  +'</form>';
		  editando = "true";
	  } 		  
	else {
		    $("#"+idArbolVista).attr('checked', false);
			alert('Solo se puede editar una línea. Recargue la página para poder editar otra');
		}

	  return false;
	}

  function capturarEnvio() {
		$
		.ajax({
			type : "GET",
			url : "http://"+urlServer + "../checklistServices/updateArbolDesicionService.json?"+
	        "idArbolDesicion="+$('#idArbol').val().trim()+
	        "&estatusEvidencia="+$('#evidencia').val().trim()+
	        "&idCheck="+$('#idChecklist').val().trim()+
	        "&idPreg="+$('#idPregunta').val().trim()+
	        "&idOrdenCheckRes="+$('#siguientePregunta').val().trim()+
	        "&setRes="+$('#respuesta').val().trim()+
	        "&reqAccion="+$('#accion').val().trim()+
	        "&reqObs="+$('#observaciones').val().trim()+
	        "&reqOblig="+$('#evidenciaObli').val().trim()+
	        "&etiquetaEvidencia="+$('#etiquetaEvi').val().trim(),
			        
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			paramName : "tagName",
			delimiter : ",",
			success : function(json) {
				//alert("ID PREG: "+json);
				
				if(json==true){
					alert("Se modificó correctamente el orden de la pregunta");									
				}else{
					alert('Algo paso al modificar el orden de la pregunta ');				
				}				
			},
			error : function() {
				alert('Algo paso al modificar el orden de la pregunta');
				
			},
			async:false
			
		});
		
	   return false;
       
	  } 

  function anular() {
	  window.location.reload();
	  } 

   function myClick(){
	    capturarEnvio();
		window.location.reload();
	    
	   }
  