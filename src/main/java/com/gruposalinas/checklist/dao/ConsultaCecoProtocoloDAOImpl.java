package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ConsultaCecoProtocoloDTO;
import com.gruposalinas.checklist.mappers.ConsultaCecoProtocoloBitacoraRowMapper;
import com.gruposalinas.checklist.mappers.ConsultaCecoProtocoloRowMapper;
import com.gruposalinas.checklist.mappers.ConsultaDetalleRowMapper;
import com.gruposalinas.checklist.mappers.ConsultaNombreProtocoloRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ConsultaCecoProtocoloDAOImpl extends DefaultDAO implements ConsultaCecoProtocoloDAO {

    private static Logger logger = LogManager.getLogger(ConsultaCecoProtocoloDAOImpl.class);

    DefaultJdbcCall jdbcObtieneConsulta;
    DefaultJdbcCall jdbcObtieneConsulta2;
    DefaultJdbcCall jdbcObtieneConsulta3;
    DefaultJdbcCall jdbcObtieneConsulta4;

    public void init() {

        jdbcObtieneConsulta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAGETCHECKCECO2")
                .withProcedureName("SPGETCHKCECO2")
                .returningResultSet("PA_CONSULTA", new ConsultaCecoProtocoloRowMapper());

        jdbcObtieneConsulta2 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAGETCHECKCECO2")
                .withProcedureName("SPGETBITACORA2")
                .returningResultSet("PA_CONSULTA", new ConsultaCecoProtocoloBitacoraRowMapper());

        jdbcObtieneConsulta3 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAGETCHECKCECO2")
                .withProcedureName("SPNOMCHECKID2")
                .returningResultSet("PA_CONSULTA", new ConsultaNombreProtocoloRowMapper());

        jdbcObtieneConsulta4 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAGETCHECKCECO2")
                .withProcedureName("SPDETALLE")
                .returningResultSet("PA_CONSULTA", new ConsultaDetalleRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaCecoProtocoloDTO> obtieneConsultaCeco() throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaCecoProtocoloDTO> listaConsulta = null;

        out = jdbcObtieneConsulta.execute();

        logger.info("Función ejecutada: FRANQUICIA.PAGETCHECKCECO2.SPGETCHKCECO2");

        listaConsulta = (List<ConsultaCecoProtocoloDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al consultar el StoreProcedure");
        } else {
            return listaConsulta;
        }

        return listaConsulta;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ConsultaCecoProtocoloDTO> obtieneConsultaBitacora(ConsultaCecoProtocoloDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaCecoProtocoloDTO> listaConsulta2 = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCIDCECO", bean.getIdCeco())
                .addValue("PA_FECHAINICIO", bean.getFechaInicio())
                .addValue("PA_FECHAFIN", bean.getFechaTermino());

        out = jdbcObtieneConsulta2.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAGETCHECKCECO2.SPGETBITACORA2");

        listaConsulta2 = (List<ConsultaCecoProtocoloDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al consultar el StoreProcedure");
        } else {
            return listaConsulta2;
        }

        return listaConsulta2;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ConsultaCecoProtocoloDTO> obtieneConsultaChecklist(ConsultaCecoProtocoloDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaCecoProtocoloDTO> listaConsulta3 = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCIDCECO", bean.getIdCeco())
                .addValue("PA_FECHAFIN", bean.getFechaTermino());

        out = jdbcObtieneConsulta3.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAGETCHECKCECO2.SPNOMCHECKID2");

        listaConsulta3 = (List<ConsultaCecoProtocoloDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo ocurrió al consultar el StoreProcedure");
        } else {
            return listaConsulta3;
        }

        return listaConsulta3;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaCecoProtocoloDTO> obtieneDetalle(ConsultaCecoProtocoloDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaCecoProtocoloDTO> listaConsulta4 = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_BITACORA", bean.getIdBitacora());

        out = jdbcObtieneConsulta4.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAGETCHECKCECO2.SPDETALLE");

        listaConsulta4 = (List<ConsultaCecoProtocoloDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al consultar el StoreProcedure");
        } else {
            return listaConsulta4;
        }
        return listaConsulta4;

    }

}
