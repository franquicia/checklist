package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.RepMedicionDTO;

public class RepMedicionRowMapper implements RowMapper<RepMedicionDTO> {

	public RepMedicionDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		RepMedicionDTO reporteDTO = new RepMedicionDTO();
		reporteDTO.setFecha(rs.getString("Fecha que se Realizo"));
		reporteDTO.setEmpleado(rs.getString("Quien lo realizo"));
		reporteDTO.setIdEmpleado(rs.getInt("Número de Usuario"));
		reporteDTO.setPuesto(rs.getString("Puesto"));
		reporteDTO.setPais(rs.getString("País"));
		reporteDTO.setTerritorio(rs.getString("Territorio"));
		reporteDTO.setZona(rs.getString("Zona"));
		reporteDTO.setRegional(rs.getString("Regional"));
		reporteDTO.setSucursal(rs.getString("Sucursal"));
		reporteDTO.setNomSucursal(rs.getString("Nombre Sucursal"));
		reporteDTO.setCanal(rs.getString("Canal"));
		reporteDTO.setPonderacion(rs.getDouble("Ponderacion"));
		reporteDTO.setCalificacion(rs.getDouble("Calificación"));
		reporteDTO.setContSi(rs.getInt("Conteo de SI"));
		reporteDTO.setContNo(rs.getInt("Conteo de NO"));
		reporteDTO.setContImp(rs.getInt("Imperdonables"));
		//reporteDTO.setDescPregunta(rs.getString("Descripción Preguntas"));
		reporteDTO.setDescRespuesta(rs.getString("Descripción Respuestas"));
		
		return reporteDTO;
		
	}
}
