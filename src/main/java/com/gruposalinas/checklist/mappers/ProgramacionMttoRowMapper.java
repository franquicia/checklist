package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.AsignaTransfDTO;
import com.gruposalinas.checklist.domain.ProgramacionMttoDTO;

public class ProgramacionMttoRowMapper implements RowMapper<ProgramacionMttoDTO> {

	public ProgramacionMttoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ProgramacionMttoDTO AsignaTransfDTO = new ProgramacionMttoDTO();
		
		AsignaTransfDTO.setIdProgramacion(rs.getInt("FIID_HISTORICO"));
		AsignaTransfDTO.setIdCeco(rs.getInt("FCID_CECO"));
		AsignaTransfDTO.setNombreCeco(rs.getString("NOMBRECECO"));
		AsignaTransfDTO.setCecoSuperior(rs.getString("CECO_SUP"));
	    AsignaTransfDTO.setIdProyecto(rs.getInt("FIID_PROYECTO"));
	    AsignaTransfDTO.setNombreProyecto(rs.getString("NOMBREPROYECTO"));
	    AsignaTransfDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
	    AsignaTransfDTO.setFechaProgramacion(rs.getString("FCPERIODO"));
	    AsignaTransfDTO.setNegocio(rs.getString("FCNEGOCIO"));
	    AsignaTransfDTO.setfechaProgramacionInicial(rs.getString("FDPERIODO"));

		return AsignaTransfDTO;
	}

}


