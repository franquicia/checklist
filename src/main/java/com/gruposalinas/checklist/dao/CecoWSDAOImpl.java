/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.CecoPasoDTO;
import com.gruposalinas.checklist.domain.ListaTablaEKT;
import com.gruposalinas.checklist.mappers.CecoWSRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author kramireza
 */
public class CecoWSDAOImpl extends DefaultDAO implements CecoWSDAO {

    private final SimpleDateFormat fechas = new SimpleDateFormat("yyyy-MM-dd");
    private SqlParameterSource parametersCeco;
    private Map<String, Object> outInsertCeco;
    private Map<String, Object> outUpdateCeco;
    private Map<String, Object> outDepuraCeco;
    private Map<String, Object> outBuscaCeco;
    private Map<String, Object> outMergeOriginal;

    private static Logger logger = LogManager.getLogger(CecoDAOImpl.class);

    DefaultJdbcCall jdbcConsultaCeco;
    DefaultJdbcCall jdbcInsertaCeco;
    DefaultJdbcCall jdbcActualizaCeco;
    DefaultJdbcCall jdbcDepuraTabla;
    DefaultJdbcCall jdbcMergeOriginal;

    public void init() {

        jdbcConsultaCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PASO_CECO")
                .withProcedureName("SP_SEL_CCPASO")
                .returningResultSet("RCL_INFO", new CecoWSRowMapper());

        jdbcInsertaCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PASO_CECO")
                .withProcedureName("SP_INS_CCPASO");

        jdbcActualizaCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PASO_CECO")
                .withProcedureName("SP_ACT_CCPASO");

        jdbcDepuraTabla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PASO_CECO")
                .withProcedureName("SP_DEP_TABLA");

        jdbcMergeOriginal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PASO_CECO")
                .withProcedureName("SP_INS_CCPASO_ORIGINAL");

    }

    @Override
    public List<CecoPasoDTO> buscaCeco(String idCeco) throws Exception {
        List<CecoPasoDTO> listaCeco = null;
        parametersCeco = new MapSqlParameterSource().addValue("PA_FCCCID", idCeco);
        outBuscaCeco = jdbcConsultaCeco.execute(parametersCeco);
        //logger.info("Funcion ejecutada:{FRANQUICIA.PA_ADM_PASO_CECO.SP_SEL_CCPASO}");
        listaCeco = (List<CecoPasoDTO>) outBuscaCeco.get("RCL_INFO");
        if (((BigDecimal) outBuscaCeco.get("PA_EJECUCION")).intValue() == 1) {
            logger.info("Algo ocurrio al consultar los CECO PASO");
        } else {
            return listaCeco;
        }
        return null;
    }

    @Override
    public boolean insertaCeco(ListaTablaEKT bean) throws Exception {

        int error = 0;
        parametersCeco = new MapSqlParameterSource()
                .addValue("PA_FCCCID", bean.getIdCC())
                .addValue("PA_FIENTIDADID", bean.getIdEntidad())
                .addValue("PA_FINUM_ECONOMICO", bean.getNumEconomico())
                .addValue("PA_FCNOMBRECC", bean.getNomCC())
                .addValue("PA_FCNOMBRE_ENT", bean.getNomEntidad())
                .addValue("PA_FCCCID_PADRE", bean.getIdCCPa())
                .addValue("PA_FIENTDID_PADRE", bean.getIdEntidadPa())
                .addValue("PA_FCNOMBRECC_PAD", bean.getNomCCPa())
                .addValue("PA_FCTIPOCANAL", bean.getIdCanal())
                .addValue("PA_FCNOMBTIPCANAL", bean.getTipoCanal().trim())
                .addValue("PA_FCCANAL", bean.getNomCanal())
                .addValue("PA_FISTATUSCCID", bean.getIdEstatus())
                .addValue("PA_FCSTATUSCC", bean.getNomEstatus())
                .addValue("PA_FITIPOCC", bean.getIdTipoCentro())
                .addValue("PA_FIESTADOID", bean.getIdEstado())
                .addValue("PA_FCMUNICIPIO", bean.getNomMunicipio())
                .addValue("PA_FCTIPOOPERACION", bean.getIdTipoOperacion())
                .addValue("PA_FCTIPOSUCURSAL", bean.getIdTipoSucursal())
                .addValue("PA_FCNOMBRETIPOSUC", bean.getNomTipoSucursal())
                .addValue("PA_FCCALLE", bean.getNomCalle())
                .addValue("PA_FIRESPONSABLEID", bean.getIdResponsable())
                .addValue("PA_FCRESPONSABLE", bean.getNomResponsable())
                .addValue("PA_FCCP", bean.getCodigoPostal())
                .addValue("PA_FCTELEFONOS", bean.getTelefono())
                .addValue("PA_FICLASIF_SIEID", bean.getIdEstructuracve())
                .addValue("PA_FCCLASIF_SIE", bean.getDesEstructuracve())
                .addValue("PA_FDAPERTURA", (bean.getFechaApertura() != null ? fechas.parse(bean.getFechaApertura()) : null))
                .addValue("PA_FDCIERR", bean.getFechaCierre() != null ? fechas.parse(bean.getFechaCierre()) : null)
                .addValue("PA_FIPAISID", bean.getIdPais())
                .addValue("PA_FCPAIS", bean.getNomPais())
                .addValue("PA_FDCARGA", bean.getFechaCreacion() != null ? fechas.parse(bean.getFechaCreacion()) : null)
                .addValue("PA_FIIDUNNEGO", bean.getIdUnidadNegocio())
                .addValue("PA_FCDESCUNBNEGO", bean.getNomunidadnegocio())
                .addValue("PA_FIIDSUBNEGO", bean.getIdSubnegocio())
                .addValue("PA_FCDESCSUBNEGO", bean.getDesSubnegocio());

        outInsertCeco = jdbcInsertaCeco.execute(parametersCeco);
        if (((BigDecimal) outInsertCeco.get("PA_EJECUCION")).intValue() == 0) {
            logger.info("Algo ocurrió al insertar CECO " + bean.toString());
        } else if (((BigDecimal) outInsertCeco.get("PA_EJECUCION")).intValue() == 3) {
            logger.info("No se pudo borrar la tabla de PASO CECO");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean actualizaCeco(ListaTablaEKT bean) throws Exception {
        int error = 0;
        parametersCeco = new MapSqlParameterSource()
                .addValue("PA_FCCCID", bean.getIdCC())
                .addValue("PA_FIENTIDADID", bean.getIdEntidad())
                .addValue("PA_FINUM_ECONOMICO", bean.getNumEconomico())
                .addValue("PA_FCNOMBRECC", bean.getNomCC())
                .addValue("PA_FCNOMBRE_ENT", bean.getNomEntidad())
                .addValue("PA_FCCCID_PADRE", bean.getIdCCPa())
                .addValue("PA_FIENTDID_PADRE", bean.getIdEntidadPa())
                .addValue("PA_FCNOMBRECC_PAD", bean.getNomCCPa())
                .addValue("PA_FCTIPOCANAL", bean.getIdCanal())
                .addValue("PA_FCNOMBTIPCANAL", bean.getTipoCanal())
                .addValue("PA_FCCANAL", bean.getNomCanal())
                .addValue("PA_FISTATUSCCID", bean.getIdEstatus())
                .addValue("PA_FCSTATUSCC", bean.getNomEstatus())
                .addValue("PA_FITIPOCC", bean.getIdTipoCentro())
                .addValue("PA_FIESTADOID", bean.getIdEstado())
                .addValue("PA_FCMUNICIPIO", bean.getNomMunicipio())
                .addValue("PA_FCTIPOOPERACION", bean.getIdTipoOperacion())
                .addValue("PA_FCTIPOSUCURSAL", bean.getIdTipoSucursal())
                .addValue("PA_FCNOMBRETIPOSUC", bean.getNomTipoSucursal())
                .addValue("PA_FCCALLE", bean.getNomCalle())
                .addValue("PA_FIRESPONSABLEID", bean.getIdResponsable())
                .addValue("PA_FCRESPONSABLE", bean.getNomResponsable())
                .addValue("PA_FCCP", bean.getCodigoPostal())
                .addValue("PA_FCTELEFONOS", bean.getTelefono())
                .addValue("PA_FICLASIF_SIEID", bean.getIdEstructuracve())
                .addValue("PA_FCCLASIF_SIE", bean.getDesEstructuracve())
                .addValue("PA_FDAPERTURA", bean.getFechaApertura() != null ? fechas.parse(bean.getFechaApertura()) : null)
                .addValue("PA_FDCIERR", bean.getFechaCierre() != null ? fechas.parse(bean.getFechaCierre()) : null)
                .addValue("PA_FIPAISID", bean.getIdPais())
                .addValue("PA_FCPAIS", bean.getNomPais())
                .addValue("PA_FDCARGA", bean.getFechaCreacion() != null ? fechas.parse(bean.getFechaCreacion()) : null)
                .addValue("PA_FIIDUNNEGO", bean.getIdUnidadNegocio())
                .addValue("PA_FCDESCUNBNEGO", bean.getNomunidadnegocio())
                .addValue("PA_FIIDSUBNEGO", bean.getIdSubnegocio())
                .addValue("PA_FCDESCSUBNEGO", bean.getDesSubnegocio());

        outUpdateCeco = jdbcActualizaCeco.execute(parametersCeco);
        if (((BigDecimal) outUpdateCeco.get("PA_EJECUCION")).intValue() == 1) {
            logger.info("Algo ocurrio al actualizar el CECO PASO");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean depuraTabla(String nombreTabla) throws Exception {
        int error = 0;
        parametersCeco = new MapSqlParameterSource().addValue("PA_NOMBTABLA", nombreTabla);
        outDepuraCeco = jdbcDepuraTabla.execute(parametersCeco);
        //logger.info("Funcion ejecutada:{FRANQUICIA.PA_ADM_PASO_CECO.SP_DEP_TABLA}");
        if (((BigDecimal) outDepuraCeco.get("PA_EJECUCION")).intValue() == 1) {
            return true;
        } else {
            logger.info("Algo ocurrio al depurar la tabla de CECO PASO");
            return false;
        }
    }

    @Override
    public boolean mergeOriginal() throws Exception {
        outMergeOriginal = jdbcMergeOriginal.execute();
        if (((BigDecimal) outMergeOriginal.get("PA_EJECUCION")).intValue() == 0) {
            logger.info("Algo ocurrió al hacer el merge de la tabla TAPASO_CECO al FRTPPASO_CECO ");
        } else {
            return true;
        }
        return false;
    }

}
