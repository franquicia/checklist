package com.gruposalinas.checklist.business;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.dao.ArchiveDAO;
import com.gruposalinas.checklist.dao.GridFSDAO;
import com.gruposalinas.checklist.domain.Archive;
import com.gruposalinas.checklist.domain.UploadArchive;
import com.gruposalinas.checklist.util.UtilFRQ;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;


public class CargaBI {
	/*
	private static Logger logger = LogManager.getLogger(CargaBI.class);

	@Autowired
	private GridFSDAO fileStorage;
	@Autowired
	private ArchiveDAO archiveParser;

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, String titulo,
			String descripcion, MultipartFile file) throws IOException {
		ModelAndView mv = new ModelAndView("cargaForm", "command", new UploadArchive());
		
		if( file.getSize() > 10485760 ) { 
			mv.addObject("response", "El tamaño máximo permitido para el archivo es de 10MB.");
			return mv;
		}
		
		// VERIFICAR QUE EL ARCHIVO ESTE BIEN FORMADO
        if (file.getContentType().toString().equalsIgnoreCase("application/pdf")) {
        	try{
        		InputStream is = file.getInputStream();
        		try {
	                PdfReader verificaPDF = new PdfReader(is);
	                verificaPDF.close();
        		} finally {
        			is.close();
        		}
                
             // TIPO DE ARCHIVO
        		file.getContentType();
        		// NOMBRE ORIGINAL DEL ARCHIVO
        		file.getOriginalFilename();
        		InputStream archive = file.getInputStream();
        		DBObject metaData = new BasicDBObject();
    			metaData.put("fileName", file.getOriginalFilename());
    			metaData.put("description", descripcion);
    			metaData.put("title", titulo);
        		
                try {

					fileStorage.store(archive, file.getOriginalFilename(), "application/pdf", metaData);
					ESTE INPUTSTREAM LO NECESITO PARA REARMAR EL PDF 
					InputStream savedArchive = fileStorage.getByArchiveName(file.getOriginalFilename());
					try {
						// Se crea instancia de PdfReader.
						PdfReader pdfReader = new PdfReader(savedArchive);
						// Se obiene el número de páginas del pdf.
						int pages = pdfReader.getNumberOfPages();
						//LocationTextExtractionStrategy
						//LocationTextExtractionStrategy th = new LocationTextExtractionStrategy();
						// Se itera a través de las páginas.
						for (int i = 1; i <= pages; i++) {
							// Se extrae el contenido usando: PdfTextExtractor.
							String pageContent = PdfTextExtractor.getTextFromPage(pdfReader, i);
							// Imprimo en consola.
							//System.out.println("Content page: " + i + ": " + pageContent.trim());
							// En las siguientes dos líneas se realiza la inserción
							// página por página en una colección de MongoDB
							Archive archive1 = new Archive(file.getOriginalFilename(), titulo, pageContent.trim(), i);
							archiveParser.addArchive(archive1);
							mv.addObject("response", "ARCHIVO PDF CARGADO CON ÉXITO");
						}
						// Se cierra PdfReader.
						pdfReader.close();
					} catch (Exception e) {
						logger.info("Ocurrio algo...");
					} finally {
						savedArchive.close();
					}
					
				} catch (Exception e) {
					mv.addObject("response", "ALGO OCURRIÓ AL CARGAR EL ARCHIVO");
				} finally {
					archive.close();

				}
               
        	} catch (Exception e) {
        		mv.addObject("response", "ALGO OCURRIÓ CON EL FORMATO DE ARCHIVO");
        	}
        	
        } else {
            mv.addObject("response", "FORMATO DE ARCHIVO NO VALIDO");
        }
		return mv;
	}*/
}
/*
 * //ESTE METODO AGREGA PARRAFO DE UN COLOR DISTINTO AL CREAR UN NUEVO PDF // Se
 * crea el documento Document documento = new Document(); // Se crea el
 * OutputStream para el fichero donde queremos dejar el pdf. FileOutputStream
 * ficheroPdf = new FileOutputStream("/Users/b189871/Desktop/creado.pdf"); // Se
 * asocia el documento al OutputStream y se indica que el espaciado entre //
 * lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
 * PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20); // Se abre
 * el documento. documento.open(); documento.add(new Paragraph(
 * "Esto es el primer párrafo, normalito"));
 * 
 * documento.add(new Paragraph("Este es el segundo y tiene una fuente rara",
 * FontFactory.getFont("arial", // fuente 22, // tamaño Font.ITALIC, // estilo
 * BaseColor.RED))); // color documento.close();
 * 
 */

/*
 * else if
 * (file.getContentType().toString().equalsIgnoreCase("application/msword")) {
 * 
 * try { fileStorage.store(archive, file.getOriginalFilename(),
 * file.getContentType(), metaData); //ESTE INPUTSTREAM LO NECESITO PARA REARMAR
 * EL DOCUMENTO InputStream savedArchive =
 * fileStorage.getByArchiveName(file.getOriginalFilename()); InputStream
 * entradaDoc = savedArchive; // W O R D D O C HWPFDocument archDoc = new
 * HWPFDocument(entradaDoc); WordExtractor extDoc = new WordExtractor(archDoc);
 * String texto = extDoc.getText(); System.out.println(extDoc +
 * "GET TEXT FROM PIECES"); int i = 1; String[] paragraphText =
 * extDoc.getParagraphText(); for (String paragraph : paragraphText) {
 * System.out.println(paragraph + i); i++; }
 * System.out.println(extDoc.getText()); Archive archive1 = new
 * Archive(file.getOriginalFilename(), titulo, texto, 0);
 * archiveParser.addArchive(archive1); extDoc.close(); } catch (Exception e) {
 * mv.addObject("response", "OCURRIO UN ERROR AL CARGAR EL ARCHIVO"); }
 * 
 * } else if (file.getContentType().toString() .equalsIgnoreCase(
 * "application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
 * 
 * try { fileStorage.store(archive, file.getOriginalFilename(),
 * file.getContentType(), metaData); InputStream entradaDocx =
 * fileStorage.getByArchiveName(file.getOriginalFilename()); XWPFDocument
 * document = new XWPFDocument(entradaDocx); XWPFWordExtractor extDocx = new
 * XWPFWordExtractor(document); String texto = extDocx.getText();
 * mv.addObject("response", "Archivo DOCX cargado con éxito");
 * 
 * } catch (Exception e) { mv.addObject("response",
 * "OCURRIO UN ERROR AL CARGAR EL ARCHIVO"); }
 * 
 * } else if (file.getContentType().toString().equalsIgnoreCase(
 * "application/vnd.ms-powerpoint")) {
 * 
 * try { fileStorage.store(archive, file.getOriginalFilename(),
 * file.getContentType(), metaData); InputStream entradappt =
 * fileStorage.getByArchiveName(file.getOriginalFilename()); HSLFSlideShowImpl
 * archppt = new HSLFSlideShowImpl(entradappt); PowerPointExtractor extppt = new
 * PowerPointExtractor(archppt); String texto = extppt.getText(); Archive
 * archive1 = new Archive(file.getOriginalFilename(), titulo, texto, 0);
 * archiveParser.addArchive(archive1); entradappt.close(); System.out.println(
 * "AL FINAL  !!!!!"); mv.addObject("response", "Archivo PPT cargado con éxito"
 * ); extppt.close(); } catch (Exception e) { mv.addObject("response",
 * "OCURRIO UN ERROR AL CARGAR EL ARCHIVO"); }
 * 
 * } else if (file.getContentType().toString() .equalsIgnoreCase(
 * "application/vnd.openxmlformats-officedocument.presentationml.presentation"))
 * {
 * 
 * try { fileStorage.store(archive, file.getOriginalFilename(),
 * file.getContentType(), metaData); InputStream entradapptx =
 * fileStorage.getByArchiveName(file.getOriginalFilename()); XMLSlideShow
 * archpptx = new XMLSlideShow(entradapptx); XSLFPowerPointExtractor extpptx =
 * new XSLFPowerPointExtractor(archpptx); String texto = extpptx.getText();
 * Archive archive1 = new Archive(file.getOriginalFilename(), titulo, texto, 0);
 * archiveParser.addArchive(archive1); entradapptx.close(); System.out.println(
 * "AL FINAL  !!!!!"); mv.addObject("response", "Archivo PPTX cargado con éxito"
 * ); } catch (Exception e) { System.out.println("HUBO FALLO!!!!1");
 * e.printStackTrace(); mv.addObject("response",
 * "OCURRIO UN ERROR AL CARGAR EL ARCHIVO" ); } }
 */
