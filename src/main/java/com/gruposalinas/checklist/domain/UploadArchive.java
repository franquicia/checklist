package com.gruposalinas.checklist.domain;


import org.springframework.web.multipart.MultipartFile;

public class UploadArchive {

	private String titulo;
	private String descripcion;
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	private MultipartFile archive;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public MultipartFile getArchive() {
		return archive;
	}
	public void setArchive(MultipartFile archive) {
		this.archive = archive;
	}
}
