package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.BitacoraAdministradorDTO;


public class BitacoraAdministradorRowMapper implements RowMapper<BitacoraAdministradorDTO>{

	public BitacoraAdministradorDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		BitacoraAdministradorDTO bitacoraAdministradorDTO = new BitacoraAdministradorDTO();
		
		bitacoraAdministradorDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		bitacoraAdministradorDTO.setIdCheckUsua(rs.getInt("FIID_CHECK_USUA"));
		bitacoraAdministradorDTO.setFechaInicio(rs.getString("FDINICIO"));
		bitacoraAdministradorDTO.setFechaFin(rs.getString("FDTERMINO"));
		bitacoraAdministradorDTO.setLongitud(rs.getString("FCLONGITUD"));
		bitacoraAdministradorDTO.setLatitud(rs.getString("FCLATITUD"));
		bitacoraAdministradorDTO.setCeco(rs.getString("FCID_CECO"));
		
		return bitacoraAdministradorDTO;
	}

	
}
