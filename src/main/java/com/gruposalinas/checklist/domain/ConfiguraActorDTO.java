package com.gruposalinas.checklist.domain;

public class ConfiguraActorDTO {
	


		private int idHallazgoConf;
		private int idConfigActor;
		private int statusActuacion;
		private String accion;
		private int statusAccion;
		private int idConfig;
		private int banderaAtencion;
		private String obs;
		private String periodo;
		private String aux;
		
		public int getIdHallazgoConf() {
			return idHallazgoConf;
		}
		public void setIdHallazgoConf(int idHallazgoConf) {
			this.idHallazgoConf = idHallazgoConf;
		}
		public int getIdConfigActor() {
			return idConfigActor;
		}
		public void setIdConfigActor(int idConfigActor) {
			this.idConfigActor = idConfigActor;
		}
		public int getStatusActuacion() {
			return statusActuacion;
		}
		public void setStatusActuacion(int statusActuacion) {
			this.statusActuacion = statusActuacion;
		}
		public String getAccion() {
			return accion;
		}
		public void setAccion(String accion) {
			this.accion = accion;
		}
		public int getStatusAccion() {
			return statusAccion;
		}
		public void setStatusAccion(int statusAccion) {
			this.statusAccion = statusAccion;
		}
		public int getIdConfig() {
			return idConfig;
		}
		public void setIdConfig(int idConfig) {
			this.idConfig = idConfig;
		}
		public int getBanderaAtencion() {
			return banderaAtencion;
		}
		public void setBanderaAtencion(int banderaAtencion) {
			this.banderaAtencion = banderaAtencion;
		}
		public String getObs() {
			return obs;
		}
		public void setObs(String obs) {
			this.obs = obs;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		public String getAux() {
			return aux;
		}
		public void setAux(String aux) {
			this.aux = aux;
		}
		@Override
		public String toString() {
			return "ConfiguraActorDTO [idHallazgoConf=" + idHallazgoConf + ", idConfigActor=" + idConfigActor
					+ ", statusActuacion=" + statusActuacion + ", accion=" + accion + ", statusAccion=" + statusAccion
					+ ", idConfig=" + idConfig + ", banderaAtencion=" + banderaAtencion + ", obs=" + obs + ", periodo="
					+ periodo + ", aux=" + aux + "]";
		}
}