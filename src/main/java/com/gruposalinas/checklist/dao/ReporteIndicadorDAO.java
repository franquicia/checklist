package com.gruposalinas.checklist.dao;


import java.util.List;

import com.gruposalinas.checklist.domain.ReporteIndicadorDTO;



public interface ReporteIndicadorDAO {

	public boolean insertaIndicador(ReporteIndicadorDTO bean) throws Exception;
	
	public String consultaPorcentaje (String idCeco, int idIndicador) throws Exception;
	
	public boolean actualizaIndicador(ReporteIndicadorDTO bean) throws Exception;
	
	public boolean insertaPendientes (String idCeco, int indicador) throws Exception;
	
	public  List<ReporteIndicadorDTO>  consultaPendientes (String idCeco, String idIndicador) throws Exception;

}
