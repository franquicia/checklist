package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.PosiblesTipoPreguntaDTO;

public interface PosiblesTipoPreguntaDAO {
	
	public List<PosiblesTipoPreguntaDTO> obtienePosiblesRespuestas(String idTipoPregunta) throws Exception;
	
	public int insertaPosibleTipoPregunta(int idPosible, int idTipoPregunta) throws Exception;
	
	public boolean actualizaPosibleTipoPregunta(PosiblesTipoPreguntaDTO posibleTipoPreguntaDTO) throws Exception;
	
	public boolean eliminaPosibleTipoPregunta(int idPosibleTipoPregunta) throws Exception;
	
}
