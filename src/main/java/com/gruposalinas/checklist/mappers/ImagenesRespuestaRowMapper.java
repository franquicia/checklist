package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ImagenesRespuestaDTO;

public class ImagenesRespuestaRowMapper implements RowMapper<ImagenesRespuestaDTO> {

	@Override
	public ImagenesRespuestaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		ImagenesRespuestaDTO imagenesRespuestaDTO = new ImagenesRespuestaDTO();
		
		imagenesRespuestaDTO.setIdImagen(rs.getInt("FIID_IMG"));
		imagenesRespuestaDTO.setIdArbol(rs.getInt("FIID_ARBOL_DES"));
		imagenesRespuestaDTO.setUrlImg(rs.getString("FCURL_IMG"));
		imagenesRespuestaDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
		
		return imagenesRespuestaDTO;
	}

}
