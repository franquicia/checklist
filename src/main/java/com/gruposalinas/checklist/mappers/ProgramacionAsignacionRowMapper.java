package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.AsignaTransfDTO;

public class ProgramacionAsignacionRowMapper implements RowMapper<AsignaTransfDTO> {

	public AsignaTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AsignaTransfDTO AsignaTransfDTO = new AsignaTransfDTO();
		
		AsignaTransfDTO.setIdProgramacion(rs.getInt("FIID_HISTORICO"));
		AsignaTransfDTO.setCeco(rs.getInt("FCID_CECO"));
		AsignaTransfDTO.setProyecto(rs.getInt("FIID_PROYECTO"));
		AsignaTransfDTO.setUsuario_asig(rs.getInt("FCUSUARIO"));
		AsignaTransfDTO.setUsuario(rs.getInt("FIID_USUARIO"));
		AsignaTransfDTO.setAgrupador(rs.getInt("FIID_AGRUPA"));
		AsignaTransfDTO.setFaseActual(rs.getInt("FIID_FASEACT"));
		AsignaTransfDTO.setIdestatus(rs.getInt("FCSTATUS"));
		AsignaTransfDTO.setObs(rs.getString("FCOBSERVACION"));
		AsignaTransfDTO.setFechaProgramacion(rs.getString("FECHA_PROGRA"));
		AsignaTransfDTO.setPeriodo(rs.getString("FDPERIODO"));
		AsignaTransfDTO.setNombreFase(rs.getString("NOMBREFASE"));
		AsignaTransfDTO.setObsFase(rs.getString("DETALLEFASE"));
		AsignaTransfDTO.setVersion(rs.getInt("FIVERSION"));
		AsignaTransfDTO.setFase(rs.getInt("FIID_FASE"));
		AsignaTransfDTO.setIdOrdenFase(rs.getInt("FIID_ORDENFASE"));
		AsignaTransfDTO.setStatusMaximo(rs.getInt("ULTFASE"));
	    
		return AsignaTransfDTO;
	}

}


