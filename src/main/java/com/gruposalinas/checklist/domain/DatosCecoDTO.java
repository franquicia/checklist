package com.gruposalinas.checklist.domain;

public class DatosCecoDTO {
	private String idCeco;
	private String nombreCeco;
	private String idCecoPadre;
	private String nombreCecoPadre;
	
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	public String getIdCecoPadre() {
		return idCecoPadre;
	}
	public void setIdCecoPadre(String idCecoPadre) {
		this.idCecoPadre = idCecoPadre;
	}
	public String getNombreCecoPadre() {
		return nombreCecoPadre;
	}
	public void setNombreCecoPadre(String nombreCecoPadre) {
		this.nombreCecoPadre = nombreCecoPadre;
	}
	
	

}
