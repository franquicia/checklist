package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistHallazgosRepoDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.HallazgosExpDTO;
import com.gruposalinas.checklist.domain.PreguntaCheckDTO;

public class HallazgosCopiaRowMapper implements RowMapper<HallazgosExpDTO> {

	public HallazgosExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		

		HallazgosExpDTO com = new HallazgosExpDTO();

       
	    com.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    com.setBitacora(rs.getInt("FIID_BITACORA"));
	    com.setObsNueva(rs.getString("FCOBSERVACIONES"));
	    com.setIdcheck(rs.getInt("FIID_CHECKLIST"));
	    com.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    com.setAux2(rs.getInt("FCRESPUESTA"));
	    com.setRespuesta(rs.getString("POSIBLE"));
	   // com.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    com.setPregPadre(rs.getInt("FIID_PREG_PADRE")); 
	    com.setPreg(rs.getString("FCDESCRIPCION"));   
	    com.setBitGral(rs.getInt("FIID_BITGRAL"));
	    com.setCeco(rs.getString("FCID_CECO"));
	    com.setRecorrido(rs.getInt("FCRECORRIDO"));
	    com.setRuta(rs.getString("FCRUTA"));

	    return com;

	   
	}
}



