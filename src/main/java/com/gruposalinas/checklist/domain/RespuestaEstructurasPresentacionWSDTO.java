/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.domain;

import java.util.List;

/**
 *
 * @author kramireza
 */
public class RespuestaEstructurasPresentacionWSDTO {

    String codigo;
    String mensaje;
    List<EKTPresentacion> listaTablaEKT;

    public RespuestaEstructurasPresentacionWSDTO() {
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<EKTPresentacion> getListaTablaEKT() {
        return listaTablaEKT;
    }

    public void setListaTablaEKT(List<EKTPresentacion> listaTablaEKT) {
        this.listaTablaEKT = listaTablaEKT;
    }

    @Override
    public String toString() {
        return "RespuestaEstructurasPresentacionWSDTO{" + "codigo=" + codigo + ", mensaje=" + mensaje + ", listaTablaEKT=" + listaTablaEKT + '}';
    }

}
