package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.DatosCecoDTO;

public class DatosCecoRowMapper implements RowMapper<DatosCecoDTO> {

	@Override
	public DatosCecoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		DatosCecoDTO datosCecoDTO = new DatosCecoDTO();
		
		datosCecoDTO.setIdCeco(rs.getString("FCID_CECO"));
		datosCecoDTO.setNombreCeco(rs.getString("FCNOMBRE"));
		datosCecoDTO.setIdCecoPadre(rs.getString("FCID_CECOPADRE"));
		datosCecoDTO.setNombreCecoPadre(rs.getString("FCNOMBRE_CECOPADRE"));
		
		return datosCecoDTO;
	}
	
	
	

}
