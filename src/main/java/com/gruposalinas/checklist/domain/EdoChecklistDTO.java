package com.gruposalinas.checklist.domain;

public class EdoChecklistDTO {

	private int idEdochecklist;
	private String descripcion;
	
	public int getIdEdochecklist() {
		return idEdochecklist;
	}
	public void setIdEdochecklist(int idEdochecklist) {
		this.idEdochecklist = idEdochecklist;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
