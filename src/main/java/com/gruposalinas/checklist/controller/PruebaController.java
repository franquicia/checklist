package com.gruposalinas.checklist.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PruebaController {

	@RequestMapping(value = "/pruebachecklist0.htm", method = RequestMethod.GET)
	public ModelAndView pruebachecklist0(HttpServletRequest request, HttpServletResponse response, Model model) {

		return new ModelAndView("pruebachecklist");
	}

	@RequestMapping(value = "/pruebachecklist1.htm", method = RequestMethod.GET)
	public ModelAndView pruebachecklist1(HttpServletRequest request, HttpServletResponse response, Model model) {

		return new ModelAndView("mailBuzon");
	}
}