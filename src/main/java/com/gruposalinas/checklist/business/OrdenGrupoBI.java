package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.OrdenGrupoDAO;
import com.gruposalinas.checklist.domain.OrdenGrupoDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class OrdenGrupoBI {
	private static Logger logger = LogManager.getLogger(OrdenGrupoBI.class);

	@Autowired
	OrdenGrupoDAO ordenGrupoDAO;
	
	List<OrdenGrupoDTO> listaOrdenGrupo = null;
	
	public List<OrdenGrupoDTO> obtieneOrdenGrupo(String idOrdenGrupo, String idChecklist, String idGrupo){
				
		try{
			listaOrdenGrupo = ordenGrupoDAO.obtieneOrdenGrupo(idOrdenGrupo, idChecklist, idGrupo);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Evidencia");
			
		}
				
		return listaOrdenGrupo;
	}	
	
	public int insertaGrupo(OrdenGrupoDTO bean) {
		
		int idGrupo = 0;
		
		try {		
			idGrupo = ordenGrupoDAO.insertaOrdenGrupo(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar el Orden  Grupo");
			
		}
		
		return idGrupo;		
	}
	

	public boolean actualizaGrupo(OrdenGrupoDTO bean) throws Exception {
		
		boolean respuesta = false;
				
		try {
			respuesta = ordenGrupoDAO.actualizaOrdenGrupo(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Orden Grupo");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaOrdenGrupo(int idOrdenGrupo){
		
		boolean respuesta = false;
		
		try{
			respuesta = ordenGrupoDAO.eliminaOrdenGrupo(idOrdenGrupo);
		}catch(Exception e){
			logger.info("No fue posible borrar el Orden  Grupo");
			
		}
		
		return respuesta;
	}

}
