package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.RepoFilExpDTO;

public class RepoFechasExpRowMapper implements RowMapper<RepoFilExpDTO> {

	public RepoFilExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		RepoFilExpDTO com = new RepoFilExpDTO();

	    com.setCeco(rs.getString("FCID_CECO"));
	    com.setNombreCeco(rs.getString("FCNOMBRE"));
	    com.setDireccion(rs.getString("DIRECCION"));
	    com.setFechaTermino(rs.getString("FDTERMINO"));
	    com.setBitGral(rs.getInt("FIID_BITGRAL"));
	    com.setIdRecorrido(rs.getString("RECORRIDO"));
	    com.setAperturable(rs.getInt("APERTURABLE"));
	    com.setCalif(rs.getDouble("CALIFICACION"));
	    com.setPrecalif(rs.getDouble("PRECALIFICACION"));
	    com.setNomUsu(rs.getString("USU"));
	    //puesto firma
	    com.setAux(rs.getString("FCPUESTO"));
	    //nombre responsable firma
	    com.setObs(rs.getString("FCRESPONSABLE"));
	    //conteo imperdonables
	    com.setIdcritica(rs.getInt("IMPERDONABLES"));
	    //conteo no
	    com.setAux2(rs.getInt("PREGUNTASNO"));
	   com.setPondTot(rs.getDouble("PONDTOTAL"));
	   //ponderacion si-na
	   com.setSumPreg(rs.getDouble("PONDSI_NA"));
	  

		return com;
	}

}
