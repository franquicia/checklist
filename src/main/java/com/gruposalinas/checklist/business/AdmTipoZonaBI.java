package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.AdmTipoZonasDAO;
import com.gruposalinas.checklist.domain.AdmTipoZonaDTO;
import com.gruposalinas.checklist.domain.AdminZonaDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class AdmTipoZonaBI {

	private static Logger logger = LogManager.getLogger(AdmTipoZonaBI.class);

	@Autowired
	AdmTipoZonasDAO admTipoZonasDAO;
	ArrayList<AdmTipoZonaDTO> lista = null;	
	
	public ArrayList<AdmTipoZonaDTO> obtieneTipoZonaById(AdmTipoZonaDTO bean){

		try{
			lista = admTipoZonasDAO.obtieneTipoZonaById(bean);	
		}
		catch(Exception e){
			logger.info("No fue posible obtener el tipo zona: "+e.getMessage());
		}
				
		return lista;
	}	
	
	public boolean insertaTipoZona(AdmTipoZonaDTO bean) {
		
		boolean respuesta = false;		
		try {		
			respuesta = admTipoZonasDAO.insertaTipoZona(bean);		
		} catch (Exception e) {
			logger.info("No fue posible insertar el tipo zona: "+e.getMessage());			
		}
		
		return respuesta;		
	}
	
	public boolean actualizaTipoZona (AdmTipoZonaDTO bean){
		
		boolean respuesta = false;				
		try {
			respuesta = admTipoZonasDAO.actualizaTipoZona(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el tipo zona: "+e.getMessage());
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaTipoZona(AdmTipoZonaDTO bean){
		
		boolean respuesta = false;		
		try{
			respuesta = admTipoZonasDAO.eliminaTipoZona(bean);
		}catch(Exception e){
			logger.info("No fue posible borrar el tipo zona: "+e.getMessage());			
		}
		
		return respuesta;
	}

}
