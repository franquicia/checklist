package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.Admin2SupDTO;
import com.gruposalinas.checklist.domain.AsignacionesSupDTO;

public class PersonalSupRowMapper implements RowMapper<Admin2SupDTO> {

	public Admin2SupDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		Admin2SupDTO asignaDTO = new Admin2SupDTO();
		
		asignaDTO.setSocio(rs.getString("Socio"));
		asignaDTO.setNumSocio(rs.getInt("No. de Socio"));
		asignaDTO.setPuesto(rs.getString("Puesto"));
		
		return asignaDTO;
		
		
	}

}
