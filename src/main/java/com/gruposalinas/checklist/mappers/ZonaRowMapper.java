package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ZonaNegoExpDTO;

public class ZonaRowMapper implements RowMapper<ZonaNegoExpDTO> {

	public ZonaNegoExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ZonaNegoExpDTO ne = new ZonaNegoExpDTO();
		ne.setIdZona(rs.getInt("FIID_ZONA"));
	    //nomenclatura zona
	    ne.setClasifica(rs.getString("CLASIFICA"));
	    ne.setIdActivo(rs.getInt("FIACTIVO"));
	    //DESCRIPCION COMPLETA
	    ne.setDescZona(rs.getString("DESCZONA"));
	    ne.setObs(rs.getString("FCAUX"));
		ne.setAux(rs.getString("FCAUX2"));
	
		return ne;
	}

}


