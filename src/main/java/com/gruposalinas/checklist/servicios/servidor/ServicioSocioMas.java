package com.gruposalinas.checklist.servicios.servidor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.gruposalinas.checklist.business.PuestoBI;
import com.gruposalinas.checklist.domain.PuestoDTO;
import com.gruposalinas.checklist.domain.PuestoUsuarioDTO;
@Controller
@RequestMapping("/SocioMas")

public class ServicioSocioMas {
	
	@Autowired
	PuestoBI puestobi;
	
	private static final Logger logger = LogManager.getLogger(ServicioSocioMas.class);

	// http://localhost:8080/checklist/SocioMas/getPuesto.json?idUsuario=<?>
	@RequestMapping(value = "/getPuesto", method = RequestMethod.GET)
	public ModelAndView getPuesto(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idUsuario = request.getParameter("idUsuario");
			
			List<PuestoUsuarioDTO> lista = puestobi.obtienePuestoUsuario(Integer.parseInt(idUsuario));
			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
			mv.addObject("PUESTO",lista);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}
	
}
