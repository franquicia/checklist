package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.ZonaNegoDAO;
import com.gruposalinas.checklist.domain.ZonaNegoExpDTO;

public class ZonaNegoBI {

	private static Logger logger = LogManager.getLogger(ZonaNegoBI.class);

private List<ZonaNegoExpDTO> listafila;
	
	@Autowired
	ZonaNegoDAO zonaNegoDAO;
		
	
	public int insertaNego(ZonaNegoExpDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = zonaNegoDAO.insertaNego(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaNego(String idTabNegocio){
		boolean respuesta = false;
		
		try {
			respuesta = zonaNegoDAO.eliminaNego(idTabNegocio);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<ZonaNegoExpDTO> obtieneDatosNego( String idNegocio) {
		
		try {
			listafila =  zonaNegoDAO.obtieneDatosNego(idNegocio);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<ZonaNegoExpDTO> obtieneInfoNego(String negocio) {

		try {
			listafila = zonaNegoDAO.obtieneInfoNego(negocio);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de la tabla Filas");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	
	public boolean actualizaNego(ZonaNegoExpDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = zonaNegoDAO.actualizaNego(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	//ZONAS
	
	public int insertaZona(ZonaNegoExpDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = zonaNegoDAO.insertaZona(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaZona(String idZona){
		boolean respuesta = false;
		
		try {
			respuesta = zonaNegoDAO.eliminaZona(idZona);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<ZonaNegoExpDTO> obtieneDatosZona( String  idZona) {
		
		try {
			listafila =  zonaNegoDAO.obtieneDatosZona( idZona);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<ZonaNegoExpDTO> obtieneInfoZona() {

		try {
			listafila = zonaNegoDAO.obtieneInfoZona();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de la tabla Filas");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	
	public boolean actualizaZona(ZonaNegoExpDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = zonaNegoDAO.actualizaZona(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
//RELACION
	public int insertaRela(ZonaNegoExpDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = zonaNegoDAO.insertaRela(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaRela(String idRela){
		boolean respuesta = false;
		
		try {
			respuesta = zonaNegoDAO.eliminaRela(idRela);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public List<ZonaNegoExpDTO> obtieneInfoRela() {

		try {
			listafila = zonaNegoDAO.obtieneInfoRela();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de la tabla Filas");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	
	public boolean actualizaRela(ZonaNegoExpDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = zonaNegoDAO.actualizaRela(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
}