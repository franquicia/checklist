/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.servicios.servidor;

import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.business.ObtieneCecos;
import com.gruposalinas.checklist.business.ParametroBI;
import com.gruposalinas.checklist.domain.ParametroDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author kramireza
 */
@Controller
@RequestMapping("/cecowsService")
public class CecosWSService {

    @Autowired
    ObtieneCecos obtieneCecos;

    private static Logger logger = LogManager.getLogger(CecosWSService.class);

    // http://localhost:8080/franquicia/cecowsService/getCecosWS.json
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getCecosWS", method = RequestMethod.GET)
    public @ResponseBody
    String getCecosFromWS(HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        logger.info("*************Comienza carga cecos*************");

        try {
            String tokenResult = obtieneCecos.getTokenCecos();
            if (tokenResult != null && !tokenResult.equals("")) {

                JSONObject jsonObj = new JSONObject(tokenResult);

                String token = jsonObj.getString("token");
                //        System.out.println("Token Recibido" + token);
                obtieneCecos.getData(token);
                logger.info("*************Termina carga cecos*************");
            } else {
                logger.info("Error Carga Cecos Ws:: El servicio del Token no responde...");
            }
        } catch (Exception c) {
            c.printStackTrace();
        }
        return "";
    }

    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/mergeTablasCecosWS", method = RequestMethod.GET)
    public @ResponseBody
    String mergeTablasCecosWS(HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        logger.info("*************Comienza merge de la tabla  TAPASO_CECO al FRTPPASO_CECO *************");

        try {
            obtieneCecos.getMergeData();
            logger.info("*************Termina merge de la tabla  TAPASO_CECO al FRTPPASO_CECO*************");

        } catch (Exception c) {
            c.printStackTrace();
        }
        return "";
    }

    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/pasoTrabajo", method = RequestMethod.GET)
    public @ResponseBody
    String pasoTrabajo(HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        logger.info("*************Comienza merge de la tabla  FRTPPASO_CECO al FRTACECO *************");

        try {
            ParametroBI paramBI = (ParametroBI) FRQAppContextProvider.getApplicationContext()
                    .getBean("parametroBI");
            int paramTotal = Integer.parseInt(paramBI.obtieneParametros("numCargas").get(0).getValor());

            CecoBI cecoBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
            logger.info("Elimina Duplicados... " + cecoBI.eliminaDuplicados());

            for (int j = 1; j <= paramTotal; j++) {
                ParametroDTO parametroDTO = new ParametroDTO();
                parametroDTO.setClave("tipoCarga");
                parametroDTO.setValor("" + j);
                parametroDTO.setActivo(1);
                ParametroBI parametroBI = (ParametroBI) FRQAppContextProvider.getApplicationContext()
                        .getBean("parametroBI");
                logger.info("Actualiza Parametro... " + parametroBI.actualizaParametro(parametroDTO));

                cecoBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
                if (j == 1) {
                    logger.info("Carga CecoBAZ... " + cecoBI.cargaBazPasoTrabajo());
                }
                if (j == 2) {
                    logger.info("Carga CecoGCC... " + cecoBI.cargaGCCPasoTrabajo());
                }
                if (j == 3) {
                    logger.info("Carga CecoCanalesTerceros... " + cecoBI.cargaCanalesTercerosPasoTrabajo());
                }
                if (j == 4) {
                    logger.info("Carga CecoEKT... " + cecoBI.cargaEKTPasoTrabajo());
                }
                if (j == 5) {
                    logger.info("Carga Micronegocios... " + cecoBI.cargaMicronegocioPasoTrabajo());

                }
                if (j == 6) {
                    logger.info("Carga CecoPPR... " + cecoBI.cargaPPRPasoTrabajo());
                }

            }

        } catch (Exception e) {
            logger.info("Algo Ocurrió Carga tabla de paso a tabla de trabajo... " + e.getMessage());
        }
        return "";
    }

}
