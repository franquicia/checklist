package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AdmFirmasCatDTO;
import com.gruposalinas.checklist.mappers.FirmasAgrupadorRowMapper;
import com.gruposalinas.checklist.mappers.FirmasCatalogoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class FirmasCatDAOImpl extends DefaultDAO implements FirmasCatDAO {

    private static Logger logger = LogManager.getLogger(FirmasCatDAOImpl.class);

    DefaultJdbcCall jdbcInsertaFirmaCat;
    DefaultJdbcCall jdbcEliminaFirmaCat;
    DefaultJdbcCall jdbcObtieneFirmaCat;
    DefaultJdbcCall jbdcActualizaFirmaCat;
    DefaultJdbcCall jdbcInsertaFirmaAgrupa;
    DefaultJdbcCall jdbcEliminaFirmaAgrupa;
    DefaultJdbcCall jdbcObtieneFirmaAgrupa;
    DefaultJdbcCall jbdcActualizaFirmaAgrupa;

    public void init() {

        jdbcInsertaFirmaCat = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFIRMAS")
                .withProcedureName("SP_INS_CTFIRMA");

        jdbcEliminaFirmaCat = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFIRMAS")
                .withProcedureName("SP_DEL_FIRMA");

        jdbcObtieneFirmaCat = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFIRMAS")
                .withProcedureName("SP_SEL_FIRMA")
                .returningResultSet("RCL_FIRMA", new FirmasCatalogoRowMapper());

        jbdcActualizaFirmaCat = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFIRMAS")
                .withProcedureName("SP_ACT_FIRMA");

        jdbcInsertaFirmaAgrupa = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFIRMAS")
                .withProcedureName("SP_INS_AGFIRMA");

        jdbcEliminaFirmaAgrupa = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFIRMAS")
                .withProcedureName("SP_DEL_AGFIRMA");

        jdbcObtieneFirmaAgrupa = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFIRMAS")
                .withProcedureName("SP_SEL_FIRMAAG")
                .returningResultSet("RCL_FIRMA", new FirmasAgrupadorRowMapper());

        jbdcActualizaFirmaAgrupa = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFIRMAS")
                .withProcedureName("SP_ACT_AGFIRMA");

    }

    public int insertaFirmaCat(AdmFirmasCatDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NOMBRE", bean.getNombreFirmaCatalogo())
                .addValue("PA_TIPOPROY", bean.getTipoProyecto())
                .addValue("PA_AUX", bean.getAux2());

        out = jdbcInsertaFirmaCat.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMFIRMAS.SP_INS_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_FIRMA");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el Empleado");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean eliminaFirmaCat(int idFirma) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIRMA", idFirma);

        out = jdbcEliminaFirmaCat.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMFIRMAS.SP_DEL_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + idFirma + ")");
        } else {
            return true;
        }

        return false;

    }

    //parametro
    @SuppressWarnings("unchecked")
    public List<AdmFirmasCatDTO> obtieneDatosFirmaCat(String idFirma) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AdmFirmasCatDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDFIRMA", idFirma);

        out = jdbcObtieneFirmaCat.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMFIRMAS.SP_SEL_CATFIRMA}");
        //lleva el nombre del cursor del procedure
        lista = (List<AdmFirmasCatDTO>) out.get("RCL_FIRMA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el empleado(" + idFirma + ")");
        }
        return lista;

    }

    public boolean actualizaFirmaCat(AdmFirmasCatDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIRMA", bean.getIdFirmaCatalogo())
                .addValue("PA_NOMBRE", bean.getNombreFirmaCatalogo())
                .addValue("PA_TIPOPROY", bean.getTipoProyecto())
                .addValue("PA_AUX", bean.getAux2());

        out = jbdcActualizaFirmaCat.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMFIRMAS.SP_ACT_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdAgrupaFirma() + ")");
        } else {
            return true;
        }
        return false;

    }

    @Override
    public int insertaFirmaAgrupa(AdmFirmasCatDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDAGRUPA", bean.getIdAgrupaFirma())
                .addValue("PA_CARGO", bean.getCargo())
                .addValue("PA_OBLIGA", bean.getObligatorio())
                .addValue("PA_ORDEN", bean.getOrden())
                .addValue("PA_AUX", bean.getAux2());

        out = jdbcInsertaFirmaAgrupa.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMFIRMAS.SP_INS_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_FIRMA");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el Empleado");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    @Override
    public boolean eliminaFirmaAgrupa(int idFirma) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIRMA", idFirma);

        out = jdbcEliminaFirmaAgrupa.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMFIRMAS.SP_DEL_AGRUPA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + idFirma + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<AdmFirmasCatDTO> obtieneDatosFirmaAgrupa(String idFirma) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AdmFirmasCatDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_AGRUP", idFirma);

        out = jdbcObtieneFirmaAgrupa.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMFIRMAS.SP_SEL_FIRMAAGRUPA}");
        //lleva el nombre del cursor del procedure
        lista = (List<AdmFirmasCatDTO>) out.get("RCL_FIRMA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el empleado(" + idFirma + ")");
        }
        return lista;

    }

    @Override
    public boolean actualizaFirmaAgrupa(AdmFirmasCatDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIRMA", bean.getIdFirmaCatalogo())
                .addValue("PA_IDAGRUPA", bean.getIdAgrupaFirma())
                .addValue("PA_CARGO", bean.getCargo())
                .addValue("PA_OBLIGA", bean.getObligatorio())
                .addValue("PA_ORDEN", bean.getOrden())
                .addValue("PA_AUX", bean.getAux2());

        out = jbdcActualizaFirmaAgrupa.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMFIRMAS.SP_ACT_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdAgrupaFirma() + ")");
        } else {
            return true;
        }
        return false;

    }

}
