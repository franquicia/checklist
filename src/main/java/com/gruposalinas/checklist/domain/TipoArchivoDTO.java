package com.gruposalinas.checklist.domain;

public class TipoArchivoDTO {
	
	private int idTipoArchivo;
	private String nombreTipo;
	
	public int getIdTipoArchivo() {
		return idTipoArchivo;
	}
	public void setIdTipoArchivo(int idTipoArchivo) {
		this.idTipoArchivo = idTipoArchivo;
	}
	public String getNombreTipo() {
		return nombreTipo;
	}
	public void setNombreTipo(String nombreTipo) {
		this.nombreTipo = nombreTipo;
	}
	
	

}
