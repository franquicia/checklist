<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html lang="es">
<head>
<title>Banco Azteca | Sistematización de actividades</title>
<link rel="stylesheet"
	href="../css/vistaCumplimientoVisitas/estilo.css" />

<link rel="stylesheet"
	href="../css/vistaCumplimientoVisitas/carrucelAux.css" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">

<script type="text/javascript">

	var rutaHostImagen = '${rutaHostImagen}';
	var json = ${json};
		
</script>
<!-- <link rel="stylesheet" type="text/css"
	href="../css/vistaCumplimientoVisitas/tooltipster.bundle.css" />  -->
<link rel="stylesheet" media="screen"
	href="../css/vistaCumplimientoVisitas/modal.css">
<link
	href="../css/vistaCumplimientoVisitas/owl.carousel.css"
	rel="stylesheet">
<link
	href="../css/vistaCumplimientoVisitas/owl.theme.css"
	rel="stylesheet">
<!-- RANGE SLIDER -->
<link rel="stylesheet" type="text/css"
	href="../css/vistaCumplimientoVisitas/jquery-ui-rangeSlider.min.css">
<link rel="stylesheet" type="text/css"
	href="../css/vistaCumplimientoVisitas/rangeSlider.css">
</head>

<body onload="nobackbutton(); init();">
	<style>
table.tblrangeSlider {
	color: #989898;
	width: 102%;
	margin-left: -1%;
}

.tRight {
	text-align: right !important;
}
class

ui-slider-handle
ui-state-default
ui-corner-all
</style>

	<div class="header">
		<div class="logo">
			<a href="/${alias}/"><img
				src="../images/vistaCumplimientoVisitas/logo.svg"></a>
		</div>
		<div class="title">
			<div class="box tleft">
				<a
					onclick="regresaVistaTerrit(${porcentajeG},${banderaCecoPadre},${nivelPerfil},${idCecoPadre},${idCeco},'${nombreCeco}',${seleccionAno},${seleccionMes},${seleccionCanal},${seleccionPais});"
					class="hom"><img
					src="../images/vistaCumplimientoVisitas/regresar.svg"
					class="imgBack">
				</a> 
				<c:if test="${!opcionReporte}">
					<a href="../central/vistaCumplimientoVisitas.htm" class="active">
						<img src="../images/vistaCumplimientoVisitas/rev01W.svg">
					</a>
				</c:if>	
				<c:if test="${opcionReporte}">
					<a href="../central/vistaCumplimientoVisitasMovil.htm" class="active">
						<img src="../images/vistaCumplimientoVisitas/rev01W.svg">
					</a>
				</c:if>	
				<!--  <a href="#" class=" "><img
					src="../images/vistaCumplimientoVisitas/rev02W.svg"></a>
				<a href="#" class=" "><img
					src="../images/vistaCumplimientoVisitas/rev03W.svg"></a>
				<a href="#" class=" "><img
					src="../images/vistaCumplimientoVisitas/rev04W.svg"></a>
				<a href="#" class=" "><img
					src="../images/vistaCumplimientoVisitas/rev05W.svg"></a>-->
			</div>
		</div>
	</div>
	<div class="wrapper">

		<c:set var="item" scope="session" value="${listaCheck}" />

		<div class="hspacer"></div>
		<div class="box">
			<div class="titPiso">
				<h2>
					<div>${item[0].nombreCheck}</div>
					<div id="nombreSucursal">${item[0].nombresuc}</div>
				</h2>
			</div>

			<table class="bloquer">
				<thead>
					<tr>
						<th class="tLeft">Resumen</th>
				</thead>
			</table>


			<dl class="accordion5">
				<dt class="Acofecha">
					<div class="AcoTit">Fecha y hora</div>
				</dt>
				<dd>
					<div class="inner2">

						<table class="calendar">
							<tbody>
								<tr>
									<td>Inicio:</td>
									<td><span id="fInicio">${item[0].fecha_inicio}</span></td>
									<td><span id="hInicio">${item[0].horaInicio}</span></td>
								</tr>
								<tr>
									<td>Final</td>
									<td><span id="fFin">${item[0].fecha_fin}</span></td>
									<td><span id="hFin">${item[0].horaFin}</span></td>
								</tr>
								<tr>
									<td>Envio</td>
									<td><span id="fEnvio">${item[0].fechaEnvio}</span></td>
									<td>Modo: <span id="hModo">${item[0].modo}</span></td>
								</tr>
							</tbody>
						</table>
					</div>
				</dd>
				<hr>
				<dt class="Acofolios">
					<div class="AcoTit">Folios</div>
				</dt>
				<dd>
					<div class="inner2">
						<table class="folio">
							<tbody>
								<c:forEach var="item" items="${listaFolios}">
									<tr onClick="cargaEvidenciasXIdPreg('${item.idPregunta}');"
										style="cursor: pointer;">
										<td>${item.pregunta}</td>
										<td><span>${item.idPregunta}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</dd>
				<hr>
				<dt class="AcoEvidencia">
					<div class="AcoTit">Evidencia</div>
				</dt>

				<dd>
					<div class="inner2">
						<table class="evidencia">
							<tbody>
								<c:forEach var="item" items="${listaEvidencias}">
									<tr onClick="cargaEvidenciasXIdPreg('${item.idPregunta}');"
										style="cursor: pointer;">
										<td>${item.descPreg}</td>
										<c:set var="rutaOriginal" value="${item.ruta}" />
										<td><img
											src="${rutaHostImagen}/franquicia/preview${fn:substringAfter(rutaOriginal,'imagenes')}"
											style="width: 24px; height: 24px;"></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</dd>
				<hr>
			</dl>

			<table class="bloquer">
				<thead>
					<tr>
						<th class="tLeft">Detalle</th>
				</thead>
			</table>

			<div id="cont1">
				<div class="mainbanner">
					<div id="owl-main" class="owl-carousel owl-theme">
						<div class="item" style="height: 400px;">

							<!-- CARRUSEL PROPIO -->

							<div class="deDetalleGeneral2" style="height: 400px;">
								<div class="deMarco2-2" style="background: transparent;">
									<div class="deMarco2-2-1">

										<span class='txt2'><span class="pregunta"
											id="preguntaN"> </span> <span class='txtRojo' id="respuestaN"
											style="color: red;"></span><br> </span> Folio: <span
											id="folioN"></span><br> Acción: <span id="accionN">
										</span><br>
										<!-- Fecha de corrección:-->
										<span id="fechaCorreccionN"> </span><br> Evidencias

										<div class="deMarcoDetalle-23"></div>
									</div>
									<div class="deMarco2-2-2"></div>
									<div class="deMarco2-2-3">
										<div class="margenCarruselUno" style="width: 5%;">
											<div class="margenCarruselUno-1"></div>
											<div class="margenCarruselUno-2">
												<img onclick="anteriorDetalle();" class="imagAnterior"
													src="../images/flecha_atras.png"
													style="width: 45%;">
											</div>
										</div>

										<div class="margenCarruselTres"
											style="width: 90%; height: 100%;">
											<div id="margenImagen">
												<img class="tamaImagen" id="tamaImagen"
													src="../images/reporteOnline/evidencia_no_requerida.png"
													style="width: 100%;">
											</div>
											<div class="margenNumeros">
												<table class="celdaNumeros">
													<tr>
														<td id="celdaCirculos"></td>
													</tr>
												</table>
											</div>
										</div>
										<div class="margenCarruselCuatro" style="width: 5%;">
											<div class="margenCarruselUno-1"></div>
											<div class="margenCarruselUno-2">
												<img onclick="siguienteDetalle();" class="imagPosterior"
													src="../images/flecha_adelante.png"
													style="width: 45%;">
											</div>
										</div>
									</div>
									<div class="deMarco2-2-4"></div>
								</div>
							</div>
							<!-- CARRUSEL PROPIO -->

						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
			<br>

			<div class="rangeSlider">
				<div id="slider"></div>
				<table class="tblrangeSlider" id="tblrangeSlider">
					<!-- SE LLENO CON JAVASCRIPT -->
				</table>
				<!--<div class="amount">
			<ul class="clearfix"></ul>
			<div class="pie"></div>
		</div>-->

			</div>



		</div>

		<form id="formTerri" method="POST"
			action="vistaTerritCumplimientoVisitas.htm"></form>

		<!-- <div class="fspacer"></div> -->
	</div>



	<!-- jquery Start -->
	<script type="text/javascript"
		src="../js/vistaCumplimientoVisitas/jquery.js"></script>

	<!-- progressbar -->
	<script>
		function move() {
			var elem = document.getElementById("myBar");
			var width = 1;
			var id = setInterval(frame, 10);
			function frame() {
				if (width >= 100) {
					clearInterval(id);
				} else {
					width++;
					elem.style.width = width + '%';
				}
			}
		}
	</script>

	<!-- dropdown -->
	<script type="text/javascript"
		src="../js/vistaCumplimientoVisitas/menu.js"></script>

	<!-- accordion -->
	<script type="text/javascript">
		jQuery(function() {
			$(".subSeccion").hide();

			var allPanels = $('.accordion5 > dd').hide();

			jQuery('.accordion5 > dt').on('click', function() {
				$this = $(this);
				//the target panel content
				$target = $this.next();

				jQuery('.accordion5 > dt').removeClass('accordion-active');
				jQuery('.accordion5 > dt').removeClass('Acofondo');

				if ($target.hasClass("in")) {
					$this.removeClass('accordion-active');
					$target.slideUp();
					$target.removeClass("in");

				} else {
					$this.addClass('accordion-active');
					jQuery('.accordion5 > dd').removeClass("in");
					$target.addClass("in");
					$this.addClass('Acofondo');

					jQuery('.accordion5 > dd').slideUp();
					$target.slideDown();
				}
			})

		})
	</script>
	/
	<!-- Slider -->
	<script
		src="../js/vistaCumplimientoVisitas/owl.carousel.js"></script>
	<!-- Slider -->
	<script
		src="../js/vistaCumplimientoVisitas/script-detalleCumplimientoVisitas.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			/*$( ".owl-theme .owl-controls .owl-buttons div.owl-prev" ).css("display":"none");*/

			$("#owl-main").owlCarousel({
				navigation : true, // Show next and prev buttons
				slideSpeed : 300,
				paginationSpeed : 400,
				singleItem : true
			});
		});
	</script>
	<!-- UI RANGE SLIDER -->

	<script type="text/javascript"
		src="../js/vistaCumplimientoVisitas/jquery-ui-rangeSlider.min.js"></script>
	<script type="text/javascript"
		src="../js/vistaCumplimientoVisitas/jquery-rangeSlider.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			var json = $
			{
				json
			}
			;
			/* slider(min,	max, step, actual, indicador); 
			  slider(500, 12000, 500, 2000, '.amount div');*/
			slider(1, json.length, 1, 1, '.amount div');
		});

		$('#slider').slider({
			change : function(event, ui) {
				if (event.originalEvent) {
					//manual change
					cargaEvidencias(ui.value - 1);
				} else {
					//programmatic change
				}
			}
		});
	</script>
	<div class="modal"></div>
</body>
</html>