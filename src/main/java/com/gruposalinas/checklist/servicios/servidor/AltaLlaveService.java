package com.gruposalinas.checklist.servicios.servidor;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.LlaveBI;
import com.gruposalinas.checklist.domain.LlaveDTO;
import com.gruposalinas.checklist.resources.FRQAuthInterceptor;


@Controller
@RequestMapping("/altaLlaveService")
public class AltaLlaveService {

	@Autowired
	LlaveBI llavebi;
	
	private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);

	// http://localhost:8080/checklist/altaLlaveService/altaLlave.json?idLlave=<?>&llave=<?>&descripcion=<?>
	@RequestMapping(value = "/altaLlave", method = RequestMethod.GET)
	public ModelAndView altaLlave(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try{
		String idLlave = request.getParameter("idLlave");
		String llaveRequest = new String(request.getParameter("llave").getBytes("ISO-8859-1"), "UTF-8");
		String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");

		LlaveDTO llave = new LlaveDTO();
		llave.setIdLlave(Integer.parseInt(idLlave));
		llave.setLlave(llaveRequest);
		llave.setDescripcion(descripcion);
		boolean res = llavebi.insertaLlave(llave);
		
		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "LLAVE CREADA");
		mv.addObject("res", res);
		return mv;
		}
		catch(Exception e){
			logger.info(e);
			return null;
		}
	}
}
