$(function() {
	$("#fechaIni1").datepicker({
		maxDate : '0d',
		dateFormat : 'yymmdd',
		showOtherMonths : true,
		selectOtherMonths : true,
		changeMonth : true,
		changeYear : true,
		beforeShow : function(input, inst) {
			var rect = input.getBoundingClientRect();
			setTimeout(function() {
				inst.dpDiv.css({
					top : rect.top + 35,
					left : rect.left + 0
				});
			}, 0);
		}
	});

	$("#fechaResp1").datepicker({
		maxDate : '0d',
		dateFormat : 'yymmdd',
		showOtherMonths : true,
		selectOtherMonths : true,
		changeMonth : true,
		changeYear : true,
		beforeShow : function(input, inst) {
			var rect = input.getBoundingClientRect();
			setTimeout(function() {
				inst.dpDiv.css({
					top : rect.top + 35,
					left : rect.left + 0
				});
			}, 0);
		}
	});
});

function validaAlta() {
	var nomCanal = document.getElementById("nomCanal").value;
	var nomPais = document.getElementById("nomPais").value;
	var latitud = document.getElementById("latitud").value;
	var longitud = document.getElementById("longitud").value;
	var nombreSuc = document.getElementById("nombresuc").value;
	var nuSucursal = document.getElementById("nuSucursal").value;
	// initMap();
	// alert("Llego a valida alta() '"+ nombreSuc+"'");
	if ((nomCanal == '' || nomCanal == null)
			|| (nomPais == '' || nomPais == null)
			|| (latitud == '' || latitud == null || latitud == '0.0')
			|| (longitud == '' || longitud == null || longitud == '0.0')
			|| (nombreSuc == '' || nombreSuc == null)
			|| (nuSucursal == '' || nuSucursal == null)) {
		alert("Ningún campo puede ir vacío!");
		return false;
	} else {
		// alert("Pasó");
		var latitud1 = latitud;
		var longitud1 = longitud;
		var pos1 = new google.maps.LatLng(latitud1, longitud1);

		var mapOptions = {
			zoom : 17,
			center : pos1,
			mapTypeId : 'roadmap'
		};

		var map1 = new google.maps.Map(document.getElementById('map'),
				mapOptions);

		var marker1 = new google.maps.Marker({
			position : pos1,
			animation : google.maps.Animation.DROP,
		});

		marker1.setMap(map1);

		marker1
				.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');

		var retVal = confirm("¿Está seguro que desea agregar la Sucursal? ");

		if (retVal == true) {
			return true;
		} else {
			activa();
			return false;
		}
		// return true;
	}
}

function validaAlta2() {
	var nomCanal = document.getElementById("nomCanal").value;
	var nomPais = document.getElementById("nomPais").value;
	var latitud = document.getElementById("latitud").value;
	var longitud = document.getElementById("longitud").value;
	var nombreSuc = document.getElementById("nombresuc").value;
	var nuSucursal = document.getElementById("nuSucursal").value;
	// initMap();
	// alert("Llego a valida alta() '"+ nombreSuc+"'");
	if ((nomCanal == '' || nomCanal == null)
			|| (nomPais == '' || nomPais == null)
			|| (latitud == '' || latitud == null || latitud == '0.0')
			|| (longitud == '' || longitud == null || longitud == '0.0')
			|| (nombreSuc == '' || nombreSuc == null)
			|| (nuSucursal == '' || nuSucursal == null)) {
		alert("Ningún campo puede ir vacío!");
		initMap();
		return false;
	} else {
		initMap();
		desactiva();
	}

}
function initMap() {
	var latitud = document.getElementById("latitud").value;
	var longitud = document.getElementById("longitud").value;
	var latitud1 = latitud;
	var longitud1 = longitud;
	var pos1 = new google.maps.LatLng(latitud1, longitud1);

	var mapOptions = {
		zoom : 17,
		center : pos1,
		mapTypeId : 'roadmap'
	};

	var map1 = new google.maps.Map(document.getElementById('map'), mapOptions);

	var marker1 = new google.maps.Marker({
		position : pos1,
		animation : google.maps.Animation.DROP,
	});

	marker1.setMap(map1);

	marker1.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
}

function desactiva() {
	document.form1.Agregar.disabled = false;
	document.form1.Agregar.disabled = false;
	return false;
}
function activa() {
	document.form1.Agregar.disabled = true;
	document.form1.Agregar.disabled = true;
	return false;
}
/*
 * function showHide() { var div = document.getElementById(hidden_div);
 * alert("Paso"); if (div.style.display == 'none') { div.style.display = ''; }
 * else { div.style.display = 'none'; } return false; }
 */