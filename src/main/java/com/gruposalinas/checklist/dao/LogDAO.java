package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.LogDTO;

public interface LogDAO {

	public List<LogDTO> obtieneErrores(String fecha) throws Exception;
}
