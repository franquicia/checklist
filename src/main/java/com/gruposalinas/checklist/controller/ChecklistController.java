package com.gruposalinas.checklist.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.ChecklistBI;
import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.RegistroChecklistDTO;
import com.gruposalinas.checklist.domain.TipoChecklistDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

@Controller
public class ChecklistController {
	
	@Autowired
	ChecklistBI checklistbi;
		
	@RequestMapping(value = {"tienda/muestraFormatoChecklist.htm" , "central/muestraFormatoChecklist.htm"}, method = RequestMethod.GET)
	public ModelAndView checkList(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {
			return new ModelAndView ("muestraFormatoChecklist");
	}
	
	@RequestMapping(value = {"tienda/registraChecklist.htm" , "central/registraChecklist.htm"}, method = RequestMethod.GET)
	public ModelAndView registraCheckList(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {
			
			RegistroChecklistDTO reg = new RegistroChecklistDTO();
			
			ChecklistDTO check = new ChecklistDTO();
			check.setFecha_fin("20171006");
			check.setFecha_inicio("20161006");
			check.setIdHorario(42);
			check.setVigente(1);
			check.setNombreCheck("Check Prueba1");
			TipoChecklistDTO tcd = new TipoChecklistDTO();
			tcd.setIdTipoCheck(21);
			check.setIdTipoChecklist(tcd);
			check.setIdEstado(21);
			reg.setListaChecklist(check);
			
			PreguntaDTO preg = new PreguntaDTO();
			PreguntaDTO preg2 = new PreguntaDTO();
			PreguntaDTO preg3 = new PreguntaDTO();
			List<PreguntaDTO> listaPreg = new ArrayList<PreguntaDTO>();
			preg.setIdModulo(21);
			preg.setIdTipo(21);
			preg.setEstatus(1);
			preg.setPregunta("Pregunta 1 Prueba");
			preg.setNumRespuestas("2");
			listaPreg.add(preg);
			
			preg2.setIdModulo(21);
			preg2.setIdTipo(21);
			preg2.setEstatus(1);
			preg2.setPregunta("Pregunta 2 Prueba");
			preg2.setNumRespuestas("2");
			listaPreg.add(preg2);
			
			preg3.setIdModulo(21);
			preg3.setIdTipo(21);
			preg3.setEstatus(1);
			preg3.setPregunta("Pregunta 3 Prueba");
			preg3.setNumRespuestas("2");
			listaPreg.add(preg3);
			reg.setListaPregunta(listaPreg);

			ArbolDecisionDTO arbol = new ArbolDecisionDTO();
			ArbolDecisionDTO arbol2 = new ArbolDecisionDTO();
			ArbolDecisionDTO arbol3 = new ArbolDecisionDTO();
			ArbolDecisionDTO arbol4 = new ArbolDecisionDTO();
			ArbolDecisionDTO arbol5 = new ArbolDecisionDTO();
			ArbolDecisionDTO arbol6 = new ArbolDecisionDTO();
			List<ArbolDecisionDTO> listaArbol = new ArrayList<ArbolDecisionDTO>();
			arbol.setEstatusEvidencia(1);
			arbol.setRespuesta("SI");
			arbol.setOrdenCheckRespuesta(2);
			arbol.setReqAccion(1);
			listaArbol.add(arbol);
			
			arbol2.setEstatusEvidencia(0);
			arbol2.setRespuesta("NO");
			arbol2.setOrdenCheckRespuesta(2);
			arbol2.setReqAccion(0);
			listaArbol.add(arbol2);

			arbol3.setEstatusEvidencia(1);
			arbol3.setRespuesta("SI");
			arbol3.setOrdenCheckRespuesta(3);
			arbol3.setReqAccion(1);
			listaArbol.add(arbol3);

			arbol4.setEstatusEvidencia(0);
			arbol4.setRespuesta("NO");
			arbol4.setOrdenCheckRespuesta(3);
			arbol4.setReqAccion(0);
			listaArbol.add(arbol4);

			arbol5.setEstatusEvidencia(1);
			arbol5.setRespuesta("SI");
			arbol5.setOrdenCheckRespuesta(4);
			arbol5.setReqAccion(1);
			listaArbol.add(arbol5);

			arbol6.setEstatusEvidencia(0);
			arbol6.setRespuesta("NO");
			arbol6.setOrdenCheckRespuesta(4);
			arbol6.setReqAccion(0);
			listaArbol.add(arbol6);
			reg.setListaArbol(listaArbol);
			
			try {
				checklistbi.registraChecklist(reg);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//System.out.println("FALLO!!!" + e);
			}
			
		return null;
	}
	
	/*REPORTE CONTROLLER*/
	@RequestMapping(value = {"getReporteCheck.htm"}, method = RequestMethod.GET)
	public String getReporteCheck(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {
		
		
		return null;
	}
	@ExceptionHandler(Exception.class)
	public ModelAndView handleError(HttpServletRequest request, Exception ex) {
		ModelAndView mv = new ModelAndView("exception");
	    return mv;
	}
}
