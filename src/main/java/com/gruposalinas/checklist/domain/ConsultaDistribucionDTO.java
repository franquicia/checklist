package com.gruposalinas.checklist.domain;

public class ConsultaDistribucionDTO {
	private String idCeco;
	private String nombreCeco;
	private String territorio;
	private String zona;
	private String region;
	private String gerente;
	
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	
	
	public String getTerritorio() {
		return territorio;
	}
	public void setTerritorio(String territorio) {
		this.territorio = territorio;
	}
	
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	
	
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	
	
	public String getGerente() {
		return gerente;
	}
	public void setGerente(String gerente) {
		this.gerente = gerente;
	}
	@Override
	public String toString() {
		return "ConsultaDistribucionDTO [idCeco=" + idCeco + ", nombreCeco=" + nombreCeco + ", territorio=" + territorio
				+ ", zona=" + zona + ", region=" + region + ", gerente=" + gerente + "]";
	}
	
	

}
