package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.PuestoDTO;

public class PuestoRowMapper implements RowMapper<PuestoDTO>{

	public PuestoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		PuestoDTO puestoDTO = new PuestoDTO();
		
		
		puestoDTO.setIdPuesto(rs.getInt("FIID_PUESTO"));
		puestoDTO.setIdNivel(rs.getInt("FIID_NIVEL"));
		puestoDTO.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
		puestoDTO.setIdTipoPuesto(rs.getInt("FIIDTIPOPUESTO"));
		puestoDTO.setIdCanal(rs.getInt("FIID_CANAL"));
		puestoDTO.setCodigo(rs.getString("FCCODIGO"));
		puestoDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
		puestoDTO.setIdSubnegocio(rs.getInt("FIIDSUBNEGOCIO"));
		puestoDTO.setActivo(rs.getInt("FIACTIVO"));
		
		return puestoDTO;
	}
}
