package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ConsultaDistribucionDTO;

public class ConsultaEstructuraRowMapper implements RowMapper<ConsultaDistribucionDTO> {

	public ConsultaDistribucionDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConsultaDistribucionDTO a1 = new ConsultaDistribucionDTO();
		a1.setTerritorio(rs.getString("FCNOMBRETERR"));
		a1.setZona(rs.getString("FCNOMBREZONA"));
		a1.setRegion(rs.getString("FCNOMBREREGI"));
		a1.setGerente(rs.getString("FCNOMBREGERE"));
	  
	return a1;
	}
	

		

}






