<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="../css/menuCheck.css" rel="stylesheet" type="text/css" />

<script	src="../js/script-menu.js"></script>


<div class="margenGeneral">
	<div class="positionGeneral">

		<div class="opcionResumen">
			<form name="formulario" method="POST"	action="../central/resumenCheck.htm" id="formActivaAdmin">
				<div class="textCenterMenu" onclick="resumen()">
					<input type="hidden" name="idUserAdmin" value="adminCheckchecklists">
					<img class="tamImagenMenuCheck"	src="../images/iconosAdmCheck/iconoResumen.png">
				RESUMEN
				</div>
			</form>
		</div>
		
		<div class="opcionDiseñar">
			<form name="formulario" method="POST"	action="../central/checklistDesignF1.htm" id="formEnvia">
				<div class="textCenterMenu" onclick="diseñar()">
					<input type="hidden" name="idChecklist" value="0">
					<img class="tamImagenMenuCheck"	src="../images/iconosAdmCheck/iconoDisenar.png">
					DISEÑAR
				</div>
			</form>
		</div>
		
		<div class="opcionRecopilar">
			<form name="formulario" method="POST"	action="../central/exportaReporte.htm" id="formEnviaReporte">
				<div class="textCenterMenu" onclick="recopilar()">
					<input type="hidden" name="idReporte" value="adminCheckchecklists">
					<img class="tamImagenMenuCheck"	src="../images/iconosAdmCheck/iconoRecopilar.png">
					RECOPILAR
				</div>
			</form>
		</div>
		
		<div class="opcionVisualizar">
			<a href="../central/visualizador.htm">
			<div class="textCenterMenu">
				<img class="tamImagenMenuCheck"	src="../images/iconosAdmCheck/iconoOjoAbierto.png">
				VISUALIZAR
			</div>
			</a>
		</div>
	</div>

</div>