package com.gruposalinas.checklist.domain;

public class TipoPreguntaDTO {
	
	private int idTipoPregunta;
	private String cvePregunta;
	private String descripcion;
	private int activo;
	
	public int getIdTipoPregunta() {
		return idTipoPregunta;
	}
	public void setIdTipoPregunta(int idPregunta) {
		this.idTipoPregunta = idPregunta;
	}
	public String getCvePregunta() {
		return cvePregunta;
	}
	public void setCvePregunta(String cvePregunta) {
		this.cvePregunta = cvePregunta;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	
	

}
