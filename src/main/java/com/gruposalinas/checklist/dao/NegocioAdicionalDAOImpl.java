package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.NegocioAdicionalDTO;
import com.gruposalinas.checklist.mappers.NegocioAdicionalRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class NegocioAdicionalDAOImpl extends DefaultDAO implements NegocioAdicionalDAO {

    private static Logger logger = LogManager.getLogger(NegocioAdicionalDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneNegocioAdicional;
    private DefaultJdbcCall jdbcInsertaNegocioAdicional;
    private DefaultJdbcCall jdbcEliminaNegocioAdicional;

    public void init() {

        jdbcObtieneNegocioAdicional = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFRTANEG_AD")
                .withProcedureName("SPOBTIENENEGO")
                .returningResultSet("RCL_CONSULTA", new NegocioAdicionalRowMapper());

        jdbcInsertaNegocioAdicional = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFRTANEG_AD")
                .withProcedureName("SPINSERTANEG");

        jdbcEliminaNegocioAdicional = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFRTANEG_AD")
                .withProcedureName("SPELIMINANEG");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NegocioAdicionalDTO> obtieneNegocioAdicional(String usuario, String negocio) throws Exception {
        Map<String, Object> out = null;
        List<NegocioAdicionalDTO> negocioadicional = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USUARIO", usuario).addValue("PA_NEGOCIO", negocio);

        out = jdbcObtieneNegocioAdicional.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMFRTANEG_AD.SPOBTIENENEGO}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        negocioadicional = (List<NegocioAdicionalDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al consultar los Negocio Adicional ");
        }

        return negocioadicional;
    }

    @Override
    public boolean insertaNegocioAdicional(int usuario, int negocio) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USUARIO", usuario).addValue("PA_NEGOCIO", negocio);

        out = jdbcInsertaNegocioAdicional.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMFRTANEG_AD.SPINSERTANEG}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean eliminaNegocioAdicional(int usuario, int negocio) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USUARIO", usuario).addValue("PA_NEGOCIO", negocio);

        out = jdbcEliminaNegocioAdicional.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMFRTANEG_AD.SPELIMINANEG}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 1) {
            return true;
        } else {
            return false;
        }
    }

}
