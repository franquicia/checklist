package com.gruposalinas.checklist.domain;

public class RespuestaPdfDTO {

	private int idPregunta;
	private String pregunta;
	private int orden;
	private int preguntaPadre;
	private String respuesta;
	private String compromiso;
	private int responsable;
	private String fechaCompromiso;
	private String evidencia;
	
	
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public String getPregunta() {
		return pregunta;
	}
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	public int getPreguntaPadre() {
		return preguntaPadre;
	}
	public void setPreguntaPadre(int preguntaPadre) {
		this.preguntaPadre = preguntaPadre;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getCompromiso() {
		return compromiso;
	}
	public void setCompromiso(String compromiso) {
		this.compromiso = compromiso;
	}
	public int getResponsable() {
		return responsable;
	}
	public void setResponsable(int responsable) {
		this.responsable = responsable;
	}
	public String getFechaCompromiso() {
		return fechaCompromiso;
	}
	public void setFechaCompromiso(String fechaCompromiso) {
		this.fechaCompromiso = fechaCompromiso;
	}
	public String getEvidencia() {
		return evidencia;
	}
	public void setEvidencia(String evidencia) {
		this.evidencia = evidencia;
	}
	

}
