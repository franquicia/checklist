var preguntas = new Array();
var posiblesRespuestas=new Array();

var pregSeleccionada=-1;
//variables para la edicion de los modulos
var editarModulo=false;
var catalogoModulo=new Array();
var idModEditar=0;
var nuevoMod=false;

var urlServer =$(location).attr('host');
var editarTipoCheck=false;
var idChecklist;
var idPregBDTmp=0;

var area;

var numVersion; 

$(document).ready(
		
		function(){
			numVersion = document.getElementById('numVersion').value;
			asignarEventosCombo();
			cargarCatalogoModulos();
			cargarPosiblesResp();
			idChecklist=$("#idChecklist").val();
			//se cargan las preguntas agregadas previamente (en caso de tenerlas)
			cargarPreguntaPreviasService();
			//alert("IDCHECKLIST: "+idChecklist);
			
			//linea para actvar el editor html para la ayuda de las preguntas
			//area=new nicEditor().panelInstance('textoAyuda');
			
		}
);



function mostrarComponente(nombreComponente){

	$("#"+nombreComponente).css('display', "inline");
	
	//return false;
}
function ocultarComponente(nombreComponente){

	$("#"+nombreComponente).css('display', "none");
	
	//return false;
}

function asignarEventosCombo(){
	/*se asignan los eventos para los combos agregados en el div dinamico del arbol de deciciones del checklist*/
	$(".caja").off();

		
	$(".caja").hover(
			
			function(e){

				asignarEventosCombo(); 
				
				var lista = $(this).find("ul");
				var triangulo = $(this).find("span:last-child");
				e.preventDefault();
				
				$(this).find("ul").toggle();
				if (lista.is(":hidden") ) {
					triangulo.removeClass("triangulosup").addClass("trianguloinf");
					//lista.hide();

					triangulo.parent().css('z-index', 0);
				} else {
					//lista.hide();
					triangulo.removeClass("trianguloinf").addClass("triangulosup");
					triangulo.parent().css('z-index', 3000);
				}
				
			}
		);
		
		$(".caja").on("click","li",
				function(e){
					
					var texto = $(this).text();
					var seleccionado = $(this).parent().prev(); 
					//var seleccionado = $(this).parent().find(".seleccionado"); 
					
					
					var valor=$(this).val();
					
					var lista = $(this).closest("ul");
					var triangulo = $(this).parent().next();
					//var lista = $(this).find("ul").toggle();

					//var lista = $(this).parents("li").find(".listaselect");
					//var triangulo = $(this).find("span:last-child");
					//var oculto = $(this).parents("li").find(".caja").attr("id");
					
					e.preventDefault();
					e.stopPropagation();
					seleccionado.text(texto);
					//alert("DIV: "+oculto);
					
					//alert("VALOR: "+valor);
					//seleccionado.val(valor);
					seleccionado.attr("id",valor);
					lista.hide();
					$(".caja").unbind('mouseleave');
					triangulo.removeClass("triangulosup").addClass("trianguloinf");

					triangulo.parent().css('z-index', 0);


					var nuevoModulo=$("#modulo .seleccionado").attr("id");
					/*
					//modificacion de un modulo
					if(editarModulo){

						cargarCatalogoModulosPadre();
						$("#moduloPadre").css('display', "inline-block");
						mostrarComponente("agregarNuevoModulo");
						mostrarComponente("nuevoModulo");
					}*/
					//alert("ID: "+$(this).attr("id"));
					if(idModEditar!=nuevoModulo && nuevoModulo!="-2"){

						cargarCatalogoModulosPadre();
						//linea comentada para agregar modulo padre
						//$("#moduloPadre").css('display', "inline-block");
						
						mostrarComponente("agregarNuevoModulo");
						mostrarComponente("nuevoModulo");
						$("#nuevoModulo").val("");
						editarModulo=false;
						nuevoMod=false;
						
					}
					//si no se edita
					if(nuevoModulo!="-2" && !editarModulo) {
						ocultarComponente("nuevoModulo");
						ocultarComponente("agregarNuevoModulo");

						$("#moduloPadre").css('display', "none");

						
					}
					//para agregar una nueva pregunta
					if(nuevoModulo=="-2" && !nuevoMod){

						cargarCatalogoModulosPadre();
						//linea comentada para agregar modulo padre
						//$("#moduloPadre").css('display', "inline-block");
						mostrarComponente("agregarNuevoModulo");
						mostrarComponente("nuevoModulo");
						//$("#nuevoModulo").val("");
						editarModulo=false;

						$("#moduloPadre .seleccionado").attr("id",0);
						$("#moduloPadre .seleccionado").text("Sin modulo padre");
						nuevoMod=true;

					}


					
				}
		);
		
}
/* se agregan los eventos del calendario*/

$( function() {
    $( "#fechaInicio" ).datepicker({

        minDate: new Date(),
        maxDate: "31/12/2050",
    	
    });
  } );

$( function() {
    $( "#fechaTermino" ).datepicker({
    	

        minDate: new Date(),
        maxDate: "31/12/2050",
    });
  } );


function eventoHoraInicio(){
	$('#horaInicio').wickedpicker({now: '12:00', twentyFour: false, title:'Hora de inicio', showSeconds: false});

	$("#wickedpicker__title").text("Hora de inicio");
	$(".wickedpicker").hide();
	
	return false;
}

		
function eventoHoraTermino(){
	$('#horaTermino').wickedpicker({now: '12:00', twentyFour: false, title:'Hora de termino', showSeconds: false});

	$("#wickedpicker__title").text("Hora de termino");
	$(".wickedpicker").hide();
	
	return false;
}
/************************************EDICION DE MODULOS****************************************************/

function cargarCatalogoModulos() {
	$
	.ajax({
		type : "GET",
		url : "http://"+urlServer + '../consultaChecklistService/getModulosService.json',
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var i = 0;
			catalogoModulo=new Array();
			var contenido='<li value="-1"><a href="#">Categoria</a></li>';
			contenido+='<li value="-2" ><a href="#">Agregar nuevo</a></li>';
			
			while (json[i] != null) {
				//document.forms.formulario.tipo.options[i+1]  = new Option(json[i].nombreCheck, json[i].idChecklist);
				catalogoModulo.push(
					{
						"id":json[i].idModulo,
						"nombre":json[i].nombre,
						"idModuloPadre":json[i].idModuloPadre,
						"nombreModuloPadre":json[i].nombrePadre
					}
				)
				contenido+='<li value="'+json[i].idModulo+'" style="text-align : left;" ><a href="#">'+json[i].nombre+'</a></li>';
				
				i++;
				
			}

			$("#modulo .listaselect").html(contenido);
			//alert("se ha cargado el catalogo!, total de regs: "+posiblesRespuestas.length);
		},
		error : function() {
			alert('Algo paso al cargar las categorias');
		}
 		,async: false
	});
}


function cargarCatalogoModulosPadre() {
	$
	.ajax({
		type : "GET",
		url : "http://"+urlServer + '../consultaChecklistService/getModulosService.json',
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var i = 0;
			//catalogoTipoChecklist=new Array();
			var contenido='<li value="0"><a href="#">Sin modulo padre</a></li>';
			
			while (json[i] != null) {
				//document.forms.formulario.tipo.options[i+1]  = new Option(json[i].nombreCheck, json[i].idChecklist);
				
				contenido+='<li value="'+json[i].idModulo+'" ><a href="#">'+json[i].nombre+'</a></li>';
				
				i++;
				
			}

			$("#moduloPadre .listaselect").html(contenido);
			//alert("se ha cargado el catalogo!, total de regs: "+posiblesRespuestas.length);
		},
		error : function() {
			alert('Algo paso al cargar las categorias');
		}
 		,async: false

	});
}


function agregarModuloService() {
	
	var nombreModulo=$("#nuevoModulo").val();
	var idModuloPadre=$("#moduloPadre .seleccionado").attr("id");
	
	//alert("http://"+urlServer + '../checklistServices/altaModulo.json?idModuloPadre='+idModuloPadre+'&nombreMod='+nombreModulo);
	$
	.ajax({
		type : "GET",
		url : "http://"+urlServer + '../checklistServices/altaModuloTemp.json?idModuloPadre='+idModuloPadre+'&nombreMod='+nombreModulo+"&numVersion="+numVersion+"&tipoModif=I",
		//contentType : "application/json; charset=utf-8",
		//dataType : "json",
		//paramName : "tagName",
		//delimiter : ",",
		success : function() {
			//alert("Se agrego correctamente la categoria: "+nombreModulo);
			cargarCatalogoModulos();

		},
		error : function() {
			alert('Algo paso al agregar la categoria');
			
		}
 		,async: false

	});
	
}
function activarEdicionModulo(){
	if($("#modulo .seleccionado").attr("id")=="-1" || $("#modulo .seleccionado").attr("id")=="-2"){
		alert("No has seleccionado ninguna opción");
	}else{

		$("#moduloPadre").css('display', "inline-block");
		mostrarComponente("agregarNuevoModulo");
		mostrarComponente("nuevoModulo");

		cargarCatalogoModulosPadre();
		
		editarModulo=true;
		nuevoMod=false;
		idModEditar=$("#modulo .seleccionado").attr("id");

		//se muestra la descripcion del modulo
		$("#nuevoModulo").val(
				$("#modulo .seleccionado").text());
		
		for(var i=0;i<catalogoModulo.length;i++){

			if(catalogoModulo[i].id==$("#modulo .seleccionado").attr("id")){

				$("#moduloPadre .seleccionado").attr("id",catalogoModulo[i].idModuloPadre);
				if(catalogoModulo[i].idModuloPadre=="0"){
					$("#moduloPadre .seleccionado").text("Sin modulo padre");
				}else{

					$("#moduloPadre .seleccionado").text(catalogoModulo[i].nombreModuloPadre);
				}
			}
		}
		
				
	}

	return false;
}



function modificarModuloService() {
	var nombreModulo=$("#nuevoModulo").val();
	var idModulo=$("#modulo .seleccionado").attr("id");
	var idModuloPadre=$("#moduloPadre .seleccionado").attr("id");
	
	$
	.ajax({
		type : "GET",
		url : "http://"+urlServer + '../checklistServices/altaModuloTemp.json?idModulo='+idModulo+'&idModuloPadre='+idModuloPadre+'&nombreMod='+nombreModulo+"&numVersion="+numVersion+"&tipoModif=U",
		//contentType : "application/json; charset=utf-8",
		//dataType : "json",
		//paramName : "tagName",
		//delimiter : ",",
		success : function() {
			//alert("Se actualizo correctamente la categoria: "+nombreModulo);
			//se actualiza el catalogo 
			cargarCatalogoModulos();

		},
		error : function() {
			alert('Algo paso al modificar la categoria');
			
		}
 		,async: false
	});
	
}

function eliminarModulo(){
	eliminarModuloService();
	
	return false;
}

function eliminarModuloService() {
	var nombreModulo=$("#modulo .seleccionado").text();
	var idModulo=$("#modulo .seleccionado").attr("id");
	
	if(idModulo=="-1" || idModulo=="-2"){
		alert("No has seleccionado ninguna opcion");
		return false;
	}
	else{
		if(confirm("¿Deseas eliminar este registro["+nombreModulo+"]?\n")){
			$
			.ajax({
				type : "GET",
				url : "http://"+urlServer + '../checklistServices/eliminaModulo.json?idModulo='+idModulo,
				//contentType : "application/json; charset=utf-8",
				//dataType : "json",
				//paramName : "tagName",
				//delimiter : ",",
				success : function() {
					alert("Se elimino  correctamente la categoria: "+nombreModulo);
					//se actualiza el catalogo de los tipos de checklist
					cargarCatalogoModulos();
					editarModulo=false;

					cargarCatalogoModulosPadre();
					$("#moduloPadre").css('display', "none");
					ocultarComponente("agregarNuevoModulo");
					ocultarComponente("nuevoModulo");
					$("#nuevoModulo").val("");
					$("#moduloPadre .seleccionado").attr("id",0);
					$("#moduloPadre .seleccionado").text("Sin modulo padre");
					

					$("#modulo .seleccionado").attr("id",-1);
					$("#modulo .seleccionado").text("Categoria");

				},
				error : function() {
					alert('Algo paso al eliminar la categoria');
					
				}
		 		,async: false
			});
		}		
	}
	
	//alert("http://"+urlServer + '../checklistServices/eliminaTipoCheck.json?idTipoCheck='+idTipoCheck);
	
	return false;
	
}


function agregarNuevoMod(){

	var nuevoTipo = $("#nuevoModulo").val();
	if(nuevoTipo.trim()==""){

		alert("No has ingresado el nombre de la categoria");
	}else{
		if(editarModulo){
			//opcion para actualizar un modulo
			editarModulo=false;
			modificarModuloService();
			idModEditar=0;
			
			$("#nuevoModulo").val("");
			ocultarComponente("nuevoModulo");
			ocultarComponente("agregarNuevoModulo");

			$("#moduloPadre").css('display', "none");

			$("#moduloPadre .seleccionado").attr("id",0);
			$("#moduloPadre .seleccionado").text("Sin modulo padre");

			$("#modulo .seleccionado").attr("id",-1);
			$("#modulo .seleccionado").text("Categoria");
			
			
		}else{

			//opcion para agregar nuevo modulo
			agregarModuloService();
			
			$("#nuevoModulo").val("");
			ocultarComponente("nuevoModulo");
			ocultarComponente("agregarNuevoModulo");

			$("#moduloPadre").css('display', "none");

			$("#moduloPadre .seleccionado").attr("id",0);
			$("#moduloPadre .seleccionado").text("Sin modulo padre");

			$("#modulo .seleccionado").attr("id",-1);
			$("#modulo .seleccionado").text("Categoria");
		}
	}
	
	return false;
}




$(document).ready(
		function() {
			$("#agregarPregunta").click(function() {
						
						
										var nombrePregunta = $("#preguntaText").val();
										var tipoPreg = $("#tipoPreg span").text();
										var modulo = $("#modulo span").text();
										
										var tipoPregId=$("#tipoPreg span").attr("id");
										var moduloId=$("#modulo span").attr("id");

										if(pregSeleccionada==-1){
											if (tipoPregId == -1) {
												alert("No has seleccionado el tipo de pregunta");
												return false;
											}
											if (moduloId == -1 || moduloId==-2) {
												alert("No has seleccionado alguna categoria de la pregunta");
												return false;
											}
											if (nombrePregunta.trim() == "") {
												alert("El nombre de la pregunta no puede ser un campo vacio");
												return false;
											}

											var idPreg=preguntas.length;
											var preg={
													"id":idPreg,
													"descripcion":nombrePregunta,
													"idTipo":tipoPregId,
													"tipo":tipoPreg,
													"moduloId":moduloId,
													"modulo":modulo,
													"idPregBD":0
													};
											altaPreguntaService(preg);
											
										}else{
											//si la pregunta existe se edita
											for(var i=0;i<preguntas.length;i++){
												if(preguntas[i].id==pregSeleccionada){
													//alert("NOMBRE VAR:1 ");
													preguntas[i].descripcion=nombrePregunta;
													preguntas[i].idTipo=tipoPregId;
													preguntas[i].tipo=tipoPreg;
													preguntas[i].moduloId=moduloId;
													preguntas[i].modulo=modulo;
													//se actualiza la pregunta en la base de datos
													modificarPreguntaService(preguntas[i]);
													
													//alert("NOMBRE VAR: 2");
													var contenidoNuevo=
															preguntas[i].descripcion + " - "
															+ preguntas[i].modulo 

															+"<button onclick='return mostrarPregunta("+preguntas[i].id+");' class='botonEditar'></button>"
															+"<button onclick='return eliminarPregunta("+preguntas[i].id+");' class='botonEliminar'></button>"
															+"<button onclick='subirNivel("+preguntas[i].id+")' class='botonContraer'></button>"
															+"<button onclick='bajarNivel("+preguntas[i].id+")' class='botonDesplegar' ></button>"
															
															+ vistaPrevia(tipoPregId);
													//alert("NOMBRE VAR: 3"+"preg"+preguntas[i].id);
													//$("#preg"+preguntas[i].id).remove();
													//$("#preg"+preguntas[i].id).append($contenidoNuevo);
													
													document.getElementById("preg"+preguntas[i].id).innerHTML=contenidoNuevo;
													

													$("#tipoPreg").css('pointer-events', "auto");
												}
											}
											
										}
										
										limpiarCamposPregunta();
										
										return false;
										

									});
				}

		);


function limpiarCamposPregunta(){

	//se regresa el estatus de la pregunta
	pregSeleccionada=-1;
	

	//se agrega la informacion de la pregunta
	$("#preguntaText").val("");
	$("#modulo .seleccionado").attr("id",-1);
	$("#tipoPreg .seleccionado").attr("id",-1);
	
	$("#modulo .seleccionado").text("Categoria");
	$("#tipoPreg .seleccionado").text("Tipo de pregunta");
	
}
function mostrarPregunta(idPregunta){
	for(var i=0;i<preguntas.length;i++){
		if(preguntas[i].id==idPregunta){
			//se agrega la informacion de la pregunta
			$("#preguntaText").val(preguntas[i].descripcion);
			$("#modulo .seleccionado").attr("id",preguntas[i].moduloId);
			$("#tipoPreg .seleccionado").attr("id",preguntas[i].idTipo);
			$("#modulo .seleccionado").text(preguntas[i].modulo);
			$("#tipoPreg .seleccionado").text(preguntas[i].tipo);
			//se asigna la pregunta seleccionada
			pregSeleccionada=idPregunta;
			
			$("#tipoPreg").css('pointer-events', "none");
			ocultarComponente('tipoPreg');
		}
	}
	
	return false;
}

function subirNivel(idPregunta){

	limpiarCamposPregunta();
	
	for(var i=0;i<preguntas.length;i++){
		
		if(preguntas[i].id==idPregunta){
			if(i>0 && i<preguntas.length){
				var pregTmp=preguntas[i-1];
				var idTmp;
				preguntas[i-1]=preguntas[i];
				preguntas[i]=pregTmp;
				
				//se actualizan los id
				idTmp=preguntas[i].id;
				preguntas[i].id=preguntas[i-1].id;
				preguntas[i-1].id=idTmp;
				
				
			}
		}
	}

	var preguntaHtml="";
	for(var i=0;i<preguntas.length;i++){
		preguntaHtml+= "<li id='preg"
				+ (preguntas[i].id ) + "'  >"
				+ preguntas[i].descripcion + " - "
				
				+ preguntas[i].modulo 
				+"<button onclick='return mostrarPregunta("+preguntas[i].id+");' class='botonEditar'></button>"
				+"<button onclick='return eliminarPregunta("+preguntas[i].id+");' class='botonEliminar'></button>"
				+"<button onclick='subirNivel("+preguntas[i].id+")' class='botonContraer'></button>"
				+"<button onclick='bajarNivel("+preguntas[i].id+")' class='botonDesplegar' ></button>"
				
				+ vistaPrevia(preguntas[i].idTipo)
				
				+ "</li>";
		//se actualiza el orden de la pregunta
		modificarCheckPregService(preguntas[i].idPregBD,preguntas[i].id+1);
		
		
	}

	//alert(preguntaHtml);

	document.getElementById("preguntas").innerHTML=preguntaHtml;

	
	return false;
}
function bajarNivel(idPregunta){

	limpiarCamposPregunta();
	
	for(var i=0;i<preguntas.length;i++){
		
		if(preguntas[i].id==idPregunta){
			if(i>=0 && i<preguntas.length-1){
				var pregTmp=preguntas[i+1];
				var idTmp;
				preguntas[i+1]=preguntas[i];
				preguntas[i]=pregTmp;
				
				//se actualizan los id
				idTmp=preguntas[i].id;
				preguntas[i].id=preguntas[i+1].id;
				preguntas[i+1].id=idTmp;
				
				
			}
		}
	}

	var preguntaHtml="";
	for(var i=0;i<preguntas.length;i++){
		preguntaHtml+= "<li id='preg"
				+ (preguntas[i].id ) + "'  >"
				+ preguntas[i].descripcion + " - "
				
				+ preguntas[i].modulo 
				+"<button onclick='return mostrarPregunta("+preguntas[i].id+");' class='botonEditar'></button>"
				
				+"<button onclick='return eliminarPregunta("+preguntas[i].id+");' class='botonEliminar'></button>"
				+"<button onclick='subirNivel("+preguntas[i].id+")' class='botonContraer'></button>"
				+"<button onclick='bajarNivel("+preguntas[i].id+")' class='botonDesplegar' ></button>"
				
				+ vistaPrevia(preguntas[i].idTipo)
				
				+ "</li>";
	
		//se actualiza el orden de la pregunta
		modificarCheckPregService(preguntas[i].idPregBD,preguntas[i].id+1);
			
			
	}

	document.getElementById("preguntas").innerHTML=preguntaHtml;
		
	
	
	return false;
}


function vistaPrevia(idTipoPreg){
	

	var vistaPreviaRespuesta = "";
	vistaPreviaRespuesta = "<ul>";
	for(var i=0;i<posiblesRespuestas.length;i++){ 
		if(posiblesRespuestas[i].idTipo==idTipoPreg){
			vistaPreviaRespuesta += "<li>"+posiblesRespuestas[i].respuesta+"</li>";
		}
	}

	vistaPreviaRespuesta += "</ul>";
	
	return vistaPreviaRespuesta;
	
}


function eliminarPregunta(idPregunta){
	
	limpiarCamposPregunta();
	
	
	
	var preguntasTmp=new Array();
	
	for(var i=0;i<preguntas.length;i++){
		
		if(preguntas[i].id!=idPregunta){
			preguntas[i].id=preguntasTmp.length;
			preguntasTmp.push(preguntas[i]);
		}else{
			//se eliminan de la base de datos
			eliminaCheckPregService(preguntas[i],preguntas[i].idPregBD, (preguntas[i].id+1));
			
		}
		
	}
	
	//se actualiza el array 
	preguntas=preguntasTmp;
	//se actualiza el contenido
	var preguntaHtml="";
	for(var i=0;i<preguntas.length;i++){
		preguntaHtml+= "<li id='preg"
				+ (preguntas[i].id ) + "'  >"
				+ preguntas[i].descripcion + " - "
				
				+ preguntas[i].modulo 
				+"<button onclick='return mostrarPregunta("+preguntas[i].id+");' class='botonEditar'></button>"
				+"<button onclick='return eliminarPregunta("+preguntas[i].id+");' class='botonEliminar'></button>"
				+"<button onclick='subirNivel("+preguntas[i].id+")' class='botonContraer'></button>"
				+"<button onclick='bajarNivel("+preguntas[i].id+")' class='botonDesplegar' ></button>"
				
				+ vistaPrevia(preguntas[i].idTipo)
				
				+ "</li>";
	}

	document.getElementById("preguntas").innerHTML=preguntaHtml;
	
	return false;
}

function cargarPosiblesResp() {
	$
	.ajax({
		type : "GET",
		url :encodeURI("http://"+urlServer + '../consultaCatalogosService/getPosibleTipoPreguntaService.json?idTipoPregunta='),
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var i = 0;
			while (json[i] != null) {
				//document.forms.formulario.tipo.options[i+1]  = new Option(json[i].nombreCheck, json[i].idChecklist);
				posiblesRespuestas.push(
					{
						"id":json[i].idPosibleTipoPregunta,
						"idTipo":json[i].idTipoPregunta,
						"idPosibleRespuesta":json[i].idPosibleRespuesta,
						"respuesta":json[i].descripcionPosible
					}
				)
				i++;


				//alert("REG "+i+"--> id: "+posiblesRespuestas[posiblesRespuestas.length-1].id+"idTipo: "+posiblesRespuestas[posiblesRespuestas.length-1].idTipo+" respuesta: "+posiblesRespuestas[posiblesRespuestas.length-1].respuesta);
			}

			//cargarValoresIniciales();
			//alert("POS: "+posiblesRespuestas.length);
			//alert("se ha cargado el catalogo!, total de regs: "+posiblesRespuestas.length);
		},
		error : function() {
			alert('Algo paso al cargar los tipos de posibles respuestas');
		},
		async: false
	});
}


function altaPreguntaService(pregX) {
	// http://localhost:8080../checklistServices/altaPregunta.json?idMod=<?>&idTipo=<?>&estatus=<?>&setPregunta=<?>&commit=<?>
	//alert( "http://"+urlServer + '../checklistServices/altaPregunta.json?idMod='+pregX.moduloId+'&idTipo='+pregX.idTipo+'&estatus=1&setPregunta='+pregX.descripcion+'&commit=1');

	//alert("ENTRO!");
	//alert(encodeURI("http://"+urlServer + '../checklistServices/altaPreguntaService.json?idMod='+pregX.moduloId+'&idTipo='+pregX.idTipo+'&estatus=1&setPregunta='+utf8_encode(pregX.descripcion)+'&commit=1'));
	
	$
	.ajax({
		type : "GET",
		url : encodeURI("http://"+urlServer+'../checklistServices/altaPreguntaServiceTemp.json?idPregunta=0&idMod='+pregX.moduloId+'&idTipo='+pregX.idTipo+'&estatus=1&setPregunta='+pregX.descripcion+'&commit=1&numRevision='+numVersion+'&tipoCambio=I'),

		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			//alert("ID PREG: "+json);
			
			if(json!=0){

					var preg={
							"id":pregX.id,
							"descripcion":pregX.descripcion,
							"idTipo":pregX.idTipo,
							"tipo":pregX.tipo,
							"moduloId":pregX.moduloId,
							"modulo":pregX.modulo,
							"idPregBD":json
							};
					//se agrega un objeto de tipo pregunta

					//altaPreguntaService(preg);
					//preg.idPregBD=idPregBD;
					

					preguntas.push(preg);
					//alert("MENSAJE BD: "+preguntas[preguntas.length-1].idPregBD);
					
		
					var $myNewElement = $("<li id='preg"
							+ (preg.id ) + "'   >"
							+ preg.descripcion + " - "
							+ preg.modulo 

							+"<button onclick='return mostrarPregunta("+preg.id+");' class='botonEditar'></button>"
							+"<button onclick='return eliminarPregunta("+preg.id+");' class='botonEliminar'></button>"
							
							+"<button onclick='subirNivel("+preg.id+")' class='botonContraer'></button>"
							+"<button onclick='bajarNivel("+preg.id+")' class='botonDesplegar' ></button>"
							+ vistaPrevia(preg.idTipo)
							+ "</li>");
					$("#preguntas").append($myNewElement);

					$("#vistaPrevia").animate({scrollTop:$("#vistaPrevia")[0].scrollHeight}, 0);
					
				 
				
			

				//alert("Se agrego correctamente la pregunta "+preguntas[preguntas.length-1].idPregBD);
				//alert("TOTAL PREGUNTAS: "+preguntas.length);
				//se agrega el orden de la pregunta
				altaCheckPregService(preg,idChecklist);
				
		
			}else{
				alert('Algo paso al agregar la pregunta');
				
			}


		},
		error : function() {

			alert('Algo paso al agregar la pregunta!');

			
		}
 		,async: false
	});
	
}

function altaCheckPregService(preguntaX,idChecklistX) {
	// http://localhost:8080../checklistServices/altaPregCheck.json?idCheck=<?>&ordenPreg=<?>&idPreg=<?>&commit=<?>
	//alert("http://"+urlServer + '../checklistServices/altaPregCheckService.json?idCheck='+idChecklistX+'&ordenPreg='+(preguntaX.id+1)+'&idPreg='+preguntaX.idPregBD+'&commit=1');
	$
	.ajax({
		type : "GET",
		url : encodeURI("http://"+urlServer + '../checklistServices/altaPregCheckTempService.json?idCheck='+idChecklistX+'&ordenPreg='+(preguntaX.id+1)+'&idPreg='+preguntaX.idPregBD+'&commit=1&pregPadre=0&numRevision='+numVersion+'&tipoCambio=I'),
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			if(json==true){
				//alert("Se agrego correctamente el orden de la pregunta");
					
			}else{
				alert('Algo paso al agregar el orden de la pregunta');
			
			}
			

		},
		error : function() {

			alert('Algo paso al agregar el orden de la pregunta');

			
		}
 		,async: false
	});
	
}

function eliminaCheckPregService(pregunta,idPreguntaBD,orden) {

	//alert("http://"+urlServer+'../checklistServices/altaPregCheckTempService.json?idCheck='+idChecklist+'&ordenPreg='+orden+'&idPreg='+idPreguntaBD+'&commit=1&pregPadre=0&numRevision='+numVersion+'&tipoCambio=D');
	$
	.ajax({
		type : "GET",
		url : encodeURI("http://"+urlServer+'../checklistServices/altaPregCheckTempService.json?idCheck='+idChecklist+'&ordenPreg='+orden+'&idPreg='+idPreguntaBD+'&commit=1&pregPadre=0&numRevision='+numVersion+'&tipoCambio=D'),
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			//alert("ID PREG: "+json);
			
			if(json==true){
				//alert("Se eliminó correctamente el orden de la pregunta");
				eliminarPreguntaService(pregunta); 
					
			}else{
				alert('Algo paso al eliminar el orden de la pregunta');
			
			}
			

		},
		error : function() {

			alert('Algo paso al eliminar la pregunta');
		}
 		,async: false
	});
	
}


function eliminarPreguntaService(pregX) {
	
	//alert( "http://"+urlServer + '../checklistServices/eliminaPreguntaService.json?idPregunta='+idPreguntaBD);

	$
	.ajax({
		type : "GET",
		url : encodeURI("http://"+urlServer+'../checklistServices/altaPreguntaServiceTemp.json?idPregunta='+pregX.idPregBD+'&idMod='+pregX.moduloId+'&idTipo='+pregX.idTipo+'&estatus=1&setPregunta='+pregX.descripcion+'&commit=1&numRevision='+numVersion+'&tipoCambio=D'),
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			//alert("ID PREG: "+json);
			
			//if(json==true){
			if(json	!=	0){
				alert("Se eliminó correctamente la pregunta");
					
			}else{
				alert('Algo paso al eliminar la pregunta');
			
			}
			

		},
		error : function() {

			alert('Algo paso al eliminar la pregunta');
		}
 		,async: false
		});
	
}


function modificarPreguntaService(pregX) {
	//alert( "http://"+urlServer + '../checklistServices/eliminaPreguntaService.json?idPregunta='+idPreguntaBD);
	// http://localhost:8080../checklistServices/updatePregunta.json?idPregunta=<?>&idMod=<?>&idTipo=<?>&estatus=<?>&setPregunta=<?>
	$
	.ajax({
		type : "GET",
		url :encodeURI( "http://"+urlServer + 
		'../checklistServices/altaPreguntaServiceTemp.json?idPregunta='+pregX.idPregBD+'&idMod='+pregX.moduloId+'&idTipo='+pregX.idTipo+'&estatus=1&setPregunta='+pregX.descripcion+'&commit=1&numRevision='+numVersion+'&tipoCambio=U'),
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			if(json	=!	0){
				modificarCheckPregService(pregX.idPregBD,(pregX.id+1));
			}else{
				alert('Algo paso al modificar la pregunta');
			
			}
			

		},
		error : function() {

			alert('Algo paso al modificar la pregunta');
		}
 		,async: false
	});
	
}

function modificarCheckPregService(idPreguntaBD,orden) {
	$
	.ajax({
		type : "GET",
		url : encodeURI("http://"+urlServer+'../checklistServices/altaPregCheckTempService.json?idCheck='+idChecklist+'&ordenPreg='+orden+'&idPreg='+idPreguntaBD+'&commit=1&pregPadre=0&numRevision='+numVersion+'&tipoCambio=U'),
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			
			if(json==true){
				//alert("Se modificó correctamente el orden de la pregunta");
					
			}else{
				alert('Algo paso al modificar el orden de la pregunta' );
			}
		},
		 error: function(jqXHR,error, errorThrown) {  
			 alert('Algo paso al modificar el orden de la pregunta' );
			 },async: false
	});
	
}


$(document).ready(
		function() {
			$("#continuar").click(function(){
				if(preguntas.length>0){
					enviarPreguntas();
					//$("input[value='formPreguntas']").submit();
					//return false;
				}else{
					alert("No has agregado preguntas");
					return false;
				}
			
			}
			
		)});


 function enviarPreguntas(){
	 /*
	 //se envian las preguntas por metodo post // construct an HTTP request
	  var xhr = new XMLHttpRequest();
	  xhr.open("post", "checklistDesignF3.htm", true);
	  xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

	  // send the collected data as JSON
	  xhr.send(JSON.stringify(preguntas));
	  */
	 /**/
	 //ALTA CHECK PREG DE TODAS LAS PREGUNTAS DEL ARRAY PREGUNTAS
	 //alert (preguntas.length);
	 for(var i=0;i<preguntas.length;i++){
		 	var j = (i + 1)
			modificarCheckPregService(preguntas[i].idPregBD,j);
			//alert ("Le vendi humo a URI" + preguntas[i].descripcion);
	 }
	
	 //LLAMA A SERVICIO PARA ACTUALIZAR ESTADO DE CHECKLIST
	 actualizaEstadoCheck(29);
	 
	  //preguntas JSON 
	  $("#preguntasJSON").val(JSON.stringify(preguntas));
	  //alert("JSON PREG: "+$("#preguntasJSON").val());
	  
	  return true;
	  
 }

 function actualizaEstadoCheck (estado){
	 $.ajax({
	 		type : "GET",
	 		url : encodeURI("http://"+urlServer + '../checklistServices/updateEstadoCheck.json?idCheck='+idChecklist+'&estado='+estado),
	 		contentType : "application/json; charset=utf-8",
	 		dataType : "json",
	 		paramName : "tagName",
	 		delimiter : ",",
	 		success : function(json) {
	 			//alert ("Estado Check actualizado Correctamente");
	 		},
	 		error : function() {
	 			alert('Algo paso al actualizar el estado del check');
	 		},
	 		async: false
	 	});
 }
 

 function cargarPreguntaPreviasService() {
	 $.ajax({
 		type : "GET",
 		url : encodeURI("http://"+urlServer + '../consultaChecklistService/getCheckPreguntaService.json?idCheck='+idChecklist),
 		contentType : "application/json; charset=utf-8",
 		dataType : "json",
 		paramName : "tagName",
 		delimiter : ",",
 		success : function(json) {
 			var jsonC = JSON.stringify(json);
 			var i = 0;
 			while (json[i] != null) {
				var pregTmp={
						"id":preguntas.length,
						"descripcion":json[i].pregunta,
						"idTipo":json[i].idTipoPregunta,
						"tipo":json[i].descTipo,
						"moduloId":json[i].idModulo,
						"modulo":json[i].nombreModulo,
						"idPregBD":json[i].idPregunta
						};
 				//document.forms.formulario.tipo.options[i+1]  = new Option(json[i].nombreCheck, json[i].idChecklist);
 				preguntas.push(pregTmp);
 				i++;

 				var $myNewElement = $("<li id='preg"
						+ (pregTmp.id ) + "'   >"
						+ pregTmp.descripcion + " - "
						+ pregTmp.modulo 

						+"<button onclick='return mostrarPregunta("+pregTmp.id+");' class='botonEditar'></button>"
						+"<button onclick='return eliminarPregunta("+pregTmp.id+");' class='botonEliminar'></button>"
						
						+"<button onclick='subirNivel("+pregTmp.id+")' class='botonContraer'></button>"
						+"<button onclick='bajarNivel("+pregTmp.id+")' class='botonDesplegar' ></button>"
						+ vistaPrevia(pregTmp.idTipo)
						+ "</li>");
				$("#preguntas").append($myNewElement);

				$("#vistaPrevia").animate({scrollTop:$("#vistaPrevia")[0].scrollHeight}, 0);
				
				
			 
			

 				//alert("REG "+i+"--> id: "+posiblesRespuestas[posiblesRespuestas.length-1].id+"idTipo: "+posiblesRespuestas[posiblesRespuestas.length-1].idTipo+" respuesta: "+posiblesRespuestas[posiblesRespuestas.length-1].respuesta);
 			}

 			//alert("preguntas cargadas: "+preguntas.length);
 			//alert("se ha cargado el catalogo!, total de regs: "+posiblesRespuestas.length);
 		},
 		error : function() {
 			alert('Algo paso al cargar las preguntas previas');
 		},
 		async: false
 	});
 }
 