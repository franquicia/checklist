package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.AltaGerenteDTO;


public class AltaGerenteRowMapper implements RowMapper<AltaGerenteDTO> {

	public AltaGerenteDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AltaGerenteDTO altaGerenteDTO = new AltaGerenteDTO();
		
		altaGerenteDTO.setIdGerente(rs.getInt("FIGERENTEID"));
		altaGerenteDTO.setNomGerente(rs.getString("FCNOMGERENTE"));
	
		return altaGerenteDTO;
	}
}
