package com.gruposalinas.checklist.util;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

import com.gruposalinas.checklist.controller.LoginController;

public class UtilDate {
	private static Logger logger = LogManager.getLogger(UtilDate.class);
	
	public static  String getSysDate(String format){
		Calendar cal = Calendar.getInstance();
		String formatted = "";
		if(format!=null && !format.equals("")){
			SimpleDateFormat format1 = new SimpleDateFormat(format);
			//formatted = format1.format(DateUtils.addMinutes(cal.getTime(), 18)); //en desarrollo el tiempo esta mal por 18 minutos aprox
			formatted = format1.format(cal.getTime());
		}
		return formatted;
	}
	
	public static  String getFechaAyer(String format){
		
		Date hoy = new Date();
		Date ayer = new Date( hoy.getTime()-86400000);

		String formatted = "";
		if(format!=null && !format.equals("")){
			SimpleDateFormat format1 = new SimpleDateFormat(format);
			formatted = format1.format(ayer);
		}
		return formatted;
	}
	
	public static  int getAnioActual(){
		int year = Calendar.getInstance().get(Calendar.YEAR);
		return year;
	}
	
	public  int getMesActual(){
		int month = Calendar.getInstance().get(Calendar.MONTH);
		return month;
	}

	public static  Date stringToDate(String fechaOriginal){
		SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy HH:mm:ss");
		Date fecha = null;
		try {
			fecha = format.parse(fechaOriginal);
		} catch (ParseException e) {
			logger.info("Ocurrio algo...");
		}
		return fecha;
	}
	
	public static  String dateToString(Date fecha, String format){
		String formatted = "";
		if(format!=null && !format.equals("")){
			SimpleDateFormat format1 = new SimpleDateFormat(format);
			//formatted = format1.format(DateUtils.addMinutes(cal.getTime(), 18)); //en desarrollo el tiempo esta mal por 18 minutos aprox
			formatted = format1.format(fecha);
		}
		return formatted;
	}
	
	public String diferenciaFecha(String fechaStart, String fechaFinal){
		String dateStart = fechaStart;
		String dateFinal = fechaFinal;
		String respuesta="";
		SimpleDateFormat format = new SimpleDateFormat("ddMMyyyyHHmmss");
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateFinal);
			DateTime dt1 = new DateTime(d1);
			DateTime dt2 = new DateTime(d2);
			respuesta=(Days.daysBetween(dt1, dt2).getDays() + " dd, ");
			respuesta=respuesta+(Hours.hoursBetween(dt1, dt2).getHours() % 24 + " HH, ");
			respuesta=respuesta+(Minutes.minutesBetween(dt1, dt2).getMinutes() % 60 + " mm, ");
			respuesta=respuesta+(Seconds.secondsBetween(dt1, dt2).getSeconds() % 60 + " ss.");

		 } catch (Exception e) {
				logger.info("Ocurrio algo...");
		 }
		return respuesta;
	}
	
	public boolean validaFecha(String fechaStart, String fechaFinal){
		String dateStart = fechaStart;
		String dateFinal = fechaFinal;
		boolean validacion=false;
		int dia=0,hor=0,min=0,seg=0;
		SimpleDateFormat format = new SimpleDateFormat("ddMMyyyyHHmmss");
		Date d1 = null;
		Date d2 = null;
		int minDif=10; //minutos que puede tener de diferencia
		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateFinal);
			DateTime dt1 = new DateTime(d1);
			DateTime dt2 = new DateTime(d2);
			dia=Days.daysBetween(dt1, dt2).getDays();
			
			if(dia==0){
				hor=Hours.hoursBetween(dt1, dt2).getHours() % 24;
				if(hor==0){
					min=Minutes.minutesBetween(dt1, dt2).getMinutes() % 60;
					if( min<(minDif+1) && min>-(minDif+1) ){
						seg=Seconds.secondsBetween(dt1, dt2).getSeconds() % 60;
						if( (min==minDif && seg==0) || (min==-minDif && seg==0)){
							validacion=true;
						}else{
							validacion=false;
						}
						if(min<minDif && min>-minDif){
							validacion=true;
						}					
					}
				}
			}		
		 } catch (Exception e) {
				logger.info("Ocurrio algo...");
		 }
		return validacion;
	}
	
	public int calculaDiasMes(int a, int m) throws IOException {

		if ((m == 1) || (m == 3) || (m == 5) || (m == 7) || (m == 8) || (m == 10) || (m == 12)) {

			return 31;

		} else if ((m == 4) || (m == 6) || (m == 9) || (m == 11)) {

			return 30;

		} else if (m == 2) {

			if ((((a % 4) == 0) && ((a % 100)) != 100) || ((a % 400) == 0)) {

				return 29;

			}

			return 28;

		} else {

			return -1;

		}
	}
	
}
