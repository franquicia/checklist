package com.gruposalinas.checklist.domain;

public class VisitasxMesDTO {
	
	private int visitas;
	private int usuario;
	private String ceco;
	private String nombreCeco;
	private String mes;
	
	public int getVisitas() {
		return visitas;
	}
	public void setVisitas(int visitas) {
		this.visitas = visitas;
	}
	public int getUsuario() {
		return usuario;
	}
	public void setUsuario(int usuario) {
		this.usuario = usuario;
	}
	public String getCeco() {
		return ceco;
	}
	public void setCeco(String ceco) {
		this.ceco = ceco;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}

}
