package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.EmpFijoDTO;

public class EmpFijoRowMapper implements RowMapper<EmpFijoDTO> {

	public EmpFijoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		EmpFijoDTO EmpFijoDTO = new EmpFijoDTO();
		
		EmpFijoDTO.setIdEmpFijo(rs.getInt("FIIDEMPFIJO"));
	    EmpFijoDTO.setIdUsuario(rs.getInt("FIIDUSUARIO"));
	    EmpFijoDTO.setIdActivo(rs.getInt("FIIDACTIVO"));
		
		return EmpFijoDTO;
	}

}


