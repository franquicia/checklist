package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AsignacionCheckDTO;
import com.gruposalinas.checklist.mappers.AsignaCheckRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AsignacionCheckDAOImpl extends DefaultDAO implements AsignacionCheckDAO {

    private Logger logger = LogManager.getLogger(AsignacionDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneAsignaciones;
    private DefaultJdbcCall jdbcObtieneAsigCorreo;
    private DefaultJdbcCall jdbcInsertaAsignacionA;
    private DefaultJdbcCall jdbcActualizaAsignacion;
    private DefaultJdbcCall jdbcEliminaAsignacion;
    private DefaultJdbcCall jdbcAsignaChecklist;

    public void init() {
        jdbcObtieneAsignaciones = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMASIGNANV")
                .withProcedureName("SP_SEL_ASIGNA")
                .returningResultSet("RCL_ASIGNA", new AsignaCheckRowMapper());

        jdbcObtieneAsigCorreo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMASIGNANV")
                .withProcedureName("SP_SEL_AS_B")
                .returningResultSet("RCL_ASIGNA", new AsignaCheckRowMapper());

        jdbcInsertaAsignacionA = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMASIGNANV")
                .withProcedureName("SP_INS_ASINGA");

        jdbcActualizaAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMASIGNANV")
                .withProcedureName("SP_ACT_ASIGNA");

        jdbcEliminaAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMASIGNANV")
                .withProcedureName("SP_DEL_ASIGNA");

        jdbcAsignaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMASIGNANV")
                .withProcedureName("SP_INS_CHECKLIST");

    }

    @Override
    public List<AsignacionCheckDTO> obtieneAsignaciones(String idChecklist, String idCeco, String idUsuario, String asigna)
            throws Exception {

        List<AsignacionCheckDTO> consulta = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECKLIST", idChecklist)
                .addValue("PA_NUMSUC", idCeco)
                .addValue("PA_IDUSUARIO", idUsuario)
                .addValue("PA_ASIGNA", asigna);

        out = jdbcObtieneAsignaciones.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMASIGNANV.SP_SEL_ASIGNA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        consulta = (List<AsignacionCheckDTO>) out.get("RCL_ASIGNA");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al consultar las asignaciones");
        }

        return consulta;
    }

    @Override
    public boolean insertaAsignacion(AsignacionCheckDTO asignacion) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;

        logger.info("asignacion...  " + asignacion);

        SqlParameterSource entra = new MapSqlParameterSource().addValue("PA_IDUSUARIO", asignacion.getIdUsuario())
                .addValue("PA_NOMBRECC", asignacion.getNombreSucursal())
                .addValue("PA_NUMSUC", asignacion.getIdSucursal())
                .addValue("PA_FECHA", asignacion.getFecha())
                .addValue("PA_BUNKER", asignacion.getBunker())
                .addValue("PA_IDCHECK", asignacion.getIdChecklist())
                .addValue("PA_HORABUNKER", asignacion.getHoraBunker())
                .addValue("PA_HORABUNKERSALIDA", asignacion.getHoraBunkerSalida())
                .addValue("PA_JUSTIFICACION", asignacion.getJustificacion())
                .addValue("PA_ASIGNA", asignacion.getAsigna())
                .addValue("PA_NOMUSU", asignacion.getNombreUsuario())
                .addValue("PA_ENVIO", asignacion.getEnvio())
                .addValue("PA_CORREOD", asignacion.getCorreoD())
                .addValue("PA_CORREOC", asignacion.getCorreoC());

        out = jdbcInsertaAsignacionA.execute(entra);

        logger.info("Funcion ejecutada:{checklist.PAADMASIGNANV.SP_INS_ASINGA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();
        if (ejecucion == 0) {
            logger.info("Algo ocurrió al insertar la asignacion");
            return false;
        }

        return true;
    }

    @Override
    public boolean actualizaAsignacion(AsignacionCheckDTO asignacion) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDUSUARIO", asignacion.getIdUsuario())
                .addValue("PA_NOMBRECC", asignacion.getNombreSucursal())
                .addValue("PA_NUMSUC", asignacion.getIdSucursal())
                .addValue("PA_FECHA", asignacion.getFecha())
                .addValue("PA_BUNKER", asignacion.getBunker())
                .addValue("PA_IDCHECK", asignacion.getIdChecklist())
                .addValue("PA_HORABUNKER", asignacion.getHoraBunker())
                .addValue("PA_HORABUNKERSALIDA", asignacion.getHoraBunkerSalida())
                .addValue("PA_JUSTIFICACION", asignacion.getJustificacion())
                .addValue("PA_ASIGNA", asignacion.getAsigna())
                .addValue("PA_NOMUSU", asignacion.getNombreUsuario())
                .addValue("PA_ENVIO", asignacion.getEnvio())
                .addValue("PA_CORREOD", asignacion.getCorreoD())
                .addValue("PA_CORREOC", asignacion.getCorreoC())
                .addValue("PA_IDASIGNA", asignacion.getIdAsigna());

        out = jdbcActualizaAsignacion.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMASIGNANV.SP_ACT_ASIGNA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al actualizar la asignacion");
            return false;
        }

        return true;
    }

    @Override
    public boolean eliminaAsignacion(AsignacionCheckDTO asignacion) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDASIGNA", asignacion.getIdAsigna());

        out = jdbcEliminaAsignacion.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMASIGNANV.SP_DEL_ASIGNA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al eliminar la asignacion");
            return false;
        }

        return true;
    }

    @Override
    public List<AsignacionCheckDTO> obtieneAsignacionC(int bunker, int pais) throws Exception {

        List<AsignacionCheckDTO> consulta = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BUNKER", bunker).addValue("PA_PAIS", pais);

        out = jdbcObtieneAsigCorreo.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMASIGNANV.SP_SEL_AS_B}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        consulta = (List<AsignacionCheckDTO>) out.get("RCL_ASIGNA");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al consultar las asignaciones");
        }

        return consulta;
    }

    @Override
    public boolean asignaCheclist() throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;
        boolean salida = false;

        out = jdbcAsignaChecklist.execute();

        logger.info("Funcion ejecutada:{checklist.PAADMASIGNANV.SP_SEL_AS_B}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al consultar las asignaciones");
        } else {
            salida = true;
        }

        return salida;
    }

}
