package com.gruposalinas.checklist.servicios.servidor;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonObject;
import com.gruposalinas.checklist.business.LlaveBI;
import com.gruposalinas.checklist.business.TokenBI;
import com.gruposalinas.checklist.domain.LlaveDTO;
import com.gruposalinas.checklist.domain.TokenDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.util.StrCipher;
import com.gruposalinas.checklist.util.UtilCryptoGS;
import com.gruposalinas.checklist.util.UtilDate;

@Controller
@RequestMapping("/servicio")
public class ServicioToken {
	
	private static final Logger logger = LogManager.getLogger(ServicioToken.class);

	@Autowired
	TokenBI tokenBI;
	@Autowired
	LlaveBI llaveBI;
	
	// servicio/token.json?idapl=666&ip=10.51.210.17&fecha=24082016101051&uri=http://10.50.109.47:8080/checklist/servicios/loginRest
	@SuppressWarnings({ "static-access", "unused" })
	// servicio/token.json?idapl=666&gWSdsMxa0pt5EFxHoYsVmKlawkbrdFKfGC0hRC6/oRJFqjDc6GEFWDU9+VP9bS6Lqf5w28LIvF2hN43WcKrfFDLU3tzGTOYGlVHGwUn9M7HVsFNtlj+1lRsFA+VTwS45HwoTogiB7RZ3GO42KamI3A==
	@RequestMapping(value = "/token", method = RequestMethod.GET)
	public @ResponseBody String getToken(HttpServletRequest request, HttpServletResponse response)
			throws IOException, KeyException, GeneralSecurityException {

		UtilCryptoGS cifra = new UtilCryptoGS();
		StrCipher cifraIOS = new StrCipher();
		TokenDTO token = new TokenDTO();
		String json = "";
		JsonObject object = new JsonObject();
		UtilDate fec = new UtilDate();
		String fecha = fec.getSysDate("dd/MM/yyyy");
		String fechavalida = fec.getSysDate("ddMMyyyyHHmmss");
		List<LlaveDTO> llave=null;

		
		String llaveApp=(request.getQueryString()).split("&")[0];
		int idLlave=Integer.parseInt(llaveApp.split("=")[1]);
		String uri = (request.getQueryString()).split("&")[1];
		String urides = "";
		
		if(idLlave==7){
			urides = llaveApp+"&"+cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS());
		}else if(idLlave==666){
			urides = llaveApp+"&"+cifra.decryptParams(uri);
		}else if(idLlave==10){
			urides = llaveApp+"&"+cifra.decryptParams(uri,FRQConstantes.LLAVE_ENCRIPCION_LOCAL_DATACENTER);
		}
		
		String[] valores = urides.split("&");
		
		String variipfinal = "", variidaplfinal = "", variurlfinal = "", varifechafinal = "";
		
		for (String valor : valores) {
			if (valor.contains("uri")) {
				variurlfinal = "uri="+valor.split("=")[1];
			}
			if (valor.contains("idapl")) {
				variidaplfinal = "idapl="+valor.split("=")[1];
			}
			if (valor.contains("ip")) {
				variipfinal = "ip="+valor.split("=")[1];
			}
			if (valor.contains("fecha")) {
				varifechafinal = "fecha="+valor.split("=")[1];;
			}
		}
		if (fec.validaFecha(varifechafinal.split("=")[1], fechavalida)) {
			/*
			//System.out.println("Id apli.. " +variidaplfinal.split("=")[1]);
			llave=llaveBI.obtieneLlave(Integer.parseInt(variidaplfinal.split("=")[1]));

			//System.out.println("valor... " + llave.get(0).getLlave());
			if(llave.size()>0)
				token.setLlave(llave.get(0).getLlave());
			else
				token.setLlave(((llaveBI.obtieneLlave(666)).get(0)).toString());
			*/
			
			////System.out.println("Llave.. "+token.getLlave());
			token.setIpOrigen(variipfinal);
			token.setFechaHora(varifechafinal);
			token.setIdAplicacion(variidaplfinal);
			token.setUrlRedirect(variurlfinal);
			token.setBandera(true);
			if(idLlave==10){
				token.setLlave(FRQConstantes.getLlaveEncripcionLocalDatacenter());
			}else {
				token.setLlave(FRQConstantes.getLlaveEncripcionLocal());
			}
			
			String str = cifra.generaToken(token);
			str = str.replace("\n", "");
			str = str.replace(" ", "+");
			token.setToken(str);
			token.setUsuario(null);

			tokenBI.insertaToken(token);

			object.addProperty("token", str);
			object.addProperty("fecha", fecha);
		}else{
			object.addProperty("token", "timeout");
			object.addProperty("fecha", fecha);
			logger.info("Hora del cliente dessincronizada");
			logger.info("Hora del cliente: " + varifechafinal.split("=")[1]);
			logger.info("Hora del del servidor: " + fechavalida);
		}
		json = object.toString();
		response.setContentType("application/json");
		response.getWriter().write(json);
		return null;
	}
}
