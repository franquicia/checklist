package com.gruposalinas.checklist.domain;

import java.util.Date;


public class SearchHistory {
	
    private String id;
    private String searchText;
    private Date date;
    
    
    public SearchHistory(){
    	
    }
    
    public SearchHistory(String searchText, Date date) {
		super();
		
		this.searchText = searchText;
		this.date = date;
	}
	
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    
    public String getSearchText() {
		return searchText;
	}


	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	@Override
    public String toString(){
        return id+"::"+id+"::"+searchText;
    }

}


