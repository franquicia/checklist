var arrayEvidencias;
var indiceActual ;

var arrayFolios;
var arrayDetalleEvidencias;
var arrayDatosVisita;
var arrayAlertas;

var rutaUrlImagen;

function inicio(idTienda,iArray,fecha,idCheck,idModulo,tienda,rutaImagen){
	$('body').css('overflow', 'hidden');
	update (false);
	$("#popup").css( "visibility", "visible" );
	$("#popup").show();
	arrayEvidencias = new Array();
	indiceActual = 0;

	arrayFolios = new Array();
	arrayDetalleEvidencias = new Array();
	arrayDatosVisita = new Array();
	arrayAlertas = new Array();

	rutaUrlImagen;
	i = 0;
		rutaUrlImagen = rutaImagen;
	//PESTAÑAS
		$(".folios").css("width","25px");
		$(".evidencias").css("width","25px");
		$(".fechaHora").css("width","25px");
		$(".alertas").css("width","85%");
		
	   	activeItem = $("#accordion li:last");
	    $(activeItem).addClass('active');
	 
	    $("#accordion li").click(function(){
	        $(activeItem).animate({width: "25px"}, {duration:300, queue:false});
	        $(this).animate({width: "85%"}, {duration:300, queue:false});
	        activeItem = this;
	    });
	    
	    
	    $(".sucursalContainer").text(tienda);
	    var res = getDetalleService(idCheck,idTienda,fecha);
		var jsonC = JSON.stringify(res);
		var objectsLength = Object.keys(res).length;
	    var data = res.respuestas;
	    
	    if (objectsLength == 4){
		    for (var i ; i < data.length ; i++){
		    	arrayEvidencias.push(data[i]);
		    }
		    //
		    var resGen = getDetalleGeneral(idCheck,idTienda,fecha);
		    //alert (JSON.stringify(resGen));
			arrayFolios.push(resGen.folios);
			arrayDetalleEvidencias.push(resGen.evidencias);
			arrayDatosVisita.push(resGen.datosVisita);
			arrayAlertas.push(resGen.alertas);

			inicializaGenerales();
			inicializaCarrusel();
		    //cargaEvidencias(iArray);
		    cargaEvidenciasXModulo(idModulo)
	    }
	    else {
	    	$("#popup").hide();
	    }
	   
}

//Funcion para cargar evidencia en especifico por idModulo
function cargaEvidenciasXModulo(idModulo){
		var i = 0;
		if (idModulo == 0)
    		cargaEvidencias(0);
		else{
			for (i ; i < arrayEvidencias.length ; i++){
		    	if (arrayEvidencias[i].idModulo == (idModulo)){
		    		cargaEvidencias(i);
		    		break;
		    	}
			}
		}
}

//Funcion para cargar evidencias en especifico por idPregunta
function cargaEvidenciasXIdPreg(idPreg){
	var i = 0;
	if (idPreg == 0)
		cargaEvidencias(0);
	else{
		for (i ; i < arrayEvidencias.length ; i++){
	    	if (arrayEvidencias[i].idPregunta == (idPreg)){
	    		cargaEvidencias(i);
	    		break;
	    	}
		}
	}
}

//Funcion para cargar el carrusel de evidencias por posicion en el array
function cargaEvidencias(iArray){
		$("#imagenEvidencia").attr("onclick" , null);
		//Limpiamos los elementos antes de pintar
		$("#tipoFuenteUnoPregunta").text("");
		document.getElementById("circuloBlack_"+(indiceActual+1)).style.background= "black";
		$("#tipoFuenteDosFolio").text("");
        $("#tipoFuenteUnoCuadro").text("");
        $("#tipoFuenteDosAccion").text("");
        $("#tipoFuenteDosFecha").text("");
		$("#tipoFuenteUnoPregunta").text("");

        if(arrayEvidencias[iArray].respuesta == "SI"){
           	document.getElementById("tipoFuenteUnoCuadro").style.color = "green";
            $("#tipoFuenteDosFolio").text("N/A");
            $("#tipoFuenteUnoCuadro").text(arrayEvidencias[iArray].respuesta);
            $("#tipoFuenteDosAccion").text("N/A");
            $("#tipoFuenteDosFecha").text("N/A");
    		$("#tipoFuenteUnoPregunta").text(arrayEvidencias[iArray].pregunta);

            //$("#imagenEvidencia").attr("src","http://10.53.29.78/Sociounico"+arrayEvidencias[iArray].evidencia);
            if (arrayEvidencias[iArray].evidencia != ""){
            	$("#imagenEvidencia").attr("onclick" , "modalImagen('"+rutaUrlImagen+arrayEvidencias[iArray].evidencia+"')");
            	$("#imagenEvidencia").attr("src",rutaUrlImagen+arrayEvidencias[iArray].evidencia);
            }
            else
            	$("#imagenEvidencia").attr("src","../images/reporteOnline/evidencia_no_requerida.png");
        }
        else if (arrayEvidencias[iArray].respuesta == "NO") { 
        	document.getElementById("tipoFuenteUnoCuadro").style.color = "#ad3039";
        	//arrayEvidencias[iArray].folio;
            $("#tipoFuenteDosFolio").text(arrayEvidencias[iArray].folio);
            $("#tipoFuenteUnoCuadro").text(arrayEvidencias[iArray].respuesta);
            $("#tipoFuenteDosAccion").text(arrayEvidencias[iArray].motivo);
            $("#tipoFuenteDosFecha").text(arrayEvidencias[iArray].fechaCompromiso);
    		$("#tipoFuenteUnoPregunta").text(arrayEvidencias[iArray].pregunta);
            //IMG PRODUCCION
            if (arrayEvidencias[iArray].evidencia != ""){
            	$("#imagenEvidencia").attr("onclick" , "modalImagen('"+rutaUrlImagen+arrayEvidencias[iArray].evidencia+"')");
            	$("#imagenEvidencia").attr("src",rutaUrlImagen+arrayEvidencias[iArray].evidencia);
            	}
            else
            	$("#imagenEvidencia").attr("src","../images/reporteOnline/evidencia_no_requerida.png");
            //$("#imagenEvidencia").attr("src","http://10.50.109.39/Sociounico../imagenes/2017/IMG_PRUEBA.JPG");
         }
    	document.getElementById("circuloBlack_"+(iArray+1)).style.background= "green";
    	indiceActual = iArray;
}

function inicializaGenerales(){
	setFolios();
    setEvidencias();
    setAlertas();
    setFechaHora();
}

function setFolios(){
	$("#tablaFoliosBody").empty();
	var i = 0, htm="";
	if (arrayFolios[0].length > 0 ){
		for (i; i <	arrayFolios[0].length ; i++){
			//
			htm += "<tr><td id='fPregunta'>"+arrayFolios[0][i].pregunta+"</td><td id='fPregunta'>"+arrayFolios[0][i].respuesta+"</td><td id='fLink' onclick='cargaEvidenciasXIdPreg("+arrayFolios[0][i].idPregunta+");'>"+arrayFolios[0][i].accion+"</td></tr>";
		}
		$("#tablaFoliosBody").append(htm);
	}
}

function setEvidencias(){
	$("#tablaEvidenciasBody").empty();
	var i = 0, htm="", preview;
	if (arrayDetalleEvidencias[0].length > 0 ){
		for (i; i < arrayDetalleEvidencias[0].length ; i++){
			preview = arrayDetalleEvidencias[0][i].evidencia.replace("imagenes", "preview");
			htm += 	"<tr><td id='ePregunta'>"+arrayDetalleEvidencias[0][i].pregunta+"</td><td id='eLink' onclick='cargaEvidenciasXIdPreg("+arrayDetalleEvidencias[0][i].idPregunta+");'><img src='"+rutaUrlImagen+preview+"' alt='img' height='80' width='80'></td></tr>";
		}
		$("#tablaEvidenciasBody").append(htm);
	}
}

function setFechaHora(){
	/*HACER EL SPLIT PARA QUE NO TRUENE*/
	
	var fi ,ff, hi, hf;
	var obj = arrayDatosVisita[0][0];

	//FECHA INICIO Y HORA INICIO SIEMPRE TIENE	
    $("#fInicio").text(obj.fechaInicio.split(" ")[0]);
    $("#hInicio").text(obj.fechaInicio.split(" ")[1]);

    //FECHA FIN, HORA FIN NO SIEMPRE EXISTEN
    if (obj.fechaFin != null){
    	$("#fFin").text(obj.fechaFin.split(" ")[0]);
    	$("#hFin").text(obj.fechaFin.split(" ")[1]);
    }
    
    //FECHA ENVIO NO SIEMPRE EXISTE
    if (obj.fechaEnvio != null)
    	$("#fEnvio").text(obj.fechaEnvio);
    
    //SIEMPRE TIENE UN MODO
    $("#hModo").text(obj.modo);

}

function setAlertas(){
	$("#tablaAlertasBody").empty();
	var i = 0, htm="";
	if (arrayAlertas[0].length > 0 ){
		for (i; i < arrayAlertas[0].length ; i++){
			htm += 	"<tr>"
						+"<td class='tdExterno'>"
						+"<table class='tbInterna'>"
							+"<thead><tr id='motivo'><th><strong>"+(i+1)+".- "+arrayAlertas[0][i].respuesta+"</strong></th></tr></thead>"
							+"<tbody>"
							+"<tr id='fAlerta'><td class='aInterno'><strong>Fecha:</strong> "+arrayAlertas[0][i].fecha+"</td></tr>"
							+"<tr id='hAlerta'><td class='aInterno'><strong>Hora del reporte:</strong> "+arrayAlertas[0][i].hora+"</td></tr>"
							+"<tr id='folioAlerta' onclick='cargaEvidenciasXIdPreg("+arrayAlertas[0][i].idPregunta+");'><td class='aInterno'><strong>Ver Detalle</strong></td></tr>"
							+"</tbody>"
						+"</table>"
					+"</td>"
				+"</tr>";
	
		}
		$("#tablaAlertasBody").append(htm);
	}
	else {
		htm += 	"<tr>"
			+"<td class='tdExterno'>"
			+"<table class='tbInterna'>"
				+"<thead><tr id='motivo'><th><strong>CHECKLIST SIN ALERTAS</strong></th></tr></thead>"
				+"<tbody>"
				+"<tr id='fAlerta'><td class='aInterno'><strong></strong> </td></tr>"
				+"<tr id='hAlerta'><td class='aInterno'><strong></strong> </td></tr>"
				+"</tbody>"
			+"</table>"
		+"</td>"
	+"</tr>";
		$("#tablaAlertasBody").append(htm);
	}
}

function inicializaCarrusel(){
	document.getElementById("circulos").innerHTML = "";
	var htm = "";
	for (var i = 1; i <= arrayEvidencias.length; i++) {
		htm += "<span class='circulo_Black' id='circuloBlack_"+i+"' onclick='cargaEvidencias("+(i-1)+")'>"+i+"</span>";
	}
	document.getElementById("circulos").innerHTML = htm;
}

//CONSUMO DEL SERVICIO PARA OBTENER DETALLE
function getDetalleService(idCheck,idCeco,fecha){
	$.ajax({
		type : "GET",
		url : '../consultaChecklistService/getDetallesRespuestasReporte.json?idChecklist='+idCheck+'&idCeco='+idCeco+'&fecha='+fecha,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			//Ocupo esta funcion para poder retornar el valor del servicio
			jsonG = json;
			var jsonC = JSON.stringify(json);
		},
	    error: function(jqXHR, textStatus, errorThrown){
	    	alert ("Algo paso");
	    },
		async: false,
		
	})
	return jsonG;
}

//CONSUMO DEL SERVICIO PARA OBTENER GENERALES
function getDetalleGeneral(idCheck,idCeco,fecha){
	$.ajax({
		type : "GET",
		url : '../consultaChecklistService/getResumenReporte.json?idChecklist='+idCheck+'&idCeco='+idCeco+'&fecha='+fecha,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			//Ocupo esta funcion para poder retornar el valor del servicio
			jsonG = json;
			var jsonC = JSON.stringify(json);
		},
	    error: function(jqXHR, textStatus, errorThrown){
	    	alert ("Algo paso");
	    },
		async: false,
		
	})
	return jsonG;
}

//FUNCIONES VISUALIZADOR
function anteriorDetalle(){	
	if (indiceActual-1 > -1)
		cargaEvidencias(indiceActual-1);
	else
		cargaEvidencias(arrayEvidencias.length-1);
	
}
function siguienteDetalle(){	

	if (indiceActual < arrayEvidencias.length-1)
		cargaEvidencias(indiceActual+1)
	else
		cargaEvidencias(0);
}

/*MODAL PARA LA IMAGEN*/
function modalImagen(url){
	document.getElementById("idImagenEmergente").src = url;
	$('#ventanaEmergente').css("visibility","visible");
	$('#ventanaEmergente').show();
}

/***************Funciones para salir de la ventana emergente******************/
function amIclicked(e, element){
    e = e || event;
    var target = e.target || e.srcElement;
    if(target.id==element.id)
        return true;
    else
        return false;
}

function oneClickFoto(event, element){
    if(amIclicked(event, element)){
		$('#ventanaEmergente').hide();
    }
}
/***************Funciones para salir de la ventana emergente******************/


