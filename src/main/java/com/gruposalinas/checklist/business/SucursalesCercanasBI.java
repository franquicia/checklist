package com.gruposalinas.checklist.business;

import java.util.Iterator;
import java.util.List;

import javax.enterprise.inject.New;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonArray;
import com.gruposalinas.checklist.dao.SucursalesCercanasDAO;
import com.gruposalinas.checklist.domain.NegociosDentroDTO;
import com.gruposalinas.checklist.domain.PuntosCercanosDTO;
import com.gruposalinas.checklist.domain.SucursalesCercanasDTO;
import com.gruposalinas.checklist.util.UtilFRQ;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

public class SucursalesCercanasBI {
	
	private Logger logger = LogManager.getLogger(SucursalesCercanasBI.class);
	
	@Autowired
	SucursalesCercanasDAO sucursalesCercanasDAO;
	
	
	public List<SucursalesCercanasDTO> obtieneSucursales(String latitud, String longitud){
		
		List<SucursalesCercanasDTO> sucursales = null;
		
		
		try {
			sucursales = sucursalesCercanasDAO.obtieneSucursales(latitud, longitud);
			
		} catch (Exception e) {
			
			logger.info("No fue posible obtener las sucursales cercanas");
				
			return null;
			
		}
		
		return sucursales;
	}
	
	
	public String obtienePuntosCercanos(String latitud, String longitud){
		
		List<PuntosCercanosDTO> puntosCercanos = null;
		
		
		JSONArray arrayPuntosCercanos = new JSONArray();
		
		JSONObject jsonSucursal = null;
		
		JSONArray jsonNegociosArray = null;
		JSONObject jsonNegocioObject =  null;
	
		JSONObject objectoRespuestaJsonObject = new JSONObject();
			
		
		try {
			
			puntosCercanos = sucursalesCercanasDAO.obtieneSucursalesCercanas(latitud, longitud);
			
			if(puntosCercanos != null && !puntosCercanos.isEmpty()) {
				
				for (PuntosCercanosDTO puntocercanoDto : puntosCercanos) {
					
					jsonSucursal = new JSONObject();
					
					jsonSucursal.put("CENTRO_COSTO",puntocercanoDto.getCeco());
					jsonSucursal.put("NOMBRE", puntocercanoDto.getNombreCeco());
					jsonSucursal.put("NUMERO_ECONOMICO", puntocercanoDto.getNuSucursal());
					jsonSucursal.put("LONGITUD", puntocercanoDto.getLongitud());
					jsonSucursal.put("LATITUD", puntocercanoDto.getLatitud());
					jsonSucursal.put("DISTANCIA", puntocercanoDto.getDistancia());
					
					
					List<NegociosDentroDTO> negociosDentroSucursal = sucursalesCercanasDAO.obtieneNegociosDentro(puntocercanoDto.getNuSucursal());
					
					if(negociosDentroSucursal != null && !negociosDentroSucursal.isEmpty()) {
						
						jsonNegociosArray = new JSONArray();
						
						for (NegociosDentroDTO negocioDentro : negociosDentroSucursal) {
							
							jsonNegocioObject = new JSONObject();
							
							jsonNegocioObject.put("CENTRO_COSTO", negocioDentro.getCeco());
							jsonNegocioObject.put("NOMBRE", negocioDentro.getNombreCeco());
							jsonNegocioObject.put("CANAL", negocioDentro.getCanal());
							jsonNegocioObject.put("NEGOCIO", negocioDentro.getNegocio());
							
							jsonNegociosArray.put(jsonNegocioObject);
							
						}
						
						jsonSucursal.put("CECOS_INTERNOS", jsonNegociosArray);
						
						
					}else {
						
						jsonSucursal.put("CECOS_INTERNOS", new JSONArray());
						
					}
					
					arrayPuntosCercanos.put(jsonSucursal);
					
				}
			}
			
			objectoRespuestaJsonObject.put("data", arrayPuntosCercanos);
			objectoRespuestaJsonObject.put("estatus", "SUCCESS");
			
			
		} catch (Exception e) {
			
			
			return "{'data' : [],'estatus' : 'FAILURE'}";
							
			
		}
		
		
		
		
		return objectoRespuestaJsonObject.toString();
	}

}
