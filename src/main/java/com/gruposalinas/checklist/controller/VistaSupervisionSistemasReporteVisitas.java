package com.gruposalinas.checklist.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.ReporteSupervisionSistemasBI;
import com.gruposalinas.checklist.domain.ReporteSupervisionSistemasDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;

@Controller
public class VistaSupervisionSistemasReporteVisitas {
	private static Logger logger = LogManager.getLogger(VistaSupervisionSistemasReporteVisitas.class);
	
	@Autowired
	ReporteSupervisionSistemasBI reporteSupervisionBI;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"central/VistaSupervisionSistemasReporteVisitas.htm","central/VistaSupervisionSistemasReporteVisitasMovil.htm"}, method = RequestMethod.GET)
	public ModelAndView vistaCumplimientoVisitas(HttpServletRequest request, HttpServletResponse response) throws Exception{
		logger.info("...ENTRO AL CONTROLLER VistaSupervisionSistemasReporteVisitas...");
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("VistaSupervisionSistemasReporteVisitas.htm");
		}else{
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
			if(idUsuario>0){
				mv = new ModelAndView("vistaSupervisionSistemas");
				mv.addObject("idUsuario", idUsuario);
				mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
				Map<String, Object> map = new HashMap<String, Object>();
				try {
					map = reporteSupervisionBI.reporteSistemas(230034);
					logger.info("Map... " + map);
					logger.info("Tamaño map de totales y reportes por direccion: "+map.size());
					if (map != null) {
						List<ReporteSupervisionSistemasDTO> listareporteTotales = new ArrayList<ReporteSupervisionSistemasDTO>();
						listareporteTotales = (List<ReporteSupervisionSistemasDTO>) map.get("listaReporteTotales");
						logger.info("Size listareporteTotales:"+listareporteTotales.size());
						mv.addObject("listareporteTotales", listareporteTotales);
						List<ReporteSupervisionSistemasDTO> listareporteSistemas = new ArrayList<ReporteSupervisionSistemasDTO>();
						listareporteSistemas = (List<ReporteSupervisionSistemasDTO>) map.get("listaReporteSistemas");
						//listareporteSistemas = new ArrayList<ReporteSupervisionSistemasDTO>();
						logger.info("Size listareporteSistemas:"+listareporteSistemas.size());
						mv.addObject("listareporteSistemas", listareporteSistemas);
					}
				} catch (Exception e) {
					logger.info("Ocurrio algo... "+e);
				}
			}else{
				logger.info("...SALE DEL CONTROLLER VistaSupervisionSistemasReporteVisitas...");
				mv = new ModelAndView("http404");
			}
		}
		logger.info("...SALE DEL CONTROLLER VistaSupervisionSistemasReporteVisitas...");
		return mv;
	}
	
	@RequestMapping(value = {"central/VistaFiltroSupervisionSistemas.htm","central/VistaFiltroSupervisionSistemasMovil.htm"}, method = RequestMethod.GET)
	public ModelAndView vistaFiltroSupervisionSistemas(HttpServletRequest request, HttpServletResponse response,@RequestParam(value = "idCeco", required = true, defaultValue = "230034") String area,
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "General") String nombreCeco)throws Exception{
		logger.info("...ENTRO AL CONTROLLER vistaFiltroSupervisionSistemas...");
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("VistaSupervisionSistemasReporteVisitas.htm");
		}else{
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
		
			if(idUsuario>0){
				logger.info("EL ID CECO DE ESTA PETICION ES "+ area);
				mv = new ModelAndView("vistaFiltroSupervisionSistemas");
				mv.addObject("area", area);
				mv.addObject("nombreCeco", nombreCeco);
				mv.addObject("idUsuario", idUsuario);
				mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
				//FILTRO SUCURSALES
				List<ReporteSupervisionSistemasDTO> listaFiltros = new ArrayList<ReporteSupervisionSistemasDTO>();
				listaFiltros = reporteSupervisionBI.filtroCecos(0);
				mv.addObject("listaFiltros", listaFiltros);
				//REPORTES CICAPS
				List<ReporteSupervisionSistemasDTO> listaReportesSicap = new ArrayList<ReporteSupervisionSistemasDTO>();
				listaReportesSicap = reporteSupervisionBI.reporteSisCap("", "", "", "",area);
				logger.info("El tamaño de la lista de reportes es de:"+listaReportesSicap.size());
				mv.addObject("listaReportesSicap", listaReportesSicap);
				//FILTRO SUCURSALES
				List<ReporteSupervisionSistemasDTO> listaFiltrosSucursales = new ArrayList<ReporteSupervisionSistemasDTO>();
				listaFiltrosSucursales = reporteSupervisionBI.filtroSucursal("");
				mv.addObject("listaFiltrosSucursales", listaFiltrosSucursales);
			}else{
				logger.info("...SALE DEL CONTROLLER vistaFiltroSupervisionSistemas...");
				mv = new ModelAndView("http404");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaFiltroSupervisionSistemas...");
		return mv;
	}
	
	@RequestMapping(value = "/ajaxReportesSisCap", method = RequestMethod.GET)
	public @ResponseBody List<ReporteSupervisionSistemasDTO> ajaxFiltroSucursal(
			@RequestParam(value = "fInicio", required = false, defaultValue = "") String fInicio,
			@RequestParam(value = "fFin", required = false, defaultValue = "") String fFin,
			@RequestParam(value = "checklist", required = true, defaultValue = "0") String checklist,
			@RequestParam(value = "sucursal", required = true, defaultValue = "") String sucursal,
			@RequestParam(value = "area", required = true, defaultValue = "000000") String area, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxFiltroSucursal...");
		
		logger.info("fInicio... "+fInicio);
		logger.info("fFin... "+fFin);
		logger.info("checklist... "+checklist);
		
		if(sucursal.length() > 4){
			sucursal = sucursal.substring(0, 4);
			logger.info("valor recortado... "+sucursal);
		}
		
		logger.info("sucursal... "+sucursal);
		logger.info("area... "+area);
		
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());
		logger.info("USUARIO "+ idUsuario);
		
		List<ReporteSupervisionSistemasDTO> listRetorno= new ArrayList<ReporteSupervisionSistemasDTO>();
		listRetorno = reporteSupervisionBI.reporteSisCap(fInicio, fFin, checklist, sucursal, area);
		
		logger.info("listRetorno.size() "+listRetorno.size());
		logger.info("listRetorno "+listRetorno);

		logger.info("...SALE DEL AJAX ajaxFiltroSucursal...");
		return listRetorno;
	}
	
	//Busqueda de sucursales. Eliminar la busqueda de sucursales de la anterior
	@RequestMapping(value = "/ajaxFiltroAutocompletaSucursal", method = RequestMethod.GET)
	public @ResponseBody String[] ajaxFiltroAutocompletaSucursal(
			@RequestParam(value = "term", required = true, defaultValue = "") String term, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxFiltroAutocompletaSucursal...");
		
		logger.info("term... "+term);
		
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());
		logger.info("USUARIO "+ idUsuario);
		
		
		List<ReporteSupervisionSistemasDTO> listaFiltrosSucursales = new ArrayList<ReporteSupervisionSistemasDTO>();
		listaFiltrosSucursales = reporteSupervisionBI.filtroSucursal(term);
		logger.info("listRetorno.size() "+listaFiltrosSucursales.size());
		logger.info("listRetorno "+listaFiltrosSucursales);
		
		//Vaciarlo en otra clase que tenga la siguiente forma
//		clase con un atributo que se llame label que sea de tipo String[] y dentro de ella debe
		//guardar todo lo que retorne el 
	
		
		String[] sucursales = new String[listaFiltrosSucursales.size()];
		for(int i =0; i<listaFiltrosSucursales.size(); i++){
			sucursales[i] = listaFiltrosSucursales.get(i).getSucursal();
			logger.info("Nombre Sucursal:"+sucursales[i]);
		}
	
		logger.info("sucursales.length "+sucursales.length);
		logger.info("sucursales "+sucursales);
		

		logger.info("...SALE DEL AJAX ajaxFiltroAutocompletaSucursal...");
		return sucursales;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/ajaxReportesEvidencia", method = RequestMethod.GET)
	public @ResponseBody List<List> ajaxEvidencia(
			@RequestParam(value = "idBitacora", required = true, defaultValue = "0") int idBitacora, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxEvidencia...");
		
		logger.info("fInicio... "+idBitacora);
		
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());
		logger.info("USUARIO "+ idUsuario);
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		List<ReporteSupervisionSistemasDTO> listaPreguntas = new ArrayList<ReporteSupervisionSistemasDTO>();
		List<ReporteSupervisionSistemasDTO> listaEvaluacionFechas = new ArrayList<ReporteSupervisionSistemasDTO>();
		try{
			map = reporteSupervisionBI.obtenerRespuestas(idBitacora);
			logger.info("Tamaño map de respuestas y evidencia: "+map.size());
			if (map != null) {
				listaPreguntas = (List<ReporteSupervisionSistemasDTO>) map.get("listaRespuestas");
				logger.info("listaPreguntas.size() "+listaPreguntas.size());
				logger.info("listaPreguntas "+listaPreguntas);
				listaEvaluacionFechas = (List<ReporteSupervisionSistemasDTO>) map.get("listaEvaluacionFechas");
				logger.info("mapa datos "+map.keySet());
				logger.info("listaEvaluacionFechas.size() "+listaEvaluacionFechas.size());
				logger.info("listaEvaluacionFechas "+listaEvaluacionFechas);
			}
		} catch (Exception e) {
			logger.info("Ocurrio algo... "+e);
			e.printStackTrace();
		}
		
		List<ReporteSupervisionSistemasDTO> listaEvidencias= new ArrayList<ReporteSupervisionSistemasDTO>();
		listaEvidencias = reporteSupervisionBI.obtenerEvidencia(idBitacora);
		logger.info("listaEvidencias.size() "+listaEvidencias.size());
		logger.info("listaEvidencias "+listaEvidencias);
		
		List<List> listaRetorno= new  ArrayList<List>();
		listaRetorno.add(listaEvidencias);
		listaRetorno.add(listaPreguntas);
		listaRetorno.add(listaEvaluacionFechas);
		logger.info("listaRetorno.size() "+listaRetorno.size());
		logger.info("listaRetorno "+listaRetorno);
		
		logger.info("...SALE DEL AJAX ajaxEvidencia...");
		return listaRetorno;
	}
	
}