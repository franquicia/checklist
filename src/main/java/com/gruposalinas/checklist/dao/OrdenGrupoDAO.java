package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.OrdenGrupoDTO;


public interface OrdenGrupoDAO {
	
	public List<OrdenGrupoDTO> obtieneOrdenGrupo(String idOrdenGrupo, String idChecklist, String idGrupo) throws Exception;
	
	public int insertaOrdenGrupo(OrdenGrupoDTO bean) throws Exception;
	
	public boolean actualizaOrdenGrupo(OrdenGrupoDTO bean) throws Exception;
	
	public boolean eliminaOrdenGrupo(int idOrdenGrupo) throws Exception;

}
