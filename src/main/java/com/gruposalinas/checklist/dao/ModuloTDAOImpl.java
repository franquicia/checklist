package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.mappers.ModuloTRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ModuloTDAOImpl extends DefaultDAO implements ModuloTDAO {

    private static Logger logger = LogManager.getLogger(ModuloTDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodosTemp;
    DefaultJdbcCall jdbcInsertaModuloTemp;
    DefaultJdbcCall jdbcActualizaModuloTemp;
    DefaultJdbcCall jdbcEliminaModuloTemp;

    public void init() {

        jdbcObtieneTodosTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMODULOTEMP")
                .withProcedureName("SP_SEL_MODULO_TEMP")
                .returningResultSet("RCL_MODULO", new ModuloTRowMapper());

        jdbcInsertaModuloTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMODULOTEMP")
                .withProcedureName("SP_INS_MODULO_TEMP");

        jdbcActualizaModuloTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMODULOTEMP")
                .withProcedureName("SP_ACT_MODULO_TEMP");

        jdbcEliminaModuloTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMODULOTEMP")
                .withProcedureName("SP_DEL_MODULO_TEMP");

    }

    @SuppressWarnings("unchecked")
    public List<ModuloDTO> obtieneModuloTemp(String idModulo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ModuloDTO> listaModulo = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_MODULO", idModulo);

        out = jdbcObtieneTodosTemp.execute(in);

        logger.info("Función ejecutada: checklist.PAADMMODULOTEMP.SP_SEL_MODULO_TEMP");

        listaModulo = (List<ModuloDTO>) out.get("RCL_MODULO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Modulos Temporales");
        } else {
            return listaModulo;
        }

        return listaModulo;

    }

    public int insertaModuloTemp(ModuloDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idModulo = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_MOD_PADRE", bean.getIdModuloPadre())
                .addValue("PA_NUM_REV", bean.getNumVersion())
                .addValue("PA_TIPO_MOD", bean.getTipoCambio())
                .addValue("PA_IDMOD", bean.getIdModulo())
                .addValue("PA_COMMIT", 1);

        out = jdbcInsertaModuloTemp.execute(in);

        logger.info("Funci�n ejecutada: checklist.PAADMMODULOTEMP.SP_INS_MODULO_TEMP");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDMOD");
        idModulo = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Modulo en la tabla temporal");
        } else {
            return idModulo;
        }

        return 0;
    }

    public boolean actualizaModuloTemp(ModuloDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDMODULO", bean.getIdModulo())
                .addValue("PA_MOD_PADRE", bean.getIdModuloPadre())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_NUM_REV", bean.getNumVersion())
                .addValue("PA_TIPO_MOD", bean.getTipoCambio());

        out = jdbcActualizaModuloTemp.execute(in);

        logger.info("Función ejecutada: checklist.PAADMMODULOTEMP, SP_ACT_MODULO_TEMP");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Modulo  id( " + bean.getIdModulo() + ") en la tabla temporal");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaModuloTemp(int idModulo) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDMODULO", idModulo);

        out = jdbcEliminaModuloTemp.execute(in);

        logger.info("Función ejecutada: checklist.PAADMMODULOTEMP.SP_DEL_MODULO_TEMP");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el Modulo id(" + idModulo + ") en la tabla temporal");
        } else {
            return true;
        }

        return false;
    }

}
