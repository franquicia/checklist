package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ConsultaCecoProtocoloDTO;

public class ConsultaCecoProtocoloRowMapper implements RowMapper<ConsultaCecoProtocoloDTO> {

	public ConsultaCecoProtocoloDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConsultaCecoProtocoloDTO consultaDTO = new ConsultaCecoProtocoloDTO();
		
		consultaDTO.setIdCeco(rs.getInt("FCID_CECO"));
		consultaDTO.setNomCeco(rs.getString("FCNOMBRE"));
		
		return consultaDTO;
		
		
	}

}
