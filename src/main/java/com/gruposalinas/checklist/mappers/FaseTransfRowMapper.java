package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.FaseTransfDTO;

public class FaseTransfRowMapper implements RowMapper<FaseTransfDTO> {

	public FaseTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		FaseTransfDTO FaseTransfDTO = new FaseTransfDTO();
		
		FaseTransfDTO.setIdTab(rs.getInt("FIID_PROYFASE"));
		FaseTransfDTO.setProyecto(rs.getInt("FIID_PROYECTO"));
		FaseTransfDTO.setFase(rs.getInt("FIID_FASE"));
		FaseTransfDTO.setVersion(rs.getInt("FIVERSION"));
		//FaseTransfDTO.setAux(rs.getString("FCAUX"));
	    FaseTransfDTO.setObs(rs.getString("FCOBSERVACION"));
	    FaseTransfDTO.setPeriodo(rs.getString("FCPERIODO"));
	    
	 
	return FaseTransfDTO;
	}

}


