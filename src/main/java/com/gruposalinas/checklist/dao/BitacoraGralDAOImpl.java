package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.BitacoraGralDTO;
import com.gruposalinas.checklist.mappers.BitacoraGralHijasRowMapper;
import com.gruposalinas.checklist.mappers.BitacoraGralRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class BitacoraGralDAOImpl extends DefaultDAO implements BitacoraGralDAO {

    private static Logger logger = LogManager.getLogger(BitacoraGralDAOImpl.class);

    DefaultJdbcCall jdbcInsertaBitaG;
    DefaultJdbcCall jdbcEliminaBitaG;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneBitaG;
    DefaultJdbcCall jbdcActualizaBitaG;
    //HIJAS
    DefaultJdbcCall jdbcInsertaBitaGH;
    DefaultJdbcCall jdbcEliminaBitaGH;
    DefaultJdbcCall jdbcObtieneTodoH;
    DefaultJdbcCall jdbcObtieneBitaGH;
    DefaultJdbcCall jbdcActualizaBitaGH;

    //inserta padre e hijos
    DefaultJdbcCall jbdcGenerapadrehijas;

    public void init() {

        jdbcInsertaBitaG = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMBITAGRAL")
                .withProcedureName("SP_INS_BITACORAG");

        jdbcEliminaBitaG = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMBITAGRAL")
                .withProcedureName("SP_DEL_BITACORAG");

        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMBITAGRAL")
                .withProcedureName("SP_SEL_BITG")
                .returningResultSet("RCL_BITACORA", new BitacoraGralRowMapper());

        jdbcObtieneBitaG = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMBITAGRAL")
                .withProcedureName("SP_SEL_BITACORAG")
                .returningResultSet("RCL_BITACORA", new BitacoraGralRowMapper());

        jbdcActualizaBitaGH = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMBITAGRAL")
                .withProcedureName("SP_ACT_BITACORAG");
        //hijas

        jdbcInsertaBitaGH = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMBITAGRAL")
                .withProcedureName("SP_INS_BITACORAHI");

        jdbcEliminaBitaGH = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMBITAGRAL")
                .withProcedureName("SP_DEL_BITACORAHI");

        jdbcObtieneTodoH = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMBITAGRAL")
                .withProcedureName("SP_SEL_BITHI")
                .returningResultSet("RCL_BITACORA", new BitacoraGralHijasRowMapper());

        jdbcObtieneBitaGH = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMBITAGRAL")
                .withProcedureName("SP_SEL_BITACORAHI")
                .returningResultSet("RCL_BITACORA", new BitacoraGralHijasRowMapper());

        jbdcActualizaBitaGH = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMBITAGRAL")
                .withProcedureName("SP_ACT_BITACORAGHI");

        //inserta papa e hijas en las bitacoras
        jbdcGenerapadrehijas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMBITPADHIJ")
                .withProcedureName("SP_INSBITAC");

    }

    public int inserta(BitacoraGralDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_STATUS", bean.getStatus());

        out = jdbcInsertaBitaG.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMBITAGRAL.SP_INS_BITACORAG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDBITAGRAL");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el Empleado");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean elimina(String idBitaGral) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDBITAGRAL", idBitaGral);

        out = jdbcEliminaBitaG.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMBITAGRAL.SP_DEL_BITACORAG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + idBitaGral + ")");
        } else {
            return true;
        }

        return false;

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<BitacoraGralDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<BitacoraGralDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneTodo.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMBITAGRAL.SP_SEL_BITG}");

        listaFijo = (List<BitacoraGralDTO>) out.get("RCL_BITACORA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la Bitacora Gral");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //parametro

    @SuppressWarnings("unchecked")
    public List<BitacoraGralDTO> obtieneDatos(int idBitaGral) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<BitacoraGralDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDBITACORA", idBitaGral);

        out = jdbcObtieneBitaG.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMBITAGRAL.SP_SEL_BITACORAG}");
        //lleva el nombre del cursor del procedure
        lista = (List<BitacoraGralDTO>) out.get("RCL_BITACORA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el empleado(" + idBitaGral + ")");
        }
        return lista;

    }

    public boolean actualiza(BitacoraGralDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDBITAGRAL", bean.getIdBitaGral())
                .addValue("PA_FFIN", bean.getFin())
                .addValue("PA_STATUS", bean.getStatus());

        out = jbdcActualizaBitaG.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMBITAGRAL.SP_ACT_BITACORAG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdBitaGral() + ")");
        } else {
            return true;
        }
        return false;

    }

//////////////////////////////////////////////////////////hijas///////////////////////////////////////////////////
    public int insertaH(BitacoraGralDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDBITAGRAL", bean.getIdBitaGral())
                .addValue("PA_IDBITACORA", bean.getIdBita())
                .addValue("PA_STATUS", bean.getStatus());

        out = jdbcInsertaBitaGH.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMBITAGRAL.SP_INS_BITACORAHI}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el Empleado");
        } else {
            return error;
        }

        return error;
    }

    public boolean eliminaH(String idBitaGral) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDBITAGRAL", idBitaGral);

        out = jdbcEliminaBitaGH.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMBITAGRAL.SP_SEL_BITACORAHI}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + idBitaGral + ")");
        } else {
            return true;
        }

        return false;

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<BitacoraGralDTO> obtieneInfoH() throws Exception {
        Map<String, Object> out = null;
        List<BitacoraGralDTO> lista = null;
        int error = 0;

        out = jdbcObtieneTodoH.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMBITAGRAL.SP_SEL_BITHI}");

        lista = (List<BitacoraGralDTO>) out.get("RCL_BITACORA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la Bitacora Hija");
        } else {
            return lista;
        }

        return lista;

    }
    //parametro

    @SuppressWarnings("unchecked")
    public List<BitacoraGralDTO> obtieneDatosH(int idBitaGral) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<BitacoraGralDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDBITACORA", idBitaGral);

        out = jdbcObtieneBitaGH.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMBITAGRAL.SP_SEL_BITACORAHI}");
        //lleva el nombre del cursor del procedure
        lista = (List<BitacoraGralDTO>) out.get("RCL_BITACORA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el la bitacora gral(" + idBitaGral + ")");
        }
        return lista;

    }

    public boolean actualizaH(BitacoraGralDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDBITAGRAL", bean.getIdBitaGral())
                .addValue("PA_IDBITACORA", bean.getIdBita())
                .addValue("PA_FFIN", bean.getFin())
                .addValue("PA_STATUS", bean.getStatus());

        out = jbdcActualizaBitaGH.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMBITAGRAL.SP_ACT_BITACORAGHI}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdBitaGral() + ")");
        } else {
            return true;
        }
        return false;

    }

    @Override
    public int ejecutaSP(String bitacora) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDBITACORA", bitacora);

        out = jbdcGenerapadrehijas.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PADMBITPADHIJ.SP_INSBITAC}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDBITAGRAL");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al Ejecutar SP ");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

}
