package com.gruposalinas.checklist.domain;

public class ConsultaCecoProtocoloDTO {
	
	private int idGrupo;
	private int idChecklist;
	private String nomChecklist;
	private int idUsuario;
	private int idBitacora;
	private int idCeco;
	private String nomCeco;
	private String fechaInicio;
	private String fechaTermino;
	

	public int getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(int idGrupo) {
		this.idGrupo = idGrupo;
	}
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public String getNomChecklist() {
		return nomChecklist;
	}
	public void setNomChecklist(String nomChecklist) {
		this.nomChecklist = nomChecklist;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public int getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(int idCeco) {
		this.idCeco = idCeco;
	}
	public String getNomCeco() {
		return nomCeco;
	}
	public void setNomCeco(String nomCeco) {
		this.nomCeco = nomCeco;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaTermino() {
		return fechaTermino;
	}
	public void setFechaTermino(String fechaTermino) {
		this.fechaTermino = fechaTermino;
	}
	
	@Override
	public String toString() {
		return "ConsultaCecoProtocoloDTO [idGrupo=" + idGrupo + ", idChecklist=" + idChecklist + ", nomChecklist=" + nomChecklist
				+ " idUsuario=" + idUsuario + " idBitacora=" + idBitacora + " idCeco=" + idCeco + " nomCeco=" + nomCeco 
				+ " fechaInicio=" + fechaInicio + " fechaTermino=" + fechaTermino + "]";
	}
	
}


