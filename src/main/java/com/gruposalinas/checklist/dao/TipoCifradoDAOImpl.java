package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.TipoCifradoDTO;
import com.gruposalinas.checklist.mappers.TipoCifradoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class TipoCifradoDAOImpl extends DefaultDAO implements TipoCifradoDAO {

    private static Logger logger = LogManager.getLogger(TipoCifradoDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaTipoCifrado;
    private DefaultJdbcCall jdbcActualizaTipoCifrado;
    private DefaultJdbcCall jdbcEliminaTipoCifrado;
    private DefaultJdbcCall jdbcBuscaTipoCifrado;

    public void init() {

        jdbcInsertaTipoCifrado = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMTIPOCIFRADO")
                .withProcedureName("SP_INS_TIPOCIFRADO");

        jdbcActualizaTipoCifrado = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMTIPOCIFRADO")
                .withProcedureName("SP_ACT_TIPOCIFRADO");

        jdbcEliminaTipoCifrado = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMTIPOCIFRADO")
                .withProcedureName("SP_DEL_TIPOCIFRADO");

        jdbcBuscaTipoCifrado = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMTIPOCIFRADO")
                .withProcedureName("SP_SEL_TIPOCIFRADO")
                .returningResultSet("RCL_TCIFRADO", new TipoCifradoRowMapper());

    }

    public boolean insertaTipoCifrado(TipoCifradoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        //boolean idToken = false;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_TIPOSERVICIO", bean.getTipoServicio())
                .addValue("PA_JSON", bean.getJson())
                .addValue("PA_FECHA", bean.getFecha())
                .addValue("PA_TIPOAPP", bean.getTipoApp())
                .addValue("PA_IDUSUARIO", bean.getNumEmpleado());

        out = jdbcInsertaTipoCifrado.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMTIPOCIFRADO.SP_INS_TIPOCIFRADO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar el TipoCifrado");
        } else {
            return true;
        }

        return false;
    }

    public boolean actualizaTipoCifrado(TipoCifradoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_TIPOSERVICIO", bean.getTipoServicio())
                .addValue("PA_IDTIPOCIFRADO", bean.getIdTipoCifrado())
                .addValue("PA_JSON", bean.getJson())
                .addValue("PA_FECHA", bean.getFecha())
                .addValue("PA_TIPOAPP", bean.getTipoApp())
                .addValue("PA_IDUSUARIO", bean.getNumEmpleado());

        out = jdbcActualizaTipoCifrado.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMTIPOCIFRADO.SP_ACT_TIPOCIFRADO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar el TipoCifrado");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaTipoCifrado(int bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDTIPOCIFRADO", bean);

        out = jdbcEliminaTipoCifrado.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMTIPOCIFRADO.SP_DEL_TIPOCIFRADO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al elimnar el TipoCifrado ");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<TipoCifradoDTO> buscaTipoCifrado(TipoCifradoDTO bean) throws Exception {
        Map<String, Object> out = null;
        List<TipoCifradoDTO> listaTipoCifradoDTO = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_TIPOSERVICIO", bean.getTipoServicio())
                .addValue("PA_IDTIPOCIFRADO", bean.getIdTipoCifrado())
                .addValue("PA_FECHA", bean.getFecha())
                .addValue("PA_TIPOAPP", bean.getTipoApp())
                .addValue("PA_IDUSUARIO", bean.getNumEmpleado());

        out = jdbcBuscaTipoCifrado.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_TIPOCIFRADO.SP_SEL_TIPOCIFRADO}");

        listaTipoCifradoDTO = (List<TipoCifradoDTO>) out.get("RCL_TCIFRADO");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al consultar los TipoCifrado ");
        } else {
            return listaTipoCifradoDTO;
        }

        return null;
    }

}
