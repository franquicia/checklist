package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.AsignacionesSupDTO;

public class AsignacionesSupRowMapper implements RowMapper<AsignacionesSupDTO> {

	public AsignacionesSupDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AsignacionesSupDTO asignaDTO = new AsignacionesSupDTO();
		
		asignaDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
		asignaDTO.setIdPerfil(rs.getInt("FIPERFIL_ID"));
		asignaDTO.setIdCeco(rs.getString("FCID_CECO"));
		asignaDTO.setIdGerente(rs.getInt("FIGERENTEID"));
		asignaDTO.setActivo(rs.getInt("FIACTIVO"));
		asignaDTO.setFechaIniAsigna(rs.getString("FDINICIO_ASIGN"));
		asignaDTO.setFechaFinAsigna(rs.getString("FDFIN_ASIGN"));
		asignaDTO.setUsuaModificado(rs.getString("FCUSUARIO_MOD"));
		asignaDTO.setFechaModificacion(rs.getString("FDFECHA_MOD"));
		
		return asignaDTO;
		
		
	}

}
