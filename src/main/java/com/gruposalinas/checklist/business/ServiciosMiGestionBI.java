package com.gruposalinas.checklist.business;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gruposalinas.checklist.resources.FRQConstantes;

public class ServiciosMiGestionBI {
		
private Logger logger = LogManager.getLogger(ServiciosMiGestionBI.class);
	
	public String obtieneFolios(String ceco) {
			
			//http://10.53.33.83:80/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getIncidentesSucursal.json?idUsuario=331952&ceco=480100
			
			String respuesta= null;
			String urlStr = FRQConstantes.getURLMiGestion()+"servicios/getIncidentesSucursal.json?idUsuario=331952&ceco="+ceco;
			HttpURLConnection conexion = null;
			
			BufferedReader in =null;
			try {
				
				URL url = new URL(urlStr);	
				
				conexion = (HttpURLConnection) url.openConnection();
				conexion.setRequestMethod("GET");
				conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
				
				int responseCode = conexion.getResponseCode();
				
				if(responseCode != 200) {
					
				}else {
					in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
					
					String inputLine;
					
					StringBuffer response = new StringBuffer();
					
					while((inputLine=in.readLine()) != null) {
						response.append(inputLine);
					}
					
					respuesta = response.toString();
					logger.info("Folios"+respuesta);
				}
				
			}catch (Exception e ) {
				respuesta = null;
			}finally {
				try {
					if(in != null) {
						in.close();
					}				
				}catch(Exception ex) {
					ex.printStackTrace();
				}

			}
		
			return respuesta;
				
		}
}
