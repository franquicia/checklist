package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.TipoChecklistDAO;
import com.gruposalinas.checklist.domain.TipoChecklistDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class TipoChecklistBI {

	private static Logger logger = LogManager.getLogger(TipoChecklistBI.class);
	
	@Autowired
	TipoChecklistDAO tipoChecklistDAO;
	
	List<TipoChecklistDTO> listaTiposChecklist = null;
	List<TipoChecklistDTO> listaTipoChecklist = null;
	
	public List<TipoChecklistDTO> obtieneTipoChecklist(){
		try
		{
			listaTiposChecklist = tipoChecklistDAO.obtieneTipoChecklist();
		}
		catch(Exception e)
		{
			logger.info("No fue posible obtener datos de la tabla Tipo Checklist");
			
		}
		
		return listaTiposChecklist;
	}
	
	public List<TipoChecklistDTO> obtieneTipoChecklist(int idTipoCK){
		try
		{
			listaTipoChecklist = tipoChecklistDAO.obtieneTipoChecklist(idTipoCK);
		}
		catch(Exception e)
		{
			logger.info("No fue posible obtener datos de la tabla Tipo Checklist");
			
		}
		
		return listaTipoChecklist;
	}
	
	public int insertaTipoChecklist(TipoChecklistDTO tipoChecklist){
		
		int idTipoChecklist = 0;
		
		try{
			idTipoChecklist = tipoChecklistDAO.insertaTipoChecklist(tipoChecklist);
		}
		catch(Exception e)
		{
			logger.info("No fue posible insertar el Tipo de Checklist");
			
		}
		
		return idTipoChecklist;
	}
	
	public boolean actualizaTipoChecklist (TipoChecklistDTO tipoChecklist){
		boolean respuesta = false;
		
		try{
			respuesta = tipoChecklistDAO.actualizaTipoChecklist(tipoChecklist);
		}
		catch(Exception e)
		{
			logger.info("No fue posible actualizar el Tipo de Checklist");
			
		}
		
		return respuesta;
	}
	
	public boolean eliminaTipoChecklist(int idTipoCK){
		boolean respuesta = false;
		
		try{
			respuesta = tipoChecklistDAO.eliminaTipoChecklist(idTipoCK);
		}
		catch(Exception e)
		{
			logger.info("No fue posible borrar el Tipo de Checklist");
			
		}
		
		return respuesta;
	}
	
	
}
