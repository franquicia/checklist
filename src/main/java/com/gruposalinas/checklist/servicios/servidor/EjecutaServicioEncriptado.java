package com.gruposalinas.checklist.servicios.servidor;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gruposalinas.checklist.business.TokenBI;
import com.gruposalinas.checklist.business.TokenDataCenterBI;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.util.UtilCryptoGS;
import com.gruposalinas.checklist.util.UtilDate;

@Controller
@RequestMapping("/ejecutaServiciosEncriptados")
public class EjecutaServicioEncriptado {
	
	@Autowired
	TokenBI tokenbi;
	
	@Autowired
	TokenDataCenterBI tokenDatacenterbi;
	
	// http://localhost:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/loginRest.json?id=196228
	// http://localhost:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/loginRest2.json?id=196228
	// http://localhost:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/checklist.json?id=196228&lat=19.30941887886347&lon=-99.18721647408643
	// http://localhost:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/checklist2.json?id=196228&lat=19.30941887886347&lon=-99.18721647408643&idCheck=2
	// http://localhost:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/checklistUsuario.json?id=191841&idCheck=1
	// http://localhost:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/checklistOffline.json?id=191312
	// http://10.51.210.239:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/checklistOffline.json?id=191312
	// http://10.51.210.239:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/tipoCifrado.json?id=191312
	// http://10.51.210.239:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getCorreosAcuerdos.json?id=189870&idtienda=G2244&idBitacora=3611
	// http://10.51.210.239:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getCorreosAcuerdos.json?id=189870&idtienda=191841&idBitacora=3875
	//http://10.51.210.239:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getTemplates.json?id=<?>
	
	//http://10.51.218.113:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getListaSucChecklist.json?id=191312
	///10.51.218.113:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getBitacoraGral.json?idUsu=191312&finicio=2018-12-12&fin=2018-12-11&status=1
	
	//10.51.218.113:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getBitacoraGralHi.json?idUsu=1913112&finicio=2018-10-11&fin=2010-12-12&status=4&idBitaGral=31&idBita=17211
	
	//10.51.218.113:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/postAltaFirmaCheckArray.json?idUsu=1913112
			
	
	//10.51.218.113:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getListaSucChecklist.json?idUsu=1913112
	//10.51.218.113:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/sendCorreoTransformacionDirecto.json?idUsuario=73610&idCeco=999995&idFase=1&idProyecto=1
	
	@RequestMapping(value = "/ejecutaServicio", method = RequestMethod.GET)
	public String ejecutaServicio(HttpServletRequest request, HttpServletResponse response)
			throws KeyException, IOException, GeneralSecurityException {
		//System.out.println("Entro a ejecutar servicio");
		
		UtilDate fec=new UtilDate();
		@SuppressWarnings("static-access")
		String fecha = fec.getSysDate("ddMMyyyyHHmmss");
		
		String service = new String(request.getQueryString().getBytes("ISO-8859-1"), "UTF-8");
		String rutaService = service.split("\\?")[0].replace("service=", "");
		String params = service.replaceAll(rutaService, "").replace("service=", "").replace("?", "");
		String enc = new UtilCryptoGS().encryptParams(params);
		String token = new UtilCryptoGS().encryptParams("ip="+FRQConstantes.getIpOrigen()+"&fecha="+fecha+"&idapl=666&uri=http://"+FRQConstantes.getIpOrigen()+"/checklist/"+rutaService).replace("\n", "");
		String tokenCifrado = tokenbi.getToken(token);
		String exec = (rutaService+"?"+enc+"&token="+tokenCifrado).replace("\n", "");
			
		return "redirect:/"+exec;
	}

	//10.51.218.113:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio2.json?service=serviciosDataCenter/getChecklistByGrupo.json?idUsu=191312
	@RequestMapping(value = "/ejecutaServicio2", method = RequestMethod.GET)
	public String ejecutaServicio2(HttpServletRequest request, HttpServletResponse response)
			throws KeyException, IOException, GeneralSecurityException {
		System.out.println("Entro a ejecutar servicio");
		
		UtilDate fec=new UtilDate();
		@SuppressWarnings("static-access")
		String fecha = fec.getSysDate("ddMMyyyyHHmmss");
		
		String service = new String(request.getQueryString().getBytes("ISO-8859-1"), "UTF-8");
		String rutaService = service.split("\\?")[0].replace("service=", "");
		String params = service.replaceAll(rutaService, "").replace("service=", "").replace("?", "");
		String enc = new UtilCryptoGS().encryptParams(params,FRQConstantes.getLlaveEncripcionLocalDatacenter());
		String token = new UtilCryptoGS().encryptParams(("ip="+FRQConstantes.getIpOrigen()+"&fecha="+fecha+"&idapl=10&uri=http://"+FRQConstantes.getIpOrigen()+"/checklist/"+rutaService).replace("\n", ""),FRQConstantes.getLlaveEncripcionLocalDatacenter());
		String tokenCifrado = tokenDatacenterbi.getToken(token);
		String exec = (rutaService+"?"+enc+"&token="+tokenCifrado).replace("\n", "");
			
		return "redirect:/"+exec;
	}


}
