<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="../css/bootstrap/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/jQuery/jquery-ui.css">

	<script	src="../js/jquery/jquery-2.2.4.min.js"></script>
	<script	src="../js/bootstrap.min.js"></script>
	<script	src="../js/jquery/jquery-ui.js"></script>

	<script	src="../js/script-altaSucursal.js"></script>
	
	<style>
       #map {
        height: 360px;
        width: 100%;
       }
    </style>
    
    
<title>Soporte - Alta Sucursal</title>
</head>
<body>

	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuSoporte.jsp" flush="true">
		<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuSoporte.jsp" />
	</tiles:insertTemplate>

	<br>
	<c:choose>
		<c:when test="${(paso=='1') && (alta=='si')}">
			<div class="container">
				<h1>Alta Sucursal</h1>
				<br>
				<c:url value = "/soporte/altaSucursal.htm" var = "altaSucursal" />
				<form:form method="POST" action="${altaSucursal}" model="command" name="form1" id="form1">
					
					<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
						<form:checkbox path="checkGCC" id="checkGCC" name="checkGCC" /> ¿Es GCC?
						<form:select id="nomCanal" name="nomCanal" path="nomCanal" class="form-control">
							<form:options items="${listaCanal}" /> 
						</form:select>
						<form:select id="nomPais" name="nomPais" path="nomPais" class="form-control">
							<form:options items="${listaPaises}" />
						</form:select>
						<form:input type="text" class="form-control" id="latitud" placeholder="Latitud" path="latitud" />	
						<form:input type="text" class="form-control" id="longitud" placeholder="Longitud" path="longitud" />
						<form:input type="text" class="form-control" id="nombresuc" placeholder="Nombre de sucursal" path="nombresuc" />
						<form:input type="text" class="form-control" id="nuSucursal" placeholder="Numero de sucursal" path="nuSucursal" />
						<input type="submit" value="Validar" name="submit" class="btn btn-default btn-lg" onclick="validaAlta2(); return false;"/>
						<input type="submit" id="Agregar" name="Agregar" class="btn btn-default btn-lg" value="Agregar" onclick="return validaAlta();" onkeypress=" initMap();" disabled/>
					</div>
				</form:form>
				
				<div id="map"></div>
			</div>
		</c:when>
		
		<c:when test="${(paso=='3') && (alta=='si')}">
			<div class="container">
				<h1>Alta Sucursal</h1>
				<br>
				<div id="collapse1-2" class="panel-collapse collapse in">
				        <div class="panel-body">
							<div id="map"></div>

								<script>
									function initMap() {
										var latitud1 = ${sucursal.latitud};
										var longitud1 = ${sucursal.longitud};
										var pos1 = new google.maps.LatLng(latitud1, longitud1);
	
										var mapOptions = {
											zoom: 17,
											center: pos1,
											mapTypeId: 'roadmap'
										};
	
										var map1 = new google.maps.Map(document.getElementById('map'), mapOptions);
	
										var marker1 = new google.maps.Marker({
											position: pos1,
											animation: google.maps.Animation.DROP,
									    });

										marker1.setMap(map1);
	
									    marker1.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
									}
							</script>
						</div>
					</div>
				<c:url value = "/soporte/confirmaAltaSucursal.htm" var = "altaSucursal" />
				<form:form method="POST" action="${altaSucursal}" model="command" name="form1" id="form1">
					
					<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
						
						<input id="idPais" name="idPais" type="hidden" value="${sucursal.idPais}"/>
						<input id="idCanal" name="idCanal" type="hidden" value="${sucursal.idCanal}"/>
						<input id="latitud" name="latitud" type="hidden" value="${sucursal.latitud}"/>
						<input id="longitud" name="longitud" type="hidden" value="${sucursal.longitud}"/>
						<input id="nombresuc" name="nombresuc" type="hidden" value="${sucursal.nombresuc}"/>
						<input id="nuSucursal" name="nuSucursal" type="hidden" value="${sucursal.nuSucursal}"/>
						<c:choose>
							<c:when test="${auxGCC=='checked'}">
								<input type="hidden" id="auxGCC" name="auxGCC" value="checked">
							</c:when>
						</c:choose>
						
						
						<input type="submit" class="btn btn-default btn-lg" value="Confirmar" onclick="return validaAlta();"/>
					</div>
				</form:form>
			</div>
		</c:when>
		
		<c:when test="${(paso=='2') && (alta=='si')}">
			<c:choose>
				<c:when test="${res==0}">
					<div class="container">
						<div class="alert alert-danger" role="alert">
							<p class="text-center"> 
								<h4>La Sucursal no fue creada correctamente!</h4>
								<br>Presione el botón de regresar para intentarlo nuevamente.
							<p>
							<br>
							<c:url value="/soporte/altaSucursal.htm" var="altaSucursal" />
							<form:form method="GET" action="${altaSucursal}" model="command" name="form-regresar2">
								<input type="submit" class="btn btn-danger btn-lg" value="Regresar" />
							</form:form>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="container">
						<div class="alert alert-success" role="alert">
							<p class="text-center"> 
								<h4>Sucursal dada de alta!</h4>
								<br>Presione el botón de regresar para dar de alta otra Sursal.
							<p>
							<br>
							<div class="container">
								<c:url value="/soporte/altaSucursal.htm" var="altaSucursal" />
								<form:form method="GET" action="${altaSucursal}" model="command" name="form-regresar">
									<input type="submit" class="btn btn-success btn-lg" value="Regresar" />
								</form:form>
							</div>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</c:when>

	</c:choose>
	

	<script async defer 
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwiEzyjP4j-rSsoN9qDZvywvdgZP5nbA4&callback=initMap">
	</script>
</body>
</html>