package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AdicionalesDTO;
import com.gruposalinas.checklist.mappers.AdicionalesHistRowMapper;
import com.gruposalinas.checklist.mappers.AdicionalesRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AdicionalesDAOImpl extends DefaultDAO implements AdicionalesDAO {

    private static Logger logger = LogManager.getLogger(AdicionalesDAOImpl.class);

    DefaultJdbcCall jdbcInsertaAdicionales;
    DefaultJdbcCall jdbcEliminaAdicionales;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneAdicionales;
    DefaultJdbcCall jdbcObtieneAdicionalesBita;
    DefaultJdbcCall jbdcActualizaAdicionales;
//HISTORIAL

    DefaultJdbcCall jdbcInsertaAdicionalesHis;
    DefaultJdbcCall jdbcEliminaAdicionalesHis;
    DefaultJdbcCall jdbcObtieneTodoHis;
    DefaultJdbcCall jdbcObtieneAdicionalesHis;
    DefaultJdbcCall jbdcActualizaAdicionalesHist;

    public void init() {

        jdbcInsertaAdicionales = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADADICIONALES")
                .withProcedureName("SP_CARGA_AD");

        jdbcEliminaAdicionales = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADADICIONALES")
                .withProcedureName("SP_DEL_ADIC");

        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADADICIONALES")
                .withProcedureName("SP_DETA_ADIC")
                .returningResultSet("RCL_AD", new AdicionalesRowMapper());

        //ceco
        jdbcObtieneAdicionales = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADADICIONALES")
                .withProcedureName("SP_SEL_ADIC")
                .returningResultSet("RCL_AD", new AdicionalesRowMapper());
        //bita
        jdbcObtieneAdicionalesBita = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADADICIONALES")
                .withProcedureName("SP_SEL_PARAM")
                .returningResultSet("RCL_AD", new AdicionalesRowMapper());

        jbdcActualizaAdicionales = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADADICIONALES")
                .withProcedureName("SP_ACT_ADIC");

        //HISTORIAL
        jdbcInsertaAdicionalesHis = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADADICIONALES")
                .withProcedureName("SP_INS_HISTAD");

        jdbcEliminaAdicionalesHis = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADADICIONALES")
                .withProcedureName("SP_ELIMINA_HISTAD");

        jdbcObtieneTodoHis = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADADICIONALES")
                .withProcedureName("SP_DETA_HIADIC")
                .returningResultSet("RCL_AD", new AdicionalesHistRowMapper());

        //bita
        jdbcObtieneAdicionalesHis = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADADICIONALES")
                .withProcedureName("SP_SEL_PARAMHIS")
                .returningResultSet("RCL_AD", new AdicionalesHistRowMapper());

    }

    public int inserta(AdicionalesDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bean.getBitGral());

        out = jdbcInsertaAdicionales.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PADADICIONALES.SP_INS_HISTAD}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el adicionales");
        } else {
            return idEmpFij;
        }
        return idEmpFij;
    }

    public boolean elimina(String bitacora) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", bitacora);

        out = jdbcEliminaAdicionales.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PADADICIONALES.SP_DEL_ADIC}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar adicionales de la  bitacora(" + bitacora + ")");
        } else {
            return true;
        }
        return false;
    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<AdicionalesDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<AdicionalesDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneTodo.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PADADICIONALES.SP_DETA_ADIC}");

        listaFijo = (List<AdicionalesDTO>) out.get("RCL_AD");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los Empleados Fijos");
        } else {
            return listaFijo;
        }
        return listaFijo;
    }
    //parametro ceco

    @SuppressWarnings("unchecked")
    public List<AdicionalesDTO> obtieneDatos(String ceco) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AdicionalesDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco);

        out = jdbcObtieneAdicionales.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PADADICIONALES.SP_SEL_ADIC}");
        //lleva el nombre del cursor del procedure
        lista = (List<AdicionalesDTO>) out.get("RCL_AD");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener adicionales del ceco(" + ceco + ")");
        }
        return lista;
    }
    //parametro bita

    @SuppressWarnings("unchecked")
    public List<AdicionalesDTO> obtieneDatosBita(String bita, String idPreg) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AdicionalesDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bita)
                .addValue("PA_PREG", idPreg);

        out = jdbcObtieneAdicionalesBita.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PADADICIONALES.SP_SEL_PARAM}");
        //lleva el nombre del cursor del procedure
        lista = (List<AdicionalesDTO>) out.get("RCL_AD");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el empleado(" + bita + ")");
        }
        return lista;
    }

    public boolean actualiza(AdicionalesDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_OBS", bean.getObs())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_PREG", bean.getIdPreg())
                .addValue("PA_RECO", bean.getRecorrido())
                .addValue("PA_RUTA", bean.getRuta());

        out = jbdcActualizaAdicionales.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PADADICIONALES.SP_ACT_ADIC}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado de la preg( " + bean.getIdPreg() + ")");
        } else {
            return true;
        }
        return false;

    }

    @Override
    public int insertaHist(AdicionalesDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bean.getBitGral())
                .addValue("PA_PREG", bean.getIdPreg());

        out = jdbcInsertaAdicionalesHis.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PADADICIONALES.SP_INS_HISTAD}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el historico adicionales");
        } else {
            return idEmpFij;
        }
        return idEmpFij;
    }

    @Override
    public boolean eliminaHist(String bitacora) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", bitacora);

        out = jdbcEliminaAdicionalesHis.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PADADICIONALES.SP_ELIMINA_HISTAD}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar adicionales de la  bitacora(" + bitacora + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<AdicionalesDTO> obtieneDatosHist(String ceco) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AdicionalesDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", ceco);

        out = jdbcObtieneAdicionalesHis.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PADADICIONALES.SP_SEL_PARAMHIS}");
        //lleva el nombre del cursor del procedure
        lista = (List<AdicionalesDTO>) out.get("RCL_AD");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener historial adicionales del ceco(" + ceco + ")");
        }
        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<AdicionalesDTO> obtieneInfoHist() throws Exception {
        Map<String, Object> out = null;
        List<AdicionalesDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneTodoHis.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PADADICIONALES.SP_DETA_HIADIC}");

        listaFijo = (List<AdicionalesDTO>) out.get("RCL_AD");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los historial adic");
        } else {
            return listaFijo;
        }

        return listaFijo;
    }

}
