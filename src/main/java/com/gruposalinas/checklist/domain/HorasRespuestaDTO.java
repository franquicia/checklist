package com.gruposalinas.checklist.domain;

public class HorasRespuestaDTO {
	
	private String hora;
	private String ceco;
	
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getCeco() {
		return ceco;
	}
	public void setCeco(String ceco) {
		this.ceco = ceco;
	}
	
	

}
