/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.business;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gruposalinas.checklist.servicios.servidor.ServiceServerLogs;
import com.gs.baz.frq.model.commons.DataNotFoundException;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.RollingFileAppender;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.apache.logging.log4j.layout.template.json.JsonTemplateLayout;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class ServerLogsBI {

    private final Logger logger = LogManager.getLogger();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final LoggerContext context = (LoggerContext) LogManager.getContext(false);
    private BasicFileAttributes basicFileAttributes;
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS aa");
    private final Configuration config = Configuration.builder()
            .jsonProvider(new JacksonJsonNodeJsonProvider())
            .mappingProvider(new JacksonMappingProvider())
            .build();

    public void init() {
    }

    public ObjectNode readLogsHistory(String file, String appender, String jsonPath, ObjectNode orderBy) {
        return readLogsFormat(appender, jsonPath, orderBy, file);
    }

    public ObjectNode readLogs(String appender, String jsonPath, ObjectNode orderBy) {
        return readLogsFormat(appender, jsonPath, orderBy, null);
    }

    private ObjectNode readLogsFormat(String appender, String jsonPath, ObjectNode orderBy, String fileHistory) {
        ObjectNode logsInfo = objectMapper.createObjectNode();
        RollingFileAppender rollingFileAppender = context.getConfiguration().getAppender(appender);
        if (rollingFileAppender != null) {
            String hostName = context.getConfiguration().getProperties().get("hostName");
            try {
                Path path;
                if (fileHistory != null) {
                    String filePattern = rollingFileAppender.getFilePattern();
                    String ext = filePattern.substring(filePattern.lastIndexOf("."), filePattern.length());
                    String filePath = filePattern.substring(0, filePattern.lastIndexOf("/")) + "/" + fileHistory + ext;
                    path = new File(filePath).toPath();
                } else {
                    path = new File(rollingFileAppender.getFileName()).toPath();
                }
                if (!path.toFile().exists()) {
                    return null;
                }
                List<ObjectNode> arrayNode = new ArrayList<>();
                List<String> linesFile = Files.readAllLines(path);
                if (rollingFileAppender.getLayout() instanceof PatternLayout) {
                    Collections.reverse(linesFile);
                    logsInfo.put("hostName", hostName);
                    logsInfo.put("size", linesFile.size());
                    logsInfo.putPOJO("logs", linesFile);
                    return logsInfo;
                } else if (rollingFileAppender.getLayout() instanceof JsonTemplateLayout) {
                    for (String line : linesFile) {
                        ObjectNode log = objectMapper.readValue(line, ObjectNode.class);
                        arrayNode.add(log);
                    }
                    ReadContext ctx = JsonPath.using(config).parse(objectMapper.writeValueAsString(arrayNode));
                    if (jsonPath == null) {
                        jsonPath = "$";
                    }
                    String logs = ctx.read(jsonPath).toString();
                    List<ObjectNode> out = objectMapper.readValue(logs, objectMapper.getTypeFactory().constructCollectionType(List.class, ObjectNode.class));
                    if (orderBy != null) {
                        Collections.sort(out, (ObjectNode p1, ObjectNode p2) -> {
                            ReadContext ctxP1 = JsonPath.using(config).parse(p1);
                            ReadContext ctxP2 = JsonPath.using(config).parse(p2);
                            Iterator<Map.Entry<String, JsonNode>> fields = orderBy.fields();
                            CompareToBuilder cb = new CompareToBuilder();
                            while (fields.hasNext()) {
                                Map.Entry<String, JsonNode> jsonField = fields.next();
                                String valueP1 = null;
                                String valueP2 = null;
                                try {
                                    valueP1 = ctxP1.read(jsonField.getKey(), String.class);
                                    valueP2 = ctxP2.read(jsonField.getKey(), String.class);
                                } catch (Exception ex) {

                                }
                                if (valueP1 != null && valueP2 != null) {
                                    if (jsonField.getValue().get("type") != null && jsonField.getValue().get("type").asText().equals("desc")) {
                                        cb.append(valueP2, valueP1);
                                    } else {
                                        cb.append(valueP1, valueP2);
                                    }
                                }
                            }
                            return cb.toComparison();
                        });
                    }
                    Collections.reverse(out);
                    logsInfo.put("hostName", hostName);
                    logsInfo.put("size", out.size());
                    logsInfo.putPOJO("logs", out);
                    return logsInfo;
                } else {
                    return null;
                }
            } catch (IOException ex) {
                logger.log(Level.ERROR, ex.getMessage(), ex);
                return null;
            }
        }
        return null;
    }

    public List<ObjectNode> listLogsHistory(String appender) {
        RollingFileAppender rollingFileAppender = context.getConfiguration().getAppender(appender);
        if (rollingFileAppender != null) {
            List<ObjectNode> nodeItems = new ArrayList<>();
            String path = rollingFileAppender.getFilePattern();
            path = path.substring(0, path.lastIndexOf("/"));
            try ( Stream<Path> paths = Files.walk(Paths.get(path), Integer.MAX_VALUE)) {
                paths.forEach(item -> {
                    try {
                        basicFileAttributes = Files.readAttributes(item.toFile().toPath(), BasicFileAttributes.class);
                        FileTime fileTime = basicFileAttributes.lastModifiedTime();
                        if (item.toFile().isFile()) {
                            long bytes = Files.size(item);
                            long kilobytes = (bytes / 1024);
                            long megabytes = (kilobytes / 1024);
                            String fileName = item.getFileName().toString();
                            ObjectNode objectNode = objectMapper.createObjectNode();
                            fileName = fileName.substring(0, fileName.lastIndexOf("."));
                            List<String> linesFile = Files.readAllLines(item);
                            Link link = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(ServiceServerLogs.class).readLogsHistory(fileName, appender, "$", "{}")).withSelfRel();
                            objectNode.put("fileName", item.getFileName().toString());
                            objectNode.put("href", link.getHref());
                            objectNode.put("lastModifiedTime", simpleDateFormat.format(new Date(fileTime.toMillis())));
                            objectNode.put("lines", linesFile.size());
                            if (kilobytes == 0) {
                                objectNode.put("size", bytes + " Bytes");
                            } else if (megabytes == 0) {
                                objectNode.put("size", kilobytes + " KB");
                            } else {
                                objectNode.put("size", megabytes + " MB");
                            }
                            nodeItems.add(objectNode);
                        }
                    } catch (DataNotFoundException | IOException ex) {
                        logger.log(Level.ERROR, ex.getMessage(), ex);
                    }
                });
                Collections.sort(nodeItems, (ObjectNode p1, ObjectNode p2) -> {
                    CompareToBuilder cb = new CompareToBuilder();
                    String valueP1 = p1.get("lastModifiedTime").asText();
                    String valueP2 = p2.get("lastModifiedTime").asText();
                    if (valueP1 != null && valueP2 != null) {
                        cb.append(valueP2, valueP1);
                    }
                    return cb.toComparison();
                });
                if (nodeItems.isEmpty()) {
                    return null;
                }
                return nodeItems;
            } catch (Exception ex) {
                return null;
            }
        }
        return null;
    }
}
