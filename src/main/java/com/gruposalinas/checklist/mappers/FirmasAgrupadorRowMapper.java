package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.AdmFirmasCatDTO;

public class FirmasAgrupadorRowMapper implements RowMapper<AdmFirmasCatDTO> {

	public AdmFirmasCatDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AdmFirmasCatDTO AdmFirmasCatDTO = new AdmFirmasCatDTO();
		
		AdmFirmasCatDTO.setIdAgrupaFirma(rs.getInt("FIID_AGRUFIR"));
		AdmFirmasCatDTO.setIdFirmaCatalogo(rs.getInt("FIID_FIRM"));
	    AdmFirmasCatDTO.setCargo(rs.getString("FCCARGO"));
	    AdmFirmasCatDTO.setObligatorio(rs.getInt("FIIDOBLIGA"));
	    AdmFirmasCatDTO.setOrden(rs.getInt("FIIDORDEN"));
	    AdmFirmasCatDTO.setAux2(rs.getString("FCAUX"));
	    AdmFirmasCatDTO.setPeriodo(rs.getString("FCPERIODO"));
	    
		return AdmFirmasCatDTO;
	}

}


