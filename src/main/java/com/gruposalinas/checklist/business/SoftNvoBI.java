package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.SoftNvoDAO;
import com.gruposalinas.checklist.domain.SoftNvoDTO;

public class SoftNvoBI {

	private static Logger logger = LogManager.getLogger(SoftNvoBI.class);

private List<SoftNvoDTO> listafila;
	
	@Autowired
	SoftNvoDAO softNvoDAO;
		
	
	public int insertaSoftN(SoftNvoDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = softNvoDAO.insertaSoftN(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	public int insertaSoftNuevoFlujoMtto(SoftNvoDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = softNvoDAO.insertaSoftNuevoFlujoMtto(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaSoftN(int idEmpFijo){
		boolean respuesta = false;
		
		try {
			respuesta = softNvoDAO.eliminaSoftN(idEmpFijo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<SoftNvoDTO> obtieneDatosSoftN( String idUsuario) {
		
		try {
			listafila =  softNvoDAO.obtieneDatosSoftN(idUsuario);
			
			if (listafila != null && listafila.size() > 0) {
				
				for (SoftNvoDTO obj : listafila) {
					
					if (obj.getIdCanal() != 21) {
						obj.setStatus(obj.getStatus()+3);
					}
					
				}
				
			}
			
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	
	
	public boolean actualizaSoftN(SoftNvoDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = softNvoDAO.actualizaSoftN(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public int insertaCalculosActa(SoftNvoDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = softNvoDAO.insertaCalculosActa(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaCalculosActa(int idEmpFijo){
		boolean respuesta = false;
		
		try {
			respuesta = softNvoDAO.eliminaCalculosActa(idEmpFijo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<SoftNvoDTO> obtieneDatosCalculosActa( String idUsuario) {
		
		try {
			listafila =  softNvoDAO.obtieneDatosCalculosActa(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	
	
	public boolean actualizaCalculosActa(SoftNvoDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = softNvoDAO.actualizaCalculosActa(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public List<SoftNvoDTO> obtieneDatosSoftNDeta( String idUsuario) {
		
		try {
			listafila =  softNvoDAO.obtieneDatosSoftNDeta(idUsuario);
			
			
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public int insertaSoftNuevoFlujo(SoftNvoDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = softNvoDAO.insertaSoftNuevoFlujo(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
}