package com.gruposalinas.checklist.mappers;

import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.TipoChecklistDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ChecklistOfflineRowMapper3 implements RowMapper<ChecklistDTO> {

    public ChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ChecklistDTO checklistDTO = new ChecklistDTO();
        TipoChecklistDTO tipochecklist = new TipoChecklistDTO();

        checklistDTO.setIdTipoChecklist(tipochecklist);
        tipochecklist.setIdTipoCheck(rs.getInt("FIID_TIPO_CHECK"));
        checklistDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
        checklistDTO.setNombreCheck(rs.getString("FCNOMBRE"));
        checklistDTO.setIdHorario(rs.getInt("FIID_HORARIO"));
        checklistDTO.setVigente(rs.getInt("FIVIGENTE"));
        checklistDTO.setFecha_inicio(rs.getString("FDFECHA_INICIO"));

        checklistDTO.setFecha_fin(rs.getString("FDFECHA_FIN"));
        checklistDTO.setIdEstado(rs.getInt("FIID_ESTADO"));
        checklistDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
        checklistDTO.setDia(rs.getString("FCDIA"));
        checklistDTO.setPeriodo(rs.getString("FCPERIODO"));
        checklistDTO.setVersion("" + rs.getInt("FIVERSION"));
        checklistDTO.setOrdeGrupo("" + rs.getInt("FIORDEN_GRUPO"));

        checklistDTO.setPonderacionTot(rs.getDouble("FCPONDTOTAL"));
        checklistDTO.setClasifica(rs.getString("FCCLASIFICA"));

        checklistDTO.setFiordenGrup(rs.getInt("FIORDEN"));

        checklistDTO.setIdProtocolo(rs.getInt("FIID_PROTOCOLO"));

        return checklistDTO;
    }
}
