package com.gruposalinas.checklist.domain;

public class AcumuladosSemana {

	private String semana;
	private Double acumuladoAlDia;
	private Double totalCompromisoAlDia;
	private Double totalRealSemana;
	private Double totalCompromisoSemana;
	private Double realAnioPasado;
	
	public String getSemana() {
		return semana;
	}
	public void setSemana(String semana) {
		this.semana = semana;
	}
	public Double getAcumuladoAlDia() {
		return acumuladoAlDia;
	}
	public void setAcumuladoAlDia(Double acumuladoAlDia) {
		this.acumuladoAlDia = acumuladoAlDia;
	}
	public Double getTotalCompromisoAlDia() {
		return totalCompromisoAlDia;
	}
	public void setTotalCompromisoAlDia(Double totalCompromisoAlDia) {
		this.totalCompromisoAlDia = totalCompromisoAlDia;
	}
	public Double getTotalRealSemana() {
		return totalRealSemana;
	}
	public void setTotalRealSemana(Double totalRealSemana) {
		this.totalRealSemana = totalRealSemana;
	}
	public Double getTotalCompromisoSemana() {
		return totalCompromisoSemana;
	}
	public void setTotalCompromisoSemana(Double totalCompromisoSemana) {
		this.totalCompromisoSemana = totalCompromisoSemana;
	}
	public Double getRealAnioPasado() {
		return realAnioPasado;
	}
	public void setRealAnioPasado(Double realAnioPasado) {
		this.realAnioPasado = realAnioPasado;
	}
	
	
}
