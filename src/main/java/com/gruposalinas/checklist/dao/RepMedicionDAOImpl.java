package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.RepMedicionDTO;
import com.gruposalinas.checklist.mappers.RepMedicionPregRowMapper;
import com.gruposalinas.checklist.mappers.RepMedicionRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class RepMedicionDAOImpl extends DefaultDAO implements RepMedicionDAO {

    private static final Logger logger = LogManager.getLogger(RepMedicionDAOImpl.class);

    private DefaultJdbcCall jdbcReporteZonas;
    private DefaultJdbcCall jdbcReportePregunta;

    public void init() {

        jdbcReporteZonas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORMEDICION")
                .withProcedureName("SPGETREPORTE")
                .returningResultSet("RCL_REPORTE", new RepMedicionRowMapper());

        jdbcReportePregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORMEDICION")
                .withProcedureName("SPGETPREGUNTA")
                .returningResultSet("RCL_PREGUNTA", new RepMedicionPregRowMapper());
    }

    @Override
    public List<RepMedicionDTO> ReporteZonas(int idProtocolo, String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        List<RepMedicionDTO> listaReporte = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PROTOCOLO", idProtocolo)
                .addValue("PA_FECHAINI", fechaInicio)
                .addValue("PA_FECHAFIN", fechaFin);

        out = jdbcReporteZonas.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAREPORMEDICION.SPGETREPORTE}");

        listaReporte = (List<RepMedicionDTO>) out.get("RCL_REPORTE");

        if (listaReporte.size() == 0) {
            logger.info("El Store Procedure retorno listaReporte vacio");
        }

        return listaReporte;

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<RepMedicionDTO> ReportePreguntas(RepMedicionDTO bean) throws Exception {
        Map<String, Object> out = null;
        List<RepMedicionDTO> lista1 = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_PROTOCOLO", bean.getIdProtocolo());

        out = jdbcReportePregunta.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAREPORMEDICION.SPGETPREGUNTA}");

        lista1 = (List<RepMedicionDTO>) out.get("RCL_PREGUNTA");

        if (lista1.size() == 0) {
            throw new Exception("El Store Procedure retorno listaReporte vacio");
        }

        return lista1;

    }

}
