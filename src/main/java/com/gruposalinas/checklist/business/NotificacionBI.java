package com.gruposalinas.checklist.business;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.NotificacionDAO;
import com.gruposalinas.checklist.domain.MovilInfoDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class NotificacionBI {
	private static final Logger logger = LogManager.getLogger(ChecklistBI.class);
	
	@Autowired
	NotificacionDAO notificacionDAO;

	Map<String, Object> listaNotificacion = null;
	List<MovilInfoDTO> listaMovil = null;

	public Map<String, Object> NotificacionPorcentaje(int idUsuario) throws Exception{
		
		logger.info("antes");
		listaNotificacion = notificacionDAO.notificacionPorcentaje(idUsuario);
		
		if(listaNotificacion.size() == 0) return null;
		else return listaNotificacion;		
		
	}
	
	public  List<MovilInfoDTO> obtieneUsuariosNoti(String so) {
				
		try{
			listaMovil = notificacionDAO.obtieneUsuariosNoti(so);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Compromiso "+e.getMessage());
					
		}
				
		return listaMovil;
	}

}
