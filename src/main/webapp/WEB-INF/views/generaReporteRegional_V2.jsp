<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="height: 98%; min-width: 1200px">
<head>

<link href="../css/jQuery/jQuery.css"
	rel="stylesheet" type="text/css" />

<link href="../css/checklistDesign/checklistTableDesignv2.css"
	rel="stylesheet" type="text/css" />

<script type="text/javascript">
	var urlServer = '${urlServer}';
</script>

<script src="../css/jQuery/jQuery.js"></script>
<script src="../js/script-reportev2.js"></script>


<!-- DESABILITA EL BOTON DE ENTER EN LA VISTA -->
<script type="text/javascript">
	$(document).ready(function() {
		$(window).keydown(function(event) {
			if (event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});
	});
</script>


<!-- PONE EL TIEMPO DEL MENSAJE -->
<script type="text/javascript">
	$(document).ready(function() {
		setTimeout(function() {
			$(".content-Mensaje").fadeOut(1500);
		}, 3000);
	});
</script>

<!-- EJECUTA EL CALENDARIO -->
<script type="text/javascript">
	$(function() {
		$("#fechaInicio").datepicker();
	});
	$(function() {
		$("#fechaFin").datepicker();
	});
</script>

</head>
<body style="height: 60%;">

	
<div id="generalContainer">

				<div id='banner' style="position: relative; text-align: center; font: 18px 'Roboto-Light';">
				Generar Reporte
				</div>
				
				<div  id="geografiaContainer">
					<ul class="margenUl">
						<div class="primer-nivel">
						<li class="li-stilo">
						<span></span>
							<div class="cajaVis" id="categoriaVis">
								<input type="hidden" name="categoria" 
								value="Selecciona una Opcion" id="valorCategoria">
								<span class="seleccionadoVis" 
								id="Selecciona una Opcion">Elije una categoria</span>
								<ul class="listaselectVis" id="buscaPuesto"
								style="height: auto; max-height: 250px; overflow: auto; z-index: 1;">
									<c:forEach items="${listaTipoCheck}" var="item">
										<li value="${item.idTipoCheck}"><a href=""></a>${item.descTipo}</li>
									</c:forEach>						
								</ul >
								<span class="trianguloinfVis"></span>
							</div>	
						</li>
						</div>
						
						<div class="primer-nivel" >
						<li class="li-stilo">
						<span></span> 
							<div class="cajaVis" id="tipoVis">
								<input type="hidden" name="tipo"
								value="Selecciona una Opcion" id="valorTipo">
								<span class="seleccionadoVis" id="Selecciona una Opcion">Elije una opcion</span>
								<ul class="listaselectVis" id="tipoVis2" 
								style="height: auto; max-height: 250px; overflow: auto; z-index: 1;">
								</ul>
								<span class="trianguloinfVis"></span>
							</div>
						</li>
						</div>
					</ul>
				
				</br></br></br></br>
			<div>
				
				<ul class="margenUl2">
					<div class="primer-nivel">
						<li class="li-stilo margenLista">
							<div>
							<input class="textBoxInput2" type="text" id="fechaInicio" name="fechaInicio" placeholder="Selecciona fecha inicio" size="22" />
							</div>
					</div>
					
					<div class="primer-nivel">
						<li class="li-stilo margenLista">
							<div>
							<input class="textBoxInput2" type="text" id="fechaFin" name="fechaFin" placeholder="Selecciona fecha fin" size="22" />
							</div>
					</div>
					
					
					</ul>
			</div>
	
				
			
			<div style="height: 10.6%; min-height: 70px;">
				
			<a href="#" id="btn-excel" onclick="return generaReporte();"><img src="/checklist/images/v2/ico-excel.png">Generar</a>
			<a href="#" id="bnt-limpiar" onclick="return limpiaReporte();">Limpiar</a> <br>
			</div>
	</div>
		</div>	
</body>
</html>
