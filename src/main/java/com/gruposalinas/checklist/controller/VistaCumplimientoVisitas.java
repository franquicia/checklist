package com.gruposalinas.checklist.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.business.ChecklistBI;
import com.gruposalinas.checklist.business.FiltrosCecoBI;
import com.gruposalinas.checklist.business.GeneraReporteCumplimiento;
import com.gruposalinas.checklist.business.ReporteBI;
import com.gruposalinas.checklist.business.ReporteRegBI;
import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.CompromisoCecoDTO;
import com.gruposalinas.checklist.domain.VistaCDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.domain.EvidenciaDTO;
import com.gruposalinas.checklist.domain.PaisDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.util.UtilDate;

@Controller
public class VistaCumplimientoVisitas {

	private static Logger logger = LogManager.getLogger(VistaCumplimientoVisitas.class);
	private String nombreCecoAux;
	private boolean opcionReporte;
	private Map<String, Object> mapGlobal;
	
	@Autowired
	FiltrosCecoBI filtrosCecoBI;
	
	@Autowired
	ReporteBI reporteBI;
	
	@Autowired
	ReporteRegBI reporteRegBI;
	
	@Autowired
	ChecklistBI checklistBI;
	
	@Autowired
	CecoBI cecobi;
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"central/vistaCumplimientoVisitas.htm","central/vistaCumplimientoVisitasMovil.htm"}, method = RequestMethod.GET)
	public ModelAndView vistaCumplimientoVisitas(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaCumplimientoVisitas...");

		String uri= request.getRequestURI();
		if(uri.contains("central/vistaCumplimientoVisitasMovil.htm"))
			this.opcionReporte=true;
		else
			this.opcionReporte=false;
		logger.info("this.opcionReporte "+this.opcionReporte);
		
		String paso=request.getParameter("paso");
		logger.info("Paso: "+paso);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
		
			if(idUsuario>0){
				mv = new ModelAndView("vistaCumplimientoVisitas");
				
				if(paso != null){
					mv.addObject("paso", new String("1"));
				}
				
				mv.addObject("idUsuario", idUsuario);
				mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
				mv.addObject("opcionReporte", this.opcionReporte);
				//Fragmento para seleccionar el filtrado de tiendas
				
				List<CompromisoCecoDTO> listaValorPerfil = new ArrayList<CompromisoCecoDTO>();
				int totalPerfil = -1;
				String idCecoPerfil = "";
				String nomCecoPerfil = "";
				logger.info("datos antes del mapa... ");
	
				try {
					listaValorPerfil = reporteBI.CecoInfo(idUsuario);
					logger.info("listaValorPerfil... " + listaValorPerfil);
					if(!listaValorPerfil.isEmpty()){
						totalPerfil = listaValorPerfil.get(0).getTotal();
						idCecoPerfil = listaValorPerfil.get(0).getIdCeco();
						nomCecoPerfil = listaValorPerfil.get(0).getDescCeco();
					}
					logger.info("nivelPerfil... " + totalPerfil);
					logger.info("idCeco... " + idCecoPerfil);
					logger.info("nombreCeco... " + nomCecoPerfil);
				} catch (Exception e) {
					logger.info("Ocurrio algo... ");
				}
				
				logger.info("nivelPerfil... " + totalPerfil);
				logger.info("idCeco... " + idCecoPerfil);
				logger.info("nombreCeco... " + nomCecoPerfil);
				mv.addObject("nivelPerfil", totalPerfil);
				mv.addObject("idCeco", idCecoPerfil);
				mv.addObject("nombreCeco", nomCecoPerfil);
				
				Map<String, Object> map= new HashMap<String, Object>();	
				//List<NegocioDTO> negocio=null;
				List<PaisDTO> pais=null;
				logger.info("datos antes del mapa 2... ");
	
				try {
					map= filtrosCecoBI.obtieneFiltros(idUsuario);
					logger.info("map... " + map);
					//negocio = (List<NegocioDTO>) map.get("negocio");
					pais = (List<PaisDTO>) map.get("pais");
					//logger.info("negocio... " + negocio.get(0).getDescripcion());
					logger.info("pais... " + pais.get(0).getNombre());
				} catch (Exception e) {
					logger.info("Ocurrio algo... ");
				}
				//int negocioId=0;
				//if(!negocio.isEmpty()){
					try {
						pais=filtrosCecoBI.obtienePaises(1,idUsuario);
					} catch (Exception e) {
						logger.info("Ocurrio algo... ");
					}
					//if(negocio.get(0).getDescripcion()!=null)
						//negocioId=negocio.get(0).getIdNegocio();
						
				//}
				//mv.addObject("negocio", negocio);
				logger.info("paises " + pais);
					
				mv.addObject("pais", pais);
				mv.addObject("negocioId", 1);
							
			}else{
				logger.info("...SALE DEL CONTROLLER vistaCumplimientoVisitas...");
				mv = new ModelAndView("http404");
			}
		}
		
		logger.info("...SALE DEL CONTROLLER vistaCumplimientoVisitas...");
		return mv;
	}
	
	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	@RequestMapping(value = "central/vistaFiltroCumplimientoVisitas.htm", method = RequestMethod.POST)
	public ModelAndView vistaFiltroCumplimientoVisitas(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
			@RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,		
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,		
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,			
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaFiltroCumplimientoVisitas...");
		
		logger.info("this.opcionReporte "+this.opcionReporte);
		
		if(nivelPerfil==1)
			banderaCecoPadre=1;
		
		logger.info("banderaCecoPadre... "+banderaCecoPadre);		
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("porcentajeG... "+porcentajeG);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("nombreCeco... "+nombreCeco);		
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		
		this.nombreCecoAux=nombreCeco.split("/")[0];
		logger.info("nombreCecoAux "+ this.nombreCecoAux);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
		
			List<ChecklistDTO> listaChecklist = null;
			List<VistaCDTO> listaSucursales = null;
			List<List> listaReporteCum= new ArrayList<List>();
			int porcentaje = 0, conteo=0;
			
			Map<String, Object> map= new HashMap<String, Object>();
			List<List> listRetorno= new ArrayList<List>();
			String fechaReporte=""+seleccionMes+"/"+seleccionAno;
			logger.info("datos antes del mapa... ");
			
			try {
				map=reporteBI.ReporteVistaCumplimiento(idUsuario,null, seleccionPais, seleccionCanal,fechaReporte,banderaCecoPadre);
				logger.info("map... " + map);
				
			} catch (Exception e) {
				logger.info("Ocurrio algo... ");
				logger.info("No existe informacion");
			}
			
			if(map!=null){
				porcentaje= (Integer) map.get("porcentaje");
				logger.info("porcentaje... "+porcentaje);
				listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
				//listaChecklist=new ArrayList<ChecklistDTO>();
				logger.info("listaChecklist... "+listaChecklist);
				
				listaReporteCum= (List<List>) map.get("listaSucursales");
				logger.info("listaSucursales... "+listaReporteCum);
				logger.info("listaSucursales.size()... "+listaReporteCum.size());
				conteo = (Integer) map.get("conteo");
				logger.info("conteo... "+conteo);
							
				List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
				
				if(conteo==0)
					conteo++;
				
				for (int i = 0; i < listaReporteCum.size(); i++) {			
					listaAux.add((VistaCDTO) listaReporteCum.get(i));
		
					if((i+1)%conteo==0){
						listRetorno.add(listaAux);				
						listaAux=new ArrayList<VistaCDTO>();
					}
				}
				logger.info("listRetorno.size() "+listRetorno.size());
				logger.info("listRetorno "+listRetorno);
				if(listRetorno.size()>0)
					logger.info("listRetorno 0 size() "+listRetorno.get(0).size());
				
				if(idUsuario>0){
					mv = new ModelAndView("vistaFiltroCumplimientoVisitas");
					
					mv.addObject("opcionReporte", this.opcionReporte);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("porcentajeG", porcentajeG);
					mv.addObject("porcentaje", porcentaje);
					
					mv.addObject("idCecoPadre", idCecoPadre);
					mv.addObject("nombreCeco", this.nombreCecoAux);
					mv.addObject("banderaCecoPadre", 0);
					
					mv.addObject("seleccionAno", seleccionAno);
					mv.addObject("seleccionMes", seleccionMes);
					mv.addObject("seleccionCanal", seleccionCanal);
					mv.addObject("seleccionPais", seleccionPais);

					mv.addObject("conteo", conteo);
					mv.addObject("listaChecklist", listaChecklist);
					mv.addObject("listaSucursales", listRetorno);
					mv.addObject("url", FRQConstantes.getURLServer());
				}else{					
					mv = new ModelAndView("http404");
					logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitas http404...");
				}
			}else{
				logger.info("No existe informacion");
				mv = new ModelAndView("redirect:vistaCumplimientoVisitas.htm");
				mv.addObject("paso", new String("1"));
				logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitas redirect...");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitas...");
		return mv;
	}
	
	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	@RequestMapping(value = "central/vistaFiltroCumplimientoVisitasDes.htm", method = RequestMethod.POST)
	public ModelAndView vistaFiltroCumplimientoVisitasDes(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
			@RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,		
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,		
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,			
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaFiltroCumplimientoVisitasDes...");
		
		logger.info("this.opcionReporte "+this.opcionReporte);
		
		if(nivelPerfil==1)
			banderaCecoPadre=1;
		
		logger.info("banderaCecoPadre... "+banderaCecoPadre);		
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("porcentajeG... "+porcentajeG);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("nombreCeco... "+nombreCeco);		
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		
		this.nombreCecoAux=nombreCeco.split("/")[0];
		logger.info("nombreCecoAux "+ this.nombreCecoAux);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
		
			List<ChecklistDTO> listaChecklist = null;
			List<VistaCDTO> listaSucursales = null;
			List<List> listaReporteCum= new ArrayList<List>();
			int porcentaje = 0, conteo=0;
			
			Map<String, Object> map= new HashMap<String, Object>();
			List<List> listRetorno= new ArrayList<List>();
			String fechaReporte=""+seleccionMes+"/"+seleccionAno;
			logger.info("datos antes del mapa... ");
			
			try {
				map=reporteRegBI.ReporteVistaCumplimiento(idUsuario,null, seleccionPais, seleccionCanal,fechaReporte,banderaCecoPadre);
				logger.info("map... " + map);			
			} catch (Exception e) {
				logger.info("Ocurrio algo... ");
			}
			
			if(map!=null){
				porcentaje= (Integer) map.get("porcentaje");
				logger.info("porcentaje... "+porcentaje);
				listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
				//listaChecklist=new ArrayList<ChecklistDTO>();
				logger.info("listaChecklist... "+listaChecklist);
				
				listaReporteCum= (List<List>) map.get("listaSucursales");
				logger.info("listaSucursales... "+listaReporteCum);
				logger.info("listaSucursales.size()... "+listaReporteCum.size());
				conteo = (Integer) map.get("conteo");
				logger.info("conteo... "+conteo);
							
				List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
				
				if(conteo==0)
					conteo++;
				
				for (int i = 0; i < listaReporteCum.size(); i++) {			
					listaAux.add((VistaCDTO) listaReporteCum.get(i));
		
					if((i+1)%conteo==0){
						listRetorno.add(listaAux);				
						listaAux=new ArrayList<VistaCDTO>();
					}
				}
				logger.info("listRetorno.size() "+listRetorno.size());
				logger.info("listRetorno "+listRetorno);
				if(listRetorno.size()>0)
					logger.info("listRetorno 0 size() "+listRetorno.get(0).size());
				
				if(idUsuario>0){
					mv = new ModelAndView("vistaFiltroCumplimientoVisitasDes");
					
					mv.addObject("opcionReporte", this.opcionReporte);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("porcentajeG", porcentajeG);
					mv.addObject("porcentaje", porcentaje);
					
					mv.addObject("idCecoPadre", idCecoPadre);
					mv.addObject("nombreCeco", this.nombreCecoAux);
					mv.addObject("banderaCecoPadre", 0);
					
					mv.addObject("seleccionAno", seleccionAno);
					mv.addObject("seleccionMes", seleccionMes);
					mv.addObject("seleccionCanal", seleccionCanal);
					mv.addObject("seleccionPais", seleccionPais);
					
					mv.addObject("conteo", conteo);
					mv.addObject("listaChecklist", listaChecklist);
					mv.addObject("listaSucursales", listRetorno);
					mv.addObject("url", FRQConstantes.getURLServer());
				}else{
					logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitas http404...");
					mv = new ModelAndView("http404");
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitas redirect...");
				mv = new ModelAndView("redirect:vistaCumplimientoVisitas.htm");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitas...");
		return mv;
	}
	
	
	/*CONTROLLER VISTA DETALLE NUEVO CONTROLLER*/
	//-DESARROLLO-central/vistaFiltroCumplimientoVisitasReg.htm?idUsuario=189870&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2017&seleccionMes=7&seleccionCanal=1&seleccionPais=21
	//-PRODUCCION-central/vistaFiltroCumplimientoVisitasReg.htm?idUsuario=147247&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2017&seleccionMes=7&seleccionCanal=1&seleccionPais=21

	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	@RequestMapping(value = "central/vistaFiltroCumplimientoVisitasReg.htm", method = RequestMethod.GET)
	public ModelAndView vistaFiltroCumplimientoVisitasReg(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idUsuario", required = true, defaultValue = "") String idUser,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
			@RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,		
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,		
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,			
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaFiltroCumplimientoVisitas...");
		
		logger.info("this.opcionReporte "+this.opcionReporte);
		
		if(nivelPerfil==1)
			banderaCecoPadre=1;
		
		logger.info("banderaCecoPadre... "+banderaCecoPadre);		
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("porcentajeG... "+porcentajeG);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("nombreCeco... "+nombreCeco);		
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		
		this.nombreCecoAux=nombreCeco.split("/")[0];
		logger.info("nombreCecoAux "+ this.nombreCecoAux);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
			
		
			List<ChecklistDTO> listaChecklist = null;
			List<VistaCDTO> listaSucursales = null;
			List<List> listaReporteCum= new ArrayList<List>();
			int porcentaje = 0, conteo=0;
			
			Map<String, Object> map= new HashMap<String, Object>();
			List<List> listRetorno= new ArrayList<List>();
			logger.info("datos antes del mapa... ");
			
			try {
				Date dtf = new Date();
				String date = new SimpleDateFormat("dd-MM-yyyy").format(dtf).toString(); // 18/07/2017
				String dia = date.substring(0, 2);
				String mes = date.substring(3, 5);
				String ano = date.substring(6, 10);
				String fechaReporte=""+mes+"/"+ano;
	
				map=reporteRegBI.ReporteVistaCumplimiento(idUsuario,null, seleccionPais, seleccionCanal,fechaReporte,banderaCecoPadre);
				logger.info("map... " + map);			
			} catch (Exception e) {
				logger.info("Ocurrio algo... ");
			}
			
			if(map!=null){
				porcentaje= (Integer) map.get("porcentaje");
				logger.info("porcentaje... "+porcentaje);
				listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
				//listaChecklist=new ArrayList<ChecklistDTO>();
				logger.info("listaChecklist... "+listaChecklist);
				
				listaReporteCum= (List<List>) map.get("listaSucursales");
				logger.info("listaSucursales... "+listaReporteCum);
				logger.info("listaSucursales.size()... "+listaReporteCum.size());
				//conteo = (Integer) map.get("conteo");
				conteo=2;
				logger.info("conteo... "+conteo);
							
				List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
				
				if(conteo==0)
					conteo++;
				
				for (int i = 0; i < listaReporteCum.size(); i++) {		
					
					listaAux.add((VistaCDTO) listaReporteCum.get(i));
		
					if((i+1)%conteo==0){
						listRetorno.add(listaAux);				
						listaAux=new ArrayList<VistaCDTO>();
					}
				}
				//logger.info("listRetorno.size() "+listRetorno.size());
				logger.info("listRetorno "+listRetorno);
				
				List<VistaCDTO> listaAuxSuma=new ArrayList<VistaCDTO>();
				
				if(listRetorno.size()>0)
					logger.info("listRetorno 0 size() "+listRetorno.get(0).size());
				
				if(idUsuario>0){
					mv = new ModelAndView("vistaFiltroCumplimientoVisitasReg");
					
					mv.addObject("opcionReporte", this.opcionReporte);
				
					Date dtf = new Date();
					String date = new SimpleDateFormat("dd-MM-yyyy").format(dtf).toString(); // 18/07/2017
					String dia = date.substring(0, 2);
					String mes = date.substring(3, 5);
					String ano = date.substring(6, 10);
					
					int diasMes = new UtilDate().calculaDiasMes(Integer.parseInt(ano) , Integer.parseInt(mes));
					double porcentajeC = ((Integer.parseInt(dia)-1)*100)/diasMes;
	
					mv.addObject("porcentajeG", porcentajeC);
					mv.addObject("porcentaje", porcentaje);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("banderaCecoPadre", banderaCecoPadre);
					mv.addObject("seleccionAno", ano);
					mv.addObject("seleccionMes", mes);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("idCecoPadre", idCecoPadre);
					mv.addObject("nombreCeco", this.nombreCecoAux);
					mv.addObject("seleccionCanal", seleccionCanal);
					mv.addObject("seleccionPais", seleccionPais);
					mv.addObject("conteo", conteo);
					mv.addObject("listaChecklist", listaChecklist);
					mv.addObject("listaSucursales", listRetorno);
					mv.addObject("url", FRQConstantes.getURLServer());
	
				}else{
					logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitas http404...");
					mv = new ModelAndView("http404");
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitas redirect...");
				mv = new ModelAndView("redirect:vistaCumplimientoVisitas.htm");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaFiltroCumplimientoVisitas...");
		return mv;
	}
	
	
	/*CONTROLLER VISTA DETALLE NUEVO CONTROLLER*/
	//-DESARROLLO-central/vistaChecklist.htm?idUsuario=189870&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2018&seleccionMes=7&seleccionCanal=1&seleccionPais=21
	//-PRODUCCION-central/vistaChecklist.htm?idUsuario=147247&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2018&seleccionMes=3&seleccionCanal=1&seleccionPais=21
	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	@RequestMapping(value = "central/vistaChecklist.htm", method = RequestMethod.GET)
	public ModelAndView vistaChecklist(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idUsuario", required = true, defaultValue = "") String idUser,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
			@RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,		
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,		
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,			
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaChecklist...");
		
		logger.info("this.opcionReporte "+this.opcionReporte);
		
		if(nivelPerfil==1)
			banderaCecoPadre=1;
		
		logger.info("banderaCecoPadre... "+banderaCecoPadre);		
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("porcentajeG... "+porcentajeG);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("nombreCeco... "+nombreCeco);		
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		
		this.nombreCecoAux=nombreCeco.split("/")[0];
		logger.info("nombreCecoAux "+ this.nombreCecoAux);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
		
			List<ChecklistDTO> listaChecklist = null;
			List<VistaCDTO> listaSucursales = null;
			List<List> listaReporteCum= new ArrayList<List>();
			int porcentaje = 0, conteo=0;
			
			Map<String, Object> map= new HashMap<String, Object>();
			List<List> listRetorno= new ArrayList<List>();
			logger.info("datos antes del mapa... ");
			
			String fechaC = seleccionMes+"/"+seleccionAno;
			logger.info("fecha... " + fechaC);
			
			try {
				Date dtf = new Date();
				String date = new SimpleDateFormat("dd-MM-yyyy").format(dtf).toString(); // 18/07/2017
				String dia = date.substring(0, 2);
				String mes = date.substring(3, 5);
				String ano = date.substring(6, 10);
				String fechaReporte=""+mes+"/"+ano;
	
				map=reporteRegBI.ReporteVistaCumplimientoTres(idUsuario,null, seleccionPais, seleccionCanal,fechaReporte,banderaCecoPadre);
				logger.info("map... " + map);			
			} catch (Exception e) {
				logger.info("Ocurrio algo... "+e);
			}
			
			if(map!=null){
				porcentaje= (Integer) map.get("porcentaje");
				logger.info("porcentaje... "+porcentaje);
				listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
				//listaChecklist=new ArrayList<ChecklistDTO>();
				logger.info("listaChecklist... "+listaChecklist);
				
				listaReporteCum= (List<List>) map.get("listaSucursales");
				logger.info("listaSucursales... "+listaReporteCum);
				logger.info("listaSucursales.size()... "+listaReporteCum.size());
				conteo = (Integer) map.get("conteo");
				logger.info("conteo checks... "+conteo);
				conteo=3;
				logger.info("conteo... "+conteo);
							
				List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
				
				if(conteo==0)
					conteo++;
				
				for (int i = 0; i < listaReporteCum.size(); i++) {		
					
					listaAux.add((VistaCDTO) listaReporteCum.get(i));
		
					if((i+1)%conteo==0){
						listRetorno.add(listaAux);				
						listaAux=new ArrayList<VistaCDTO>();
					}
				}
				logger.info("listRetorno.size() "+listRetorno.size());
				logger.info("listRetorno "+listRetorno);
				
				double totalCaja=0.0;
				double totalPiso=0.0;
				double totalS=0.0;
				
				int proCaja=0;
				int proPiso=0;
				int proS=0;
				
				List<VistaCDTO> listaAuxSuma=new ArrayList<VistaCDTO>();
				
				if(listRetorno.size()>0){
					logger.info("listRetorno 0 size() "+listRetorno.get(0).size());
					for (int k = 0; k < listRetorno.size(); k++) {
						listaAuxSuma = listRetorno.get(k);
						totalCaja = totalCaja + listaAuxSuma.get(0).getTotal();
						totalPiso = totalPiso + listaAuxSuma.get(1).getTotal();
						//totalS = totalS + listaAuxSuma.get(2).getTotal();
					}
					proCaja = (int) Math.round(totalCaja/listRetorno.size());
					proPiso = (int) Math.round(totalPiso/listRetorno.size());
					//proS = (int) Math.round(totalS/listRetorno.size());
				}else{
					proCaja=0;
					proPiso=0;
					proS=0;
				}
				
				if(idUsuario>0){
					mv = new ModelAndView("vistaChecklist");
					
					mv.addObject("opcionReporte", this.opcionReporte);
				
					Date dtf = new Date();
					String date = new SimpleDateFormat("dd-MM-yyyy").format(dtf).toString(); // 18/07/2017
					String dia = date.substring(0, 2);
					String mes = date.substring(3, 5);
					String ano = date.substring(6, 10);
					
					int diasMes = new UtilDate().calculaDiasMes(Integer.parseInt(ano) , Integer.parseInt(mes));
					double porcentajeC = ((Integer.parseInt(dia)-1)*100)/diasMes;
	
					mv.addObject("porcentajeG", porcentajeC);
					mv.addObject("porcentaje", porcentaje);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("banderaCecoPadre", banderaCecoPadre);
					mv.addObject("seleccionAno", ano);
					mv.addObject("seleccionMes", mes);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("idCecoPadre", idCecoPadre);
					mv.addObject("nombreCeco", this.nombreCecoAux);
					mv.addObject("seleccionCanal", seleccionCanal);
					mv.addObject("seleccionPais", seleccionPais);
					mv.addObject("conteo", conteo);
					mv.addObject("listaChecklist", listaChecklist);
					mv.addObject("listaSucursales", listRetorno);
					mv.addObject("url", FRQConstantes.getURLServer());
					
					mv.addObject("proCaja",proCaja);
					mv.addObject("proPiso",proPiso);
					mv.addObject("proS",proS);
	
				}else{
					logger.info("...SALE DEL CONTROLLER vistaChecklist http404...");
					mv = new ModelAndView("http404");
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaChecklist redirect...");
				mv = new ModelAndView("redirect:vistaCumplimientoVisitas.htm");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaChecklist...");
		return mv;
	}
	
	// /checklist/central/vistaChecklistFecha.htm?idUsuario=147247&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=100&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2018&seleccionMes=01&seleccionCanal=1&seleccionPais=21
	//-DESARROLLO-central/vistaChecklistFecha.htm?idUsuario=189870&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2018&seleccionMes=3&seleccionCanal=1&seleccionPais=21
	//-PRODUCCION-central/vistaChecklistFecha.htm?idUsuario=147247&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2018&seleccionMes=3&seleccionCanal=1&seleccionPais=21
	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	@RequestMapping(value = "central/vistaChecklistFecha.htm", method = RequestMethod.GET)
	public ModelAndView vistaChecklistFecha(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idUsuario", required = true, defaultValue = "") String idUser,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
			@RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,		
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,		
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,			
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaChecklist...");
		
		logger.info("this.opcionReporte "+this.opcionReporte);
		
		if(nivelPerfil==1)
			banderaCecoPadre=1;
		
		logger.info("banderaCecoPadre... "+banderaCecoPadre);		
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("porcentajeG... "+porcentajeG);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("nombreCeco... "+nombreCeco);		
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		
		this.nombreCecoAux=nombreCeco.split("/")[0];
		logger.info("nombreCecoAux "+ this.nombreCecoAux);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
		
			List<ChecklistDTO> listaChecklist = null;
			List<VistaCDTO> listaSucursales = null;
			List<List> listaReporteCum= new ArrayList<List>();
			int porcentaje = 0, conteo=0;
			
			Map<String, Object> map= new HashMap<String, Object>();
			List<List> listRetorno= new ArrayList<List>();
			logger.info("datos antes del mapa... ");
			
			String fechaC = seleccionMes+"/"+seleccionAno;
			logger.info("fecha... " + fechaC);
			
			try {
				map=reporteRegBI.ReporteVistaCumplimientoTres(idUsuario,null, seleccionPais, seleccionCanal,fechaC,banderaCecoPadre);
				logger.info("map... " + map);			
			} catch (Exception e) {
				logger.info("Ocurrio algo... "+e);
			}
			
			if(map!=null){
				porcentaje= (Integer) map.get("porcentaje");
				logger.info("porcentaje... "+porcentaje);
				listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
				//listaChecklist=new ArrayList<ChecklistDTO>();
				logger.info("listaChecklist... "+listaChecklist);
				
				listaReporteCum= (List<List>) map.get("listaSucursales");
				logger.info("listaSucursales... "+listaReporteCum);
				logger.info("listaSucursales.size()... "+listaReporteCum.size());
				conteo = (Integer) map.get("conteo");
				logger.info("conteo checks... "+conteo);
				conteo=3;
				logger.info("conteo... "+conteo);
							
				List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
				
				if(conteo==0)
					conteo++;
				
				for (int i = 0; i < listaReporteCum.size(); i++) {		
					
					listaAux.add((VistaCDTO) listaReporteCum.get(i));
		
					if((i+1)%conteo==0){
						listRetorno.add(listaAux);				
						listaAux=new ArrayList<VistaCDTO>();
					}
				}
				logger.info("listRetorno.size() "+listRetorno.size());
				logger.info("listRetorno "+listRetorno);
				
				double totalCaja=0.0;
				double totalPiso=0.0;
				double totalS=0.0;
				
				int proCaja=0;
				int proPiso=0;
				int proS=0;
				
				List<VistaCDTO> listaAuxSuma=new ArrayList<VistaCDTO>();
				
				if(listRetorno.size()>0){
					logger.info("listRetorno 0 size() "+listRetorno.get(0).size());
					for (int k = 0; k < listRetorno.size(); k++) {
						listaAuxSuma = listRetorno.get(k);
						totalCaja = totalCaja + listaAuxSuma.get(0).getTotal();
						totalPiso = totalPiso + listaAuxSuma.get(1).getTotal();
						//totalS = totalS + listaAuxSuma.get(2).getTotal();
					}
					proCaja = (int) Math.round(totalCaja/listRetorno.size());
					proPiso = (int) Math.round(totalPiso/listRetorno.size());
					//proS = (int) Math.round(totalS/listRetorno.size());
				}else{
					proCaja=0;
					proPiso=0;
					proS=0;
				}
				
				if(idUsuario>0){
					mv = new ModelAndView("vistaChecklistFecha");
					
					mv.addObject("opcionReporte", this.opcionReporte);
				
					Date dtf = new Date();
					String date = new SimpleDateFormat("dd-MM-yyyy").format(dtf).toString(); // 18/07/2017
					String dia = date.substring(0, 2);
					String mes = date.substring(3, 5);
					String ano = date.substring(6, 10);
					
					int diasMes = new UtilDate().calculaDiasMes(Integer.parseInt(ano) , Integer.parseInt(mes));
					double porcentajeC = ((Integer.parseInt(dia)-1)*100)/diasMes;
	
					mv.addObject("porcentajeG", porcentajeG);
					mv.addObject("porcentaje", porcentaje);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("banderaCecoPadre", banderaCecoPadre);
					mv.addObject("seleccionAno", seleccionAno);
					mv.addObject("seleccionMes", seleccionMes);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("idCecoPadre", idCecoPadre);
					mv.addObject("nombreCeco", this.nombreCecoAux);
					mv.addObject("seleccionCanal", seleccionCanal);
					mv.addObject("seleccionPais", seleccionPais);
					mv.addObject("conteo", conteo);
					mv.addObject("listaChecklist", listaChecklist);
					mv.addObject("listaSucursales", listRetorno);
					mv.addObject("url", FRQConstantes.getURLServer());
					
					mv.addObject("proCaja",proCaja);
					mv.addObject("proPiso",proPiso);
					mv.addObject("proS",proS);
	
				}else{
					logger.info("...SALE DEL CONTROLLER vistaChecklist http404...");
					mv = new ModelAndView("http404");
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaChecklist redirect...");
				mv = new ModelAndView("redirect:vistaCumplimientoVisitas.htm");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaChecklist...");
		return mv;
	}
	
	
	
	/*CONTROLLER VISTA DETALLE NUEVO CONTROLLER*/
	//-DESARROLLO-central/vistaTiempoReal.htm?idUsuario=189870&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2017&seleccionMes=7&seleccionCanal=1&seleccionPais=21
	//-PRODUCCION-central/vistaTiempoReal.htm?idUsuario=147247&banderaCecoPadre=0&nivelPerfil=0&porcentajeG=19&idCecoPadre=230001&nombreCeco=MEXICO&seleccionAno=2017&seleccionMes=7&seleccionCanal=1&seleccionPais=21
	
	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	@RequestMapping(value = "central/vistaTiempoReal.htm", method = RequestMethod.GET)
	public ModelAndView vistaTiempoReal(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idUsuario", required = true, defaultValue = "") String idUser,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
			@RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,		
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,		
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,			
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaTiempoReal...");
		
		logger.info("this.opcionReporte "+this.opcionReporte);
		
		if(nivelPerfil==1)
			banderaCecoPadre=1;
		
		logger.info("banderaCecoPadre... "+banderaCecoPadre);		
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("porcentajeG... "+porcentajeG);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("nombreCeco... "+nombreCeco);		
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		
		this.nombreCecoAux=nombreCeco.split("/")[0];
		logger.info("nombreCecoAux "+ this.nombreCecoAux);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
			
		
			List<ChecklistDTO> listaChecklist = null;
			List<VistaCDTO> listaSucursales = null;
			List<List> listaReporteCum= new ArrayList<List>();
			int porcentaje = 0, conteo=0;
			
			Map<String, Object> map= new HashMap<String, Object>();
			List<List> listRetorno= new ArrayList<List>();
			logger.info("datos antes del mapa... ");
			
			try {
				Date dtf = new Date();
				String date = new SimpleDateFormat("dd-MM-yyyy").format(dtf).toString(); // 18/07/2017
				String dia = date.substring(0, 2);
				String mes = date.substring(3, 5);
				String ano = date.substring(6, 10);
				String fechaReporte=""+mes+"/"+ano;
	
				map=reporteRegBI.ReporteVistaCumplimientoDos(idUsuario,null, seleccionPais, seleccionCanal,fechaReporte,banderaCecoPadre);
				logger.info("map... " + map);			
			} catch (Exception e) {
				logger.info("Ocurrio algo... ");
			}
			
			if(map!=null){
				porcentaje= (Integer) map.get("porcentaje");
				logger.info("porcentaje... "+porcentaje);
				listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
				//listaChecklist=new ArrayList<ChecklistDTO>();
				logger.info("listaChecklist... "+listaChecklist);
				
				listaReporteCum= (List<List>) map.get("listaSucursales");
				logger.info("listaSucursales... "+listaReporteCum);
				logger.info("listaSucursales.size()... "+listaReporteCum.size());
				conteo = (Integer) map.get("conteo");
				logger.info("conteo checks... "+conteo);
				conteo=2;
				logger.info("conteo... "+conteo);
							
				List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
				
				if(conteo==0)
					conteo++;
				
				for (int i = 0; i < listaReporteCum.size(); i++) {		
					
					listaAux.add((VistaCDTO) listaReporteCum.get(i));
		
					if((i+1)%conteo==0){
						listRetorno.add(listaAux);				
						listaAux=new ArrayList<VistaCDTO>();
					}
				}
				logger.info("listRetorno.size() "+listRetorno.size());
				logger.info("listRetorno "+listRetorno);
				
				if(listRetorno.size()>0)
					logger.info("listRetorno 0 size() "+listRetorno.get(0).size());
				
				if(idUsuario>0){
					mv = new ModelAndView("vistaTiempoReal");
					
					mv.addObject("opcionReporte", this.opcionReporte);
				
					Date dtf = new Date();
					String date = new SimpleDateFormat("dd-MM-yyyy").format(dtf).toString(); // 18/07/2017
					String dia = date.substring(0, 2);
					String mes = date.substring(3, 5);
					String ano = date.substring(6, 10);
					
					int diasMes = new UtilDate().calculaDiasMes(Integer.parseInt(ano) , Integer.parseInt(mes));
					double porcentajeC = ((Integer.parseInt(dia)-1)*100)/diasMes;
	
					mv.addObject("porcentajeG", porcentajeC);
					mv.addObject("porcentaje", porcentaje);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("banderaCecoPadre", banderaCecoPadre);
					mv.addObject("seleccionAno", ano);
					mv.addObject("seleccionMes", mes);
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("idCecoPadre", idCecoPadre);
					mv.addObject("nombreCeco", this.nombreCecoAux);
					mv.addObject("seleccionCanal", seleccionCanal);
					mv.addObject("seleccionPais", seleccionPais);
					mv.addObject("conteo", conteo);
					mv.addObject("listaChecklist", listaChecklist);
					mv.addObject("listaSucursales", listRetorno);
					mv.addObject("url", FRQConstantes.getURLServer());
	
				}else{
					logger.info("...SALE DEL CONTROLLER vistaTiempoReal http404...");
					mv = new ModelAndView("http404");
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaTiempoReal redirect...");
				mv = new ModelAndView("redirect:vistaCumplimientoVisitas.htm");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaTiempoReal...");
		return mv;
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "central/vistaTerritCumplimientoVisitas.htm", method = RequestMethod.POST)
	public ModelAndView vistaTerritCumplimientoVisitas(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "porcentajeG", required = true, defaultValue = "0") int porcentajeG,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "0") int idCecoPadre,
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,
			
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais) throws Exception{
			
		logger.info("...ENTRO AL CONTROLLER vistaTerritCumplimientoVisitas...");
		
		logger.info("this.opcionReporte "+this.opcionReporte);
		logger.info("porcentajeG... "+porcentajeG);
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("idCeco... "+idCeco);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("nombreCeco... "+nombreCeco);
		logger.info("banderaCecoPadre... "+banderaCecoPadre);
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		
		String nombreAux []=nombreCeco.split(",");
		int nombreAuxSize= nombreAux.length;		
		this.nombreCecoAux=nombreAux[nombreAuxSize-1];
		logger.info("nombreCecoAux "+ nombreCecoAux);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);

			List<ChecklistDTO> listaChecklist = null;
			List<List> listaReporteCum= new ArrayList<List>();
			int porcentaje = 0, conteo=0;
			
			Map<String, Object> map= new HashMap<String, Object>();
			String fechaReporte=""+seleccionMes+"/"+seleccionAno;
			logger.info("datos antes del mapa... ");

			try {
				if(nivelPerfil==0 || nivelPerfil==1)
					map=reporteBI.ReporteVistaCumplimientoSucursal(idUsuario,idCeco, seleccionPais, seleccionCanal,fechaReporte,banderaCecoPadre);
				else
					map=reporteBI.ReporteVistaCumplimiento(idUsuario , null, seleccionPais, seleccionCanal,fechaReporte,banderaCecoPadre);
				logger.info("map... " + map);
			} catch (Exception e) {
				logger.info("Ocurrio algo... ");
			}
			
			if(map!=null){
				porcentaje= (Integer) map.get("porcentaje");
				logger.info("porcentaje... "+porcentaje);
				listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
				//listaChecklist=new ArrayList<ChecklistDTO>();
				logger.info("listaChecklist... "+listaChecklist);
				listaReporteCum= (List<List>) map.get("listaSucursales");
				logger.info("listaSucursales... "+listaReporteCum);
				conteo = (Integer) map.get("conteo");
				logger.info("conteo... "+conteo);
		
				if(conteo==0)
					conteo++;
					
				if(idUsuario>0){
					mv = new ModelAndView("vistaTerritCumplimientoVisitas");
					
					mv.addObject("opcionReporte", this.opcionReporte);
					
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("porcentajeG", porcentajeG);
					mv.addObject("idCeco", idCeco);
					mv.addObject("idCecoPadre", idCecoPadre);
					mv.addObject("nombreCeco", this.nombreCecoAux);
					mv.addObject("banderaCecoPadre", banderaCecoPadre);
					mv.addObject("seleccionAno", seleccionAno);
					mv.addObject("seleccionMes", seleccionMes);
					mv.addObject("seleccionCanal", seleccionCanal);
					mv.addObject("seleccionPais", seleccionPais);
					
					mv.addObject("porcentaje", porcentaje);
					mv.addObject("conteo", conteo);
					mv.addObject("listaChecklist", listaChecklist);
					mv.addObject("listaSucursales", listaReporteCum);
					mv.addObject("listaSucursalesSize", listaReporteCum.size());
				}else{
					logger.info("...SALE DEL CONTROLLER vistaTerritCumplimientoVisitas...");
					mv = new ModelAndView("http404");
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaTerritCumplimientoVisitas...");
				mv = new ModelAndView("redirect:vistaCumplimientoVisitas.htm");
			}
		}
		
		
		return mv;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "central/vistaDetalleCumplimientoVisitas.htm", method = RequestMethod.POST)
	public ModelAndView vistaDetalleCumplimientoVisitas(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "porcentajeG", required = true, defaultValue = "") int porcentajeG,
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "") int idCecoPadre,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") int idCeco,
			@RequestParam(value = "idCecoHijo", required = true, defaultValue = "") int idCecoHijo,
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "") String nombreCeco,			
			@RequestParam(value = "idCheck", required = true, defaultValue = "") int idCheck,
			@RequestParam(value = "nivelPerfil", required = true, defaultValue = "0") int nivelPerfil,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "") int banderaCecoPadre,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "") int seleccionPais) throws Exception{

		logger.info("...ENTRO AL CONTROLLER vistaDetalleCumplimientoVisitas...");
		
		logger.info("this.opcionReporte "+this.opcionReporte);
		logger.info("porcentajeG... "+porcentajeG);
		logger.info("idCecoPadre... "+idCecoPadre);
		logger.info("idCeco... "+idCeco);
		logger.info("idCecoHijo... "+idCecoHijo);
		logger.info("nombreCeco... "+nombreCeco);		
		logger.info("idCheck... "+idCheck);
		logger.info("nivelPerfil... "+nivelPerfil);
		logger.info("banderaCecoPadre... "+banderaCecoPadre);
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaCumplimientoVisitas.htm");
		}else{
			
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
		
			List <ChecklistDTO> listaChecklist = null;
			List <EvidenciaDTO> listaEvidencias = null;
			List <PreguntaDTO> listaFolios = null;
			List<CompromisoDTO> preguntas = null;
			String json = "";
			int totalE = 0;
			int totalF = 0;
			int idBit = 0;
			
	
			Map<String, Object> map= new HashMap<String, Object>();
			String fechaC = seleccionMes+"/"+seleccionAno;
			logger.info("fecha... " + fechaC);
			//String fechaReporte=""+seleccionMes+"/"+seleccionAno;
			logger.info("datos antes del mapa... ");
			
			try {
				logger.info("ID CECO " + idCecoHijo);
				logger.info("ID CHECK " + idCheck);
				logger.info("ID USUARIO " + idUsuario);
				logger.info("ID FECHA " + fechaC);
	
				map=reporteBI.ReporteDetVC(idCecoHijo, idCheck, idUsuario, fechaC);
	
				listaChecklist = (List<ChecklistDTO>) map.get("listaChecklist");
				listaEvidencias = (List<EvidenciaDTO>) map.get("listaEvidencias");
				listaFolios = (List<PreguntaDTO>) map.get("listaFolios");
				totalE = (Integer) map.get("totalEvidencias");
				totalF = (Integer) map.get("totalFolios");
				idBit =  (Integer) map.get("idBitacora");
	
				
				//System.out.println("***************ID BITACORA***************" + idBit);
				preguntas = reporteBI.ReporteDetPreguntasRegional(idBit);
	
				json = new Gson().toJson(preguntas);
				//System.out.println("***************JSON***************" + json);
	
				
				//map=reporteBI.ReporteDetVC(idCecoHijo, idCheck, idUsuario, fechaC);
				map=this.mapGlobal;
				logger.info("map... " + map);
				
				if(map!=null){
					listaChecklist = (List<ChecklistDTO>) map.get("listaChecklist");
					listaEvidencias = (List<EvidenciaDTO>) map.get("listaEvidencias");
					listaFolios = (List<PreguntaDTO>) map.get("listaFolios");
					totalE = (Integer) map.get("totalEvidencias");
					totalF = (Integer) map.get("totalFolios");
					idBit =  (Integer) map.get("idBitacora");
					
					preguntas = reporteBI.ReporteDetPreguntasRegional(idBit);
					json = new Gson().toJson(preguntas);
				}			
			} catch (Exception e) {
				logger.info("Ocurrio algo... ");
			}
			String rutaImagen;
			if(this.opcionReporte)
				rutaImagen= FRQConstantes.getRutaImagenMovil();
			else
				rutaImagen= FRQConstantes.getRutaImagen();
			logger.info("rutaImagen "+rutaImagen);
			
			if(map!=null){
				if(Integer.parseInt(userSession.getIdUsuario())>0){
					mv = new ModelAndView("vistaDetalleCumplimientoVisitas");
					
					mv.addObject("opcionReporte", this.opcionReporte);
					
					mv.addObject("listaCheck", listaChecklist);
					mv.addObject("listaEvidencias", listaEvidencias);
					mv.addObject("listaFolios", listaFolios);
					mv.addObject("totalE", totalE);
					mv.addObject("totalF", totalF);
					mv.addObject("listaPreguntas", preguntas);
					mv.addObject("json", json);
					mv.addObject("rutaHostImagen", rutaImagen);
					
					mv.addObject("porcentajeG", porcentajeG);
					mv.addObject("idCecoPadre", idCecoPadre);
					mv.addObject("idCeco", idCeco);
					mv.addObject("nombreCeco", nombreCeco);				
					mv.addObject("nivelPerfil", nivelPerfil);
					mv.addObject("banderaCecoPadre", banderaCecoPadre);
					mv.addObject("seleccionAno", seleccionAno);
					mv.addObject("seleccionMes", seleccionMes);
					mv.addObject("seleccionCanal", seleccionCanal);
					mv.addObject("seleccionPais", seleccionPais);
					
					if(listaChecklist!=null)
						logger.info(listaChecklist.size());
					if(listaFolios!=null)
						logger.info(listaFolios.size());
					if(listaEvidencias!=null)
						logger.info(listaEvidencias.size());
					logger.info(totalE);
					logger.info(totalF);
					if(preguntas!=null)
						logger.info(preguntas.size());
				}else{
					logger.info("...SALE DEL CONTROLLER vistaDetalleCumplimientoVisitas...");
					mv = new ModelAndView("http404");
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaDetalleCumplimientoVisitas...");
				//mv = new ModelAndView("redirect:vistaCumplimientoVisitas.htm");
				mv=null;
			}
		}
		return mv;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/ajaxFiltroSucursal", method = RequestMethod.GET)
	public @ResponseBody List<List> ajaxFiltroSucursal(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais,
			@RequestParam(value = "valorPila", required = true, defaultValue = "0") int valorPila, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxFiltroSucursal...");
		
		logger.info("idCeco... "+idCeco);
		logger.info("banderaCecoPadre... "+banderaCecoPadre);
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		logger.info("valorPila... "+valorPila);
		
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());
		logger.info("USUARIO "+ idUsuario);
		
		List<ChecklistDTO> listaChecklist = null;
		List<List> listaReporteCum= new ArrayList<List>();
		List<VistaCDTO> listaReporteCumPila= new ArrayList<VistaCDTO>();
		
		int porcentaje = 0, conteo=0;
		Map<String, Object> map= new HashMap<String, Object>();
		Map<String, Object> mapPila= new HashMap<String, Object>();
		List<List> listRetorno= new ArrayList<List>();
		String fechaReporte=""+seleccionMes+"/"+seleccionAno;
		logger.info("datos antes de los mapas... ");
		
		try {
			map=reporteBI.ReporteVistaCumplimientoSucursal(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,0);
			logger.info("map... " + map);
			if(valorPila==1){
				mapPila=reporteBI.ReporteVistaCumplimientoSucursal(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,2);
				logger.info("mapPila... " + mapPila);			
			}
				
		} catch (Exception e) {
			logger.info("Ocurrio algo... ");
		}
		if(map!=null){
			porcentaje= (Integer) map.get("porcentaje");
			logger.info("porcentaje... "+porcentaje);
			listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
			logger.info("listaChecklist... "+listaChecklist);
			listRetorno.add(listaChecklist);
			
			listaReporteCum= (List<List>) map.get("listaSucursales");
			logger.info("listaSucursales... "+listaReporteCum);
			conteo = (Integer) map.get("conteo");
			logger.info("conteo... "+conteo);
		}			
		
		if(valorPila==1){
			if(mapPila!=null){
				listaReporteCumPila= (List<VistaCDTO>) mapPila.get("listaSucursales");
				logger.info("listaReporteCumPila... "+listaReporteCumPila);
				
				// NUEVA VALIDACION
				if(listaReporteCumPila.size()>1){					
					boolean flag = false;
					int l =1;
					int sumaAnt = 0;
					int sumaAct = 0;
					VistaCDTO act = null;
					VistaCDTO ant = null;
					while(l < listaReporteCumPila.size()){	
						sumaAnt= listaReporteCumPila.get(l - 1).getAsignados()+listaReporteCumPila.get(l - 1).getTerminados();
						sumaAct= listaReporteCumPila.get(l).getAsignados()+listaReporteCumPila.get(l).getTerminados();						
						if(sumaAnt < sumaAct){
							ant=listaReporteCumPila.get(l - 1);
							act=listaReporteCumPila.get(l);							
							listaReporteCumPila.set(l - 1, act);
							listaReporteCumPila.set(l, ant);
							flag=true;
						}
						if (flag && (l + 1) == listaReporteCumPila.size()) {
							l = 0;
							flag = false;
						}
						l++;
					}				
				}
				// NUEVA VALIDACION				
			}			
			for (int i = 0; i < listaReporteCumPila.size(); i++) {
				VistaCDTO listaVista = new VistaCDTO();
				listaVista=(VistaCDTO) listaReporteCum.get(i);
				
				VistaCDTO listaVistaPila = new VistaCDTO();
				listaVistaPila=(VistaCDTO) listaReporteCumPila.get(i);

				listaVista.setAsignados(listaVistaPila.getAsignados());
				listaVista.setTerminados(listaVistaPila.getTerminados());
			}
		}
		
		if(conteo==0)
			conteo++;
		
		List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
		
		logger.info("listaReporteCum.size() "+ listaReporteCum.size());
		for (int i = 0; i < listaReporteCum.size(); i++) {							
			listaAux.add((VistaCDTO) listaReporteCum.get(i));			
			if((i+1)%conteo==0 ){
				listRetorno.add(listaAux);				
				listaAux=new ArrayList<VistaCDTO>();
			}
		}
		
		logger.info("listRetorno.size() "+listRetorno.size());
		logger.info("listRetorno "+listRetorno);

		logger.info("...SALE DEL AJAX ajaxFiltroSucursal...");
		return listRetorno;
	}
	
	/*::::::::::::::::::::AJAX FILTRO SUCURSAL PILA::::::::::::::::::::*/
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/ajaxFiltroSucursalPila", method = RequestMethod.GET)
	public @ResponseBody List<List> ajaxFiltroSucursalPila(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais,
			@RequestParam(value = "valorPila", required = true, defaultValue = "0") int valorPila, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxFiltroSucursal...");
		
		logger.info("idCeco... "+idCeco);
		logger.info("banderaCecoPadre... "+banderaCecoPadre);
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		logger.info("valorPila... "+valorPila);
		
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());
		logger.info("USUARIO "+ idUsuario);
		
		List<ChecklistDTO> listaChecklist = null;
		List<List> listaReporteCum= new ArrayList<List>();
		List<VistaCDTO> listaReporteCumPila= new ArrayList<VistaCDTO>();
		
		int porcentaje = 0, conteo=0;
		Map<String, Object> map= new HashMap<String, Object>();
		Map<String, Object> mapPila= new HashMap<String, Object>();
		List<List> listRetorno= new ArrayList<List>();
		String fechaReporte=""+seleccionMes+"/"+seleccionAno;
		logger.info("datos antes de los mapas... ");
		
		try {
			map=reporteBI.ReporteVistaCumplimientoSucursalPila(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,0);
			logger.info("map... " + map);
			if(valorPila==1){
				mapPila=reporteBI.ReporteVistaCumplimientoSucursalPila(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,2);
				logger.info("mapPila... " + mapPila);
			}
				
		} catch (Exception e) {
			logger.info("Ocurrio algo... ");
		}
		if(map!=null){
			porcentaje= (Integer) map.get("porcentaje");
			logger.info("porcentaje... "+porcentaje);
			listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
			logger.info("listaChecklist... "+listaChecklist);
			listRetorno.add(listaChecklist);
			
			listaReporteCum= (List<List>) map.get("listaSucursales");
			logger.info("listaSucursales... "+listaReporteCum);
			conteo = (Integer) map.get("conteo");
			logger.info("conteo... "+conteo);
		}			
		
		if(valorPila==1){
			if(mapPila!=null){
				listaReporteCumPila= (List<VistaCDTO>) mapPila.get("listaSucursales");
				logger.info("listaReporteCumPila... "+listaReporteCumPila);
				
				// NUEVA VALIDACION
				if(listaReporteCumPila.size()>1){					
					boolean flag = false;
					int l =1;
					int sumaAnt = 0;
					int sumaAct = 0;
					VistaCDTO act = null;
					VistaCDTO ant = null;
					while(l < listaReporteCumPila.size()){	
						sumaAnt= listaReporteCumPila.get(l - 1).getAsignados()+listaReporteCumPila.get(l - 1).getTerminados();
						sumaAct= listaReporteCumPila.get(l).getAsignados()+listaReporteCumPila.get(l).getTerminados();						
						if(sumaAnt < sumaAct){
							ant=listaReporteCumPila.get(l - 1);
							act=listaReporteCumPila.get(l);							
							listaReporteCumPila.set(l - 1, act);
							listaReporteCumPila.set(l, ant);
							flag=true;
						}
						if (flag && (l + 1) == listaReporteCumPila.size()) {
							l = 0;
							flag = false;
						}
						l++;
					}				
				}
				// NUEVA VALIDACION				
			}			
			for (int i = 0; i < listaReporteCumPila.size(); i++) {
				VistaCDTO listaVista = new VistaCDTO();
				listaVista=(VistaCDTO) listaReporteCum.get(i);
				
				VistaCDTO listaVistaPila = new VistaCDTO();
				listaVistaPila=(VistaCDTO) listaReporteCumPila.get(i);

				listaVista.setAsignados(listaVistaPila.getAsignados());
				listaVista.setTerminados(listaVistaPila.getTerminados());
			}
		}
		
		if(conteo==0)
			conteo++;
		
		List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
		
		logger.info("listaReporteCum.size() "+ listaReporteCum.size());
		for (int i = 0; i < listaReporteCum.size(); i++) {							
			listaAux.add((VistaCDTO) listaReporteCum.get(i));			
			if((i+1)%conteo==0 ){
				listRetorno.add(listaAux);				
				listaAux=new ArrayList<VistaCDTO>();
			}
		}
		
		logger.info("listRetorno.size() "+listRetorno.size());
		logger.info("listRetorno "+listRetorno);

		logger.info("...SALE DEL AJAX ajaxFiltroSucursal...");
		return listRetorno;
	}
	/*::::::::::::::::::::FIN AJAX FILTRO SUCURSAL PILA::::::::::::::::::::*/

	/*::::::::::::::::::::AJAX FILTRO SUCURSAL POR CAJA::::::::::::::::::::*/
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/ajaxFiltroSucursalCaja", method = RequestMethod.GET)
	public @ResponseBody List<List> ajaxFiltroSucursalCaja(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais,
			@RequestParam(value = "valorPila", required = true, defaultValue = "0") int valorPila, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxFiltroSucursal CAJA!!!!!...");
		
		logger.info("idCeco... "+idCeco);
		logger.info("banderaCecoPadre... "+banderaCecoPadre);
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);
		logger.info("valorPila... "+valorPila);
		
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());
		logger.info("USUARIO "+ idUsuario);
		
		List<ChecklistDTO> listaChecklist = null;
		List<List> listaReporteCum= new ArrayList<List>();
		List<VistaCDTO> listaReporteCumPila= new ArrayList<VistaCDTO>();
		
		int porcentaje = 0, conteo=0;
		Map<String, Object> map= new HashMap<String, Object>();
		Map<String, Object> mapPila= new HashMap<String, Object>();
		List<List> listRetorno= new ArrayList<List>();
		logger.info("datos antes de los mapas... ");
		
		try {
			/*Date dtf = new Date();
			String date = new SimpleDateFormat("dd-MM-yyyy").format(dtf).toString(); // 18/07/2017
			String mes = date.substring(3, 5);
			String ano = date.substring(6, 10);*/
			//String fechaReporte=""+mes+"/"+ano;
			String fechaReporte=""+seleccionMes+"/"+seleccionAno;
			map=reporteRegBI.ReporteVistaCumplimientoSucursal(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,0);
			logger.info("map... " + map);
			if(valorPila==1){
				mapPila=reporteRegBI.ReporteVistaCumplimientoSucursal(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,2);
				logger.info("mapPila... " + mapPila);
			}
				
		} catch (Exception e) {
			logger.info("Ocurrio algo... ");
		}
		if(map!=null){
			porcentaje= (Integer) map.get("porcentaje");
			logger.info("porcentaje... "+porcentaje);
			listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
			logger.info("listaChecklist... "+listaChecklist);
			listRetorno.add(listaChecklist);
			
			listaReporteCum= (List<List>) map.get("listaSucursales");
			logger.info("listaSucursales... "+listaReporteCum);
			conteo = (Integer) map.get("conteo");
			logger.info("conteo... "+conteo);
		}			
		
		if(valorPila==1){
			if(mapPila!=null){
				listaReporteCumPila= (List<VistaCDTO>) mapPila.get("listaSucursales");
				logger.info("listaReporteCumPila... "+listaReporteCumPila);
				
				// NUEVA VALIDACION
				if(listaReporteCumPila.size()>1){					
					boolean flag = false;
					int l =1;
					int sumaAnt = 0;
					int sumaAct = 0;
					VistaCDTO act = null;
					VistaCDTO ant = null;
					while(l < listaReporteCumPila.size()){	
						sumaAnt= listaReporteCumPila.get(l - 1).getAsignados()+listaReporteCumPila.get(l - 1).getTerminados();
						sumaAct= listaReporteCumPila.get(l).getAsignados()+listaReporteCumPila.get(l).getTerminados();						
						if(sumaAnt < sumaAct){
							ant=listaReporteCumPila.get(l - 1);
							act=listaReporteCumPila.get(l);							
							listaReporteCumPila.set(l - 1, act);
							listaReporteCumPila.set(l, ant);
							flag=true;
						}
						if (flag && (l + 1) == listaReporteCumPila.size()) {
							l = 0;
							flag = false;
						}
						l++;
					}				
				}
				// NUEVA VALIDACION				
			}			
			for (int i = 0; i < listaReporteCumPila.size(); i++) {
				VistaCDTO listaVista = new VistaCDTO();
				listaVista=(VistaCDTO) listaReporteCum.get(i);
				
				VistaCDTO listaVistaPila = new VistaCDTO();
				listaVistaPila=(VistaCDTO) listaReporteCumPila.get(i);

				listaVista.setAsignados(listaVistaPila.getAsignados());
				listaVista.setTerminados(listaVistaPila.getTerminados());
			}
		}
		
		if(conteo==0)
			conteo++;
		
		List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
		
		logger.info("listaReporteCum.size() "+ listaReporteCum.size());
		for (int i = 0; i < listaReporteCum.size(); i++) {							
			listaAux.add((VistaCDTO) listaReporteCum.get(i));			
			if((i+1)%conteo==0 ){
				listRetorno.add(listaAux);				
				listaAux=new ArrayList<VistaCDTO>();
			}
		}
		
		logger.info("listRetorno.size() "+listRetorno.size());
		logger.info("listRetorno "+listRetorno);

		logger.info("...SALE DEL AJAX ajaxFiltroSucursal CAJA...");
		return listRetorno;
	}
	/*::::::::::::::::::::FIN AJAX FILTRO SUCURSAL POR CAJA::::::::::::::::::::*/

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/ajaxFiltroSucursalCheck", method = RequestMethod.GET)
	public @ResponseBody List<List> ajaxFiltroSucursalCheck(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxFiltroSucursalCheck...");
		
		logger.info("idCeco... "+idCeco);
		logger.info("banderaCecoPadre... "+banderaCecoPadre);
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);

		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());
		logger.info("USUARIO "+ idUsuario);
		
		List<ChecklistDTO> listaChecklist = null;
		List<List> listaReporteCum= new ArrayList<List>();
		
		int porcentaje = 0, conteo=0;
		Map<String, Object> map= new HashMap<String, Object>();
		List<List> listRetorno= new ArrayList<List>();
		String fechaReporte=""+seleccionMes+"/"+seleccionAno;
		logger.info("datos antes de los mapas... ");
		
		try {		
			map=reporteBI.ReporteVistaCumplimientoSucursal(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,1);
			logger.info("map... " + map);			
		} catch (Exception e) {
			logger.info("Ocurrio algo... ");
		}
		
		if(map!=null){
			porcentaje= (Integer) map.get("porcentaje");
			logger.info("porcentaje... "+porcentaje);
			listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
			logger.info("listaChecklist... "+listaChecklist);
			listRetorno.add(listaChecklist);
			
			listaReporteCum= (List<List>) map.get("listaSucursales");
			logger.info("listaSucursales... "+listaReporteCum);
			conteo = (Integer) map.get("conteo");
			logger.info("conteo... "+conteo);
		}			
			
		List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
		
		logger.info("listaReporteCum.size() "+ listaReporteCum.size());
		if(listaReporteCum.size()==0){
			
		}else{
			for (int i = 0; i < conteo; i++) {					
				listaAux.add((VistaCDTO) listaReporteCum.get(i));				
				if((i+1)%conteo==0){
					listRetorno.add(listaAux);				
					listaAux=new ArrayList<VistaCDTO>();
				}
			}
		}
		
		List<VistaCDTO> listaConteo=new ArrayList<VistaCDTO>();
		VistaCDTO conteoVista= new VistaCDTO();
		conteoVista.setTotal(conteo);
		listaConteo.add(conteoVista);
		listRetorno.add(listaConteo);
		logger.info("listRetorno.size() "+listRetorno.size());
		logger.info("listRetorno "+listRetorno);

		logger.info("...SALE DEL AJAX ajaxFiltroSucursalCheck...");
		return listRetorno;
	}
	
	/*AJAX FILTRO SUCURSAL CHECK PILA */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/ajaxFiltroSucursalCheckPila", method = RequestMethod.GET)
	public @ResponseBody List<List> ajaxFiltroSucursalCheckPila(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxFiltroSucursalCheckPILA...");
		
		logger.info("idCeco... "+idCeco);
		logger.info("banderaCecoPadre... "+banderaCecoPadre);
		logger.info("seleccionAno... "+seleccionAno);
		logger.info("seleccionMes... "+seleccionMes);
		logger.info("seleccionCanal... "+seleccionCanal);
		logger.info("seleccionPais... "+seleccionPais);

		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());
		logger.info("USUARIO "+ idUsuario);
		
		List<ChecklistDTO> listaChecklist = null;
		List<List> listaReporteCum= new ArrayList<List>();
		
		int porcentaje = 0, conteo=0;
		Map<String, Object> map= new HashMap<String, Object>();
		List<List> listRetorno= new ArrayList<List>();
		String fechaReporte=""+seleccionMes+"/"+seleccionAno;
		logger.info("datos antes de los mapas... ");
		
		try {		
			map=reporteBI.ReporteVistaCumplimientoSucursalPila(idUsuario,idCeco ,seleccionPais, seleccionCanal,fechaReporte,1);
			logger.info("map... " + map);			
		} catch (Exception e) {
			logger.info("Ocurrio algo... ");
		}
		
		if(map!=null){
			porcentaje= (Integer) map.get("porcentaje");
			logger.info("porcentaje... "+porcentaje);
			listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
			logger.info("listaChecklist... "+listaChecklist);
			listRetorno.add(listaChecklist);
			
			listaReporteCum= (List<List>) map.get("listaSucursales");
			logger.info("listaSucursales... "+listaReporteCum);
			conteo = (Integer) map.get("conteo");
			logger.info("conteo... "+conteo);
		}			
			
		List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
		
		logger.info("listaReporteCum.size() "+ listaReporteCum.size());
		if(listaReporteCum.size()==0){
			
		}else{
			for (int i = 0; i < conteo; i++) {					
				listaAux.add((VistaCDTO) listaReporteCum.get(i));				
				if((i+1)%conteo==0){
					listRetorno.add(listaAux);				
					listaAux=new ArrayList<VistaCDTO>();
				}
			}
		}
		
		List<VistaCDTO> listaConteo=new ArrayList<VistaCDTO>();
		VistaCDTO conteoVista= new VistaCDTO();
		conteoVista.setTotal(conteo);
		listaConteo.add(conteoVista);
		listRetorno.add(listaConteo);
		logger.info("listRetorno.size() "+listRetorno.size());
		logger.info("listRetorno "+listRetorno);

		logger.info("...SALE DEL AJAX ajaxFiltroSucursalCheckPILA...");
		return listRetorno;
	}
	/*FIN AJAX FILTRO SUCURSAL CHECK PILA*/
	
	@RequestMapping(value = "/ajaxValidaDetalle", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> ajaxValidaDetalle(
			@RequestParam(value = "idCecoHijo", required = true, defaultValue = "0") int idCecoHijo,
			@RequestParam(value = "idCheck", required = true, defaultValue = "0") int idCheck,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxValidaDetalle...");

		Map<String, Object> map= null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());		
		String fechaC = seleccionMes+"/"+seleccionAno;
		
		logger.info("ID CECO " + idCecoHijo);
		logger.info("ID CHECK " + idCheck);
		logger.info("ID USUARIO " + idUsuario);
		logger.info("ID FECHA " + fechaC);
		
		logger.info("datos antes del mapa... ");
		
		try {
			map=reporteBI.ReporteDetVC(idCecoHijo, idCheck, idUsuario, fechaC);
			
			if(map!=null){
				if(!map.isEmpty()){
					this.mapGlobal=map;
				}else{
					map=null;
					this.mapGlobal=null;
				}					
			}					
		} catch (Exception e) {
			logger.info("Ocurrio algo... ");
		}
		logger.info("map... " + map);		
		logger.info("...SALE DEL AJAX ajaxValidaDetalle...");
		
		return map;
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/resporteCumplimientoDoc", method = RequestMethod.GET)
	public @ResponseBody List<List> resporteCumplimientoDoc(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais,
			@RequestParam(value = "valorPila", required = true, defaultValue = "0") int valorPila, HttpServletRequest request,HttpServletResponse response) throws Exception {
	
		ServletOutputStream sos = null;
		String archivo = "";
		
		String[] territorios = {"236737"
				,"236736","236738"};
		
		//Recorre y consume por Territorio
		archivo =  "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'><tr>";
		
		archivo += "<thead>"
				+ "<tr>"
				+ "<th align='center'>Ceco</th>"
				+ "<th align='center'>Nombre Ceco</th>"
				+ "<th align='center'>Caja</th>"			
				+ "<th align='center'>Piso Bancario</th>"
				+ "<th align='center'>7s</th>"
				+ "</tr>"
				+ "</thead>"
				+ "<tbody>";				
		
		String aux = "";
		
		for (String cecoTerritorio : territorios) {
			List<List> listaZonasService = consumeServicio("147247", "062018", cecoTerritorio);
			aux += escribeFila(listaZonasService);
			
			List<CecoDTO> listaCecos  = cecobi.buscaCecosPasoP(Integer.parseInt(cecoTerritorio));
			
			for(int i=0; i<listaCecos.size() ; i++){
				String cecoZona = listaCecos.get(i).getIdCeco();
				//System.out.println("Se ejecuta el ceco "+cecoZona);
				
				List<List> listaRegionesService = consumeServicio("147247", "062018", cecoZona);
				aux += escribeFila(listaRegionesService);	
				
				List<CecoDTO> listaRegiones  = cecobi.buscaCecosPasoP(Integer.parseInt(cecoZona));
				
				for(int j=0; j<listaRegiones.size() ; j++){
					String cecoRegion = listaRegiones.get(j).getIdCeco();
					//System.out.println("Se ejecuta el ceco "+cecoRegion);
					
					List<List> listaSucursales = consumeServicio("147247", "062018", cecoRegion);
					aux += escribeFila(listaSucursales);
					////System.out.println(listaSucursales);
					}
				 
				////System.out.println(listaZonas);
				}
			
			////System.out.println(listas.toString());
		}
		
		//System.out.println("AUXUILIAR  : "+aux);
		archivo += aux;
		archivo += "</tbody></table>";
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String fecha = dateFormat.format(date).replace(" ", "_").replace(":","_").replace("/","_");
		response.setContentType("Content-type: application/xls; name='excel'");
		response.setHeader("Content-Disposition","attachment; filename=reporte"+fecha+".xls");
		response.setHeader("Pragma: no-cache", "Expires: 0");
		sos = response.getOutputStream();
		//byte[] encode = archivo.getBytes("UTF-8");
		sos.write(archivo.getBytes());		
		
		//System.out.println("Termine");
		
		return null;
	}
	
	public List<List> consumeServicio(String usuario,String fecha,String ceco){
		int idUsuario=0;
		
		idUsuario=Integer.parseInt(usuario);
		logger.info("USUARIO "+ idUsuario);
		
		List<ChecklistDTO> listaChecklist = null;
		List<List> listaReporteCum= new ArrayList<List>();
		List<VistaCDTO> listaReporteCumPila= new ArrayList<VistaCDTO>();
		
		int porcentaje = 0, conteo=0;
		Map<String, Object> map= new HashMap<String, Object>();
		Map<String, Object> mapPila= new HashMap<String, Object>();
		List<List> listRetorno= new ArrayList<List>();
		//String fechaReporte=""+seleccionMes+"/"+seleccionAno;
		logger.info("datos antes de los mapas... ");
		int valorPila = 1;
		
		try {
			map=reporteBI.ReporteVistaCumplimientoSucursal(idUsuario,ceco ,21, 1,fecha,0);
			logger.info("map... " + map);
			if(valorPila==1){
				mapPila=reporteBI.ReporteVistaCumplimientoSucursal(idUsuario,ceco ,21, 1,fecha,2);
				logger.info("mapPila... " + mapPila);			
			}
				
		} catch (Exception e) {
			logger.info("Ocurrio algo... ");
		}
		if(map!=null){
			porcentaje= (Integer) map.get("porcentaje");
			logger.info("porcentaje... "+porcentaje);
			listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
			logger.info("listaChecklist... "+listaChecklist);
			listRetorno.add(listaChecklist);
			
			listaReporteCum= (List<List>) map.get("listaSucursales");
			logger.info("listaSucursales... "+listaReporteCum);
			conteo = (Integer) map.get("conteo");
			logger.info("conteo... "+conteo);
		}			
		
		if(valorPila==1){
			if(mapPila!=null){
				listaReporteCumPila= (List<VistaCDTO>) mapPila.get("listaSucursales");
				logger.info("listaReporteCumPila... "+listaReporteCumPila);
				
				// NUEVA VALIDACION
				if(listaReporteCumPila.size()>1){					
					boolean flag = false;
					int l =1;
					int sumaAnt = 0;
					int sumaAct = 0;
					VistaCDTO act = null;
					VistaCDTO ant = null;
					while(l < listaReporteCumPila.size()){	
						sumaAnt= listaReporteCumPila.get(l - 1).getAsignados()+listaReporteCumPila.get(l - 1).getTerminados();
						sumaAct= listaReporteCumPila.get(l).getAsignados()+listaReporteCumPila.get(l).getTerminados();						
						if(sumaAnt < sumaAct){
							ant=listaReporteCumPila.get(l - 1);
							act=listaReporteCumPila.get(l);							
							listaReporteCumPila.set(l - 1, act);
							listaReporteCumPila.set(l, ant);
							flag=true;
						}
						if (flag && (l + 1) == listaReporteCumPila.size()) {
							l = 0;
							flag = false;
						}
						l++;
					}				
				}
				// NUEVA VALIDACION				
			}			
			for (int i = 0; i < listaReporteCumPila.size(); i++) {
				VistaCDTO listaVista = new VistaCDTO();
				listaVista=(VistaCDTO) listaReporteCum.get(i);
				
				VistaCDTO listaVistaPila = new VistaCDTO();
				listaVistaPila=(VistaCDTO) listaReporteCumPila.get(i);

				listaVista.setAsignados(listaVistaPila.getAsignados());
				listaVista.setTerminados(listaVistaPila.getTerminados());
			}
		}
		
		if(conteo==0)
			conteo++;
		
		List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
		
		logger.info("listaReporteCum.size() "+ listaReporteCum.size());
		for (int i = 0; i < listaReporteCum.size(); i++) {							
			listaAux.add((VistaCDTO) listaReporteCum.get(i));			
			if((i+1)%conteo==0 ){
				listRetorno.add(listaAux);				
				listaAux=new ArrayList<VistaCDTO>();
			}
		}
		
		logger.info("listRetorno.size() "+listRetorno.size());
		logger.info("listRetorno "+listRetorno);

		logger.info("...SALE DEL AJAX ajaxFiltroSucursal...");
		
		return listRetorno;
	}
		
	public String escribeFila(List<List> lista ) {
		
		String respuesta ="";
		String filaChecklist ="";
	    int[] totales = new int[3];
	    int veces = 0;
		
		for(int i=1; i<lista.size(); i++) {
			
			String idCeco = "";
			String nombreCeco = "";
			
			for(int j =0; j< lista.get(i).size(); j++ ){
				
				VistaCDTO checklist = (VistaCDTO) lista.get(i).get(j);

				idCeco = checklist.getIdCeco();
				nombreCeco = checklist.getDescCeco();
				
				if(checklist.getIdChecklist().equals("97")) 
					totales[0] = checklist.getTotal();
				else if(checklist.getIdChecklist().equals("99"))
					totales[1] = checklist.getTotal();
				else if (checklist.getIdChecklist().equals("156")) 
					totales[2] = checklist.getTotal();	  
				
				
			}
			filaChecklist += "<tr>"
					       + "<td align='center'>"+idCeco+"</td>"
					       + "<td align='center'>"+nombreCeco+"</td>"
					       + "<td align='center'>"+totales[0]+" </td>"
					       + "<td align='center'>"+totales[1]+" </td>"
					       + "<td align='center'>"+totales[2]+" </td>"
					       + "</tr>";
			respuesta += filaChecklist;
			filaChecklist="";
			
			totales[0] =0;
			totales[1] =0;
			totales[2] =0;
			
		}
		
		return respuesta;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/reporteCumplimientoThread", method = RequestMethod.GET)
	public @ResponseBody List<List> reporteCumplimientoThread(
			@RequestParam(value = "idCeco", required = true, defaultValue = "0") String idCeco,
			@RequestParam(value = "banderaCecoPadre", required = true, defaultValue = "0") int banderaCecoPadre,
			@RequestParam(value = "seleccionAno", required = true, defaultValue = "0") int seleccionAno,
			@RequestParam(value = "seleccionMes", required = true, defaultValue = "0") int seleccionMes,
			@RequestParam(value = "seleccionCanal", required = true, defaultValue = "0") int seleccionCanal,
			@RequestParam(value = "seleccionPais", required = true, defaultValue = "0") int seleccionPais,
			@RequestParam(value = "valorPila", required = true, defaultValue = "0")int valorPila,
			@RequestParam(value = "fecha", required = true, defaultValue = "0") String fechaParam, HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		
		GeneraReporteCumplimiento territorio1 = new GeneraReporteCumplimiento(fechaParam, "236737");
		GeneraReporteCumplimiento territorio2 = new GeneraReporteCumplimiento(fechaParam, "236736");
		GeneraReporteCumplimiento territorio3 = new GeneraReporteCumplimiento(fechaParam, "236738");
		GeneraReporteCumplimiento territorioGuadalajara = new GeneraReporteCumplimiento(fechaParam, "6781");
		

		
		long inicio = System.currentTimeMillis();
		logger.info("INICIA EJECUCION DE HILOS " + new Date());
		
		territorio1.start();
		territorio2.start();
		territorio3.start();
		territorioGuadalajara.start();
		
		while(territorio1.isAlive() 
				|| territorio2.isAlive() 
				|| territorio3.isAlive()
				|| territorioGuadalajara.isAlive()
		){
			//Ciclo que sirve para validar que aun siguen vivos los hilos
		}
		
		ServletOutputStream sos = null;
		String archivo = "";
		
		//Recorre y consume por Territorio
		archivo =  "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'><tr>";
		
		archivo += "<thead>"
				+ "<tr>"
				+ "<th align='center'>Ceco</th>"
				+ "<th align='center'>Nombre Ceco</th>"
				+ "<th align='center'>Caja</th>"			
				+ "<th align='center'>Piso Bancario</th>"
				+ "<th align='center'>7s</th>"
				+ "</tr>"
				+ "</thead>"
				+ "<tbody>";				
		
		archivo += territorio1.getRespuesta() + territorio2.getRespuesta() + territorio3.getRespuesta() + territorioGuadalajara.getRespuesta();
		

		archivo += "</tbody></table>";

		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String fecha = dateFormat.format(date).replace(" ", "_").replace(":","_").replace("/","_");
		response.setContentType("Content-type: application/xls; name='excel'");
		response.setHeader("Content-Disposition","attachment; filename=reporte"+fecha+".xls");
		response.setHeader("Pragma: no-cache", "Expires: 0");
		sos = response.getOutputStream();
		//byte[] encode = archivo.getBytes("UTF-8");
		sos.write(archivo.getBytes());		
		
		long acabo = System.currentTimeMillis();
		long totaltiempo = (acabo - inicio) / 1000;
		
		return null;
	}

}