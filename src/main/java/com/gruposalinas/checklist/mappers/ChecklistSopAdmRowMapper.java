package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.CheckSoporteAdmDTO;

public class ChecklistSopAdmRowMapper implements RowMapper<CheckSoporteAdmDTO> {

	@Override
	public CheckSoporteAdmDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		CheckSoporteAdmDTO chk = new CheckSoporteAdmDTO();
		
		chk.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		chk.setTipo(rs.getInt("FIID_TIPO_CHECK"));
		chk.setNombreCheck(rs.getString("FCNOMBRE"));
		chk.setIdHorario(rs.getInt("FIID_HORARIO"));
		chk.setVigente(rs.getInt("FIVIGENTE"));
		chk.setFecha_inicio(rs.getString("FDFECHA_INICIO"));
		chk.setIdEstado(rs.getInt("FIID_ESTADO"));
		chk.setIdUsuario(rs.getInt("FIID_USUARIO"));
		chk.setDia(rs.getString("FCDIA"));
		chk.setPeriodo(rs.getString("FCPERIODO"));
		chk.setVersion(rs.getString("FIVERSION"));
		chk.setOrdeGrupo(rs.getString("FIORDEN_GRUPO"));
		chk.setPonderacionTot(rs.getDouble("FCPONDTOTAL"));
		chk.setClasifica(rs.getString("FCCLASIFICA"));
		chk.setIdProtocolo(rs.getInt("FIID_PROTOCOLO"));
		chk.setFecha(rs.getString("FDPERIODO"));
		chk.setNego(rs.getString("FCNEGOCIO"));
		chk.setNomusu(rs.getString("NOMUSU"));
        
		return chk;
	}
	
	

}
