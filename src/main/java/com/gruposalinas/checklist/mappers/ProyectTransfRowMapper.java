package com.gruposalinas.checklist.mappers;

import com.gruposalinas.checklist.domain.AsignaTransfDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ProyectTransfRowMapper implements RowMapper<AsignaTransfDTO> {

    public AsignaTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        AsignaTransfDTO AsignaTransfDTO = new AsignaTransfDTO();

        AsignaTransfDTO.setProyecto(rs.getInt("FIID_PROYECTO"));
        AsignaTransfDTO.setNombProy(rs.getString("NOMPROYECTO"));
        AsignaTransfDTO.setObsProyecto(rs.getString("OBSPROY"));
        AsignaTransfDTO.setIdestatus(rs.getInt("FIIDACTIVO"));
        AsignaTransfDTO.setTipoProyecto(rs.getInt("FIIDTIPOPROY"));
        AsignaTransfDTO.setEdoCargaHallazgo(rs.getInt("FIID_EDOCARGAINI"));
        AsignaTransfDTO.setStatusMaximo(rs.getInt("FCEDOHALLAFIN"));
        AsignaTransfDTO.setPeriodo(rs.getString("FCPERIODO"));
        return AsignaTransfDTO;
    }

}
