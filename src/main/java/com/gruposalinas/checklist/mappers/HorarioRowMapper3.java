package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.HorarioDTO;

public class HorarioRowMapper3 implements RowMapper<HorarioDTO> {

	
	public HorarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		HorarioDTO horarioDTO = new HorarioDTO();
		
		horarioDTO.setIdHorario(rs.getInt("FIID_HORARIO"));
		horarioDTO.setCveHorario(rs.getString("FCCVE_HORARIO"));
		horarioDTO.setValorIni(rs.getString("FCVALOR_INI"));
		horarioDTO.setValorFin(rs.getString("FCVALOR_FIN"));
		
		return horarioDTO;
	}
}
