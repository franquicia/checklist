package com.gruposalinas.checklist.util;

import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;

import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.resources.FRQResourceLoader;

public class UtilResource {
	public static Resource getResourceFromInternet(String paginaEstatica){
		ApplicationContext appContext = FRQAppContextProvider.getApplicationContext();
		FRQResourceLoader resourceLoader = (FRQResourceLoader)appContext.getBean("frqResourceLoader");
		//Resource resource = resourceLoader.getResourceLoader().getResource("http://ipdelserver/static/prueba.html");
		Resource resource = resourceLoader.getResourceLoader().getResource(FRQConstantes.getURLServer() + "/checklist/static/" + paginaEstatica);
		return resource;
	}
}
