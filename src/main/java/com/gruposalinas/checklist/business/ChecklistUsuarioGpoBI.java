package com.gruposalinas.checklist.business;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ChecklistUsuarioGpoDAO;


public class ChecklistUsuarioGpoBI { 

	private static Logger logger= LogManager.getLogger(ChecklistUsuarioGpoBI.class);
	

	@Autowired
	ChecklistUsuarioGpoDAO checklistUsuarioGpoDAO;
	

	
	public boolean insertaCheck(String idUsuario, String idCeco, String idGrupo){
		
		boolean respuesta = false;
		
		try {
			respuesta= checklistUsuarioGpoDAO.insertaCheck(idUsuario, idCeco, idGrupo);
		} catch (Exception e) {
			logger.info("No fue posible insertar el Checklist por Grupo");
			
		}
		
		return respuesta;
		
	}
}
