package com.gruposalinas.checklist.domain;

public class RepoCheckDTO {
	

		private String archivoExcel;
		private int idUsuario;
		private String nombre;
		private String ceco;
		private  int idcheckList;
		private  int calif;
		private  String fecha;
		private  String tiempo;
		private  String nombCheck;
		private  String fechaInic;
		private  String fechaFin;
		
		
		
		public String getArchivoExcel() {
			return archivoExcel;
		}
		public void setArchivoExcel(String archivoExcel) {
			this.archivoExcel = archivoExcel;
		}
		public int getIdUsu() {
			return idUsuario;
		}
		public void setIdUsu(int idUsuario) {
			this.idUsuario = idUsuario;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getCeco() {
			return ceco;
		}
		public void setCeco(String ceco) {
			this.ceco = ceco;
		}
		public int getIdcheckList() {
			return idcheckList;
		}
		public void setIdcheckList(int idcheckList) {
			this.idcheckList = idcheckList;
		}
		public int getCalif() {
			return calif;
		}
		public void setCalif(int calif) {
			this.calif = calif;
		}
		public String getFecha() {
			return fecha;
		}
		public void setFecha(String fecha) {
			this.fecha = fecha;
		}
		public String getTiempo() {
			return tiempo;
		}
		public void setTiempo(String tiempo) {
			this.tiempo = tiempo;
		}
		public String getNombCheck() {
			return nombCheck;
		}
		public void setNombCheck(String nombCheck) {
			this.nombCheck = nombCheck;
		}
		public String getFechaInic() {
			return fechaInic;
		}
		public void setFechaInic(String fechaInic) {
			this.fechaInic = fechaInic;
		}
		public String getFechaFin() {
			return fechaFin;
		}
		public void setFechaFin(String fechaFin) {
			this.fechaFin = fechaFin;
		}
		@Override
		public String toString() {
			return "RepoCheckDTO [archivoExcel=" + archivoExcel + ", idUsuario=" + idUsuario + ", nombre=" + nombre + ", ceco="
					+ ceco + ", idcheckList=" + idcheckList + ", calif=" + calif + ", fecha=" + fecha + ", tiempo="
					+ tiempo + ", nombCheck=" + nombCheck + ", fechaInic=" + fechaInic + ", fechaFin=" + fechaFin + "]";
		}
		
		
		
	}