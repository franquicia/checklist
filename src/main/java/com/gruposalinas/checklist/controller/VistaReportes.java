package com.gruposalinas.checklist.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.gruposalinas.checklist.business.FiltrosCecoBI;
import com.gruposalinas.checklist.business.ReporteBI;
import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.domain.EvidenciaDTO;
import com.gruposalinas.checklist.domain.FiltrosCecoDTO;
import com.gruposalinas.checklist.domain.NegocioDTO;
import com.gruposalinas.checklist.domain.PaisDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.ReporteDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;

@Controller
public class VistaReportes {
	
	private static Logger logger = LogManager.getLogger(VistaReportes.class);
	
	@Autowired
	ReporteBI reporteBI;
	@Autowired
	FiltrosCecoBI filtrosCecoBI;
		
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "central/vistaCumplimiento.htm", method = RequestMethod.GET)
	public ModelAndView vistaCumplimiento(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		logger.info("USUARIO "+ userSession.getIdUsuario());
		
		if(Integer.parseInt(userSession.getIdUsuario())>0){ 
			ModelAndView mv = new ModelAndView("vistaReporte");
			mv.addObject("idUsuario", userSession.getIdUsuario());
			mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
			
			//Fragmento para seleccionar el filtrado de tiendas
			List<NegocioDTO> negocio=null;
			List<PaisDTO> pais=null;
			List<FiltrosCecoDTO> territorio=null;
			List<FiltrosCecoDTO> zona=null;
			List<FiltrosCecoDTO> region=null;
			Map<String, Object> map= new HashMap<String, Object>();	
			map= filtrosCecoBI.obtieneFiltros(Integer.parseInt(userSession.getIdUsuario()));
			logger.info("map... " + map);
			negocio = (List<NegocioDTO>) map.get("negocio");
			pais = (List<PaisDTO>) map.get("pais");
			territorio = (List<FiltrosCecoDTO>) map.get("territorio");
			zona = (List<FiltrosCecoDTO>) map.get("zona");
			region = (List<FiltrosCecoDTO>) map.get("region");
			logger.info("negocio... " + negocio.get(0).getDescripcion());
			logger.info("pais... " + pais.get(0).getNombre());
			logger.info("territorio... " + territorio.get(0).getNombreCeco());
			logger.info("zona... " + zona.get(0).getNombreCeco());
			logger.info("region... " + region.get(0).getNombreCeco());
			/*ESTE CODIGO COMENTADO ES LO QUE LLENA EL CANAL Y EL PAIS DE MANERA ESTATICA
			try {
				List<PaisDTO> listaPaisAux = filtrosCecoBI.obtienePaises(negocio.get(0).getIdNegocio());
				mv.addObject("paisIdAux", listaPaisAux.get(0).getIdPais());
				mv.addObject("paisNameAux", listaPaisAux.get(0).getNombre());
			} catch (Exception e) {
				logger.info("No hay lista en el pais");
			}*/
			
			int nivelAdmin=4;
			String regionId=null;
			String zonaId=null;
			String territorioId=null;
			int paisId=0;
			int negocioId=0;
					
			String regionName=null;
			String zonaName=null;
			String territorioName=null;
			String paisName=null;
			String negocioName=null;
			
			if(negocio.get(0).getDescripcion()!=null){
				negocioId=negocio.get(0).getIdNegocio();
				mv.addObject("negocioId", negocioId);
				negocioName=negocio.get(0).getDescripcion();
				mv.addObject("negocioName", negocioName);
				nivelAdmin=4;
				
				if(pais.get(0).getNombre()!=null){
					paisId=pais.get(0).getIdPais();
					mv.addObject("paisId", paisId);
					paisName=pais.get(0).getNombre();
					mv.addObject("paisName", paisName);
					nivelAdmin=3;
					if(territorio.get(0).getNombreCeco()!=null){
						
						if(zona.size()>1){
							territorioId=territorio.get(0).getIdCeco();
							mv.addObject("territorioId", territorioId);
							territorioName=territorio.get(0).getNombreCeco();
							mv.addObject("territorioName", territorioName);
							nivelAdmin=2;
						}else{
							territorioId=territorio.get(0).getIdCeco();
							mv.addObject("territorioId", territorioId);
							territorioName=territorio.get(0).getNombreCeco();
							mv.addObject("territorioName", territorioName);
							nivelAdmin=2;
							if(zona.get(0).getNombreCeco()!=null){
								
								if(region.size()>1){
									zonaId=zona.get(0).getIdCeco();
									mv.addObject("zonaId", zonaId);
									zonaName=zona.get(0).getNombreCeco();
									mv.addObject("zonaName", zonaName);
									nivelAdmin=1;
								}else{
									zonaId=zona.get(0).getIdCeco();
									mv.addObject("zonaId", zonaId);
									zonaName=zona.get(0).getNombreCeco();
									mv.addObject("zonaName", zonaName);
									nivelAdmin=1;
									if(region.get(0).getNombreCeco()!=null){
										regionId=region.get(0).getIdCeco();
										mv.addObject("regionId", regionId);
										regionName=region.get(0).getNombreCeco();
										mv.addObject("regionName", regionName);
										nivelAdmin=0;
									}else{									
										mv.addObject("regionId", regionId);
										mv.addObject("regionName", regionName);
									}
								}							
							}else{							
								mv.addObject("zonaId", zonaId);
								mv.addObject("zonaName", zonaName);
								mv.addObject("regionId", regionId);
								mv.addObject("regionName", regionName);
							}
						}					
					}else{					
						mv.addObject("territorioId", territorioId);
						mv.addObject("territorioName", territorioName);
						mv.addObject("zonaId", zonaId);
						mv.addObject("zonaName", zonaName);
						mv.addObject("regionId", regionId);
						mv.addObject("regionName", regionName);
					}
				}else{				
					mv.addObject("paisId", paisId);
					mv.addObject("paisName", paisName);
					mv.addObject("territorioId", territorioId);
					mv.addObject("territorioName", territorioName);
					mv.addObject("zonaId", zonaId);
					mv.addObject("zonaName", zonaName);
					mv.addObject("regionId", regionId);
					mv.addObject("regionName", regionName);
				}
			}else{
				mv.addObject("negocioId", negocioId);
				mv.addObject("negocioName", negocioName);
				mv.addObject("paisId", paisId);
				mv.addObject("paisName", paisName);
				mv.addObject("territorioId", territorioId);
				mv.addObject("territorioName", territorioName);
				mv.addObject("zonaId", zonaId);
				mv.addObject("zonaName", zonaName);
				mv.addObject("regionId", regionId);
				mv.addObject("regionName", regionName);
			}	
			mv.addObject("nivelAdmin", nivelAdmin);
			return mv;
		}else{
			ModelAndView mv = new ModelAndView("http404");
			return mv;
		}
		
		
	}
	
	@RequestMapping(value = "/ajaxListaCheck", method = RequestMethod.GET)
	public @ResponseBody List<ChecklistDTO> getListaCheck(
			@RequestParam(value = "idUsuario", required = true, defaultValue = "null") String idUsuario, HttpServletRequest request) throws Exception {
		
			if(idUsuario.equals("null"))
				idUsuario="1";	
			logger.info("idUsuario______ "+ idUsuario);
			List<ChecklistDTO> listaCheck = new ArrayList<ChecklistDTO>();
			listaCheck = filtrosCecoBI.obtieneChecklist(Integer.parseInt(idUsuario));			
			logger.info("listaCheck______ "+ listaCheck);
		return listaCheck;
	}
	
	@RequestMapping(value = "/ajaxListaReporte", method = RequestMethod.GET)
	public @ResponseBody List<ReporteDTO> getListaReporte(
			@RequestParam(value = "priValor", required = true, defaultValue = "null") String priValor,
			@RequestParam(value = "segValor", required = true, defaultValue = "null") String segValor,
			@RequestParam(value = "terValor", required = true, defaultValue = "null") String terValor,
			@RequestParam(value = "cuaValor", required = true, defaultValue = "null") String cuaValor,
			@RequestParam(value = "idCanal", required = true, defaultValue = "null") String idCanal,
			@RequestParam(value = "idPais", required = true, defaultValue = "null") String idPais,
			@RequestParam(value = "fechaInicio", required = true, defaultValue = "null") String fechaInicio,
			@RequestParam(value = "fechaFin", required = true, defaultValue = "null") String fechaFin,
			@RequestParam(value = "idCheck", required = true, defaultValue = "null") String idCheck,
			@RequestParam(value = "id", required = true, defaultValue = "null") String id, HttpServletRequest request) throws Exception {
		
			if(priValor.equals("null"))
				priValor="1";
			if(segValor.equals("null"))
				segValor="0";
			if(terValor.equals("null"))
				terValor="0";
			if(cuaValor.equals("null"))
				cuaValor=null;
			if(id.equals("null"))
				id=null;
			if(fechaInicio.equals("null"))
				fechaInicio=null;
			if(fechaFin.equals("null"))
				fechaFin=null;
			if(idCheck.equals("null") || idCheck.equals("Selecciona una Opcion"))
				idCheck="0";
			
			logger.info("priValor______ "+ priValor);
			logger.info("segValor______ "+ segValor);
			logger.info("terValor______ "+ terValor);
			logger.info("cuaValor______ "+ cuaValor);
			logger.info("id______ "+ id);
			logger.info("idCanal______ "+ idCanal);
			logger.info("idPais______ "+ idPais);
			logger.info("fechaInicio______ "+ fechaInicio);
			logger.info("fechaFin______ "+ fechaFin);
			logger.info("idCheck______ "+ idCheck);
			List<ReporteDTO> listaRep=reporteBI.ReporteSupReg(priValor,segValor, terValor, cuaValor, id,idPais,idCanal, fechaInicio, fechaFin, idCheck);
			logger.info("Resultado de ajaxListaReporte... "+ listaRep);
		
		return listaRep;
	}
	
	@RequestMapping(value = "/ajaxTablaOperandoHijo", method = RequestMethod.GET)
	public @ResponseBody List<ReporteDTO> getTablaOperandoHijo(
			@RequestParam(value = "idCheck", required = true, defaultValue = "null") String idCheck,
			@RequestParam(value = "varIdPregunta", required = true, defaultValue = "varIdPregunta") String varIdPregunta,
			@RequestParam(value = "fechaInicio", required = true, defaultValue = "null") String fechaInicio,
			@RequestParam(value = "fechaFin", required = true, defaultValue = "null") String fechaFin,
			@RequestParam(value = "idCeco", required = true, defaultValue = "idCeco") String idCeco, 
			//@RequestParam(value = "nivelTerr", required = true, defaultValue = "null") String nivelTerritorio,
			//@RequestParam(value = "nivelZona", required = true, defaultValue = "null") String nivelZona,
			@RequestParam(value = "idCanal", required = true, defaultValue = "null") String idCanal,
			@RequestParam(value = "idPais", required = true, defaultValue = "null") String idPais,
			@RequestParam(value = "nivelRegion", required = true, defaultValue = "null") String nivelRegion, HttpServletRequest request) throws Exception {
		
		
		if(idCheck.equals("null") || idCheck.equals("Selecciona una Opcion"))
			idCheck="0";
		
		//logger.info(" FRQConstantes.getNumeroCheck()... "+ FRQConstantes.getNumeroCheck());
		logger.info("idCheck "+ idCheck);
		logger.info(" varIdPregunta... "+ varIdPregunta);
		logger.info(" fechaInicio... "+  fechaInicio);
		logger.info(" fechaFin... "+  fechaFin);
		logger.info(" idCeco... "+ idCeco);	
		logger.info(" idCanal... "+ idCanal);	
		logger.info(" idPais... "+ idPais);	
		logger.info(" nivelRegion... "+ nivelRegion);	

		List<ReporteDTO> listaRep=reporteBI.ReporteRegSup(Integer.parseInt(idCheck), Integer.parseInt(varIdPregunta), fechaInicio,fechaFin, idPais, idCanal, nivelRegion, idCeco);
		logger.info("resultado de tabla ajaxTablaOperandoHijo " + listaRep);
		
		return listaRep;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/ajaxReporteDetSupReg", method = RequestMethod.GET)
	public @ResponseBody List<List> getReporteDetSupReg(
			@RequestParam(value = "idCheck", required = true, defaultValue = "null") String idCheck,
			@RequestParam(value = "varIdCeco", required = true, defaultValue = "1") String varIdCeco,
			@RequestParam(value = "idCanal", required = true, defaultValue = "null") String idCanal,
			@RequestParam(value = "idPais", required = true, defaultValue = "null") String idPais,
			@RequestParam(value = "fechaInicio", required = true, defaultValue = "null") String fechaInicio,
			@RequestParam(value = "fechaFin", required = true, defaultValue = "null") String fechaFin, HttpServletRequest request) throws Exception {
		
		if(idCheck.equals("null") || idCheck.equals("Selecciona una Opcion"))
			idCheck="0";
		if(fechaInicio.equals("null"))
			fechaInicio=null;
		if(fechaFin.equals("null"))
			fechaFin=null;
		
		String finalFecha="";
		if(fechaFin!=null)
			finalFecha=fechaFin.substring(0, 2);
		
		logger.info("Fecha FinalFecha " + finalFecha);
		
		List<List> listaTotal= new ArrayList<List>();
		List<ReporteDTO> listaAuxiliar= null;
		List<CecoDTO> listaSucursales = null;
		List<ReporteDTO> listaDatos = null;
		Map<String, Object> map= new HashMap<String, Object>();
		logger.info("datos antes del mapa... ");
		//logger.info("FRQConstantes.getNumeroCheck()... " + FRQConstantes.getNumeroCheck());
		logger.info("idCheck "+ idCheck);
		logger.info("varIdCeco... " +  varIdCeco);
		logger.info("idCanal______ "+ idCanal);
		logger.info("idPais______ "+ idPais);
		logger.info("fechaInicio... " +  fechaInicio);
		logger.info("fechaFin... " +  fechaFin);
		map= reporteBI.ReporteDetSupReg_versBorra(Integer.parseInt(idCheck), varIdCeco,idPais,idCanal,fechaInicio, fechaFin);
		logger.info("map... " + map);
		
		listaSucursales = (List<CecoDTO>) map.get("listaSucursales");
		listaDatos = (List<ReporteDTO>) map.get("listaDatos");
			
		String[] strDias = new String[]{"D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S"};
		String[] diasSemana = new String[37];
		logger.info("**************************DOS***************************************");
		
		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");		
		Date fechaFormato = null;
		

		try {
			fechaFormato = format1.parse(fechaInicio);
			} catch (Exception ex) {
			ex.printStackTrace();
		}	
        
        
		GregorianCalendar fechaCalendario = new GregorianCalendar();
        fechaCalendario.setTime(fechaFormato);
        int diaSemana = (fechaCalendario.get(Calendar.DAY_OF_WEEK))-1;
        logger.info("diaSemana ajaxIdHijoVisitaTituloTabla: " + diaSemana);

        logger.info("***************************DOS**************************************");
		
        
		for (int i = 0; i < 37; i++) {
			
			//lista.add(strDias[diaSemana]);
			diasSemana[i]=strDias[diaSemana];
			logger.info("Total dias de la semana " +diasSemana[i]);
			diaSemana++;
		}		
		
		if(!listaSucursales.isEmpty()){			
			
			for (int i = 0; i < listaSucursales.size(); i++) {
				listaAuxiliar=new ArrayList<ReporteDTO>();
				logger.info("listaSucursales... "+i+ ": " + listaSucursales.get(i).getIdCeco() + " ... " + listaSucursales.get(i).getDescCeco());
								
				if(!listaDatos.isEmpty()){
					logger.info("dentro del listaDatos.isEmpty() ");
					boolean banderaCompa=false;
					for (int j = 0; j < listaDatos.size(); j++) {
						logger.info("entre al for "+j+": y antes de comparar en if " +listaSucursales.get(i).getIdCeco() + " . "+listaDatos.get(j).getIdCeco());
						if(listaSucursales.get(i).getIdCeco().equals(listaDatos.get(j).getIdCeco())){	
							banderaCompa=true;
							if((j+1)!=listaDatos.size()){
								logger.info("entre al (j+1)!=listaDatos.size() y antes del comparacion en if ");
								if(listaSucursales.get(i).getIdCeco().equals(listaDatos.get(j+1).getIdCeco())){
									logger.info("despues del if listaSucursales.get(i).getIdCeco().equals(listaDatos.get(j+1).getIdCeco()) ");
									
									
									logger.info("tamaño listaDatos " + listaDatos.size());	
									int iniciaCompara=j;
									//Reparar este cambio si es que vienen mas visitas mas de 2 ***************************************
									int numTotal=0;
									
									logger.info("lista de dias " + listaDatos.get(j).getDia());	
									
									logger.info("tamaño lista diasSemana " + diasSemana.length);	
									for (int k = 0; k < diasSemana.length; k++) {
										logger.info("entra a k " + k + ": " + diasSemana[k]);
										ReporteDTO repo= new ReporteDTO();	
										repo.setIdCeco(listaSucursales.get(i).getIdCeco());
										repo.setNombreCeco(listaSucursales.get(i).getDescCeco());
										repo.setImagen(listaDatos.get(j).getImagen());
										repo.setIdBitacora(listaDatos.get(j).getIdBitacora());
										repo.setConteo(listaDatos.get(j).getTotal());										
										
										if(iniciaCompara<listaDatos.size()){
											if(listaSucursales.get(i).getIdCeco().equals(listaDatos.get(iniciaCompara).getIdCeco())){
												logger.info("valor de la semana "+listaDatos.get(iniciaCompara).getSemana());
												if(listaDatos.get(iniciaCompara).getSemana().equals("1")){											
													logger.info("Solo visito la primera semana ");
													logger.info("Visito el dia: " + listaDatos.get(iniciaCompara).getDia());
													if(listaDatos.get(iniciaCompara).getDia().equals(diasSemana[k])){
														repo.setNumero(listaDatos.get(j+1).getNumero());
														iniciaCompara++;													
													}else{
														repo.setNumero(0);
													}																								
												}else if(listaDatos.get(iniciaCompara).getSemana().equals("2") && k>6){
													logger.info("Solo visito la segunda semana ");
													logger.info("Visito el dia: " + listaDatos.get(iniciaCompara).getDia());	
													if(listaDatos.get(iniciaCompara).getDia().equals(diasSemana[k])){
														repo.setNumero(listaDatos.get(j+1).getNumero());
														iniciaCompara++;
													}else{
														repo.setNumero(0);
													}																							
												}
											}else{
												logger.info("Acabaron los datos y llenara con 0 ");
												repo.setNumero(0);
											}

										}else{
											logger.info("Acabaron los datos  y ya no hay datos en lista y llenara con 0 ");
											repo.setNumero(0);
										}
										
										listaAuxiliar.add(repo);
										numTotal=numTotal+repo.getNumero();									
										
									}									
									
									ReporteDTO repo= new ReporteDTO();
									repo.setIdCeco(listaSucursales.get(i).getIdCeco());
									repo.setNombreCeco(listaSucursales.get(i).getDescCeco());	
									repo.setImagen(listaDatos.get(j).getImagen());
									repo.setIdBitacora(listaDatos.get(j).getIdBitacora());
									repo.setConteo(listaDatos.get(j).getTotal());									
									repo.setTotal(numTotal);
									listaAuxiliar.add(repo);
									logger.info("Acabo el llenado de visitas... ");
									listaTotal.add(listaAuxiliar);
									break;
									
									
								}else{
									logger.info("else por que solo existe una visita de esta sucursal listaSucursales.get(i).getIdCeco().equals(listaDatos.get(j+1).getIdCeco())");
									int numTotal=0;		
									logger.info("Valor de la semana " + listaDatos.get(j).getSemana());
									if(listaDatos.get(j).getSemana().equals("1")){	
										logger.info("Solo visito la primera semana ");
										logger.info("Visito el dia:" + listaDatos.get(j).getDia());										
										for (int k = 0; k <7; k++) {
											ReporteDTO repo= new ReporteDTO();		
											repo.setIdCeco(listaSucursales.get(i).getIdCeco());
											repo.setNombreCeco(listaSucursales.get(i).getDescCeco());		
											repo.setImagen(listaDatos.get(j).getImagen());
											if(listaDatos.get(j).getDia().equals(diasSemana[k]))										
												repo.setNumero(listaDatos.get(j).getNumero());											
											else
												repo.setNumero(0);	
											repo.setIdBitacora(listaDatos.get(j).getIdBitacora());
											repo.setConteo(listaDatos.get(j).getTotal());
											listaAuxiliar.add(repo);
											numTotal=numTotal+repo.getNumero();
										}	
										for (int k = 0; k <7; k++) {
											ReporteDTO repo= new ReporteDTO();
											repo.setIdCeco(listaSucursales.get(i).getIdCeco());
											repo.setNombreCeco(listaSucursales.get(i).getDescCeco());	
											repo.setImagen(listaDatos.get(j).getImagen());
											repo.setNumero(0);	
											repo.setIdBitacora(0);
											repo.setConteo(listaDatos.get(j).getTotal());
											listaAuxiliar.add(repo);
											numTotal=numTotal+repo.getNumero();
										}
									}else{
										logger.info("Solo visito la segunda semana ");
										logger.info("Visito el dia: " + listaDatos.get(j).getDia());										
										for (int k = 0; k <7; k++) {
											ReporteDTO repo= new ReporteDTO();		
											repo.setIdCeco(listaSucursales.get(i).getIdCeco());
											repo.setNombreCeco(listaSucursales.get(i).getDescCeco());	
											repo.setImagen(listaDatos.get(j).getImagen());
											repo.setNumero(0);	
											repo.setIdBitacora(0);
											repo.setConteo(listaDatos.get(j).getTotal());
											listaAuxiliar.add(repo);
											numTotal=numTotal+repo.getNumero();
										}
										for (int k = 0; k <7; k++) {
											ReporteDTO repo= new ReporteDTO();
											repo.setIdCeco(listaSucursales.get(i).getIdCeco());
											repo.setNombreCeco(listaSucursales.get(i).getDescCeco());	
											repo.setImagen(listaDatos.get(j).getImagen());
											if(listaDatos.get(j).getDia().equals(diasSemana[k]))										
												repo.setNumero(listaDatos.get(j).getNumero());											
											else
												repo.setNumero(0);	
											repo.setIdBitacora(listaDatos.get(j).getIdBitacora());
											repo.setConteo(listaDatos.get(j).getTotal());
											listaAuxiliar.add(repo);
											numTotal=numTotal+repo.getNumero();
										}
									}
									ReporteDTO repo= new ReporteDTO();
									repo.setIdCeco(listaSucursales.get(i).getIdCeco());
									repo.setNombreCeco(listaSucursales.get(i).getDescCeco());	
									repo.setImagen(listaDatos.get(j).getImagen());
									repo.setIdBitacora(listaDatos.get(j).getIdBitacora());
									repo.setConteo(listaDatos.get(j).getTotal());
									repo.setTotal(numTotal);
									listaAuxiliar.add(repo);
									logger.info("Acabo el llenado de visitas... ");
									listaTotal.add(listaAuxiliar);
									break;
								}								
							}else{
								logger.info("else que llena la ultima semana de la lista de datos");
								int numTotal=0;
								logger.info("valor de la semana "+listaDatos.get(j).getSemana());
								if(listaDatos.get(j).getSemana().equals("1")){									
									logger.info("Solo visito la primera semana ");
									logger.info("Visito el dia:" + listaDatos.get(j).getDia());										
									for (int k = 0; k <7; k++) {
										ReporteDTO repo= new ReporteDTO();	
										repo.setIdCeco(listaSucursales.get(i).getIdCeco());
										repo.setNombreCeco(listaSucursales.get(i).getDescCeco());
										repo.setImagen(listaDatos.get(j).getImagen());
										if(listaDatos.get(j).getDia().equals(diasSemana[k]))										
											repo.setNumero(listaDatos.get(j).getNumero());											
										else
											repo.setNumero(0);
										repo.setIdBitacora(listaDatos.get(j).getIdBitacora());
										repo.setConteo(listaDatos.get(j).getTotal());
										listaAuxiliar.add(repo);
										numTotal=numTotal+repo.getNumero();
									}	
									for (int k = 0; k <7; k++) {
										ReporteDTO repo= new ReporteDTO();
										repo.setIdCeco(listaSucursales.get(i).getIdCeco());
										repo.setNombreCeco(listaSucursales.get(i).getDescCeco());	
										repo.setImagen(listaDatos.get(j).getImagen());
										repo.setNumero(0);
										repo.setIdBitacora(0);
										repo.setConteo(listaDatos.get(j).getTotal());
										listaAuxiliar.add(repo);
										numTotal=numTotal+repo.getNumero();
									}
								}else{
									logger.info("Solo visito la segunda semana ");
									logger.info("Visito el dia: " + listaDatos.get(j).getDia());										
									for (int k = 0; k <7; k++) {
										ReporteDTO repo= new ReporteDTO();	
										repo.setIdCeco(listaSucursales.get(i).getIdCeco());
										repo.setNombreCeco(listaSucursales.get(i).getDescCeco());	
										repo.setImagen(listaDatos.get(j).getImagen());
										repo.setNumero(0);	
										repo.setIdBitacora(0);
										repo.setConteo(listaDatos.get(j).getTotal());
										listaAuxiliar.add(repo);
										numTotal=numTotal+repo.getNumero();
									}
									for (int k = 0; k <7; k++) {
										ReporteDTO repo= new ReporteDTO();
										repo.setIdCeco(listaSucursales.get(i).getIdCeco());
										repo.setNombreCeco(listaSucursales.get(i).getDescCeco());	
										repo.setImagen(listaDatos.get(j).getImagen());
										if(listaDatos.get(j).getDia().equals(diasSemana[k]))										
											repo.setNumero(listaDatos.get(j).getNumero());											
										else
											repo.setNumero(0);	
										repo.setIdBitacora(listaDatos.get(j).getIdBitacora());
										repo.setConteo(listaDatos.get(j).getTotal());
										listaAuxiliar.add(repo);
										numTotal=numTotal+repo.getNumero();
									}
								}
								ReporteDTO repo= new ReporteDTO();
								repo.setIdCeco(listaSucursales.get(i).getIdCeco());
								repo.setNombreCeco(listaSucursales.get(i).getDescCeco());
								repo.setImagen(listaDatos.get(j).getImagen());
								repo.setIdBitacora(listaDatos.get(j).getIdBitacora());
								repo.setConteo(listaDatos.get(j).getTotal());
								repo.setTotal(numTotal);
								listaAuxiliar.add(repo);
								logger.info("Acabo el llenado de visitas y cambia de sucursal... ");
								listaTotal.add(listaAuxiliar);
							}
						}else{	
							banderaCompa=false;
						}						
					}
					if(!banderaCompa){
						logger.info("Llena la sucursal actual con ceros por que no existen visitas");
						for (int k = 0; k < 15; k++) {
							ReporteDTO repo= new ReporteDTO();
							repo.setIdCeco(listaSucursales.get(i).getIdCeco());
							repo.setNombreCeco(listaSucursales.get(i).getDescCeco());
							repo.setImagen("0");
							repo.setNumero(0);	
							repo.setIdBitacora(0);
							repo.setConteo(0);
							repo.setTotal(0);
							listaAuxiliar.add(repo);
						}					
						listaTotal.add(listaAuxiliar);
					}
				}else{
					logger.info("Llena todas las sucursales con ceros por que no existen ninguna visita");
					for (int k = 0; k < 15; k++) {
						ReporteDTO repo= new ReporteDTO();
						repo.setIdCeco(listaSucursales.get(i).getIdCeco());
						repo.setNombreCeco(listaSucursales.get(i).getDescCeco());
						repo.setImagen("0");
						repo.setNumero(0);	
						repo.setIdBitacora(0);
						repo.setConteo(0);
						repo.setTotal(0);
						listaAuxiliar.add(repo);
					}					
					listaTotal.add(listaAuxiliar);
				}		
			}
		}else{
			logger.info("No existen sucursales ");
			listaAuxiliar=new ArrayList<ReporteDTO>();
			for (int j = 0; j < 15; j++) {
				ReporteDTO repo= new ReporteDTO();
				repo.setIdCeco("0");
				repo.setNombreCeco("NO HAY REGISTROS");	
				repo.setImagen("0");
				repo.setNumero(0);	
				repo.setIdBitacora(0);
				repo.setConteo(0);
				repo.setTotal(0);
				listaAuxiliar.add(repo);
			}			
			listaTotal.add(listaAuxiliar);
		}
				
		return listaTotal;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	@RequestMapping(value = "/ajaxReporteDetSupRegSucursal", method = RequestMethod.GET)
	public @ResponseBody List<List> getReporteDetSupRegSucursal(
			@RequestParam(value = "idCheck", required = true, defaultValue = "null") String idCheck,
			@RequestParam(value = "varIdCeco", required = true, defaultValue = "1") String varIdCeco,
			@RequestParam(value = "idCanal", required = true, defaultValue = "null") String idCanal,
			@RequestParam(value = "idPais", required = true, defaultValue = "null") String idPais,
			@RequestParam(value = "fechaInicio", required = true, defaultValue = "null") String fechaInicio,
			@RequestParam(value = "fechaFin", required = true, defaultValue = "null") String fechaFin,
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "null") String idCecoPadre, HttpServletRequest request) throws Exception {
		
		if(fechaInicio.equals("null"))
			fechaInicio=null;
		if(fechaFin.equals("null"))
			fechaFin=null;
		if(idCheck.equals("null") || idCheck.equals("Selecciona una Opcion"))
			idCheck="0";
		
		List<List> listaTotal= new ArrayList<List>();
		List<ReporteDTO> listaAuxiliar= null;
		List<CecoDTO> listaSucursalesOri = null;
		List<ReporteDTO> listaDatos = null;
		Map<String, Object> map= new HashMap<String, Object>();
		logger.info("datos antes del mapa... ");
		//logger.info("FRQConstantes.getNumeroCheck()... " + FRQConstantes.getNumeroCheck());
		logger.info("idCheck... " +  idCheck);
		logger.info("idCecoPadre... " +  idCecoPadre);
		logger.info("varIdCeco... " +  varIdCeco);
		logger.info("idCanal______ "+ idCanal);
		logger.info("idPais______ "+ idPais);
		logger.info("fechaInicio... " +  fechaInicio);
		logger.info("fechaFin... " +  fechaFin);
		map= reporteBI.ReporteDetSupReg(Integer.parseInt(idCheck), idCecoPadre,idPais,idCanal, fechaInicio, fechaFin);
		logger.info("map... " + map);
		
		listaSucursalesOri = (List<CecoDTO>) map.get("listaSucursales");
		listaDatos = (List<ReporteDTO>) map.get("listaDatos");
	
		List<CecoDTO> listaSucursales= new ArrayList<CecoDTO>();
		
		
		for (int i = 0; i < listaSucursalesOri.size(); i++) {
			logger.info("Simplifica la lista a 1 elemento" + listaSucursalesOri.get(i).getIdCeco()+".."+(varIdCeco));
			
			if(listaSucursalesOri.get(i).getIdCeco().equals(varIdCeco)){
				logger.info("Se añadio a la listaSucursales... " + listaSucursalesOri.get(i).getDescCeco());				
				listaSucursales.add(listaSucursalesOri.get(i));
			}
		}
		logger.info("listaSucursales " + listaSucursales);
		
		String[] strDias = new String[]{"D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S"};
		String[] diasSemana = new String[37];
		logger.info("**************************DOS***************************************");
		
		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");		
		Date fechaFormato = null;
		
		try {
			fechaFormato = format1.parse(fechaInicio);
			} catch (Exception ex) {
			ex.printStackTrace();
		}	
        
		GregorianCalendar fechaCalendario = new GregorianCalendar();
        fechaCalendario.setTime(fechaFormato);
        int diaSemana = (fechaCalendario.get(Calendar.DAY_OF_WEEK))-1;
		int diaSemanaG = diaSemana;
		
		int finalFecha=0;
		
		if(fechaFin!=null){
		finalFecha=Integer.parseInt(fechaFin.substring(0, 2));		
		}
		
        logger.info("Dia de la semana que inicia el Mes: " + diaSemana);
        logger.info("Total de dias del Mes: " + finalFecha);

        logger.info("***************************DOS**************************************");
		
		
		for (int i = 0; i < 37; i++) {
			
			//lista.add(strDias[diaSemana]);
			diasSemana[i]=strDias[diaSemana];
			logger.info("Total dias de la semana " +diasSemana[i]);
			diaSemana++;
		}		
		
		
		if(!listaSucursales.isEmpty()){			
			
			for (int i = 0; i < listaSucursales.size(); i++) {
				listaAuxiliar=new ArrayList<ReporteDTO>();
				logger.info("listaSucursales... "+i+ ": " + listaSucursales.get(i).getIdCeco() + " ... " + listaSucursales.get(i).getDescCeco());
								
				if(!listaDatos.isEmpty()){
					logger.info("dentro del listaDatos.isEmpty() ");
					boolean banderaCompa=false;
					for (int j = 0; j < listaDatos.size(); j++) {
						logger.info("entre al for "+j+": y antes de comparar en if " +listaSucursales.get(i).getIdCeco() + " . "+listaDatos.get(j).getIdCeco());
						
						//******************************VALIDACION******************************//
						if(listaSucursales.get(i).getIdCeco().equals(listaDatos.get(j).getIdCeco())){	
							banderaCompa=true;
							if((j+1)!=listaDatos.size()){
								logger.info("entre al (j+1)!=listaDatos.size() y antes del comparacion en if ");
								if(listaSucursales.get(i).getIdCeco().equals(listaDatos.get(j+1).getIdCeco())){
									logger.info("despues del if listaSucursales.get(i).getIdCeco().equals(listaDatos.get(j+1).getIdCeco()) ");

									int iniciaCompara=j;
									//Reparar este cambio si es que vienen mas visitas mas de 2 ***************************************
									int numTotal=0;
									
									logger.info("tamaño listaDatos " + listaDatos.size());	
									logger.info("Fecha de la visita " + listaDatos.get(j).getSemana());					
									logger.info("Dia de la visita " + listaDatos.get(j).getDia());									
									logger.info("tamaño lista diasSemana " + diasSemana.length);
									int diaMenor=0;
									for (int k = 0; k < diasSemana.length; k++) {
										logger.info("entra a k " + k + ": " + diasSemana[k]);
										logger.info("entra a j " + j);
										ReporteDTO repo= new ReporteDTO();									
										diaMenor=(k+1)-diaSemanaG;
										if(j<listaDatos.size()){
											if(listaSucursales.get(i).getIdCeco().equals(listaDatos.get(j).getIdCeco())){
														
												logger.info("Valor de la semana "+listaDatos.get(j).getSemana());
												logger.info("Visito el dia:" + listaDatos.get(j).getDia());	
												logger.info("Valor de diaSemanaG:" + diaSemanaG);
												logger.info("Valor de diaMenor:" + diaMenor);
												
													
												if(k>=diaSemanaG && diaMenor<=finalFecha){
													
													
													logger.info("Compara dias:" + diaMenor   + " vs  dia semana " + listaDatos.get(j).getSemana());	
													
													if(Integer.parseInt(listaDatos.get(j).getSemana())==diaMenor){	
														logger.info("Hubo un llenado de No dias: "+ listaDatos.get(j).getNumero());	
														repo.setNombreCeco(listaSucursales.get(i).getDescCeco());
														repo.setNumero(listaDatos.get(j).getNumero());	
														repo.setImagen(listaDatos.get(j).getImagen());
														repo.setIdBitacora(listaDatos.get(j).getIdBitacora());
														//listaAuxiliar.add(repo);
														//numTotal=numTotal+repo.getNumero();
														j++;
													}else{	
														logger.info("No hubo llenado solo 0....  " + listaSucursales.get(i).getDescCeco());
														repo.setNombreCeco(listaSucursales.get(i).getDescCeco());
														repo.setImagen(listaDatos.get(j).getImagen());
														repo.setNumero(0);
														repo.setIdBitacora(0);
														//listaAuxiliar.add(repo);
														//numTotal=numTotal+repo.getNumero();														
													}
												}else{	
													logger.info("Hubo un llenado de NULL");
													repo.setNombreCeco("NULL");	
													repo.setImagen("0");
													repo.setNumero(0);	
													repo.setIdBitacora(0);
												}																	
											}else{
												logger.info("Acabaron los datos  y ya no hay datos en lista y llenara con 0 " + diaMenor +"  ...  "+finalFecha);
												if(diaMenor>finalFecha)
													repo.setNombreCeco("NULL");
												else
													repo.setNombreCeco(listaSucursales.get(i).getDescCeco());
												repo.setImagen("0");
												repo.setNumero(0);
												repo.setIdBitacora(0);
											}
										}else{
											logger.info("Acabaron los datos  y ya no hay datos en lista y llenara con 0 " + diaMenor +"  ...  "+finalFecha);
											if(diaMenor>finalFecha)
												repo.setNombreCeco("NULL");
											else
												repo.setNombreCeco(listaSucursales.get(i).getDescCeco());
											repo.setImagen("0");
											repo.setNumero(0);
											repo.setIdBitacora(0);
										}
										
										listaAuxiliar.add(repo);
										numTotal=numTotal+repo.getNumero();									
										
									}									
									
									/*ReporteDTO repo= new ReporteDTO();
									repo.setNombreCeco(listaSucursales.get(i).getDescCeco());	
									repo.setImagen(listaDatos.get(j).getImagen());
									repo.setTotal(numTotal);
									listaAuxiliar.add(repo);*/
									logger.info("Acabo el llenado de visitas... ");
									listaTotal.add(listaAuxiliar);
									break;
									
									
								}else{
									logger.info("else por que solo existe una visita de esta sucursal listaSucursales.get(i).getIdCeco().equals(listaDatos.get(j+1).getIdCeco())");
									int numTotal=0;								
									logger.info("Valor de la semana "+listaDatos.get(j).getSemana());
									logger.info("Visito el dia:" + listaDatos.get(j).getDia());	
									logger.info("Valor de diaSemanaG:" + diaSemanaG);	
									int diaMenor=0;
									for (int k = 0; k <37; k++) {
										
										if(k>=diaSemanaG && diaMenor<finalFecha){
											diaMenor=(k+1)-diaSemanaG;
											/*String diaMenorCom="0";
											if(diaMenor<10){
												diaMenorCom=diaMenorCom+diaMenor;											
											}else{
												diaMenorCom=""+diaMenor;
											}*/
											logger.info("Compara dias:" + diaMenor   + " vs  dia semana " + listaDatos.get(j).getSemana());	
											
											if(Integer.parseInt(listaDatos.get(j).getSemana())==diaMenor){	
												logger.info("Hubo un llenado de No dias: "+ listaDatos.get(j).getNumero());
												ReporteDTO repo= new ReporteDTO();											
												repo.setNombreCeco(listaSucursales.get(i).getDescCeco());	
												repo.setImagen(listaDatos.get(j).getImagen());
												repo.setNumero(listaDatos.get(j).getNumero());	
												repo.setIdBitacora(listaDatos.get(j).getIdBitacora());
												listaAuxiliar.add(repo);
												numTotal=numTotal+repo.getNumero();
											}else{
												ReporteDTO repo= new ReporteDTO();											
												repo.setNombreCeco(listaSucursales.get(i).getDescCeco());
												repo.setImagen(listaDatos.get(j).getImagen());
												repo.setNumero(0);		
												repo.setIdBitacora(0);
												listaAuxiliar.add(repo);
												numTotal=numTotal+repo.getNumero();
											}
										}else{
											ReporteDTO repo= new ReporteDTO();											
											repo.setNombreCeco("NULL");	
											repo.setImagen("0");
											repo.setNumero(0);	
											repo.setIdBitacora(0);
											listaAuxiliar.add(repo);
											numTotal=numTotal+repo.getNumero();
										}
										
									}
									
									ReporteDTO repo= new ReporteDTO();
									repo.setNombreCeco(listaSucursales.get(i).getDescCeco());	
									repo.setImagen(listaDatos.get(j).getImagen());
									repo.setTotal(numTotal);
									listaAuxiliar.add(repo);
									logger.info("Acabo el llenado de visitas y cambia de sucursal... ");
									listaTotal.add(listaAuxiliar);
								}								
							}else{
								logger.info("else que llena la ultima semana de la lista de datos");
								int numTotal=0;								
								logger.info("Valor de la semana "+listaDatos.get(j).getSemana());
								logger.info("Visito el dia:" + listaDatos.get(j).getDia());	
								logger.info("Valor de diaSemanaG:" + diaSemanaG);	
								int diaMenor=0;
								for (int k = 0; k <37; k++) {
									
									if(k>=diaSemanaG && diaMenor<finalFecha){
										diaMenor=(k+1)-diaSemanaG;
										/*String diaMenorCom="0";
										if(diaMenor<10){
											diaMenorCom=diaMenorCom+diaMenor;											
										}else{
											diaMenorCom=""+diaMenor;
										}*/
										logger.info("Compara dias:" + diaMenor   + " vs  dia semana " + listaDatos.get(j).getSemana());	
										
										if(Integer.parseInt(listaDatos.get(j).getSemana())==diaMenor){	
											logger.info("Hubo un llenado de No dias: "+ listaDatos.get(j).getNumero());
											ReporteDTO repo= new ReporteDTO();											
											repo.setNombreCeco(listaSucursales.get(i).getDescCeco());	
											repo.setImagen(listaDatos.get(j).getImagen());
											repo.setNumero(listaDatos.get(j).getNumero());	
											repo.setIdBitacora(listaDatos.get(j).getIdBitacora());
											listaAuxiliar.add(repo);
											numTotal=numTotal+repo.getNumero();
										}else{
											ReporteDTO repo= new ReporteDTO();											
											repo.setNombreCeco(listaSucursales.get(i).getDescCeco());
											repo.setImagen(listaDatos.get(j).getImagen());
											repo.setNumero(0);	
											repo.setIdBitacora(0);
											listaAuxiliar.add(repo);
											numTotal=numTotal+repo.getNumero();
										}
									}else{
										ReporteDTO repo= new ReporteDTO();											
										repo.setNombreCeco("NULL");	
										repo.setImagen("0");
										repo.setNumero(0);	
										repo.setIdBitacora(0);
										listaAuxiliar.add(repo);
										numTotal=numTotal+repo.getNumero();
									}
									
								}
								
								ReporteDTO repo= new ReporteDTO();
								repo.setNombreCeco(listaSucursales.get(i).getDescCeco());	
								repo.setImagen(listaDatos.get(j).getImagen());
								repo.setTotal(numTotal);
								listaAuxiliar.add(repo);
								logger.info("Acabo el llenado de visitas y cambia de sucursal... ");
								listaTotal.add(listaAuxiliar);
								
								
							}
						}else{	//******************************VALIDACION******************************//
							banderaCompa=false;
						}						
					}
					if(!banderaCompa){
						logger.info("Llena la sucursal actual con ceros por que no existen visitas");
						int numTotal=0;				
						logger.info("Valor de diaSemanaG:" + diaSemanaG);	
						int diaMenor=0;
						for (int k = 0; k <37; k++) {				
							if(k>=diaSemanaG && diaMenor<finalFecha){
								diaMenor=(k+1)-diaSemanaG;					
									ReporteDTO repo= new ReporteDTO();											
									repo.setNombreCeco("Sin Visitas");
									repo.setImagen("0");
									repo.setNumero(0);	
									repo.setIdBitacora(0);
									listaAuxiliar.add(repo);
									numTotal=numTotal+repo.getNumero();
							}else{
								ReporteDTO repo= new ReporteDTO();											
								repo.setNombreCeco("NULL");	
								repo.setImagen("0");
								repo.setNumero(0);	
								repo.setIdBitacora(0);
								listaAuxiliar.add(repo);
								numTotal=numTotal+repo.getNumero();
							}				
						}
						listaTotal.add(listaAuxiliar);
					}
				}else{
					logger.info("Llena todas las sucursales con ceros por que no existen ninguna visita");
					int numTotal=0;				
					logger.info("Valor de diaSemanaG:" + diaSemanaG);	
					int diaMenor=0;
					for (int k = 0; k <37; k++) {				
						if(k>=diaSemanaG && diaMenor<finalFecha){
							diaMenor=(k+1)-diaSemanaG;					
								ReporteDTO repo= new ReporteDTO();											
								repo.setNombreCeco("Sin Visitas");
								repo.setImagen("0");
								repo.setNumero(0);		
								repo.setIdBitacora(0);
								listaAuxiliar.add(repo);
								numTotal=numTotal+repo.getNumero();
						}else{
							ReporteDTO repo= new ReporteDTO();											
							repo.setNombreCeco("NULL");	
							repo.setImagen("0");
							repo.setNumero(0);		
							repo.setIdBitacora(0);
							listaAuxiliar.add(repo);
							numTotal=numTotal+repo.getNumero();
						}				
					}
					listaTotal.add(listaAuxiliar);
				}		
			}
		}else{
			logger.info("No existen sucursales ");
			listaAuxiliar=new ArrayList<ReporteDTO>();
			int numTotal=0;				
			logger.info("Valor de diaSemanaG:" + diaSemanaG);	
			int diaMenor=0;
			for (int k = 0; k <37; k++) {				
				if(k>=diaSemanaG && diaMenor<finalFecha){
					diaMenor=(k+1)-diaSemanaG;					
						ReporteDTO repo= new ReporteDTO();											
						repo.setNombreCeco("Sin Visitas");
						repo.setImagen("0");
						repo.setNumero(0);	
						repo.setIdBitacora(0);
						listaAuxiliar.add(repo);
						numTotal=numTotal+repo.getNumero();
				}else{
					ReporteDTO repo= new ReporteDTO();											
					repo.setNombreCeco("NULL");	
					repo.setImagen("0");
					repo.setNumero(0);	
					repo.setIdBitacora(0);
					listaAuxiliar.add(repo);
					numTotal=numTotal+repo.getNumero();
				}				
			}
			listaTotal.add(listaAuxiliar);
		}	
		return listaTotal;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/ajaxNegocioList", method = RequestMethod.GET)
	public @ResponseBody List<NegocioDTO> getNegocioList(
			@RequestParam(value = "varIdPregunta", required = true, defaultValue = "varIdPregunta") String varIdPregunta, HttpServletRequest request) throws Exception {
		//Fragmento para seleccionar el filtrado de tiendas
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		List<NegocioDTO> negocio=null;
		if(Integer.parseInt(userSession.getIdUsuario())>0){
			Map<String, Object> map= new HashMap<String, Object>();
			map= filtrosCecoBI.obtieneFiltros(Integer.parseInt(userSession.getIdUsuario()));
			logger.info("map... " + map);
			negocio = (List<NegocioDTO>) map.get("negocio");
			logger.info("Resultado de ajaxNegocioList " + negocio);
		}else{
			logger.info("No existe sesion de usuario y regresa null en ajaxNegocioList... ");
		}
		return negocio;
	}
		
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/getInicioCanal", method = RequestMethod.GET)
	public @ResponseBody List getInicioCanal(HttpServletRequest request) throws Exception {
		
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		List<NegocioDTO> negocio=null;
		if(Integer.parseInt(userSession.getIdUsuario())>0){
			Map<String, Object> map= new HashMap<String, Object>();
			map= filtrosCecoBI.obtieneFiltros(Integer.parseInt(userSession.getIdUsuario()));
			logger.info("map... " + map);
			negocio = (List<NegocioDTO>) map.get("negocio");
			logger.info("Resultado de getInicioCanal " + negocio);
		}else{
			logger.info("No existe sesion de usuario y regresa null en ajaxNegocioList... ");
		}
		return negocio;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/getListaReporteOperacion", method = RequestMethod.GET)
	public @ResponseBody List<List> getListaReporteOperacion(
			@RequestParam(value = "idCheck", required = true, defaultValue = "null") String idCheck,
			@RequestParam(value = "fechaInicio", required = true, defaultValue = "null") String fechaInicio,
			@RequestParam(value = "fechaFin", required = true, defaultValue = "null") String fechaFin,
			@RequestParam(value = "idCanal", required = true, defaultValue = "null") String idCanal,
			@RequestParam(value = "idPais", required = true, defaultValue = "null") String idPais,
			@RequestParam(value = "territorio", required = true, defaultValue = "null") String territorio,
			@RequestParam(value = "zona", required = true, defaultValue = "null") String zona,
			@RequestParam(value = "region", required = true, defaultValue = "null") String region,
			@RequestParam(value = "ceco", required = true, defaultValue = "null") String ceco, HttpServletRequest request) throws Exception {
		
		if(idCheck.equals("null") || idCheck.equals("Selecciona una Opcion"))
			idCheck="0";
		if(fechaInicio.equals("null"))
			fechaInicio=null;
		if(fechaFin.equals("null"))
			fechaFin=null;
		if(territorio.equals("null"))
			territorio=null;
		if(zona.equals("null"))
			zona=null;
		if(region.equals("null"))
			region=null;
		if(ceco.equals("null"))
			ceco=null;
		
		//logger.info(" reporteBI.ReporteOperacion... "+ FRQConstantes.getNumeroCheck());
		logger.info(" idCheck... "+  idCheck);
		logger.info(" fecha... "+ fechaInicio);
		logger.info(" fecha... "+ fechaFin);
		logger.info("idCanal______ "+ idCanal);
		logger.info("idPais______ "+ idPais);
		logger.info(" territorio... "+  territorio);
		logger.info(" zona... "+ zona);
		logger.info(" region... "+  region);
		logger.info(" ceco... "+  ceco);
				
		Map<String, Object> map= new HashMap<String, Object>();
		map=reporteBI.ReporteOperacion(Integer.parseInt(idCheck), fechaInicio,fechaFin,idPais,idCanal, territorio, zona, region, ceco);
		logger.info("map... " + map);
		List<ReporteDTO> listaReporte = (List<ReporteDTO>) map.get("listaReporte");
		int conteo =  (Integer) map.get("conteo");
		
		logger.info("listaReporte... " + listaReporte);
		logger.info("pais... " + conteo);
		
		List<Integer> listaConteo = new ArrayList<Integer>();
		listaConteo.add(conteo);
	
		List<List> listaFinal= new ArrayList<List>(); 
		listaFinal.add(listaReporte);
		listaFinal.add(listaConteo);
		
		logger.info("Resultado de getListaReporteOperacion " + listaFinal);
			
		return listaFinal;
	}

	@SuppressWarnings({ "rawtypes" })
	@RequestMapping(value = "/getCambioFecha", method = RequestMethod.GET)
	public @ResponseBody List getCambioFecha(
			@RequestParam(value = "fecha", required = true, defaultValue = "null") String fecha,
			@RequestParam(value = "opcion", required = true, defaultValue = "0") int opcion, HttpServletRequest request) throws Exception {
				
		List<String> fechaRetorno=new ArrayList<String>();
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");		
		Date fechaFormato = null;
		
		logger.info("Fecha " + fecha);
		logger.info("Opcion " + opcion);
		
		try {
			fechaFormato = format1.parse(fecha);
			} catch (Exception ex) {
			ex.printStackTrace();
		}		
		
		calendar.setTime(fechaFormato); // Configuramos la fecha que se recibe
		
		if(opcion==0)
			calendar.add(Calendar.DAY_OF_YEAR, 13);  // numero de días a restar
		if(opcion==1)
			calendar.add(Calendar.DAY_OF_YEAR, -13);  // numero de días a añadir
  		
		fechaRetorno.add(format1.format(calendar.getTime()));
		
		logger.info("Resultado de getCambioFecha " + fechaRetorno);
				
		return fechaRetorno;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked"})
	@RequestMapping(value = "/ajaxIdHijoVisitaTituloTabla", method = RequestMethod.GET)
	public @ResponseBody List getIdHijoVisitaTituloTabla(
			@RequestParam(value = "fecha", required = true, defaultValue = "0") String fecha, HttpServletRequest request) throws Exception {
		
		//logger.info("Fecha ajaxIdHijoVisitaTituloTabla " + fecha);
		
		String[] strDias = new String[]{"D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S"};
		String[] diasSemana = new String[14];
		List lista= new ArrayList();
		
		//String fechaFinal="";
		
		/*
		Calendar now = Calendar.getInstance();
		// El dia de la semana inicia en el 1 mientras que el array empieza en el 0
		
		int numDia=now.get(Calendar.DAY_OF_WEEK);
		logger.info("dia de la semana " + numDia);
		*/
        logger.info("*****************************************************************");
		
		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");		
		Date fechaFormato = null;
		

		try {
			fechaFormato = format1.parse(fecha);
			} catch (Exception ex) {
			ex.printStackTrace();
		}	
        
        
		GregorianCalendar fechaCalendario = new GregorianCalendar();
        fechaCalendario.setTime(fechaFormato);
        int diaSemana = fechaCalendario.get(Calendar.DAY_OF_WEEK);
		
        logger.info("diaSemana ajaxIdHijoVisitaTituloTabla: " + diaSemana);

        logger.info("*****************************************************************");
		
		
		for (int i = 0; i < 14; i++) {
			
			lista.add(strDias[diaSemana]);
			diasSemana[i]=strDias[diaSemana];
			logger.info("Total dias de la semana " +diasSemana[i]);
			diaSemana++;
		}		
		logger.info("Resultado de ajaxIdHijoVisitaTituloTabla " + lista);
		
		return lista;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/ajaxVistaResumen", method = RequestMethod.GET)
	public @ResponseBody List<List> getVistaResumen(
			@RequestParam(value = "idBitacora", required = true, defaultValue = "0") String idBitacora, HttpServletRequest request) throws Exception {
		
		logger.info("Entra a ejecutar el Mapa ReporteDetRegional");
		Map<String, Object> map=null;
		try {
			map=reporteBI.ReporteDetRegional(Integer.parseInt(idBitacora));
		} catch (Exception e) {
			logger.info("No se obtuvo el Mapa ReporteDetRegional "+e);
		}
		
		logger.info("Salio del Mapa ReporteDetRegional");
		
		List<List> lista= new ArrayList<List>();
		List listaAux= new ArrayList();
		List<ChecklistDTO> listaChecklist = null;
		List<EvidenciaDTO> listaEvidencias = null;
		List<PreguntaDTO> listaFolios = null;
		List<CompromisoDTO> listaCompromisos=null;
		
		int totalEvidencias=0;
		int totalFolios=0;
		if(map!=null){
			totalEvidencias= (Integer) map.get("totalEvidencias");
			totalFolios= (Integer) map.get("totalFolios");
			listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
			listaEvidencias= (List<EvidenciaDTO>) map.get("listaEvidencias");
			listaFolios=(List<PreguntaDTO>) map.get("listaFolios");
		}
		
		listaAux.add(totalEvidencias);
		listaAux.add(totalFolios);
		
		lista.add(listaAux);//Posicion 0
		lista.add(listaChecklist);//Posicion 1
		lista.add(listaEvidencias);//Posicion 2
		lista.add(listaFolios);//Posicion 3
		
		try {
			listaCompromisos=reporteBI.ReporteDetPreguntasRegional(Integer.parseInt(idBitacora));
		} catch (Exception e) {
			logger.info("No se obtuvo la lista ReporteDetPreguntasRegional: "+e);
		}		
		logger.info("Salio de la lista ReporteDetPreguntasRegional");
		lista.add(listaCompromisos);//Posicion 4
		return lista;
	}
	
	@SuppressWarnings({ "rawtypes", "unused" })
	@RequestMapping(value = "/ajaxReporteDetReg", method = RequestMethod.GET)
	public @ResponseBody List<List> getReporteDetReg(
			@RequestParam(value = "idCheck", required = true, defaultValue = "null") String idCheck,
			@RequestParam(value = "fechaInicio", required = true, defaultValue = "null") String fechaInicio,
			@RequestParam(value = "fechaFin", required = true, defaultValue = "null") String fechaFin,
			@RequestParam(value = "idCecoPadre", required = true, defaultValue = "null") String idCecoPadre, HttpServletRequest request) throws Exception {
		
		if(idCheck.equals("null") || idCheck.equals("Selecciona una Opcion"))
			idCheck="0";
		if(fechaInicio.equals("null"))
			fechaInicio=null;
		if(fechaFin.equals("null"))
			fechaFin=null;

		List<List> listaTotal= new ArrayList<List>();
		List<ReporteDTO> listaAuxiliar= null;
		List<ReporteDTO> listaDatos = null;
		logger.info("datos antes de la lista... ");
		//logger.info("FRQConstantes.getNumeroCheck()... " + FRQConstantes.getNumeroCheck());
		logger.info("idCheck... " + idCheck);
		logger.info("idCecoPadre... " +  idCecoPadre);
		logger.info("fechaInicio... " +  fechaInicio);
		logger.info("fechaFin... " +  fechaFin);
		listaDatos= reporteBI.ReporteDetReg(Integer.parseInt(idCheck), Integer.parseInt(idCecoPadre), fechaInicio, fechaFin);
		logger.info("lista... " + listaDatos);
		
		String[] strDias = new String[]{"D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S","D","L","M","Mi","J","V","S"};
		String[] diasSemana = new String[37];
		logger.info("**************************REGIONALES***************************************");
		
		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");		
		Date fechaFormato = null;
		
		try {
			fechaFormato = format1.parse(fechaInicio);
			} catch (Exception ex) {
			ex.printStackTrace();
		}	
        
		GregorianCalendar fechaCalendario = new GregorianCalendar();
        fechaCalendario.setTime(fechaFormato);
        int diaSemana = (fechaCalendario.get(Calendar.DAY_OF_WEEK))-1;
		int diaSemanaG = diaSemana;
		
		int finalFecha=0;
		if(fechaFin!=null){
			finalFecha=Integer.parseInt(fechaFin.substring(0, 2));		
		}
			
        logger.info("Dia de la semana que inicia el Mes: " + diaSemana);
        logger.info("Total de dias del Mes: " + finalFecha);

        logger.info("***************************REGIONALES**************************************");
		
		
		for (int i = 0; i < 37; i++) {
			
			//lista.add(strDias[diaSemana]);
			diasSemana[i]=strDias[diaSemana];
			logger.info("Total dias de la semana " +diasSemana[i]);
			diaSemana++;
		}		
		
		
	
		listaAuxiliar=new ArrayList<ReporteDTO>();
						
		if(!listaDatos.isEmpty()){
			logger.info("dentro del listaDatos.isEmpty() ");
			logger.info("tamaño listaDatos " + listaDatos.size());	
			for (int j = 0; j < listaDatos.size(); j++) {
				
				//******************************VALIDACION******************************//
				logger.info("Dia de la visita " + listaDatos.get(j).getSemana());
				int numTotal=0;
				int diaMenor=0;
				for (int k = 0; k < diasSemana.length; k++) {
					logger.info("entra a k " + k + ": " + diasSemana[k]);
					logger.info("entra a j " + j);
					ReporteDTO repo= new ReporteDTO();									
					diaMenor=(k+1)-diaSemanaG;
									
							logger.info("Valor de diaSemanaG:" + diaSemanaG);
							logger.info("Valor de diaMenor:" + diaMenor);
							
								
							if(k>=diaSemanaG && diaMenor<=finalFecha){
								
								if(j<listaDatos.size()){

									logger.info("Compara dias:" + diaMenor   + " vs  dia semana " + listaDatos.get(j).getSemana());	
									
									if(Integer.parseInt(listaDatos.get(j).getSemana())==diaMenor){	
										logger.info("Hubo un llenado de No dias: "+ idCecoPadre);	
										repo.setNombreCeco(idCecoPadre);
										repo.setNumero(1);	
										repo.setImagen("0");
										repo.setObservaciones(listaDatos.get(j).getIdCeco());
										//listaAuxiliar.add(repo);
										//numTotal=numTotal+repo.getNumero();
										j++;
									}else{	
										logger.info("No hubo llenado solo 0 ....  ");
										repo.setNombreCeco(idCecoPadre);
										repo.setImagen("0");
										repo.setNumero(0);
										repo.setObservaciones("0");
										//listaAuxiliar.add(repo);
										//numTotal=numTotal+repo.getNumero();														
									}
								}else{	
									logger.info("Hubo un llenado de NULL");
									repo.setNombreCeco(idCecoPadre);	
									repo.setImagen("0");
									repo.setNumero(0);	
									repo.setObservaciones("0");
								}
							}else{	
								logger.info("Hubo un llenado de NULL");
								repo.setNombreCeco("NULL");	
								repo.setImagen("0");
								repo.setNumero(0);	
								repo.setObservaciones("0");
							}																	

					listaAuxiliar.add(repo);
					numTotal=numTotal+repo.getNumero();									
					
				}									
				
				logger.info("Acabo el llenado de visitas... ");
				listaTotal.add(listaAuxiliar);
				break;
							
														
				//******************************VALIDACION******************************//												
			}
		}else{
			logger.info("Llena todas las sucursales con ceros por que no existen ninguna visita");
			int numTotal=0;				
			logger.info("Valor de diaSemanaG:" + diaSemanaG);	
			int diaMenor=0;
			for (int k = 0; k <37; k++) {				
				if(k>=diaSemanaG && diaMenor<finalFecha){
					diaMenor=(k+1)-diaSemanaG;					
						ReporteDTO repo= new ReporteDTO();											
						repo.setNombreCeco("Sin Visitas");
						repo.setImagen("0");
						repo.setNumero(0);		
						repo.setObservaciones("0");
						listaAuxiliar.add(repo);
						numTotal=numTotal+repo.getNumero();
				}else{
					ReporteDTO repo= new ReporteDTO();											
					repo.setNombreCeco("NULL");	
					repo.setImagen("0");
					repo.setNumero(0);		
					repo.setObservaciones("0");
					listaAuxiliar.add(repo);
					numTotal=numTotal+repo.getNumero();
				}				
			}
			listaTotal.add(listaAuxiliar);
		}		
			
		
		logger.info("listaTotal "+ listaTotal);
		return listaTotal;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "central/vistaCumplimientoMovil.htm", method = RequestMethod.GET)
	public ModelAndView vistaCumplimientoMovil(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		//return new ModelAndView("redirect:" + "vistaCumplimientoVisitas.htm");
		
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		//userSession.setIdUsuario("189870");
		logger.info("USUARIO "+ userSession.getIdUsuario());
		
		if(Integer.parseInt(userSession.getIdUsuario())>0){ 
			ModelAndView mv = new ModelAndView("vistaReporteMovil");
			mv.addObject("idUsuario",userSession.getIdUsuario());
			mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
			
			//Fragmento para seleccionar el filtrado de tiendas
			List<NegocioDTO> negocio=null;
			List<PaisDTO> pais=null;
			List<FiltrosCecoDTO> territorio=null;
			List<FiltrosCecoDTO> zona=null;
			List<FiltrosCecoDTO> region=null;
			Map<String, Object> map= new HashMap<String, Object>();
			map= filtrosCecoBI.obtieneFiltros(Integer.parseInt(userSession.getIdUsuario()));
			logger.info("map... " + map);
			negocio = (List<NegocioDTO>) map.get("negocio");
			pais = (List<PaisDTO>) map.get("pais");
			territorio = (List<FiltrosCecoDTO>) map.get("territorio");
			zona = (List<FiltrosCecoDTO>) map.get("zona");
			region = (List<FiltrosCecoDTO>) map.get("region");
			logger.info("negocio... " + negocio.get(0).getDescripcion());
			logger.info("pais... " + pais.get(0).getNombre());
			logger.info("territorio... " + territorio.get(0).getNombreCeco());
			logger.info("zona... " + zona.get(0).getNombreCeco());
			logger.info("region... " + region.get(0).getNombreCeco());
			
			try {
				List<PaisDTO> listaPaisAux = filtrosCecoBI.obtienePaises(negocio.get(0).getIdNegocio());
				mv.addObject("paisIdAux", listaPaisAux.get(0).getIdPais());
				mv.addObject("paisNameAux", listaPaisAux.get(0).getNombre());
			} catch (Exception e) {
				logger.info("No hay lista en el pais");
			}
			
			
			int nivelAdmin=4;
			String regionId=null;
			String zonaId=null;
			String territorioId=null;
			int paisId=0;
			int negocioId=0;
					
			String regionName=null;
			String zonaName=null;
			String territorioName=null;
			String paisName=null;
			String negocioName=null;
			
			if(negocio.get(0).getDescripcion()!=null){
				negocioId=negocio.get(0).getIdNegocio();
				mv.addObject("negocioId", negocioId);
				negocioName=negocio.get(0).getDescripcion();
				mv.addObject("negocioName", negocioName);
				nivelAdmin=4;
				
				if(pais.get(0).getNombre()!=null){
					paisId=pais.get(0).getIdPais();
					mv.addObject("paisId", paisId);
					paisName=pais.get(0).getNombre();
					mv.addObject("paisName", paisName);
					nivelAdmin=3;
					if(territorio.get(0).getNombreCeco()!=null){
						
						if(zona.size()>1){
							territorioId=territorio.get(0).getIdCeco();
							mv.addObject("territorioId", territorioId);
							territorioName=territorio.get(0).getNombreCeco();
							mv.addObject("territorioName", territorioName);
							nivelAdmin=2;
						}else{
							territorioId=territorio.get(0).getIdCeco();
							mv.addObject("territorioId", territorioId);
							territorioName=territorio.get(0).getNombreCeco();
							mv.addObject("territorioName", territorioName);
							nivelAdmin=2;
							if(zona.get(0).getNombreCeco()!=null){
								
								if(region.size()>1){
									zonaId=zona.get(0).getIdCeco();
									mv.addObject("zonaId", zonaId);
									zonaName=zona.get(0).getNombreCeco();
									mv.addObject("zonaName", zonaName);
									nivelAdmin=1;
								}else{
									zonaId=zona.get(0).getIdCeco();
									mv.addObject("zonaId", zonaId);
									zonaName=zona.get(0).getNombreCeco();
									mv.addObject("zonaName", zonaName);
									nivelAdmin=1;
									if(region.get(0).getNombreCeco()!=null){
										regionId=region.get(0).getIdCeco();
										mv.addObject("regionId", regionId);
										regionName=region.get(0).getNombreCeco();
										mv.addObject("regionName", regionName);
										nivelAdmin=0;
									}else{									
										mv.addObject("regionId", regionId);
										mv.addObject("regionName", regionName);
									}
								}							
							}else{							
								mv.addObject("zonaId", zonaId);
								mv.addObject("zonaName", zonaName);
								mv.addObject("regionId", regionId);
								mv.addObject("regionName", regionName);
							}
						}					
					}else{					
						mv.addObject("territorioId", territorioId);
						mv.addObject("territorioName", territorioName);
						mv.addObject("zonaId", zonaId);
						mv.addObject("zonaName", zonaName);
						mv.addObject("regionId", regionId);
						mv.addObject("regionName", regionName);
					}
				}else{				
					mv.addObject("paisId", paisId);
					mv.addObject("paisName", paisName);
					mv.addObject("territorioId", territorioId);
					mv.addObject("territorioName", territorioName);
					mv.addObject("zonaId", zonaId);
					mv.addObject("zonaName", zonaName);
					mv.addObject("regionId", regionId);
					mv.addObject("regionName", regionName);
				}
			}else{
				mv.addObject("negocioId", negocioId);
				mv.addObject("negocioName", negocioName);
				mv.addObject("paisId", paisId);
				mv.addObject("paisName", paisName);
				mv.addObject("territorioId", territorioId);
				mv.addObject("territorioName", territorioName);
				mv.addObject("zonaId", zonaId);
				mv.addObject("zonaName", zonaName);
				mv.addObject("regionId", regionId);
				mv.addObject("regionName", regionName);
			}	
			mv.addObject("nivelAdmin", nivelAdmin);
			return mv;
		}else{
			ModelAndView mv = new ModelAndView("http404");
			return mv;
		}
		
		
				
	}	
}