package com.gruposalinas.checklist.domain;

public class ConteoGenExpDTO {
	

		private int idNegocio;
		private int idPreg;
		private int idModulo;
		private int critica;
		private int tipoPreg;
		private int pregPadre;
		private int idCheck;
		private int version;
		private double pondTotal;
		private double ponderacion;
		private String pregunta;
		private String  area;
		private String  nomCheck;
		private String  clasifica;
		private String  posible;
		private String  negocio;
		private String  obs;
		private String  fcResp;
		private String  aux;
		private int  aux2;
		private int  bitaGral;
		private int  bitacora;
		private int  idResp;
		public int getIdNegocio() {
			return idNegocio;
		}
		public void setIdNegocio(int idNegocio) {
			this.idNegocio = idNegocio;
		}
		public int getIdPreg() {
			return idPreg;
		}
		public void setIdPreg(int idPreg) {
			this.idPreg = idPreg;
		}
		public int getIdModulo() {
			return idModulo;
		}
		public void setIdModulo(int idModulo) {
			this.idModulo = idModulo;
		}
		public int getCritica() {
			return critica;
		}
		public void setCritica(int critica) {
			this.critica = critica;
		}
		public int getTipoPreg() {
			return tipoPreg;
		}
		public void setTipoPreg(int tipoPreg) {
			this.tipoPreg = tipoPreg;
		}
		public int getPregPadre() {
			return pregPadre;
		}
		public void setPregPadre(int pregPadre) {
			this.pregPadre = pregPadre;
		}
		public int getIdCheck() {
			return idCheck;
		}
		public void setIdCheck(int idCheck) {
			this.idCheck = idCheck;
		}
		public int getVersion() {
			return version;
		}
		public void setVersion(int version) {
			this.version = version;
		}
		public double getPondTotal() {
			return pondTotal;
		}
		public void setPondTotal(double pondTotal) {
			this.pondTotal = pondTotal;
		}
		public double getPonderacion() {
			return ponderacion;
		}
		public void setPonderacion(double ponderacion) {
			this.ponderacion = ponderacion;
		}
		public String getPregunta() {
			return pregunta;
		}
		public void setPregunta(String pregunta) {
			this.pregunta = pregunta;
		}
		public String getArea() {
			return area;
		}
		public void setArea(String area) {
			this.area = area;
		}
		public String getNomCheck() {
			return nomCheck;
		}
		public void setNomCheck(String nomCheck) {
			this.nomCheck = nomCheck;
		}
		public String getClasifica() {
			return clasifica;
		}
		public void setClasifica(String clasifica) {
			this.clasifica = clasifica;
		}
		public String getPosible() {
			return posible;
		}
		public void setPosible(String posible) {
			this.posible = posible;
		}
		public String getNegocio() {
			return negocio;
		}
		public void setNegocio(String negocio) {
			this.negocio = negocio;
		}
		public String getObs() {
			return obs;
		}
		public void setObs(String obs) {
			this.obs = obs;
		}
		public String getFcResp() {
			return fcResp;
		}
		public void setFcResp(String fcResp) {
			this.fcResp = fcResp;
		}
		public String getAux() {
			return aux;
		}
		public void setAux(String aux) {
			this.aux = aux;
		}
		public int getAux2() {
			return aux2;
		}
		public void setAux2(int aux2) {
			this.aux2 = aux2;
		}
		public int getBitaGral() {
			return bitaGral;
		}
		public void setBitaGral(int bitaGral) {
			this.bitaGral = bitaGral;
		}
		public int getBitacora() {
			return bitacora;
		}
		public void setBitacora(int bitacora) {
			this.bitacora = bitacora;
		}
		public int getIdResp() {
			return idResp;
		}
		public void setIdResp(int idResp) {
			this.idResp = idResp;
		}
		@Override
		public String toString() {
			return "ConteoGenExpDTO [idNegocio=" + idNegocio + ", idPreg=" + idPreg + ", idModulo=" + idModulo
					+ ", critica=" + critica + ", tipoPreg=" + tipoPreg + ", pregPadre=" + pregPadre + ", idCheck="
					+ idCheck + ", version=" + version + ", pondTotal=" + pondTotal + ", ponderacion=" + ponderacion
					+ ", pregunta=" + pregunta + ", area=" + area + ", nomCheck=" + nomCheck + ", clasifica="
					+ clasifica + ", posible=" + posible + ", negocio=" + negocio + ", obs=" + obs + ", fcResp="
					+ fcResp + ", aux=" + aux + ", aux2=" + aux2 + ", bitaGral=" + bitaGral + ", bitacora=" + bitacora
					+ ", idResp=" + idResp + "]";
		}
		

		
	}