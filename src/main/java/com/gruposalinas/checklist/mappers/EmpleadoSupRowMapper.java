package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.AdminSupDTO;
import com.gruposalinas.checklist.domain.AsignacionesSupDTO;

public class EmpleadoSupRowMapper implements RowMapper<AdminSupDTO> {

	public AdminSupDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AdminSupDTO asignaDTO = new AdminSupDTO();
		
		asignaDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
		asignaDTO.setNomUsuario(rs.getString("FCNOMBRE"));
		
		return asignaDTO;
		
		
	}

}
