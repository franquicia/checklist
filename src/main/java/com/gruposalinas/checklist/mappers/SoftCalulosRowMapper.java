package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.SoftNvoDTO;

public class SoftCalulosRowMapper implements RowMapper<SoftNvoDTO> {

	public SoftNvoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		SoftNvoDTO SoftNvoDTO = new SoftNvoDTO();
		
		SoftNvoDTO.setIdCalculo(rs.getInt("FIID_CALC"));
		SoftNvoDTO.setIdSoft(rs.getInt("FIID_SOFTN"));
	    SoftNvoDTO.setClasificacion(rs.getString("FCCLASIF"));
	    SoftNvoDTO.setItemSi(rs.getInt("FIID_SI"));
	    SoftNvoDTO.setItemNo(rs.getInt("FIID_NO"));
	    SoftNvoDTO.setItemNa(rs.getInt("FIID_NA"));
	    SoftNvoDTO.setItemrevisados(rs.getInt("FIID_REVISA"));
	    SoftNvoDTO.setItemTotales(rs.getInt("FIID_ITEMTOT"));
	    SoftNvoDTO.setItemImperd(rs.getInt("FIID_IMP"));
	    SoftNvoDTO.setPorcentajeReSi(rs.getDouble("FCPORCRESPSI"));
	    SoftNvoDTO.setPorcentajeReNo(rs.getDouble("FCPORCRESPNO"));
	    SoftNvoDTO.setPorcentajeToSi(rs.getDouble("FCPORCSI"));
	    SoftNvoDTO.setPorcentajeToNo(rs.getDouble("FCPORCNO"));
	    SoftNvoDTO.setPorcentajeToNA(rs.getDouble("FCPORCNA"));
	    SoftNvoDTO.setPonderacionMaximaReal(rs.getDouble("FCPONDMAXREAL"));
	    SoftNvoDTO.setPonderacionObtenidaReal(rs.getDouble("FCPONDOBTREAL"));
	    SoftNvoDTO.setPonderacionMaximaCalculada(rs.getDouble("FCPONDMAXCAL"));
	    SoftNvoDTO.setPonderacionObtenidaCalculada(rs.getDouble("FCPONDOBTCAL"));
	    SoftNvoDTO.setCalificacionReal(rs.getDouble("FCCALREAL"));
	    SoftNvoDTO.setCalificacionCalculada(rs.getDouble("FCALCALCU"));
	    SoftNvoDTO.setPeriodo(rs.getString("FCPERIODO"));
           
		return SoftNvoDTO;
	}

}


