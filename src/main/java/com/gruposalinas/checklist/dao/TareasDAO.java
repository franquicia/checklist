package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.TareaDTO;

public interface TareasDAO {
	
	public boolean insertaTarea(TareaDTO bean)throws Exception;
	
	public boolean actualizaTarea(TareaDTO bean)throws Exception;
	
	public boolean eliminaTarea(int idTarea)throws Exception;
	
	public List<TareaDTO> obtieneTareas()throws Exception;
	
	public List<TareaDTO> obtieneActivas() throws Exception; 
	
}
