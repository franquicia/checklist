package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.CheckAutoProtoDTO;
import com.gruposalinas.checklist.mappers.CheckAutoProtoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class CheckAutoProtoDAOImpl extends DefaultDAO implements CheckAutoProtoDAO {

    private static Logger logger = LogManager.getLogger(CheckAutoProtoDAOImpl.class);

    DefaultJdbcCall jdbcObtieneAutorizacion;
    DefaultJdbcCall jdbcInsertaAutor;
    DefaultJdbcCall jdbcActualizaAutor;
    DefaultJdbcCall jdbcActualizaStatus;
    DefaultJdbcCall jdbcEliminaAutor;

    public void init() {

        jdbcObtieneAutorizacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMAUTPROADM")
                .withProcedureName("SP_SEL_USU")
                .returningResultSet("RCL_CHECK", new CheckAutoProtoRowMapper());

        jdbcInsertaAutor = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMAUTPROADM")
                .withProcedureName("SP_INS_AUT");

        jdbcActualizaAutor = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMAUTPROADM")
                .withProcedureName("SP_ACT_AUT");

        jdbcActualizaStatus = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMAUTPROADM")
                .withProcedureName("SP_ACT_STAT");

        jdbcEliminaAutor = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMAUTPROADM")
                .withProcedureName("SP_DEL_AUT");
    }

    @SuppressWarnings("unchecked")
    public List<CheckAutoProtoDTO> obtieneAutoProto(String idTab) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<CheckAutoProtoDTO> listaCompromiso = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECK", idTab);

        out = jdbcObtieneAutorizacion.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMAUTPROADM.SP_SEL_COMP}");

        listaCompromiso = (List<CheckAutoProtoDTO>) out.get("RCL_CHECK");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el la autorizacion con id(" + idTab + ")");
        } else {
            return listaCompromiso;
        }

        return null;
    }

    public int insertaAutoProto(CheckAutoProtoDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idCompromiso = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_VERSION", bean.getFiversion())
                .addValue("PA_USU", bean.getIdUsu())
                .addValue("PA_DETALLE", bean.getDetalle())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_TIPOUS", bean.getUsuTipo())
                .addValue("PA_FECHALIBERA", bean.getFechaLiberacion());

        out = jdbcInsertaAutor.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMAUTPROADM.SP_INS_COMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDAUT");
        idCompromiso = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Compromiso");
        } else {
            return idCompromiso;
        }

        return idCompromiso;
    }

    public boolean actualizaAutoProto(CheckAutoProtoDTO bean) throws Exception {

        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_VERSION", bean.getIdCheck())
                .addValue("PA_USU", bean.getIdUsu())
                .addValue("PA_DETALLE", bean.getDetalle())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_TIPOUS", bean.getUsuTipo())
                .addValue("PA_FECHALIBERA", bean.getFechaLiberacion())
                .addValue("PA_IDAUT", bean.getIdTab());

        out = jdbcActualizaAutor.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMAUTPROADM.SP_ACT_COMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el  id de la tabla( " + bean.getIdTab() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean actualizaAutoProtoStatus(CheckAutoProtoDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_VERSION", bean.getIdCheck())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_FECHALIBERA", bean.getFechaLiberacion())
                .addValue("PA_IDAUT", bean.getIdTab());

        out = jdbcActualizaAutor.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMAUTPROADM.SP_ACT_COMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el  id de la tabla( " + bean.getIdTab() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaAutoProto(int idTab) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDAUT", idTab);

        out = jdbcEliminaAutor.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMAUTPROADM.SP_DEL_COMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el Compromiso id(" + idTab + ")");
        } else {
            return true;
        }

        return false;
    }
}
