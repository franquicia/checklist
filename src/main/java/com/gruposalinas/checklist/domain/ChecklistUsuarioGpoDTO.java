package com.gruposalinas.checklist.domain;

public class ChecklistUsuarioGpoDTO {
	
	private String idUsuario;
	private String idCeco;
	private String idGrupo;
	
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	
	public String getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(String idGrupo) {
		this.idGrupo = idGrupo;
	}
	@Override
	public String toString() {
		return "ChecklistUsuarioGpoDTO [idUsuario=" + idUsuario + ", idCeco=" + idCeco + ", idGrupo=" + idGrupo + "]";
	}
	
	


}
