package com.gruposalinas.checklist.dao;

import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CargaCheckListDAOImpl extends DefaultDAO implements CargaCheckListDAO {

    private static Logger logger = LogManager.getLogger(CargaCheckListDAOImpl.class);

    DefaultJdbcCall jdbInsertaCheckList;

    public void init() {

        jdbInsertaCheckList = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_INSCHECKLST")
                .withProcedureName("SPINSCHECK");
        //.returningResultSet("CU_PUESTO", new CargaCheckListRowMapper());

    }

    @Override
    public boolean insertaCheckList() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        //.addValue("PA_EJECUCION", bean.getEjecucion());
        out = jdbInsertaCheckList.execute();

        //logger.info("Funcion ejecutada:{FRANQUICIA.PA_INSCHECKLST.SPINSCHECK}");
        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar el protocolo");
        } else {
            return true;

        }

        return false;
    }

}
