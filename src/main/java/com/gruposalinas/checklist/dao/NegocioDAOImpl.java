package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.NegocioDTO;
import com.gruposalinas.checklist.mappers.NegocioRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class NegocioDAOImpl extends DefaultDAO implements NegocioDAO {

    private static Logger logger = LogManager.getLogger(NegocioDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneNegocio;
    DefaultJdbcCall jdbcInsertaNegocio;
    DefaultJdbcCall jdbcInsertaNegocioN;
    DefaultJdbcCall jdbcActualizaNegocio;
    DefaultJdbcCall jdbcEliminaNegocio;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_NEGOCIO")
                .withProcedureName("SP_SEL_G_NEGOCIO")
                .returningResultSet("RCL_NEGOCIO", new NegocioRowMapper());

        jdbcObtieneNegocio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_NEGOCIO")
                .withProcedureName("SP_SEL_NEGOCIO")
                .returningResultSet("RCL_NEGOCIO", new NegocioRowMapper());

        jdbcInsertaNegocio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_NEGOCIO")
                .withProcedureName("SP_INS_NEGOCIO");

        jdbcInsertaNegocioN = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_NEGOCIO")
                .withProcedureName("SP_INS_NEGOCIO_N");

        jdbcActualizaNegocio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_NEGOCIO")
                .withProcedureName("SP_ACT_NEGOCIO");

        jdbcEliminaNegocio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_NEGOCIO")
                .withProcedureName("SP_DEL_NEGOCIO");
    }

    @SuppressWarnings("unchecked")
    public List<NegocioDTO> obtieneNegocio() throws Exception {

        Map<String, Object> out = null;
        List<NegocioDTO> listaNegocios = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funcion ejecutada: {checklist.PA_ADM_NEGOCIO.SP_SEL_G_NEGOCIO}");

        listaNegocios = (List<NegocioDTO>) out.get("RCL_NEGOCIO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Negocios");
        } else {
            return listaNegocios;
        }

        return listaNegocios;

    }

    @SuppressWarnings("unchecked")
    public List<NegocioDTO> obtieneNegocio(int idNegocio) throws Exception {
        Map<String, Object> out = null;
        List<NegocioDTO> listaNegocio = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDNEGOCIO", idNegocio);

        out = jdbcObtieneNegocio.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_NEGOCIO.SP_SEL_NEGOCIO}");

        listaNegocio = (List<NegocioDTO>) out.get("RCL_NEGOCIO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Negocio con id(" + idNegocio + ")");
        } else {
            return listaNegocio;
        }

        return null;
    }

    public int insertaNegocio(NegocioDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idNegocio = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_ACTIVO", bean.getActivo());

        out = jdbcInsertaNegocio.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_NEGOCIO.SP_INS_NEGOCIO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_FIIDNEGOCIO");
        idNegocio = idTipoReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Negocio");
        } else {
            return idNegocio;
        }

        return idNegocio;

    }

    public boolean actualizaNegocio(NegocioDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDNEGOCIO", bean.getIdNegocio())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_ACTIVO", bean.getActivo());

        out = jdbcActualizaNegocio.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_NEGOCIO.SP_ACT_NEGOCIO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el Negocio  id( " + bean.getIdNegocio() + ")");
        } else {
            return true;
        }

        return false;

    }

    public boolean eliminaNegocio(int idNegocio) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDNEGOCIO", idNegocio);

        out = jdbcEliminaNegocio.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_NEGOCIO.SP_DEL_NEGOCIO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el Negocio id(" + idNegocio + ")");
        } else {
            return true;
        }

        return false;

    }

    @Override
    public boolean insertaNegocioNuevo(NegocioDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDNEGOCIO", bean.getIdNegocio())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_ACTIVO", bean.getActivo());

        out = jdbcInsertaNegocioN.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_NEGOCIO.SP_INS_NEGOCIO_N}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Negocio");
        } else {
            return true;
        }

        return false;
    }
}
