<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="../css/bootstrap/bootstrap.css">

	<script	src="../js/jquery/jquery-2.2.4.min.js"></script>
	<script	src="../js/bootstrap.min.js"></script>

	<title>Soporte - Inicio</title>
	<% response.addHeader("X-Frame-Options", "SAMEORIGIN"); %>
	<% response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate"); %>
	<% response.addHeader("Pragma", "no-cache"); %>
	<% response.addHeader("Expires", "0"); %>
	<!-- SAMEORIGIN -->
</head>
<body>
	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuSoporte.jsp" flush="true">
		<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuSoporte.jsp" />
	</tiles:insertTemplate>

	<div id="container">
		<div id="container">
			<br>
			<p class="text-center lead"><strong>Soporte - Inicio <br> <small>Bienvenido a la página de Soporte de checklist</small></strong></p>	
		</div>
	</div>

</body>
</html>