<div><b></b></div><%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="../css/bootstrap/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">

	<script	src="../js/jquery/jquery-2.2.4.min.js"></script>
	<script	src="../js/bootstrap.min.js"></script>

	<title>Reporte Supervisi&oacute;n</title>
</head>


<body style="background-color: #FFFFFF; padding-left: 10px;">

	<div id="header">
		<span class="text-center lead"><small>Reporte Supervisi&oacute;n / </small></span>
    	<br>
    	<span class="text-center lead"><Strong> Detalle del Protocolo </Strong></span>
	</div>
	
	<div class="page">
    		<div class="contHome top60"><br><br>	    		
	    		<div class="border borderLeft">  
					<div class=" overflow mh500">   
						<table class="items DetalleProtocolo" style="margin-right: 20px; margin-left: 20px; " width="95%" border="1" bordercolor="#5B5B5B">
                            <tbody>
                            <tr>   
                                    <td>
                                        <div>
                                            1. En el lugar de trabajo ¿hay sólo artículos que son necesarios para la operación?
                                        </div>
                                    </td> 
                                    <td><b>Si</b></td>
                                    <td><b>2%</b></td>
                                    <td>
                                        <a href="#" class="seccionPreguntaUno btnModal mactive"></a>
                                    </td>
                                </tr>  
                                <tr>   
                                    <td>
                                        <div>
                                            2. ¿Existen espacios para materiales en “Cuarentena”?
                                        </div>
                                    </td> 
                                    <td><b>Si</b></td>
                                    <td><b>1%</b></td>
                                    <td>
                                        <a href="#" class="seccionPreguntaUno btnModal mactive"></a>
                                    </td>
                                </tr>  
                                <tr>   
                                    <td>
                                        <div>
                                            3. ¿Existe evidencia de la destrucción de minucias (Acta de hechos y 3 fotos)?
                                        </div>
                                    </td> 
                                    <td><b>Si</b></td>
                                    <td><b>2%</b></td>
                                    <td>
                                        <a href="#" class="seccionPreguntaUno btnModal mactive"></a>
                                    </td>
                                </tr>  
                                <tr>   
                                    <td>
                                        <div>
                                            4. ¿Existen evidencias de la depuración de activos?
                                        </div>
                                    </td> 
                                    <td><b>Si</b></td>
                                    <td><b>1%</b></td>
                                    <td>
                                        <a href="#" class="seccionPreguntaUno btnModal mfail"></a>
                                    </td>
                                </tr>
                                <tr>   
                                    <td>
                                        <div>
                                            5. ¿El personal selecciona y resguarda los expedientes correctamente? (Activos, inactivos en Judicial y Cancelados).
                                        </div>
                                    </td> 
                                    <td><b>Si</b></td>
                                    <td><b>2%</b></td>
                                    <td>
                                        <a href="#" class="seccionPreguntaUno btnModal mactive"></a>
                                    </td>
                                </tr>  
                                <tr>   
                                    <td>
                                        <div>
                                           6. ¿Hay evidencia del envío del paquete operativo?
                                        </div>
                                    </td> 
                                    <td><b>Si</b></td>
                                    <td><b>2%</b></td>
                                    <td>
                                        <a href="#" class="seccionPreguntaUno btnModal mactive"></a>
                                    </td>
                                </tr> 
                            </tbody>
                        </table> 
					</div>  
				</div>
	    		<br><br>
	    		<div class="boton anterior"> 
					<a href="inicio.htm" class="btn arrowl">
						<div><img src="../images/logoBAZ.png" height="50"></div>
						<div style="padding-top: 10px;">VOLVER A LA PÁGINA PRINCIPAL</div>
					</a> 
					</div>
			</div>
    	</div>
	

</body>
</html>