package com.gruposalinas.checklist.domain;

public class PreguntaSupDTO {

    private int idPregunta;
    private String idModulo;
    private String idTipo;
    private String estatus;
    private String descPregunta;
    private String detalle;
    private String critica;
    private String sla;
    private String area;
    private String numSerie;
    private int commit;
    private int idZona;
    private String responsable;
    private String negocio;

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(String idModulo) {
        this.idModulo = idModulo;
    }

    public String getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(String idTipo) {
        this.idTipo = idTipo;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getDescPregunta() {
        return descPregunta;
    }

    public void setDescPregunta(String descPregunta) {
        this.descPregunta = descPregunta;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getCritica() {
        return critica;
    }

    public void setCritica(String critica) {
        this.critica = critica;
    }

    public String getSla() {
        return sla;
    }

    public void setSla(String sla) {
        this.sla = sla;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(String numSerie) {
        this.numSerie = numSerie;
    }

    public int getCommit() {
        return commit;
    }

    public void setCommit(int commit) {
        this.commit = commit;
    }

    public int getIdZona() {
        return idZona;
    }

    public void setIdZona(int idZona) {
        this.idZona = idZona;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    @Override
    public String toString() {
        return "PreguntaSupDTO{" + "idPregunta=" + idPregunta + ", idModulo=" + idModulo + ", idTipo=" + idTipo + ", estatus=" + estatus + ", descPregunta=" + descPregunta + ", detalle=" + detalle + ", critica=" + critica + ", sla=" + sla + ", area=" + area + ", numSerie=" + numSerie + ", commit=" + commit + ", idZona=" + idZona + ", responsable=" + responsable + ", negocio=" + negocio + '}';
    }

}
