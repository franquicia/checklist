package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.DetalleRespuestaReporteDTO;

public class DetalleRespuestaReporteRowMapper implements RowMapper<DetalleRespuestaReporteDTO> {

	@Override
	public DetalleRespuestaReporteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		DetalleRespuestaReporteDTO detalleRepuestaReporteDTO = new DetalleRespuestaReporteDTO();
		
		detalleRepuestaReporteDTO.setIdModulo(rs.getInt("FIID_MODULO"));
		detalleRepuestaReporteDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		detalleRepuestaReporteDTO.setPregunta(rs.getString("PREGUNTA"));
		detalleRepuestaReporteDTO.setIdPreguntaPadre(rs.getInt("FIID_PREG_PADRE"));
		detalleRepuestaReporteDTO.setIdRepuesta(rs.getInt("FIID_RESPUESTA"));
		detalleRepuestaReporteDTO.setObservaciones(rs.getString("FCOBSERVACIONES"));
		detalleRepuestaReporteDTO.setIdArbol(rs.getInt("FIID_ARBOL_DES"));
		detalleRepuestaReporteDTO.setRespuesta(rs.getString("RESPUESTA"));
		detalleRepuestaReporteDTO.setResAdicional(rs.getString("RES_ADICIONAL"));
		detalleRepuestaReporteDTO.setEvidencia(rs.getString("EVIDENCIA"));
		detalleRepuestaReporteDTO.setAccion(rs.getString("ACCION"));
		detalleRepuestaReporteDTO.setFechaCompromiso(rs.getString("FECHA_COMP"));
		
		
		return 	detalleRepuestaReporteDTO;
	}
	

}
