package com.gruposalinas.checklist.config.init;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

/**
 *
 * @author cescobarh
 */
@SpringBootApplication
@Import({ConfigApp.class})
@EnableAutoConfiguration(exclude = {HibernateJpaAutoConfiguration.class})
public class BaseInitApp extends SpringBootServletInitializer {

    /**
     *
     * @param application
     * @return
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(BaseInitApp.class);
    }

    public static void main(String[] args) {

    }

}
