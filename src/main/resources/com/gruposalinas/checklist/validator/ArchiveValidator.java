package com.gruposalinas.checklist.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.gruposalinas.checklist.domain.UploadArchive;

public class ArchiveValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return UploadArchive.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "nombre", "name.required");
		ValidationUtils.rejectIfEmpty(errors, "titulo", "password.required");
		ValidationUtils.rejectIfEmpty(errors, "descripcion", "gender.required");
		UploadArchive file = (UploadArchive) target;
		if(file.getArchive().isEmpty())
		{
			errors.rejectValue("archive","file.required");
		}
	}

}
