/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.domain;

/**
 *
 * @author kramireza
 */
public class ListaTablaEKT {

    String fechaProceso;
    Integer idPGrupoEmp;
    Integer idGrupoEmp;
    String nomGrupoEmp;
    Integer idPDivision;
    Integer idDivision;
    String nomDivision;
    Integer idPEntidad;
    Integer idEntidad;
    String nomEntidad;
    Integer idEstructura;
    String nomEstructura;
    Integer idTipoCentro;
    String nombreTipoCentro;
    String idCC;
    String nomCC;
    String fechaCreacion;
    String fechaApertura;
    String fechaCierre;
    Integer idCanal;
    String nomCanal;
    Integer idEstatus;
    String nomEstatus;
    String idColor;
    String nomColor;
    Integer idResponsable;
    String nomResponsable;
    Integer idResponsableAlt;
    String nomResponsableAlt;
    Integer idCCPa;
    String nomCCPa;
    Integer idPGrupoEmpPa;
    Integer idGrupoEmpPa;
    String nomGrupoEmpPa;
    Integer idPDivisionPa;
    Integer idDivisionPa;
    String nomDivisionPa;
    Integer idPEntidadPa;
    Integer idEntidadPa;
    String nomEntidadPa;
    String nivel;
    Integer presupuesto;
    Integer sppi;
    Integer idPais;
    String nomPais;
    Integer idEstado;
    String nomEstado;
    Integer idMunicipio;
    String nomMunicipio;
    Integer idColonia;
    String nomColonia;
    Integer idLocalidad;
    String nomLocalidad;
    String nomCalle;
    String numExterior;
    String numInterior;
    String codigoPostal;
    String telefono;
    String latitud;
    String longitud;
    String usuarioCreacion;
    String usuarioModificacion;
    String fechaModificacion;
    String numEconomico;
    String fechaReubicacion;
    String idCanalAlnova;
    String nomCanalAlnova;
    Integer idCanalCredimax;
    String nomCanalCredimax;
    String tipoCanal;
    Integer idTipoSucursal;
    String nomTipoSucursal;
    Integer idTipoOperacion;
    String nomTipoOperacion;
    String sucTransferencia;
    Integer idVicepresidencia;
    String nomVicepresidencia;
    String centroContable;
    Integer idTipoOficina;
    String nomTipoOficina;
    Integer idTipoRelacion;
    String nomTipoRelacion;
    Integer idTipoCcAlnova;
    String nomTipoCcAlnova;
    Integer idTipoPerfil;
    String nomTipoPerfil;
    String codigoCamara;
    Integer idNivelSap;
    String nomNivelSap;
    Integer idIva;
    String valorIva;
    Integer idIsrm;
    String valorIsr;
    String plazaBanxico;
    String plazaCecoban;
    String horarioCecoban;
    String codigoAbm;
    String bancoCr;
    String ccAdmonConta;
    String ccAdmon;
    String ccRegionalConta;
    String ccAdmonRegional;
    String caja;
    String desEstructuracve;
    String idSubnegocio;
    String desSubnegocio;
    Integer idUnidadNegocio;
    String nomunidadnegocio;
    String idCostoVida;
    String descCostoVida;
    String IdEstructuracve;

    public ListaTablaEKT() {
    }

    public String getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(String fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public Integer getIdPGrupoEmp() {
        return idPGrupoEmp;
    }

    public void setIdPGrupoEmp(Integer idPGrupoEmp) {
        this.idPGrupoEmp = idPGrupoEmp;
    }

    public Integer getIdGrupoEmp() {
        return idGrupoEmp;
    }

    public void setIdGrupoEmp(Integer idGrupoEmp) {
        this.idGrupoEmp = idGrupoEmp;
    }

    public String getNomGrupoEmp() {
        return nomGrupoEmp;
    }

    public void setNomGrupoEmp(String nomGrupoEmp) {
        this.nomGrupoEmp = nomGrupoEmp;
    }

    public Integer getIdPDivision() {
        return idPDivision;
    }

    public void setIdPDivision(Integer idPDivision) {
        this.idPDivision = idPDivision;
    }

    public Integer getIdDivision() {
        return idDivision;
    }

    public void setIdDivision(Integer idDivision) {
        this.idDivision = idDivision;
    }

    public String getNomDivision() {
        return nomDivision;
    }

    public void setNomDivision(String nomDivision) {
        this.nomDivision = nomDivision;
    }

    public Integer getIdPEntidad() {
        return idPEntidad;
    }

    public void setIdPEntidad(Integer idPEntidad) {
        this.idPEntidad = idPEntidad;
    }

    public Integer getIdEntidad() {
        return idEntidad;
    }

    public void setIdEntidad(Integer idEntidad) {
        this.idEntidad = idEntidad;
    }

    public String getNomEntidad() {
        return nomEntidad;
    }

    public void setNomEntidad(String nomEntidad) {
        this.nomEntidad = nomEntidad;
    }

    public Integer getIdEstructura() {
        return idEstructura;
    }

    public void setIdEstructura(Integer idEstructura) {
        this.idEstructura = idEstructura;
    }

    public String getNomEstructura() {
        return nomEstructura;
    }

    public void setNomEstructura(String nomEstructura) {
        this.nomEstructura = nomEstructura;
    }

    public Integer getIdTipoCentro() {
        return idTipoCentro;
    }

    public void setIdTipoCentro(Integer idTipoCentro) {
        this.idTipoCentro = idTipoCentro;
    }

    public String getNombreTipoCentro() {
        return nombreTipoCentro;
    }

    public void setNombreTipoCentro(String nombreTipoCentro) {
        this.nombreTipoCentro = nombreTipoCentro;
    }

    public String getIdCC() {
        return idCC;
    }

    public void setIdCC(String idCC) {
        this.idCC = idCC;
    }

    public String getNomCC() {
        return nomCC;
    }

    public void setNomCC(String nomCC) {
        this.nomCC = nomCC;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(String fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public String getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(String fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public Integer getIdCanal() {
        return idCanal;
    }

    public void setIdCanal(Integer idCanal) {
        this.idCanal = idCanal;
    }

    public String getNomCanal() {
        return nomCanal;
    }

    public void setNomCanal(String nomCanal) {
        this.nomCanal = nomCanal;
    }

    public Integer getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(Integer idEstatus) {
        this.idEstatus = idEstatus;
    }

    public String getNomEstatus() {
        return nomEstatus;
    }

    public void setNomEstatus(String nomEstatus) {
        this.nomEstatus = nomEstatus;
    }

    public String getIdColor() {
        return idColor;
    }

    public void setIdColor(String idColor) {
        this.idColor = idColor;
    }

    public String getNomColor() {
        return nomColor;
    }

    public void setNomColor(String nomColor) {
        this.nomColor = nomColor;
    }

    public Integer getIdResponsable() {
        return idResponsable;
    }

    public void setIdResponsable(Integer idResponsable) {
        this.idResponsable = idResponsable;
    }

    public String getNomResponsable() {
        return nomResponsable;
    }

    public void setNomResponsable(String nomResponsable) {
        this.nomResponsable = nomResponsable;
    }

    public Integer getIdResponsableAlt() {
        return idResponsableAlt;
    }

    public void setIdResponsableAlt(Integer idResponsableAlt) {
        this.idResponsableAlt = idResponsableAlt;
    }

    public String getNomResponsableAlt() {
        return nomResponsableAlt;
    }

    public void setNomResponsableAlt(String nomResponsableAlt) {
        this.nomResponsableAlt = nomResponsableAlt;
    }

    public Integer getIdCCPa() {
        return idCCPa;
    }

    public void setIdCCPa(Integer idCCPa) {
        this.idCCPa = idCCPa;
    }

    public String getNomCCPa() {
        return nomCCPa;
    }

    public void setNomCCPa(String nomCCPa) {
        this.nomCCPa = nomCCPa;
    }

    public Integer getIdPGrupoEmpPa() {
        return idPGrupoEmpPa;
    }

    public void setIdPGrupoEmpPa(Integer idPGrupoEmpPa) {
        this.idPGrupoEmpPa = idPGrupoEmpPa;
    }

    public Integer getIdGrupoEmpPa() {
        return idGrupoEmpPa;
    }

    public void setIdGrupoEmpPa(Integer idGrupoEmpPa) {
        this.idGrupoEmpPa = idGrupoEmpPa;
    }

    public String getNomGrupoEmpPa() {
        return nomGrupoEmpPa;
    }

    public void setNomGrupoEmpPa(String nomGrupoEmpPa) {
        this.nomGrupoEmpPa = nomGrupoEmpPa;
    }

    public Integer getIdPDivisionPa() {
        return idPDivisionPa;
    }

    public void setIdPDivisionPa(Integer idPDivisionPa) {
        this.idPDivisionPa = idPDivisionPa;
    }

    public Integer getIdDivisionPa() {
        return idDivisionPa;
    }

    public void setIdDivisionPa(Integer idDivisionPa) {
        this.idDivisionPa = idDivisionPa;
    }

    public String getNomDivisionPa() {
        return nomDivisionPa;
    }

    public void setNomDivisionPa(String nomDivisionPa) {
        this.nomDivisionPa = nomDivisionPa;
    }

    public Integer getIdPEntidadPa() {
        return idPEntidadPa;
    }

    public void setIdPEntidadPa(Integer idPEntidadPa) {
        this.idPEntidadPa = idPEntidadPa;
    }

    public Integer getIdEntidadPa() {
        return idEntidadPa;
    }

    public void setIdEntidadPa(Integer idEntidadPa) {
        this.idEntidadPa = idEntidadPa;
    }

    public String getNomEntidadPa() {
        return nomEntidadPa;
    }

    public void setNomEntidadPa(String nomEntidadPa) {
        this.nomEntidadPa = nomEntidadPa;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public Integer getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(Integer presupuesto) {
        this.presupuesto = presupuesto;
    }

    public Integer getSppi() {
        return sppi;
    }

    public void setSppi(Integer sppi) {
        this.sppi = sppi;
    }

    public Integer getIdPais() {
        return idPais;
    }

    public void setIdPais(Integer idPais) {
        this.idPais = idPais;
    }

    public String getNomPais() {
        return nomPais;
    }

    public void setNomPais(String nomPais) {
        this.nomPais = nomPais;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getNomEstado() {
        return nomEstado;
    }

    public void setNomEstado(String nomEstado) {
        this.nomEstado = nomEstado;
    }

    public Integer getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(Integer idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public String getNomMunicipio() {
        return nomMunicipio;
    }

    public void setNomMunicipio(String nomMunicipio) {
        this.nomMunicipio = nomMunicipio;
    }

    public Integer getIdColonia() {
        return idColonia;
    }

    public void setIdColonia(Integer idColonia) {
        this.idColonia = idColonia;
    }

    public String getNomColonia() {
        return nomColonia;
    }

    public void setNomColonia(String nomColonia) {
        this.nomColonia = nomColonia;
    }

    public Integer getIdLocalidad() {
        return idLocalidad;
    }

    public void setIdLocalidad(Integer idLocalidad) {
        this.idLocalidad = idLocalidad;
    }

    public String getNomLocalidad() {
        return nomLocalidad;
    }

    public void setNomLocalidad(String nomLocalidad) {
        this.nomLocalidad = nomLocalidad;
    }

    public String getNomCalle() {
        return nomCalle;
    }

    public void setNomCalle(String nomCalle) {
        this.nomCalle = nomCalle;
    }

    public String getNumExterior() {
        return numExterior;
    }

    public void setNumExterior(String numExterior) {
        this.numExterior = numExterior;
    }

    public String getNumInterior() {
        return numInterior;
    }

    public void setNumInterior(String numInterior) {
        this.numInterior = numInterior;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getNumEconomico() {
        return numEconomico;
    }

    public void setNumEconomico(String numEconomico) {
        this.numEconomico = numEconomico;
    }

    public String getFechaReubicacion() {
        return fechaReubicacion;
    }

    public void setFechaReubicacion(String fechaReubicacion) {
        this.fechaReubicacion = fechaReubicacion;
    }

    public String getIdCanalAlnova() {
        return idCanalAlnova;
    }

    public void setIdCanalAlnova(String idCanalAlnova) {
        this.idCanalAlnova = idCanalAlnova;
    }

    public String getNomCanalAlnova() {
        return nomCanalAlnova;
    }

    public void setNomCanalAlnova(String nomCanalAlnova) {
        this.nomCanalAlnova = nomCanalAlnova;
    }

    public Integer getIdCanalCredimax() {
        return idCanalCredimax;
    }

    public void setIdCanalCredimax(Integer idCanalCredimax) {
        this.idCanalCredimax = idCanalCredimax;
    }

    public String getNomCanalCredimax() {
        return nomCanalCredimax;
    }

    public void setNomCanalCredimax(String nomCanalCredimax) {
        this.nomCanalCredimax = nomCanalCredimax;
    }

    public String getTipoCanal() {
        return tipoCanal;
    }

    public void setTipoCanal(String tipoCanal) {
        this.tipoCanal = tipoCanal;
    }

    public Integer getIdTipoSucursal() {
        return idTipoSucursal;
    }

    public void setIdTipoSucursal(Integer idTipoSucursal) {
        this.idTipoSucursal = idTipoSucursal;
    }

    public String getNomTipoSucursal() {
        return nomTipoSucursal;
    }

    public void setNomTipoSucursal(String nomTipoSucursal) {
        this.nomTipoSucursal = nomTipoSucursal;
    }

    public Integer getIdTipoOperacion() {
        return idTipoOperacion;
    }

    public void setIdTipoOperacion(Integer idTipoOperacion) {
        this.idTipoOperacion = idTipoOperacion;
    }

    public String getNomTipoOperacion() {
        return nomTipoOperacion;
    }

    public void setNomTipoOperacion(String nomTipoOperacion) {
        this.nomTipoOperacion = nomTipoOperacion;
    }

    public String getSucTransferencia() {
        return sucTransferencia;
    }

    public void setSucTransferencia(String sucTransferencia) {
        this.sucTransferencia = sucTransferencia;
    }

    public Integer getIdVicepresidencia() {
        return idVicepresidencia;
    }

    public void setIdVicepresidencia(Integer idVicepresidencia) {
        this.idVicepresidencia = idVicepresidencia;
    }

    public String getNomVicepresidencia() {
        return nomVicepresidencia;
    }

    public void setNomVicepresidencia(String nomVicepresidencia) {
        this.nomVicepresidencia = nomVicepresidencia;
    }

    public String getCentroContable() {
        return centroContable;
    }

    public void setCentroContable(String centroContable) {
        this.centroContable = centroContable;
    }

    public Integer getIdTipoOficina() {
        return idTipoOficina;
    }

    public void setIdTipoOficina(Integer idTipoOficina) {
        this.idTipoOficina = idTipoOficina;
    }

    public String getNomTipoOficina() {
        return nomTipoOficina;
    }

    public void setNomTipoOficina(String nomTipoOficina) {
        this.nomTipoOficina = nomTipoOficina;
    }

    public Integer getIdTipoRelacion() {
        return idTipoRelacion;
    }

    public void setIdTipoRelacion(Integer idTipoRelacion) {
        this.idTipoRelacion = idTipoRelacion;
    }

    public String getNomTipoRelacion() {
        return nomTipoRelacion;
    }

    public void setNomTipoRelacion(String nomTipoRelacion) {
        this.nomTipoRelacion = nomTipoRelacion;
    }

    public Integer getIdTipoCcAlnova() {
        return idTipoCcAlnova;
    }

    public void setIdTipoCcAlnova(Integer idTipoCcAlnova) {
        this.idTipoCcAlnova = idTipoCcAlnova;
    }

    public String getNomTipoCcAlnova() {
        return nomTipoCcAlnova;
    }

    public void setNomTipoCcAlnova(String nomTipoCcAlnova) {
        this.nomTipoCcAlnova = nomTipoCcAlnova;
    }

    public Integer getIdTipoPerfil() {
        return idTipoPerfil;
    }

    public void setIdTipoPerfil(Integer idTipoPerfil) {
        this.idTipoPerfil = idTipoPerfil;
    }

    public String getNomTipoPerfil() {
        return nomTipoPerfil;
    }

    public void setNomTipoPerfil(String nomTipoPerfil) {
        this.nomTipoPerfil = nomTipoPerfil;
    }

    public String getCodigoCamara() {
        return codigoCamara;
    }

    public void setCodigoCamara(String codigoCamara) {
        this.codigoCamara = codigoCamara;
    }

    public Integer getIdNivelSap() {
        return idNivelSap;
    }

    public void setIdNivelSap(Integer idNivelSap) {
        this.idNivelSap = idNivelSap;
    }

    public String getNomNivelSap() {
        return nomNivelSap;
    }

    public void setNomNivelSap(String nomNivelSap) {
        this.nomNivelSap = nomNivelSap;
    }

    public Integer getIdIva() {
        return idIva;
    }

    public void setIdIva(Integer idIva) {
        this.idIva = idIva;
    }

    public String getValorIva() {
        return valorIva;
    }

    public void setValorIva(String valorIva) {
        this.valorIva = valorIva;
    }

    public Integer getIdIsrm() {
        return idIsrm;
    }

    public void setIdIsrm(Integer idIsrm) {
        this.idIsrm = idIsrm;
    }

    public String getValorIsr() {
        return valorIsr;
    }

    public void setValorIsr(String valorIsr) {
        this.valorIsr = valorIsr;
    }

    public String getPlazaBanxico() {
        return plazaBanxico;
    }

    public void setPlazaBanxico(String plazaBanxico) {
        this.plazaBanxico = plazaBanxico;
    }

    public String getPlazaCecoban() {
        return plazaCecoban;
    }

    public void setPlazaCecoban(String plazaCecoban) {
        this.plazaCecoban = plazaCecoban;
    }

    public String getHorarioCecoban() {
        return horarioCecoban;
    }

    public void setHorarioCecoban(String horarioCecoban) {
        this.horarioCecoban = horarioCecoban;
    }

    public String getCodigoAbm() {
        return codigoAbm;
    }

    public void setCodigoAbm(String codigoAbm) {
        this.codigoAbm = codigoAbm;
    }

    public String getBancoCr() {
        return bancoCr;
    }

    public void setBancoCr(String bancoCr) {
        this.bancoCr = bancoCr;
    }

    public String getCcAdmonConta() {
        return ccAdmonConta;
    }

    public void setCcAdmonConta(String ccAdmonConta) {
        this.ccAdmonConta = ccAdmonConta;
    }

    public String getCcAdmon() {
        return ccAdmon;
    }

    public void setCcAdmon(String ccAdmon) {
        this.ccAdmon = ccAdmon;
    }

    public String getCcRegionalConta() {
        return ccRegionalConta;
    }

    public void setCcRegionalConta(String ccRegionalConta) {
        this.ccRegionalConta = ccRegionalConta;
    }

    public String getCcAdmonRegional() {
        return ccAdmonRegional;
    }

    public void setCcAdmonRegional(String ccAdmonRegional) {
        this.ccAdmonRegional = ccAdmonRegional;
    }

    public String getCaja() {
        return caja;
    }

    public void setCaja(String caja) {
        this.caja = caja;
    }

    public String getDesEstructuracve() {
        return desEstructuracve;
    }

    public void setDesEstructuracve(String desEstructuracve) {
        this.desEstructuracve = desEstructuracve;
    }

    public String getIdSubnegocio() {
        return idSubnegocio;
    }

    public void setIdSubnegocio(String idSubnegocio) {
        this.idSubnegocio = idSubnegocio;
    }

    public String getDesSubnegocio() {
        return desSubnegocio;
    }

    public void setDesSubnegocio(String desSubnegocio) {
        this.desSubnegocio = desSubnegocio;
    }

    public Integer getIdUnidadNegocio() {
        return idUnidadNegocio;
    }

    public void setIdUnidadNegocio(Integer idUnidadNegocio) {
        this.idUnidadNegocio = idUnidadNegocio;
    }

    public String getNomunidadnegocio() {
        return nomunidadnegocio;
    }

    public void setNomunidadnegocio(String nomunidadnegocio) {
        this.nomunidadnegocio = nomunidadnegocio;
    }

    public String getIdCostoVida() {
        return idCostoVida;
    }

    public void setIdCostoVida(String idCostoVida) {
        this.idCostoVida = idCostoVida;
    }

    public String getDescCostoVida() {
        return descCostoVida;
    }

    public void setDescCostoVida(String descCostoVida) {
        this.descCostoVida = descCostoVida;
    }

    public String getIdEstructuracve() {
        return IdEstructuracve;
    }

    public void setIdEstructuracve(String IdEstructuracve) {
        this.IdEstructuracve = IdEstructuracve;
    }

    @Override
    public String toString() {
        return "ListaTablaEKT{" + "fechaProceso=" + fechaProceso + ", idPGrupoEmp=" + idPGrupoEmp + ", idGrupoEmp=" + idGrupoEmp + ", nomGrupoEmp=" + nomGrupoEmp + ", idPDivision=" + idPDivision + ", idDivision=" + idDivision + ", nomDivision=" + nomDivision + ", idPEntidad=" + idPEntidad + ", idEntidad=" + idEntidad + ", nomEntidad=" + nomEntidad + ", idEstructura=" + idEstructura + ", nomEstructura=" + nomEstructura + ", idTipoCentro=" + idTipoCentro + ", nombreTipoCentro=" + nombreTipoCentro + ", idCC=" + idCC + ", nomCC=" + nomCC + ", fechaCreacion=" + fechaCreacion + ", fechaApertura=" + fechaApertura + ", fechaCierre=" + fechaCierre + ", idCanal=" + idCanal + ", nomCanal=" + nomCanal + ", idEstatus=" + idEstatus + ", nomEstatus=" + nomEstatus + ", idColor=" + idColor + ", nomColor=" + nomColor + ", idResponsable=" + idResponsable + ", nomResponsable=" + nomResponsable + ", idResponsableAlt=" + idResponsableAlt + ", nomResponsableAlt=" + nomResponsableAlt + ", idCCPa=" + idCCPa + ", nomCCPa=" + nomCCPa + ", idPGrupoEmpPa=" + idPGrupoEmpPa + ", idGrupoEmpPa=" + idGrupoEmpPa + ", nomGrupoEmpPa=" + nomGrupoEmpPa + ", idPDivisionPa=" + idPDivisionPa + ", idDivisionPa=" + idDivisionPa + ", nomDivisionPa=" + nomDivisionPa + ", idPEntidadPa=" + idPEntidadPa + ", idEntidadPa=" + idEntidadPa + ", nomEntidadPa=" + nomEntidadPa + ", nivel=" + nivel + ", presupuesto=" + presupuesto + ", sppi=" + sppi + ", idPais=" + idPais + ", nomPais=" + nomPais + ", idEstado=" + idEstado + ", nomEstado=" + nomEstado + ", idMunicipio=" + idMunicipio + ", nomMunicipio=" + nomMunicipio + ", idColonia=" + idColonia + ", nomColonia=" + nomColonia + ", idLocalidad=" + idLocalidad + ", nomLocalidad=" + nomLocalidad + ", nomCalle=" + nomCalle + ", numExterior=" + numExterior + ", numInterior=" + numInterior + ", codigoPostal=" + codigoPostal + ", telefono=" + telefono + ", latitud=" + latitud + ", longitud=" + longitud + ", usuarioCreacion=" + usuarioCreacion + ", usuarioModificacion=" + usuarioModificacion + ", fechaModificacion=" + fechaModificacion + ", numEconomico=" + numEconomico + ", fechaReubicacion=" + fechaReubicacion + ", idCanalAlnova=" + idCanalAlnova + ", nomCanalAlnova=" + nomCanalAlnova + ", idCanalCredimax=" + idCanalCredimax + ", nomCanalCredimax=" + nomCanalCredimax + ", tipoCanal=" + tipoCanal + ", idTipoSucursal=" + idTipoSucursal + ", nomTipoSucursal=" + nomTipoSucursal + ", idTipoOperacion=" + idTipoOperacion + ", nomTipoOperacion=" + nomTipoOperacion + ", sucTransferencia=" + sucTransferencia + ", idVicepresidencia=" + idVicepresidencia + ", nomVicepresidencia=" + nomVicepresidencia + ", centroContable=" + centroContable + ", idTipoOficina=" + idTipoOficina + ", nomTipoOficina=" + nomTipoOficina + ", idTipoRelacion=" + idTipoRelacion + ", nomTipoRelacion=" + nomTipoRelacion + ", idTipoCcAlnova=" + idTipoCcAlnova + ", nomTipoCcAlnova=" + nomTipoCcAlnova + ", idTipoPerfil=" + idTipoPerfil + ", nomTipoPerfil=" + nomTipoPerfil + ", codigoCamara=" + codigoCamara + ", idNivelSap=" + idNivelSap + ", nomNivelSap=" + nomNivelSap + ", idIva=" + idIva + ", valorIva=" + valorIva + ", idIsrm=" + idIsrm + ", valorIsr=" + valorIsr + ", plazaBanxico=" + plazaBanxico + ", plazaCecoban=" + plazaCecoban + ", horarioCecoban=" + horarioCecoban + ", codigoAbm=" + codigoAbm + ", bancoCr=" + bancoCr + ", ccAdmonConta=" + ccAdmonConta + ", ccAdmon=" + ccAdmon + ", ccRegionalConta=" + ccRegionalConta + ", ccAdmonRegional=" + ccAdmonRegional + ", caja=" + caja + ", desEstructuracve=" + desEstructuracve + ", idSubnegocio=" + idSubnegocio + ", desSubnegocio=" + desSubnegocio + ", idUnidadNegocio=" + idUnidadNegocio + ", nomunidadnegocio=" + nomunidadnegocio + ", idCostoVida=" + idCostoVida + ", descCostoVida=" + descCostoVida + ", IdEstructuracve=" + IdEstructuracve + '}';
    }

}
