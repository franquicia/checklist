package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ProyectFaseTransfDTO;

public class ProyectFaseTransfRowMapper implements RowMapper<ProyectFaseTransfDTO> {

	public ProyectFaseTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ProyectFaseTransfDTO ProyectFaseTransfDTO = new ProyectFaseTransfDTO();
		
		ProyectFaseTransfDTO.setIdProyecto(rs.getInt("FIID_PROYECTO"));
	    ProyectFaseTransfDTO.setIdStatus(rs.getInt("FIIDACTIVO"));
	    ProyectFaseTransfDTO.setNomProy(rs.getString("FCNOMBRE"));
	    ProyectFaseTransfDTO.setObsProy(rs.getString("FCOBSERVACION"));
	    ProyectFaseTransfDTO.setPeriodo(rs.getString("FCPERIODO"));
	    ProyectFaseTransfDTO.setEdoCargaIni(rs.getInt("FIID_EDOCARGAINI"));
	
		return ProyectFaseTransfDTO;
	}

}


