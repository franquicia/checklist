<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%-- 
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
	
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/estilos2.css"> --%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervisionPI2/estilos.css">

<script src="../js/jquery/jquery-2.2.4.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

<title>Reporte Supervisi&oacute;n</title>


<script type="text/javascript">
	function getIdCeco() {
		var select = document.getElementById("socioS"), //El <select>
        value = select.value, //El valor seleccionado
        text = select.options[select.selectedIndex].innerText;
        //alert("Elemento: "+value+","+text);
        
        document.getElementById('idUsuario').value = value;
        document.getElementById('nomUsuario').value = text;

        var select = document.getElementById("anioS"), //El <select>
        value = select.value, //El valor seleccionado
        text = select.options[select.selectedIndex].innerText;
        //alert("Elemento: "+value+","+text);
        
        document.getElementById('anio').value = value;

        var select = document.getElementById("mesS"), //El <select>
        value = select.value, //El valor seleccionado
        text = select.options[select.selectedIndex].innerText;
        //alert("Elemento: "+value+","+text);
        
        document.getElementById('mes').value = value;
        document.getElementById('nomMes').value = text;
        
		form = document.getElementById("form1");
		form.submit();
	}
</script>

</head>

<body ng-app="app" ng-controller="validarForm">

	<div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="${pageContext.request.contextPath}/img/supervision/btn_hamburguer.svg" alt="Menú principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="inicio.htm"><img src="${pageContext.request.contextPath}/img/supervision/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.htm" class="selMenu">
          <div>Salir</div>
          <div><img src="${pageContext.request.contextPath}/img/supervision/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="reporteAsistencia.htm">Reporte Asistencia</a> 
		</div>
    	Consulta
    </div>

	<!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervision/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Menú Principal<br>
          </div>
        </div>
        <%-- <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la página">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> --%>
        <div id="menu">
        	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuAseguramiento.jsp" flush="true">
				<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuAseguramiento.jsp" />
			</tiles:insertTemplate>
          <!-- <ul class="l-navegacion nivel1">
          		<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Supervisor</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>Búsqueda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>Búsqueda Supervisor</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galería de Evidencias</div></a>
				</li>
			</ul> -->
        </div>
      </div>
    </div>


	<div class="contSecc">
	
	<div class="liga">
		<a href="inicio.htm" class="liga">
		<img style="height: 15px; width: 15px;" src="${pageContext.request.contextPath}/img/supervision/arrowmleft.png">
		<span>PÁGINA PRINCIPAL</span>
		</a>
		</div>
		<br><br>
	
		<div class="gris">
				<c:url value="/central/reporteAsistencia.htm" var="archivoPasivo" />
				<form:form method="POST" action="${archivoPasivo}" model="command" name="form1" id="form1">
				<div class="divCol3">
						
						<div class="col3">
							SOCIO:<br>
							<select id="socioS"  >
								  <c:forEach var="lista" items="${listaUsu}">
											<tr>
												<td style="font-size: 20px; padding: 8px;">
													<div>
														<option value="${lista.idUsuario}"><c:out value="${lista.nombreUsuario}"></c:out></option>
													</div>
												</td>
											</tr>
									</c:forEach>
							</select>  
						</div>
						
						<div class="col3">
							AÑO:<br>
							<select id="anioS" > 
								  <c:forEach var="anios" items="${anios}">
											<tr>
												<td style="font-size: 20px; padding: 8px;">
													<div>
														<option value="${anios}"><c:out value="${anios}"/></option>
													</div>
												</td>
											</tr>
									</c:forEach>
							</select>  
						</div>
						<div class="col3">
							MES:<br>
							<select id="mesS" > 
								<c:set var="nFilters" value="${1}" scope="request"/>
								  <c:forEach var="mesesS" items="${meses}">
											<tr>
												<td style="font-size: 20px; padding: 8px;">
													<div>
														<option value="${nFilters}"><c:out value="${mesesS}"/></option>
													</div>
												</td>
											</tr>
											<c:set var="nFilters" value="${nFilters + 1}"  scope="request" />
									</c:forEach>
								
							</select>  
						</div>
						
					
				</div>
				<div class="btnCenter">
						<a href="#" class="btn btnGuardar" onclick="return getIdCeco();"> Consultar</a>
				</div>
				
				
				<input id="anio" name="anio" type="hidden" value="" />
				<input id="idUsuario" name="idUsuario" type="hidden" value="" />
				<input id="nomUsuario" name="nomUsuario" type="hidden" value="" />
				<input id="mes" name="mes" type="hidden" value="" />
				<input id="nomMes" name="nomMes" type="hidden" value="" />
				
	
		</div>
		</form:form>
		
			<table class="tblGeneral">
						<tbody>
							<tr>
							  	<th>Socio</th>
							  	<th>Mes</th>
							  	<th>dia</th>
							  	<th>Hora Entrada</th>
							  	<th>Hora Salida</th>
							  	<th>Sucursales</th>
							</tr>
							<c:forEach var="asistencia" items="${asistencia}">
											<tr>
												<td><c:out value="${nomUsuario}"/></td>
												<td><c:out value="${nomMes}"/></td>
												<td><c:out value="${asistencia.dia}"/></td>
												<td><c:out value="${asistencia.horaEntrada}"/></td>
												<td><c:out value="${asistencia.horaSalita}"/></td>
												<td><c:out value="${asistencia.sucursales}"/></td>
											</tr>
							</c:forEach>
						</tbody>
					</table>
					<br/>
					<br/>
			<!-- <div class="btnCenter">
						<a href="/checklist/central/inicio.htm" class="btn btnGuardar"> <div>Regresar</div></a>
				</div> -->
				<br/>
	</div>
	<br/>

	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualización Febrero 2019</div>
	</div>
	


</body>
</html>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/custom-file-input.js"></script><!--Unpload -->
<script src="${pageContext.request.contextPath}/js/jquery.treeview.js"></script><!--Unpload -->
<script src="${pageContext.request.contextPath}/js/owl.carousel.js"></script><!-- Slider -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/funciones.js"></script>