package com.gruposalinas.checklist.servicios.servidor;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.gruposalinas.checklist.business.ActorEdoHallaBI;
import com.gruposalinas.checklist.business.AdicionalesBI;
import com.gruposalinas.checklist.business.AdmHandbookBI;
import com.gruposalinas.checklist.business.AdmInformProtocolosBI;
import com.gruposalinas.checklist.business.AltaGerenteBI;
import com.gruposalinas.checklist.business.AsignaExpBI;
import com.gruposalinas.checklist.business.AsignaTransfBI;
import com.gruposalinas.checklist.business.BitacoraAdministradorBI;
import com.gruposalinas.checklist.business.BitacoraGralBI;
import com.gruposalinas.checklist.business.CanalBI;
import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.business.CheckInAsistenciaBI;
import com.gruposalinas.checklist.business.CheckinBI;
import com.gruposalinas.checklist.business.ChecklistNegocioBI;
import com.gruposalinas.checklist.business.ConfiguraActorBI;
import com.gruposalinas.checklist.business.ConsultaCecoProtocoloBI;
import com.gruposalinas.checklist.business.ConsultaFechasBI;
import com.gruposalinas.checklist.business.DatosInformeBI;
import com.gruposalinas.checklist.business.EmpContacExtBI;
import com.gruposalinas.checklist.business.GrupoBI;
import com.gruposalinas.checklist.business.HallagosMatrizBI;
import com.gruposalinas.checklist.business.HallazgosEviExpBI;
import com.gruposalinas.checklist.business.HallazgosExpBI;
import com.gruposalinas.checklist.business.HallazgosTransfBI;
import com.gruposalinas.checklist.business.HorarioBI;
import com.gruposalinas.checklist.business.NegocioAdicionalBI;
import com.gruposalinas.checklist.business.NegocioBI;
import com.gruposalinas.checklist.business.NivelBI;
import com.gruposalinas.checklist.business.OrdenGrupoBI;
import com.gruposalinas.checklist.business.PaisBI;
import com.gruposalinas.checklist.business.PaisNegocioBI;
import com.gruposalinas.checklist.business.ParametroBI;
import com.gruposalinas.checklist.business.PerfilBI;
import com.gruposalinas.checklist.business.PerfilUsuarioBI;
import com.gruposalinas.checklist.business.PeriodoBI;
import com.gruposalinas.checklist.business.PosibleTipoPreguntaBI;
import com.gruposalinas.checklist.business.PosiblesBI;
import com.gruposalinas.checklist.business.ProtocoloBI;
import com.gruposalinas.checklist.business.ProyectFaseTransfBI;
import com.gruposalinas.checklist.business.PuestoBI;
import com.gruposalinas.checklist.business.RecursoBI;
import com.gruposalinas.checklist.business.RecursoPerfilBI;
import com.gruposalinas.checklist.business.SoftNvoBI;
import com.gruposalinas.checklist.business.SoftOpeningBI;
import com.gruposalinas.checklist.business.SucursalBI;
import com.gruposalinas.checklist.business.SucursalChecklistBI;
import com.gruposalinas.checklist.business.TipoArchivoBI;
import com.gruposalinas.checklist.business.TransformacionBI;
import com.gruposalinas.checklist.business.Usuario_ABI;
import com.gruposalinas.checklist.business.VersionChecklistGralBI;
import com.gruposalinas.checklist.business.VersionExpanBI;
import com.gruposalinas.checklist.business.ZonaNegoBI;
import com.gruposalinas.checklist.domain.ActorEdoHallaDTO;
import com.gruposalinas.checklist.domain.AdicionalesDTO;
import com.gruposalinas.checklist.domain.AdmFirmasCatDTO;
import com.gruposalinas.checklist.domain.AdmHandbookDTO;
import com.gruposalinas.checklist.domain.AdmInformProtocolosDTO;
import com.gruposalinas.checklist.domain.AltaGerenteDTO;
import com.gruposalinas.checklist.domain.AsignaExpDTO;
import com.gruposalinas.checklist.domain.AsignaTransfDTO;
import com.gruposalinas.checklist.domain.BitacoraAdministradorDTO;
import com.gruposalinas.checklist.domain.BitacoraGralDTO;
import com.gruposalinas.checklist.domain.CanalDTO;
import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.CheckInAsistenciaDTO;
import com.gruposalinas.checklist.domain.CheckinDTO;
import com.gruposalinas.checklist.domain.ConfiguraActorDTO;
import com.gruposalinas.checklist.domain.ConsultaCecoProtocoloDTO;
import com.gruposalinas.checklist.domain.ConsultaFechasDTO;
import com.gruposalinas.checklist.domain.DatosInformeDTO;
import com.gruposalinas.checklist.domain.EmpContacExtDTO;
import com.gruposalinas.checklist.domain.GrupoDTO;
import com.gruposalinas.checklist.domain.HallazgosEviExpDTO;
import com.gruposalinas.checklist.domain.HallazgosExpDTO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;
import com.gruposalinas.checklist.domain.HorarioDTO;
import com.gruposalinas.checklist.domain.NegocioDTO;
import com.gruposalinas.checklist.domain.NivelDTO;
import com.gruposalinas.checklist.domain.OrdenGrupoDTO;
import com.gruposalinas.checklist.domain.PaisDTO;
import com.gruposalinas.checklist.domain.ParametroDTO;
import com.gruposalinas.checklist.domain.PerfilDTO;
import com.gruposalinas.checklist.domain.PerfilUsuarioDTO;
import com.gruposalinas.checklist.domain.PeriodoDTO;
import com.gruposalinas.checklist.domain.ProtocoloDTO;
import com.gruposalinas.checklist.domain.ProyectFaseTransfDTO;
import com.gruposalinas.checklist.domain.PuestoDTO;
import com.gruposalinas.checklist.domain.RecursoDTO;
import com.gruposalinas.checklist.domain.RecursoPerfilDTO;
import com.gruposalinas.checklist.domain.SoftNvoDTO;
import com.gruposalinas.checklist.domain.SoftOpeningDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;
import com.gruposalinas.checklist.domain.SucursalDTO;
import com.gruposalinas.checklist.domain.TipoArchivoDTO;
import com.gruposalinas.checklist.domain.TransformacionDTO;
import com.gruposalinas.checklist.domain.Usuario_ADTO;
import com.gruposalinas.checklist.domain.VersionChecklistGralDTO;
import com.gruposalinas.checklist.domain.VersionExpanDTO;
import com.gruposalinas.checklist.domain.ZonaNegoExpDTO;
import com.gruposalinas.checklist.resources.FRQAuthInterceptor;
import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.EstadosDTO;
import com.gruposalinas.checklist.domain.FaseTransfDTO;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;
import com.gruposalinas.checklist.domain.FragmentMenuDTO;
import com.gruposalinas.checklist.business.EmpFijoBI;
import com.gruposalinas.checklist.business.EstadosBI;
import com.gruposalinas.checklist.business.FaseTransfBI;
import com.gruposalinas.checklist.business.FirmaCheckBI;
import com.gruposalinas.checklist.business.FirmasCatalogoBI;
import com.gruposalinas.checklist.business.FragmentMenuBI;

@Controller
@RequestMapping("/catalogosService")
public class AltaCatalogosService {

	@Autowired
	CanalBI canalbi;
	@Autowired
	PerfilBI perfilbi;
	@Autowired
	PuestoBI puestobi;
	@Autowired
	TipoArchivoBI tipoarchivobi;
	@Autowired
	CecoBI cecobi;
	@Autowired
	SucursalBI sucursalbi;
	@Autowired
	Usuario_ABI usuarioabi;
	@Autowired
	PaisBI paisbi;
	@Autowired
	HorarioBI horariobi;
	@Autowired
	ParametroBI parametrobi;
	@Autowired
	NegocioBI negociobi;
	@Autowired
	NivelBI nivelbi;
	@Autowired
	PerfilUsuarioBI perfilusuariobi;
	@Autowired
	RecursoBI recursobi;
	@Autowired
	RecursoPerfilBI recursoperfilbi;
	@Autowired
	PosiblesBI posiblesbi;
	@Autowired
	PosibleTipoPreguntaBI posibletipopreguntabi;	
	@Autowired
	ChecklistNegocioBI checklistNegocioBI;
	@Autowired
	NegocioAdicionalBI adicionalBI;
	@Autowired
	GrupoBI grupoBI;
	@Autowired
	OrdenGrupoBI ordenGrupoBI;
	@Autowired
	PeriodoBI periodoBI;
	@Autowired
	PaisNegocioBI paisNegocioBI;
	@Autowired
	EmpFijoBI empFijoBI;
	@Autowired
	EstadosBI estadosBI;
	@Autowired
	BitacoraGralBI  bitacoraGralBI;
	@Autowired
	FirmaCheckBI	firmaCheckBI;
	@Autowired
	SucursalChecklistBI sucursalChecklistBI;
	@Autowired
	SoftOpeningBI softOpeningBI;
	@Autowired
	CheckinBI checkinBI;
	@Autowired
	AsignaExpBI 	asignaExpBI;
	@Autowired
	VersionExpanBI versionExpanBI;
	@Autowired
	VersionChecklistGralBI versionChecklistGralBI;
	@Autowired 
	ConsultaFechasBI consultaFechasBI;
	@Autowired
	EmpContacExtBI empContacExtBI;
	@Autowired
	HallazgosExpBI hallazgosExpBI;
	@Autowired	
	HallazgosEviExpBI hallazgosEviExpBI;
	@Autowired
	BitacoraAdministradorBI bitacoraAdministradorBI;
	@Autowired
	AdmHandbookBI admHandbookBI;
	@Autowired
	ProtocoloBI protocoloBI;
	@Autowired
	ZonaNegoBI zonaNegoBI;
	@Autowired
	AdicionalesBI adicionalesBI;
	@Autowired
	AltaGerenteBI altaGerenteBI;
    @Autowired
    AdmInformProtocolosBI admInformProtocolosBI;
	@Autowired
	HallazgosTransfBI hallazgosTransfBI;
	@Autowired
	TransformacionBI transformacionBI;
	@Autowired
	FaseTransfBI faseTransfBI;
	@Autowired
	ProyectFaseTransfBI proyectFaseTransfBI;
	@Autowired
	AsignaTransfBI asignaTransfBI;
	@Autowired
	FirmasCatalogoBI firmasCatalogoBI;
	@Autowired
	FragmentMenuBI fragmentMenuBI;
	@Autowired
	SoftNvoBI softNvoBI;
	@Autowired
	ActorEdoHallaBI actorEdoHallaBI;
	@Autowired
	ConfiguraActorBI configuraActorBI;
	@Autowired
	HallagosMatrizBI hallagosMatrizBI;
	@Autowired
	DatosInformeBI datosInformeBI;
	@Autowired
	CheckInAsistenciaBI checkInAsistenciaBI;public AltaCatalogosService() {
		// TODO Auto-generated constructor stub
	}

	
	private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);

	// http://localhost:8080/checklist/catalogosService/altaCanal.json?setActivo=<?>&descripcion=<?>
	@RequestMapping(value = "/altaCanal", method = RequestMethod.GET)
	public ModelAndView altaCanal(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String activo = request.getParameter("setActivo");
			String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");

			CanalDTO canal = new CanalDTO();
			canal.setActivo(Integer.parseInt(activo));
			canal.setDescrpicion(descripcion);
			int idCanal = canalbi.insertaCanal(canal);
			canal.setIdCanal(idCanal);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID DE CANAL CREADO");
			mv.addObject("res", idCanal);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/altaPerfil.json?setDescripcion=<?>
	@RequestMapping(value = "/altaPerfil", method = RequestMethod.GET)
	public ModelAndView altaPerfil(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String descripcion = new String(request.getParameter("setDescripcion").getBytes("ISO-8859-1"), "UTF-8");

			PerfilDTO perfil = new PerfilDTO();
			perfil.setDescripcion(descripcion);
			int idPerfil = perfilbi.insertaPerfil(perfil);
			perfil.setIdPerfil(idPerfil);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID DE PERFIL CREADO");
			mv.addObject("res", idPerfil);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/altaPuesto.json?idPuesto=<?>&setActivo=<?>&setCodigo=<?>&descripcion=<?>&idCanal=<?>&idNegocio=<?>&idNivel=<?>&idSubnegocio=<?>&idTipoPuesto=<?>
	@RequestMapping(value = "/altaPuesto", method = RequestMethod.GET)
	public ModelAndView altaPuesto(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idPuestoParam = request.getParameter("idPuesto");
			String activo = request.getParameter("setActivo");
			String codigo = request.getParameter("setCodigo");
			String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
			String idCanal = request.getParameter("idCanal");
			String idNegocio = request.getParameter("idNegocio");
			String idNivel = request.getParameter("idNivel");
			String idSubnegocio = request.getParameter("idSubnegocio");
			String idTipoPuesto = request.getParameter("idTipoPuesto");

			PuestoDTO puesto = new PuestoDTO();
			puesto.setIdPuesto(Integer.parseInt(idPuestoParam));
			puesto.setActivo(Integer.parseInt(activo));
			puesto.setCodigo(codigo);
			puesto.setDescripcion(descripcion);
			puesto.setIdCanal(Integer.parseInt(idCanal));
			puesto.setIdNegocio(Integer.parseInt(idNegocio));
			puesto.setIdNivel(Integer.parseInt(idNivel));
			puesto.setIdSubnegocio(Integer.parseInt(idSubnegocio));
			puesto.setIdTipoPuesto(Integer.parseInt(idTipoPuesto));

			int puestoId = puestobi.insertaPuesto(puesto);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID DE PUESTO CREADO");
			mv.addObject("res", puestoId);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/altaTipoArchivo.json?setNombreTipo=<?>
	@RequestMapping(value = "/altaTipoArchivo", method = RequestMethod.GET)
	public ModelAndView altaTipoArchivo(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String nombre = new String(request.getParameter("setNombreTipo").getBytes("ISO-8859-1"), "UTF-8");

			TipoArchivoDTO tipoArchivo = new TipoArchivoDTO();
			tipoArchivo.setNombreTipo(nombre);
			int idArchivo = tipoarchivobi.insertaTipoArchivo(tipoArchivo);
			tipoArchivo.setIdTipoArchivo(idArchivo);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID TIPO ARCHIVO CREADO");
			mv.addObject("res", idArchivo);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/altaCeco.json?activo=<?>&calle=<?>&ciudad=<?>&cp=<?>&idCeco=<?>&descCeco=<?>&faxContacto=<?>&idCanal=<?>&idCecoSuperior=<?>&idEstado=<?>&idNegocio=<?>&idNivel=<?>&idPais=<?>&nombreContacto=<?>&puestoContacto=<?>&telefonoContacto=<?>&fecha=<?>&usuarioMod=<?>
	@RequestMapping(value = "/altaCeco", method = RequestMethod.GET)
	public ModelAndView altaCeco(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String activo = request.getParameter("activo");
			String calle = new String(request.getParameter("calle").getBytes("ISO-8859-1"), "UTF-8");
			String ciudad = new String(request.getParameter("ciudad").getBytes("ISO-8859-1"), "UTF-8");
			String cp = new String(request.getParameter("cp").getBytes("ISO-8859-1"), "UTF-8");
			String idCeco = request.getParameter("idCeco");
			String descCeco = new String(request.getParameter("descCeco").getBytes("ISO-8859-1"), "UTF-8");
			String faxContacto = new String(request.getParameter("faxContacto").getBytes("ISO-8859-1"), "UTF-8");
			String idCanal = request.getParameter("idCanal");
			String idCecoSuperior = request.getParameter("idCecoSuperior");
			String idEstado = request.getParameter("idEstado");
			String idNegocio = request.getParameter("idNegocio");
			String idNivel = request.getParameter("idNivel");
			String idPais = request.getParameter("idPais");
			String nombreContacto = new String(request.getParameter("nombreContacto").getBytes("ISO-8859-1"), "UTF-8");
			String puestoContacto = new String(request.getParameter("puestoContacto").getBytes("ISO-8859-1"), "UTF-8");
			String telefonoContacto = new String(request.getParameter("telefonoContacto").getBytes("ISO-8859-1"), "UTF-8");
			String fecha = request.getParameter("fecha");
			String usuarioMod = request.getParameter("usuarioMod");

			CecoDTO ceco = new CecoDTO();
			ceco.setActivo(Integer.parseInt(activo));
			ceco.setCalle(calle);
			ceco.setCiudad(ciudad);
			ceco.setCp(cp);
			ceco.setDescCeco(descCeco);
			ceco.setFaxContacto(faxContacto);
			ceco.setIdCanal(Integer.parseInt(idCanal));
			ceco.setIdCeco(idCeco);
			ceco.setIdCecoSuperior(Integer.parseInt(idCecoSuperior));
			ceco.setIdEstado(Integer.parseInt(idEstado));
			ceco.setIdNegocio(Integer.parseInt(idNegocio));
			ceco.setIdNivel(Integer.parseInt(idNivel));
			ceco.setIdPais(Integer.parseInt(idPais));
			ceco.setNombreContacto(nombreContacto);
			ceco.setPuestoContacto(puestoContacto);
			ceco.setTelefonoContacto(telefonoContacto);
			ceco.setFechaModifico(fecha);
			ceco.setUsuarioModifico(usuarioMod);
			boolean cecoId = cecobi.insertaCeco(ceco);

			//ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

			mv.addObject("tipo", "CECO CREADO");
			mv.addObject("res", cecoId);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/altaSucursal.json?idCanal=<?>&idPais=<?>&latitud=<?>&longitud=<?>&nombreSuc=<?>&nuSuc=<?>
	@RequestMapping(value = "/altaSucursal", method = RequestMethod.GET)
	public ModelAndView altaSucursal(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idCanal = request.getParameter("idCanal");
			String idPais = request.getParameter("idPais");
			String latitud = request.getParameter("latitud");
			String longitud = request.getParameter("longitud");
			String nombreSuc = new String(request.getParameter("nombreSuc").getBytes("ISO-8859-1"), "UTF-8");
			String nuSuc = request.getParameter("nuSuc");

			SucursalDTO sucursal = new SucursalDTO();
			sucursal.setIdCanal(Integer.parseInt(idCanal));
			sucursal.setIdPais(Integer.parseInt(idPais));
			sucursal.setLatitud(Double.parseDouble(latitud));
			sucursal.setLongitud(Double.parseDouble(longitud));
			sucursal.setNombresuc(nombreSuc);
			sucursal.setNuSucursal(nuSuc);
			sucursal.setIdSucursal(0);
			int idS = sucursalbi.insertaSucursal(sucursal);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID SUCURSAL CREADA");
			mv.addObject("res", idS);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/altaSucursalGCC.json?idCanal=<?>&idPais=<?>&latitud=<?>&longitud=<?>&nombreSuc=<?>&nuSuc=<?>
		@RequestMapping(value = "/altaSucursalGCC", method = RequestMethod.GET)
		public ModelAndView altaSucursalGCC(HttpServletRequest request, HttpServletResponse response)
				throws UnsupportedEncodingException {
			try {
				String idCanal = request.getParameter("idCanal");
				String idPais = request.getParameter("idPais");
				String latitud = request.getParameter("latitud");
				String longitud = request.getParameter("longitud");
				String nombreSuc = new String(request.getParameter("nombreSuc").getBytes("ISO-8859-1"), "UTF-8");
				String nuSuc = request.getParameter("nuSuc");

				SucursalDTO sucursal = new SucursalDTO();
				sucursal.setIdCanal(Integer.parseInt(idCanal));
				sucursal.setIdPais(Integer.parseInt(idPais));
				sucursal.setLatitud(Double.parseDouble(latitud));
				sucursal.setLongitud(Double.parseDouble(longitud));
				sucursal.setNombresuc(nombreSuc);
				sucursal.setNuSucursal(nuSuc);
				
				int idS = sucursalbi.insertaSucursalGCC(sucursal);

				ModelAndView mv = new ModelAndView("altaModuloRes");
				mv.addObject("tipo", "ID SUCURSAL CREADA");
				mv.addObject("res", idS);
				return mv;
			} catch (Exception e) {
				logger.info(e);
				return null;
			}
		}

	// http://localhost:8080/checklist/catalogosService/altaUsuarioA.json?activo=<?>&fecha=<?>&idCeco=<?>&idPuesto=<?>&idUsuario=<?>&nombre=<?>
	@RequestMapping(value = "/altaUsuarioA", method = RequestMethod.GET)
	public ModelAndView altaUsuarioA(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String activo = request.getParameter("activo");
			String fecha = request.getParameter("fecha");
			String idCeco = request.getParameter("idCeco");
			String idPuesto = request.getParameter("idPuesto");
			String idUsuario = request.getParameter("idUsuario");
			String nombre = new String(request.getParameter("nombre").getBytes("ISO-8859-1"), "UTF-8");

			Usuario_ADTO usuarioA = new Usuario_ADTO();
			usuarioA.setActivo(Integer.parseInt(activo));
			usuarioA.setFecha(fecha);
			usuarioA.setIdCeco(idCeco);
			usuarioA.setIdPuesto(Integer.parseInt(idPuesto));
			usuarioA.setIdUsuario(Integer.parseInt(idUsuario));
			usuarioA.setNombre(nombre);
			boolean usuA = usuarioabi.insertaUsuario(usuarioA);

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "FRTA Usuario CREADO");
			mv.addObject("res", usuA);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/altaPais.json?activo=<?>&nombre=<?>
	@RequestMapping(value = "/altaPais", method = RequestMethod.GET)
	public ModelAndView altaPais(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String activo = request.getParameter("activo");
			String nombre = new String(request.getParameter("nombre").getBytes("ISO-8859-1"), "UTF-8");

			PaisDTO pais = new PaisDTO();
			pais.setActivo(Integer.parseInt(activo));
			pais.setNombre(nombre);
			int idP = paisbi.insertaPais(pais);
			pais.setIdPais(idP);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID PAIS CREADO");
			mv.addObject("res", idP);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/altaHorario.json?cveHorario=<?>&valorIni=<?>&valorFin=<?>
	@RequestMapping(value = "/altaHorario", method = RequestMethod.GET)
	public ModelAndView altaHorario(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String cveHorario = request.getParameter("cveHorario");
			String valorIni = request.getParameter("valorIni");
			String valorFin = request.getParameter("valorFin");

			HorarioDTO horario = new HorarioDTO();
			horario.setCveHorario(cveHorario);
			horario.setValorIni(valorIni);
			horario.setValorFin(valorFin);
			int idHora = horariobi.insertaHorario(horario);
			horario.setIdHorario(idHora);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID HORARIO CREADO");
			mv.addObject("res", idHora);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/altaParametro.json?setActivo=<?>&setClave=<?>&setValor=<?>
	//http://localhost:8080/checklist/catalogosService/altaParametro.json?setActivo=1&setClave=correoBuzon&setValor=checklistbaz@bancoazteca.com.mx
	@RequestMapping(value = "/altaParametro", method = RequestMethod.GET)
	public ModelAndView altaParametro(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String activo = request.getParameter("setActivo");
			String clave = request.getParameter("setClave");
			String valor = request.getParameter("setValor");

			ParametroDTO parametro = new ParametroDTO();
			parametro.setActivo(Integer.parseInt(activo));
			parametro.setClave(clave);
			parametro.setValor(valor);
			boolean param = parametrobi.insertaPrametros(parametro);

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "PARAMETRO CREADO CORRECTAMENTE");
			mv.addObject("res", param);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/altaNegocio.json?setActivo=<?>&descripcion=<?>
	@RequestMapping(value = "/altaNegocio", method = RequestMethod.GET)
	public ModelAndView altaNegocio(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String activo = request.getParameter("setActivo");
			String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");

			NegocioDTO negocio = new NegocioDTO();
			negocio.setActivo(Integer.parseInt(activo));
			negocio.setDescripcion(descripcion);
			int id = negociobi.insertaNegocio(negocio);
			negocio.setIdNegocio(id);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "NEGOCIO CREADO CORRECTAMENTE");
			mv.addObject("res", id);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}
	
	
	// http://localhost:8080/checklist/catalogosService/altaNegocioNuevo.json?setActivo=<?>&descripcion=<?>&idNegocio=<?>
	@RequestMapping(value = "/altaNegocioNuevo", method = RequestMethod.GET)
	public ModelAndView altaNegocioNuevo(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String activo = request.getParameter("setActivo");
			String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
			String idNegocio = request.getParameter("idNegocio");

			NegocioDTO negocio = new NegocioDTO();
			negocio.setActivo(Integer.parseInt(activo));
			negocio.setDescripcion(descripcion);
			negocio.setIdNegocio(Integer.parseInt(idNegocio));
			boolean resp = negociobi.insertaNegocioNuevo(negocio);
			
			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "NEGOCIO CREADO CORRECTAMENTE");
			mv.addObject("res", resp);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/altaNivel.json?setActivo=<?>&codigo=<?>&descripcion=<?>&idNegocio=<?>
	@RequestMapping(value = "/altaNivel", method = RequestMethod.GET)
	public ModelAndView altaNivel(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String activo = request.getParameter("setActivo");
			String codigo = request.getParameter("codigo");
			String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
			String idNegocio = request.getParameter("idNegocio");

			NivelDTO nivel = new NivelDTO();
			nivel.setActivo(Integer.parseInt(activo));
			nivel.setCodigo(codigo);
			nivel.setDescripcion(descripcion);
			nivel.setIdNegocio(Integer.parseInt(idNegocio));
			int id = nivelbi.insertaNivel(nivel);
			nivel.setIdNivel(id);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "NIVEL CREADO CORRECTAMENTE");
			mv.addObject("res", id);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
		
		
	}
	
	// http://localhost:8080/checklist/catalogosService/altaPerfilUsuario.json?idUsuario=<?>&idPerfil=<?>
	@RequestMapping(value="/altaPerfilUsuario", method = RequestMethod.GET)
	public ModelAndView altaPerfilUsuario(HttpServletRequest request, HttpServletResponse response){
		try{
			
			String idUsuario = request.getParameter("idUsuario");
			String idPerfil = request.getParameter("idPerfil");
			
			PerfilUsuarioDTO perfilUsuarioDTO = new PerfilUsuarioDTO();
			perfilUsuarioDTO.setIdPerfil(Integer.parseInt(idPerfil));
			perfilUsuarioDTO.setIdUsuario(Integer.parseInt(idUsuario));
			
			boolean res = perfilusuariobi.insertaPerfilUsuario(perfilUsuarioDTO);
			
			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "PERFIL ASIGNADO : ");
			mv.addObject("res",res);
			
			return mv;
			
		}catch(Exception e){
			logger.info(e);
			return null;
		}
	}
	
	//http://localhost:8080/checklist/catalogosService/altaRecurso.json?nombRecurso=<?>
	@RequestMapping(value="/altaRecurso", method = RequestMethod.GET)
	public ModelAndView altaRecurso(HttpServletRequest request, HttpServletResponse response){
		try{
			String nombRecurso= request.getParameter("nombRecurso");
			
			RecursoDTO recursoDTO = new RecursoDTO();
			recursoDTO.setNombreRecurso(nombRecurso);
			
			int res = recursobi.insertaRecurso(recursoDTO);
			
			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID RECURSO ASIGNADO ");
			mv.addObject("res", res);
			
			return mv;
		}catch(Exception e){
			logger.info(e);
			return null;
		}
	}
	
	//http://localhost:8080/checklist/catalogosService/altaRecursoPerfil.json?idPerfil=<?>&idRecurso=<?>&inserta=<?>&consulta=<?>&elimina=<?>&modifca=<?>
	@RequestMapping(value="/altaRecursoPerfil", method = RequestMethod.GET)
	public ModelAndView altaRecursoPerfil(HttpServletRequest request, HttpServletResponse response){
		try{

			String idPerfil = request.getParameter("idPerfil");
			String idRecurso = request.getParameter("idRecurso");
			String consulta = request.getParameter("consulta");
			String elimina = request.getParameter("elimina");
			String inserta = request.getParameter("inserta");
			String modifica = request.getParameter("modifca");


			RecursoPerfilDTO recursoPerfilDTO = new RecursoPerfilDTO();

			recursoPerfilDTO.setIdPerfil(idPerfil);
			recursoPerfilDTO.setIdRecurso(idRecurso);
			recursoPerfilDTO.setInserta(Integer.parseInt(inserta));
			recursoPerfilDTO.setConsulta(Integer.parseInt(consulta));
			recursoPerfilDTO.setElimina(Integer.parseInt(elimina));
			recursoPerfilDTO.setModifica(Integer.parseInt(modifica));

			boolean res = recursoperfilbi.insertaRecursoP(recursoPerfilDTO);
			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "RECURSO PERFIL ASIGNADO: ");
			mv.addObject("res", res);
			
			return mv;
		}catch(Exception e){
			logger.info(e);
			return null;
		}
	}	
	
	
	//http://localhost:8080/checklist/catalogosService/altaPosible.json?respuesta=<?>
	@RequestMapping(value="/altaPosible", method = RequestMethod.GET)
	public ModelAndView altaPosible(HttpServletRequest request, HttpServletResponse response){
		try{
			String respuesta= new String (request.getParameter("respuesta").getBytes("ISO-8859-1"), "UTF-8");
			
			
			char[] ca = { '\u0061', '\u0301' };
			char[] ce = { '\u0065', '\u0301' };
			char[] ci = { '\u0069', '\u0301' };
			char[] co = { '\u006F', '\u0301' };
			char[] cu = { '\u0075', '\u0301' };
			// mayusculas
			char[] c1 = { '\u0041', '\u0301' };
			char[] c2 = { '\u0045', '\u0301' };
			char[] c3 = { '\u0049', '\u0301' };
			char[] c4 = { '\u004F', '\u0301' };
			char[] c5 = { '\u0055', '\u0301' };
			char[] c6 = { '\u006E', '\u0303' };

			String setPregunta = request.getParameter("respuesta");
			//System.out.println("setPregunta1 " + setPregunta);
			if (setPregunta.contains(String.valueOf(ca)) || setPregunta.contains(String.valueOf(ce))
					|| setPregunta.contains(String.valueOf(ci)) || setPregunta.contains(String.valueOf(co))
					|| setPregunta.contains(String.valueOf(cu)) || setPregunta.contains(String.valueOf(c1))
					|| setPregunta.contains(String.valueOf(c2)) || setPregunta.contains(String.valueOf(c3))
					|| setPregunta.contains(String.valueOf(c4)) || setPregunta.contains(String.valueOf(c5))
					|| setPregunta.contains(String.valueOf(c6))) {

				String rr = setPregunta.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
						.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
						.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
						.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
						.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
						.replaceAll(String.valueOf(c6), "ñ");
				//System.out.println("rr " + rr);
				
				setPregunta=rr;
				rr = null;
			}	
			
			
			int res = posiblesbi.insertaPosible(setPregunta);
			
			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID POSIBLE ASIGNADO ");
			mv.addObject("res", res);
			
			return mv;
		}catch(Exception e){
			logger.info(e);
			return null;
		}
	}
	
	//http://localhost:8080/checklist/catalogosService/altaPosibleTipo.json?tipoPreg=<?>&idPosible=<?>
	@RequestMapping(value="/altaPosibleTipo", method = RequestMethod.GET)
	public ModelAndView altaPosibleTipo(HttpServletRequest request, HttpServletResponse response){
		try{
			String idTipoPregunta= request.getParameter("tipoPreg");
			String idPosible= request.getParameter("idPosible");
			
			int res = posibletipopreguntabi.insertaPosibleTipoPregunta(Integer.parseInt(idPosible), Integer.parseInt(idTipoPregunta));
			
			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID POSIBLE TIPO ASIGNADO ");
			mv.addObject("res", res);
			
			return mv;
		}catch(Exception e){
			logger.info(e);
			return null;
		}
	}
	
	//http://localhost:8080/checklist/catalogosService/altaChecklistNegocio.json?idChecklist=<?>&idNegocio=<?>
	@RequestMapping(value="/altaChecklistNegocio", method = RequestMethod.GET)
	public ModelAndView altaChecklistNegocio(HttpServletRequest request, HttpServletResponse response){
		try{
			String idChecklist= request.getParameter("idChecklist");
			String idNegocio= request.getParameter("idNegocio");
			
			boolean res = checklistNegocioBI.insertaChecklistNegocio(Integer.parseInt(idChecklist), Integer.parseInt(idNegocio));
			
			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Checklist-Negocio Asignado :");
			mv.addObject("res", res);
			
			return mv;
		}catch(Exception e){
			logger.info(e);
			return null;
		}
	}
	
	
	//http://localhost:8080/checklist/catalogosService/altaNegocioAdicional.json?idUsuario=<?>&idNegocio=<?>
	@RequestMapping(value="/altaNegocioAdicional", method = RequestMethod.GET)
	public ModelAndView altaNegocioAdicional(HttpServletRequest request, HttpServletResponse response){
		try{
			String idUsuario= request.getParameter("idUsuario");
			String idNegocio= request.getParameter("idNegocio");
			
			boolean res = adicionalBI.insertaNegocioAdicional(Integer.parseInt(idUsuario), Integer.parseInt(idNegocio));
			
			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Usuario-Negocio Adicional Asignado :");
			mv.addObject("res", res);
			
			return mv;
		}catch(Exception e){
			logger.info(e);
			return null;
		}
	}
	
	//http://localhost:8080/checklist/catalogosService/altaNegocioPais.json?idNegocio=<?>&idPais=<?>
	@RequestMapping(value="/altaNegocioPais", method = RequestMethod.GET)
	public ModelAndView altaNegocioPais(HttpServletRequest request, HttpServletResponse response){
		try{
			
			String idNegocio= request.getParameter("idNegocio");
			String idPais= request.getParameter("idPais");
			
			boolean res = paisNegocioBI.insertaPaisNegocio(Integer.parseInt(idNegocio), Integer.parseInt(idPais));
			
			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Usuario-Negocio Adicional Asignado :");
			mv.addObject("res", res);
			
			return mv;
		}catch(Exception e){
			logger.info(e);
			return null;
		}
	}
	

	//http://localhost:8080/checklist/catalogosService/altaGrupo.json?commit=<?>&descripcion=<?>
	@RequestMapping(value="/altaGrupo", method = RequestMethod.GET)
	public ModelAndView altaGrupo(HttpServletRequest request, HttpServletResponse response){
		try{
			
			String commit= request.getParameter("commit");
			String descripcion= request.getParameter("descripcion");
			
			GrupoDTO grupo = new GrupoDTO();
			grupo.setCommit(Integer.parseInt(commit));
			grupo.setDescripcion(descripcion);
			
			int res = grupoBI.insertaGrupo(grupo);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "Id Grupo");
			mv.addObject("res", res);
			
			return mv;
		}catch(Exception e){
			logger.info(e);
			return null;
		}
	}
	
	

	//http://localhost:8080/checklist/catalogosService/altaOrdenGrupo.json?idGrupo=<?>&idChecklist=<?>&orden=<?>&commit=<?>
	@RequestMapping(value="/altaOrdenGrupo", method = RequestMethod.GET)
	public ModelAndView altaOrdenGrupo(HttpServletRequest request, HttpServletResponse response){
		try{
			
			String idGrupo= request.getParameter("idGrupo");
			String idChecklist= request.getParameter("idChecklist");
			String orden= request.getParameter("orden");
			String commit= request.getParameter("commit");
			
			OrdenGrupoDTO grupoOrden = new OrdenGrupoDTO();
			grupoOrden.setIdGrupo(Integer.parseInt(idGrupo));
			grupoOrden.setIdChecklist(Integer.parseInt(idChecklist));
			grupoOrden.setOrden(Integer.parseInt(orden));
			grupoOrden.setCommit(Integer.parseInt(commit));
			
			int res = ordenGrupoBI.insertaGrupo(grupoOrden);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "Id Orden Grupo");
			mv.addObject("res", res);
			
			return mv;
		}catch(Exception e){
			logger.info(e);
			return null;
		}
	}
	
	

	//http://localhost:8080/checklist/catalogosService/altaPeriodo.json?commit=<?>&descripcion=<?>
	@RequestMapping(value="/altaPeriodo", method = RequestMethod.GET)
	public ModelAndView altaPeriodo(HttpServletRequest request, HttpServletResponse response){
		try{
			
			String commit= request.getParameter("commit");
			String descripcion= request.getParameter("descripcion");
			
			PeriodoDTO periodo = new PeriodoDTO();
			periodo.setIdPeriodo(Integer.parseInt(commit));
			periodo.setDescripcion(descripcion);
			

			int res = periodoBI.insertaPeriodo(periodo);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "Id Grupo");
			mv.addObject("res", res);
			
			return mv;
		}catch(Exception e){
			logger.info(e);
			return null;
		}
	}
	
	//http://localhost:8080/checklist/catalogosService/altaEmpfijo.json?idUsuario=<?>&idActivo=<?>
		@RequestMapping(value="/altaEmpfijo", method = RequestMethod.GET)
		public ModelAndView altaEmpfijo(HttpServletRequest request, HttpServletResponse response){
			try{
				
				String idUsuario= request.getParameter("idUsuario");
				String idActivo= request.getParameter("idActivo");
				
				EmpFijoDTO empFijoDTO = new EmpFijoDTO();
				empFijoDTO.setIdUsuario(Integer.parseInt(idUsuario));
				empFijoDTO.setIdActivo(Integer.parseInt(idActivo));
				
				int res = empFijoBI.inserta(empFijoDTO);

				ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
				mv.addObject("tipo", "Alta empleado "+idUsuario);
				mv.addObject("res", res);
				
				return mv;
			}catch(Exception e){
				logger.info(e);
				return null;
			}
		}
		
		//http://localhost:8080/checklist/catalogosService/altaEstado.json?idPais=<?>&idEstado=<?>&descripcion=<?>&abreviatura=<?>
		@RequestMapping(value="/altaEstado", method = RequestMethod.GET)
		public ModelAndView altaEstado(HttpServletRequest request, HttpServletResponse response){
			try{
				
				String idPais= request.getParameter("idPais");
				String idEstado= request.getParameter("idEstado");
				String descripcion= request.getParameter("descripcion");
				String abreviatura= request.getParameter("abreviatura");
				
				EstadosDTO estadosDTO = new EstadosDTO();
				estadosDTO.setIdPais(Integer.parseInt(idPais));
				estadosDTO.setIdEstado(Integer.parseInt(idEstado));
				estadosDTO.setDescripcion(descripcion);
				estadosDTO.setAbreviatura(abreviatura);
				
				int respuesta = estadosBI.inserta(estadosDTO);

				ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
				mv.addObject("tipo", "Alta Estado "+idEstado);
				mv.addObject("res", respuesta);
				
				return mv;
			}catch(Exception e){
				logger.info(e);
				return null;
			}
		}
		
		//SERVICIOS PARA EXPANSION
		
		//http://localhost:8080/checklist/catalogosService/altaBitaGral.json?finicio=<?>&fin=<?>&status=<?>
				@RequestMapping(value="/altaBitaGral", method = RequestMethod.GET)
				public ModelAndView altaBitaGral(HttpServletRequest request, HttpServletResponse response){
					try{
						
						String finicio= request.getParameter("finicio");
						String fin= request.getParameter("fin");
						int status=Integer.parseInt( request.getParameter("status"));
						
						BitacoraGralDTO bitaG = new BitacoraGralDTO();
						bitaG.setFinicio(finicio);
						bitaG.setFin(fin);
						bitaG.setStatus(status);
						
						int respuesta = bitacoraGralBI.inserta(bitaG);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta Bitacora Gral ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				//http://localhost:8080/checklist/catalogosService/altaBitaH.json?idBitaGral=<?>&idBita=<?>&finicio=<?>&fin=<?>&status=<?>
				@RequestMapping(value="/altaBitaH", method = RequestMethod.GET)
				public ModelAndView altaBitaH(HttpServletRequest request, HttpServletResponse response){
					try{
						int idBitaGral=Integer.parseInt(request.getParameter("idBitaGral"));
						int idBita=Integer.parseInt(request.getParameter("idBita"));
						String finicio= request.getParameter("finicio");
						String fin= request.getParameter("fin");
						int status=Integer.parseInt( request.getParameter("status"));
						
						
						BitacoraGralDTO bitaH = new BitacoraGralDTO();
						
						bitaH.setIdBitaGral(idBitaGral);
						bitaH.setIdBita(idBita);
						bitaH.setFinicio(finicio);
						bitaH.setFin(fin);
						bitaH.setStatus(status);
						
						
						int respuesta = bitacoraGralBI.insertaH(bitaH);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta Bitacora Gral_hija "+idBitaGral+" : "+idBita);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaBitaHijaPadre.json?bitacora=<?>
				@RequestMapping(value="/altaBitaHijaPadre", method = RequestMethod.GET)
				public ModelAndView altaBitaHijaPadre(HttpServletRequest request, HttpServletResponse response){
					try{
						String bitacora=request.getParameter("bitacora");
						
						
						BitacoraGralDTO bitaSer = new BitacoraGralDTO();
						
						bitaSer.setBitacora(bitacora);
						
						int respuesta = bitacoraGralBI.ejecutaSP(bitacora);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta Bitacoras General e Hijas "+bitacora+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaFirmaCheck.json?bitacora=<?>&ceco=<?>&idUsuario=<?>&puesto=<?>&responsable=<?>&correo=<?>&nombre=<?>&ruta=<?>
				@RequestMapping(value="/altaFirmaCheck", method = RequestMethod.GET)
				public ModelAndView altaFirmaCheck(HttpServletRequest request, HttpServletResponse response){
					try{
						String bitacora=request.getParameter("bitacora");
						String ceco=request.getParameter("ceco");
						String idUsuario=request.getParameter("idUsuario");
						String puesto=request.getParameter("puesto");
						String responsable=request.getParameter("responsable");
						String correo=request.getParameter("correo");
						String nombre=request.getParameter("nombre");
						String ruta=request.getParameter("ruta");
						
						FirmaCheckDTO firm = new FirmaCheckDTO();
						
						firm.setBitacora(bitacora);
						firm.setCeco(ceco);
						firm.setIdUsuario(idUsuario);
						firm.setPuesto(puesto);
						firm.setResponsable(responsable);
						firm.setCorreo(correo);
						firm.setNombre(nombre);
						firm.setRuta(ruta);
						
						int respuesta = firmaCheckBI.inserta(firm);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de Firma de checklist  del ceco"+ceco);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaImagSuc.json?idSucursal=<?>&nombre=<?>&ruta=<?>&observ=<?>
				@RequestMapping(value="/altaImagSuc", method = RequestMethod.GET)
				public ModelAndView altaImagSuc(HttpServletRequest request, HttpServletResponse response){
					try{
						int idSucursal=Integer.parseInt(request.getParameter("idSucursal"));
						String nombre=request.getParameter("nombre");
						String ruta=request.getParameter("ruta");
						String observ=request.getParameter("observ");
						

						SucursalChecklistDTO suci = new SucursalChecklistDTO();
						
						suci.setIdSucursal(idSucursal);
						suci.setNombre(nombre);
						suci.setRuta(ruta);
						suci.setObserv(observ);
						
						int respuesta = sucursalChecklistBI.inserta(suci);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta  imagen Sucursal "+idSucursal+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://10.51.219.179:8080/checklist/catalogosService/altaCeckin.json?idBitacora=28964&idUsuario=82144&idCeco=480100&fecha=20190806&latitud=45&longitud=456&bandera=1&plataforma=IOS&tiempoConexion=1
				@RequestMapping(value="/altaCeckin", method = RequestMethod.GET)
				public ModelAndView altaCeckin(HttpServletRequest request, HttpServletResponse response){
					try{
						int idBitacora=Integer.parseInt(request.getParameter("idBitacora"));
						int idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
						String idCeco=request.getParameter("idCeco");
						String fecha=request.getParameter("fecha");
						String latitud=request.getParameter("latitud");
						String longitud=request.getParameter("longitud");
						int bandera=Integer.parseInt(request.getParameter("bandera"));
						String plataforma=request.getParameter("plataforma");
						int tiempoConexion=Integer.parseInt(request.getParameter("tiempoConexion"));

						CheckinDTO asistencia = new CheckinDTO();
						asistencia.setIdBitacora(idBitacora);
						asistencia.setIdUsuario(idUsuario);
						asistencia.setIdCeco(idCeco);
						asistencia.setFecha(fecha);
						asistencia.setLatitud(latitud); 
						asistencia.setLongitud(longitud);
						asistencia.setBandera(bandera);
						asistencia.setPlataforma(plataforma);
						asistencia.setTiempoConexion(tiempoConexion);
						
						
						int respuesta = checkinBI.insertaCheckin(asistencia);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta  checkin bitacora "+idBitacora+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaSoftOp.json?bitacora=<?>
				@RequestMapping(value="/altaSoftOp", method = RequestMethod.GET)
				public ModelAndView altaSoftOp(HttpServletRequest request, HttpServletResponse response){
					try{
						
						int bitacora=Integer.parseInt(request.getParameter("bitacora"));

						SoftOpeningDTO so = new SoftOpeningDTO();
					so.setBitacora(bitacora);
						
						
						int respuesta = softOpeningBI.inserta(so);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta  soft con bitacora "+bitacora+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				//YYYYMMDD HH24:MI:SS:FF3
				//http://localhost:8080/checklist/catalogosService/altaSoftOpMan.json?ceco=<?>&idUsuario=<?>&bitacora=<?>&recorrido=<?>&recorrido=<?>&fechaini=<?>&fechafin=<?>&aux=<?>
				@RequestMapping(value="/altaSoftOpMan", method = RequestMethod.GET)
				public ModelAndView altaSoftOpMan(HttpServletRequest request, HttpServletResponse response){
					try{
						int ceco=Integer.parseInt(request.getParameter("ceco"));
						int idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
						int bitacora=Integer.parseInt(request.getParameter("bitacora"));
						int recorrido=Integer.parseInt(request.getParameter("recorrido"));
						String fechaini=request.getParameter("fechaini");
						String fechafin=request.getParameter("fechafin");
						String aux=request.getParameter("aux");

						SoftOpeningDTO so = new SoftOpeningDTO();
						
						so.setCeco(ceco);
						so.setIdUsuario(idUsuario);
						so.setBitacora(bitacora);
						so.setRecorrido(recorrido);
						so.setFechaini(fechaini);
						so.setFechafin(fechafin);
						so.setAux(aux);
						
						int respuesta = softOpeningBI.insertaMan(so);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta  soft con ceco: "+ceco+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaAsignaExp.json?ceco=<?>&version=<?>&usuario=<?>&usuario_asig=<?>&obs=<?>
				@RequestMapping(value="/altaAsignaExp", method = RequestMethod.GET)
				public ModelAndView altaAsignaExp(HttpServletRequest request, HttpServletResponse response){
					try{
						int ceco=Integer.parseInt(request.getParameter("ceco"));
						int version=Integer.parseInt(request.getParameter("version"));
						int usuario=Integer.parseInt(request.getParameter("usuario"));
						int usuario_asig=Integer.parseInt(request.getParameter("usuario_asig"));
						String obs=request.getParameter("obs");


						AsignaExpDTO asig = new AsignaExpDTO();
						
						asig.setCeco(ceco);
						asig.setVersion(version);
						asig.setUsuario_asig(usuario_asig);
						asig.setUsuario(usuario);
						asig.setObs(obs);
						
						int respuesta = asignaExpBI.inserta(asig);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta   con ceco: "+ceco+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaVersionExp.json?idCheck=<?>&idVers=<?>&idactivo=<?>&obs=<?>&aux=<?>
				@RequestMapping(value="/altaVersionExp", method = RequestMethod.GET)
				public ModelAndView altaVersionExp(HttpServletRequest request, HttpServletResponse response){
					try{
						int idCheck=Integer.parseInt(request.getParameter("idCheck"));
						int idVers=Integer.parseInt(request.getParameter("idVers"));
						int idactivo=Integer.parseInt(request.getParameter("idactivo"));
						String obs=request.getParameter("obs");
						String aux=request.getParameter("aux");
				


						VersionExpanDTO ver = new VersionExpanDTO();
						
					ver.setIdCheck(idCheck);
					ver.setIdVers(idVers);
					ver.setIdactivo(idactivo);
					ver.setObs(obs);
					ver.setAux(aux);
						
						int respuesta = versionExpanBI.inserta(ver);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta   con ceco: "+idCheck+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/AltaVersionGral.json?idCheck=<?>&idVers=<?>&idactivo=<?>&obs=<?>&&nombrecheck=<?>&versionAnt=<?>
				@RequestMapping(value="/AltaVersionGral", method = RequestMethod.GET)
				public ModelAndView AltaVersionGral(HttpServletRequest request, HttpServletResponse response){
					try{
					
						int idCheck=Integer.parseInt(request.getParameter("idCheck"));
						int idVers=Integer.parseInt(request.getParameter("idVers"));
						int idactivo=Integer.parseInt(request.getParameter("idactivo"));
						String obs=request.getParameter("obs");
						String nombrecheck=request.getParameter("nombrecheck");
						int versionAnt =Integer.parseInt(request.getParameter("versionAnt"));

						VersionChecklistGralDTO ver = new VersionChecklistGralDTO();
						
						
						ver.setIdCheck(idCheck);
						ver.setIdVers(idVers);
						ver.setIdactivo(idactivo);
						ver.setObs(obs);
						ver.setVersionAnt(versionAnt);
						ver.setNombrecheck(nombrecheck);
						
						
						int respuesta = versionChecklistGralBI.inserta(ver);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "update   checklist: "+idCheck+" : con la version: "+idVers+"");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				//http://localhost:8080/checklist/catalogosService/AltaContactoExt.json?ceco=<?>&bitGral=<?>&idUsu=<?>&domicilio=<?>&&nombre=<?>&telefono=<?>&contacto=<?>&area=<?>&compania=<?>aux=<?>aux2=<?>
				@RequestMapping(value="/AltaContactoExt", method = RequestMethod.GET)
				public ModelAndView AltaContactoExt(HttpServletRequest request, HttpServletResponse response){
					try{
						
						
						String ceco=request.getParameter("ceco");
						int bitGral=Integer.parseInt(request.getParameter("bitGral"));
						int idUsu=Integer.parseInt(request.getParameter("idUsu"));
						String domicilio=request.getParameter("domicilio");
						String nombre=request.getParameter("nombre");
						String telefono=request.getParameter("telefono");
						String contacto=request.getParameter("contacto");
						String area=request.getParameter("area");
						String compania=request.getParameter("compania");
						String aux=request.getParameter("aux");
						String aux2=request.getParameter("aux2");
						
						
						EmpContacExtDTO cont = new EmpContacExtDTO();
						
						cont.setCeco(ceco);
						cont.setBitGral(bitGral);
						cont.setIdUsu(idUsu);
						cont.setDomicilio(domicilio);
						cont.setNombre(nombre);
						cont.setTelefono(telefono);
						cont.setContacto(contacto);
						cont.setArea(area);
						cont.setCompania(compania);
						cont.setAux(aux);
						cont.setAux2(aux2);
					
						
						int respuesta = empContacExtBI.inserta(cont);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "alta   ceco: "+ceco+" : con la bitGral: "+bitGral+"");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				
				// http://10.51.219.179:8080/checklist/catalogosService/altaFechas.json?fechaInicial=01012020&fechaFinal=01012020
				@RequestMapping(value="/altaFechas", method = RequestMethod.GET)
				public ModelAndView altaFechas(HttpServletRequest request, HttpServletResponse response){
					try{
						
						String fechaInicial= request.getParameter("fechaInicial");
						String fechaFinal= request.getParameter("fechaFinal");

						
						ConsultaFechasDTO consultaFecha = new ConsultaFechasDTO();
						consultaFecha.setFechaInicial(fechaInicial);
						consultaFecha.setFechaFinal(fechaFinal);
				
						boolean res = consultaFechasBI.insertaFecha(consultaFecha);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Registro Insertado");
						mv.addObject("res", res);
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				// http://10.51.219.179:8080/checklist/catalogosService/altaHallazgo.json?idResp=&status=&idPreg=&preg=&respuesta=&pregPadre=&responsable=&disposi=&area=&obs=&bitGral=&sla=&aux2=&fechaIni=
				@RequestMapping(value="/altaHallazgo", method = RequestMethod.GET)
				public ModelAndView altaHallazgo(HttpServletRequest request, HttpServletResponse response){
					try{
						
						
						int idResp= Integer.parseInt(request.getParameter("idResp"));
						int status= Integer.parseInt(request.getParameter("status"));
						int idPreg= Integer.parseInt(request.getParameter("idPreg"));
						String preg= request.getParameter("preg");
						String respuesta= request.getParameter("respuesta");
						int pregPadre= Integer.parseInt(request.getParameter("pregPadre"));
						String responsable= request.getParameter("responsable");
						String disposi= request.getParameter("disposi");
						String area= request.getParameter("area");
						String obs= request.getParameter("obs");
						int bitGral= Integer.parseInt(request.getParameter("bitGral"));
						String sla= request.getParameter("sla");
						int aux2= Integer.parseInt(request.getParameter("aux2"));
						
						
						HallazgosExpDTO h = new HallazgosExpDTO();
				
						h.setIdResp(idResp);
						h.setStatus(status);
						h.setIdPreg(idPreg);
						h.setPreg(preg);
						h.setRespuesta(respuesta);
						h.setPregPadre(pregPadre);
						h.setResponsable(responsable);
						h.setDisposi(disposi);
						h.setArea(area);
						h.setObs(obs);
						h.setBitGral(bitGral);
						h.setSla(sla);
						h.setAux2(aux2);
						
				
						int res = hallazgosExpBI.inserta(h);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Registro Insertado");
						mv.addObject("res", res);
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				// http://10.51.219.179:8080/checklist/catalogosService/altaHallazgoEvi.json?idHallazgo=&idResp=&ruta=&nombre=&totEvi=
				@RequestMapping(value="/altaHallazgoEvi", method = RequestMethod.GET)
				public ModelAndView altaHallazgoEvi(HttpServletRequest request, HttpServletResponse response){
					try{
						
						
						int idHallazgo= Integer.parseInt(request.getParameter("idHallazgo"));
						int idResp= Integer.parseInt(request.getParameter("idResp"));
						String ruta= request.getParameter("ruta");
						String nombre= request.getParameter("nombre");
						int totEvi= Integer.parseInt(request.getParameter("totEvi"));
						
						
						HallazgosEviExpDTO he = new HallazgosEviExpDTO();
						
						he.setIdHallazgo(idHallazgo);
						he.setIdResp(idResp);
						he.setRuta(ruta);
						he.setNombre(nombre);
						he.setTotEvi(totEvi);
						
				
						int res = hallazgosEviExpBI.inserta(he);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Registro INSERTADO");
						mv.addObject("res", res);
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://10.51.219.179:8080/checklist/catalogosService/altaBitaco.json?idCheckUsua=85549&modo=1&ceco=1
				@RequestMapping(value="/altaBitaco", method = RequestMethod.GET)
				public ModelAndView altaBitaco(HttpServletRequest request, HttpServletResponse response){
					try{
						
						int idBitacora=0;
						
						String idBitacoraS = request.getParameter("idBitacora");
						if(idBitacoraS != null) {
							idBitacora = Integer.parseInt(idBitacoraS);
							}
						
						int idCheckUsua =Integer.parseInt(request.getParameter("idCheckUsua"));
						int modo = Integer.parseInt(request.getParameter("modo"));
						String ceco = request.getParameter("ceco");
						//int idBitacora = Integer.parseInt(request.getParameter("idBitacora"));
				
					 
						
						BitacoraAdministradorDTO bitacoraAdministrador = new BitacoraAdministradorDTO();
						
						bitacoraAdministrador.setIdCheckUsua(idCheckUsua);
						bitacoraAdministrador.setModo(modo);
						bitacoraAdministrador.setCeco(ceco);
						bitacoraAdministrador.setIdBitacora(idBitacora);
	
						 
						int respuesta =  bitacoraAdministradorBI.insertaBitacora(bitacoraAdministrador);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de Bitacora:" + respuesta);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				// http://10.51.219.179:8080/checklist/catalogosService/cargaHallazgo.json?bitGral=<?>
				@RequestMapping(value = "/cargaHallazgo", method = RequestMethod.GET)
				public ModelAndView cargaHallazgo(HttpServletRequest request, HttpServletResponse response)
				{
					try {
						
						int bitGral =Integer.parseInt(request.getParameter("bitGral"));

						boolean res = hallazgosExpBI.cargaHallazgos(bitGral);
						
						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "HALLAZGOS CARGADOS");
						mv.addObject("res", res);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;  
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaCargaAsignaExp.json?
				@RequestMapping(value="/altaCargaAsignaExp", method = RequestMethod.GET)
				public ModelAndView altaCargaAsignaExp(HttpServletRequest request, HttpServletResponse response){
					try{
						
						boolean  respuesta = asignaExpBI.CargaAsig();

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta   carga Asignaciones");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				// http://10.51.219.179:8080/checklist/catalogosService/cargaHallazgoXbita.json?bitGral=<?>
				@RequestMapping(value = "/cargaHallazgoXbita", method = RequestMethod.GET)
				public ModelAndView cargaHallazgoXbita(HttpServletRequest request, HttpServletResponse response)
				{
					try {
						
						int bitGral =Integer.parseInt(request.getParameter("bitGral"));

						boolean res = hallazgosExpBI.cargaHallazgosxBita(bitGral);
						
						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "HALLAZGOS CARGADOS");
						mv.addObject("res", res);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;  
					}
				}
				
				//http://10.51.219.179:8080/checklist/catalogosService/altaHandbook.json?descripcion=TEST6&ruta=ALGO&activo=1&padre=1&tipo=TEST6&fecha=20190930 11:33:00
				@RequestMapping(value="/altaHandbook", method = RequestMethod.GET)
				public ModelAndView altaHandbook(HttpServletRequest request, HttpServletResponse response){
					try{
						String descripcion=request.getParameter("descripcion");
						String ruta=request.getParameter("ruta");
						String activo=request.getParameter("activo");
						String padre=request.getParameter("padre");
						String tipo=request.getParameter("tipo");
						String fecha=request.getParameter("fecha");
			

						AdmHandbookDTO admHandbook = new AdmHandbookDTO();
						admHandbook.setDescripcion(descripcion);
						admHandbook.setRuta(ruta);
						admHandbook.setActivo(activo);
						admHandbook.setPadre(padre); 
						admHandbook.setTipo(tipo);
						admHandbook.setFecha(fecha);
	
						
						
						boolean respuesta = admHandbookBI.insertaHandbook(admHandbook);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta  HandBook");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				// http://10.51.219.179:8080/checklist/catalogosService/altaHallazgoHistorial.json?idHallazgo=<?>
				@RequestMapping(value = "/altaHallazgoHistorial", method = RequestMethod.GET)
				public ModelAndView altaHallazgoHistorial(HttpServletRequest request, HttpServletResponse response)
				{
					try {
						
						int idHallazgo =Integer.parseInt(request.getParameter("idHallazgo"));

						boolean res = hallazgosExpBI.insertaHallazgosHist(idHallazgo);
						
						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "HALLAZGOS Historico cargado: "+idHallazgo);
						mv.addObject("res", res);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;  
					}
				}
				
				//http://10.51.219.179:8080/checklist/catalogosService/altaProtocolo.json?descripcion=<?<&observaciones=<?>&status=<?>&statusRetro=<?>
				@RequestMapping(value="/altaProtocolo", method = RequestMethod.GET)
				public ModelAndView altaProtocolo(HttpServletRequest request, HttpServletResponse response){
					try{
						String descripcion=request.getParameter("descripcion");
						String observaciones=request.getParameter("observaciones");
						String status=request.getParameter("status");
						int statusRetro=Integer.parseInt(request.getParameter("statusRetro"));
						
						
						ProtocoloDTO protocolo = new ProtocoloDTO();
						protocolo.setDescripcion(descripcion);
						protocolo.setObservaciones(observaciones);
						protocolo.setStatus(Integer.parseInt(status));
						protocolo.setStatusRetro(statusRetro);
			
						int respuesta = protocoloBI.insertaProtocolo(protocolo);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta  Protocolo");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				// http://10.53.33.83/checklist/catalogosService/altaNegoExp.json?idNego=&Nombcorto=&obs=&descripcion=
				@RequestMapping(value = "/altaNegoExp", method = RequestMethod.GET)
				public ModelAndView altaNegoExp(HttpServletRequest request, HttpServletResponse response)
				{
					try {
						
						int idNego =Integer.parseInt(request.getParameter("idNego"));
						String Nombcorto =request.getParameter("Nombcorto");
						String obs =request.getParameter("obs");
						String descripcion =request.getParameter("descripcion");
						
						ZonaNegoExpDTO zn=new ZonaNegoExpDTO();
						zn.setIdNegocio(idNego);
						zn.setNegocio(Nombcorto);
						zn.setObs(obs);
						zn.setDescNego(descripcion);		

						int res = zonaNegoBI.insertaNego(zn);
						
						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", " Negocio Guardado: "+idNego);
						mv.addObject("res", res);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;  
					}
				}
				// http://10.53.33.83/checklist/catalogosService/altaZonaExp.json?Nombcorto=&obs=&descripcion=&aux
				@RequestMapping(value = "/altaZonaExp", method = RequestMethod.GET)
				public ModelAndView altaZonaExp(HttpServletRequest request, HttpServletResponse response)
				{
					try {
						
						String Nombcorto =request.getParameter("Nombcorto");
						String obs =request.getParameter("obs");
						String descripcion =request.getParameter("descripcion");
						String aux =request.getParameter("aux");
						
						ZonaNegoExpDTO zn=new ZonaNegoExpDTO();
					
						zn.setZona(Nombcorto);
						zn.setObs(obs);
						zn.setDescZona(descripcion);
						zn.setAux(aux);
						
						int res = zonaNegoBI.insertaZona(zn);
						
						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", " Zona Guardada: ");
						mv.addObject("res", res);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;  
					}
				}
				// http://10.53.33.83/checklist/catalogosService/altaZonaNegoExp.json?idZona=&idNego=
				@RequestMapping(value = "/altaZonaNegoExp", method = RequestMethod.GET)
				public ModelAndView altaZonaNegoExp(HttpServletRequest request, HttpServletResponse response)
				{
					try {
						
						int idNego =Integer.parseInt(request.getParameter("idNego"));
						int idZona =Integer.parseInt(request.getParameter("idZona"));    
						ZonaNegoExpDTO zn=new ZonaNegoExpDTO();
					
						zn.setIdNegocio(idNego);
						zn.setIdZona(idZona);
						int res = zonaNegoBI.insertaRela(zn);
						
						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", " Relacion Zona Negocio Correcta: ");
						mv.addObject("res", res);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;  
					}
				}
				
				// http://10.53.33.83/checklist/catalogosService/altaAdicional.json?idbita=
				@RequestMapping(value = "/altaAdicional", method = RequestMethod.GET)
				public ModelAndView altaAdicional(HttpServletRequest request, HttpServletResponse response)
				{
					try {
						
						int idbita =Integer.parseInt(request.getParameter("idbita"));
						  
						AdicionalesDTO ad=new AdicionalesDTO();
					
						ad.setBitGral(idbita);
						int res = adicionalesBI.inserta(ad);
						
						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", " Alta adicional : ");
						mv.addObject("res", res);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;  
					}
				}
				
				//http://10.51.219.179:8080/checklist/catalogosService/altaGerente.json?idGerente=331952&idUsuario=73433&Zona=T4&Region=T4&idPerfil=1&nomGerente=CARLOS&nomUsuario=VANNIA
				@RequestMapping(value = "/altaGerente", method = RequestMethod.GET)
				public @ResponseBody ModelAndView altaGerente(HttpServletRequest request, HttpServletResponse response) {
							try {
								int idGerente = Integer.parseInt(request.getParameter("idGerente"));
								String  nomGerente = request.getParameter("nomGerente");
								int  idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
								String nomUsuario = request.getParameter("nomUsuario");
								String Zona = request.getParameter("Zona");
								String Region = request.getParameter("Region");
								int idPerfil = Integer.parseInt(request.getParameter("idPerfil"));
								
								AltaGerenteDTO altaGerente = new AltaGerenteDTO();
								altaGerente.setIdGerente(idGerente);
								altaGerente.setNomGerente(nomGerente);
								altaGerente.setIdUsuario(idUsuario);
								altaGerente.setNomUsuario(nomUsuario);
								altaGerente.setZona(Zona);
								altaGerente.setRegion(Region);
								altaGerente.setIdPerfil(idPerfil);
								boolean respuesta=altaGerenteBI.insertaGerente(altaGerente);
								ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
								mv.addObject("res", respuesta);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;
					
							}
							
				}
				

				// http://localhost/checklist/catalogosService/altaHallazgoTransf.json?usuario=&ceco=&version=&idProyecto=
				@RequestMapping(value="/altaHallazgoTransf", method = RequestMethod.GET)
				public ModelAndView altaHallazgoTransf(HttpServletRequest request, HttpServletResponse response){
					try{
						
						
						int usuario= Integer.parseInt(request.getParameter("usuario"));
						String ceco= request.getParameter("ceco");
						int version= Integer.parseInt(request.getParameter("version"));
						int idProyecto= Integer.parseInt(request.getParameter("idProyecto"));

						HallazgosTransfDTO h = new HallazgosTransfDTO();
				
						h.setCeco(ceco);
						h.setUsuario(usuario);
						h.setIdVersion(version);
						h.setIdProyecto(idProyecto);
				
						int res = hallazgosTransfBI.inserta(h);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Registro Insertado");
						mv.addObject("res", res);
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				// http://10.51.219.179:8080/checklist/catalogosService/cargaHallazgoXbitaTransf.json?bitGral=<?>
				@RequestMapping(value = "/cargaHallazgoXbitaTransf", method = RequestMethod.GET)
				public ModelAndView cargaHallazgoXbitaTransf(HttpServletRequest request, HttpServletResponse response)
				{
					try {
						
						int bitGral =Integer.parseInt(request.getParameter("bitGral"));
						int cargaIni =Integer.parseInt(request.getParameter("cargaIni"));

						boolean res = hallazgosTransfBI.cargaHallazgosxBita(bitGral,cargaIni);
						
						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "HALLAZGOS transf CARGADOS");
						mv.addObject("res", res);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;  
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaTransf.json?bitacora=<?>
				@RequestMapping(value="/altaTransf", method = RequestMethod.GET)
				public ModelAndView altaTransf(HttpServletRequest request, HttpServletResponse response){
					try{
						
						int bitacora=Integer.parseInt(request.getParameter("bitacora"));

						TransformacionDTO so = new TransformacionDTO();
					so.setBitacora(bitacora);
						
						
						int respuesta = transformacionBI.inserta(so);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta  soft con bitacora "+bitacora+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				//YYYYMMDD HH24:MI:SS:FF3
				//http://localhost:8080/checklist/catalogosService/altaTransfMan.json?ceco=<?>&idUsuario=<?>&bitacora=<?>&recorrido=<?>&recorrido=<?>&fechaini=<?>&fechafin=<?>&aux=<?>
				@RequestMapping(value="/altaTransfMan", method = RequestMethod.GET)
				public ModelAndView altaTransfMan(HttpServletRequest request, HttpServletResponse response){
					try{
						int ceco=Integer.parseInt(request.getParameter("ceco"));
						int idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
						int bitacora=Integer.parseInt(request.getParameter("bitacora"));
						int recorrido=Integer.parseInt(request.getParameter("recorrido"));
						String fechaini=request.getParameter("fechaini");
						String fechafin=request.getParameter("fechafin");
						String aux=request.getParameter("aux");

						TransformacionDTO so = new TransformacionDTO();
						
						so.setCeco(ceco);
						so.setIdUsuario(idUsuario);
						so.setBitacora(bitacora);
						so.setRecorrido(recorrido);
						so.setFechaini(fechaini);
						so.setFechafin(fechafin);
						so.setAux(aux);
						
						int respuesta = transformacionBI.insertaMan(so);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta  soft con ceco: "+ceco+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				//http://localhost:8080/checklist/catalogosService/altaFaseTransf.json?idProyecto=<?>&fase=<?>&version=<?>&obs=<?>
				@RequestMapping(value="/altaFaseTransf", method = RequestMethod.GET)
				public ModelAndView altaFaseTransf(HttpServletRequest request, HttpServletResponse response){
					try{
						int idProyecto=Integer.parseInt(request.getParameter("idProyecto"));
						int fase=Integer.parseInt(request.getParameter("fase"));
						int version=Integer.parseInt(request.getParameter("version"));
						String obs=request.getParameter("obs");

						FaseTransfDTO ver = new FaseTransfDTO();
						
						ver.setProyecto(idProyecto);
						ver.setFase(fase);
						ver.setVersion(version);
						ver.setObs(obs);

						int respuesta = faseTransfBI.inserta(ver);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta   fase: "+fase+" con proyecto: "+idProyecto+" ---> ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				// http://10.51.219.179:8080/checklist/catalogosService/altaInforme.json?ceco=480100&usuario=82144&fecha=20200213&ruta=Ruta que almacenara Informe&lider=Nombre del Lider&firmaLider=Firma del Lider&numLider=331952&firmaFrq=Ruta que almacenara la firma de FRQ&retroAlimentacion=0&idProtocolo=30
				@RequestMapping(value="/altaInforme", method = RequestMethod.GET)
				public ModelAndView altaInforme(HttpServletRequest request, HttpServletResponse response){
					try{
						String ceco=request.getParameter("ceco");
						String usuario=request.getParameter("usuario");
						String fecha=request.getParameter("fecha");
						String ruta=request.getParameter("ruta");
						String lider = request.getParameter("lider");
						String firmaLider = request.getParameter("firmaLider");
						String numLider = request.getParameter("numLider");
						String firmaFrq = request.getParameter("firmaFrq");
						String retroAlimentacion = request.getParameter("retroAlimentacion");
						String idProtocolo = request.getParameter("idProtocolo");
				
							AdmInformProtocolosDTO admInformProtocolos = new AdmInformProtocolosDTO();
							admInformProtocolos.setCeco(ceco);
							admInformProtocolos.setUsuario(usuario);
							admInformProtocolos.setFecha(fecha);
							admInformProtocolos.setRuta(ruta);
							admInformProtocolos.setLider(lider);
							admInformProtocolos.setFirmaLider(firmaLider);
							admInformProtocolos.setNumLider(numLider);
							admInformProtocolos.setFirmaFrq(firmaFrq);
							admInformProtocolos.setRetroAlimentacion(retroAlimentacion);
							admInformProtocolos.setIdProtocolo(idProtocolo);

							boolean respuesta = admInformProtocolosBI.insertaInformProtocolos(admInformProtocolos);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "RUTA INFORME");
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
				
				//http://localhost:8080/checklist/catalogosService/altaAsignaTransf.json?ceco=<?>&idProyecto=<?>&usuario=<?>&usuario_asig=<?>&obs=<?>&idAgrupa=&edoCargaHalla=&bandFolio=
				@RequestMapping(value="/altaAsignaTransf", method = RequestMethod.GET)
				public ModelAndView altaAsignaTransf(HttpServletRequest request, HttpServletResponse response){
					try{
						int ceco=Integer.parseInt(request.getParameter("ceco"));
						int idProyecto=Integer.parseInt(request.getParameter("idProyecto"));
						int usuario=Integer.parseInt(request.getParameter("usuario"));
						int usuario_asig=Integer.parseInt(request.getParameter("usuario_asig"));
						String obs=request.getParameter("obs");
						int idAgrupa=Integer.parseInt(request.getParameter("idAgrupa"));
						int edoCargaHalla=Integer.parseInt(request.getParameter("edoCargaHalla"));
						int bandFolio=Integer.parseInt(request.getParameter("bandFolio"));


						AsignaTransfDTO asig = new AsignaTransfDTO();
						
						asig.setCeco(ceco);
						asig.setProyecto(idProyecto);
						asig.setUsuario_asig(usuario_asig);
						asig.setUsuario(usuario);
						asig.setObs(obs);
						asig.setAgrupador(idAgrupa);
						asig.setEdoCargaHallazgo(edoCargaHalla);
						asig.setBanderaFolio(bandFolio);
						int respuesta = asignaTransfBI.inserta(asig);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta   con ceco: "+ceco+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaProyectoTransf.json?nombre=<?>&obs=<?>&idTipoProyecto=&edoCargaIni=&edoMaxHalla=
				@RequestMapping(value="/altaProyectoTransf", method = RequestMethod.GET)
				public ModelAndView altaProyectoTransf(HttpServletRequest request, HttpServletResponse response){
					try{
						
						String nombre=request.getParameter("nombre");
						String obs=request.getParameter("obs");
						int idTipoProyecto=Integer.parseInt(request.getParameter("idTipoProyecto"));
						int edoCargaIni=0;
						edoCargaIni=Integer.parseInt(request.getParameter("edoCargaIni"));
						int edoMaxHalla=Integer.parseInt(request.getParameter("edoMaxHalla"));
						
						ProyectFaseTransfDTO pro = new ProyectFaseTransfDTO();
						pro.setNomProy(nombre);
						pro.setObsProy(obs);
						pro.setIdTipoProyecto(idTipoProyecto);
						pro.setEdoCargaIni(edoCargaIni);
						pro.setHallazgoEdoMax(edoMaxHalla);
						int respuesta = proyectFaseTransfBI.insertaProyec(pro);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta   con proyecto: "+nombre+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				
				//http://localhost:8080/checklist/catalogosService/altaAgrupaFase.json?idAgrupa=<?>&fase=<?>&ordenFase=<?>&obs=<?>&aux=
				@RequestMapping(value="/altaAgrupaFase", method = RequestMethod.GET)
				public ModelAndView altaAgrupaFase(HttpServletRequest request, HttpServletResponse response){
					try{
						int idAgrupa=Integer.parseInt(request.getParameter("idAgrupa"));
						int fase=Integer.parseInt(request.getParameter("fase"));
						int ordenFase=Integer.parseInt(request.getParameter("ordenFase"));
						String obs=request.getParameter("obs");
						String aux=request.getParameter("aux");

						FaseTransfDTO ver = new FaseTransfDTO();
						
						ver.setIdAgrupa(idAgrupa);
						ver.setFase(fase);
						ver.setOrdenFase(ordenFase);
						ver.setObs(obs);
						ver.setAux(aux);
						
			
						int respuesta = faseTransfBI.insertaAgrupa(ver);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta   fase: "+fase+" con agrupador: "+idAgrupa+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				// http://localhost:8080/checklist/catalogosService/altaPerfilUsuarioNvo.json?idUsuario=<?>&idPerfil=<?>&tipoProyecto=<?>&bandNvoEsq=<?>
				@RequestMapping(value = "/altaPerfilUsuarioNvo", method = RequestMethod.GET)
				public ModelAndView altaPerfilUsuarioNvo(HttpServletRequest request, HttpServletResponse response) {
					try {

						String idUsuario = request.getParameter("idUsuario");
						String idPerfil = request.getParameter("idPerfil");
						String tipoProyecto=null;
						tipoProyecto = request.getParameter("tipoProyecto");
						String bandNvoEsq=null;
						bandNvoEsq= request.getParameter("bandNvoEsq");

						PerfilUsuarioDTO perfilUsuarioDTO = new PerfilUsuarioDTO();
						perfilUsuarioDTO.setIdPerfil(Integer.parseInt(idPerfil));
						perfilUsuarioDTO.setIdUsuario(Integer.parseInt(idUsuario));
						perfilUsuarioDTO.setIdActor(tipoProyecto);
						perfilUsuarioDTO.setBandNvoEsq(bandNvoEsq);

						boolean res = perfilusuariobi.insertaPerfilUsuarioNvo(perfilUsuarioDTO);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "PERFIL ASIGNADO : ");
						mv.addObject("res", res);

						return mv;

					} catch (Exception e) {
						//logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaFirmaCheckAgrupa.json?bitacora=<?>&ceco=<?>&idUsuario=<?>&puesto=<?>&responsable=<?>&correo=<?>&nombre=<?>&ruta=<?>&idAgrupa=
				@RequestMapping(value="/altaFirmaCheckAgrupa", method = RequestMethod.GET)
				public ModelAndView altaFirmaCheckAgrupa(HttpServletRequest request, HttpServletResponse response){
					try{
						String bitacora=request.getParameter("bitacora");
						String ceco=request.getParameter("ceco");
						String idUsuario=request.getParameter("idUsuario");
						String puesto=request.getParameter("puesto");
						String responsable=request.getParameter("responsable");
						String correo=request.getParameter("correo");
						String nombre=request.getParameter("nombre");
						String ruta=request.getParameter("ruta");
						int idAgrupa=0;
						if (request.getParameter("idAgrupa") != null && request.getParameter("idAgrupa").length() <= 0) {
							idAgrupa = 0;
						} else {
							idAgrupa = Integer.parseInt(request.getParameter("idProyecto"));
						}
						
						FirmaCheckDTO firm = new FirmaCheckDTO();
						
						firm.setBitacora(bitacora);
						firm.setCeco(ceco);
						firm.setIdUsuario(idUsuario);
						firm.setPuesto(puesto);
						firm.setResponsable(responsable);
						firm.setCorreo(correo);
						firm.setNombre(nombre);
						firm.setRuta(ruta);
						firm.setIdAgrupa(idAgrupa);
						int respuesta = firmaCheckBI.insertaN(firm);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de Firma de checklist  del ceco"+ceco);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
			
				//http://localhost:8080/checklist/catalogosService/altaFirmaCatalogo.json?nombreFirma=&tipoProy&aux=
				@RequestMapping(value="/altaFirmaCatalogo", method = RequestMethod.GET)
				public ModelAndView altaFirmaCatalogo(HttpServletRequest request, HttpServletResponse response){
					try{
						String nombreFirma=request.getParameter("nombreFirma");
						int tipoProy=Integer.parseInt(request.getParameter("tipoProy"));
						String aux=request.getParameter("aux");
						
						AdmFirmasCatDTO firm = new AdmFirmasCatDTO();
						firm.setNombreFirmaCatalogo(nombreFirma);
						firm.setTipoProyecto(tipoProy);
						firm.setAux2(aux);
						
						int respuesta = firmasCatalogoBI.insertaFirmaCat(firm);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de Firma :  "+nombreFirma);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaFirmaAgrupa.json?idFirmaCatalogo=&cargo=&obligatorio=&ordenFirma=&aux=
				@RequestMapping(value="/altaFirmaAgrupa", method = RequestMethod.GET)
				public ModelAndView altaFirmaAgrupa(HttpServletRequest request, HttpServletResponse response){
					try{
						int idFirmaCatalogo=Integer.parseInt(request.getParameter("idFirmaCatalogo"));
						String cargo=request.getParameter("cargo");
						int obligatorio=Integer.parseInt(request.getParameter("obligatorio"));
						int ordenFirma=Integer.parseInt(request.getParameter("ordenFirma"));
						String aux=request.getParameter("aux");
						
						AdmFirmasCatDTO firm = new AdmFirmasCatDTO();
						firm.setIdAgrupaFirma(idFirmaCatalogo);
						firm.setCargo(cargo);
						firm.setObligatorio(obligatorio);
						firm.setOrden(ordenFirma);
						firm.setAux2(aux);
						
						int respuesta = firmasCatalogoBI.insertaFirmaAgrupa(firm);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de Firma con agrupador :  "+idFirmaCatalogo);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaFragmentCatalogo.json?nombreFragment=&parametro=
				@RequestMapping(value="/altaFragmentCatalogo", method = RequestMethod.GET)
				public ModelAndView altaFragmentCatalogo(HttpServletRequest request, HttpServletResponse response){
					try{
						
						String nombreFragment=request.getParameter("nombreFragment");
						String parametro=request.getParameter("parametro");
						
						FragmentMenuDTO fra = new FragmentMenuDTO();
						fra.setNombreFragment(nombreFragment);
						fra.setParametro(parametro);
				
						int respuesta = fragmentMenuBI.insertaFragment(fra);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de Fragment :  "+nombreFragment);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaMenuOpcionesP.json?titulo=&idPerfil=&ordenMenu=&aux=
				@RequestMapping(value="/altaMenuOpcionesP", method = RequestMethod.GET)
				public ModelAndView altaMenuOpcionesP(HttpServletRequest request, HttpServletResponse response){
					try{
						
						String titulo=request.getParameter("titulo");
						int idPerfil=Integer.parseInt(request.getParameter("idPerfil"));
						int ordenMenu=Integer.parseInt(request.getParameter("ordenMenu"));
						String aux=request.getParameter("aux");
						
						FragmentMenuDTO men = new FragmentMenuDTO();
						men.setTituloMenu(titulo);
						men.setIdPerfil(idPerfil);
						men.setOrdenMenu(ordenMenu);
						men.setAux(aux);
				
						int respuesta = fragmentMenuBI.insertaMenu(men);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de menu con nombre :  "+titulo);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaConfigProyecto.json?idConfMenu=&idFragment=&ordenFrag=&idAgrupaFirma=&parametro=&aux
				@RequestMapping(value="/altaConfigProyecto", method = RequestMethod.GET)
				public ModelAndView altaConfigProyecto(HttpServletRequest request, HttpServletResponse response){
					try{
						
						int idConfMenu=Integer.parseInt(request.getParameter("idConfMenu"));
						int idFragment=Integer.parseInt(request.getParameter("idFragment"));
						int ordenFrag=Integer.parseInt(request.getParameter("ordenFrag"));
						int idAgrupaFirma=Integer.parseInt(request.getParameter("idAgrupaFirma"));
						String parametro=request.getParameter("parametro");
						String aux=request.getParameter("aux");
						
						FragmentMenuDTO conf = new FragmentMenuDTO();
						conf.setIdConfigMenu(idConfMenu);
						conf.setIdFragment(idFragment);
						conf.setOrdenFragment(ordenFrag);
						conf.setIdAgrupaFirma(idAgrupaFirma);
						conf.setParametro(parametro);
						conf.setAux(aux);
				
						int respuesta = fragmentMenuBI.insertaConf(conf);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de config con idconfiguracionMenu :  "+idConfMenu);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaSoftProyecto.json?ceco=&idFase=&idProyecto=&bitacora=&idUsuario=
				@RequestMapping(value="/altaSoftProyecto", method = RequestMethod.GET)
				public ModelAndView altaSoftProyecto(HttpServletRequest request, HttpServletResponse response){
					try{
						
						String ceco=request.getParameter("ceco");
						int idFase=Integer.parseInt(request.getParameter("idFase"));
						int idProyecto=Integer.parseInt(request.getParameter("idProyecto"));
						int bitacora=Integer.parseInt(request.getParameter("bitacora"));
						int idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
						
						SoftNvoDTO soft = new SoftNvoDTO();
						soft.setCeco(ceco);
						soft.setIdFase(idFase);
						soft.setIdProyecto(idProyecto);
						soft.setBitacora(bitacora);
						soft.setIdUsuario(idUsuario);
						
						int respuesta = softNvoBI.insertaSoftN(soft);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de proyecto: "+idProyecto+"  con ceco :  "+ceco);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
//http://localhost:8080/checklist/catalogosService/altaCalificacionesActa.json?ceco=&idFase=&idProyecto=&idSoft=&clasificacion=&totalSi=&totalNo=&totalNA=&totalRevisados=&totalItems=&totImperdonables=&porcPondSi=&porcPondNo=&porcPondNA=&pondMaxReal=&pondObtCal=&calificacionCalc=
				@RequestMapping(value="/altaCalificacionesActa", method = RequestMethod.GET)
				public ModelAndView altaCalificacionesActa(HttpServletRequest request, HttpServletResponse response){
					try{
						String ceco=(request.getParameter("ceco"));
						int idFase=Integer.parseInt(request.getParameter("idFase"));
						int idProyecto=Integer.parseInt(request.getParameter("idProyecto"));
						int idSoft=Integer.parseInt(request.getParameter("idSoft"));
						String clasificacion=request.getParameter("clasificacion");
						int totalSi=Integer.parseInt(request.getParameter("totalSi"));
						int totalNo=Integer.parseInt(request.getParameter("totalNo"));
						int totalNA=Integer.parseInt(request.getParameter("totalNA"));
						int totalRevisados=Integer.parseInt(request.getParameter("totalRevisados"));
						int totalItems=Integer.parseInt(request.getParameter("totalItems"));
						int totImperdonables=Integer.parseInt(request.getParameter("totImperdonables"));
						double porcPondSi=Double.parseDouble(request.getParameter("porcPondSi"));
						double porcPondNo=Double.parseDouble(request.getParameter("porcPondNo"));
						double porcPondNA=Double.parseDouble(request.getParameter("porcPondNA"));
						double pondMaxReal=Double.parseDouble(request.getParameter("pondMaxReal"));
						double pondObtCal=Double.parseDouble(request.getParameter("pondObtCal"));
						double calificacionCalc=Double.parseDouble(request.getParameter("calificacionCalc"));
						
						SoftNvoDTO soft = new SoftNvoDTO();
						soft.setCeco(ceco);
						soft.setIdFase(idFase);
						soft.setIdProyecto(idProyecto);
						soft.setIdSoft(idSoft);
						soft.setClasificacion(clasificacion);
						soft.setItemSi(totalSi);
						soft.setItemNo(totalNo);
						soft.setItemNa(totalNA);
						soft.setItemrevisados(totalRevisados);
						soft.setItemTotales(totalItems);
						soft.setItemImperd(totImperdonables);
						soft.setPorcentajeReSi(porcPondSi);
						soft.setPorcentajeReNo(porcPondNo);
						soft.setPorcentajeToNA(porcPondNA);
						soft.setPonderacionMaximaReal(pondMaxReal);
						soft.setPonderacionObtenidaCalculada(pondObtCal);
						soft.setCalificacionCalculada(calificacionCalc);
						
						int respuesta = softNvoBI.insertaCalculosActa(soft);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de calculo  proyecto:");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaActorHallazgo.json?nombre=&obs=&aux=&status=&idPerfil=&idConfig=
				@RequestMapping(value="/altaActorHallazgo", method = RequestMethod.GET)
				public ModelAndView altaActorHallazgo(HttpServletRequest request, HttpServletResponse response){
					try{
						
						String nombre=request.getParameter("nombre");
						String obs=request.getParameter("obs");
						String aux=request.getParameter("aux");
						int status=Integer.parseInt(request.getParameter("status"));
						int idPerfil=Integer.parseInt(request.getParameter("idPerfil"));
						int idConfig=Integer.parseInt(request.getParameter("idConfig"));
							
						ActorEdoHallaDTO actor = new ActorEdoHallaDTO();
						actor.setNombreActor(nombre);
						actor.setObservacion(obs);
						actor.setAux(aux);
						actor.setStatus(status);		
						actor.setIdPerfil(idPerfil);
						actor.setIdConfiguracion(idConfig);
						
						int respuesta = actorEdoHallaBI.insertaActorHalla(actor);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de actot: "+nombre+"  con status :  "+status);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaGpoEdoHallazgo.json?nombre=&idConfig=&obs=&aux=&status=&color=
				@RequestMapping(value="/altaGpoEdoHallazgo", method = RequestMethod.GET)
				public ModelAndView altaGpoEdoHallazgo(HttpServletRequest request, HttpServletResponse response){
					try{
						
						String nombre=request.getParameter("nombre");
						int status=Integer.parseInt(request.getParameter("status"));
						int idConfig=Integer.parseInt(request.getParameter("idConfig"));
						String obs=request.getParameter("obs");
						String aux=request.getParameter("aux");
						String color=request.getParameter("color");
						
						ActorEdoHallaDTO actor = new ActorEdoHallaDTO();
						actor.setNombStatus(nombre);
						actor.setStatus(status);
						actor.setIdConfiguracion(idConfig);
						actor.setObservacion(obs);
						actor.setAux(aux);					
					   actor.setColor(color);
						int respuesta = actorEdoHallaBI.insertaConfEdoHalla(actor);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de idConfig: "+idConfig+"  con status :  "+status);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				//http://localhost:8080/checklist/catalogosService/altaConfigActor.json?idConfigActor=&statusAccion=&accion=&bandAtencion=&idConfig=&aux=&obs=&statusActuacion=
				@RequestMapping(value="/altaConfigActor", method = RequestMethod.GET)
				public ModelAndView altaConfigActor(HttpServletRequest request, HttpServletResponse response){
					try{
						
						int idConfigActor=Integer.parseInt(request.getParameter("idConfigActor"));
						int statusAccion=Integer.parseInt(request.getParameter("statusAccion"));
						int statusActuacion=Integer.parseInt(request.getParameter("statusActuacion"));
						String accion=request.getParameter("accion");
						int bandAtencion=Integer.parseInt(request.getParameter("bandAtencion"));
						int idConfig=Integer.parseInt(request.getParameter("idConfig"));
						String obs=request.getParameter("obs");
						String aux=request.getParameter("aux");
							
						ConfiguraActorDTO actor = new ConfiguraActorDTO();
						actor.setIdConfigActor(idConfigActor);
						actor.setStatusAccion(statusAccion);
						actor.setStatusActuacion(statusActuacion);
						actor.setAccion(accion);
						actor.setBanderaAtencion(bandAtencion);
						actor.setIdConfig(idConfig);
						actor.setObs(obs);
						actor.setAux(aux);			
	                     
						int respuesta = configuraActorBI.insertaConfActor(actor);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de idConfig: "+idConfigActor+"  con statusAcccion :  "+statusAccion);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaMatrizHallazgo.json?tipoProyecto=&statusEnvio=&statusHallazgo=&perfil=&color=&nombreStatus=
				@RequestMapping(value="/altaMatrizHallazgo", method = RequestMethod.GET)
				public ModelAndView altaMatrizHallazgo(HttpServletRequest request, HttpServletResponse response){
					try{
						
                        String tipoProyecto=request.getParameter("tipoProyecto");
						int statusEnvio=Integer.parseInt(request.getParameter("statusEnvio"));
						int statusHallazgo=Integer.parseInt(request.getParameter("statusHallazgo"));
						int perfil=Integer.parseInt(request.getParameter("perfil"));
						String color=request.getParameter("color");
						String nombreStatus=request.getParameter("nombreStatus");
							
						HallazgosTransfDTO matriz = new HallazgosTransfDTO();
						matriz.setTipoProy(tipoProyecto);
						matriz.setStatusEnvio(statusEnvio);
						matriz.setStatusHallazgo(statusHallazgo);
						matriz.setIdPerfil(perfil);
						matriz.setColor(color);
						matriz.setNombreStatus(nombreStatus);
	                     
						int respuesta = hallagosMatrizBI.insertaHallazgoMatriz(matriz);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de tipoProyecto: "+tipoProyecto);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				// http://localhost:8080/checklist/catalogosService/cargaHallazgoXbitaTransfNvoFlujo.json?bitGral=<?>&ceco=&fase=&proyecto=
				@RequestMapping(value = "/cargaHallazgoXbitaTransfNvoFlujo", method = RequestMethod.GET)
				public ModelAndView cargaHallazgoXbitaTransfNvoFlujo(HttpServletRequest request, HttpServletResponse response)
				{
					try {
						
						int bitGral =Integer.parseInt(request.getParameter("bitGral"));
						int ceco =Integer.parseInt(request.getParameter("ceco"));
						int fase =Integer.parseInt(request.getParameter("fase"));
						int proyecto =Integer.parseInt(request.getParameter("proyecto"));

						boolean res = hallazgosTransfBI.cargaHallazgosxBitaNvo(bitGral,ceco,fase,proyecto);
						
						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "HALLAZGOS transf CARGADOS");
						mv.addObject("res", res);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;  
					}
				}
				//http://localhost:8080/checklist/catalogosService/altaSoftProyectoNuevoFlujo.json?ceco=&idFase=&idProyecto=&bitacora=&idUsuario=
				@RequestMapping(value="/altaSoftProyectoNuevoFlujo", method = RequestMethod.GET)
				public ModelAndView altaSoftProyectoNuevoFlujo(HttpServletRequest request, HttpServletResponse response){
					try{
						
						String ceco=request.getParameter("ceco");
						int idFase=Integer.parseInt(request.getParameter("idFase"));
						int idProyecto=Integer.parseInt(request.getParameter("idProyecto"));
						int bitacora=Integer.parseInt(request.getParameter("bitacora"));
						int idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
						
						SoftNvoDTO soft = new SoftNvoDTO();
						soft.setCeco(ceco);
						soft.setIdFase(idFase);
						soft.setIdProyecto(idProyecto);
						soft.setBitacora(bitacora);
						soft.setIdUsuario(idUsuario);
						
						int respuesta = softNvoBI.insertaSoftNuevoFlujo(soft);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de proyecto: "+idProyecto+"  con ceco :  "+ceco);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaCheckInAsistencia.json?idUsuario=&latitud=&longitud=&fecha=&ruta=&observaciones=&lugar=&tipoCheckIn=
				@RequestMapping(value="/altaCheckInAsistencia", method = RequestMethod.GET)
				public ModelAndView altaCheckInAsistencia(HttpServletRequest request, HttpServletResponse response){
					try{
						
						int idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
						double latitud=Double.parseDouble(request.getParameter("latitud"));
						double longitud=Double.parseDouble(request.getParameter("longitud"));
						String fecha=request.getParameter("fecha");
						String ruta=request.getParameter("ruta");
						String observaciones=request.getParameter("observaciones");
						String lugar=request.getParameter("lugar");
						//entrada 1 salida 2 0 N/A
						int tipoCheckIn=Integer.parseInt(request.getParameter("tipoCheckIn"));
						int tipoProyecto=Integer.parseInt(request.getParameter("tipoProyecto"));
					
						CheckInAsistenciaDTO asist = new CheckInAsistenciaDTO();
						asist.setIdUsuario(idUsuario);
						asist.setLatitud(latitud);
						asist.setLongitud(longitud);
						asist.setFecha(fecha);
						asist.setObservaciones(observaciones);
						asist.setRuta(ruta);
						asist.setLugar(lugar);
						asist.setTipoCheckIn(tipoCheckIn);
						asist.setIdTipoProyecto(tipoProyecto);
						
						int respuesta = checkInAsistenciaBI.insertaAsistencia(asist);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de usuario: "+idUsuario+"  con tipo checkInAsistencia :  (1 entrada 2 Salida)-->"+tipoCheckIn);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/altaAsignacionMtto.json?ceco=<?>&idProyecto=<?>&usuario=<?>&usuario_asig=<?>&obs=<?>&idAgrupa=&fechaProgramacion=
				@RequestMapping(value="/altaAsignacionMtto", method = RequestMethod.GET)
				public ModelAndView altaAsignacionMtto(HttpServletRequest request, HttpServletResponse response){
					try{
						int ceco=Integer.parseInt(request.getParameter("ceco"));
						int idProyecto=Integer.parseInt(request.getParameter("idProyecto"));
						//USUARIO QUE HACE RECCORIDO
						int usuario_asig=Integer.parseInt(request.getParameter("usuario_asig"));
						//USUARIO QUE HIZO LA ASIGNACION
						int usuario=Integer.parseInt(request.getParameter("usuario"));
						String obs=request.getParameter("obs");
						int idAgrupa=Integer.parseInt(request.getParameter("idAgrupa"));
						//AAAAMMDD
						String fechaProgramacion=request.getParameter("fechaProgramacion");
						//int bandFolio=Integer.parseInt(request.getParameter("bandFolio"));
						
					
						AsignaTransfDTO asig = new AsignaTransfDTO();
						
						asig.setCeco(ceco);
						asig.setProyecto(idProyecto);
						asig.setUsuario_asig(usuario_asig);
						asig.setUsuario(usuario);
						asig.setObs(obs);
						asig.setAgrupador(idAgrupa);
						asig.setFechaProgramacion(fechaProgramacion);;
						//asig.setBanderaFolio(bandFolio);
						int respuesta = asignaTransfBI.insertaAsignacionMtto(asig);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta   con ceco: "+ceco+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				//http://localhost:8080/checklist/catalogosService/altaSoftProyectoNuevoFlujoMtto.json?ceco=&idProyecto=&idProgramacion=&idUsuario=&bitacora=
				@RequestMapping(value="/altaSoftProyectoNuevoFlujoMtto", method = RequestMethod.GET)
				public ModelAndView altaSoftProyectoNuevoFlujoMtto(HttpServletRequest request, HttpServletResponse response){
					try{
						
						String ceco=request.getParameter("ceco");
						int idProyecto=Integer.parseInt(request.getParameter("idProyecto"));
						int idProgramacion=Integer.parseInt(request.getParameter("idProgramacion"));
						int idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
						int bitacora=Integer.parseInt(request.getParameter("bitacora"));
						
						SoftNvoDTO soft = new SoftNvoDTO();
						soft.setCeco(ceco);
						soft.setIdProyecto(idProyecto);
						soft.setIdProgramacion(idProgramacion);
						soft.setIdUsuario(idUsuario);
						soft.setBitacora(bitacora);
						
						int respuesta = softNvoBI.insertaSoftNuevoFlujoMtto(soft);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de proyecto: "+idProyecto+"  con ceco :  "+ceco);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				
				//http://localhost:8080/checklist/catalogosService/altaDatosInforme.json?idCeco=&idUsuario=&idProtocolo=&imperdonables=&calificacion=&totalRespuestas=&fecha=DDMMYY HH:MM:SS
				@RequestMapping(value="/altaDatosInforme", method = RequestMethod.GET)
				public ModelAndView altaDatosInforme(HttpServletRequest request, HttpServletResponse response){
					try{
	
						
						String fecha=request.getParameter("fecha");
						int idCeco=Integer.parseInt(request.getParameter("idCeco"));
						int idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
						int idProtocolo=Integer.parseInt(request.getParameter("idProtocolo"));
						int imperdonables=Integer.parseInt(request.getParameter("imperdonables"));
						double calificacion=Double.parseDouble(request.getParameter("calificacion"));
						int totalRespuestas=Integer.parseInt(request.getParameter("totalRespuestas"));
						
						
						DatosInformeDTO informe = new DatosInformeDTO();
						informe.setIdCeco(idCeco);
						informe.setIdUsuario(idUsuario);
						informe.setIdProtocolo(idProtocolo);
						informe.setImperdonables(imperdonables);
						informe.setTotalRespuestas(totalRespuestas);
						informe.setCalificacion(calificacion);
						informe.setFecha(fecha);
						
						int respuesta = datosInformeBI.insertaDatosInforme(informe);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Alta de ceco: "+idCeco+"  con usuario :  "+idUsuario);
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
			    
		        
}
