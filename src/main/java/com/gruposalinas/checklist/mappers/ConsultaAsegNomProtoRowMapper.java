package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ConsultaAseguradorDTO;

public class ConsultaAsegNomProtoRowMapper implements RowMapper<ConsultaAseguradorDTO> {

	public ConsultaAseguradorDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConsultaAseguradorDTO consultasAseguradorDTO = new ConsultaAseguradorDTO();
		consultasAseguradorDTO.setFechaTermino(rs.getString("FDTERMINO"));
		consultasAseguradorDTO.setIdCeco(rs.getInt("FCID_CECO"));
		consultasAseguradorDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		consultasAseguradorDTO.setNomCheckList(rs.getString("FCNOMBRE"));
		consultasAseguradorDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		return consultasAseguradorDTO;
		
		
	}

}
