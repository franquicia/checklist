package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.TipoArchivoDTO;

public interface TipoArchivoDAO {

	public int insertaTipoArchivo(TipoArchivoDTO bean) throws Exception;
	
	public boolean eliminaTipoArchivo(int idTipo) throws Exception;
	
	public boolean actualizaTipoArchivo(TipoArchivoDTO bean) throws Exception;
	
	public List<TipoArchivoDTO> obtieneTipoArchivo() throws Exception;
	
	public List<TipoArchivoDTO> obtieneTipoArchivo(int idTipo) throws Exception;
	
	
}
