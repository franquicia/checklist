package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AppPerfilDTO;
import com.gruposalinas.checklist.mappers.AppPerfilRowMapper;
import com.gruposalinas.checklist.mappers.AppRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AppDAOImpl extends DefaultDAO implements AppDAO {

    private static Logger logger = LogManager.getLogger(AppDAOImpl.class);

    private DefaultJdbcCall insertaApp;
    private DefaultJdbcCall actualizaApp;
    private DefaultJdbcCall eliminaApp;
    private DefaultJdbcCall buscaApp;
    private DefaultJdbcCall insertaAppPerfil;
    private DefaultJdbcCall actualizaAppPerfil;
    private DefaultJdbcCall eliminaAppPerfil;
    private DefaultJdbcCall buscaAppPerfil;

    public void init() {

        insertaApp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILAPP")
                .withProcedureName("SP_INS_APP");

        actualizaApp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILAPP")
                .withProcedureName("SP_ACT_APP");

        eliminaApp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILAPP")
                .withProcedureName("SP_DEL_APP");

        buscaApp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILAPP")
                .withProcedureName("SP_SEL_APP")
                .returningResultSet("RCL_APP", new AppRowMapper());

        insertaAppPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILAPP")
                .withProcedureName("SP_INS_PERFIL");

        actualizaAppPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILAPP")
                .withProcedureName("SP_ACT_PERFIL");

        eliminaAppPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILAPP")
                .withProcedureName("SP_DEL_PERFIL");

        buscaAppPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILAPP")
                .withProcedureName("SP_SEL_PERFIL")
                .returningResultSet("RCL_PERFIL", new AppPerfilRowMapper());

    }

    @Override
    public List<AppPerfilDTO> buscaApp(String idApp) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<AppPerfilDTO> listaApp = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_APP", idApp);

        out = buscaApp.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_SEL_APP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        listaApp = (List<AppPerfilDTO>) out.get("RCL_APP");

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return listaApp;
        }

        return null;
    }

    @Override
    public int insertaApp(AppPerfilDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int id = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_DESCRIPCION", bean.getDescripcion());

        out = insertaApp.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_INS_APP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        BigDecimal returnId = (BigDecimal) out.get("PA_IDAPP");
        id = returnId.intValue();
        //System.out.println(error);
        if (error == 0) {
            logger.info("Algo ocurrió al insertar");
        } else {
            return id;
        }

        return 0;
    }

    @Override
    public boolean actualizaApp(AppPerfilDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDAPP", bean.getIdApp())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_COMMIT", bean.getCommit());

        out = actualizaApp.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_ACT_APP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaApp(int idApp) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDAPP", idApp);

        out = eliminaApp.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_DEL_APP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public List<AppPerfilDTO> buscaAppPerfil(String idApp, String idUsuario) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<AppPerfilDTO> listaApp = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSU", idUsuario).addValue("PA_IDAPP", idApp);

        out = buscaAppPerfil.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_SEL_PERFIL}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        listaApp = (List<AppPerfilDTO>) out.get("RCL_PERFIL");

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return listaApp;
        }

        return null;
    }

    @Override
    public int insertaAppPerfil(AppPerfilDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int id = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_IDAPP", bean.getIdApp())
                .addValue("PA_IDUSU", bean.getIdUsuario());

        out = insertaAppPerfil.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_INS_APP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        BigDecimal returnId = (BigDecimal) out.get("PA_IDPERFILAPP");
        id = returnId.intValue();
        //System.out.println(error);
        if (error == 0) {
            logger.info("Algo ocurrió al insertar");
        } else {
            return id;
        }

        return 0;
    }

    @Override
    public boolean actualizaAppPerfil(AppPerfilDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPERFIL", bean.getIdAppPerfil())
                .addValue("PA_IDAPP", bean.getIdApp())
                .addValue("PA_IDUSU", bean.getIdUsuario())
                .addValue("PA_COMMIT", bean.getCommit());

        out = actualizaAppPerfil.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_ACT_PERFIL}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaAppPerfil(int idAppPerfil) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPERFIL", idAppPerfil);

        out = eliminaAppPerfil.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_DEL_PERFIL}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return true;
        }

        return false;
    }

}
