$body = $("body");
$(document).on({
    ajaxStart: function() {$body.addClass("loading");},
    ajaxStop: function() {$body.removeClass("loading");}    
});

var porcentajeL=0;
var validaCheck=0;
var intervalo=null;
var intervaloDespliegue=null;
var urlCache = "";
var filaHijo=0;


$(window).load(function() {
	//document.getElementById("tipoModal").className="modal3";
	intervalo = setInterval("reporte()",300000);
});

function reporte(){
		
	modal = 1;
	document.getElementById("tipoModal").className="modal3";
	location.href = "/checklist/reportes/reporteCliente.htm";
}

function myFunction() {
	$body.removeClass("loading1");
	location.reload();
}

function nobackbutton(){
	window.location.hash="no-back-button";
	window.location.hash="Again-No-back-button" //chrome
	window.onhashchange=function(){window.location.hash="";}
}

var arreglo = [];
var banG=0;

function muestraZonas(fila, idCeco, nomCeco, tam , nivel){

	
	//document.getElementById("tipoModal").className="modal3";
	//alert(fila + ' - ' + idCeco + ' - ' + nomCeco + ' - ' + tam + ' - ' + nivel);

	
	
	var ban=false;
	$.ajax({
		type : "GET",
		url : '/checklist/reportes/ajaxPorcentajeApertura.json?idCeco='+idCeco+'&nomCeco='+nomCeco,
		//url :'/checklist/reportes/ajaxRH.json?idCeco='+idCeco+'&nomCeco='+nomCeco,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async:true,
		delimiter : ",",
		success : function(json) {
			
			if(json==null){
				ban=true;
			}else{
				ban=false;	
				
				document.getElementById("ape"+nivel+fila).innerHTML = "";

				
				if(json.apertura!=null && json.apertura!="null"){
					if(json.apertura==100)
						$("#ape"+nivel+""+fila).append("<div class='radio verdeN'>"+json.apertura+"%</div>");
					else if(json.apertura>89 && json.apertura<100)
						$("#ape"+nivel+""+fila).append("<div class='radio amarilloN' style='color:black;'>"+json.apertura+"%</div>");
					else 
						$("#ape"+nivel+""+fila).append("<div class='radio rojo'>"+json.apertura+"%</div>");
				}					
				else{
					$("#ape"+nivel+""+fila).append("<div class='radio grisN'>-</div>");
				}
				
				if(fila == (tam-1)){
					//document.getElementById("tipoModal").className="modal1";
					banG=banG+1;
				}						
			}			
			if(fila == (tam-1)){
				if(ban){
					//alert('Sin Datos de Apertura');
					//document.getElementById("tipoModal").className="modal1";
					banG=banG+1;
				}
			}		
		},
		error : function() {
			//alert('Favor de actualizar la pagina');
		}
	});	

	
	var ban2=false;
	$.ajax({
		type : "GET",
		url : '/checklist/reportes/ajaxPorcentajeCierre.json?idCeco='+idCeco+'&nomCeco='+nomCeco,
		//url :'/checklist/reportes/ajaxRH.json?idCeco='+idCeco+'&nomCeco='+nomCeco,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async:true,
		delimiter : ",",
		success : function(json) {
			
			if(json==null){
				ban2=true;
			}else{
				ban2=false;	
				
				document.getElementById("cie"+nivel+fila).innerHTML = "";
				
				if(json.cierre!=null && json.cierre!="null"){
					if(json.cierre==100)
						$("#cie"+nivel+""+fila).append("<div class='radio verdeN'>"+json.cierre+"%</div>");
					else if(json.cierre>89 && json.cierre<100)
						$("#cie"+nivel+""+fila).append("<div class='radio amarilloN' style='color:black;'>"+json.cierre+"%</div>");
					else 
						$("#cie"+nivel+""+fila).append("<div class='radio rojo'>"+json.cierre+"%</div>");
				}					
				else{
					$("#cie"+nivel+""+fila).append("<div class='radio grisN'>-</div>");
				}
				
				if(fila == (tam-1)){
					//document.getElementById("tipoModal").className="modal1";
					banG=banG+1;
				}					
			}			
			
			if(fila == (tam-1)){
				if(ban2){
					//alert('Sin Datos de Cierre');
					//document.getElementById("tipoModal").className="modal1";
					banG=banG+1;
				}
			}		
		},
		error : function() {
			//alert('Favor de actualizar la pagina');
		}
	});	

	var ban3=false;
	$.ajax({
		type : "GET",
		url : '/checklist/reportes/ajaxBuscaChecklist.json?idCeco='+idCeco+'&idCheck='+156,
		//url :'/checklist/reportes/ajaxRH.json?idCeco='+idCeco+'&nomCeco='+nomCeco,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async:true,
		delimiter : ",",
		success : function(json) {
			
			if(json==null){
				ban3=true;
			}else{
				ban3=false;	
				
				document.getElementById("cum1"+nivel+fila).innerHTML = "";
				
	
				$("#cum1"+nivel+""+fila).append("<div class='radio rojo'>"+json.total+"%</div>");
		
				if(fila == (tam-1)){
					//document.getElementById("tipoModal").className="modal1";
					banG=banG+1;
				}
			}
			
			if(fila == (tam-1)){
				if(ban3){
					//alert('Sin Datos de Checklist');
					//document.getElementById("tipoModal").className="modal1";
					banG=banG+1;
				}
			}
		},
		error : function() {
			//alert('Favor de actualizar la pagina');
		}
	});	

	var ban4=false;
	$.ajax({
		type : "GET",
		url : '/checklist/reportes/ajaxBuscaChecklist.json?idCeco='+idCeco+'&idCheck='+99,
		//url :'/checklist/reportes/ajaxRH.json?idCeco='+idCeco+'&nomCeco='+nomCeco,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async:true,
		delimiter : ",",
		success : function(json) {
			
			if(json==null){
				ban4=true;
			}else{
				ban4=false;	
				
				document.getElementById("cum2"+nivel+fila).innerHTML = "";
				
	
				$("#cum2"+nivel+""+fila).append("<div class='radio rojo'>"+json.total+"%</div>");
		
				if(fila == (tam-1)){
					document.getElementById("tipoModal").className="modal1";
					banG=banG+1;
					
					if(banG==4){
						document.getElementById("tipoModal").className="modal1";
						banG=0;
					}
				}
			}
			
			if(fila == (tam-1)){
				if(ban4){
					//alert('Sin Datos de Checklist');
					//document.getElementById("tipoModal").className="modal1";
					banG=banG+1;
					
				}
			}
		},
		error : function() {
			//alert('Favor de actualizar la pagina');
		}
	});	

	
	
		
}

function metodoOrdenaObjetos(matriz){
	
	var flag = false;
	var l =1;
	var sumaAct;
	var sumaAnt;
	var act;
	var ant;
	
	while(l < matriz.length){

		sumaAnt=matriz[l-1].rh.plantilla - matriz[l-1].rh.rotacion  - matriz[l-1].rh.asesores  + matriz[l-1].rh.certificados + matriz[l-1].rh.tareas;
		sumaAct=matriz[l].rh.plantilla - matriz[l].rh.rotacion - matriz[l].rh.asesores + matriz[l].rh.certificados + matriz[l].rh.tareas;

		if(sumaAnt < sumaAct){
			ant=matriz[l-1];
			act=matriz[l];
			matriz[l - 1] = act;
			matriz[l] = ant;
			flag=true;
		}

		if (flag && (l + 1) == matriz.length) {
			l = 0;
			flag = false;
		}
		l++;
	}
	
	return matriz;
}

function despliega(id, fila){
	
	despliegaCheck(id, fila);
	modal = 2;
	filaHijo=fila;
	//LLAMAR A AJAX PARA OBTENER LOS DATOS	
}



function despliegaCheck(idCeco, fila){
	
	if(validaCheck!=idCeco){ 
		
		document.getElementById("tipoModal").className="modal3";
		
		arreglo = [];

		validaCheck=idCeco;
		document.getElementById("idLlenaZonas"+idCeco).innerHTML = "";

		$.ajax({
			type : "GET",
			url : '/checklist/reportes/ajaxBuscaCecosPA.json?idCeco='+idCeco,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {
				
				if (json == null) {
					//alert('Sin Datos');
					$("#idLlenaZonas"+idCeco).append("<span class='Gris1'>Sin Datos</span>");
					document.getElementById("tipoModal").className="modal1";
				}else{					

					for (var a = 0; a < Object.keys(json).length; a++) {
						$("#idLlenaZonas"+idCeco).append("<table class='bloquer subnivel'><tbody><tr id='"+json[a].idCeco+"' class='pointer'><td id='nom"+(fila+1)+(a)+"' class='nombreCheck'>"+json[a].descCeco+"</td>");
			
						$("#"+json[a].idCeco).append("<td id='ape"+(fila+1)+(a)+"'><div class='radio grisN'>-<div></td>");
						$("#"+json[a].idCeco).append("<td id='cie"+(fila+1)+(a)+"'><div class='radio grisN'>-<div></td>");
						$("#"+json[a].idCeco).append("<td id='cum1"+(fila+1)+(a)+"'><div class='radio grisN'>-<div></td>");
						$("#"+json[a].idCeco).append("<td id='cum2"+(fila+1)+(a)+"'><div class='radio grisN'>-<div></td>");
						$("#"+json[a].idCeco).append("<td id='tar"+(fila+1)+(a)+"'><div class='radio grisN'>-<div></td>");
					
						$("#idLlenaZonas"+idCeco).append("</tr><tbody></table>");
						
						muestraZonas(a,json[a].idCeco,json[a].descCeco,Object.keys(json).length,""+(fila+1));
					
					}
				} 	
			},error : function() {
				//alert('Favor de actualizar la pagina');
			}
		});
	}	
}


function despliegaCheckIntervalo(){

	document.getElementById("tipoModal").className="modal3";
	document.getElementById("idLlenaZonas"+validaCheck).innerHTML = "";
	arreglo = [];
	
	$.ajax({
		type : "GET",
		url : '/checklist/reportes/ajaxBuscaCecosPA.json?idCeco='+validaCheck,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			
			if (json == null) {
				//alert('Sin Datos');
				$("#idLlenaZonas"+validaCheck).append("<span class='Gris1'>Sin Datos</span>");
				document.getElementById("tipoModal").className="modal1";
			}else{					
				
				for (var a = 0; a < Object.keys(json).length; a++) {
					$("#idLlenaZonas"+validaCheck).append("<table class='bloquer subnivel'><tbody><tr id='"+json[a].idCeco+"' class='pointer'><td id='nom"+(filaHijo+1)+(a)+"' class='nombreCheck'>"+json[a].descCeco+"</td>");
		
					$("#"+json[a].idCeco).append("<td id='ape"+(filaHijo+1)+(a)+"'><div class='radio grisN'>-<div></td>");
					$("#"+json[a].idCeco).append("<td id='cie"+(filaHijo+1)+(a)+"'><div class='radio grisN'>-<div></td>");
					$("#"+json[a].idCeco).append("<td id='cum1"+(filaHijo+1)+(a)+"'><div class='radio grisN'>-<div></td>");
					$("#"+json[a].idCeco).append("<td id='cum2"+(filaHijo+1)+(a)+"'><div class='radio grisN'>-<div></td>");
					$("#"+json[a].idCeco).append("<td id='tar"+(filaHijo+1)+(a)+"'><div class='radio grisN'>-<div></td>");
				
					$("#idLlenaZonas"+validaCheck).append("</tr><tbody></table>");
					
					muestraZonas(a,json[a].idCeco,json[a].descCeco,Object.keys(json).length,""+(filaHijo+1));
				
				}
			} 	
		},error : function() {
			//alert('Favor de actualizar la pagina');
		}
	});
}
