package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistActualDTO;

public class ChecklistActualRowMapper implements RowMapper<ChecklistActualDTO> {

	@Override
	public ChecklistActualDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		ChecklistActualDTO checklistActualDTO = new ChecklistActualDTO();
		
		checklistActualDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		checklistActualDTO.setNombreCheck(rs.getString("FCNOMBRE"));
		checklistActualDTO.setIdHorario(rs.getInt("FIID_HORARIO"));
		checklistActualDTO.setIdTipoChecklist(rs.getInt("FIID_TIPO_CHECK"));
		checklistActualDTO.setFechaInicioCheck(rs.getString("FDFECHA_INICIO"));
		checklistActualDTO.setFechaFinCheck(rs.getString("FDFECHA_FIN"));
		checklistActualDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		checklistActualDTO.setPregunta(rs.getString("PREGUNTA"));
		checklistActualDTO.setIdModulo(rs.getInt("IDMODULO"));
		checklistActualDTO.setNombreModulo(rs.getString("MODULO"));
		checklistActualDTO.setIdModuloPadre(rs.getInt("FIID_MOD_PADRE"));
		checklistActualDTO.setNombreModuloPadre(rs.getString("MODULOPADRE"));
		checklistActualDTO.setOrdenCheck(rs.getInt("ORDEN_CHECK"));
		checklistActualDTO.setIdArbol(rs.getInt("FIID_ARBOL_DES"));
		checklistActualDTO.setIdPosible(rs.getInt("FIIDPOSIBLE"));
		checklistActualDTO.setPosibleRespuesta(rs.getString("FCDESCRIPCION"));
		checklistActualDTO.setEstatusEvidencia(rs.getInt("FIESTATUS_E"));
		checklistActualDTO.setSiguientePregunta(rs.getInt("FIORDEN_CHECK"));
		checklistActualDTO.setRequiereAccion(rs.getInt("FIREQACCION"));
		checklistActualDTO.setRequiereObsv(rs.getInt("FIREQOBS"));
		checklistActualDTO.setEvidenciaObligatoria(rs.getInt("FIOBLIGA"));
		checklistActualDTO.setEtiquetaEvidencia(rs.getString("FCDESCRIPCION_E"));
		checklistActualDTO.setTipoPregunta(rs.getInt("FIID_TIPO_PREG"));
		checklistActualDTO.setPonderacion(rs.getString("FIPODENRACION"));
		checklistActualDTO.setIdPlantilla(rs.getInt("FIID_PLANTILLA"));
		checklistActualDTO.setIdProtocolo(rs.getInt("FIID_PROTOCOLO"));
		
		return checklistActualDTO;
	}
	
	

}
