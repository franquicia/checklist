package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.TipoArchivoDAO;
import com.gruposalinas.checklist.domain.TipoArchivoDTO;

public class TipoArchivoBI {
	
	private static Logger logger = LogManager.getLogger(TipoArchivoBI.class);
	
	@Autowired
    TipoArchivoDAO tipoArchivoDAO;
		
	List<TipoArchivoDTO> listaTipoArchivos = null;
	
	List<TipoArchivoDTO> listaTipoArchivo = null;
	
	public int insertaTipoArchivo( TipoArchivoDTO tipoArchivo ) {
		
		int idTipoArchivo = 0;
		
		try {		
			idTipoArchivo = tipoArchivoDAO.insertaTipoArchivo(tipoArchivo);			
		} catch (Exception e) {
			logger.info("No fue posible insertar el Tipo de Archivo");
		}
		
		return idTipoArchivo;		
	}
	
	public boolean eliminaTipoArchivo(int idTipo){
		
		boolean respuesta = false;
		
		try{
			respuesta = tipoArchivoDAO.eliminaTipoArchivo(idTipo);
		}catch(Exception e){
			logger.info("No fue posible borrar el Tipo de Archivo");
		}
		
		return respuesta;
	}
	
	public boolean actualizaTipoArchivo (TipoArchivoDTO tipoArchivo){
		
		boolean respuesta = false;
				
		try {
			respuesta = tipoArchivoDAO.actualizaTipoArchivo(tipoArchivo);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Tipo de Archivo");
		}
							
		return respuesta;
	}
	
	
	public List<TipoArchivoDTO> obtieneTipoArchivo(){
				
		try{
			listaTipoArchivos = tipoArchivoDAO.obtieneTipoArchivo();	
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Tipo Archivo");
		}
				
		return listaTipoArchivos;
	}
	
	public List<TipoArchivoDTO> obtieneTipoArchivo(int idTipo){
				
		try{
			listaTipoArchivo = tipoArchivoDAO.obtieneTipoArchivo(idTipo);	
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Tipo Archivo");
		}
				
		return listaTipoArchivo;
	}	
}
