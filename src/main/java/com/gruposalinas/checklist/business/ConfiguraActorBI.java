package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.ConfiguraActorDAO;
import com.gruposalinas.checklist.domain.ConfiguraActorDTO;

public class ConfiguraActorBI {

	private static Logger logger = LogManager.getLogger(ConfiguraActorBI.class);

private List<ConfiguraActorDTO> listafila;
	
	@Autowired
	ConfiguraActorDAO configuraActorDAO;
		
	
	public int insertaConfActor(ConfiguraActorDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = configuraActorDAO.insertaConfActor(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaConfActor(String idEmpFijo){
		boolean respuesta = false;
		
		try {
			respuesta = configuraActorDAO.eliminaConfActor(idEmpFijo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<ConfiguraActorDTO> obtieneDatosConfActor( String idUsuario) {
		
		try {
			listafila =  configuraActorDAO.obtieneDatosConfActor(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	
	
	public boolean actualizaConfActor(ConfiguraActorDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = configuraActorDAO.actualizaConfActor(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	
	
}