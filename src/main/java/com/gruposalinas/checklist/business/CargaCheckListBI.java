package com.gruposalinas.checklist.business;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.CargaCheckListDAO;
import com.gruposalinas.checklist.domain.CargaCheckListDTO;


public class CargaCheckListBI {
	
	private Logger logger = LogManager.getLogger(ChecklistNegocioBI.class);
	
	@Autowired
	CargaCheckListDAO cargaCheckListDAO;
	
	
	public boolean insertaCheckList(){
		boolean respuesta = false;
		
		try {
			respuesta = cargaCheckListDAO.insertaCheckList();
		} catch (Exception e) {
			logger.info("No fue posible insertar informacion checklist 313 y 291 en la tabla FRTACHECKUSUA");
				
		}
		
		return respuesta;
	}
	

}
