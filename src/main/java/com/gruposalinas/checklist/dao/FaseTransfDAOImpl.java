package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.FaseTransfDTO;
import com.gruposalinas.checklist.mappers.FaseTransfAgrupaRowMapper;
import com.gruposalinas.checklist.mappers.FaseTransfRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class FaseTransfDAOImpl extends DefaultDAO implements FaseTransfDAO {

    private static Logger logger = LogManager.getLogger(FaseTransfDAOImpl.class);

    DefaultJdbcCall jdbcInsertaVers;
    DefaultJdbcCall jdbcEliminaVers;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneVers;
    DefaultJdbcCall jbdcActualizaVers;
    DefaultJdbcCall jbdcActualizaVers2;

    //fase
    DefaultJdbcCall jdbcInsertaFase;
    DefaultJdbcCall jdbcEliminaFase;
    DefaultJdbcCall jdbcObtieneFase;
    DefaultJdbcCall jbdcActualizaFase;

    public void init() {

        jdbcInsertaVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFASETRANSF")
                .withProcedureName("SP_INS_VERS");

        jdbcEliminaVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFASETRANSF")
                .withProcedureName("SP_DEL_VERS");

        jdbcObtieneVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFASETRANSF")
                .withProcedureName("SP_SEL_VERS")
                .returningResultSet("RCL_VERS", new FaseTransfAgrupaRowMapper());

        jbdcActualizaVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFASETRANSF")
                .withProcedureName("SP_ACT_VERS");

        //FAses
        jdbcInsertaFase = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFASETRANSF")
                .withProcedureName("SP_INS_PROYFA");

        jdbcEliminaFase = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFASETRANSF")
                .withProcedureName("SP_DEL_PROYFA");

        jdbcObtieneFase = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFASETRANSF")
                .withProcedureName("SP_SEL_PROYFA")
                .returningResultSet("RCL_PROYE", new FaseTransfRowMapper());

        jbdcActualizaFase = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADFASETRANSF")
                .withProcedureName("SP_ACT_PROYFA");

    }

    public int insertaAgrupa(FaseTransfDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_AGRUPA", bean.getIdAgrupa())
                .addValue("PA_FASE", bean.getFase())
                .addValue("PA_ORDENFAS", bean.getOrdenFase())
                //.addValue("PA_STATUS",bean.getIdactivo())
                .addValue("PA_AUX", bean.getAux())
                .addValue("PA_OBS", bean.getObs());

        out = jdbcInsertaVers.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADFASETRANSF.SP_INS_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDTABLA");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el Empleado");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean eliminaAgrupa(String idTab) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDTABLA", idTab);

        out = jdbcEliminaVers.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADFASETRANSF.SP_DEL_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el dato(" + idTab + ")");
        } else {
            return true;
        }

        return false;

    }

    //parametro
    @SuppressWarnings("unchecked")
    public List<FaseTransfDTO> obtieneDatosAgrupa(String fase) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<FaseTransfDTO> listaN = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROY", fase);

        out = jdbcObtieneVers.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADFASETRANSF.SP_SEL_VERS}");
        //lleva el nombre del cursor del procedure
        listaN = (List<FaseTransfDTO>) out.get("RCL_VERS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la version(" + fase + ")");
        }
        return listaN;

    }

    public boolean actualizaAgrupa(FaseTransfDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDTABLA", bean.getIdTab())
                .addValue("PA_AGRUPA", bean.getIdAgrupa())
                .addValue("PA_FASE", bean.getFase())
                .addValue("PA_ORDENFAS", bean.getOrdenFase())
                .addValue("PA_STATUS", bean.getIdactivo())
                .addValue("PA_AUX", bean.getAux())
                .addValue("PA_OBS", bean.getObs());

        out = jbdcActualizaVers.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADFASETRANSF.SP_ACT_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar  del  id( " + bean.getIdTab() + ")");
        } else {
            return true;
        }
        return false;

    }

    @Override
    public int inserta(FaseTransfDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROY", bean.getProyecto())
                .addValue("PA_FASE", bean.getFase())
                .addValue("PA_VERS", bean.getVersion())
                .addValue("PA_OBS", bean.getObs());

        out = jdbcInsertaFase.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADFASETRANSF.SP_INS_PROYFA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_PROYFA");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el FASE");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    @Override
    public boolean elimina(String idTab) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_PROYFA", idTab);

        out = jdbcEliminaFase.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADFASETRANSF.SP_DEL_PROYFA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el dato(" + idTab + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<FaseTransfDTO> obtieneDatos(String idproyecto) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<FaseTransfDTO> listaN = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROY", idproyecto);

        out = jdbcObtieneFase.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADFASETRANSF.SP_SEL_PROYFA}");
        //lleva el nombre del cursor del procedure
        listaN = (List<FaseTransfDTO>) out.get("RCL_PROYE");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener datos del proyecto(" + idproyecto + ")");
        }
        return listaN;

    }

    @Override
    public boolean actualiza(FaseTransfDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROYFA", bean.getIdTab())
                .addValue("PA_PROY", bean.getProyecto())
                .addValue("PA_FASE", bean.getFase())
                .addValue("PA_VERS", bean.getVersion())
                .addValue("PA_OBS", bean.getObs());

        out = jbdcActualizaFase.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADFASETRANSF.SP_ACT_PROYFA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar  del  id( " + bean.getIdTab() + ")");
        } else {
            return true;
        }
        return false;
    }

}
