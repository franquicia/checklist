// JavaScript Document

// Select 
jQuery(document).ready(function($) {
	$( ".normal_select").dropkick({
		mobile: true
	});
});

$(document).ready(function() {	
	$("#demo5").hide();
	$("#effect").toggle(false);
	$("#hamburger").click(function (event) {
		event.stopPropagation();
		 $( "#effect" ).toggle( "slide"); 
	});

	$(document).click(function() {
		$("#effect").toggle(false);
	});
	$("#effect").click (function (event){
		event.stopPropagation();
	});
});
//Menu perfil
$(document).ready(function() {
	$(".contConfig, divConfig").hide();

	$('.configuraciones').click(function() {
		$(".contConfig, .divConfig").toggle("ver");
	});
});
/*	
$( ".divConfig1.uno" ).mouseover(function() {
	$(".divConfig1 div img.imgIco1").attr("src","../images/supervisionSistemas/ico01.png");
	$(".divConfig1 div img.imgIco2").attr("src","../images/supervisionSistemas/ico2.svg");
});

$( ".divConfig1.dos" ).mouseover(function() {
	$(".divConfig1 div img.imgIco1").attr("src","../images/supervisionSistemas/ico1.png");
	$(".divConfig1 div img.imgIco2").attr("src","../images/supervisionSistemas/ico02.svg");
});*/

$( ".divConfig1.dos" ).mouseover(function() {
	//$(".divConfig1 div img.imgIco1").attr("src","../images/supervisionSistemas/ico1.png");
	$(".divConfig1 div img.imgIco2").attr("src","../images/supervisionSistemas/ico02.svg");
});

$( ".divConfig1.dos" ).mouseout(function() {
	//$(".divConfig1 div img.imgIco1").attr("src","../images/supervisionSistemas/ico1.png");
	$(".divConfig1 div img.imgIco2").attr("src","../images/supervisionSistemas/ico2.svg");
});
	
// Select 
jQuery(document).ready(function($) {
	
	$(".pag2, #miForm1").hide();	
	$(".btnpag1").click(function (event) {
		$(".pag1").hide();
		$(".pag2").show();
		$("#miForm1").hide();
	});
	
	$(".titFecha").hide();
	$(".titChecklist").hide();
	$(".titSucursal").hide();
	$("#paginationdemo").hide();
	$(".divSucursal").hide();
});

function carga(f) {
	$("#demo5").paginate({
			count 		: 1,
			start 		: 1,
			display     : 1,
			border					: false,
			border_color			: false,
			text_color  			: '#8a8a8a',
			background_color    	: 'transparent',	
			border_hover_color		: 'transparent',
			text_hover_color  		: '#000',
			background_hover_color	: 'transparent', 
			images					: true,
			mouse					: 'press',
			onChange     			: function(page){
										$('._current','#paginationdemo').removeClass('_current').hide();
										$('#p'+page).addClass('_current').show();
									  }
		});
}

function valida(f) {
	$("#demo5").show();
	obtenerChecklist();
  	$(".titFecha").show();
	$(".titChecklist").hide();
	$(".titSucursal").hide();
	$("#paginationdemo").show();
	$(".divSucursal").hide();
	carga();
	$('input[type="text"]').val('');
	$(".aviso").text('');
  return false;
	
	if((f.datepicker2.value !== "")  && (f.datepicker3.value !== ""))
  	{	
		obtenerChecklist();
	  	$(".titFecha").show();
		$(".titChecklist").hide();
		$(".titSucursal").hide();
		$("#paginationdemo").show();
		$(".divSucursal").hide();
		carga();
		$('input[type="text"]').val('');
		$(".aviso").text('');
	  return false;
  	}
	else if(f.selCheck.value == "1") 
  	{	
		obtenerChecklist();
	  	$(".titFecha").hide();
		$(".titChecklist").show();
		$(".titSucursal").hide();
		$("#paginationdemo").show();
		$(".divSucursal").hide();
		$('#miForm')[0].reset();
		$(".aviso").text('');
		carga();
	  return false;
  	}
	else if(((f.datepicker2.value == "")  && (f.datepicker3.value == "")) && (f.selSucursal.value !== '2') )
  	{	
	  	$(".aviso").text('Por favor ingrese un criterio de busqueda');
		$(".titFecha").hide();
		$(".titChecklist").hide();
		$(".titSucursal").hide();
		$("#paginationdemo").hide();
	  	return false;
  	}

	else if(f.selSucursal.value == '2')  
  	{	
	  	$(".titFecha").hide();
		$(".titChecklist").hide();
		$(".titSucursal").show();
		$("#paginationdemo").hide();
		$(".divSucursal").show();
		$('#miForm')[0].reset();
		$(".aviso").text('');
	  return false;
  	}
	else{
		$(".aviso").text('Por favor ingrese un criterio de busqueda');
		$(".titFecha").hide();
		$(".titChecklist").hide();
		$(".titSucursal").hide();
		$("#paginationdemo").hide();
	  	return false;
	}
}

function obtenerChecklist(){
	var datepicker2 = $("#datepicker2").val();
	var datepicker3 = $("#datepicker3").val();
	var selCheck = $("#selCheck").val();
	var selSucursal = $("#tags").val();
	$("#periodoBusqueda").text(" por Periodo: " + datepicker2+" - "+datepicker3);
	
	if(selCheck=="")
		selCheck = selCheckL;
	
	$.ajax({
		type : "GET",
		url :'../ajaxReportesSisCap.json?fInicio='+datepicker2+'&fFin='+datepicker3+'&checklist='+selCheck+'&sucursal='+selSucursal+'&area='+idCecoBusquedaL,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			var tablaBusqueda = $("#busqueda");
			tablaBusqueda.html("<tr><th>Fecha</th>\r\n" + 
					"				<th>#Sucursal</th>\r\n" + 
					"				<th>Sucursal</th>\r\n" + 
					"				<th>Región</th>\r\n" + 
					"				<th>Tipo de Checklist</th>\r\n" + 
					"			</tr>");
			/*tablaBusqueda.html("<tr><th>Fecha <img src='../images/supervisionSistemas/flechaT.svg' class=\"flechaT flechaT1\"></th>\r\n" + 
					"				<th>#Sucursal <img src=\"../images/supervisionSistemas/flechaT.svg\" class=\"flechaT\"></th>\r\n" + 
					"				<th>Sucursal <img src=\"../images/supervisionSistemas/flechaT.svg\" class=\"flechaT\"></th>\r\n" + 
					"				<th>Región <img src=\"../images/supervisionSistemas/flechaT.svg\" class=\"flechaT\"></th>\r\n" + 
					"				<th>Tipo de Checklist</th>\r\n" + 
					"			</tr>");*/
			for (var i = 0; i < Object.keys(json).length; i++) {
				tablaBusqueda.append("<tr style='cursor: pointer;' onclick='detalleBitacora("+json[i].idBitacora+")' ><td>"+json[i].finicio+"</td><td>"+json[i].evaluado+"</td><td>"+json[i].sucursal+"</td><td>"+json[i].region+"</td><td>"+json[i].checklist+"</td>");
			}

		}
	});	
}

function detalleBitacora(id,nomSuc){
	$.ajax({
		type : "GET",
		url :'../ajaxSupervisionEvidencia.json?idBitacora='+id,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			//alert("Ruta evidencias:"+json[0][0].ruta);
			//Evidencia
						
			var contenedorEvidencia = $("#contenedorEvidencia");
			contenedorEvidencia.html("<table class='tblGeneral' style='border: none !important;'><tbody><tr class='rdSecc'><td class='titTabla' style='background-color: #f3f4f4 !important;'>Evidencias</td></tr></table>");
			for (var i = 0; i < Object.keys(json[0]).length; i++) {
				contenedorEvidencia.append("" +
					"<div id=evidencia"+json[0][i].idrespuesta+" class='gal evidenciaPregunta'>" +
						(json[0][i].ruta.includes("/.franquicia/") ? "<img src='http://200.38.122.186"+json[0][i].ruta.replace("/.franquicia/.", "/franquicia/imagenes/2018/")+"'>": "<img src='http://200.38.122.186"+json[0][i].ruta+"'>")  + /*"<img src='http://200.38.122.186"+json[0][i].ruta+"'>" +*/
						"<div class='clear'></div>" + 
				"<table class='tblGal'><tr><td>"+json[0][i].respuesta+"<br>EVIDENCIA</td><td class='tRight'>"+json[0][i].fecha+"</td></tr></table></div>");
			}
//			if(json[2] != null){
				$("#nombreSucursal").text(" "+ nomSuc);
				$("#nombreEvaluador").text(" "+ json[2][0].evaluador);
				$("#fechaEvaluacion").text(" "+ json[2][0].fecha);
//			}
			    
			//Preguntas
			var tablaBusqueda = $("#tablaPreguntas");
			tablaBusqueda.html("<tr class=\"rdSecc\">\r\n" + 
		"					  <th colspan=\"2\" class=\"titTabla\" style='background-color: #f3f4f4 !important;'>Criterios de medición</th>\r\n" + 
		"					  <th colspan=\"2\" class=\"titTabla\" style='background-color: #f3f4f4 !important;'><strong>Fecha: <span>"+json[2][0].fecha+"</span></strong></th>\r\n" + 
		"					</tr>");
			tablaBusqueda.append("<tr><th>Folio</th><th>Ponderacion</th><th>Pregunta</th><th>-</th></tr>");
			for (var i = 0; i < Object.keys(json[1]).length; i++) {
				tablaBusqueda.append("<tr style='cursor: pointer;' onclick='iluminarEvidencia("+json[1][i].idrespuesta+")'><td>"+json[1][i].idPregunta+"</td><td>"+json[1][i].ponderacion+"</td><td>"+json[1][i].pregunta+"</td><td>"+json[1][i].respuesta+"</td></tr>");
			}
			
			$(".margenRegresa").show();
			$(".margenRegresaPrincipal").hide();
			
			$(".titFecha").hide();
			$(".titChecklist").hide();
			$(".titSucursal").show();
			$("#paginationdemo").hide();
			$(".divSucursal").show();
			$('#miForm')[0].reset();
			$(".aviso").text('');
			$(".evidenciaPregunta").css('border','3px solid WHITE');
			
		}
	});	
	
}
	
function iluminarEvidencia(id){
	$(".evidenciaPregunta").css('border','3px solid WHITE');
	$("#evidencia"+id).css('border','3px solid RED');
}






//Calendario
$.datepicker.regional['es'] = {
	 closeText: 'Cerrar',
	 prevText: '',
	 nextText: ' ',
	 currentText: 'Hoy',
	 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
	 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
	 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
	 weekHeader: 'Sm',
	 dateFormat: 'dd/mm/yy',
	 firstDay: 1,
	 isRTL: false,
	 showMonthAfterYear: false,
	 yearSuffix: ''
	 };
	$.datepicker.setDefaults($.datepicker.regional['es']);
	$(function() {
		$( "#datepicker1" ).datepicker({
			firstDay: 1 
		});
		$( "#datepicker2" ).datepicker({
			firstDay: 1 
		});
		$( "#datepicker3" ).datepicker({
			firstDay: 1 
		});
		
	});






	