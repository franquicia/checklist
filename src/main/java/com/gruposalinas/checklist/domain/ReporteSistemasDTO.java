package com.gruposalinas.checklist.domain;

public class ReporteSistemasDTO {

	private int idChecklist;
	private String nombreCheck;
	private int idGrupo;
	private int ordenGrupo;
	
	private int idCeco;
	private String nombreCeco;
	private int numSucursal;
	private int porcentaje;
	private int terminados;
	private int asignados;
	private int totales;
	private int actuales;
	
	private int idPregunta;
	private String pregunta;
	private String ponderacion;
	private int idrespuesta;
	private String respuesta;
	private String evaluador;
	private String fecha;
	
	private int idEvidencia;
	private String ruta;
	
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public String getNombreCheck() {
		return nombreCheck;
	}
	public void setNombreCheck(String nombreCheck) {
		this.nombreCheck = nombreCheck;
	}
	public int getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(int idGrupo) {
		this.idGrupo = idGrupo;
	}
	public int getOrdenGrupo() {
		return ordenGrupo;
	}
	public void setOrdenGrupo(int ordenGrupo) {
		this.ordenGrupo = ordenGrupo;
	}
	public int getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(int idCeco) {
		this.idCeco = idCeco;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	public int getNumSucursal() {
		return numSucursal;
	}
	public void setNumSucursal(int numSucursal) {
		this.numSucursal = numSucursal;
	}
	public int getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(int porcentaje) {
		this.porcentaje = porcentaje;
	}
	public int getTerminados() {
		return terminados;
	}
	public void setTerminados(int terminados) {
		this.terminados = terminados;
	}
	public int getAsignados() {
		return asignados;
	}
	public void setAsignados(int asignados) {
		this.asignados = asignados;
	}
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public String getPregunta() {
		return pregunta;
	}
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}
	public String getPonderacion() {
		return ponderacion;
	}
	public void setPonderacion(String ponderacion) {
		this.ponderacion = ponderacion;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public int getIdrespuesta() {
		return idrespuesta;
	}
	public void setIdrespuesta(int idrespuesta) {
		this.idrespuesta = idrespuesta;
	}
	public int getIdEvidencia() {
		return idEvidencia;
	}
	public void setIdEvidencia(int idEvidencia) {
		this.idEvidencia = idEvidencia;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public int getTotales() {
		return totales;
	}
	public void setTotales(int totales) {
		this.totales = totales;
	}
	public int getActuales() {
		return actuales;
	}
	public void setActuales(int actuales) {
		this.actuales = actuales;
	}
	public String getEvaluador() {
		return evaluador;
	}
	public void setEvaluador(String evaluador) {
		this.evaluador = evaluador;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	@Override
	public String toString() {
		return "ReporteSistemasDTO [idChecklist=" + idChecklist + ", nombreCheck=" + nombreCheck + ", idGrupo="
				+ idGrupo + ", ordenGrupo=" + ordenGrupo + ", idCeco=" + idCeco + ", nombreCeco=" + nombreCeco
				+ ", numSucursal=" + numSucursal + ", porcentaje=" + porcentaje + ", terminados=" + terminados
				+ ", asignados=" + asignados + ", totales=" + totales + ", actuales=" + actuales + ", idPregunta="
				+ idPregunta + ", pregunta=" + pregunta + ", ponderacion=" + ponderacion + ", idrespuesta="
				+ idrespuesta + ", respuesta=" + respuesta + ", evaluador=" + evaluador + ", fecha=" + fecha
				+ ", idEvidencia=" + idEvidencia + ", ruta=" + ruta + "]";
	}
	
}
