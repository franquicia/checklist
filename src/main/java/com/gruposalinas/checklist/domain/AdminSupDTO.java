package com.gruposalinas.checklist.domain;

public class AdminSupDTO {
	/* Búsqueda de empleado */
	private int idUsuario;
	private String nomUsuario;
	private int idMenu;
	private String descMenu;
	
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNomUsuario() {
		return nomUsuario;
	}
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}
	public int getIdMenu() {
		return idMenu;
	}
	public void setIdMenu(int idMenu) {
		this.idMenu = idMenu;
	}
	public String getDescMenu() {
		return descMenu;
	}
	public void setDescMenu(String descMenu) {
		this.descMenu = descMenu;
	}
	
	@Override
	public String toString() {
		return "AdminSupDTO [idUsuario=" + idUsuario + ", nomUsuario=" + nomUsuario + ", idMenu=" + idMenu
				+ ", descMenu=" + descMenu + "]";
	}
	
		
}


