package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ChecklistUsuarioDTO;

public interface ChecklistUsuarioDAO {

	public List<ChecklistUsuarioDTO> obtieneCheckUsuario(int idCheckU) throws Exception;
	
	public List<ChecklistUsuarioDTO> obtieneCheckU (String idUsuario) throws Exception;
	
	public List<ChecklistUsuarioDTO> obtieneCheckUsua (String idUsuario, String ceco, String fechaInicio) throws Exception;
	
	public int insertaCheckUsuario(ChecklistUsuarioDTO bean) throws Exception;
	
	public boolean actualizaCheckUsuario(ChecklistUsuarioDTO bean) throws Exception;
	
	public boolean eliminaCheckUsuario(int idCheckU) throws Exception;
	
	public boolean desactivaChecklist(int idChecklist)throws Exception;
	
	public boolean desactivaCheckUsua(int idCeco,int idChecklist)throws Exception;
	
	public boolean insertaCheckusuaEspecial(int idUsuario, int idChecklist, String ceco)throws Exception;
	
	
}
