package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.TokenDTO;

public class TokenRowMapper implements RowMapper<TokenDTO> {

	public TokenDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		TokenDTO tokenDTO = new TokenDTO();
		
		tokenDTO.setIdToken(rs.getInt("FIID_TOKEN"));
		tokenDTO.setUsuario(rs.getInt("FIID_USUARIO"));
		tokenDTO.setToken(rs.getString("FCTOKEN"));
		tokenDTO.setEstatus(rs.getInt("FIESTATUS"));
	
		return tokenDTO;
	}

}
