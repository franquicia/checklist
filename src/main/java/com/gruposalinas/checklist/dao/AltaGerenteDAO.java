package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.AltaGerenteDTO;

public interface AltaGerenteDAO {


	public boolean insertaGerente(AltaGerenteDTO bean) throws Exception;
	public List<AltaGerenteDTO> obtieneIdGerente(AltaGerenteDTO bean) throws Exception;
	public List<AltaGerenteDTO> obtieneIdUsuarios(AltaGerenteDTO bean) throws Exception;
	public boolean actualizaDisSup(AltaGerenteDTO bean) throws Exception;
	public boolean eliminaDisSup(AltaGerenteDTO bean) throws Exception;
	//public List<AltaGerenteDTO> obtienefullTable(AltaGerenteDTO bean) throws Exception;
	public List<AltaGerenteDTO> obtienefullTable(String idUsuario, String idGerente) throws Exception;
	
}
