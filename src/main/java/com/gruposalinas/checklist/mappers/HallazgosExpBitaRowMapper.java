package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.HallazgosExpDTO;

public class HallazgosExpBitaRowMapper implements RowMapper<HallazgosExpDTO> {

	public HallazgosExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		HallazgosExpDTO HallazgosExpDTO = new HallazgosExpDTO();
		
		HallazgosExpDTO.setIdHallazgo(rs.getInt("FIID_HALLAZGO"));
	    HallazgosExpDTO.setIdFolio(rs.getInt("FIID_FOLIO"));
	    HallazgosExpDTO.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    HallazgosExpDTO.setStatus(rs.getInt("FCSTATUS"));
	    HallazgosExpDTO.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    HallazgosExpDTO.setPreg(rs.getString("FCPREGUNTA"));
	    HallazgosExpDTO.setRespuesta(rs.getString("FCRESPUESTA"));
	    HallazgosExpDTO.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
	    HallazgosExpDTO.setResponsable(rs.getString("FCRESPONSABLE"));
	    HallazgosExpDTO.setDisposi(rs.getString("FCDISPOSITIVO"));
	    HallazgosExpDTO.setArea(rs.getString("FCAREA"));
	    HallazgosExpDTO.setObs(rs.getString("FCOBSERVACION"));
	    HallazgosExpDTO.setBitGral(rs.getInt("FIID_BITGRAL"));
	    //ES EL ARBOL DESICION AUX
	    HallazgosExpDTO.setArbol(rs.getString("IDARBOL"));
	    //RUTAEVI
	    HallazgosExpDTO.setRuta(rs.getString("RUTA"));
	    HallazgosExpDTO.setFechaIni(rs.getString("FDINICIO"));
	    HallazgosExpDTO.setFechaFin(rs.getString("FDFIN"));
	    HallazgosExpDTO.setSla(rs.getString("FCSLA"));
	    HallazgosExpDTO.setFechaAutorizo(rs.getString("FDAUTORIZO"));
	    HallazgosExpDTO.setPeriodo(rs.getString("FCPERIODO"));
	    HallazgosExpDTO.setCeco(rs.getString("FCID_CECO"));
	    HallazgosExpDTO.setIdcheck(rs.getInt("FIID_CHECKLIST"));
		
	   
		return HallazgosExpDTO;
	}

}


