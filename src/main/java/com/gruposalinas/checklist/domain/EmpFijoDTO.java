package com.gruposalinas.checklist.domain;

public class EmpFijoDTO {
	


		private int idEmpFijo;
		private int idUsuario;
		private int idActivo;
		
		public int getIdEmpFijo() {
			return idEmpFijo;
		}
		public void setIdEmpFijo(int idEmpFijo) {
			this.idEmpFijo = idEmpFijo;
		}
		public int getIdUsuario() {
			return idUsuario;
		}
		public void setIdUsuario(int idUsuario) {
			this.idUsuario = idUsuario;
		}
		public int getIdActivo() {
			return idActivo;
		}
		public void setIdActivo(int idActivo) {
			this.idActivo = idActivo;
		}
		
		public String toString() {
			return "EmpFijoDTO [idEmpFijo="+idEmpFijo+",idUsuario="+idUsuario+",idActivo="+idActivo+"]";
		}
		
	}