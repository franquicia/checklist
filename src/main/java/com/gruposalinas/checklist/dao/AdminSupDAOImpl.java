package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AdminSupDTO;
import com.gruposalinas.checklist.mappers.BusCecoSupRowMapper;
import com.gruposalinas.checklist.mappers.DetCecoSupRowMapper;
import com.gruposalinas.checklist.mappers.DetPersonalSupRowMapper;
import com.gruposalinas.checklist.mappers.DetalleSupRowMapper;
import com.gruposalinas.checklist.mappers.EmpleadoSupRowMapper;
import com.gruposalinas.checklist.mappers.MenuSupRowMapper;
import com.gruposalinas.checklist.mappers.PersonalSupRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AdminSupDAOImpl extends DefaultDAO implements AdminSupDAO {

    private static Logger logger = LogManager.getLogger(AdminSupDAOImpl.class);

    DefaultJdbcCall jdbcBuscaEmp;
    DefaultJdbcCall jdbcDetalleEmp;
    DefaultJdbcCall jdbcPersonalAsig;
    DefaultJdbcCall jdbcCecosAsig;
    DefaultJdbcCall jdbcBuscaCeco;

    public void init() {

        jdbcBuscaEmp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMONPERFIL")
                .withProcedureName("SPBUSCADOREMP")
                .returningResultSet("PA_CONSULTA", new EmpleadoSupRowMapper());

        jdbcDetalleEmp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMONPERFIL")
                .withProcedureName("SPGETMENUASIGN")
                .returningResultSet("PA_CONSMENU", new MenuSupRowMapper())
                .returningResultSet("PA_CONSULTA", new DetalleSupRowMapper());

        jdbcPersonalAsig = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMONPERFIL")
                .withProcedureName("SPGETPERSNASIGN")
                .returningResultSet("PA_CONSPERN", new DetPersonalSupRowMapper())
                .returningResultSet("PA_CONSULTA", new PersonalSupRowMapper());

        jdbcCecosAsig = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMONPERFIL")
                .withProcedureName("SPGETCECOSASIGN")
                .returningResultSet("PA_CONSCECO", new BusCecoSupRowMapper())
                .returningResultSet("PA_CONSULTA", new PersonalSupRowMapper());

        jdbcBuscaCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMONPERFIL")
                .withProcedureName("SPBUSCADORCECO")
                .returningResultSet("PA_CONSULTA", new DetCecoSupRowMapper());
    }

    @Override
    public List<AdminSupDTO> getEmpleado(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        List<AdminSupDTO> respuesta = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario);

        out = jdbcBuscaEmp.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADMONPERFIL.SPBUSCADOREMP");

        respuesta = (List<AdminSupDTO>) out.get("PA_CONSULTA");

        if (respuesta.size() == 0) {
            logger.info("Algo ocurrió al el empleado");
        }
        return respuesta;
    }

    @Override
    public Map<String, Object> getDetalleEmpleado(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> mapa = null;
        List<AdminSupDTO> respDetalle = null;
        List<AdminSupDTO> respMenus = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario);

        out = jdbcDetalleEmp.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADMONPERFIL.SPGETMENUASIGN");
        respDetalle = (List<AdminSupDTO>) out.get("PA_CONSULTA");
        respMenus = (List<AdminSupDTO>) out.get("PA_CONSMENU");

        if (respDetalle.size() == 0) {
            logger.info("Algo ocurrió al obtener el detalle del empleado");
        }
        if (respMenus.size() == 0) {
            logger.info("Algo ocurrió al obtener los menus");
        }

        mapa = new HashMap<String, Object>();
        mapa.put("detalle", respDetalle);
        mapa.put("menus", respMenus);
        return mapa;
    }

    @Override
    public Map<String, Object> getPersonalAsig(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> respuesta = null;
        List<AdminSupDTO> respDetalle = null;
        List<AdminSupDTO> respPersonal = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario);

        out = jdbcPersonalAsig.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADMONPERFIL.SPGETPERSNASIGN");

        respDetalle = (List<AdminSupDTO>) out.get("PA_CONSULTA");
        respPersonal = (List<AdminSupDTO>) out.get("PA_CONSPERN");

        if (respDetalle.size() == 0) {
            logger.info("Algo ocurrió al obtener el detalle del socio");
        }
        if (respPersonal.size() == 0) {
            logger.info("Algo ocurrió al obtener la linea inmediata");
        }

        respuesta = new HashMap<String, Object>();
        respuesta.put("detalle", respDetalle);
        respuesta.put("personal", respPersonal);
        return respuesta;
    }

    @Override
    public Map<String, Object> getCecosAsig(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> respuesta = null;
        List<AdminSupDTO> respDetalle = null;
        List<AdminSupDTO> respCecos = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario);

        out = jdbcCecosAsig.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADMONPERFIL.SPGETCECOSASIGN");

        respDetalle = (List<AdminSupDTO>) out.get("PA_CONSULTA");
        respCecos = (List<AdminSupDTO>) out.get("PA_CONSCECO");

        if (respDetalle.size() == 0) {
            logger.info("Algo ocurrió al obtener el detalle del socio");
        }
        if (respCecos.size() == 0) {
            logger.info("Algo ocurrió al obtener los cecos asignados");
        }

        respuesta = new HashMap<String, Object>();
        respuesta.put("detalle", respDetalle);
        respuesta.put("cecos", respCecos);
        return respuesta;
    }

    @Override
    public List<AdminSupDTO> busquedaCeco(String ceco) throws Exception {
        Map<String, Object> out = null;
        List<AdminSupDTO> respuesta = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FCID_CECO", ceco);

        out = jdbcBuscaCeco.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADMONPERFIL.SPBUSCADORCECO");

        respuesta = (List<AdminSupDTO>) out.get("PA_CONSULTA");

        if (respuesta.size() == 0) {
            logger.info("Algo ocurrió al obtener la sucursal");
        }
        return respuesta;
    }

}
