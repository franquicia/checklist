package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.EmpFijoDAO;
import com.gruposalinas.checklist.dao.SucUbicacionDAO;
import com.gruposalinas.checklist.domain.ConteoTablasDTO;
import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.SucUbicacionDTO;

public class SucUbicacionBI {

	private static Logger logger = LogManager.getLogger(SucUbicacionBI.class);

	private List<SucUbicacionDTO> listafila;
	private List<ConteoTablasDTO> listacont;

	@Autowired
	SucUbicacionDAO sucUbicacionDAO;

	public List<SucUbicacionDTO> obtieneDatos(String idpais) {

		try {
			logger.info("Obteniendo Sucursales ");
			listafila = sucUbicacionDAO.obtieneDatos(idpais);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos de ubicacion sucursales ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	public List<SucUbicacionDTO> obtieneInfo() {

		try {
			listafila = sucUbicacionDAO.obtieneInfo();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}
	
	public boolean cargaSucUbic() {
			boolean resp=false;
		try {
			resp = sucUbicacionDAO.cargaSucUbic();
		} catch (Exception e) {
			logger.info("No fue posible hacer carga de SucUbica ");
			UtilFRQ.printErrorLog(null, e);
		}

		return resp;
	}
	
	public boolean EliminacargaSucUbic() {
			boolean resp=false;
		try {
			resp = sucUbicacionDAO.EliminacargaSucUbic();
		} catch (Exception e) {
			logger.info("No fue posible liberar sucUbica ");
			UtilFRQ.printErrorLog(null, e);
		}

		return resp;
	}

	public List<SucUbicacionDTO> obtieneDatosTab(String idpais) {

		try {
			logger.info("Obteniendo Sucursales ");
			listafila = sucUbicacionDAO.obtieneDatosTab(idpais);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos de ubicacion sucursales ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}
	
	//conteo tablas
	public List<ConteoTablasDTO> obtieneInfoTab( ) {

		try {
			logger.info("Obteniendo Sucursales ");
			listacont = sucUbicacionDAO.obtieneInfoTab();
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos de los conteos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listacont;
	}
}
