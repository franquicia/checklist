package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.DetalleProtoDTO;

public class BitacoraCerradaRowMapper implements RowMapper<DetalleProtoDTO> {

	public DetalleProtoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		DetalleProtoDTO detprotoDTO = new DetalleProtoDTO();
		
		detprotoDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		detprotoDTO.setFdInicio(rs.getString("FDINICIO"));
		detprotoDTO.setFdTermino(rs.getString("FDTERMINO"));;
		
	return detprotoDTO;
	}
	

		

}






