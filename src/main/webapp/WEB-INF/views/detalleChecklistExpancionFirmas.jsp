<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css"
		href="../css/expancion/detalleExpancion.css">
	<link rel="stylesheet" type="text/css"
		href="../css/expancion/jquery.dataTablesImperdonables.css">

<script src="../js/expancion/jquery-3.3.1.js"></script>
<script src="../js/expancion/jquery.dataTables.js"></script>
<script src="../js/expancion/tableExpand.js"></script>
<style type="text/css"></style>
<title>DETALLES DE ASISTENTES FIRMAS</title>
</head>
<body style="max-width:1500px; margin: auto;">
	<div class="marginTotal">
		<div class="left">
			<div class="centrar">
				<img alt="elektra" src="../images/expancion/LogoElektra.svg"
					class="imagenBanner"> <img alt="baz"
					src="../images/expancion/LogoBAZ.svg" class="imagenBanner">
			</div>
		</div>
		<div class="main">
			<h1 style="color: #6B696E;" class="fuenteBold">
				Entrega de Sucursales / <strong class="fuenteBold">DETALLES
					DE ASISTENTES(FIRMAS)</strong>
			</h1>
			<br />

		</div>
		<div class="right">
			<div class="centrar">
				<img alt="elektra" src="../images/expancion/LogoElektra.svg"
					class="imagenBanner"> <img alt="baz"
					src="../images/expancion/LogoBAZ.svg" class="imagenBanner">
			</div>
		</div>

	</div>
	<div class="marginTotal">
		<div class="left"></div>
		<div class="main" style="width:96%;">
			<h2 class="fuenteBold">DETALLE DE ACEPTACION</h2>
			<br />
			<hr />
			

			<div style="padding-left: 5%; width: 100%; padding-top: 3%;">
				<!-- <button id="btn-show-all-children" type="button">Expand All</button>
				<button id="btn-hide-all-children" type="button">Collapse All</button> -->
				${firmasHtml}
				<c:if test="${firmasB != null }">
					<table id="tabla" class="display" cellspacing="0" width="100%">
					<tbody>
					
						<c:set var="count" value="0" scope="page" />
						<c:forEach var="valor" items="${firmasB}">
							<c:if test="${count == 0 || count ==3 }">
								<c:if test="${ count ==3 }">
									</tr>
								</c:if>
									<tr>
							</c:if>
							<td style="padding: 5%; border: none;">
								 <b> ${valor.puesto} </b>
								 <br></br>
								Nombre: ${valor.responsable} 
								 <br></br>
								 <br></br>
								<img src="${valor.ruta}"  width="200" height="100"/>
							</td>
						</c:forEach>
					
					
					</tbody>
					</table>
				</c:if>
			</div>
			<br /> <br /> <br />
			<p>Regresar al Reporte</p>
		</div> 
		<div class="right"></div>

	</div>





</body>
</html>