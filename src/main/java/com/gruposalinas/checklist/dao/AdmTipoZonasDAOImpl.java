package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AdmTipoZonaDTO;
import com.gruposalinas.checklist.mappers.AdmTipoZonasRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AdmTipoZonasDAOImpl extends DefaultDAO implements AdmTipoZonasDAO {

    private static Logger logger = LogManager.getLogger(AdmZonasDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTipoZonaById;
    DefaultJdbcCall jdbcObtieneTiposZonas;
    DefaultJdbcCall jdbcInsertaTipoZona;
    DefaultJdbcCall jdbcActualizaTipoZona;
    DefaultJdbcCall jdbcEliminaTipoZona;

    public void init() {
        jdbcObtieneTipoZonaById = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINTIPOZONA")
                .withProcedureName("SP_SEL_TIPOZONA")
                .returningResultSet("RCL_INFO", new AdmTipoZonasRowMapper());

        jdbcInsertaTipoZona = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINTIPOZONA")
                .withProcedureName("SP_INS_TIPOZONA");

        jdbcActualizaTipoZona = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINTIPOZONA")
                .withProcedureName("SP_ACT_TIPOZONA");

        jdbcEliminaTipoZona = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINTIPOZONA")
                .withProcedureName("SP_DEL_TIPOZONA");
    }

    public boolean insertaTipoZona(AdmTipoZonaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        boolean resp = false;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCDESCRIPCION", bean.getDescripcion())
                .addValue("PA_FIIDESTATUS", bean.getIdStatus());

        out = jdbcInsertaTipoZona.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICA.PAADMINTIPOZONA.SP_INS_TIPOZONA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar el tipo zona");
        } else {
            resp = true;
        }

        return resp;
    }

    public boolean actualizaTipoZona(AdmTipoZonaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        boolean resp = false;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCDESCRIPCION", bean.getDescripcion())
                .addValue("PA_FIIDESTATUS", bean.getIdStatus())
                .addValue("PA_FIIDTIPOZO", bean.getIdTipoZona());

        out = jdbcActualizaTipoZona.execute(in);
        logger.info("Funcion ejecutada: {FRANQUICA.PAADMINTIPOZONA.SP_ACT_TIPOZONA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al actualizar el tipo zona: " + bean.getIdTipoZona());
        } else {
            resp = true;
        }

        return resp;
    }

    public boolean eliminaTipoZona(AdmTipoZonaDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        boolean resp = false;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIIDTIPOZO", bean.getIdTipoZona());

        out = jdbcEliminaTipoZona.execute(in);
        logger.info("Funcion ejecutada: {FRANQUICA.PAADMINTIPOZONA.SP_DEL_TIPOZONA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al borrar el tipo zona: " + bean.getIdTipoZona());
        } else {
            resp = true;
        }

        return resp;
    }

    @SuppressWarnings("unchecked")
    public ArrayList<AdmTipoZonaDTO> obtieneTipoZonaById(AdmTipoZonaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        ArrayList<AdmTipoZonaDTO> zona = new ArrayList<>();

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIIDTIPOZO", bean.getIdTipoZona());

        out = jdbcObtieneTipoZonaById.execute(in);
        logger.info("Funcion ejecutada: {FRANQUICA.PAADMINTIPOZONA.SP_SEL_TIPOZONA}");
        zona = (ArrayList<AdmTipoZonaDTO>) out.get("RCL_INFO");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al consultar el tipo zona: " + bean.getIdTipoZona());
        }

        return zona;
    }

}
