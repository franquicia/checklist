package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteDTO;
import com.gruposalinas.checklist.domain.ReporteSupervisionSistemasDTO;

public class ReporteSupervisionSistemasFiltroCheckRowMapper implements RowMapper<ReporteSupervisionSistemasDTO>{

public ReporteSupervisionSistemasDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteSupervisionSistemasDTO reporteDTO = new ReporteSupervisionSistemasDTO();
		
		reporteDTO.setIdCecos(rs.getInt("FIID_CHECKLIST"));
		reporteDTO.setNombreCeco(rs.getString("FCNOMBRE"));
		
		return reporteDTO;
		
	}

}
