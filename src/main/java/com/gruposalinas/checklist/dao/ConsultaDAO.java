package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.ConsultaDTO;




public interface ConsultaDAO {
	
	public List<ConsultaDTO> obtieneConsultaCeco(int idGrupo) throws Exception;
	public List<ConsultaDTO> obtieneConsultaChecklist(int idGrupo) throws Exception;
	public List<ConsultaDTO> obtieneConsultaBitacora(int idCeco, int idChecklist) throws Exception;
	

}
