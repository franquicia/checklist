package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.DetalleProtoDTO;

public class NombreCecoRowMapper implements RowMapper<DetalleProtoDTO> {

	public DetalleProtoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		DetalleProtoDTO detprotoDTO = new DetalleProtoDTO();
		
		detprotoDTO.setIdCeco(rs.getString("FCID_CECO"));
		detprotoDTO.setNomCeco(rs.getString("FCNOMBRE"));
	return detprotoDTO;
	}
	

		

}






