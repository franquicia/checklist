<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="../css/bootstrap/bootstrap.css">

	<script	src="../js/jquery/jquery-2.2.4.min.js"></script>
	<script	src="../js/bootstrap.min.js"></script>
	
	<script src="../js/script-valida-coordenadas.js"></script>

	<style>
       #map {
        height: 360px;
        width: 100%;
       }
    </style>

<title>Soporte - Validar sucursal</title>
</head>
<body>

	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuSoporte.jsp" flush="true">
		<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuSoporte.jsp" />
	</tiles:insertTemplate>

	<br>
	<c:choose>
		<c:when test="${paso=='no'}">
			<div class="container">
				<div class="alert alert-danger" role="alert">
					<p class="text-center"> <h4>No se encontró la sucursal! </h4> 
						<br> Presione el botón de regresar para realizar otra búsqueda.
						<br>
					<form:form method="GET" action="${validaSucursalUrl}" model="command" name="form" id="form-no">
						<input type="submit" class="btn btn-danger btn-lg" value="Regresar" />
					</form:form>
				</div>
			</div>
		</c:when>
		<c:when test="${paso=='no2'}">
			<div class="container">
				<div class="alert alert-danger" role="alert">
					<p class="text-center"> <h4>No se encontró la sucursal! </h4>
						<h5>Pero se encontro en los paises ${Paises}</h5><label><c:out value="${paises}"/></label> 
						<br> Presione el botón de regresar para realizar otra búsqueda.
						<br>
					<form:form method="GET" action="${validaSucursalUrl}" model="command" name="form" id="form-no">
						<input type="submit" class="btn btn-danger btn-lg" value="Regresar" />
					</form:form>
				</div>
			</div>
		</c:when>

		<c:when test="${paso=='0'}">
			<div class="container">
				<div class="panel-group" id="accordion-0">
			    	<div class="panel panel-default">
			    		<div class="panel-heading">
			        		<h4 class="panel-title">
			          			<a data-toggle="collapse" data-parent="#accordion-0" href="#collapse1">Buscar sucursal</a>
			        		</h4>
			    		</div>
			    		<div id="collapse1" class="panel-collapse collapse in">
			        		<div class="panel-body">
								<c:url value = "/soporte/validaSucursal.htm" var = "validaSucursalUrl" />
								<form:form method="POST" class="form-inline" action="${validaSucursalUrl}" model="command" name="form0" id="form0">
									<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<form:select id="nomPais" name="nomPais" path="nomPais" class="form-control">
											<form:options items="${listaPaises}" />
										</form:select>
										<form:input type="text" class="form-control" id="numSucursal0" placeholder="Número de sucursal" path="idSucursal" onkeypress="return fieldFormat(event)" />
										<form:checkbox path="checkGCC" id="checkGCC" name="checkGCC" class="form-control" /> ¿Es GCC?
										<input type="submit" class="btn btn-default btn-lg" value="Obtener sucursal" onclick="return validateForm0();"/>
									</div>
								</form:form>
								<div id="map"></div>
			        		</div>
						</div>
					</div>
				</div>
			</div>
		</c:when>
		<c:when test="${paso=='1'}">
			<div class="container">
				<div class="panel-group" id="accordion-1">
					<div class="panel panel-default">
			    		<div class="panel-heading">
			        		<h4 class="panel-title">
			          			<a data-toggle="collapse" data-parent="#accordion-1" href="#collapse1-1">Buscar sucursal</a>
			        		</h4>
			    		</div>
			    		<div id="collapse1-1" class="panel-collapse collapse in">
			        		<div class="panel-body">
								<c:url value = "/soporte/validaSucursal.htm" var = "validaSucursalUrl" />
								<form:form method="POST" class="form-inline" action="${validaSucursalUrl}" model="form1" name="form1">
									<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
										 
										<form:select id="nomPais" name="nomPais" path="nomPais" class="form-control">
											<form:options items="${listaPaises}" />
										</form:select>
										
										<form:input class="form-control" id="numSucursal-1" value="${sucursal.nuSucursal}" placeholder="Número de sucursal" path="idSucursal" onkeypress="return fieldFormat(event)" />
											
											<c:choose>
												<c:when test="${auxGCC=='checked'}">
													<input type="checkbox" id="checkGCC" name="checkGCC" class="form-control" checked /> ¿Es GCC? 
												</c:when>
												<c:otherwise>
													<input type="checkbox" id="checkGCC" name="checkGCC" class="form-control" /> ¿Es GCC? 
												</c:otherwise>
											</c:choose>
										<input type="submit" class="btn btn-default btn-lg" value="Obtener sucursal" onclick="return validateForm1();"/>
									</div>
								</form:form>
			        		</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
				        	<h4 class="panel-title">
				       			<a data-toggle="collapse" data-parent="#accordion-1" href="#collapse1-2">Validar coordenadas</a>
				        	</h4>
				      	</div>
				      	<div id="collapse1-2" class="panel-collapse collapse in">
				        	<div class="panel-body">
				        		<div id="map"></div>
								  
								<script>
									function initMap() {
										var latitud1 = ${sucursal.latitud};
										var longitud1 = ${sucursal.longitud};
										var pos1 = new google.maps.LatLng(latitud1, longitud1);
	
										var mapOptions = {
											zoom: 17,
											center: pos1,
											mapTypeId: 'roadmap'
										};
	
										var map1 = new google.maps.Map(document.getElementById('map'), mapOptions);
	
										var marker1 = new google.maps.Marker({
											position: pos1,
											animation: google.maps.Animation.DROP,
									    });

										marker1.setMap(map1);
	
									    marker1.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
									}
								</script>
								
								<h3> Insertar nuevas coordenadas: </h3>

								<c:url value = "/soporte/validaCoordenadas.htm" var = "validaCoordenadas" />
								<form:form method="POST" class="form-inline" action="${validaCoordenadas}" model="command" name="form2" >
									<div class="form-group">
										<form:input class="form-control" id="coord1" path="latitud" placeholder="Latitud, Longitud"/>
										<input id="idPais" name="idPais" type="hidden" value="${sucursal.idPais}"/>
										<input id="idSucursal" name="idSucursal" type="hidden" value="${sucursal.nuSucursal}"/>
										<c:choose>
											<c:when test="${auxGCC=='checked'}">
												<input type="hidden" id="auxGCC" name="auxGCC" value="checked">
											</c:when>
										</c:choose>
										<input type="submit" class="btn btn-default btn-lg" value="Validar coordenadas" onclick="return validateForm2();" />
									</div>
								</form:form>

								<table class="table table-condensed">
									<tr>
										<td><label>Nombre de Sucursal: </label></td>
										<td><label><c:out value="${sucursal.nombresuc}" /></label></td>
									</tr>
									<tr>
										<td><label>Sucursal:</label></td>
										<td><label><c:out value="${sucursal.nuSucursal}" /></label>
										</td>
									</tr>
									<tr>	
										<td><label>País: </label></td>
										<td><label><c:out value="${sucursal.idPais}"/></label></td>
									</tr>
									<tr> 
										<td><label>Canal: </label></td>
										<td><label><c:out value="${sucursal.idCanal}" /></label></td>
									</tr>
									<tr> 
										<td><label>Ceco: </label></td>
										<td><label><c:out value="${sucursal.idCeco}" /></label></td>
									</tr>
									<tr> 
										<td><label>Latitud: </label></td>
										<td><label><c:out value="${sucursal.latitud}" /></label></td>
									</tr>
									<tr> 
										<td><label>Longitud: </label></td>
										<td><label><c:out value="${sucursal.longitud}" /></label></td>
									</tr>
									<tr> 
										<td colspan="2" ><h3>Historial de actualizaciones de coordenadas</h3></td>
									</tr>
									<tr> 
										<td><label>Fecha</label></td>
										<td><label>Latitud y Longitud</label></td>
									</tr>
									<c:forEach var="list" items="${listaHist}">
										<tr>
											<td><c:out value="${list.fecha}"></c:out></td>
											<td><c:out value="${list.latitud}"></c:out>, <c:out value="${list.longitud}"></c:out></td>
										</tr>
									</c:forEach>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</c:when>
		<c:when test="${paso=='2'}">
			<div class="container">
				<div class="panel-group" id="accordion-2">
			    	<div class="panel panel-default">
			    		<div class="panel-heading">
			        		<h4 class="panel-title">
			          			<a data-toggle="collapse" data-parent="#accordion-2" href="#collapse1-2">Buscar sucursal</a>
			        		</h4>
			    		</div>
			    		<div id="collapse1-2" class="panel-collapse collapse in">
			        		<div class="panel-body">
								<c:url value="/soporte/validaSucursal.htm" var="validaSucursalUrl" />
								<form:form method="POST" class="form-inline" action="${validaSucursalUrl}" model="command" name="form3" id="form3">
									<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
										  
										<form:select id="nomPais" name="nomPais" path="nomPais" class="form-control">
											<form:options items="${listaPaises}" />
										</form:select>
										
										<form:input class="form-control" id="idSucursal3" placeholder="Número de sucursal" value="${sucursal.nuSucursal}" path="idSucursal" onkeypress="return fieldFormat(event)" />
											<c:choose>
												<c:when test="${auxGCC=='checked'}">
													<input type="checkbox" id="checkGCC" name="checkGCC" class="form-control" onclick="return false;" checked /> ¿Es GCC?
												</c:when>
												<c:otherwise>
													<input type="checkbox" id="checkGCC" name="checkGCC" class="form-control" /> ¿Es GCC? 
												</c:otherwise>
											</c:choose>
										<input type="submit" class="btn btn-default btn-lg" value="Obtener sucursal" onclick="return validateForm3();"/>
									</div>
								</form:form>
			        		</div>
						</div>
			    	</div>
			    	<div class="panel panel-default">
						<div class="panel-heading">
			        		<h4 class="panel-title">
			          			<a data-toggle="collapse" data-parent="#accordion-2" href="#collapse3-2">Confirmar coordenadas</a>
			        		</h4>
			      		</div>
			      		<div id="collapse3-2" class="panel-collapse collapse in">
			        		<div class="panel-body">
			        		
			        			<div id="distance"></div>

								<div id="map"></div>

								<script>
									function initMap() {
										var latitud2 = ${sucursal.latitud};
								        var longitud2 = ${sucursal.longitud};
								        var latitud3 = ${nuevaLatitud};
								        var longitud3 = ${nuevaLongitud};

										var pos2 = new google.maps.LatLng(latitud2, longitud2);
										var pos3 = new google.maps.LatLng(latitud3, longitud3);
										
										var mapOptions2 = {
											zoom: 17,
											center: pos3,
											mapTypeId: 'roadmap'
										};
	
								        var map2 = new google.maps.Map(document.getElementById('map'), mapOptions2);
	
								        var marker2 = new google.maps.Marker({
								        	position: pos2,
								        	dragable: true,
								        	animation: google.maps.Animation.DROP,
								        	map: map2
								        });

								        var marker3 = new google.maps.Marker({
								        	position: pos3,
								        	dragable: true,
								        	animation: google.maps.Animation.DROP,
								        	map: map2
								        });
	
								        var poly2 = new google.maps.Polyline({
									    	path: [
									        	new google.maps.LatLng(latitud2, longitud2), 
									            new google.maps.LatLng(latitud3, longitud3)
									     	],
									        strokeColor: "#3387E8",
									        strokeOpacity: 0.85,
									        strokeWeight: 5,
									        map: map2
									 	});

								        var markers = [marker2, marker3];

								        var bounds = new google.maps.LatLngBounds();

								        for (var i = 0; i < markers.length; i++) {
								        	bounds.extend(markers[i].getPosition());
								        }
								        
								        map2.fitBounds(bounds);
										
								        var icon2 = "http://maps.google.com/mapfiles/ms/icons/green-dot.png";
								        var icon3 = "http://maps.google.com/mapfiles/ms/icons/blue-dot.png";
								        
								        marker2.setIcon(icon2);
								        marker3.setIcon(icon3);

										var service = new google.maps.DistanceMatrixService;

										service.getDistanceMatrix({
											origins: [pos2],
											destinations: [pos3],
											travelMode: 'WALKING',
											unitSystem: google.maps.UnitSystem.METRIC,
											avoidHighways: false,
											avoidTolls: false
										}, function(response, status) {
											if (status !== 'OK') {
												alert('Error was: ' + status);
											} else {
												var distance = "Distancia: " + response.rows[0]["elements"][0]["distance"]["text"];

												var infowindow = new google.maps.InfoWindow({
													content: distance
												});

												poly2.addListener('mouseover', function() {
													infowindow.open(map2, poly2);
												});

												marker2.addListener('mouseover', function() {
													infowindow.open(map2, marker2);
												});
												
												marker3.addListener('mouseover', function() {
													infowindow.open(map2, marker3);
												});
												
												poly2.addListener('click', function() {
													infowindow.open(map2, poly2);
												});

												marker2.addListener('click', function() {
													infowindow.open(map2, marker2);
												});
												
												marker3.addListener('click', function() {
													infowindow.open(map2, marker3);
												});
												
												$('#distance').append('<blockquote><p class="text-center"><strong>' + distance +'</strong></p></blockquote>');

											}
										});
									}
								</script>
								
								<br>

								<div class="container-fluid">
									<c:url value = "/soporte/confirmaCoordenadas.htm" var = "confirmaCoordenadas" />
									<form:form method="POST" action="${confirmaCoordenadas}" model ="command" name="form-actualizar">
										<input id="idSucursal" name="idSucursal" type="hidden" value="${sucursal.nuSucursal}"/>
										<input id="idPais" name="idPais" type="hidden" value="${sucursal.idPais}"/>
										<input id="nuevaLatitud" name="nuevaLatitud" type="hidden" value="${nuevaLatitud}"/>
										<input id="nuevaLongitud" name="nuevaLongitud" type="hidden" value="${nuevaLongitud}"/>
										<c:choose>
											<c:when test="${auxGCC=='checked'}">
												<input type="hidden" id="auxGCC" name="auxGCC" value="checked">
											</c:when>
										</c:choose>
										<input type="submit" class="btn btn-success btn-lg" value="Actualizar coordenadas" onclick="return getConfirmation()" />
									</form:form>

									<c:url value = "/soporte/validaSucursal.htm" var = "validaSucursalUrl" />
									<form:form method="GET" action="${validaSucursalUrl}" model="command" name="form-cancelar">
										<input type="submit" class="btn btn-danger btn-lg" value="Cancelar" onclick="return getCancel()"/>
									</form:form>
								</div>

								<br>
								
								<div class="container">
									<table class="table table-condensed">
										<tr> 
											<td><label>Sucursal: </label></td>
											<td><label><c:out value="${sucursal.idSucursal}" /></label></td>
										</tr>
										<tr> 
											<td><label>Nombre de Sucursal: </label></td>
											<td><label><c:out value="${sucursal.nombresuc}" /></label></td>
										</tr>
										<tr>
											<td><label>Número de Sucursal: </label></td>
											<td><label><c:out value="${sucursal.nuSucursal}" /></label></td>
										</tr>
										<tr> 
											<td><label>País: </label></td>
											<td><label><c:out value="${sucursal.idPais}"/></label></td>
										</tr>
										<tr> 
											<td><label>Canal: </label></td>
											<td><label><c:out value="${sucursal.idCanal}" /></label></td>
										</tr>
										<tr> 
											<td><label>Ceco: </label></td>
											<td><label><c:out value="${sucursal.idCeco}" /></label></td>
										</tr>
										<tr> 
											<td><label>Latitud: </label></td>
											<td><label><c:out value="${sucursal.latitud}" /></label></td>
										</tr>
										<tr> 
											<td><label>Longitud: </label></td>
											<td><label><c:out value="${sucursal.longitud}" /></label></td>
										</tr>
										<tr> 
											<td colspan="2" ><h3>Historial de actualizaciones de coordenadas</h3></td>
										</tr>
										<tr> 
											<td><label>Fecha</label></td>
											<td><label>Latitud y Longitud</label></td>
										</tr>
										<c:forEach var="list" items="${listaHist}">
											<tr>
												<td><c:out value="${list.fecha}"></c:out></td>
												<td><c:out value="${list.latitud}"></c:out>, <c:out value="${list.longitud}"></c:out></td>
											</tr>
										</c:forEach>
									</table>
									<input id="idSucursal" name="idSucursal" type="hidden" value="${sucursal.nuSucursal}"/>
									<input id="nuevaLatitud" name="nuevaLatitud" type="hidden" value="${nuevaLatitud}"/>
									<input id="nuevaLongitud" name="nuevaLongitud" type="hidden" value="${nuevaLongitud}"/>
								</div>
			        		</div>
			      		</div>
			    	</div>
			  </div>
			</div>
		</c:when>
		
		<c:when test="${paso=='3'}">
			<div class="container">
				<div class="alert alert-success" role="alert">
					<p class="text-center"> 
						<h4>Coordenadas actualizadas!</h4>
						<br>Presione el botón de regresar para buscar otra sucursal.
					<p>
					<br>
					<c:url value="/soporte/validaSucursal.htm" var="validaSucursalUrl" />
					<form:form method="GET" action="${validaSucursalUrl}" model="command" name="form-cancelar">
						<input type="submit" class="btn btn-success btn-lg" value="Regresar" />
					</form:form>
				</div>
			</div>
		</c:when>
	</c:choose>

	<script async defer 
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwiEzyjP4j-rSsoN9qDZvywvdgZP5nbA4&callback=initMap">
	</script>

</body>
</html>