package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ConsultaCecoCheckDTO;
import com.gruposalinas.checklist.domain.ConsultaCecoCheckDTO2;


public class ConsultaCecoCheckRowMapper2 implements RowMapper<ConsultaCecoCheckDTO2> {

	//SACA EL TOTAL DE CHECKLIST DE IN USUARIO
	public ConsultaCecoCheckDTO2 mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConsultaCecoCheckDTO2 consultaCecoCheckDTO2 = new ConsultaCecoCheckDTO2();
		
		consultaCecoCheckDTO2.setChecklist(rs.getInt("TOTAL_CHECKL"));
		consultaCecoCheckDTO2.setCeco(rs.getInt("FCID_CECO"));
		consultaCecoCheckDTO2.setIdUsuario(rs.getInt("FIID_USUARIO"));
		

		return consultaCecoCheckDTO2;
	}

}


