package com.gruposalinas.checklist.domain;

import java.io.Serializable;

public class UsuarioDTO implements Serializable {
	
	private static final long serialVersionUID = -3068816794967556264L;
	private String idUsuario;
	private String nombre;
	private String llaveMaestra;
	private String password;
	private boolean admin;

	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLlaveMaestra() {
		return llaveMaestra;
	}
	public void setLlaveMaestra(String llaveMaestra) {
		this.llaveMaestra = llaveMaestra;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
}


