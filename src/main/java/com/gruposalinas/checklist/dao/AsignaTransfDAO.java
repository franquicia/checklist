package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.AsignaTransfDTO;


public interface AsignaTransfDAO {

	  public int inserta(AsignaTransfDTO bean)throws Exception;
		
		public boolean elimina(String ceco,String usuario_asig)throws Exception;
		
		//carga cuando no existe en la tabla check_usua
		public boolean CargaAsig()throws Exception;
		
		public List<AsignaTransfDTO> obtieneDatos(String proyecto) throws Exception; 
		
		public boolean actualiza(AsignaTransfDTO bean)throws Exception;
		
		public boolean actualizaN(AsignaTransfDTO bean)throws Exception;
		
		public List<AsignaTransfDTO> obtieneDatosAsignaVer(String usuario ) throws Exception; 

		//Cursores
		public Map<String, Object>  obtieneDatosCursores(String usu) throws Exception; 
		
		//Cursores Ceco
		public Map<String, Object>  obtieneDatosCursoresCeco(String ceco) throws Exception; 
		
		//SERVICIOS MTTO
		  public int insertaAsignacionMtto(AsignaTransfDTO bean)throws Exception;
		
	}