package com.gruposalinas.checklist.servicios.servidor;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.gruposalinas.checklist.business.AdmPregZonasBI;
import com.gruposalinas.checklist.business.AdmTipoZonaBI;
import com.gruposalinas.checklist.business.AdmZonasBI;
import com.gruposalinas.checklist.business.AppBI;
import com.gruposalinas.checklist.business.ArbolDecisionBI;
import com.gruposalinas.checklist.business.ArbolDecisionTBI;
import com.gruposalinas.checklist.business.AsignacionBI;
import com.gruposalinas.checklist.business.AsignacionNewBI;
import com.gruposalinas.checklist.business.BitacoraBI;
import com.gruposalinas.checklist.business.ChecklistBI;
import com.gruposalinas.checklist.business.ChecklistPreguntaBI;
import com.gruposalinas.checklist.business.ChecklistPreguntaTBI;
import com.gruposalinas.checklist.business.ChecklistProtocoloBI;
import com.gruposalinas.checklist.business.ChecklistTBI;
import com.gruposalinas.checklist.business.ChecklistUsuarioBI;
import com.gruposalinas.checklist.business.ChecklistUsuarioGpoBI;
import com.gruposalinas.checklist.business.CompromisoBI;
import com.gruposalinas.checklist.business.EdoChecklistBI;
import com.gruposalinas.checklist.business.EvidenciaBI;
import com.gruposalinas.checklist.business.ImagenesRespuestaBI;
import com.gruposalinas.checklist.business.ModuloBI;
import com.gruposalinas.checklist.business.ModuloTBI;
import com.gruposalinas.checklist.business.PlantillaBI;
import com.gruposalinas.checklist.business.PosibleTipoPreguntaTBI;
import com.gruposalinas.checklist.business.PosiblesTBI;
import com.gruposalinas.checklist.business.PreguntaBI;
import com.gruposalinas.checklist.business.PreguntaSupBI;
import com.gruposalinas.checklist.business.PreguntaTBI;
import com.gruposalinas.checklist.business.RespuestaBI;
import com.gruposalinas.checklist.business.TipoChecklistBI;
import com.gruposalinas.checklist.business.TipoPreguntaBI;
import com.gruposalinas.checklist.domain.AdmPregZonaDTO;
import com.gruposalinas.checklist.domain.AdmTipoZonaDTO;
import com.gruposalinas.checklist.domain.AdminZonaDTO;
import com.gruposalinas.checklist.domain.AppPerfilDTO;
import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.domain.AsignacionDTO;
import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.domain.ChecklistUsuarioDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.domain.EdoChecklistDTO;
import com.gruposalinas.checklist.domain.EvidenciaDTO;
import com.gruposalinas.checklist.domain.ImagenesRespuestaDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.PlantillaDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.PreguntaSupDTO;
import com.gruposalinas.checklist.domain.RespuestaDTO;
import com.gruposalinas.checklist.domain.TipoChecklistDTO;
import com.gruposalinas.checklist.domain.TipoPreguntaDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.resources.FRQAuthInterceptor;

@Controller
@RequestMapping("/checklistServices")
public class AltaChecklistService {

	@Autowired
	ModuloBI modulobi;
	@Autowired
	TipoPreguntaBI tipPreguntabi;
	@Autowired
	PreguntaBI preguntabi;
	@Autowired
	TipoChecklistBI tipoChecklistbi;
	@Autowired
	ChecklistBI checklistbi;
	@Autowired
	ChecklistPreguntaBI checklistPreguntabi;
	@Autowired
	ChecklistUsuarioBI checklistUsuariobi;
	@Autowired
	EdoChecklistBI edoChecklistbi;
	@Autowired
	ArbolDecisionBI arbolDecisionbi;
	@Autowired
	BitacoraBI bitacorabi;
	@Autowired
	RespuestaBI respuestabi;
	@Autowired
	CompromisoBI compromisobi;
	@Autowired
	EvidenciaBI evidenciabi;
	// Temporales
	@Autowired
	ArbolDecisionTBI arbolDecisionTbi;
	@Autowired
	ChecklistTBI checkTbi;
	@Autowired
	ModuloTBI moduloTbi;
	@Autowired
	PosibleTipoPreguntaTBI posiblePreguntaTbi;
	@Autowired
	PreguntaTBI preguntaTbi;
	@Autowired
	ChecklistPreguntaTBI checklistPreguntaTbi;
	@Autowired
	PosiblesTBI posiblesTbi;
	@Autowired
	ImagenesRespuestaBI imagenesRespuestaBI;
	@Autowired
	AsignacionBI asignacionBI;
	@Autowired
	AsignacionNewBI asignacionNewBI;
	@Autowired
	AppBI appBI;
	@Autowired
	PlantillaBI plantillaBI;
	@Autowired
	ChecklistUsuarioGpoBI checkListUsuarioGpoBI;
	@Autowired
	ChecklistProtocoloBI checklistProtocoloBI;
	@Autowired
	PreguntaSupBI preguntaSupBI;
	@Autowired
	AdmZonasBI admZonasBI;
	@Autowired
	AdmTipoZonaBI admTipoZonaBI;
	@Autowired
	AdmPregZonasBI admPregZonasBI;

	private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);

	// http://localhost:8080/checklist/checklistServices/altaModulo.json?idModuloPadre=<?>&nombreMod=<?>
	@RequestMapping(value = "/altaModulo", method = RequestMethod.GET)
	public ModelAndView altaModulo(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idModuloPadre = request.getParameter("idModuloPadre");

			char[] ca = { '\u0061', '\u0301' };
			char[] ce = { '\u0065', '\u0301' };
			char[] ci = { '\u0069', '\u0301' };
			char[] co = { '\u006F', '\u0301' };
			char[] cu = { '\u0075', '\u0301' };
			// mayusculas
			char[] c1 = { '\u0041', '\u0301' };
			char[] c2 = { '\u0045', '\u0301' };
			char[] c3 = { '\u0049', '\u0301' };
			char[] c4 = { '\u004F', '\u0301' };
			char[] c5 = { '\u0055', '\u0301' };
			char[] c6 = { '\u006E', '\u0303' };
			String nombreModulo = request.getParameter("nombreMod");

			String nombreModuloCod = "";
			// System.out.println("setPregunta1 " + nombreModulo);
			if (nombreModulo.contains(String.valueOf(ca)) || nombreModulo.contains(String.valueOf(ce))
					|| nombreModulo.contains(String.valueOf(ci)) || nombreModulo.contains(String.valueOf(co))
					|| nombreModulo.contains(String.valueOf(cu)) || nombreModulo.contains(String.valueOf(c1))
					|| nombreModulo.contains(String.valueOf(c2)) || nombreModulo.contains(String.valueOf(c3))
					|| nombreModulo.contains(String.valueOf(c4)) || nombreModulo.contains(String.valueOf(c5))
					|| nombreModulo.contains(String.valueOf(c6))) {

				String rr = nombreModulo.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
						.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
						.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
						.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
						.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
						.replaceAll(String.valueOf(c6), "ñ");
				// System.out.println("rr " + rr);

				nombreModuloCod = rr;
				rr = null;
			} else {
				// System.out.println("setPregunta " + nombreModulo);

				nombreModuloCod = nombreModulo;

			}

			ModuloDTO modulo = new ModuloDTO();
			modulo.setIdModuloPadre(Integer.parseInt(idModuloPadre));
			modulo.setNombre(nombreModuloCod);
			int idMod = modulobi.insertaModulo(modulo);
			modulo.setIdModulo(idMod);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID DE MODULO CREADO");
			mv.addObject("res", idMod);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaModuloTemp.json?idModuloPadre=<?>&nombreMod=<?>&numVersion=<?>&tipoCambio=<?>
	@RequestMapping(value = "/altaModuloTemp", method = RequestMethod.GET)
	public ModelAndView altaModuloTemp(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			// System.out.println("ALTA MODULO TEMP");
			String idModuloPadre = request.getParameter("idModuloPadre");
			String nombreModulo = new String(request.getParameter("nombreMod").getBytes("ISO-8859-1"), "UTF-8");
			String numVersion = request.getParameter("numVersion");
			String tipoCambio = request.getParameter("tipoModif");

			ModuloDTO modulo = new ModuloDTO();
			modulo.setIdModuloPadre(Integer.parseInt(idModuloPadre));
			modulo.setNombre(nombreModulo);
			modulo.setNumVersion(numVersion);
			modulo.setTipoCambio(tipoCambio);
			int idMod = moduloTbi.insertaModulo(modulo);
			modulo.setIdModulo(idMod);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID DE MODULO CREADO");
			// mv.addObject("res", idMod);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaTipoPregunta.json?cveTipoPreg=<?>&desTipoPreg=<?>&setActivo=<?>
	@RequestMapping(value = "/altaTipoPregunta", method = RequestMethod.GET)
	public ModelAndView altaTipoPregunta(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String cveTipoPreg = new String(request.getParameter("cveTipoPreg").getBytes("ISO-8859-1"), "UTF-8");
			String desTipoPreg = new String(request.getParameter("desTipoPreg").getBytes("ISO-8859-1"), "UTF-8");
			String setActivo = request.getParameter("setActivo");

			TipoPreguntaDTO tipoPregunta = new TipoPreguntaDTO();
			tipoPregunta.setCvePregunta(cveTipoPreg);
			tipoPregunta.setDescripcion(desTipoPreg);
			tipoPregunta.setActivo(Integer.parseInt(setActivo));
			int idTP = tipPreguntabi.insertaTipoPregunta(tipoPregunta);
			tipoPregunta.setIdTipoPregunta(idTP);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID DE TIPO_PREGUNTA CREADA");
			mv.addObject("res", idTP);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaPregunta.json?idMod=<?>&idTipo=<?>&estatus=<?>&setPregunta=<?>&commit=<?>
	@RequestMapping(value = "/altaPregunta", method = RequestMethod.GET)
	public ModelAndView altaPregunta(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idMod = request.getParameter("idMod");
			String idTipo = request.getParameter("idTipo");
			String estatus = request.getParameter("estatus");
			String setPregunta = new String(request.getParameter("setPregunta").getBytes("ISO-8859-1"), "UTF-8");
			String commit = request.getParameter("commit");

			PreguntaDTO pregunta = new PreguntaDTO();
			pregunta.setIdModulo(Integer.parseInt(idMod));
			pregunta.setIdTipo(Integer.parseInt(idTipo));
			pregunta.setEstatus(Integer.parseInt(estatus));
			pregunta.setPregunta(setPregunta);
			pregunta.setCommit(Integer.parseInt(commit));
			int idPreg = this.preguntabi.insertaPreguntas(pregunta);
			pregunta.setIdPregunta(idPreg);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID DE PREGUNTA CREADA");
			mv.addObject("res", idPreg);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaPregunta.json?idMod=<?>&idTipo=<?>&estatus=<?>&setPregunta=<?>&commit=<?>
	@RequestMapping(value = "/altaPreguntaService", method = RequestMethod.GET)
	public @ResponseBody int altaPreguntaService(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idMod = request.getParameter("idMod");
			String idTipo = request.getParameter("idTipo");
			String estatus = request.getParameter("estatus");
			String setPregunta = new String(request.getParameter("setPregunta").getBytes("ISO-8859-1"), "UTF-8");
			String commit = request.getParameter("commit");

			PreguntaDTO pregunta = new PreguntaDTO();
			pregunta.setIdModulo(Integer.parseInt(idMod));
			pregunta.setIdTipo(Integer.parseInt(idTipo));
			pregunta.setEstatus(Integer.parseInt(estatus));
			pregunta.setPregunta(setPregunta);
			pregunta.setCommit(Integer.parseInt(commit));
			int idPreg = this.preguntabi.insertaPreguntas(pregunta);
			pregunta.setIdPregunta(idPreg);
			logger.info("PREGUNTA: " + setPregunta);
			logger.info("PREGUNTA REQUEST: " + request.getParameter("setPregunta"));

			return idPreg;
		} catch (Exception e) {
			logger.info(e);
			return 0;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaPreguntaServiceTemp.json?idMod=<?>&idTipo=<?>&estatus=<?>&setPregunta=<?>&commit=<?>&numVersion=<?>&tipoCambio<?>
	@RequestMapping(value = "/altaPreguntaServiceTemp", method = RequestMethod.GET)
	public @ResponseBody int altaPreguntaServiceTemp(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idMod = request.getParameter("idMod");
			String idTipo = request.getParameter("idTipo");
			String estatus = request.getParameter("estatus");
			String setPregunta = new String(request.getParameter("setPregunta").getBytes("ISO-8859-1"), "UTF-8");
			String commit = request.getParameter("commit");
			String numVersion = request.getParameter("numRevision");
			String tipoCambio = request.getParameter("tipoCambio");
			String idPregunta = request.getParameter("idPregunta");

			PreguntaDTO pregunta = new PreguntaDTO();
			pregunta.setIdModulo(Integer.parseInt(idMod));
			pregunta.setIdTipo(Integer.parseInt(idTipo));
			pregunta.setEstatus(Integer.parseInt(estatus));
			pregunta.setPregunta(setPregunta);
			pregunta.setCommit(Integer.parseInt(commit));
			pregunta.setNumVersion(numVersion);
			pregunta.setTipoCambio(tipoCambio);
			pregunta.setIdPregunta(Integer.parseInt(idPregunta));
			int idPreg = this.preguntaTbi.insertaPreguntaTemp(pregunta);
			pregunta.setIdPregunta(idPreg);
			logger.info("PREGUNTA: " + setPregunta);
			logger.info("PREGUNTA REQUEST: " + request.getParameter("setPregunta"));

			return idPreg;
		} catch (Exception e) {
			logger.info(e);
			return 0;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaTipoCheck.json?descTipoCheck=<?>
	@RequestMapping(value = "/altaTipoCheck", method = RequestMethod.GET)
	public ModelAndView altaTipoChecklist(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String descTipoCheck = new String(request.getParameter("descTipoCheck").getBytes("ISO-8859-1"), "UTF-8");

			TipoChecklistDTO tipoChecklist = new TipoChecklistDTO();
			tipoChecklist.setDescTipo(descTipoCheck);
			int tChecklist = tipoChecklistbi.insertaTipoChecklist(tipoChecklist);
			tipoChecklist.setIdTipoCheck(tChecklist);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID DE DE TIPO CHECKLIST CREADO");
			mv.addObject("res", tChecklist);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}

	}

	// http://localhost:8080/checklist/checklistServices/estadoChecklist.json?descripcion=<?>
	@RequestMapping(value = "/estadoChecklist", method = RequestMethod.GET)
	public ModelAndView estadoChecklist(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");

			EdoChecklistDTO edoCheck = new EdoChecklistDTO();
			edoCheck.setDescripcion(descripcion);
			int idEdoCheck = edoChecklistbi.insertaEdoChecklist(edoCheck);
			edoCheck.setIdEdochecklist(idEdoCheck);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID ESTADO CHECKLIST CREADO--->");
			mv.addObject("res", idEdoCheck);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaCheck.json?fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>&commit=<?>
	// FORMATO FECHA INI Y FECHA FIN (AAAAMMDD)
	@RequestMapping(value = "/altaCheck", method = RequestMethod.GET)
	public ModelAndView altaCheck(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String fechaIni = request.getParameter("fechaIni");
			String fechaFin = request.getParameter("fechaFin");
			String idEstado = request.getParameter("idEstado");
			String idHorario = request.getParameter("idHorario");
			String idTipoCheck = request.getParameter("idTipoCheck");
			String nombreCheck = new String(request.getParameter("nombreCheck").getBytes("ISO-8859-1"), "UTF-8");
			String setVigente = request.getParameter("setVigente");
			String commit = request.getParameter("commit");

			ChecklistDTO checklist = new ChecklistDTO();
			checklist.setFecha_inicio(fechaIni);
			checklist.setFecha_fin(fechaFin);
			checklist.setIdEstado(Integer.parseInt(idEstado));
			checklist.setIdHorario(Integer.parseInt(idHorario));
			checklist.setCommit(Integer.parseInt(commit));
			TipoChecklistDTO chk = new TipoChecklistDTO();
			chk.setIdTipoCheck(Integer.parseInt(idTipoCheck));
			checklist.setIdTipoChecklist(chk);
			checklist.setNombreCheck(nombreCheck);
			checklist.setVigente(Integer.parseInt(setVigente));
			// Obtengo el ID del checklist
			int idChecklist = checklistbi.insertaChecklist(checklist);
			checklist.setIdChecklist(idChecklist);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID DE CHECKLIST");
			mv.addObject("res", idChecklist);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}

	}

	// http://localhost:8080/checklist/checklistServices/altaCheckService.json?fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>&commit=<?>&idUsuario=<?>&periodicidad=<?>&dia=<?>&version=<?>&ordenGrupo=<?>
	// FORMATO FECHA INI Y FECHA FIN (AAAAMMDD)
	@RequestMapping(value = "/altaCheckService", method = RequestMethod.GET)
	public @ResponseBody ChecklistDTO altaCheckService(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String fechaIni = request.getParameter("fechaIni");
			String fechaFin = request.getParameter("fechaFin");
			String idEstado = request.getParameter("idEstado");
			String idHorario = request.getParameter("idHorario");
			String idTipoCheck = request.getParameter("idTipoCheck");
			String nombreCheck = new String(request.getParameter("nombreCheck").getBytes("UTF-8"), "ISO-8859-1");
			String setVigente = request.getParameter("setVigente");
			String commit = request.getParameter("commit");
			String idUsuario = request.getParameter("idUsuario");
			String periodicidad = request.getParameter("periodicidad");
			String version = request.getParameter("version");
			String ordenGrupo = request.getParameter("ordenGrupo");
			String dia = request.getParameter("dia");

			UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
			idUsuario = userSession.getIdUsuario();

			ChecklistDTO checklist = new ChecklistDTO();
			checklist.setFecha_inicio(fechaIni);
			checklist.setFecha_fin(fechaFin);
			checklist.setIdEstado(Integer.parseInt(idEstado));
			checklist.setIdHorario(Integer.parseInt(idHorario));
			checklist.setCommit(Integer.parseInt(commit));
			TipoChecklistDTO chk = new TipoChecklistDTO();
			chk.setIdTipoCheck(Integer.parseInt(idTipoCheck));
			checklist.setIdTipoChecklist(chk);
			checklist.setNombreCheck(nombreCheck);
			checklist.setVigente(Integer.parseInt(setVigente));
			// SE AGREGA USUARIO RESPONSABLE, DIA Y PERIDO
			checklist.setIdUsuario(Integer.parseInt(idUsuario));
			checklist.setPeriodicidad(periodicidad);
			checklist.setVersion(version);
			checklist.setOrdeGrupo(ordenGrupo);
			checklist.setDia(dia);
			// Obtengo el ID del checklist
			int idChecklist = checklistbi.insertaChecklist(checklist);
			checklist.setIdChecklist(idChecklist);

			return checklist;

		} catch (Exception e) {
			logger.info(e);
			return null;
		}

	}

	// http://localhost:8080/checklist/checklistServices/altaCheckTemp.json?fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>&commit=<?>&idCheck=<?>&numVersion=<?>&tipoCambio=<?>
	// FORMATO FECHA INI Y FECHA FIN (AAAAMMDD)
	@RequestMapping(value = "/altaCheckServiceTemp", method = RequestMethod.GET)
	public @ResponseBody ChecklistProtocoloDTO altaCheckServiceTemp(HttpServletRequest request,
			HttpServletResponse response) throws UnsupportedEncodingException {
		try {
			String fechaIni = request.getParameter("fechaIni");
			String fechaFin = request.getParameter("fechaFin");
			String idEstado = request.getParameter("idEstado");
			String idHorario = request.getParameter("idHorario");
			String idTipoCheck = request.getParameter("idTipoCheck");
			String nombreCheck = new String(request.getParameter("nombreCheck").getBytes("ISO-8859-1"), "UTF-8");
			String setVigente = request.getParameter("setVigente");
			String commit = request.getParameter("commit");
			String idCheck = request.getParameter("idCheck");
			String numVersion = request.getParameter("numVersion");
			String tipoCambio = request.getParameter("tipoCambio");

			ChecklistProtocoloDTO checklist = new ChecklistProtocoloDTO();
			checklist.setFecha_inicio(fechaIni);
			checklist.setFecha_fin(fechaFin);
			checklist.setIdEstado(Integer.parseInt(idEstado));
			checklist.setIdHorario(Integer.parseInt(idHorario));
			checklist.setCommit(Integer.parseInt(commit));
			TipoChecklistDTO chk = new TipoChecklistDTO();
			chk.setIdTipoCheck(Integer.parseInt(idTipoCheck));
			// checklist.setIdTipoChecklist(chk);
			checklist.setIdTipoChecklist(Integer.parseInt(idTipoCheck));
			checklist.setNombreCheck(nombreCheck);
			checklist.setVigente(Integer.parseInt(setVigente));
			// Obtengo el ID del checklist
			checklist.setIdChecklist(Integer.parseInt(idCheck));

			// checklist.setNumVersion(numVersion);
			checklist.setVersion("1");
			// checklist.setTipoCambio(tipoCambio);
			// checklist.
			int idChecklist = checkTbi.insertaChecklist(checklist);
			checklist.setIdChecklist(idChecklist);
			// System.out.println("NUM VERSION" + checklist.getNumVersion());
			return checklist;

		} catch (Exception e) {
			logger.info(e);
			return null;
		}

	}

	// http://localhost:8080/checklist/checklistServices/altaPregCheck.json?idCheck=<?>&ordenPreg=<?>&idPreg=<?>&commit=<?>&pregPadre=<?>
	@RequestMapping(value = "/altaPregCheck", method = RequestMethod.GET)
	public ModelAndView altaPreguntaCheck(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idCheck = request.getParameter("idCheck");
			String ordenPreg = request.getParameter("ordenPreg");
			String idPreg = request.getParameter("idPreg");
			String idPadre = request.getParameter("pregPadre");
			String commit = request.getParameter("commit");

			ChecklistPreguntaDTO pregDTO = new ChecklistPreguntaDTO();
			pregDTO.setIdChecklist(Integer.parseInt(idCheck));
			pregDTO.setOrdenPregunta(Integer.parseInt(ordenPreg));
			pregDTO.setIdPregunta(Integer.parseInt(idPreg));
			pregDTO.setCommit(Integer.parseInt(commit));
			pregDTO.setPregPadre(Integer.parseInt(idPadre));
			boolean checklistPreg = checklistPreguntabi.insertaChecklistPregunta(pregDTO);

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "PREGUNTA CREADA CON EXITO--->");
			mv.addObject("res", checklistPreg);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaPregCheck.json?idCheck=<?>&ordenPreg=<?>&idPreg=<?>&commit=<?>&pregPadre=<?>
	@RequestMapping(value = "/altaPregCheckService", method = RequestMethod.GET)
	public @ResponseBody boolean altaPreguntaCheckService(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idCheck = request.getParameter("idCheck");
			String ordenPreg = request.getParameter("ordenPreg");
			String idPreg = request.getParameter("idPreg");
			String idPadre = request.getParameter("pregPadre");
			String commit = request.getParameter("commit");

			ChecklistPreguntaDTO pregDTO = new ChecklistPreguntaDTO();
			pregDTO.setIdChecklist(Integer.parseInt(idCheck));
			pregDTO.setOrdenPregunta(Integer.parseInt(ordenPreg));
			pregDTO.setIdPregunta(Integer.parseInt(idPreg));
			pregDTO.setCommit(Integer.parseInt(commit));
			pregDTO.setPregPadre(Integer.parseInt(idPadre));
			boolean checklistPreg = checklistPreguntabi.insertaChecklistPregunta(pregDTO);

			return checklistPreg;
		} catch (Exception e) {
			logger.info(e);
			return false;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaPregCheckTempService.json?idCheck=<?>&ordenPreg=<?>&idPreg=<?>&commit=<?>&pregPadre=<?>&numRevision=<?>&tipoCambio=<?>
	@RequestMapping(value = "/altaPregCheckTempService", method = RequestMethod.GET)
	public @ResponseBody boolean altaPreguntaCheckTempService(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String idCheck = request.getParameter("idCheck");
			String ordenPreg = request.getParameter("ordenPreg");
			String idPreg = request.getParameter("idPreg");
			String idPadre = request.getParameter("pregPadre");
			String commit = request.getParameter("commit");
			String numRevision = request.getParameter("numRevision");
			String tipoCambio = request.getParameter("tipoCambio");

			ChecklistPreguntaDTO pregDTO = new ChecklistPreguntaDTO();
			pregDTO.setIdChecklist(Integer.parseInt(idCheck));
			pregDTO.setOrdenPregunta(Integer.parseInt(ordenPreg));
			pregDTO.setIdPregunta(Integer.parseInt(idPreg));
			pregDTO.setCommit(Integer.parseInt(commit));
			pregDTO.setPregPadre(Integer.parseInt(idPadre));
			pregDTO.setNumVersion(numRevision);
			pregDTO.setTipoCambio(tipoCambio);

			boolean checklistPreg = checklistPreguntaTbi.insertaChecklistPreguntaTemp(pregDTO);
			// System.out.println("retorno true -->" + checklistPreg );

			return checklistPreg;
		} catch (Exception e) {
			logger.info(e);
			// System.out.println("IMPRIME LOG -->");
			return false;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaUsuCheck.json?setActivo=<?>&fechaIni=<?>&fechaResp=<?>&idCeco=<?>&idChecklist=<?>&idUsu=<?>
	@RequestMapping(value = "/altaUsuCheck", method = RequestMethod.GET)
	public ModelAndView altaUsuarioCheck(HttpServletRequest request, HttpServletResponse response) {
		try {
			String setActivo = request.getParameter("setActivo");
			String fechaIni = request.getParameter("fechaIni");
			String fechaResp = request.getParameter("fechaResp");
			String idCeco = request.getParameter("idCeco");
			String idChecklist = request.getParameter("idChecklist");
			String idUsu = request.getParameter("idUsu");

			ChecklistUsuarioDTO checkUsu = new ChecklistUsuarioDTO();
			checkUsu.setActivo(Integer.parseInt(setActivo));
			checkUsu.setFechaIni(fechaIni);
			checkUsu.setFechaResp(fechaResp);
			checkUsu.setIdCeco(Integer.parseInt(idCeco));
			checkUsu.setIdChecklist(Integer.parseInt(idChecklist));
			checkUsu.setIdUsuario(Integer.parseInt(idUsu));
			int idChecklistUsu = checklistUsuariobi.insertaCheckUsuario(checkUsu);
			checkUsu.setIdChecklist(idChecklistUsu);

			// ModelAndView mv = new ModelAndView("altaModuloRes");
			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
			mv.addObject("tipo", "ID CHECKLIST USUARIO CREADO--->");
			mv.addObject("res", idChecklistUsu);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaUsuCheckEsp.json?idChecklist=<?>&idCeco=<?>&idUsu=<?>
	@RequestMapping(value = "/altaUsuCheckEsp", method = RequestMethod.GET)
	public ModelAndView altaUsuCheckEsp(HttpServletRequest request, HttpServletResponse response) {
		try {

			String idCeco = request.getParameter("idCeco");
			String idChecklist = request.getParameter("idChecklist");
			String idUsu = request.getParameter("idUsu");

			boolean repuesta = checklistUsuariobi.insertaCheckusuaEspecial(Integer.parseInt(idUsu),
					Integer.parseInt(idChecklist), idCeco);

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "ID CHECKLIST-USUARIO INSERTADO -->  ");
			mv.addObject("res", repuesta);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaUsuCheckEsp2.json?idChecklist=<?>&idCeco=<?>&idUsu=<?>
	@RequestMapping(value = "/altaUsuCheckEsp2", method = RequestMethod.GET)
	public @ResponseBody boolean altaUsuCheckEsp2(HttpServletRequest request, HttpServletResponse response) {
		try {

			String idCeco = request.getParameter("idCeco");
			String idChecklist = request.getParameter("idChecklist");
			String idUsu = request.getParameter("idUsu");

			boolean repuesta = checklistUsuariobi.insertaCheckusuaEspecial(Integer.parseInt(idUsu),
					Integer.parseInt(idChecklist), idCeco);

			return repuesta;

		} catch (Exception e) {
			logger.info(e);
			return false;
		}
	}

	// http://localhost:8080/checklist/checklistServices/arbolDesicion.json?estatusEvidencia=<?>&idCheck=<?>&idPreg=<?>&idOrdenCheckRes=<?>&setRes=<?>&reqAccion=<?>&commit=<?>&reqObs=<?>&reqOblig=<?>&etiquetaEvidencia=<?>&ponderacion=<?>&idPlantilla=<?>&idProtocolo=<?>

	@RequestMapping(value = "/arbolDesicion", method = RequestMethod.GET)
	public ModelAndView arbolDecision(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String estatusEvidencia = request.getParameter("estatusEvidencia");
			String idCheck = request.getParameter("idCheck");
			String idPreg = request.getParameter("idPreg");
			String idOrdenCheckRes = request.getParameter("idOrdenCheckRes");
			String setRes = new String(request.getParameter("setRes").getBytes("ISO-8859-1"), "UTF-8");
			String reqAccion = request.getParameter("reqAccion");
			String commit = request.getParameter("commit");
			String reqObs = request.getParameter("reqObs");
			String reqOblig = request.getParameter("reqOblig");
			String etiquetaEvi = request.getParameter("etiquetaEvidencia");
			String ponderacion = request.getParameter("ponderacion");
			String idPlantilla = request.getParameter("idPlantilla");
			String idProtocolo = request.getParameter("idProtocolo");
			// System.out.println(ponderacion);
			int idProtocoloI = 0;
			if (idProtocolo != null) {
				idProtocoloI = Integer.parseInt(idProtocolo);
			}

			ArbolDecisionDTO arbol = new ArbolDecisionDTO();
			arbol.setEstatusEvidencia(Integer.parseInt(estatusEvidencia));
			arbol.setIdCheckList(Integer.parseInt(idCheck));
			arbol.setIdPregunta(Integer.parseInt(idPreg));
			arbol.setOrdenCheckRespuesta(Integer.parseInt(idOrdenCheckRes));
			arbol.setRespuesta(setRes);
			arbol.setReqAccion(Integer.parseInt(reqAccion));
			arbol.setCommit(Integer.parseInt(commit));
			arbol.setReqObservacion(Integer.parseInt(reqObs));
			arbol.setReqOblig(Integer.parseInt(reqOblig));
			arbol.setDescEvidencia(etiquetaEvi);
			arbol.setPonderacion(ponderacion);
			arbol.setIdPlantilla(Integer.parseInt(idPlantilla));
			arbol.setIdProtocolo(idProtocoloI);

			// System.out.println(arbol.getPonderacion());

			int resArbol = arbolDecisionbi.insertaArbolDecision(arbol);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID ARBOL  CREADO --->");
			mv.addObject("res", resArbol);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/arbolDesicion.json?estatusEvidencia=<?>&idCheck=<?>&idPreg=<?>&idOrdenCheckRes=<?>&setRes=<?>&reqAccion=<?>&commit=<?>&reqObs=<?>&reqOblig=<?>&etiquetaEvidencia=<?>
	@RequestMapping(value = "/arbolDesicionService", method = RequestMethod.GET)
	public @ResponseBody int arbolDecisionService(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String estatusEvidencia = request.getParameter("estatusEvidencia");
			String idCheck = request.getParameter("idCheck");
			String idPreg = request.getParameter("idPreg");
			String idOrdenCheckRes = request.getParameter("idOrdenCheckRes");
			String setRes = new String(request.getParameter("setRes").getBytes("ISO-8859-1"), "UTF-8");
			String reqAccion = request.getParameter("reqAccion");
			String commit = request.getParameter("commit");
			String reqObs = request.getParameter("reqObs");
			String reqOblig = request.getParameter("reqOblig");
			String etiquetaEvi = request.getParameter("etiquetaEvidencia");

			ArbolDecisionDTO arbol = new ArbolDecisionDTO();
			arbol.setEstatusEvidencia(Integer.parseInt(estatusEvidencia));
			arbol.setIdCheckList(Integer.parseInt(idCheck));
			arbol.setIdPregunta(Integer.parseInt(idPreg));
			arbol.setOrdenCheckRespuesta(Integer.parseInt(idOrdenCheckRes));
			arbol.setRespuesta(setRes);
			arbol.setReqAccion(Integer.parseInt(reqAccion));
			arbol.setCommit(Integer.parseInt(commit));
			arbol.setReqObservacion(Integer.parseInt(reqObs));
			arbol.setReqOblig(Integer.parseInt(reqOblig));
			arbol.setDescEvidencia(etiquetaEvi);

			int resArbol = arbolDecisionbi.insertaArbolDecision(arbol);

			return resArbol;
		} catch (Exception e) {
			logger.info(e);
			return 0;
		}
	}

	// http://localhost:8080/checklist/checklistServices/arbolDesicion.json?idArbol=<?>&estatusEvidencia=<?>&idCheck=<?>&idPreg=<?>&idOrdenCheckRes=<?>&setRes=<?>&reqAccion=<?>&commit=<?>&reqObs=<?>&reqOblig=<?>&etiquetaEvidencia=<?>&numRevision=<?>&tipoCambio=<?>
	@RequestMapping(value = "/arbolDesicionTempService", method = RequestMethod.GET)
	public @ResponseBody int arbolDecisionTempService(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String estatusEvidencia = request.getParameter("estatusEvidencia");
			String idCheck = request.getParameter("idCheck");
			String idPreg = request.getParameter("idPreg");
			String idOrdenCheckRes = request.getParameter("idOrdenCheckRes");
			String setRes = new String(request.getParameter("setRes").getBytes("ISO-8859-1"), "UTF-8");
			String reqAccion = request.getParameter("reqAccion");
			String commit = request.getParameter("commit");
			String reqObs = request.getParameter("reqObs");
			String reqOblig = request.getParameter("reqOblig");
			String etiquetaEvi = request.getParameter("etiquetaEvidencia");
			String numRevision = request.getParameter("numRevision");
			String tipoCambio = request.getParameter("tipoCambio");
			String idArbol = request.getParameter("idArbol");

			ArbolDecisionDTO arbol = new ArbolDecisionDTO();
			arbol.setEstatusEvidencia(Integer.parseInt(estatusEvidencia));
			arbol.setIdCheckList(Integer.parseInt(idCheck));
			arbol.setIdPregunta(Integer.parseInt(idPreg));
			arbol.setOrdenCheckRespuesta(Integer.parseInt(idOrdenCheckRes));
			arbol.setRespuesta(setRes);
			arbol.setReqAccion(Integer.parseInt(reqAccion));
			arbol.setCommit(Integer.parseInt(commit));
			arbol.setReqObservacion(Integer.parseInt(reqObs));
			arbol.setReqOblig(Integer.parseInt(reqOblig));
			arbol.setDescEvidencia(etiquetaEvi);
			arbol.setNumVersion(numRevision);
			arbol.setTipoCambio(tipoCambio);
			arbol.setIdArbolDesicion(Integer.parseInt(idArbol));
			int resArbol = arbolDecisionTbi.insertaArbolDecisionTemp(arbol);
			arbol.setIdArbolDesicion(resArbol);
			return resArbol;
			// return resArbol;
		} catch (Exception e) {
			logger.info(e);
			return 0;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaBitacora.json?fechaIni=<?>&fechaFin=<?>&idCheckUsua=<?>&latitud=<?>&longitud=<?>&modo=<?>
	@RequestMapping(value = "/altaBitacora", method = RequestMethod.GET)
	public ModelAndView altaBitacora(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String fechaInicio = request.getParameter("fechaIni");
			String fechaFin = request.getParameter("fechaFin");
			String idCheckUsua = request.getParameter("idCheckUsua");
			String latitud = request.getParameter("latitud");
			String longitud = request.getParameter("longitud");
			String modo = request.getParameter("modo");

			BitacoraDTO bitacora = new BitacoraDTO();
			bitacora.setFechaInicio(fechaInicio);
			bitacora.setFechaFin(fechaFin);
			bitacora.setIdCheckUsua(Integer.parseInt(idCheckUsua));
			bitacora.setLatitud(latitud);
			bitacora.setLongitud(longitud);
			bitacora.setModo(Integer.parseInt(modo));
			int idBitacora = bitacorabi.insertaBitacora(bitacora);
			bitacora.setIdBitacora(idBitacora);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID BITACORA CREADA-->");
			mv.addObject("res", idBitacora);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaRespuesta.json?idArbolDesicion=<?>&idBitacora=<?>&commit=<?>&observaciones=<?>
	@RequestMapping(value = "/altaRespuesta", method = RequestMethod.GET)
	public ModelAndView altaRespuesta(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idArbolDesicion = request.getParameter("idArbolDesicion");
			String idBitacora = request.getParameter("idBitacora");
			String commit = request.getParameter("commit");
			String observaciones = new String(request.getParameter("observaciones").getBytes("ISO-8859-1"), "UTF-8");

			RespuestaDTO res = new RespuestaDTO();

			res.setIdArboldecision(Integer.parseInt(idArbolDesicion));
			res.setIdBitacora(Integer.parseInt(idBitacora));
			res.setCommit(Integer.parseInt(commit));
			res.setObservacion(observaciones);
			int idRes = respuestabi.insertaRespuesta(res);
			res.setIdRespuesta(idRes);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID RESPUESTA CREADA-->");
			mv.addObject("res", idRes);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaCompromiso.json?commit=<?>&descripcion=<?>&estatus=<?>&fechaCompromiso=<?>&idRespuesta=<?>&idResponsable=<?>
	@RequestMapping(value = "/altaCompromiso", method = RequestMethod.GET)
	public ModelAndView altaCompromiso(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String commit = request.getParameter("commit");
			String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
			String estatus = request.getParameter("estatus");
			String fechaCompromiso = request.getParameter("fechaCompromiso");
			String idRespuesta = request.getParameter("idRespuesta");
			String idResponsable = request.getParameter("idResponsable");

			CompromisoDTO compr = new CompromisoDTO();
			compr.setCommit(Integer.parseInt(commit));
			compr.setDescripcion(descripcion);
			compr.setEstatus(Integer.parseInt(estatus));
			compr.setFechaCompromiso(fechaCompromiso);
			compr.setIdRespuesta(Integer.parseInt(idRespuesta));
			compr.setIdResponsable(Integer.parseInt(idResponsable));
			int comp = compromisobi.insertaCompromiso(compr);
			compr.setIdCompromiso(comp);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID COMPROMISO CREADO-->");
			mv.addObject("res", comp);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaEvidencia.json?commit=<?>&idRespuesta=<?>&idTipo=<?>&ruta=<?>&idElementoP=<?>
	@RequestMapping(value = "/altaEvidencia", method = RequestMethod.GET)
	public ModelAndView altaEvidencia(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String commit = request.getParameter("commit");
			String idRespuesta = request.getParameter("idRespuesta");
			String idTipo = request.getParameter("idTipo");
			String ruta = request.getParameter("ruta");
			String idElementoP = request.getParameter("idElementoP");

			EvidenciaDTO evidencia = new EvidenciaDTO();
			evidencia.setCommit(Integer.parseInt(commit));
			evidencia.setIdRespuesta(Integer.parseInt(idRespuesta));
			evidencia.setIdTipo(Integer.parseInt(idTipo));
			evidencia.setRuta(ruta);
			evidencia.setIdPlantilla(Integer.parseInt(idElementoP));
			int idEvi = evidenciabi.insertaEvidencia(evidencia);
			evidencia.setIdEvidencia(idEvi);
			;

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID EVIDENCIA CREADA-->");
			mv.addObject("res", idEvi);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/asignaChecklist.json?ceco=<?>&puesto=<?>&idCheck=<?>
	@RequestMapping(value = "/asignaChecklist", method = RequestMethod.GET)
	public ModelAndView asignaCheclist(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String ceco = request.getParameter("ceco");
			String puesto = request.getParameter("puesto");
			String idCheck = request.getParameter("idCheck");

			boolean res = checklistbi.asignaChecklist(ceco, Integer.parseInt(puesto), Integer.parseInt(idCheck));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "CHECKLIST ASIGNADO CORRECTAMENTE-->");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/asignaChecklistService.json?ceco=<?>&puesto=<?>&idCheck=<?>
	@RequestMapping(value = "/asignaChecklistService", method = RequestMethod.GET)
	public @ResponseBody boolean asignaCheclistService(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String ceco = request.getParameter("ceco");
			String puesto = request.getParameter("puesto");
			String idCheck = request.getParameter("idCheck");

			boolean res = checklistbi.asignaChecklist(ceco, Integer.parseInt(puesto), Integer.parseInt(idCheck));

			return res;
		} catch (Exception e) {
			logger.info(e);
			return false;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaUsuCheck.json?setActivo=<?>&fechaIni=<?>&fechaResp=<?>&idCeco=<?>&idChecklist=<?>&idUsu=<?>
	@RequestMapping(value = "/altaUsuCheckService", method = RequestMethod.GET)
	public @ResponseBody int altaUsuarioCheckService(HttpServletRequest request, HttpServletResponse response) {
		try {
			String setActivo = request.getParameter("setActivo");
			String fechaIni = request.getParameter("fechaIni");
			String fechaResp = request.getParameter("fechaResp");
			String idCeco = request.getParameter("idCeco");
			String idChecklist = request.getParameter("idChecklist");
			String idUsu = request.getParameter("idUsu");

			ChecklistUsuarioDTO checkUsu = new ChecklistUsuarioDTO();
			checkUsu.setActivo(Integer.parseInt(setActivo));
			checkUsu.setFechaIni(fechaIni);
			checkUsu.setFechaResp(fechaResp);
			checkUsu.setIdCeco(Integer.parseInt(idCeco));
			checkUsu.setIdChecklist(Integer.parseInt(idChecklist));
			checkUsu.setIdUsuario(Integer.parseInt(idUsu));
			int idChecklistUsu = checklistUsuariobi.insertaCheckUsuario(checkUsu);
			checkUsu.setIdChecklist(idChecklistUsu);

			return idChecklistUsu;
		} catch (Exception e) {
			logger.info(e);
			return 0;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaImagenesArbol.json?idArbol=<?>&urlImg=<?>&descripcion=<?>
	@RequestMapping(value = "/altaImagenesArbol", method = RequestMethod.GET)
	public ModelAndView altaImagenesArbol(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idArbol = request.getParameter("idArbol");
			String urlImg = request.getParameter("urlImg");
			String descripcion = request.getParameter("descripcion");

			ImagenesRespuestaDTO imagen = new ImagenesRespuestaDTO();

			imagen.setIdArbol(Integer.parseInt(idArbol));
			imagen.setUrlImg(urlImg);
			imagen.setDescripcion(descripcion);

			int idImagen = imagenesRespuestaBI.insertaImagen(imagen);

			imagen.setIdImagen(idImagen);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID IMAGEN CREADA-->");
			mv.addObject("res", idImagen);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaAsignaciones.json?idChecklist=<?>&idCeco=<?>&idPuesto=<?>&activo=<?>
	@RequestMapping(value = "/altaAsignaciones", method = RequestMethod.GET)
	public ModelAndView altaAsignaciones(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idChecklist = request.getParameter("idChecklist");
			String idCeco = request.getParameter("idCeco");
			String idPuesto = request.getParameter("idPuesto");
			String activo = request.getParameter("activo");

			AsignacionDTO asignacionDTO = new AsignacionDTO();

			asignacionDTO.setIdChecklist(Integer.parseInt(idChecklist));
			asignacionDTO.setCeco(idCeco);
			asignacionDTO.setIdPuesto(Integer.parseInt(idPuesto));
			asignacionDTO.setActivo(Integer.parseInt(activo));

			boolean res = asignacionBI.insertaAsignacion(asignacionDTO);

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "RESULTADO DE INSERTAR ASIGNACION ");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/asignaChecklistNew.json?ceco=<?>&puesto=<?>&idCheck=<?>
	@RequestMapping(value = "/asignaChecklistNew", method = RequestMethod.GET)
	public ModelAndView asignaChecklistNew(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String ceco = request.getParameter("ceco");
			String puesto = request.getParameter("puesto");
			String idCheck = request.getParameter("idCheck");

			Map<String, Object> res = asignacionNewBI.ejecutaAsignacion(ceco, Integer.parseInt(puesto),
					Integer.parseInt(idCheck));

			ModelAndView mv = new ModelAndView("muestraServicios");
			mv.addObject("tipo", "EJECUTE ASIGNACIONES_NEW");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/asignaChecklistEspecialNew.json?usrNew=<?>&usrOld=<?>
	@RequestMapping(value = "/asignaChecklistEspecialNew", method = RequestMethod.GET)
	public ModelAndView asignaChecklistEspecialNew(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String usrNew = request.getParameter("usrNew");
			String usrOld = request.getParameter("usrOld");

			Map<String, Object> res = asignacionNewBI.asignacionEspecial(usrNew, usrOld);

			ModelAndView mv = new ModelAndView("muestraServicios");
			mv.addObject("tipo", "EJECUTE ASIGNACIONES_NEW");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaApp.json?descripcion=<?>&commit=<?>
	@RequestMapping(value = "/altaApp", method = RequestMethod.GET)
	public ModelAndView altaApp(HttpServletRequest request, HttpServletResponse response) {
		try {
			String descripcion = request.getParameter("descripcion");
			String commit = request.getParameter("commit");

			AppPerfilDTO app = new AppPerfilDTO();
			app.setCommit(Integer.parseInt(commit));
			app.setDescripcion(descripcion);

			int idApp = appBI.insertaApp(app);

			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

			mv.addObject("res", idApp);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaAppPerfil.json?idApp=<?>&idUsuario=<?>&commit=<?>
	@RequestMapping(value = "/altaAppPerfil", method = RequestMethod.GET)
	public ModelAndView altaAppPerfil(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idApp = request.getParameter("idApp");
			String idUsuario = request.getParameter("idUsuario");
			String commit = request.getParameter("commit");

			AppPerfilDTO app = new AppPerfilDTO();
			app.setCommit(Integer.parseInt(commit));
			app.setIdApp(Integer.parseInt(idApp));
			app.setIdUsuario(Integer.parseInt(idUsuario));

			int idAppPerfil = appBI.insertaAppPerfil(app);

			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

			mv.addObject("res", idAppPerfil);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaPlantilla.json?descripcion=<?>
	@RequestMapping(value = "/altaPlantilla", method = RequestMethod.GET)
	public ModelAndView altaPlantilla(HttpServletRequest request, HttpServletResponse response) {
		try {
			String descripcion = request.getParameter("descripcion");

			PlantillaDTO plantilla = new PlantillaDTO();
			plantilla.setDescripcion(descripcion);

			int idApp = plantillaBI.insertaPlantilla(plantilla);

			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

			mv.addObject("res", idApp);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaElemento.json?etiqueta=<?>&orden=<?>&obliga=<?>&idPlantilla=<?>&tipo=<?>
	@RequestMapping(value = "/altaElemento", method = RequestMethod.GET)
	public ModelAndView altaElemento(HttpServletRequest request, HttpServletResponse response) {
		try {
			String etiqueta = request.getParameter("etiqueta");
			String orden = request.getParameter("orden");
			String obliga = request.getParameter("obliga");
			String idPlantilla = request.getParameter("idPlantilla");
			String tipo = request.getParameter("tipo");

			PlantillaDTO plantilla = new PlantillaDTO();
			plantilla.setEtiqueta(etiqueta);
			plantilla.setOrden(Integer.parseInt(orden));
			plantilla.setObligatorio(Integer.parseInt(obliga));
			plantilla.setIdPlantilla(Integer.parseInt(idPlantilla));
			plantilla.setIdArchivo(Integer.parseInt(tipo));

			int idApp = plantillaBI.insertaElementoP(plantilla);

			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

			mv.addObject("res", idApp);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaPreguntaN.json?idMod=<?>&idTipo=<?>&estatus=<?>&setPregunta=<?>&commit=<?>&critica=<?>&sla=<?>&area=<?>&numSerie=<?>
	// http://10.51.219.179:8080/checklist/checklistServices/altaPreguntaN.json?idMod=130&idTipo=24&estatus=1&setPregunta=¿Pregunta
	// test?&commit=1&detalle<?>&critica=1&sla=1&area=
	@RequestMapping(value = "/altaPreguntaN", method = RequestMethod.GET)
	public ModelAndView altaPreguntaN(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		PreguntaDTO pregunta = new PreguntaDTO();
		try {

			String idMod = request.getParameter("idMod");
			String idTipo = request.getParameter("idTipo");
			String estatus = request.getParameter("estatus");
			String detalle = request.getParameter("detalle");
			int critica = Integer.parseInt(request.getParameter("critica"));

			char[] ca = { '\u0061', '\u0301' };
			char[] ce = { '\u0065', '\u0301' };
			char[] ci = { '\u0069', '\u0301' };
			char[] co = { '\u006F', '\u0301' };
			char[] cu = { '\u0075', '\u0301' };
			// mayusculas
			char[] c1 = { '\u0041', '\u0301' };
			char[] c2 = { '\u0045', '\u0301' };
			char[] c3 = { '\u0049', '\u0301' };
			char[] c4 = { '\u004F', '\u0301' };
			char[] c5 = { '\u0055', '\u0301' };
			char[] c6 = { '\u006E', '\u0303' };

			String setPregunta = request.getParameter("setPregunta");
			// System.out.println("setPregunta1 " +setPregunta);
			if (setPregunta.contains(String.valueOf(ca)) || setPregunta.contains(String.valueOf(ce))
					|| setPregunta.contains(String.valueOf(ci)) || setPregunta.contains(String.valueOf(co))
					|| setPregunta.contains(String.valueOf(cu)) || setPregunta.contains(String.valueOf(c1))
					|| setPregunta.contains(String.valueOf(c2)) || setPregunta.contains(String.valueOf(c3))
					|| setPregunta.contains(String.valueOf(c4)) || setPregunta.contains(String.valueOf(c5))
					|| setPregunta.contains(String.valueOf(c6))) {

				String rr = setPregunta.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
						.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
						.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
						.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
						.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
						.replaceAll(String.valueOf(c6), "ñ");
				// System.out.println("rr " +rr);

				pregunta.setPregunta(rr);
				rr = null;
			} else {
				// System.out.println("setPregunta " +setPregunta);

				pregunta.setPregunta(setPregunta);

			}

			if (detalle.contains(String.valueOf(ca)) || detalle.contains(String.valueOf(ce))
					|| detalle.contains(String.valueOf(ci)) || detalle.contains(String.valueOf(co))
					|| detalle.contains(String.valueOf(cu)) || detalle.contains(String.valueOf(c1))
					|| detalle.contains(String.valueOf(c2)) || detalle.contains(String.valueOf(c3))
					|| detalle.contains(String.valueOf(c4)) || detalle.contains(String.valueOf(c5))
					|| detalle.contains(String.valueOf(c6))) {

				String rr = detalle.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
						.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
						.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
						.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
						.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
						.replaceAll(String.valueOf(c6), "ñ");

				pregunta.setDetalle(rr);
				rr = null;
			} else {

				pregunta.setDetalle(detalle);

			}

			String commit = request.getParameter("commit");
			String sla = request.getParameter("sla");
			String area = "";
			String numSerie = request.getParameter("numSerie");
			area = request.getParameter("area");

			pregunta.setIdModulo(Integer.parseInt(idMod));
			pregunta.setIdTipo(Integer.parseInt(idTipo));
			pregunta.setEstatus(Integer.parseInt(estatus));
			pregunta.setCritica(critica);

			if (numSerie != null) {
				if (!numSerie.equals("")) {
					pregunta.setCodigo(Integer.parseInt(numSerie));
				} else {
					pregunta.setCodigo(0);
				}
			} else {
				pregunta.setCodigo(0);
			}

			// pregunta.setDetalle(detalle);

			pregunta.setCommit(Integer.parseInt(commit));
			pregunta.setSla(sla);
			pregunta.setArea(area);
			int idPreg = this.preguntabi.insertaPreguntaCom(pregunta);
			pregunta.setIdPregunta(idPreg);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID DE PREGUNTA CREADA");
			mv.addObject("res", idPreg);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/altaCheckServiceCom.json?fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>&commit=<?>&idUsuario=<?>&periodicidad=<?>&dia=<?>&version=<?>&ordenGrupo=<?>&ponderacionTot=<?>clasifica=<?>
	// FORMATO FECHA INI Y FECHA FIN (AAAAMMDD)
	@RequestMapping(value = "/altaCheckServiceCom", method = RequestMethod.GET)
	public @ResponseBody ChecklistDTO altaCheckServiceCom(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String fechaIni = request.getParameter("fechaIni");
			String fechaFin = request.getParameter("fechaFin");
			String idEstado = request.getParameter("idEstado");
			String idHorario = request.getParameter("idHorario");
			String idTipoCheck = request.getParameter("idTipoCheck");
			String nombreCheck = request.getParameter("nombreCheck");
			String setVigente = request.getParameter("setVigente");
			String commit = request.getParameter("commit");
			String idUsuario = request.getParameter("idUsuario");
			String periodicidad = request.getParameter("periodicidad");
			String version = request.getParameter("version");
			String ordenGrupo = request.getParameter("ordenGrupo");
			String dia = request.getParameter("dia");
			String clasifica = request.getParameter("clasifica");
			String nego = request.getParameter("nego");
			double ponderacionTot = Double.parseDouble(request.getParameter("ponderacionTot"));

			UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
			idUsuario = userSession.getIdUsuario();

			ChecklistDTO checklist = new ChecklistDTO();
			checklist.setFecha_inicio(fechaIni);
			checklist.setFecha_fin(fechaFin);
			checklist.setIdEstado(Integer.parseInt(idEstado));
			checklist.setIdHorario(Integer.parseInt(idHorario));
			checklist.setCommit(Integer.parseInt(commit));
			TipoChecklistDTO chk = new TipoChecklistDTO();
			chk.setIdTipoCheck(Integer.parseInt(idTipoCheck));
			checklist.setIdTipoChecklist(chk);
			checklist.setNombreCheck(nombreCheck);
			checklist.setVigente(Integer.parseInt(setVigente));
			// SE AGREGA USUARIO RESPONSABLE, DIA Y PERIDO
			checklist.setIdUsuario(Integer.parseInt(idUsuario));
			checklist.setPeriodicidad(periodicidad);
			checklist.setVersion(version);
			checklist.setOrdeGrupo(ordenGrupo);
			checklist.setDia(dia);
			checklist.setPonderacionTot(ponderacionTot);
			checklist.setClasifica(clasifica);
			checklist.setNego(nego);
			// Obtengo el ID del checklist
			int idChecklist = checklistbi.insertaChecklistCom(checklist);
			checklist.setIdChecklist(idChecklist);

			return checklist;

		} catch (Exception e) {
			logger.info(e);
			return null;
		}

	}

	// http://10.51.219.179:8080/checklist/checklistServices/altaChecklistProtocolo.json?idTipoChecklist=21&nombreCheck=PRUEBA_PACKAGE&idHorario=23&vigente=1&fecha_inicio=20190610&fecha_fin=20190610&idEstado=21&idUsuario=82144&dia=L&periodo=1&version=1&ordenGrupo=321&commit=1&ponderacionTot=0&clasifica=A&idProtocolo=2
	@RequestMapping(value = "/altaChecklistProtocolo", method = RequestMethod.GET)
	public @ResponseBody ModelAndView altaChecklistProtocolo(HttpServletRequest request, HttpServletResponse response) {
		try {

			String idTipoChecklist = request.getParameter("idTipoChecklist");
			// int idChecklist =
			// Integer.parseInt(request.getParameter("idChecklist"));
			String nombreCheck = request.getParameter("nombreCheck");
			String idHorario = request.getParameter("idHorario");
			String vigente = request.getParameter("vigente");
			String fecha_inicio = request.getParameter("fecha_inicio");
			String fecha_fin = request.getParameter("fecha_fin");
			String idEstado = request.getParameter("idEstado");
			String idUsuario = request.getParameter("idUsuario");
			String dia = request.getParameter("dia");
			String periodo = request.getParameter("periodo");
			String version = request.getParameter("version");
			String ordenGrupo = request.getParameter("ordenGrupo");
			int commit = Integer.parseInt(request.getParameter("commit"));
			double ponderacionTot = Double.parseDouble(request.getParameter("ponderacionTot"));
			String clasifica = request.getParameter("clasifica");
			String idProtocolo = request.getParameter("idProtocolo");

			ChecklistProtocoloDTO checklistProtocolo = new ChecklistProtocoloDTO();

			checklistProtocolo.setIdTipoChecklist(Integer.parseInt(idTipoChecklist));
			checklistProtocolo.setNombreCheck(nombreCheck);
			checklistProtocolo.setIdHorario(Integer.parseInt(idHorario));
			checklistProtocolo.setVigente(Integer.parseInt(vigente));
			checklistProtocolo.setFecha_inicio(fecha_inicio);
			checklistProtocolo.setFecha_fin(fecha_fin);
			checklistProtocolo.setIdEstado(Integer.parseInt(idEstado));
			checklistProtocolo.setIdUsuario(idUsuario);
			checklistProtocolo.setDia(dia);
			checklistProtocolo.setPeriodo(periodo);
			checklistProtocolo.setVersion(version);
			checklistProtocolo.setOrdenGrupo(ordenGrupo);
			checklistProtocolo.setCommit(commit);
			checklistProtocolo.setPonderacionTot(ponderacionTot);
			checklistProtocolo.setClasifica(clasifica);
			checklistProtocolo.setIdProtocolo(idProtocolo);

			int idChecklist = checklistProtocoloBI.insertaChecklistCom(checklistProtocolo);
			checklistProtocolo.setIdChecklist(idChecklist);
			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

			mv.addObject("res", idChecklist);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;

		}

	}

	// http://10.51.219.179:8080/checklist/checklistServices/altaChecklistGpo.json?idUsuario=331952&idCeco=999995&idGpo=326
	@RequestMapping(value = "/altaChecklistGpo", method = RequestMethod.GET)
	public @ResponseBody ModelAndView altaChecklistGpo(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idUsuario = request.getParameter("idUsuario");
			String idCeco = request.getParameter("idCeco");
			String idGpo = request.getParameter("idGpo");
			boolean respuesta = checkListUsuarioGpoBI.insertaCheck(idUsuario, idCeco, idGpo);
			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
			mv.addObject("res", respuesta);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;

		}

	}

	// http://10.51.218.72:8080/checklist/checklistServices/altaPreguntaSup.json?idMod=130&idTipo=21&estatus=1&setPregunta=¿Prueba correcta?&detalle=Ayuda contextual&critica=1&sla=&area=Baño&numSerie=1234
	@RequestMapping(value = "/altaPreguntaSup", method = RequestMethod.GET)
	public ModelAndView altaPreguntaSup(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		PreguntaSupDTO pregunta = new PreguntaSupDTO();
		try {

			String idMod = request.getParameter("idMod");
			String idTipo = request.getParameter("idTipo");
			String estatus = request.getParameter("estatus");
			String detalle = request.getParameter("detalle");
			String critica = request.getParameter("critica");

			char[] ca = { '\u0061', '\u0301' };
			char[] ce = { '\u0065', '\u0301' };
			char[] ci = { '\u0069', '\u0301' };
			char[] co = { '\u006F', '\u0301' };
			char[] cu = { '\u0075', '\u0301' };
			// mayusculas
			char[] c1 = { '\u0041', '\u0301' };
			char[] c2 = { '\u0045', '\u0301' };
			char[] c3 = { '\u0049', '\u0301' };
			char[] c4 = { '\u004F', '\u0301' };
			char[] c5 = { '\u0055', '\u0301' };
			char[] c6 = { '\u006E', '\u0303' };

			String setPregunta = request.getParameter("setPregunta");
			if (setPregunta.contains(String.valueOf(ca)) || setPregunta.contains(String.valueOf(ce))
					|| setPregunta.contains(String.valueOf(ci)) || setPregunta.contains(String.valueOf(co))
					|| setPregunta.contains(String.valueOf(cu)) || setPregunta.contains(String.valueOf(c1))
					|| setPregunta.contains(String.valueOf(c2)) || setPregunta.contains(String.valueOf(c3))
					|| setPregunta.contains(String.valueOf(c4)) || setPregunta.contains(String.valueOf(c5))
					|| setPregunta.contains(String.valueOf(c6))) {

				String rr = setPregunta.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
						.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
						.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
						.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
						.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
						.replaceAll(String.valueOf(c6), "ñ");

				pregunta.setDescPregunta(rr);
				rr = null;
			} else {

				pregunta.setDescPregunta(setPregunta);

			}

			if (detalle.contains(String.valueOf(ca)) || detalle.contains(String.valueOf(ce))
					|| detalle.contains(String.valueOf(ci)) || detalle.contains(String.valueOf(co))
					|| detalle.contains(String.valueOf(cu)) || detalle.contains(String.valueOf(c1))
					|| detalle.contains(String.valueOf(c2)) || detalle.contains(String.valueOf(c3))
					|| detalle.contains(String.valueOf(c4)) || detalle.contains(String.valueOf(c5))
					|| detalle.contains(String.valueOf(c6))) {

				String rr = detalle.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
						.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
						.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
						.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
						.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
						.replaceAll(String.valueOf(c6), "ñ");

				pregunta.setDetalle(rr);
				rr = null;
			} else {

				pregunta.setDetalle(detalle);

			}

			String sla = request.getParameter("sla");
			String numSerie = request.getParameter("numSerie");
			String area = request.getParameter("area");

			pregunta.setIdModulo(idMod);
			pregunta.setIdTipo(idTipo);
			pregunta.setEstatus(estatus);
			pregunta.setCritica(critica);
			pregunta.setSla(sla);
			pregunta.setNumSerie(numSerie);
			pregunta.setCommit(1);
			pregunta.setArea(area);

			int idPreg = preguntaSupBI.insertaPreguntas(pregunta);
			pregunta.setIdPregunta(idPreg);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID DE PREGUNTA CREADA");
			mv.addObject("res", idPreg);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://10.51.218.72:8080/checklist/checklistServices/altaZona.json?descripcion=?&idStatus=?&idTipo=?
	@RequestMapping(value = "/altaZona", method = RequestMethod.GET)
	public ModelAndView altaZona(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		try {
			String descripcion = request.getParameter("descripcion");
			String idStatus = request.getParameter("idStatus");
			String idTipo = request.getParameter("idTipo");

			AdminZonaDTO zona = new AdminZonaDTO();
			zona.setDescripcion(descripcion);
			zona.setIdStatus(idStatus);
			zona.setIdTipo(idTipo);

			boolean respuesta = admZonasBI.insertaZona(zona);
			mv.addObject("tipo", "Inserta Zona");
			mv.addObject("res", respuesta);

		} catch (Exception e) {
			System.out.println("Catch altaZona: " + e.getStackTrace());
		}

		return mv;
	}
	
	//http://10.51.218.72:8080/checklist/checklistServices/altaTipoZona.json?descripcion=?&idStatus=?
    @RequestMapping(value = "/altaTipoZona", method = RequestMethod.GET)
    public ModelAndView altaTipoZona(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        try {
            String descripcion = request.getParameter("descripcion");
            String idStatus = request.getParameter("idStatus");
            
            AdmTipoZonaDTO tipoZona = new AdmTipoZonaDTO();
            tipoZona.setDescripcion(descripcion);
            tipoZona.setIdStatus(idStatus);
            
            boolean respuesta = admTipoZonaBI.insertaTipoZona(tipoZona);
            mv.addObject("tipo", "Inserta Tipo Zona");
            mv.addObject("res", respuesta);

        } catch (Exception e) {
        	System.out.println("Catch altaTipoZona: "+e.getStackTrace());
        }

        return mv;
    }
    
  //http://10.51.218.72:8080/checklist/checklistServices/altaPregZona.json?idPregunta=?&idZona=?
    @RequestMapping(value = "/altaPregZona", method = RequestMethod.GET)
    public ModelAndView altaPregZona(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        try {
            String idPregunta = request.getParameter("idPregunta");
            String idZona = request.getParameter("idZona");
            
            AdmPregZonaDTO pregZona = new AdmPregZonaDTO();
            pregZona.setIdPreg(idPregunta);
            pregZona.setIdZona(idZona);
            
            boolean respuesta = admPregZonasBI.insertaPregZona(pregZona);
            mv.addObject("tipo", "Inserta Pregunta Zona");
            mv.addObject("res", respuesta);

        } catch (Exception e) {
        	System.out.println("Catch altaPreguntaZona: "+e.getStackTrace());
        }

        return mv;
    }

}
