package com.gruposalinas.checklist.business;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gruposalinas.checklist.domain.AsignacionCheckDTO;
import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.Usuario_ADTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import com.gruposalinas.checklist.resources.FRQConstantes;

public class EnviaCorreoVisitasBI {

	private static final Logger logger = LogManager.getLogger(EnviaCorreoVisitasBI.class);
	
	public String enviaCorre() {

		//0 para lam
		//1 para mexico
		
		String estado= "Fallo";

		estado = "Correo Entra al Bunker México: " + enviaCorreVisitas(1, 1);
		estado += "     Correo Entra al Bunker LAM: " + enviaCorreVisitas(1 , 0);
		
		estado += "     Correo No Entra al Bunker México: " + enviaCorreVisitas(2 , 1);		
		estado += "     Correo No Entra al Bunker LAM: " + enviaCorreVisitas(2 , 0);

		logger.info("Estatus de los correos de Asignación: " + estado);
		return estado;
	}
	
	public String enviaEstadoCorreos() {
		String estado="";
		CorreoBI correoBI = (CorreoBI) FRQAppContextProvider.getApplicationContext().getBean("correoBI");
		//estado = ""+ correoBI.sendMailEstado("");	
		return estado;
	}
	
	public String enviaCorreVisitas(int bunker, int pais) {
		
		String estado= "Fallo";
		
		List<AsignacionCheckDTO > asignaciones = null;
		Calendar calendar = Calendar.getInstance();
		int numeroSemana = calendar.get(Calendar.WEEK_OF_YEAR) + 1;

		List<String> destinatarios = new ArrayList<>();
	    List<String> copiados = new ArrayList<>();
		
		String archivo = "";
		CorreoBI correoBI = (CorreoBI) FRQAppContextProvider.getApplicationContext().getBean("correoBI");
		AsignacionCheckBI asignacionCheckBI = (AsignacionCheckBI) FRQAppContextProvider.getApplicationContext().getBean("asignacionCheckBI");
		
		try{
			logger.info("bunker " + bunker);
			asignaciones = asignacionCheckBI.obtieneAsignacionC(bunker, pais);
			logger.info("asignaciones " + asignaciones);
			
			if(asignaciones!=null && asignaciones.size()>0) {
			
				archivo = "<table style='width:80%; border-collapse:collapse;' cellpadding='30' cellspacing='0' border='1' bordercolor='#666666' id='Exportar_a_Excel'>"
						+ "<thead>"
						+ 	"<tr>"
						+ 		"<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Periodo</th>"
						+ 		"<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Área</th>"
						+ 		"<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Recurso</th>"
						+ 		"<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Día</th>"
						+ 		"<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Tienda</th>"
						+ 		"<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Responsable</th>"
						+ 		"<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Entra a Bunker</th>"
						+ 		"<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Horario entrada a Bunker</th>"
						+ 		"<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Horario salida de Bunker</th>"
						+ 		"<th align='center' style='background:#386345; color:white; font:12px; white-space: nowrap;'>Justificación</th>"
						+	"</tr>"
						+ "</thead>";
				archivo += "<tbody>";
				
				
				for (int i = 0; i < asignaciones.size(); i++) {
					
					int idUsuario = asignaciones.get(i).getAsigna();					
					Usuario_ADTO usuario = getUsuario(idUsuario);
					String direccion = obtenDireccion(usuario);
					
					String nomUsuario = "";
					if(usuario!=null)
						nomUsuario=usuario.getNombre();
					
					AsignacionCheckDTO aux = asignaciones.get(i);
					copiados.add(aux.getCorreoC());
					if(i==0)
						//destinatarios.add(aux.getCorreoD());
					archivo += "<tr>"
							+ 	"<td align='center' style='font:12px; white-space: nowrap;'>Semana "+numeroSemana+"</td>"
							+ 	"<td align='center' style='font:12px; white-space: nowrap;'>"+direccion+"</td>"
							+ 	"<td align='center' style='font:12px; white-space: nowrap;'>"+aux.getNombreUsuario()+"</td>"
							+ 	"<td align='center' style='font:12px; white-space: nowrap;'>"+aux.getFecha()+"</td>"
							+ 	"<td align='center' style='font:12px; white-space: nowrap;'>"+aux.getNombreSucursal()+"</td>"
							+ 	"<td align='center' style='font:12px; white-space: nowrap;'>"+nomUsuario+"</td>"+
							(aux.getBunker() != 1 ? "<td align='center' style='font:12px; white-space: nowrap;'>NO</td><td align='center' style='font:12px; white-space: nowrap;'>-</td><td align='center' style='font:12px; white-space: nowrap;'>-</td>" : "<td align='center' style='font:12px; white-space: nowrap;'>SI</td><td align='center' style='font:12px; white-space: nowrap;'>"+aux.getHoraBunker()+"</td>"+"<td align='center' style='font:12px; white-space: nowrap;'>"+aux.getHoraBunkerSalida()+"</td>")
							+ 	"<td align='center' style='font:12px; white-space: nowrap;'>"+aux.getJustificacion()+"</td>";		
					archivo +="</tr>";
					
					
					/******************************ENVIA CORREO SUC Y USU********************************/
					if(aux.getBunker() != 1 )
						estado = "Correo " + correoBI.sendMailEstado(aux.getIdUsuario(),aux.getNombreUsuario(),aux.getIdSucursal(),aux.getNombreSucursal(),aux.getFecha(),"","");
					else
						estado = "Correo " + correoBI.sendMailEstado(aux.getIdUsuario(),aux.getNombreUsuario(),aux.getIdSucursal(),aux.getNombreSucursal(),aux.getFecha(),", el socio solicitó entrar a Bunker de las <strong>"+aux.getHoraBunker().substring(11, 16)+"</strong> hrs a las <strong>"+aux.getHoraBunkerSalida().substring(11, 16)+"</strong> hrs"," - Bunker");
					/******************************ENVIA CORREO SUC Y USU********************************/
				}					
				archivo += "</tbody><table>";
				
				
				String mailContactoMaria = FRQConstantes.mailContactoMaria;
				String mailContactoJorge = FRQConstantes.mailContactoJorge;
				String mailContactoGustavo = FRQConstantes.mailContactoGustavo;
				
				String mailLAM = FRQConstantes.mailLAM;
				
				String correosD="";
				
				if(pais==1) {
					if(bunker==1) {
						correosD = mailContactoMaria +"," + mailContactoJorge +"," + mailContactoGustavo;
					}else {
						correosD = mailContactoMaria;
					}
				}else {
					if(bunker==1) {
						correosD = mailLAM +"," + mailContactoJorge +"," + mailContactoGustavo;
					}else {
						correosD = mailLAM;
					}
				}
				
				logger.info("correosD... " + correosD);
				
				destinatarios.add(correosD);
				
				logger.info("destinatarios "+ destinatarios.toString());
				logger.info("copiados "+ copiados.toString());
		        
				byte[] encode = archivo.getBytes("UTF-8");
				
				File temp = null;
				temp = File.createTempFile("reporteVisitas-", ".xls");
		        try (FileOutputStream file = new FileOutputStream(temp)) {
		            file.write(encode);
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		        //estado = "ENVIADO";		
		        if(bunker == 1)
		        		estado = ""+ correoBI.sendMailVisitas(destinatarios, copiados, temp, " - Bunker","con visita a Bunker");		
		        else
		        		estado = ""+ correoBI.sendMailVisitas(destinatarios, copiados, temp, "","a visitar");		
		        
			}else {
				estado="Vacio";
			}			
		}catch(Exception e){
			logger.info("Ocurrio algo: " + e);
		}
		logger.info("respuesta del envio: " + estado);
		return estado;
	}
	
	public String obtenDireccion(Usuario_ADTO usuario) {
		
		String ceco="";
				
		if(usuario!=null) {
			CecoBI cecoBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
			
			try {	
				List<CecoDTO> listaCeco = null;
				try {
					listaCeco = cecoBI.buscaCecoPaso(usuario.getIdCeco(), "NULL", "NULL");
					if(listaCeco!= null & listaCeco.size()>0) {
						CecoDTO c = listaCeco.get(0);
						ceco = c.getDescCeco();
					}
				} catch (Exception e) {
					logger.info("Ocurrio algo adentro: " +e);
				}
			} catch (Exception e) {
				logger.info("Ocurrio algo afuera: " +e);
			}
		}
		
		logger.info("ceco " + ceco);

		return ceco;
	}
	
	public Usuario_ADTO getUsuario(int idUsuario) {
		
		Usuario_ADTO usuario = null;
		List<Usuario_ADTO> listaUsuario = null;
		Usuario_ABI usuario_ABI = (Usuario_ABI) FRQAppContextProvider.getApplicationContext().getBean("usuario_ABI");
		
		try {
			listaUsuario = usuario_ABI.obtieneUsuario(idUsuario);
			if(listaUsuario!= null & listaUsuario.size()>0) {
				usuario = listaUsuario.get(0);	
			}
		} catch (Exception e) {
			logger.info("Ocurrio algo afuera: " +e);
		}
		logger.info("usuario " + usuario);
		return usuario;
	}
	
	
	/*
	 public String obtenDireccion(int idUsuario) {
		
		String ceco="";

		List<Usuario_ADTO> listaUsuario = null;
		CecoBI cecoBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
		Usuario_ABI usuario_ABI = (Usuario_ABI) FRQAppContextProvider.getApplicationContext().getBean("usuario_ABI");
		try {
			listaUsuario = usuario_ABI.obtieneUsuario(idUsuario);
			if(listaUsuario!= null & listaUsuario.size()>0) {
				Usuario_ADTO usuario = listaUsuario.get(0);	
				List<CecoDTO> listaCeco = null;
				try {
					listaCeco = cecoBI.buscaCecoPaso(usuario.getIdCeco(), "NULL", "NULL");
					if(listaCeco!= null & listaCeco.size()>0) {
						CecoDTO c = listaCeco.get(0);
						ceco = c.getDescCeco();
					}
				} catch (Exception e) {
					logger.info("Ocurrio algo adentro: " +e);
				}
			}
		} catch (Exception e) {
			logger.info("Ocurrio algo afuera: " +e);
		}
		logger.info("ceco " + ceco);

		return ceco;
	}
	*/
	
}
