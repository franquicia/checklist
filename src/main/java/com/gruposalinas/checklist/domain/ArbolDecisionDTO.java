package com.gruposalinas.checklist.domain;

public class ArbolDecisionDTO {
	
	private int idArbolDesicion;
	private int idCheckList;
	private int idPregunta;
	private String respuesta;
	private String descResp;
	private int estatusEvidencia;
	private int ordenCheckRespuesta;
	private int commit;
	private int reqAccion;
	private int reqObservacion;
	private int reqOblig;
	private String descEvidencia;
	private String numVersion;
	private String tipoCambio;
	private String ponderacion;
	private int idPlantilla;
	private int idProtocolo;
	
	public int getIdArbolDesicion() {
		return idArbolDesicion;
	}
	public void setIdArbolDesicion(int idArbolDesicion) {
		this.idArbolDesicion = idArbolDesicion;
	}
	public int getIdCheckList() {
		return idCheckList;
	}
	public void setIdCheckList(int idCheckList) {
		this.idCheckList = idCheckList;
	}
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public int getEstatusEvidencia() {
		return estatusEvidencia;
	}
	public void setEstatusEvidencia(int estatusEvidencia) {
		this.estatusEvidencia = estatusEvidencia;
	}
	public int getOrdenCheckRespuesta() {
		return ordenCheckRespuesta;
	}
	public void setOrdenCheckRespuesta(int ordenCheckRespuesta) {
		this.ordenCheckRespuesta = ordenCheckRespuesta;
	}
	public int getCommit() {
		return commit;
	}
	public void setCommit(int commit) {
		this.commit = commit;
	}
	public int getReqAccion() {
		return reqAccion;
	}
	public void setReqAccion(int reqAccion) {
		this.reqAccion = reqAccion;
	}
	public int getReqObservacion() {
		return reqObservacion;
	}
	public void setReqObservacion(int reqObservacion) {
		this.reqObservacion = reqObservacion;
	}
	public int getReqOblig() {
		return reqOblig;
	}
	public void setReqOblig(int reqOblig) {
		this.reqOblig = reqOblig;
	}
	public String getDescEvidencia() {
		return descEvidencia;
	}
	public void setDescEvidencia(String descEvidencia) {
		this.descEvidencia = descEvidencia;
	}
	public String getNumVersion() {
		return numVersion;
	}
	public void setNumVersion(String numVersion) {
		this.numVersion = numVersion;
	}
	public String getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	
	public String getPonderacion(){
		return ponderacion;
	}
	
	public void setPonderacion(String ponderacion){
		this.ponderacion = ponderacion;
	}
	public int getIdPlantilla() {
		return idPlantilla;
	}
	public void setIdPlantilla(int idPlantilla) {
		this.idPlantilla = idPlantilla;
	}
	public int getIdProtocolo() {
		return idProtocolo;
	}
	public void setIdProtocolo(int idProtocolo) {
		this.idProtocolo = idProtocolo;
	}
	public String getDescResp() {
		return descResp;
	}
	public void setDescResp(String descResp) {
		this.descResp = descResp;
	}
	@Override
	public String toString() {
		return "ArbolDecisionDTO [idArbolDesicion=" + idArbolDesicion + ", idCheckList=" + idCheckList + ", idPregunta="
				+ idPregunta + ", respuesta=" + respuesta + ", descResp=" + descResp + ", estatusEvidencia="
				+ estatusEvidencia + ", ordenCheckRespuesta=" + ordenCheckRespuesta + ", commit=" + commit
				+ ", reqAccion=" + reqAccion + ", reqObservacion=" + reqObservacion + ", reqOblig=" + reqOblig
				+ ", descEvidencia=" + descEvidencia + ", numVersion=" + numVersion + ", tipoCambio=" + tipoCambio
				+ ", ponderacion=" + ponderacion + ", idPlantilla=" + idPlantilla + ", idProtocolo=" + idProtocolo
				+ "]";
	}
	
	
}
