package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteDTO;

public class ReporteUrlRowMapper implements RowMapper<ReporteDTO> {

	public ReporteDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteDTO reporteDTO = new ReporteDTO();
		
		reporteDTO.setIdTipoCheck(rs.getInt("FIID_TIPO_CHECK"));
		reporteDTO.setRespuesta(rs.getString("FCRESPUESTA"));
		reporteDTO.setNombreCeco(rs.getString("CECO"));
		reporteDTO.setIdCeco(rs.getString("FCID_CECO"));
		reporteDTO.setIdPregunta(rs.getString("FIID_PREGUNTA"));
		reporteDTO.setDesPregunta(rs.getString("FCDESCRIPCION"));
		reporteDTO.setIdGeografia(rs.getInt("IDGEOGRAFIA"));
		reporteDTO.setNombreGeografia(rs.getString("NOMBREGEOGRAFIA"));
		reporteDTO.setNombreUsuario(rs.getString("FCNOMBRE"));
		reporteDTO.setFechaRespuesta(rs.getString("FDFECHA_RES"));
		reporteDTO.setIdCheckUsua(rs.getInt("FIID_CHECK_USUA"));
		reporteDTO.setNombreChecklist(rs.getString("CHECKLIST"));
		reporteDTO.setIdRuta(rs.getInt("FIIDEVIDENCIA"));
		reporteDTO.setRuta(rs.getString("FCRUTA"));
		
		return reporteDTO;
		
	}
}
