package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistModificacionesDTO;

public class ChecklistModificacionesRowMapper implements RowMapper<ChecklistModificacionesDTO> {

	@Override
	public ChecklistModificacionesDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ChecklistModificacionesDTO checklistModificacionesDTO = new ChecklistModificacionesDTO();
		
		checklistModificacionesDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		checklistModificacionesDTO.setNombreCheck(rs.getString("FCNOMBRE"));
		checklistModificacionesDTO.setIdHorario(rs.getInt("FIID_HORARIO"));		
		checklistModificacionesDTO.setIdTipoChecklist(rs.getInt("FIID_TIPO_CHECK"));
		checklistModificacionesDTO.setFechaInicioCheck(rs.getString("FDFECHA_INICIO"));
		checklistModificacionesDTO.setFechaFinCheck(rs.getString("FDFECHA_FIN"));
		checklistModificacionesDTO.setModificacionCheck(rs.getString("MODCHECK"));
		checklistModificacionesDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		checklistModificacionesDTO.setPregunta(rs.getString("PREGUNTA"));
		checklistModificacionesDTO.setModificacionPreg(rs.getString("MODPREG"));
		checklistModificacionesDTO.setIdModulo(rs.getInt("IDMODULO"));
		checklistModificacionesDTO.setNombreModulo(rs.getString("MODULO"));
		checklistModificacionesDTO.setIdModuloPadre(rs.getInt("FIID_MOD_PADRE"));
		checklistModificacionesDTO.setNombreModuloPadre(rs.getString("MODULOPADRE"));
		checklistModificacionesDTO.setModificacionModulo(rs.getString("MODMODU"));
		checklistModificacionesDTO.setOrdenCheck(rs.getInt("ORDEN_CHECK"));
		checklistModificacionesDTO.setModificacionCheckPreg(rs.getString("MODCHECK_PREG"));
		checklistModificacionesDTO.setIdArbol(rs.getInt("FIID_ARBOL_DES"));
		checklistModificacionesDTO.setIdPosible(rs.getInt("FIIDPOSIBLE"));
		checklistModificacionesDTO.setPosibleRespuesta(rs.getString("FCDESCRIPCION"));
		checklistModificacionesDTO.setEstatusEvidencia(rs.getInt("FIESTATUS_E"));
		checklistModificacionesDTO.setSiguientePregunta(rs.getInt("FIORDEN_CHECK"));
		checklistModificacionesDTO.setRequiereAccion(rs.getInt("FIREQACCION"));
		checklistModificacionesDTO.setRequiereObsv(rs.getInt("FIREQOBS"));
		checklistModificacionesDTO.setEvidenciaObligatoria(rs.getInt("FIOBLIGA"));
		checklistModificacionesDTO.setEtiquetaEvidencia(rs.getString("FCDESCRIPCION_E"));
		checklistModificacionesDTO.setModificacionArbol(rs.getString("MODARBOL"));
		checklistModificacionesDTO.setIdTipoPreg(rs.getInt("FIID_TIPO_PREG"));
		
		return checklistModificacionesDTO;
	}
	
	

}
