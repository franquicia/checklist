/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.business;

import com.gruposalinas.checklist.util.UtilString;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.InetAddress;
import java.util.Calendar;
import org.springframework.beans.factory.annotation.Autowired;
import sun.misc.BASE64Decoder;

/**
 *
 * @author leodan1991
 */
public class DocsHandlerBI {
    
    //@Autowired
    //private CorreoBI correoBI;

    public void saveDoc(String mainPath, String fileBase64, String fileName, String ext) {

        try {

            Calendar cal = Calendar.getInstance();
            String rootPath = getRootPath() + "/" + mainPath;
            String monthPath = rootPath + "/" + cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1);

            File dirRootPath = new File(monthPath);
            String fullPath = monthPath + "/" + UtilString.cleanFilename(fileName) + "." + UtilString.cleanFilename(ext);
            //se crean los paths de donde se almacenaran los docs
            dirRootPath.mkdirs();

            BASE64Decoder decoder = new BASE64Decoder();

            byte[] fileDecode = decoder.decodeBuffer(fileBase64);

            FileOutputStream fileOutput = new FileOutputStream(fullPath);

            BufferedOutputStream bufferOutput = new BufferedOutputStream(fileOutput);

            bufferOutput.write(fileDecode);
            bufferOutput.close();

            
            
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
    
    
    public String getFullPath(String mainPath, String fileName, String ext) {
        
        String fullPath=""; 

        try {

            Calendar cal = Calendar.getInstance();
            String rootPath = getRootPath() + "/" + mainPath;
            String monthPath = rootPath + "/" + cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1);

            File dirRootPath = new File(monthPath);
            fullPath = monthPath + "/" + UtilString.cleanFilename(fileName) + "." + UtilString.cleanFilename(ext);
          
            
            
            
        } catch (Exception e) {
            e.printStackTrace();

        }
        
        return fullPath;
    }

    private String getRootPath() {
        String rootPath = null;

        try {

            if (!this.verificaServidor(InetAddress.getLocalHost().getHostAddress().toString())) {
                rootPath = "/franquicia/";

            } else {
                rootPath = "/franquicia/";

            }

        } catch (Exception e) {

            rootPath = "/franquicia/";

        }
        
        //rootPath="/Users/leodan1991/franquicia";

        return rootPath;
    }

    private boolean verificaServidor(String ipActual) {
        boolean resp = false;
        String[] ipFrq = {"10.53.33.74", "10.53.33.75", "10.53.33.76", "10.53.33.77"};

        for (String data : ipFrq) {
            if (data.contains(ipActual)) {
                return true;
            }
        }
        return resp;
    }

}
