package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.PosiblesTipoPreguntaDTO;

public class PosibleTipoPregTRowMapper implements RowMapper<PosiblesTipoPreguntaDTO> {

	@Override
	public PosiblesTipoPreguntaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		PosiblesTipoPreguntaDTO posibleTipoPregunta = new PosiblesTipoPreguntaDTO();
		
		posibleTipoPregunta.setIdPosibleTipoPregunta(rs.getInt("FIIDPOSIPREG"));
		posibleTipoPregunta.setIdTipoPregunta(rs.getInt("FIID_TIPO_PREG"));
		posibleTipoPregunta.setIdPosibleRespuesta(rs.getInt("FIIDPOSIBLE"));
		posibleTipoPregunta.setDescripcionPosible(rs.getString("FCDESCRIPCION"));
		posibleTipoPregunta.setNumeroRevision(rs.getString("PA_NUM_REV"));
		posibleTipoPregunta.setTipoCambio(rs.getString("PA_TIPO_MOD"));
		
		return posibleTipoPregunta;
	}
}
