package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ConsultaDAO;
import com.gruposalinas.checklist.domain.ConsultaDTO;

public class ConsultaBI {
		
	private static Logger logger = LogManager.getLogger(ConsultaBI.class);
	
	@Autowired
	ConsultaDAO consultaDAO;
	
	List<ConsultaDTO> listaConsulta = null;

	public List<ConsultaDTO> obtieneConsultaCeco(int idGrupo){
	
		try {
			listaConsulta = consultaDAO.obtieneConsultaCeco(idGrupo);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
		}			
		
		return listaConsulta;
	
	}
	
	List<ConsultaDTO> listaConsulta2 = null;

	public List<ConsultaDTO> obtieneConsultaChecklist(int idGrupo){
		
		try {
			listaConsulta2 = consultaDAO.obtieneConsultaChecklist(idGrupo);
		} catch (Exception e) {
			logger.info(e);
			logger.info("No fue posible consultar el StoreProcedure");
		}			
		
		return listaConsulta2;
	
		}
	
	List<ConsultaDTO> listaConsulta3 = null;

	public List<ConsultaDTO> obtieneConsultaBitacora(int idCeco, int idChecklist){
		
		try {
			listaConsulta3 = consultaDAO.obtieneConsultaBitacora(idCeco, idChecklist);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
		}			
		
		return listaConsulta3;
	
		}
}
