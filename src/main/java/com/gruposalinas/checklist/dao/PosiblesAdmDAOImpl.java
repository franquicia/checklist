package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.PosiblesDTO;
import com.gruposalinas.checklist.mappers.PosiblesRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PosiblesAdmDAOImpl extends DefaultDAO implements PosiblesAdmDAO {

    private static Logger logger = LogManager.getLogger(PosiblesAdmDAOImpl.class);

    private DefaultJdbcCall jdbcBuscaPosible;
    private DefaultJdbcCall jdbcInsertaPosible;
    private DefaultJdbcCall jdbcActualizaPosible;
    private DefaultJdbcCall jdbcEliminaPosible;
    private DefaultJdbcCall jdbcBuscaPosibleById;

    public void init() {

        jdbcBuscaPosible = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPOSIABLESADM")
                .withProcedureName("SP_SELGRALPOS")
                .returningResultSet("RCL_POS", new PosiblesRowMapper());

        jdbcInsertaPosible = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPOSIABLESADM")
                .withProcedureName("SP_INSPOSIBLE");

        jdbcActualizaPosible = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPOSIABLESADM")
                .withProcedureName("SP_ACTPOSIBLE");

        jdbcEliminaPosible = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPOSIABLESADM")
                .withProcedureName("SP_DELPOSIBLE");

        jdbcBuscaPosibleById = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPOSIABLESADM")
                .withProcedureName("SP_SELPOSID")
                .returningResultSet("RCL_POS", new PosiblesRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PosiblesDTO> buscaPosible() throws Exception {
        Map<String, Object> out = null;

        List<PosiblesDTO> listaPosible = null;
        int ejecucion = 0;

        out = jdbcBuscaPosible.execute();

        logger.info("Funcion ejecutada: {checklist.PAPOSIABLESADM.SP_SELGRALPOS}");

        listaPosible = (List<PosiblesDTO>) out.get("RCL_POS");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion != 0) {
            logger.info("Algo ocurrió al obtener las Posibles Respuestas");
        } else {
            return listaPosible;
        }

        return null;
    }

    @Override
    public int insertaPosible(String posible) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        int idPosible = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_DESCRIPCION", posible);

        out = jdbcInsertaPosible.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAPOSIABLESADM.SP_INS_RECURSO}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDPOS");
        idPosible = idReturn.intValue();

        if (ejecucion != 0) {
            logger.info("Algo ocurrió al insertar la Posible Respuesta");
        } else {
            return idPosible;
        }

        return 0;
    }

    @Override
    public boolean actualizaPosible(PosiblesDTO bean) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPOS", bean.getIdPosible())
                .addValue("PA_DESCRIPCION", bean.getDescripcion());

        out = jdbcActualizaPosible.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAPOSIABLESADM.SP_ACTPOSIBLE}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion != 0) {
            logger.info("Algo ocurrió al actualizar la Posible Respuesta con id : " + bean.getIdPosible());
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaPosible(int idPosible) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPOS", idPosible);

        out = jdbcEliminaPosible.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAPOSIABLESADM.SP_DELPOSIBLE}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion != 0) {
            logger.info("Algo ocurrió al eliminar la Posible Respuesta con id: " + idPosible);
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PosiblesDTO> buscaPosible(int idPosible) throws Exception {
        Map<String, Object> out = null;

        List<PosiblesDTO> listaPosible = null;
        int ejecucion = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPOS", idPosible);

        out = jdbcBuscaPosibleById.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAPOSIABLESADM.SP_SELPOSID}");

        listaPosible = (List<PosiblesDTO>) out.get("RCL_POS");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion != 0) {
            logger.info("Algo ocurrió al obtener las Posibles Respuestas");
        } else {
            return listaPosible;
        }

        return null;
    }

}
