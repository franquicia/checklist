package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.ConsultaDistribucionDTO;


public interface ConsultaDistribucionDAO {

	public List<ConsultaDistribucionDTO> ObtieneTerritorio() throws Exception;
	public List<ConsultaDistribucionDTO> ObtieneNivelZona(ConsultaDistribucionDTO bean) throws Exception;
	public List<ConsultaDistribucionDTO> ObtieneNivelRegion(ConsultaDistribucionDTO bean) throws Exception;
	public List<ConsultaDistribucionDTO> ObtieneNivelGerente (ConsultaDistribucionDTO bean) throws Exception;
	public List<ConsultaDistribucionDTO> ObtieneNivelSucursal (ConsultaDistribucionDTO bean) throws Exception;
	public List<ConsultaDistribucionDTO> ObtieneEstructura (ConsultaDistribucionDTO bean) throws Exception;
	



}
