package com.gruposalinas.checklist.domain;

public class CheckSoporteAdmDTO {
	


	private int idChecklist;
	private int idCheckUsua;
	private String nombreCheck;
	private String zona;
	private long rango;
	private String fecha_inicio;
	private String fecha_fin;
	private int idHorario;
	private String valorIni;
	private String valorFin;
	private int vigente;
	private int idEstado;
	private int commit;
	private String ultimaVisita;
	private String fechaModificacion;
	private String usuarios;
	private String numVersion;
	private String tipoCambio;
	private boolean isPendiente;
	private boolean isRechazoCambios;
	private String periodo;
	private int idUsuario;
	private String dia;
	private String periodicidad;
	private int estatus;
	private double distancia;
	private String horaInicio;
	private String horaFin;
	private String fechaEnvio;
	private String modo;
	private String version;
	private String ordeGrupo;
	private double ponderacionTot;
	private String clasifica;
	private int idProtocolo;
	private int fiordenGrup;
	private int tipo;
	private String nego;
	private String fecha;
	private String nomusu;
	//consulta nueva
	private int idtab;
	private String obs;
	private String fechaLibera;
	private String grupo;
	
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public String getNombreCheck() {
		return nombreCheck;
	}
	public void setNombreCheck(String nombreCheck) {
		this.nombreCheck = nombreCheck;
	}
	public long getRango() {
		return rango;
	}
	public void setRango(long rango) {
		this.rango = rango;
	}
	public String getFecha_inicio() {
		return fecha_inicio;
	}
	public void setFecha_inicio(String fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}
	public String getFecha_fin() {
		return fecha_fin;
	}
	public void setFecha_fin(String fecha_fin) {
		this.fecha_fin = fecha_fin;
	}
	public int getIdHorario() {
		return idHorario;
	}
	public void setIdHorario(int idHorario) {
		this.idHorario = idHorario;
	}
	public int getVigente() {
		return vigente;
	}
	public void setVigente(int vigente) {
		this.vigente = vigente;
	}
	public int getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}
	public int getIdCheckUsua() {
		return idCheckUsua;
	}
	public void setIdCheckUsua(int idCheckUsua) {
		this.idCheckUsua = idCheckUsua;
	}
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	public int getCommit() {
		return commit;
	}
	public void setCommit(int commit) {
		this.commit = commit;
	}
	public String getUltimaVisita() {
		return ultimaVisita;
	}
	public void setUltimaVisita(String ultimaVisita) {
		this.ultimaVisita = ultimaVisita;
	}
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public String getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(String usuarios) {
		this.usuarios = usuarios;
	}
	public String getNumVersion() {
		return numVersion;
	}
	public void setNumVersion(String numVersion) {
		this.numVersion = numVersion;
	}
	public String getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public boolean getIsPendiente() {
		return isPendiente;
	}
	public void setIsPendiente(boolean isPendiente) {
		this.isPendiente = isPendiente;
	}
	public boolean getIsRechazoCambios() {
		return isRechazoCambios;
	}
	public void setRechazoCambios(boolean isRechazoCambios) {
		this.isRechazoCambios = isRechazoCambios;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public void setPendiente(boolean isPendiente) {
		this.isPendiente = isPendiente;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getPeriodicidad() {
		return periodicidad;
	}
	public void setPeriodicidad(String periodicidad) {
		this.periodicidad = periodicidad;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public double getDistancia() {
		return distancia;
	}
	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}
	public String getValorIni() {
		return valorIni;
	}
	public void setValorIni(String valorIni) {
		this.valorIni = valorIni;
	}
	public String getValorFin() {
		return valorFin;
	}
	public void setValorFin(String valorFin) {
		this.valorFin = valorFin;
	}
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	
	public String getHoraFin() {
		return horaFin;
	}
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
	public String getFechaEnvio() {
		return fechaEnvio;
	}
	public void setFechaEnvio(String fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}
	public String getModo() {
		return modo;
	}
	public void setModo(String modo) {
		this.modo = modo;
	}
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getOrdeGrupo() {
		return ordeGrupo;
	}
	public void setOrdeGrupo(String ordeGrupo) {
		this.ordeGrupo = ordeGrupo;
	}
	
	public double getPonderacionTot() {
		return ponderacionTot;
	}
	public void setPonderacionTot(double ponderacionTot) {
		this.ponderacionTot = ponderacionTot;
	}
	
	
	public String getClasifica() {
		return clasifica;
	}
	public void setClasifica(String clasifica) {
		this.clasifica = clasifica;
	}
	
	public int getIdProtocolo() {
		return idProtocolo;
	}
	public void setIdProtocolo(int idProtocolo) {
		this.idProtocolo = idProtocolo;
	}
	
	public int getFiordenGrup() {
        return fiordenGrup;
    }

    public void setFiordenGrup(int fiordenGrup) {
        this.fiordenGrup = fiordenGrup;
    }
    
    
    public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	
	public String getNego() {
		return nego;
	}
	public void setNego(String nego) {
		this.nego = nego;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getNomusu() {
		return nomusu;
	}
	public void setNomusu(String nomusu) {
		this.nomusu = nomusu;
	}
	
	public int getIdtab() {
		return idtab;
	}
	public void setIdtab(int idtab) {
		this.idtab = idtab;
	}
	public String getObs() {
		return obs;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}
	public String getFechaLibera() {
		return fechaLibera;
	}
	public void setFechaLibera(String fechaLibera) {
		this.fechaLibera = fechaLibera;
	}
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	@Override
	public String toString() {
		return "CheckSoporteAdmDTO [idChecklist=" + idChecklist + ", idCheckUsua=" + idCheckUsua + ", nombreCheck="
				+ nombreCheck + ", zona=" + zona + ", rango=" + rango + ", fecha_inicio=" + fecha_inicio
				+ ", fecha_fin=" + fecha_fin + ", idHorario=" + idHorario + ", valorIni=" + valorIni + ", valorFin="
				+ valorFin + ", vigente=" + vigente + ", idEstado=" + idEstado + ", commit=" + commit
				+ ", ultimaVisita=" + ultimaVisita + ", fechaModificacion=" + fechaModificacion + ", usuarios="
				+ usuarios + ", numVersion=" + numVersion + ", tipoCambio=" + tipoCambio + ", isPendiente="
				+ isPendiente + ", isRechazoCambios=" + isRechazoCambios + ", periodo=" + periodo + ", idUsuario="
				+ idUsuario + ", dia=" + dia + ", periodicidad=" + periodicidad + ", estatus=" + estatus
				+ ", distancia=" + distancia + ", horaInicio=" + horaInicio + ", horaFin=" + horaFin + ", fechaEnvio="
				+ fechaEnvio + ", modo=" + modo + ", version=" + version + ", ordeGrupo=" + ordeGrupo
				+ ", ponderacionTot=" + ponderacionTot + ", clasifica=" + clasifica + ", idProtocolo=" + idProtocolo
				+ ", fiordenGrup=" + fiordenGrup + ", tipo=" + tipo + ", nego=" + nego + ", fecha=" + fecha
				+ ", nomusu=" + nomusu + ", idtab=" + idtab + ", obs=" + obs + ", fechaLibera=" + fechaLibera
				+ ", grupo=" + grupo + "]";
	}
	 
}
