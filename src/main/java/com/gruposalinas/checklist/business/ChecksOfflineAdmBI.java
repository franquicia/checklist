package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.ChecksOfflineAdmDAO;
import com.gruposalinas.checklist.domain.ChecksOfflineCompletosDTO;
import com.gruposalinas.checklist.domain.Usuario_ADTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class ChecksOfflineAdmBI {
	
	@Autowired
	ChecksOfflineAdmDAO checksOfflineAdmDAO;
	
	Logger logger = LogManager.getLogger(ChecksOfflineAdmBI.class);
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> obtieneChecksOffline(int idUsuario) {
		
		Map<String, Object> listas = null;
		
		try {
			listas = checksOfflineAdmDAO.obtieneChecks(idUsuario);
			if( listas.get("checklistsCompletos") != null){
				
				List<ChecksOfflineCompletosDTO> datosChecks = (List<ChecksOfflineCompletosDTO>)  listas.get("checklistsCompletos");
				List<ChecksOfflineCompletosDTO> listaAux = null;		

				//List<Object> listaTotal = new ArrayList<Object>();
				Map<String,Object> listaTotal = new HashMap<String, Object>();
				
				int anterior = 0;
				int cont = 0;
				
				while(cont < datosChecks.size() ){
					listaAux=new ArrayList<ChecksOfflineCompletosDTO>();
					anterior = datosChecks.get(cont).getIdCheckUsua();
							
					while(cont < datosChecks.size()  && anterior == datosChecks.get(cont).getIdCheckUsua() ){
						listaAux.add(datosChecks.get(cont));
						cont ++;
					}			
					listaTotal.put(String.valueOf(anterior), listaAux);
				}

				listas.remove("checklistsCompletos");				
				listas.put("checklistsCompletos", listaTotal);		
				
			}
			
			if (listas.get("listaUsuarios") != null){
				
				List<Usuario_ADTO> listaUsuarios = (List<Usuario_ADTO>) listas.get("listaUsuarios");
				List<Usuario_ADTO> listaAux1 = null;		

				List<Object> listaTotal1 = new ArrayList<Object>();
				
				String anterior1 = "";
				int cont1 = 0;
				
				while(cont1 < listaUsuarios.size() ){
					listaAux1=new ArrayList<Usuario_ADTO>();
					anterior1 =listaUsuarios.get(cont1).getIdCeco();
							
					while(cont1 < listaUsuarios.size()  && anterior1.equals(listaUsuarios.get(cont1).getIdCeco())){
						listaAux1.add(listaUsuarios.get(cont1));
						cont1 ++;
					}			
					listaTotal1.add(listaAux1);
					
				}
				listas.remove("listaUsuarios");
				listas.put("listaUsuarios",listaTotal1);
			}
			
			
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de los checklist offline");
				
			
		}
		
		return listas;
		
	}

	public Map<String, Object> obtieneChecksNuevo(int idUsuario)  {
		
		Map<String, Object> listas = null;
		
		try {
			listas = checksOfflineAdmDAO.obtieneChecksNuevo(idUsuario);
			
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de los checklist offline");
			logger.info("Algo paso offline: "+e);	
			
		}
		if (listas!=null){
			if(listas.size() == 0){
				return null;
			}
			else{
				return listas;		
			}
		}
		return listas;
	}
	
	public Map<String, Object> obtieneChecksNuevo3(int idUsuario,int idChecklist)  {
		
		Map<String, Object> listas = null;
		
		try {
			listas = checksOfflineAdmDAO.obtieneChecksNuevo3(idUsuario,idChecklist);
			
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de los checklist offline");
			logger.info("Algo paso offline: "+e);	
			
		}
		if (listas!=null){
			if(listas.size() == 0){
				return null;
			}
			else{
				return listas;		
			}
		}
		return listas;
	}
	
public Map<String, Object> obtieneChecksNuevo4(int idUsuario)  {
		
		Map<String, Object> listas = null;
		
		try {
			listas = checksOfflineAdmDAO.obtieneChecksNuevo4(idUsuario);
			
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de los checklist offline");
			logger.info("Algo paso offline: "+e);	
			
		}
		if (listas!=null){
			if(listas.size() == 0){
				return null;
			}
			else{
				return listas;		
			}
		}
		return listas;
	}


        public Map<String, Object> getBitacoras(int idUsuario,int idChecklist){
            Map<String, Object> listas = null;
            try {
                listas = checksOfflineAdmDAO.getBitacoras(idUsuario,idChecklist);
			
            } catch (Exception e) {
                logger.info("No fue posible obtener datos de los checklist offline");
		logger.info("Algo paso offline: "+e);	
			
            }
            if (listas!=null){
                if(listas.size() == 0){
                    return null;	
                }
                else{
                    return listas;			
                }
            }	
            return listas;
            
        }

}
