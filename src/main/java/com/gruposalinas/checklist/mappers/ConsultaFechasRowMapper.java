package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ConsultaFechasDTO;

public class ConsultaFechasRowMapper implements RowMapper<ConsultaFechasDTO> {

	public ConsultaFechasDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ConsultaFechasDTO  consultaFechasDTO = new ConsultaFechasDTO();
		
		consultaFechasDTO.setFecha(rs.getString("FDFECHA"));
		consultaFechasDTO.setAnio(rs.getString("FIANIO"));
		consultaFechasDTO.setMes(rs.getString("FIMES"));
		consultaFechasDTO.setTrimestre(rs.getString("FITRIMESTRE"));
		consultaFechasDTO.setSemana(rs.getString("FISEMANA"));
		consultaFechasDTO.setNumeroDiaSemana(rs.getString("FINUMDIASEM"));
		consultaFechasDTO.setNumDia(rs.getString("FINUMDIA"));
		consultaFechasDTO.setDiaFestivo(rs.getString("FIDIAFESTIVO"));
		consultaFechasDTO.setUsuarioMod(rs.getString("FCUSUARIO_MOD"));
		consultaFechasDTO.setFechaMod(rs.getString("FCUSUARIO_MOD"));
                consultaFechasDTO.setBimestre(rs.getString("FIBIMESTRE"));
                
		return consultaFechasDTO;
	}

}
