/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.domain;

/**
 *
 * @author kramireza
 */
public class CecoPasoDTO {

    String fccid;
    Integer fientidadid;
    Integer finum_economico;
    String fcnombrecc;
    String fcnombre_ent;
    String fcccid_padre;
    Integer fientdid_padre;
    String fcnombrecc_pad;
    String fctipocanal;
    String fcnombtipcanal;
    String fccanal;
    Integer fistatusccid;
    String fcstatuscc;
    Integer fitipocc;
    Integer fiestadoid;
    String fcmunicipio;
    String fctipooperacion;
    String fctiposucursal;
    String fcnombretiposuc;
    String fccalle;
    Integer firesponsableid;
    String fcresponsable;
    String fccp;
    String fctelefonos;
    String ficlasif_sieid;
    String fcclasif_sie;
    Integer figastoxnegid;
    String fcgastoxnegocio;
    String fdapertura;
    String fdcierre;
    Integer fipaisid;
    String fcpais;
    String fdcarga;
    Integer fiidunnego;
    String fcdescunbnego;
    Integer fiidsubnego;
    String fcdescsubnego;
    String fcusuario_mod;
    String fdfecha_mod;

    public CecoPasoDTO() {
    }

    public String getFccid() {
        return fccid;
    }

    public void setFccid(String fccid) {
        this.fccid = fccid;
    }

    public Integer getFientidadid() {
        return fientidadid;
    }

    public void setFientidadid(Integer fientidadid) {
        this.fientidadid = fientidadid;
    }

    public Integer getFinum_economico() {
        return finum_economico;
    }

    public void setFinum_economico(Integer finum_economico) {
        this.finum_economico = finum_economico;
    }

    public String getFcnombrecc() {
        return fcnombrecc;
    }

    public void setFcnombrecc(String fcnombrecc) {
        this.fcnombrecc = fcnombrecc;
    }

    public String getFcnombre_ent() {
        return fcnombre_ent;
    }

    public void setFcnombre_ent(String fcnombre_ent) {
        this.fcnombre_ent = fcnombre_ent;
    }

    public String getFcccid_padre() {
        return fcccid_padre;
    }

    public void setFcccid_padre(String fcccid_padre) {
        this.fcccid_padre = fcccid_padre;
    }

    public Integer getFientdid_padre() {
        return fientdid_padre;
    }

    public void setFientdid_padre(Integer fientdid_padre) {
        this.fientdid_padre = fientdid_padre;
    }

    public String getFcnombrecc_pad() {
        return fcnombrecc_pad;
    }

    public void setFcnombrecc_pad(String fcnombrecc_pad) {
        this.fcnombrecc_pad = fcnombrecc_pad;
    }

    public String getFctipocanal() {
        return fctipocanal;
    }

    public void setFctipocanal(String fctipocanal) {
        this.fctipocanal = fctipocanal;
    }

    public String getFcnombtipcanal() {
        return fcnombtipcanal;
    }

    public void setFcnombtipcanal(String fcnombtipcanal) {
        this.fcnombtipcanal = fcnombtipcanal;
    }

    public String getFccanal() {
        return fccanal;
    }

    public void setFccanal(String fccanal) {
        this.fccanal = fccanal;
    }

    public Integer getFistatusccid() {
        return fistatusccid;
    }

    public void setFistatusccid(Integer fistatusccid) {
        this.fistatusccid = fistatusccid;
    }

    public String getFcstatuscc() {
        return fcstatuscc;
    }

    public void setFcstatuscc(String fcstatuscc) {
        this.fcstatuscc = fcstatuscc;
    }

    public Integer getFitipocc() {
        return fitipocc;
    }

    public void setFitipocc(Integer fitipocc) {
        this.fitipocc = fitipocc;
    }

    public Integer getFiestadoid() {
        return fiestadoid;
    }

    public void setFiestadoid(Integer fiestadoid) {
        this.fiestadoid = fiestadoid;
    }

    public String getFcmunicipio() {
        return fcmunicipio;
    }

    public void setFcmunicipio(String fcmunicipio) {
        this.fcmunicipio = fcmunicipio;
    }

    public String getFctipooperacion() {
        return fctipooperacion;
    }

    public void setFctipooperacion(String fctipooperacion) {
        this.fctipooperacion = fctipooperacion;
    }

    public String getFctiposucursal() {
        return fctiposucursal;
    }

    public void setFctiposucursal(String fctiposucursal) {
        this.fctiposucursal = fctiposucursal;
    }

    public String getFcnombretiposuc() {
        return fcnombretiposuc;
    }

    public void setFcnombretiposuc(String fcnombretiposuc) {
        this.fcnombretiposuc = fcnombretiposuc;
    }

    public String getFccalle() {
        return fccalle;
    }

    public void setFccalle(String fccalle) {
        this.fccalle = fccalle;
    }

    public Integer getFiresponsableid() {
        return firesponsableid;
    }

    public void setFiresponsableid(Integer firesponsableid) {
        this.firesponsableid = firesponsableid;
    }

    public String getFcresponsable() {
        return fcresponsable;
    }

    public void setFcresponsable(String fcresponsable) {
        this.fcresponsable = fcresponsable;
    }

    public String getFccp() {
        return fccp;
    }

    public void setFccp(String fccp) {
        this.fccp = fccp;
    }

    public String getFctelefonos() {
        return fctelefonos;
    }

    public void setFctelefonos(String fctelefonos) {
        this.fctelefonos = fctelefonos;
    }

    public String getFiclasif_sieid() {
        return ficlasif_sieid;
    }

    public void setFiclasif_sieid(String ficlasif_sieid) {
        this.ficlasif_sieid = ficlasif_sieid;
    }

    public String getFcclasif_sie() {
        return fcclasif_sie;
    }

    public void setFcclasif_sie(String fcclasif_sie) {
        this.fcclasif_sie = fcclasif_sie;
    }

    public Integer getFigastoxnegid() {
        return figastoxnegid;
    }

    public void setFigastoxnegid(Integer figastoxnegid) {
        this.figastoxnegid = figastoxnegid;
    }

    public String getFcgastoxnegocio() {
        return fcgastoxnegocio;
    }

    public void setFcgastoxnegocio(String fcgastoxnegocio) {
        this.fcgastoxnegocio = fcgastoxnegocio;
    }

    public String getFdapertura() {
        return fdapertura;
    }

    public void setFdapertura(String fdapertura) {
        this.fdapertura = fdapertura;
    }

    public String getFdcierre() {
        return fdcierre;
    }

    public void setFdcierre(String fdcierre) {
        this.fdcierre = fdcierre;
    }

    public Integer getFipaisid() {
        return fipaisid;
    }

    public void setFipaisid(Integer fipaisid) {
        this.fipaisid = fipaisid;
    }

    public String getFcpais() {
        return fcpais;
    }

    public void setFcpais(String fcpais) {
        this.fcpais = fcpais;
    }

    public String getFdcarga() {
        return fdcarga;
    }

    public void setFdcarga(String fdcarga) {
        this.fdcarga = fdcarga;
    }

    public Integer getFiidunnego() {
        return fiidunnego;
    }

    public void setFiidunnego(Integer fiidunnego) {
        this.fiidunnego = fiidunnego;
    }

    public String getFcdescunbnego() {
        return fcdescunbnego;
    }

    public void setFcdescunbnego(String fcdescunbnego) {
        this.fcdescunbnego = fcdescunbnego;
    }

    public Integer getFiidsubnego() {
        return fiidsubnego;
    }

    public void setFiidsubnego(Integer fiidsubnego) {
        this.fiidsubnego = fiidsubnego;
    }

    public String getFcdescsubnego() {
        return fcdescsubnego;
    }

    public void setFcdescsubnego(String fcdescsubnego) {
        this.fcdescsubnego = fcdescsubnego;
    }

    public String getFcusuario_mod() {
        return fcusuario_mod;
    }

    public void setFcusuario_mod(String fcusuario_mod) {
        this.fcusuario_mod = fcusuario_mod;
    }

    public String getFdfecha_mod() {
        return fdfecha_mod;
    }

    public void setFdfecha_mod(String fdfecha_mod) {
        this.fdfecha_mod = fdfecha_mod;
    }

    @Override
    public String toString() {
        return "CecoPasoDTO{" + "fccid=" + fccid + ", fientidadid=" + fientidadid + ", finum_economico=" + finum_economico + ", fcnombrecc=" + fcnombrecc + ", fcnombre_ent=" + fcnombre_ent + ", fcccid_padre=" + fcccid_padre + ", fientdid_padre=" + fientdid_padre + ", fcnombrecc_pad=" + fcnombrecc_pad + ", fctipocanal=" + fctipocanal + ", fcnombtipcanal=" + fcnombtipcanal + ", fccanal=" + fccanal + ", fistatusccid=" + fistatusccid + ", fcstatuscc=" + fcstatuscc + ", fitipocc=" + fitipocc + ", fiestadoid=" + fiestadoid + ", fcmunicipio=" + fcmunicipio + ", fctipooperacion=" + fctipooperacion + ", fctiposucursal=" + fctiposucursal + ", fcnombretiposuc=" + fcnombretiposuc + ", fccalle=" + fccalle + ", firesponsableid=" + firesponsableid + ", fcresponsable=" + fcresponsable + ", fccp=" + fccp + ", fctelefonos=" + fctelefonos + ", ficlasif_sieid=" + ficlasif_sieid + ", fcclasif_sie=" + fcclasif_sie + ", figastoxnegid=" + figastoxnegid + ", fcgastoxnegocio=" + fcgastoxnegocio + ", fdapertura=" + fdapertura + ", fdcierre=" + fdcierre + ", fipaisid=" + fipaisid + ", fcpais=" + fcpais + ", fdcarga=" + fdcarga + ", fiidunnego=" + fiidunnego + ", fcdescunbnego=" + fcdescunbnego + ", fiidsubnego=" + fiidsubnego + ", fcdescsubnego=" + fcdescsubnego + ", fcusuario_mod=" + fcusuario_mod + ", fdfecha_mod=" + fdfecha_mod + '}';
    }

}
