package com.gruposalinas.checklist.controller;

import java.io.IOException;
import java.security.KeyException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gruposalinas.checklist.business.IncidentesBI;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Controller
@RequestMapping("/incidentes")
public class IncidentesController {

	@Autowired
	IncidentesBI incidenteBi;

	private Logger logger = LogManager.getLogger(IncidentesController.class);

	//formato fechas yyyy-mm-dd hh:mm:ss
	//http://10.51.210.239:8080/checklist/incidentes/getReporteMotivos.json?fechaInicio=&fechaFin=
	//http://localhost:8080/checklist/incidentes/getReporteMotivos.json?fechaInicio=<?>&fechaFin=<?>&motivo=<?>
	@RequestMapping(value = "/getReporteMotivos", method = RequestMethod.GET)
	public @ResponseBody String getReporteMotivos(HttpServletRequest request, HttpServletResponse response)
			throws KeyException, IOException {

		String json = "";
		String fechaInicio = "";
		String fechaFin ="";
		String motivo="";

		try {
			fechaInicio = request.getParameter("fechaInicio").replace("%20", " ");
			fechaFin = request.getParameter("fechaFin").replace("%20", " ");
			motivo = request.getParameter("motivo").replace("%20", " ");
			json = incidenteBi.getIncidentesPorMotivo(fechaInicio, fechaFin,motivo);
			
		} catch (Exception e) {
			logger.info("ocurrio algo :"+e);
			json = "";
		}

		logger.info("json:" + json);
		return json;
	}	

	//formato fechas yyyy-mm-dd hh:mm:ss
	// http://localhost:8080/checklist/incidentes/getInformesIncidentes.json?estatus=<?>&motivo=<?>&fechaInicio=<?>&fechaFin=<?>&director=<?>
	@RequestMapping(value = "/getInformesIncidentes", method = RequestMethod.GET)
	public @ResponseBody String getInformesIncidentes(HttpServletRequest request, HttpServletResponse response)
			throws KeyException, IOException {
		String json = "";

		String estatus = "";
		String motivo = "";
		String fechaInicio = "";
		String fechaFin ="";
		String director = "";

		try {
			estatus = request.getParameter("estatus");

			motivo = request.getParameter("motivo");;

			fechaInicio =request.getParameter("fechaInicio").replace("%20", " ");

			fechaFin = request.getParameter("fechaFin").replace("%20", " ");

			director = request.getParameter("director").replace("%20", " ");

			json = incidenteBi.getInformes(estatus, motivo, fechaInicio, fechaFin, director);

		} catch (Exception e) {
			logger.info("ocurrio algo :"+e);
			json = "";
		}

		logger.info("json:" + json);
		return json;
	}	
	
	//formato fechas yyyy-mm-dd hh:mm:ss
	//http://10.51.210.239:8080/checklist/incidentes/getReporteMotivos.json?fechaInicio=&fechaFin=
	//http://localhost:8080/checklist/incidentes/getReporteDirectores.json?fechaInicio=<?>&fechaFin=<?>&motivo=<?>
	@RequestMapping(value = "/getReporteDirectores", method = RequestMethod.GET)
	public @ResponseBody String getReporteDirectores(HttpServletRequest request, HttpServletResponse response)
			throws KeyException, IOException {

		String json = "";
		String fechaInicio = "";
		String fechaFin ="";
		String motivo="";

		try {
			fechaInicio = request.getParameter("fechaInicio").replace("%20", " ");
			fechaFin = request.getParameter("fechaFin").replace("%20", " ");
			motivo = request.getParameter("motivo").replace("%20", " ");
			json = incidenteBi.getIncidentesByDirector(fechaInicio, fechaFin, motivo);
			
		} catch (Exception e) {
			logger.info("ocurrio algo :"+e);
			json = "";
		}

		logger.info("json:" + json);
		return json;
	}
}
