package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.PlantillaDTO;


public interface PlantillaDAO {

	public List<PlantillaDTO> obtienePlantilla(String idPlantilla) throws Exception;
	
	public int insertaPlantilla(PlantillaDTO bean) throws Exception;
	
	public boolean actualizaPlantilla(PlantillaDTO bean) throws Exception;
	
	public boolean eliminaPlantilla(int idPlantilla) throws Exception;
	

	public List<PlantillaDTO> obtieneElementoP(String idPlantilla, String idElemento, String tipo) throws Exception;
	
	public int insertaElementoP(PlantillaDTO bean) throws Exception;
	
	public boolean actualizaElementoP(PlantillaDTO bean) throws Exception;
	
	public boolean eliminaElementoP(int idElemento) throws Exception;
}
