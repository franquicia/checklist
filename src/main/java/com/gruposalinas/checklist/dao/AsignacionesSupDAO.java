package com.gruposalinas.checklist.dao;

import java.util.List;
import com.gruposalinas.checklist.domain.AsignacionesSupDTO;

public interface AsignacionesSupDAO {
	
	public int insertAsignacion(int idUsuario, int idPerfil, String idCeco, int idGerente, int activo, String fechaInicio, String fechaFin) throws Exception;
	public int updateAsignacion(int idUsuario, int idPerfil, String idCeco, int idGerente, int activo, String fechaInicio, String fechaFin) throws Exception;
	public int deleteAsignacion(int idUsuario, String idCeco) throws Exception;
	public List<AsignacionesSupDTO> getAsignaciones(String idUsuario, String idPerfil, String idCeco, String idGerente, String activo, String fechaInicio, String fechaFin) throws Exception;
	public int updateStatus() throws Exception;
	public int mergeAsignaciones(int idUsuario, int idPerfil, String idCeco, String idGerente, int activo, String fechaInicio, String fechaFin) throws Exception;
	List<AsignacionesSupDTO> getAsignacionesByUsr(String idUsuario,String activo) throws Exception;
        
}
