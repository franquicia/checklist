package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.CecoDTO;

public class CecoRowMapper implements RowMapper<CecoDTO>{

	public CecoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		CecoDTO cecoDTO = new CecoDTO();
		
		cecoDTO.setIdCeco(rs.getString("FCID_CECO"));
		cecoDTO.setIdPais(rs.getInt("FIID_PAIS"));
		cecoDTO.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
		cecoDTO.setIdCanal(rs.getInt("FIID_CANAL"));
		cecoDTO.setIdEstado(rs.getInt("FIID_ESTADO"));
		cecoDTO.setDescCeco(rs.getString("FCNOMBRE"));
		cecoDTO.setIdCecoSuperior(rs.getInt("FICECO_SUPERIOR"));
		cecoDTO.setActivo(rs.getInt("FIACTIVO"));
		cecoDTO.setIdNivel(rs.getInt("FIID_NIVEL"));
		cecoDTO.setCalle(rs.getString("FCCALLE"));
		cecoDTO.setCiudad(rs.getString("FCCIUDAD"));
		cecoDTO.setCp(rs.getString("FCCP"));
		cecoDTO.setNombreContacto(rs.getString("FCNOMBRE_CTO"));
		cecoDTO.setPuestoContacto(rs.getString("FCPUESTO_CTO"));
		cecoDTO.setTelefonoContacto(rs.getString("FCTELEFONO_CTO"));
		cecoDTO.setFaxContacto(rs.getString("FCFAX_CTO"));
		cecoDTO.setUsuarioModifico(rs.getString("FCUSUARIO_MOD"));
		cecoDTO.setFechaModifico(rs.getString("FDFECHA_MOD"));
		
		
		return cecoDTO;
	}
	
	
	
	
	

}
