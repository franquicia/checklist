package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.CheckInAsistenciaDTO;
import com.gruposalinas.checklist.domain.ProgramacionMttoDTO;
import com.gruposalinas.checklist.mappers.CheckInAsistenciaRowMapper;
import com.gruposalinas.checklist.mappers.ProgramacionMttoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class CheckInAsistenciaDAOImpl extends DefaultDAO implements CheckInAsistenciaDAO {

    private static Logger logger = LogManager.getLogger(CheckInAsistenciaDAOImpl.class);

    DefaultJdbcCall jdbcInsertaAsistencia;
    DefaultJdbcCall jdbcEliminaAsistencia;
    DefaultJdbcCall jdbcObtieneAsistencia;
    DefaultJdbcCall jbdcActualizaAsistencia;
    DefaultJdbcCall jbdcObtieneProgramacion;
    DefaultJdbcCall jbdcActualizaProgramacion;

    public void init() {

        jdbcInsertaAsistencia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ASISTEN")
                .withProcedureName("SP_INSASIST");

        jdbcEliminaAsistencia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ASISTEN")
                .withProcedureName("SP_ELIMINAASIST");

        jdbcObtieneAsistencia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ASISTEN")
                .withProcedureName("SP_SELASISTEN")
                .returningResultSet("RCL_ASIST", new CheckInAsistenciaRowMapper());

        jbdcActualizaAsistencia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ASISTEN")
                .withProcedureName("SP_ACTASIST");

        jbdcActualizaProgramacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAFLUJOMMTO")
                .withProcedureName("SP_ACTPROGRAMA");

        jbdcObtieneProgramacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_REPOMTTO")
                .withProcedureName("SP_PROGRAM")
                .returningResultSet("RCL_PROG", new ProgramacionMttoRowMapper());

    }

    public int insertaAsistencia(CheckInAsistenciaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_LATITUD", bean.getLatitud())
                .addValue("PA_LONGITUD", bean.getLongitud())
                .addValue("PA_FECHA", bean.getFecha())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_OBSERVACIONES", bean.getObservaciones())
                .addValue("PA_LUGARASIST", bean.getLugar())
                //es 1 entrada 2 salida 0 N/A
                .addValue("PA_TIPOASIST", bean.getTipoCheckIn())
                .addValue("PA_TIPOPROY", bean.getIdTipoProyecto());

        out = jdbcInsertaAsistencia.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADM_ASISTEN.PA_IDCHEKASIST}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDCHEKASIST");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el Empleado");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean eliminaAsistencia(String idAsistencia) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCHEKASIST", idAsistencia);

        out = jdbcEliminaAsistencia.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADM_ASISTEN.SP_DEL_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el asistencia(" + idAsistencia + ")");
        } else {
            return true;
        }

        return false;

    }

    //parametro
    @SuppressWarnings("unchecked")
    public List<CheckInAsistenciaDTO> obtieneDatosAsistencia(String fechaEntrada, String fechaSalida, String idUsuario) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<CheckInAsistenciaDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FECHAENTRADA", fechaEntrada)
                .addValue("PA_FECHASALIDA", fechaSalida)
                .addValue("PA_USUARIO", idUsuario);

        out = jdbcObtieneAsistencia.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADM_ASISTEN.SP_SEL_EMP}");
        //lleva el nombre del cursor del procedure
        lista = (List<CheckInAsistenciaDTO>) out.get("RCL_ASIST");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el empleado(" + idUsuario + ")");
        }
        return lista;

    }

    public boolean actualizaAsistencia(CheckInAsistenciaDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_LATITUD", bean.getLatitud())
                .addValue("PA_LONGITUD", bean.getLongitud())
                .addValue("PA_FECHA", bean.getFecha())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_OBSERVACIONES", bean.getObservaciones())
                .addValue("PA_LUGARASIST", bean.getLugar())
                .addValue("PA_TIPOPROY", bean.getIdTipoProyecto())
                //es 0 entrada 1 salida
                .addValue("PA_TIPOASIST", bean.getTipoCheckIn())
                .addValue("PA_IDCHEKASIST", bean.getIdAsistencia());

        out = jbdcActualizaAsistencia.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADM_ASISTEN.SP_ACT_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdAsistencia() + ")");
        } else {
            return true;
        }
        return false;

    }

    public boolean actualizaProgramacion(ProgramacionMttoDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPROG", bean.getIdProgramacion())
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_COMENTREPROG", bean.getComentarioReprogramacion())
                .addValue("PA_FECHAPROG", bean.getFechaProgramacion());

        out = jbdcActualizaProgramacion.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAFLUJOMMTO.SP_ACTPROGRAMA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id PROGRAMACION ( " + bean.getIdProgramacion() + ")");
        } else {
            return true;
        }
        return false;

    }

    @SuppressWarnings("unchecked")
    public List<ProgramacionMttoDTO> obtieneProgramaciones(String idUsuario) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ProgramacionMttoDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDUSUARIO", idUsuario);

        out = jbdcObtieneProgramacion.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PA_REPOMTTO.SP_SELPROGRAMACIONES}");
        //lleva el nombre del cursor del procedure
        lista = (List<ProgramacionMttoDTO>) out.get("RCL_PROG");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el empleado(" + idUsuario + ")");
        }
        return lista;

    }

}
