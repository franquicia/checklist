package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.BitacoraDTO;

public class BitacorasCerradasRowMapper implements RowMapper<BitacoraDTO>{

	public BitacoraDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		BitacoraDTO bitacoraDTO = new BitacoraDTO();

		bitacoraDTO.setIdCheckUsua(rs.getInt("FIID_CHECK_USUA"));
		bitacoraDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
		bitacoraDTO.setUsuario(rs.getString("FCNOMBRE"));
		bitacoraDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		bitacoraDTO.setFechaInicio(rs.getString("FDINICIO"));
		bitacoraDTO.setFechaFin(rs.getString("FDTERMINO"));
		
		return bitacoraDTO;
	}

}
