<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>Visita de sucursal</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervision/estilos.css">
  <!-- Tablas/Paginado -->
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervision/paginador.css">
</head>

<body ng-app="app" ng-controller="validarForm">

  <div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="${pageContext.request.contextPath}/img/supervision/btn_hamburguer.svg" alt="Men� principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="inicio.htm"><img src="${pageContext.request.contextPath}/img/supervision/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.htm" class="selMenu">
          <div>Salir</div>
          <div><img src="${pageContext.request.contextPath}/img/supervision/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="indexSupervision.htm">Reporte de Supervisores</a> /
			<a href="#">B�squeda</a>
		</div>
    	Visita de Sucursal
    </div>

	<!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervision/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Men� Principal<br>
          </div>
        </div>
        <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la p�gina">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div>
        <div id="menu">
          <ul class="l-navegacion nivel1">
          		<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>B�squeda de Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>B�squeda de Supervisor</div></a>
				</li>
			</ul>
        </div>
      </div>
    </div>

    <!-- Contenido -->
    <div class="contSecc">
		<div class="titSec">Coincidencias de tu b�squeda</div>
		<div class="gris">
			<table class="tblGeneral ">
	            <thead>
	              <tr>
	                <th>Nombre de Sucursal</th>
	                <th>Fecha de Inicio</th>
	                <th>Fecha de Fin</th>
	                <th>Supervisor</th>
	              </tr>
	            </thead>
	            <tbody>
	              <tr>
	              	<td><u><a href="detalleProtocoloSupervision.htm" class="txtVerde">Mega Elektra La Morita</a></u></td>
	              	<td>15/09/2019</td>
	              	<td>02/12/2019</td>
	              	<td>Jos� Alfredo Almaguer Villa</td>
	              </tr>
	              <tr>
	              	<td><u><a href="detalleProtocoloSupervision.htm" class="txtVerde">Mega Elektra La Morita</a></u></td>
	              	<td>15/09/2019</td>
	              	<td>02/12/2019</td>
	              	<td>Jos� Alfredo Almaguer Villa</td>
	              </tr>
	              <tr>
	              	<td><u><a href="detalleProtocoloSupervision.htm" class="txtVerde">Mega Elektra La Morita</a></u></td>
	              	<td>15/09/2019</td>
	              	<td>02/12/2019</td>
	              	<td>Jos� Alfredo Almaguer Villa</td>
	              </tr>
	              <tr>
	              	<td><u><a href="detalleProtocoloSupervision.htm" class="txtVerde">Mega Elektra La Morita</a></u></td>
	              	<td>15/09/2019</td>
	              	<td>02/12/2019</td>
	              	<td>Jos� Alfredo Almaguer Villa</td>
	              </tr>
	            </tbody>
	        </table>
		</div>
		
	</div>	<!-- Fin conSecc -->
	
	<!-- Footer -->
	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualizaci�n Febrero 2019</div>
	</div>
</div><!-- Fin page -->
</body>
</html>


<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/custom-file-input.js"></script><!--Unpload -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/funciones.js"></script>
<!-- Tablas/Paginado -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/paginador.js"></script> 
<script>
$(document).ready(function() {
    $('#registro').DataTable({searching: false, paging: true, info: true, ordering: false, pageLength : 5, lengthMenu: [[5], [5]]});
} );
</script>

    