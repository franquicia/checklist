package com.gruposalinas.checklist.domain;

public class NumeroTiendasDTO {
	
	private String ceco;
	private String nombreCeco;
	private int numTiendas;
	
	public String getCeco() {
		return ceco;
	}
	public void setCeco(String ceco) {
		this.ceco = ceco;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	public int getNumTiendas() {
		return numTiendas;
	}
	public void setNumTiendas(int numTiendas) {
		this.numTiendas = numTiendas;
	}
	
	

}
