package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.VersionProtoCheckDTO;
import com.gruposalinas.checklist.mappers.VersioneProtoCheckRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class VersionProtoCheckDAOImpl extends DefaultDAO implements VersionProtoCheckDAO {

    private static Logger logger = LogManager.getLogger(VersionProtoCheckDAOImpl.class);

    DefaultJdbcCall jdbcInsertaVers;
    DefaultJdbcCall jdbcEliminaVers;
    DefaultJdbcCall jdbcObtieneVers;
    DefaultJdbcCall jbdcActualizaVers;

    public void init() {

        jdbcInsertaVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECKXPRO")
                .withProcedureName("SPINSCHECKXPROTC");

        jdbcEliminaVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECKXPRO")
                .withProcedureName("SPDELCHECKXPROTC");

        jdbcObtieneVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECKXPRO")
                .withProcedureName("SPGETCHECKXPROTC")
                .returningResultSet("PA_CDATOS", new VersioneProtoCheckRowMapper());

        jbdcActualizaVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECKXPRO")
                .withProcedureName("SPACTCHECKXPROTC");

    }

    public boolean insertaVersion(VersionProtoCheckDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        boolean result = true;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECKLIST", bean.getIdChecklist())
                .addValue("PA_FIID_PROTOCOLO", bean.getIdProtocolo())
                .addValue("PA_FDFECHAINICIO", bean.getFecha())
                .addValue("PA_FIVIGENTE", bean.getVigente());

        out = jdbcInsertaVers.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMCHECKXPRO.SPINSCHECKXPROTC}");

        BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar la version protocolo dashboard");
            result = true;

        } else {
            return result;
        }

        return result;
    }

    public boolean eliminaVersion(VersionProtoCheckDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PROTOCOLO", bean.getIdProtocolo())
                .addValue("PA_FIID_CHECKLIST", bean.getIdChecklist());

        out = jdbcEliminaVers.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMCHECKXPRO.SPDELCHECKXPROTC}");

        BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al borrar el idProtocolo(" + bean.getIdProtocolo() + ") y checklist(" + bean.getIdChecklist() + ")");
        } else {
            return true;
        }

        return false;

    }

    @SuppressWarnings("unchecked")
    public List<VersionProtoCheckDTO> consultaVersion(VersionProtoCheckDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<VersionProtoCheckDTO> listaN = null;

        Long auxParam = new Long(bean.getIdProtocolo());
        Long ssd = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_PROTOCOLO", auxParam)
                .addValue("PA_FIID_CHECKLIST", ssd);

        out = jdbcObtieneVers.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMCHECKXPRO.SPGETCHECKXPROTC}");
        //lleva el nombre del cursor del procedure
        listaN = (List<VersionProtoCheckDTO>) out.get("PA_CDATOS");

        if (listaN != null) {
            if (listaN.size() > 0) {
                error = 1;
            }

        }

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la version protocolo(" + bean.getIdProtocolo() + ") y/o checklist(" + bean.getIdChecklist() + ")");
        }
        return listaN;

    }

    public boolean actualizaVersion(VersionProtoCheckDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_PROTOCOLO", bean.getIdProtocolo())
                .addValue("PA_FIID_CHECKLIST", bean.getIdChecklist())
                .addValue("PA_FDFECHAINICIO", bean.getFecha())
                .addValue("PA_FIVIGENTE", bean.getVigente());

        out = jbdcActualizaVers.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMCHECKXPRO.SPACTCHECKXPROTC}");

        BigDecimal resultado = (BigDecimal) out.get("PA_NRESEJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al actualizar el idProtocolo(" + bean.getIdProtocolo() + ") y checklist(" + bean.getIdChecklist() + ")");
        } else {
            return true;
        }
        return false;

    }

}
