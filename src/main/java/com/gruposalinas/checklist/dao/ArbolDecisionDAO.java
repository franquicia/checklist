package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ArbolDecisionDTO;

public interface ArbolDecisionDAO {
	
	public int insertaArbolDecision(ArbolDecisionDTO bean) throws Exception;
	
	public boolean actualizaArbolDecision(ArbolDecisionDTO bean) throws Exception;
	
	public boolean eliminaArbolDecision(int idArbolDecision) throws Exception;
	
	public List<ArbolDecisionDTO> buscaArbolDecision(int idChecklist) throws Exception;
	
	public boolean cambiaRespuestasPorId() throws Exception;
	
}
