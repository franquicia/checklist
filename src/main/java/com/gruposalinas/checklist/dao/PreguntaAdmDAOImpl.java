package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.mappers.ChecklistGeneralRowMapper;
import com.gruposalinas.checklist.mappers.ChecklistPreguntaRowMapper;
import com.gruposalinas.checklist.mappers.ModuloRowMapper;
import com.gruposalinas.checklist.mappers.PreguntaRowMapper;
import com.gruposalinas.checklist.mappers.PreguntasXCheklistRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PreguntaAdmDAOImpl extends DefaultDAO implements PreguntaAdmDAO {

    private static Logger logger = LogManager.getLogger(PreguntaDTO.class);

    //MODULOS
    DefaultJdbcCall jdbcObtieneModulos;
    DefaultJdbcCall jdbcObtieneModulo;
    DefaultJdbcCall jdbcInsertaModulo;
    DefaultJdbcCall jdbcActualizaModulo;
    DefaultJdbcCall jdbcEliminaModulo;
    //CHECKLIST
    private DefaultJdbcCall jdbcInsertaChecklistCom;
    private DefaultJdbcCall jdbcInsertaChecklist;
    private DefaultJdbcCall jdbcActualizaChecklistCom;
    private DefaultJdbcCall jdbcEliminaChecklist;
    private DefaultJdbcCall jdbcBuscaChecklist;
    private DefaultJdbcCall jdbcBuscaChecklists;
    //PREGUNTAS
    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtienePregunta;
    DefaultJdbcCall jdbcInsertaPregunta;
    DefaultJdbcCall jdbcInsertaPreguntaCom;
    DefaultJdbcCall jdbcActualizaPreguntaCom;
    DefaultJdbcCall jdbcActualizaPregunta;
    DefaultJdbcCall jdbcEliminaPregunta;
//CHECK_PREG
    private DefaultJdbcCall insertaChecklistPregunta;
    private DefaultJdbcCall actualizaChecklistPregunta;
    private DefaultJdbcCall eliminaChecklistPregunta;
    private DefaultJdbcCall buscaChecklistPregunta;
    private DefaultJdbcCall obtienePreguntas;

    public void init() {
//MODULOS
        jdbcObtieneModulos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_MODULOADM")
                .withProcedureName("SP_SEL_G_MODULO")
                .returningResultSet("RCL_MODULO", new ModuloRowMapper());

        jdbcObtieneModulo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_MODULOADM")
                .withProcedureName("SP_SEL_MODULO")
                .returningResultSet("RCL_MODULO", new ModuloRowMapper());

        jdbcInsertaModulo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_MODULOADM")
                .withProcedureName("SP_INS_MODULO");

        jdbcActualizaModulo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_MODULOADM")
                .withProcedureName("SP_ACT_MODULO");

        jdbcEliminaModulo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_MODULOADM")
                .withProcedureName("SP_DEL_MODULO");

//CHECKLIST
        jdbcInsertaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKADM")
                .withProcedureName("SP_INS_CHECK");
        //checklist con ponderacion total
        jdbcInsertaChecklistCom = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKADM")
                .withProcedureName("SP_INS_CHECKCOM");

        jdbcActualizaChecklistCom = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKADM")
                .withProcedureName("SP_ACT_CHECKCOM");

        jdbcEliminaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKADM")
                .withProcedureName("SP_DEL_CHECK");

        jdbcBuscaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKADM")
                .withProcedureName("SP_SEL_CHECK")
                .returningResultSet("RCL_CHECK", new ChecklistGeneralRowMapper());

        jdbcBuscaChecklists = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKADM")
                .withProcedureName("SP_SEL_G_CK")
                .returningResultSet("RCL_CHECK", new ChecklistGeneralRowMapper());

//PREGUNTAS
        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_PREGADM")
                .withProcedureName("SP_SEL_G_PREG")
                .returningResultSet("RCL_PREG", new PreguntaRowMapper());

        jdbcObtienePregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_PREGADM")
                .withProcedureName("SP_SEL_PREG")
                .returningResultSet("RCL_PREG", new PreguntaRowMapper());

        jdbcInsertaPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_PREGADM")
                .withProcedureName("SP_INS_PREG");
        //preguntas Imperdonables
        jdbcInsertaPreguntaCom = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_PREGADM")
                .withProcedureName("SP_INS_PREG_N");
        //act preg imp
        jdbcActualizaPreguntaCom = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_PREGADM")
                .withProcedureName("SP_ACT_PREG_N");

        jdbcActualizaPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_PREGADM")
                .withProcedureName("SP_ACT_PREG");

        jdbcEliminaPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_PREGADM")
                .withProcedureName("SP_DEL_PREG");
//CHECK_PREG

        insertaChecklistPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKPREGADM")
                .withProcedureName("SP_INS_CHECK_PREG");

        actualizaChecklistPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKPREGADM")
                .withProcedureName("SP_ACT_CHECK_PREG");

        eliminaChecklistPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKPREGADM")
                .withProcedureName("SP_DEL_CHECK_PREG");

        buscaChecklistPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKPREGADM")
                .withProcedureName("SP_SEL_CHECK_PREG")
                .returningResultSet("RCL_CHECK_PREG", new ChecklistPreguntaRowMapper());

        obtienePreguntas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKPREGADM")
                .withProcedureName("SPOBTIENEPREGS")
                .returningResultSet("RCL_PREGS", new PreguntasXCheklistRowMapper());

    }

    @SuppressWarnings("unchecked")
    public List<ModuloDTO> obtieneModulo() throws Exception {
        Map<String, Object> out = null;
        List<ModuloDTO> listaModulo = null;
        int error = 0;

        out = jdbcObtieneModulos.execute();

        logger.info("Funci�n ejecutada: {checklist.PA_MODULOADM.SP_SEL_G_MODULO}");

        listaModulo = (List<ModuloDTO>) out.get("RCL_MODULO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Modulos");
        } else {
            return listaModulo;
        }

        return listaModulo;

    }

    @SuppressWarnings("unchecked")
    public List<ModuloDTO> obtieneModulo(int idModulo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ModuloDTO> listaModulo = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_MODULO", idModulo);

        out = jdbcObtieneModulo.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_MODULOADM,SP_SEL_MODULO}");

        listaModulo = (List<ModuloDTO>) out.get("RCL_MODULO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Modulos");
        } else {
            return listaModulo;
        }

        return listaModulo;

    }

    public int insertaModulo(ModuloDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idModulo = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_MOD_PADRE", bean.getIdModuloPadre());

        out = jdbcInsertaModulo.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_MODULOADM.SP_INS_MODULO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idModuloReturn = (BigDecimal) out.get("PA_IDMODULO");
        idModulo = idModuloReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Modulo");
        } else {
            return idModulo;
        }

        return idModulo;
    }

    public boolean actualizaModulo(ModuloDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDMODULO", bean.getIdModulo())
                .addValue("PA_MOD_PADRE", bean.getIdModuloPadre())
                .addValue("PA_NOMBRE", bean.getNombre());

        out = jdbcActualizaModulo.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_MODULOADM, SP_ACT_MODULO}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Modulo  id( " + bean.getIdModulo() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaModulo(int idModulo) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDMODULO", idModulo);

        out = jdbcEliminaModulo.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_MODULOADM.SP_DEL_MODULO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el Modulo id(" + idModulo + ")");
        } else {
            return true;
        }

        return false;
    }

    //CHECKLIST
    public int insertaChecklist(ChecklistProtocoloDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        int idChecklist = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_TIPO_CHECK", bean.getIdTipoChecklist())
                .addValue("PA_FCNOMBRE", bean.getNombreCheck())
                .addValue("PA_IDHORARIO", bean.getIdHorario())
                .addValue("PA_VIGENTE", bean.getVigente())
                .addValue("PA_FECHA_I", bean.getFecha_inicio())
                .addValue("PA_FECHA_FIN", bean.getFecha_fin())
                .addValue("PA_FIID_ESTADO", bean.getIdEstado())
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_DIA", bean.getDia())
                .addValue("PA_PERIODO", bean.getPeriodo())
                .addValue("PA_VERSION", bean.getVersion())
                .addValue("PA_ORDEN_G", bean.getOrdenGrupo())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_NEGO", bean.getNegocio())
                .addValue("PA_PROTOCOLO", bean.getIdProtocolo())
                .addValue("PA_CLASIFICA", bean.getClasifica())
                .addValue("PA_VALPOND", bean.getPonderacionTot());

        out = jdbcInsertaChecklistCom.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PA_ADM_CHECK.SP_INS_CHECK}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal idreturn = (BigDecimal) out.get("PA_IDCHECKLIST");
        idChecklist = idreturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar Checklist");
        } else {
            return idChecklist;
        }

        return idChecklist;
    }
    //checklist ponderacion tot

    public int insertaChecklistCom(ChecklistDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        int idChecklist = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_TIPO_CHECK", bean.getIdTipoChecklist().getIdTipoCheck())
                .addValue("PA_FCNOMBRE", bean.getNombreCheck())
                .addValue("PA_IDHORARIO", bean.getIdHorario())
                .addValue("PA_VIGENTE", bean.getVigente())
                .addValue("PA_FECHA_I", bean.getFecha_inicio())
                .addValue("PA_FECHA_FIN", bean.getFecha_fin())
                .addValue("PA_FIID_ESTADO", bean.getIdEstado())
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_DIA", bean.getDia())
                .addValue("PA_PERIODO", bean.getPeriodicidad())
                .addValue("PA_VERSION", bean.getVersion())
                .addValue("PA_ORDEN_G", bean.getOrdeGrupo())
                .addValue("PA_VALPOND", bean.getPonderacionTot())
                .addValue("PA_ORDEN_G", bean.getOrdeGrupo())
                .addValue("PA_CLASIFICA", bean.getClasifica())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_NEGO", bean.getNego());

        out = jdbcInsertaChecklistCom.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PA_CHECKADM.SP_INS_CHECKCOM}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal idreturn = (BigDecimal) out.get("PA_IDCHECKLIST");
        idChecklist = idreturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar Checklist");
        } else {
            return idChecklist;
        }

        return idChecklist;
    }

    //ponderacion tot
    public boolean actualizaChecklistCom(ChecklistDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECK", bean.getIdChecklist())
                .addValue("PA_FIID_TIPO_CHECK", bean.getIdTipoChecklist().getIdTipoCheck())
                .addValue("PA_FCNOMBRE", bean.getNombreCheck())
                .addValue("PA_IDHORARIO", bean.getIdHorario())
                .addValue("PA_VIGENTE", bean.getVigente())
                .addValue("PA_FECHA_I", bean.getFecha_inicio())
                .addValue("PA_FECHA_FIN", bean.getFecha_fin())
                .addValue("PA_FIID_ESTADO", bean.getIdEstado())
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_DIA", bean.getDia())
                .addValue("PA_PERIODO", bean.getPeriodicidad())
                .addValue("PA_VERSION", bean.getVersion())
                .addValue("PA_VALPOND", bean.getPonderacionTot())
                .addValue("PA_ORDEN_G", bean.getOrdeGrupo())
                .addValue("PA_CLASIFICA", bean.getClasifica())
                .addValue("PA_NEGO", bean.getNego());

        out = jdbcActualizaChecklistCom.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PA_CHECKADM.SP_ACT_CHECKCOM}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Checklist");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaChecklist(int idCheckList) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECK", idCheckList);

        out = jdbcEliminaChecklist.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PA_CHECKADM.SP_DEL_CHECK}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar Checklist el id: " + idCheckList);
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistDTO> buscaChecklist() throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        List<ChecklistDTO> listaChecklist = null;

        out = jdbcBuscaChecklists.execute();

        logger.info("Funci�n ejecutada:{checklist.PA_CHECKADM.SP_SEL_G_CK}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");

        if (error == 1) {
            logger.info("Algo ocurrió al consultar los Checklist");
        } else {
            return listaChecklist;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistDTO> buscaChecklist(int idChecklist) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ChecklistDTO> listaChecklist = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECK", idChecklist);

        out = jdbcBuscaChecklist.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PA_CHECKADM.SP_SEL_CHECK}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");

        if (error == 1) {
            logger.info("Algo ocurrió al consultar el Checklist id: " + idChecklist);
        } else {
            return listaChecklist;
        }

        return null;
    }
    //PREGUNTAS

    @SuppressWarnings("unchecked")
    public List<PreguntaDTO> obtienePregunta() throws Exception {
        Map<String, Object> out = null;
        List<PreguntaDTO> listaPregunta = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funci�n ejecutada: {checklist.PA_CHECKPREGADM.SP_SEL_G_PREG}");

        listaPregunta = (List<PreguntaDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener las Preguntas");
        } else {
            return listaPregunta;
        }

        return listaPregunta;
    }

    @SuppressWarnings("unchecked")
    public List<PreguntaDTO> obtienePregunta(int idPreg) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        List<PreguntaDTO> listaPregunta = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREG", idPreg);

        out = jdbcObtienePregunta.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_CHECKPREGADM.SP_SEL_PREG}");

        listaPregunta = (List<PreguntaDTO>) out.get("RCL_PREG");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener las Preguntas");
        } else {
            return listaPregunta;
        }

        return listaPregunta;

    }

    public int insertaPreguntaCom(PreguntaDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idPreg = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDMODULO", bean.getIdModulo())
                .addValue("PA_IDT_PREG", bean.getIdTipo())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_DESCRIPCION", bean.getPregunta())
                .addValue("PA_FCDETALLE", bean.getDetalle())
                .addValue("PA_CRITICA", bean.getCritica())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_SLA", bean.getSla())
                .addValue("PA_AREA", bean.getArea())
                .addValue("PA_NUMSERIE", bean.getCodigo())
                .addValue("PA_FCRESPONSABLE", bean.getResponsable())
                .addValue("PA_FCNEGOCIO", bean.getNegocio());

        out = jdbcInsertaPreguntaCom.execute(in);

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idPregReturn = (BigDecimal) out.get("PA_IDPREGUNTA");
        idPreg = idPregReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar la Pregunta IMP");
        } else {
            return idPreg;
        }

        return idPreg;

    }

    public int insertaPregunta(PreguntaDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idPreg = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDMODULO", bean.getIdModulo())
                .addValue("PA_IDT_PREG", bean.getIdTipo())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_DESCRIPCION", bean.getPregunta())
                .addValue("PA_FCRESPONSABLE", bean.getResponsable())
                .addValue("PA_FCNEGOCIO", bean.getNegocio())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcInsertaPregunta.execute(in);

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idPregReturn = (BigDecimal) out.get("PA_IDPREGUNTA");
        idPreg = idPregReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar la Pregunta");
        } else {
            return idPreg;
        }

        return idPreg;

    }

    public boolean actualizaPregunta(PreguntaDTO bean) throws Exception {

        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREG", bean.getIdPregunta())
                .addValue("PA_IDMODULO", bean.getIdModulo())
                .addValue("PA_IDT_PREG", bean.getIdTipo())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_DESCRIPCION", bean.getPregunta())
                .addValue("PA_FCRESPONSABLE", bean.getResponsable())
                .addValue("PA_FCNEGOCIO", bean.getNegocio());

        out = jdbcActualizaPregunta.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_CHECKPREGADM.SP_ACT_PREG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar la Pregunta id( " + bean.getIdPregunta() + ")");
        } else {
            return true;
        }

        return false;
    }

    //imperd
    public boolean actualizaPreguntaCom(PreguntaDTO bean) throws Exception {

        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREG", bean.getIdPregunta())
                .addValue("PA_IDMODULO", bean.getIdModulo())
                .addValue("PA_IDT_PREG", bean.getIdTipo())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_DESCRIPCION", bean.getPregunta())
                .addValue("PA_FCDETALLE", bean.getDetalle())
                .addValue("PA_CRITICA", bean.getCritica())
                .addValue("PA_SLA", bean.getSla())
                .addValue("PA_FCRESPONSABLE", bean.getResponsable())
                .addValue("PA_FCNEGOCIO", bean.getNegocio());
        //.addValue("PA_AREA", bean.getArea());

        out = jdbcActualizaPreguntaCom.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_CHECKPREGADM.SP_ACT_PREG_N}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar la Pregunta id( " + bean.getIdPregunta() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaPregunta(int idPreg) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREG", idPreg);

        out = jdbcEliminaPregunta.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_CHECKPREGADM.SP_DEL_PREG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar la pregunta id(" + idPreg + ")");
        } else {
            return true;
        }

        return false;

    }
//CHECKPREG

    public boolean insertaChecklistPregunta(ChecklistPreguntaDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECKLIST", bean.getIdChecklist())
                .addValue("PA_FIID_PREGUNTA", bean.getIdPregunta())
                .addValue("PA_FIORDEN_CHECK", bean.getOrdenPregunta())
                .addValue("PA_FIID_PREG_PADRE", bean.getPregPadre())
                .addValue("PA_COMMIT", bean.getCommit());

        out = insertaChecklistPregunta.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_CHECKPREGADM.SP_INS_CHECK_PREG}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar ChecklistPregunta");
        } else {
            return true;
        }
        return false;
    }

    public boolean actualizaChecklistPregunta(ChecklistPreguntaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECKLIST", bean.getIdChecklist())
                .addValue("PA_FIID_PREGUNTA", bean.getIdPregunta())
                .addValue("PA_FIORDEN_CHECK", bean.getOrdenPregunta())
                .addValue("PA_FIID_PREG_PADRE", bean.getPregPadre());

        out = actualizaChecklistPregunta.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_CHECKPREGADM.SP_ACT_CHECK_PREG}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar ChecklistPregunta");
        } else {
            return true;
        }
        return false;

    }

    public boolean eliminaChecklistPregunta(int idChecklist, int idPregunta) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECKLIST", idChecklist)
                .addValue("PA_FIID_PREGUNTA", idPregunta);

        out = eliminaChecklistPregunta.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_CHECKPREGADM.SP_DEL_CHECK_PREG}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al eliminar ChecklistPregunta");
        } else {
            return true;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntaDTO> buscaPreguntas(int idChecklist) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        List<ChecklistPreguntaDTO> listaChecklistPregunta = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECKLIST", idChecklist);

        out = buscaChecklistPregunta.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_CHECKPREGADM.SP_SEL_CHECK_PREG}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listaChecklistPregunta = (List<ChecklistPreguntaDTO>) out.get("RCL_CHECK_PREG");

        if (error == 1) {
            logger.info("Algo ocurrió al obtener ChecklistPregunta");
        } else {
            return listaChecklistPregunta;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntaDTO> obtienePregXcheck(int idChecklist) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;

        List<ChecklistPreguntaDTO> listaChecklistPregunta = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_CHECKLIST", idChecklist);

        out = obtienePreguntas.execute(in);

        logger.info("Funcion ejecutada:{checklist.PA_CHECKPREGADM.SPOBTIENEPREGS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = errorReturn.intValue();

        listaChecklistPregunta = (List<ChecklistPreguntaDTO>) out.get("RCL_PREGS");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al obtener preguntas por checklist");
        } else {
            return listaChecklistPregunta;
        }

        return null;
    }

}
