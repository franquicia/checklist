<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="es">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
	<title>Supervisi&oacute;n Sucursales</title>
	<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/estilos.css">
	<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/menu.css">
	<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/secciones.css">
	<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/dropkick.css">
	<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/paginador.css" media="screen">
	<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/calendario.css" media="screen">

</head>

<body>

<div class="page">
	<div class="header">
		<div class="tblHeader">
		 <div class="headerMenu">
		 	<a href="#"  id="hamburger">
				<img src="../images/supervisionSistemas/btn_hamburguer.svg" alt="Men� principal" class="imgHamburgesa">
			</a>
		 </div>
		 <div class="headerLogo">
				<a href="index.html">
					<img src="../images/supervisionSistemas/logo-baz.svg"  id="imgLogo">
				</a>
		</div>
		   <div class="headerUser">
				<div class="name">
					<b>${user.nombre}</b> <br> 
					<c:if test="${user.admin == true}">Administrador</c:if>
					<c:if test="${user.admin == false}">Usuario</c:if>
				</div>
				<div class="pic-name">
					<div class="pic" style="background-image: url('https://portal.socio.gs/foto/elektra/empleados/${user.idUsuario}.jpg'), url('../images/supervisionSistemas/person-icon.jpg');"></div>
					<span class="tCenter">0</span>
				</div>
				<div class="user-menu">
					<a href="#"><img src="../images/supervisionSistemas/btn_desp_menu.jpg" class="configuraciones"></a>
				</div>
				<div class="fecha">
					<c:set var = "now" value = "<%= new java.util.Date()%>" />
					<b><fmt:formatDate type="date" dateStyle = "short" value = "${now}" /></b><br>
					<span><fmt:formatDate type="time" timeStyle="short" value = "${now}" />Hrs. | CDMX</span>
				</div>
			</div>
			<div class="contConfig">
				<div class="divConfig">
					<a href="#" class="divConfig1 uno">
						<div>Cambiar contrase�a</div>
						<div><img src="../images/supervisionSistemas/ico1.png" class="imgConfig imgIco1"></div>
					</a>
					
					<a href="#" class="divConfig1 dos">
						<div>Cerrar sesi�n</div>
						<div><img src="../images/supervisionSistemas/ico2.svg" class="imgConfig imgIco2"></div>
					</a>
				</div>	
			</div>
		</div>
	</div>
	<div class="clear"></div>

	<div class="title" id="title">
		<div class="wrapper">
			<div class="h2">
			<a href="#">P�gina Principal </a> /
			<a href="#">Supervisi&oacute;n Sucursales </a>
			</div>
			<h1>Supervisi&oacute;n Sucursales</h1>
		</div>
		<div class="divlupa">
			<img src="../images/supervisionSistemas/interrogacion.png" class="lupa">
		</div>
	</div>
   
   <div class="clear"></div>


    
	
	<!-- Menu -->
	<div id="effect" class="mismoalto ui-widget-content ui-corner-all">
		<div id="menuPrincipal">
			<div class="header-usuario">
				<div id=foto><img src="../images/supervisionSistemas/logo-baz2.svg" class="imgLogo"></div>
				<div id="inf-usuario">
					<span>Cuadre de inventario f�sico</span>
					<p>Men� Principal<br>
				</div>
				<div class="w87"><input class="buscar" type="text"></div>
			</div>
			<div class="c-mright" id="menu">
				<ul class="l-navegacion nivel1">
					<li class="has-sub">
						<a href="#">
							<p>Nivel1</p>
							<div class="flecha"></div>
						</a>
						<ul class="nivel2">
							<li class="has-sub">
								<a title="" href="#">
									<p>Nivel2</p>
									<div class="flecha"></div>
								</a>
								<ul class="nivel3">
									<li class="has-sub liSimple">
										<a title="" href="#">
											<p>Nivel3</p>
										</a>
									</li>
									
								</ul>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Contenido -->
	<div class="contHome">
		<div class="titN">
			<table class="tblHeader">
			  <tbody>
				<tr>
				  <td>&nbsp;</td>
				  <c:forEach var="i" begin="0" end="${(fn:length(listareporteTotales))-1}">
					  <td>Total visitas programadas:<strong>${listareporteTotales[i].totales}</strong></td>
					</tr>
					<tr>
					  <td>Elige un sistema para ver su detalle</td>
					  <td>Total visitas actuales: <strong>${listareporteTotales[i].actuales}</strong></td>
				  </c:forEach>
				</tr>
			  </tbody>
			</table>
		</div>
		<div class="clear"></div><br>
		<div class="home">
		
		<!-- Hacer aqui el loop, mostrar la informacion que viene en el  -->
		
			<c:forEach var="k" begin="0" end="${(fn:length(listareporteSistemas))-1}">
				<a href="VistaFiltroSupervisionSistemas.htm?idCeco=${listareporteSistemas[k].idCecos}&nombreCeco=${listareporteSistemas[k].nombreCeco}" class="btnHome">
					<table class="tblbtnHome"><tr><td>${listareporteSistemas[k].nombreCeco}</td><td class="tRight"><span>${listareporteSistemas[k].terminados}</span>/<span class="txtVerde"> ${listareporteSistemas[k].asignados} </span></td></tr></table>
				</a>
			</c:forEach>
			<a href="VistaFiltroSupervisionSistemas.htm?idCeco=230034&nombreCeco=General" class="btnHome">
					<table class="tblbtnHome"><tr><td>TODAS LAS DIRECCIONES</td><td class="tRight"> </td></tr></table>
				</a>
		</div>
		
		<div class="clear"></div>
	</div>
	
<div class="clear"></div>
	<div class="footer1">
		<table class="tblFooter1">
		  <tbody>
			<tr>
			  <td>Sistema Reportes</td>
			  <td><a href="#" class="btnRojo">Malas Pr�cticas</a></td>
			</tr>
		  </tbody>
		</table>
	</div>
	<div class="footer">
		<table class="tblFooter">
		  <tbody>
			<tr>
			  <td>Banco Azteca S.A. Instituci�n de Banca M�ltiple</td>
			  <td>Derechos Reservados 2014 (T�rminos y Condiciones de uso del Portal.</td>
			</tr>
		  </tbody>
		</table>
	</div>
		
</div>
</body>
</html>

<script type="text/javascript" src="../js/supervisionSistemas/jquery-1.12.4.min.js"></script> 
<script type="text/javascript" src="../js/supervisionSistemas/jquery.dropkick.js"></script>
<script type="text/javascript" src="../js/supervisionSistemas/content_height.js"></script>
<script type="text/javascript" src="../js/supervisionSistemas/jquery-ui.js"></script> 

<script type="text/javascript" src="../js/supervisionSistemas/jquery.paginate.js"></script>
<script type="text/javascript" src="../js/supervisionSistemas/funciones.js"></script> 

<script type="text/javascript">
$(function() {
	$("#demo5").paginate({
		count 		: 3,
		start 		: 1,
		display     : 3,
		border					: false,
		border_color			: false,
		text_color  			: '#8a8a8a',
		background_color    	: 'transparent',	
		border_hover_color		: 'transparent',
		text_hover_color  		: '#000',
		background_hover_color	: 'transparent', 
		images					: true,
		mouse					: 'press',
		onChange     			: function(page){
									$('._current','#paginationdemo').removeClass('_current').hide();
									$('#p'+page).addClass('_current').show();
								  }
	});
});
</script>