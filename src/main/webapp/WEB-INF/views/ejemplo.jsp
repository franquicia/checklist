<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/7s/modal.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/7s/index.css">
	
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/estilos2.css">

<script src="../js/jquery/jquery-2.2.4.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

<title>Reporte Supervisi&oacute;n</title>


<script type="text/javascript">
	function getIdCeco(id) {
		document.getElementById('idProtocolo').value = id;
		form = document.getElementById("form1");
		//alert("ID: " + document.getElementById('idCeco').value);
		form.submit();
	}
</script>

</head>

<body>

	<div class="header">
		<span class="subSeccion">Reporte Supervisi&oacute;n / </span> <span
			class="tituloSeccion"><b>Consulta de Protocolos</b></span>
	</div>	
	<div class="contSecc">
		<div class="gris">
					<div class="divCol3">
						<div class="col3">
							Socio:<br>
							<select id="socio">
								 <!--  <option value="">Opción 1</option>
								  <option value="">Opción 2</option>
								  <option value="">Opción 3</option> -->
								  <c:forEach var="lista" items="${listaUsu}">
											<tr>
												<td style="font-size: 20px; padding: 8px;">
													<div>
														<option value="${lista.idUsuario}"><c:out value="${lista.nombreUsuario}"></c:out></option>
													</div>
												</td>
											</tr>
									</c:forEach>
							</select>  
						</div>
						
						<div class="col3">
							Protocolo:<br>
							<select id="protocolo"> 
								  <c:forEach var="lista" items="${listaProto}">
											<tr>
												<td style="font-size: 20px; padding: 8px;">
													<div>
														<option value="${lista.idChecklist}"><c:out value="${lista.nomChecklist}"></c:out></option>
													</div>
												</td>
											</tr>
									</c:forEach>
							</select>  
						</div>
					</div>
		</div>
	</div>
	
	
	<c:choose>
		<c:when test="${datos == 1}">
		
			<c:url value="/central/reporteProtocolo.htm" var="archivoPasivo" />
			<form:form method="POST" action="${archivoPasivo}" model="command" name="form1" id="form1">
			
				<div class="page">
					<div class="contHome top60">
					
					<div class="tituloScont"><b>Selecciona el protocolo que desea consultar.</b></div><br>
					
						<div class="border borderLeft">
							<div class=" overflow mh500">
								<table class="items anexoDos" id="tablaprueba">
									<tbody>
										<c:forEach var="lista" items="${lista}">
											<tr>
												<td style="font-size: 20px; padding: 8px;">
													<div>
														<a href="javascript:getIdCeco(${lista.idUsuario})">
														<c:out value="${lista.nomCheklist}"></c:out></a>
													</div>
												</td>
											</tr>
										</c:forEach>
										<input id="idProtocolo" name="idProtocolo" type="hidden" value="" />
										<input id="idUsuario" name="idUsuario" type="hidden" value="${idUsuario}" />
									</tbody>
								</table>
							</div>
						</div>
						
						<div class="botones">
							<div class="tleft w50">
								<a href="/checklist/central/inicio.htm" class="btn arrowl">
									<div style="height: 31px;">
										<img style="padding-top: 8px;"
											src="${pageContext.request.contextPath}/img/7s/arrowleft.png">
									</div>
									<div>PÁGINA PRINCIPAL</div>
								</a>
							</div>
						</div>
						
					</div>
				</div>
				
			</form:form>
		</c:when>

		<c:when test="${datos == 0}">
			<div class="contHome top60">
				<div class="buscar" style="height: 102.31px;">
					<img src="${pageContext.request.contextPath}/img/7s/buscar.png">
				</div>
				<span class="sinDatos"><b> NO SE ENCONTRARON DETALLES
						PARA CONSULTA.</b></span>
				<div class="tituloScont">
					<b>No existe ningun supervisor para mostrar.</b>
				</div>
				<br>

				<div class="botones">
					<div class="tleft w50">
						<a href="/checklist/central/inicio.htm" class="btn arrowl">
							<div style="height: 31px;">
										<img style="padding-top: 8px;"
											src="${pageContext.request.contextPath}/img/7s/arrowleft.png">
									</div>
							<div>PÁGINA PRINCIPAL</div>
						</a>
					</div>
				</div>
			</div>
		</c:when>

	</c:choose>

</body>
</html>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/custom-file-input.js"></script><!--Unpload -->
<script src="${pageContext.request.contextPath}/js/jquery.treeview.js"></script><!--Unpload -->
<script src="${pageContext.request.contextPath}/js/owl.carousel.js"></script><!-- Slider -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/funciones.js"></script>