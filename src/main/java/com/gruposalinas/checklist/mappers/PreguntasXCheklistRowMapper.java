package com.gruposalinas.checklist.mappers;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;

public class PreguntasXCheklistRowMapper implements RowMapper<ChecklistPreguntaDTO> {

	@Override
	public ChecklistPreguntaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ChecklistPreguntaDTO checklistPreguntaDTO = new ChecklistPreguntaDTO();
		checklistPreguntaDTO.setIdPregunta((BigDecimal) (BigDecimal) rs.getObject("ID_PREGUNTA"));
		checklistPreguntaDTO.setPregunta(rs.getString("PREGUNTA"));
		checklistPreguntaDTO.setIdTipoPregunta((BigDecimal) rs.getObject("ID_TIPO_PREGUNTA"));
		//checklistPreguntaDTO.setDescTipo(rs.getString("TIPO_PREGUNTA"));
		checklistPreguntaDTO.setIdModulo((BigDecimal) rs.getObject("ID_MODULO"));
		checklistPreguntaDTO.setNombreModulo(rs.getString("NOMBRE_MODULO"));
		checklistPreguntaDTO.setTipoCambio(rs.getString("FCTIPOMODIF"));
		checklistPreguntaDTO.setOrdenPregunta((BigDecimal) rs.getObject("FIORDEN_CHECK"));
		checklistPreguntaDTO.setPregPadre((BigDecimal) rs.getObject("FIID_PREG_PADRE"));
		checklistPreguntaDTO.setDetalle(rs.getString("FCDETALLE"));
		checklistPreguntaDTO.setCodigo((BigDecimal) rs.getObject("FINUMSERIE"));
		checklistPreguntaDTO.setCritica((BigDecimal) rs.getObject("FICRITICA"));
		if(((String) rs.getObject("FCSLA")) ==null) {
			checklistPreguntaDTO.setSla(new Integer("0"));
		}
		else {
			if( ((String) rs.getObject("FCSLA")).equals("null")) {
				checklistPreguntaDTO.setSla(new Integer("0"));
			}else {
				checklistPreguntaDTO.setSla(new Integer((String) rs.getObject("FCSLA")));
			}
		}
		checklistPreguntaDTO.setCommit(new BigDecimal(1));
		checklistPreguntaDTO.setResponsable(rs.getString("FCAREA"));
                checklistPreguntaDTO.setNegocio(rs.getString("FCRESPONSABLE"));
                checklistPreguntaDTO.setResponsable(rs.getString("FCNEGOCIO"));
		return checklistPreguntaDTO;
	}
	

}
