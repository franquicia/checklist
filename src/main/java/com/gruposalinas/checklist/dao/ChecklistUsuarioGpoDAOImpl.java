package com.gruposalinas.checklist.dao;

import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ChecklistUsuarioGpoDAOImpl extends DefaultDAO implements ChecklistUsuarioGpoDAO {

    private static Logger logger = LogManager.getLogger(ChecklistUsuarioDAOImpl.class);

    DefaultJdbcCall jdbcInsertaCheck;

    public void init() {

        jdbcInsertaCheck = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAINSCHKGPO")
                .withProcedureName("SPGPOCHECK");

    }

    public boolean insertaCheck(String idUsuario, String idCeco, String idGrupo) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_USUARIO", idUsuario)
                .addValue("PA_CECO", idCeco)
                .addValue("PA_GPO", idGrupo);

        out = jdbcInsertaCheck.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAASIGNAESP.SP_INSERTACHECKUSUA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al insertar checklist-usuario idUsuario: " + idUsuario + " idCeco: " + idCeco + " idGrupo: " + idGrupo);
        } else {
            return true;
        }

        return false;
    }

}
