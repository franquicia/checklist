package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteSistemasDTO;

public class ReporteTotalesSupervisionSistRowMapper implements RowMapper<ReporteSistemasDTO> {

	public ReporteSistemasDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteSistemasDTO reporteCecosSistemas = new ReporteSistemasDTO();
		
		reporteCecosSistemas.setTotales(rs.getInt("VTOTALES"));
		reporteCecosSistemas.setActuales(rs.getInt("VACTUALES"));
		
		return reporteCecosSistemas;
	}
}
