package com.gruposalinas.checklist.domain;

public class DatosInformeDTO {
	
		private int idCeco;
		private int idUsuario;
		private int idProtocolo;
		private int idInforme;
		private int imperdonables;
		private int totalRespuestas;
		private double calificacion;
		private String periodo;
		private String fecha;
		
		public int getIdCeco() {
			return idCeco;
		}
		public void setIdCeco(int idCeco) {
			this.idCeco = idCeco;
		}
		public int getIdUsuario() {
			return idUsuario;
		}
		public void setIdUsuario(int idUsuario) {
			this.idUsuario = idUsuario;
		}
		public int getIdProtocolo() {
			return idProtocolo;
		}
		public void setIdProtocolo(int idProtocolo) {
			this.idProtocolo = idProtocolo;
		}
		public int getIdInforme() {
			return idInforme;
		}
		public void setIdInforme(int idInforme) {
			this.idInforme = idInforme;
		}
		public int getImperdonables() {
			return imperdonables;
		}
		public void setImperdonables(int imperdonables) {
			this.imperdonables = imperdonables;
		}
		public int getTotalRespuestas() {
			return totalRespuestas;
		}
		public void setTotalRespuestas(int totalRespuestas) {
			this.totalRespuestas = totalRespuestas;
		}
		public double getCalificacion() {
			return calificacion;
		}
		public void setCalificacion(double calificacion) {
			this.calificacion = calificacion;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		public String getFecha() {
			return fecha;
		}
		public void setFecha(String fecha) {
			this.fecha = fecha;
		}
		@Override
		public String toString() {
			return "DatosInformeDTO [idCeco=" + idCeco + ", idUsuario=" + idUsuario + ", idProtocolo=" + idProtocolo
					+ ", idInforme=" + idInforme + ", imperdonables=" + imperdonables + ", totalRespuestas="
					+ totalRespuestas + ", calificacion=" + calificacion + ", periodo=" + periodo + ", fecha=" + fecha
					+ "]";
		}
		
		

		
	}