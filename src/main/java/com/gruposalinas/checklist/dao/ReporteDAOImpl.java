package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.CompromisoCecoDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.domain.EvidenciaDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.ReporteDTO;
import com.gruposalinas.checklist.domain.VistaCDTO;
import com.gruposalinas.checklist.mappers.CecoCompromisoRowMapper;
import com.gruposalinas.checklist.mappers.CecoGeografiaRowMapper;
import com.gruposalinas.checklist.mappers.ChecklistComboRowMapper;
import com.gruposalinas.checklist.mappers.ComparaImagenRowMapper;
import com.gruposalinas.checklist.mappers.PreguntaRowMapper;
import com.gruposalinas.checklist.mappers.ReporteCheckDetRegRowMapper;
import com.gruposalinas.checklist.mappers.ReporteCheckRowMapper;
import com.gruposalinas.checklist.mappers.ReporteChecklistCRowMapper;
import com.gruposalinas.checklist.mappers.ReporteDetRegRowMapper;
import com.gruposalinas.checklist.mappers.ReporteDetRegSupRowMapper;
import com.gruposalinas.checklist.mappers.ReporteEvidenciaRowMapper;
import com.gruposalinas.checklist.mappers.ReporteFoliosRegRowMapper;
import com.gruposalinas.checklist.mappers.ReporteGeneralRowMapper;
import com.gruposalinas.checklist.mappers.ReporteOperacionRowMapper;
import com.gruposalinas.checklist.mappers.ReportePregDetRegRowMapper;
import com.gruposalinas.checklist.mappers.ReporteProtocoloRowMapper;
import com.gruposalinas.checklist.mappers.ReporteRegionaRowMapper;
import com.gruposalinas.checklist.mappers.ReporteRegionalesRowMapper;
import com.gruposalinas.checklist.mappers.ReporteRowMapper;
import com.gruposalinas.checklist.mappers.ReporteSupRegionalRowMapper;
import com.gruposalinas.checklist.mappers.ReporteUrlRowMapper;
import com.gruposalinas.checklist.mappers.VisitaCRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ReporteDAOImpl extends DefaultDAO implements ReporteDAO {

    private static final Logger logger = LogManager.getLogger(ReporteDAOImpl.class);

    private DefaultJdbcCall jdbcBuscaReporte;
    private DefaultJdbcCall jdbcBuscaReporteUrl;
    private DefaultJdbcCall jdbcComparaImagen;
    private DefaultJdbcCall jdbcBuscaReporteReg;
    private DefaultJdbcCall jdbcBuscaReporteRegSup;
    private DefaultJdbcCall jdbcBuscaReporteDetRegSup;
    private DefaultJdbcCall jdbcBuscaReporteDetRegional;
    private DefaultJdbcCall jdbcBuscaReporteDetPregReg;
    private DefaultJdbcCall jdbcBuscaReporteOperacion;
    private DefaultJdbcCall jdbcBuscaReporteSupReg;
    private DefaultJdbcCall jdbcReporteGeneral;
    private DefaultJdbcCall jdbcReporteGCompromisos;
    private DefaultJdbcCall jdbcReporteCheckUsua;
    private DefaultJdbcCall jdbcReporteDetReg;
    private DefaultJdbcCall jdbcReporteVistaCumplimiento;
    private DefaultJdbcCall jdbcReporteVistaC;
    private DefaultJdbcCall jdbcReporteChecklist;
    private DefaultJdbcCall jdbcCecoInfo;
    private DefaultJdbcCall jdbcBuscaReporteDetVC;
    private DefaultJdbcCall jdbcCargaIncial;
    private DefaultJdbcCall jdbcCargaCecosH;
    private DefaultJdbcCall jdbcCargaGeo;
    private DefaultJdbcCall jdbcReporteProtocolo;
    private DefaultJdbcCall jdbcCalificacion;

    public void init() {
        jdbcBuscaReporte = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE")
                .withProcedureName("SP_REPORTE")
                .returningResultSet("RCL_REPORTE", new ReporteRowMapper());

        jdbcBuscaReporteUrl = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE")
                .withProcedureName("SP_REPORTE_URL")
                .returningResultSet("RCL_REPORTE", new ReporteUrlRowMapper());

        jdbcComparaImagen = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE")
                .withProcedureName("SP_COMPARAR_IMG")
                .returningResultSet("RCL_IMG", new ComparaImagenRowMapper());

        jdbcBuscaReporteReg = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE2")
                .withProcedureName("SP_REPORTE_REG")
                .returningResultSet("RCL_REGIONAL", new ReporteRegionaRowMapper());

        jdbcReporteGeneral = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE2")
                .withProcedureName("SP_REPORTE_GENERAL")
                .returningResultSet("RCL_PREGUNTAS", new PreguntaRowMapper())
                .returningResultSet("RCL_REPORTE", new ReporteGeneralRowMapper());

        jdbcReporteGCompromisos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE2")
                .withProcedureName("SP_REPORTE_G_COMP")
                .returningResultSet("RCL_PREGUNTAS", new PreguntaRowMapper())
                .returningResultSet("RCL_REPORTE", new ReporteGeneralRowMapper());

        jdbcReporteCheckUsua = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK_USUA")
                .withProcedureName("SP_SELXCHECK")
                .returningResultSet("RCL_CHECK_USUA", new ReporteCheckRowMapper());

        jdbcBuscaReporteRegSup = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE4")
                .withProcedureName("SP_PRUEBA_PIP")
                .returningResultSet("RCL_REGIONAL", new ReporteRegionalesRowMapper());

        jdbcBuscaReporteOperacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE3")
                .withProcedureName("SP_REPORTE_OP")
                .returningResultSet("RCL_PREGUNTAS", new ReporteOperacionRowMapper());

        jdbcBuscaReporteSupReg = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE3")
                .withProcedureName("SP_REPORTE_SUP_REG")
                .returningResultSet("RCL_PREGUNTAS", new ReporteSupRegionalRowMapper());

        jdbcBuscaReporteDetRegSup = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE3")
                .withProcedureName("SP_DET_REP")
                .returningResultSet("RCL_SUCURSAL", new CecoGeografiaRowMapper())
                .returningResultSet("RCL_DATOS", new ReporteDetRegSupRowMapper());

        jdbcBuscaReporteDetRegional = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE5")
                .withProcedureName("SPDETALLE_SUP")
                .returningResultSet("RCL_CHECKLIST", new ReporteCheckDetRegRowMapper())
                .returningResultSet("RCL_EVIDENCIAS", new ReporteEvidenciaRowMapper())
                .returningResultSet("RCL_FOLIOS", new ReporteFoliosRegRowMapper());

        jdbcBuscaReporteDetVC = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAVISTAC")
                .withProcedureName("SPDETALLE_VC")
                .returningResultSet("RCL_CHECKLIST", new ReporteCheckDetRegRowMapper())
                .returningResultSet("RCL_EVIDENCIAS", new ReporteEvidenciaRowMapper())
                .returningResultSet("RCL_FOLIOS", new ReporteFoliosRegRowMapper());

        jdbcBuscaReporteDetPregReg = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE5")
                .withProcedureName("SPDETALLE_SUP_PREG")
                .returningResultSet("RCL_PREGUNTAS", new ReportePregDetRegRowMapper());

        jdbcReporteDetReg = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE4")
                .withProcedureName("SP_DET_REGIONAL")
                .returningResultSet("RCL_REGIONAL", new ReporteDetRegRowMapper());
        //Este es
        jdbcReporteVistaCumplimiento = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAVISTAC")
                .withProcedureName("SP_REPORTE_VC")
                .returningResultSet("RCL_CHECK", new ChecklistComboRowMapper());

        jdbcReporteVistaC = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAVISTAC")
                .withProcedureName("SP_REPORTE_VC1")
                .returningResultSet("RCL_PORCENTAJE", new VisitaCRowMapper());

        jdbcCecoInfo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE5")
                .withProcedureName("SP_CECO_INFO")
                .returningResultSet("RCL_CECO", new CecoCompromisoRowMapper());

        jdbcCargaIncial = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHISTORICOCC")
                .withProcedureName("SP_CARGA_INICIAL");

        jdbcCargaCecosH = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHISTORICOCC")
                .withProcedureName("SP_CARGA_CECOS_HIST");

        jdbcCargaGeo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHISTORICOCC")
                .withProcedureName("SP_CARGA_GEO_HIST");

        jdbcReporteChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPSISTEMAS")
                .withProcedureName("SP_REPORTE_CUATRO")
                .returningResultSet("RCL_PORCENTAJE", new ReporteChecklistCRowMapper());

        jdbcReporteProtocolo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTEPRO")
                .withProcedureName("SP_REPORTE_PROTOCOLO")
                .returningResultSet("RCL_PREGUNTAS", new PreguntaRowMapper())
                .returningResultSet("RCL_REPORTE", new ReporteProtocoloRowMapper());

        jdbcCalificacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTEPRO")
                .withProcedureName("SP_CALIFICACION");

    }

    @SuppressWarnings("unchecked")
    public List<ReporteDTO> buscaReporte(String idCheck, String idUsuario, String nombreUsuario, String nombreSucursal, String fechaRespuesta, String nombreCheck, String metodoCheck) throws Exception {
        Map<String, Object> out = null;
        List<ReporteDTO> listaReporte = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCHECKLIST", idCheck)
                .addValue("PA_NUSUARIO", idUsuario)
                .addValue("PA_NOMBREU", nombreUsuario)
                .addValue("PA_NOMBRE_S", nombreSucursal)
                .addValue("PA_FECHA_RESP", fechaRespuesta)
                .addValue("PA_NOMBRE_CK", nombreCheck)
                .addValue("PA_METODO", metodoCheck);

        out = jdbcBuscaReporte.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAREPORTE.SP_REPORTE}");

        listaReporte = (List<ReporteDTO>) out.get("RCL_REPORTE");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");

        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno Algo paso");
        }

        return listaReporte;

    }

    @SuppressWarnings("unchecked")
    public List<ReporteDTO> buscaReporteURL(String idCategoria, String idTipo, String idPuesto, String idUsuario, String idCeco, String idSucursal, int idNivel, String fecha, String idPais) throws Exception {
        Map<String, Object> out = null;
        List<ReporteDTO> listaReporte = null;
        int respuesta = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CATEGORIA", idCategoria)
                .addValue("PA_TIPO_CHECK", idTipo)
                .addValue("PA_PUESTO", idPuesto)
                .addValue("PA_USUARIO", idUsuario)
                .addValue("PA_CECO", idCeco)
                .addValue("PA_TERRITORIO", idSucursal)
                .addValue("PA_NIVEL", idNivel)
                .addValue("PA_FECHA_RESP", fecha)
                .addValue("PA_PAIS", idPais);

        out = jdbcBuscaReporteUrl.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAREPORTE.SP_REPORTE_URL}");

        listaReporte = (List<ReporteDTO>) out.get("RCL_REPORTE");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");

        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno Algo paso");
        }
        return listaReporte;
    }

    @SuppressWarnings("unchecked")
    public List<ReporteDTO> ComparaImagen(int idRuta, int idPregunta, int idCheckUsuario, String fecha) throws Exception {
        Map<String, Object> out = null;
        List<ReporteDTO> listaImagenes = null;
        int respuesta = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDEVIDENCIA", idRuta)
                .addValue("PA_IDPREGUNTA", idPregunta)
                .addValue("PA_IDCHECK_US", idCheckUsuario)
                .addValue("PA_FECHA", fecha);

        out = jdbcComparaImagen.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAREPORTE.SP_REPORTE_URL}");

        listaImagenes = (List<ReporteDTO>) out.get("RCL_IMG");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("Algo paso al retornar las imagenes");
        }

        return listaImagenes;

    }

    @SuppressWarnings("unchecked")
    public List<ReporteDTO> ReporteRegional(String fechaIni, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        List<ReporteDTO> listaReporte = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("FECHA_INI", fechaIni)
                .addValue("FECHA_FIN", fechaFin);
        out = jdbcBuscaReporteReg.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAREPORTE.SP_REPORTE_REG}");

        listaReporte = (List<ReporteDTO>) out.get("RCL_REGIONAL");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno Algo paso");
        }

        return listaReporte;

    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> ReporteGeneral(int idChecklist, String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<PreguntaDTO> listaPreguntas = null;
        List<ReporteDTO> listaRespuestas = null;
        int respuesta = 0;
        int conteo = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCHECKLIST", idChecklist)
                .addValue("PA_FECHA_I", fechaInicio)
                .addValue("PA_FECHA_F", fechaFin);

        out = jdbcReporteGeneral.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAREPORTE2.SP_REPORTE_GENERAL}");

        listaPreguntas = (List<PreguntaDTO>) out.get("RCL_PREGUNTAS");
        listaRespuestas = (List<ReporteDTO>) out.get("RCL_REPORTE");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        BigDecimal idreturn = (BigDecimal) out.get("PA_CONTEO");
        conteo = idreturn.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno ERRROR");
        }

        lista = new HashMap<String, Object>();
        lista.put("listaPregunta", listaPreguntas);
        lista.put("listaRespuesta", listaRespuestas);
        lista.put("conteo", conteo);
        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReporteDTO> ReporteCheckUsua(String idCheck, String idPuesto) throws Exception {
        Map<String, Object> out = null;
        List<ReporteDTO> listaReporte = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECKLIST", idCheck)
                .addValue("PA_PUESTO", idPuesto);

        out = jdbcReporteCheckUsua.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PA_ADM_CHECK_USUA.SP_SELXCHECK}");

        listaReporte = (List<ReporteDTO>) out.get("RCL_CHECK_USUA");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");

        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno Algo paso");
        }

        return listaReporte;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReporteDTO> ReporteSupReg(String nivelTerritorio, String nivelZona, String nivelRegion, String region, String idCecoSup, String pais, String negocio, String fechaI, String fechaF, String idChecklist) throws Exception {
        Map<String, Object> out = null;
        List<ReporteDTO> listaReporte = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NTERRITORIO", nivelTerritorio)
                .addValue("PA_NZONA", nivelZona)
                .addValue("PA_NREGION", nivelRegion)
                .addValue("PA_REGION", region)
                .addValue("PA_CC", idCecoSup)
                .addValue("PA_PAIS", pais)
                .addValue("PA_NEGOCIO", negocio)
                .addValue("PA_FECHA_I", fechaI)
                .addValue("PA_FECHA_F", fechaF)
                .addValue("PA_IDCHECKLIST", idChecklist);

        out = jdbcBuscaReporteRegSup.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAREPORTE3.SP_REPORTE}");

        listaReporte = (List<ReporteDTO>) out.get("RCL_REGIONAL");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno Algo paso");
        }

        return listaReporte;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> ReporteDetSupReg(int idChecklist, String idCeco, String pais, String negocio, String fechaI, String fechaF) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<CecoDTO> listaSucursal = null;
        List<ReporteDTO> listaReporte = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCHECKLIST", idChecklist)
                .addValue("PA_IDCECO", idCeco)
                .addValue("PA_FECHA_I", fechaI)
                .addValue("PA_FECHA_F", fechaF)
                .addValue("PA_PAIS", pais)
                .addValue("PA_NEGOCIO", negocio);

        out = jdbcBuscaReporteDetRegSup.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAREPORTE3.SP_DET_REP}");

        listaSucursal = (List<CecoDTO>) out.get("RCL_SUCURSAL");
        listaReporte = (List<ReporteDTO>) out.get("RCL_DATOS");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno Algo paso");
        }

        lista = new HashMap<String, Object>();
        lista.put("listaDatos", listaReporte);
        lista.put("listaSucursales", listaSucursal);
        //logger.info("listaReporte " +listaReporte.get(0).getIdBitacora() );
        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> ReporteDetSupReg_versBorra(int idChecklist, String idCeco, String pais, String negocio, String fechaI, String fechaF) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<CecoDTO> listaSucursal = new ArrayList<CecoDTO>();
        List<CecoDTO> listaSucursalAux = null;
        List<ReporteDTO> listaReporte = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCHECKLIST", idChecklist)
                .addValue("PA_IDCECO", idCeco)
                .addValue("PA_FECHA_I", fechaI)
                .addValue("PA_FECHA_F", fechaF)
                .addValue("PA_PAIS", pais)
                .addValue("PA_NEGOCIO", negocio);

        out = jdbcBuscaReporteDetRegSup.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAREPORTE3.SP_DET_REP}");

        listaSucursalAux = (List<CecoDTO>) out.get("RCL_SUCURSAL");
        listaReporte = (List<ReporteDTO>) out.get("RCL_DATOS");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno Algo paso");
        }

        lista = new HashMap<String, Object>();
        //lista.put("listaSucursales",listaSucursal);
        lista.put("listaDatos", listaReporte);
        //logger.info("********************* Empieza limpieza ********************");

        for (int i = 0; i < listaSucursalAux.size(); i++) {
            if (!idCeco.equals(listaSucursalAux.get(i).getIdCeco())) {
                listaSucursal.add(listaSucursalAux.get(i));
            } else {
                //logger.info("********************* saque al padre ********************");
            }
        }
        lista.put("listaSucursales", listaSucursal);

        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> ReporteOperacion(int idChecklist, String fechaInicio, String fechaFin, String pais, String negocio, String territorio, String zona, String region, String ceco) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> listaReporteOpe = new HashMap<String, Object>();
        List<ReporteDTO> listaReporte = null;
        int respuesta = 0;
        int conteo = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCHECKLIST", idChecklist)
                .addValue("PA_FECHA_I", fechaInicio)
                .addValue("PA_FECHA_F", fechaFin)
                .addValue("PA_TERRITORIO", territorio)
                .addValue("PA_ZONA", zona)
                .addValue("PA_REGION", region)
                .addValue("PA_CC", ceco)
                .addValue("PA_PAIS", pais)
                .addValue("PA_NEGOCIO", negocio);

        out = jdbcBuscaReporteOperacion.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAREPORTE3.SP_REPORTE_OP}");

        listaReporte = (List<ReporteDTO>) out.get("RCL_PREGUNTAS");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        BigDecimal idreturn = (BigDecimal) out.get("PA_TOTAL");
        conteo = idreturn.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno Algo paso");
        }

        listaReporteOpe.put("listaReporte", listaReporte);
        listaReporteOpe.put("conteo", conteo);

        return listaReporteOpe;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReporteDTO> ReporteRegSup(int idCheck, int idPregunta, String fechaI, String fechaFin, String pais, String negocio, String nivelRegion, String idCeco) throws Exception {
        Map<String, Object> out = null;
        List<ReporteDTO> listaReporte = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCHECKLIST", idCheck)
                .addValue("PA_PREGUNTA", idPregunta)
                .addValue("PA_FECHA_I", fechaI)
                .addValue("PA_FECHA_F", fechaFin)
                .addValue("PA_NREGION", nivelRegion)
                .addValue("PA_CC", idCeco)
                .addValue("PA_PAIS", pais)
                .addValue("PA_NEGOCIO", negocio);

        out = jdbcBuscaReporteSupReg.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAREPORTE3.SP_REPORTE_SUP_REG}");

        listaReporte = (List<ReporteDTO>) out.get("RCL_PREGUNTAS");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno Algo paso");
        }

        return listaReporte;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> ReporteDetRegional(int idBitacora) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<ChecklistDTO> ListaChecks = null;
        List<EvidenciaDTO> listaEvidencias = null;
        List<PreguntaDTO> listaFolios = null;
        int totalE = 0;
        int totalF = 0;

        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITACORA", idBitacora);

        out = jdbcBuscaReporteDetRegional.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAPRUEBA.SPDETALLE_SUP}");

        ListaChecks = (List<ChecklistDTO>) out.get("RCL_CHECKLIST");
        listaEvidencias = (List<EvidenciaDTO>) out.get("RCL_EVIDENCIAS");
        listaFolios = (List<PreguntaDTO>) out.get("RCL_FOLIOS");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        BigDecimal conteoE = (BigDecimal) out.get("TOTAL_EVID");
        totalE = conteoE.intValue();

        BigDecimal conteoF = (BigDecimal) out.get("TOTAL_FOLIOS");
        totalF = conteoF.intValue();

        if (respuesta == 0) {
            throw new Exception("El Store Procedure retorno Algo paso");
        }

        lista = new HashMap<String, Object>();
        lista.put("listaChecklist", ListaChecks);
        lista.put("listaEvidencias", listaEvidencias);
        lista.put("listaFolios", listaFolios);
        lista.put("totalEvidencias", totalE);
        lista.put("totalFolios", totalF);

        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CompromisoDTO> ReporteDetPreguntasRegional(int idBitacora) throws Exception {
        Map<String, Object> out = null;
        List<CompromisoDTO> listaPreguntas = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITACORA", idBitacora);

        out = jdbcBuscaReporteDetPregReg.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAPRUEBA.SPDETALLE_SUP_PREG}");

        listaPreguntas = (List<CompromisoDTO>) out.get("RCL_PREGUNTAS");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = respuestaEjec.intValue();

        if (respuesta == 0) {
            throw new Exception("El Store Procedure retorno Algo paso");
        }

        return listaPreguntas;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReporteDTO> ReporteDetReg(int idCheck, int idCeco, String fechaIni, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        List<ReporteDTO> listaReporte = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCHECK", idCheck)
                .addValue("PA_IDCECO", idCeco)
                .addValue("PA_FECHAINI", fechaIni)
                .addValue("PA_FECHAFIN", fechaFin);

        out = jdbcReporteDetReg.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAREPORTE4.SP_DET_REGIONAL}");

        listaReporte = (List<ReporteDTO>) out.get("RCL_REGIONAL");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno Algo paso");
        }

        return listaReporte;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> ReporteGeneralCompromisos(int idChecklist, String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<PreguntaDTO> listaPreguntas = null;
        List<ReporteDTO> listaRespuestas = null;
        int respuesta = 0;
        int conteo = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCHECKLIST", idChecklist)
                .addValue("PA_FECHA_I", fechaInicio)
                .addValue("PA_FECHA_F", fechaFin);

        out = jdbcReporteGCompromisos.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAREPORTE2.SP_REPORTE_GENERAL}");

        listaPreguntas = (List<PreguntaDTO>) out.get("RCL_PREGUNTAS");
        listaRespuestas = (List<ReporteDTO>) out.get("RCL_REPORTE");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        BigDecimal idreturn = (BigDecimal) out.get("PA_CONTEO");
        conteo = idreturn.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno ERRROR");
        }

        lista = new HashMap<String, Object>();
        lista.put("listaPregunta", listaPreguntas);
        lista.put("listaRespuesta", listaRespuestas);
        lista.put("conteo", conteo);
        return lista;

    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> ReporteVistaCumplimiento(int idUsuario, String idCeco, int pais, int canal, String fecha) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<ChecklistDTO> listaChecklist = null;
        int respuesta = 0;
        int porcentaje = 0;
        int ceco = 0;
        int conteo = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", idUsuario)
                .addValue("PA_PAIS", pais)
                .addValue("PA_CANAL", canal)
                .addValue("PA_CECOVC", idCeco);

        out = jdbcReporteVistaCumplimiento.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAVISTAC.SP_REPORTE_VC}");

        listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        BigDecimal valorPorcentaje = (BigDecimal) out.get("PA_PORCENTAJE");
        porcentaje = valorPorcentaje.intValue();

        BigDecimal valorCeco = (BigDecimal) out.get("PA_CECO");
        ceco = valorCeco.intValue();

        BigDecimal valorConteo = (BigDecimal) out.get("PA_CONTEO");
        conteo = valorConteo.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno ERROR");
        }

        lista = new HashMap<String, Object>();
        lista.put("listaCheck", listaChecklist);
        lista.put("porcentaje", porcentaje);
        lista.put("ceco", ceco);
        lista.put("conteo", conteo);
        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<VistaCDTO> ReporteVistaC(int idUsuario, int idCeco, int idChecklist, int pais, int canal, String fecha, int bandera) throws Exception {
        Map<String, Object> out = null;
        List<VistaCDTO> listaConteo = null;
        int respuesta = 0;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDUSUARIO", idUsuario)
                .addValue("PA_IDCECO", idCeco)
                .addValue("PA_IDCHECKLIST", idChecklist)
                .addValue("PA_PAIS", pais)
                .addValue("PA_CANAL", canal)
                .addValue("PA_FECHA", fecha)
                .addValue("PA_BANDERA", bandera);

        out = jdbcReporteVistaC.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAVISTAC.SP_REPORTE_VC1}");

        listaConteo = (List<VistaCDTO>) out.get("RCL_PORCENTAJE");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno ERROR");
        }

        return listaConteo;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CompromisoCecoDTO> CecoInfo(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        List<CompromisoCecoDTO> listaCeco = null;
        int respuesta = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", idUsuario);

        out = jdbcCecoInfo.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAREPORTE.SP_CECO_INFO}");

        listaCeco = (List<CompromisoCecoDTO>) out.get("RCL_CECO");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno ERROR");
        }

        return listaCeco;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> ReporteDetVC(int idCeco, int idChecklist, int idUsuario, String fecha) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<ChecklistDTO> ListaChecks = null;
        List<EvidenciaDTO> listaEvidencias = null;
        List<PreguntaDTO> listaFolios = null;
        int totalE = 0;
        int totalF = 0;
        int idBit = 0;

        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCECO", idCeco)
                .addValue("PA_IDCHECKLIST", idChecklist)
                .addValue("PA_IDUSUARIO", idUsuario)
                .addValue("PA_FECHA", fecha);

        out = jdbcBuscaReporteDetVC.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAPRUEBA.SPDETALLE_SUP}");

        ListaChecks = (List<ChecklistDTO>) out.get("RCL_CHECKLIST");
        listaEvidencias = (List<EvidenciaDTO>) out.get("RCL_EVIDENCIAS");
        listaFolios = (List<PreguntaDTO>) out.get("RCL_FOLIOS");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        BigDecimal conteoE = (BigDecimal) out.get("TOTAL_EVID");
        totalE = conteoE.intValue();

        BigDecimal conteoF = (BigDecimal) out.get("TOTAL_FOLIOS");
        totalF = conteoF.intValue();

        BigDecimal IdBitacora = (BigDecimal) out.get("ID_BITACORA");
        idBit = IdBitacora.intValue();

        if (respuesta == 0) {
            throw new Exception("El Store Procedure retorno Algo paso");
        }

        lista = new HashMap<String, Object>();
        lista.put("listaChecklist", ListaChecks);
        lista.put("listaEvidencias", listaEvidencias);
        lista.put("listaFolios", listaFolios);
        lista.put("totalEvidencias", totalE);
        lista.put("totalFolios", totalF);
        lista.put("idBitacora", idBit);

        return lista;
    }

    @Override
    public boolean CargaInicialHistorico() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaIncial.execute();

        logger.info("Funcion ejecutada: {checklist.PAHISTORICOCC.SP_CARGA_INICIAL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrio al cargar el historico");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean CargaCecosHistorico() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaCecosH.execute();

        logger.info("Funcion ejecutada: {checklist.PAHISTORICOCC.SP_CARGA_CECOS_HIST}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrio al cargar los cecos en el historico");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean CargaGeografiaHistorico() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaGeo.execute();

        logger.info("Funcion ejecutada: {checklist.PAHISTORICOCC.SP_CARGA_GEO_HIST}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrio la geografia en el historico");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public List<VistaCDTO> ReporteChecklist(int idCeco, int idChecklist) throws Exception {
        Map<String, Object> out = null;
        List<VistaCDTO> listaConteo = null;
        int respuesta = 0;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCECO", idCeco)
                .addValue("PA_IDCHECKLIST", idChecklist);

        out = jdbcReporteChecklist.execute(in);

        logger.info("Funcion Ejecutada FRANQUICIA.PAREPSISTEMAS.SP_REPORTE_CUATRO");

        listaConteo = (List<VistaCDTO>) out.get("RCL_PORCENTAJE");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno ERROR");
        }

        return listaConteo;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> ReporteProtocolos(int idChecklist, String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<PreguntaDTO> listaPreguntas = null;
        List<ReporteDTO> listaRespuestas = null;
        int respuesta = 0;
        int conteo = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCHECKLIST", idChecklist)
                .addValue("PA_FECHA_I", fechaInicio)
                .addValue("PA_FECHA_F", fechaFin);

        out = jdbcReporteProtocolo.execute(in);

        logger.info("FunciÃ¯Â¿Â½n ejecutada:{checklist.PAREPORTE2.SP_REPORTE_GENERAL}");

        listaPreguntas = (List<PreguntaDTO>) out.get("RCL_PREGUNTAS");
        listaRespuestas = (List<ReporteDTO>) out.get("RCL_REPORTE");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        BigDecimal idreturn = (BigDecimal) out.get("PA_CONTEO");
        conteo = idreturn.intValue();

        if (respuesta == 1) {
            throw new Exception("El Store Procedure retorno ERRROR");
        }

        lista = new HashMap<String, Object>();
        lista.put("listaPregunta", listaPreguntas);
        lista.put("listaRespuesta", listaRespuestas);
        lista.put("conteo", conteo);
        return lista;

    }

    @Override
    public int ReporteCalificacion(int idBitacora, int bandera) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int calificacion = 0;
        //System.out.println(idBitacora + "   " + bandera);
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDBITACORA", idBitacora)
                .addValue("PA_BANDERA", bandera);

        out = jdbcCalificacion.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAREPORTEPRO.SP_CALIFICACION}");

        BigDecimal resultado = (BigDecimal) out.get("PA_CALIFICACION");
        calificacion = resultado.intValue();

        BigDecimal respuesta = (BigDecimal) out.get("PA_EJECUCION");
        error = respuesta.intValue();
        if (error == 0) {
            logger.info("Algo ocurrio");
        } else {
            return calificacion;
        }

        return calificacion;
    }

}
