package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.RecursoDTO;

public class RecursoRowMapper implements RowMapper<RecursoDTO> {
public RecursoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
	RecursoDTO recursoDTO = new RecursoDTO();
	
	recursoDTO.setIdRecurso(rs.getInt("FIIDRECURSO"));
	recursoDTO.setNombreRecurso(rs.getString("FCNOMBRE"));
	
	return recursoDTO;
}
}
