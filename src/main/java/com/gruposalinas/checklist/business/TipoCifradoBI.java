package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.TipoCifradoDAO;
import com.gruposalinas.checklist.domain.TipoCifradoDTO;

public class TipoCifradoBI {

private static Logger logger = LogManager.getLogger(TokenBI.class);
	
	@Autowired
	TipoCifradoDAO tipoCifradoDAO;
	
	List<TipoCifradoDTO> datosTipoCifrado= null;	
	
	public boolean insertaTipoCifrado(TipoCifradoDTO bean){
		boolean resultado=false;
		try {
			resultado=tipoCifradoDAO.insertaTipoCifrado(bean);
		} catch (Exception e) {
			logger.info("Algo Ocurrió...... " + e);
		}
		return resultado;
	}

	public boolean actualizaTipoCifrado(TipoCifradoDTO bean) {
		boolean resultado=false;
		try {
			resultado=tipoCifradoDAO.actualizaTipoCifrado(bean);
		} catch (Exception e) {
			logger.info("Algo Ocurrió...... " + e);
		}
		return resultado;
	}
	
	public boolean eliminaTipoCifrado(int bean){
		boolean resultado=false;
		try {
			resultado=tipoCifradoDAO.eliminaTipoCifrado(bean);
		} catch (Exception e) {
			logger.info("Algo Ocurrió...... " + e);
		}
		return resultado;
	}

	public List<TipoCifradoDTO> buscaTipoCifrado(TipoCifradoDTO bean) {
		try {
			datosTipoCifrado=tipoCifradoDAO.buscaTipoCifrado(bean);
		} catch (Exception e) {
			logger.info("Algo Ocurrió...... " + e);
		}
		return datosTipoCifrado;
	}
}
