package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.EmpContacExtDTO;
import com.gruposalinas.checklist.mappers.EmpContacExtRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class EmpContacExtDAOImpl extends DefaultDAO implements EmpContacExtDAO {

    private static Logger logger = LogManager.getLogger(EmpContacExtDAOImpl.class);

    DefaultJdbcCall jdbcInsertaContactoExt;
    DefaultJdbcCall jdbcEliminaContactoExt;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneContactoExt;
    DefaultJdbcCall jbdcActualizaContactoExt;

    public void init() {

        jdbcInsertaContactoExt = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCONTACEXP")
                .withProcedureName("SP_INS_CONTACT");

        jdbcEliminaContactoExt = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCONTACEXP")
                .withProcedureName("SP_DEL_CONTACT");

        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCONTACEXP")
                .withProcedureName("SP_SEL_CONTACT")
                .returningResultSet("RCL_CONTACTO", new EmpContacExtRowMapper());

        jdbcObtieneContactoExt = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCONTACEXP")
                .withProcedureName("SP_SEL_CONTCECO")
                .returningResultSet("RCL_CONTACTO", new EmpContacExtRowMapper());

        jbdcActualizaContactoExt = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCONTACEXP")
                .withProcedureName("SP_ACT_CONTACT");

    }

    public int inserta(EmpContacExtDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_BITGRAL", bean.getBitGral())
                .addValue("PA_ID_EMP", bean.getIdUsu())
                .addValue("PA_DOMICILIO", bean.getDomicilio())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_TELEFONO", bean.getTelefono())
                .addValue("PA_CONTACTO", bean.getContacto())
                .addValue("PA_AREA", bean.getArea())
                .addValue("PA_COMPANIA", bean.getCompania())
                .addValue("PA_AUX", bean.getAux())
                .addValue("PA_AUX2", bean.getAux2());

        out = jdbcInsertaContactoExt.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMCONTACEXP.SP_INS_CONTACT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDTAB");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar ");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean elimina(String idTab) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDTAB", idTab);

        out = jdbcEliminaContactoExt.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMCONTACEXP.SP_DEL_CONTACT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar (" + idTab + ")");
        } else {
            return true;
        }

        return false;

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<EmpContacExtDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<EmpContacExtDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneTodo.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMCONTACEXP.SP_SEL_CONTACT}");

        listaFijo = (List<EmpContacExtDTO>) out.get("RCL_CONTACTO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los Empleados Fijos");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //parametro

    @SuppressWarnings("unchecked")
    public List<EmpContacExtDTO> obtieneDatos(String ceco) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<EmpContacExtDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco);

        out = jdbcObtieneContactoExt.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMCONTACEXP.SP_SEL_CONTCECO}");
        //lleva el nombre del cursor del procedure
        lista = (List<EmpContacExtDTO>) out.get("RCL_CONTACTO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el empleado(" + ceco + ")");
        }
        return lista;

    }

    public boolean actualiza(EmpContacExtDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_BITGRAL", bean.getBitGral())
                .addValue("PA_ID_EMP", bean.getIdUsu())
                .addValue("PA_DOMICILIO", bean.getDomicilio())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_TELEFONO", bean.getTelefono())
                .addValue("PA_CONTACTO", bean.getContacto())
                .addValue("PA_AREA", bean.getArea())
                .addValue("PA_COMPANIA", bean.getCompania())
                .addValue("PA_AUX", bean.getAux())
                .addValue("PA_AUX2", bean.getAux2())
                .addValue("PA_IDTAB", bean.getIdTab());
        out = jbdcActualizaContactoExt.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMCONTACEXP.SP_ACT_CONTACT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar   id( " + bean.getIdTab() + " ceco--> " + bean.getCeco() + ")");
        } else {
            return true;
        }
        return false;

    }

}
