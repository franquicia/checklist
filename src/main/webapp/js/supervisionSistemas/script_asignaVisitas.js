
$(document).ready(function() {
	
	document.getElementById("tipoModal").style.display = 'block';
	
	cargaAsignaciones();
	
	$("#dk2-listbox").css("z-index", "-1");
	$("#dk3-listbox").css("z-index", "-1");
	
	$("#dk1-1").click(function () {    
	    $("#dk2-listbox").css("z-index", "1");
     });
	
	$("#dk1-2").click(function () {    
	    $("#dk2-listbox").css("z-index", "-1");
	    document.getElementById("dk2-combobox").innerHTML="";	    
	    document.getElementById("selHoraBunker").value="0";	  
	    
	    $("#dk3-listbox").css("z-index", "-1");
	    document.getElementById("dk3-combobox").innerHTML="";	    
	    document.getElementById("selHoraBunkerSalida").value="0";	  
	 });
	
	
	$("#dk2-9, #dk2-10, #dk2-11, #dk2-12, #dk2-13, #dk2-14, #dk2-15, #dk2-16, #dk2-17, #dk2-18, #dk2-19, #dk2-20, #dk2-21").click(function () {    
	    $("#dk3-listbox").css("z-index", "1");
     });
	
	/*$("#dk3-0, #dk3-9, #dk3-10, #dk3-11, #dk3-12, #dk3-13, #dk3-14, #dk3-15, #dk3-16, #dk3-17, #dk3-18, #dk3-19, #dk3-20, #dk3-21").click(function () {    
     });*/
});
	
function validaCalendario(){

	var fecha=new Date();
	var diaSemana = new Date(fecha.getTime() - (24*60*60*1000)*0).getUTCDay();
	
	switch (diaSemana) {
	case 0:
		$( "#datepicker2" ).datepicker({
	   	 	minDate: 8,  
	    });
		break;
	case 1:
		$( "#datepicker2" ).datepicker({
	   	 	minDate: 7,  
	    });
		break;
	case 2:
		$( "#datepicker2" ).datepicker({
	   	 	minDate: 6,  
	    });
		break;
	case 3:
		$( "#datepicker2" ).datepicker({
	   	 	minDate: 12,  
	    });
		break;
	case 4:
		$( "#datepicker2" ).datepicker({
	   	 	minDate: 11,  
	    });
		break;
	case 5:
		$( "#datepicker2" ).datepicker({
	   	 	minDate: 10,  
	    });
		break;
	case 6:
		$( "#datepicker2" ).datepicker({
	   	 	minDate: 9,  
	    });
		break;

	default:
		break;
	}
	

}

function cargaAsignaciones() {
	
	$.ajax({
		type : "GET",
		url : '../ajaxConsultaAsignacion.json?idChecklist=null&idCeco=null&idUsuario=null&asigna='+idUsuarioL,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			
			if(json != null){
				if(json.length > 0){
					
					document.getElementById("consultaAsignados").innerHTML="";					
					$("#consultaAsignados").append("<tbody>");
					var i = 0;
					while (json[i] != null) {

						$("#consultaAsignados").append("" +
							"<tr>" +
								"<td style='width:"+((100/9)+2)+"%; text-align: center;'>"+json[i].nombreUsuario+"</td>" +
								"<td style='width:"+((100/9)+2)+"%; text-align: center;'>"+json[i].nombreSucursal+"</td>" +
								"<td style='width:"+(100/9)+"%; text-align: center;'>"+json[i].fecha+"</td>" +
								(json[i].bunker==1 ? "<td style='width:"+(100/9)+"%; text-align: center;'>SI</td>" : "<td style='width:"+(100/9)+"%; text-align: center;'>NO</td>")+
								(json[i].bunker==1 ? "<td style='width:"+(100/9)+"%; text-align: center;'>"+json[i].horaBunker+"</td>" : "<td style='width:"+(100/9)+"%; text-align: center;'>-</td>")+
								(json[i].bunker==1 ? "<td style='width:"+(100/9)+"%; text-align: center;'>"+json[i].horaBunkerSalida+"</td>" : "<td style='width:"+(100/9)+"%; text-align: center;'>-</td>")+
								"<td style='width:"+((100/9)+2)+"%; text-align: center;'>"+json[i].justificacion+"</td>" +
								"<td style='width:"+(100/9)+"%; text-align: center;'><img onclick='editar(&quot;"+json[i].idAsigna+"&quot;,&quot;"+json[i].nombreUsuario+"&quot;,&quot;"+json[i].nombreSucursal+"&quot;,&quot;"+json[i].fecha+"&quot;,&quot;"+json[i].bunker+"&quot;,&quot;"+json[i].horaBunker+"&quot;,&quot;"+json[i].horaBunkerSalida+"&quot;,&quot;"+json[i].justificacion+"&quot;);' style='width: 20px;height: 20px;cursor:pointer;' src='../images/edita03.png'></td>" +
								"<td style='width:"+((100/9)-6)+"%; text-align: center;'><img onclick='elimina("+json[i].idAsigna+")'; style='width: 20px;height: 20px;cursor:pointer;' src='../images/eliminarVerde.png'></td>" +
							"</tr>");			
						i++;
					}
					$("#consultaAsignados").append("</tbody>");
					document.getElementById("tipoModal").style.display = 'none';
				}else{
					document.getElementById("tipoModal").style.display = 'none';
				}
			}
			
		},
		error : function() {
		}
	});
}

function insertaAsignacion(t) {
	
	//alert(t.selHora.value + "," + t.selBunker.value + "," + t.selHoraBunker.value + "," + t.selHoraBunkerSalida.value);
	
	if(t.empleado.value=="" || t.sucursal.value=="" || t.datepicker2.value=="" || t.selHora.value==0 || t.selBunker.value==0 || t.textarea.value=="" ){
		alert('Faltan campos por llenar');
		return false;
	}else{
		var horaBunker = "";
		var horaBunkerSalida = "";
		if(t.selBunker.value==1){
			if(t.selHoraBunker.value != 0 && t.selHoraBunker.value >= parseInt(t.selHora.value)){
				horaBunker = t.datepicker2.value+' ' +t.selHoraBunker.value + ':00:00';
				if(t.selHoraBunkerSalida.value >= parseInt(t.selHoraBunker.value)){
					horaBunkerSalida = t.datepicker2.value+' ' +t.selHoraBunkerSalida.value + ':00:00';
				}else{
					alert('Selecciona una hora de salida de bunker correcta');
					//document.getElementById("tipoModal").style.display = 'none';
					return false;
				}
			}else{
				alert('Selecciona una hora de ingreso al bunker correcta');
				//document.getElementById("tipoModal").style.display = 'none';
				return false;
			}
		}else{
			horaBunker = "null";
			horaBunkerSalida = "null";
		}
		
		document.getElementById("tipoModal").style.display = 'block';
		
		$.ajax({
			type : "GET",
			url : '../ajaxInsertaAsignacion.json?'+
				'idUsuario='+(t.empleado.value).split("-")[0]+
				'&nomUsuario='+t.empleado.value+
				'&idSucursal='+(t.sucursal.value).split("-")[0]+
				'&nomSucursal='+t.sucursal.value+
				'&fecha='+t.datepicker2.value+' '+t.selHora.value+':00:00'+
				'&bunker=' + t.selBunker.value + ''+
				'&horaBunker='+ horaBunker +
				'&horaBunkerSalida='+ horaBunkerSalida +
				'&justificacion=' + t.textarea.value +
				'&idChecklist=0'+
				'&asigna='+idUsuarioL,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			paramName : "tagName",
			delimiter : ",",
			async : false,
			success : function(json) {
				document.getElementById("tipoModal").style.display = 'none';
				return true;
			},
			error : function() {
				alert("No se pudo registrar");
				document.getElementById("tipoModal").style.display = 'none';
				return false;
			}
		});
	}
}

function elimina(e){
	var res = false;
	$("#dialog-confirm").dialog({
		resizable : false,
		height : "auto",
		width : 350,
		modal : true,
		async : false,
		buttons : {
			Eliminar : function() {
				document.getElementById("tipoModal").style.display = 'block';
				res = eliminaById(e);
				if(res){
					location.href = "/checklist/central/vistaProgramaVisitas.htm";
					//cargaAsignaciones();
					//$(this).dialog("close");
					//document.getElementById("tipoModal").style.display = 'none';
				}else{
					alert('No se pudo eliminar');
					$(this).dialog("close");
					document.getElementById("tipoModal").style.display = 'none';
				}
			},
			Cancelar : function() {
				$(this).dialog("close");
			}
		}
	});
}

function eliminaById(id){
	var respuesta = false;
	$.ajax({
		type : "GET",
		url : '../ajaxEliminaAsignacion.json?idAsigna='+id,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		async : false,
		success : function(json) {
			respuesta = true;
		},
		error : function() {
			respuesta = false;
		}
	});
	return respuesta;
}

function editar(idAsigna, nombre, sucursal, fecha, bunker, bunkerEntra, bunkerSale, justificacion){

	document.getElementById("btnMuestra").innerHTML="";
	$("#btnMuestra").append("" +
		"<input onclick='actualiza(&quot;"+idAsigna+"&quot;);' type='button' value='Guardar' id='btnRegistrar' style='float: left;' class='btnRegistrar'> "+
		"<input onclick='cancelar();' type='button' value='Cancelar' id='btnRegistrar' style='float: right;' class='btnRegistrar'>");

	document.getElementById("empleado").value=nombre;
	document.getElementById("sucursal").value=sucursal;
	
	var date =""+fecha.substring(0, 10);
	$( "#datepicker2" ).datepicker().datepicker("setDate", new Date(date.replace(/-/g, '\/')));
	
	var fechaVisita=fecha.substring(11,13);
	if(fechaVisita=="09"){
		$("#selHora option[value='9']").prop('selected', true);
	}else{
		$("#selHora option[value='"+fechaVisita+"']").prop('selected', true);
	}	
	
	limpiaHora();
	$("#dk0-"+fecha.substring(11,13)).addClass( "dk-option-selected" );
	
	if(parseInt(fecha.substring(11,13))<12){
		$("#dk0-combobox").text(fecha.substring(11,16)+" am.");
	}else{
		$("#dk0-combobox").text((parseInt(fecha.substring(11,13))-12) +":"+fecha.substring(14,16)+" pm.");
	}
	
	document.getElementById("selBunker").value=bunker;
	if(bunker=="1"){
		
		$("#dk1-combobox").text("SI");
		limpiaBunker();
		$("#dk1-1").addClass("dk-option-selected");
		
		
		var fechaEntra=bunkerEntra.substring(0,2);
		if(fechaEntra=="09"){
			$("#selHoraBunker option[value='9']").prop('selected', true);
		}else{
			$("#selHoraBunker option[value='"+fechaEntra+"']").prop('selected', true);
		}
		
		if(parseInt(bunkerEntra.substring(0,2))<12){
			$("#dk2-combobox").text(bunkerEntra+" am.");
		}else if(parseInt(bunkerEntra.substring(0,2))==12){
			$("#dk2-combobox").text(bunkerEntra+" pm.");
		}else{
			$("#dk2-combobox").text((parseInt(bunkerEntra.substring(0,2))-12) +":"+bunkerEntra.substring(3,5)+" pm.");
		}
		limpiaHoraBunker();
		$("#dk2-"+bunkerEntra.substring(0,2)).addClass("dk-option-selected");
	
		
		//document.getElementById("selHoraBunkerSalida").value=bunkerSale.substring(0,2);
		
		limpiaHoraBunkerSalida();
		$("#dk3-"+bunkerSale.substring(0,2)).addClass("dk-option-selected");
		var fechaSale=bunkerSale.substring(0,2);
		if(fechaSale=="09"){
			$("#selHoraBunkerSalida option[value='9']").prop('selected', true);
		}else{
			$("#selHoraBunkerSalida option[value='"+fechaSale+"']").prop('selected', true);
		}

		if(parseInt(bunkerSale.substring(0,2))<12){
			$("#dk3-combobox").text(bunkerSale+" am.");
		}else if(parseInt(bunkerSale.substring(0,2))==12){
			$("#dk3-combobox").text(bunkerSale+" pm.");
		}else{
			$("#dk3-combobox").text((parseInt(bunkerSale.substring(0,2))-12) +":"+bunkerSale.substring(3,5)+" pm.");
		}
		
				
		$("#dk2-listbox").css("z-index", "1");
		$("#dk3-listbox").css("z-index", "1");
	}else{
		$("#dk1-combobox").text("NO");
		$("#dk2-combobox").text("");
		$("#dk3-combobox").text("");
		limpiaBunker();
		limpiaHoraBunker();
		limpiaHoraBunkerSalida();
		$("#dk2-listbox").css("z-index", "-1");
		$("#dk3-listbox").css("z-index", "-1");
		$("#dk1-2").addClass("dk-option-selected");
	}
	
	document.getElementById("textarea").value=justificacion;
}

function actualiza(idAsigna){
	
	var empleado = document.getElementById("empleado").value;
	var sucursal = document.getElementById("sucursal").value ;
	var datepicker2 = document.getElementById("datepicker2").value;
	var selHora = document.getElementById("selHora").value;
	var selBunker = document.getElementById("selBunker").value;
	var selHoraBunker = document.getElementById("selHoraBunker").value;
	var selHoraBunkerSalida = document.getElementById("selHoraBunkerSalida").value;
	var textarea = document.getElementById("textarea").value;

	
	//alert('empleado ' + empleado + '\nsucursal ' + sucursal + '\ndatepicker2 ' + datepicker2 + '\nselHora ' + selHora + '\nselBunker ' + selBunker + '\ntextarea ' + textarea);
	//alert('selHora ' + selHora + '\nselBunker ' + selBunker + '\nselHoraBunker ' + selHoraBunker + '\nselHoraBunkerSalida ' + selHoraBunkerSalida );
	
	if(empleado=="" || sucursal=="" || datepicker2=="" || selHora=="" || selBunker=="" || textarea=="" ){
		alert('Faltan campos por llenar');
		//alert('empleado ' + empleado + '\nsucursal ' + sucursal + '\ndatepicker2 ' + datepicker2 + '\nselHora ' + selHora + '\nselBunker ' + selBunker + '\ntextarea ' + textarea);
		return false;
	}else{
		var horaBunker = "";
		var horaBunkerSalida = "";
		if(selBunker==1){
			if(selHoraBunker != 0 && selHoraBunker >= parseInt(selHora)){
				horaBunker = datepicker2+' ' + selHoraBunker + ':00:00';
				if(selHoraBunkerSalida >= parseInt(selHoraBunker)){
					horaBunkerSalida = datepicker2  + ' ' + selHoraBunkerSalida + ':00:00';
				}else{
					alert('Selecciona una hora de salida de bunker correcta');
					//document.getElementById("tipoModal").style.display = 'none';
					return false;
				}
			}else{
				alert('Selecciona una hora de ingreso al bunker correcta');
				//document.getElementById("tipoModal").style.display = 'none';
				return false;
			}
		}else{
			horaBunker = "null";
			horaBunkerSalida = "null";
		}
		
		document.getElementById("tipoModal").style.display = 'block';
		
		$.ajax({
			type : "GET",
			url : '../ajaxActualizaAsignacion.json?'+
				'idAsigna='+idAsigna+
				'&idUsuario='+(empleado).split("-")[0]+
				'&nomUsuario='+empleado+
				'&idSucursal='+(sucursal).split("-")[0]+
				'&nomSucursal='+sucursal+
				'&fecha='+datepicker2+' '+selHora+':00:00'+
				'&bunker=' + selBunker + ''+
				'&horaBunker='+ horaBunker +
				'&horaBunkerSalida='+ horaBunkerSalida +
				'&justificacion=' + textarea +
				'&idChecklist=0'+
				'&asigna='+idUsuarioL,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			paramName : "tagName",
			delimiter : ",",
			async : false,
			success : function(json) {
				location.href = "/checklist/central/vistaProgramaVisitas.htm";
				//document.getElementById("tipoModal").style.display = 'none';
				return true;
			},
			error : function() {
				alert("No se pudo registrar");
				document.getElementById("tipoModal").style.display = 'none';
				return false;
			}
		});		
	}
}

function cancelar(){
	location.href = "/checklist/central/vistaProgramaVisitas.htm";
}

function nobackbutton(){
	window.location.hash="no-back-button";
	window.location.hash="Again-No-back-button" //chrome
	window.onhashchange=function(){window.location.hash="";}
}

function limpiaHora(){
	for(var i=0; i<21; i++){
		$("#dk0-"+i).removeClass("dk-option-selected");
	}
}

function limpiaBunker(){
	for(var i=0; i<3; i++){
		$("#dk1-"+i).removeClass("dk-option-selected");
	}
}

function limpiaHoraBunker(){
	for(var i=0; i<22; i++){
		$("#dk2-"+i).removeClass("dk-option-selected");
	}
}

function limpiaHoraBunkerSalida(){
	for(var i=0; i<22; i++){
		$("#dk3-"+i).removeClass("dk-option-selected");
	}
}