package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.VersionExpanDTO;


public interface VersionExpanDAO {

	  public int inserta(VersionExpanDTO bean)throws Exception;
		
		public boolean elimina(String idTab)throws Exception;
		
		public List<VersionExpanDTO> obtieneDatos(int idVers) throws Exception; 
		
		public List<VersionExpanDTO> obtieneInfo() throws Exception; 
		
		public boolean actualiza(VersionExpanDTO bean)throws Exception;
		
		public boolean actualiza2(VersionExpanDTO bean)throws Exception;
		
	}