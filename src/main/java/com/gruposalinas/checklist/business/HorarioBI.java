package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.HorarioDAO;
import com.gruposalinas.checklist.domain.HorarioDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class HorarioBI {
	
	private static Logger logger = LogManager.getLogger(HorarioBI.class);
	
	@Autowired
	
	HorarioDAO horarioDAO;
	
	List<HorarioDTO> listaHorarios = null;
	List<HorarioDTO> listaHorario = null;
	
	public List<HorarioDTO> obtieneHorario(){
		try{
			listaHorarios = horarioDAO.obtieneHorario();
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Horarios");
						
		}
				
		return listaHorarios;
	}
	
	public List<HorarioDTO> obtienehorario(int idHorario){
		try{
			listaHorario = horarioDAO.obtenerHorario(idHorario);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Horarios");
						
		}
				
		return listaHorario;
	}

	public int insertaHorario(HorarioDTO horario){
		int idHorario =0;
		try{
			idHorario = horarioDAO.insertaHorario(horario);
		}catch (Exception e) {
			logger.info("No fue posible insertar el Horario");
			
		}
		
		return idHorario;
	}
	
	public boolean actualizaHorario(HorarioDTO horario){
		
		boolean respuesta = false;
		try{
			respuesta = horarioDAO.actualizaHorario(horario);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Horario");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaHorario(int idHorario){
		boolean respuesta = false;
		try{
			respuesta = horarioDAO.eliminaHorario(idHorario);
		}catch(Exception e){
			logger.info("No fue posible borrar el Horario");
			
		}
		
		return respuesta;
	}
}
