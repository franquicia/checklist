package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ArbolDecisionDTO;

public interface ArbolDecisionTDAO {
	
	public List<ArbolDecisionDTO> buscaArbolDecisionTemp(int idChecklist) throws Exception;
	
	public int insertaArbolDecisionTemp(ArbolDecisionDTO bean) throws Exception;
	
	public boolean actualizaArbolDecisionTemp(ArbolDecisionDTO bean) throws Exception;
	
	public boolean eliminaArbolDecisionTemp(int idArbolDecision) throws Exception;
	
	

}
