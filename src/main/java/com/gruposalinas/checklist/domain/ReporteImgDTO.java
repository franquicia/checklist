package com.gruposalinas.checklist.domain;

public class ReporteImgDTO {
	

	private String ceco;
	private String sucursal;
	private String fecha;
	private String protocolo;
	private int idChecklist;
	private int ordenGpo;
	private int idBitacora;
	private String ruta;
	
	public String getCeco() {
		return ceco;
	}
	public void setCeco(String ceco) {
		this.ceco = ceco;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getProtocolo() {
		return protocolo;
	}
	public void setProtocolo(String protocolo) {
		this.protocolo = protocolo;
	}
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public int getOrdenGpo() {
		return ordenGpo;
	}
	public void setOrdenGpo(int ordenGpo) {
		this.ordenGpo = ordenGpo;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	@Override
	public String toString() {
		return "ReporteImgDTO [ceco=" + ceco + ", sucursal=" + sucursal + ", fecha=" + fecha + ", protocolo="
				+ protocolo + ", idChecklist=" + idChecklist + ", ordenGpo=" + ordenGpo + ", idBitacora=" + idBitacora
				+ ", ruta=" + ruta + "]";
	}
	
		
}


