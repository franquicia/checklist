package com.gruposalinas.checklist.domain;

public class ReporteHallazgosDTO {
	
	//Generico
			private int conteoTotal;
			private int conteoPorAtender;
			private int conteoPorAutorizar;
			private int conteoAtendido;
			private int conteoRechazado;
			
			private String ceco;
			private String negocio;
			private int fase;
			private int proyecto;
			private int idCanal;
	
			
			private String fechaInicio;
			private String fechaFin;
			private String nombreProyecto;
			private String nombreFase;
			private String nombreCeco;
			
			public int getConteoTotal() {
				return conteoTotal;
			}
			public void setConteoTotal(int conteoTotal) {
				this.conteoTotal = conteoTotal;
			}
			public int getConteoPorAtender() {
				return conteoPorAtender;
			}
			public void setConteoPorAtender(int conteoPorAtender) {
				this.conteoPorAtender = conteoPorAtender;
			}
			public int getConteoPorAutorizar() {
				return conteoPorAutorizar;
			}
			public void setConteoPorAutorizar(int conteoPorAutorizar) {
				this.conteoPorAutorizar = conteoPorAutorizar;
			}
			public int getConteoAtendido() {
				return conteoAtendido;
			}
			public void setConteoAtendido(int conteoAtendido) {
				this.conteoAtendido = conteoAtendido;
			}
			public int getConteoRechazado() {
				return conteoRechazado;
			}
			public void setConteoRechazado(int conteoRechazado) {
				this.conteoRechazado = conteoRechazado;
			}
			public String getCeco() {
				return ceco;
			}
			public void setCeco(String ceco) {
				this.ceco = ceco;
			}
			public String getNegocio() {
				return negocio;
			}
			public void setNegocio(String negocio) {
				this.negocio = negocio;
			}
			public int getFase() {
				return fase;
			}
			public void setFase(int fase) {
				this.fase = fase;
			}
			public int getProyecto() {
				return proyecto;
			}
			public void setProyecto(int proyecto) {
				this.proyecto = proyecto;
			}
			public int getIdCanal() {
				return idCanal;
			}
			public void setIdCanal(int idCanal) {
				this.idCanal = idCanal;
			}
			public String getFechaInicio() {
				return fechaInicio;
			}
			public void setFechaInicio(String fechaInicio) {
				this.fechaInicio = fechaInicio;
			}
			public String getFechaFin() {
				return fechaFin;
			}
			public void setFechaFin(String fechaFin) {
				this.fechaFin = fechaFin;
			}
			public String getNombreProyecto() {
				return nombreProyecto;
			}
			public void setNombreProyecto(String nombreProyecto) {
				this.nombreProyecto = nombreProyecto;
			}
			public String getNombreFase() {
				return nombreFase;
			}
			public void setNombreFase(String nombreFase) {
				this.nombreFase = nombreFase;
			}
			public String getNombreCeco() {
				return nombreCeco;
			}
			public void setNombreCeco(String nombreCeco) {
				this.nombreCeco = nombreCeco;
			}
			@Override
			public String toString() {
				return "ReporteHallazgosDTO [conteoTotal=" + conteoTotal + ", conteoPorAtender=" + conteoPorAtender
						+ ", conteoPorAutorizar=" + conteoPorAutorizar + ", conteoAtendido=" + conteoAtendido
						+ ", conteoRechazado=" + conteoRechazado + ", ceco=" + ceco + ", negocio=" + negocio + ", fase="
						+ fase + ", proyecto=" + proyecto + ", idCanal=" + idCanal + ", fechaInicio=" + fechaInicio
						+ ", fechaFin=" + fechaFin + ", nombreProyecto=" + nombreProyecto + ", nombreFase=" + nombreFase
						+ ", nombreCeco=" + nombreCeco + "]";
			}
		

		
		
	}