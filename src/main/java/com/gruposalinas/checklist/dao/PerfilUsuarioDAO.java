package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.PerfilUsuarioDTO;

public interface PerfilUsuarioDAO {
	
	public List<PerfilUsuarioDTO>obtienePerfiles(String idUsuario, String idPerfil) throws Exception;
	
	public boolean insertaPerfilUsuario(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception;
	
	public boolean insertaPerfilUsuarioNvo(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception;
	
	public boolean actualizaPerfilUsuario(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception; 
	
	public boolean eliminaPerfilUsaurio(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception;
	
	public boolean verificaPerfilTienda(String idUsuario) throws Exception;
	
}
