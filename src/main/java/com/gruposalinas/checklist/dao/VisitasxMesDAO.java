package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.VisitasxMesDTO;

public interface VisitasxMesDAO {
	
	public List<VisitasxMesDTO> obtieneVisitas(int checklist, String fechaInicio, String fechaFin)throws Exception;

}
