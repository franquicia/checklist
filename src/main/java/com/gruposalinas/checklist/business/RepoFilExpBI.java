package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.util.UtilFRQ;
import com.google.gson.JsonObject;
import com.gruposalinas.checklist.dao.RepoFilExpDAO;
import com.gruposalinas.checklist.dao.SoftNvoDAO;
import com.gruposalinas.checklist.domain.RepoFilExpDTO;
import com.gruposalinas.checklist.domain.SoftNvoDTO;


public class RepoFilExpBI {

	private static Logger logger = LogManager.getLogger(RepoFilExpBI.class);

	private List<RepoFilExpDTO> listafila;

	private List<RepoFilExpDTO> lista;
	private List<SoftNvoDTO> listaCalculos;
	@Autowired
	RepoFilExpDAO repoFilExpDAO;
	@Autowired
	SoftNvoDAO softNvoDAO;

	// FILTRO 1
	public List<RepoFilExpDTO> obtieneDatosGen(String ceco, String nombreCeco, String fechaIni, String fechaFin,
			String idRecorrido) {

		try {
			listafila = repoFilExpDAO.obtieneDatosGen(ceco, nombreCeco, fechaIni, fechaFin, idRecorrido);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// FILTRO 2

	public List<RepoFilExpDTO> obtieneInfo(String ceco, String idRecorrido) {

		try {
			listafila = repoFilExpDAO.obtieneInfo(ceco, idRecorrido);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// FILTRO 3
	public List<RepoFilExpDTO> obtieneInfofiltro(String bita1, String bita2, String pregFilt) {

		try {
			listafila = repoFilExpDAO.obtieneInfofiltro(bita1, bita2, pregFilt);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// PREGUNTAS

	public List<RepoFilExpDTO> obtienePreguntas(String ceco) {

		try {
			listafila = repoFilExpDAO.obtienePreguntas(ceco);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// PREGUNTAS HIJAS

	public List<RepoFilExpDTO> obtienePreguntasHijas(String ceco) {

		try {
			lista = repoFilExpDAO.obtienePreguntasHijas(ceco);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return lista;
	}

	// DASH

	public List<RepoFilExpDTO> obtienedash(String ceco, String idRecorrido, String aux) {

		try {
			lista = repoFilExpDAO.obtienedash(ceco, idRecorrido, aux);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos del dash ");
			UtilFRQ.printErrorLog(null, e);
		}

		return lista;
	}

	public List<RepoFilExpDTO> obtienedash2(String ceco) {

		try {
			lista = repoFilExpDAO.obtienedash2(ceco);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos del dash ");
			UtilFRQ.printErrorLog(null, e);
		}

		return lista;
	}

	public List<RepoFilExpDTO> obtienedash3(String bita1) {

		try {
			lista = repoFilExpDAO.obtienedash3(bita1);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos del dash ");
			UtilFRQ.printErrorLog(null, e);
		}

		return lista;
	}

	// REPORTE FECHAS
	public List<RepoFilExpDTO> repoFechas(String fechaIni, String fechaFin) {

		try {
			listafila = repoFilExpDAO.repoFechas(fechaIni, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// REPO ACTA
	public List<RepoFilExpDTO> obtieneInfofActa(String bita1) {

		try {
			listafila = repoFilExpDAO.obtieneInfofActa(bita1);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// PREGUNTAS Acta

	public List<RepoFilExpDTO> obtienePreguntasActa(String bita1) {

		try {
			listafila = repoFilExpDAO.obtienePreguntasActa(bita1);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// REPORTE FECHAS NUEVO
	public List<RepoFilExpDTO> repoFechasNvo(String fechaIni, String fechaFin) {

		try {
			listafila = repoFilExpDAO.repoFechasNvo(fechaIni, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	// FILTRO 1 CECO FASE PROYECTO
	public List<RepoFilExpDTO> obtieneDatosFiltro1(String ceco, String fase, String proyecto, String nombreCeco,String recorrido,String fechaIni, String fechaFin) {

		try {
			listafila = repoFilExpDAO.obtieneDatosFiltro1(ceco, fase, proyecto, nombreCeco,recorrido, fechaIni, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}
	// trae respuestas
		public String obtieneDatosFiltro1Fase(String ceco, String fase, String proyecto, String nombreCeco,String recorrido,String fechaIni, String fechaFin) {
				JSONObject respuesta = new JSONObject();
			try {
				List<RepoFilExpDTO> listaSoft = new ArrayList<>();
				Map<String, Object> listaSofts = null;

				listaSofts = (Map<String, Object>) repoFilExpDAO.obtieneDatosFiltro1Fase(ceco, fase, proyecto, nombreCeco,recorrido, fechaIni, fechaFin);
				
				listaSoft = (List<RepoFilExpDTO>) listaSofts.get("listaSoft");
				@SuppressWarnings("unchecked")
				List<SoftNvoDTO> listaCalculos = (List<SoftNvoDTO>) listaSofts.get("listaCalculos");
				JSONArray soft = new JSONArray();
				JSONArray calculos=new JSONArray();
				
				//lista tabla soft
				for(int i=0; i < listaSoft.size();i++) {
				
					JSONObject obj =new  JSONObject();
					obj.put("bitacora", listaSoft.get(i).getIdBita());
					obj.put("ceco", listaSoft.get(i).getCeco());
					obj.put("nombreCeco", listaSoft.get(i).getNombreCeco());
					obj.put("statusCeco", listaSoft.get(i).getActivoCeco());
					obj.put("cecoPadre", listaSoft.get(i).getCecoSuperior());
					obj.put("direccion", listaSoft.get(i).getDireccion());
					obj.put("idFase", listaSoft.get(i).getFase());
					obj.put("idProyecto", listaSoft.get(i).getProyecto());
					obj.put("idSoft", listaSoft.get(i).getIdSoft());
					obj.put("region", listaSoft.get(i).getRegion());
					obj.put("territorio", listaSoft.get(i).getTerritorio());
					obj.put("zona", listaSoft.get(i).getZona());
					obj.put("ceco", listaSoft.get(i).getCeco());
					obj.put("liderObra", listaSoft.get(i).getNomUsu());
					obj.put("periodo", listaSoft.get(i).getPerido());
					obj.put("recorrido", listaSoft.get(i).getIdRecorrido());

					soft.put(obj);
				
				} 
			
			
				   for (RepoFilExpDTO aux : listaSoft)
			              
		            {
		              int papa=0;
		              JSONObject resp = new JSONObject();
		              
		              System.out.println("softP -->: " + aux.getIdSoft());
		              //System.out.println("Padre: " + aux.getIdPreg());
		              
		              papa = aux.getIdSoft();

		              for(SoftNvoDTO calculosLista:listaCalculos)
		              	{
		                //System.out.println("issoft:--> " + aux.getIdSoft());
		                //System.out.println("issoft 2-->: " + calculosLista.getIdSoft());
			                if(aux.getIdSoft()==calculosLista.getIdSoft())
			                {
			                	JSONObject obj =new  JSONObject();
			                	
			                	obj.put("idSoft", calculosLista.getIdSoft());
								obj.put("clasificacion", calculosLista.getClasificacion());
								obj.put("itemSi", calculosLista.getItemSi());		
								obj.put("itemNo", calculosLista.getItemNo());
								obj.put("itemNA", calculosLista.getItemNa());
								obj.put("itemRevisados", calculosLista.getItemrevisados());
								obj.put("itemTotales", calculosLista.getItemTotales());
								obj.put("itemImperdonables", calculosLista.getItemImperd());
								obj.put("porcentajeRespSi", calculosLista.getPorcentajeReSi());
								obj.put("porcentajeRespNo", calculosLista.getPorcentajeReNo());
								obj.put("porcentajeTotalSi", calculosLista.getPorcentajeReSi());
								obj.put("porcentajeTotalNo", calculosLista.getPorcentajeToNo());
								obj.put("porcentajeTotalNa", calculosLista.getPorcentajeToNA());
								obj.put("ponderacionNa", calculosLista.getPonderacionNA());
								obj.put("ponderacionMaxReal", calculosLista.getPonderacionMaximaReal());
								obj.put("ponderacionObtenidaCalcula", calculosLista.getPonderacionObtenidaCalculada());
								obj.put("ponderacionObtenidaReal", calculosLista.getPonderacionObtenidaReal());
								obj.put("calificacionReal", calculosLista.getCalificacionReal());
								obj.put("itemSi", calculosLista.getCalificacionCalculada());
								
								 calculos.put(obj);

			                	
				    			} //IF 
			                }//SEG FOR

		              	} //PRIMER FOR
		    
				
			respuesta.append("listaSoft", soft);
			respuesta.append("calculosLista", calculos);

		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta.toString();
	}
		
		public List<RepoFilExpDTO>  obtieneDatosFiltro1FaseResult(String ceco, String fase, String proyecto, String nombreCeco,String recorrido,String fechaIni, String fechaFin) {
			JSONObject respuesta = new JSONObject();
			
			List<RepoFilExpDTO> listaSoft = new ArrayList<>();
			
		try {
			Map<String, Object> listaSofts = null;
			
			listaSofts = (Map<String, Object>) repoFilExpDAO.obtieneDatosFiltro1Fase(ceco, fase, proyecto, nombreCeco,recorrido, fechaIni, fechaFin);

			listaSoft = (List<RepoFilExpDTO>) listaSofts.get("listaSoft");
			@SuppressWarnings("unchecked")
			List<SoftNvoDTO> listaCalculos = (List<SoftNvoDTO>) listaSofts.get("listaCalculos");
			JSONArray soft = new JSONArray();
			JSONArray calculos=new JSONArray();
			
			//lista tabla soft
			for(int i=0; i < listaSoft.size();i++) {
			
				JSONObject obj =new  JSONObject();
				obj.put("bitacora", listaSoft.get(i).getIdBita());
				obj.put("ceco", listaSoft.get(i).getCeco());
				obj.put("nombreCeco", listaSoft.get(i).getNombreCeco());
				obj.put("statusCeco", listaSoft.get(i).getActivoCeco());
				obj.put("cecoPadre", listaSoft.get(i).getCecoSuperior());
				obj.put("direccion", listaSoft.get(i).getDireccion());
				obj.put("idFase", listaSoft.get(i).getFase());
				obj.put("idProyecto", listaSoft.get(i).getProyecto());
				obj.put("idSoft", listaSoft.get(i).getIdSoft());
				obj.put("region", listaSoft.get(i).getRegion());
				obj.put("territorio", listaSoft.get(i).getTerritorio());
				obj.put("zona", listaSoft.get(i).getZona());
				obj.put("ceco", listaSoft.get(i).getCeco());
				obj.put("liderObra", listaSoft.get(i).getNomUsu());
				obj.put("periodo", listaSoft.get(i).getPerido());
				obj.put("recorrido", listaSoft.get(i).getIdRecorrido());

				soft.put(obj);
			
			} 
		
		
			   for (RepoFilExpDTO aux : listaSoft)
		              
	            {
	              int papa=0;
	              JSONObject resp = new JSONObject();
	              
	              System.out.println("softP -->: " + aux.getIdSoft());
	              //System.out.println("Padre: " + aux.getIdPreg());
	              
	              papa = aux.getIdSoft();

	              for(SoftNvoDTO calculosLista:listaCalculos)
	              	{
	                //System.out.println("issoft:--> " + aux.getIdSoft());
	                //System.out.println("issoft 2-->: " + calculosLista.getIdSoft());
		                if(aux.getIdSoft()==calculosLista.getIdSoft())
		                {
		                	JSONObject obj =new  JSONObject();
		                	
		                	obj.put("idSoft", calculosLista.getIdSoft());
							obj.put("clasificacion", calculosLista.getClasificacion());
							obj.put("itemSi", calculosLista.getItemSi());		
							obj.put("itemNo", calculosLista.getItemNo());
							obj.put("itemNA", calculosLista.getItemNa());
							obj.put("itemRevisados", calculosLista.getItemrevisados());
							obj.put("itemTotales", calculosLista.getItemTotales());
							obj.put("itemImperdonables", calculosLista.getItemImperd());
							obj.put("porcentajeRespSi", calculosLista.getPorcentajeReSi());
							obj.put("porcentajeRespNo", calculosLista.getPorcentajeReNo());
							obj.put("porcentajeTotalSi", calculosLista.getPorcentajeReSi());
							obj.put("porcentajeTotalNo", calculosLista.getPorcentajeToNo());
							obj.put("porcentajeTotalNa", calculosLista.getPorcentajeToNA());
							obj.put("ponderacionNa", calculosLista.getPonderacionNA());
							obj.put("ponderacionMaxReal", calculosLista.getPonderacionMaximaReal());
							obj.put("ponderacionObtenidaCalcula", calculosLista.getPonderacionObtenidaCalculada());
							obj.put("ponderacionObtenidaReal", calculosLista.getPonderacionObtenidaReal());
							obj.put("calificacionReal", calculosLista.getCalificacionReal());
							obj.put("itemSi", calculosLista.getCalificacionCalculada());
							
							 calculos.put(obj);

		                	
			    			} //IF 
		                }//SEG FOR

	              	} //PRIMER FOR
	    
			
		respuesta.append("listaSoft", soft);
		respuesta.append("calculosLista", calculos);
		
		try {
			listaSoft.get(0).setCalif(respuesta.getJSONObject("calculosLista").getDouble("ponderacionObtenidaCalcula"));
		} catch (Exception e) {
			logger.info("AP al poner la calificación");
		}

	} catch (Exception e) {
		logger.info("No fue posible obtener los datos");
		UtilFRQ.printErrorLog(null, e);
	}

		//Fallo
	return listaSoft;
}
		
		public List<RepoFilExpDTO> obtienedashCecoProyecto(String ceco, String fase, String proyecto) {

			try {
				listafila = repoFilExpDAO.obtienedashCecoProyecto(ceco, fase,proyecto);
			} catch (Exception e) {
				logger.info("No fue posible obtener datos ");
				UtilFRQ.printErrorLog(null, e);
			}

			return listafila;
		}
		
		//ACTA CECO FASE PROYECTO
		public List<RepoFilExpDTO> obtieneActaCecoFaseProyecto(String ceco, String fase, String proyecto) {

			try {
				listafila = repoFilExpDAO.obtieneActaCecoFaseProyecto(ceco, fase,proyecto);
			} catch (Exception e) {
				logger.info("No fue posible obtener datos ");
				UtilFRQ.printErrorLog(null, e);
			}

			return listafila;
		}
		//ACTA CECO FASE PROYECTO PREGUNTAS
		public List<RepoFilExpDTO> obtienePregActaCecoFaseProyecto(String ceco, String fase, String proyecto) {

			try {
				listafila = repoFilExpDAO.obtienePregActaCecoFaseProyecto(ceco, fase,proyecto);
			} catch (Exception e) {
				logger.info("No fue posible obtener datos ");
				UtilFRQ.printErrorLog(null, e);
			}

			return listafila;
		}		
		//DASHBOARD GENERICO NUEVO 
		public List<RepoFilExpDTO> obtieneInfoDashboardGenerico(String bita1, String parametro)throws Exception {

			try {
				lista = repoFilExpDAO.obtieneInfoDashboardGenerico(bita1,parametro);
			} catch (Exception e) {
				logger.info("No fue posible obtener datos del dash ");
				UtilFRQ.printErrorLog(null, e);
			}

			return lista;
		}
	
}
