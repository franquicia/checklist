package com.gruposalinas.checklist.domain;

public class NegocioAdicionalDTO {

	private int usuario;
	private int negocio;
	public int getUsuario() {
		return usuario;
	}
	public void setUsuario(int usuario) {
		this.usuario = usuario;
	}
	public int getNegocio() {
		return negocio;
	}
	public void setNegocio(int negocio) {
		this.negocio = negocio;
	}

}
