package com.gruposalinas.checklist.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.AdicionalesBI;
import com.gruposalinas.checklist.business.AsignaTransfBI;
import com.gruposalinas.checklist.business.CorreoBI;
import com.gruposalinas.checklist.business.HallazgosEviExpBI;
import com.gruposalinas.checklist.business.HallazgosExpBI;
import com.gruposalinas.checklist.business.HallazgosTransfBI;
import com.gruposalinas.checklist.business.PerfilUsuarioBI;
import com.gruposalinas.checklist.business.SucursalChecklistBI;
import com.gruposalinas.checklist.domain.AsignaTransfDTO;
import com.gruposalinas.checklist.domain.HallazgosExpDTO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;
import com.gruposalinas.checklist.domain.PerfilUsuarioDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;

@Controller
public class TransformacionController {

	private static final Logger logger = LogManager.getLogger(TransformacionController.class);

	@Autowired
	HallazgosExpBI hallazgosExpBI;

	@Autowired
	AdicionalesBI adicionalesExpBI;

	@Autowired
	SucursalChecklistBI sucursalChecklistBI;

	@Autowired
	HallazgosEviExpBI hallazgosEviExpBI;

	@Autowired
	PerfilUsuarioBI perfilUsuarioBI;
    
    @Autowired
    AsignaTransfBI asignaTransfBI;  
    
    @Autowired
    HallazgosTransfBI hallazgosTransfBI;
    
	@Autowired
	CorreoBI correoBI;

    @RequestMapping(value = { "central/seleccionSucursalTransformacionHallazgos.htm" }, method = RequestMethod.GET)
	public ModelAndView seleccionSucursalTransformacionHallazgos(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "idCeco", required = false, defaultValue = "0") int idCeco,
			@RequestParam(value = "sucursal", required = false, defaultValue = "") String sucursal,
			@RequestParam(value = "busqueda", required = false, defaultValue = "0") int busqueda)
			throws ServletException, IOException {
		
		UsuarioDTO userSession = null;
		userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		List<PerfilUsuarioDTO> listaPerfiles = null;
		int estado = 0;
		
		String respuesta="";
		List<AsignaTransfDTO> listaFiltrosSucursales;
		String bodyJson = "";
		String json = "";


		if (userSession != null) {
				//listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(),""+FRQConstantes.PERFIL_TRANSFORMACION_SUP_OBRA);
				//if (listaPerfiles != null && listaPerfiles.size() > 0) estado = FRQConstantes.PERFIL_TRANSFORMACION_SUP_OBRA;
				listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(), ""+FRQConstantes.PERFIL_TRANSFORMACION_CORDINADOR);
				if (listaPerfiles != null && listaPerfiles.size() > 0) estado = FRQConstantes.PERFIL_TRANSFORMACION_CORDINADOR;
				listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(), ""+FRQConstantes.PERFIL_TRANSFORMACION_FRANQUICIA);
				if (listaPerfiles != null && listaPerfiles.size() > 0) estado = FRQConstantes.PERFIL_TRANSFORMACION_FRANQUICIA;
		}
		
		ModelAndView mv;
		if(estado!=0) {
			mv = new ModelAndView("transformacion/seleccionSucursalTransformacionHallazgos");
			if(busqueda!=0) {
				try {
					listaFiltrosSucursales = asignaTransfBI.obtieneDatos(null);
					listaFiltrosSucursales = listaFiltrosSucursales
						    .stream()
						    .filter(x -> x.getCeco()==idCeco )
						    .collect(Collectors.toList());
					respuesta = asignaTransfBI.obtieneDatosCursoresCeco(idCeco+"");

					JSONObject obj = new JSONObject(respuesta.toString());
					JSONArray proyec= obj.getJSONArray("listaProyecto");
					JSONArray fases = obj.getJSONArray("listaFases");
					JSONArray agrupadores=obj.getJSONArray("listaAgrupador");
					String nombreProyecto;
					int items;
					
					for (int i = 0; i < listaFiltrosSucursales.size(); i++) {
						nombreProyecto="";
						items=0;
						for (int j = 0; j < proyec.length(); j++) {
							if(Integer.parseInt(proyec.getJSONObject(j).getString("idProyecto"))==listaFiltrosSucursales.get(i).getProyecto()) {
								nombreProyecto=proyec.getJSONObject(j).getString("NombreProyecto");
								break;
							}
						}
						if(bodyJson!="") bodyJson+=",";
						bodyJson += "{name:'"+nombreProyecto+"',"
									+ "link:'#',"
									+ "sub:[";
						
						for (int j = 0; j < agrupadores.length(); j++) {
							if(listaFiltrosSucursales.get(i).getAgrupador()==Integer.parseInt(agrupadores.getJSONObject(j).getString("idAgrupador"))) {
								if(listaFiltrosSucursales.get(i).getFaseAct()>Integer.parseInt(agrupadores.getJSONObject(j).getString("idOrdenFase"))) {
									for (int k = 0; k < fases.length(); k++) {
										if(Integer.parseInt(fases.getJSONObject(k).getString("idFase"))==Integer.parseInt(agrupadores.getJSONObject(j).getString("idFase"))) {
											if(items>0)bodyJson += ",";
											items++;
											bodyJson += "{"+
															"name:'"+fases.getJSONObject(k).getString("nombreFase")+"'," + 
															"link:'/checklist/central/getEvaluacionTransformacion.htm?busqueda=1&idCeco="+idCeco+"&idProyecto="+listaFiltrosSucursales.get(i).getProyecto()+"&idFase="+Integer.parseInt(fases.getJSONObject(k).getString("idFase"))+"'," + 
															"sub:null"+
														"}";
										}
									
									}
								}
								else if(listaFiltrosSucursales.get(i).getFaseAct()==Integer.parseInt(agrupadores.getJSONObject(j).getString("idOrdenFase")) &&
										listaFiltrosSucursales.get(i).getIdestatus()==0){
									for (int k = 0; k < fases.length(); k++) {
										if(Integer.parseInt(fases.getJSONObject(k).getString("idFase"))==Integer.parseInt(agrupadores.getJSONObject(j).getString("idFase"))) {
											if(items>0)bodyJson += ",";
											items++;
											bodyJson += "{"+
															"name:'"+fases.getJSONObject(k).getString("nombreFase")+"'," + 
															"link:'/checklist/central/getEvaluacionTransformacion.htm?busqueda=1&idCeco="+idCeco+"&idProyecto="+listaFiltrosSucursales.get(i).getProyecto()+"&idFase="+Integer.parseInt(fases.getJSONObject(k).getString("idFase"))+"'," + 
															"sub:null"+
														"}";
										}
									
									}
								}
							}
						}
						

						bodyJson +=		"]"+
									"}"	;
						

					}

					json += "{menu:["+bodyJson+"]}";

					mv.addObject("json", json);
				
				} catch (Exception e) {
					logger.info("Ocurrio algo " + e);
				}
			}
		}
		else {
			mv = new ModelAndView("mensajes");
		}

		mv.addObject("perfil", estado);
		mv.addObject("sucursal", sucursal);
		
		return mv;
	}

	@RequestMapping(value = { "central/getEvaluacionTransformacion.htm" }, method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView getEvaluacionTransformacion(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") int idCeco,
			@RequestParam(value = "idProyecto", required = true, defaultValue = "") int idProyecto,
			@RequestParam(value = "idFase", required = true, defaultValue = "") int idFase)

			throws ServletException, IOException {
		
		UsuarioDTO userSession = null;
		userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		List<PerfilUsuarioDTO> listaPerfiles = null;
		int estado = 0;
		int perfil = 0;

		if (userSession != null) {
				//listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(),""+FRQConstantes.PERFIL_TRANSFORMACION_SUP_OBRA);
				//if (listaPerfiles != null && listaPerfiles.size() > 0) estado = FRQConstantes.PERFIL_TRANSFORMACION_SUP_OBRA;
				listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(), ""+FRQConstantes.PERFIL_TRANSFORMACION_CORDINADOR);
				if (listaPerfiles != null && listaPerfiles.size() > 0) { perfil=1; estado = FRQConstantes.PERFIL_TRANSFORMACION_CORDINADOR; }
				listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(), ""+FRQConstantes.PERFIL_TRANSFORMACION_FRANQUICIA);
				if (listaPerfiles != null && listaPerfiles.size() > 0) { perfil=2; estado = FRQConstantes.PERFIL_TRANSFORMACION_FRANQUICIA; }
		}
		
		ModelAndView mv;
		if(estado!=0) {
			
			mv = new ModelAndView("transformacion/evaluacionSucursalTransformacion");
			if(idCeco!=0 && idProyecto!=0 && idFase!=0) {
				List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();
				//List<HallazgosTransfDTO> listaEvidencias = new ArrayList<HallazgosTransfDTO>();
		
				userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		
				try {
					//http://localhost:8080/checklist/servicios/getHallazgosByCecoFaseProyecto.json?idUsuario=<?>&idCeco=<?>&idProyecto=<?>&idFase=<?>
					listaHallazgos = hallazgosTransfBI.obtieneDatos(""+idCeco, ""+idProyecto, ""+idFase);
	           	 	//listaEvidencias = hallazgosTransfBI.obtieneDatosBita(idProyecto,idFase,idCeco);
		           	 
					//listaHallazgos = hallazgosExpBI.obtieneDatos(Integer.parseInt(idCeco));
					//listaEvidencias = hallazgosEviExpBI.obtieneDatosBita(Integer.parseInt(idCeco));
		
				} catch (Exception e) {
					logger.info( "AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos y hallazgosEviExpBI.obtieneDatosBita del CECO: "+ idCeco);
				}
		
				listaHallazgos = listaHallazgos.stream().sorted(Comparator.comparingInt(HallazgosTransfDTO::getIdHallazgo)).collect(Collectors.toList());
				
				String tabla = "";
				String bodytable = "";
				String nombreCeco = "";
				int hallazgosTotales = 0;
				if (listaHallazgos != null && listaHallazgos.size() > 0) {
					nombreCeco = listaHallazgos.get(0).getCecoNom();
					//hallazgosTotales = listaHallazgos.size();
				}
		
				int hallazgosPorAtender = 0;
				int hallazgosPorAutorizar = 0;
				int hallazgosAtendidos = 0;
				//int hallazgosRechazadosPorAtender = 0;
		
				if (listaHallazgos != null) {
		
					for (int i = 0; i < listaHallazgos.size(); i++) {
		
						if(listaHallazgos.get(i).getIdFolio()!=0) {
							hallazgosTotales++;
						}
						switch (listaHallazgos.get(i).getStatus()) {
						case 0:
							//hallazgosTotales++;
							break;
						case 2:
							hallazgosPorAtender++;
							break;
						case 3:
							hallazgosPorAutorizar++;
							break;
						case 4:
							hallazgosAtendidos++;
							break;
						default:
							break;
						}
		
						bodytable += "<tr>" +
		
								//"<td align='center'>" + listaHallazgos.get(i).getIdHallazgo() + "</a></td>" +
		
								//"<td align='center'>" + listaHallazgos.get(i).getZonaClasi() + "</a></td>" +
		
								"<td align='center'><b>" + listaHallazgos.get(i).getNombChek() + "</b></td>" +
		
								getLigaDetalle(userSession, listaHallazgos.get(i).getStatus(),
										listaHallazgos.get(i).getIdHallazgo(), ""+idCeco, listaHallazgos.get(i).getPreg(), perfil, idProyecto, idFase,1)
								+
		
								"<td align='center'><b>" + listaHallazgos.get(i).getRespuesta() + "</b></td>" +
		
								"<td align='center' style='color:" + getColorStatus(listaHallazgos.get(i).getStatus(), perfil) 
								+ ";'><b>" + getStatus(listaHallazgos.get(i).getStatus()) + "</b></td>" +
		
								//"<td align='center'><b> <a href='getHistorialHallazgoTransformacion.htm?idCeco=" + idCeco
								//+ "&idHallazgo=" + listaHallazgos.get(i).getIdHallazgo() + "'> Ver Detalle</a></b></td>" +
		
								// "<td align='center'> <a target='_blank'
								// href='generaDashboard.htm?idCeco="+res.get(i).getCeco()+"&tipoRecorrido="+res.get(i).getIdRecorrido()+"&tipoReporte=0'>Descargar</a></td>"
								// +
		
								"</tr>";
		
					}
		
					tabla = "<table id='tabla' class='tablaChecklist'>" +
		
							" <thead>" +
		
							" <tr>";
		
					tabla += " <th style='width: 20%;'><b>Protocolo</b></th>"
							+ " <th><b>Item</b></th>" + " <th><b>Respuesta</b></th>" + " <th style='width: 20%;'><b>Estatus</b></th>"
							+ " </tr>" +
		
							" </thead>" + " <tbody>";
		
					tabla += bodytable;
		
					tabla += "                        </tbody>" +
		
							"</table>";
		
				}
		
				String tablaConteoHallazgos = "";
		
				tablaConteoHallazgos += "<div style='float:right;'>" +
		
						"<table style='margin-top: 4%; width: 100%; float:right;' >" + "<tbody>" + "<tr>"
						+ "<td>Total Hallazgos reportados: " + hallazgosTotales + "</td>" + "</tr>" + "<tr>" + "<td>Autorizados: "
						+ hallazgosAtendidos + "</td>" + "</tr>" + "<tr>" + "<td>Pendiente por Autorizar Franquicia: "
						+ hallazgosPorAutorizar + "</td>" + "</tr>" + "<tr>" + "<td>Pendiente por Autorizar Coordinador: "
						+ hallazgosPorAtender + "</td>" + "</tr>" + "</tbody>" + "</table>" + "</div>";

				mv.addObject("nombreCeco", nombreCeco);
				mv.addObject("listaHallazgos", listaHallazgos);
				//mv.addObject("listaEvidencias", listaEvidencias);
				mv.addObject("tablaConteoHallazgos", tablaConteoHallazgos);
				mv.addObject("tabla", tabla);
				mv.addObject("idCeco", idCeco);
				mv.addObject("idProyecto", idProyecto);
				mv.addObject("idFase", idFase);
			}
	

		}
		else {
			mv = new ModelAndView("mensajes");
		}

		mv.addObject("perfil", estado);

		return mv;

	}

	@RequestMapping(value = { "central/getHallazgoTransformacion.htm" }, method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView getHallazgoTransformacion(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") int idCeco,
			@RequestParam(value = "idProyecto", required = true, defaultValue = "") int idProyecto,
			@RequestParam(value = "idFase", required = true, defaultValue = "") int idFase)

			throws ServletException, IOException {
		
		UsuarioDTO userSession = null;
		userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		List<PerfilUsuarioDTO> listaPerfiles = null;
		int estado = 0;
		int perfil = 0;

		if (userSession != null) {
				//listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(),""+FRQConstantes.PERFIL_TRANSFORMACION_SUP_OBRA);
				//if (listaPerfiles != null && listaPerfiles.size() > 0) { perfil=0; estado = FRQConstantes.PERFIL_TRANSFORMACION_SUP_OBRA;}
				listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(), ""+FRQConstantes.PERFIL_TRANSFORMACION_CORDINADOR);
				if (listaPerfiles != null && listaPerfiles.size() > 0) { perfil=1; estado = FRQConstantes.PERFIL_TRANSFORMACION_CORDINADOR; }
				listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(), ""+FRQConstantes.PERFIL_TRANSFORMACION_FRANQUICIA);
				if (listaPerfiles != null && listaPerfiles.size() > 0) { perfil=2; estado = FRQConstantes.PERFIL_TRANSFORMACION_FRANQUICIA; }
		}
		
		ModelAndView mv;
		if(estado!=0) {

			mv = new ModelAndView("transformacion/hallazgosSucursalTransformacion");
			
			if(idCeco!=0 && idProyecto!=0 && idFase!=0) {
				List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();
				//List<HallazgosTransfDTO> listaEvidencias = new ArrayList<HallazgosTransfDTO>();
		
				userSession = (UsuarioDTO) request.getSession().getAttribute("user");
	
				String nombreCeco = "";
				
				try {
					//http://localhost:8080/checklist/servicios/getHallazgosByCecoFaseProyecto.json?idUsuario=<?>&idCeco=<?>&idProyecto=<?>&idFase=<?>
					listaHallazgos = hallazgosTransfBI.obtieneDatos(""+idCeco, ""+idProyecto, ""+idFase);
	
					if (listaHallazgos != null && listaHallazgos.size() > 0) {
						nombreCeco = listaHallazgos.get(0).getCecoNom();
					}
					listaHallazgos = listaHallazgos
						    .stream()
						    .filter(x -> (x.getIdFolio()!=0))
						    .sorted(Comparator.comparingInt(HallazgosTransfDTO::getIdHallazgo))
						    .collect(Collectors.toList());
					
	           	 	//listaEvidencias = hallazgosTransfBI.obtieneDatosBita(idProyecto,idFase,idCeco);
		           	 
					//listaHallazgos = hallazgosExpBI.obtieneDatos(Integer.parseInt(idCeco));
					//listaEvidencias = hallazgosEviExpBI.obtieneDatosBita(Integer.parseInt(idCeco));
		
				} catch (Exception e) {
					logger.info( "AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos y hallazgosEviExpBI.obtieneDatosBita del CECO: "+ idCeco);
				}
		
		
				String tabla = "";
				String bodytable = "";
				int hallazgosTotales = 0;
				if (listaHallazgos != null && listaHallazgos.size() > 0) {
					hallazgosTotales = listaHallazgos.size();
				}
		
				//int hallazgosPorAtender = 0;
				//int hallazgosPorAutorizar = 0;
				//int hallazgosAtendidos = 0;
				//int hallazgosRechazadosPorAtender = 0;
		
				if (listaHallazgos != null) {
		
					for (int i = 0; i < listaHallazgos.size(); i++) {
		
						/*switch (listaHallazgos.get(i).getStatus()) {
						case 0:
							hallazgosRechazadosPorAtender++;
							break;
						case 1:
							hallazgosPorAtender++;
							break;
						case 2:
							hallazgosPorAutorizar++;
							break;
						case 3:
							hallazgosAtendidos++;
							break;
						default:
							break;
						}*/
		
						bodytable += "<tr>" +
		
								"<td align='center'>" + listaHallazgos.get(i).getIdHallazgo() + "</a></td>" +
		
								"<td align='center'>" + listaHallazgos.get(i).getZonaClasi() + "</a></td>" +
		
								"<td align='center'><b>" + listaHallazgos.get(i).getNombChek() + "</b></td>" +
		
								getLigaDetalle(userSession, listaHallazgos.get(i).getStatus(),
										listaHallazgos.get(i).getIdHallazgo(), ""+idCeco, listaHallazgos.get(i).getPreg(), perfil, idProyecto,idFase,2)
								+
		
								"<td align='center'><b>" + listaHallazgos.get(i).getRespuesta() + "</b></td>" +
		
								"<td align='center' style='color:" + getColorStatus(listaHallazgos.get(i).getStatus(), perfil)
								+ ";'><b>" + getStatus(listaHallazgos.get(i).getStatus()) + "</b></td>" +
		
								//"<td align='center'><b> <a href='getHistorialHallazgoTransformacion.htm?idCeco=" + idCeco
								//+ "&idHallazgo=" + listaHallazgos.get(i).getIdHallazgo() + "'> Ver Detalle</a></b></td>" +
		
								// "<td align='center'> <a target='_blank'
								// href='generaDashboard.htm?idCeco="+res.get(i).getCeco()+"&tipoRecorrido="+res.get(i).getIdRecorrido()+"&tipoReporte=0'>Descargar</a></td>"
								// +
		
								"</tr>";
		
					}
		
					tabla = "<table id='tabla' class='tablaChecklist'>" +
		
							" <thead>" +
		
							" <tr>";
		
					tabla += " <th><b>Id</b></th>" + " <th><b>Zona</b></th>" + " <th style='width: 20%;'><b>Checkslit</b></th>"
							+ " <th><b>Item</b></th>" + " <th><b>Respuesta</b></th>" + " <th style='width: 20%;'><b>Estatus</b></th>"
							//+ " <th><b>Historial</b></th>" 
							+ " </tr>" +
		
							" </thead>" + " <tbody>";
		
					tabla += bodytable;
		
					tabla += "                        </tbody>" +
		
							"</table>";
		
				}
		
				String tablaConteoHallazgos = "";
		
				tablaConteoHallazgos += "<div style='float:right;'>" +
		
						"<table style='margin-top: 4%; width: 100%; float:right;' >" + "<tbody>" + "<tr>"
						+ "<td>Total Hallazgos reportados: " + hallazgosTotales + "</td>"+ "</tbody>" + "</table>" + "</div><br><br><br><br><br>";
						//+ "</tr>" + "<tr>" + "<td>Atendidos: "
						//+ hallazgosAtendidos + "</td>" + "</tr>" + "<tr>" + "<td>Pendientes Por Autorizar: "
						//+ hallazgosPorAutorizar + "</td>" + "</tr>" + "<tr>" + "<td>Rechazados Por Atender: "
						//+ hallazgosRechazadosPorAtender + " </td>" + "</tr>" + "<tr>" + "<td>Por Atender: "
						//+ hallazgosPorAtender + "</td>" + "</tr>" + "</tbody>" + "</table>" + "</div>";
		
				mv.addObject("tabla", tabla);
				mv.addObject("idCeco", idCeco);
				mv.addObject("idProyecto", idProyecto);
				mv.addObject("idFase", idFase);
				mv.addObject("nombreCeco", nombreCeco);
				mv.addObject("listaHallazgos", listaHallazgos);
				//mv.addObject("listaEvidencias", listaEvidencias);
				mv.addObject("tablaConteoHallazgos", tablaConteoHallazgos);
			}
		}
		else {
			mv = new ModelAndView("mensajes");
		}

		mv.addObject("perfil", estado);

		return mv;

	}

	
	@RequestMapping(value = { "central/visualizarEvaluacionHallazgo.htm" }, method = RequestMethod.GET)
	public ModelAndView visualizarEvaluacionHallazgo(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "where", required = true, defaultValue = "1") int where,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
			@RequestParam(value = "idHallazgo", required = true, defaultValue = "") String idHallazgo,
			@RequestParam(value = "idProyecto", required = true, defaultValue = "") int idProyecto,
			@RequestParam(value = "idFase", required = true, defaultValue = "") int idFase)
			throws ServletException, IOException {
		UsuarioDTO userSession = null;
		userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		List<PerfilUsuarioDTO> listaPerfiles = null;
		int estado = 0;
		int perfil = 0;

		if (userSession != null) {
				//listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(),""+FRQConstantes.PERFIL_TRANSFORMACION_SUP_OBRA);
				//if (listaPerfiles != null && listaPerfiles.size() > 0) estado = FRQConstantes.PERFIL_TRANSFORMACION_SUP_OBRA;
				listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(), ""+FRQConstantes.PERFIL_TRANSFORMACION_CORDINADOR);
				if (listaPerfiles != null && listaPerfiles.size() > 0) { perfil=1; estado = FRQConstantes.PERFIL_TRANSFORMACION_CORDINADOR; }
				listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(), ""+FRQConstantes.PERFIL_TRANSFORMACION_FRANQUICIA);
				if (listaPerfiles != null && listaPerfiles.size() > 0) { perfil=2; estado = FRQConstantes.PERFIL_TRANSFORMACION_FRANQUICIA; }
		}
		
		ModelAndView mv;
		if(estado!=0) {
		
			List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();
			List<HallazgosTransfDTO> listaEvidencias = new ArrayList<HallazgosTransfDTO>();
	
			String pregunta = "";
			String imagen = "";
			String comentarios = "";
			String obsAtencion = "";
			String motivoRechazo = "";
			
			int tipoArchivo = 21; //21=imagenes 22=video
	
			int idResp = 0;
			int status = 0;
			String respuesta = "";
			String ruta = "";
			String fechaAutorizacion = "";
			String fechaFin = "";
		 	HallazgosTransfDTO objPreg = null;

		 	String carrusel = "";
		 	String carrusel2 = "";
		 	String carrusel3 = "";
	
			try {
	
				//listaHallazgos = hallazgosTransfBI.obtieneDatos(Integer.parseInt(idCeco));
				//listaEvidencias = hallazgosTransfBI.obtieneDatosBita(Integer.parseInt(idCeco)); // para obtener tipoEvidencia si la tabla tiene tipo evidencia no sera necesario
	
				 listaHallazgos = hallazgosTransfBI.obtieneDatos(idCeco, ""+idProyecto, ""+idFase);
		       	 listaEvidencias = hallazgosTransfBI.obtieneDatosBita(idProyecto,idFase,Integer.parseInt(idCeco));
	           			
		       	 
		       	 // Se obtiene el hallazgo
		       	objPreg = listaHallazgos.stream()
		       		  .filter(hallazgo -> (hallazgo.getIdHallazgo() == Integer.parseInt(idHallazgo)))
		       		  .findAny()
		       		  .orElse(null);
		       	 // se setean los datos
		       	pregunta = objPreg.getPreg();
				comentarios = objPreg.getObs();
				obsAtencion = objPreg.getObsAtencion();
				motivoRechazo = objPreg.getMotivrechazo();
				idResp = objPreg.getIdResp();
				status = objPreg.getStatus();
				respuesta = objPreg.getRespuesta();
				ruta = objPreg.getRuta();
				imagen = ruta;
				if(imagen!=null) {
					Path imgPath = Paths.get(imagen);
					
					String encodedA1 = "";
					try {
						byte[] evidencia = Files.readAllBytes(imgPath);
						encodedA1 = new String(Base64.encodeBase64(evidencia), "UTF-8");
					} catch (NoSuchFileException e) {
						logger.info("No se encontro Evidencia en el servidor" + imagen);
						encodedA1 = "";
					}
	
					imagen = encodedA1;
				}

				SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
				fechaAutorizacion = f.format(new Date());
				fechaFin = objPreg.getFechaFin();
				
				// se obtiene el tipo de evidencia
				listaEvidencias = listaEvidencias.stream()
			       		  .filter(evi -> (evi.getIdHallazgo() == Integer.parseInt(idHallazgo)))
			       		  .collect(Collectors.toList());
				
				HallazgosTransfDTO objEvidencia  = listaEvidencias.stream()
			       		  .filter(hallazgo -> (hallazgo.getIdHallazgo() == Integer.parseInt(idHallazgo)))
			       		  .findAny()
			       		  .orElse(null);
				if(objEvidencia!=null) {
					tipoArchivo = objEvidencia.getTipoEvi();
				}
				
	
			} catch (Exception e) {
				logger.info(
						"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos y hallazgosEviExpBI.obtieneDatosBita del CECO: "
								+ idCeco);
			}
	
			mv = new ModelAndView("transformacion/visualizarEvaluacionHallazgo");
			
			//carrusel 1
			carrusel = "<div class='cuadrobigUnic'>" + "<div id='txtMdl1' class='txtMdl spBold'>"
					+ "Item <span class='imgNum'>1</span> /2" + "</div>" + "<div class='pad-slider'>"
					+ "<div id='owl-carousel1' class='owl-carousel owl-theme'>";
	
			for (int i = 0; i < 1; i++) {
				carrusel += "<div class='item'>";
				if (imagen != null && imagen.length() > 0) {
					if(tipoArchivo==21) {
						carrusel += "<img src='data:image/png;base64," + imagen + "'>";
					}
					else {
						carrusel+="<video controls style='max-height: 400px; width: 100%'><source type='video/mp4' src='data:video/mp4;base64,"+imagen+"'></video>";
					}
				} 
				else {
					if(tipoArchivo==21) {
						carrusel += "<img src='../images/expancion/fotoBig.svg'>";
					}
					else {
						carrusel+="<video controls style='max-height: 400px; width: 100%'><source type='video/mp4' src=''></video>";
					}
				}
				carrusel += "</div>";
			}
			carrusel += "</div> </div> </div>";
			//Carrusel 2
			carrusel2 = "<div class='cuadrobigUnic'>" + "<div id='txtMdl2' class='txtMdl spBold'>"
					+ "Item <span class='imgNum'>1</span> /2" + "</div>" + "<div class='pad-slider'>"
					+ "<div id='owl-carousel2' class='owl-carousel owl-theme'>";
	
			for (int i = 0; i < listaEvidencias.size(); i++) {
				String encoded="";
				if(listaEvidencias.get(i).getRuta()!=null) {
					Path imgPath2 = Paths.get(listaEvidencias.get(i).getRuta());
					try {
						byte[] evidencia = Files.readAllBytes(imgPath2);
						encoded = new String(Base64.encodeBase64(evidencia), "UTF-8");
					} catch (NoSuchFileException e) {
						logger.info("No se encontro Evidencia en el servidor" + listaEvidencias.get(i).getRuta());
						encoded = "";
					}
				}
	
				carrusel2 += "<div class='item'>";
				if (encoded != null && encoded.length() > 0) {
					if(tipoArchivo==21) {
						carrusel2 += "<img src='data:image/png;base64," + encoded + "'>";
					}
					else {
						carrusel2+="<video controls style='max-height: 400px; width: 100%'><source type='video/mp4' src='data:video/mp4;base64,"+encoded+"'></video>";
					}
	
				} 
				else {
					if(tipoArchivo==21) {
						carrusel2 += "<img src='../images/expancion/fotoBig.svg'>";
					}
					else {
						carrusel2+="<video controls style='max-height: 400px; width: 100%'><source type='video/mp4' src=''></video>";
					}
				}
				carrusel2 += "</div>";
			}
			carrusel2 += "</div> </div> </div>";
			
			
			if(objPreg!=null && objPreg.getIdFolio()==0) {
				carrusel3="";
				carrusel="";
			}
			else if(objPreg!=null && objPreg.getIdFolio()!=0 && objPreg.getStatus()>0) {
				carrusel3=carrusel;
				carrusel="";
			}
			else {
				carrusel3="";
				carrusel="";
			}
			String titulo = "";
			if(where == 1) {
				titulo="EVALUACIÓN DE PROTOCOLO";
			}
			if(where == 2) {
				titulo="ATENCIÓN DE HALLAZGO";
			}
			
	
			mv.addObject("urlServer", FRQConstantes.getURLServer());
			mv.addObject("carrusel3", carrusel3);
			mv.addObject("carrusel2", carrusel2);
			mv.addObject("carrusel1", carrusel);
			mv.addObject("titulo", titulo);
			mv.addObject("obsAtencion", obsAtencion);
			mv.addObject("visibleBoton", "hidden");
			mv.addObject("motivoRechazo", motivoRechazo);
			mv.addObject("imagen", imagen);
			mv.addObject("pregunta", pregunta);
			mv.addObject("comentarios", comentarios);
	
			mv.addObject("ceco", idCeco);
			mv.addObject("idResp", idResp);
			mv.addObject("status", status);
			mv.addObject("respuesta", respuesta);
			mv.addObject("ruta", ruta);
			mv.addObject("fechaAutorizacion", fechaAutorizacion);
			mv.addObject("fechaFin", fechaFin);
		}
		else {
			mv = new ModelAndView("mensajes");
		}

		mv.addObject("perfil", perfil);
		return mv;
	}

	@RequestMapping(value = { "central/autorizaRechazaEvaluacion.htm" }, method = RequestMethod.GET)
	public ModelAndView autorizaRechazaEvaluacion(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "where", required = true, defaultValue = "1") int where,
			@RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
			@RequestParam(value = "idHallazgo", required = true, defaultValue = "") String idHallazgo,
			@RequestParam(value = "idProyecto", required = true, defaultValue = "") int idProyecto,
			@RequestParam(value = "idFase", required = true, defaultValue = "") int idFase)
			throws ServletException, IOException {
		UsuarioDTO userSession = null;
		userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		List<PerfilUsuarioDTO> listaPerfiles = null;
		int estado = 0;
		int perfil = 0;

		if (userSession != null) {
				//listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(),""+FRQConstantes.PERFIL_TRANSFORMACION_SUP_OBRA);
				//if (listaPerfiles != null && listaPerfiles.size() > 0) estado = FRQConstantes.PERFIL_TRANSFORMACION_SUP_OBRA;
				listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(), ""+FRQConstantes.PERFIL_TRANSFORMACION_CORDINADOR);
				if (listaPerfiles != null && listaPerfiles.size() > 0) { perfil=1; estado = FRQConstantes.PERFIL_TRANSFORMACION_CORDINADOR; }
				listaPerfiles = perfilUsuarioBI.obtienePerfiles(userSession.getIdUsuario(), ""+FRQConstantes.PERFIL_TRANSFORMACION_FRANQUICIA);
				if (listaPerfiles != null && listaPerfiles.size() > 0) { perfil=2; estado = FRQConstantes.PERFIL_TRANSFORMACION_FRANQUICIA; }
		}
		
		ModelAndView mv;
		if(estado!=0) {
		
			List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();
			List<HallazgosTransfDTO> listaEvidencias = new ArrayList<HallazgosTransfDTO>();
			
			String visible = "hidden";
			String pregunta = "";
			String imagen = "";
			String comentarios = "";
			String obsAtencion = "";
			String motivoRechazo = "";
			
			int tipoArchivo = 21; //21=imagenes 22=video
	
			int idResp = 0;
			int status = 0;
			String respuesta = "";
			String ruta = "";
			String obtieneRutaEvi = "";
			String fechaAutorizacion = "";
			String fechaFin = "";
		 	HallazgosTransfDTO objPreg = null;

		 	String carrusel = "";
		 	String carrusel2 = "";
		 	String carrusel3 = "";
	
			try {
	
				//listaHallazgos = hallazgosTransfBI.obtieneDatos(Integer.parseInt(idCeco));
				//listaEvidencias = hallazgosTransfBI.obtieneDatosBita(Integer.parseInt(idCeco)); // para obtener tipoEvidencia si la tabla tiene tipo evidencia no sera necesario
	
				 listaHallazgos = hallazgosTransfBI.obtieneDatos(idCeco, ""+idProyecto, ""+idFase);
		       	 listaEvidencias = hallazgosTransfBI.obtieneDatosBita(idProyecto,idFase,Integer.parseInt(idCeco));
	           			
		       	 
		       	 // Se obtiene el hallazgo
		       	objPreg = listaHallazgos.stream()
		       		  .filter(hallazgo -> (hallazgo.getIdHallazgo() == Integer.parseInt(idHallazgo)))
		       		  .findAny()
		       		  .orElse(null);
		       	 // se setean los datos
		       	pregunta = objPreg.getPreg();
				comentarios = objPreg.getObs();
				obsAtencion = objPreg.getObsAtencion();
				motivoRechazo = objPreg.getMotivrechazo();
				idResp = objPreg.getIdResp();
				status = objPreg.getStatus();
				respuesta = objPreg.getRespuesta();
				ruta = objPreg.getRuta();
				imagen = ruta;
				if(imagen!=null) {
					Path imgPath = Paths.get(imagen);
					
					String encodedA1 = "";
					try {
						byte[] evidencia = Files.readAllBytes(imgPath);
						encodedA1 = new String(Base64.encodeBase64(evidencia), "UTF-8");
					} catch (NoSuchFileException e) {
						logger.info("No se encontro Evidencia en el servidor" + imagen);
						encodedA1 = "";
					}
	
					imagen = encodedA1;
				}

				SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
				fechaAutorizacion = f.format(new Date());
				fechaFin = objPreg.getFechaFin();
				
				// se obtiene el tipo de evidencia
				listaEvidencias = listaEvidencias.stream()
			       		  .filter(evi -> (evi.getIdHallazgo() == Integer.parseInt(idHallazgo)))
			       		  .collect(Collectors.toList());
				
				HallazgosTransfDTO objEvidencia  = listaEvidencias.stream()
			       		  .filter(hallazgo -> (hallazgo.getIdHallazgo() == Integer.parseInt(idHallazgo)))
			       		  .findAny()
			       		  .orElse(null);
				if(objEvidencia!=null) {
					tipoArchivo = objEvidencia.getTipoEvi();
				}
				
	
			} catch (Exception e) {
				logger.info(
						"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos y hallazgosEviExpBI.obtieneDatosBita del CECO: "
								+ idCeco);
			}
	
			mv = new ModelAndView("transformacion/autorizaRechazaEvaluacion");
			
			//carrusel 1
			carrusel = "<div class='cuadrobigUnic'>" + "<div id='txtMdl1' class='txtMdl spBold'>"
					+ "Item <span class='imgNum'>1</span> /2" + "</div>" + "<div class='pad-slider'>"
					+ "<div id='owl-carousel1' class='owl-carousel owl-theme'>";
	
			for (int i = 0; i < 1; i++) {
				carrusel += "<div class='item'>";
				if (imagen != null && imagen.length() > 0) {
					if(tipoArchivo==21) {
						carrusel += "<img src='data:image/png;base64," + imagen + "'>";
					}
					else {
						carrusel+="<video controls style='max-height: 400px; width: 100%'><source type='video/mp4' src='data:video/mp4;base64,"+imagen+"'></video>";
					}
				} 
				else {
					if(tipoArchivo==21) {
						carrusel += "<img src='../images/expancion/fotoBig.svg'>";
					}
					else {
						carrusel+="<video controls style='max-height: 400px; width: 100%'><source type='video/mp4' src=''></video>";
					}
				}
				carrusel += "</div>";
			}
			carrusel += "</div> </div> </div>";
			//Carrusel 2
			carrusel2 = "<div class='cuadrobigUnic'>" + "<div id='txtMdl2' class='txtMdl spBold'>"
					+ "Item <span class='imgNum'>1</span> /2" + "</div>" + "<div class='pad-slider'>"
					+ "<div id='owl-carousel2' class='owl-carousel owl-theme'>";
	
			for (int i = 0; i < listaEvidencias.size(); i++) {
				String encoded="";
				if(listaEvidencias.get(i).getRuta()!=null) {
					if(obtieneRutaEvi==null ||obtieneRutaEvi=="") obtieneRutaEvi= listaEvidencias.get(i).getRuta();
					Path imgPath2 = Paths.get(listaEvidencias.get(i).getRuta());
					try {
						byte[] evidencia = Files.readAllBytes(imgPath2);
						encoded = new String(Base64.encodeBase64(evidencia), "UTF-8");
					} catch (NoSuchFileException e) {
						logger.info("No se encontro Evidencia en el servidor" + listaEvidencias.get(i).getRuta());
						encoded = "";
					}
				}
	
				carrusel2 += "<div class='item'>";
				if (encoded != null && encoded.length() > 0) {
					if(tipoArchivo==21) {
						carrusel2 += "<img src='data:image/png;base64," + encoded + "'>";
					}
					else {
						carrusel2+="<video controls style='max-height: 400px; width: 100%'><source type='video/mp4' src='data:video/mp4;base64,"+encoded+"'></video>";
					}
	
				} 
				else {
					if(tipoArchivo==21) {
						carrusel2 += "<img src='../images/expancion/fotoBig.svg'>";
					}
					else {
						carrusel2+="<video controls style='max-height: 400px; width: 100%'><source type='video/mp4' src=''></video>";
					}
				}
				carrusel2 += "</div>";
			}
			carrusel2 += "</div> </div> </div>";
			
			
			if(objPreg!=null && objPreg.getIdFolio()==0) {
				carrusel3="";
				carrusel="";
			}
			else if(objPreg!=null && objPreg.getIdFolio()!=0 && objPreg.getStatus()>0) {
				carrusel3=carrusel;
				carrusel="";
			}
			else {
				carrusel3="";
				carrusel="";
			}
	
			if(perfil==1 && objPreg.getStatus()==2) {
				visible="visible";
			}
			if(perfil==2 && objPreg.getStatus()==3) {
				visible="visible";
			}
			String titulo = "";
			if(where == 1) {
				titulo="EVALUACIÓN DE PROTOCOLO";
			}
			if(where == 2) {
				titulo="ATENCIÓN DE HALLAZGO";
			}
			if(ruta!=null && ruta!="") {
				obtieneRutaEvi=ruta;
			}
			
			mv.addObject("urlServer", FRQConstantes.getURLServer());
			mv.addObject("carrusel3", carrusel3);
			mv.addObject("carrusel2", carrusel2);
			mv.addObject("carrusel1", carrusel);
			mv.addObject("titulo", titulo);
			mv.addObject("visibleBoton", visible);
			mv.addObject("obsAtencion", obsAtencion);
			mv.addObject("motivoRechazo", motivoRechazo);
			mv.addObject("imagen", imagen);
			mv.addObject("pregunta", pregunta);
			mv.addObject("comentarios", comentarios);

			mv.addObject("idHallazgo", idHallazgo);
			mv.addObject("idFolio", objPreg.getIdFolio());
			mv.addObject("ceco", idCeco);
			mv.addObject("idProyecto", idProyecto);
			mv.addObject("idFase", idFase);
			mv.addObject("tipoArchivo", tipoArchivo);
			mv.addObject("idResp", idResp);
			mv.addObject("status", status);
			mv.addObject("respuesta", respuesta);
			mv.addObject("ruta", ruta);
			mv.addObject("obtieneRutaEvi", obtieneRutaEvi);
			mv.addObject("fechaAutorizacion", fechaAutorizacion);
			mv.addObject("fechaFin", fechaFin);
		}
		else {
			mv = new ModelAndView("mensajes");
		}

		mv.addObject("perfil", perfil);
		return mv;
	}
	

	// ========================================================================================================================================
	/* DIRECTOS */
	@RequestMapping(value = { "central/actualizaHallazgoDirectoTransformacion.htm" }, method = RequestMethod.GET)
	public String actualizaHallazgoDirectoTransformacion(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "ceco", required = true, defaultValue = "") int ceco,
			@RequestParam(value = "idResp", required = true, defaultValue = "") String idResp,
			@RequestParam(value = "status", required = true, defaultValue = "") String status,
			@RequestParam(value = "respuesta", required = true, defaultValue = "") String respuesta,
			@RequestParam(value = "ruta", required = true, defaultValue = "") String ruta,
			@RequestParam(value = "fechaAutorizacion", required = true, defaultValue = "") String fechaAutorizacion,
			@RequestParam(value = "fechaFin", required = true, defaultValue = "") String fechaFin,
			@RequestParam(value = "idHallazgo", required = true, defaultValue = "") String idHallazgo,
			@RequestParam(value = "idFolio", required = true, defaultValue = "") int idFolio,
			@RequestParam(value = "tipoEvidencia", required = true, defaultValue = "") int tipoEvidencia,
			@RequestParam(value = "idFase", required = true, defaultValue = "") int idFase,
			@RequestParam(value = "idProyecto", required = true, defaultValue = "") int idProyecto,
			@RequestParam(value = "comentario", required = true, defaultValue = "") String comentario,
			@RequestParam(value = "comentarioInicio", required = true, defaultValue = "") String comentarioInicio,
			@RequestParam(value = "motivoRechazo", required = true, defaultValue = "") String motivoRechazo)
			throws ServletException, IOException {

		int idRespLocal = 0;
		int statusLocal = 0;
		String respuestaLocal = "";
		String rutaLocal = "";
		String fechaAutorizacionLocal = "";
		String fechaFinLocal = "";
		String comentarioLocal = "";
		String motivoRechazoLocal = "";
		String usuModif = "";
		UsuarioDTO userSession = null;

		userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if (userSession != null)
			usuModif = userSession.getIdUsuario() + "-"
					+ ((userSession.getNombre() != null && !userSession.getNombre().equalsIgnoreCase("null"))
							? userSession.getNombre()
							: "DESDEWEB");
		else
			usuModif = "DESDEWEB";
		HallazgosTransfDTO bean = new HallazgosTransfDTO();

		idRespLocal = Integer.parseInt(idResp);
		statusLocal = Integer.parseInt(status);
		respuestaLocal = respuesta;
		rutaLocal = ruta;
		fechaAutorizacionLocal = fechaAutorizacion;
		fechaFinLocal = fechaFin;
		comentarioLocal = comentario;
		motivoRechazoLocal = motivoRechazo;

		bean.setIdResp(idRespLocal);
		bean.setStatus(statusLocal);
		bean.setRespuesta(respuestaLocal);
		bean.setRuta(rutaLocal);
		bean.setFechaAutorizo(fechaAutorizacionLocal.replaceAll("-", ""));
		bean.setFechaFin(fechaFinLocal.replaceAll("-", ""));

		// SE ACTUALIZA CON ID RESPUESTA, NUEVO ES CON ID HALLAZGO
		bean.setIdHallazgo(Integer.parseInt(idHallazgo));
		bean.setIdFolio(idFolio);
		bean.setIdResp(Integer.parseInt(idResp));
		// SE ACTUALIZA MOTIVO RECGAZO Y USUARIO MODIF
		bean.setMotivrechazo(motivoRechazo);
		bean.setTipoEvi(tipoEvidencia);
		bean.setUsuModif(usuModif);

		bean.setObs(comentarioInicio);
		bean.setObsNueva(comentarioLocal);

		bean.setMotivrechazo(motivoRechazoLocal);

		//boolean actualizaResponse = hallazgosExpBI.actualizaResp(bean);
		Map<String, Object> resp = hallazgosTransfBI.actualizaMov(bean);
		
		int banderaRespuesta = (int) resp.get("response");
		String banderaCierreHallazgos = (String) resp.get("validaCompleto");

		if (banderaRespuesta == 0) {
			logger.info("No fue posible ACTUALIZAR el HALLAZGO " + idHallazgo);
		} else {
			logger.info("HALLAZGO: " + idHallazgo + " ACTUALIZADO ");
		}

		if (banderaRespuesta == 1 && banderaCierreHallazgos.compareToIgnoreCase("true") == 0) {
			sendCorreoTransformacion( ceco,  idProyecto,  idFase);
			//Se hace el envío del correo
		}

		//http://10.51.218.140:8080/checklist/central/getEvaluacionTransformacion.htm?busqueda=1&idCeco=480100&idProyecto=1&idFase=2
		return "redirect:/central/getEvaluacionTransformacion.htm?busqueda=1&idCeco=" + ceco+"&idProyecto="+idProyecto+"&idFase="+idFase;

		// return mv;

	}
	
	//Busqueda de sucursales. Eliminar la busqueda de sucursales de la anterior
	@RequestMapping(value = "/ajaxAutocompletaSucursalTransformacion", method = RequestMethod.GET)
	public @ResponseBody String[] ajaxAutocompletaSucursal(
			@RequestParam(value = "term", required = true, defaultValue = "") String term,
			HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxAutocompletaSucursalTransformacion...");
		
		logger.info("term... "+term);
		
		
		List<AsignaTransfDTO> listaFiltrosSucursales = new ArrayList<AsignaTransfDTO>();
		listaFiltrosSucursales = asignaTransfBI.obtieneDatos(null);
		
		
		List<AsignaTransfDTO> filtros;

		filtros = listaFiltrosSucursales
			    .stream()
			    .filter(x -> ((x.getCeco()+"").contains(term) || (x.getNomCeco().toUpperCase()).contains(term.toUpperCase())))
			    .collect(Collectors.toList());
	
		
		logger.info("listRetorno.size() "+filtros.size());
		logger.info("listRetorno "+filtros);
		
		//Vaciarlo en otra clase que tenga la siguiente forma
//		clase con un atributo que se llame label que sea de tipo String[] y dentro de ella debe
		//guardar todo lo que retorne el 
	
		
		List<String> suc = new ArrayList<>();
		for(int i =0; i<filtros.size(); i++){
			if(!suc.contains(filtros.get(i).getCeco() +" - "+filtros.get(i).getNomCeco())){
				suc.add(filtros.get(i).getCeco() +" - "+filtros.get(i).getNomCeco());
				logger.info("Ceco Sucursal:"+ filtros.get(i).getCeco());
				logger.info("Nombre Sucursal:"+filtros.get(i).getNomCeco());
			}
		}
		
		String[] sucursales = new String[suc.size()];
		suc.toArray(sucursales); // fill the array

		logger.info("...SALE DEL AJAX ajaxAutocompletaSucursalTransformacion...");
		return sucursales;
	}

	// ================================================================================================

	public boolean sendCorreoTransformacion(int idCeco, int idProyecto, int idFase) {
		boolean retorno = false;
		boolean banderaCompleto = false;
		List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();       
		
		//INCREMENTA NÚMERO RECORRIDOMAP
		//SE ACTUALIZAN BANDERA DE FASE Y ESTATUS
		AsignaTransfDTO asig = new AsignaTransfDTO();
		asig.setCeco(idCeco);
		asig.setProyecto(idProyecto);
		//Fase (Se actualiza solo en ulimo hallazgo con un 1)
		asig.setIdOrden("1");
		//Estatus
		asig.setIdestatus(1);
		 
		//Esta es la bandera que lleva el peso para la respuesta (Si es true,no importa lo adentro, retorno un true)
		boolean respuestaAct = asignaTransfBI.actualiza(asig);    	    	
		
		if (respuestaAct) {
			logger.info("HALLAZGOS ESTATUS ACTUALIZADOS CON EXITO  _");
			banderaCompleto = true;
			
			//LLAMAR AL BI PARA CARGAR LOS HALLAZGOS QUE SE GENERAN DEL RECORRIDO
			 listaHallazgos = hallazgosTransfBI.obtieneDatosRepo(idCeco+"", idProyecto+"", idFase+"");
           	 //listaEvidencias = hallazgosTransfBI.obtieneDatosBita(idProyecto,idFase,idCeco);
           	 
           	 if (idCeco != 0 && idFase != 0 && idProyecto != 0) {
           		 
					correoBI.sendMailTransformacion(listaHallazgos,""+idFase,"Acta Transformacion", ""+idCeco, ""+0, "", "", "Acta transformacion");

					banderaCompleto = true;

           	 } else {
    			logger.info("AP ALAGUN VALOR == 0");
    			
    			banderaCompleto = true;
           		 
           	 }
			

		} else {
			logger.info("AP AL ENVIAR CORREO HALLAZGOS TRANSFORMACIÓN ESTATUS");
			banderaCompleto = false;

		}
		retorno = banderaCompleto;
		
		return retorno;
	}
	
	public List<HallazgosExpDTO> getHallazgosConcatenadas(List<HallazgosExpDTO> lista) {

		List<HallazgosExpDTO> chklistPreguntas = new ArrayList<HallazgosExpDTO>();
		List<HallazgosExpDTO> listaResult = new ArrayList<HallazgosExpDTO>();

		listaResult.clear();

		chklistPreguntas = lista;

		for (int i = 0; i < chklistPreguntas.size(); i++) {

			if (chklistPreguntas.get(i).getPregPadre() == 0) {

				for (int j = 0; j < chklistPreguntas.size(); j++) {

					if (chklistPreguntas.get(i).getIdPreg() == chklistPreguntas.get(j).getPregPadre()) {

						HallazgosExpDTO data = chklistPreguntas.get(j);
						data.setPreg(chklistPreguntas.get(i).getPreg() + " : " + chklistPreguntas.get(j).getPreg());
						listaResult.add(data);
					}
				}
			}
		}

		return listaResult;

	}

	public String getLigaDetalle(UsuarioDTO userSession, int status, int idHallazgo, String ceco, String pregunta, int perfil, int idProyecto,int idFase,int where) {

		switch(perfil) {
		default:
		case 0:
			return "<td align='center'><b> <a href='#'>" + pregunta + "</a></b></td>";
		case 1:
			switch (status) {
			case 2:
				return "<td align='center'><b> <a href='autorizaRechazaEvaluacion.htm?idCeco=" + ceco
						+ "&idHallazgo=" + idHallazgo + "&idProyecto="+idProyecto+"&idFase="+idFase+"&where="+where+"'>" + pregunta + "</a></b></td>";
				//return "<td align='center'><b> <a href='vistaDetalleHallazgoTransformacion.htm?idCeco=" + ceco
				//		+ "&idHallazgo=" + idHallazgo + "'>" + pregunta + "</a></b></td>";
			case 0:
			case 1:
			case 3:
			case 4:
				return "<td align='center'><b> <a href='visualizarEvaluacionHallazgo.htm?idCeco=" + ceco 
						+ "&idHallazgo=" + idHallazgo + "&idProyecto="+idProyecto+"&idFase="+idFase+"&where="+where+"'>" + pregunta + "</a></b></td>";
				//return "<td align='center'><b> <a href='vistaDetalleHallazgoAutorizadoTransformacion.htm?idCeco=" + ceco 
				//		+ "&idHallazgo=" + idHallazgo + "'>" + pregunta + "</a></b></td>";
			default:
				return "<td align='center'><b> <a href='#'>" + pregunta + "</a></b></td>";
	
			}
		case 2:
			switch (status) {
			case 3:
				return "<td align='center'><b> <a href='autorizaRechazaEvaluacion.htm?idCeco=" + ceco
						+ "&idHallazgo=" + idHallazgo + "&idProyecto="+idProyecto+"&idFase="+idFase+"&where="+where+"'>" + pregunta + "</a></b></td>";
				//return "<td align='center'><b> <a href='vistaDetalleHallazgoTransformacion.htm?idCeco=" + ceco
				//		+ "&idHallazgo=" + idHallazgo + "'>" + pregunta + "</a></b></td>";
			case 0:
			case 1:
			case 2:
			case 4:
				return "<td align='center'><b> <a href='visualizarEvaluacionHallazgo.htm?idCeco=" + ceco 
						+ "&idHallazgo=" + idHallazgo + "&idProyecto="+idProyecto+"&idFase="+idFase+"&where="+where+"'>" + pregunta + "</a></b></td>";
				//return "<td align='center'><b> <a href='vistaDetalleHallazgoAutorizadoTransformacion.htm?idCeco=" + ceco 
				//		+ "&idHallazgo=" + idHallazgo + "'>" + pregunta + "</a></b></td>";
			default:
				return "<td align='center'><b> <a href='#'>" + pregunta + "</a></b></td>";
	
			}
		}
		 
	}

	public String getColorStatus(int status, int perfil) {

        String colorRojo = "#FE003D";
        String colorAmarillo = "#F4C431";
        String colorVerde = "#46ad35";
        String colorGris = "#CACACA";

		
		switch (perfil) {
		case 0:
			switch (status) {
			case 0:
				return colorRojo;
			case 1:
				return colorRojo;
			case 2:
				return colorGris;
			case 3:
				return colorGris;
			case 4:
				return colorVerde;
			}
			
		case 1:
			switch (status) {
			case 0:
				return colorRojo;
			case 1:
				return colorRojo;
			case 2:
				return colorAmarillo;
			case 3:
				return colorGris;
			case 4:
				return colorVerde;
			}
		case 2:
			switch (status) {
			case 0:
				return colorRojo;
			case 1:
				return colorRojo;
			case 2:
				return colorGris;
			case 3:
				return colorAmarillo;
			case 4:
				return colorVerde;
			}
		}
		return colorGris;
	}

	public String getStatus(int status) {

		switch (status) {
		case 0:
			return "Rechazado pendiente por Atender";
		case 1:
			return "Pendiente Por Atender";
		case 2:
			return "Pendiente por Autorizar Coordinador";
		case 3:
			return "Pendiente por Autorizar Franquicia";
		case 4:
			return "Autorizado";
		default:
			return "Pendiente Por Atender";

		}

	}

}
