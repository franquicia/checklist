/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.mappers;


import com.gruposalinas.checklist.domain.ComentariosRepAsgDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author leodan1991
 */
public class ComentariosRepAsgRowMapper implements RowMapper<ComentariosRepAsgDTO> {

    public ComentariosRepAsgDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ComentariosRepAsgDTO comentarioDTO = new ComentariosRepAsgDTO();


        comentarioDTO.setCeco(rs.getString("FCID_CECO"));
        comentarioDTO.setFecha(rs.getString("FDFECHA"));
        comentarioDTO.setSucursal(rs.getString("FCSUCURSAL"));
        comentarioDTO.setAnio(rs.getInt("FIANIO"));
        comentarioDTO.setMes(rs.getInt("FIMES"));
        comentarioDTO.setComentario(rs.getString("FCCOMENTARIO"));
        comentarioDTO.setUsrMod(rs.getString("FCUSUARIO_MOD"));
        comentarioDTO.setFechaMod(rs.getString("FDFECHA_MOD"));

        return comentarioDTO;
    }

}
