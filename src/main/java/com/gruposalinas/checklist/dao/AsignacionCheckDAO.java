package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.AsignacionCheckDTO;


public interface AsignacionCheckDAO {
	
	public List<AsignacionCheckDTO> obtieneAsignaciones(String idChecklist, String idCeco, String idUsuario, String asigna) throws Exception;
	
	public List<AsignacionCheckDTO> obtieneAsignacionC(int bunker, int pais) throws Exception;
	
	public boolean insertaAsignacion(AsignacionCheckDTO asignacion) throws Exception;
	
	public boolean actualizaAsignacion(AsignacionCheckDTO asignacion)throws Exception;
	
	public boolean eliminaAsignacion(AsignacionCheckDTO asignacion )throws Exception;
	
	public boolean asignaCheclist() throws Exception;

}
