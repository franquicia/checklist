package com.gruposalinas.checklist.servicios.servidor;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.gruposalinas.checklist.business.CheckInAsistenciaBI;
import com.gruposalinas.checklist.business.ParametroBI;
import com.gruposalinas.checklist.domain.CheckInAsistenciaDTO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;
import com.gruposalinas.checklist.domain.ParametroDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.util.StrCipher;
import com.gruposalinas.checklist.util.UtilCryptoGS;

@Controller
@RequestMapping("/servicios")
public class ServiciosMantenimientoSupervision {
	
	private static Logger logger = LogManager.getLogger(ServiciosMantenimientoSupervision.class);
	
    @Autowired
    CheckInAsistenciaBI checkInAsistenciaBI;
    
    //SERVICIO QUE GUARDA EL CHECK IN -- CHECK OUT DE INICIO Y FIN DE DÍA
	// http://10.89.85.167:8080/checklist/servicios/cargaDataInfo.json?idUsuario=189871
	@SuppressWarnings("static-access")
	@RequestMapping(value = "/cargaDataInfo", method = { RequestMethod.POST })
	public @ResponseBody boolean setCheckDia(@RequestBody String provider,
			HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {

		UtilCryptoGS cifra = new UtilCryptoGS();
		StrCipher cifraIOS = new StrCipher();
		UtilCryptoGS descifra = new UtilCryptoGS();
		
		String uri = request.getQueryString();
		String uriAp = request.getParameter("token");
		int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
		uri = uri.split("&")[0];
		String urides = "";

		String contenido = "";
		boolean responseService = false;

		try {
			
			if (idLlave == 7) {
				
				urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
				// logger.info("URIDES IOS: " + urides);

				String tmp = "";
				tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
				tmp = tmp.replaceAll(" ", "+");
				// logger.info("JSON IOS TMP: " + tmp);
				contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

				// logger.info("JSON IOS: " + contenido);

			} else if (idLlave == 666) {
				
				urides = (cifra.decryptParams(uri));
				contenido = descifra.decryptParams(provider);
			}

			// Json

			try {

				if (contenido != null) {

					JSONObject rec = new JSONObject(contenido);
					
					logger.info("Json de entrada");
					logger.info(contenido);
					logger.info("Fin Json de entrada");
					
					JSONObject results = rec.getJSONObject("data");
					
					String ruta = results.getString("foto");
					String lugar = results.getString("lugarCheck");
					double latitud = results.getDouble("latitud");
					double longitud = results.getDouble("longitud");
					String fecha = results.getString("fecha");request.getParameter("");
					int idUsuario = results.getInt("idUsuario");
					String observaciones = results.getString("comentario");
					int tipoCheckIn = results.getInt("tipoCheck");
					int tipoProyecto = results.getInt("tipoProyecto");

					CheckInAsistenciaDTO asist = new CheckInAsistenciaDTO();
					asist.setIdUsuario(idUsuario);
					asist.setLatitud(latitud);
					asist.setLongitud(longitud);
					asist.setFecha(fecha);
					asist.setObservaciones(observaciones);
					asist.setRuta(ruta);
					asist.setLugar(lugar);
					asist.setTipoCheckIn(tipoCheckIn);
					asist.setIdTipoProyecto(tipoProyecto);
					 
					
					try {

						//Error -1 -- 1 Correcto
						int respuesta = checkInAsistenciaBI.insertaAsistencia(asist);
						
						responseService = (respuesta > 0 ) ? true : false;

					
					} catch (Exception e) {
					
						logger.info("AP EN INSERCIÓN DE checkInAsistenciaBI" + e);

						return false;
					}
				}
				
			} catch (Exception e) {
				
				logger.info("AP AL OBTENER EL JSON checkInAsistenciaBI" + e.getStackTrace());
				return false;
			}
		} catch (Exception e) {
			
			logger.info("AP AL OBTENER PARAMETROS DE REQUEST checkInAsistenciaBI" + e);
			return false;
		}

		return responseService;
	}
}
