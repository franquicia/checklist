package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.PreguntaCheckDTO;
import com.gruposalinas.checklist.mappers.ChecklistPregComEvidRowMapper;
import com.gruposalinas.checklist.mappers.ChecklistPregComGenRowMapper;
import com.gruposalinas.checklist.mappers.ChecklistPregComRepoRowMapper;
import com.gruposalinas.checklist.mappers.ChecklistPregComRowMapper;
import com.gruposalinas.checklist.mappers.ConteoSINAGralHijasRowMapper;
import com.gruposalinas.checklist.mappers.ConteoSINOGralExtraRowMapper;
import com.gruposalinas.checklist.mappers.ConteoSINOGralRowMapper;
import com.gruposalinas.checklist.mappers.ConteoSINORowMapper;
import com.gruposalinas.checklist.mappers.HallazXcecoRecoRowMapper;
import com.gruposalinas.checklist.mappers.HallazgosTablaxCecoRowMapper;
import com.gruposalinas.checklist.mappers.PregNoImpCecoFaaseProyectoRowMapper;
import com.gruposalinas.checklist.mappers.PreguntaCheckImpConcatRowMapper;
import com.gruposalinas.checklist.mappers.PreguntaCheckImpRowMapper;
import com.gruposalinas.checklist.mappers.PreguntaCheckPadreRowMapper;
import com.gruposalinas.checklist.mappers.PreguntaCheckRowMapper;
import com.gruposalinas.checklist.mappers.PreguntaPregConcatRowMapper;
import com.gruposalinas.checklist.mappers.RespSoftCecoFaseProyectoRowMapper;
import com.gruposalinas.checklist.mappers.SoftEvidenciasRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ChecklistPreguntasComDAOImpl extends DefaultDAO implements ChecklistPreguntasComDAO {

    private static Logger logger = LogManager.getLogger(ChecklistPreguntasComDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcConteoGrupo;
    DefaultJdbcCall jdbcObtieneEmpFijo;
    DefaultJdbcCall jdbcObtienePreguntas;
    DefaultJdbcCall jdbcObtienePregGen;
    DefaultJdbcCall jdbcObtienePreg;
    DefaultJdbcCall jdbcObtieneConteoHij;
    DefaultJdbcCall jdbcObtienePregImp;
    DefaultJdbcCall jdbcObtienePregNoImp;
    DefaultJdbcCall jdbcObtienePregPadreHij;
    DefaultJdbcCall jdbcObtieneCampExtras;
    DefaultJdbcCall jdbcObtienePregConcatImp;
    DefaultJdbcCall jdbcObtienePregConcatImppru;
    DefaultJdbcCall jdbcObtieHallazReco;
    DefaultJdbcCall jdbcObtieHallaztab;
    DefaultJdbcCall jdbcObtieHallazRecoNvo;
    DefaultJdbcCall jdbcObtieHallaztabNvo;
    //OCC

    DefaultJdbcCall jdbcConteoGrupoOcc;
    DefaultJdbcCall jdbcConteoComGrupoOcc;
    DefaultJdbcCall jdbcConteoComSiNAGrupoOcc;

    //SOFT CECO FASE PROYECTO
    DefaultJdbcCall jdbcSoftCecoFaseProyecto;

    //SOFT CECO FASE PROYECTO STRING
    DefaultJdbcCall jdbcSoftCecoFaseProyectoString;
    ///
    DefaultJdbcCall jdbcObtienePregNoImpCecoFasePooyecto;

    public void init() {

        //EKT
        //trae si y no conteo por checklist
        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGCHECKS")
                .withProcedureName("SPCONT_SINO")
                .returningResultSet("RCL_PREG", new ConteoSINORowMapper());

        //trae si y no conteo por grupo
        jdbcConteoGrupo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGCONTEO")
                .withProcedureName("SPCONT_BITGEN")
                .returningResultSet("RCL_PREG", new ConteoSINOGralRowMapper());
        //trae si y no conteo por grupo los campos Extras
        jdbcObtieneCampExtras = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGCONTEO2")
                .withProcedureName("SPCONT_BITGEN2")
                .returningResultSet("RCL_PREG", new ConteoSINOGralExtraRowMapper());

        //trae si y no conteo por grupo hijas
        jdbcObtieneConteoHij = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGCONTEO3")
                .withProcedureName("SPCONT_BITGEN3")
                .returningResultSet("RCL_PREG", new ConteoSINAGralHijasRowMapper());

        // trae respuestas
        jdbcObtieneEmpFijo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGCHECKS").withProcedureName("SP_SEL_RESP")
                .returningResultSet("RCL_EVID", new ChecklistPregComEvidRowMapper())
                .returningResultSet("RCL_PREG", new ChecklistPregComRowMapper());

        // trae respuestas Generico
        jdbcObtienePregGen = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGCHECKS").withProcedureName("SP_SEL_RESPGEN")
                .returningResultSet("RCL_PREG", new ChecklistPregComGenRowMapper());

        // trae preguntas del check
        jdbcObtienePreg = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PREG").withProcedureName("SP_SEL_PREG_CHECK")
                .returningResultSet("RCL_PREG", new PreguntaCheckRowMapper());

        // trae respuestasb imperdonables
        jdbcObtienePregImp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGSEXP").withProcedureName("SP_CONS_NOIMP")
                .returningResultSet("RCL_PREG", new PreguntaCheckImpRowMapper());

        // trae respuestas sin imperdonables
        jdbcObtienePregNoImp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGSEXP").withProcedureName("SP_CONS_NOGEN")
                .returningResultSet("RCL_PREG", new PreguntaCheckImpRowMapper());

        // trae preguntas padres e hijas
        jdbcObtienePregPadreHij = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGSEXP").withProcedureName("SP_CONS_PADHIJ")
                .returningResultSet("RCL_PREGPAD", new PreguntaCheckPadreRowMapper())
                .returningResultSet("RCL_PREGHIJ", new PreguntaCheckPadreRowMapper());

        // trae respuestas de todas las preguntas
        jdbcObtienePreguntas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGCHECKS").withProcedureName("SP_SEL_RESPREPO")
                .returningResultSet("RCL_PREG", new ChecklistPregComRepoRowMapper());

        // trae preguntas preguntas concatenadas imperdonables
        jdbcObtienePregConcatImp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGSEXP")
                .withProcedureName("SP_NOIMP_NUEV")
                .returningResultSet("RCL_PREG", new PreguntaCheckImpConcatRowMapper())
                .returningResultSet("RCL_PREGUNTAS", new PreguntaPregConcatRowMapper());

        // trae preguntas preguntas concatenadas imperdonables en una consulta
        jdbcObtienePregConcatImppru = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGSEXP")
                .withProcedureName("SP_NOIMP")
                .returningResultSet("RCL_PREG", new ChecklistPregComRowMapper());

        // hallazgos recorrido
        jdbcObtieHallazReco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREVISAHALLAZ")
                .withProcedureName("SP_RECO_HALLAZ")
                .returningResultSet("RCL_HALLAZRECO", new HallazXcecoRecoRowMapper());

        // hallazgos tabla
        jdbcObtieHallaztab = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREVISAHALLAZ")
                .withProcedureName("SP_TAB_HALLAZ")
                .returningResultSet("RCL_HALLAZTAB", new HallazgosTablaxCecoRowMapper());

        //OCC
        //trae si y no conteo por grupo
        jdbcConteoGrupoOcc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGCONTEOCC")
                .withProcedureName("SPCONT_BITGEN")
                .returningResultSet("RCL_PREG", new ConteoSINOGralRowMapper());

        // trae complemento totgral,totsi,totna,sumagrupo
        jdbcConteoComGrupoOcc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGCONTEOCC2")
                .withProcedureName("SPCONT_BITGEN2")
                .returningResultSet("RCL_PREG", new ConteoSINOGralExtraRowMapper());
        //ConteoSINOGralOCCRowMapper completo en un cursor

        jdbcConteoComSiNAGrupoOcc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGCONTEOCC3")
                .withProcedureName("SPCONT_BITGEN3")
                .returningResultSet("RCL_PREG", new ConteoSINAGralHijasRowMapper());

        // hallazgos recorrido NVO
        jdbcObtieHallazRecoNvo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREVISAHALLANV")
                .withProcedureName("SP_RECO_HALLAZ")
                .returningResultSet("RCL_HALLAZRECO", new HallazXcecoRecoRowMapper());

        // hallazgos tabla NVO
        jdbcObtieHallaztabNvo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREVISAHALLANV")
                .withProcedureName("SP_TAB_HALLAZ")
                .returningResultSet("RCL_HALLAZTAB", new HallazgosTablaxCecoRowMapper());

        // SOFT CECO FASE PROYECTO
        jdbcSoftCecoFaseProyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASOFTRECO")
                .withProcedureName("SP_SEL_SOFT")
                .returningResultSet("RCL_PREG", new RespSoftCecoFaseProyectoRowMapper());

        // SOFT CECO FASE PROYECTOString
        jdbcSoftCecoFaseProyectoString = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASOFTRECO")
                .withProcedureName("SP_SEL_SOFT")
                .returningResultSet("RCL_PREG", new RespSoftCecoFaseProyectoRowMapper())
                .returningResultSet("RCL_EVI", new SoftEvidenciasRowMapper());

        // PREGUNTAS NO IMP CECO FASE PROYECTOString
        jdbcObtienePregNoImpCecoFasePooyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEXPNVOFLUJO")
                .withProcedureName("SP_CONS_NO")
                .returningResultSet("RCL_PREG", new PregNoImpCecoFaaseProyectoRowMapper());

    }

    // trae si y no conteo
    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneInfo(String idBita) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", idBita);

        out = jdbcObtieneTodo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGCHECKS.SP_CONT_SINO}");

        listaFijo = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener EL CONTEO DE PREGUNTAS");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    // trae preguntas
    @SuppressWarnings("unchecked")
    public Map<String, Object> obtieneDatos(String idBita) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        int error = 0;
        List<ChecklistPreguntasComDTO> listapreg = null;
        List<ChecklistPreguntasComDTO> listaEvi = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", idBita);

        out = jdbcObtieneEmpFijo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGCHECKS.SP_SEL_RESP}");
        // lleva el nombre del cursor del procedure
        listapreg = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");
        listaEvi = (List<ChecklistPreguntasComDTO>) out.get("RCL_EVID");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener LAS RESPUESTAS(" + idBita + ")");
        }
        lista = new HashMap<String, Object>();
        lista.put("listaPregunta", listapreg);
        lista.put("listaRespuesta", listaEvi);

        return lista;
    }

    // trae preguntas Generico
    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneDatosGen(String idBita) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ChecklistPreguntasComDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", idBita);

        out = jdbcObtienePregGen.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGCHECKS.SP_SEL_RESPGEN}");
        // lleva el nombre del cursor del procedure
        lista = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener LAS RESPUESTAS(" + idBita + ")");
        }
        return lista;

    }

    // trae si y no conteo por Grupo falta implementarlo
    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneInfoCont(String idBita) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", idBita);

        out = jdbcConteoGrupo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGCONTEO.SPCONT_BITGEN}");

        listaFijo = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");
        logger.info("*lista de areas*: " + listaFijo);

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener EL CONTEO DE PREGUNTAS POR GRUPO");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    // trae si y no conteo por Grupo campos Extras
    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneInfoContExt(String idBita) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", idBita);

        out = jdbcObtieneCampExtras.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGCONTEO.SPCONT_BITGEN}");

        listaFijo = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");
        logger.info("*lista de areas*: " + listaFijo);

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener EL CONTEO DE PREGUNTAS POR GRUPO");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    // trae si y no conteo por Grupo campos Extras

    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneInfoContHij(String idBita) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", idBita);

        out = jdbcObtieneConteoHij.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGCONTEO.SPCONT_BITGEN}");

        listaFijo = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");
        logger.info("*lista de areas*: " + listaFijo);

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener EL CONTEO DE PREGUNTAS POR GRUPO");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    // trae preguntas check

    @SuppressWarnings("unchecked")
    public List<PreguntaCheckDTO> obtieneInfopreg(int idCheck) throws Exception {
        Map<String, Object> out = null;
        List<PreguntaCheckDTO> list = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECKLIST", idCheck);

        out = jdbcObtienePreg.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PA_ADM_PREG.SP_SEL_PREG_CHECK}");

        list = (List<PreguntaCheckDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las preguntas del check:  " + idCheck);
        } else {
            return list;
        }

        return list;

    }

//trae preg imperdonables
    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneInfoImp(int idBita) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> listaimper = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", idBita);

        out = jdbcObtienePregImp.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGSEXP.SP_CONS_NOIMP}");

        listaimper = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion de imperdonables");
        } else {
            return listaimper;
        }

        return listaimper;

    }
//trae preg no imperdonables

    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneInfoNoImp(int idBita) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> listanoimp = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", idBita);

        out = jdbcObtienePregNoImp.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGSEXP.SP_CONS_NOGEN}");

        listanoimp = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion de no  imperdonables");
        } else {
            return listanoimp;
        }

        return listanoimp;

    }
//trae preg padres e hijas

    @SuppressWarnings("unchecked")
    public Map<String, Object> obtieneInfoPadHij(String idBita) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<ChecklistPreguntasComDTO> listapadres = null;
        List<ChecklistPreguntasComDTO> listahijas = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", idBita);

        out = jdbcObtienePregPadreHij.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGSEXP.SP_CONS_PADHIJ}");

        listapadres = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREGPAD");
        listahijas = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREGHIJ");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion de padres e hijas");
        } else {
            lista = new HashMap<String, Object>();
            lista.put("listapadres", listapadres);
            lista.put("listahijas", listahijas);

            return lista;

        }
        return lista;
    }

    // trae todas las preguntas apartir de una hermana
    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneDatosGenales(String idBita) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", idBita);

        out = jdbcObtienePreguntas.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGCHECKS.SP_SEL_RESPREPO}");

        listaFijo = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las preguntas ");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    // obtiene respuesta NO IMPERDONABLE CONCATENADAS
    @SuppressWarnings("unchecked")
    public Map<String, Object> obtieneDatosImpconc(String idBita) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<ChecklistPreguntasComDTO> listapadres = null;
        List<ChecklistPreguntasComDTO> listahijas = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", idBita);

        out = jdbcObtienePregConcatImp.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGSEXP.SP_NOIMP_NUEV}");

        listapadres = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREGUNTAS");
        listahijas = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion de padres e hijas");
        } else {
            lista = new HashMap<String, Object>();
            lista.put("listapadres", listapadres);
            lista.put("listahijas", listahijas);

            return lista;

        }
        return lista;
    }

    // obtiene respuesta NO IMPERDONABLE CONCATENADAS
    @SuppressWarnings("unchecked")
    public Map<String, Object> obtieneDatosImpconcPru(String idBita) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<ChecklistPreguntasComDTO> listahijas = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", idBita);

        out = jdbcObtienePregConcatImppru.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGSEXP.SP_NOIMP_NUEV}");

        listahijas = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion de padres e hijas");
        } else {
            lista = new HashMap<String, Object>();

            lista.put("listahijas", listahijas);

            return lista;

        }
        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneInfoHallaz(int ceco) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> lista = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco);

        out = jdbcObtieHallazReco.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREVISAHALLAZ.SP_RECO_HALLAZ}");

        lista = (List<ChecklistPreguntasComDTO>) out.get("RCL_HALLAZRECO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió ");
        } else {
            return lista;
        }

        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneInfoHallazTab(int ceco) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> lista = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco);

        out = jdbcObtieHallaztab.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREVISAHALLAZ.SP_TAB_HALLAZ}");

        lista = (List<ChecklistPreguntasComDTO>) out.get("RCL_HALLAZTAB");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió  ");
        } else {
            return lista;
        }

        return lista;
    }

    //OCC
    // trae si y no conteo
    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneInfoOCC(String idBita) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", idBita);

        out = jdbcConteoGrupoOcc.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGCONTEOCC.SPCONT_BITGEN}");

        listaFijo = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener EL CONTEO DE PREGUNTAS");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    //OCC
    // trae complemento totgral,sumagrupo
    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneComplemOCC(String idBita) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", idBita);

        out = jdbcConteoComGrupoOcc.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGCONTEOCC2.SPCONT_BITGEN2}");

        listaFijo = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener EL CONTEO DE PREGUNTAS");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    //OCC
    // trae complemento ,totsi,totna,
    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneComplemSiNaOCC(String idBita) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", idBita);

        out = jdbcConteoComSiNAGrupoOcc.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGCONTEOCC3.SPCONT_BITGEN}");

        listaFijo = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener EL CONTEO DE PREGUNTAS");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneInfoHallazNvo(int ceco) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> lista = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco);

        out = jdbcObtieHallazRecoNvo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREVISAHALLANV.SP_RECO_HALLAZ}");

        lista = (List<ChecklistPreguntasComDTO>) out.get("RCL_HALLAZRECO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió ");
        } else {
            return lista;
        }

        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneInfoHallazTabNvo(int ceco) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> lista = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco);

        out = jdbcObtieHallaztabNvo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREVISAHALLANV.SP_TAB_HALLAZ}");

        lista = (List<ChecklistPreguntasComDTO>) out.get("RCL_HALLAZTAB");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió  ");
        } else {
            return lista;
        }

        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtieneSoftCecoFaseProyecto(String ceco, String fase, String proyecto, String checklist) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> lista = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto)
                .addValue("PA_CHECKLIST", checklist);

        out = jdbcSoftCecoFaseProyecto.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PASOFTRECO.RESPUESTASSOFTNVO}");

        lista = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió  ");
        } else {
            return lista;
        }

        return lista;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> obtieneSoftCecoFaseProyectoCom(String ceco, String fase, String proyecto, String checklist) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        int error = 0;
        List<ChecklistPreguntasComDTO> listapreg = null;
        List<ChecklistPreguntasComDTO> listaEvi = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto)
                .addValue("PA_CHECKLIST", checklist);

        out = jdbcSoftCecoFaseProyectoString.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PASOFTRECO.RESPUESTASSOFTNVO}");

        // lleva el nombre del cursor del procedure
        listapreg = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");
        listaEvi = (List<ChecklistPreguntasComDTO>) out.get("RCL_EVI");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener LAS RESPUESTAS SOFT" + ceco + "  " + fase + " " + proyecto);
        }
        lista = new HashMap<String, Object>();
        lista.put("listaRespuesta", listapreg);
        lista.put("listaEvidencia", listaEvi);

        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistPreguntasComDTO> obtienePregNoImpCecoFaseProyecto(String ceco, String fase, String proyecto)
            throws Exception {
        Map<String, Object> out = null;
        List<ChecklistPreguntasComDTO> listanoimp = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto);

        out = jdbcObtienePregNoImpCecoFasePooyecto.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAEXPNVOFLUJO.SP_CONS_NO}");

        listanoimp = (List<ChecklistPreguntasComDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion de no  imperdonables");
        } else {
            return listanoimp;
        }

        return listanoimp;
    }

}
