package com.gruposalinas.checklist.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.SoporteHallazgosTransfDAO;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.FaseFirmasDTO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;


public class SoporteHallazgosTransfBI {

	private static Logger logger = LogManager.getLogger(SoporteHallazgosTransfBI.class);

private List<HallazgosTransfDTO> listafila;

private List<FaseFirmasDTO> faseFirma;
	
	@Autowired
	SoporteHallazgosTransfDAO soporteHallazgosTransfDAO;
	

			
	
	
	public List<HallazgosTransfDTO> obtieneHallazgosSoporte(String ceco,String fase,String proyecto) {
		
		try {
			listafila =  soporteHallazgosTransfDAO.obtieneHallazgosSoporte(ceco,fase,proyecto);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	

	
	public boolean actualizaFaseHallazgos(HallazgosTransfDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = soporteHallazgosTransfDAO.actualizaFaseHallazgos(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	
	public boolean eliminaHallazgosEHistorico(String bita,String ceco) {
		boolean respuesta = false;

		try {
			respuesta = soporteHallazgosTransfDAO.eliminaHallazgosEHistorico(bita,ceco);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta;
	}
	
	// trae respuestas
		public String obtieneOrdenFaseFirmas(String ceco, String fase, String proyecto) {
			JSONObject respuesta = new JSONObject();

			try {
				Map<String, Object> listaFaseFirma = null;
				listaFaseFirma = soporteHallazgosTransfDAO.obtieneOrdenFaseFirmas(ceco,fase,proyecto);

				@SuppressWarnings("unchecked")
				List<FaseFirmasDTO> listaOrdenFase = (List<FaseFirmasDTO>) listaFaseFirma.get("listaOrdenFase");
				@SuppressWarnings("unchecked")
				List<FaseFirmasDTO> listaFirmas = (List<FaseFirmasDTO>) listaFaseFirma.get("listaFirmas");

				JSONArray ordenFase = new JSONArray();
				JSONArray firmas = new JSONArray();

				// lista preguntas
				for (int i = 0; i < listaOrdenFase.size(); i++) 
				{

					JSONObject obj = new JSONObject();
					obj.put("idSoft", listaOrdenFase.get(i).getIdSoft());
					obj.put("ceco", listaOrdenFase.get(i).getCeco());
					obj.put("fase", listaOrdenFase.get(i).getIdFase());
					obj.put("proyecto", listaOrdenFase.get(i).getIdProyecto());
					obj.put("idOrdenFase", listaOrdenFase.get(i).getIdOrdenFaFse());
					obj.put("nombreFase", listaOrdenFase.get(i).getNombreFase());
					obj.put("detalleFase", listaOrdenFase.get(i).getDetalleFase());
								
					ordenFase.put(obj);

				}
				for (int i = 0; i < listaFirmas.size(); i++) {
					
					{
					JSONObject obj = new JSONObject();
					obj.put("idSoft", listaFirmas.get(i).getIdSoft());
					obj.put("ceco", listaFirmas.get(i).getCeco());
					obj.put("fase", listaFirmas.get(i).getIdFase());
					obj.put("proyecto", listaFirmas.get(i).getIdProyecto());
					obj.put("puesto", listaFirmas.get(i).getPuesto());
					obj.put("responsable", listaFirmas.get(i).getResponsable());
					obj.put("correo", listaFirmas.get(i).getCorreo());
					obj.put("orden", listaFirmas.get(i).getNombre());
					obj.put("ruta", listaFirmas.get(i).getRuta());
					obj.put("idAgupaFirma", listaFirmas.get(i).getIdAgrupa());
					
					firmas.put(obj);
					}

				}
				respuesta.append("listaOrdenFase", ordenFase);
				respuesta.append("listaFirmas", firmas);

			} catch (Exception e) {
				logger.info("No fue posible obtener los datos");
				UtilFRQ.printErrorLog(null, e);
			}

			return respuesta.toString();
		}
		public List<HallazgosTransfDTO> obtieneAdicionalesCecoFaseProyecto(String ceco,String fase,String proyecto,int parametro) {
			
			try {
				listafila =  soporteHallazgosTransfDAO.obtieneAdicionalesCecoFaseProyecto(ceco,fase,proyecto,parametro);
			} catch (Exception e) {
				logger.info("No fue posible obtener los datos");
				UtilFRQ.printErrorLog(null, e);	
			}
			
			return listafila;			
		}

	
}