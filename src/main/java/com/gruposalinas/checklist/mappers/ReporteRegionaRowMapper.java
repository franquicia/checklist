package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteDTO;

public class ReporteRegionaRowMapper implements RowMapper<ReporteDTO>  {

	public ReporteDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteDTO reporteDTO = new ReporteDTO();

		reporteDTO.setTerritorio(rs.getString("TERRITORIO"));
		reporteDTO.setZona(rs.getString("ZONA"));
		reporteDTO.setRegional(rs.getString("REGIONAl"));
		reporteDTO.setSucursal(rs.getString("SUCURSAL"));
		reporteDTO.setFecha_r(rs.getString("FECHA_R"));
		reporteDTO.setPregunta_cp(rs.getString("PREGUNTA_CP"));
		reporteDTO.setMonto(rs.getString("MONTO"));
		reporteDTO.setMotivo(rs.getString("MOTIVO"));
		reporteDTO.setFecha_c(rs.getString("FECHA_C"));
		reporteDTO.setPregunta_ca(rs.getString("PREGUNTA_CA"));
		reporteDTO.setMonto_ca(rs.getString("MONTO_CA"));
		reporteDTO.setMotivo_ca(rs.getString("MOTIVO_CA"));
		reporteDTO.setFecha_ca(rs.getString("FECHA_CA"));
		
		return reporteDTO;
		
	}

}
