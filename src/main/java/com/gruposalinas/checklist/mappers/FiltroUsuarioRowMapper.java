package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteSupervisionSistemasDTO;

public class FiltroUsuarioRowMapper implements RowMapper<ReporteSupervisionSistemasDTO> {

	public ReporteSupervisionSistemasDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		ReporteSupervisionSistemasDTO reporteCecosSistemas = new ReporteSupervisionSistemasDTO();
		reporteCecosSistemas.setUsuario(rs.getString("USUARIO"));
		reporteCecosSistemas.setIdCecos(rs.getInt("FIID_USUARIO"));
		
		return reporteCecosSistemas;
	}

}
