package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.FaseTransfDTO;

public class FaseTransfAgrupaRowMapper implements RowMapper<FaseTransfDTO> {

	public FaseTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		FaseTransfDTO FaseTransfDTO = new FaseTransfDTO();
		
		FaseTransfDTO.setIdTab(rs.getInt("FIID_TAB"));
		FaseTransfDTO.setIdAgrupa(rs.getInt("FIID_AGRUPA"));
		FaseTransfDTO.setFase(rs.getInt("FIID_FASE"));
		FaseTransfDTO.setIdactivo(rs.getInt("FIIDACTIVO"));
	    FaseTransfDTO.setObs(rs.getString("FCOBSERVACION"));
	    FaseTransfDTO.setOrdenFase(rs.getInt("FIID_ORDENFASE"));
	    FaseTransfDTO.setPeriodo(rs.getString("FCPERIODO"));
	 
	return FaseTransfDTO;
	}

}


