package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.NegocioAdicionalDTO;

public class NegocioAdicionalRowMapper implements RowMapper<NegocioAdicionalDTO>{

	@Override
	public NegocioAdicionalDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		NegocioAdicionalDTO adicionalDTO = new NegocioAdicionalDTO();
		
		adicionalDTO.setNegocio(rs.getInt("FIID_NEGOCIO"));
		adicionalDTO.setUsuario(rs.getInt("FIID_USUARIO"));
		
		
		return adicionalDTO;
	}
	

}
