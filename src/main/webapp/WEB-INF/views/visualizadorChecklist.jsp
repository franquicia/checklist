<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="height: 98%; min-width: 1200px">
<head>
<link href="../css/visualizador/visualizaImagenes.css"
	rel="stylesheet" type="text/css" />
	
<link href="../css/jQuery/jQuery.css"
	rel="stylesheet" type="text/css" />
	
<link href="../css/checklistDesign.css"
	rel="stylesheet" type="text/css" />
	
<link rel="stylesheet" href="../css/visualizador/w3.css">
	
<script type="text/javascript">
	var urlServer = '${urlServer}';
</script>

<script	src="../css/jQuery/jQuery.js"></script>
<script	src="../js/script-consultas.js"></script>


<!-- DESABILITA EL BOTON DE ENTER EN LA VISTA -->
<script type="text/javascript">
$(document).ready(function() {
	  $(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
	});
</script>


<!-- PONE EL TIEMPO DEL MENSAJE -->
<script type="text/javascript">
$(document).ready(function() {
    setTimeout(function() {
        $(".content-Mensaje").fadeOut(1500);
    },3000);
});
</script>

<!-- EJECUTA EL CALENDARIO -->
<script type="text/javascript">
$(function() {
    $( "#fechaImagen" ).datepicker();
});
</script>

</head>
<body onload="init()" style="height: 95%;">

	<h2 class="w3-animate-zoom  datos-vacios content-Mensaje">${fallo}</h2>
					
	<div style="background: white; margin: 30px 24px 50px 24px; min-width: 700px; min-height:674px; height: 94%; box-shadow: 5px 5px 17px #000000;" >

		<form:form name="formulario" method="POST"	action="visualizadorController.htm" commandName="command" style="height:100%;">
			<div style="height: 21.46%; min-height: 146px;">
				<ul class="margenUl">
				
					<div style="height:10%;">
					</div>
					
					<div style="height:37%;">
					<li class="li-stilo"><span class="circulo">1</span> Categoría	
						<div class="cajaVis" id="categoriaVis">
							<input type="hidden" name="categoria" value="Selecciona una Opcion" id="valorCategoria">
							<span class="seleccionadoVis" id="Selecciona una Opcion">Elije una opcion</span>
							<ul class="listaselectVis">
								<c:forEach items="${listaTipoCheck}" var="item">
									<li value="${item.idTipoCheck}"><a href="">${item.descTipo}</a></li>
								</c:forEach>						
							</ul >
							<span class="trianguloinfVis"></span>
						</div>	
					</li>
					</div>
					<div style="height:53%;">
					<li class="li-stilo margenLista"><span class="segundo-nivel">Tipo</span> 
						<div class="cajaVis" id="tipoVis">
							<input type="hidden" name="tipo" value="Selecciona una Opcion" id="valorTipo">
							<span class="seleccionadoVis " id="Selecciona una Opcion">Elije una opcion</span>
								<ul class="listaselectVis" id="tipoVis2"></ul>
							<span class="trianguloinfVis"></span>
						</div>
					</li>
					</div>
				</ul>
							
			
			</div>
	
			<div style="height: 31.15%; min-height: 210px;">
				<ul class="margenUl">
				
				<div style="height:25%;">
					<li class="li-stilo"><span class="circulo">2</span> Puesto 
					<div class="cajaVis" id="puestoVis">
					<input type="hidden" name="puesto" value="Selecciona una Opcion" id="valorPuesto">
						<span class="seleccionadoVis" id="Selecciona una Opcion">Elije una opcion</span>
						<ul class="listaselectVis"  id="buscaPuesto">
							<c:forEach items="${listaPuesto}" var="item">
								<li value="${item.idPuesto}"><a href="">${item.descripcion}</a></li>
							</c:forEach>						
						</ul >
						<span class="trianguloinfVis"></span>
					</div>
					</li>
				</div>	
				<div style="height:22%;">	
					<li class="li-stilo"><span class="primer-nivel">Usuario</span> 
					<input class="textBoxInput" type="text" name="usuario" placeholder="Escribe el número de socio" id="buscaUsuario2" size="22"/>
				</div>
				<div style="height:22%;">
					<li class="li-stilo"><span class="primer-nivel">Centro de Costos</span> 
					<input class="textBoxInput" type="text" name="costos" placeholder="Escribe el centro de costos" id="buscaCostos2" size="22" />
					</li>
				</div>
				<div style="height:22%;">
					<li class="li-stilo"><span class="primer-nivel">Fecha</span> 
					<input class="textBoxInput" type="text" id="fechaImagen" name="fecha" placeholder="Selecciona la fecha" size="22" />
						</li>
				</div>
				</ul>	
			</div>
			
			
			
			<div style="height: 36.79%; min-height: 248px;">
				<ul class="margenUl">
				
				<div style="height:25%;">
					<li class="li-stilo"><span class="circulo">3</span> Geografía</li>
					
				</div>	
					
									
				<div style="height:11%; ">	
					<li class="li-stilo"><span class="primer-nivel">Pais</span>
					<c:set var="margen" scope="session" value="${-20}" /> 
						<div class="cajaVis" id="paisVis">
							<input type="hidden" name="pais" value="Selecciona una Opcion" id="valorPais">
							<span class="seleccionadoVis" id="Selecciona una Opcion">Elije una opcion</span>
							<ul class="listaselectVis">
								<c:forEach items="${listaPaises}" var="item">
									<li style="margin:${margen}px 0 0 0;" value="${item.idPais}"><a href="">${item.nombre}</a></li>
									<c:set var="margen" scope="session" value="${-40}" /> 
								</c:forEach>					
								</ul >
							<span class="trianguloinfVis"></span>
						</div>
					</li>
				</div>	
					
				<div style="height:11%;">	
					<li class="li-stilo margenLista"><span class="segundo-nivel">Territorio</span> 
						<div class="cajaVis" id="territorioVis">
							<input type="hidden" name="territorio" value="Selecciona una Opcion" id="valorTerritorio">
							<span class="seleccionadoVis " id="Selecciona una Opcion">Elije una opcion</span>
								<ul class="listaselectVis" id="territorioVis2"></ul>
							<span class="trianguloinfVis"></span>
						</div>
					</li>
				</div>
				
				<div style="height:11%;">	
					<li class="li-stilo margenLista"><span class="tercer-nivel">Zona</span> 
						<div class="cajaVis" id="zonaVis">
							<input type="hidden" name="zona" value="Selecciona una Opcion" id="valorZona">
							<span class="seleccionadoVis " id="Selecciona una Opcion">Elije una opcion</span>
								<ul class="listaselectVis" id="zonaVis2"></ul>
							<span class="trianguloinfVis"></span>
						</div>
					</li>
				</div>	
							
				<div style="height:11%;">				
					<li class="li-stilo margenLista"><span class="cuarto-nivel">Región</span> 
						<div class="cajaVis" id="regionVis">
							<input type="hidden" name="region" value="Selecciona una Opcion" id="valorRegion">
							<span class="seleccionadoVis " id="Selecciona una Opcion">Elije una opcion</span>
								<ul class="listaselectVis" id="regionVis2"></ul>
							<span class="trianguloinfVis"></span>
						</div>
					</li>
				</div>					
			
				<div style="height:11%;">					
					<li class="li-stilo margenLista"><span class="quinto-nivel">Sucursal</span> 
						<div class="cajaVis" id="sucursalVis">
							<input type="hidden" name="sucursal" value="Selecciona una Opcion" id="valorSucursal">
							<span class="seleccionadoVis " id="Selecciona una Opcion">Elije una opcion</span>
								<ul class="listaselectVis" id="sucursalVis2"></ul>
							<span class="trianguloinfVis"></span>
						</div>
					</li>
				</div>
				
										
				</ul>	
			</div>
					
			
			<div style="height: 10.6%; min-height: 70px;">
				
				<div style="margin-top:1.5%; height: 50px; width: 18%; float:right;">
					<button type="submit" class="logEnviar-btn" id="logEnviar-btn" name="envia"	value="update" >Enviar</button>
				</div>
				
			</div>
		</form:form>
	</div>


</body>


</html>