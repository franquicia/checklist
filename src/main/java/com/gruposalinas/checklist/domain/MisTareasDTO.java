package com.gruposalinas.checklist.domain;

import com.google.gson.annotations.SerializedName;

public class MisTareasDTO {
	@SerializedName("PID") private String pid;
	@SerializedName("PName") private String pName;
	@SerializedName("Start") private String start;
	@SerializedName("Due") private String due;
	@SerializedName("PType") private String pType;
	@SerializedName("PTypeDesc") private String pTypeDesc;
	@SerializedName("Comments") private String comments;
	@SerializedName("Attachments") private String attachments;
	@SerializedName("Priority") private String priority;
	@SerializedName("AssignedTo") private String assignedTo;
	@SerializedName("StatusCd") private String statusCd;
	@SerializedName("StDesc") private String stDesc;
	@SerializedName("idCeco") private String idCeco;
	@SerializedName("subordinado") private boolean subordinado;
	@SerializedName("idOwner") private String idOwner;
	
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getDue() {
		return due;
	}
	public void setDue(String due) {
		this.due = due;
	}
	public String getpType() {
		return pType;
	}
	public void setpType(String pType) {
		this.pType = pType;
	}
	public String getpTypeDesc() {
		return pTypeDesc;
	}
	public void setpTypeDesc(String pTypeDesc) {
		this.pTypeDesc = pTypeDesc;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getAttachments() {
		return attachments;
	}
	public void setAttachments(String attachments) {
		this.attachments = attachments;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	public String getStatusCd() {
		return statusCd;
	}
	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}
	public String getStDesc() {
		return stDesc;
	}
	public void setStDesc(String stDesc) {
		this.stDesc = stDesc;
	}
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public boolean isSubordinado() {
		return subordinado;
	}
	public void setSubordinado(boolean subordinado) {
		this.subordinado = subordinado;
	}
	public String getIdOwner() {
		return idOwner;
	}
	public void setIdOwner(String idOwner) {
		this.idOwner = idOwner;
	}
}
