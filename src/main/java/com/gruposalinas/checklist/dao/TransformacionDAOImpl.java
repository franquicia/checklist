package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.TransformacionDTO;
import com.gruposalinas.checklist.mappers.TransformacionRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class TransformacionDAOImpl extends DefaultDAO implements TransformacionDAO {

    private static Logger logger = LogManager.getLogger(TransformacionDAOImpl.class);

    DefaultJdbcCall jdbcInsertaSoftOpen;
    DefaultJdbcCall jdbcInsertaSoftOpen2;
    DefaultJdbcCall jdbcInsertaManSoftOpen;
    DefaultJdbcCall jdbcEliminaSoftOpen;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneSoftOpen;
    DefaultJdbcCall jbdcActualizaSoftOpen;

    public void init() {

        jdbcInsertaSoftOpen = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADTRANSFORM")
                .withProcedureName("SP_INS_TRANSF");

        jdbcInsertaSoftOpen2 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADTRANSFORM")
                .withProcedureName("SP_INS_TRANSFMAN");

        jdbcEliminaSoftOpen = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADTRANSFORM")
                .withProcedureName("SP_DEL_TRANSF");

        jdbcObtieneSoftOpen = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADTRANSFORM")
                .withProcedureName("SP_SEL_CECO")
                .returningResultSet("RCL_SOFT", new TransformacionRowMapper());

        jbdcActualizaSoftOpen = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADTRANSFORM")
                .withProcedureName("SP_ACT_TRANSF");

    }

    public int inserta(TransformacionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bean.getBitacora());

        out = jdbcInsertaSoftOpen.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADTRANSFORM.SP_INS_TRANSF}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar registro de soft");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public int insertaMan(TransformacionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_USUA", bean.getIdUsuario())
                .addValue("PA_BITA", bean.getBitacora())
                .addValue("PA_RECORRIDO", bean.getRecorrido())
                .addValue("PA_FECHAINI", bean.getFechaini())
                .addValue("PA_FECHAFIN", bean.getFechafin())
                .addValue("PA_AUX", bean.getAux());

        out = jdbcInsertaSoftOpen.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADTRANSFORM.SP_INS_TRANSFMAN}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el soft manual");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean elimina(String ceco, String recorrido) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco)
                .addValue("PA_RECORRIDO", recorrido);

        out = jdbcEliminaSoftOpen.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADTRANSFORM.SP_DEL_TRANSF}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el soft del ceco: (" + ceco + ")");
        } else {
            return true;
        }

        return false;

    }

    //parametro
    @SuppressWarnings("unchecked")
    public List<TransformacionDTO> obtieneDatos(String ceco) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<TransformacionDTO> listaN = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco);

        out = jdbcObtieneSoftOpen.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADTRANSFORM.SP_SEL_CECO}");
        //lleva el nombre del cursor del procedure
        listaN = (List<TransformacionDTO>) out.get("RCL_SOFT");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener lod datos del ceco: (" + ceco + ")");
        }
        return listaN;

    }

    public boolean actualiza(TransformacionDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_USUA", bean.getIdUsuario())
                .addValue("PA_BITA", bean.getBitacora())
                .addValue("PA_RECORRIDO", bean.getRecorrido())
                .addValue("PA_FECHAINI", bean.getFechaini())
                .addValue("PA_FECHAFIN", bean.getFechafin())
                .addValue("PA_AUX", bean.getAux());

        out = jbdcActualizaSoftOpen.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADTRANSFORM.SP_ACT_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getCeco() + ")");
        } else {
            return true;
        }
        return false;

    }

}
