package com.gruposalinas.checklist.servicios.servidor;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.gruposalinas.checklist.business.AdmPregZonasBI;
import com.gruposalinas.checklist.business.AdmTipoZonaBI;
import com.gruposalinas.checklist.business.AdmZonasBI;
import com.gruposalinas.checklist.business.AppBI;
import com.gruposalinas.checklist.business.ArbolDecisionBI;
import com.gruposalinas.checklist.business.BitacoraBI;
import com.gruposalinas.checklist.business.ChecklistBI;
import com.gruposalinas.checklist.business.ChecklistPreguntaBI;
import com.gruposalinas.checklist.business.ChecklistProtocoloBI;
import com.gruposalinas.checklist.business.ChecklistTBI;
import com.gruposalinas.checklist.business.ChecklistUsuarioBI;
import com.gruposalinas.checklist.business.CompromisoBI;
import com.gruposalinas.checklist.business.EdoChecklistBI;
import com.gruposalinas.checklist.business.EvidenciaBI;
import com.gruposalinas.checklist.business.ModuloBI;
import com.gruposalinas.checklist.business.PlantillaBI;
import com.gruposalinas.checklist.business.PosiblesBI;
import com.gruposalinas.checklist.business.PreguntaBI;
import com.gruposalinas.checklist.business.PreguntaSupBI;
import com.gruposalinas.checklist.business.RespuestaBI;
import com.gruposalinas.checklist.business.TipoChecklistBI;
import com.gruposalinas.checklist.business.TipoPreguntaBI;
import com.gruposalinas.checklist.domain.AdmPregZonaDTO;
import com.gruposalinas.checklist.domain.AdmTipoZonaDTO;
import com.gruposalinas.checklist.domain.AdminZonaDTO;
import com.gruposalinas.checklist.domain.AppPerfilDTO;
import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.domain.ChecklistUsuarioDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.domain.EdoChecklistDTO;
import com.gruposalinas.checklist.domain.EvidenciaDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.PlantillaDTO;
import com.gruposalinas.checklist.domain.PosiblesDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.PreguntaSupDTO;
import com.gruposalinas.checklist.domain.RespuestaDTO;
import com.gruposalinas.checklist.domain.TipoChecklistDTO;
import com.gruposalinas.checklist.domain.TipoPreguntaDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;

@Controller
@RequestMapping("/checklistServices")
public class ModificaChecklistService {

	@Autowired
	ModuloBI modulobi;
	@Autowired
	TipoPreguntaBI tipPreguntabi;
	@Autowired
	PreguntaBI preguntabi;
	@Autowired
	TipoChecklistBI tipoChecklistbi;
	@Autowired
	ChecklistBI checklistbi;
	@Autowired
	ChecklistPreguntaBI checklistPreguntabi;
	@Autowired
	ChecklistUsuarioBI checklistUsuariobi;
	@Autowired
	EdoChecklistBI edoChecklistbi;
	@Autowired
	ArbolDecisionBI arbolDecisionbi;
	@Autowired
	BitacoraBI bitacorabi;
	@Autowired
	RespuestaBI respuestabi;
	@Autowired
	CompromisoBI compromisobi;
	@Autowired
	EvidenciaBI evidenciabi;
	// TEMPORALES
	@Autowired
	ChecklistTBI checkTbi;
	@Autowired
	PosiblesBI posiblesbi;
	@Autowired
	AppBI appBI;
	@Autowired
	PlantillaBI plantillaBI;
	
	@Autowired
	ChecklistProtocoloBI checklistProtocoloBI;
	@Autowired
	PreguntaSupBI preguntaSupBI;
	@Autowired
	AdmZonasBI admZonasBI;
	@Autowired
	AdmTipoZonaBI admTipoZonaBI;
	@Autowired
	AdmPregZonasBI admPregZonasBI;

	// http://localhost:8080/checklist/checklistServices/updateModulo.json?idModulo=<?>&idModuloPadre=<?>&nombreMod=<?>
	@RequestMapping(value = "/updateModulo", method = RequestMethod.GET)
	public ModelAndView updateModulo(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String idModulo = request.getParameter("idModulo");
		String idModuloPadre = request.getParameter("idModuloPadre");
		
		char[] ca = { '\u0061', '\u0301' };
		char[] ce = { '\u0065', '\u0301' };
		char[] ci = { '\u0069', '\u0301' };
		char[] co = { '\u006F', '\u0301' };
		char[] cu = { '\u0075', '\u0301' };
		// mayusculas
		char[] c1 = { '\u0041', '\u0301' };
		char[] c2 = { '\u0045', '\u0301' };
		char[] c3 = { '\u0049', '\u0301' };
		char[] c4 = { '\u004F', '\u0301' };
		char[] c5 = { '\u0055', '\u0301' };
		char[] c6 = { '\u006E', '\u0303' };

		String nombreModulo = request.getParameter("nombreMod");
		
		String nombreModuloCod="";
		//System.out.println("setPregunta1 " + nombreModulo);
		if (nombreModulo.contains(String.valueOf(ca)) || nombreModulo.contains(String.valueOf(ce))
				|| nombreModulo.contains(String.valueOf(ci)) || nombreModulo.contains(String.valueOf(co))
				|| nombreModulo.contains(String.valueOf(cu)) || nombreModulo.contains(String.valueOf(c1))
				|| nombreModulo.contains(String.valueOf(c2)) || nombreModulo.contains(String.valueOf(c3))
				|| nombreModulo.contains(String.valueOf(c4)) || nombreModulo.contains(String.valueOf(c5))
				|| nombreModulo.contains(String.valueOf(c6))) {

			String rr = nombreModulo.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
					.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
					.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
					.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
					.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
					.replaceAll(String.valueOf(c6), "ñ");
			//System.out.println("rr " + rr);

			nombreModuloCod=rr;
			rr = null;
		} else {
			//System.out.println("setPregunta " + nombreModulo);

			nombreModuloCod=nombreModulo;

		}

		ModuloDTO modulo = new ModuloDTO();
		modulo.setIdModuloPadre(Integer.parseInt(idModuloPadre));
		modulo.setNombre(nombreModuloCod);
		modulo.setIdModulo(Integer.parseInt(idModulo));
		boolean res = modulobi.actualizaModulo(modulo);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "MODULO MODIFICADO");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/checklistServices/updateTipoPregunta.json?idTipoPreg=<?>&cveTipoPreg=<?>&desTipoPreg=<?>&setActivo=<?>
	@RequestMapping(value = "/updateTipoPregunta", method = RequestMethod.GET)
	public ModelAndView updateTipoPregunta(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String cveTipoPreg = new String(request.getParameter("cveTipoPreg").getBytes("ISO-8859-1"), "UTF-8");
		String desTipoPreg = new String(request.getParameter("desTipoPreg").getBytes("ISO-8859-1"), "UTF-8");
		String setActivo = request.getParameter("setActivo");
		String idTipoPregunta = request.getParameter("idTipoPreg");

		TipoPreguntaDTO tipoPregunta = new TipoPreguntaDTO();
		tipoPregunta.setCvePregunta(cveTipoPreg);
		tipoPregunta.setDescripcion(desTipoPreg);
		tipoPregunta.setActivo(Integer.parseInt(setActivo));
		tipoPregunta.setIdTipoPregunta(Integer.parseInt(idTipoPregunta));
		boolean res = tipPreguntabi.actualizaTipoPregunta(tipoPregunta);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "TIPO_PREGUNTA MODIFICADO");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/checklistServices/updatePregunta.json?idPregunta=<?>&idMod=<?>&idTipo=<?>&estatus=<?>&setPregunta=<?>&detalle=<?>&commit=<?>
   // http://10.51.219.179:8080/checklist/checklistServices/updatePregunta.json?idPregunta=12636&idMod=130&idTipo=24&estatus=1&setPregunta=¿esta es una pregunta de prueba?&detalle=Sin detalle5&commit=
	@RequestMapping(value = "/updatePregunta", method = RequestMethod.GET)
	public ModelAndView updatePregunta(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		PreguntaDTO pregunta = new PreguntaDTO();
		String commit="1";
		String idMod = request.getParameter("idMod");
		String idTipo = request.getParameter("idTipo");
		String estatus = request.getParameter("estatus");
		//String setPregunta = new String(request.getParameter("setPregunta").getBytes("ISO-8859-1"), "UTF-8");
		String idPregunta = request.getParameter("idPregunta");
		 commit = request.getParameter("commit");
			char[] ca = { '\u0061', '\u0301' };
			char[] ce = { '\u0065', '\u0301' };
			char[] ci = { '\u0069', '\u0301' };
			char[] co = { '\u006F', '\u0301' };
			char[] cu = { '\u0075', '\u0301' };
			// mayusculas
			char[] c1 = { '\u0041', '\u0301' };
			char[] c2 = { '\u0045', '\u0301' };
			char[] c3 = { '\u0049', '\u0301' };
			char[] c4 = { '\u004F', '\u0301' };
			char[] c5 = { '\u0055', '\u0301' };
			char[] c6 = { '\u006E', '\u0303' };

			String setPregunta = request.getParameter("setPregunta");
			//System.out.println("setPregunta1 " + setPregunta);
			if (setPregunta.contains(String.valueOf(ca)) || setPregunta.contains(String.valueOf(ce))
					|| setPregunta.contains(String.valueOf(ci)) || setPregunta.contains(String.valueOf(co))
					|| setPregunta.contains(String.valueOf(cu)) || setPregunta.contains(String.valueOf(c1))
					|| setPregunta.contains(String.valueOf(c2)) || setPregunta.contains(String.valueOf(c3))
					|| setPregunta.contains(String.valueOf(c4)) || setPregunta.contains(String.valueOf(c5))
					|| setPregunta.contains(String.valueOf(c6))) {

				String rr = setPregunta.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
						.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
						.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
						.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
						.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
						.replaceAll(String.valueOf(c6), "ñ");
				//System.out.println("rr " + rr);

				pregunta.setPregunta(rr);
				rr = null;
			} else {
				//System.out.println("setPregunta " + setPregunta);

				pregunta.setPregunta(setPregunta);

			}

	
		pregunta.setIdModulo(Integer.parseInt(idMod));
		pregunta.setIdTipo(Integer.parseInt(idTipo));
		pregunta.setEstatus(Integer.parseInt(estatus));
		pregunta.setPregunta(setPregunta);
		pregunta.setIdPregunta(Integer.parseInt(idPregunta));
		pregunta.setCommit(Integer.parseInt(commit));

		boolean res = this.preguntabi.actualizaPregunta(pregunta);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "ID DE PREGUNTA ACTUALIZADO");
		mv.addObject("res", res);
		return mv;

	}

	// http://localhost:8080/checklist/checklistServices/updatePregunta.json?idPregunta=<?>&idMod=<?>&idTipo=<?>&estatus=<?>&setPregunta=<?>
	@RequestMapping(value = "/updatePreguntaService", method = RequestMethod.GET)
	public @ResponseBody boolean updatePreguntaService(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String idMod = request.getParameter("idMod");
		String idTipo = request.getParameter("idTipo");
		String estatus = request.getParameter("estatus");
		String setPregunta = new String(request.getParameter("setPregunta").getBytes("ISO-8859-1"), "UTF-8");
		String idPregunta = request.getParameter("idPregunta");

		PreguntaDTO pregunta = new PreguntaDTO();
		pregunta.setIdModulo(Integer.parseInt(idMod));
		pregunta.setIdTipo(Integer.parseInt(idTipo));
		pregunta.setEstatus(Integer.parseInt(estatus));
		pregunta.setPregunta(setPregunta);
		pregunta.setIdPregunta(Integer.parseInt(idPregunta));
		boolean res = this.preguntabi.actualizaPregunta(pregunta);

		return res;

	}

	// http://localhost:8080/checklist/checklistServices/updateTipoCheck.json?idTipoCheck=<?>&descTipoCheck=<?>
	@RequestMapping(value = "/updateTipoCheck", method = RequestMethod.GET)
	public ModelAndView updateTipoChecklist(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String descTipoCheck = new String(request.getParameter("descTipoCheck").getBytes("ISO-8859-1"), "UTF-8");
		String idTipoChck = request.getParameter("idTipoCheck");

		TipoChecklistDTO tipoChecklist = new TipoChecklistDTO();
		tipoChecklist.setDescTipo(descTipoCheck);
		tipoChecklist.setIdTipoCheck(Integer.parseInt(idTipoChck));
		boolean res = tipoChecklistbi.actualizaTipoChecklist(tipoChecklist);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "TIPO CHECKLIST MODIFICADO");
		mv.addObject("res", res);
		return mv;

	}

	// http://localhost:8080/checklist/checklistServices/updateEstadoChecklist.json?idEdoCheck=<?>&descripcion=<?>
	@RequestMapping(value = "/updateEstadoChecklist", method = RequestMethod.GET)
	public ModelAndView updateEstadoChecklist(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
		String idEdoChck = request.getParameter("idEdoCheck");

		EdoChecklistDTO edoCheck = new EdoChecklistDTO();
		edoCheck.setDescripcion(descripcion);
		edoCheck.setIdEdochecklist(Integer.parseInt(idEdoChck));
		boolean res = edoChecklistbi.actualizaEdoChecklist(edoCheck);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "ESTADO CHECKLIST ACTUALIZADO");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/checklistServices/updateCheckService.json?idChecklist=<?>&fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>&idUsuario=<?>&periodicidad=<?>&dia=<?>&version=<?>&ordenGrupo=<?>
	@RequestMapping(value = "/updateCheckService", method = RequestMethod.GET)
	public @ResponseBody boolean updateCheckService(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String fechaIni = request.getParameter("fechaIni");
		String fechaFin = request.getParameter("fechaFin");
		String idEstado = request.getParameter("idEstado");
		String idHorario = request.getParameter("idHorario");
		String idTipoCheck = request.getParameter("idTipoCheck");
		String nombreCheck = new String(request.getParameter("nombreCheck").getBytes("ISO-8859-1"), "UTF-8");
		String setVigente = request.getParameter("setVigente");
		String idChck = request.getParameter("idChecklist");
		String idUsuario = request.getParameter("idUsuario");
		String periodicidad = request.getParameter("periodicidad");
		String version = request.getParameter("version");
		String ordenGrupo = request.getParameter("ordenGrupo");
		String dia = request.getParameter("dia");

		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario = userSession.getIdUsuario();

		ChecklistDTO checklist = new ChecklistDTO();
		checklist.setFecha_inicio(fechaIni);
		checklist.setFecha_fin(fechaFin);
		checklist.setIdEstado(Integer.parseInt(idEstado));
		checklist.setIdHorario(Integer.parseInt(idHorario));
		TipoChecklistDTO chk = new TipoChecklistDTO();
		chk.setIdTipoCheck(Integer.parseInt(idTipoCheck));
		checklist.setIdTipoChecklist(chk);
		checklist.setNombreCheck(nombreCheck);
		checklist.setVigente(Integer.parseInt(setVigente));
		// Obtengo el ID del checklist
		checklist.setIdChecklist(Integer.parseInt(idChck));
		// SE AGREGA USUARIO RESPONSABLE, DIA Y PERIDO
		checklist.setIdUsuario(Integer.parseInt(idUsuario));
		checklist.setPeriodicidad(periodicidad);
		checklist.setVersion(version);
		checklist.setOrdeGrupo(ordenGrupo);
		checklist.setDia(dia);
		boolean res = checklistbi.actualizaChecklist(checklist);

		return res;

	}

	// http://localhost:8080/checklist/checklistServices/updateCheck.json?idChecklist=<?>&fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>
	// FORMATO FECHA INI Y FECHA FIN (AAAAMMDD)
	@RequestMapping(value = "/updateCheck", method = RequestMethod.GET)
	public ModelAndView updateCheck(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String fechaIni = request.getParameter("fechaIni");
		String fechaFin = request.getParameter("fechaFin");
		String idEstado = request.getParameter("idEstado");
		String idHorario = request.getParameter("idHorario");
		String idTipoCheck = request.getParameter("idTipoCheck");
		String nombreCheck = new String(request.getParameter("nombreCheck").getBytes("ISO-8859-1"), "UTF-8");
		String setVigente = request.getParameter("setVigente");
		String idChck = request.getParameter("idChecklist");

		ChecklistDTO checklist = new ChecklistDTO();
		checklist.setFecha_inicio(fechaIni);
		checklist.setFecha_fin(fechaFin);
		checklist.setIdEstado(Integer.parseInt(idEstado));
		checklist.setIdHorario(Integer.parseInt(idHorario));
		TipoChecklistDTO chk = new TipoChecklistDTO();
		chk.setIdTipoCheck(Integer.parseInt(idTipoCheck));
		checklist.setIdTipoChecklist(chk);
		checklist.setNombreCheck(nombreCheck);
		checklist.setVigente(Integer.parseInt(setVigente));
		// Obtengo el ID del checklist
		checklist.setIdChecklist(Integer.parseInt(idChck));
		boolean res = checklistbi.actualizaChecklist(checklist);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "CHECKLIST MODIFICADO");
		mv.addObject("res", res);
		return mv;

	}

	// http://localhost:8080/checklist/checklistServices/updatePregCheck.json?idPreg=<?>&idCheck=<?>&ordenPreg=<?>&pregPadre=<?>
	@RequestMapping(value = "/updatePregCheck", method = RequestMethod.GET)
	public ModelAndView updatePreguntaCheck(HttpServletRequest request, HttpServletResponse response) {
		String idCheck = request.getParameter("idCheck");
		String ordenPreg = request.getParameter("ordenPreg");
		String idPadre = request.getParameter("pregPadre");
		String idPreg = request.getParameter("idPreg");

		ChecklistPreguntaDTO pregDTO = new ChecklistPreguntaDTO();
		pregDTO.setIdChecklist(Integer.parseInt(idCheck));
		pregDTO.setOrdenPregunta(Integer.parseInt(ordenPreg));
		pregDTO.setPregPadre(Integer.parseInt(idPadre));
		pregDTO.setIdPregunta(Integer.parseInt(idPreg));
		boolean res = checklistPreguntabi.actualizaChecklistPregunta(pregDTO);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "PREGUNTA CHECKLIST MODIFICADA");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/checklistServices/updatePregCheckService.json?idPreg=<?>&idCheck=<?>&ordenPreg=<?>&pregPadre=<?>
	@RequestMapping(value = "/updatePregCheckService", method = RequestMethod.GET)
	public @ResponseBody boolean updatePreguntaCheckService(HttpServletRequest request, HttpServletResponse response) {
		String idCheck = request.getParameter("idCheck");
		String ordenPreg = request.getParameter("ordenPreg");
		String idPadre = request.getParameter("pregPadre");
		String idPreg = request.getParameter("idPreg");

		ChecklistPreguntaDTO pregDTO = new ChecklistPreguntaDTO();
		pregDTO.setIdChecklist(Integer.parseInt(idCheck));
		pregDTO.setOrdenPregunta(Integer.parseInt(ordenPreg));
		pregDTO.setPregPadre(Integer.parseInt(idPadre));
		pregDTO.setIdPregunta(Integer.parseInt(idPreg));
		boolean res = checklistPreguntabi.actualizaChecklistPregunta(pregDTO);

		return res;
	}

	/* AGREGUE IDCHECKUSUARIO (ID) */
	// http://localhost:8080/checklist/checklistServices/updateUsuCheck.json?idCheckUsuario=<?>&setActivo=<?>&fechaIni=<?>&fechaResp=<?>&idCeco=<?>&idChecklist=<?>&idUsu=<?>
	@RequestMapping(value = "/updateUsuCheck", method = RequestMethod.GET)
	public ModelAndView updateUsuarioCheck(HttpServletRequest request, HttpServletResponse response) {
		String setActivo = request.getParameter("setActivo");
		String fechaIni = request.getParameter("fechaIni");
		String fechaResp = request.getParameter("fechaResp");
		String idCeco = request.getParameter("idCeco");
		String idChecklist = request.getParameter("idChecklist");
		String idUsu = request.getParameter("idUsu");
		String idCheckUsuario = request.getParameter("idCheckUsuario");

		ChecklistUsuarioDTO checkUsu = new ChecklistUsuarioDTO();
		checkUsu.setActivo(Integer.parseInt(setActivo));
		checkUsu.setFechaIni(fechaIni);
		checkUsu.setFechaResp(fechaResp);
		checkUsu.setIdCeco(Integer.parseInt(idCeco));
		checkUsu.setIdChecklist(Integer.parseInt(idChecklist));
		checkUsu.setIdUsuario(Integer.parseInt(idUsu));
		checkUsu.setIdCheckUsuario(Integer.parseInt(idCheckUsuario));
		boolean res = checklistUsuariobi.actualizaChecklistUsuario(checkUsu);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "ID CHECKLIST MODIFICADO");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/checklistServices/descativaCheckUsua.json?idCeco=<?>&idChecklist=<?>
	@RequestMapping(value = "/descativaCheckUsua", method = RequestMethod.GET)
	public ModelAndView descativaCheckUsua(HttpServletRequest request, HttpServletResponse response) {

		String idCeco = request.getParameter("idCeco");
		String idChecklist = request.getParameter("idChecklist");

		boolean res = checklistUsuariobi.desactivaCheckUsua(Integer.parseInt(idCeco), Integer.parseInt(idChecklist));

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "ID CHECKLIST USUARIO DESACTIVADO");
		mv.addObject("res", res);
		return mv;
	}

	/* AGREGUE IDSETARBOLDESICION (ID) */
	// http://10.51.219.179:8080/checklist/checklistServices/updateArbolDesicion.json?idArbolDesicion=4845&estatusEvidencia=1&idCheck=1024&idPreg=12451&idOrdenCheckRes=1&setRes=0&reqAccion=0&reqObs=0&reqOblig=1&etiquetaEvidencia=1&ponderacion=1&idPlantilla=1&idprotocolo=10
	@RequestMapping(value = "/updateArbolDesicion", method = RequestMethod.GET)
	public ModelAndView updateArbolDecision(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String estatusEvidencia = request.getParameter("estatusEvidencia");
		String idCheck = request.getParameter("idCheck");
		String idPreg = request.getParameter("idPreg");
		String idOrdenCheckRes = request.getParameter("idOrdenCheckRes");
		String setRes = new String(request.getParameter("setRes").getBytes("ISO-8859-1"), "UTF-8");
		String idArbolDesicion = request.getParameter("idArbolDesicion");
		String reqAccion = request.getParameter("reqAccion");
		String reqObs = request.getParameter("reqObs");
		String reqOblig = request.getParameter("reqOblig");
		String etiquetaEvi = request.getParameter("etiquetaEvidencia");
		String ponderacion = request.getParameter("ponderacion");
		String idPlantilla = request.getParameter("idPlantilla");
		String idProtocolo = request.getParameter("idProtocolo");
		
		int idProtocoloI=0;
		if(idProtocolo!=null) {
			idProtocoloI=Integer.parseInt(idProtocolo);
		}

		ArbolDecisionDTO arbol = new ArbolDecisionDTO();
		arbol.setEstatusEvidencia(Integer.parseInt(estatusEvidencia));
		arbol.setIdCheckList(Integer.parseInt(idCheck));
		arbol.setIdPregunta(Integer.parseInt(idPreg));
		arbol.setOrdenCheckRespuesta(Integer.parseInt(idOrdenCheckRes));
		arbol.setRespuesta(setRes);
		arbol.setIdArbolDesicion(Integer.parseInt(idArbolDesicion));
		arbol.setReqAccion(Integer.parseInt(reqAccion));
		arbol.setReqObservacion(Integer.parseInt(reqObs));
		arbol.setReqOblig(Integer.parseInt(reqOblig));
		arbol.setDescEvidencia(etiquetaEvi);
		arbol.setPonderacion(ponderacion);
		arbol.setIdPlantilla(Integer.parseInt(idPlantilla));
		arbol.setIdProtocolo(idProtocoloI);

		boolean res = arbolDecisionbi.actualizaArbolDecision(arbol);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "ARBOLR CREADO CON EXITO--->");
		mv.addObject("res", res);
		return mv;
	}

	/* AGREGUE IDSETARBOLDESICION (ID) */
	// http://localhost:8080/checklist/checklistServices/updateArbolDesicion.json?idArbolDesicion=<?>&estatusEvidencia=<?>&idCheck=<?>&idPreg=<?>&idOrdenCheckRes=<?>&setRes=<?>&reqAccion=<?>&reqObs=<?>&reqOblig=<?>&etiquetaEvidencia=<?>
	@RequestMapping(value = "/updateArbolDesicionService", method = RequestMethod.GET)
	public @ResponseBody boolean updateArbolDecisionService(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String estatusEvidencia = request.getParameter("estatusEvidencia");
		String idCheck = request.getParameter("idCheck");
		String idPreg = request.getParameter("idPreg");
		String idOrdenCheckRes = request.getParameter("idOrdenCheckRes");
		String setRes = new String(request.getParameter("setRes").getBytes("ISO-8859-1"), "UTF-8");
		String idArbolDesicion = request.getParameter("idArbolDesicion");
		String reqAccion = request.getParameter("reqAccion");
		String reqObs = request.getParameter("reqObs");
		String reqOblig = request.getParameter("reqOblig");
		String etiquetaEvi = request.getParameter("etiquetaEvidencia");

		ArbolDecisionDTO arbol = new ArbolDecisionDTO();
		arbol.setEstatusEvidencia(Integer.parseInt(estatusEvidencia));
		arbol.setIdCheckList(Integer.parseInt(idCheck));
		arbol.setIdPregunta(Integer.parseInt(idPreg));
		arbol.setOrdenCheckRespuesta(Integer.parseInt(idOrdenCheckRes));
		arbol.setRespuesta(setRes);
		arbol.setIdArbolDesicion(Integer.parseInt(idArbolDesicion));
		arbol.setReqAccion(Integer.parseInt(reqAccion));
		arbol.setReqObservacion(Integer.parseInt(reqObs));
		arbol.setReqOblig(Integer.parseInt(reqOblig));
		arbol.setDescEvidencia(etiquetaEvi);
		boolean res = arbolDecisionbi.actualizaArbolDecision(arbol);

		return res;
	}

	// http://localhost:8080/checklist/checklistServices/updateBitacora.json?idBitacora=<?>&fechaIni=<?>&fechaFin=<?>&idCheckUsua=<?>&latitud=<?>&longitud=<?>
	@RequestMapping(value = "/updateBitacora", method = RequestMethod.GET)
	public ModelAndView updateBitacora(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String fechaInicio = request.getParameter("fechaIni");
		String fechaFin = request.getParameter("fechaFin");
		String idCheckUsua = request.getParameter("idCheckUsua");
		String latitud = request.getParameter("latitud");
		String longitud = request.getParameter("longitud");
		String idBit = request.getParameter("idBitacora");

		BitacoraDTO bitacora = new BitacoraDTO();
		bitacora.setFechaInicio(fechaInicio);
		bitacora.setFechaFin(fechaFin);
		bitacora.setIdCheckUsua(Integer.parseInt(idCheckUsua));
		bitacora.setLatitud(latitud);
		bitacora.setLongitud(longitud);
		bitacora.setIdBitacora(Integer.parseInt(idBit));
		boolean res = bitacorabi.actualizaBitacora(bitacora);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "BITACORA MODIFICADA");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/checklistServices/updateTBitacora.json
	@RequestMapping(value = "/updateTBitacora", method = RequestMethod.GET)
	public ModelAndView updateTBitacora(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		boolean res = bitacorabi.cierraBitacora();

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "BITACORAS TERMINADAS");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/checklistServices/updateRespuesta.json?idRespuesta=<?>&idArbolDesicion=<?>&idBitacora=<?>&commit=<?>&observaciones=<?>
	@RequestMapping(value = "/updateRespuesta", method = RequestMethod.GET)
	public ModelAndView updateRespuesta(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String idArbolDesicion = request.getParameter("idArbolDesicion");
		String idBitacora = request.getParameter("idBitacora");
		String commit = request.getParameter("commit");
		String idRes = request.getParameter("idRespuesta");
		String observaciones = new String(request.getParameter("observaciones").getBytes("ISO-8859-1"), "UTF-8");

		RespuestaDTO respuesta = new RespuestaDTO();
		respuesta.setIdArboldecision(Integer.parseInt(idArbolDesicion));
		respuesta.setIdBitacora(Integer.parseInt(idBitacora));
		respuesta.setCommit(Integer.parseInt(commit));
		respuesta.setIdRespuesta(Integer.parseInt(idRes));
		respuesta.setObservacion(observaciones);
		boolean res = respuestabi.actualizaRespuesta(respuesta);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "RESPUESTA MODIFICADA");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/checklistServices/updateFechaResp.json?idRespuesta=<?>&fecha=<?>&commit=<?>
	@RequestMapping(value = "/updateFechaResp", method = RequestMethod.GET)
	public ModelAndView updateFechaResp(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String idRespuesta = request.getParameter("idRespuesta");
		String fechaTermino = request.getParameter("fecha");
		String commit = request.getParameter("commit");

		RespuestaDTO respuesta = new RespuestaDTO();
		respuesta.setIdRespuesta(Integer.parseInt(idRespuesta));
		respuesta.setFechaModificacion(fechaTermino);
		respuesta.setCommit(Integer.parseInt(commit));

		boolean res = respuestabi.actualizaFechaResp(idRespuesta, fechaTermino, commit);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "RESPUESTA MODIFICADA");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/checklistServices/updateCompromiso.json?idCompromiso=<?>&commit=<?>&descripcion=<?>&estatus=<?>&fechaCompromiso=<?>&idRespuesta=<?>&idResponsable=<?>
	@RequestMapping(value = "/updateCompromiso", method = RequestMethod.GET)
	public ModelAndView updateCompromiso(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String commit = request.getParameter("commit");
		String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
		String estatus = request.getParameter("estatus");
		String fechaCompromiso = request.getParameter("fechaCompromiso");
		String idRespuesta = request.getParameter("idRespuesta");
		String idCompromiso = request.getParameter("idCompromiso");
		String idResponsable = request.getParameter("idResponsable");

		CompromisoDTO compromiso = new CompromisoDTO();
		compromiso.setCommit(Integer.parseInt(commit));
		compromiso.setDescripcion(descripcion);
		compromiso.setEstatus(Integer.parseInt(estatus));
		compromiso.setFechaCompromiso(fechaCompromiso);
		compromiso.setIdRespuesta(Integer.parseInt(idRespuesta));
		compromiso.setIdCompromiso(Integer.parseInt(idCompromiso));
		compromiso.setIdResponsable(Integer.parseInt(idResponsable));
		boolean res = compromisobi.actualizaCompromiso(compromiso);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "COMPROMISO MODIFICADO");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/checklistServices/updateEvidencia.json?idEvidencia=<?>&commit=<?>&idRespuesta=<?>&idTipo=<?>&ruta=<?>
	@RequestMapping(value = "/updateEvidencia", method = RequestMethod.GET)
	public ModelAndView updateEvidencia(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String commit = request.getParameter("commit");
		String idRespuesta = request.getParameter("idRespuesta");
		String idTipo = request.getParameter("idTipo");
		String ruta = request.getParameter("ruta");
		String idEvidencia = request.getParameter("idEvidencia");

		EvidenciaDTO evidencia = new EvidenciaDTO();
		evidencia.setCommit(Integer.parseInt(commit));
		evidencia.setIdRespuesta(Integer.parseInt(idRespuesta));
		evidencia.setIdTipo(Integer.parseInt(idTipo));
		evidencia.setRuta(ruta);
		evidencia.setIdEvidencia(Integer.parseInt(idEvidencia));
		boolean idEvi = evidenciabi.actualizaEvidencia(evidencia);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "EVIDENCIA MODIFICADA");
		mv.addObject("res", idEvi);
		return mv;
	}

	// Servicio para modificar el estado del checklist
	// http://localhost:8080/checklist/checklistServices/updateEstadoCheck.json?idCheck=<?>&estado=<?>

	@RequestMapping(value = "/updateEstadoCheck", method = RequestMethod.GET)
	public @ResponseBody boolean updateEstadoCheck(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String idCheck = request.getParameter("idCheck");
		String estado = request.getParameter("estado");

		// Llamo al bi para hacer la actualizacion
		boolean res = checkTbi.actualizaEdo(Integer.parseInt(idCheck), Integer.parseInt(estado));

		return res;
	}

	// Servicio para modificar posible respuesta
	// http://localhost:8080/checklist/checklistServices/updatePosible.json?idPosible=<?>&respuesta=<?>

	@RequestMapping(value = "/updatePosible", method = RequestMethod.GET)
	public @ResponseBody boolean updatePosible(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String idPosible = request.getParameter("idPosible");
		String respuesta = request.getParameter("respuesta");

		PosiblesDTO posible = new PosiblesDTO();
		posible.setIdPosible(Integer.parseInt(idPosible));
		posible.setDescripcion(respuesta);

		// Llamo al bi para hacer la actualizacion
		boolean res = posiblesbi.actualizaPosible(posible);

		return res;
	}

	// http://localhost:8080/checklist/checklistServices/updateApp.json?descripcion=<?>&commit=<?>&idApp=<?>
	@RequestMapping(value = "/updateApp", method = RequestMethod.GET)
	public ModelAndView updateApp(HttpServletRequest request, HttpServletResponse response) {
		try {
			String descripcion = request.getParameter("descripcion");
			String commit = request.getParameter("commit");
			String idApp = request.getParameter("idApp");

			AppPerfilDTO app = new AppPerfilDTO();
			app.setCommit(Integer.parseInt(commit));
			app.setIdApp(Integer.parseInt(idApp));
			app.setDescripcion(descripcion);

			boolean respuesta = appBI.actualizaApp(app);

			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

			mv.addObject("res", respuesta);
			return mv;
		} catch (Exception e) {
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/updateAppPerfil.json?idApp=<?>&idUsuario=<?>&commit=<?>&idAppPerfil=<?>
	@RequestMapping(value = "/updateAppPerfil", method = RequestMethod.GET)
	public ModelAndView updateAppPerfil(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idApp = request.getParameter("idApp");
			String idUsuario = request.getParameter("idUsuario");
			String idAppPerfil = request.getParameter("idAppPerfil");
			String commit = request.getParameter("commit");

			AppPerfilDTO app = new AppPerfilDTO();
			app.setCommit(Integer.parseInt(commit));
			app.setIdApp(Integer.parseInt(idApp));
			app.setIdUsuario(Integer.parseInt(idUsuario));
			app.setIdAppPerfil(Integer.parseInt(idAppPerfil));

			boolean respuesta = appBI.actualizaAppPerfil(app);

			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

			mv.addObject("res", respuesta);
			return mv;
		} catch (Exception e) {
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/updatePlantilla.json?descripcion=<?>&idPlantilla=<?>
	@RequestMapping(value = "/updatePlantilla", method = RequestMethod.GET)
	public ModelAndView updatePlantilla(HttpServletRequest request, HttpServletResponse response) {
		try {
			String descripcion = request.getParameter("descripcion");
			String idPlantilla = request.getParameter("idPlantilla");

			PlantillaDTO plantilla = new PlantillaDTO();
			plantilla.setIdPlantilla(Integer.parseInt(idPlantilla));
			plantilla.setDescripcion(descripcion);

			boolean respuesta = plantillaBI.actualizaPlantilla(plantilla);

			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

			mv.addObject("res", respuesta);
			return mv;
		} catch (Exception e) {
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/updateElemento.json?etiqueta=<?>&orden=<?>&obligatorio=<?>&idPlantilla=<?>&tipo=<?>&idElemento=<?>
	@RequestMapping(value = "/updateElemento", method = RequestMethod.GET)
	public ModelAndView updateElemento(HttpServletRequest request, HttpServletResponse response) {
		try {
			String etiqueta = request.getParameter("etiqueta");
			String orden = request.getParameter("orden");
			String obligatorio = request.getParameter("obligatorio");
			String idPlantilla = request.getParameter("idPlantilla");
			String tipo = request.getParameter("tipo");
			String idElemento = request.getParameter("idElemento");

			PlantillaDTO plantilla = new PlantillaDTO();
			plantilla.setIdPlantilla(Integer.parseInt(idPlantilla));
			plantilla.setEtiqueta(etiqueta);
			plantilla.setOrden(Integer.parseInt(orden));
			plantilla.setObligatorio(Integer.parseInt(obligatorio));
			plantilla.setIdArchivo(Integer.parseInt(tipo));
			plantilla.setIdElemento(Integer.parseInt(idElemento));

			boolean respuesta = plantillaBI.actualizaElementoP(plantilla);

			ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

			mv.addObject("res", respuesta);
			return mv;
		} catch (Exception e) {
			return null;
		}
	}

	// http://localhost:8080/checklist/checklistServices/updatePreguntaServiceCom.json?idPregunta=<?>&idMod=<?>&idTipo=<?>&estatus=<?>&setPregunta=<?>&critica=<?>&sla=<?>
	//http://10.51.219.179:8080/checklist/checklistServices/updatePreguntaServiceCom.json?idPregunta=12636&idMod=130&idTipo=24&estatus=1&setPregunta=¿esta es una pregunta de prueba?&detalle=SIN DETALLE&critica=1&sla=1&area=
	@RequestMapping(value = "/updatePreguntaServiceCom", method = RequestMethod.GET)
	public @ResponseBody boolean updatePreguntaServiceCom(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		PreguntaDTO pregunta = new PreguntaDTO();
		String idMod = request.getParameter("idMod");
		String idTipo = request.getParameter("idTipo");
		String estatus = request.getParameter("estatus");
		//String setPregunta = new String(request.getParameter("setPregunta").getBytes("ISO-8859-1"), "UTF-8");
		String idPregunta = request.getParameter("idPregunta");
		String detalle = request.getParameter("detalle");
		int critica = Integer.parseInt(request.getParameter("critica"));
		char[] ca = { '\u0061', '\u0301' };
		char[] ce = { '\u0065', '\u0301' };
		char[] ci = { '\u0069', '\u0301' };
		char[] co = { '\u006F', '\u0301' };
		char[] cu = { '\u0075', '\u0301' };
		// mayusculas
		char[] c1 = { '\u0041', '\u0301' };
		char[] c2 = { '\u0045', '\u0301' };
		char[] c3 = { '\u0049', '\u0301' };
		char[] c4 = { '\u004F', '\u0301' };
		char[] c5 = { '\u0055', '\u0301' };
		char[] c6 = { '\u006E', '\u0303' };

		String setPregunta = request.getParameter("setPregunta");
		//System.out.println("setPregunta1 " + setPregunta);
		if (setPregunta.contains(String.valueOf(ca)) || setPregunta.contains(String.valueOf(ce))
				|| setPregunta.contains(String.valueOf(ci)) || setPregunta.contains(String.valueOf(co))
				|| setPregunta.contains(String.valueOf(cu)) || setPregunta.contains(String.valueOf(c1))
				|| setPregunta.contains(String.valueOf(c2)) || setPregunta.contains(String.valueOf(c3))
				|| setPregunta.contains(String.valueOf(c4)) || setPregunta.contains(String.valueOf(c5))
				|| setPregunta.contains(String.valueOf(c6))) {

			String rr = setPregunta.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
					.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
					.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
					.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
					.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
					.replaceAll(String.valueOf(c6), "ñ");
			//System.out.println("rr " + rr);

			pregunta.setPregunta(rr);
			rr = null;
		} else {
			//System.out.println("setPregunta " + setPregunta);

			pregunta.setPregunta(setPregunta);

		}
		
		if(detalle.contains(String.valueOf(ca)) 
                ||detalle.contains(String.valueOf(ce))
                ||detalle.contains(String.valueOf(ci))
                ||detalle.contains(String.valueOf(co))
                ||detalle.contains(String.valueOf(cu))
                ||detalle.contains(String.valueOf(c1))
                ||detalle.contains(String.valueOf(c2))
                ||detalle.contains(String.valueOf(c3))
                ||detalle.contains(String.valueOf(c4))
                ||detalle.contains(String.valueOf(c5))
                ||detalle.contains(String.valueOf(c6))){

    String rr=detalle.replaceAll(String.valueOf(ca), "á")
                           .replaceAll(String.valueOf(ce), "é")
                           .replaceAll(String.valueOf(ci), "í")
                           .replaceAll(String.valueOf(co), "ó")
                           .replaceAll(String.valueOf(cu), "ú")
                           .replaceAll(String.valueOf(c1), "Á")
                           .replaceAll(String.valueOf(c2), "É")
                           .replaceAll(String.valueOf(c3), "Í")
                           .replaceAll(String.valueOf(c4), "Ó")
                           .replaceAll(String.valueOf(c5), "Ú")
                           .replaceAll(String.valueOf(c6), "ñ")
                           ;
               

		    pregunta.setDetalle(rr);
		    rr=null;
		}else{
		
			pregunta.setDetalle(detalle);
		
		}

		String sla=request.getParameter("sla");
		//String area="";
		//area=request.getParameter("area");
		pregunta.setIdModulo(Integer.parseInt(idMod));
		pregunta.setIdTipo(Integer.parseInt(idTipo));
		pregunta.setEstatus(Integer.parseInt(estatus));
		pregunta.setPregunta(setPregunta);
		pregunta.setIdPregunta(Integer.parseInt(idPregunta));
		//pregunta.setDetalle(detalle);
		pregunta.setCritica(critica);
		pregunta.setSla(sla);
		//pregunta.setArea(area);
		boolean res = this.preguntabi.actualizaPreguntaCom(pregunta);

		return res;

	}

	// http://localhost:8080/checklist/checklistServices/updateCheckServiceCom.json?idChecklist=<?>&fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>&idUsuario=<?>&periodicidad=<?>&dia=<?>&version=<?>&ordenGrupo=<?>&ponderacionTot=<?>&clasifica=<?>&idProtocolo=<?>
	@RequestMapping(value = "/updateCheckServiceCom", method = RequestMethod.GET)
	public @ResponseBody boolean updateCheckServiceCom(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		ChecklistDTO checklist = new ChecklistDTO();
		String fechaIni = request.getParameter("fechaIni");
		String fechaFin = request.getParameter("fechaFin");
		String idEstado = request.getParameter("idEstado");
		String idHorario = request.getParameter("idHorario");
		String idTipoCheck = request.getParameter("idTipoCheck");
		
		//String nombreCheck = new String(request.getParameter("nombreCheck").getBytes("ISO-8859-1"), "UTF-8");
		char[] ca = { '\u0061', '\u0301' };
		char[] ce = { '\u0065', '\u0301' };
		char[] ci = { '\u0069', '\u0301' };
		char[] co = { '\u006F', '\u0301' };
		char[] cu = { '\u0075', '\u0301' };
		// mayusculas
		char[] c1 = { '\u0041', '\u0301' };
		char[] c2 = { '\u0045', '\u0301' };
		char[] c3 = { '\u0049', '\u0301' };
		char[] c4 = { '\u004F', '\u0301' };
		char[] c5 = { '\u0055', '\u0301' };
		char[] c6 = { '\u006E', '\u0303' };

		String nombreCheck = request.getParameter("nombreCheck");
		//System.out.println("nombreCheck1 " + nombreCheck);
		if (nombreCheck.contains(String.valueOf(ca)) || nombreCheck.contains(String.valueOf(ce))
				|| nombreCheck.contains(String.valueOf(ci)) || nombreCheck.contains(String.valueOf(co))
				|| nombreCheck.contains(String.valueOf(cu)) || nombreCheck.contains(String.valueOf(c1))
				|| nombreCheck.contains(String.valueOf(c2)) || nombreCheck.contains(String.valueOf(c3))
				|| nombreCheck.contains(String.valueOf(c4)) || nombreCheck.contains(String.valueOf(c5))
				|| nombreCheck.contains(String.valueOf(c6))) {

			String rr = nombreCheck.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
					.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
					.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
					.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
					.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
					.replaceAll(String.valueOf(c6), "ñ");
			//System.out.println("rr " + rr);

			checklist.setNombreCheck(rr);
			rr = null;
		} else {
			//System.out.println("nombreCheck " + nombreCheck);

			checklist.setNombreCheck(nombreCheck);

		}
		
		String setVigente = request.getParameter("setVigente");
		String idChck = request.getParameter("idChecklist");
		String idUsuario = request.getParameter("idUsuario");
		String periodicidad = request.getParameter("periodicidad");
		String version = request.getParameter("version");
		String ordenGrupo = request.getParameter("ordenGrupo");
		String dia = request.getParameter("dia");
		String clasifica=request.getParameter("clasifica");
		String nego=request.getParameter("nego");
		double ponderacionTot = Double.parseDouble(request.getParameter("ponderacionTot"));

		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario = userSession.getIdUsuario();

		
		checklist.setFecha_inicio(fechaIni);
		checklist.setFecha_fin(fechaFin);
		checklist.setIdEstado(Integer.parseInt(idEstado));
		checklist.setIdHorario(Integer.parseInt(idHorario));
		TipoChecklistDTO chk = new TipoChecklistDTO();
		chk.setIdTipoCheck(Integer.parseInt(idTipoCheck));
		checklist.setIdTipoChecklist(chk);
		checklist.setNombreCheck(nombreCheck);
		checklist.setVigente(Integer.parseInt(setVigente));
		// Obtengo el ID del checklist
		checklist.setIdChecklist(Integer.parseInt(idChck));
		// SE AGREGA USUARIO RESPONSABLE, DIA Y PERIDO
		checklist.setIdUsuario(Integer.parseInt(idUsuario));
		checklist.setPeriodicidad(periodicidad);
		checklist.setVersion(version);
		checklist.setOrdeGrupo(ordenGrupo);
		checklist.setDia(dia);
		checklist.setPonderacionTot(ponderacionTot);
		checklist.setClasifica(clasifica);
		checklist.setNego(nego);
		boolean res = checklistbi.actualizaChecklistCom(checklist);

		return res;

	}
	
	// http://localhost:8080/checklist/checklistServices/updateCheckNew.json?idChecklist=<?>&fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>&idUsuario=<?>&periodicidad=<?>&dia=<?>&version=<?>&ordenGrupo=<?>&ponderacionTot=<?>&clasifica=<?>&idProtocolo=<?>
		@RequestMapping(value = "/updateCheckNew", method = RequestMethod.GET)
		public @ResponseBody boolean updateCheckNew(HttpServletRequest request, HttpServletResponse response)
				throws UnsupportedEncodingException {
			ChecklistDTO checklist = new ChecklistDTO();
			String fechaIni = request.getParameter("fechaIni");
			String fechaFin = request.getParameter("fechaFin");
			String idEstado = request.getParameter("idEstado");
			String idHorario = request.getParameter("idHorario");
			String idTipoCheck = request.getParameter("idTipoCheck");
			String idProtocolo = request.getParameter("idProtocolo");
			if(idProtocolo==null){
				idProtocolo="0";
			}
			//String nombreCheck = new String(request.getParameter("nombreCheck").getBytes("ISO-8859-1"), "UTF-8");
			char[] ca = { '\u0061', '\u0301' };
			char[] ce = { '\u0065', '\u0301' };
			char[] ci = { '\u0069', '\u0301' };
			char[] co = { '\u006F', '\u0301' };
			char[] cu = { '\u0075', '\u0301' };
			// mayusculas
			char[] c1 = { '\u0041', '\u0301' };
			char[] c2 = { '\u0045', '\u0301' };
			char[] c3 = { '\u0049', '\u0301' };
			char[] c4 = { '\u004F', '\u0301' };
			char[] c5 = { '\u0055', '\u0301' };
			char[] c6 = { '\u006E', '\u0303' };

			String nombreCheck = request.getParameter("nombreCheck");
			//System.out.println("nombreCheck1 " + nombreCheck);
			if (nombreCheck.contains(String.valueOf(ca)) || nombreCheck.contains(String.valueOf(ce))
					|| nombreCheck.contains(String.valueOf(ci)) || nombreCheck.contains(String.valueOf(co))
					|| nombreCheck.contains(String.valueOf(cu)) || nombreCheck.contains(String.valueOf(c1))
					|| nombreCheck.contains(String.valueOf(c2)) || nombreCheck.contains(String.valueOf(c3))
					|| nombreCheck.contains(String.valueOf(c4)) || nombreCheck.contains(String.valueOf(c5))
					|| nombreCheck.contains(String.valueOf(c6))) {

				String rr = nombreCheck.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
						.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
						.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
						.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
						.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
						.replaceAll(String.valueOf(c6), "ñ");
				//System.out.println("rr " + rr);

				checklist.setNombreCheck(rr);
				rr = null;
			} else {
				//System.out.println("nombreCheck " + nombreCheck);

				checklist.setNombreCheck(nombreCheck);

			}
			
			String setVigente = request.getParameter("setVigente");
			String idChck = request.getParameter("idChecklist");
			String idUsuario = request.getParameter("idUsuario");
			String periodicidad = request.getParameter("periodicidad");
			String version = request.getParameter("version");
			String ordenGrupo = request.getParameter("ordenGrupo");
			String dia = request.getParameter("dia");
			String clasifica=request.getParameter("clasifica");
			double ponderacionTot = Double.parseDouble(request.getParameter("ponderacionTot"));

			UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
			idUsuario = userSession.getIdUsuario();

			ChecklistProtocoloDTO checklistProtocolo = new ChecklistProtocoloDTO();
			
			
			checklistProtocolo.setIdTipoChecklist(Integer.parseInt(idTipoCheck));
			checklistProtocolo.setNombreCheck(nombreCheck);
			checklistProtocolo.setIdHorario(Integer.parseInt(idHorario));
			checklistProtocolo.setVigente(Integer.parseInt(setVigente));
			checklistProtocolo.setFecha_inicio(fechaIni);
			checklistProtocolo.setFecha_fin(fechaFin);
			checklistProtocolo.setIdEstado(Integer.parseInt(idEstado));
			checklistProtocolo.setIdUsuario(idUsuario);
			checklistProtocolo.setDia(dia);
			checklistProtocolo.setPeriodo(periodicidad);
			checklistProtocolo.setVersion(version);
			checklistProtocolo.setOrdenGrupo(ordenGrupo);
			checklistProtocolo.setCommit(1);
			checklistProtocolo.setPonderacionTot(ponderacionTot);	
			checklistProtocolo.setClasifica(clasifica);
			checklistProtocolo.setIdProtocolo(idProtocolo);
			checklistProtocolo.setIdChecklist(Integer.parseInt(request.getParameter("idChecklist")));
	
			boolean res = checklistProtocoloBI.actualizaChecklistCom(checklistProtocolo);
			
			

			return res;

		}
		
		/*Para el procedure de actualización de pregunta, el ID de pregunta es obligatorio, se debe enviar el valor de los campos a modificar, 
		por ejemplo: si sólo se va a modificar el número de serie se enviaría el valor de PA_FIID_PREGUNTA  y PA_FINUMSERIE, los demás campos no enviar valor.*/
		
		//http://10.51.218.72:8080/checklist/checklistServices/updatePreguntaSup.json?idPregunta=368&setPregunta=¿Prueba correcta updt?&detalle=Ayuda contextua updtl&numSerie=5678
		@RequestMapping(value = "/updatePreguntaSup", method = RequestMethod.GET)
		public @ResponseBody boolean updatePreguntaSup(HttpServletRequest request, HttpServletResponse response)
				throws UnsupportedEncodingException {


			String idPregunta = request.getParameter("idPregunta");
			String idMod = request.getParameter("idMod");
			String idTipo = request.getParameter("idTipo");
			String estatus = request.getParameter("estatus");
			String setPregunta = request.getParameter("setPregunta");
			String detalle = request.getParameter("detalle");
			String critica = request.getParameter("critica");
			String sla = request.getParameter("sla");
			String area = request.getParameter("area");
			String numSerie = request.getParameter("numSerie");

			PreguntaSupDTO pregunta = new PreguntaSupDTO();
			pregunta.setIdPregunta(Integer.parseInt(idPregunta));
			pregunta.setIdModulo(idMod);
			pregunta.setIdTipo(idTipo);
			pregunta.setEstatus(estatus);
			
			if(setPregunta==null) {
				setPregunta="";
			}
			
			
			char[] ca = { '\u0061', '\u0301' };
			char[] ce = { '\u0065', '\u0301' };
			char[] ci = { '\u0069', '\u0301' };
			char[] co = { '\u006F', '\u0301' };
			char[] cu = { '\u0075', '\u0301' };
			// mayusculas
			char[] c1 = { '\u0041', '\u0301' };
			char[] c2 = { '\u0045', '\u0301' };
			char[] c3 = { '\u0049', '\u0301' };
			char[] c4 = { '\u004F', '\u0301' };
			char[] c5 = { '\u0055', '\u0301' };
			char[] c6 = { '\u006E', '\u0303' };
			
			System.out.println(String.valueOf(ca));

			if (setPregunta.contains(String.valueOf(ca)) || setPregunta.contains(String.valueOf(ce))
					|| setPregunta.contains(String.valueOf(ci)) || setPregunta.contains(String.valueOf(co))
					|| setPregunta.contains(String.valueOf(cu)) || setPregunta.contains(String.valueOf(c1))
					|| setPregunta.contains(String.valueOf(c2)) || setPregunta.contains(String.valueOf(c3))
					|| setPregunta.contains(String.valueOf(c4)) || setPregunta.contains(String.valueOf(c5))
					|| setPregunta.contains(String.valueOf(c6))) {

				String rr = setPregunta.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
						.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
						.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
						.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
						.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
						.replaceAll(String.valueOf(c6), "ñ");
				//System.out.println("rr " + rr);

				pregunta.setDescPregunta(rr);
				rr = null;
			} else {
				
				if(setPregunta.equals("")) {
					String aux=null;
					pregunta.setDescPregunta(aux);
				}else {
					pregunta.setDescPregunta(setPregunta);
				}
			}
			
			
			//pregunta.setDescPregunta(setPregunta);
			pregunta.setDetalle(detalle);
			pregunta.setCritica(critica);
			pregunta.setSla(sla);
			pregunta.setArea(area);
			pregunta.setNumSerie(numSerie);
			pregunta.setCommit(1);
			boolean res = preguntaSupBI.actualizaPregunta(pregunta);

			return res;

		}
		
		//http://10.51.218.72:8080/checklist/checklistServices/actualizaZona.json?idZona=?&descripcion=?&idStatus=?&idTipo=?
	    @RequestMapping(value = "/actualizaZona", method = RequestMethod.GET)
	    public ModelAndView actualizaZona(HttpServletRequest request, HttpServletResponse response) {
	        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
	        try {
	            String idZona = request.getParameter("idZona");
	            String descripcion = request.getParameter("descripcion");
	            String idStatus = request.getParameter("idStatus");
	            String idTipo = request.getParameter("idTipo");
	            
	            AdminZonaDTO zona = new AdminZonaDTO();
	            zona.setIdZona(idZona);
	            zona.setDescripcion(descripcion);
	            zona.setIdStatus(idStatus);
	            zona.setIdTipo(idTipo);
	            
	            boolean respuesta = admZonasBI.actualizaZona(zona);
	            mv.addObject("tipo", "Actualiza Zona");
	            mv.addObject("res", respuesta);

	        } catch (Exception e) {
	        	System.out.println("Catch actualizaZona: "+e.getStackTrace());
	        }
	        return mv;
	    }
	    
	  //http://10.51.218.72:8080/checklist/checklistServices/actualizaTipoZona.json?idTipoZona=?&descripcion=?&idStatus=?
	    @RequestMapping(value = "/actualizaTipoZona", method = RequestMethod.GET)
	    public ModelAndView actualizaTipoZona(HttpServletRequest request, HttpServletResponse response) {
	        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
	        try {
	            String idTipoZona = request.getParameter("idTipoZona");
	            String descripcion = request.getParameter("descripcion");
	            String idStatus = request.getParameter("idStatus");
	            
	            AdmTipoZonaDTO tipoZona = new AdmTipoZonaDTO();
	            tipoZona.setIdTipoZona(idTipoZona);
	            tipoZona.setDescripcion(descripcion);
	            tipoZona.setIdStatus(idStatus);
	            
	            boolean respuesta = admTipoZonaBI.actualizaTipoZona(tipoZona);
	            mv.addObject("tipo", "Actualiza Tipo Zona");
	            mv.addObject("res", respuesta);

	        } catch (Exception e) {
	        	System.out.println("Catch actualizaTipoZona: "+e.getStackTrace());
	        }
	        return mv;
	    }
	    
	  //http://10.51.218.72:8080/checklist/checklistServices/actualizaPregZona.json?idPregZona=?&idPregunta=?&idZona=?
	    @RequestMapping(value = "/actualizaPregZona", method = RequestMethod.GET)
	    public ModelAndView actualizaPregZona(HttpServletRequest request, HttpServletResponse response) {
	        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
	        try {
	            String idPregZona = request.getParameter("idPregZona");
	            String idPregunta = request.getParameter("idPregunta");
	            String idZona = request.getParameter("idZona");
	            
	            AdmPregZonaDTO pregZona = new AdmPregZonaDTO();
	            pregZona.setIdPregZona(idPregZona);
	            pregZona.setIdPreg(idPregunta);
	            pregZona.setIdZona(idZona);
	            
	            boolean respuesta = admPregZonasBI.actualizaPregZona(pregZona);
	            mv.addObject("tipo", "Actualiza Pregunta Zona");
	            mv.addObject("res", respuesta);

	        } catch (Exception e) {
	        	System.out.println("Catch actualizaPregZona: "+e.getStackTrace());
	        }
	        return mv;
	    }

}
