package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.AsignaExpDTO;

public class AsignaExpCheckuURowMapper implements RowMapper<AsignaExpDTO> {

	public AsignaExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AsignaExpDTO AsignaExpDTO = new AsignaExpDTO();
		
		AsignaExpDTO.setCeco(rs.getInt("FCID_CECO"));
	    AsignaExpDTO.setAux(rs.getInt("FIID_CHECKLIST"));
	    AsignaExpDTO.setUsuario(rs.getInt("FIID_USUARIO"));
	    AsignaExpDTO.setUsuario_asig(rs.getInt("FIID_CHECK_USUA"));
	    AsignaExpDTO.setObs(rs.getString("FIACTIVO"));
	    AsignaExpDTO.setAux2(rs.getString("FIID_BITACORA"));
	    //FECHA INICIO
	    AsignaExpDTO.setCec(rs.getString("FDINICIO"));
	    //fecha termino
	    AsignaExpDTO.setPeriodo(rs.getString("FDTERMINO"));
	    
	    
	    
		return AsignaExpDTO;
	}

}


