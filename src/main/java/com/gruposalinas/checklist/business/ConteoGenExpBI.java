package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.ConteoGenExpDAO;
import com.gruposalinas.checklist.domain.ConteoGenExpDTO;

public class ConteoGenExpBI {

	private static Logger logger = LogManager.getLogger(ConteoGenExpBI.class);

private List<ConteoGenExpDTO> listafila;
	
	@Autowired
	ConteoGenExpDAO conteoGenExpDAO;
		
	
	

	public String obtieneDatos( String bita, String clasifica) {
		JSONObject respuesta = new JSONObject();
		
		try {
			Map<String, Object> lista = null;
			lista = conteoGenExpDAO.obtieneDatos(bita,clasifica);
			
			@SuppressWarnings("unchecked")
			List<ConteoGenExpDTO> listaPreg = (List<ConteoGenExpDTO>) lista.get("preguntas");
			@SuppressWarnings("unchecked")
			List<ConteoGenExpDTO> listaRespuestas = (List<ConteoGenExpDTO>) lista.get("respuestas");
			@SuppressWarnings("unchecked")
			List<ConteoGenExpDTO> listaChecks = (List<ConteoGenExpDTO>) lista.get("checks");
			
			JSONArray preguntas = new JSONArray();
			JSONArray resp=new JSONArray();
			JSONArray check=new JSONArray();
			//lista preguntas
			for(int i=0; i < listaPreg.size();i++) {
				
				JSONObject obj =new  JSONObject();
				obj.put("idpreg", listaPreg.get(i).getIdPreg());
				obj.put("idmodulo", listaPreg.get(i).getIdModulo());
				obj.put("pregunta", listaPreg.get(i).getPregunta());
				obj.put("critica", listaPreg.get(i).getCritica());
				obj.put("area", listaPreg.get(i).getArea());
				obj.put("tipopreg", listaPreg.get(i).getTipoPreg());
				obj.put("pregpadre", listaPreg.get(i).getPregPadre());
				obj.put("idcheck", listaPreg.get(i).getIdCheck());
				obj.put("nomcheck", listaPreg.get(i).getNomCheck());
				obj.put("clasifica", listaPreg.get(i).getClasifica());
				obj.put("pontot", listaPreg.get(i).getPondTotal());
				obj.put("version", listaPreg.get(i).getVersion());
				preguntas.put(obj);
			
			} 
			for(int i=0; i < listaRespuestas.size();i++) {
                
				JSONObject obj =new  JSONObject();
				obj.put("bitagral", listaRespuestas.get(i).getBitaGral());
				obj.put("bita", listaRespuestas.get(i).getBitacora());
				obj.put("obs", (listaRespuestas.get(i).getObs()!=null && !listaRespuestas.get(i).getObs().isEmpty())?listaRespuestas.get(i).getObs():"");
				obj.put("idresp", listaRespuestas.get(i).getFcResp());
				obj.put("posible", listaRespuestas.get(i).getPosible());
				obj.put("idcheck", listaRespuestas.get(i).getIdCheck());
				obj.put("idpreg", listaRespuestas.get(i).getIdPreg());
				obj.put("pregunta", listaRespuestas.get(i).getPregunta());
				obj.put("ponderacion", listaRespuestas.get(i).getPonderacion());
				obj.put("nomcheck", listaRespuestas.get(i).getNomCheck());
				obj.put("clasifica", listaRespuestas.get(i).getClasifica());
				obj.put("critica", listaRespuestas.get(i).getCritica());
				resp.put(obj);		
			
			} 
			for(int i=0; i < listaChecks.size();i++) {
				
				 
				JSONObject obj =new  JSONObject();
				obj.put("idcheck", listaChecks.get(i).getIdCheck());
				obj.put("nomcheck", listaChecks.get(i).getNomCheck());
				obj.put("clasifica", listaChecks.get(i).getClasifica());
				obj.put("pontot", listaChecks.get(i).getPondTotal());
				obj.put("version", listaChecks.get(i).getVersion());
				obj.put("negocio", listaChecks.get(i).getNegocio());
				check.put(obj);		
			
			} 
			respuesta.append("listaPreg", preguntas);
			respuesta.append("listaRespuestas", resp);
			respuesta.append("listaChecks", check);

		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta.toString();
	}	
	
	public String obtieneDatosRepoSem( String bitaGral) {
		JSONObject respuesta = new JSONObject();
		
		try {
			Map<String, Object> lista = null;
			lista = conteoGenExpDAO.obtieneDatosRepoSem(bitaGral);
			
			@SuppressWarnings("unchecked")
			List<ConteoGenExpDTO> listaPreg = (List<ConteoGenExpDTO>) lista.get("preguntas");
			@SuppressWarnings("unchecked")
			List<ConteoGenExpDTO> listaRespuestas = (List<ConteoGenExpDTO>) lista.get("respuestas");
			
			
			JSONArray preguntas = new JSONArray();
			JSONArray resp=new JSONArray();
			
			//lista preguntas
			for(int i=0; i < listaPreg.size();i++) {
				
				JSONObject obj =new  JSONObject();
				obj.put("idpreg", listaPreg.get(i).getIdPreg());
				obj.put("idmodulo", listaPreg.get(i).getIdModulo());
				obj.put("pregunta", listaPreg.get(i).getPregunta());
				obj.put("critica", listaPreg.get(i).getCritica());
				obj.put("area", listaPreg.get(i).getArea());
				obj.put("tipopreg", listaPreg.get(i).getTipoPreg());
				obj.put("pregpadre", listaPreg.get(i).getPregPadre());
				obj.put("idcheck", listaPreg.get(i).getIdCheck());
				obj.put("nomcheck", listaPreg.get(i).getNomCheck());
				obj.put("clasifica", listaPreg.get(i).getClasifica());
				obj.put("pontot", listaPreg.get(i).getPondTotal());
				obj.put("version", listaPreg.get(i).getVersion());
				preguntas.put(obj);
			
			} 
			for(int i=0; i < listaRespuestas.size();i++) {
                
				JSONObject obj =new  JSONObject();
				obj.put("bitagral", listaRespuestas.get(i).getBitaGral());
				obj.put("bita", listaRespuestas.get(i).getBitacora());
				obj.put("obs", (listaRespuestas.get(i).getObs()!=null && !listaRespuestas.get(i).getObs().isEmpty())?listaRespuestas.get(i).getObs():"");
				obj.put("idresp", listaRespuestas.get(i).getFcResp());
				obj.put("posible", listaRespuestas.get(i).getPosible());
				obj.put("idcheck", listaRespuestas.get(i).getIdCheck());
				obj.put("idpreg", listaRespuestas.get(i).getIdPreg());
				obj.put("pregunta", listaRespuestas.get(i).getPregunta());
				obj.put("ponderacion", listaRespuestas.get(i).getPonderacion());
				obj.put("nomcheck", listaRespuestas.get(i).getNomCheck());
				obj.put("clasifica", listaRespuestas.get(i).getClasifica());
				obj.put("critica", listaRespuestas.get(i).getCritica());
				resp.put(obj);		
			
			} 
			
			respuesta.append("listaPreg", preguntas);
			respuesta.append("listaRespuestas", resp);
			

		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta.toString();
	}	

	public String obtieneDatosConteoGral(String ceco,String fase,String proyecto,String clasifica) {
		JSONObject respuesta = new JSONObject();
		
		try {
			Map<String, Object> lista = null;
			lista = conteoGenExpDAO.obtieneDatosConteoGral(ceco,fase,proyecto,clasifica);
			
			@SuppressWarnings("unchecked")
			List<ConteoGenExpDTO> listaPreg = (List<ConteoGenExpDTO>) lista.get("preguntas");
			@SuppressWarnings("unchecked")
			List<ConteoGenExpDTO> listaRespuestas = (List<ConteoGenExpDTO>) lista.get("respuestas");
			@SuppressWarnings("unchecked")
			List<ConteoGenExpDTO> listaChecks = (List<ConteoGenExpDTO>) lista.get("checks");
			
			JSONArray preguntas = new JSONArray();
			JSONArray resp=new JSONArray();
			JSONArray check=new JSONArray();
			//lista preguntas
			for(int i=0; i < listaPreg.size();i++) {
				
				JSONObject obj =new  JSONObject();
				obj.put("idpreg", listaPreg.get(i).getIdPreg());
				obj.put("idmodulo", listaPreg.get(i).getIdModulo());
				obj.put("pregunta", listaPreg.get(i).getPregunta());
				obj.put("critica", listaPreg.get(i).getCritica());
				obj.put("area", listaPreg.get(i).getArea());
				obj.put("tipopreg", listaPreg.get(i).getTipoPreg());
				obj.put("pregpadre", listaPreg.get(i).getPregPadre());
				obj.put("idcheck", listaPreg.get(i).getIdCheck());
				obj.put("nomcheck", listaPreg.get(i).getNomCheck());
				obj.put("clasifica", listaPreg.get(i).getClasifica());
				obj.put("pontot", listaPreg.get(i).getPondTotal());
				obj.put("version", listaPreg.get(i).getVersion());
				preguntas.put(obj);
			
			} 
			for(int i=0; i < listaRespuestas.size();i++) {
                
				JSONObject obj =new  JSONObject();
				obj.put("bitagral", listaRespuestas.get(i).getBitaGral());
				obj.put("bita", listaRespuestas.get(i).getBitacora());
				obj.put("obs", (listaRespuestas.get(i).getObs()!=null && !listaRespuestas.get(i).getObs().isEmpty())?listaRespuestas.get(i).getObs():"");
				obj.put("idresp", listaRespuestas.get(i).getFcResp());
				obj.put("posible", listaRespuestas.get(i).getPosible());
				obj.put("idcheck", listaRespuestas.get(i).getIdCheck());
				obj.put("idpreg", listaRespuestas.get(i).getIdPreg());
				obj.put("pregunta", listaRespuestas.get(i).getPregunta());
				obj.put("ponderacion", listaRespuestas.get(i).getPonderacion());
				obj.put("nomcheck", listaRespuestas.get(i).getNomCheck());
				obj.put("clasifica", listaRespuestas.get(i).getClasifica());
				obj.put("critica", listaRespuestas.get(i).getCritica());
				resp.put(obj);		
			
			} 
			for(int i=0; i < listaChecks.size();i++) {
				
				 
				JSONObject obj =new  JSONObject();
				obj.put("idcheck", listaChecks.get(i).getIdCheck());
				obj.put("nomcheck", listaChecks.get(i).getNomCheck());
				obj.put("clasifica", listaChecks.get(i).getClasifica());
				obj.put("pontot", listaChecks.get(i).getPondTotal());
				obj.put("version", listaChecks.get(i).getVersion());
				obj.put("negocio", listaChecks.get(i).getNegocio());
				check.put(obj);		
			
			} 
			respuesta.append("listaPreg", preguntas);
			respuesta.append("listaRespuestas", resp);
			respuesta.append("listaChecks", check);

		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta.toString();
	}	
}