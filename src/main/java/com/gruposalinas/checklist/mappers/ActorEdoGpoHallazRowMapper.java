package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ActorEdoHallaDTO;

public class ActorEdoGpoHallazRowMapper implements RowMapper<ActorEdoHallaDTO> {

	public ActorEdoHallaDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ActorEdoHallaDTO ActorEdoHallaDTO = new ActorEdoHallaDTO();
		
		ActorEdoHallaDTO.setIdEdoHallazgo(rs.getInt("FIID_EDOHALLA"));
	    ActorEdoHallaDTO.setNombStatus(rs.getString("FCNOMBRESTATUS"));
	    ActorEdoHallaDTO.setStatus(rs.getInt("FISTATUS"));
		ActorEdoHallaDTO.setIdConfiguracion(rs.getInt("FIID_CONFIG"));
	    ActorEdoHallaDTO.setObservacion(rs.getString("FCOBSERVACION"));
	    ActorEdoHallaDTO.setAux(rs.getString("FCAUX"));
	    ActorEdoHallaDTO.setPeriodo(rs.getString("FCPERIODO"));
	    ActorEdoHallaDTO.setColor(rs.getString("FCOLOR"));
        
		return ActorEdoHallaDTO;
		
	}

}


