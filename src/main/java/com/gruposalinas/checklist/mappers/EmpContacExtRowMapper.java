package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.EmpContacExtDTO;

public class EmpContacExtRowMapper implements RowMapper<EmpContacExtDTO> {

	public EmpContacExtDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		EmpContacExtDTO EmpContacExtDTO = new EmpContacExtDTO();
		
		EmpContacExtDTO.setIdTab(rs.getInt("FIID_CONTACTO"));
	    EmpContacExtDTO.setCeco(rs.getString("FCID_CECO"));
	    EmpContacExtDTO.setBitGral(rs.getInt("FIID_BITGRAL"));
	    EmpContacExtDTO.setIdUsu(rs.getInt("FCID_EMP"));
	    EmpContacExtDTO.setDomicilio(rs.getString("FCDOMICILIO"));
	    EmpContacExtDTO.setNombre(rs.getString("FCNOMBRE"));
	    EmpContacExtDTO.setTelefono(rs.getString("FCTELEFONO"));
	    EmpContacExtDTO.setContacto(rs.getString("FCCONTACTO"));
	    EmpContacExtDTO.setArea(rs.getString("FCAREA"));
	    EmpContacExtDTO.setCompania(rs.getString("FCCOMPANIA"));
	    EmpContacExtDTO.setAux(rs.getString("FCAUX"));
	    EmpContacExtDTO.setAux2(rs.getString("FCAUX2"));
	    EmpContacExtDTO.setPeriodo(rs.getString("FCPERIODO"));
	   
		return EmpContacExtDTO;
	}

}


