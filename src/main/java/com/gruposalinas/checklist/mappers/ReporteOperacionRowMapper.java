package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteDTO;

public class ReporteOperacionRowMapper  implements RowMapper<ReporteDTO>   {

	@Override
	public ReporteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		ReporteDTO reporteDTO = new ReporteDTO();
		
		reporteDTO.setIdPregunta(rs.getString("FIID_PREGUNTA"));
		reporteDTO.setDesPregunta(rs.getString("FCDESCRIPCION"));
		reporteDTO.setSi(rs.getString("SI"));
		reporteDTO.setNo(rs.getString("NO"));
		reporteDTO.setTotal(rs.getInt("TOTAL"));
		reporteDTO.setAvance(rs.getInt("AVANCE"));
		reporteDTO.setFondo(rs.getString("FONDO"));
		//reporteDTO.setConteo(rs.getString("CONTEO"));
		
		return reporteDTO;
	}

}
