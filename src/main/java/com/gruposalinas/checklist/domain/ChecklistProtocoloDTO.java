package com.gruposalinas.checklist.domain;

public class ChecklistProtocoloDTO {
	

	private int idTipoChecklist;
	private int idChecklist;
	private String nombreCheck;
	private int idHorario;
	private int vigente;
	private String fecha_inicio;
	private String fecha_fin;
	private int idEstado;
	private String idUsuario;
	private int commit;
	private String usuarioModificiacion;
	private String fechaModificacion;
	private String dia;
	private String periodo;
   private String version;
   private String ordenGrupo;
	private double ponderacionTot;
	private String clasifica;
	private String idProtocolo;
	private String negocio;




	
	public String getNegocio() {
		return negocio;
	}
	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}
	public int getIdTipoChecklist() {
		return idTipoChecklist;
	}
	public void setIdTipoChecklist(int idTipoChecklist) {
		this.idTipoChecklist = idTipoChecklist;
	}
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public String getNombreCheck() {
		return nombreCheck;
	}
	public void setNombreCheck(String nombreCheck) {
		this.nombreCheck = nombreCheck;
	}
	public int getIdHorario() {
		return idHorario;
	}
	public void setIdHorario(int idHorario) {
		this.idHorario = idHorario;
	}
	public int getVigente() {
		return vigente;
	}
	public void setVigente(int vigente) {
		this.vigente = vigente;
	}
	public String getFecha_inicio() {
		return fecha_inicio;
	}
	public void setFecha_inicio(String fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}
	public String getFecha_fin() {
		return fecha_fin;
	}
	public void setFecha_fin(String fecha_fin) {
		this.fecha_fin = fecha_fin;
	}
	public int getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}
	
	
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getCommit() {
		return commit;
	}
	public void setCommit(int commit) {
		this.commit = commit;
	}
	public String getUsuarioModificiacion() {
		return usuarioModificiacion;
	}
	public void setUsuarioModificiacion(String usuarioModificiacion) {
		this.usuarioModificiacion = usuarioModificiacion;
	}
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getOrdenGrupo() {
		return ordenGrupo;
	}
	public void setOrdenGrupo(String ordenGrupo) {
		this.ordenGrupo = ordenGrupo;
	}
	public double getPonderacionTot() {
		return ponderacionTot;
	}
	public void setPonderacionTot(double ponderacionTot) {
		this.ponderacionTot = ponderacionTot;
	}
	public String getClasifica() {
		return clasifica;
	}
	public void setClasifica(String clasifica) {
		this.clasifica = clasifica;
	}
	public String getIdProtocolo() {
		return idProtocolo;
	}
	public void setIdProtocolo(String idProtocolo) {
		this.idProtocolo = idProtocolo;
	}
	@Override
	public String toString() {
		return "ChecklistProtocoloDTO [idTipoChecklist=" + idTipoChecklist + ", idChecklist=" + idChecklist
				+ ", nombreCheck=" + nombreCheck + ", idHorario=" + idHorario + ", vigente=" + vigente
				+ ", fecha_inicio=" + fecha_inicio + ", fecha_fin=" + fecha_fin + ", idEstado=" + idEstado
				+ ", idUsuario=" + idUsuario + ", commit=" + commit + ", usuarioModificiacion=" + usuarioModificiacion
				+ ", fechaModificacion=" + fechaModificacion + ", dia=" + dia + ", periodo=" + periodo + ", version="
				+ version + ", ordenGrupo=" + ordenGrupo + ", ponderacionTot=" + ponderacionTot + ", clasifica="
				+ clasifica + ", idProtocolo=" + idProtocolo + "]";
	}




	 
}
