package com.gruposalinas.checklist.servicios.servidor;

import com.gruposalinas.checklist.business.AdmPregZonasBI;
import com.gruposalinas.checklist.business.AdmZonasBI;
import com.gruposalinas.checklist.business.ArbolRespAdiEviAdmBI;
import com.gruposalinas.checklist.business.BitacoraCheckUsuAdmBI;
import com.gruposalinas.checklist.business.CheckAutProtoBI;
import com.gruposalinas.checklist.business.CheckSoporteAdmBI;
import com.gruposalinas.checklist.business.PosiblesAdmBI;
import com.gruposalinas.checklist.business.PreguntaAdmBI;
import com.gruposalinas.checklist.business.VersionCheckAdmBI;
import com.gruposalinas.checklist.dao.BitacoraCheckUsuAdmDAO;
import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.CheckAutoProtoDTO;
import com.gruposalinas.checklist.domain.CheckSoporteAdmDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.domain.ChecklistUsuarioDTO;
import com.gruposalinas.checklist.domain.EvidenciaDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.RespuestaAdDTO;
import com.gruposalinas.checklist.domain.RespuestaDTO;
import com.gruposalinas.checklist.domain.TipoChecklistDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.domain.VersionExpanDTO;
import com.gruposalinas.checklist.resources.FRQAuthInterceptor;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@Controller
@RequestMapping("/catalogosAdmService")
public class AltaChecklistAdminService {

    @Autowired
    PreguntaAdmBI preguntaAdmBI;
    @Autowired
    BitacoraCheckUsuAdmDAO bitacoraCheckUsuAdmDAO;
    @Autowired
    ArbolRespAdiEviAdmBI arbolRespAdiEviAdmBI;
    @Autowired
    CheckAutProtoBI checkAutProtoBI;
    @Autowired
    PosiblesAdmBI posiblesAdmBI;
    @Autowired
    BitacoraCheckUsuAdmBI bitacoraCheckUsuAdmBI;

    @Autowired
    VersionCheckAdmBI versionCheckAdmBI;
    @Autowired
    CheckSoporteAdmBI checkSoporteAdmBI;
    @Autowired
    AdmPregZonasBI admPregZonasBI;
    @Autowired
    AdmZonasBI admZonasBI;

    private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);

    // http://localhost:8080/checklist/catalogosAdmService/altaModulo.json?idModuloPadre=<?>&nombreMod=<?>
    @RequestMapping(value = "/altaModulo", method = RequestMethod.GET)
    public ModelAndView altaModulo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idModuloPadre = request.getParameter("idModuloPadre");

            char[] ca = {'\u0061', '\u0301'};
            char[] ce = {'\u0065', '\u0301'};
            char[] ci = {'\u0069', '\u0301'};
            char[] co = {'\u006F', '\u0301'};
            char[] cu = {'\u0075', '\u0301'};
            // mayusculas
            char[] c1 = {'\u0041', '\u0301'};
            char[] c2 = {'\u0045', '\u0301'};
            char[] c3 = {'\u0049', '\u0301'};
            char[] c4 = {'\u004F', '\u0301'};
            char[] c5 = {'\u0055', '\u0301'};
            char[] c6 = {'\u006E', '\u0303'};
            String nombreModulo = request.getParameter("nombreMod");

            String nombreModuloCod = "";
            // System.out.println("setPregunta1 " + nombreModulo);
            if (nombreModulo.contains(String.valueOf(ca)) || nombreModulo.contains(String.valueOf(ce))
                    || nombreModulo.contains(String.valueOf(ci)) || nombreModulo.contains(String.valueOf(co))
                    || nombreModulo.contains(String.valueOf(cu)) || nombreModulo.contains(String.valueOf(c1))
                    || nombreModulo.contains(String.valueOf(c2)) || nombreModulo.contains(String.valueOf(c3))
                    || nombreModulo.contains(String.valueOf(c4)) || nombreModulo.contains(String.valueOf(c5))
                    || nombreModulo.contains(String.valueOf(c6))) {

                String rr = nombreModulo.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
                        .replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
                        .replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
                        .replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
                        .replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
                        .replaceAll(String.valueOf(c6), "ñ");
                // System.out.println("rr " + rr);

                nombreModuloCod = rr;
                rr = null;
            } else {
                // System.out.println("setPregunta " + nombreModulo);

                nombreModuloCod = nombreModulo;

            }

            ModuloDTO modulo = new ModuloDTO();
            modulo.setIdModuloPadre(Integer.parseInt(idModuloPadre));
            modulo.setNombre(nombreModuloCod);
            int idMod = preguntaAdmBI.insertaModulo(modulo);
            modulo.setIdModulo(idMod);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID DE MODULO CREADO");
            mv.addObject("res", idMod);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/catalogosAdmService/altaCheckServiceCom.json?fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>&commit=<?>&idUsuario=<?>&periodicidad=<?>&dia=<?>&version=<?>&ordenGrupo=<?>&ponderacionTot=<?>clasifica=<?>&nego=
    // FORMATO FECHA INI Y FECHA FIN (AAAAMMDD)
    @RequestMapping(value = "/altaCheckServiceCom", method = RequestMethod.GET)
    public @ResponseBody
    ChecklistDTO altaCheckServiceCom(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String fechaIni = request.getParameter("fechaIni");
            String fechaFin = request.getParameter("fechaFin");
            String idEstado = request.getParameter("idEstado");
            String idHorario = request.getParameter("idHorario");
            String idTipoCheck = request.getParameter("idTipoCheck");
            String nombreCheck = request.getParameter("nombreCheck");
            String setVigente = request.getParameter("setVigente");
            String commit = request.getParameter("commit");
            String idUsuario = request.getParameter("idUsuario");
            String periodicidad = request.getParameter("periodicidad");
            String version = request.getParameter("version");
            String ordenGrupo = request.getParameter("ordenGrupo");
            String dia = request.getParameter("dia");
            String clasifica = request.getParameter("clasifica");
            String nego = request.getParameter("nego");
            double ponderacionTot = Double.parseDouble(request.getParameter("ponderacionTot"));

            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            idUsuario = userSession.getIdUsuario();

            ChecklistDTO checklist = new ChecklistDTO();
            checklist.setFecha_inicio(fechaIni);
            checklist.setFecha_fin(fechaFin);
            checklist.setIdEstado(Integer.parseInt(idEstado));
            checklist.setIdHorario(Integer.parseInt(idHorario));
            checklist.setCommit(Integer.parseInt(commit));
            TipoChecklistDTO chk = new TipoChecklistDTO();
            chk.setIdTipoCheck(Integer.parseInt(idTipoCheck));
            checklist.setIdTipoChecklist(chk);
            checklist.setNombreCheck(nombreCheck);
            checklist.setVigente(Integer.parseInt(setVigente));
            // SE AGREGA USUARIO RESPONSABLE, DIA Y PERIDO
            checklist.setIdUsuario(Integer.parseInt(idUsuario));
            checklist.setPeriodicidad(periodicidad);
            checklist.setVersion(version);
            checklist.setOrdeGrupo(ordenGrupo);
            checklist.setDia(dia);
            checklist.setPonderacionTot(ponderacionTot);
            checklist.setClasifica(clasifica);
            checklist.setNego(nego);
            // Obtengo el ID del checklist
            int idChecklist = preguntaAdmBI.insertaChecklistCom(checklist);
            checklist.setIdChecklist(idChecklist);

            return checklist;

        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/checklist/catalogosAdmService/altaCheckService.json?fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>&commit=<?>&idUsuario=<?>&periodicidad=<?>&dia=<?>&version=<?>&ordenGrupo=<?>
    // FORMATO FECHA INI Y FECHA FIN (AAAAMMDD)
    @RequestMapping(value = "/altaCheckService", method = RequestMethod.GET)
    public @ResponseBody
    ChecklistProtocoloDTO altaCheckService(HttpServletRequest request,
            HttpServletResponse response) throws UnsupportedEncodingException {
        try {
            String fechaIni = request.getParameter("fechaIni");
            String fechaFin = request.getParameter("fechaFin");
            String idEstado = request.getParameter("idEstado");
            String idHorario = request.getParameter("idHorario");
            String idTipoCheck = request.getParameter("idTipoCheck");
            String nombreCheck = new String(request.getParameter("nombreCheck").getBytes("UTF-8"), "ISO-8859-1");
            String setVigente = request.getParameter("setVigente");
            String commit = request.getParameter("commit");
            String idUsuario = request.getParameter("idUsuario");
            String periodicidad = request.getParameter("periodicidad");
            String version = request.getParameter("version");
            String ordenGrupo = request.getParameter("ordenGrupo");
            String dia = request.getParameter("dia");

            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            idUsuario = userSession.getIdUsuario();

            ChecklistProtocoloDTO checklist = new ChecklistProtocoloDTO();
            checklist.setFecha_inicio(fechaIni);
            checklist.setFecha_fin(fechaFin);
            checklist.setIdEstado(Integer.parseInt(idEstado));
            checklist.setIdHorario(Integer.parseInt(idHorario));
            checklist.setCommit(Integer.parseInt(commit));
            TipoChecklistDTO chk = new TipoChecklistDTO();
            chk.setIdTipoCheck(Integer.parseInt(idTipoCheck));
            // checklist.setIdTipoChecklist(idTipoCheck);
            checklist.setIdTipoChecklist(Integer.parseInt(idTipoCheck));
            checklist.setNombreCheck(nombreCheck);
            checklist.setVigente(Integer.parseInt(setVigente));
            // SE AGREGA USUARIO RESPONSABLE, DIA Y PERIDO
            // checklist.setIdUsuario(Integer.parseInt(idUsuario));
            checklist.setIdUsuario(idUsuario);
            // checklist.setPeriodicidad(periodicidad);
            checklist.setPeriodo(periodicidad);
            checklist.setVersion(version);
            // checklist.setOrdeGrupo(ordenGrupo);
            checklist.setOrdenGrupo(ordenGrupo);
            checklist.setDia(dia);
            // Obtengo el ID del checklist
            int idChecklist = preguntaAdmBI.insertaChecklist(checklist);
            checklist.setIdChecklist(idChecklist);

            return checklist;

        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://10.51.219.179:8080/checklist/catalogosAdmService/altaPreguntaN.json?idMod=130&idTipo=24&estatus=1&setPregunta=¿Pregunta
    // test?&commit=1&detalle<?>&critica=1&sla=1&area=
    @RequestMapping(value = "/altaPreguntaN", method = RequestMethod.GET)
    public ModelAndView altaPreguntaN(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        PreguntaDTO pregunta = new PreguntaDTO();
        try {

            String idMod = request.getParameter("idMod");
            String idTipo = request.getParameter("idTipo");
            String estatus = request.getParameter("estatus");
            String detalle = request.getParameter("detalle");
            int critica = Integer.parseInt(request.getParameter("critica"));

            char[] ca = {'\u0061', '\u0301'};
            char[] ce = {'\u0065', '\u0301'};
            char[] ci = {'\u0069', '\u0301'};
            char[] co = {'\u006F', '\u0301'};
            char[] cu = {'\u0075', '\u0301'};
            // mayusculas
            char[] c1 = {'\u0041', '\u0301'};
            char[] c2 = {'\u0045', '\u0301'};
            char[] c3 = {'\u0049', '\u0301'};
            char[] c4 = {'\u004F', '\u0301'};
            char[] c5 = {'\u0055', '\u0301'};
            char[] c6 = {'\u006E', '\u0303'};

            String setPregunta = request.getParameter("setPregunta");
            // System.out.println("setPregunta1 " +setPregunta);
            if (setPregunta.contains(String.valueOf(ca)) || setPregunta.contains(String.valueOf(ce))
                    || setPregunta.contains(String.valueOf(ci)) || setPregunta.contains(String.valueOf(co))
                    || setPregunta.contains(String.valueOf(cu)) || setPregunta.contains(String.valueOf(c1))
                    || setPregunta.contains(String.valueOf(c2)) || setPregunta.contains(String.valueOf(c3))
                    || setPregunta.contains(String.valueOf(c4)) || setPregunta.contains(String.valueOf(c5))
                    || setPregunta.contains(String.valueOf(c6))) {

                String rr = setPregunta.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
                        .replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
                        .replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
                        .replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
                        .replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
                        .replaceAll(String.valueOf(c6), "ñ");
                // System.out.println("rr " +rr);

                pregunta.setPregunta(rr);
                rr = null;
            } else {
                // System.out.println("setPregunta " +setPregunta);

                pregunta.setPregunta(setPregunta);

            }

            if (detalle.contains(String.valueOf(ca)) || detalle.contains(String.valueOf(ce))
                    || detalle.contains(String.valueOf(ci)) || detalle.contains(String.valueOf(co))
                    || detalle.contains(String.valueOf(cu)) || detalle.contains(String.valueOf(c1))
                    || detalle.contains(String.valueOf(c2)) || detalle.contains(String.valueOf(c3))
                    || detalle.contains(String.valueOf(c4)) || detalle.contains(String.valueOf(c5))
                    || detalle.contains(String.valueOf(c6))) {

                String rr = detalle.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
                        .replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
                        .replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
                        .replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
                        .replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
                        .replaceAll(String.valueOf(c6), "ñ");

                pregunta.setDetalle(rr);
                rr = null;
            } else {

                pregunta.setDetalle(detalle);

            }

            String commit = request.getParameter("commit");
            String sla = request.getParameter("sla");
            String area = "";
            area = request.getParameter("area");

            pregunta.setIdModulo(Integer.parseInt(idMod));
            pregunta.setIdTipo(Integer.parseInt(idTipo));
            pregunta.setEstatus(Integer.parseInt(estatus));
            pregunta.setCritica(critica);
            // pregunta.setDetalle(detalle);

            pregunta.setCommit(Integer.parseInt(commit));
            pregunta.setSla(sla);
            pregunta.setArea(area);
            int idPreg = this.preguntaAdmBI.insertaPreguntaCom(pregunta);
            pregunta.setIdPregunta(idPreg);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID DE PREGUNTA CREADA");
            mv.addObject("res", idPreg);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/catalogosAdmService/altaPregCheck.json?idCheck=<?>&ordenPreg=<?>&idPreg=<?>&commit=<?>&pregPadre=<?>
    @RequestMapping(value = "/altaPregCheck", method = RequestMethod.GET)
    public @ResponseBody
    boolean altaPregCheck(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCheck = request.getParameter("idCheck");
            String ordenPreg = request.getParameter("ordenPreg");
            String idPreg = request.getParameter("idPreg");
            String idPadre = request.getParameter("pregPadre");
            String commit = request.getParameter("commit");

            ChecklistPreguntaDTO pregDTO = new ChecklistPreguntaDTO();
            pregDTO.setIdChecklist(Integer.parseInt(idCheck));
            pregDTO.setOrdenPregunta(Integer.parseInt(ordenPreg));
            pregDTO.setIdPregunta(Integer.parseInt(idPreg));
            pregDTO.setCommit(Integer.parseInt(commit));
            pregDTO.setPregPadre(Integer.parseInt(idPadre));
            boolean checklistPreg = preguntaAdmBI.insertaChecklistPregunta(pregDTO);

            return checklistPreg;
        } catch (Exception e) {
            logger.info(e);
            return false;
        }
    }

    // BITACORA
    // http://localhost:8080/checklist/catalogosAdmService/altaBitacora.json?fechaIni=<?>&fechaFin=<?>&idCheckUsua=<?>&latitud=<?>&longitud=<?>&modo=<?>
    @RequestMapping(value = "/altaBitacora", method = RequestMethod.GET)
    public ModelAndView altaBitacora(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String fechaInicio = request.getParameter("fechaIni");
            String fechaFin = request.getParameter("fechaFin");
            String idCheckUsua = request.getParameter("idCheckUsua");
            String latitud = request.getParameter("latitud");
            String longitud = request.getParameter("longitud");
            String modo = request.getParameter("modo");

            BitacoraDTO bitacora = new BitacoraDTO();
            bitacora.setFechaInicio(fechaInicio);
            bitacora.setFechaFin(fechaFin);
            bitacora.setIdCheckUsua(Integer.parseInt(idCheckUsua));
            bitacora.setLatitud(latitud);
            bitacora.setLongitud(longitud);
            bitacora.setModo(Integer.parseInt(modo));
            int idBitacora = bitacoraCheckUsuAdmDAO.insertaBitacora(bitacora);
            bitacora.setIdBitacora(idBitacora);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID BITACORA CREADA-->");
            mv.addObject("res", idBitacora);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/catalogosAdmService/altaUsuCheck.json?setActivo=<?>&fechaIni=<?>&fechaResp=<?>&idCeco=<?>&idChecklist=<?>&idUsu=<?>
    @RequestMapping(value = "/altaUsuCheck", method = RequestMethod.GET)
    public ModelAndView altaUsuarioCheck(HttpServletRequest request, HttpServletResponse response) {
        try {
            String setActivo = request.getParameter("setActivo");
            String fechaIni = request.getParameter("fechaIni");
            String fechaResp = request.getParameter("fechaResp");
            String idCeco = request.getParameter("idCeco");
            String idChecklist = request.getParameter("idChecklist");
            String idUsu = request.getParameter("idUsu");

            ChecklistUsuarioDTO checkUsu = new ChecklistUsuarioDTO();
            checkUsu.setActivo(Integer.parseInt(setActivo));
            checkUsu.setFechaIni(fechaIni);
            checkUsu.setFechaResp(fechaResp);
            checkUsu.setIdCeco(Integer.parseInt(idCeco));
            checkUsu.setIdChecklist(Integer.parseInt(idChecklist));
            checkUsu.setIdUsuario(Integer.parseInt(idUsu));
            int idChecklistUsu = bitacoraCheckUsuAdmDAO.insertaCheckUsuario(checkUsu);
            checkUsu.setIdChecklist(idChecklistUsu);

            // ModelAndView mv = new ModelAndView("altaModuloRes");
            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
            mv.addObject("tipo", "ID CHECKLIST USUARIO CREADO--->");
            mv.addObject("res", idChecklistUsu);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
    // http://localhost:8080/checklist/catalogosAdmService/arbolDesicion.json?estatusEvidencia=<?>&idCheck=<?>&idPreg=<?>&idOrdenCheckRes=<?>&setRes=<?>&reqAccion=<?>&commit=<?>&reqObs=<?>&reqOblig=<?>&etiquetaEvidencia=<?>&ponderacion=<?>&idPlantilla=<?>&idProtocolo=<?>

    @RequestMapping(value = "/arbolDesicion", method = RequestMethod.GET)
    public ModelAndView arbolDecision(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String estatusEvidencia = request.getParameter("estatusEvidencia");
            String idCheck = request.getParameter("idCheck");
            String idPreg = request.getParameter("idPreg");
            String idOrdenCheckRes = request.getParameter("idOrdenCheckRes");
            String setRes = new String(request.getParameter("setRes").getBytes("ISO-8859-1"), "UTF-8");
            String reqAccion = request.getParameter("reqAccion");
            String commit = request.getParameter("commit");
            String reqObs = request.getParameter("reqObs");
            String reqOblig = request.getParameter("reqOblig");
            String etiquetaEvi = request.getParameter("etiquetaEvidencia");
            String ponderacion = request.getParameter("ponderacion");
            String idPlantilla = request.getParameter("idPlantilla");
            String idProtocolo = request.getParameter("idProtocolo");
            // System.out.println(ponderacion);
            int idProtocoloI = 0;
            if (idProtocolo != null) {
                idProtocoloI = Integer.parseInt(idProtocolo);
            }

            ArbolDecisionDTO arbol = new ArbolDecisionDTO();
            arbol.setEstatusEvidencia(Integer.parseInt(estatusEvidencia));
            arbol.setIdCheckList(Integer.parseInt(idCheck));
            arbol.setIdPregunta(Integer.parseInt(idPreg));
            arbol.setOrdenCheckRespuesta(Integer.parseInt(idOrdenCheckRes));
            arbol.setRespuesta(setRes);
            arbol.setReqAccion(Integer.parseInt(reqAccion));
            arbol.setCommit(Integer.parseInt(commit));
            arbol.setReqObservacion(Integer.parseInt(reqObs));
            arbol.setReqOblig(Integer.parseInt(reqOblig));
            arbol.setDescEvidencia(etiquetaEvi);
            arbol.setPonderacion(ponderacion);
            arbol.setIdPlantilla(Integer.parseInt(idPlantilla));
            arbol.setIdProtocolo(idProtocoloI);

            // System.out.println(arbol.getPonderacion());
            int resArbol = arbolRespAdiEviAdmBI.insertaArbolDecision(arbol);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID ARBOL  CREADO --->");
            mv.addObject("res", resArbol);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/catalogosAdmService/altaRespuesta.json?idArbolDesicion=<?>&idBitacora=<?>&commit=<?>&observaciones=<?>
    @RequestMapping(value = "/altaRespuesta", method = RequestMethod.GET)
    public ModelAndView altaRespuesta(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idArbolDesicion = request.getParameter("idArbolDesicion");
            String idBitacora = request.getParameter("idBitacora");
            String commit = request.getParameter("commit");
            String observaciones = new String(request.getParameter("observaciones").getBytes("ISO-8859-1"), "UTF-8");

            RespuestaDTO res = new RespuestaDTO();

            res.setIdArboldecision(Integer.parseInt(idArbolDesicion));
            res.setIdBitacora(Integer.parseInt(idBitacora));
            res.setCommit(Integer.parseInt(commit));
            res.setObservacion(observaciones);
            int idRes = arbolRespAdiEviAdmBI.insertaRespuesta(res);
            res.setIdRespuesta(idRes);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID RESPUESTA CREADA-->");
            mv.addObject("res", idRes);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/catalogosAdmService/altaRespuestaAD.json?idRespAd=<?>&descripcion=<?>&commit=
    @RequestMapping(value = "/altaRespuestaAD", method = RequestMethod.GET)
    public ModelAndView altaRespuestaAD(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idRespAd = request.getParameter("idRespAd");
            String descripcion = request.getParameter("descripcion");
            String commit = request.getParameter("commit");

            RespuestaAdDTO res = new RespuestaAdDTO();

            res.setIdRespuesta(Integer.parseInt(idRespAd));
            res.setDescripcion(descripcion);
            res.setCommit(Integer.parseInt(commit));

            int idRes = arbolRespAdiEviAdmBI.insertaRespAD(res);
            res.setIdRespuesta(idRes);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID RESPUESTA CREADA-->");
            mv.addObject("res", idRes);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/catalogosAdmService/altaEvidencia.json?commit=<?>&idRespuesta=<?>&idTipo=<?>&ruta=<?>&idElementoP=<?>
    @RequestMapping(value = "/altaEvidencia", method = RequestMethod.GET)
    public ModelAndView altaEvidencia(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String commit = request.getParameter("commit");
            String idRespuesta = request.getParameter("idRespuesta");
            String idTipo = request.getParameter("idTipo");
            String ruta = request.getParameter("ruta");
            String idElementoP = request.getParameter("idElementoP");

            EvidenciaDTO evidencia = new EvidenciaDTO();
            evidencia.setCommit(Integer.parseInt(commit));
            evidencia.setIdRespuesta(Integer.parseInt(idRespuesta));
            evidencia.setIdTipo(Integer.parseInt(idTipo));
            evidencia.setRuta(ruta);
            evidencia.setIdPlantilla(Integer.parseInt(idElementoP));
            int idEvi = arbolRespAdiEviAdmBI.insertaEvidencia(evidencia);
            evidencia.setIdEvidencia(idEvi);
            ;

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID EVIDENCIA CREADA-->");
            mv.addObject("res", idEvi);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/catalogosAdmService/altaCheckAutoriza.json?version=<?>&usu=<?>&deta=<?>&status=<?>&tipousu=<?>&fechalibera=<?>
    @RequestMapping(value = "/altaCheckAutoriza", method = RequestMethod.GET)
    public ModelAndView altaCheckAutoriza(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int version = Integer.parseInt(request.getParameter("version"));
            int usu = Integer.parseInt(request.getParameter("usu"));
            String deta = request.getParameter("deta");
            int status = Integer.parseInt(request.getParameter("status"));
            String tipousu = request.getParameter("tipousu");
            String fechalibera = request.getParameter("fechalibera");

            CheckAutoProtoDTO aut = new CheckAutoProtoDTO();

            aut.setFiversion(version);
            aut.setIdUsu(usu);
            aut.setDetalle(deta);
            aut.setStatus(status);
            aut.setUsuTipo(tipousu);
            aut.setFechaLiberacion(fechalibera);

            int resp = checkAutProtoBI.insertaAutoProto(aut);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", " Autorizacion Checklist-->");
            mv.addObject("res", resp);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/catalogosAdmService/altaPosible.json?respuesta=<?>
    @RequestMapping(value = "/altaPosible", method = RequestMethod.GET)
    public ModelAndView altaPosible(HttpServletRequest request, HttpServletResponse response) {
        try {
            String respuesta = new String(request.getParameter("respuesta").getBytes("ISO-8859-1"), "UTF-8");

            char[] ca = {'\u0061', '\u0301'};
            char[] ce = {'\u0065', '\u0301'};
            char[] ci = {'\u0069', '\u0301'};
            char[] co = {'\u006F', '\u0301'};
            char[] cu = {'\u0075', '\u0301'};
            // mayusculas
            char[] c1 = {'\u0041', '\u0301'};
            char[] c2 = {'\u0045', '\u0301'};
            char[] c3 = {'\u0049', '\u0301'};
            char[] c4 = {'\u004F', '\u0301'};
            char[] c5 = {'\u0055', '\u0301'};
            char[] c6 = {'\u006E', '\u0303'};

            String setPregunta = request.getParameter("respuesta");
            // System.out.println("setPregunta1 " + setPregunta);
            if (setPregunta.contains(String.valueOf(ca)) || setPregunta.contains(String.valueOf(ce))
                    || setPregunta.contains(String.valueOf(ci)) || setPregunta.contains(String.valueOf(co))
                    || setPregunta.contains(String.valueOf(cu)) || setPregunta.contains(String.valueOf(c1))
                    || setPregunta.contains(String.valueOf(c2)) || setPregunta.contains(String.valueOf(c3))
                    || setPregunta.contains(String.valueOf(c4)) || setPregunta.contains(String.valueOf(c5))
                    || setPregunta.contains(String.valueOf(c6))) {

                String rr = setPregunta.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
                        .replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
                        .replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
                        .replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
                        .replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
                        .replaceAll(String.valueOf(c6), "ñ");
                // System.out.println("rr " + rr);

                setPregunta = rr;
                rr = null;
            }

            int res = posiblesAdmBI.insertaPosible(setPregunta);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID POSIBLE ASIGNADO ");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/checklist/catalogosAdmService/asignaValidacion.json?setActivo=1&idCeco=<?>&idVersion=<?>&idUsu=<?>&fechaLiberacion=<?>
    @RequestMapping(value = "/asignaValidacion", method = RequestMethod.GET)

    public @ResponseBody
    String asignaValidacion(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "setActivo", required = false, defaultValue = "") String setActivo,
            @RequestParam(value = "fechaIni", required = false, defaultValue = "") String fechaIni,
            @RequestParam(value = "fechaResp", required = false, defaultValue = "") String fechaResp,
            @RequestParam(value = "idCeco", required = false, defaultValue = "") String idCeco,
            @RequestParam(value = "idVersion", required = false, defaultValue = "") String idVersion,
            @RequestParam(value = "idUsu", required = false, defaultValue = "") String idUsu,
            @RequestParam(value = "fechaLiberacion", required = false, defaultValue = "") String fechaLiberacion)
            throws KeyException, GeneralSecurityException, IOException {

        String json = "";
        String idVersionParam = idVersion;
        boolean responseGlobal = true;
        int idChecklistUsu = 0;

        try {

            List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkVersParam(idVersionParam, "0");

            for (CheckSoporteAdmDTO objLista : lista) {

                String pattern = "yyyyMMdd";
                SimpleDateFormat format = new SimpleDateFormat(pattern);
                String fecha = format.format(new Date().getDate());
                String setActivoParam = setActivo;
                String fechaIniParam = fecha;
                // Fecha de Liberación
                String fechaRespParam = fecha;
                String idUsuParam = request.getParameter("idUsu");

                ChecklistUsuarioDTO checkUsu = new ChecklistUsuarioDTO();
                checkUsu.setActivo(Integer.parseInt(setActivoParam));
                checkUsu.setFechaIni(fechaIniParam);
                checkUsu.setFechaResp(fechaRespParam);
                checkUsu.setIdCeco(Integer.parseInt(idCeco));
                checkUsu.setIdChecklist(objLista.getIdChecklist());
                checkUsu.setIdUsuario(Integer.parseInt(idUsuParam));

                idChecklistUsu = bitacoraCheckUsuAdmBI.insertaCheckUsuario(checkUsu);

                if (idChecklistUsu == 0) {

                    responseGlobal = false;

                }

            }
        } catch (Exception e) {

            logger.info("" + e);
            json = "{ }";

        }

        if (responseGlobal) {

            json = "{\"response\":true}";

        } else {

            json = "{\"response\":false}";

        }

        return json;

    }

    // http://localhost:8080/checklist/catalogosAdmService/altaCheckVersAdm.json?check=<?>&version=<?>&deta=<?>&status=<?>&nego=<?>&obs=
    @RequestMapping(value = "/altaCheckVersAdm", method = RequestMethod.GET)
    public ModelAndView altaCheckVersAdm(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int check = Integer.parseInt(request.getParameter("check"));
            int version = Integer.parseInt(request.getParameter("version"));
            String deta = request.getParameter("deta");
            int status = Integer.parseInt(request.getParameter("status"));
            String nego = request.getParameter("nego");
            String obs = request.getParameter("obs");

            VersionExpanDTO ver = new VersionExpanDTO();

            ver.setIdCheck(check);
            ver.setIdVers(version);
            ver.setAux(deta);
            ver.setIdactivo(status);
            ver.setNego(nego);
            ver.setObs(obs);

            int resp = versionCheckAdmBI.inserta(ver);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", " Inserta versionCheckADM-->");
            mv.addObject("res", resp);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/catalogosAdmService/altaCheckVersAdmNego.json?&version=<?>&deta=<?>&status=<?>&nego=<?>&fechalibera=&obs=
    @RequestMapping(value = "/altaCheckVersAdmNego", method = RequestMethod.GET)
    public ModelAndView altaCheckVersAdmNego(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            int version = Integer.parseInt(request.getParameter("version"));
            int status = Integer.parseInt(request.getParameter("status"));
            String nego = request.getParameter("nego");
            String obs = request.getParameter("obs");
            String fechalibera = request.getParameter("fechalibera");

            VersionExpanDTO ver = new VersionExpanDTO();

            ver.setIdVers(version);
            ver.setIdactivo(status);
            ver.setNego(nego);
            ver.setObs(obs);
            ver.setFechaLibera(fechalibera);

            int resp = versionCheckAdmBI.insertaNegoVer(ver);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", " Inserta versionNegoCheckADM-->");
            mv.addObject("res", resp);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
    // http://localhost:8080/checklist/catalogosAdmService/cargaCheckVersAdmNego.json?&protocolo=

    @RequestMapping(value = "/cargaCheckVersAdmNego", method = RequestMethod.GET)
    public ModelAndView cargaCheckVersAdmNego(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String protocolo = request.getParameter("protocolo");

            boolean resp = versionCheckAdmBI.cargaVesionesTab(protocolo);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", " Inserta VersionesTabsPaso-->");
            mv.addObject("res", resp);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
    // http://localhost:8080/checklist/catalogosAdmService/cargaCheckVersAdmNegoProd.json?&protocolo=

    @RequestMapping(value = "/cargaCheckVersAdmNegoProd", method = RequestMethod.GET)
    public ModelAndView cargaCheckVersAdmNegoProd(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String protocolo = request.getParameter("protocolo");

            boolean resp = versionCheckAdmBI.cargaVesionesTabProd(protocolo);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", " Inserta VersionesTabsPaso-->");
            mv.addObject("res", resp);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/checklist/catalogosAdmService/postAsignaValidacion.json?setActivo=1&idCeco=<?>&idVersion=<?>&idUsu=<?>&fechaLiberacion=<?>
    @RequestMapping(value = "/postAsignaValidacion", method = RequestMethod.POST)

    public @ResponseBody
    String postAsignaValidacion(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "setActivo", required = false, defaultValue = "") String setActivo,
            @RequestParam(value = "fechaIni", required = false, defaultValue = "") String fechaIni,
            @RequestParam(value = "fechaResp", required = false, defaultValue = "") String fechaResp,
            @RequestParam(value = "idCeco", required = false, defaultValue = "") String idCeco,
            @RequestParam(value = "idVersion", required = false, defaultValue = "") String idVersion,
            @RequestParam(value = "idUsu", required = false, defaultValue = "") String idUsu,
            @RequestParam(value = "fechaLiberacion", required = false, defaultValue = "") String fechaLiberacion)
            throws KeyException, GeneralSecurityException, IOException {

        String json = "";
        String idVersionParam = idVersion;
        boolean responseGlobal = true;
        int idChecklistUsu = 0;

        try {

            List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkVersParam(idVersionParam, "0");

            CheckAutoProtoDTO aut = new CheckAutoProtoDTO();

            aut.setFiversion(Integer.parseInt(idVersion));
            aut.setIdUsu(Integer.parseInt(idUsu));
            aut.setDetalle("Automatico");
            aut.setStatus(1);
            aut.setUsuTipo("" + 1);
            aut.setFechaLiberacion(fechaLiberacion);

            int resp = checkAutProtoBI.insertaAutoProto(aut);

            for (CheckSoporteAdmDTO objLista : lista) {

                String pattern = "yyyyMMdd";
                SimpleDateFormat format = new SimpleDateFormat(pattern);
                String fecha = format.format(new Date().getDate());
                String setActivoParam = setActivo;
                String fechaIniParam = fecha;
                // Fecha de Liberación
                String fechaRespParam = fecha;
                String idUsuParam = request.getParameter("idUsu");

                ChecklistUsuarioDTO checkUsu = new ChecklistUsuarioDTO();
                checkUsu.setActivo(Integer.parseInt(setActivoParam));
                checkUsu.setFechaIni(fechaIniParam);
                checkUsu.setFechaResp(fechaRespParam);
                checkUsu.setIdCeco(Integer.parseInt(idCeco));
                checkUsu.setIdChecklist(objLista.getIdChecklist());
                checkUsu.setIdUsuario(Integer.parseInt(idUsuParam));

                idChecklistUsu = bitacoraCheckUsuAdmBI.insertaCheckUsuario(checkUsu);

                if (idChecklistUsu == 0) {

                    responseGlobal = false;

                }

            }
        } catch (Exception e) {

            logger.info("" + e);
            json = "{ }";

        }

        if (responseGlobal) {

            json = "{\"response\":true}";

        } else {

            json = "{\"response\":false}";

        }

        return json;

    }

}
