package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.ZonaNegoExpDTO;


public interface ZonaNegoDAO {


	// NEGOCIO
	public int insertaNego(ZonaNegoExpDTO bean) throws Exception;

	public boolean eliminaNego(String idTabNegocio) throws Exception;

	public List<ZonaNegoExpDTO> obtieneDatosNego(String idNegocio) throws Exception;

	public List<ZonaNegoExpDTO> obtieneInfoNego(String negocio) throws Exception;

	public boolean actualizaNego(ZonaNegoExpDTO bean) throws Exception;
	// ZONAS

	public int insertaZona(ZonaNegoExpDTO bean) throws Exception;

	public boolean eliminaZona(String idZona) throws Exception;

	public List<ZonaNegoExpDTO> obtieneDatosZona(String idZona) throws Exception;

	public List<ZonaNegoExpDTO> obtieneInfoZona() throws Exception;

	public boolean actualizaZona(ZonaNegoExpDTO bean) throws Exception;

	// RELACION ZONA NEGO
	public int insertaRela(ZonaNegoExpDTO bean) throws Exception;

	public boolean eliminaRela(String idRela) throws Exception;

	public boolean actualizaRela(ZonaNegoExpDTO bean) throws Exception;
	
	public List<ZonaNegoExpDTO> obtieneInfoRela() throws Exception;

	}