package com.gruposalinas.checklist.domain;

public class AsignaExpDTO {
	


		private int ceco;
		private int version;
		private int usuario;
		private int usuario_asig;
		private int idestatus;
		private String obs;
		private String periodo;
		private int aux;
		private String aux2;
		private String cec;
		
		public int getCeco() {
			return ceco;
		}
		public void setCeco(int ceco) {
			this.ceco = ceco;
		}
		public int getVersion() {
			return version;
		}
		public void setVersion(int version) {
			this.version = version;
		}
		public int getUsuario() {
			return usuario;
		}
		public void setUsuario(int usuario) {
			this.usuario = usuario;
		}
		public int getUsuario_asig() {
			return usuario_asig;
		}
		public void setUsuario_asig(int usuario_asig) {
			this.usuario_asig = usuario_asig;
		}
		public int getIdestatus() {
			return idestatus;
		}
		public void setIdestatus(int idestatus) {
			this.idestatus = idestatus;
		}
		public String getObs() {
			return obs;
		}
		public void setObs(String obs) {
			this.obs = obs;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		public int getAux() {
			return aux;
		}
		public void setAux(int aux) {
			this.aux = aux;
		}
		public String getAux2() {
			return aux2;
		}
		public void setAux2(String aux2) {
			this.aux2 = aux2;
		}
		
		
		public String getCec() {
			return cec;
		}
		public void setCec(String cec) {
			this.cec = cec;
		}
		@Override
		public String toString() {
			return "AsignaExpDTO [ceco=" + ceco + ", version=" + version + ", usuario=" + usuario + ", usuario_asig="
					+ usuario_asig + ", idestatus=" + idestatus + ", obs=" + obs + ", periodo=" + periodo + ", aux="
					+ aux + ", aux2=" + aux2 + ", cec=" + cec + "]";
		}
	
		
	}