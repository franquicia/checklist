package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ChecklistNegocioDAO;
import com.gruposalinas.checklist.domain.ChecklistNegocioDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class ChecklistNegocioBI {
	
	private Logger logger = LogManager.getLogger(ChecklistNegocioBI.class);
	
	@Autowired
	ChecklistNegocioDAO checklistNegocioDAO;
	
	public List<ChecklistNegocioDTO> obtieneChecklistNegocio(String checklist, String negocio ){
		
		List<ChecklistNegocioDTO> consulta = null;
		
		try {
			consulta = checklistNegocioDAO.obtieneChecklistNegocio(checklist, negocio);
		} catch (Exception e) {
			logger.info("No fue posible obtener informacion Checklist-Negocio");
				
		}
		
		return consulta;
	}
	
	public boolean insertaChecklistNegocio(int checklist, int negocio){
		boolean respuesta = false;
		
		try {
			respuesta = checklistNegocioDAO.insertaChecklistNegocio(checklist, negocio);
		} catch (Exception e) {
			logger.info("No fue posible insertar informacion Checklist-Negocio");
				
		}
		
		return respuesta;
	}
	
	public boolean eliminaChecklistNegocio(int checklist, int negocio){
		boolean respuesta = false;
		
		try {
			respuesta = checklistNegocioDAO.eliminaChecklistNegocio(checklist, negocio);
		} catch (Exception e) {
			logger.info("No fue posible eliminar informacion Checklist-Negocio");
				
		}
		
		return respuesta;
	}

}
