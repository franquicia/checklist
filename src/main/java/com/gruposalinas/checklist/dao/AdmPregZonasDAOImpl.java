package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AdmPregZonaDTO;
import com.gruposalinas.checklist.mappers.AdmPregZonasRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AdmPregZonasDAOImpl extends DefaultDAO implements AdmPregZonasDAO {

    private static Logger logger = LogManager.getLogger(AdmPregZonasDAOImpl.class);

    DefaultJdbcCall jdbcObtienePregZonaById;
    DefaultJdbcCall jdbcInsertaPregZona;
    DefaultJdbcCall jdbcActualizaPregZona;
    DefaultJdbcCall jdbcEliminaPregZona;

    public void init() {
        jdbcObtienePregZonaById = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINPREGZONA")
                .withProcedureName("SP_SEL_PREGZONA")
                .returningResultSet("RCL_INFO", new AdmPregZonasRowMapper());

        jdbcInsertaPregZona = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINPREGZONA")
                .withProcedureName("SP_INS_PREGZONA");

        jdbcActualizaPregZona = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINPREGZONA")
                .withProcedureName("SP_ACT_PREGZONA");

        jdbcEliminaPregZona = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINPREGZONA")
                .withProcedureName("SP_DEL_PREGZONA");
    }

    public boolean insertaPregZona(AdmPregZonaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        boolean resp = false;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIIDPREG", bean.getIdPreg())
                .addValue("PA_FIIDZONA", bean.getIdZona());

        out = jdbcInsertaPregZona.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICA.PAADMINPREGZONA.SP_INS_PREGZONA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar la pregunta zona");
        } else {
            resp = true;
        }

        return resp;
    }

    public boolean actualizaPregZona(AdmPregZonaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        boolean resp = false;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIIDPREG", bean.getIdPreg())
                .addValue("PA_FIIDZONA", bean.getIdZona())
                .addValue("PA_FIIDPRZO", bean.getIdPregZona());

        out = jdbcActualizaPregZona.execute(in);
        logger.info("Funcion ejecutada: {FRANQUICA.PAADMINPREGZONA.SP_ACT_PREGZONA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al actualizar la pregunta zona: " + bean.getIdZona());
        } else {
            resp = true;
        }

        return resp;
    }

    public boolean eliminaPregZona(AdmPregZonaDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        boolean resp = false;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIIDPRZO", bean.getIdPregZona());

        out = jdbcEliminaPregZona.execute(in);
        logger.info("Funcion ejecutada: {FRANQUICA.PAADMINPREGZONA.SP_DEL_PREGZONA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al borrar la pregunta zona: " + bean.getIdZona());
        } else {
            resp = true;
        }

        return resp;
    }

    @SuppressWarnings("unchecked")
    public ArrayList<AdmPregZonaDTO> obtienePregZonaById(AdmPregZonaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        ArrayList<AdmPregZonaDTO> zona = new ArrayList<>();

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIIDPREG", bean.getIdPreg())
                .addValue("PA_FIIDZONA", bean.getIdZona())
                .addValue("PA_FIIDPRZO", bean.getIdPregZona());

        out = jdbcObtienePregZonaById.execute(in);
        logger.info("Funcion ejecutada: {FRANQUICA.PAADMINPREGZONA.SP_SEL_PREGZONA}");
        zona = (ArrayList<AdmPregZonaDTO>) out.get("RCL_INFO");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al consultar la pregunta zona: " + bean.getIdZona());
        }

        return zona;
    }

}
