package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.PlantillaDTO;

public class PlatillaRowMapper implements RowMapper<PlantillaDTO>{

	public PlantillaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
	    
		PlantillaDTO plantillaDTO = new PlantillaDTO();
		
		plantillaDTO.setIdPlantilla(rs.getInt("FIID_PLANTILLA"));
		plantillaDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
		
		return plantillaDTO;
}
	

}
