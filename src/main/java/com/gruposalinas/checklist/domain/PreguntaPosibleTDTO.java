package com.gruposalinas.checklist.domain;

public class PreguntaPosibleTDTO {

	private int idPregunta;
	private int idPosiblePreg;
	private String numeroRevision;
	private String tipoCambio;
	private int commit;
	
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public int getIdPosiblePreg() {
		return idPosiblePreg;
	}
	public void setIdPosiblePreg(int idPosiblePreg) {
		this.idPosiblePreg = idPosiblePreg;
	}
	public String getNumeroRevision() {
		return numeroRevision;
	}
	public void setNumeroRevision(String numeroRevision) {
		this.numeroRevision = numeroRevision;
	}
	public String getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public int getCommit() {
		return commit;
	}
	public void setCommit(int commit) {
		this.commit = commit;
	}
	
	
}
