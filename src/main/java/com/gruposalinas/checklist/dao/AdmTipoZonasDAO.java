package com.gruposalinas.checklist.dao;

import java.util.ArrayList;
import java.util.List;
import com.gruposalinas.checklist.domain.AdmTipoZonaDTO;

public interface AdmTipoZonasDAO {
	
	public ArrayList<AdmTipoZonaDTO> obtieneTipoZonaById(AdmTipoZonaDTO bean) throws Exception;
	
	public boolean insertaTipoZona(AdmTipoZonaDTO bean) throws Exception;

	public boolean actualizaTipoZona(AdmTipoZonaDTO bean) throws Exception;
	
	public boolean eliminaTipoZona(AdmTipoZonaDTO bean) throws Exception;
}
