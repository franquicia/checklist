/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.business;

import com.gruposalinas.checklist.dao.ListaChkAsignDAO;
import com.gruposalinas.checklist.domain.ListaAsignChkDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author leodan1991
 */
public class ListaChkAsignBI {
   @Autowired
   ListaChkAsignDAO listaChkAsignDAO;
   
   
   public List<ListaAsignChkDTO> getListaAsginChk(int idUsuario){
       List<ListaAsignChkDTO> lista=null;
       try{
           lista=listaChkAsignDAO.getListaAsignaciones(idUsuario);
       }catch(Exception e){
           e.printStackTrace();
           lista=null;
       }
       
       
       return lista;
   }
           
    
    
}
