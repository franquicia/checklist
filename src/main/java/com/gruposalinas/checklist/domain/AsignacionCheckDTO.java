package com.gruposalinas.checklist.domain;

public class AsignacionCheckDTO {
	private int idAsigna;
	private int idUsuario;
	private String nombreUsuario;
	private String nombreSucursal;
	private String idSucursal;
	private String fecha;
	private int bunker;
	private int idChecklist;
	private String horaBunker;
	private String horaBunkerSalida;
	private String justificacion;
	private int asigna;
	private String envio;
	private String correoD;
	private String correoC;
	
	public int getIdAsigna() {
		return idAsigna;
	}
	public void setIdAsigna(int idAsigna) {
		this.idAsigna = idAsigna;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombreSucursal() {
		return nombreSucursal;
	}
	public void setNombreSucursal(String nombreSucursal) {
		this.nombreSucursal = nombreSucursal;
	}
	public String getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(String idSucursal) {
		this.idSucursal = idSucursal;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getBunker() {
		return bunker;
	}
	public void setBunker(int bunker) {
		this.bunker = bunker;
	}
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public String getHoraBunker() {
		return horaBunker;
	}
	public void setHoraBunker(String horaBunker) {
		this.horaBunker = horaBunker;
	}
	public String getHoraBunkerSalida() {
		return horaBunkerSalida;
	}
	public void setHoraBunkerSalida(String horaBunkerSalida) {
		this.horaBunkerSalida = horaBunkerSalida;
	}
	public String getJustificacion() {
		return justificacion;
	}
	public void setJustificacion(String justificacion) {
		this.justificacion = justificacion;
	}
	public int getAsigna() {
		return asigna;
	}
	public void setAsigna(int asigna) {
		this.asigna = asigna;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getEnvio() {
		return envio;
	}
	public void setEnvio(String envio) {
		this.envio = envio;
	}
	public String getCorreoD() {
		return correoD;
	}
	public void setCorreoD(String correoD) {
		this.correoD = correoD;
	}
	public String getCorreoC() {
		return correoC;
	}
	public void setCorreoC(String correoC) {
		this.correoC = correoC;
	}
	@Override
	public String toString() {
		return "AsignacionCheckDTO [idAsigna=" + idAsigna + ", idUsuario=" + idUsuario + ", nombreUsuario="
				+ nombreUsuario + ", nombreSucursal=" + nombreSucursal + ", idSucursal=" + idSucursal + ", fecha="
				+ fecha + ", bunker=" + bunker + ", idChecklist=" + idChecklist + ", horaBunker=" + horaBunker
				+ ", horaBunkerSalida=" + horaBunkerSalida + ", justificacion=" + justificacion + ", asigna=" + asigna
				+ ", envio=" + envio + ", correoD=" + correoD + ", correoC=" + correoC + "]";
	}	
}
