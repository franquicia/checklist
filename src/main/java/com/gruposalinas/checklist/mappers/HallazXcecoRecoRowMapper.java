package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.PreguntaCheckDTO;

public class HallazXcecoRecoRowMapper implements RowMapper<ChecklistPreguntasComDTO> {

	public ChecklistPreguntasComDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		

		ChecklistPreguntasComDTO com = new ChecklistPreguntasComDTO();

	    com.setIdResp(rs.getInt("FIID_RESPUESTA"));
        com.setIdBita(rs.getInt("FIID_BITACORA"));
	    com.setObserv(rs.getString("FCOBSERVACIONES"));
	    com.setIdCheck(rs.getInt("FIID_CHECKLIST"));
	    com.setIdPosible(rs.getInt("FCRESPUESTA"));
	    com.setPosible(rs.getString("POSIBLE"));
	    com.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	   // com.setIdArbol(rs.getInt("FIID_ARBOL_DES"));

	    com.setPregPadre(rs.getInt("FIID_PREG_PADRE")); 
	    com.setIdBitaGral(rs.getInt("FIID_BITGRAL"));  
	    com.setPregunta(rs.getString("FCDESCRIPCION"));
	    com.setRuta(rs.getString("FCRUTA"));
	    com.setCeco(rs.getString("FCID_CECO")); 
	    com.setIdcritica(rs.getInt("FICRITICA"));
	    com.setClasif(rs.getString("FCCLASIFICA"));
	    com.setNombCeco(rs.getString("FCNOMBRE"));
	    com.setFechaIni(rs.getString("FDFECHA_FIN"));
	    com.setLiderObra(rs.getString("LIDEROBRA"));
	    com.setRealizoReco(rs.getString("USUARIO"));
	    com.setEco(rs.getInt("ECO"));
	   // com.setArea(rs.getString("AREA"));
	  
		return com;
	}
}



