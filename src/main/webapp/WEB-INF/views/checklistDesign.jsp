<!-- 
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<link href="../css/checklistDesign.css"
	rel="stylesheet" type="text/css" />

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Diseño del checklist[checklist]</title>
<script
	src="../js/jquery/jquery-2.2.4.min.js"></script>
<script type="text/javascript">
	var preguntas = new Array();
	var tipoPreguntas = new Array();
	var modulos = new Array();

	function mostrarTitulo() {
		document.getElementById("tituloChecklist").value = document
				.getElementById("nombreChecklist").value;
	}
	function cambiarFondo(indice) {
		//document.getElementById('prueba').style.background='#CCCCCC';
		//#E5F0D9
		//objeto.style.background='#E5F0D9';
		var elemento = "lista" + indice;

		document.getElementById("lista1").style = "background:#fff;";
		document.getElementById("lista2").style = "background:#fff;";
		//document.getElementById("lista3").style = "background:#fff;";

		//document.getElementById(elemento).style = "background:rgb(217,240,229);";

		document.getElementById(elemento).style = "background:rgb(217, 240, 229);";

		return false;

	}
	function mostrarDiv(nombreDiv) {

		if (document.getElementById(nombreDiv).style.display == 'none') {

			document.getElementById(nombreDiv).style.display = 'block';
		} else {

			document.getElementById(nombreDiv).style.display = 'none';
		}

		//objetoDiv.style.display = 'none';
		//alert("se oculto el div");

		return false;

	}

	$(document)
			.ready(
					function() {
						$("#agregarPregunta")
								.click(
										function() {

											//alert("mensaje");
											var nombrePregunta = $(
													"#preguntaText").val();
											var tipoPreg = $("#tipoPreg span")
													.text();
											var modulo = $("#modulo span")
													.text();

											if (tipoPreg == "Tipo de pregunta") {
												alert("No has seleccionado el tipo de pregunta");
												return false;
											}
											if (modulo == "Categoria") {
												alert("No has seleccionado alguna categoria de la pregunta");
												return false;
											}
											if (nombrePregunta.trim() == "") {
												alert("El nombre de la pregunta no puede ser un campo vacio");
												return false;
											}

											//se agregan la informacion a las listas
											preguntas.push(nombrePregunta);
											tipoPreguntas.push(tipoPreg);
											modulos.push(modulo);

											//alert("PREGUNTAS: "+preguntas+"<br>TIPO PREGUNTAS: "+tipoPreguntas+"<br>MODULOS: "+modulos); 

											var vistaPreviaRespuesta = "";
											if (tipoPreg == "SI-NO") {
												vistaPreviaRespuesta = "<ul>";
												vistaPreviaRespuesta += "<li>SI</li>";
												vistaPreviaRespuesta += "<li>NO</li>";
												vistaPreviaRespuesta += "</ul>";
											}

											if (tipoPreg == "SI-NO-N/A") {
												vistaPreviaRespuesta = "<ul>";
												vistaPreviaRespuesta += "<li>SI</li>";
												vistaPreviaRespuesta += "<li>NO</li>";
												vistaPreviaRespuesta += "<li>N/A</li>";
												vistaPreviaRespuesta += "</ul>";
											}
											if (tipoPreg == "Abierta") {
												vistaPreviaRespuesta = "<ul>";
												vistaPreviaRespuesta += "<li>Abierta</li>";
												vistaPreviaRespuesta += "</ul>";
											}
											//se agrega la pregunta a la lista de vista previa
											var $myNewElement = $("<li id='preg"
													+ (preguntas.length - 1)
													+ "'>"
													+ nombrePregunta
													+ " - "
													+ modulo
													+ vistaPreviaRespuesta
													+ "</li>");
											$("#preguntas").append(
													$myNewElement);

											return false;

										});
					}

			);
	/*codigo de jquery para el combo personalizado*/
	$(document)
			.ready(
					function() {
						function clickcaja(e) {
							var lista = $(this).find("ul"), triangulo = $(this)
									.find("span:last-child");
							e.preventDefault();
							//lista.is(":hidden") ? $(this).find("ul").show() : $(this).find("ul").hide();
							$(this).find("ul").toggle();
							if (lista.is(":hidden")) {
								triangulo.removeClass("triangulosup").addClass(
										"trianguloinf");
							} else {
								triangulo.removeClass("trianguloinf").addClass(
										"triangulosup");
							}
						}
						function clickli(e) {
							var texto = $(this).text(), seleccionado = $(this)
									.parent().prev(), lista = $(this).closest(
									"ul"), triangulo = $(this).parent().next();
							e.preventDefault();
							e.stopPropagation();
							seleccionado.text(texto);
							lista.hide();
							triangulo.removeClass("triangulosup").addClass(
									"trianguloinf");
						}
						$(".cajaselect").click(clickcaja);
						$(".cajaselect").on("click", "li", clickli);
					});
</script>
</head>
<body>
--><form:form name="formulario" method="POST"	action="checklistDesign.htm" commandName="command">
		
	<div class="contenedor" id="contenedor">
		<div id="definicionPregunta" class="seccionD">
			<h3>Diseño de checklist</h3> 
			
			<ul class="listaAgregarPregunta">
				<li id="lista1"><input type="text" id="nombreChecklist"
					placeholder="Titulo" class="textBoxEstilo"
					onkeyup="mostrarTitulo();" onblur="mostrarTitulo();" onfocus="cambiarFondo(1);" style="width:95%; text-align:center;"/>
				</li>
					
				<li id="lista2">
					
						
					<div id="tipoChecklist" class="caja" style="margin-left: 10px;">
						<span class="seleccionado" id="-1">Tipo de checklist</span>
						<ul class="listaselect">
							<li value="-1"><a href="#">Tipo de checklist</a></li>
							<li value="-2" ><a href="#">Agregar nuevo</a></li>
						
							
							</li>
						</ul>
						<span class="trianguloinf"></span>
					</div>
					
					<div class="tooltip"  style="width: 25px;">
						<span class="tooltiptext">Editar</span>
						<button class='botonEditar'
							onclick="return activarEdicionTipoChecklist();"
							style="position: relative; right:0%; margin:0px;"></button>
					</div>
					
					<div class="tooltip"  style="width: 25px; margin:5px;">
						<span class="tooltiptext">Eliminar</span>
						<button class='botonEliminar' 
							onclick="return eliminarTipoChecklist();"
							style="position: relative; right:0%; margin:0px;"></button>
					</div>									
					
					
						
					
					<div class="tooltip" >
						<span class="tooltiptext">Fecha de inicio</span>
						<input type="text" id="fechaInicio" class="textBoxEstilo" placeholder="Fecha de inicio" onfocus="cambiarFondo(2);" style="width: 100%; "/>
					</div>
					
					<div class="tooltip" >
						<span class="tooltiptext">Fecha de termino</span>
						<input type="text" id="fechaTermino" class="textBoxEstilo" placeholder="Fecha de termino" onfocus="cambiarFondo(2);" style="width: 100%;" />
					</div>
					
						
						
					<div id="horario" class="caja" style="margin-left: 10px; width:23%;">
						<span class="seleccionado" id="-1">Selecciona el horario</span>
						<ul class="listaselect">
							<li value="-1"><a href="#">Selecciona el horario</a></li>
							<c:forEach items="${horarios}" var="item">
								<li value="${item.idHorario }"><a href="#">${item.cveHorario}: ${item.valorIni}-${item.valorFin}</a></li>
							</c:forEach>
						</ul>
						<span class="trianguloinf"></span>
					</div>
					
					<!-- 
					<div class="tooltip" >
					<span class="tooltiptext">Hora de inicio</span>
					<input type="text" id="horaInicio" placeholder="Hora de inicio" class=" textBoxEstilo"  style="width: 100%; "  onfocus="cambiarFondo(2);  eventoHoraInicio();"/>
					</div>
					
					
					<div class="tooltip" >
						<span class="tooltiptext">Hora de termino</span>
						<input type="text" id="horaTermino"  placeholder="Hora de termino" class=" textBoxEstilo"  style="width: 100%; "  onfocus="cambiarFondo(2); eventoHoraTermino()"/>
					</div>
					-->
					
					<br>
					<input type="text" id="nuevoTipoChecklist" 
						class="textBoxEstilo" placeholder="Nombre del tipo" 
						onfocus="cambiarFondo(2);" style="width:20%; display:none;"  />
						
					<button id="agregarTipoChecklist" class="botonAgregar" style="display:none;" onclick="return agregarNuevoTipoChecklist();">+</button>
				
				</li>
					

				<li id="lista3">
					<hr style="width:100%;">
					<h3>Nueva pregunta</h3>
					<input type="text" id="preguntaText" placeholder="Pregunta" class="textBoxEstilo"
						onfocus="cambiarFondo(3);" /> <br>

					<div id="modulo" class="caja" style="margin-left: 10px; margin-bottom:10px;width:200px;">
						<span class="seleccionado" id="-1">Categoria</span>
						<ul class="listaselect">
							<li value="-1"><a href="#">Categoria</a></li>
							<li value="-2"><a href="#">Agregar nuevo</a></li>
							
						</ul>
						<span class="trianguloinf"></span>
					</div>
								
					<div class="tooltip"  style="width: 25px;">
						<span class="tooltiptext">Editar</span>
						<button class='botonEditar'
						onclick="return activarEdicionModulo();"
						 style="position: relative; right:0%; margin:0px;"></button>
					</div>
					
					<div class="tooltip"  style="width: 25px; margin:5px;">
						<span class="tooltiptext">Eliminar</span>
						<button class='botonEliminar' 
							onclick="return eliminarModulo();"
							style="position: relative; right:0%; margin:0px;"></button>
					</div>									
					
					

					<div id="tipoPreg" class="caja" style="margin-left: 10px; margin-bottom:10px;">
						<span class="seleccionado" id="-1">Tipo de pregunta</span>
						<ul class="listaselect">
							<li value="-1"><a href="#">Tipo de pregunta</a></li>
							<c:forEach items="${tiposPregunta}" var="item">	
								<li value="${item.idTipoPregunta }"><a href="#">${item.descripcion}</a></li>
							</c:forEach>
						</ul>
						<span class="trianguloinf"></span>
					</div>
					
					<button id="agregarPregunta" class="botonAgregar" style="top:80%; position: absolute; right:43%;">+</button>
					
					<br>
					
					<input type="text" id="nuevoModulo" 
						class="textBoxEstilo" placeholder="Nombre de la categoria" 
						onfocus="cambiarFondo(3);" style="width:20%; display:none;"
						 />
						
						<div id="moduloPadre" class="caja" style="margin-left: 10px; margin-bottom:10px;display:none;">
						<span class="seleccionado" id="0">Sin modulo padre</span>
						<ul class="listaselect">
							<li value="0"><a href="#">Sin modulo padre</a></li>
							
						</ul>
						<span class="trianguloinf"></span>
					</div>
					<button id="agregarNuevoModulo" class="botonAgregar" style="display:none;" onclick="return agregarNuevoMod();">+</button>
					
					
					</li>
					
			</ul>

		</div>
		<br>
		<div id="vistaPrevia" class="seccionVP">
			<h3>Vista previa</h3>

			<input type="text" id="tituloChecklist" disabled placeholder="Titulo"
				class="textBoxEstilo" style="font-weight: 700; color: #000;" />
			<!-- lista donde se mostran las preguntas de la vista previa -->
			<h3>Preguntas</h3>
			<ul id="preguntas" style="list-style-type: decimal;">
			</ul>

		</div>



		<!-- contenedor para mostrar el formulario de la definicion del arbol de decision -->
		<div class="seccionArbol" id="contenedorArbol"
			style="display: none; background: #fff; z-index: 0;">
			<h3>Configuración de preguntas</h3> 
			<input type="text" id="tituloChecklistArbol" disabled
				placeholder="Titulo" class="textBoxEstilo"
				style="font-weight: 700; color: #000;" />
			<!-- lista donde se mostran las preguntas de la vista previa -->
			<h3>Preguntas</h3>

			<table class="tabla
			">
			<thead >
				<tr class="encabezado">
				<th>Respuesta</th>
				<th>Evidencia</th>
				<th style='width:190px;'>Tipo de evidencia</th>
				<th>Evidencia obligatoria</th>
				<th>Observaciones</th>
				<th>Acciones</th>
				<th style='width:190px;'>Pregunta siguiente</th>
				</tr>
			
			</thead>
				<tbody id="preguntasArbol">
				
				</tbody>
			</table>
			<!--  
			<ul id="preguntasArbol">
			</ul>
			-->
			
		</div>
		
		
		<div class="seccionAsignacion" id="contenedorAsignacion"
			style="display: none; background: #fff; z-index: 0; ">
			
			<h3>Asignar checklist</h3>
			<ul style="color:#000;" class="opciones">
				<li>Negocio 
				
					<div id="negocio" class="caja" style="margin-left: 10px;">
						<span class="seleccionado" id="-1">Elige una opcion</span>
						<ul class="listaselect">
							<li value="-1"><a href="#">Elige una opcion</a></li>
							<li value="1"><a href="#">Banco Azteca</a></li>
							<li value="2"><a href="#">Elektra</a></li>
							<li value="3"><a href="#">Italika</a></li>
						</ul>
						<span class="trianguloinf"></span>
					</div>
				
					
				</li>
				<li>Geografia
					<ul>
					
						<li style="text-indent: 5em;">Territorio
							<div id="territorio" class="caja" style="margin-left: 40px;">
							<span class="seleccionado">Elige una opcion</span>
							<ul class="listaselect">
								<li><a href="#">Elige una opcion</a></li>
								<li><a href="#">Territorio 1</a></li>
								<li><a href="#">Territorio 2</a></li>
								<li><a href="#">Territorio 3</a></li>
							</ul>
							<span class="trianguloinf"></span>
							</div>
						
						<button id="agregarPregunta" class="botonAgregar">+</button></li>
						<li style="text-indent: 6em;">Zona 
						<div id="zona" class="caja" style="margin-left: 50px;">
							<span class="seleccionado">Elige una opcion</span>
							<ul class="listaselect">
								<li><a href="#">Elige una opcion</a></li>
								<li><a href="#">Zona 1</a></li>
								<li><a href="#">Zona 2</a></li>
								<li><a href="#">Zona 3</a></li>
							</ul>
							<span class="trianguloinf"></span>
							</div>
						
						<button id="agregarPregunta" class="botonAgregar">+</button></li>
						<li style="text-indent: 7em;">Region
						<div id="region" class="caja" style="margin-left:25px;">
							<span class="seleccionado">Elige una opcion</span>
							<ul class="listaselect">
								<li><a href="#">Elige una opcion</a></li>
								<li><a href="#">Region 1</a></li>
								<li><a href="#">Region 2</a></li>
								<li><a href="#">Region 3</a></li>
							</ul>
							<span class="trianguloinf"></span>
							</div>
						<button id="agregarPregunta" class="botonAgregar">+</button></li>
						<li style="text-indent: 8em;">Sucursal
						<div id="sucursal" class="caja" style="margin-left: 0px;">
							<span class="seleccionado">Elige una opcion</span>
							<ul class="listaselect">
								<li><a href="#">Elige una opcion</a></li>
								<li><a href="#">Sucursal 1</a></li>
								<li><a href="#">Sucursal 2</a></li>
								<li><a href="#">Sucursal 3</a></li>
							</ul>
							<span class="trianguloinf"></span>
							</div>
						
						<button id="agregarPregunta" class="botonAgregar">+</button></li>
						
					</ul>
				</li>
				<li>Puesto 
				
					<div id="puesto" class="caja" style="margin-left: 10px;">
						<span class="seleccionado">Elige una opcion</span>
						<ul class="listaselect">
							<li><a href="#">Elige una opcion</a></li>
							<li><a href="#">Desarrollador</a></li>
							<li><a href="#">Ejecutivo</a></li>
							<li><a href="#">Gerente</a></li>
						</ul>
						<span class="trianguloinf"></span>
					</div>
				
					
				</li>
				<li>Número de empleado<input type="text" placeholder="Ingresa el numero de empleado"/>	</li>
			</ul>
		
		</div>
		
		<div class="seccionFinal">
		
			<button id="regresar" class="log-btn2 " style="display:none; margin-right:50%;">Regresar</button>
			
			<button id="continuar" class="log-btn2 " style="margin-left:10%;">Continuar</button>
			
		</div>

	</div>

</form:form>
<!-- 	
</body>
</html>

-->