<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script type="text/javascript">
history.forward();
</script>

<!--Definicion de un encabezado-->	

<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/estilos.css">
<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/menuNuevo.css">
<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/dropkickModificado.css">
<link rel="stylesheet" type="text/css" href="../css/procesoCalidad.css">

<body >
	<div class="header">
			<div class="tblHeader">
				<!-- <div class="headerMenu">
					<a href="#" id="hamburger"> <img
						src="../images/supervisionSistemas/btn_hamburguer.svg"
						alt="Men� principal" class="imgHamburgesa">
					</a>
				</div>	 -->
				<div id="cotenidoLogoCabecera" onclick="linkPortalQa();" style="cursor: pointer;">
					<img id="imgProcesosLogo" src="../images/logo_nombre.png">
				</div>	
				<div id="divImagenSalir">
					<a style="cursor:pointer;"><img src="../images/salir.png"></a>
				</div>			
				<div class="headerUser">
					<c:set var="now" value="<%=new java.util.Date()%>" /><fmt:formatDate type="date" dateStyle="short" value="${now}" /><br>
					Bienvenido@ ${user.nombre}
				</div>	
						
			</div>
		</div>

		<div class="clear"></div>
		
		<!-- Menu -->
		<div id="effect" class="mismoalto ui-widget-content ui-corner-all">
			<div id="menuPrincipal">
				<div class="header-usuario">
					<div id=foto>
						<img src="../images/supervisionSistemas/logo-baz2.svg"
							class="imgLogo">
					</div>
					<div id="inf-usuario">
						<span>Cuadre de inventario f�sico</span>
						<p>
							Men� Principal<br>
					</div>
				</div>
				<div class="c-mright" id="menuNuevo">
					<ul class="l-navegacion nivel1">

						<li class="has-sub" style="cursor:pointer;">
							<a>
								<p style="margin: 10px 32px 0px;">�Qu&eacute; es?</p>
							</a>
						</li>
						
						<li class="has-sub" style="cursor:pointer;">
							<a>
								<p style="margin: 10px 32px 0px;">&nbsp;�Qu&eacute; tengo que hacer?</p>
							</a>
						</li>
						<li class="has-sub" style="cursor:pointer;">
							<a>
								<p style="margin: 10px 32px 0px;">�C&oacute;mo lo tengo que hacer?</p>
							</a>
						</li>
						
						<li class="has-sub" style="cursor:pointer;">
							<a>
								<p style="margin: 10px 32px 0px;">�Qu&eacute; tengo que verificar?</p>
							</a>
						</li>

						<!-- <c:if test="${perfilAdmin==0}">	
							<form name="formulario" method="POST" action="../central/resumenCheck.htm" id="formActivaAdmin">							
								<li onclick="resumen()" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Administrador Checklist</p>
									</a>
								</li>
								<input type="hidden" name="idUserAdmin" value="adminCheckchecklists">
							</form>	
						</c:if>	-->

						
						<li class="has-sub" style="cursor:pointer;">
							<a>
								<p style="margin: 10px 32px 0px;">Supervisi�n Sucursales</p>
							</a>
							<ul class="nivel2">
								<!-- <li class="has-sub" onclick="javascript:location.href='reporteOnlineApertura.htm'" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">�C�mo operan mis sucursales?</p></a></li> -->
								<c:if test="${perfilReportes==1}">
									<li class="has-sub" onclick="javascript:location.href='vistaCumplimientoVisitas.htm'" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">�C�mo supervisamos?</p></a></li>
								</c:if>	
								
								<c:if test="${perfilSistemas==1}">									
									<li class="has-sub" onclick="linkSistemas();" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">Supervisi�n Sistemas</p></a></li>
								</c:if>
								<c:if test="${perfilSoporte==1}">		
									<li class="has-sub" onclick="linkSoporte();" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">Supervisi�n Soporte</p></a></li>
								</c:if>
								<c:if test="${perfilImagen==1}">										
									<li class="has-sub" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">Supervisi�n Imagen</p></a></li>
								</c:if>
								<!--<li class="has-sub" onclick="linkImagen();" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">Supervisi�n Imagen</p></a></li> -->
								<c:if test="${perfilSistemas==1}">										
									<li class="has-sub" onclick="linkAsignacion();" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">Asignaci�n de Visitas</p></a></li>
									<li class="has-sub" onclick="linkIncidencias();" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">Reporte Incidencias</p></a></li>
								</c:if>
								
							</ul>
						</li>				
						
						
						<li onclick="javascript:location.href='carrusel7s.htm'" class="has-sub" style="cursor:pointer;">
							<a>
								<p style="margin: 10px 32px 0px;">7S</p>
							</a>
						</li>

					</ul>
				</div>
			</div>
		</div>
		
		
</body>

<script type="text/javascript"
	src="../js/supervisionSistemas/script_reportes.js"></script>

<script type="text/javascript"
	src="../js/supervisionSistemas/jquery-1.12.4.min.js"></script>
<script type="text/javascript"
	src="../js/supervisionSistemas/jquery.dropkick.js"></script>
<script type="text/javascript"
	src="../js/supervisionSistemas/content_height.js"></script>
<script type="text/javascript"
	src="../js/supervisionSistemas/jquery-ui.js"></script>

<script type="text/javascript"
	src="../js/supervisionSistemas/jquery.paginate.js"></script>
	
<script type="text/javascript"
	src="../js/supervisionSistemas/funcionesMenu.js"></script>

<script type="text/javascript">
	$(function() {
		$("#demo5").paginate(
				{
					count : 3,
					start : 1,
					display : 3,
					border : false,
					border_color : false,
					text_color : '#8a8a8a',
					background_color : 'transparent',
					border_hover_color : 'transparent',
					text_hover_color : '#000',
					background_hover_color : 'transparent',
					images : true,
					mouse : 'press',
					onChange : function(page) {
						$('._current', '#paginationdemo').removeClass(
								'_current').hide();
						$('#p' + page).addClass('_current').show();
					}
				});
	});
</script>