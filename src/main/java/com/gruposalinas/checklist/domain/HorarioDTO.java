package com.gruposalinas.checklist.domain;

public class HorarioDTO {

	private int idHorario;
	private String cveHorario;
	private String valorIni;
	private String valorFin;

	
	public int getIdHorario() {
		return idHorario;
	}
	public void setIdHorario(int idHorario) {
		this.idHorario = idHorario;
	}
	public String getCveHorario() {
		return cveHorario;
	}
	public void setCveHorario(String cveHorario) {
		this.cveHorario = cveHorario;
	}
	public String getValorIni() {
		return valorIni;
	}
	public void setValorIni(String valorIni) {
		this.valorIni = valorIni;
	}
	public String getValorFin() {
		return valorFin;
	}
	public void setValorFin(String valorFin) {
		this.valorFin = valorFin;
	}
	
}
