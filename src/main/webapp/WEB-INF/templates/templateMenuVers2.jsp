<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script type="text/javascript">
history.forward();
</script>

<!--Definicion de un encabezado-->	

<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/estilos.css">
<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/menuNuevo.css">
<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/dropkickModificado.css">


<body >
	<div class="header">
			<div class="tblHeader">
				<div class="headerMenu">
					<a href="#" id="hamburger"> <img
						src="../images/supervisionSistemas/btn_hamburguer.svg"
						alt="Men� principal" class="imgHamburgesa">
					</a>
				</div>
				<div onclick="menuPrincipal();" class="headerLogo">
					<a> <img
						src="../images/supervisionSistemas/logo-baz.svg" id="imgLogo">
					</a>
				</div>
				<div class="headerUser">
					<div class="name">
						<b>${user.nombre}</b> <br>
						<c:if test="${user.admin == true}">Administrador</c:if>
						<c:if test="${user.admin == false}">Usuario</c:if>
					</div>
					<div class="pic-name">
						<div class="pic"
							style="background-image: url('https://portal.socio.gs/foto/elektra/empleados/${user.idUsuario}.jpg'), url('../images/supervisionSistemas/person-icon.jpg');"></div>
						<span class="tCenter">0</span>
					</div>
					<div class="user-menu">
						<a href="#"><img
							src="../images/supervisionSistemas/btn_desp_menu.jpg"
							class="configuraciones"></a>
					</div>
					<div class="fecha">
						<c:set var="now" value="<%=new java.util.Date()%>" />
						<b><fmt:formatDate type="date" dateStyle="short"
								value="${now}" /></b><br> <span><fmt:formatDate
								type="time" timeStyle="short" value="${now}" />Hrs. | CDMX</span>
					</div>
				</div>
				<div class="contConfig">
					<div class="divConfig">
						<!-- <a href="#" class="divConfig1 uno">
							<div>Cambiar contrase�a</div>
							<div>
								<img src="../images/supervisionSistemas/ico1.png"
									class="imgConfig imgIco1">
							</div>
						</a>  -->
						
						<a href="cierraSesion.htm" class="divConfig1 dos">
							<div>Cerrar sesi�n</div>
							<div style="height: 29px;">
								<img src="../images/supervisionSistemas/ico2.svg"
									class="imgConfig imgIco2">
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="clear"></div>
		
		<!-- Menu -->
		<div id="effect" class="mismoalto ui-widget-content ui-corner-all">
			<div id="menuPrincipal">
				<div class="header-usuario">
					<div id=foto>
						<img src="../images/supervisionSistemas/logo-baz2.svg"
							class="imgLogo">
					</div>
					<div id="inf-usuario">
						<span></span>
						<p>
							Men� Principal<br>
					</div>
				</div>
				<div class="c-mright" id="menuNuevo">
					<ul class="l-navegacion nivel1">
					
					
						<c:if test="${perfilSupervision != 1 && perfilSupervision != 3 && perfilSupervision != 4 && perfilSupervision != 5}">

						<li class="has-sub" style="cursor:pointer;">
							<a>
								<p style="margin: 10px 32px 0px;">�Qu&eacute; es?</p>
							</a>
						</li>
						
						<li class="has-sub" style="cursor:pointer;">
							<a>
								<p style="margin: 10px 32px 0px;">&nbsp;�Qu&eacute; tengo que hacer?</p>
							</a>
						</li>
						<li class="has-sub" style="cursor:pointer;">
							<a>
								<p style="margin: 10px 32px 0px;">�C&oacute;mo lo tengo que hacer?</p>
							</a>
						</li>
						
						<li class="has-sub" style="cursor:pointer;">
							<a>
								<p style="margin: 10px 32px 0px;">�Qu&eacute; tengo que verificar?</p>
							</a>
						</li>

						<!-- <c:if test="${perfilAdmin==0}">	
							<form name="formulario" method="POST" action="../central/resumenCheck.htm" id="formActivaAdmin">							
								<li onclick="resumen()" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Administrador Checklist</p>
									</a>
								</li>
								<input type="hidden" name="idUserAdmin" value="adminCheckchecklists">
							</form>	
						</c:if>	-->

						
						<li class="has-sub" style="cursor:pointer;">
							<a>
								<p style="margin: 10px 32px 0px;">Supervisi�n Sucursales</p>
							</a>
							<ul class="nivel2">
								<!-- <li class="has-sub" onclick="javascript:location.href='reporteOnlineApertura.htm'" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">�C�mo operan mis sucursales?</p></a></li> -->
								<c:if test="${perfilReportes==1}">
									<li class="has-sub" onclick="javascript:location.href='vistaCumplimientoVisitas.htm'" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">�C�mo supervisamos?</p></a></li>
								</c:if>	
								
								<c:if test="${perfilSistemas==1}">									
									<li class="has-sub" onclick="linkSistemas();" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">Supervisi�n Sistemas</p></a></li>
								</c:if>
								<c:if test="${perfilSoporte==1}">		
									<li class="has-sub" onclick="linkSoporte();" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">Supervisi�n Soporte</p></a></li>
								</c:if>
								<c:if test="${perfilImagen==1}">										
									<li class="has-sub" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">Supervisi�n Imagen</p></a></li>
								</c:if>
								<!--<li class="has-sub" onclick="linkImagen();" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">Supervisi�n Imagen</p></a></li> -->
								<c:if test="${perfilSistemas==1}">										
									<li class="has-sub" onclick="linkAsignacion();" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">Asignaci�n de Visitas</p></a></li>
									<li class="has-sub" onclick="linkIncidencias();" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">Reporte Incidencias</p></a></li>
								</c:if>
								
							</ul>
						</li>				
						
						<li onclick="javascript:location.href='carrusel7s.htm'" class="has-sub" style="cursor:pointer;">
							<a>
								<p style="margin: 10px 32px 0px;">7S</p>
							</a>
						</li>
						
						</c:if>	
						
						<!-- EXPANSION -->
						<c:if test="${perfilSupervision==2}">
						
							<li onclick="#" class="has-sub" style="cursor:pointer;">
								<a>
									<p style="margin: 10px 32px 0px;">Expansi�n</p>
								</a>
									
									
									<ul class="nivel2">
									<!-- <li class="has-sub" onclick="javascript:location.href='reporteOnlineApertura.htm'" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">�C�mo operan mis sucursales?</p></a></li> -->
											<li class="has-sub" onclick="javascript:location.href='seleccionFiltroExpansionPrincipal.htm?busqueda=0'" style="cursor:pointer;"><a><p style="margin: 10px 32px 0;">Reportes</p></a></li>
									</ul>
							</li>
						</c:if>	

						<c:if test="${perfilSupervision==1}">
						<li class="has-sub" style="cursor:pointer;">
						
							<a>
								<p style="margin: 10px 32px 0px;">Reporte Supervisi&oacute;n</p>
							</a>
							<ul class="nivel2" style="height: 350px; overflow-y: auto;">
							
								<!-- <li class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Administrador</p>
									</a>
									<ul class="nivel3" style="height: 250px; overflow-y: auto;">
										<li onclick="javascript:location.href='asignaPerfiles.htm'" class="has-sub" style="cursor:pointer;">
											<a>
												<p style="margin: 10px 32px 0px;">Asignaci�n de Perfiles</p>
											</a>
										</li>
										<li onclick="javascript:location.href='asignaPersonal.htm'" class="has-sub" style="cursor:pointer;">
											<a>
												<p style="margin: 10px 32px 0px;">Asignaci�n de Personal</p>
											</a>
										</li>
										<li onclick="javascript:location.href='asignaCecos.htm'" class="has-sub" style="cursor:pointer;">
											<a>
												<p style="margin: 10px 32px 0px;">Asignaci�n de Cecos</p>
											</a>
										</li>
									</ul>
								</li>	 -->				
								<li onclick="javascript:location.href='reporteCeco.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Reporte Sucursal</p>
									</a>
								</li>
								<li onclick="javascript:location.href='reporteUsuario.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Reporte Asegurador</p>
									</a>
								</li>
								<li onclick="javascript:location.href='reporteAsistencia.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Reporte Asistencia</p>
									</a>
								</li>
								<li onclick="javascript:location.href='indexSupervision.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">B�squeda Sucursal</p>
									</a>
								</li>
								<li onclick="javascript:location.href='busquedaSupervisorSupervision.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">B�squeda Asegurador</p>
									</a>
								</li>
								<li onclick="javascript:location.href='listaSucursales.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Folios de Mantenimiento</p>
									</a>
								</li>
								<!-- <li onclick="javascript:location.href='descargaBase.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Descarga Base de Protocolos</p>
									</a>
								</li> 
								<li onclick="javascript:location.href='getPerfilSuperGenerica.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Perfil Supervisi�n Generica</p>
									</a>
								</li>-->
								<li onclick="javascript:location.href='indexGaleria.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Galer�a de Evidencias</p>
									</a>
								</li>
							</ul>
						</li>
						<!-- 
						<li onclick="javascript:location.href='indexSupervision.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Reporte Supervision 2.0</p>
									</a>
						</li>
						 -->
						</c:if>
						
						<!-- TRANSFORMACION DE SUCURSALES -->
						<c:if test="${perfilSupervision==3}">
						<li class="has-sub" style="cursor:pointer;">						
							<a>
							<p style="margin: 10px 32px 0px;">Reporte Supervisi&oacute;n</p>
							</a>
							
							<ul class="nivel2" style="height: 350px; overflow-y: auto;">
							<li onclick="javascript:location.href='indexGaleria.htm'" class="has-sub" style="cursor:pointer;">
								<a>
								<p style="margin: 10px 32px 0px;">Galer�a de Evidencias</p>
								</a>
							</li>
							<li onclick="javascript:location.href='reporteCeco.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Reporte Sucursal</p>
									</a>
								</li>
							</ul>
						</c:if>	
						
						<!-- PEDESTAL DIGITAL -->
						<c:if test="${perfilSupervision==5}">
						<li class="has-sub" style="cursor:pointer;">						
							<a>
							<p style="margin: 10px 32px 0px;">Reporte Visitas</p>
							</a>
							
							<ul class="nivel2" style="height: 350px; overflow-y: auto;">
							<li onclick="javascript:location.href='reporteCeco.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Reporte Sucursal</p>
									</a>
								</li>
							</ul>
						</c:if>	
						
						<c:if test="${perfilSupervision==4}">
						<li class="has-sub" style="cursor:pointer;">
						
							<a>
								<p style="margin: 10px 32px 0px;">Reporte Supervisi&oacute;n</p>
							</a>
							<ul class="nivel2" style="height: 350px; overflow-y: auto;">
							
								<!-- <li class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Administrador</p>
									</a>
									<ul class="nivel3" style="height: 250px; overflow-y: auto;"> 
										<li onclick="javascript:location.href='asignaPerfiles.htm'" class="has-sub" style="cursor:pointer;">
										
											<a>
												<p style="margin: 10px 32px 0px;">Asignaci�n de Perfiles</p>
											</a>
										</li>
										<li onclick="javascript:location.href='asignaPersonal.htm'" class="has-sub" style="cursor:pointer;">
											<a>
												<p style="margin: 10px 32px 0px;">Asignaci�n de Personal</p>
											</a>
										</li>
										<li onclick="javascript:location.href='asignaCecos.htm'" class="has-sub" style="cursor:pointer;">
											<a>
												<p style="margin: 10px 32px 0px;">Asignaci�n de Cecos</p>
											</a>
										</li>
								 	</ul>
								</li>	 		 -->		
								<li onclick="javascript:location.href='reporteCeco.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Reporte Sucursal</p>
									</a>
								</li>
								<li onclick="javascript:location.href='reporteUsuario.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Reporte Asegurador</p>
									</a>
								</li>
								<li onclick="javascript:location.href='reporteAsistencia.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Reporte Asistencia</p>
									</a>
								</li>
								<li onclick="javascript:location.href='indexSupervision.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">B�squeda Sucursal</p>
									</a>
								</li>
								<li onclick="javascript:location.href='busquedaSupervisorSupervision.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">B�squeda Asegurador</p>
									</a>
								</li>
								<li onclick="javascript:location.href='listaSucursales.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Folios de Mantenimiento</p>
									</a>
								</li>
								<!--  <li onclick="javascript:location.href='descargaBase.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Descarga Base de Protocolos</p>
									</a>
								</li> --> 
								<li onclick="javascript:location.href='getPerfilSuperGenerica.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Perfil Supervisi�n Generica</p>
									</a>
								</li>
								<li onclick="javascript:location.href='indexGaleria.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Galer�a de Evidencias</p>
									</a>
								</li>
							</ul>
						</li>
						<!-- 
						<li onclick="javascript:location.href='indexSupervision.htm'" class="has-sub" style="cursor:pointer;">
									<a>
										<p style="margin: 10px 32px 0px;">Reporte Supervision 2.0</p>
									</a>
						</li>
						 -->
						</c:if>
						
						
					</ul>
				</div>
			</div>
		</div>
		
		
</body>

<script type="text/javascript"
	src="../js/supervisionSistemas/script_reportes.js"></script>

<script type="text/javascript"
	src="../js/supervisionSistemas/jquery-1.12.4.min.js"></script>
<script type="text/javascript"
	src="../js/supervisionSistemas/jquery.dropkick.js"></script>
<script type="text/javascript"
	src="../js/supervisionSistemas/content_height.js"></script>
<script type="text/javascript"
	src="../js/supervisionSistemas/jquery-ui.js"></script>

<script type="text/javascript"
	src="../js/supervisionSistemas/jquery.paginate.js"></script>
	
<script type="text/javascript"
	src="../js/supervisionSistemas/funcionesMenu.js"></script>

<script type="text/javascript">
	$(function() {
		$("#demo5").paginate(
				{
					count : 3,
					start : 1,
					display : 3,
					border : false,
					border_color : false,
					text_color : '#8a8a8a',
					background_color : 'transparent',
					border_hover_color : 'transparent',
					text_hover_color : '#000',
					background_hover_color : 'transparent',
					images : true,
					mouse : 'press',
					onChange : function(page) {
						$('._current', '#paginationdemo').removeClass(
								'_current').hide();
						$('#p' + page).addClass('_current').show();
					}
				});
	});
</script>