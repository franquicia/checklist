package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.AsignaTransfDTO;

public class SucTransfRowMapper implements RowMapper<AsignaTransfDTO> {

	public AsignaTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AsignaTransfDTO AsignaTransfDTO = new AsignaTransfDTO();
		
		AsignaTransfDTO.setCeco(rs.getInt("FCID_CECO"));
		AsignaTransfDTO.setNomCeco(rs.getString("NOMCECO"));
	    AsignaTransfDTO.setProyecto(rs.getInt("FIID_PROYECTO"));
	    AsignaTransfDTO.setUsuario(rs.getInt("FIID_USUARIO"));
	    AsignaTransfDTO.setAgrupador(rs.getInt("FIID_AGRUPA"));
	    AsignaTransfDTO.setIdestatus(rs.getInt("FCSTATUS"));
	    AsignaTransfDTO.setObs(rs.getString("OBSASIG"));
	    AsignaTransfDTO.setFaseAct(rs.getInt("FIID_FASEACT"));
	    AsignaTransfDTO.setNegocio(rs.getString("NEGOCIO"));
	    AsignaTransfDTO.setRegion(rs.getString("FCREGION"));
	    AsignaTransfDTO.setZona(rs.getString("FCZONA"));
	    AsignaTransfDTO.setTerritorio(rs.getString("FCTERRITORIO"));
	    AsignaTransfDTO.setEco(rs.getString("ECO"));
        
		return AsignaTransfDTO;
	}

}


