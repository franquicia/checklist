package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ConsultaVisitasDTO;

public class ConsultaPromedioCalifRowMapper implements RowMapper<ConsultaVisitasDTO> {

	public ConsultaVisitasDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConsultaVisitasDTO a1 = new ConsultaVisitasDTO();
		a1.setFimes(rs.getString("FIMES"));
		a1.setPromedioCalif(rs.getString("FIPROMEDIOCALF"));

	return a1;
	}
	

		

}






