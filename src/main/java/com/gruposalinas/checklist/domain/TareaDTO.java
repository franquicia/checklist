package com.gruposalinas.checklist.domain;

import java.sql.Date;

public class TareaDTO {
	
	private int idTarea;
	private String cveTarea;
	private Date fechaTarea;
	private String strFechaTarea;
	private int activo;
	
	public int getIdTarea() {
		return idTarea;
	}
	public void setIdTarea(int idTarea) {
		this.idTarea = idTarea;
	}
	public String getCveTarea() {
		return cveTarea;
	}
	public void setCveTarea(String cveTarea) {
		this.cveTarea = cveTarea;
	}
	public Date getFechaTarea() {
		return fechaTarea;
	}
	public void setFechaTarea(Date fechaTarea) {
		this.fechaTarea = fechaTarea;
	}
	public String getStrFechaTarea() {
		return strFechaTarea;
	}
	public void setStrFechaTarea(String strFechaTarea) {
		this.strFechaTarea = strFechaTarea;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	
	

}
