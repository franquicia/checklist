package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.PerfilDAO;
import com.gruposalinas.checklist.domain.PerfilDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class PerfilBI {

	private static Logger logger = LogManager.getLogger(PerfilBI.class);
	
	@Autowired
	PerfilDAO perfilDAO;
	
	List<PerfilDTO> listaPerfiles = null;
	List<PerfilDTO> listaperfil = null;
	

	public List<PerfilDTO> obtienePerfil(){
				
		try{
			listaPerfiles = perfilDAO.obtienePerfil();	
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Perfil");
						
		}
				
		return listaPerfiles;
	}
	
	public List<PerfilDTO> obtienePerfil(int idPerfil){
				
		try{
			listaperfil = perfilDAO.obtienePerfil(idPerfil);	
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Perfil");
						
		}
				
		return listaperfil;
	}	
	
	public int insertaPerfil(PerfilDTO perfil) {
		
		int idPerfil = 0;
		
		try {		
			idPerfil = perfilDAO.insertaPerfil(perfil);			
		} catch (Exception e) {
			logger.info("No fue posible insertar el Perfil");
			
		}
		
		return idPerfil;		
	}
	
	public boolean actualizaPerfil (PerfilDTO perfil){
		
		boolean respuesta = false;
				
		try {
			respuesta = perfilDAO.actualizaPerfil(perfil);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Perfil");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaPerfil(int idPerfil){
		
		boolean respuesta = false;
		
		try{
			respuesta = perfilDAO.eliminaPerfil(idPerfil);
		}catch(Exception e){
			logger.info("No fue posible borrar el Perfil");
			
		}
		
		return respuesta;
	}
	
	public int[] agregaNuevoPerfil(){
		
		int [] respuesta = new int[2];
		
		try {
			respuesta = perfilDAO.agregaNuevoPerfil();
		} catch (Exception e) {
			logger.info("No fue posible agregar el perfil a la nueva tabla");
			
		}
		
		return respuesta;
	}
}
