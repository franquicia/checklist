<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">

<head>

<script src="../js/expancion/jquery-3.3.1.js"></script>
<script src="../js/expancion/jquery-ui-1.12.1.js"></script>
<script src="../js/expancion/busquedaSucursal.js" type="text/javascript"></script>
<script src="../js/expancion/jquery.dataTables.js"></script>
<script src="../js/expancion/tabla.js"></script>



<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />

<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/secciones.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/calendarioCaptacion.css"
	media="screen">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/estiloVentana.css" media="screen">


<link rel="stylesheet" type="text/css"
	href="../css/expancion/tabla-filtros.css">


<!-- -->
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/secciones.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/calendarioCaptacion.css"
	media="screen">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/estiloVentana.css" media="screen">

<link rel="stylesheet" type="text/css" href="../css/expancion/modal.css">
<link rel="stylesheet" type="text/css" href="../css/expancion/index.css">

<!-- Owl Stylesheets -->
<link rel="stylesheet" href="../css/expancion/owl.carousel.css">
<link rel="stylesheet" href="../css/expancion/owl.theme.default.css">

<!-- DropkickDos -->
<link rel="stylesheet" type="text/css"
	href="../css/expancion/dropkickDos.css">
<link rel="stylesheet"
	href="../css/expancion/jquery-ui-datepicker.min.css">



</head>

<body>

	<div class="header h90">
		<span class="titulagsBold spBold fz146">REALIZA UNA BÚSQUEDA</span> <span
			class="subSeccion tp30">Selecciona los criterios de búsqueda</span> <a
			href="" class="msucursales"> <span>MIS SUCURSALES</span>
		</a>
	</div>
	<div class="page">
		<div class="contHome top60">
			<div class="conInfo" style="text-align:center;">

				<img src="../images/expancion/docsearch.svg" class="imgsearch" style="margin-left: auto; margin-right: auto; display: block;">
				<div class="w30d">


					<table style="margin-top: 4%; width: 100%;">
						<tbody>
							<tr>
								<td class="etiquetas">
									<div class="spBold bot10a">Información sobre Sucursal</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="ui-widget buscar1">

											<input class="input insuc" id="sucursal" class="buscar1"
												autocomplete="off" placeholder="CECO / NOMBRE SUCURSAL"
												style="width: 100%;">
									</div>
								</td>
							</tr>

						</tbody>
					</table>
				</div>
			</div>
		</div>
		</div>
		<br></br>	
		
		<div class="w15c" style="margin-left: auto; margin-right: auto; display: block;" >
						<div class="tcenter">
							<a id="btnBuscar" href="#" class="btn busc ml20" onclick="getHallazgos();">Buscar</a>
						</div>
		</div>
		

		<div class="marginTotal">
			<div style="width: 80%; margin: auto;"
				style="width: 80%; margin: auto;">${tabla}</div>
		</div>
</body>
<script type="text/javascript"
	src="../js/expancion/jquery.dropkickDos.js"></script>

<script type="text/javascript"
	src="../js/expancion/jquery.dropkickDos.js"></script>


<script type="text/javascript">

	$("#empleado").autocomplete({
		source : '../ajaxAutocompletaUsuario.json'
	});

	$("#sucursal").autocomplete({
		source : '../ajaxAutocompletaSucursal.json'
	});

	function maximo(campo, limite) {
		if (campo.value.length >= limite) {
			campo.value = campo.value.substring(0, limite);
		}
	}

	function getHallazgos(){
		
		var nomCeco = $('#sucursal').val().split(" - ")[1];
	 	var idCeco = $('#sucursal').val().split(" - ")[0];

	 	if (idCeco.length > 0 ){
			$(location).attr("href","getHallazgos.htm?busqueda=1&idCeco="+idCeco)
		}

	 	
	}

	validaCalendario();
</script>
</html>