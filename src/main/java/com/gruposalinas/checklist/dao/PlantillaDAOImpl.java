package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.PlantillaDTO;
import com.gruposalinas.checklist.mappers.ElementoPlantillaRowMapper;
import com.gruposalinas.checklist.mappers.PlatillaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PlantillaDAOImpl extends DefaultDAO implements PlantillaDAO {

    private static Logger logger = LogManager.getLogger(AppDAOImpl.class);

    private DefaultJdbcCall insertaPlantilla;
    private DefaultJdbcCall actualizaPlantilla;
    private DefaultJdbcCall eliminaPlantilla;
    private DefaultJdbcCall buscaPlantilla;
    private DefaultJdbcCall insertaElemento;
    private DefaultJdbcCall actualizaElemento;
    private DefaultJdbcCall eliminaElemento;
    private DefaultJdbcCall buscaElemento;

    public void init() {

        insertaPlantilla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPLANTILLA")
                .withProcedureName("SP_INS_PLANTILLA");

        actualizaPlantilla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPLANTILLA")
                .withProcedureName("SP_ACT_PLANTILLA");

        eliminaPlantilla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPLANTILLA")
                .withProcedureName("SP_DEL_PLANTILLA");

        buscaPlantilla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPLANTILLA")
                .withProcedureName("SP_SEL_PLANTILLA")
                .returningResultSet("RCL_PLANTILLA", new PlatillaRowMapper());

        insertaElemento = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPLANTILLA")
                .withProcedureName("SP_INS_ELEM_PLA");

        actualizaElemento = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPLANTILLA")
                .withProcedureName("SP_ACT_ELEM_PLA");

        eliminaElemento = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPLANTILLA")
                .withProcedureName("SP_DEL_ELEM_PLA");

        buscaElemento = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPLANTILLA")
                .withProcedureName("SP_SEL_ELEM_PLA")
                .returningResultSet("RCL_ELEMENTO", new ElementoPlantillaRowMapper());

    }

    @Override
    public List<PlantillaDTO> obtienePlantilla(String idPlantilla) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<PlantillaDTO> listaPlantilla = null;
        //System.out.println("kk");
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPLANTILLA", idPlantilla);

        out = buscaPlantilla.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_SEL_PLANTILLA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();
        //System.out.println(error);
        listaPlantilla = (List<PlantillaDTO>) out.get("RCL_PLANTILLA");

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return listaPlantilla;
        }

        return null;
    }

    @Override
    public int insertaPlantilla(PlantillaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int id = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_DESCRIPCION", bean.getDescripcion());

        out = insertaPlantilla.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_INS_PLANTILLA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        BigDecimal returnId = (BigDecimal) out.get("PA_PLANTILLA");
        id = returnId.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar");
        } else {
            return id;
        }

        return 0;

    }

    @Override
    public boolean actualizaPlantilla(PlantillaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_PLANTILLA", bean.getIdPlantilla());

        out = actualizaPlantilla.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_ACT_PLANTILLA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaPlantilla(int idPlantilla) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_PLANTILLA", idPlantilla);

        out = eliminaPlantilla.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_DEL_PLANTILLA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public List<PlantillaDTO> obtieneElementoP(String idPlantilla, String idElemento, String tipo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<PlantillaDTO> listaPlantilla = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPLANTILLA", idPlantilla).addValue("PA_ID_ELEM", idElemento).addValue("PA_TIPO", tipo);

        out = buscaElemento.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_SEL_ELEM_PLA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        listaPlantilla = (List<PlantillaDTO>) out.get("RCL_ELEMENTO");

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return listaPlantilla;
        }

        return null;
    }

    @Override
    public int insertaElementoP(PlantillaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int id = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ETIQUETA", bean.getEtiqueta())
                .addValue("PA_ORDEN", bean.getOrden())
                .addValue("PA_OBLIGA", bean.getObligatorio())
                .addValue("PA_IDPLAN", bean.getIdPlantilla())
                .addValue("PA_TIPO", bean.getIdArchivo());

        out = insertaElemento.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_INS_ELEM_PLA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        BigDecimal returnId = (BigDecimal) out.get("PA_ELEMENTO");
        id = returnId.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar");
        } else {
            return id;
        }

        return 0;
    }

    @Override
    public boolean actualizaElementoP(PlantillaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ETIQUETA", bean.getEtiqueta())
                .addValue("PA_ORDEN", bean.getOrden())
                .addValue("PA_OBLIGA", bean.getObligatorio())
                .addValue("PA_IDPLAN", bean.getIdPlantilla())
                .addValue("PA_TIPO", bean.getIdArchivo())
                .addValue("PA_ELEMENTO", bean.getIdElemento());

        out = actualizaElemento.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_ACT_PLANTILLA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaElementoP(int idElemento) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ELEMENTO", idElemento);

        out = eliminaElemento.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMPERFILAPP.SP_DEL_PLANTILLA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return true;
        }

        return false;
    }

}
