package com.gruposalinas.checklist.domain;

public class ChecklistPreguntasComDTO {
	
	private int idBitaGral;
	private int idBita;
	private int idResp;
	private int idArbol;
	private String observ;
	private int idPreg;
	private int idCheck;
	private int idPosible;
	private int idObserv;
	private String posible;
	private String pregunta;
	private int idcritica;
	private String ruta;
	private int idcheckUsua;
	private int idUsu;
	private String ceco;
	private String checklist;
	private int idOrdenGru;
	private int idperiodicidad;
	private String respuestaAbierta;
	private int idRespAb;
	private int banderaRespAb;
	private String modulo;
	private double calif;
	private double precalif; 
	private int pregSi;
	private int pregNo;
	private int pregNa;
	private int pregAbier;
	private String clasif;
	private double calCheck;
	private int idPlantilla;
	private int idElem;
	private int idOrden;
	private int tipoEvi;
	private String sla;
	private int pregPadre;
	private int totPreg;
	private int sumGrupo;
	private String aux;
	private int aux2;
	private String ponderacion;
	private int idModulo; 
	private String liderObra;
	private String fechaIni;
	private String fechaFin;
	private String realizoReco;
	private int eco;
	private String nombCeco;
	private double totGral;
	private double ponTot;
	private int idHalla;
	private int folio;
	private String responsable;
	private String obsNueva;
	private int reco;
	private int status;
	private String fechaAut;
	private String area;
	
	public int getIdBita() {
		return idBita;
		
	}
	public int getIdBitaGral() {
		return idBitaGral;
	}
	public void setIdBitaGral(int idBitaGral) {
		this.idBitaGral = idBitaGral;
	}
	public void setIdBita(int idBita) {
		this.idBita = idBita;
	}
	public int getIdResp() {
		return idResp;
	}
	public void setIdResp(int idResp) {
		this.idResp = idResp;
	}
	public int getIdArbol() {
		return idArbol;
	}
	public void setIdArbol(int idArbol) {
		this.idArbol = idArbol;
	}
	public String getObserv() {
		return observ;
	}
	public void setObserv(String observ) {
		this.observ = observ;
	}
	public int getIdPreg() {
		return idPreg;
	}
	public void setIdPreg(int idPreg) {
		this.idPreg = idPreg;
	}
	public int getIdCheck() {
		return idCheck;
	}
	public void setIdCheck(int idCheck) {
		this.idCheck = idCheck;
	}
	public int getIdPosible() {
		return idPosible;
	}
	public void setIdPosible(int idPosible) {
		this.idPosible = idPosible;
	}
	public int getIdObserv() {
		return idObserv;
	}
	public void setIdObserv(int idObserv) {
		this.idObserv = idObserv;
	}
	public String getPosible() {
		return posible;
	}
	public void setPosible(String posible) {
		this.posible = posible;
	}
	public String getPregunta() {
		return pregunta;
	}
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}
	public int getIdcritica() {
		return idcritica;
	}
	public void setIdcritica(int idcritica) {
		this.idcritica = idcritica;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public int getIdcheckUsua() {
		return idcheckUsua;
	}
	public void setIdcheckUsua(int idcheckUsua) {
		this.idcheckUsua = idcheckUsua;
	}
	public int getIdUsu() {
		return idUsu;
	}
	public void setIdUsu(int idUsu) {
		this.idUsu = idUsu;
	}
	public String getCeco() {
		return ceco;
	}
	public void setCeco(String ceco) {
		this.ceco = ceco;
	}
	public String getChecklist() {
		return checklist;
	}
	public void setChecklist(String checklist) {
		this.checklist = checklist;
	}
	public int getIdOrdenGru() {
		return idOrdenGru;
	}
	public void setIdOrdenGru(int idOrdenGru) {
		this.idOrdenGru = idOrdenGru;
	}
	public int getIdperiodicidad() {
		return idperiodicidad;
	}
	public void setIdperiodicidad(int idperiodicidad) {
		this.idperiodicidad = idperiodicidad;
	}
	public String getRespuestaAbierta() {
		return respuestaAbierta;
	}
	public void setRespuestaAbierta(String respuestaAbierta) {
		this.respuestaAbierta = respuestaAbierta;
	}
	public int getIdRespAb() {
		return idRespAb;
	}
	public void setIdRespAb(int idRespAb) {
		this.idRespAb = idRespAb;
	}
	public int getBanderaRespAb() {
		return banderaRespAb;
	}
	public void setBanderaRespAb(int banderaRespAb) {
		this.banderaRespAb = banderaRespAb;
	}
	public String getModulo() {
		return modulo;
	}
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
	
	
	public int getPregSi() {
		return pregSi;
	}
	public void setPregSi(int pregSi) {
		this.pregSi = pregSi;
	}
	public int getPregNo() {
		return pregNo;
	}
	public void setPregNo(int pregNo) {
		this.pregNo = pregNo;
	}
	public int getPregNa() {
		return pregNa;
	}
	public void setPregNa(int pregNa) {
		this.pregNa = pregNa;
	}
	public int getPregAbier() {
		return pregAbier;
	}
	public void setPregAbier(int pregAbier) {
		this.pregAbier = pregAbier;
	}
	
	public double getCalif() {
		return calif;
	}
	public void setCalif(double calif) {
		this.calif = calif;
	}
	public double getPrecalif() {
		return precalif;
	}
	public void setPrecalif(double precalif) {
		this.precalif = precalif;
	}
	
	
	public String getClasif() {
		return clasif;
	}
	public void setClasif(String clasif) {
		this.clasif = clasif;
	}
	public double getCalCheck() {
		return calCheck;
	}
	public void setCalCheck(double calCheck) {
		this.calCheck = calCheck;
	}
	
	
	public int getIdPlantilla() {
		return idPlantilla;
	}
	public void setIdPlantilla(int idPlantilla) {
		this.idPlantilla = idPlantilla;
	}
	public int getIdElem() {
		return idElem;
	}
	public void setIdElem(int idElem) {
		this.idElem = idElem;
	}
	public int getIdOrden() {
		return idOrden;
	}
	public void setIdOrden(int idOrden) {
		this.idOrden = idOrden;
	}
	public int getTipoEvi() {
		return tipoEvi;
	}
	public void setTipoEvi(int tipoEvi) {
		this.tipoEvi = tipoEvi;
	}
	
	public String getSla() {
		return sla;
	}
	public void setSla(String sla) {
		this.sla = sla;
	}
	
	
	public int getPregPadre() {
		return pregPadre;
	}
	public void setPregPadre(int pregPadre) {
		this.pregPadre = pregPadre;
	}
	
	
	public int getTotPreg() {
		return totPreg;
	}
	public void setTotPreg(int totPreg) {
		this.totPreg = totPreg;
	}
	public int getSumGrupo() {
		return sumGrupo;
	}
	public void setSumGrupo(int sumGrupo) {
		this.sumGrupo = sumGrupo;
	}
	public String getAux() {
		return aux;
	}
	public void setAux(String aux) {
		this.aux = aux;
	}
	
	public int getAux2() {
		return aux2;
	}
	public void setAux2(int aux2) {
		this.aux2 = aux2;
	}
	
	public String getPonderacion() {
		return ponderacion;
	}
	public void setPonderacion(String ponderacion) {
		this.ponderacion = ponderacion;
	}
	public int getIdModulo() {
		return idModulo;
	}
	public void setIdModulo(int idModulo) {
		this.idModulo = idModulo;
	}
	public String getLiderObra() {
		return liderObra;
	}
	public void setLiderObra(String liderObra) {
		this.liderObra = liderObra;
	}
	public String getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getRealizoReco() {
		return realizoReco;
	}
	public void setRealizoReco(String realizoReco) {
		this.realizoReco = realizoReco;
	}
	
	
	public int getEco() {
		return eco;
	}
	public void setEco(int eco) {
		this.eco = eco;
	}
	public String getNombCeco() {
		return nombCeco;
	}
	public void setNombCeco(String nombCeco) {
		this.nombCeco = nombCeco;
	}
	
	
	public double getTotGral() {
		return totGral;
	}
	public void setTotGral(double totGral) {
		this.totGral = totGral;
	}
	
	
	public double getPonTot() {
		return ponTot;
	}
	public void setPonTot(double ponTot) {
		this.ponTot = ponTot;
	}
	
	public int getIdHalla() {
		return idHalla;
	}
	public void setIdHalla(int idHalla) {
		this.idHalla = idHalla;
	}
	public int getFolio() {
		return folio;
	}
	public void setFolio(int folio) {
		this.folio = folio;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getObsNueva() {
		return obsNueva;
	}
	public void setObsNueva(String obsNueva) {
		this.obsNueva = obsNueva;
	}
	public int getReco() {
		return reco;
	}
	public void setReco(int reco) {
		this.reco = reco;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getFechaAut() {
		return fechaAut;
	}
	public void setFechaAut(String fechaAut) {
		this.fechaAut = fechaAut;
	}
	
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	@Override
	public String toString() {
		return "ChecklistPreguntasComDTO [idBitaGral=" + idBitaGral + ", idBita=" + idBita + ", idResp=" + idResp
				+ ", idArbol=" + idArbol + ", observ=" + observ + ", idPreg=" + idPreg + ", idCheck=" + idCheck
				+ ", idPosible=" + idPosible + ", idObserv=" + idObserv + ", posible=" + posible + ", pregunta="
				+ pregunta + ", idcritica=" + idcritica + ", ruta=" + ruta + ", idcheckUsua=" + idcheckUsua + ", idUsu="
				+ idUsu + ", ceco=" + ceco + ", checklist=" + checklist + ", idOrdenGru=" + idOrdenGru
				+ ", idperiodicidad=" + idperiodicidad + ", respuestaAbierta=" + respuestaAbierta + ", idRespAb="
				+ idRespAb + ", banderaRespAb=" + banderaRespAb + ", modulo=" + modulo + ", calif=" + calif
				+ ", precalif=" + precalif + ", pregSi=" + pregSi + ", pregNo=" + pregNo + ", pregNa=" + pregNa
				+ ", pregAbier=" + pregAbier + ", clasif=" + clasif + ", calCheck=" + calCheck + ", idPlantilla="
				+ idPlantilla + ", idElem=" + idElem + ", idOrden=" + idOrden + ", tipoEvi=" + tipoEvi + ", sla=" + sla
				+ ", pregPadre=" + pregPadre + ", totPreg=" + totPreg + ", sumGrupo=" + sumGrupo + ", aux=" + aux
				+ ", aux2=" + aux2 + ", ponderacion=" + ponderacion + ", idModulo=" + idModulo + ", liderObra="
				+ liderObra + ", fechaIni=" + fechaIni + ", fechaFin=" + fechaFin + ", realizoReco=" + realizoReco
				+ ", eco=" + eco + ", nombCeco=" + nombCeco + ", totGral=" + totGral + ", ponTot=" + ponTot
				+ ", idHalla=" + idHalla + ", folio=" + folio + ", responsable=" + responsable + ", obsNueva="
				+ obsNueva + ", reco=" + reco + ", status=" + status + ", fechaAut=" + fechaAut + ", area=" + area
				+ "]";
	}
	
}
