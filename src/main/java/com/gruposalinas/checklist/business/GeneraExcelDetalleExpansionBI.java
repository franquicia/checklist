package com.gruposalinas.checklist.business;

import java.awt.Image;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.gruposalinas.checklist.dao.SucursalChecklistDAO;
import com.gruposalinas.checklist.domain.AdicionalesDTO;
import com.gruposalinas.checklist.domain.ChecklistHallazgosRepoDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;
import com.gruposalinas.checklist.domain.RepoFilExpDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import com.gruposalinas.checklist.resources.FRQConstantes;

public class GeneraExcelDetalleExpansionBI {
	private int tipoReporte; // 0=Hallazgos 1=General
	private String tituloPestana = "";
	private String idCeco;
	private String liderObra;
	private String franquicia;
	private List<RepoFilExpDTO> listaPreguntas;
	private List<RepoFilExpDTO> preguntasHijas;
	private List<RepoFilExpDTO> respuestasEvi;
	private List<RepoFilExpDTO> listaSucursalRecorrido;

	CellStyle boddyCellStyle;
	CellStyle headerCellStyle;

	List<RowsExcel> rowsExcelHijas;
	List<RowsExcel> rowsExcelPadre;

	private static final Logger logger = LogManager.getLogger(GeneraExcelDetalleExpansionBI.class);

	@Autowired
	RepoFilExpBI repoFilExpBI;

	public GeneraExcelDetalleExpansionBI(int tipoReporte, String idCeco, RepoFilExpBI repoFilExpBI) {
		this.tipoReporte = tipoReporte;
		this.repoFilExpBI = repoFilExpBI;
		this.idCeco = idCeco;
		this.liderObra = "";
		this.franquicia = "";
		if (this.tipoReporte == 0)
			this.tituloPestana = "Hallazgos";
		if (this.tipoReporte == 1)
			this.tituloPestana = "Dashboard";
		if (this.tipoReporte > 1)
			this.tituloPestana = "Otro";

	}

	public GeneraExcelDetalleExpansionBI() {

	}

	public Workbook generar() {

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet(tituloPestana);

		// Cabecera
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 18);

		headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerCellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Cuerpo
		Font bodyFont = workbook.createFont();
		bodyFont.setBold(false);
		bodyFont.setFontHeightInPoints((short) 18);

		boddyCellStyle = workbook.createCellStyle();
		boddyCellStyle.setFont(bodyFont);
		boddyCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		boddyCellStyle.setAlignment(HorizontalAlignment.CENTER);

		Row headerRow = sheet.createRow(0);

		listaSucursalRecorrido = repoFilExpBI.obtieneDatosGen(idCeco, "", "", "", "");
		// Lista de titulos de columnas Nota: tomar encuenta que afecta al armado del
		// excel y se requiere agregar esos campos en el metodo

		String[] columnas = { "Eco", "CECO", "Sucursal", "Líder de Obra", "Franquicia", "Capítulo", "Prioridad",
				"Fecha Compromiso", "Estatus", "Área de atención", "2a Área de Atención", "Filtro P/H", "Item" };
		List<String> columns = new ArrayList<>();
		columns.addAll(Arrays.asList(columnas)); // "Fecha de Recorrido" "Item", "Detalle"

		// Columnas dinamicas por recorrido sucursal
		for (int i = 0; i < listaSucursalRecorrido.size(); i++) {
			if ((i + 1) == listaSucursalRecorrido.size()) {
				liderObra = listaSucursalRecorrido.get(i).getNomUsu();
				franquicia = listaSucursalRecorrido.get(i).getAux();
			}
			columns.add("Fecha: " + listaSucursalRecorrido.get(i).getPerido() + " Respuesta");
			columns.add("Fecha: " + listaSucursalRecorrido.get(i).getPerido() + " Observaciones");
			columns.add("Fecha: " + listaSucursalRecorrido.get(i).getPerido() + " Evidencia");
		}

		for (int i = 0; i < columns.size(); i++) {

			Cell cell = headerRow.createCell(i);

			cell.setCellValue(columns.get(i));

			cell.setCellStyle(headerCellStyle);

		}

		rowsExcelPadre = new ArrayList<>();
		rowsExcelHijas = new ArrayList<>();

		if (tipoReporte == 1) { // DASHBOARD
			generaRowsPadres(); // genera rows padres con sus respectivas hijas
			generaDashboard(workbook, sheet); // genera dasbord(excel) con sus respectivas hijas
		}

		/*
		 * if (tipoReporte == 0) { generaHallazgos(sheet); }
		 */

		// Se ajusta el anche de las columnas
		for (int i = 0; i < columns.size(); i++) {
			sheet.autoSizeColumn(i);
		}

		return workbook;
	}

	/*
	 * private void generaHallazgos(Sheet sheet) { //en espera }
	 */

	private void generaDashboard(Workbook workbook, Sheet sheet) {
		int fila = 0;

		// ============= Se empieza por preguntas padres (i)====================
		for (int i = 0; i < rowsExcelPadre.size(); i++) {
			Cell cell;
			fila++;
			int columna = 0;
			Row filaExcel = sheet.createRow(fila);
			if (idCeco.length() == 6) {
				(cell = filaExcel.createCell(columna++)).setCellValue("" + idCeco.substring(2, 6)); // Eco
				cell.setCellStyle(boddyCellStyle);
			}
			if (idCeco.length() == 5) {
				(cell = filaExcel.createCell(columna++)).setCellValue("0" + idCeco.substring(1, 5)); // Eco
				cell.setCellStyle(boddyCellStyle);
			}

			(cell = filaExcel.createCell(columna++)).setCellValue(idCeco); // Ceco
			cell.setCellStyle(boddyCellStyle);
			(cell = filaExcel.createCell(columna++)).setCellValue(listaSucursalRecorrido.get(0).getNombreCeco()); // NombreCeco
			cell.setCellStyle(boddyCellStyle);
			(cell = filaExcel.createCell(columna++)).setCellValue(liderObra); // Líder de Obra
			cell.setCellStyle(boddyCellStyle);
			(cell = filaExcel.createCell(columna++)).setCellValue(franquicia); // Fanquicia
			cell.setCellStyle(boddyCellStyle);
			(cell = filaExcel.createCell(columna++)).setCellValue(rowsExcelPadre.get(i).clasif); // Capitulo
			cell.setCellStyle(boddyCellStyle);
			(cell = filaExcel.createCell(columna++)).setCellValue(""); // Prioridad
			cell.setCellStyle(boddyCellStyle);
			(cell = filaExcel.createCell(columna++)).setCellValue(""); // Fecha Compromiso
			cell.setCellStyle(boddyCellStyle);
			(cell = filaExcel.createCell(columna++)).setCellValue(""); // Estatus
			cell.setCellStyle(boddyCellStyle);
			(cell = filaExcel.createCell(columna++)).setCellValue(""); // Área de atención
			cell.setCellStyle(boddyCellStyle);
			(cell = filaExcel.createCell(columna++)).setCellValue(""); // 2a Área de atención
			cell.setCellStyle(boddyCellStyle);
			(cell = filaExcel.createCell(columna++)).setCellValue("P"); // Filtro P/H
			cell.setCellStyle(boddyCellStyle);
			(cell = filaExcel.createCell(columna++)).setCellValue((i + 1) + ".-" + rowsExcelPadre.get(i).pregunta); // Item
			cell.setCellStyle(boddyCellStyle);

			int columnaResEviPadre = 0;
			// ============= Respuestas por preguntas padres (n)====================
			for (int n = 0; n < rowsExcelPadre.get(i).respuestasRecorrido.size(); n++) {
				columnaResEviPadre = ((columna + (rowsExcelPadre.get(i).idRecorrido.get(n) * 2)
						+ (rowsExcelPadre.get(i).idRecorrido.get(n) - 2))) - 1; // Se calcula en que columna se graba
																				// por recorrido
				(cell = filaExcel.createCell(columnaResEviPadre++))
						.setCellValue(rowsExcelPadre.get(i).respuestasRecorrido.get(n)); // Respuesta
				cell.setCellStyle(boddyCellStyle);
				columnaResEviPadre++; // Salto de columna debido a que la papa no tiene observacion
				if (rowsExcelPadre.get(i).evidencias.get(n) != null
						&& !rowsExcelPadre.get(i).evidencias.get(n).equals("")) { // Se agrega Evidencia
					try {
						InputStream inputStream = new FileInputStream(rowsExcelPadre.get(i).evidencias.get(n));
						byte[] bytes = IOUtils.toByteArray(inputStream);
						int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
						inputStream.close();
						CreationHelper helper = workbook.getCreationHelper();
						Drawing drawing = sheet.createDrawingPatriarch();
						ClientAnchor anchor = helper.createClientAnchor();
						anchor.setCol1(columnaResEviPadre); // Column Inicial
						anchor.setRow1(fila); // Row Inicial
						anchor.setCol2(columnaResEviPadre + 1); // Column Final
						anchor.setRow2(fila + 1); // Row Final

						Picture pict = drawing.createPicture(anchor, pictureIdx);

					} catch (IOException e) {
						(cell = filaExcel.createCell(columnaResEviPadre++)).setCellValue("");
						cell.setCellStyle(boddyCellStyle);
						logger.info("No se encontro la evidencia padre " + rowsExcelPadre.get(i).evidencias.get(n));
					}
				} else {
					(cell = filaExcel.createCell(columnaResEviPadre++)).setCellValue("");
					cell.setCellStyle(boddyCellStyle);
				}

			}
			filaExcel.setRowStyle(boddyCellStyle);
			filaExcel.setHeightInPoints(150);

			// ============= Preguntas hijas por padres (j)====================
			List<Integer> idPreguntasHijas = new ArrayList<>();
			int contadorPregHijas = 0;
			for (int j = 0; j < rowsExcelHijas.size(); j++) {
				if (rowsExcelHijas.get(j).idPreguntaPadre == rowsExcelPadre.get(i).idPregunta) {

					if (!idPreguntasHijas.contains(rowsExcelHijas.get(j).idPregunta)) {
						contadorPregHijas++;
						Cell cell2;
						fila++;
						columna = 0;

						Row filaExcel2 = sheet.createRow(fila);
						if (idCeco.length() == 6) {
							(cell2 = filaExcel2.createCell(columna++)).setCellValue("" + idCeco.substring(2, 6)); // Eco
							cell2.setCellStyle(boddyCellStyle);
						}
						if (idCeco.length() == 5) {
							(cell2 = filaExcel2.createCell(columna++)).setCellValue("0" + idCeco.substring(1, 5)); // Eco
							cell2.setCellStyle(boddyCellStyle);
						}
						(cell2 = filaExcel2.createCell(columna++)).setCellValue(idCeco); // Ceco
						cell2.setCellStyle(boddyCellStyle);
						(cell2 = filaExcel2.createCell(columna++))
								.setCellValue(listaSucursalRecorrido.get(0).getNombreCeco()); // NombreCeco
						cell2.setCellStyle(boddyCellStyle);
						(cell2 = filaExcel2.createCell(columna++)).setCellValue(liderObra); // Líder de Obra
						cell2.setCellStyle(boddyCellStyle);
						(cell2 = filaExcel2.createCell(columna++)).setCellValue(franquicia); // Fanquicia
						cell2.setCellStyle(boddyCellStyle);
						(cell2 = filaExcel2.createCell(columna++)).setCellValue(rowsExcelHijas.get(j).clasif); // Capitulo
						cell2.setCellStyle(boddyCellStyle);
						(cell2 = filaExcel2.createCell(columna++)).setCellValue(""); // Prioridad
						cell2.setCellStyle(boddyCellStyle);
						(cell2 = filaExcel2.createCell(columna++)).setCellValue(""); // Fecha Compromiso
						cell2.setCellStyle(boddyCellStyle);
						(cell2 = filaExcel2.createCell(columna++)).setCellValue(""); // Estatus
						cell2.setCellStyle(boddyCellStyle);
						(cell2 = filaExcel2.createCell(columna++)).setCellValue(""); // Área de atención
						cell2.setCellStyle(boddyCellStyle);
						(cell2 = filaExcel2.createCell(columna++)).setCellValue(""); // 2a Área de atención
						cell2.setCellStyle(boddyCellStyle);
						(cell2 = filaExcel2.createCell(columna++)).setCellValue("H"); // Filtro P/H
						cell2.setCellStyle(boddyCellStyle);
						(cell2 = filaExcel2.createCell(columna++)).setCellValue(
								"\t" + (i + 1) + "." + contadorPregHijas + "-" + rowsExcelHijas.get(j).pregunta); // Item
						cell2.setCellStyle(boddyCellStyle);

						// ============= Respuestas hijas por idHijas (j)====================
						int columnaResEviHija = 0;
						for (int n = 0; n < rowsExcelHijas.size(); n++) {
							if (rowsExcelHijas.get(j).idPregunta == rowsExcelHijas.get(n).idPregunta) {
								columnaResEviHija = (columna + ((rowsExcelHijas.get(n).idRecorrido.get(0) * 2)
										+ (rowsExcelHijas.get(n).idRecorrido.get(0) - 2))) - 1; // Se calcula en que
																								// columna se graba por
																								// recorrido

								(cell2 = filaExcel2.createCell(columnaResEviHija++))
										.setCellValue(rowsExcelHijas.get(n).respuestasRecorrido.get(0)); // Respuesta
								cell2.setCellStyle(boddyCellStyle);
								(cell2 = filaExcel2.createCell(columnaResEviHija++))
										.setCellValue(rowsExcelHijas.get(n).observaciones.get(0)); // Observacion
								cell2.setCellStyle(boddyCellStyle);

								if (rowsExcelHijas.get(n).evidencias.get(0) != null
										&& !rowsExcelHijas.get(n).evidencias.get(0).equals("")) { // Se agrega Evidencia
									try {
										InputStream inputStream = new FileInputStream(
												rowsExcelHijas.get(n).evidencias.get(0));
										byte[] bytes = IOUtils.toByteArray(inputStream);
										int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
										inputStream.close();
										CreationHelper helper = workbook.getCreationHelper();
										Drawing drawing = sheet.createDrawingPatriarch();
										ClientAnchor anchor = helper.createClientAnchor();
										anchor.setCol1(columnaResEviHija); // Column Inicial
										anchor.setRow1(fila); // Row Inicial
										anchor.setCol2(columnaResEviHija + 1); // Column Final
										anchor.setRow2(fila + 1); // Row Final

										Picture pict = drawing.createPicture(anchor, pictureIdx);
									} catch (IOException e) {
										(cell2 = filaExcel2.createCell(columnaResEviHija++)).setCellValue("");
										cell2.setCellStyle(boddyCellStyle);
										logger.info("No se encontro la evidencia hija "
												+ rowsExcelHijas.get(n).evidencias.get(0));
									}
								} else {
									(cell2 = filaExcel2.createCell(columnaResEviPadre++)).setCellValue("");
									cell2.setCellStyle(boddyCellStyle);
								}
							}
						}
						filaExcel2.setRowStyle(boddyCellStyle);
						filaExcel2.setHeightInPoints(150);
					}

				}
				idPreguntasHijas.add(rowsExcelHijas.get(j).idPregunta);

			}

		}

	}

	private void generaRowsPadres() {
		listaPreguntas = repoFilExpBI.obtienedash2(idCeco);
		List<String> bitacoraIdPregPadre = new ArrayList<>();
		int rowCount = 0;
		for (int a = 0; a < listaPreguntas.size(); a++) {
			if (!listaPreguntas.get(a).getPosible().equals("")) {
				RowsExcel rel = new RowsExcel();
				if (a != 0) {
					if (rowsExcelPadre.get(rowCount).idPregunta == listaPreguntas.get(a).getIdPreg()) {
						if (!bitacoraIdPregPadre.contains(
								listaPreguntas.get(a).getBitGral() + "" + listaPreguntas.get(a).getIdPreg())) {

							rowsExcelPadre.get(rowCount).evidencias.add(listaPreguntas.get(a).getRuta());
							rowsExcelPadre.get(rowCount).respuestasRecorrido.add(listaPreguntas.get(a).getPosible());
							rowsExcelPadre.get(rowCount).idRecorrido
									.add(Integer.parseInt(listaPreguntas.get(a).getIdRecorrido()));

							generaRowsHijas(Integer.parseInt(listaPreguntas.get(a).getIdRecorrido()),
									listaPreguntas.get(a).getPosible(), listaPreguntas.get(a).getRuta(),
									listaPreguntas.get(a).getPregunta(), listaPreguntas.get(a).getIdPreg(),
									listaPreguntas.get(a).getBitGral(), idCeco, listaPreguntas.get(a).getClasif());
							bitacoraIdPregPadre
									.add(listaPreguntas.get(a).getBitGral() + "" + listaPreguntas.get(a).getIdPreg());
						}
					} else {
						rowCount++;

						rel.evidencias.add(listaPreguntas.get(a).getRuta());
						rel.respuestasRecorrido.add(listaPreguntas.get(a).getPosible());
						rel.numeroRow = rowCount;
						rel.idPregunta = listaPreguntas.get(a).getIdPreg();
						rel.pregunta = listaPreguntas.get(a).getPregunta();
						rel.clasif = listaPreguntas.get(a).getClasif();
						rel.idRecorrido.add(Integer.parseInt(listaPreguntas.get(a).getIdRecorrido()));
						rowsExcelPadre.add(rel);

						generaRowsHijas(Integer.parseInt(listaPreguntas.get(a).getIdRecorrido()),
								listaPreguntas.get(a).getPosible(), listaPreguntas.get(a).getRuta(),
								listaPreguntas.get(a).getPregunta(), listaPreguntas.get(a).getIdPreg(),
								listaPreguntas.get(a).getBitGral(), idCeco, listaPreguntas.get(a).getClasif());
						bitacoraIdPregPadre
								.add(listaPreguntas.get(a).getBitGral() + "" + listaPreguntas.get(a).getIdPreg());
					}
				} else {
					rel.evidencias.add(listaPreguntas.get(a).getRuta());
					rel.respuestasRecorrido.add(listaPreguntas.get(a).getPosible());
					rel.numeroRow = 1;
					rel.idPregunta = listaPreguntas.get(a).getIdPreg();
					rel.pregunta = listaPreguntas.get(a).getPregunta();
					rel.clasif = listaPreguntas.get(a).getClasif();
					rel.idRecorrido.add(Integer.parseInt(listaPreguntas.get(a).getIdRecorrido()));
					rowsExcelPadre.add(rel);

					generaRowsHijas(Integer.parseInt(listaPreguntas.get(a).getIdRecorrido()),
							listaPreguntas.get(a).getPosible(), listaPreguntas.get(a).getRuta(),
							listaPreguntas.get(a).getPregunta(), listaPreguntas.get(a).getIdPreg(),
							listaPreguntas.get(a).getBitGral(), idCeco, listaPreguntas.get(a).getClasif());

					bitacoraIdPregPadre
							.add(listaPreguntas.get(a).getBitGral() + "" + listaPreguntas.get(a).getIdPreg());

				}

			}
		}

	}

	public void generaRowsHijas(int recorrido, String respuestaPdre, String rutaPadre, String detallePreguntaPadre,
			int idPreguntaPadre, int idBitacoraGeneral, String idCeco, String clasif) {

		List<SucursalChecklistDTO> sucursal = null;
		// String tipoNegocio = "";
		int version = 0;

		SucursalChecklistBI sucursalChecklistBI = (SucursalChecklistBI) FRQAppContextProvider.getApplicationContext()
				.getBean("sucursalChecklistBI");
		try {
			sucursal = sucursalChecklistBI.obtieneDatosSuc(idCeco);
			version = sucursal.get(0).getAux();
			// tipoNegocio = sucursal.get(0).getNegocio();
			// version =sucursal.get(0).get
		} catch (Exception e) {
			logger.info("hubo un problema al obtener adicionales para generar el excel de hallazgos");
		}

		respuestasEvi = repoFilExpBI.obtieneInfofiltro("" + idBitacoraGeneral, "", "" + idPreguntaPadre);
		preguntasHijas = repoFilExpBI.obtienePreguntasHijas("" + idCeco);

		for (int i = 0; i < preguntasHijas.size(); i++) {
			RowsExcel rel = new RowsExcel();

			if (preguntasHijas.get(i).getPregPadre() == idPreguntaPadre) { // Se obtiene preguntas hijas con la pregunta
																			// padre recibida

				// if (tipoNegocio.equalsIgnoreCase("EKT")){
				if (version == 1) {
					rel.pregunta = detallePreguntaPadre + " " + preguntasHijas.get(i).getPregunta();
				} else {
					rel.pregunta = preguntasHijas.get(i).getPregunta();
				}

				rel.idPregunta = preguntasHijas.get(i).getIdPreg();
				rel.idPreguntaPadre = preguntasHijas.get(i).getPregPadre();
				rel.clasif = clasif;

				if (respuestaPdre.equalsIgnoreCase("NO")) { // Se busca respuestas de la preguntas hijas si la pregunta
															// padre es NO
					int respAgregada = 0;
					int evidAgregada = 0;
					for (int k = 0; k < respuestasEvi.size(); k++) { // Se obtiene respuestas y evidencias hijas por el
																		// idPreguntaHija
						if (respuestasEvi.get(k).getIdPreg() == preguntasHijas.get(i).getIdPreg()) {
							if (respAgregada == 0) { // Se agrega solo 1 vez la respuesta recorrido y observacion
								rel.respuestasRecorrido.add(respuestasEvi.get(k).getPosible());
								rel.idRecorrido.add(recorrido);
								rel.observaciones.add(respuestasEvi.get(k).getObserv());
								respAgregada = 1;
							}

							if (evidAgregada == 0) { // Se agrega la primera evidencia encontrada
								if (respuestasEvi.get(k).getRuta() != null
										&& !respuestasEvi.get(k).getRuta().equals("")) {
									rel.evidencias.add(respuestasEvi.get(k).getRuta());
									evidAgregada = 1;
								}
							}
						}
					}
					if (respAgregada == 0) { // Se Agrega respuesta S/R si no se encontro respuesta a la pregunta hija
						rel.respuestasRecorrido.add("S/R");
						rel.idRecorrido.add(recorrido);
						rel.observaciones.add("");
					}
					if (evidAgregada == 0) { // Se agrega evidencia vacia si no se encontro alguna
						rel.evidencias.add("");
					}
				} else { // Se agrega respuesta si y evidencia de la padre a la hija si respuesta de la
							// padre no es si
					rel.respuestasRecorrido.add("SI");
					rel.observaciones.add("");
					rel.idRecorrido.add(recorrido);
					rel.evidencias.add(rutaPadre);
				}

				rowsExcelHijas.add(rel); // Se agrega a la lista de rows de hijas

			}
		}
	}

	private class RowsExcel { // Se crea clase para realizar mas facil los rows de los recorridos
		int numeroRow;
		int idPregunta;
		int idPreguntaPadre;
		List<String> respuestasRecorrido;
		List<String> evidencias;
		List<String> observaciones;
		String pregunta;
		String clasif;
		List<Integer> idRecorrido;

		RowsExcel() {
			this.respuestasRecorrido = new ArrayList<>();
			this.evidencias = new ArrayList<>();
			this.observaciones = new ArrayList<>();
			this.idRecorrido = new ArrayList<>();

		}

	}

	public Workbook generaHallazgos(String tipoRecorrido, List<ChecklistPreguntasComDTO> chklistPreguntas1,
			String idCeco, SucursalChecklistBI sucursalChecklistBI) throws IOException {
		/**/
		int version = 0;
		List<SucursalChecklistDTO> sucursal = sucursalChecklistBI.obtieneDatosSuc(idCeco);
		version = sucursal.get(0).getAux();
		// String tipoNegocio = sucursal.get(0).getNegocio();

		// if (tipoNegocio.equalsIgnoreCase("EKT")) {
		if (version == 1) {
			chklistPreguntas1 = getHallazgosConcatenados(chklistPreguntas1);
		}

		/**/
		tipoRecorrido = getNombreRecorrido(tipoRecorrido);

		// GENERAMOS WOORKBOOK
		String[] columns = { "Tipo", "Proyecto", "Eco", "CECO", "Sucursal", "Líder de Obra", "Franquicia",
				"Fecha de Recorrido", "Capítulo", "Item", "Respuesta", "Imagen", "Prioridad", "Observaciones",
				"Compromiso", "Estatus", "Área de atención" };

		// Create a Workbook
		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file
		/*
		 * CreationHelper helps us create instances of various things like DataFormat,
		 * Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way
		 */
		CreationHelper createHelper = workbook.getCreationHelper();
		// Create a Sheet
		Sheet sheet = workbook.createSheet("Hallazgos");
		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 18);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerCellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Create a Row
		Row headerRow = sheet.createRow(0);

		// Create cells
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		// Cells Format
		Font bodyFont = workbook.createFont();
		bodyFont.setBold(false);
		bodyFont.setFontHeightInPoints((short) 18);

		boddyCellStyle = workbook.createCellStyle();
		boddyCellStyle.setFont(bodyFont);
		boddyCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		boddyCellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Create Other rows and cells with data
		int rowNum = 1;
		if (chklistPreguntas1 != null && chklistPreguntas1.size() > 0) {
			for (ChecklistPreguntasComDTO obj : chklistPreguntas1) {
				if (rowNum == 1) {
					idCeco = obj.getCeco();
				}
				Cell cell;
				Row row = sheet.createRow(rowNum++);
				// (cell = filaExcel.createCell(columna++)).setCellValue(idCeco);
				(cell = row.createCell(0)).setCellValue("Hallazgo");
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(1)).setCellValue(tipoRecorrido);
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(2)).setCellValue(obj.getEco());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(3)).setCellValue(obj.getCeco());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(4)).setCellValue(obj.getNombCeco());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(5)).setCellValue(obj.getLiderObra());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(6)).setCellValue(obj.getRealizoReco());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(7)).setCellValue(obj.getFechaFin());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(8)).setCellValue(obj.getClasif());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(9)).setCellValue(obj.getPregunta());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(10)).setCellValue(obj.getPosible());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(12)).setCellValue("");
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(13)).setCellValue(obj.getObserv());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(14)).setCellValue("");
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(15)).setCellValue("");
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(16)).setCellValue("");
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(17)).setCellValue("");
				cell.setCellStyle(boddyCellStyle);

				/* IMAGEN */
				// FileInputStream obtains input bytes from the image file

				if (obj.getRuta() != null && !obj.getRuta().equals("")) {

					try {

						javaxt.io.Image image = new javaxt.io.Image(obj.getRuta());
						String desc = "";
						
						try {
							
							URL url = new URL(FRQConstantes.getRutaImagen()+obj.getRuta());
							InputStream fis =  url.openStream();
							Metadata metadata = ImageMetadataReader.readMetadata(fis);
							
							for (Directory directory : metadata.getDirectories()) {
				                for (Tag tag : directory.getTags()) {
				                    if(tag.getTagName().equals("Orientation")){
				                    	desc =  tag.getDescription();
				                    }
				                }
				             }
							
							switch (desc) {
							
							case "Top, left side (Horizontal / normal)":
								break;
							
							case "Top, right side (Mirror horizontal)":
								break;
								
							case "Bottom, right side (Rotate 180)":
								image.rotate(180);
								break;
								
							case "Bottom, left side (Mirror vertical)":
								image.rotate(-180);
								break;
								
							case "Left side, top (Mirror horizontal and rotate 270 CW)":
								image.rotate(-90);
								break;
								
							case "Right side, top (Rotate 90 CW)":
								image.rotate(90);
								break;
								
							case "Right side, bottom (Mirror horizontal and rotate 90 CW)":
								image.rotate(-270);
								break;
								
							case "Left side, bottom (Rotate 270 CW)":
								image.rotate(270);
								break;
								
							default:
								break;

							}
						
						} catch (Exception e) {
							image.rotate(90);
							// image.saveAs(obj.getRuta()+"erick.jpg");
						}
						byte[] b = image.getByteArray();

						InputStream inputStream = new FileInputStream(obj.getRuta());
						// Get the contents of an InputStream as a byte[].
						byte[] bytes = IOUtils.toByteArray(inputStream);
						// Adds a picture to the workbook
						int pictureIdx = workbook.addPicture(b, Workbook.PICTURE_TYPE_PNG);
						// close the input stream
						inputStream.close();
						// Returns an object that handles instantiating concrete classes
						CreationHelper helper = workbook.getCreationHelper();
						// Creates the top-level drawing patriarch.
						Drawing drawing = sheet.createDrawingPatriarch();
						// Create an anchor that is attached to the worksheet
						ClientAnchor anchor = helper.createClientAnchor();
						// create an anchor with upper left cell _and_ bottom right cell
						anchor.setCol1(11); // Column B
						anchor.setRow1(rowNum - 1); // Row 3
						anchor.setCol2(12); // Column C
						anchor.setRow2(rowNum); // Row 4
						// Creates a picture
						Picture pict = drawing.createPicture(anchor, pictureIdx);

					} catch (Exception e) {
						logger.info("AP IM HALLAZGOS ---" + obj.getRuta());
					}
				}

				row.setRowStyle(boddyCellStyle);
				row.setHeightInPoints(150);

				/* IMAGEN */

			}
		}
		// Se Agrega Adicionales AdicionalesBI adicionalesExpBI;
		if (idCeco != null && !idCeco.isEmpty()) {

			List<AdicionalesDTO> listaAdicionales = null;
			AdicionalesBI adicionalesBI = (AdicionalesBI) FRQAppContextProvider.getApplicationContext()
					.getBean("adicionalesBI");
			try {
				listaAdicionales = adicionalesBI.obtieneDatos(idCeco);
			} catch (Exception e) {
				logger.info("hubo un problema al obtener adicionales para generar el excel de hallazgos");
			}
			/*
			 * Cell cell; Row row = sheet.createRow(rowNum++); (cell =
			 * row.createCell(0)).setCellValue("ADICIONALES");
			 * cell.setCellStyle(headerCellStyle); // Create a Row
			 * 
			 */
			if (listaAdicionales != null && listaAdicionales.size() > 0) {
				if (rowNum > 1) {
					Row row2 = sheet.createRow(rowNum++);
					for (int i = 0; i < columns.length; i++) {
						Cell cell2 = row2.createCell(i);
						cell2.setCellValue(columns[i]);
						cell2.setCellStyle(headerCellStyle);
					}
				}
				int rowNum2 = rowNum++;
				for (AdicionalesDTO adi : listaAdicionales) {
					Cell cell;
					Row row = sheet.createRow(rowNum2++);
					(cell = row.createCell(0)).setCellValue("Adicional");
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(1)).setCellValue(tipoRecorrido); // Proyecto
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(2)).setCellValue(adi.getEco()); // Eco
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(3)).setCellValue(adi.getCeco()); // CECO
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(4)).setCellValue(adi.getNomCeco()); // Sucursal
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(5)).setCellValue(adi.getLiderObra());// Líder de Obra
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(6)).setCellValue(adi.getUsuReco()); // Franquicia
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(7)).setCellValue(adi.getFechaIni()); // Fecha de Recorrido
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(8)).setCellValue(adi.getClasifica()); // Capítulo
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(9)).setCellValue(adi.getPregunta()); // Item
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(10)).setCellValue(adi.getPosible()); // Respuesta
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(12)).setCellValue((adi.getCritica() == 0) ? "BAJA"
							: (adi.getCritica() == 1) ? "IMPERDONABLE"
									: (adi.getCritica() == 2) ? "CRITICA" : (adi.getCritica() == 3) ? "MEDIA" : (adi.getCritica() == 4) ? "BAJA" : ""); // Prioridad
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(13)).setCellValue(adi.getObs()); // Observaciones
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(14)).setCellValue("");// compromiso
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(15)).setCellValue((adi.getStatus() == 0) ? "RECHAZADO"
							: (adi.getStatus() == 1) ? "PENDIENTE POR ATENDER"
									: (adi.getStatus() == 2) ? "PENDIENTE POR AUTORIZAR"
											: (adi.getStatus() == 3) ? "AUTORIZADO" : "");// status
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(16)).setCellValue("");// area de atencion
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(17)).setCellValue("");
					cell.setCellStyle(boddyCellStyle);

					/* IMAGEN */
					// FileInputStream obtains input bytes from the image file

					if (adi.getRuta() != null && !adi.getRuta().isEmpty()) {

						try {
							InputStream inputStream = new FileInputStream(adi.getRuta());
							// Get the contents of an InputStream as a byte[].
							byte[] bytes = IOUtils.toByteArray(inputStream);
							// Adds a picture to the workbook
							int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
							// close the input stream
							inputStream.close();
							// Returns an object that handles instantiating concrete classes
							CreationHelper helper = workbook.getCreationHelper();
							// Creates the top-level drawing patriarch.
							Drawing drawing = sheet.createDrawingPatriarch();
							// Create an anchor that is attached to the worksheet
							ClientAnchor anchor = helper.createClientAnchor();
							// create an anchor with upper left cell _and_ bottom right cell
							anchor.setCol1(11); // Column B
							anchor.setRow1(rowNum2 - 1); // Row 3
							anchor.setCol2(12); // Column C
							anchor.setRow2(rowNum2); // Row 4
							// Creates a picture
							Picture pict = drawing.createPicture(anchor, pictureIdx);
						} catch (Exception e) {
							logger.info("AP IM HALLAZGOS-ADICIONALES ---" + adi.getRuta());
						}
					}

					row.setRowStyle(boddyCellStyle);
					row.setHeightInPoints(150);

					/* IMAGEN */
				}
			}
		}

		// Resize all columns to fit the content size
		for (int i = 0; i < columns.length; i++) {

			if (i == 10) {
				sheet.setColumnWidth(i, 10000);
			} else {
				sheet.autoSizeColumn(i);
			}
		}

		return workbook;

	}

	public Workbook generaHallazgosCore(String tipoRecorrido, List<HallazgosTransfDTO> chklistPreguntas1,
			List<HallazgosTransfDTO> listaEvidencias, String idCeco, SucursalChecklistBI sucursalChecklistBI)
			throws IOException, ParseException {
		/**/
		//int version = 0;
		
		/*try {
		List<SucursalChecklistDTO> sucursal = sucursalChecklistBI.obtieneDatosSuc(idCeco);
		version = sucursal.get(0).getAux();
		} catch (Exception e) {
			logger.info("AP Al obtener la sucursalChecklistBI.obtieneDatosSuc(idCeco);");
		}*/

		/**/
		tipoRecorrido = "" + tipoRecorrido;

		// GENERAMOS WOORKBOOK
		String[] columns = { "Tipo", "Proyecto", "CECO", "Sucursal", "Líder de Obra", "Franquicia",
				"Fecha de Recorrido", "Capítulo", "Item", "Respuesta", "Imagen", "Observaciones", "Criticidad",
				"Fecha Compromiso", "Área de atención" };

		// Create a Workbook
		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file
		/*
		 * CreationHelper helps us create instances of various things like DataFormat,
		 * Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way
		 */
		CreationHelper createHelper = workbook.getCreationHelper();
		// Create a Sheet
		Sheet sheet = workbook.createSheet("Hallazgos");
		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 18);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerCellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Create a Row
		Row headerRow = sheet.createRow(0);

		// Create cells
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		// Cells Format
		Font bodyFont = workbook.createFont();
		bodyFont.setBold(false);
		bodyFont.setFontHeightInPoints((short) 14);

		boddyCellStyle = workbook.createCellStyle();
		boddyCellStyle.setFont(bodyFont);
		boddyCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		boddyCellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Create Other rows and cells with data
		int rowNum = 1;
		if (chklistPreguntas1 != null && chklistPreguntas1.size() > 0) {
			for (HallazgosTransfDTO obj : chklistPreguntas1) {
				if (rowNum == 1) {
					idCeco = obj.getCeco();
				}
				Cell cell;
				Row row = sheet.createRow(rowNum++);
				// (cell = filaExcel.createCell(columna++)).setCellValue(idCeco);
				(cell = row.createCell(0)).setCellValue("Hallazgo");
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(1)).setCellValue(obj.getNombreFase());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(2)).setCellValue(obj.getCeco());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(3)).setCellValue(obj.getCecoNom());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(4)).setCellValue(obj.getLiderObra());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(5)).setCellValue(obj.getNomUsu());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(6)).setCellValue(obj.getFechaTermino());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(7)).setCellValue(obj.getNombChek());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(8)).setCellValue(obj.getPreg());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(9)).setCellValue(obj.getRespuesta());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(11)).setCellValue(obj.getObs());
				cell.setCellStyle(boddyCellStyle);
				switch (obj.getCriticidad()) {
				case 0:
					(cell = row.createCell(12)).setCellValue("Bajo");// Criticidad
					break;
				case 1:
					(cell = row.createCell(12)).setCellValue("Imperdonable");// Criticidad
					break;
				case 2:
					(cell = row.createCell(12)).setCellValue("Alto");// Criticidad
					break;
				case 3:
					(cell = row.createCell(12)).setCellValue("Medio");// Criticidad
					break;
				case 4:
					(cell = row.createCell(12)).setCellValue("Bajo");// Criticidad
					break;
				}
				cell.setCellStyle(boddyCellStyle);
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				String dateInString = obj.getFechaTermino();

				if (obj.getFechaTermino() != null) {
					Date date = formatter.parse(dateInString);
					LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
					localDateTime = localDateTime.plusDays(Long.parseLong(obj.getSla()));
					date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(13)).setCellValue(formatter.format(date) + "");// Fecha Hecha + SLA
				} else {
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(13)).setCellValue("");
				}

				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(14)).setCellValue(obj.getArea());//
				cell.setCellStyle(boddyCellStyle);

				/* IMAGEN */
				// FileInputStream obtains input bytes from the image file

				if (obj.getRuta() != null && !obj.getRuta().equals("")) {

					try {

						javaxt.io.Image image = new javaxt.io.Image(obj.getRuta());
						String desc = "";
						
						try {
							
							URL url = new URL(FRQConstantes.getRutaImagen()+obj.getRuta());
							InputStream fis =  url.openStream();
							Metadata metadata = ImageMetadataReader.readMetadata(fis);
							
							for (Directory directory : metadata.getDirectories()) {
				                for (Tag tag : directory.getTags()) {
				                    if(tag.getTagName().equals("Orientation")){
				                    	desc =  tag.getDescription();
				                    }
				                }
				             }
							
							switch (desc) {
							
							case "Top, left side (Horizontal / normal)":
								break;
							
							case "Top, right side (Mirror horizontal)":
								break;
								
							case "Bottom, right side (Rotate 180)":
								image.rotate(180);
								break;
								
							case "Bottom, left side (Mirror vertical)":
								image.rotate(-180);
								break;
								
							case "Left side, top (Mirror horizontal and rotate 270 CW)":
								image.rotate(-90);
								break;
								
							case "Right side, top (Rotate 90 CW)":
								image.rotate(90);
								break;
								
							case "Right side, bottom (Mirror horizontal and rotate 90 CW)":
								image.rotate(-270);
								break;
								
							case "Left side, bottom (Rotate 270 CW)":
								image.rotate(270);
								break;
								
							default:
								break;

							}
						
						} catch (Exception e) {
							logger.info("AP al obtener la información de la imagen a rotar");
							//image.rotate(90);
							// image.saveAs(obj.getRuta()+"erick.jpg");
						}
						byte[] b = image.getByteArray();

						InputStream inputStream = new FileInputStream(obj.getRuta());
						// Get the contents of an InputStream as a byte[].
						byte[] bytes = IOUtils.toByteArray(inputStream);
						// Adds a picture to the workbook
						int pictureIdx = workbook.addPicture(b, Workbook.PICTURE_TYPE_PNG);
						// close the input stream
						inputStream.close();
						// Returns an object that handles instantiating concrete classes
						CreationHelper helper = workbook.getCreationHelper();
						// Creates the top-level drawing patriarch.
						Drawing drawing = sheet.createDrawingPatriarch();
						// Create an anchor that is attached to the worksheet
						ClientAnchor anchor = helper.createClientAnchor();
						// create an anchor with upper left cell _and_ bottom right cell
						anchor.setCol1(10); // Column B
						anchor.setRow1(rowNum - 1); // Row 3
						anchor.setCol2(11); // Column C
						anchor.setRow2(rowNum); // Row 4
						// Creates a picture
						Picture pict = drawing.createPicture(anchor, pictureIdx);

					} catch (Exception e) {
						logger.info("AP IM HALLAZGOS ---" + obj.getRuta());
					}
				}

				row.setRowStyle(boddyCellStyle);
				row.setHeightInPoints(150);

				/* IMAGEN */

			}
		}
		// Se Agrega Adicionales AdicionalesBI adicionalesExpBI;

		/*
		 * if(idCeco!=null && !idCeco.isEmpty()) {
		 * 
		 * List<AdicionalesDTO> listaAdicionales = null; AdicionalesBI adicionalesBI =
		 * (AdicionalesBI) FRQAppContextProvider
		 * .getApplicationContext().getBean("adicionalesBI"); try {
		 * listaAdicionales=adicionalesBI.obtieneDatos(idCeco); } catch (Exception e) {
		 * logger.
		 * info("hubo un problema al obtener adicionales para generar el excel de hallazgos"
		 * ); } Cell cell; Row row = sheet.createRow(rowNum++); (cell =
		 * row.createCell(0)).setCellValue("ADICIONALES");
		 * cell.setCellStyle(headerCellStyle); // Create a Row
		 * 
		 */
		/*
		 * if(listaAdicionales != null && listaAdicionales.size()>0) { if(rowNum>1) {
		 * Row row2 = sheet.createRow(rowNum++); for(int i = 0; i < columns.length; i++)
		 * { Cell cell2 = row2.createCell(i); cell2.setCellValue(columns[i]);
		 * cell2.setCellStyle(headerCellStyle); } } int rowNum2 = rowNum++;
		 * for(AdicionalesDTO adi: listaAdicionales) { Cell cell; Row row =
		 * sheet.createRow(rowNum2++); (cell =
		 * row.createCell(0)).setCellValue("Adicional");
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(1)).setCellValue(tipoRecorrido); //Proyecto
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(2)).setCellValue(adi.getEco()); //Eco
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(3)).setCellValue(adi.getCeco()); //CECO
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(4)).setCellValue(adi.getNomCeco()); //Sucursal
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(5)).setCellValue(adi.getLiderObra());//Líder de Obra
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(6)).setCellValue(adi.getUsuReco()); //Franquicia
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(7)).setCellValue(adi.getFechaIni()); //Fecha de Recorrido
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(8)).setCellValue(adi.getClasifica()); //Capítulo
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(9)).setCellValue(adi.getPregunta()); //Item
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(10)).setCellValue(adi.getPosible()); //Respuesta
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(12)).setCellValue((adi.getCritica()==0)?"BAJA":(adi.getCritica
		 * ()==1)?"IMPERDONABLE":(adi.getCritica()==2)?"CRITICA":(adi.getCritica()==3)?
		 * "MEDIA":""); //Prioridad cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(13)).setCellValue(adi.getObs()); //Observaciones
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(14)).setCellValue("");//compromiso
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(15)).setCellValue((adi.getStatus()==0)?"RECHAZADO":(adi.
		 * getStatus()==1)?"PENDIENTE POR ATENDER":(adi.getStatus()==2)
		 * ?"PENDIENTE POR AUTORIZAR":(adi.getStatus()==3)?"AUTORIZADO":"");//status
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(16)).setCellValue("");//area de atencion
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(17)).setCellValue(""); cell.setCellStyle(boddyCellStyle);
		 * 
		 * //IMAGEN //FileInputStream obtains input bytes from the image file
		 * 
		 * if (adi.getRuta() != null && !adi.getRuta().isEmpty() ) {
		 * 
		 * try { InputStream inputStream = new FileInputStream(adi.getRuta()); //Get the
		 * contents of an InputStream as a byte[]. byte[] bytes =
		 * IOUtils.toByteArray(inputStream); //Adds a picture to the workbook int
		 * pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG); //close
		 * the input stream inputStream.close(); //Returns an object that handles
		 * instantiating concrete classes CreationHelper helper =
		 * workbook.getCreationHelper(); //Creates the top-level drawing patriarch.
		 * Drawing drawing = sheet.createDrawingPatriarch(); //Create an anchor that is
		 * attached to the worksheet ClientAnchor anchor = helper.createClientAnchor();
		 * //create an anchor with upper left cell _and_ bottom right cell
		 * anchor.setCol1(11); //Column B anchor.setRow1(rowNum2 - 1); //Row 3
		 * anchor.setCol2(12); //Column C anchor.setRow2(rowNum2); //Row 4 //Creates a
		 * picture Picture pict = drawing.createPicture(anchor, pictureIdx); } catch
		 * (Exception e) { logger.info("AP IM HALLAZGOS-ADICIONALES ---" +
		 * adi.getRuta()); } }
		 * 
		 * row.setRowStyle(boddyCellStyle); row.setHeightInPoints(150);
		 * 
		 * //IMAGEN } } }
		 */

		// Resize all columns to fit the content size
		for (int i = 0; i < columns.length; i++) {

			if (i == 10) {
				sheet.setColumnWidth(i, 10000);
			} else {
				sheet.autoSizeColumn(i);
			}
		}

		return workbook;

	}

	public Workbook generaAdicionalesCore(String tipoRecorrido, List<HallazgosTransfDTO> chklistPreguntas1,
			List<HallazgosTransfDTO> listaEvidencias, String idCeco, SucursalChecklistBI sucursalChecklistBI)
			throws IOException, ParseException {
		/**/
		//int version = 0;
		
		/*try {
		List<SucursalChecklistDTO> sucursal = sucursalChecklistBI.obtieneDatosSuc(idCeco);
		version = sucursal.get(0).getAux();
		} catch (Exception e) {
			logger.info("AP Al obtener la sucursalChecklistBI.obtieneDatosSuc(idCeco);");
		}*/

		/**/
		tipoRecorrido = "" + tipoRecorrido;

		// GENERAMOS WOORKBOOK
		String[] columns = { "Tipo", "Proyecto", "CECO", "Sucursal", "Líder de Obra", "Franquicia",
				"Fecha de Recorrido", "Capítulo", "Item", "Respuesta", "Imagen", "Observaciones", "Criticidad",
				"Fecha Compromiso", "Área de atención" };

		// Create a Workbook
		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file
		/*
		 * CreationHelper helps us create instances of various things like DataFormat,
		 * Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way
		 */
		CreationHelper createHelper = workbook.getCreationHelper();
		// Create a Sheet
		Sheet sheet = workbook.createSheet("Adicionales");
		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 18);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerCellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Create a Row
		Row headerRow = sheet.createRow(0);

		// Create cells
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		// Cells Format
		Font bodyFont = workbook.createFont();
		bodyFont.setBold(false);
		bodyFont.setFontHeightInPoints((short) 14);

		boddyCellStyle = workbook.createCellStyle();
		boddyCellStyle.setFont(bodyFont);
		boddyCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		boddyCellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Create Other rows and cells with data
		int rowNum = 1;
		if (chklistPreguntas1 != null && chklistPreguntas1.size() > 0) {
			for (HallazgosTransfDTO obj : chklistPreguntas1) {
				if (rowNum == 1) {
					idCeco = obj.getCeco();
				}
				Cell cell;
				Row row = sheet.createRow(rowNum++);
				// (cell = filaExcel.createCell(columna++)).setCellValue(idCeco);
				(cell = row.createCell(0)).setCellValue("Adicional");
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(1)).setCellValue(obj.getNombreFase());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(2)).setCellValue(obj.getCeco());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(3)).setCellValue(obj.getCecoNom());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(4)).setCellValue(obj.getLiderObra());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(5)).setCellValue(obj.getNomUsu());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(6)).setCellValue(obj.getFechaTermino());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(7)).setCellValue(obj.getNombChek());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(8)).setCellValue(obj.getPreg());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(9)).setCellValue(obj.getRespuesta());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(11)).setCellValue(obj.getObs());
				cell.setCellStyle(boddyCellStyle);
				switch (obj.getCriticidad()) {
				case 0:
					(cell = row.createCell(12)).setCellValue("Bajo");// Criticidad
					break;
				case 1:
					(cell = row.createCell(12)).setCellValue("Imperdonable");// Criticidad
					break;
				case 2:
					(cell = row.createCell(12)).setCellValue("Alto");// Criticidad
					break;
				case 3:
					(cell = row.createCell(12)).setCellValue("Medio");// Criticidad
					break;
				case 4:
					(cell = row.createCell(12)).setCellValue("Bajo");// Criticidad
					break;
				}
				cell.setCellStyle(boddyCellStyle);
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				String dateInString = obj.getFechaTermino();

				if (obj.getFechaTermino() != null) {
					Date date = formatter.parse(dateInString);
					LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
					localDateTime = localDateTime.plusDays(Long.parseLong(obj.getSla()));
					date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(13)).setCellValue(formatter.format(date) + "");// Fecha Hecha + SLA
				} else {
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(13)).setCellValue("");
				}

				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(14)).setCellValue(obj.getArea());//
				cell.setCellStyle(boddyCellStyle);

				/* IMAGEN */
				// FileInputStream obtains input bytes from the image file

				if (obj.getRuta() != null && !obj.getRuta().equals("")) {

					try {

						javaxt.io.Image image = new javaxt.io.Image(obj.getRuta());
						String desc = "";
						
						try {
							
							URL url = new URL(FRQConstantes.getRutaImagen()+obj.getRuta());
							InputStream fis =  url.openStream();
							Metadata metadata = ImageMetadataReader.readMetadata(fis);
							
							for (Directory directory : metadata.getDirectories()) {
				                for (Tag tag : directory.getTags()) {
				                    if(tag.getTagName().equals("Orientation")){
				                    	desc =  tag.getDescription();
				                    }
				                }
				             }
							
							switch (desc) {
							
							case "Top, left side (Horizontal / normal)":
								break;
							
							case "Top, right side (Mirror horizontal)":
								break;
								
							case "Bottom, right side (Rotate 180)":
								image.rotate(180);
								break;
								
							case "Bottom, left side (Mirror vertical)":
								image.rotate(-180);
								break;
								
							case "Left side, top (Mirror horizontal and rotate 270 CW)":
								image.rotate(-90);
								break;
								
							case "Right side, top (Rotate 90 CW)":
								image.rotate(90);
								break;
								
							case "Right side, bottom (Mirror horizontal and rotate 90 CW)":
								image.rotate(-270);
								break;
								
							case "Left side, bottom (Rotate 270 CW)":
								image.rotate(270);
								break;
								
							default:
								break;

							}
						
						} catch (Exception e) {
							image.rotate(90);
							// image.saveAs(obj.getRuta()+"erick.jpg");
						}
						byte[] b = image.getByteArray();

						InputStream inputStream = new FileInputStream(obj.getRuta());
						// Get the contents of an InputStream as a byte[].
						byte[] bytes = IOUtils.toByteArray(inputStream);
						// Adds a picture to the workbook
						int pictureIdx = workbook.addPicture(b, Workbook.PICTURE_TYPE_PNG);
						// close the input stream
						inputStream.close();
						// Returns an object that handles instantiating concrete classes
						CreationHelper helper = workbook.getCreationHelper();
						// Creates the top-level drawing patriarch.
						Drawing drawing = sheet.createDrawingPatriarch();
						// Create an anchor that is attached to the worksheet
						ClientAnchor anchor = helper.createClientAnchor();
						// create an anchor with upper left cell _and_ bottom right cell
						anchor.setCol1(10); // Column B
						anchor.setRow1(rowNum - 1); // Row 3
						anchor.setCol2(11); // Column C
						anchor.setRow2(rowNum); // Row 4
						// Creates a picture
						Picture pict = drawing.createPicture(anchor, pictureIdx);

					} catch (Exception e) {
						logger.info("AP IM HALLAZGOS ---" + obj.getRuta());
					}
				}

				row.setRowStyle(boddyCellStyle);
				row.setHeightInPoints(150);

				/* IMAGEN */

			}
		}
		// Se Agrega Adicionales AdicionalesBI adicionalesExpBI;

		/*
		 * if(idCeco!=null && !idCeco.isEmpty()) {
		 * 
		 * List<AdicionalesDTO> listaAdicionales = null; AdicionalesBI adicionalesBI =
		 * (AdicionalesBI) FRQAppContextProvider
		 * .getApplicationContext().getBean("adicionalesBI"); try {
		 * listaAdicionales=adicionalesBI.obtieneDatos(idCeco); } catch (Exception e) {
		 * logger.
		 * info("hubo un problema al obtener adicionales para generar el excel de hallazgos"
		 * ); } Cell cell; Row row = sheet.createRow(rowNum++); (cell =
		 * row.createCell(0)).setCellValue("ADICIONALES");
		 * cell.setCellStyle(headerCellStyle); // Create a Row
		 * 
		 */
		/*
		 * if(listaAdicionales != null && listaAdicionales.size()>0) { if(rowNum>1) {
		 * Row row2 = sheet.createRow(rowNum++); for(int i = 0; i < columns.length; i++)
		 * { Cell cell2 = row2.createCell(i); cell2.setCellValue(columns[i]);
		 * cell2.setCellStyle(headerCellStyle); } } int rowNum2 = rowNum++;
		 * for(AdicionalesDTO adi: listaAdicionales) { Cell cell; Row row =
		 * sheet.createRow(rowNum2++); (cell =
		 * row.createCell(0)).setCellValue("Adicional");
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(1)).setCellValue(tipoRecorrido); //Proyecto
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(2)).setCellValue(adi.getEco()); //Eco
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(3)).setCellValue(adi.getCeco()); //CECO
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(4)).setCellValue(adi.getNomCeco()); //Sucursal
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(5)).setCellValue(adi.getLiderObra());//Líder de Obra
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(6)).setCellValue(adi.getUsuReco()); //Franquicia
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(7)).setCellValue(adi.getFechaIni()); //Fecha de Recorrido
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(8)).setCellValue(adi.getClasifica()); //Capítulo
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(9)).setCellValue(adi.getPregunta()); //Item
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(10)).setCellValue(adi.getPosible()); //Respuesta
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(12)).setCellValue((adi.getCritica()==0)?"BAJA":(adi.getCritica
		 * ()==1)?"IMPERDONABLE":(adi.getCritica()==2)?"CRITICA":(adi.getCritica()==3)?
		 * "MEDIA":""); //Prioridad cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(13)).setCellValue(adi.getObs()); //Observaciones
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(14)).setCellValue("");//compromiso
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(15)).setCellValue((adi.getStatus()==0)?"RECHAZADO":(adi.
		 * getStatus()==1)?"PENDIENTE POR ATENDER":(adi.getStatus()==2)
		 * ?"PENDIENTE POR AUTORIZAR":(adi.getStatus()==3)?"AUTORIZADO":"");//status
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(16)).setCellValue("");//area de atencion
		 * cell.setCellStyle(boddyCellStyle); (cell =
		 * row.createCell(17)).setCellValue(""); cell.setCellStyle(boddyCellStyle);
		 * 
		 * //IMAGEN //FileInputStream obtains input bytes from the image file
		 * 
		 * if (adi.getRuta() != null && !adi.getRuta().isEmpty() ) {
		 * 
		 * try { InputStream inputStream = new FileInputStream(adi.getRuta()); //Get the
		 * contents of an InputStream as a byte[]. byte[] bytes =
		 * IOUtils.toByteArray(inputStream); //Adds a picture to the workbook int
		 * pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG); //close
		 * the input stream inputStream.close(); //Returns an object that handles
		 * instantiating concrete classes CreationHelper helper =
		 * workbook.getCreationHelper(); //Creates the top-level drawing patriarch.
		 * Drawing drawing = sheet.createDrawingPatriarch(); //Create an anchor that is
		 * attached to the worksheet ClientAnchor anchor = helper.createClientAnchor();
		 * //create an anchor with upper left cell _and_ bottom right cell
		 * anchor.setCol1(11); //Column B anchor.setRow1(rowNum2 - 1); //Row 3
		 * anchor.setCol2(12); //Column C anchor.setRow2(rowNum2); //Row 4 //Creates a
		 * picture Picture pict = drawing.createPicture(anchor, pictureIdx); } catch
		 * (Exception e) { logger.info("AP IM HALLAZGOS-ADICIONALES ---" +
		 * adi.getRuta()); } }
		 * 
		 * row.setRowStyle(boddyCellStyle); row.setHeightInPoints(150);
		 * 
		 * //IMAGEN } } }
		 */

		// Resize all columns to fit the content size
		for (int i = 0; i < columns.length; i++) {

			if (i == 10) {
				sheet.setColumnWidth(i, 10000);
			} else {
				sheet.autoSizeColumn(i);
			}
		}

		return workbook;

	}
	public Workbook generaHallazgosAtendidos(List<ChecklistPreguntasComDTO> chklistPreguntasIniciales,
			List<ChecklistPreguntasComDTO> chklistPreguntasTabla, String idCeco) throws IOException {
		/**/
		int version = 0;
		List<SucursalChecklistDTO> sucursal = null;
		SucursalChecklistBI sucursalChecklistBI = (SucursalChecklistBI) FRQAppContextProvider.getApplicationContext()
				.getBean("sucursalChecklistBI");
		try {
			sucursal = sucursalChecklistBI.obtieneDatosSuc(idCeco);
		} catch (Exception e) {
			logger.info("hubo un problema al obtener adicionales para generar el excel de hallazgos");
		}

		// String tipoNegocio = sucursal.get(0).getNegocio();
		version = sucursal.get(0).getAux();

		// if (tipoNegocio.equalsIgnoreCase("EKT")) {
		if (version == 1) {
			chklistPreguntasIniciales = getHallazgosConcatenados(chklistPreguntasIniciales);
			chklistPreguntasTabla = getHallazgosConcatenados(chklistPreguntasTabla);
		}

		List<ChecklistPreguntasComDTO> nuevasPreguntas = new ArrayList<ChecklistPreguntasComDTO>();

		// GENERAMOS WOORKBOOK
		String[] columns = { "Eco", "CECO", "Sucursal", "Líder de Obra", "Franquicia", "Fecha de Recorrido", "Capítulo",
				"Item", "Respuesta", "Imagen", "Observaciones", "", "Item Hallazgo", "Respuesta Hallazgo",
				"Imagen Hallazgo", "Observaciones Hallazgo", "" };

		// Create a Workbook
		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file
		/*
		 * CreationHelper helps us create instances of various things like DataFormat,
		 * Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way
		 */
		CreationHelper createHelper = workbook.getCreationHelper();
		// Create a Sheet
		Sheet sheet = workbook.createSheet("Hallazgos");
		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 18);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerCellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Create a Row
		Row headerRow = sheet.createRow(0);

		// Create cells
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		// Cells Format
		Font bodyFont = workbook.createFont();
		bodyFont.setBold(false);
		bodyFont.setFontHeightInPoints((short) 18);

		boddyCellStyle = workbook.createCellStyle();
		boddyCellStyle.setFont(bodyFont);
		boddyCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		boddyCellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Create Other rows and cells with data
		int rowNum = 1;
		for (ChecklistPreguntasComDTO obj : chklistPreguntasIniciales) {

			Cell cell;
			Row row = sheet.createRow(rowNum++);
			// (cell = filaExcel.createCell(columna++)).setCellValue(idCeco);
			(cell = row.createCell(0)).setCellValue(obj.getEco());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(1)).setCellValue(obj.getCeco());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(2)).setCellValue(obj.getNombCeco());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(3)).setCellValue(obj.getLiderObra());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(4)).setCellValue(obj.getRealizoReco());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(5)).setCellValue(obj.getFechaFin());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(6)).setCellValue(obj.getClasif());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(7)).setCellValue(obj.getPregunta());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(8)).setCellValue(obj.getPosible());
			cell.setCellStyle(boddyCellStyle);
			// LA 9 ES LA IMAGEN
			(cell = row.createCell(10)).setCellValue(obj.getObserv());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(11)).setCellValue("");
			cell.setCellStyle(boddyCellStyle);

			/* IMAGEN */
			// FileInputStream obtains input bytes from the image file

			if (obj.getRuta() != null && !obj.getRuta().equals("")) {
				try {
					InputStream inputStream = new FileInputStream(obj.getRuta());
					// Get the contents of an InputStream as a byte[].
					byte[] bytes = IOUtils.toByteArray(inputStream);
					// Adds a picture to the workbook
					int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
					// close the input stream
					inputStream.close();
					// Returns an object that handles instantiating concrete classes
					CreationHelper helper = workbook.getCreationHelper();
					// Creates the top-level drawing patriarch.
					Drawing drawing = sheet.createDrawingPatriarch();
					// Create an anchor that is attached to the worksheet
					ClientAnchor anchor = helper.createClientAnchor();
					// create an anchor with upper left cell _and_ bottom right cell
					anchor.setCol1(9); // Column B
					anchor.setRow1(rowNum - 1); // Row 3
					anchor.setCol2(10); // Column C
					anchor.setRow2(rowNum); // Row 4
					// Creates a picture
					Picture pict = drawing.createPicture(anchor, pictureIdx);
				} catch (Exception e) {
					logger.info("AP IM HALLAZGOS ---" + obj.getRuta());
				}
			}
			// IMAGEN
			row.setRowStyle(boddyCellStyle);
			row.setHeightInPoints(150);

			// Hallazgos Tabla, buscamos el ID de la Pregunta en la tabla de hallazgos y
			// pintamos de este objeto
			for (ChecklistPreguntasComDTO objTabla : chklistPreguntasTabla) {

				if ((obj.getIdPreg() == objTabla.getIdPreg()) && objTabla.getPosible().equalsIgnoreCase("SI")) {
					(cell = row.createCell(12)).setCellValue(objTabla.getPregunta());
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(13)).setCellValue(objTabla.getPosible());
					cell.setCellStyle(boddyCellStyle);
					// LA 14 ES LA IMAGEN
					(cell = row.createCell(15)).setCellValue(objTabla.getObsNueva());
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(16)).setCellValue("");
					cell.setCellStyle(boddyCellStyle);

					/* IMAGEN */
					// FileInputStream obtains input bytes from the image file
					if (objTabla.getRuta() != null && !objTabla.getRuta().equals("")) {
						try {
							InputStream inputStream = new FileInputStream(objTabla.getRuta());
							// Get the contents of an InputStream as a byte[].
							byte[] bytes = IOUtils.toByteArray(inputStream);
							// Adds a picture to the workbook
							int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
							// close the input stream
							inputStream.close();
							// Returns an object that handles instantiating concrete classes
							CreationHelper helper = workbook.getCreationHelper();
							// Creates the top-level drawing patriarch.
							Drawing drawing = sheet.createDrawingPatriarch();
							// Create an anchor that is attached to the worksheet
							ClientAnchor anchor = helper.createClientAnchor();
							// create an anchor with upper left cell _and_ bottom right cell
							anchor.setCol1(14); // Column B
							anchor.setRow1(rowNum - 1); // Row 3
							anchor.setCol2(15); // Column C
							anchor.setRow2(rowNum); // Row 4
							// Creates a picture
							Picture pict = drawing.createPicture(anchor, pictureIdx);
						} catch (Exception e) {
							logger.info("AP IM HALLAZGOS ---" + objTabla.getRuta());
						}
					}
					// IMAGEN HALLAZGO TABLA
					row.setRowStyle(boddyCellStyle);
					row.setHeightInPoints(150);
					/* IMAGEN HALLAZGO TABLA */
					break;
				}
			}
			// Hallazgos Tabla
			/* IMAGEN HALLAZGO TABLA */
		}

		Cell cellDiv0;
		Row rowDiv0 = sheet.createRow(rowNum++);
		(cellDiv0 = rowDiv0.createCell(0)).setCellValue("");
		Cell cellDiv;
		Row rowDiv = sheet.createRow(rowNum++);
		(cellDiv = rowDiv.createCell(0)).setCellValue("ESTATUS DE HALLAZGOS");
		Cell cellDiv1;
		Row rowDiv1 = sheet.createRow(rowNum++);
		(cellDiv1 = rowDiv1.createCell(0)).setCellValue("");

		CellStyle divCellStyle;
		divCellStyle = workbook.createCellStyle();
		divCellStyle.setFont(headerFont);
		divCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		divCellStyle.setAlignment(HorizontalAlignment.CENTER);

		cellDiv.setCellStyle(divCellStyle);

		// OBTENEMOS NUEVAS EVIDENCIAS
		nuevasPreguntas.clear();
		for (ChecklistPreguntasComDTO objPregTabla : chklistPreguntasTabla) {
			boolean hay = false;
			for (ChecklistPreguntasComDTO objPregIniciales : chklistPreguntasIniciales) {
				if (objPregTabla.getIdPreg() == objPregIniciales.getIdPreg()) {
					hay = true;
					break;
				}
			}
			if (!hay) {
				nuevasPreguntas.add(objPregTabla);
			}
		}

		// SE INSERTAN LAS NUEVAS PREGUNTAS
		for (ChecklistPreguntasComDTO objNuevas : nuevasPreguntas) {

			// if (objNuevas.getPosible().equalsIgnoreCase("SI")) {
			Cell cell;
			Row row = sheet.createRow(rowNum++);
			// (cell = filaExcel.createCell(columna++)).setCellValue(idCeco);
			(cell = row.createCell(0)).setCellValue(objNuevas.getEco());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(1)).setCellValue(objNuevas.getCeco());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(2)).setCellValue(objNuevas.getNombCeco());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(3)).setCellValue(objNuevas.getLiderObra());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(4)).setCellValue(objNuevas.getRealizoReco());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(5)).setCellValue(objNuevas.getFechaFin());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(6)).setCellValue(objNuevas.getClasif());
			cell.setCellStyle(boddyCellStyle);

			row.setRowStyle(boddyCellStyle);
			row.setHeightInPoints(150);
			(cell = row.createCell(7)).setCellValue(objNuevas.getPregunta());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(8)).setCellValue(objNuevas.getPosible());
			cell.setCellStyle(boddyCellStyle);

			// LA 9 ES LA IMAGEN

			(cell = row.createCell(10)).setCellValue(objNuevas.getObserv());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(11)).setCellValue("");
			cell.setCellStyle(boddyCellStyle);

			/* IMAGEN */
			// FileInputStream obtains input bytes from the image file
			// IMAGEN

			/*
			 * row.setRowStyle(boddyCellStyle); row.setHeightInPoints(150); (cell =
			 * row.createCell(12)).setCellValue(objNuevas.getPregunta());
			 * cell.setCellStyle(boddyCellStyle); (cell =
			 * row.createCell(13)).setCellValue(objNuevas.getPosible());
			 * cell.setCellStyle(boddyCellStyle); //LA 14 ES LA IMAGEN (cell =
			 * row.createCell(15)).setCellValue(objNuevas.getObserv());
			 * cell.setCellStyle(boddyCellStyle); (cell =
			 * row.createCell(16)).setCellValue(""); cell.setCellStyle(boddyCellStyle);
			 */
			if (objNuevas.getRuta() != null && !objNuevas.getRuta().equals("")) {
				try {
					InputStream inputStream = new FileInputStream(objNuevas.getRuta());
					// Get the contents of an InputStream as a byte[].
					byte[] bytes = IOUtils.toByteArray(inputStream);
					// Adds a picture to the workbook
					int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
					// close the input stream
					inputStream.close();
					// Returns an object that handles instantiating concrete classes
					CreationHelper helper = workbook.getCreationHelper();
					// Creates the top-level drawing patriarch.
					Drawing drawing = sheet.createDrawingPatriarch();
					// Create an anchor that is attached to the worksheet
					ClientAnchor anchor = helper.createClientAnchor();
					// create an anchor with upper left cell _and_ bottom right cell
					anchor.setCol1(10); // Column B
					anchor.setRow1(rowNum - 1); // Row 3
					anchor.setCol2(11); // Column C
					anchor.setRow2(rowNum); // Row 4
					// Creates a picture
					Picture pict = drawing.createPicture(anchor, pictureIdx);
				} catch (Exception e) {
					logger.info("AP IM HALLAZGOS ---" + objNuevas.getRuta());
				}
			}
			// IMAGEN HALLAZGO TABLA
			row.setRowStyle(boddyCellStyle);
			row.setHeightInPoints(150);
			/* IMAGEN HALLAZGO TABLA */
			// }
		}

		// Resize all columns to fit the content size
		for (int i = 0; i < columns.length; i++) {

			if (i == 10) {
				sheet.setColumnWidth(i, 10000);
			} else {
				sheet.autoSizeColumn(i);
			}
		}

		return workbook;
	}

	public Workbook generaHallazgosSemana(String tipoRecorrido, List<ChecklistHallazgosRepoDTO> chklistPreguntas1)
			throws IOException {
		/**/

		chklistPreguntas1 = getHallazgosConcatenadosSemana(chklistPreguntas1);

		/**/
		tipoRecorrido = getNombreRecorrido(tipoRecorrido);

		// GENERAMOS WOORKBOOK
		String[] columns = { "Proyecto", "Eco", "CECO", "Sucursal", "Líder de Obra", "Franquicia", "Fecha de Recorrido",
				"Capítulo", "Item", "Respuesta", "Imagen", "Prioridad", "Observaciones", "Compromiso", "Estatus",
				"Área de atención" };

		// Create a Workbook
		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file
		/*
		 * CreationHelper helps us create instances of various things like DataFormat,
		 * Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way
		 */
		CreationHelper createHelper = workbook.getCreationHelper();
		// Create a Sheet
		Sheet sheet = workbook.createSheet("Hallazgos");
		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 18);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerCellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Create a Row
		Row headerRow = sheet.createRow(0);

		// Create cells
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		// Cells Format
		Font bodyFont = workbook.createFont();
		bodyFont.setBold(false);
		bodyFont.setFontHeightInPoints((short) 18);

		boddyCellStyle = workbook.createCellStyle();
		boddyCellStyle.setFont(bodyFont);
		boddyCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		boddyCellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Create Other rows and cells with data
		int rowNum = 1;
		int bitacoraPapa = 0;
		for (int i = 0; i < chklistPreguntas1.size(); i++) {

			ChecklistHallazgosRepoDTO obj = chklistPreguntas1.get(i);

			if (i == 0) {
				bitacoraPapa = chklistPreguntas1.get(0).getIdBitaGral();
			}

			if (chklistPreguntas1.get(i).getIdBitaGral() != bitacoraPapa) {
				bitacoraPapa = chklistPreguntas1.get(i).getIdBitaGral();

				// Create a Row
				Row headerRow1 = sheet.createRow(rowNum++);

				// Create cells
				for (int j = 0; j < columns.length; j++) {
					Cell cell = headerRow1.createCell(j);
					cell.setCellValue(columns[j]);
					cell.setCellStyle(headerCellStyle);
				}

			}

			logger.info("BITACORA");

			Cell cell;
			Row row = sheet.createRow(rowNum++);
			// (cell = filaExcel.createCell(columna++)).setCellValue(idCeco);

			(cell = row.createCell(0)).setCellValue(tipoRecorrido);
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(1)).setCellValue(obj.getEco());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(2)).setCellValue(obj.getCeco());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(3)).setCellValue(obj.getNombCeco());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(4)).setCellValue(obj.getLiderObra());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(5)).setCellValue(obj.getRealizoReco());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(6)).setCellValue(obj.getFechaFin());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(7)).setCellValue(obj.getClasif());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(8)).setCellValue(obj.getPregunta());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(9)).setCellValue(obj.getPosible());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(11)).setCellValue("");
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(12)).setCellValue(obj.getObserv());
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(13)).setCellValue("");
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(14)).setCellValue("");
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(15)).setCellValue("");
			cell.setCellStyle(boddyCellStyle);
			(cell = row.createCell(16)).setCellValue("");
			cell.setCellStyle(boddyCellStyle);

			/* IMAGEN */
			// FileInputStream obtains input bytes from the image file

			if (obj.getRuta() != null && !obj.getRuta().equals("")) {

				try {
					InputStream inputStream = new FileInputStream(obj.getRuta());
					// Get the contents of an InputStream as a byte[].
					byte[] bytes = IOUtils.toByteArray(inputStream);
					// Adds a picture to the workbook
					int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
					// close the input stream
					inputStream.close();
					// Returns an object that handles instantiating concrete classes
					CreationHelper helper = workbook.getCreationHelper();
					// Creates the top-level drawing patriarch.
					Drawing drawing = sheet.createDrawingPatriarch();
					// Create an anchor that is attached to the worksheet
					ClientAnchor anchor = helper.createClientAnchor();
					// create an anchor with upper left cell _and_ bottom right cell
					anchor.setCol1(10); // Column B
					anchor.setRow1(rowNum - 1); // Row 3
					anchor.setCol2(11); // Column C
					anchor.setRow2(rowNum); // Row 4
					// Creates a picture
					Picture pict = drawing.createPicture(anchor, pictureIdx);
				} catch (Exception e) {
					logger.info("AP IM HALLAZGOS ---" + obj.getRuta());
				}
			}

			row.setRowStyle(boddyCellStyle);
			row.setHeightInPoints(150);

			/* IMAGEN */

		}

		// Resize all columns to fit the content size
		for (int i = 0; i < columns.length; i++) {

			if (i == 10) {
				sheet.setColumnWidth(i, 10000);
			} else {
				sheet.autoSizeColumn(i);
			}
		}

		return workbook;

	}

	public List<ChecklistPreguntasComDTO> getHallazgosConcatenados(List<ChecklistPreguntasComDTO> lista) {

		List<ChecklistPreguntasComDTO> chklistPreguntasImp = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> listaResult = new ArrayList<ChecklistPreguntasComDTO>();

		listaResult.clear();

		chklistPreguntasImp = lista;

		for (int i = 0; i < chklistPreguntasImp.size(); i++) {

			if (chklistPreguntasImp.get(i).getPregPadre() == 0) {

				for (int j = 0; j < chklistPreguntasImp.size(); j++) {

					if (chklistPreguntasImp.get(i).getIdPreg() == chklistPreguntasImp.get(j).getPregPadre()) {

						ChecklistPreguntasComDTO data = chklistPreguntasImp.get(j);
						data.setPregunta(chklistPreguntasImp.get(i).getPregunta() + " : "
								+ chklistPreguntasImp.get(j).getPregunta());
						listaResult.add(data);
					}
				}
			}
		}

		return listaResult;

	}

	public List<ChecklistHallazgosRepoDTO> getHallazgosConcatenadosSemana(List<ChecklistHallazgosRepoDTO> lista) {

		List<ChecklistHallazgosRepoDTO> chklistPreguntasImp = new ArrayList<ChecklistHallazgosRepoDTO>();
		List<ChecklistHallazgosRepoDTO> listaResult = new ArrayList<ChecklistHallazgosRepoDTO>();

		listaResult.clear();

		chklistPreguntasImp = lista;

		int preguntasPadre = 0;
		int preguntashijas = 0;

		for (int i = 0; i < chklistPreguntasImp.size(); i++) {

			if (chklistPreguntasImp.get(i).getPregPadre() == 0) {

				logger.info("Preguntas padre -----> " + preguntasPadre++);

				for (int j = 0; j < chklistPreguntasImp.size(); j++) {

					logger.info("BITA PAPA 1: " + chklistPreguntasImp.get(i).getIdBitaGral());
					logger.info("BITA PAPA 2: " + chklistPreguntasImp.get(j).getIdBitaGral());

					if (chklistPreguntasImp.get(i).getIdPreg() == chklistPreguntasImp.get(j).getPregPadre()
							&& chklistPreguntasImp.get(i).getIdBitaGral() == chklistPreguntasImp.get(j)
									.getIdBitaGral()) {
						logger.info("Preguntas hija -----> " + preguntashijas++);

						logger.info("Pregunta Padre: " + chklistPreguntasImp.get(i).getPregunta());
						logger.info("ID Padre: " + chklistPreguntasImp.get(i).getIdPreg());
						logger.info("----------------------------");
						logger.info("Pregunta Hija: " + chklistPreguntasImp.get(j).getPregunta());
						logger.info("ID Padre: " + chklistPreguntasImp.get(j).getIdPreg());

						logger.info(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ");

						ChecklistHallazgosRepoDTO data = chklistPreguntasImp.get(j);

						data.setPregunta(chklistPreguntasImp.get(i).getPregunta() + " : "
								+ chklistPreguntasImp.get(j).getPregunta());

						listaResult.add(data);
					}
				}
			}
		}

		return listaResult;

	}
	
	public Workbook generaHistorialHallazgosCore(String fase, List<HallazgosTransfDTO> listaHallazgos, 
			List<HallazgosTransfDTO> listaHallazgos2, String idCeco, SucursalChecklistBI sucursalChecklistBI)
			throws IOException, ParseException {
		/**/
		int version = 0;
		List<SucursalChecklistDTO> sucursal = sucursalChecklistBI.obtieneDatosSuc(idCeco);
		//version = sucursal.get(0).getAux();

		// GENERAMOS WOORKBOOK
		String[] columns = { "Tipo", "Proyecto", "CECO", "Sucursal", "Líder de Obra", "Franquicia",
				"Fecha de Recorrido", "Capítulo", "Item", "Respuesta", "Imagen", "Observaciones", "Criticidad",
				"Fecha Compromiso", "Área de atención", "", "Tipo", "Proyecto", "CECO", "Sucursal", "Líder de Obra", "Franquicia", 
				"Capítulo", "Item", "Respuesta", "Imagen", "Observaciones", "Criticidad",
				"Fecha de Atención", "Área de atención"};

		// Create a Workbook
		Workbook workbook = new XSSFWorkbook();
		/*
		 * CreationHelper helps us create instances of various things like DataFormat,
		 * Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way
		 */
		CreationHelper createHelper = workbook.getCreationHelper();
		// Create a Sheet
		Sheet sheet = workbook.createSheet("Historial_Hallazgos");
		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 18);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerCellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Create a Row
		Row headerRow = sheet.createRow(0);

		// Create cells
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		// Cells Format
		Font bodyFont = workbook.createFont();
		bodyFont.setBold(false);
		bodyFont.setFontHeightInPoints((short) 14);

		boddyCellStyle = workbook.createCellStyle();
		boddyCellStyle.setFont(bodyFont);
		boddyCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		boddyCellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Create Other rows and cells with data
		int rowNum = 1;
		if (listaHallazgos != null && listaHallazgos.size() > 0) {
			//for (HallazgosTransfDTO obj : listaHallazgos) {
			for (int a = 0; a<listaHallazgos.size(); a++) {
				HallazgosTransfDTO obj = listaHallazgos.get(a);
				HallazgosTransfDTO obj2 = listaHallazgos2.get(a);
				if (rowNum == 1) {
					idCeco = obj.getCeco();
				}
				Cell cell;
				Row row = sheet.createRow(rowNum++);
				//ListaHallazgos
				(cell = row.createCell(0)).setCellValue("Hallazgo");
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(1)).setCellValue(obj.getNombreFase());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(2)).setCellValue(obj.getCeco());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(3)).setCellValue(obj.getCecoNom());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(4)).setCellValue(obj.getLiderObra());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(5)).setCellValue(obj.getNomUsu());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(6)).setCellValue(obj.getFechaTermino());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(7)).setCellValue(obj.getNombChek());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(8)).setCellValue(obj.getPreg());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(9)).setCellValue(obj.getRespuesta());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(11)).setCellValue(obj.getObs());
				cell.setCellStyle(boddyCellStyle);
				//ListaHallazgos2
				(cell = row.createCell(16)).setCellValue("Atencion de Hallazgo");
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(17)).setCellValue(obj2.getNombreFase());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(18)).setCellValue(obj2.getCeco());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(19)).setCellValue(obj2.getCecoNom());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(20)).setCellValue(obj2.getLiderObra());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(21)).setCellValue(obj2.getNomUsu());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(22)).setCellValue(obj2.getNombChek());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(23)).setCellValue(obj2.getPreg());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(24)).setCellValue(obj2.getRespuesta());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(26)).setCellValue(obj2.getObsAtencion());
				cell.setCellStyle(boddyCellStyle);
				//ListaHallazgos
				switch (obj.getCriticidad()) {
				case 0:
					(cell = row.createCell(12)).setCellValue("Bajo");// Criticidad
					break;
				case 1:
					(cell = row.createCell(12)).setCellValue("Imperdonable");// Criticidad
					break;
				case 2:
					(cell = row.createCell(12)).setCellValue("Alto");// Criticidad
					break;
				case 3:
					(cell = row.createCell(12)).setCellValue("Medio");// Criticidad
					break;
				case 4:
					(cell = row.createCell(12)).setCellValue("Bajo");// Criticidad
					break;
				}
				cell.setCellStyle(boddyCellStyle);
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				String dateInString = obj.getFechaTermino();

				if (obj.getFechaTermino() != null) {
					Date date = formatter.parse(dateInString);
					LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
					localDateTime = localDateTime.plusDays(Long.parseLong(obj.getSla()));
					date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(13)).setCellValue(formatter.format(date) + "");// Fecha Hecha + SLA
				} else {
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(13)).setCellValue("");
				}

				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(14)).setCellValue(obj.getArea());
				cell.setCellStyle(boddyCellStyle);
				
				//ListaHallazgos2
				switch (obj2.getCriticidad()) {
				case 0:
					(cell = row.createCell(27)).setCellValue("Bajo");// Criticidad
					break;
				case 1:
					(cell = row.createCell(27)).setCellValue("Imperdonable");// Criticidad
					break;
				case 2:
					(cell = row.createCell(27)).setCellValue("Alto");// Criticidad
					break;
				case 3:
					(cell = row.createCell(27)).setCellValue("Medio");// Criticidad
					break;
				case 4:
					(cell = row.createCell(27)).setCellValue("Bajo");// Criticidad
					break;
					
				}
				cell.setCellStyle(boddyCellStyle);
				String dateInString2 = obj2.getPeriodo();

				if (obj2.getPeriodo() != null) {
					Date date2 = formatter.parse(dateInString2);
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(28)).setCellValue(formatter.format(date2) + "");// Fecha Hecha + SLA
				} else {
					cell.setCellStyle(boddyCellStyle);
					(cell = row.createCell(28)).setCellValue("");
				}

				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(29)).setCellValue(obj2.getArea());
				cell.setCellStyle(boddyCellStyle);

				// IMAGEN ListaHallazgos
				if (obj.getRuta() != null && !obj.getRuta().equals("")) {
					try {
						javaxt.io.Image image = new javaxt.io.Image(obj.getRuta());
						String desc = "";
						try {
							
							URL url = new URL(FRQConstantes.getRutaImagen()+obj.getRuta());
							InputStream fis =  url.openStream();
							Metadata metadata = ImageMetadataReader.readMetadata(fis);
							
							for (Directory directory : metadata.getDirectories()) {
				                for (Tag tag : directory.getTags()) {
				                    if(tag.getTagName().equals("Orientation")){
				                    	desc =  tag.getDescription();
				                    }
				                }
				             }
							
							switch (desc) {
							
							case "Top, left side (Horizontal / normal)":
								break;
							
							case "Top, right side (Mirror horizontal)":
								break;
								
							case "Bottom, right side (Rotate 180)":
								image.rotate(180);
								break;
								
							case "Bottom, left side (Mirror vertical)":
								image.rotate(-180);
								break;
								
							case "Left side, top (Mirror horizontal and rotate 270 CW)":
								image.rotate(-90);
								break;
								
							case "Right side, top (Rotate 90 CW)":
								image.rotate(90);
								break;
								
							case "Right side, bottom (Mirror horizontal and rotate 90 CW)":
								image.rotate(-270);
								break;
								
							case "Left side, bottom (Rotate 270 CW)":
								image.rotate(270);
								break;
								
							default:
								break;

							}
						
						} catch (Exception e) {
							logger.info(e.getMessage());
						}
						byte[] b = image.getByteArray();
						InputStream inputStream = new FileInputStream(obj.getRuta());
						// Get the contents of an InputStream as a byte[].
						byte[] bytes = IOUtils.toByteArray(inputStream);
						// Adds a picture to the workbook
						int pictureIdx = workbook.addPicture(b, Workbook.PICTURE_TYPE_PNG);
						// close the input stream
						inputStream.close();
						// Returns an object that handles instantiating concrete classes
						CreationHelper helper = workbook.getCreationHelper();
						// Creates the top-level drawing patriarch.
						Drawing drawing = sheet.createDrawingPatriarch();
						// Create an anchor that is attached to the worksheet
						ClientAnchor anchor = helper.createClientAnchor();
						// create an anchor with upper left cell _and_ bottom right cell
						anchor.setCol1(10); // Column B
						anchor.setRow1(rowNum - 1); // Row 3
						anchor.setCol2(11); // Column C
						anchor.setRow2(rowNum); // Row 4
						// Creates a picture
						Picture pict = drawing.createPicture(anchor, pictureIdx);
						
						// IMAGEN ListaHallazgos2
						if (obj2.getRuta() != null && !obj2.getRuta().equals("")) {
							try {
								javaxt.io.Image image2 = new javaxt.io.Image(obj2.getRuta());
								String desc2 = "";
								try {
									
									URL url = new URL(FRQConstantes.getRutaImagen()+obj2.getRuta());
									InputStream fis =  url.openStream();
									Metadata metadata = ImageMetadataReader.readMetadata(fis);
									
									for (Directory directory : metadata.getDirectories()) {
						                for (Tag tag : directory.getTags()) {
						                    if(tag.getTagName().equals("Orientation")){
						                    	desc2 =  tag.getDescription();
						                    }
						                }
						             }
									
									switch (desc2) {
									
									case "Top, left side (Horizontal / normal)":
										break;
									
									case "Top, right side (Mirror horizontal)":
										break;
										
									case "Bottom, right side (Rotate 180)":
										image2.rotate(180);
										break;
										
									case "Bottom, left side (Mirror vertical)":
										image2.rotate(-180);
										break;
										
									case "Left side, top (Mirror horizontal and rotate 270 CW)":
										image2.rotate(-90);
										break;
										
									case "Right side, top (Rotate 90 CW)":
										image2.rotate(90);
										break;
										
									case "Right side, bottom (Mirror horizontal and rotate 90 CW)":
										image2.rotate(-270);
										break;
										
									case "Left side, bottom (Rotate 270 CW)":
										image2.rotate(270);
										break;
										
									default:
										break;

									}
								
								} catch (Exception e) {
									logger.info(e.getMessage());
								}
								byte[] b2 = image2.getByteArray();
								InputStream inputStream2 = new FileInputStream(obj2.getRuta());
								// Get the contents of an InputStream as a byte[].
								byte[] bytes2 = IOUtils.toByteArray(inputStream2);
								// Adds a picture to the workbook
								int pictureIdx2 = workbook.addPicture(b2, Workbook.PICTURE_TYPE_PNG);
								// close the input stream
								inputStream2.close();
								// Returns an object that handles instantiating concrete classes
								CreationHelper helper2 = workbook.getCreationHelper();
								// Creates the top-level drawing patriarch.
								Drawing drawing2 = sheet.createDrawingPatriarch();
								// Create an anchor that is attached to the worksheet
								ClientAnchor anchor2 = helper2.createClientAnchor();
								// create an anchor with upper left cell _and_ bottom right cell
								anchor2.setCol1(25); // Column B
								anchor2.setRow1(rowNum - 1); // Row 3
								anchor2.setCol2(26); // Column C
								anchor2.setRow2(rowNum); // Row 4
								// Creates a picture
								Picture pict2 = drawing2.createPicture(anchor2, pictureIdx2);

					} catch (Exception e) {
						logger.info("No se encontro la IMG: "  + obj.getRuta()+" --- "+obj2.getRuta());
					}
				}
				row.setRowStyle(boddyCellStyle);
				row.setHeightInPoints(150);
			} catch (Exception e) {
				logger.info("No se encontro la IMG 2: "  + obj.getRuta()+" --- "+obj2.getRuta());
			}
		}

		// Resize all columns to fit the content size
		for (int i = 0; i < columns.length; i++) {
			if (i == 10) {
				sheet.setColumnWidth(i, 10000);
			} else {
				sheet.autoSizeColumn(i);
			}
		}
		
		}
		}
		return workbook;
	}
	
	public Workbook generaDashboard(String fase, List<RepoFilExpDTO> lista, String idCeco, SucursalChecklistBI sucursalChecklistBI)
			throws IOException, ParseException {
		int version = 0;
	
		// GENERAMOS WOORKBOOK
		String[] columns = { "CECO", "Sucursal", "Líder de Obra", "Franquicia", "Capítulo", "Item", "Respuesta", "Observaciones", "Evidencia",
				"Prioridad", "Área de atención", "2a Área de atención" };

		// Create a Workbook
		Workbook workbook = new XSSFWorkbook();
		/*
		 * CreationHelper helps us create instances of various things like DataFormat,
		 * Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way
		 */
		CreationHelper createHelper = workbook.getCreationHelper();
		// Create a Sheet
		Sheet sheet = workbook.createSheet("Dashboard");
		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 18);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerCellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Create a Row
		Row headerRow = sheet.createRow(0);

		// Create cells
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		// Cells Format
		Font bodyFont = workbook.createFont();
		bodyFont.setBold(false);
		bodyFont.setFontHeightInPoints((short) 14);

		boddyCellStyle = workbook.createCellStyle();
		boddyCellStyle.setFont(bodyFont);
		boddyCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		boddyCellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Create Other rows and cells with data
		int rowNum = 1;
		if (lista != null && lista.size() > 0) {
			for (RepoFilExpDTO obj : lista) {
				if (rowNum == 1) {
					idCeco = obj.getCeco();
				}
				Cell cell;
				Row row = sheet.createRow(rowNum++);
				
				String nombreSucursalTemp = "";
				
				try {
					
					nombreSucursalTemp = obj.getDetalle();
					
				} catch (Exception e) {
					
					nombreSucursalTemp = "";
					logger.info("AP al obtener el nombre de la sucursal nuevo");
				}
				
				(cell = row.createCell(0)).setCellValue(obj.getCeco());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(1)).setCellValue(nombreSucursalTemp);
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(2)).setCellValue(obj.getLiderObra());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(3)).setCellValue(obj.getNomUsu());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(4)).setCellValue(obj.getChecklist());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(5)).setCellValue(obj.getPregunta());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(6)).setCellValue(obj.getPosible());
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(7)).setCellValue(obj.getObserv());
				cell.setCellStyle(boddyCellStyle);
				switch (obj.getIdcritica()) {
				case 0:
					(cell = row.createCell(9)).setCellValue("Bajo");// Criticidad
					break;
				case 1:
					(cell = row.createCell(9)).setCellValue("Imperdonable");// Criticidad
					break;
				case 2:
					(cell = row.createCell(9)).setCellValue("Alto");// Criticidad
					break;
				case 3:
					(cell = row.createCell(9)).setCellValue("Medio");// Criticidad
					break;
				case 4:
					(cell = row.createCell(9)).setCellValue("Bajo");// Criticidad
					break;
				}
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(10)).setCellValue("Construcción");
				cell.setCellStyle(boddyCellStyle);
				(cell = row.createCell(11)).setCellValue(obj.getArea());
				cell.setCellStyle(boddyCellStyle);
				
				/* IMAGEN */
				if (obj.getRuta() != null && !obj.getRuta().equals("")) {
					try {
						javaxt.io.Image image = new javaxt.io.Image(obj.getRuta());
						String desc = "";
						try {
							
							URL url = new URL(FRQConstantes.getRutaImagen()+obj.getRuta());
							InputStream fis =  url.openStream();
							Metadata metadata = ImageMetadataReader.readMetadata(fis);
							
							for (Directory directory : metadata.getDirectories()) {
				                for (Tag tag : directory.getTags()) {
				                    if(tag.getTagName().equals("Orientation")){
				                    	desc =  tag.getDescription();
				                    }
				                }
				             }
							
							switch (desc) {
							
							case "Top, left side (Horizontal / normal)":
								break;
							
							case "Top, right side (Mirror horizontal)":
								break;
								
							case "Bottom, right side (Rotate 180)":
								image.rotate(180);
								break;
								
							case "Bottom, left side (Mirror vertical)":
								image.rotate(-180);
								break;
								
							case "Left side, top (Mirror horizontal and rotate 270 CW)":
								image.rotate(-90);
								break;
								
							case "Right side, top (Rotate 90 CW)":
								image.rotate(90);
								break;
								
							case "Right side, bottom (Mirror horizontal and rotate 90 CW)":
								image.rotate(-270);
								break;
								
							case "Left side, bottom (Rotate 270 CW)":
								image.rotate(270);
								break;
								
							default:
								break;

							}
						
						} catch (Exception e) {
							logger.info(e.getMessage());
						}
						
						byte[] b = image.getByteArray();
						InputStream inputStream = new FileInputStream(obj.getRuta());
						// Get the contents of an InputStream as a byte[].
						byte[] bytes = IOUtils.toByteArray(inputStream);
						// Adds a picture to the workbook
						int pictureIdx = workbook.addPicture(b, Workbook.PICTURE_TYPE_PNG);
						// close the input stream
						inputStream.close();
						// Returns an object that handles instantiating concrete classes
						CreationHelper helper = workbook.getCreationHelper();
						// Creates the top-level drawing patriarch.
						Drawing drawing = sheet.createDrawingPatriarch();
						// Create an anchor that is attached to the worksheet
						ClientAnchor anchor = helper.createClientAnchor();
						// create an anchor with upper left cell _and_ bottom right cell
						anchor.setCol1(8); // Column B
						anchor.setRow1(rowNum - 1); // Row 3
						anchor.setCol2(9); // Column C
						anchor.setRow2(rowNum); // Row 4
						// Creates a picture
						Picture pict = drawing.createPicture(anchor, pictureIdx);

					} catch (Exception e) {
						logger.info("No se encontro la IMG: " + obj.getRuta());
					}
				}
				row.setRowStyle(boddyCellStyle);
				row.setHeightInPoints(150);
			}
		}
		// Resize all columns to fit the content size
		for (int i = 0; i < columns.length; i++) {

			if (i == 10) {
				sheet.setColumnWidth(i, 10000);
			} else {
				sheet.autoSizeColumn(i);
			}
		}
		return workbook;
	}

	public String getNombreRecorrido(String recorrido) {

		try {

			int numRecorrido = Integer.parseInt(recorrido);

			if (numRecorrido > 3) {

				return "Recorrido especial " + numRecorrido;

			} else {

				switch (numRecorrido) {

				case 1:
					return "Caminata";

				case 2:
					return "Soft Opening";

				case 3:
					return "Grand Opening";

				default:
					return "Pre Caminata";
				}

			}

		} catch (Exception e) {

			logger.info("AP al obtener el numero de recorrido Expansion");
			return "Caminata " + recorrido;

		}

	}
}
