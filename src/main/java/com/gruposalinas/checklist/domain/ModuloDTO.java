package com.gruposalinas.checklist.domain;

public class ModuloDTO {
	
	private int idModulo;
	private String nombre;
	private int idModuloPadre;
	private String nombrePadre;
	private String numVersion;
	private String tipoCambio;
	private int idChecklist;
	
	public String getNombrePadre() {
		return nombrePadre;
	}
	public void setNombrePadre(String nombrePadre) {
		this.nombrePadre = nombrePadre;
	}
	public int getIdModulo() {
		return idModulo;
	}
	public void setIdModulo(int idModulo) {
		this.idModulo = idModulo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getIdModuloPadre() {
		return idModuloPadre;
	}
	public void setIdModuloPadre(int idModuloPadre) {
		this.idModuloPadre = idModuloPadre;
	}
	public String getNumVersion() {
		return numVersion;
	}
	public void setNumVersion(String numVersion) {
		this.numVersion = numVersion;
	}
	public String getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	@Override
	public String toString() {
		return "ModuloDTO [idModulo=" + idModulo + ", nombre=" + nombre + ", idModuloPadre=" + idModuloPadre
				+ ", nombrePadre=" + nombrePadre + ", numVersion=" + numVersion + ", tipoCambio=" + tipoCambio
				+ ", idChecklist=" + idChecklist + "]";
	}
	

}
