package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.AdicionalesDAO;
import com.gruposalinas.checklist.domain.AdicionalesDTO;

public class AdicionalesBI {

	private static Logger logger = LogManager.getLogger(AdicionalesBI.class);

private List<AdicionalesDTO> listafila;
	
	@Autowired
	AdicionalesDAO adicionalesDAO;
		
	
	public int inserta(AdicionalesDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = adicionalesDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean elimina(String idEmpFijo){
		boolean respuesta = false;
		
		try {
			respuesta = adicionalesDAO.elimina(idEmpFijo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<AdicionalesDTO> obtieneDatos( String ceco) {
		
		try {
			listafila =  adicionalesDAO.obtieneDatos(ceco);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
public List<AdicionalesDTO> obtieneDatosBita( String bita,String idPreg) {
		
		try {
			listafila =  adicionalesDAO.obtieneDatosBita(bita, idPreg);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	public List<AdicionalesDTO> obtieneInfo() {

		try {
			listafila = adicionalesDAO.obtieneInfo();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de la tabla Filas");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	
	public boolean actualiza(AdicionalesDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = adicionalesDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public int insertaHist(AdicionalesDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = adicionalesDAO.insertaHist(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaHist(String bita){
		boolean respuesta = false;
		
		try {
			respuesta = adicionalesDAO.eliminaHist(bita);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	
public List<AdicionalesDTO> obtieneDatosHist( String bita) {
		
		try {
			listafila =  adicionalesDAO.obtieneDatosHist(bita);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}

	public List<AdicionalesDTO> obtieneInfoHist() {

		try {
			listafila = adicionalesDAO.obtieneInfoHist();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de la tabla Filas");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}
}