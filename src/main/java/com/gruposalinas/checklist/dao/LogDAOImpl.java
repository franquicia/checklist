package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.LogDTO;
import com.gruposalinas.checklist.mappers.LogRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class LogDAOImpl extends DefaultDAO implements LogDAO {

    private static Logger logger = LogManager.getLogger(LogDAOImpl.class);

    DefaultJdbcCall jdbcObtieneErrores;

    public void init() {

        jdbcObtieneErrores = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMERRORES")
                .withProcedureName("SP_SEL_ERRORES")
                .returningResultSet("RCL_ERRORES", new LogRowMapper());
    }

    @SuppressWarnings("unchecked")
    public List<LogDTO> obtieneErrores(String fecha) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<LogDTO> listaErrores = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FECHA", fecha);
        out = jdbcObtieneErrores.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAAINFO}");

        //logger.info("Funcion ejecutada: {checklist.PAADMERRORES.SP_SEL_ERRORES}");
        listaErrores = (List<LogDTO>) out.get("RCL_ERRORES");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Info en la fecha: " + fecha);
        } else {
            return listaErrores;
        }

        return null;

    }
}
