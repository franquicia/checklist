package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.PosiblesDTO;

public interface PosiblesTDAO {

	public List<PosiblesDTO> buscaPosibleTemp() throws Exception;
	
	public boolean insertaPosibleTemp(PosiblesDTO bean) throws Exception;
	
	public boolean actualizaPosibleTemp(PosiblesDTO bean) throws Exception;
	
	public boolean eliminaPosibleTemp(int idPosible) throws Exception;
}
