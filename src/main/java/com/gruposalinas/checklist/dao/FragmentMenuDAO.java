package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.FragmentMenuDTO;

public interface FragmentMenuDAO {

	public int insertaFragment(FragmentMenuDTO bean) throws Exception;

	public boolean eliminaFragment(String idFragment) throws Exception;

	public List<FragmentMenuDTO> obtieneDatosFragment(String idFragment) throws Exception;

	public boolean actualizaFragment(FragmentMenuDTO bean) throws Exception;

	//MENU
	public int insertaMenu(FragmentMenuDTO bean) throws Exception;

	public boolean eliminaMenu(String menu) throws Exception;

	public List<FragmentMenuDTO> obtieneDatosMenu(String menu) throws Exception;

	public boolean actualizaMenu(FragmentMenuDTO bean) throws Exception;
	//CONFIG PROY
	public int insertaConf(FragmentMenuDTO bean) throws Exception;

	public boolean eliminaConf(String menu) throws Exception;

	public List<FragmentMenuDTO> obtieneDatosConf(String menu) throws Exception;

	public boolean actualizaConf(FragmentMenuDTO bean) throws Exception;
}