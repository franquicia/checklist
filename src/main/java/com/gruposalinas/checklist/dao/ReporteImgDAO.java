package com.gruposalinas.checklist.dao;

import java.util.List;
import com.gruposalinas.checklist.domain.ReporteImgDTO;




public interface ReporteImgDAO {
	
	public List<ReporteImgDTO> obtieneDetalleCeco(String idCeco, String fechaInicio, String fechaFin, String idChecklist) throws Exception;
	public List<ReporteImgDTO> obtieneProtocolosZonas(int idGrupo) throws Exception;
	public List<ReporteImgDTO> obtieneSucursales() throws Exception;
	public List<ReporteImgDTO> obtieneAcervo(int idBitacora, int idChecklist) throws Exception;
	

}
