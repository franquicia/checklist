package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.AsignaTransfDTO;

public class FasesTransformacionRowMapper implements RowMapper<AsignaTransfDTO> {

	public AsignaTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AsignaTransfDTO AsignaTransfDTO = new AsignaTransfDTO();
		
		AsignaTransfDTO.setIdTabla(rs.getInt("FIID_PROYFASE"));
		AsignaTransfDTO.setProyecto(rs.getInt("FIID_PROYECTO"));
		AsignaTransfDTO.setFase(rs.getInt("FIID_FASE"));
		AsignaTransfDTO.setAux2(rs.getString("FCAUX"));
	    AsignaTransfDTO.setVersion(rs.getInt("FIVERSION"));
	    AsignaTransfDTO.setObsFase(rs.getString("NOMBREFASE"));
	    AsignaTransfDTO.setPeriodo(rs.getString("FCPERIODO"));
	    AsignaTransfDTO.setBanderaUltFase(rs.getString("BANDERAULTFAS"));

		return AsignaTransfDTO;
	}

}


