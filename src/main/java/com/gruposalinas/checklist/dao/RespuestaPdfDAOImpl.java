package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.DatosChecklistPdfDTO;
import com.gruposalinas.checklist.domain.RespuestaPdfDTO;
import com.gruposalinas.checklist.mappers.DatosChecklistPdfRowMapper;
import com.gruposalinas.checklist.mappers.RespuestaPDFRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class RespuestaPdfDAOImpl extends DefaultDAO implements RespuestaPdfDAO {

    private Logger logger = LogManager.getLogger(RespuestaPdfDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneRespuestas;
    private DefaultJdbcCall jdbcEnviaCompromiso;
    private DefaultJdbcCall jdbcObtieneCecoPorBit;

    public void init() {

        jdbcObtieneRespuestas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPUESTASPDF")
                .withProcedureName("SP_GETRESPUESTA")
                .returningResultSet("RCL_CONSULTA", new RespuestaPDFRowMapper())
                .returningResultSet("RCL_DATOS_CECO", new DatosChecklistPdfRowMapper());

        jdbcEnviaCompromiso = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAVERIFICAENVIO")
                .withProcedureName("SP_VERIFICACK");

        jdbcObtieneCecoPorBit = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPUESTASPDF")
                .withProcedureName("SP_GETCECOPBIT");
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> obtieneRespuestas(int usuario, int checklist, String ceco) throws Exception {

        Map<String, Object> out = null;
        Map<String, Object> resultado = new HashMap<String, Object>();
        int ejecucion = 0;

        List<RespuestaPdfDTO> respuestas = null;
        List<DatosChecklistPdfDTO> datos = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", usuario).addValue("PA_CHECKLIST", checklist).addValue("PA_CECO", ceco);

        out = jdbcObtieneRespuestas.execute(in);

        logger.info("Funcion ejecutada: {checklist.PARESPUESTASPDF.SP_GETRESPUESTA}");

        respuestas = (List<RespuestaPdfDTO>) out.get("RCL_CONSULTA");
        datos = (List<DatosChecklistPdfDTO>) out.get("RCL_DATOS_CECO");

        BigDecimal ejecReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener las respuestas");
            resultado.put("respuestas", new ArrayList<RespuestaPdfDTO>());
            resultado.put("datos", new ArrayList<DatosChecklistPdfDTO>());

            return resultado;
        }

        resultado.put("respuestas", respuestas);
        resultado.put("datos", datos);

        return resultado;
    }

    @Override
    public int enviaCompromisos(int idUsuario, int bitacora) throws Exception {
        // TODO Auto-generated method stub
        Map<String, Object> resultado = new HashMap<String, Object>();
        Map<String, Object> out = null;
        int ret = 0;

        logger.info("RespuestaPdfDAOIMPLEMENT ENVIA COMPROMISOS --- USUARIO -- " + idUsuario + "--- BITACORA ---" + bitacora);
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", idUsuario).addValue("PA_IDBITACORA", bitacora);
        logger.info("Funcion ejecutada: {checklist.PAVERIFICAENVIO.SP_VERIFICACK}");
        out = jdbcEnviaCompromiso.execute(in);

        BigDecimal ejecReturn = (BigDecimal) out.get("PA_EJECUCION");
        BigDecimal ejecResp = (BigDecimal) out.get("PA_RESPUESTA");
        String checkUsuas = (String) out.get("PA_ASIGNACION");

        logger.info("PA_EJECUCION RESPUESTAPDFDAOIMPL ---> " + ejecReturn);
        logger.info("PA_RESPUESTA RESPUESTAPDFDAOIMPL ---> " + ejecResp);
        logger.info("PA_ASIGNACION RESPUESTAPDFDAOIMPL ---> " + checkUsuas);

        logger.info("CHECK USUAS QUE DEBO TENER CERRADOS PARA ENVIAR EL CORREO" + checkUsuas);

        if (ejecReturn.intValue() == 0) {
            logger.info("Algo paso al obtener las respuestas");
            resultado.put("respuestas", new ArrayList<RespuestaPdfDTO>());
            resultado.put("datos", new ArrayList<DatosChecklistPdfDTO>());

            ret = ejecResp.intValue();
        }

        if (ejecResp.intValue() == 2) {
            logger.info("NO TIENE TODAS LAS BITACORAS CERRADAS PARA ENVIAR CORREO");
            resultado.put("respuestas", new ArrayList<RespuestaPdfDTO>());
            resultado.put("datos", new ArrayList<DatosChecklistPdfDTO>());

            ret = ejecResp.intValue();
        } else if (ejecResp.intValue() == 1) {
            logger.info("TODO BIEN AL VERIFICAR LAS BITACORAS");
            ret = ejecResp.intValue();
        }

        return ret;

    }

    @Override
    public Map<String, String> obtieneCecoPorBitacora(int idUsuario, int idBitacora) {
        // TODO Auto-generated method stub
        Map<String, String> resultado = new HashMap<String, String>();
        Map<String, Object> out = null;

        logger.info("RespuestaPdfDAOIMPLEMENT ENVIA COMPROMISOS --- USUARIO -- " + idUsuario + "--- BITACORA ---" + idBitacora);
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", idUsuario).addValue("PA_IDBIT", idBitacora);

        logger.info("Funcion ejecutada: {checklist.PARESPUESTASPDF.SP_GETCECOPBIT}");
        out = jdbcObtieneCecoPorBit.execute(in);

        BigDecimal ejecReturn = (BigDecimal) out.get("PA_EJECUCION");
        BigDecimal ejecResp = (BigDecimal) out.get("PA_CECO");
        BigDecimal ejecNumNegocio = (BigDecimal) out.get("PA_NEGOCIO");

        logger.info("ESTO ES EJECUCION DENTRO DEL DAO IMPLEMENT ----> " + ejecReturn);
        logger.info("ESTO ES CECO DENTRO DEL DAO IMPLEMENT ----> " + ejecResp);
        logger.info("ESTO ES NEGOCIO DENTRO DEL DAO IMPLEMENT ----> " + ejecNumNegocio);

        if (ejecReturn.intValue() == 1) {
            logger.info("Se llena el ceco y el negocio");
            resultado.put("idCeco", ejecResp + "");
            resultado.put("numNegocio", ejecNumNegocio + "");

            return resultado;
        }

        return resultado;

    }

}
