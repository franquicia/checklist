package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ConsultaDashBoardDTO;

public class ConsultaDashBoardRowMapper implements RowMapper<ConsultaDashBoardDTO> {

	public ConsultaDashBoardDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ConsultaDashBoardDTO  consultaDashBoardDTO = new ConsultaDashBoardDTO();
		
		consultaDashBoardDTO.setIdUsuario(rs.getString("FCID_USUARIO"));
		consultaDashBoardDTO.setNomCeco(rs.getString("FCNOMBRE"));	
		consultaDashBoardDTO.setFecha(rs.getString("FDFECHA"));
		consultaDashBoardDTO.setSucVisitadas(rs.getString("FISUC_VISITADAS"));
		consultaDashBoardDTO.setSucxVisitar(rs.getString("FISUCXVISITAR"));
		
		

		return consultaDashBoardDTO;
	}

}
