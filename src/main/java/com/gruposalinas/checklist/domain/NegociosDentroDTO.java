package com.gruposalinas.checklist.domain;

public class NegociosDentroDTO {
	
	private String ceco;
	private String nombreCeco;
	private int canal;
	private int negocio;
	
	public String getCeco() {
		return ceco;
	}
	public void setCeco(String ceco) {
		this.ceco = ceco;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	public int getCanal() {
		return canal;
	}
	public void setCanal(int canal) {
		this.canal = canal;
	}
	public int getNegocio() {
		return negocio;
	}
	public void setNegocio(int negocio) {
		this.negocio = negocio;
	}
	
	
	
	

}
