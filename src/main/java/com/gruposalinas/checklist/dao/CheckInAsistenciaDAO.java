package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.CheckInAsistenciaDTO;
import com.gruposalinas.checklist.domain.ProgramacionMttoDTO;


public interface CheckInAsistenciaDAO {

	  public int insertaAsistencia(CheckInAsistenciaDTO bean)throws Exception;
		
		public boolean eliminaAsistencia(String idAsistencia)throws Exception;
		
		public List<CheckInAsistenciaDTO> obtieneDatosAsistencia(String fechaEntrada,String fechaSalida,String idUsuario) throws Exception; 
		
		public boolean actualizaAsistencia(CheckInAsistenciaDTO bean)throws Exception;
		
		public List<ProgramacionMttoDTO>obtieneProgramaciones(String idUsuario) throws Exception; 
		
		public boolean actualizaProgramacion(ProgramacionMttoDTO bean)throws Exception;
	}