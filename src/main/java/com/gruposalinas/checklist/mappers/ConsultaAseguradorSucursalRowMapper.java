package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ConsultaAseguradorDTO;

public class ConsultaAseguradorSucursalRowMapper implements RowMapper<ConsultaAseguradorDTO> {

	public ConsultaAseguradorDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ConsultaAseguradorDTO  consultaSucursalDTO = new ConsultaAseguradorDTO();
		
		consultaSucursalDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
		consultaSucursalDTO.setIdCeco(rs.getInt("FCID_CECO"));
		consultaSucursalDTO.setNomCeco(rs.getString("FCNOMBRE"));

		return consultaSucursalDTO;
	}

}
