package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.DistribCecoDTO;
import com.gruposalinas.checklist.mappers.DistribCecoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class DistribCecoDAOImpl extends DefaultDAO implements DistribCecoDAO {

    private static Logger logger = LogManager.getLogger(CargaCheckListDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaDistrib;
    private DefaultJdbcCall jdbcEliminaCecoDistrib;
    private DefaultJdbcCall jdbcObtieneCecoDistrib;

    public void init() {

        jdbcInsertaDistrib = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADISTRIBCECO")
                .withProcedureName("SPDISTRIBCECO");

        jdbcObtieneCecoDistrib = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADISTRIBCECO")
                .withProcedureName("SPGETCECODIST")
                .returningResultSet("PA_CONSULTA", new DistribCecoRowMapper());

        jdbcEliminaCecoDistrib = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADISTRIBCECO")
                .withProcedureName("SPDELCECODIST");

    }

    @Override
    public int insertaDistrib() throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        out = jdbcInsertaDistrib.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PADISTRIBCECO.SPDISTRIBCECO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_RESEJECUCION");
        ejecucion = errorReturn.intValue();

        if (ejecucion != 1) {
            logger.info("Algo ocurrió al realizar la distribucion");

        }
        return ejecucion;

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DistribCecoDTO> obtieneCeco(DistribCecoDTO bean) throws Exception {

        Map<String, Object> out = null;
        List<DistribCecoDTO> listaCeco = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_CECO", bean.getCeco())
                .addValue("PA_FCID_TERR", bean.getTerritorio())
                .addValue("PA_FCID_ZONA", bean.getZona())
                .addValue("PA_FCID_REGI", bean.getRegion())
                .addValue("PA_FCID_GERE", bean.getGerente());

        out = jdbcObtieneCecoDistrib.execute(in);

        logger.info("Funcion ejecutada:{checklist.PADISTRIBCECO.SPGETCECODIST}");

        listaCeco = (List<DistribCecoDTO>) out.get("PA_CONSULTA");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_ERROR");
        error = returnEjecucion.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al consultar el Ceco");
            return listaCeco;
        }
        return listaCeco;
    }

    @Override
    public boolean EliminaDistrib(String ceco) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_CECO", ceco);

        out = jdbcEliminaCecoDistrib.execute(in);

        logger.info("Funcion ejecutada:{checklist.PADISTRIBCECO.SPDELCECODIST.ceco}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_RESEJECUCION");
        error = returnEjecucion.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al eliminar el ceco en la Distribucion");
            return false;
        }

        return true;
    }

}
