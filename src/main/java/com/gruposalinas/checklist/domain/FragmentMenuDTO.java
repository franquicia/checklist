package com.gruposalinas.checklist.domain;

public class FragmentMenuDTO {
	


		private int idFragment;
		private String nombreFragment;
		private int numFragment;
		private String parametro;
		private String periodo;
		private int idConfigMenu;
		private String tituloMenu;
		private int ordenMenu;
		private String aux;
		private int aux2;
		private int idPerfil;
		
		private int idConfigProyecto;
		private int idAgrupaFirma;
		private int ordenFragment;

	
        
		public int getIdFragment() {
			return idFragment;
		}
		public void setIdFragment(int idFragment) {
			this.idFragment = idFragment;
		}
		public String getNombreFragment() {
			return nombreFragment;
		}
		public void setNombreFragment(String nombreFragment) {
			this.nombreFragment = nombreFragment;
		}
		public int getNumFragment() {
			return numFragment;
		}
		public void setNumFragment(int numFragment) {
			this.numFragment = numFragment;
		}
		public String getParametro() {
			return parametro;
		}
		public void setParametro(String parametro) {
			this.parametro = parametro;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		public int getIdConfigMenu() {
			return idConfigMenu;
		}
		public void setIdConfigMenu(int idConfigMenu) {
			this.idConfigMenu = idConfigMenu;
		}
		public String getTituloMenu() {
			return tituloMenu;
		}
		public void setTituloMenu(String tituloMenu) {
			this.tituloMenu = tituloMenu;
		}
		public int getOrdenMenu() {
			return ordenMenu;
		}
		public void setOrdenMenu(int ordenMenu) {
			this.ordenMenu = ordenMenu;
		}
		public String getAux() {
			return aux;
		}
		public void setAux(String aux) {
			this.aux = aux;
		}
		public int getAux2() {
			return aux2;
		}
		public void setAux2(int aux2) {
			this.aux2 = aux2;
		}
		
		public int getIdPerfil() {
			return idPerfil;
		}
		public void setIdPerfil(int idPerfil) {
			this.idPerfil = idPerfil;
		}
		
		public int getIdConfigProyecto() {
			return idConfigProyecto;
		}
		public void setIdConfigProyecto(int idConfigProyecto) {
			this.idConfigProyecto = idConfigProyecto;
		}
		public int getIdAgrupaFirma() {
			return idAgrupaFirma;
		}
		public void setIdAgrupaFirma(int idAgrupaFirma) {
			this.idAgrupaFirma = idAgrupaFirma;
		}
		
		public int getOrdenFragment() {
			return ordenFragment;
		}
		public void setOrdenFragment(int ordenFragment) {
			this.ordenFragment = ordenFragment;
		}
		@Override
		public String toString() {
			return "FragmentMenuDTO [idFragment=" + idFragment + ", nombreFragment=" + nombreFragment + ", numFragment="
					+ numFragment + ", parametro=" + parametro + ", periodo=" + periodo + ", idConfigMenu="
					+ idConfigMenu + ", tituloMenu=" + tituloMenu + ", ordenMenu=" + ordenMenu + ", aux=" + aux
					+ ", aux2=" + aux2 + ", idPerfil=" + idPerfil + ", idConfigProyecto=" + idConfigProyecto
					+ ", idAgrupaFirma=" + idAgrupaFirma + ", ordenFragment=" + ordenFragment + "]";
		}
		
		
		
	}