<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="height: 98%; min-width: 1200px">
<head>

<link href="../css/jQuery/jQuery.css"
	rel="stylesheet" type="text/css" />
	
<link href="../css/checklistDesign.css"
	rel="stylesheet" type="text/css" />
	
<link rel="stylesheet" href="../css/visualizador/w3.css">

<link rel="stylesheet" href="../css/exportaReporte.css">


	
<script type="text/javascript">
	var urlServer = '${urlServer}';
</script>

<script	src="../css/jQuery/jQuery.js"></script>
<script	src="../js/script-reporte.js"></script>


<!-- DESABILITA EL BOTON DE ENTER EN LA VISTA -->
<script type="text/javascript">
$(document).ready(function() {
	  $(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
	});
</script>


<!-- PONE EL TIEMPO DEL MENSAJE -->
<script type="text/javascript">
$(document).ready(function() {
    setTimeout(function() {
        $(".content-Mensaje").fadeOut(1500);
    },3000);
});
</script>

<!-- EJECUTA EL CALENDARIO -->
<script type="text/javascript">
$(function() {
    $( "#fechaInicio" ).datepicker();
});
$(function() {
    $( "#fechaFin" ).datepicker();
});
</script>

</head>
<body  style="height: 95%;">
	
	
	<h2 class="w3-animate-zoom  datos-vacios content-Mensaje">${fallo}</h2>
	
				
	<div style="background: white; margin: 30px 24px 50px 24px; min-width: 700px; min-height:674px; height: 94%; box-shadow: 5px 5px 17px #000000;" >

			<div style="height: 35%; min-height: 146px;">
				<ul class="margenUl">
				
					<div style="height:30%;">
					</div>
					
					<div style="height:50%; display: inline-block; width: 50%;">
					<li class="li-stilo"><span class="circulo" style="margin: 37px 20px 0 35%;">1</span> Categoría	
						<div class="cajaVis" id="categoriaVis">
							<input type="hidden" name="categoria" value="Selecciona una Opcion" id="valorCategoria">
							<span class="seleccionadoVis" id="Selecciona una Opcion">Elije una opcion</span>
							<ul class="listaselectVis">
								<c:forEach items="${listaTipoCheck}" var="item">
									<li value="${item.idTipoCheck}"><a href="">${item.descTipo}</a></li>
								</c:forEach>						
							</ul >
							<span class="trianguloinfVis"></span>
						</div>	
					</li>
					</div>
					<div style="height:50%; display: inline-block; width: 49%;">
					<li class="li-stilo margenLista"><span class="segundo-nivel">Tipo</span> 
						<div class="cajaVis" id="tipoVis">
							<input type="hidden" name="tipo" value="Selecciona una Opcion" id="valorTipo">
							<span class="seleccionadoVis" id="Selecciona una Opcion">Elije una opcion</span>
								<ul class="listaselectVis" id="tipoVis2"></ul>
							<span class="trianguloinfVis"></span>
						</div>
					</li>
					</div>
				</ul>
							
			
			</div>
	
			
	
			<div style="height: 30%; min-height: 210px;">
				
				<div style="background:red; width: 50%; display: inline-block;">
					<ul class="margenUl">
						<li class="li-stilo"><span class="primer-nivel">Fecha Inicio</span> 
							<input class="textBoxInput" type="text" id="fechaInicio" name="fechaInicio" placeholder="Selecciona la fecha" size="22" />
						</li>							
					</ul>
				</div>			
				
				<div style=" background:blue; width: 50%; float:right;">
					<ul class="margenUl">
						<li class="li-stilo"><span class="segundo-nivel">Fecha Fin</span> 
							<input class="textBoxInput" type="text" id="fechaFin" name="fechaFin" placeholder="Selecciona la fecha" size="22" />
						</li>							
					</ul>					
				</div>
						
				
			</div>

			<div style="height:170px;">
				
				<a href="#" id="btn-excel" onclick="return generaReporte();"><img src="/checklist/images/v2/ico-excel.png" >General</a>
				<a href="#" id="bnt-limpiar" onclick="return limpiaReporte();">Limpiar</a> <br>
				
			</div>
	</div>


</body>


</html>