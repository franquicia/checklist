package com.gruposalinas.checklist.domain;

import java.util.List;

public class PreguntaDTO {
	
	private int idPregunta;
	private int idModulo;
	private int idTipo;
	private int estatus;
	private String pregunta;
	private List<ArbolDecisionDTO> desiciones;
	private int commit;
	private String numVersion;
	private String tipoCambio;
	private int pregPadre; 
	private String detalle;
	private int critica;
	private String sla;
	private String area;
	private String numRespuestas;
	private int codigo;
        private String responsable;
        private String negocio;

	//Agregado Administrador Checklist
	private int idConsecutivo;
	private int idPreguntaPadre;
	

	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public int getIdConsecutivo() {
		return idConsecutivo;
	}
	public void setIdConsecutivo(int idConsecutivo) {
		this.idConsecutivo = idConsecutivo;
	}
	public int getIdPreguntaPadre() {
		return idPreguntaPadre;
	}
	public void setIdPreguntaPadre(int idPreguntaPadre) {
		this.idPreguntaPadre = idPreguntaPadre;
	}
	
	public String getPregunta() {
		return pregunta;
	}
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public int getIdModulo() {
		return idModulo;
	}
	public void setIdModulo(int idModulo) {
		this.idModulo = idModulo;
	}
	public int getIdTipo() {
		return idTipo;
	}
	public void setIdTipo(int idTipo) {
		this.idTipo = idTipo;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public List<ArbolDecisionDTO> getDesiciones() {
		return desiciones;
	}
	public void setDesiciones(List<ArbolDecisionDTO> desiciones) {
		this.desiciones = desiciones;
	}
	public String getNumRespuestas() {
		return numRespuestas;
	}
	public void setNumRespuestas(String numRespuestas) {
		this.numRespuestas = numRespuestas;
	}
	public int getCommit() {
		return commit;
	}
	public void setCommit(int commit) {
		this.commit = commit;
	}
	public String getNumVersion() {
		return numVersion;
	}
	public void setNumVersion(String numVersion) {
		this.numVersion = numVersion;
	}
	public String getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public int getPregPadre() {
		return pregPadre;
	}
	public void setPregPadre(int pregPadre) {
		this.pregPadre = pregPadre;
	}
	
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public int getCritica() {
		return critica;
	}
	public void setCritica(int critica) {
		this.critica = critica;
	}
	public String getSla() {
		return sla;
	}
	public void setSla(String sla) {
		this.sla = sla;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    @Override
    public String toString() {
        return "PreguntaDTO{" + "idPregunta=" + idPregunta + ", idModulo=" + idModulo + ", idTipo=" + idTipo + ", estatus=" + estatus + ", pregunta=" + pregunta + ", desiciones=" + desiciones + ", commit=" + commit + ", numVersion=" + numVersion + ", tipoCambio=" + tipoCambio + ", pregPadre=" + pregPadre + ", detalle=" + detalle + ", critica=" + critica + ", sla=" + sla + ", area=" + area + ", numRespuestas=" + numRespuestas + ", codigo=" + codigo + ", responsable=" + responsable + ", negocio=" + negocio + ", idConsecutivo=" + idConsecutivo + ", idPreguntaPadre=" + idPreguntaPadre + '}';
    }	
	
}
