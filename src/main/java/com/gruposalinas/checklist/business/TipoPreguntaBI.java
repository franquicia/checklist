package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.TipoPreguntaDAO;
import com.gruposalinas.checklist.domain.TipoPreguntaDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class TipoPreguntaBI {
	
	private static Logger logger = LogManager.getLogger(TipoPreguntaBI.class);

	@Autowired
	TipoPreguntaDAO tipoPreguntaDAO;
	
	List<TipoPreguntaDTO> listatipoPregunta = null;
	List<TipoPreguntaDTO> listatiposPregunta = null;
	
	public List<TipoPreguntaDTO> obtieneTipoPregunta(){
		try{
			listatiposPregunta = tipoPreguntaDAO.obtieneTipoPregunta();
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Tipo Pregunta");
					
		}
		
		return listatiposPregunta;
	}

	public List<TipoPreguntaDTO> obtieneTipoPregunta(int idTipoPreg){
		try{
			listatipoPregunta = tipoPreguntaDAO.obtieneTipoPregunta(idTipoPreg);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Tipo Pregunta");
			
		}
				
		return listatipoPregunta;
	}
	
	public int insertaTipoPregunta(TipoPreguntaDTO tipoPregunta){
		int idTipoPreg = 0;
		
		try{
			idTipoPreg = tipoPreguntaDAO.insertaTipoPregunta(tipoPregunta);
		}
		catch (Exception e) {
			logger.info("No fue posible insertar el Tipo de Pregunta");
			
		}
		
		return idTipoPreg;	
	}
	
	public boolean actualizaTipoPregunta(TipoPreguntaDTO tipoPregunta){
		boolean respuesta = false;
		try{
			respuesta = tipoPreguntaDAO.actualizaTipoPregunta(tipoPregunta);
		}
		catch (Exception e) {
			logger.info("No fue posible actualizar el Tipo de Pregunta");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaTipoPregunta(int idTipoPreg){
		boolean respuesta = false;
		try{
			respuesta = tipoPreguntaDAO.eliminaTipoPregunta(idTipoPreg);
		}
		catch(Exception e){
			logger.info("No fue posible borrar el Tipo de Pregunta");
			
		}
		
		return respuesta;
	}
}
