package com.gruposalinas.checklist.domain;

public class ConsultaVisitasDTO {
	private String idCeco;
	private String protocolo;
	private String fimes;
	private String ficonteo;
	private String fdfecha1;
	private String fdfecha2;
	private String bitacora;
	private String promedioCalif;
	private String calificacion;
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getProtocolo() {
		return protocolo;
	}
	public void setProtocolo(String protocolo) {
		this.protocolo = protocolo;
	}
	public String getFimes() {
		return fimes;
	}
	public void setFimes(String fimes) {
		this.fimes = fimes;
	}
	public String getFiconteo() {
		return ficonteo;
	}
	public void setFiconteo(String ficonteo) {
		this.ficonteo = ficonteo;
	}
	public String getFdfecha1() {
		return fdfecha1;
	}
	public void setFdfecha1(String fdfecha1) {
		this.fdfecha1 = fdfecha1;
	}
	public String getFdfecha2() {
		return fdfecha2;
	}
	public void setFdfecha2(String fdfecha2) {
		this.fdfecha2 = fdfecha2;
	}
	
	public String getBitacora() {
		return bitacora;
	}
	public void setBitacora(String bitacora) {
		this.bitacora = bitacora;
	}
	public String getPromedioCalif() {
		return promedioCalif;
	}
	public void setPromedioCalif(String promedioCalif) {
		this.promedioCalif = promedioCalif;
	}
	public String getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}
	@Override
	public String toString() {
		return "ConsultaVisitasDTO [idCeco=" + idCeco + ", protocolo=" + protocolo + ", fimes=" + fimes + ", ficonteo="
				+ ficonteo + ", fdfecha1=" + fdfecha1 + ", fdfecha2=" + fdfecha2 + ", bitacora=" + bitacora
				+ ", promedioCalif=" + promedioCalif + ", calificacion=" + calificacion + "]";
	}
	
	
	
	

}
