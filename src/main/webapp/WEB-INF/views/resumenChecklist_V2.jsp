<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="height: 98%; min-width: 1200px">
<head>
<link href="../css/resumenCheck.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/script-detalleCambio.js"></script>
<title>ADMINISTRADOR DE CHECK</title>
<script type="text/javascript">
	var urlServer = '${urlServer}';
	var m =  ${show};
</script>
		<!--stilos-->
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge"/>
        <title>Agenda Sucursales BAZ Checklist</title>
        <!--stilos-->
	   <link rel="stylesheet" type="text/css" href="../css/v2/estilos.css">
	   <link rel="stylesheet" type="text/css" href="../css/v2/dropkick.css">	
<head>
	 
	    <!-- Owl Carousel Assets -->
        <link href="owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="owl-carousel/owl.theme.css" rel="stylesheet">		 	 
    </head>

</head>
<body style="height: 95%" onload="modal()">

<div class="wraper" style="width:100%; overflow:hidden;">
	<div class="marco" style="overflow-y: scroll">
	<!-- TABLA DE CHECKLISTS AUTORIZADOS-->
	<div id="tabs-resumen" class="tab-con">
			<h3 style="color:#369C82; text-align:center;">CHECKLIST AUTORIZADOS</h3>
		                <div id="contenedro-tap-uno">

			                <div class="titulo">
				        	    <table class="tblGeneral" id="tblTitResumen">
					        	    <tbody>
					        	        <tr>
						        	        <td>Checklist guardados</td>
						        	        <td>Fecha de creación</td>
						        	        <td>Fecha de modificación</td>
						        	        <td>Usuarios destinados</td>
						        	        <td>&nbsp;</td>
					      	            </tr>
					      	        </tbody>
				      	        </table>
				        	</div>

							<c:if test="${listaResumen!=null}">
								<table class="tblGeneral" id="tblResumen">
									<tbody>
										<c:set var="i" scope="session" value="0" />
										<c:forEach items="${listaResumen}" var="item">
										<c:set var="i" scope="session" value="${i+1}" />
											<tr class="border">
											    <td>${i}.- ${item.nombreCheck}
													<c:if test="${item.idEstado==26 || item.idEstado==28}">										
													<p class="espacio"></p>
													<div class="tooltip">
														<span class="tooltiptext">En Edici&oacute;n</span>
														<img class="tamImagenResumenInterro" id="imaAlerta${item.idChecklist}" src="../images/iconosAdmCheck/interroRojo.png" >
													</div>
													</c:if>	
												</td>
											    <td class="tb-light">${item.fecha_inicio}</td>
											    <td class="tb-light">${item.fechaModificacion}</td>
											    <td class="tb-light">${item.usuarios}</td>
												<td>
													<div class="tooltip">
														<span class="tooltiptext">Editar</span>
															<form class="tamImagenResumen" name="formulario" method="POST" action="../central/checklistDesignF1.htm" id="formResumenEdita${item.idChecklist}">
																<img class="tamImagenResumen"  onclick="resumenEdita( ${item.idChecklist}, ${item.isPendiente})" id="imaEdita${item.idChecklist}" src="../images/v2/iconEditar.svg"  >
																<input type="hidden" name="idChecklist" value="${item.idChecklist}">
															</form>
													</div>
													<!-- 
													<p class="espacio"></p>
													<div class="tooltip">
														<span class="tooltiptext">Eliminar</span>
														<img class="tamImagenResumen" onclick="resumenElimina(${item.idChecklist})" id="imaElimina${item.idChecklist}" src="../images/iconosAdmCheck/iconoElimina.png" >
													</div>
													-->
													<!-- 
													<p class="espacio"></p>
													<div class="tooltip">
														<span class="tooltiptext">Clona</span>
														<img class="tamImagenResumen" onclick="resumenClona(${item.idChecklist})" id="imaClona${item.idChecklist}" src="../images/iconosAdmCheck/iconoClona.png" >
													</div>
													-->
													<p class="espacio"></p>
													<div class="tooltip">
														<span class="tooltiptext">Visualiza</span>
															<form class="tamImagenResumen" name="formulario" method="POST" action="../central/checklistDetail.htm" id="formDetail${item.idChecklist}">
															<img class="tamImagenResumenPrimera" onclick="resumenVisualiza(${item.idChecklist})" id="imaVisualiza${item.idChecklist}" src="../images/iconosAdmCheck/iconoOjoAbierto.png" >
															<input type="hidden" name="nombreCheck" value="${item.nombreCheck}">
															<input type="hidden" name="idChecklist" value="${item.idChecklist}">
														</form>
													</div>
								
													<p class="espacio"></p>
													<div class="tooltip">
														<span class="tooltiptext">Asigna Check</span>
															<form class="tamImagenResumen" name="formulario" method="POST" action="../central/asignacionChecklist.htm" id="formAsigna${item.idChecklist}">
															<img class="tamImagenResumenPrimera" onclick="resumenAsigna(${item.idChecklist})" src="../images/iconosAdmCheck/iconoAdmin.png" >
															<input type="hidden" name="idChecklist" value="${item.idChecklist}">
															<input type="hidden" id="origen" name="origen" value="false" />
															
														</form>
													</div>
										
													<!-- 
													<p class="espacio"></p>
													<div class="tooltip">
														<span class="tooltiptext">Alerta</span>
														<img class="tamImagenResumenInterro" onclick="resumenAlerta(${item.idChecklist})" id="imaAlerta${item.idChecklist}" src="../images/iconosAdmCheck/interroRojo.png" >
													</div>
													-->
													<p class="espacio"></p>
													<div class="tooltip" id="spanResumenActivo${item.idChecklist}">
													<span class="tooltiptext">Activar/Desactivar</span>
														<c:if test="${item.vigente==0}">										
															<img class="tamImagenResumenActivo imaEstado${item.idChecklist}" onclick="resumenEstado(${item.idChecklist})" id="${item.vigente}" src="../images/iconosAdmCheck/iconoDesactivado.png" >
														</c:if>
														<c:if test="${item.vigente==1}">										
															<img class="tamImagenResumenActivo imaEstado${item.idChecklist}" onclick="resumenEstado(${item.idChecklist})" id="${item.vigente}" src="../images/iconosAdmCheck/iconoActivado.png" >
														</c:if>
													</div>
												</td>
											</tr>
										</c:forEach>	
									</tbody>
						        </table>
						   </c:if>     	
						        				       	                	
		                </div><!--div contenedro uno-->	
		            </div>
					<!-- FIN TABLA CHECKLIST -->
					
					
					<!-- TABLA DE CHECKLISTS PENDIENTES -->
		<div id="tabs-resumen" class="tab-con">
			<h3 style="color:#369C82; text-align:center;">CHECKLIST PENDIENTES Y/O POR AUTORIZAR</h3>
		                <div id="contenedro-tap-uno">

			                <div class="titulo">
				        	    <table class="tblGeneral" id="tblTitResumen">
					        	    <tbody>
					        	        <tr>
						        	        <td>Checklist guardados</td>
						        	        <td>Fecha de creación</td>
						        	        <td>Fecha de modificación</td>
						        	        <td>Usuarios destinados</td>
						        	        <td>&nbsp;</td>
					      	            </tr>
					      	        </tbody>
				      	        </table>
				        	</div>

							<c:if test="${listaResumen!=null}">
								<table class="tblGeneral" id="tblResumen">
									<tbody>
										<c:set var="i" scope="session" value="0" />
										<c:forEach items="${listaPendientes}" var="item">
										<c:set var="i" scope="session" value="${i+1}" />
											<tr class="border">
											    <td>${i}.- ${item.nombreCheck}</td>
											    <td class="tb-light">${item.fecha_inicio}</td>
											    <td class="tb-light">${item.fechaModificacion}</td>
											    <td class="tb-light">${item.usuarios}</td>
												<td>
												<p class="espacio"></p>
													<div class="tooltip">
														<span class="tooltiptext">Visualiza</span>
															<form class="tamImagenResumen" name="formulario" method="POST" action="../central/checklistChangeDetail.htm" id="formChangeDetail${item.idChecklist}">
																<img class="tamImagenResumenPrimera" onclick="resumenChangeDetail(${item.idChecklist})" id="imaVisualiza${item.idChecklist}" src="../images/iconosAdmCheck/iconoOjoAbierto.png" >
																<input type="hidden" name="idChecklist" value="${item.idChecklist}">
																<input type="hidden" name="nombreCheck" value="${item.nombreCheck}">
															</form>
														</div>
												</td>
											</tr>
										</c:forEach>	
									</tbody>
						        </table>
						   </c:if>     	
						        				       	                	
		                </div><!--div contenedro uno-->	
		            </div>
					<!-- FIN TABLA CHECKLIST PENDIENTES-->
	</div>
</div>
	<!-- DIALOG VENTANA MODAL -->
	<div id="dialog" title="Mensaje">
	<p id="mensajeEmergente"></p>
	</div>

</body>
</html>