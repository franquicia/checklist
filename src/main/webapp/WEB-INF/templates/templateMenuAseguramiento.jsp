<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 

	
	
	
 <!-- Menu 
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervision/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Menú Principal<br>
          </div>
        </div>-->
        
        
       <%--  <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la página">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> 
        <div id="menu">--%>
        <c:if test="${perfilSupervision==1}">
          <ul class="l-navegacion nivel1">
          		<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Asegurador</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>Búsqueda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>Búsqueda Asegurador</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<!--  <li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>-->
				<li>
					<a href="indexGaleria.htm"><div>Galería de Evidencias</div></a>
				</li>
				<!--
				<li>
					<a href="#"><div>Administrador</div></a>
					<ul class="nivel2" style="height: 350px; overflow-y: auto;">
			            <li><a href="asignaciones.htm"><div>AsignaciÃ³n de Perfiles</div></a></li>
			            <li><a href="asignaPersonal.htm"><div>AsignaciÃ³n de Personal</div></a></li>
			            <li><a href="asignaCecos.htm"><div>AsignaciÃ³n de Cecos</div></a></li>
			            <li><a href="asignaCecosMasiva.htm"><div>AsignaciÃ³n de Cecos Masiva</div></a></li>
		          	</ul>
				</li> 
				-->
			</ul>
			</c:if>
			<c:if test="${perfilSupervision==3}">
				<ul class="l-navegacion nivel1">
                                        <li>
                                            <a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
                                        </li>
	          		
					<li>
						<a href="indexGaleria.htm"><div>Galería de Evidencias</div></a>
					</li>
					
				</ul>
			</c:if>	
			
			<c:if test="${perfilSupervision==5}">
				<ul class="l-navegacion nivel1">
                                        <li>
                                            <a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
                                        </li>
				</ul>
			</c:if>	
			<c:if test="${perfilSupervision==4}">
          <ul class="l-navegacion nivel1">
          		<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Asegurador</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>Búsqueda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>Búsqueda Asegurador</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<!--  <li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>-->
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galería de Evidencias</div></a>
				</li>
				<!--
				<li>
					<a href="#"><div>Administrador</div></a>
					<ul class="nivel2" style="height: 350px; overflow-y: auto;">
			            <li><a href="asignaciones.htm"><div>AsignaciÃ³n de Perfiles</div></a></li>
			            <li><a href="asignaPersonal.htm"><div>AsignaciÃ³n de Personal</div></a></li>
			            <li><a href="asignaCecos.htm"><div>AsignaciÃ³n de Cecos</div></a></li>
			            <li><a href="asignaCecosMasiva.htm"><div>AsignaciÃ³n de Cecos Masiva</div></a></li>
		          	</ul>
				</li> 
				-->
			</ul>
			</c:if>
    <!--     </div>
       </div>
    </div>-->
    
  <%--   
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/custom-file-input.js"></script><!--Unpload -->
 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/funciones.js"></script>
<!-- Tablas/Paginado -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/paginador.js"></script> 
<script>
$(document).ready(function() {
    $('#registro').DataTable({searching: false, paging: true, info: true, ordering: false, pageLength : 10, lengthMenu: [[5], [5]]});
} );


document.querySelector("#buscar").onkeyup = function(){
    $TableFilter("#registro", this.value);
}

$TableFilter = function(id, value){
    var rows = document.querySelectorAll(id + ' tbody tr');
    for(var i = 0; i < rows.length; i++){
        var showRow = false;
        var row = rows[i];
        row.style.display = 'none';        

        for(var x = 0; x < row.childElementCount; x++){
            if(row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1){
                showRow = true;
                break;
            }
        }        

        if(showRow){
            row.style.display = null;
        }
    }
} --%>
    