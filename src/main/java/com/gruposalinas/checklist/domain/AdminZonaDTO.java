package com.gruposalinas.checklist.domain;

public class AdminZonaDTO {
	private String idZona;
	private String idStatus;
	private String idTipo;
	private String descripcion;
	
	public String getIdZona() {
		return idZona;
	}
	public void setIdZona(String idZona) {
		this.idZona = idZona;
	}
	public String getIdStatus() {
		return idStatus;
	}
	public void setIdStatus(String idStatus) {
		this.idStatus = idStatus;
	}
	public String getIdTipo() {
		return idTipo;
	}
	public void setIdTipo(String idTipo) {
		this.idTipo = idTipo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	@Override
	public String toString() {
		return "AdminZonaDTO [idZona=" + idZona + ", idStatus=" + idStatus + ", idTipo=" + idTipo + ", descripcion="
				+ descripcion + "]";
	}
	
}


