package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.AsignaTransfDTO;

public class FasesTransformacionDetaRowMapper implements RowMapper<AsignaTransfDTO> {

	public AsignaTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AsignaTransfDTO AsignaTransfDTO = new AsignaTransfDTO();
		
		//AsignaTransfDTO.setCeco(rs.getInt("FCID_CECO"));
		AsignaTransfDTO.setIdTabla(rs.getInt("FIID_TAB"));
		AsignaTransfDTO.setAgrupador(rs.getInt("FIID_AGRUPA"));
		AsignaTransfDTO.setFase(rs.getInt("FIID_FASE"));
		AsignaTransfDTO.setIdestatus(rs.getInt("FIIDACTIVO"));
		AsignaTransfDTO.setIdOrdenFase(rs.getInt("FIID_ORDENFASE"));
	    AsignaTransfDTO.setPeriodo(rs.getString("FCPERIODO"));
	    

		return AsignaTransfDTO;
	}

}


