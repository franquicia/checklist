package com.gruposalinas.checklist.business;

import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.ReporteMedicionDAO;
import com.gruposalinas.checklist.domain.ReporteMedPregDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class ReporteMedicionBI {

	private static final Logger logger = LogManager.getLogger(ReporteMedicionBI.class);

	@Autowired
	ReporteMedicionDAO reporteMedicionDAO;
	Map<String, Object> mapReporteMed = null;
	List<ReporteMedPregDTO> cursor = null;


	public Map<String, Object> ReporteMedicion(int idProtocolo, String fechaInicio, String fechaFin)
			throws Exception {
		try {
			mapReporteMed = null;
			mapReporteMed = reporteMedicionDAO.ReporteMedicion(idProtocolo, fechaInicio, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información");
		}

		return mapReporteMed;
	}
	
	public Map<String, Object> ReporteProtMedicion(int idProtocolo, String fechaInicio, String fechaFin)
			throws Exception {
		try {
			mapReporteMed = null;
			mapReporteMed = reporteMedicionDAO.ReporteProtMedicion(idProtocolo, fechaInicio, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" );
		}

		return mapReporteMed;
	}
	
	public List<ReporteMedPregDTO> getCursor(int idProtocolo, String fechaInicio, String fechaFin)
			throws Exception {
		try {
			cursor = null;
			cursor = reporteMedicionDAO.getCursor(idProtocolo, fechaInicio, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información");
		}

		return cursor;
	}

}