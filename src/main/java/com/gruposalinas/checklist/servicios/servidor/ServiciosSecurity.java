package com.gruposalinas.checklist.servicios.servidor;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.gruposalinas.checklist.business.ParametroBI;
import com.gruposalinas.checklist.domain.ParametroDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.util.StrCipher;
import com.gruposalinas.checklist.util.UtilCryptoGS;

@Controller
@RequestMapping("/servicios")
public class ServiciosSecurity {
	private static Logger logger = LogManager.getLogger(ServiciosSecurity.class);
	
    @Autowired
    ParametroBI parametrobi;
    
    // http://10.51.218.140:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/cargaInformacionGeneral.json?idUsuario=73610&idParametro=enviaBuzon
	// http://localhost:8080/checklist/servicios/cargaInformacionGeneral.json?idUsuario=73610&idParametro=<?>
	@SuppressWarnings({ "static-access", "unused" })
	@RequestMapping(value = "/cargaInformacionGeneral", method = RequestMethod.GET)
	public @ResponseBody List<ParametroDTO> getParametros(HttpServletRequest request, HttpServletResponse response)
			throws KeyException, GeneralSecurityException, IOException {

		UtilCryptoGS cifra = new UtilCryptoGS();
		StrCipher cifraIOS = new StrCipher();
		String uri = request.getQueryString();
		String uriAp = request.getParameter("token");
		int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
		uri = uri.split("&")[0];
		String urides = "";
		List<ParametroDTO> parametroResult;
		String json = "";

		// se lee los valores enviados para la pregunta
		UtilCryptoGS descifra = new UtilCryptoGS();

		if (idLlave == 7) {
			urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
		} else if (idLlave == 666) {
			urides = (cifra.decryptParams(uri));
		}

		String idUsuario = urides.split("&")[0].split("=")[1];
		String idParametro = urides.split("&")[1].split("=")[1];

		parametroResult = parametrobi.obtieneParametros(idParametro);

		return parametroResult;

	}
}
