package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.LoginDTO;

public interface LoginDAO {

	public List<LoginDTO> buscaUsuario(int noEmpleado) throws Exception;
	
	public Map<String, Object> buscaUsuarioCheck(int noEmpleado) throws Exception;
}
