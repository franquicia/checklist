package com.gruposalinas.checklist.domain;

public class ProtocoloByCheckDTO {
	
	private int idChecklist;
	private String nomChecklist;
	private int idProtocolo;
	private String nomProtocolo;
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public String getNomChecklist() {
		return nomChecklist;
	}
	public void setNomChecklist(String nomChecklist) {
		this.nomChecklist = nomChecklist;
	}
	public int getIdProtocolo() {
		return idProtocolo;
	}
	public void setIdProtocolo(int idProtocolo) {
		this.idProtocolo = idProtocolo;
	}
	public String getNomProtocolo() {
		return nomProtocolo;
	}
	public void setNomProtocolo(String nomProtocolo) {
		this.nomProtocolo = nomProtocolo;
	}
	@Override
	public String toString() {
		return "ProtocoloByCheckDTO [idChecklist=" + idChecklist + ", nomChecklist=" + nomChecklist + ", idProtocolo="
				+ idProtocolo + ", nomProtocolo=" + nomProtocolo + "]";
	}
	
	
	
}
