package com.gruposalinas.checklist.business;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.gruposalinas.checklist.servicios.servidor.ConsultaCatalogosService;

public class LeerExcelBI {
	private static final Logger logger = LogManager.getLogger(LeerExcelBI.class);

	public boolean leerExcell() {
		try {
			String rutaArchivoExcel = "/franquicia/PROTOCOLOS/OCC.xlsx";
			FileInputStream inputStream = new FileInputStream(new File(rutaArchivoExcel));
			// leer archivo excel
			XSSFWorkbook worbook = new XSSFWorkbook(inputStream);
			// obtener la hoja que se va leer
			XSSFSheet sheet = worbook.getSheetAt(0);
			for (Row row : sheet) {
				for (Cell cell : row) {
					String format="";
					DataFormatter formatter = new DataFormatter();
					// calcula el resultado de la formula
					
					
					 
					// obtiene el valor y se le da formato, en este caso String
					switch (cell.getCellTypeEnum()) {
					case STRING:
						// logger.info(cell.getRichStringCellValue().getString());
						format = formatter.formatCellValue(cell);
						logger.info(format);
						break;
					case NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							// logger.info(cell.getDateCellValue());
							format = formatter.formatCellValue(cell);
							logger.info(format);
						} else {
							// logger.info(cell.getNumericCellValue());
							format = formatter.formatCellValue(cell);
							logger.info(format);
						}
						break;
					case BOOLEAN:
						// logger.info(cell.getBooleanCellValue());
						format = formatter.formatCellValue(cell);
						logger.info(format);
						break;
					case FORMULA:
						// logger.info(cell.getCellFormula());
						 XSSFFormulaEvaluator evaluator = worbook.getCreationHelper().createFormulaEvaluator();
						 CellReference cellRef = new CellReference(cell.getColumnIndex(), cell.getRowIndex()); 
						 CellValue cellValue = evaluator.evaluate(cell); 
						 logger.info("Cell Value: "+cellValue);
						format = formatter.formatCellValue(cell);
						logger.info(format);
						break;
					case BLANK:
						logger.info("");
						break;
					default:
						logger.info("*");
					} // Cierra switch
					
					
					
					
					
					
					
				} // Cierra for cell
			} // Cierra for row

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}