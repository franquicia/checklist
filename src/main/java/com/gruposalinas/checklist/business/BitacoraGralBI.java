package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.BitacoraGralDAO;
import com.gruposalinas.checklist.domain.BitacoraGralDTO;

public class BitacoraGralBI {

	private static Logger logger = LogManager.getLogger(BitacoraGralBI.class);

private List<BitacoraGralDTO> listafila;
	
	@Autowired
	BitacoraGralDAO bitacoraGralDAO;
		
	
	public int inserta(BitacoraGralDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = bitacoraGralDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean elimina(String idBitaGral){
		boolean respuesta = false;
		
		try {
			respuesta = bitacoraGralDAO.elimina(idBitaGral);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<BitacoraGralDTO> obtieneDatos( int idBitaGral) {
		
		try {
			listafila =  bitacoraGralDAO.obtieneDatos(idBitaGral);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<BitacoraGralDTO> obtieneInfo() {

		try {
			listafila = bitacoraGralDAO.obtieneInfo();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de la tabla bitacora Gral");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	
	public boolean actualiza(BitacoraGralDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = bitacoraGralDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	////////////////////////////////////////hijas/////////////////////////////////////
	public int insertaH(BitacoraGralDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = bitacoraGralDAO.insertaH(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaH(String idBitaGral){
		boolean respuesta = false;
		
		try {
			respuesta = bitacoraGralDAO.elimina(idBitaGral);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<BitacoraGralDTO> obtieneDatosH( int idBitaGral) {
		
		try {
			listafila =  bitacoraGralDAO.obtieneDatosH(idBitaGral);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<BitacoraGralDTO> obtieneInfoH() {

		try {
			listafila = bitacoraGralDAO.obtieneInfoH();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de la tabla BITACORA HIJA");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	
	public boolean actualizaH(BitacoraGralDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = bitacoraGralDAO.actualizaH(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	//inserta bitacoras padre e hijas
	
	public int ejecutaSP(String bitacora){
		int respuesta = 0;
		
		try {
			respuesta = bitacoraGralDAO.ejecutaSP(bitacora);
		} catch (Exception e) {
			logger.info("No fue posible insertar las bitacoras");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
}