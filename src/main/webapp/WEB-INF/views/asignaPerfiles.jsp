<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>Asignaci�n de Perfiles</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supPI2/estilos.css">
</head>

<body ng-app="app" ng-controller="validarForm" onpaste="return false">

  <div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="${pageContext.request.contextPath}/img/supervisionPI2/btn_hamburguer.svg" alt="Men� principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="inicio.htm"><img src="${pageContext.request.contextPath}/img/supervisionPI2/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.html" class="selMenu">
          <div>Salir</div>
          <div><img src="${pageContext.request.contextPath}/img/supervisionPI2/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="#">Administrador</a> 
		</div>
    	Asignaci�n de Perfiles
    </div>

	<!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervisionPI2/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Men� Principal<br>
          </div>
        </div>
        <%-- <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la p�gina">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> --%>
        <div id="menu">
          <ul class="l-navegacion nivel1">
				<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
				<li>
					<a href="#"><div>Administrador</div></a>
					<ul class="nivel2" style="height: 350px; overflow-y: auto;">
			            <li><a href="asignaciones.htm"><div>Asignaci�n de Perfiles</div></a></li>
			            <li><a href="asignaPersonal.htm"><div>Asignaci�n de Personal</div></a></li>
			            <li><a href="asignaCecos.htm"><div>Asignaci�n de Cecos</div></a></li>
			            <!-- <li><a href="asignaCecosMasiva.htm"><div>Asignaci�n de Cecos Masiva</div></a></li> -->
		          	</ul>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Asegurador</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>B&uacute;squeda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>B&uacute;squeda Asegurador</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galer&iacute;a de Evidencias</div></a>
				</li>
			</ul>
        </div>
      </div>
    </div>

  <!-- Contenido -->
  <div class="contSecc">
  
  	<div class="liga">
			<a href="inicio.htm" class="liga">
			<img style="height: 15px; width: 15px;" src="${pageContext.request.contextPath}/img/supervision/arrowmleft.png">
			<span>P&Aacute;GINA PRINCIPAL</span>
			</a>
	</div><br>
 

		<div class="titSec">Selecciona al Socio por medio de su n�mero de empleado para asignarle un perfil.</div>
		    <div class="gris">
		      <div class="w33">
		        <div class="divCol1">
		        <c:url value="/central/asignaPerfiles.htm" var="envia" />
				<form:form method="POST" action="${envia}" model="command" name="form" id="form"> 
				
		          <div class="col1">
		            Buscar:<br>
		            <div class="pRelative">
		                <input type="text" value="${numSocio}" id="busca" placeholder="N�mero de Socio" class="buscarNNSocio" maxlength="6" onKeypress="if(event.keyCode == 13) event.returnValue = false;">
		                <input type="submit" class="buscar" onclick="resultado();">
		                <input type="hidden" id="numSocio" name="numSocio" value="" />
		            </div>
		          </div>
  				</form:form>
		          
		        </div>
		      </div>
		   </div>
    
    <c:choose>
    
    	<c:when test="${paso==1}">    	
    		<div class="divResultadoAsegurador">
		      <div class="titSec">Resultado Asegurador Encontrado:</div>
		      <div class="gris">
		      
		      <c:url value="/central/postAsignaPerfilesMenu.htm" var="asignaPerfiles" />
			<form:form method="POST" action="${asignaPerfiles}" model="command" name="formAsigna" id="formAsigna">
		        <div class="divCol2A">
		        
		        <c:forEach var="listaDetalle" items="${listaDetalle}">
		          <div class="w30">
		            <div><span class="bold">Socio:</span> ${listaDetalle.socio}</div>
		            <div><span class="bold">No. de Socio:</span> ${listaDetalle.numSocio}</div>
		            <div><span class="bold">Puesto:</span> ${listaDetalle.puesto}</div>
		            <div><span class="bold">Ceco:</span> ${listaDetalle.ceco}</div>
		            
		            <input type="hidden" id="numEmpleado" name="numEmpleado" value="${listaDetalle.numSocio}"/>
		          </div>
		         </c:forEach>
		          
		          <div class="w70">			          			
		            <div class="divRadio">

		               <c:forEach var="menus" items="${menus}">
		               		<c:set var = "flag" scope = "session" value = "0"/>
		               		<c:forEach var="listaMenusUser" items="${listaMenus}">
		               			<c:if test="${listaMenusUser.idMenu==menus.idMenu}">
		               				<div><input type="checkbox" name="grupo1" id="checkbox${menus.idMenu}" class="checkPerfil" checked = "checked"  value="${menus.idMenu}"><label for="checkbox${menus.idMenu}">${menus.descMenu}</label> </div>
		               				<c:set var = "flag" scope = "session" value = "1"/>
		               			</c:if>
		               		</c:forEach>
		               		<c:if test="${flag==0}">
		               			<div><input type="checkbox" name="grupo1" id="checkbox${menus.idMenu}" class="checkPerfil" value="${menus.idMenu}" ><label for="checkbox${menus.idMenu}">${menus.descMenu}</label> </div>
		               		</c:if>
		               </c:forEach>
		            </div>
		          </div>
		        </div>
		        
		      </div>
		     
				
				<input type="hidden" id="checklist" name="checklist" value=""/>
		      <div class="btnCenter">
		        <a href="#" class="btn btnA" onclick="uncheckAll2()">Limpiar</a>
		        <a href="#" class="btn btnA modalFinalizado_view2" onclick="guardar();">Guardar</a>
		      </div>
		    </div>
		    </form:form>
		    
		    <c:if test="${pasoPopup==1}">
		    
		    	
		    	<script type="text/javascript">
			    	window.onload = function(){
							$('#modalFinalizado').modal();
							return false;
			    	};
		    	</script>
		    </c:if>
    	</c:when>
    
    	<c:when test="${paso==0}">
    		<div class="divResultadoAsegurador">				
				<div class="titSec"><span ><b> NO SE ENCONTRARON DETALLES PARA CONSULTA.</b></span>
				<br><br>
				<div><b>No se encontraron resultados para esta consulta.</b></div>					
				</div>
			</div>
    	</c:when>
    
    </c:choose>
    
    <!-- <div class="divResultadoAsegurador">
      <div class="titSec">Resultado Asegurador Encontrado:</div>
      <div class="gris">
      
        <div class="divCol2A">
          <div class="w30">
            <div><span class="bold">Socio:</span> Jos� Juan Obrador Carrillo</div>
            <div><span class="bold">No. de Socio:</span> 40534</div>
            <div><span class="bold">Puesto:</span> Gerente</div>
            <div><span class="bold">Ceco:</span> 245867</div>
          </div>
          <div class="w70">
            <div class="divRadio">
              <div><input type="checkbox" name="grupo1" id="checkbox02" class="checkPerfil"><label for="checkbox02">Reporte Sucursal</label></div>
              <div><input type="checkbox" name="grupo1" id="checkbox03" class="checkPerfil"><label for="checkbox03">Reporte Asegurador</label></div>
              <div><input type="checkbox" name="grupo1" id="checkbox04" class="checkPerfil"><label for="checkbox04">Reporte Asistencia</label></div>
              <div><input type="checkbox" name="grupo1" id="checkbox05" class="checkPerfil"><label for="checkbox05">B�squeda Sucursal</label></div>
              <div><input type="checkbox" name="grupo1" id="checkbox06" class="checkPerfil"><label for="checkbox06">B�squeda Asegurador</label></div>
              <div><input type="checkbox" name="grupo1" id="checkbox07" class="checkPerfil"><label for="checkbox07">Folios de Mantenimiento</label></div>
              <div><input type="checkbox" name="grupo1" id="checkbox08" class="checkPerfil"><label for="checkbox08">Descarga Base de Protocolos</label></div> 
              <div><input type="checkbox" name="grupo1" id="checkbox09" class="checkPerfil"><label for="checkbox09">Perfil Supervisi�n Gen�rica</label></div>
              <div><input type="checkbox" name="grupo1" id="checkbox10" class="checkPerfil"><label for="checkbox10">Galer�a de Evidencias</label></div>
            </div>
          </div>
        </div>
        
      </div>
      <div class="btnCenter">
        <a href="#" class="btn btnA" onclick="uncheckAll2()">Limpiar</a>
        <a href="#" class="btn btnA modalFinalizado_view">Guardar</a>
      </div>
    </div> -->
    
    
    
	</div>	<!-- Fin conSecc -->
	
	<!-- Footer -->
	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualizaci�n Noviembre 2019</div>
	</div>
</div><!-- Fin page -->

</body>
</html>

<!--Modal-->
<div id="modalFinalizado" class="modal">
  <div class="cuadro cuadroM">
    <a href="#" class="simplemodal-close btnCerrar"><img src="${pageContext.request.contextPath}/img/supervisionPI2/icoCerrar.svg"></a>
    <div class="titModal">Finalizado</div><br>

    <div class="w94Modal">
      <div class="clear"></div>
      <div class="tCenter">Se ha finalizado con �xito el p�rfil asignado</div>
      <div>&nbsp;</div>
    </div>
    <br>
  </div>
  <div class="clear"></div><br>
</div>


<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/supPI2/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supPI2/custom-file-input.js"></script><!--Unpload -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/funciones.js"></script>

<script type="text/javascript">

$(document).ready(function() {

	  $('[name="grupo1"]').click(function() {
	      
	    var arr = $('[name="grupo1"]:checked').map(function(){
	      return this.value;
	    }).get();
	    
	    var str = arr.join(',');
	    
	    $('#arr').text(JSON.stringify(arr));
	    
	    $('#str').text(str);

	    
	    document.getElementById("checklist").value=str;
	    
	  
	  });

	});

function resultado() {
	document.getElementById("numSocio").value = document.getElementById("busca").value;
	//alert("NumSocio: "+document.getElementById("numSocio").value);
	form = document.getElementById("form");
	form.submit();
}

function guardar() {
	//alert("Guardar");
	var valoresI="";
	
		$('[name="grupo1"]:checked').each(
			    function() {
			        valoresI=valoresI+$(this).val()+",";
			    }
			);
	document.getElementById("checklist").value=valoresI;

	//alert(document.getElementById("checklist").value);
	//alert(document.getElementById("numEmpleado").value);
	/* document.getElementById("numSocio").value = document.getElementById("busca").value;
	alert("NumSocio: "+document.getElementById("numSocio").value);
	*/
	form = document.getElementById("formAsigna");
	form.submit(); 
}

</script>
