package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.ReporteChecksExpDTO;



public interface ReporteChecksExpDAO {


		
		public List<ReporteChecksExpDTO> ObtieneBitacoras(String idusuario,String ceco,String idcheck) throws Exception; 
		//public List<ReporteChecksExpDTO> ObtieneBitacorasN(int idcheck) throws Exception; 
		
		public List<ReporteChecksExpDTO> obtieneRespuestasNo(int bitacora) throws Exception; 
		
		//preguntas no imp  ceco fase proyecto 
		public List<ReporteChecksExpDTO> obtienePregNoImpCecoFaseProyecto(String ceco,String fase,String proyecto) throws Exception;
		

		
	}