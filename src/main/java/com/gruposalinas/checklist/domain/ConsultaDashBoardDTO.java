package com.gruposalinas.checklist.domain;

public class ConsultaDashBoardDTO {
	private String idUsuario;
	private String nomCeco;
	private String fecha;
	private String sucVisitadas;
	private String sucxVisitar;
	private String sucAsignada;
	private String sucVisitada;
	
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNomCeco() {
		return nomCeco;
	}
	public void setNomCeco(String nomCeco) {
		this.nomCeco = nomCeco;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getSucVisitadas() {
		return sucVisitadas;
	}
	public void setSucVisitadas(String sucVisitadas) {
		this.sucVisitadas = sucVisitadas;
	}
	public String getSucxVisitar() {
		return sucxVisitar;
	}
	public void setSucxVisitar(String sucxVisitar) {
		this.sucxVisitar = sucxVisitar;
	}
	
	public String getSucAsignada() {
		return sucAsignada;
	}
	public void setSucAsignada(String sucAsignada) {
		this.sucAsignada = sucAsignada;
	}
	public String getSucVisitada() {
		return sucVisitada;
	}
	public void setSucVisitada(String sucVisitada) {
		this.sucVisitada = sucVisitada;
	}
	@Override
	public String toString() {
		return "ConsultaDashBoardDTO [idUsuario=" + idUsuario + ", nomCeco=" + nomCeco + ", fecha=" + fecha
				+ ", sucVisitadas=" + sucVisitadas + ", sucxVisitar=" + sucxVisitar + ", sucAsignada=" + sucAsignada
				+ ", sucVisitada=" + sucVisitada + "]";
	}
	
	
	
	

	
	
	}