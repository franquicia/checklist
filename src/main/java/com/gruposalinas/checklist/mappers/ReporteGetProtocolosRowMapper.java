package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.ReporteImgDTO;

public class ReporteGetProtocolosRowMapper implements RowMapper<ReporteImgDTO> {

	public ReporteImgDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteImgDTO consultaDTO = new ReporteImgDTO();
		consultaDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		consultaDTO.setProtocolo(rs.getString("PROTOCOLO"));
		consultaDTO.setOrdenGpo(rs.getInt("FIORDEN_GRUPO"));
		
		return consultaDTO;
		
		
	}

}
