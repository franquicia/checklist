<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="../css/expancion/modal.css">
<link rel="stylesheet" type="text/css" href="../css/expancion/index.css">
<link rel="stylesheet" type="text/css" href="../css/expancion/tabla-filtros.css">
<link rel="stylesheet" type="text/css" href="../css/expancion/detalleExpancion.css">
<link rel="stylesheet" type="text/css" href="../css/expancion/zoomify.css">


<!-- Owl Stylesheets -->
<link rel="stylesheet" href="../css/expancion/owl.carousel.css">
<link rel="stylesheet" href="../css/expancion/owl.theme.default.css">


<script src="../js/expancion/jquery-3.3.1.js"></script>
<script src="../js/expancion/jquery.dataTables.js"></script>
<script src="../js/expancion/owl.carousel.js"></script>
<script src="../js/expancion/zoomify.js"></script>

<script>
function goBack() {
  window.history.back();
}
</script>

<script type="text/javascript">
	$(document).ready(
			function() {
				$('#owl-carousel1').on(
						'initialized.owl.carousel changed.owl.carousel',
						function(e) {
							if (!e.namespace) {
								return;
							}
							var carousel = e.relatedTarget;
							$('#txtMdl1').html(
									" Item "
											+ '<span class="imgNum">'
											+ (carousel.relative(carousel
													.current()) + 1)
											+ '</span> de '
											+ carousel.items().length);
						}).owlCarousel({
					loop : false,
					margin : 10,
					nav : true,
					items : 1,
					dots : false,
				});

				$('#owl-carousel2').on(
						'initialized.owl.carousel changed.owl.carousel',
						function(e) {
							if (!e.namespace) {
								return;
							}
							var carousel = e.relatedTarget;
							$('#txtMdl2').html(
									" Item "
											+ '<span class="imgNum">'
											+ (carousel.relative(carousel
													.current()) + 1)
											+ '</span> de '
											+ carousel.items().length);
						}).owlCarousel({
					loop : false,
					margin : 10,
					nav : true,
					items : 1,
					dots : false,
				});

				$('.owl-stage-outer').zoomify({

					  // animation duration
					  duration: 200,

					  // easing effect
					  easing:   'linear',

					  // zoom scale
					  // 1 = fullscreen
					  scale:    0.9

					});
			});

	$(document).ready(function() {
		jQuery.extend(jQuery.fn.dataTableExt.oSort, {
			"formatted-num-pre" : function(a) {
				a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
				return parseFloat(a);
			},

			"formatted-num-asc" : function(a, b) {
				return a - b;
			},

			"formatted-num-desc" : function(a, b) {
				return b - a;
			}
		});

		$('#tabla1').DataTable({
			columnDefs : [ {
				type : 'formatted-num',
				targets : 0
			} ]
		});
		$('#tabla2').DataTable({
			columnDefs : [ {
				type : 'formatted-num',
				targets : 0
			} ]
		});
	});
</script>
<style type="text/css">
</style>

<title>PROTOCOLO DETALLE DE SUCURSAL</title>
</head>
<body>

	<div class="header">
		<span class="titulagsBold spBold">REALIZA UNA BÚSQUEDA</span>
	</div>
	<div class="page">
		<div class="contHome top60">

			<div class="detped">
				<div class="detlab">
					<span class="spBold">Sucursal:</span> ${sucursal[0]}
				</div>

				<div class="detlab">
					<span class="spBold">CECO:</span> ${sucursal[1]}
				</div>

				<div class="detlab">
					<span class="spBold">Zona:</span> ${sucursal[2]}
				</div>

				<div class="detlab">
					<span class="spBold">Región:</span> ${sucursal[3]}
				</div>

				<div class="detlab">
					<span class="spBold">Tipo de Visita:</span> ${sucursal[4]}
				</div>

				<div class="detlab">
					<span class="spBold">Fecha:</span> ${sucursal[5]}
				</div>

				<div class="detlab">
					<span class="spBold">Calificación:</span> ${sucursal[6]}
				</div>
			</div>


			<c:if test="${idBitGeneral[0] != 0 }">
				<div>

					<div class="ovtabs">
						<div class="header">
							<span class="titulagsBold spBold">${numPregFilt}.- ${detallePregunta} ${detalleResPregunta[0]}</span>
						</div>
						<span class="titulagsBold spBold" style="float: right;">Recorrido ${fechaRecorrido[0]}</span>
					</div>
					<div class="clear"></div>
					<br>

					<div class="marginTotal">
						<div class="tablahija">
							${tablahijas[0]}
						</div>
					</div>

					<div class="marginTotal">
						<div id="PreguntaUno" style="padding-bottom: 5%;">
							${carrusel[0]}
						</div>

					</div>
				</div>
			</c:if>

			<!--  -->
			<c:if test="${idBitGeneral[1] != 0 }">
				<div>
					<div class="clear"></div>
					<div class="ovtabs">
						<div class="header">
							<span class="titulagsBold spBold">${numPregFilt}.- ${detallePregunta} ${detalleResPregunta[1]}</span>
						</div>
						<span class="titulagsBold spBold" style="float: right;">Recorrido ${fechaRecorrido[1]}</span>
					</div>

					<div class="clear"></div>
					<br>

					<div class="marginTotal">
						<div class="tablahija">
							${tablahijas[1]}
						</div>
					</div>

					<div class="marginTotal">
						<div id="PreguntaDos">
							${carrusel[1]}
						</div>

					</div>
					<br>
				</div>
			</div>
		</div>
	</c:if>
	<div class="pfixed">
		<div class="lfant">
			<a href="#" onclick="goBack()"> Regresar al Buscador
			</a>
		</div>
	</div>

</body>
</html> 	