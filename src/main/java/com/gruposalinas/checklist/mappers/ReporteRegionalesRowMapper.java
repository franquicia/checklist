package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteDTO;

public class ReporteRegionalesRowMapper implements RowMapper<ReporteDTO>  {
	@Override
	public ReporteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		ReporteDTO reporteDTO = new ReporteDTO();
		
		reporteDTO.setIdCeco(rs.getString("FCID_CECO"));
		reporteDTO.setNombreCeco(rs.getString("FCNOMBRE"));
		reporteDTO.setPlan(rs.getInt("PLAN"));
		reporteDTO.setReal(rs.getInt("REAL"));
		reporteDTO.setAvance(rs.getInt("AVANCE"));
		reporteDTO.setDistintos(rs.getInt("DISTINTOS"));
		reporteDTO.setFondo(rs.getString("FONDO"));
		reporteDTO.setImagen(rs.getString("IMAGEN"));
		
		return reporteDTO;
	}
}
