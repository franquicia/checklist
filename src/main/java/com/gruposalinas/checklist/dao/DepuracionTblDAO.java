/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.dao;

import java.util.ArrayList;

/**
 *
 * @author leodan1991
 */
public interface DepuracionTblDAO {
    
    boolean depuraTabla(String nombreTabla) throws Exception;
    ArrayList<Object>  depuraBitacora(String fechaInicio,String fechaFin) throws Exception;
    ArrayList<Object> depuraErrores(String fechaInicio,String fechaFin) throws Exception ;
}
