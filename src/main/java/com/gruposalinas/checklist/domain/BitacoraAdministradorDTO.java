package com.gruposalinas.checklist.domain;

public class BitacoraAdministradorDTO {
	
	private int idBitacora;
	private int idCheckUsua;
	private String longitud;
	private String latitud;
	private String fechaInicio;
	private String fechaFin;
	private int commit;
	private int modo;
	private String ceco;
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public int getIdCheckUsua() {
		return idCheckUsua;
	}
	public void setIdCheckUsua(int idCheckUsua) {
		this.idCheckUsua = idCheckUsua;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public int getCommit() {
		return commit;
	}
	public void setCommit(int commit) {
		this.commit = commit;
	}
	public int getModo() {
		return modo;
	}
	public void setModo(int modo) {
		this.modo = modo;
	}
	public String getCeco() {
		return ceco;
	}
	public void setCeco(String ceco) {
		this.ceco = ceco;
	}
	@Override
	public String toString() {
		return "BitacoraAdministradorDTO [idBitacora=" + idBitacora + ", idCheckUsua=" + idCheckUsua + ", longitud="
				+ longitud + ", latitud=" + latitud + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin
				+ ", commit=" + commit + ", modo=" + modo + ", ceco=" + ceco + "]";
	}
	
}
