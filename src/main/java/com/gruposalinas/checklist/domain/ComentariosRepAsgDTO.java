/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.domain;

/**
 *
 * @author leodan1991
 */
public class ComentariosRepAsgDTO {
    
    private String ceco;
    private String fecha;
    private String sucursal;
    private Integer anio;
    private Integer mes;
    private String comentario;
    private String usrMod;
    private String fechaMod;

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getUsrMod() {
        return usrMod;
    }

    public void setUsrMod(String usrMod) {
        this.usrMod = usrMod;
    }

    public String getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(String fechaMod) {
        this.fechaMod = fechaMod;
    }

    @Override
    public String toString() {
        return "ComentariosRepAsgDTO{" + "ceco=" + ceco + ", fecha=" + fecha + ", sucursal=" + sucursal + ", anio=" + anio + ", mes=" + mes + ", comentario=" + comentario + ", usrMod=" + usrMod + ", fechaMod=" + fechaMod + '}';
    }
    
    
    
    
}
