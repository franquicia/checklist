package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.NegocioDTO;

public interface NegocioDAO {

	public List<NegocioDTO> obtieneNegocio() throws Exception;
	
	public List<NegocioDTO> obtieneNegocio(int idNegocio) throws Exception;
	
	public int insertaNegocio(NegocioDTO bean) throws Exception;
	
	public boolean insertaNegocioNuevo(NegocioDTO bean) throws Exception;
	
	public boolean actualizaNegocio(NegocioDTO bean) throws Exception;
	
	public boolean eliminaNegocio(int idNegocio) throws Exception;
}
