package com.gruposalinas.checklist.mappers;

import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.CompromisoCecoDTO;

public class SucursalChecklistRowMapper  implements RowMapper<CompromisoCecoDTO> {

	public CompromisoCecoDTO mapRow(java.sql.ResultSet rs, int rowNum) throws SQLException {
		
		CompromisoCecoDTO cecoDTO = new CompromisoCecoDTO();

		cecoDTO.setIdCeco(rs.getString("FCID_CECO"));
		cecoDTO.setDescCeco(rs.getString("FCNOMBRE"));
		cecoDTO.setIdChecklist(rs.getString("FIID_CHECKLIST"));
		cecoDTO.setNombreChecklist(rs.getString("NOMBRECHECK"));
		
		return cecoDTO;
	}
}
