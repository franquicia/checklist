$body = $("body");
$(document).on({
    ajaxStart: function() {$body.addClass("loading");},
    ajaxStop: function() {$body.removeClass("loading");}    
});

$(window).load(function() {
	fecha();
});

function myFunction() {
	$body.removeClass("loading");
	location.reload();
}

function nobackbutton(){
	window.location.hash="no-back-button";
	window.location.hash="Again-No-back-button" //chrome
	window.onhashchange=function(){window.location.hash="";}
}

var porcentajeL=0;

function fecha(){
	
	//alert('entro al nombre '+nombreCecoL);
	document.getElementById("idNombreFiltro").innerHTML = "";
	document.getElementById("idNombreFiltro").innerHTML =nombreCecoL ;
	
	document.getElementById("idDiaActual").innerHTML = "";
	document.getElementById("idTotalDias").innerHTML = "";
	
	var hoy = new Date();
	var dd = hoy.getDate();
	var mm = hoy.getMonth()+1;
	var yy = hoy.getFullYear();

	var meses= new Array ('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre');
	
	var mesInicial=seleccionMesL;
	var anoInicial=seleccionAnoL;				
	var diasMes= new Date(anoInicial || new Date().getFullYear(), mesInicial, 0).getDate();
	var ddF = dd;
	var porcentaje=0;
	var por2="";
	var f = new Date();
	
	if((f.getMonth()+1)==mesInicial && f.getFullYear()==anoInicial){
		porcentaje=((f.getDate()-1)*100)/diasMes;
	}else{
		ddF=diasMes;
		porcentaje=100;	
	}

	por2=""+porcentaje;
	var porcenI= por2.split(".");
	porcentajeL=porcenI[0];
	//alert(porcenI[0]);
	
	document.getElementById("idDiaActual").innerHTML = ddF+" de "+meses[mesInicial-1]+" es de "+ porcentajeL+"%";
	document.getElementById("idTotalDias").innerHTML = diasMes;
	
	document.getElementById("idBarPor").innerHTML = porcentajeL+"%";
	document.getElementById("idBarPor").style.width = porcentajeL+"%";
}


function filtraSucursal(idCecoHijo,idCheck,seleccionAno,seleccionMes,seleccionCanal,seleccionPais){
	
	$.ajax({
		type : "GET",
		url : '../ajaxValidaDetalle.json?idCecoHijo='+idCecoHijo+'&idCheck='+idCheck+'&seleccionAno='+seleccionAno	+ '&seleccionMes='+seleccionMes,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		error: function(){ 
			alert('No hay detalles'); 
		}, 
		success : function(json) {	
			if(json!=null){
				setTimeout(myFunction, 20000);
				$body.addClass("loading");
				$("#formDetalle").append("<input type='hidden' name='porcentajeG' id='porcentajeG' value='"+porcentajeG+"' />");
				$("#formDetalle").append("<input type='hidden' name='idCecoPadre' id='idCecoPadre' value='"+idCecoPadreL+"' />");
				$("#formDetalle").append("<input type='hidden' name='idCeco' id='idCeco' value='"+idCecoL+"' />");
				$("#formDetalle").append("<input type='hidden' name='idCecoHijo' id='idCecoHijo' value='"+idCecoHijo+"' />");
				$("#formDetalle").append("<input type='hidden' name='nombreCeco' id='nombreCeco' value='"+nombreCecoL+"' />");
				$("#formDetalle").append("<input type='hidden' name='idCheck' id='idCheck' value='"+idCheck+"' />");
				$("#formDetalle").append("<input type='hidden' name='nivelPerfil' id='nivelPerfil' value='"+nivelPerfilL+"' />");
				$("#formDetalle").append("<input type='hidden' name='banderaCecoPadre' id='banderaCecoPadre' value='"+banderaCecoPadreL+"' />");
				$("#formDetalle").append("<input type='hidden' name='seleccionAno' id='seleccionAno' value='"+seleccionAno+"' />");
				$("#formDetalle").append("<input type='hidden' name='seleccionMes' id='seleccionMes' value='"+seleccionMes+"' />");
				$("#formDetalle").append("<input type='hidden' name='seleccionCanal' id='seleccionCanal' value='"+seleccionCanal+"' />");
				$("#formDetalle").append("<input type='hidden' name='seleccionPais' id='seleccionPais' value='"+seleccionPais+"' />");
				
				document.getElementById("formDetalle").submit();
				return true;
			}else{
				alert('No hay detalles');
			}
		}
	});	
}

var valida=-1;
function muestraSucursal(numOp,idCeco,seleccionAno,seleccionMes,seleccionCanal,seleccionPais){

	if(valida!=numOp){
		valida=numOp;	
		document.getElementById("idLlenaSucursales"+numOp).innerHTML = "";
		//alert(" "+idCeco+" "+idCheck+" "+seleccionAno+" "+seleccionMes+" "+seleccionCanal+" "+seleccionPais);
		
		$.ajax({
			type : "GET",
			url : '../ajaxFiltroSucursalPila.json?idCeco='+idCeco+'&seleccionAno='+seleccionAno
							+ '&seleccionMes='+seleccionMes+'&seleccionCanal='+seleccionCanal+'&seleccionPais='+seleccionPais+'&valorPila='+1,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {
				
				if (json == null) {
					alert('Sin Datos');
					$("#idLlenaSucursales"+numOp).append("<span class='Gris1'>Sin Datos</span>");
				}else{					
					$("#idLlenaSucursales"+numOp).append("<span class='Gris1'>Sucursal</span><br>");

					for (var a = 1; a < Object.keys(json).length; a++) {

						var valor=0;
						$("#idLlenaSucursales"+numOp).append("" +
							"<dt onclick='despliegaCheck("+json[a][0].idCeco+");' id='"+json[a][0].idCeco+"'> "+json[a][0].idCeco+" - <b"+json[a][0].idCeco+" style='font-weight: bold;'>"+json[a][0].descCeco+"</b"+json[a][0].idCeco+">" +
							"</dt>" +
							"<dd>" +
								"<div id='idRellCheck"+json[a][0].idCeco+"' class='inner'>" +
								"</div>" +
							"</dd>" +
							"<hr>");
												
						var retornaB = varBefore(json[a][0].asignados,json[a][0].terminados);
						$("#"+json[a][0].idCeco).append("" +
								"<style>.accordion dt b"+json[a][0].idCeco+"::before{" +
									"content: '"+json[a][0].terminados+" de "+json[a][0].asignados+"';"+
									"background-image: "+retornaB+";"+
									"background-size:50px;"+
									"background-repeat: no-repeat;"+
									"text-align: center;"+
									"float: right;"+
									"width: 50px;"+
									"height: 24px;"+
								"}</style>");
					}							
					ocultaPrueba();
				} 				
			},
			error : function() {
				alert('Favor de actualizar la pagina');
			}
		});
	}
}

var validaCheck=-1;
function despliegaCheck(idCeco){

	if(validaCheck!=idCeco){
		validaCheck=idCeco;
		
		document.getElementById("idRellCheck"+idCeco).innerHTML = "";
		$.ajax({
			type : "GET",
			url : '../ajaxFiltroSucursalCheckPila.json?idCeco='+idCeco+'&seleccionAno='+seleccionAnoL
							+ '&seleccionMes='+seleccionMesL+'&seleccionCanal='+seleccionCanalL+'&seleccionPais='+seleccionPaisL,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {
				
				if (json == null || Object.keys(json).length<=2) {
					$("#idRellCheck"+idCeco).append("" +
							"<table class='bloquer bloquer2 sub1'>" +
								"<tr class='pointer'><td>Sin Checklist Asignados</td></tr>" +
						"</table>");
				}else{
					var adjunta="";
					var adjuntaOtro="";
					var tam=Object.keys(json).length;
					var tama=json[tam-1][0].total;
					
					adjunta="";
					adjuntaOtro="";
					
					for (var b = 0; b < tama; b++) {
						adjunta+="<th style='text-align: center; width: "+64/conteoL+"%;'>"+json[0][b].nombreCheck+"</th>";					
					}
					for (var c = 0; c < tama; c++) {
						
								var div = "<div class='radio amarilloN' style='color:black;' onclick='filtraSucursal("+json[1][c].idCeco+","+json[1][c].idChecklist+","+seleccionAnoL+","+seleccionMesL+","+seleccionCanalL+","+seleccionPaisL+");'>"+json[1][c].total+"%</div>";
							
								if(json[1][c].total >= porcentajeG)
									div = "<div class='radio verde valor1' onclick='filtraSucursal("+json[1][c].idCeco+","+json[1][c].idChecklist+","+seleccionAnoL+","+seleccionMesL+","+seleccionCanalL+","+seleccionPaisL+");'>"+json[1][c].total+"%</div>";
								if(json[1][c].total < (porcentajeG-5))
									div = "<div class='radio rojo valor1' onclick='filtraSucursal("+json[1][c].idCeco+","+json[1][c].idChecklist+","+seleccionAnoL+","+seleccionMesL+","+seleccionCanalL+","+seleccionPaisL+");'>"+json[1][c].total+"%</div>";
						
						adjuntaOtro+="<td style='width: "+64/conteoL+"%;'>" +div+	
									"</td>";
					}
					var valor=0;
					$("#idRellCheck"+idCeco).append("" +
						"<table class='bloquer bloquer2'>" +
							"<thead>" +
								"<tr>"+
									adjunta+
								"</tr>" +
							"</thead>" +
						"</table>" +
						"<table class='bloquer bloquer2 sub1'>" +
							"<tbody>" +
								"<tr class='pointer'>"+
									adjuntaOtro+
								"</tr>" +
							"</tbody>" +
						"</table>");
				} 				
			},
			error : function() {
				alert('Favor de actualizar la pagina');
			}
		});
	}	
}

function varBefore(asignados,terminados){

	var pinta=0;
	if(asignados==0){
		pinta=0;
	}else{
		var resultado=(terminados*100)/asignados;
		if(resultado==0)
			pinta=0;
		else if(resultado>0 && resultado<=25)
			pinta=1;
		else if(resultado>25 && resultado<=50)
			pinta=2;
		else if(resultado>50 && resultado<=75)
			pinta=3;
		else if(resultado>75 && resultado<100)
			pinta=4;
		else if(resultado==100)
			pinta=5;
	}
	//alert('pinta '+ pinta);
	
	var pila="";
	switch (pinta) {
	case 0:
		pila="url('../images/vistaCumplimientoVisitas/avance/pila0.png')";
		break;
	case 1:
		pila="url('../images/vistaCumplimientoVisitas/avance/pila1.png')";
		break;
	case 2:
		pila="url('../images/vistaCumplimientoVisitas/avance/pila2.png')";
	break;
	case 3:
		pila="url('../images/vistaCumplimientoVisitas/avance/pila3.png')";
	break;
	case 4:
		pila="url('../images/vistaCumplimientoVisitas/avance/pila4.png')";
	break;
	case 5:
		pila="url('../images/vistaCumplimientoVisitas/avance/pila5.png')";
	break;
	default:
		break;
	}
	return 	pila;
}

function ocultaPrueba(){
	jQuery(function() {
		$(".subSeccion").hide();		
		var allPanels = $('.accordion > dd').hide();
		jQuery('.accordion > dt').on("click", function() {
			$this = $(this);
			//the target panel content
			$target = $this.next();

			jQuery('.accordion > dt').removeClass('accordion-active');
			if ($target.hasClass("in")) {
				$this.removeClass('accordion-active');
				$target.slideUp();
				$target.removeClass("in");
				$(".subSeccion").hide();
				
			} else {
				$this.addClass('accordion-active');
				jQuery('.accordion > dd').removeClass("in");
				$target.addClass("in");
				$(".subSeccion").show();
				
				jQuery('.accordion > dd').slideUp();
				$target.slideDown();
			}			
		})
	})
}

function regresaVistaAnterior(nivelPerfil,idCecoPadre,nombreCeco,banderaCecoPadre,seleccionAno,seleccionMes,seleccionCanal,seleccionPais){
	setTimeout(myFunction, 20000);
	$body.addClass("loading");
	if(nivelPerfil==0 || nivelPerfil==1){
		$("#formFiltro").append("<input type='hidden' name='porcentajeG' id='porcentajeG' value='"+porcentajeG+"' />");
		$("#formFiltro").append("<input type='hidden' name='banderaCecoPadre' id='banderaCecoPadre' value='"+banderaCecoPadre+"' />");
		$("#formFiltro").append("<input type='hidden' name='nivelPerfil' id='nivelPerfil' value='"+nivelPerfil+"' />");
		$("#formFiltro").append("<input type='hidden' name='idCecoPadre' id='idCecoPadre' value='"+idCecoPadre+"' />");
		$("#formFiltro").append("<input type='hidden' name='nombreCeco' id='nombreCeco' value='"+nombreCeco+"' />");
		$("#formFiltro").append("<input type='hidden' name='seleccionAno' id='seleccionAno' value='"+seleccionAno+"' />");
		$("#formFiltro").append("<input type='hidden' name='seleccionMes' id='seleccionMes' value='"+seleccionMes+"' />");
		$("#formFiltro").append("<input type='hidden' name='seleccionCanal' id='seleccionCanal' value='"+seleccionCanal+"' />");
		$("#formFiltro").append("<input type='hidden' name='seleccionPais' id='seleccionPais' value='"+seleccionPais+"' />");
		document.getElementById("formFiltro").submit();
		return true;
	}else{
		window.location.replace("vistaCumplimientoVisitas.htm");
	}	
}
