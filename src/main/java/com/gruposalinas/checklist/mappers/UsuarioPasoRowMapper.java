package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.Usuario_ADTO;

public class UsuarioPasoRowMapper implements RowMapper<Usuario_ADTO> {
	
	public Usuario_ADTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		Usuario_ADTO usuarioADTO = new Usuario_ADTO();
		
		usuarioADTO.setIdUsuario(rs.getInt("FIEMPLEADO"));
		usuarioADTO.setNombre(rs.getString("FCNOMBRE"));
		usuarioADTO.setIdCeco(rs.getString("FICC"));
		usuarioADTO.setIdPuesto(rs.getInt("FIFUNCION"));
		usuarioADTO.setPuestof(rs.getInt("FIPUESTO"));
		usuarioADTO.setDescPuesto(rs.getString("FCDESCFUN"));
		usuarioADTO.setDesPuestof(rs.getString("FCDESCPUESTO"));
		usuarioADTO.setPais(rs.getString("FCPAIS"));
		usuarioADTO.setFechaBaja(rs.getString("FIFECHABAJA"));
		usuarioADTO.setIdEmpleadoRep(rs.getInt("FIEMPLEADOREP"));
		usuarioADTO.setCcEmpleadoRep(rs.getInt("FICCEMPREP"));
		usuarioADTO.setActivo(rs.getInt("FIDISPONIBLE"));
		usuarioADTO.setFecha(rs.getString("FDNACIMIENTO"));
		
		return usuarioADTO;
	}
	

}
