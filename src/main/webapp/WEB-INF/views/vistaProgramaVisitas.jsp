<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="es">
<head>

<script src="../js/supervisionSistemas/script_asignaVisitas.js" type="text/javascript"></script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />

<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/secciones.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/calendarioCaptacion.css"
	media="screen">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/estiloVentana.css" media="screen">

<title>Supervisi&oacute;n Sucursales</title>

	<script>
		var idUsuarioL=${idUsuario};
	</script>
	
	<% response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate"); %>
	<% response.addHeader("Pragma", "no-cache"); %>
	<% response.addHeader("Expires", "0"); %>
</head>

<body>

	<div class="page">
	
		<div id="dialog-confirm" title="Eliminar" style="display: none;">
			<p>
				<span class="ui-icon ui-icon-alert"
					style=" margin:20px 10px 20px -15px;"></span>¿Deseas
				eliminar la asignaci&oacute;n?
			</p>
		</div>


		<div class="clear"></div>

		<div class="title" id="title">
			<div class="wrapper">
				<div class="h2">
					<a href="/checklist/central/vistaProgramaVisitas.htm">P&aacute;gina Principal </a> / <a>Supervisi&oacute;n Sistemas</a>
				</div>
				<h1>Supervisi&oacute;n Sistemas</h1>
			</div>
		</div>

		<div class="clear"></div>

		<!-- Contenido -->
		<div class="contHome">
			<div class="titN"
				style="width: 90%; max-width: 1300px; margin: auto; margin-bottom: 1%; margin-top: 4%;">
				<table width="100%">
					<tbody>
						<tr>
							<!-- <td width="40%"
								style="text-align: left; font-family: 'Avenir_Next_Demi_Bold'; font-size: 16px; color: #006341;">Asignación
								de Visitas</td>
								<td width="60%"
								style="text-align: left; font-family: 'Avenir_Next_Demi_Bold'; font-size: 16px; color: #006341;">Visitas
								Asignadas</td> -->
							<td width="40%"
								style="text-align: left; font-family: ''Roboto Regular',Fallback,sans-serif'; font-size: 20px; line-height: 1.42857143; color: #333; font-weight: 900;">
								Asignación de Visitas</td>
							<td width="60%"
								style="text-align: left; font-family: ''Roboto Regular',Fallback,sans-serif'; font-size: 20px; line-height: 1.42857143; color: #333; font-weight: 900;">
								Visitas Asignadas</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="clear"></div>



			<div class="home" style="max-width: 1300px; min-width: 750px; height: 485px;">

				<table style="width: 100%;">
					<tr>
						<td style="width: 40%;">

							<form id="miForm" name="miForm" action="" method="get"
								onsubmit="return insertaAsignacion(this);"
								style="height: 440px;">


								<table style="width: 100%;">
									<tbody>
										<tr>
											<td class="etiquetas">¿Quién visita sucursal?</td>
										</tr>
										<tr>
											<td>
												<div class="ui-widget buscar1">
													<input id="empleado" class="buscar1" placeholder="No. empleado / Nombre">
												</div>
											</td>
										</tr>
										
									</tbody>
								</table>

								<table style="margin-top: 4%; width: 100%;">
									<tbody>
										<tr>
											<td class="etiquetas">¿A dónde va a ir?</td>
										</tr>
										<tr>
											<td>											
												<div class="ui-widget buscar1">
													<input id="sucursal" class="buscar1" autocomplete="off" placeholder="Sucursal">
												</div>
											</td>
										</tr>
										
									</tbody>
								</table>

								<table style="margin-top: 4%; width: 97%; max-width: 396px;">
									<tbody>
										<tr>
											<td class="etiquetas" >Fecha:</td>
											<td style="width: 2%;"></td>
											<td><input style="width: 100px;" type="text"
												placeholder="--/--/----" class="datapicker1 date" id="datepicker2">
											</td>
											<td style="width: 5%;"></td>
											<td class="etiquetas">Hora:</td>
											<td style="width: 2%;"></td>
											<td style="width: 100px;">
											
												<select class="normal_select" id="selHora">
														<option value="0"></option>
														<option value="9">09:00 am.</option>
														<option value="10">10:00 am.</option>
														<option value="11">11:00 am.</option>
														<option value="12">12:00 pm.</option>
														<option value="13">1:00 pm.</option>
														<option value="14">2:00 pm.</option>
														<option value="15">3:00 pm.</option>
														<option value="16">4:00 pm.</option>
														<option value="17">5:00 pm.</option>
														<option value="18">6:00 pm.</option>
														<option value="19">7:00 pm.</option>
														<option value="20">8:00 pm.</option>
												</select>
												
											</td>											
										</tr>
									</tbody>
								</table>

								<table style="margin-top: 4%;">
									<tbody>
										<tr>
											<td class="etiquetas">¿Entra a bunker?</td>
											<td style="width: 3%"></td>
											<td style="width: 100px;"><select class="normal_select"
												id="selBunker">
													<option value="0"></option>
													<option value="1">SI</option>
													<option value="2">NO</option>
											</select></td>
										</tr>
									</tbody>
								</table>

								<table
									style="margin-top: 4%; margin-right: 4%; max-width: 400px;">
									<tr>
										<td class="etiquetas">¿A qué hora va a entrar a bunker?</td>
										<td style="width: 3%"></td>
										<td style="width: 110px;">
											<select class="normal_select" id="selHoraBunker">
												<option value="0"></option>
												<option value="9">09:00 am.</option>
												<option value="10">10:00 am.</option>
												<option value="11">11:00 am.</option>
												<option value="12">12:00 pm.</option>
												<option value="13">1:00 pm.</option>
												<option value="14">2:00 pm.</option>
												<option value="15">3:00 pm.</option>
												<option value="16">4:00 pm.</option>
												<option value="17">5:00 pm.</option>
												<option value="18">6:00 pm.</option>
												<option value="19">7:00 pm.</option>
												<option value="20">8:00 pm.</option>
												<option value="21">9:00 pm.</option>
											</select>
										</td>
									</tr>
								</table>
								
								<table
									style="margin-top: 6%; margin-right: 4%; max-width: 400px;">
									<tr>
										<td	class="etiquetas">¿A qu&eacute; hora va a salir de bunker?</td>
										<td style="width: 3%"></td>
										<td style="width: 110px;">
											<div id="validaHoraSalida">
												<select class="normal_select" id="selHoraBunkerSalida">
													<option value="0"></option>
													<option value="9">09:00 am.</option>
													<option value="10">10:00 am.</option>
													<option value="11">11:00 am.</option>
													<option value="12">12:00 pm.</option>
													<option value="13">1:00 pm.</option>
													<option value="14">2:00 pm.</option>
													<option value="15">3:00 pm.</option>
													<option value="16">4:00 pm.</option>
													<option value="17">5:00 pm.</option>
													<option value="18">6:00 pm.</option>
													<option value="19">7:00 pm.</option>
													<option value="20">8:00 pm.</option>
													<option value="21">9:00 pm.</option>
												</select>
											</div>
										</td>
									</tr>
								</table>

								<table style="margin-top: 4%; width: 90%; max-width: 400px;">
									<tbody>
										<tr>
											<td>
												<div class="ui-widget buscar1">
													<textarea id="textarea" name="textarea" rows="10" cols="50"
														onKeyUp="maximo(this,400);" onKeyDown="maximo(this,400);"
														placeholder="Escribe..."></textarea>
												</div>
											</td>
										</tr>
										<tr> <td class="etiquetas"></td> </tr>
										<tr> <td class="etiquetas"></td> </tr>
										<tr>
											<td class="etiquetas">Justificaci&oacute;n. (Max. 400 caracteres)</td>
										</tr>
									</tbody>
								</table>

								<table style="margin-top: 35px; width: 100%; max-width: 400px;">
									<tbody>
										<tr>
											<td rowspan="2" id="btnMuestra">
												<input type="submit" value="Agrega" id="btnRegistrar" style="margin: auto;" class="btnRegistrar">
											</td>
										</tr>
									</tbody>
								</table>
							</form>


						</td>


						<td style="width: 60%; height: 100%">

							<div style="height: 100%; width: 100%;">

								<div class="pagedemo _current" style="display: block; margin-right: 0.5%;">

									<table class="tblGeneral" style="border-top: 0px solid #4fd6b8 !important;">
										<tbody>
											<tr>
												<th style="background-color: #f3f4f4 !important; border:none !important; width: ${(100/9)+2}%;">Nombre</th>
												<th style="background-color: #f3f4f4 !important; border:none !important; width: ${(100/9)+2}%;">Sucursal</th>
												<th style="background-color: #f3f4f4 !important; border:none !important; width: ${100/9}%;">Fecha</th>
												<th style="background-color: #f3f4f4 !important; border:none !important; width: ${100/9}%;">Entra al Bunker</th>
												<th style="background-color: #f3f4f4 !important; border:none !important; width: ${100/9}%;">Hora Entrada Bunker</th>
												<th style="background-color: #f3f4f4 !important; border:none !important; width: ${100/9}%;">Hora Salida Bunker</th>
											
												<th style="background-color: #f3f4f4 !important; border:none !important; width: ${(100/9)+2}%;">Justificacion</th>
												<th style="background-color: #f3f4f4 !important; border:none !important; width: ${(100/9)}%;">Actualizar</th>
												<th style="background-color: #f3f4f4 !important; border:none !important; width: ${(100/9)-6}%;">Eliminar</th>
											</tr>
										</tbody>
									</table>
								</div>


								<div class="pagedemo _current"
									style="display: block; height: 325px; overflow: scroll;">

									<table class="tblGeneral" id="consultaAsignados">
										<tbody>
											<tr>
												<td style="width: ${(100/9)+2}%; text-align: center;">-</td>
												<td style="width: ${(100/9)+2}%; text-align: center;">-</td>
												<td style="width: ${100/9}%; text-align: center;">-</td>
												<td style="width: ${100/9}%; text-align: center;">-</td>
												<td style="width: ${100/9}%; text-align: center;">-</td>
												<td style="width: ${(100/9)+2}%; text-align: center;">-</td>
												<td style="width: ${100/9}%; text-align: center;">-</td>
												<td style="width: ${(100/9)-6}%; text-align: center;">-</td>
											</tr>
										</tbody>
									</table>
								</div>

							</div>
						</td>
					</tr>
				</table>

			</div>

			<div class="clear"></div>

		</div>

		<div class="clear"></div>

		<div class="footer1">
			<table class="tblFooter1">
				<tbody>
					<tr>
						<td style="width: 200px;">Sistema Reportes</td>
						<td style="width: 200px;"><a href="#" class="btnRojo">Malas Pr&aacute;cticas</a></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="footer">
			<table class="tblFooter">
				<tbody>
					<tr>
						<td style="width: 200px;">Banco Azteca S.A. Instituci&oacute;n de Banca M&uacute;ltiple</td>
						<td style="width: 200px;">Derechos Reservados 2014 (T&eacute;rminos y Condiciones de uso
							del Portal.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript">
	$("#empleado").autocomplete({
		source : '../ajaxAutocompletaUsuario.json'
	});

	$("#sucursal").autocomplete({
		source : '../ajaxAutocompletaSucursal.json'
	});

	function maximo(campo, limite) {
		if (campo.value.length >= limite) {
			campo.value = campo.value.substring(0, limite);
		}
	}

	validaCalendario();
</script>

<div id="tipoModal" class="modal"></div>

</html>


