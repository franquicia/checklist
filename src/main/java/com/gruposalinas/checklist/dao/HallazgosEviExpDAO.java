package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.HallazgosEviExpDTO;


public interface HallazgosEviExpDAO {

	    public int inserta(HallazgosEviExpDTO bean)throws Exception;
		
		public boolean elimina(String idEvi)throws Exception;
		
		public List<HallazgosEviExpDTO> obtieneDatos(int idResp) throws Exception; 
		
		public List<HallazgosEviExpDTO> obtieneDatosBita(int bitaGral) throws Exception; 
		
		public List<HallazgosEviExpDTO> obtieneInfo() throws Exception; 
		
		public boolean actualiza(HallazgosEviExpDTO bean)throws Exception;

		

	
	}