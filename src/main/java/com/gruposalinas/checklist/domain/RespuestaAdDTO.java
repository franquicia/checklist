package com.gruposalinas.checklist.domain;

public class RespuestaAdDTO {
	private int idRespuestaAd;
	private int idRespuesta;
	private String descripcion;
	private int commit;
	
	public int getIdRespuestaAd() {
		return idRespuestaAd;
	}
	public void setIdRespuestaAd(int idRespuestaAd) {
		this.idRespuestaAd = idRespuestaAd;
	}
	public int getIdRespuesta() {
		return idRespuesta;
	}
	public void setIdRespuesta(int idRespuesta) {
		this.idRespuesta = idRespuesta;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getCommit() {
		return commit;
	}
	public void setCommit(int commit) {
		this.commit = commit;
	}
	
	
}
