package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.SesionDTO;
import com.gruposalinas.checklist.mappers.SesionRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class SesionDAOImpl extends DefaultDAO implements SesionDAO {

    private static Logger logger = LogManager.getLogger(SesionDAOImpl.class);

    private DefaultJdbcCall insertaSesion;
    private DefaultJdbcCall actualizaSesion;
    private DefaultJdbcCall eliminaSesion;
    private DefaultJdbcCall buscaSesion;
    private DefaultJdbcCall buscaSesionUsuario;
    private DefaultJdbcCall eliminaSesiones;

    public void init() {

        insertaSesion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSESION")
                .withProcedureName("SP_INS_SESION");

        actualizaSesion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSESION")
                .withProcedureName("SP_ACT_SESION");

        eliminaSesion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSESION")
                .withProcedureName("SP_DEL_SESION");

        buscaSesion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSESION")
                .withProcedureName("SP_SEL_SESION")
                .returningResultSet("RCL_SESION", new SesionRowMapper());

        buscaSesionUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSESION")
                .withProcedureName("SP_SEL_SESION_U")
                .returningResultSet("RCL_SESION", new SesionRowMapper());

        eliminaSesiones = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSESION")
                .withProcedureName("SP_DEL_SESIONES");

    }

    @Override
    public List<SesionDTO> buscaSesion(String navegador, String idUsuario, String ip, String fechaLogueo,
            String fechaActualiza, String bandera) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<SesionDTO> listaSesion = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NAVEGADOR", navegador)
                .addValue("PA_USUARIO", idUsuario)
                .addValue("PA_IP", ip)
                .addValue("PA_LOGUEO", fechaLogueo)
                .addValue("PA_ACTUAL", fechaActualiza)
                .addValue("PA_BANDERA", bandera);

        out = buscaSesion.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMSESION.SP_SEL_SESION}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        listaSesion = (List<SesionDTO>) out.get("RCL_SESION");

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return listaSesion;
        }

        return null;
    }

    @Override
    public int insertaSesion(SesionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int id = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NAVEGADOR", bean.getNavegador())
                .addValue("PA_USUARIO", bean.getIdUsuario())
                .addValue("PA_IP", bean.getIp())
                .addValue("PA_LOGUEO", bean.getFechaLogueo())
                .addValue("PA_ACTUAL", bean.getFechaActualiza())
                .addValue("PA_BANDERA", bean.getBandera())
                .addValue("PA_SESIONNAV", bean.getSesionNav());

        out = insertaSesion.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMSESION.SP_INS_SESION}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        BigDecimal returnId = (BigDecimal) out.get("PA_SESION");
        id = returnId.intValue();
        //System.out.println(error);
        if (error == 0) {
            logger.info("Algo ocurrió al insertar");
        } else {
            return id;
        }

        return 0;
    }

    @Override
    public boolean actualizaSesion(SesionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NAVEGADOR", bean.getNavegador())
                .addValue("PA_USUARIO", bean.getIdUsuario())
                .addValue("PA_IP", bean.getIp())
                .addValue("PA_LOGUEO", bean.getFechaLogueo())
                .addValue("PA_ACTUAL", bean.getFechaActualiza())
                .addValue("PA_BANDERA", bean.getBandera())
                .addValue("PA_SESION", bean.getIdSesion())
                .addValue("PA_SESIONNAV", bean.getSesionNav());

        out = actualizaSesion.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMSESION.SP_ACT_SESION}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaSesion(int idSesion) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_SESION", idSesion);

        out = eliminaSesion.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMSESION.SP_DEL_SESION}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public List<SesionDTO> buscaSesion(String idUsuario, String idSesion) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<SesionDTO> listaSesion = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_SESION", idSesion)
                .addValue("PA_USUARIO", idUsuario);

        out = buscaSesionUsuario.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMSESION.SP_SEL_SESION}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        listaSesion = (List<SesionDTO>) out.get("RCL_SESION");

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return listaSesion;
        }

        return null;
    }

    @Override
    public boolean eliminaSesiones(String fechaInicio, String FechaFin) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FECHA_I", fechaInicio).addValue("PA_FECHA_F", FechaFin);

        out = eliminaSesiones.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMSESION.SP_DEL_SESIONES}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió");
        } else {
            return true;
        }

        return false;
    }

}
