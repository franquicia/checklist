<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="height: 98%; min-width: 1200px">
<head>
<link href="../css/resumenCheck.css" rel="stylesheet" type="text/css" />
<script src="/js/jquery/jquery-2.2.4.min.js"></script>

<title>ADMINISTRADOR DE CHECK</title>
<script type="text/javascript">
	var urlServer = '${urlServer}';
	var m =  ${show};
</script>
</head>
<body style="height: 95%" onload="modal()">
	<div class="margenResumenGeneral" >
		<div class="marco" style="overflow-y: scroll">
		<h3>CHECKLIST AUTORIZADOS</h3>
		
			<c:if test="${listaResumen!=null}">
				<table class="diseñoResumenTabla">
					<tr class="diseñoResumenTr">
						<th class="diseñoResumenThIzquierdo margenTrResumenUno">
							<div class="textCenterResumenLetra">
								<img class="tamImagenResumenTabla" src="../images/iconosAdmCheck/iconoGuarda.png" >
								Checklist guardados
							</div>
						</th>
						<th class="diseñoResumenTh margenTrResumenDos">	
							<div class="textCenterResumenLetra">
								<img class="tamImagenResumenTabla" src="../images/iconosAdmCheck/iconoCalendario.png" >
								Fecha de creaci&oacute;n
							</div>
						</th>
						<th class="diseñoResumenTh margenTrResumenTres">
							<div class="textCenterResumenLetra">
								<img class="tamImagenResumenTabla" src="../images/iconosAdmCheck/iconoCalendario.png" >
							Última modificaci&oacute;n
							</div>						
						</th>
						<th class="diseñoResumenTh margenTrResumenCuatro">
							<div class="textCenterResumenLetra">
								<img class="tamImagenResumenTabla" src="../images/iconosAdmCheck/iconoGuarda.png" >
							Usuarios destinados
							</div>
						</th>
						<th class="diseñoResumenThDerecho margenTrResumenCinco"></th>
					</tr>		
					<c:set var="i" scope="session" value="0" />
					<c:forEach items="${listaResumen}" var="item">
					<c:set var="i" scope="session" value="${i+1}" />
						<tr class="diseñoResumenTr">
							<td class="diseñoResumenTd textAliUno bordeIzquierdoResumen">&nbsp;&nbsp;&nbsp;${i}.- ${item.nombreCheck}
							<c:if test="${item.idEstado==26 || item.idEstado==28}">										
									<p class="espacio"></p>
									<div class="tooltip">
										<span class="tooltiptext">En Edici&oacute;n</span>
										<img class="tamImagenResumenInterro" id="imaAlerta${item.idChecklist}" src="../images/iconosAdmCheck/interroRojo.png" >
									</div>
								</c:if>	
							</td>	
							<td class="diseñoResumenTd bordeCentroResumen">${item.fecha_inicio}</td>
							<td class="diseñoResumenTd bordeCentroResumen">${item.fechaModificacion}</td>
							<td class="diseñoResumenTd bordeCentroResumen">${item.usuarios}</td>
							<td class="diseñoResumenTd bordeDerechoResumen"> 
							
								<div class="tooltip">
									<span class="tooltiptext">Editar</span>
									<form class="tamImagenResumen" name="formulario" method="POST" action="../central/checklistDesignF1.htm" id="formResumenEdita${item.idChecklist}">
										<img class="tamImagenResumen"  onclick="resumenEdita( ${item.idChecklist}, ${item.isPendiente})" id="imaEdita${item.idChecklist}" src="../images/iconosAdmCheck/iconoLapiz.png"  >
										<input type="hidden" name="idChecklist" value="${item.idChecklist}">
									</form>
								</div>
								<!-- 
								<p class="espacio"></p>
								<div class="tooltip">
									<span class="tooltiptext">Eliminar</span>
									<img class="tamImagenResumen" onclick="resumenElimina(${item.idChecklist})" id="imaElimina${item.idChecklist}" src="../images/iconosAdmCheck/iconoElimina.png" >
								</div>
								-->
								<!-- 
								<p class="espacio"></p>
								<div class="tooltip">
									<span class="tooltiptext">Clona</span>
									<img class="tamImagenResumen" onclick="resumenClona(${item.idChecklist})" id="imaClona${item.idChecklist}" src="../images/iconosAdmCheck/iconoClona.png" >
								</div>
								-->
					
								<p class="espacio"></p>
								<div class="tooltip">
									<span class="tooltiptext">Visualiza</span>
										<form class="tamImagenResumen" name="formulario" method="POST" action="../central/checklistDetail.htm" id="formDetail${item.idChecklist}">
										<img class="tamImagenResumenPrimera" onclick="resumenVisualiza(${item.idChecklist})" id="imaVisualiza${item.idChecklist}" src="../images/iconosAdmCheck/iconoOjoAbierto.png" >
										<input type="hidden" name="nombreCheck" value="${item.nombreCheck}">
										<input type="hidden" name="idChecklist" value="${item.idChecklist}">
									</form>
								</div>
			
								<p class="espacio"></p>
								<div class="tooltip">
									<span class="tooltiptext">Asigna Check</span>
										<form class="tamImagenResumen" name="formulario" method="POST" action="../central/asignacionChecklist.htm" id="formAsigna${item.idChecklist}">
										<img class="tamImagenResumenPrimera" onclick="resumenAsigna(${item.idChecklist})" src="../images/iconosAdmCheck/iconoAdmin.png" >
										<input type="hidden" name="idChecklist" value="${item.idChecklist}">
										<input type="hidden" id="origen" name="origen" value="false" />
										
									</form>
								</div>
					
								<!-- 
								<p class="espacio"></p>
								<div class="tooltip">
									<span class="tooltiptext">Alerta</span>
									<img class="tamImagenResumenInterro" onclick="resumenAlerta(${item.idChecklist})" id="imaAlerta${item.idChecklist}" src="../images/iconosAdmCheck/interroRojo.png" >
								</div>
								-->
								<p class="espacio"></p>
								<div class="tooltip" id="spanResumenActivo${item.idChecklist}">
								<span class="tooltiptext">Activar/Desactivar</span>
									<c:if test="${item.vigente==0}">										
										<img class="tamImagenResumenActivo imaEstado${item.idChecklist}" onclick="resumenEstado(${item.idChecklist})" id="${item.vigente}" src="../images/iconosAdmCheck/iconoDesactivado.png" >
									</c:if>
									<c:if test="${item.vigente==1}">										
										<img class="tamImagenResumenActivo imaEstado${item.idChecklist}" onclick="resumenEstado(${item.idChecklist})" id="${item.vigente}" src="../images/iconosAdmCheck/iconoActivado.png" >
									</c:if>
								</div>
							</td>		
						</tr>		
					</c:forEach>	
				</table>
			</c:if>
			
			<h3>CHECKLIST NUEVOS Y/O PENDIENTES POR AUTORIZAR</h3>
			<c:if test="${listaResumen!=null}">
				<table class="diseñoResumenTabla">
					<tr class="diseñoResumenTr">
						<th class="diseñoResumenThIzquierdo margenTrResumenUno">
							<div class="textCenterResumenLetra">
								<img class="tamImagenResumenTabla" src="../images/iconosAdmCheck/iconoGuarda.png" >
								Checklist guardados
							</div>
						</th>
						<th class="diseñoResumenTh margenTrResumenDos">	
							<div class="textCenterResumenLetra">
								<img class="tamImagenResumenTabla" src="../images/iconosAdmCheck/iconoCalendario.png" >
								Fecha de creaci&oacute;n
							</div>
						</th>
						<th class="diseñoResumenTh margenTrResumenTres">
							<div class="textCenterResumenLetra">
								<img class="tamImagenResumenTabla" src="../images/iconosAdmCheck/iconoCalendario.png" >
							Última modificaci&oacute;n
							</div>						
						</th>
						<th class="diseñoResumenTh margenTrResumenCuatro">
							<div class="textCenterResumenLetra">
								<img class="tamImagenResumenTabla" src="../images/iconosAdmCheck/iconoGuarda.png" >
							Usuarios destinados
							</div>
						</th>
						<th class="diseñoResumenThDerecho margenTrResumenCinco"></th>
					</tr>
							
					<c:set var="i" scope="session" value="0" />
					<c:forEach items="${listaPendientes}" var="item">
					<c:set var="i" scope="session" value="${i+1}" />
						<tr class="diseñoResumenTr">
							<td class="diseñoResumenTd textAliUno bordeIzquierdoResumen">&nbsp;&nbsp;&nbsp;${i}.- ${item.nombreCheck}</td>	
							<td class="diseñoResumenTd bordeCentroResumen">${item.fecha_inicio}</td>
							<td class="diseñoResumenTd bordeCentroResumen">${item.fechaModificacion}</td>
							<td class="diseñoResumenTd bordeCentroResumen">${item.usuarios}</td>
							
							<td class="diseñoResumenTd bordeDerechoResumen"> 
								<p class="espacio"></p>
								<div class="tooltip">
									<span class="tooltiptext">Visualiza</span>
										<form class="tamImagenResumen" name="formulario" method="POST" action="../central/checklistChangeDetail.htm" id="formChangeDetail${item.idChecklist}">
										<img class="tamImagenResumenPrimera" onclick="resumenChangeDetail(${item.idChecklist})" id="imaVisualiza${item.idChecklist}" src="../images/iconosAdmCheck/iconoOjoAbierto.png" >
										<input type="hidden" name="idChecklist" value="${item.idChecklist}">
										<input type="hidden" name="nombreCheck" value="${item.nombreCheck}">
									</form>
								</div>
							</td>		
						</tr>		
					</c:forEach>	
				</table>
			</c:if>
		</div>	
	</div>
	
	<div id="dialog" title="Mensaje">
	<p id="mensajeEmergente"></p>
	</div>

</body>
</html>