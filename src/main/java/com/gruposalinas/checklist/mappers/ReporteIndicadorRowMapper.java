package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteIndicadorDTO;


public class ReporteIndicadorRowMapper implements RowMapper<ReporteIndicadorDTO> {

	public ReporteIndicadorDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteIndicadorDTO reporteIndicador = new ReporteIndicadorDTO();
		
		reporteIndicador.setIdIndicador(rs.getInt("FIID_INDICADOR"));
		reporteIndicador.setIdCeco(rs.getString("FIID_IDCECO"));
		reporteIndicador.setNombreCeco(rs.getString("FCNOMBRECC"));
		reporteIndicador.setIndicador(rs.getInt("FID_INDICADOR"));
		reporteIndicador.setValor(rs.getString("FIVALOR"));
		
		return reporteIndicador;
	}

}
