package com.gruposalinas.checklist.business;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.checklist.dao.ParametroDAO;
import com.gruposalinas.checklist.dao.RespuestaPdfDAO;
import com.gruposalinas.checklist.domain.DatosChecklistPdfDTO;
import com.gruposalinas.checklist.domain.RespuestaPdfDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class RespuestaPdfBI {
	
	private Logger logger = LogManager.getLogger(RespuestaPdfBI.class);
	
	@Autowired
	RespuestaPdfDAO respuestaPdfDAO;
	
	@Autowired
	ParametroDAO parametroDAO;
	
	@SuppressWarnings("unchecked")
	public JsonArray obtieneRespuestas(int usuario, String ceco){
		
		Map<String, Object> resultado = null;
		List<RespuestaPdfDTO> respuestas = null;
		List<DatosChecklistPdfDTO> datos = null;
		JsonObject principalCheck = null;
		JsonArray principal = new JsonArray();
		
		try{
			
			String checklistParametro = parametroDAO.obtieneParametro("checklistsPDF").get(0).getValor();
			
			String [] checks = checklistParametro.split(",");
			
			for (String idcheck : checks) {
				
				resultado = respuestaPdfDAO.obtieneRespuestas(usuario, Integer.parseInt( idcheck),ceco);
				
				respuestas = (List<RespuestaPdfDTO>) resultado.get("respuestas");
				
				datos =  (List<DatosChecklistPdfDTO>) resultado.get("datos");
				
				principalCheck = new JsonObject();
				
				if(respuestas.size()  > 0){
					
					principalCheck.addProperty("idCheck", idcheck);
					principalCheck.addProperty("nombrecheck", datos.get(0).getNombreCheck());
					principalCheck.addProperty("nombreCeco", datos.get(0).getNombreCeco());
					principalCheck.addProperty("gerente", datos.get(0).getGerente());
					principalCheck.addProperty("regional", datos.get(0).getRegional());
					principalCheck.addProperty("fechaInicio", datos.get(0).getFechaInicio());
					principalCheck.addProperty("fechaFin", datos.get(0).getFechaFin());
					
					
					JsonObject aux = null;
					JsonArray arrayPreguntas = new JsonArray();
					
					JsonObject preguntaPrincipal = null;
					JsonArray arrayRespuestas = null;
					
					for(int i = 0; i < respuestas.size() ; i++ ){//Recorrer pregunta por pregunta
						
						RespuestaPdfDTO pregunta1 = respuestas.get(i);
						
						arrayRespuestas = new JsonArray();
						
						for(int j = i + 1;j<respuestas.size();j++){
						
							RespuestaPdfDTO pregunta2 = respuestas.get(j);

							//Valida si una pregunta depende de otra.
							if(pregunta1.getIdPregunta() == pregunta2.getPreguntaPadre()){							
								aux = new JsonObject();
								aux.addProperty("pregunta", pregunta2.getPregunta());
								aux.addProperty("respuesta",pregunta2.getRespuesta());													
								arrayRespuestas.add(aux);					
							}
												
						}														
						//Agrego Pregunta Padre respuesta1 y agregar la lista de hijas 									
						if (pregunta1.getPreguntaPadre()==0 && pregunta1.getRespuesta().equals("NO") ){
							preguntaPrincipal = new JsonObject();						
							preguntaPrincipal.addProperty("preguntaPadre", pregunta1.getPregunta()) ;
							preguntaPrincipal.addProperty("respuestaPadre", pregunta1.getRespuesta()) ;						
							preguntaPrincipal.add("respuestas", arrayRespuestas);	
							arrayPreguntas.add(preguntaPrincipal);						
						}									
					}					
					principalCheck.add("preguntas", arrayPreguntas);					
				}
				
				principal.add(principalCheck);
				

			}
			
						
			
		}
		catch(Exception e){
			logger.info("Ocurrio algo al obtener las respuestas para el usuario ("+usuario+") ");
				
		}
		
		logger.info("JSON OBJECT ---> " + principal.toString());
		return principal;
		
	}
	
	public int obtieneEnvioCompromiso(int idUsuario, int idBitacora){
		int res = 0;
		
		try {
			logger.info("RespuestaPDFBI OBTIENE ENVIO COMPROMISOS --- USUARIO -- "+ idUsuario + "--- BITACORA ---" + idBitacora);
			res = respuestaPdfDAO.enviaCompromisos(idUsuario , idBitacora);
			
		}catch(Exception e){
			logger.info("Ocurrio algo al obtener el envio de compromisos");
				
		}
		
		
		return res;
	}
	
	
	public  Map<String, String>  obtieneCecoPorBitacora(int idUsuario, int idBitacora){
		Map<String, String> res = new HashMap<String, String>();
		try {
			logger.info("RespuestaPDFBI CECO POR BITACORA --- USUARIO -- "+ idUsuario + "--- BITACORA ---" + idBitacora);
			res = respuestaPdfDAO.obtieneCecoPorBitacora(idUsuario, idBitacora);
			
		}catch(Exception e){
			logger.info("Ocurrio algo al obtener el envio de compromisos");
				
		}
		
		return res;
	}
	

}
