package com.gruposalinas.checklist.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.BuscadorBI;
import com.gruposalinas.checklist.business.CanalBI;
import com.gruposalinas.checklist.business.PDFMarksBI;
import com.gruposalinas.checklist.dao.GridFSDAO;
import com.gruposalinas.checklist.domain.Tag;
import com.gruposalinas.checklist.util.CleanParameter;
import com.gruposalinas.checklist.util.UtilFRQ;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

@Controller
public class BuscadorController {
	private static Logger logger = LogManager.getLogger(BuscadorController.class);

	@Autowired
	private GridFSDAO fileStorage;
	@Autowired
	private CleanParameter UtilString;

	private String recurso;
	private String tagName;
	
	/* +++++++++Metodos de controlador para el autocompletado+++++++++ */
	public BuscadorController() {
		this.recurso = null;
	}

	@RequestMapping(value = "/busqueda.htm", method = RequestMethod.GET)
	public ModelAndView metodoCompleta() {
		return new ModelAndView("buscador");
	}

	@RequestMapping(value = "/getTags", method = RequestMethod.GET)
	public @ResponseBody List<Tag> getTags(@RequestParam String tagName, HttpServletRequest request) {
		/*try {
			this.tagName = UtilString.cleanParameter(tagName);
			tagName = new String(tagName.getBytes("ISO-8859-1"), "UTF-8");
			if (tagName.length() > 3) {
				return buscadorBI.getTags(tagName, request);
			} else {
				return null;
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			logger.info("Ocurrio algo...");
			return null;
		}*/
		return null;
	}

	// AJAX CONTROLLER METHODS
	@RequestMapping(value = "/ajaxtest", method = RequestMethod.GET)
	public @ResponseBody List<Tag> getData(
			@RequestParam(value = "termino", required = true, defaultValue = "termino") String termino, HttpServletRequest request) {
		/*try {
				termino = UtilString.cleanParameter(termino);
				termino = new String(termino.getBytes("ISO-8859-1"), "UTF-8");
				return buscadorBI.getResults(termino);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				logger.info("Ocurrio algo...");
				return null;
			}*/
		return null;
	}

	@RequestMapping(value = "/muestraDoc.htm", method = RequestMethod.GET)
	public @ResponseBody ModelAndView muestraDoc(HttpServletResponse response, HttpServletRequest request,
			@RequestParam(value = "recurso", required = true, defaultValue = "") String recurso) throws IOException {
		/*//No aplico el limpiar por que quita el punto
		this.recurso = recurso;
		List<String> np = buscadorBI.muestraDoc(recurso,this.tagName, request);
		ModelAndView mv = new ModelAndView("muestraDoc");
		mv.addObject("numPags", np);
		mv.addObject("recurso", recurso);
		return mv;*/
		return null;

	}
	
	/* METODO PARA EMBEBER EL DOCUMENTO PDF AL DIV-EMBED */
	@RequestMapping(value = "getPdfDocument.htm", method = RequestMethod.GET)
	public String reporte(HttpServletRequest request, HttpServletResponse response) throws IOException {
		/*File f = fileStorage.getFile(this.recurso);
		
	    try {
			
	    	File nf = new PDFMarksBI().getMarks(f.getAbsolutePath(),this.tagName, request);
			InputStream fileInputStream = FileUtils.openInputStream(nf);
	    	
			response.setHeader("Content-Type", "application/pdf");
			response.setHeader("Content-Disposition", "inline; filename=Doc.pdf");
			response.setHeader("Content-Transfer-Encoding: binary", "Accept-Ranges: bytes");
			response.setHeader("Cache-Control:private", "Pragma: private");
			response.setHeader("Expires:0", "attachment;filename=documento.pdf");
			try {
				FileCopyUtils.copy(fileInputStream, response.getOutputStream());
			} catch ( Exception e ) {
				logger.info("Ocurrio algo...");
			} finally {
				fileInputStream.close();
			}
			
		} catch (Exception e1) {
			logger.info("Ocurrio algo...");
		}
		response.getOutputStream();
		response.flushBuffer();
		
		return null;*/
		return "null";

	}
	
	@ExceptionHandler(Exception.class)
	public ModelAndView handleError(HttpServletRequest request, Exception ex) {
		logger.info("Ocurrio algo...");
		ModelAndView mv = new ModelAndView("exception");
	    return mv;
	}
}

//CODIGO DOCUMENTO QUE ME DIO CESAR
/*
@RequestMapping(value="getDocument.htm", method = RequestMethod.GET)
public String reporteExcel(HttpServletRequest request, HttpServletResponse response) throws IOException{
	ServletOutputStream sos = null;
	String edo = "ESTADO";
	String tipo = "TIPO";
	String since = "SINCE";
	String to = "TO";
	String archivo = "Folio,Fecha,Nombre de Socio,Categoría,Título,Estado\n";
	archivo += "SI ME CREO EL DOCUMENTO!!!!!!!!"+edo+tipo+since+to+archivo;		
	response.setContentType("application/pdf");
	response.addHeader("Content-Disposition", "attachment; filename=ReporteAdministracion.pdf");
	sos = response.getOutputStream();
	sos.write(archivo.getBytes());
	return null;
}
 */
/*FUNCIONA EN LOS NAVEGADORES
 * response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-Disposition", null);
 */
