<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>B�squeda de Sucursal</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervision/estilos.css">
</head>

<body ng-app="app" ng-controller="validarForm">

  <div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="${pageContext.request.contextPath}/img/supervision/btn_hamburguer.svg" alt="Men� principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="inicio.htm"><img src="${pageContext.request.contextPath}/img/supervision/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.htm" class="selMenu">
          <div>Salir</div>
          <div><img src="${pageContext.request.contextPath}/img/supervision/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="indexSupervision.htm">Reporte de Supervisores</a> 
		</div>
    	Dashboard de Sucursal
    </div>

	<!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervision/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Men� Principal<br>
          </div>
        </div>
        <%-- <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la p�gina">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> --%>
        <div id="menu">
       		 <tiles:insertTemplate template="/WEB-INF/templates/templateMenuAseguramiento.jsp" flush="true">
				<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuAseguramiento.jsp" />
			</tiles:insertTemplate>
         <!--  <ul class="l-navegacion nivel1">
          		<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Asegurador</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>B�squeda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>B�squeda Asegurador</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galer�a de Evidencias</div></a>
				</li>
			</ul> -->
        </div>
      </div>
    </div>

    <!-- Contenido -->
    <div class="contSecc">
    
    <div class="liga">
		<a href="#" onclick="javascript:window.history.back()" class="liga">
		<img style="height: 15px; width: 15px;" src="${pageContext.request.contextPath}/img/supervision/arrowmleft.png">
		<span>P�GINA ANTERIOR</span>
		</a>
		</div>
    
		<div class="titSec"></div>
		<div class="contBalco">
			<div class="datSuc">
				<div>
					<div><strong>Sucursal:</strong> ${nomSucursal} </div>
					<div><strong>Zona:</strong> ${nomZona} </div>
					<div><strong>Regi�n:</strong> ${nomRegion}</div>
					<div><strong>Territorio:</strong> ${nomTerritorio} </div>
				</div>
				<!-- <div class="btnDer">
					<a href="#" class="btn">Revisar tu �ltima visita</a>
				</div> -->
			</div>
		</div>
        
        <script type="text/javascript">
        
                        var mesCalif1 = '<c:out value="${mesCalif1}"/>';
                        var mesCalif2 = '<c:out value="${mesCalif2}"/>';
                        var mesCalif3 = '<c:out value="${mesCalif3}"/>';
                        var mesCalif4 = '<c:out value="${mesCalif4}"/>';
                        var mesCalif5 = '<c:out value="${mesCalif5}"/>';
                        var mesCalif6 = '<c:out value="${mesCalif6}"/>';
                        var mesCalif7 = '<c:out value="${mesCalif7}"/>';
                        var mesCalif8 = '<c:out value="${mesCalif8}"/>';
                        var mesCalif9 = '<c:out value="${mesCalif9}"/>';
                        var mesCalif10 = '<c:out value="${mesCalif10}"/>';
                        var mesCalif11 = '<c:out value="${mesCalif11}"/>';
                        var mesCalif12 = '<c:out value="${mesCalif12}"/>';

                        var mesVis1 = '<c:out value="${mesVis1}"/>';
                        var mesVis2 = '<c:out value="${mesVis2}"/>';
                        var mesVis3 = '<c:out value="${mesVis3}"/>';
                        var mesVis4 = '<c:out value="${mesVis4}"/>';
                        var mesVis5 = '<c:out value="${mesVis5}"/>';
                        var mesVis6 = '<c:out value="${mesVis6}"/>';
                        var mesVis7 = '<c:out value="${mesVis7}"/>';
                        var mesVis8 = '<c:out value="${mesVis8}"/>';
                        var mesVis9 = '<c:out value="${mesVis9}"/>';
                        var mesVis10 = '<c:out value="${mesVis10}"/>';
                        var mesVis11 = '<c:out value="${mesVis11}"/>';
                        var mesVis12 = '<c:out value="${mesVis12}"/>';

                        var m1 = 0;
            			var m2 = 0;
            			var m3 = 0;
            			var m4 = 0;
            			var m5 = 0;
            			var m6 =0;
            			var m7 = 0;
            			var m8 = 0;
            			var m9 = 0;
            			var m10 = 0;
            			var m11 = 0;
            			var m12 = 0;

            			
            			var visitar=0;
            			var visitadas=0;

            			//alert(mesCalif8+" , "+mesVis8);

        </script>
		
		<div class="divTablero5" >
            <div class="divVertical" >
                <canvas id="canvas-vertical" class="graVerical" style="height: 145px; width: 100px; "></canvas>
            </div>
            <div class="divVertical">
                <canvas id="canvas-vertical3" class="graVerical" style="height: 145px; width: 100px;"></canvas>
            </div>
    	</div>

		<div class="clear"></div>
		
		<div class="titSec">Genera una consulta de Protocolos por Fecha o Protocolos</div>
		<div class="gris">
		
			<div class="divCol4 flexJusCen">
				<div class="col4 col4B">
					Fecha de inicio:<br>
					<input type="text" placeholder="DD/MM/AAAA" value="${cal1}" class="datapicker1 date" id="datepicker" autocomplete="off">
				</div>
				<div class="col4 w30A"><div><strong>a</strong></div></div>
				<div class="col4 col4B">
					Fecha de fin:<br>
					<input type="text" placeholder="DD/MM/AAAA" value="${cal2}" class="datapicker1 date" id="datepicker1" autocomplete="off">
				</div>
				<div class="col4 col4B">
					Protocolo:<br>
					
					<c:url value="/central/busquedaSucursalSeleccionados.htm" var="archivoPasivo" />
					<form:form method="POST" action="${archivoPasivo}" model="command" name="formProtocolo" id="formProtocolo">
					
						<select id="Protocolo" onchange="javascript:getProtocoloSelected()">
							  <c:forEach var="listaProtocolos" items="${listaProtocolos}">
									<option value="${listaProtocolos.idChecklist}">
										<c:out value="${listaProtocolos.nomChecklist}"></c:out>
									</option>
								</c:forEach>
								
								<input id="nomSucursal" name="nomSucursal" type="hidden" value="${nomSucursal}" >
								<input id="nomZona" name="nomZona" type="hidden" value="${nomZona}" >
								<input id="nomRegion" name="nomRegion" type="hidden" value="${nomRegion}" >
								<input id="nomTerritorio" name="nomTerritorio" type="hidden" value="${nomTerritorio}" >
								<input id="ProtocoloSel" name="ProtocoloSel" type="hidden" value="${idProtocolo}" />
								<input id="SucursalSel" name="SucursalSel" type="hidden" value="${idSucursal}" />							
								<input id="RegionSel" name="RegionSel" type="hidden" value="${idRegion}" />
								<input id="ZonaSel" name="ZonaSel" type="hidden" value="${idZona}" />
								<input id="TerritorioSel" name="TerritorioSel" type="hidden" value="${idTerritorio}" />
								<input id="calendarioIni" name="calendarioIni" type="hidden" value="${cal1}"/>
								<input id="calendarioFin" name="calendarioFin" type="hidden" value="${cal2}"/>
						</select>
					</form:form>
				</div>
			</div>
			
			<c:url value="/central/busquedaSucursalDatos.htm" var="archivoPasivo" />
			<form:form method="POST" action="${archivoPasivo}" model="command" name="formEnvia" id="formEnvia">		
						
				<div class="btnCenter">
					<a href="#" class="btn btnBuscarP" onclick="return validaFechas();">Buscar</a>
				</div>
					<input id="idProtocolo" name="idProtocolo" type="hidden" value="" />
					<input id="fechaInicio" name="fechaInicio" type="hidden" value="${cal1}" />							
					<input id="fechaFin" name="fechaFin" type="hidden" value="${cal2}" />
					<input id="nomSucursal" name="nomSucursal" type="hidden" value="${nomSucursal}" >
					<input id="nomZona" name="nomZona" type="hidden" value="${nomZona}" >
					<input id="nomRegion" name="nomRegion" type="hidden" value="${nomRegion}" >
					<input id="nomTerritorio" name="nomTerritorio" type="hidden" value="${nomTerritorio}" >
					<input id="idSucursal" name="idSucursal" type="hidden" value="${idSucursal}" />							
					<input id="idRegion" name="idRegion" type="hidden" value="${idRegion}" />
					<input id="idZona" name="idZona" type="hidden" value="${idZona}" />
					<input id="idTerritorio" name="idTerritorio" type="hidden" value="${idTerritorio}" /> 
					
			</form:form>
			
		</div>
		
		<c:choose>
		<c:when test="${paso == 1}"> <!-- Protocolo Seleccionado -->
		
		<div class="divHideV">
							
							
		<br><br>
		
		
    	<input id="buscar" maxlength="50" type="text" class="form-control" placeholder="B�squeda" />
    	
			<div class="titSec">Visitas que coinciden con tu b�squeda</div>
			<div class="gris">
				<div class="scroll">
					<table id="resultado" class="tblGeneral tblVisitas">
						<tbody>
							<tr>
								<th>Nombre de Sucursal</th>
								<th>Fecha de Inicio</th>
								<th>Fecha de Fin</th>
								<th>Supervisor</th>
							</tr>
							<c:forEach var="listaDatos" items="${listaDatos}">
								<tr>
												
								<c:url value="/central/detalleProtocoloSupervision.htm" var="envioDetalle" />
								<form:form method="POST" action="${envioDetalle}" model="command" name="formDetalleProtocolo" id="formDetalleProtocolo">
									<td class="liga">
										<a href="#" onclick="javascript:enviarDatos(${listaDatos.idBitacora});">${listaDatos.nomSucursal}</a>
									</td>
									<td>${listaDatos.fechaInicio}</td>
									<td>${listaDatos.fechaFin}</td>
									<td>${listaDatos.nomUsuario}</td>
									<input id="idBitacora" name="idBitacora" type="hidden" value=""/>
									<input id="sucursalEnv" name="sucursalEnv" type="hidden" value=""/>
									<input id="zonaEnv" name="zonaEnv" type="hidden" value=""/>													
									<input id="regionEnv" name="regionEnv" type="hidden" value=""/>
									<input id="terrEnv" name="terrEnv" type="hidden" value=""/>
									<input id="proEnv" name="proEnv" type="hidden" value="${listaDatos.nomProtocolo}"/>
									<input id="idProEnv" name="idProEnv" type="hidden" value=""/>
								</form:form>
												
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		</c:when>
		<c:when test="${paso == 2}"> <!-- Protocolo no Seleccionado -->

		<div class="divHideP">
							
							
		<br><br>
		
		
    	<input id="buscar" maxlength="50" type="text" class="form-control" placeholder="B�squeda" />
    	
			<div class="titSec">Protocolos que coinciden con tu b�squeda</div>
			<div class="gris">
				<div class="scroll">
					<table id="resultado" class="tblGeneral tblVisitas">
						<tbody>
							<tr>
								<th>Nombre de Sucursal</th>
								<th>Protocolo</th>
								<th>Fecha de Visita</th>
								<th>Asegurador</th>
							</tr>
							
							<c:forEach var="listaDatos" items="${listaDatos}">
								<tr>
												
								<c:url value="/central/detalleProtocoloSupervision.htm" var="envioDetalle" />
								<form:form method="POST" action="${envioDetalle}" model="command" name="formDetalleProtocolo" id="formDetalleProtocolo">
									<td class="liga">
										<a href="#" onclick="javascript:enviarDatos(${listaDatos.idBitacora});">${listaDatos.nomSucursal}</a>
									</td>
									<td>${listaDatos.nomProtocolo}</td>
									<td>${listaDatos.fechaFin}</td>
									<td>${listaDatos.nomUsuario}</td>
									<input id="idBitacora" name="idBitacora" type="hidden" value=""/>
									<input id="sucursalEnv" name="sucursalEnv" type="hidden" value=""/>
									<input id="zonaEnv" name="zonaEnv" type="hidden" value=""/>													
									<input id="regionEnv" name="regionEnv" type="hidden" value=""/>
									<input id="terrEnv" name="terrEnv" type="hidden" value=""/>
									<input id="proEnv" name="proEnv" type="hidden" value="${listaDatos.nomProtocolo}"/>
									<input id="idProEnv" name="idProEnv" type="hidden" value=""/>
								</form:form>
												
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		</c:when>
		<c:when test="${paso == 3}"> <!-- Lista de Consulta Vacia -->
			<div class="divHideP">
			<div class="tit tCenter"><span ><b> NO SE ENCONTRARON DETALLES PARA CONSULTA.</b></span>
						<br><br>
						<div><b>No se encontraron resultados para esta consulta.</b></div>					
					</div>
		</div>
		</c:when>
		</c:choose>
		
	</div>	<!-- Fin conSecc -->
	
	<!-- Footer -->
	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualizaci�n Febrero 2019</div>
	</div>
</div><!-- Fin page -->

</body>
</html>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/custom-file-input.js"></script><!--Unpload -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/funciones.js"></script>

<!-- Graficas -->

<script src="${pageContext.request.contextPath}/js/supervision/Chart.bundle.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/utils.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/Chart.PieceLabel.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/grafica.js"></script>

<script>

document.querySelector("#buscar").onkeyup = function(){
    $TableFilter("#resultado", this.value);
}

$TableFilter = function(id, value){
    var rows = document.querySelectorAll(id + ' tbody tr');
    for(var i = 0; i < rows.length; i++){
        var showRow = false;
        var row = rows[i];
        row.style.display = 'none';        

        for(var x = 0; x < row.childElementCount; x++){
            if(row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1){
                showRow = true;
                break;
            }
        }        

        if(showRow){
            row.style.display = null;
        }
    }
}



</script>

<script type="text/javascript">

function enviarDatos(id){
	document.getElementById("idBitacora").value = id;
	document.getElementById("sucursalEnv").value = document.getElementById("idSucursal").value;
	document.getElementById("zonaEnv").value = document.getElementById("idZona").value;
	document.getElementById("regionEnv").value = document.getElementById("idRegion").value;
	document.getElementById("terrEnv").value = document.getElementById("idTerritorio").value;
	document.getElementById("idProEnv").value = document.getElementById("idProtocolo").value;
	form = document.getElementById("formDetalleProtocolo");
	//alert("ID: " + document.getElementById('idBitacora').value+", "+document.getElementById("idProEnv").value);
	form.submit();
}

 function getProtocoloSelected() {
	//alert("Entra");
	var select = document.getElementById("Protocolo");
    value = select.value;
    
	form = document.getElementById("formProtocolo");
	document.getElementById('ProtocoloSel').value = value;
	document.getElementById('idProtocolo').value = value;
	document.getElementById("calendarioIni").value=document.getElementById("datepicker").value
	document.getElementById("calendarioFin").value=document.getElementById("datepicker1").value
	form.submit();
} 

 function validaFechas(){	
		var f1=document.getElementById("datepicker").value;
		var f2=document.getElementById("datepicker1").value;
		
	 	var Fecha1 = stringToDate(f1,"dd/MM/yyyy","/");
	 	var Fech2 = stringToDate(f2,"dd/MM/yyyy","/");
	 	var FHoy = new Date();
		
	    var flag1=false;
	 	var flag2=false;
		
		if (isNaN(Fecha1)){
			alert("Debes colocar fecha inicio");
			return false;
		}
		else{
			if(Fecha1<FHoy){
			flag1=true;
			}else{
				alert("La fecha inicio no puede ser mayor que hoy");
				return false;
			}
		}
		
		if (isNaN(Fech2)){
			alert("Debes colocar fecha fin");
			return false;
		}
		else{
			if(Fech2<FHoy){
				flag2=true;
				}else{
					alert("La fecha fin no puede ser mayor que hoy");
					return false;
				}
		} 

		if(flag1==true && flag2==true){
			if(Fecha1>Fech2){
				alert("La fecha de inicio no debe ser mayor que la de fin");
				return false;
			}else{

				document.getElementById("calendarioIni").value=f1;
				document.getElementById("calendarioFin").value=f2;
				
				document.getElementById('fechaInicio').value = f1;
				document.getElementById('fechaFin').value = f2;
				//alert(document.getElementById('idProtocolo').value);
				form = document.getElementById("formEnvia");
				form.submit();
			}
		} 	
		return false;	
	}

	function stringToDate(_date,_format,_delimiter)
	{
	            var formatLowerCase=_format.toLowerCase();
	            var formatItems=formatLowerCase.split(_delimiter);
	            var dateItems=_date.split(_delimiter);
	            var monthIndex=formatItems.indexOf("mm");
	            var dayIndex=formatItems.indexOf("dd");
	            var yearIndex=formatItems.indexOf("yyyy");
	            var month=parseInt(dateItems[monthIndex]);
	            month-=1;
	            var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
	            return formatedDate;
	}

</script>

