package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.MovilInfoDTO;
import com.gruposalinas.checklist.mappers.MovilInfoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class MovilInfoDAOImpl extends DefaultDAO implements MovilInfoDAO {

    private static Logger logger = LogManager.getLogger(MovilInfoDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodosInfo;
    DefaultJdbcCall jdbcInsertaInfo;
    DefaultJdbcCall jdbcActualizaInfo;
    DefaultJdbcCall jdbcEliminaInfo;

    public void init() {

        jdbcObtieneTodosInfo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMOVINF")
                .withProcedureName("SP_SEL_MOVINF")
                .returningResultSet("RCL_MOVINF", new MovilInfoRowMapper());

        jdbcInsertaInfo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMOVINF")
                .withProcedureName("SP_INS_MOVINF");

        jdbcActualizaInfo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMOVINF")
                .withProcedureName("SP_ACT_MOVINF");

        jdbcEliminaInfo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMOVINF")
                .withProcedureName("SP_DEL_MOVINF");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<MovilInfoDTO> obtieneInfoMovil(MovilInfoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<MovilInfoDTO> listaInfo = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_FECHA", bean.getFecha())
                .addValue("PA_SO", bean.getSo())
                .addValue("PA_MODELO", bean.getModelo())
                .addValue("PA_VERSION_APP", bean.getVersionApp())
                .addValue("PA_FABRICANTE", bean.getFabricante())
                .addValue("PA_NUMMOVIL", bean.getNumMovil())
                .addValue("PA_TIPOCON", bean.getTipoCon())
                .addValue("PA_IDENT", bean.getIdentificador());

        out = jdbcObtieneTodosInfo.execute(in);

        logger.info("Función ejecutada: checklist.PAADMMOVINF.SP_SEL_MOVINF");

        listaInfo = (List<MovilInfoDTO>) out.get("RCL_MOVINF");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener la Información de los dispositivos moviles");
        } else {
            return listaInfo;
        }

        return listaInfo;
    }

    @Override
    public boolean insertaInfoMovil(MovilInfoDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_FECHA", bean.getFecha())
                .addValue("PA_SO", bean.getSo())
                .addValue("PA_VERSION", bean.getVersion())
                .addValue("PA_VERSION_APP", bean.getVersionApp())
                .addValue("PA_MODELO", bean.getModelo())
                .addValue("PA_FABRICANTE", bean.getFabricante())
                .addValue("PA_NUMMOVIL", bean.getNumMovil())
                .addValue("PA_TIPOCON", bean.getTipoCon())
                .addValue("PA_IDENT", bean.getIdentificador())
                .addValue("PA_TOKEN", bean.getToken());

        logger.info(bean.getIdentificador());

        out = jdbcInsertaInfo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMMOVINF.SP_INS_MOVINF}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar la información del dispositivo movil");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaInfoMovil(MovilInfoDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_FECHA", bean.getFecha())
                .addValue("PA_SO", bean.getSo())
                .addValue("PA_VERSION", bean.getVersion())
                .addValue("PA_VERSION_APP", bean.getVersionApp())
                .addValue("PA_MODELO", bean.getModelo())
                .addValue("PA_FABRICANTE", bean.getFabricante())
                .addValue("PA_NUMMOVIL", bean.getNumMovil())
                .addValue("PA_TIPOCON", bean.getTipoCon())
                .addValue("PA_IDENT", bean.getIdentificador())
                .addValue("PA_FIIDMOVIL", bean.getIdMovil())
                .addValue("PA_TOKEN", bean.getToken());

        out = jdbcActualizaInfo.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PAADMMOVINF.SP_ACT_MOVINF}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar la información del dispositivo movil");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaInfoMovil(int idMovil) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDMOVIL", idMovil);

        out = jdbcEliminaInfo.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PAADMMOVINF.SP_DEL_MOVINF}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar la información del dispositivo movil");
        } else {
            return true;
        }

        return false;
    }

}
