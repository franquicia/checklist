package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.ConsultaDTO;

public class ConsultaBitacoraRowMapper implements RowMapper<ConsultaDTO> {

	public ConsultaDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConsultaDTO consultaDTO = new ConsultaDTO();
		
		consultaDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		consultaDTO.setIdCeco(rs.getInt("FCID_CECO"));
		consultaDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		consultaDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
		consultaDTO.setFechaInicio(rs.getString("FDINICIO"));
		consultaDTO.setFechaTermino(rs.getString("FDTERMINO"));
		
		return consultaDTO;
		
		
	}

}
