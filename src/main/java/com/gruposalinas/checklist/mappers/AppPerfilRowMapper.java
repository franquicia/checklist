package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.AppPerfilDTO;

public class AppPerfilRowMapper implements RowMapper<AppPerfilDTO>{


	public AppPerfilDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		AppPerfilDTO appPerfil = new AppPerfilDTO();
		
		appPerfil.setIdAppPerfil(rs.getInt("FIID_PERFILA"));
		appPerfil.setIdApp(rs.getInt("FIID_APP"));
		appPerfil.setIdUsuario(rs.getInt("FIID_USUARIO"));
		
		return appPerfil;
	}
	

}
