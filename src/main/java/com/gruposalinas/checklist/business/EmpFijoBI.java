package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.EmpFijoDAO;
import com.gruposalinas.checklist.domain.EmpFijoDTO;

public class EmpFijoBI {

	private static Logger logger = LogManager.getLogger(EmpFijoBI.class);

private List<EmpFijoDTO> listafila;
	
	@Autowired
	EmpFijoDAO empFijoDAO;
		
	
	public int inserta(EmpFijoDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = empFijoDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean elimina(String idEmpFijo){
		boolean respuesta = false;
		
		try {
			respuesta = empFijoDAO.elimina(idEmpFijo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<EmpFijoDTO> obtieneDatos( int idUsuario) {
		
		try {
			listafila =  empFijoDAO.obtieneDatos(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<EmpFijoDTO> obtieneInfo() {

		try {
			listafila = empFijoDAO.obtieneInfo();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de la tabla Filas");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	
	public boolean actualiza(EmpFijoDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = empFijoDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	

}