package com.gruposalinas.checklist.servicios.servidor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gruposalinas.checklist.business.ActivarCheckListBI;
import com.gruposalinas.checklist.business.ActorEdoHallaBI;
import com.gruposalinas.checklist.business.AdicionalesBI;
import com.gruposalinas.checklist.business.AdmHandbookBI;
import com.gruposalinas.checklist.business.AdmInformProtocolosBI;
import com.gruposalinas.checklist.business.AdminSupBI;
import com.gruposalinas.checklist.business.AltaGerenteBI;
import com.gruposalinas.checklist.business.AsignaExpBI;
import com.gruposalinas.checklist.business.AsignaTransfBI;
import com.gruposalinas.checklist.business.AsignacionesSupBI;
import com.gruposalinas.checklist.business.AsistenciaSupervisorBI;
import com.gruposalinas.checklist.business.BitacoraAdministradorBI;
import com.gruposalinas.checklist.business.BitacoraGralBI;
import com.gruposalinas.checklist.business.CanalBI;
import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.business.CheckInAsistenciaBI;
import com.gruposalinas.checklist.business.CheckinBI;
import com.gruposalinas.checklist.business.ChecklistNegocioBI;
import com.gruposalinas.checklist.business.ChecklistPreguntasComBI;
import com.gruposalinas.checklist.business.ChecksOfflineBI;
import com.gruposalinas.checklist.business.ComentariosRepAsgBI;
import com.gruposalinas.checklist.business.ConfiguraActorBI;
import com.gruposalinas.checklist.business.ConsultaAseguradorBI;
import com.gruposalinas.checklist.business.ConsultaAsistenciaBI;
import com.gruposalinas.checklist.business.ConsultaBI;
import com.gruposalinas.checklist.business.ConsultaCecoProtocoloBI;
import com.gruposalinas.checklist.business.ConsultaDashBoardBI;
import com.gruposalinas.checklist.business.ConsultaDistribucionBI;
import com.gruposalinas.checklist.business.ConsultaFechasBI;
import com.gruposalinas.checklist.business.ConsultaSupervisionBI;
import com.gruposalinas.checklist.business.ConsultaVisitasBI;
import com.gruposalinas.checklist.business.ConteoGenExpBI;
import com.gruposalinas.checklist.business.DatosInformeBI;
import com.gruposalinas.checklist.business.DetalleProtoBI;
import com.gruposalinas.checklist.business.DistribCecoBI;
import com.gruposalinas.checklist.business.EmpContacExtBI;
import com.gruposalinas.checklist.business.EmpFijoBI;
import com.gruposalinas.checklist.business.EstadosBI;
import com.gruposalinas.checklist.business.FaseTransfBI;
import com.gruposalinas.checklist.business.FiltrosCecoBI;
import com.gruposalinas.checklist.business.FirmaCheckBI;
import com.gruposalinas.checklist.business.FirmasCatalogoBI;
import com.gruposalinas.checklist.business.FragmentMenuBI;
import com.gruposalinas.checklist.business.GrupoBI;
import com.gruposalinas.checklist.business.HallagosMatrizBI;
import com.gruposalinas.checklist.business.HallazgosEviExpBI;
import com.gruposalinas.checklist.business.HallazgosExpBI;
import com.gruposalinas.checklist.business.HallazgosRepoBI;
import com.gruposalinas.checklist.business.HallazgosTransfBI;
import com.gruposalinas.checklist.business.HorarioBI;
import com.gruposalinas.checklist.business.MenusSupBI;
import com.gruposalinas.checklist.business.MovilInfoBI;
import com.gruposalinas.checklist.business.NegocioAdicionalBI;
import com.gruposalinas.checklist.business.NegocioBI;
import com.gruposalinas.checklist.business.NivelBI;
import com.gruposalinas.checklist.business.NotificacionBI;
import com.gruposalinas.checklist.business.ObtieneCecos;
import com.gruposalinas.checklist.business.OrdenGrupoBI;
import com.gruposalinas.checklist.business.PaisBI;
import com.gruposalinas.checklist.business.PaisNegocioBI;
import com.gruposalinas.checklist.business.ParametroBI;
import com.gruposalinas.checklist.business.PerfilBI;
import com.gruposalinas.checklist.business.PerfilUsuarioBI;
import com.gruposalinas.checklist.business.PerfilesSupBI;
import com.gruposalinas.checklist.business.PeriodoBI;
import com.gruposalinas.checklist.business.PosibleTipoPreguntaBI;
import com.gruposalinas.checklist.business.PosiblesBI;
import com.gruposalinas.checklist.business.ProtocoloBI;
import com.gruposalinas.checklist.business.ProtocoloByCheckBI;
import com.gruposalinas.checklist.business.ProyectFaseTransfBI;
import com.gruposalinas.checklist.business.PuestoBI;
import com.gruposalinas.checklist.business.RecursoBI;
import com.gruposalinas.checklist.business.RecursoPerfilBI;
import com.gruposalinas.checklist.business.RepMedicionBI;
import com.gruposalinas.checklist.business.RepoCheckPaperBI;
import com.gruposalinas.checklist.business.RepoFilExpBI;
import com.gruposalinas.checklist.business.ReporteChecksExpBI;
import com.gruposalinas.checklist.business.ReporteHallazgosBI;
import com.gruposalinas.checklist.business.ReporteImgBI;
import com.gruposalinas.checklist.business.ReporteMedicionBI;
import com.gruposalinas.checklist.business.RolesSupBI;
import com.gruposalinas.checklist.business.SoftNvoBI;
import com.gruposalinas.checklist.business.SoftOpeningBI;
import com.gruposalinas.checklist.business.SoporteExpBI;
import com.gruposalinas.checklist.business.SoporteHallazgosTransfBI;
import com.gruposalinas.checklist.business.SucUbicacionBI;
import com.gruposalinas.checklist.business.SucursalBI;
import com.gruposalinas.checklist.business.SucursalChecklistBI;
import com.gruposalinas.checklist.business.SucursalHistBI;
import com.gruposalinas.checklist.business.SucursalesCercanasBI;
import com.gruposalinas.checklist.business.SupervisorBI;
import com.gruposalinas.checklist.business.TareasBI;
import com.gruposalinas.checklist.business.TipoArchivoBI;
import com.gruposalinas.checklist.business.TransformacionBI;
import com.gruposalinas.checklist.business.Usuario_ABI;
import com.gruposalinas.checklist.business.VersionChecklistGralBI;
import com.gruposalinas.checklist.business.VersionExpanBI;
import com.gruposalinas.checklist.business.ZonaNegoBI;
import com.gruposalinas.checklist.domain.ActorEdoHallaDTO;
import com.gruposalinas.checklist.domain.AdicionalesDTO;
import com.gruposalinas.checklist.domain.AdmFirmasCatDTO;
import com.gruposalinas.checklist.domain.AdmHandbookDTO;
import com.gruposalinas.checklist.domain.AdmInformProtocolosDTO;
import com.gruposalinas.checklist.domain.AdminSupDTO;
import com.gruposalinas.checklist.domain.AltaGerenteDTO;
import com.gruposalinas.checklist.domain.AsignaExpDTO;
import com.gruposalinas.checklist.domain.AsignaTransfDTO;
import com.gruposalinas.checklist.domain.AsignacionesSupDTO;
import com.gruposalinas.checklist.domain.AsistenciaSupervisorDTO;
import com.gruposalinas.checklist.domain.BitacoraAdministradorDTO;
import com.gruposalinas.checklist.domain.BitacoraGralDTO;
import com.gruposalinas.checklist.domain.CanalDTO;
import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.CheckInAsistenciaDTO;
import com.gruposalinas.checklist.domain.CheckinDTO;
import com.gruposalinas.checklist.domain.ChecklistHallazgosRepoDTO;
import com.gruposalinas.checklist.domain.ChecklistNegocioDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.ComentariosRepAsgDTO;
import com.gruposalinas.checklist.domain.ConfiguraActorDTO;
import com.gruposalinas.checklist.domain.ConsultaAseguradorDTO;
import com.gruposalinas.checklist.domain.ConsultaAsistenciaDTO;
import com.gruposalinas.checklist.domain.ConsultaCecoProtocoloDTO;
import com.gruposalinas.checklist.domain.ConsultaDTO;
import com.gruposalinas.checklist.domain.ConsultaDashBoardDTO;
import com.gruposalinas.checklist.domain.ConsultaDistribucionDTO;
import com.gruposalinas.checklist.domain.ConsultaFechasDTO;
import com.gruposalinas.checklist.domain.ConsultaSupervisionDTO;
import com.gruposalinas.checklist.domain.ConsultaVisitasDTO;
import com.gruposalinas.checklist.domain.ConteoTablasDTO;
import com.gruposalinas.checklist.domain.DatosInformeDTO;
import com.gruposalinas.checklist.domain.DetalleProtoDTO;
import com.gruposalinas.checklist.domain.DistribCecoDTO;
import com.gruposalinas.checklist.domain.EmpContacExtDTO;
import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.EstadosDTO;
import com.gruposalinas.checklist.domain.FaseTransfDTO;
import com.gruposalinas.checklist.domain.FiltrosCecoDTO;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;
import com.gruposalinas.checklist.domain.FragmentMenuDTO;
import com.gruposalinas.checklist.domain.GrupoDTO;
import com.gruposalinas.checklist.domain.HallazgosEviExpDTO;
import com.gruposalinas.checklist.domain.HallazgosExpDTO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;
import com.gruposalinas.checklist.domain.HorarioDTO;
import com.gruposalinas.checklist.domain.MenusSupDTO;
import com.gruposalinas.checklist.domain.MovilInfoDTO;
import com.gruposalinas.checklist.domain.NegocioAdicionalDTO;
import com.gruposalinas.checklist.domain.NegocioDTO;
import com.gruposalinas.checklist.domain.NivelDTO;
import com.gruposalinas.checklist.domain.OrdenGrupoDTO;
import com.gruposalinas.checklist.domain.PaisDTO;
import com.gruposalinas.checklist.domain.PaisNegocioDTO;
import com.gruposalinas.checklist.domain.ParametroDTO;
import com.gruposalinas.checklist.domain.PerfilDTO;
import com.gruposalinas.checklist.domain.PerfilUsuarioDTO;
import com.gruposalinas.checklist.domain.PerfilesSupDTO;
import com.gruposalinas.checklist.domain.PeriodoDTO;
import com.gruposalinas.checklist.domain.PosiblesDTO;
import com.gruposalinas.checklist.domain.PosiblesTipoPreguntaDTO;
import com.gruposalinas.checklist.domain.PreguntaCheckDTO;
import com.gruposalinas.checklist.domain.ProgramacionMttoDTO;
import com.gruposalinas.checklist.domain.ProtocoloByCheckDTO;
import com.gruposalinas.checklist.domain.ProtocoloDTO;
import com.gruposalinas.checklist.domain.ProyectFaseTransfDTO;
import com.gruposalinas.checklist.domain.PuestoDTO;
import com.gruposalinas.checklist.domain.RecursoDTO;
import com.gruposalinas.checklist.domain.RecursoPerfilDTO;
import com.gruposalinas.checklist.domain.RepMedicionDTO;
import com.gruposalinas.checklist.domain.RepoCheckPaperDTO;
import com.gruposalinas.checklist.domain.RepoFilExpDTO;
import com.gruposalinas.checklist.domain.ReporteChecksExpDTO;
import com.gruposalinas.checklist.domain.ReporteHallazgosDTO;
import com.gruposalinas.checklist.domain.ReporteImgDTO;
import com.gruposalinas.checklist.domain.ReporteMedPregDTO;
import com.gruposalinas.checklist.domain.RolesSupDTO;
import com.gruposalinas.checklist.domain.SoftNvoDTO;
import com.gruposalinas.checklist.domain.SoftOpeningDTO;
import com.gruposalinas.checklist.domain.SoporteExpDTO;
import com.gruposalinas.checklist.domain.SucUbicacionDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;
import com.gruposalinas.checklist.domain.SucursalDTO;
import com.gruposalinas.checklist.domain.SucursalHistDTO;
import com.gruposalinas.checklist.domain.SucursalesCercanasDTO;
import com.gruposalinas.checklist.domain.SupervisorDTO;
import com.gruposalinas.checklist.domain.TareaDTO;
import com.gruposalinas.checklist.domain.TipoArchivoDTO;
import com.gruposalinas.checklist.domain.Usuario_ADTO;
import com.gruposalinas.checklist.domain.VersionChecklistGralDTO;
import com.gruposalinas.checklist.domain.VersionExpanDTO;
import com.gruposalinas.checklist.domain.ZonaNegoExpDTO;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@Controller
@RequestMapping("/consultaCatalogosService")
public class ConsultaCatalogosService {

    @Autowired
    CanalBI canalbi;
    @Autowired
    PerfilBI perfilbi;
    @Autowired
    PuestoBI puestobi;
    @Autowired
    TipoArchivoBI tipoarchivobi;
    @Autowired
    CecoBI cecobi;
    @Autowired
    SucursalBI sucursalbi;
    @Autowired
    Usuario_ABI usuarioabi;
    @Autowired
    PaisBI paisbi;
    @Autowired
    HorarioBI horariobi;
    @Autowired
    ParametroBI parametrobi;
    @Autowired
    NegocioBI negociobi;
    @Autowired
    NivelBI nivelbi;
    @Autowired
    PerfilUsuarioBI perfilusuariobi;
    @Autowired
    RecursoPerfilBI recursoperfilbi;
    @Autowired
    RecursoBI recursobi;
    @Autowired
    PosiblesBI posiblesbi;
    @Autowired
    PosibleTipoPreguntaBI posibletipopreguntabi;
    @Autowired
    FiltrosCecoBI filtrosCecosBI;
    @Autowired
    ChecklistNegocioBI checklistNegocioBI;
    @Autowired
    NegocioAdicionalBI adicionalBI;
    @Autowired
    PaisNegocioBI paisNegocioBI;
    @Autowired
    SucursalesCercanasBI sucursalesCercanasBI;
    @Autowired
    NotificacionBI notificacionBI;
    @Autowired
    MovilInfoBI movilInfoBI;
    @Autowired
    GrupoBI grupoBI;
    @Autowired
    OrdenGrupoBI ordenGrupoBI;
    @Autowired
    PeriodoBI periodoBI;
    @Autowired
    TareasBI tareasBI;
    @Autowired
    SucursalHistBI sucHistBI;
    @Autowired
    EmpFijoBI empFijoBI;
    @Autowired
    EstadosBI estadosBI;
    @Autowired
    RepoCheckPaperBI repoCheckPaperBI;
    @Autowired
    BitacoraGralBI bitacoraGralBI;
    @Autowired
    SucursalChecklistBI sucursalChecklistBI;
    @Autowired
    FirmaCheckBI firmaCheckBI;
    @Autowired
    ReporteChecksExpBI reporteChecksExpBI;
    @Autowired
    SucUbicacionBI sucUbicacionBI;
    @Autowired
    ChecklistPreguntasComBI checklistPreguntasComBI;
    @Autowired
    CheckinBI checkinBI;
    @Autowired
    ConsultaBI consultaBI;
    @Autowired
    SoftOpeningBI softOpeningBI;
    @Autowired
    AsignaExpBI asignaExpBI;
    @Autowired
    VersionExpanBI versionExpanBI;
    @Autowired
    DetalleProtoBI detalleProtoBI;
    @Autowired
    VersionChecklistGralBI versionChecklistGralBI;
    @Autowired
    SoporteExpBI soporteExpBI;
    @Autowired
    RepoFilExpBI repoFilExpBI;
    @Autowired
    ActivarCheckListBI activaCheckListBI;
    @Autowired
    EmpContacExtBI empContacExtBI;
    @Autowired
    ConsultaDashBoardBI consultaDashBoardBI;
    @Autowired
    ConsultaAsistenciaBI consultaAsistenciaBI;
    @Autowired
    ConsultaFechasBI consultaFechasBI;
    @Autowired
    ConsultaDistribucionBI consultaDistribucionBI;
    @Autowired
    ConsultaVisitasBI consultaVisitasBI;
    @Autowired
    BitacoraAdministradorBI bitacoraAdministradorBI;
    @Autowired
    SupervisorBI supervisorBI;
    @Autowired
    AsistenciaSupervisorBI asistenciaSupervisorBI;
    @Autowired
    ConsultaSupervisionBI consultaSupervisionBI;
    @Autowired
    HallazgosExpBI hallazgosExpBI;
    @Autowired
    HallazgosEviExpBI hallazgosEviExpBI;
    @Autowired
    HallazgosRepoBI hallazgosRepoBI;
    @Autowired
    DistribCecoBI distribCecoBI;
    @Autowired
    AdmHandbookBI admHandbookBI;
    @Autowired
    ProtocoloBI protocoloBI;
    @Autowired
    ProtocoloByCheckBI protocoloByCheckBI;
    @Autowired
    ZonaNegoBI zonaNegoBI;
    @Autowired
    ReporteImgBI reporteImgBI;
    @Autowired
    ReporteMedicionBI reporteMedicionBI;
    @Autowired
    ConsultaCecoProtocoloBI consultaCecoProtocoloBI;
    @Autowired
    ConsultaAseguradorBI consultaAseguradorBI;
    @Autowired
    RepMedicionBI repMedicionBI;
    @Autowired
    AsignacionesSupBI asignacionesSupBI;
    @Autowired
    PerfilesSupBI perfilesSupBI;
    @Autowired
    MenusSupBI menusSupBI;
    @Autowired
    RolesSupBI rolesSupBI;
    @Autowired
    AdminSupBI adminSupBI;
    @Autowired
    AltaGerenteBI altaGerenteBI;
    @Autowired
    AdicionalesBI adicionalesBI;
    @Autowired
    ConteoGenExpBI conteoGenExpBI;
    @Autowired
    HallazgosTransfBI hallazgosTransfBI;
    @Autowired
    TransformacionBI transformacionBI;
    @Autowired
    FaseTransfBI faseTransfBI;

    @Autowired
    ComentariosRepAsgBI comentariosRepAsgBI;
    @Autowired
    AdmInformProtocolosBI admInformProtocolosBI;

    @Autowired
    ProyectFaseTransfBI proyectFaseTransfBI;
    @Autowired
    AsignaTransfBI asignaTransfBI;
    @Autowired
    FirmasCatalogoBI firmasCatalogoBI;
    @Autowired
    FragmentMenuBI fragmentMenuBI;
    @Autowired
    SoftNvoBI softNvoBI;
    @Autowired
    ActorEdoHallaBI actorEdoHallaBI;
    @Autowired
    ConfiguraActorBI configuraActorBI;
    @Autowired
    ObtieneCecos obtieneCecos;
    @Autowired
    ChecksOfflineBI checksOfflineBI;
    @Autowired
    HallagosMatrizBI hallagosMatrizBI;
    @Autowired
    SoporteHallazgosTransfBI soporteHallazgosTransfBI;
    @Autowired
    ReporteHallazgosBI reporteHallazgosBI;
    @Autowired
    CheckInAsistenciaBI checkInAsistenciaBI;
    @Autowired
    DatosInformeBI datosInformeBI;
    private static final Logger logger = LogManager.getLogger(ConsultaCatalogosService.class);

    // http://localhost:8080/checklist/consultaCatalogosService/getCanales.json?activo=<?>
    @RequestMapping(value = "/getCanales", method = RequestMethod.GET)
    public ModelAndView getCanales(HttpServletRequest request, HttpServletResponse response) {
        try {
            String activo = request.getParameter("activo");

            List<CanalDTO> lista = canalbi.buscaCanales(Integer.parseInt(activo));

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA CANALES");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("Paso Algo al consultar canales activos 147 - 156");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPerfiles.json
    @RequestMapping(value = "/getPerfiles", method = RequestMethod.GET)
    public ModelAndView getPerfiles(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<PerfilDTO> lista = perfilbi.obtienePerfil();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA PERFILES");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPerfiles()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPuestos.json
    @RequestMapping(value = "/getPuestos", method = RequestMethod.GET)
    public ModelAndView getPuestos(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<PuestoDTO> lista = puestobi.obtienePuesto();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "PUESTOS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPuestos()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getTiposArchivo.json
    @RequestMapping(value = "/getTiposArchivo", method = RequestMethod.GET)
    public ModelAndView getTiposArchivo(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<TipoArchivoDTO> lista = tipoarchivobi.obtieneTipoArchivo();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA TIPOS ARCHIVO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getTiposArchivo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCecos.json?activo=<?>
    @RequestMapping(value = "/getCecos", method = RequestMethod.GET)
    public ModelAndView getCecos(HttpServletRequest request, HttpServletResponse response) {
        try {
            String activo = request.getParameter("activo");

            List<CecoDTO> lista = cecobi.buscaCecos(Integer.parseInt(activo));

            // ModelAndView mv = new ModelAndView("muestraServicios");
            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

            mv.addObject("tipo", "LISTA CECOS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getCecos()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getSucursales.json
    @RequestMapping(value = "/getSucursales", method = RequestMethod.GET)
    public ModelAndView getSucursales2(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<SucursalDTO> lista = sucursalbi.obtieneSucursal();

            // ModelAndView mv = new ModelAndView("muestraServicios");
            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

            mv.addObject("tipo", "LISTA SUCURSALES");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info("getSucursales()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getUsuarios.json
    @RequestMapping(value = "/getUsuarios", method = RequestMethod.GET)
    public ModelAndView getUsuarios(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<Usuario_ADTO> lista = usuarioabi.obtieneUsuario();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA USUARIOS A");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getUsuarios()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPaises.json
    @RequestMapping(value = "/getPaises", method = RequestMethod.GET)
    public ModelAndView getPaises(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<PaisDTO> lista = paisbi.obtienePais();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA PAISES");
            mv.addObject("res", lista);
            return mv;

        } catch (Exception e) {
            logger.info("getPaises()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getHorarios.json
    @RequestMapping(value = "/getHorarios", method = RequestMethod.GET)
    public ModelAndView getHorarios(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<HorarioDTO> lista = horariobi.obtieneHorario();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA HORARIOS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getHorarios()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getParametros.json
    @RequestMapping(value = "/getParametros", method = RequestMethod.GET)
    public ModelAndView getParametros(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<ParametroDTO> lista = parametrobi.obtieneParametro();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA PARAMETROS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getParametros()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getNegocios.json
    @RequestMapping(value = "/getNegocios", method = RequestMethod.GET)
    public ModelAndView getNegocios(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<NegocioDTO> lista = negociobi.obtieneNegocio();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA NEGOCIOS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getNegocios()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getNiveles.json
    @RequestMapping(value = "/getNiveles", method = RequestMethod.GET)
    public ModelAndView getNiveles(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<NivelDTO> lista = nivelbi.obtieneNivel();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA NIVELES");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getNiveles()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getSucursalById.json?idSuc=<?>
    @RequestMapping(value = "/getSucursalById", method = RequestMethod.GET)
    public ModelAndView getSucursalById(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idSuc = request.getParameter("idSuc");

            List<SucursalDTO> lista = sucursalbi.obtieneSucursal(idSuc);

            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
            mv.addObject("tipo", "SUCURSAL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getSucursalById()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getSucursalByIdGCC.json?idSuc=<?>
    @RequestMapping(value = "/getSucursalByIdGCC", method = RequestMethod.GET)
    public ModelAndView getSucursalByIdGCC(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idSuc = request.getParameter("idSuc");

            List<SucursalDTO> lista = sucursalbi.obtieneSucursalGCC(idSuc);

            // ModelAndView mv = new ModelAndView("muestraServicios");
            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

            mv.addObject("tipo", "SUCURSAL GCC");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getSucursalByIdGCC()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getSucursalPaso.json?idSuc=<?>
    @RequestMapping(value = "/getSucursalPaso", method = RequestMethod.GET)
    public ModelAndView getSucursalPaso(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idSuc = request.getParameter("idSuc");

            List<SucursalDTO> lista = sucursalbi.obtieneSucursalPaso(idSuc);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "SUCURSAL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getSucursalPaso()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCecoById.json?idCeco=<?>
    @RequestMapping(value = "/getCecoById", method = RequestMethod.GET)
    public ModelAndView getCecoById(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCeco = request.getParameter("idCeco");

            List<CecoDTO> lista = cecobi.buscaCeco(Integer.parseInt(idCeco));

            // ModelAndView mv = new ModelAndView("muestraServicios");
            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

            mv.addObject("tipo", "OBTIENE CECO POR IDCECO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getCecoById()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getUsuarioById.json?idUsu=<?>
    @RequestMapping(value = "/getUsuarioById", method = RequestMethod.GET)
    public ModelAndView getUsuarioById(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsu = request.getParameter("idUsu");

            List<Usuario_ADTO> lista = usuarioabi.obtieneUsuario(Integer.parseInt(idUsu));

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "USUARIO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getUsuarioById()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCargaSucursal.json
    @RequestMapping(value = "/getCargaSucursal", method = RequestMethod.GET)
    public ModelAndView getCargaSucursal(HttpServletRequest request, HttpServletResponse response) {
        try {
            boolean res = sucursalbi.cargaSucursales();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "SUCURSALES Cargadas Correctamente -->");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info("getCargaSucursal()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCargaUsuariosPaso.json
    @RequestMapping(value = "/getCargaUsuariosPaso", method = RequestMethod.GET)
    public ModelAndView getCargaUsuariosPaso(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<Usuario_ADTO> res = usuarioabi.obtieneUsuarioPaso();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "USUARIOS PASO");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info("getCargaUsuariosPaso()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCargaUsuariosPasoByParams.json?idUsu=<?>&idPuesto=<?>&idCeco=<?>
    @RequestMapping(value = "/getCargaUsuariosPasoByParams", method = RequestMethod.GET)
    public ModelAndView getCargaUsuariosPasoByParams(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsu = request.getParameter("idUsu");
            String idPuesto = request.getParameter("idPuesto");
            String idCeco = request.getParameter("idCeco");

            List<Usuario_ADTO> res = usuarioabi.obtieneUsuarioPaso(idUsu, idPuesto, idCeco);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "USUARIOS PASO PARAMS");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info("getCargaUsuariosPasoByParams()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCecosPaso.json
    @RequestMapping(value = "/getCecosPaso", method = RequestMethod.GET)
    public ModelAndView getCecosPaso(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<CecoDTO> res = cecobi.buscaCecosPaso();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CECO PASO");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info("getCecosPaso()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCecosPasoByParams.json?ceco=<?>&cecoPadre=<?>&desc=<?>
    @RequestMapping(value = "/getCecosPasoByParams", method = RequestMethod.GET)
    public ModelAndView getCecosPasoByParams(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String ceco = request.getParameter("ceco");
            String cecoPadre = request.getParameter("cecoPadre");
            String descripcion = new String(request.getParameter("desc").getBytes("ISO-8859-1"), "UTF-8");

            List<CecoDTO> res = cecobi.buscaCecoPaso(ceco, cecoPadre, descripcion);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CECO PASO PARAMS");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info("getCecosPasoByParams()");
            return null;
        }

    }

    // http://localhost:8080/checklist/consultaCatalogosService/getRecursosPerfil.json?idRecurso=<?>&idPerfil=<?>
    @RequestMapping(value = "/getRecursosPerfil", method = RequestMethod.GET)
    public ModelAndView getRecursosPerfil(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idRecurso = request.getParameter("idRecurso");
            String idPerfil = request.getParameter("idPerfil");

            List<RecursoPerfilDTO> res = recursoperfilbi.buscaRecursoP(idRecurso, idPerfil);
            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "GET RECURSOS PERFIL");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info("getRecursosPerfil()");
            return null;
        }

    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPerfilUsuarios.json?idUsuario=<?>&idPerfil=<?>
    @RequestMapping(value = "/getPerfilUsuarios", method = RequestMethod.GET)
    public ModelAndView getPerfilUsuarios(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");

            List<PerfilUsuarioDTO> res = perfilusuariobi.obtienePerfiles(idUsuario, idPerfil);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "PERFILES USUARIO");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info("getPerfilUsuarios()");
            return null;
        }

    }

    // http://localhost:8080/checklist/consultaCatalogosService/getRecursos.json?idRecurso=<?>
    @RequestMapping(value = "/getRecursos", method = RequestMethod.GET)
    public ModelAndView getRecursos(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idRecurso = request.getParameter("idRecurso");

            List<RecursoDTO> res = recursobi.buscaRecurso(idRecurso);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "RECURSOS");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info("getRecursos()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPosibles.json
    @RequestMapping(value = "/getPosibles", method = RequestMethod.GET)
    public ModelAndView getPosibles(HttpServletRequest request, HttpServletResponse response) {

        try {

            List<PosiblesDTO> res = posiblesbi.buscaPosible();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "POSIBLES ");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info("getPosibles()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPosibleId.json?idPosible=<?>
    @RequestMapping(value = "/getPosibleId", method = RequestMethod.GET)
    public ModelAndView getPosibleId(HttpServletRequest request, HttpServletResponse response) {

        try {

            String idPosS = request.getParameter("idPosible");

            int idPos = 0;

            if (idPosS != null) {
                idPos = Integer.parseInt(idPosS);
            }

            List<PosiblesDTO> res = posiblesbi.buscaPosibleId(idPos);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "POSIBLES ");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info("getPosibleId()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPosibleTipoPregunta.json?idTipoPregunta=<?>
    @RequestMapping(value = "/getPosibleTipoPregunta", method = RequestMethod.GET)
    public ModelAndView getPosibleTipoPregunta(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idTipoPregunta = request.getParameter("idTipoPregunta");

            if (idTipoPregunta.equals("")) {
                idTipoPregunta = null;
            }

            List<PosiblesTipoPreguntaDTO> res = posibletipopreguntabi.obtienePosiblesRespuestas(idTipoPregunta);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "POSIBLES TIPO PREGUNTA");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info("getPosibleTipoPregunta()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPosibleTipoPreguntaService.json?idTipoPregunta=<?>
    @RequestMapping(value = "/getPosibleTipoPreguntaService", method = RequestMethod.GET)
    public @ResponseBody
    List<PosiblesTipoPreguntaDTO> getPosibleTipoPreguntaService(HttpServletRequest request,
            HttpServletResponse response) {

        try {
            String idTipoPregunta = request.getParameter("idTipoPregunta");

            if (idTipoPregunta.equals("")) {
                idTipoPregunta = null;
            }

            List<PosiblesTipoPreguntaDTO> res = posibletipopreguntabi.obtienePosiblesRespuestas(idTipoPregunta);

            return res;
        } catch (Exception e) {
            logger.info("getPosibleTipoPreguntaService()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getFiltrosCecos.json?idUsuario=<?>
    @RequestMapping(value = "/getFiltrosCecos", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getFiltrosCecos(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idUsuario = request.getParameter("idUsuario");

            Map<String, Object> res = filtrosCecosBI.obtieneFiltros(Integer.parseInt(idUsuario));

            return res;
        } catch (Exception e) {
            logger.info("getFiltrosCecosv()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCecosCombo.json?idCeco=<?>&tipoBusqueda=<?>
    @RequestMapping(value = "/getCecosCombo", method = RequestMethod.GET)
    public @ResponseBody
    List<FiltrosCecoDTO> getCecosCombo(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idCeco = request.getParameter("idCeco");
            String tipoBusqueda = request.getParameter("tipoBusqueda");

            List<FiltrosCecoDTO> res = filtrosCecosBI.obtieneCecos(Integer.parseInt(idCeco),
                    Integer.parseInt(tipoBusqueda));

            return res;
        } catch (Exception e) {
            logger.info("getCecosCombo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPaisesCombo.json?idNegocio=<?>
    @RequestMapping(value = "/getPaisesCombo", method = RequestMethod.GET)
    public @ResponseBody
    List<PaisDTO> getPaisesCombo(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idNegocio = request.getParameter("idNegocio");

            List<PaisDTO> res = filtrosCecosBI.obtienePaises(Integer.parseInt(idNegocio));

            return res;
        } catch (Exception e) {
            logger.info("getPaisesCombo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getTerritoriosCombo.json?idPais=<?>&idNegocio=<?>
    @RequestMapping(value = "/getTerritoriosCombo", method = RequestMethod.GET)
    public @ResponseBody
    List<FiltrosCecoDTO> getTerritoriosCombo(HttpServletRequest request,
            HttpServletResponse response) {

        try {
            String idPais = request.getParameter("idPais");
            String idNegocio = request.getParameter("idNegocio");

            List<FiltrosCecoDTO> res = filtrosCecosBI.obtieneTerritorios(Integer.parseInt(idPais),
                    Integer.parseInt(idNegocio));

            return res;
        } catch (Exception e) {
            logger.info("getTerritoriosCombo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getChecklistNegocio.json?idChecklist=<?>&idNegocio=<?>
    @RequestMapping(value = "/getChecklistNegocio", method = RequestMethod.GET)
    public ModelAndView getChecklistNegocio(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idChecklist = request.getParameter("idChecklist");
            String idNegocio = request.getParameter("idNegocio");

            List<ChecklistNegocioDTO> lista = checklistNegocioBI.obtieneChecklistNegocio(idChecklist, idNegocio);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA CHECKLIST NEGOCIO");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info("getChecklistNegocio()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getNegocioAdicional.json?idUsuario=<?>&idNegocio=<?>
    @RequestMapping(value = "/getNegocioAdicional", method = RequestMethod.GET)
    public ModelAndView getNegocioAdicional(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idUsuario = request.getParameter("idUsuario");
            String idNegocio = request.getParameter("idNegocio");

            List<NegocioAdicionalDTO> lista = adicionalBI.obtieneNegocioAdicional(idUsuario, idNegocio);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA NEGOCIO ADICIONAL");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info("getNegocioAdicional()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getNegocioPais.json?idPais=<?>&idNegocio=<?>
    @RequestMapping(value = "/getNegocioPais", method = RequestMethod.GET)
    public ModelAndView getNegocioPais(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idPais = request.getParameter("idPais");
            String idNegocio = request.getParameter("idNegocio");

            List<PaisNegocioDTO> lista = paisNegocioBI.obtienePaisNegocio(idNegocio, idPais);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA PAIS NEGOCIO");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info("getNegocioPais()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getSucursalesCercanas.json?latitud=<?>&longitud=<?>
    @RequestMapping(value = "/getSucursalesCercanas", method = RequestMethod.GET)
    public @ResponseBody
    List<SucursalesCercanasDTO> getSucursalesCercanas(HttpServletRequest request,
            HttpServletResponse response) {

        try {
            String latitud = request.getParameter("latitud");
            String longitud = request.getParameter("longitud");

            List<SucursalesCercanasDTO> lista = sucursalesCercanasBI.obtieneSucursales(latitud, longitud);

            return lista;
        } catch (Exception e) {
            logger.info("getSucursalesCercanas()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getNotificacion.json?idUsuario=<?>
    @RequestMapping(value = "/getNotificacion", method = RequestMethod.GET)
    public ModelAndView getNotificacion(HttpServletRequest request, HttpServletResponse response) {

        Map<String, Object> listaNotificacion = null;
        try {
            String idUsuario = request.getParameter("idUsuario");
            Gson g = new Gson();

            listaNotificacion = notificacionBI.NotificacionPorcentaje(Integer.parseInt(idUsuario));

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "NOTIFICACION REGIONAL");
            mv.addObject("res", g.toJson(listaNotificacion));
            // mv.addObject("res", "SE HA RECIBIDO NOTIFICACION");

            logger.info("JSON: " + g.toJson(listaNotificacion));

            return mv;
        } catch (Exception e) {
            logger.info("getNotificacion()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getUsuaNotificacion.json?so=<?>
    @SuppressWarnings("unused")
    @RequestMapping(value = "/getUsuaNotificacion", method = RequestMethod.GET)
    public ModelAndView getUsuaNotificacion(HttpServletRequest request, HttpServletResponse response) {

        List<MovilInfoDTO> listaNotificacion = null;
        try {
            String so = request.getParameter("so");
            Gson g = new Gson();

            listaNotificacion = notificacionBI.obtieneUsuariosNoti(so);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA NOTIFICACION");
            mv.addObject("res", listaNotificacion);

            return mv;
        } catch (Exception e) {
            logger.info("getUsuaNotificacion()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getMovilInfo.json?idUsuario=<?>
    @RequestMapping(value = "/getMovilInfo", method = RequestMethod.GET)
    public ModelAndView getMovilInfo(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idUsuario = request.getParameter("idUsuario");

            MovilInfoDTO movil = new MovilInfoDTO();
            movil.setIdUsuario(Integer.parseInt(idUsuario));

            List<MovilInfoDTO> lista = movilInfoBI.obtieneInfoMovil(movil);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA MOVIL");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info("getMovilInfo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getGrupo.json?idGrupo=<?>
    @RequestMapping(value = "/getGrupo", method = RequestMethod.GET)
    public ModelAndView getGrupo(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idGrupo = request.getParameter("idGrupo");

            List<GrupoDTO> lista = grupoBI.obtieneGrupo(idGrupo);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA GRUPO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getGrupo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getOrdenGrupo.json?=idOrdenGrupo=<?>&idChecklist=<?>&idGrupo=<?>
    @RequestMapping(value = "/getOrdenGrupo", method = RequestMethod.GET)
    public ModelAndView getOrdenGrupo(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idOrdenGrupo = request.getParameter("idOrdenGrupo");
            String idChecklist = request.getParameter("idChecklist");
            String idGrupo = request.getParameter("idGrupo");

            List<OrdenGrupoDTO> lista = ordenGrupoBI.obtieneOrdenGrupo(idOrdenGrupo, idChecklist, idGrupo);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA ORDEN GRUPO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getOrdenGrupo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPeriodo.json?idPeriodo=<?>
    @RequestMapping(value = "/getPeriodo", method = RequestMethod.GET)
    public ModelAndView getPeriodo(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idPeriodo = request.getParameter("idPeriodo");

            List<PeriodoDTO> lista = periodoBI.obtienePeriodo(idPeriodo);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA PERIODO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPeriodo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getObtieneTareas.json
    @RequestMapping(value = "/getObtieneTareas", method = RequestMethod.GET)
    public ModelAndView getObtieneTareas(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<TareaDTO> listaTareas = tareasBI.obtieneTareas();

            logger.info("listaTareas " + listaTareas);
            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA DE TAREAS");
            mv.addObject("res", listaTareas);
            return mv;
        } catch (Exception e) {
            logger.info("getObtieneTareas()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getSucursalHist.json?idHistSuc=<?>&idNumSuc=<?>
    @RequestMapping(value = "/getSucursalHist", method = RequestMethod.GET)
    public ModelAndView getSucursalHist(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idHistSuc = request.getParameter("idHistSuc");
            String idNumSuc = request.getParameter("idNumSuc");
            // String idNumSuc = "0100";

            List<SucursalHistDTO> lista = sucHistBI.obtieneSucursal(idHistSuc, idNumSuc);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "HISTORICO SUCURSAL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getSucursalHist()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCecosGCC.json
    @RequestMapping(value = "/getCecosGCC", method = RequestMethod.GET)
    public ModelAndView getCecosGCC(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<CecoDTO> lista = cecobi.buscaCecosGCC();

            // ModelAndView mv = new ModelAndView("muestraServicios");
            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

            mv.addObject("tipo", "LISTA CECOS GCC");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getSucursalHist()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getObtieneEmpFijo.json?idUsuario=<?>
    @RequestMapping(value = "/getObtieneEmpFijo", method = RequestMethod.GET)
    public ModelAndView getObtieneEmpFijo(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idusu = 0;
            if (request.getParameter("idUsuario") != null) {
                idusu = Integer.parseInt(request.getParameter("idUsuario"));
            }
            List<EmpFijoDTO> lista = empFijoBI.obtieneDatos(idusu);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Fijo");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getObtieneEmpFijo()");
            return null;
        }

    }

    // http://localhost:8080/checklist/consultaCatalogosService/getObtieneEmpleadosFijos.json
    @RequestMapping(value = "/getObtieneEmpleadosFijos", method = RequestMethod.GET)
    public ModelAndView getObtieneEmpleadosFijos(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<EmpFijoDTO> lista = empFijoBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Fijo");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getObtieneEmpleadosFijos()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getObtieneEstado.json?idEstado=<?>
    @RequestMapping(value = "/getObtieneEstado", method = RequestMethod.GET)
    public ModelAndView getObtieneEstado(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idesta = 0;
            if (request.getParameter("idEstado") != null) {
                idesta = Integer.parseInt(request.getParameter("idEstado"));
            }
            List<EstadosDTO> lista = estadosBI.obtieneDatos(idesta);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Estados");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getObtieneEstado()");
            return null;
        }

    }

    // http://localhost:8080/checklist/consultaCatalogosService/getObtieneEstados.json
    @RequestMapping(value = "/getObtieneEstados", method = RequestMethod.GET)
    public ModelAndView getObtieneEstados(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<EstadosDTO> listaEstados = estadosBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(listaEstados);
            mv.addObject("tipo", "Estados");
            mv.addObject("res", listaEstados);
            return mv;
        } catch (Exception e) {
            logger.info("getObtieneEstados()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getObtieneDetalleEstados.json
    @RequestMapping(value = "/getObtieneDetalleEstados", method = RequestMethod.GET)
    public ModelAndView getObtieneDetalleEstados(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<EstadosDTO> lista = estadosBI.obtieneDetalle();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Estados");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getObtieneDetalleEstados()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getReportePaperCeco.json?idEstado=<?>
    @RequestMapping(value = "/getReportePaperCeco", method = RequestMethod.GET)
    public ModelAndView getReportePaperCeco(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idCheclist = 313;

            String ceco = request.getParameter("ceco");
            idCheclist = Integer.parseInt(request.getParameter("idCheclist"));

            List<RepoCheckPaperDTO> lista = repoCheckPaperBI.obtieneDatos(ceco, idCheclist);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "PAPERLESSCHECK");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getReportePaperCeco()");
            return null;
        }

    }

    // http://localhost:8080/checklist/consultaCatalogosService/getReportePaperUsua.json?idEstado=<?>
    @RequestMapping(value = "/getReportePaperUsua", method = RequestMethod.GET)
    public ModelAndView getReportePaperUsua(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idCheclist = 313;
            int idUsuario = 0;

            idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
            idCheclist = Integer.parseInt(request.getParameter("idCheclist"));

            List<RepoCheckPaperDTO> lista = repoCheckPaperBI.obtieneInfo(idUsuario, idCheclist);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "PAPERLESSCHECK");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getReportePaperUsua()");
            return null;
        }

    }

    // http://localhost:8080/checklist/consultaCatalogosService/getReportePaperCecoPreguntas.json?ceco=<?>idCheclist=<?>
    @RequestMapping(value = "/getReportePaperCecoPreguntas", method = RequestMethod.GET)
    public ModelAndView getReportePaperCecoPreguntas(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idCheclist = 313;

            String ceco = request.getParameter("ceco");
            idCheclist = Integer.parseInt(request.getParameter("idCheclist"));

            List<RepoCheckPaperDTO> lista = repoCheckPaperBI.obtieneDatosPreguntas(ceco, idCheclist);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "PAPERLESSCHECK");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getReportePaperCecoPreguntas()");
            return null;
        }

    }

    // http://localhost:8080/checklist/consultaCatalogosService/getReportePaperUsuaPreg.json?idUsuario=<?>idCheclist=<?>
    @RequestMapping(value = "/getReportePaperUsuaPreg", method = RequestMethod.GET)
    public ModelAndView getReportePaperUsuaPreg(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idCheclist = 313;
            int idUsuario = 0;

            idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
            idCheclist = Integer.parseInt(request.getParameter("idCheclist"));

            List<RepoCheckPaperDTO> lista = repoCheckPaperBI.obtieneInfoPreguntas(idUsuario, idCheclist);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "PAPERLESSCHECK");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getReportePaperUsuaPre()");
            return null;
        }

    }

    // http://localhost:8080/checklist/consultaCatalogosService/getBitaGral.json?idBitaGral=<?>
    @RequestMapping(value = "/getBitaGral", method = RequestMethod.GET)
    public ModelAndView getBitaGral(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idBitaGral = Integer.parseInt(request.getParameter("idBitaGral"));

            List<BitacoraGralDTO> lista = bitacoraGralBI.obtieneDatos(idBitaGral);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "BITAGRAL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getBitaGral()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getBitaGralDetalle.json?
    @RequestMapping(value = "/getBitaGralDetalle", method = RequestMethod.GET)
    public ModelAndView getBitaGralDetalle(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<BitacoraGralDTO> lista = bitacoraGralBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "BITAGRAL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getBitaGralDetalle()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getBitaGralh.json?idBitaGral=<?>
    @RequestMapping(value = "/getBitaGralh", method = RequestMethod.GET)
    public ModelAndView getBitaGralh(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idBitaGral = Integer.parseInt(request.getParameter("idBitaGral"));

            List<BitacoraGralDTO> lista = bitacoraGralBI.obtieneDatosH(idBitaGral);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "BITAGRAL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getBitaGralh()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getBitaGralDetalleH.json?
    @RequestMapping(value = "/getBitaGralDetalleH", method = RequestMethod.GET)
    public ModelAndView getBitaGralDetalleH(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<BitacoraGralDTO> lista = bitacoraGralBI.obtieneInfoH();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "BITAGRAL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getBitaGralDetalleH()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getSucChecklist.json?
    @RequestMapping(value = "/getSucChecklist", method = RequestMethod.GET)
    public @ResponseBody
    String getSucChecklist(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {

            String res = sucursalChecklistBI.obtieneInfo();

            return res;

        } catch (Exception e) {
            logger.info("getSucChecklist()");
            return null;
        }

    }

    // http://localhost:8080/checklist/consultaCatalogosService/getFirmaCheckCeco.json?ceco=<?>
    @RequestMapping(value = "/getFirmaCheckCeco", method = RequestMethod.GET)
    public ModelAndView getFirmaCheckCeco(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = request.getParameter("ceco");

            List<FirmaCheckDTO> lista = firmaCheckBI.obtieneDatos(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "FIRMACHECK");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getFirmaCheckCeco()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getFirmaCheckDet.json?
    @RequestMapping(value = "/getFirmaCheckDet", method = RequestMethod.GET)
    public ModelAndView getFirmaCheckDet(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<FirmaCheckDTO> lista = firmaCheckBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "FIRMACHECK");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getFirmaCheckDet()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getRepocheckNo.json?bitacora=<?>
    @RequestMapping(value = "/getRepocheckNo", method = RequestMethod.GET)
    public ModelAndView getRepocheckNo(HttpServletRequest request, HttpServletResponse response) {
        try {

            int bitacora = Integer.parseInt(request.getParameter("bitacora"));

            List<ReporteChecksExpDTO> lista = reporteChecksExpBI.obtieneRespuestasNo(bitacora);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "REPOCHEKSEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getRepocheckNo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getRepoBitacorasGen.json?idusuario=<?>&ceco=<?>&idcheck=<?>
    @RequestMapping(value = "/getRepoBitacorasGen", method = RequestMethod.GET)
    public ModelAndView getRepoBitacorasGen(HttpServletRequest request, HttpServletResponse response) {
        try {

            /*
             * if(request.getParameter("idusuario")!=null)
             * if(!request.getParameter("idusuario").equals(""))
             */
            // String idusuario =request.getParameter("idusuario");
            /*
             * String ceco=null; if(request.getParameter("ceco")!=null)
             * if(!request.getParameter("ceco").equals(""))
             */
            // String ceco =request.getParameter("ceco");
            /*
             * String idcheck=null; if(request.getParameter("idcheck")!=null)
             * if(!request.getParameter("idcheck").equals(""))
             */
            String idusuario = request.getParameter("idusuario");
            String ceco = request.getParameter("ceco");
            String idcheck = request.getParameter("idcheck");

            List<ReporteChecksExpDTO> lista = reporteChecksExpBI.ObtieneBitacoras(idusuario, ceco, idcheck);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "REPOCHEKSEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getRepoBitacorasGen()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getImagenSuc.json?ceco=<?>
    @RequestMapping(value = "/getImagenSuc", method = RequestMethod.GET)
    public ModelAndView getImagenSuc(HttpServletRequest request, HttpServletResponse response) {
        try {

            int ceco = Integer.parseInt(request.getParameter("ceco"));

            List<SucursalChecklistDTO> lista = sucursalChecklistBI.obtieneDatos(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "IMAGSUCK");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getImagenSuc()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getImagenSucDet.json?
    @RequestMapping(value = "/getImagenSucDet", method = RequestMethod.GET)
    public ModelAndView getImagenSucDet(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<SucursalChecklistDTO> lista = sucursalChecklistBI.obtieneInfoG();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "IMAGSUCK");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getImagenSucDet()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getSucUbicacion.json?idpais=<?>
    @RequestMapping(value = "/getSucUbicacion", method = RequestMethod.GET)
    public ModelAndView getSucUbicacion(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idpais = request.getParameter("idpais");

            List<SucUbicacionDTO> lista = sucUbicacionBI.obtieneDatos(idpais);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "SUCUBICA");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getSucUbicacion()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getResp.json?idBita=<?>
    @RequestMapping(value = "/getResp", method = RequestMethod.GET)
    public @ResponseBody
    String getResp(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idBita = request.getParameter("idBita");
            String res = checklistPreguntasComBI.obtieneDatos(idBita);

            // System.out.println(res);
            return res;

        } catch (Exception e) {
            logger.info("getResp()");
            return null;
        }
    }

    // conteo preguntas
    // http://localhost:8080/checklist/consultaCatalogosService/getPregTot.json?idBita=<?>
    @RequestMapping(value = "/getPregTot", method = RequestMethod.GET)
    public ModelAndView getPregTot(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idBita = request.getParameter("idBita");

            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneInfo(idBita);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "TOTPREG");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPregTot()");
            return null;
        }
    }

    // respuestas Generico
    // http://localhost:8080/checklist/consultaCatalogosService/getRespGen.json?idBita=<?>
    @RequestMapping(value = "/getRespGen", method = RequestMethod.GET)
    public ModelAndView getRespGen(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idBita = request.getParameter("idBita");

            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneDatosGen(idBita);
            ModelAndView mv = new ModelAndView("muestraServicios");
            //logger.info(lista);
            mv.addObject("tipo", "RESPUESTAS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getRespGen()");
            return null;
        }
    }

    // respuestas por checkl
    // http://localhost:8080/checklist/consultaCatalogosService/getPregChecklist.json?idCheck=<?>
    @RequestMapping(value = "/getPregChecklist", method = RequestMethod.GET)
    public ModelAndView getPregChecklist(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idCheck = Integer.parseInt(request.getParameter("idCheck"));

            List<PreguntaCheckDTO> lista = checklistPreguntasComBI.obtieneInfopreg(idCheck);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "PREG_CHEC");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPregChecklist()");
            return null;
        }
    }

    // conteo preguntas por grupo
    // http://localhost:8080/checklist/consultaCatalogosService/getPregTotGrupo.json?idBita=<?>
    @RequestMapping(value = "/getPregTotGrupo", method = RequestMethod.GET)
    public ModelAndView getPregTotGrupo(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idBita = request.getParameter("idBita");

            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneInfoCont(idBita);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "TOTPREG");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPregTotGrupo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getSoftCeco.json?ceco=<?>
    @RequestMapping(value = "/getSoftCeco", method = RequestMethod.GET)
    public ModelAndView getSoftCeco(HttpServletRequest request, HttpServletResponse response) {
        try {

            int ceco = Integer.parseInt(request.getParameter("ceco"));

            List<SoftOpeningDTO> lista = softOpeningBI.obtieneDatos(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "SOFTOPEN");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getSoftCeco()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getSoftDeta.json?
    @RequestMapping(value = "/getSoftDeta", method = RequestMethod.GET)
    public ModelAndView getSoftDeta(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<SoftOpeningDTO> lista = softOpeningBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "SOFTOPEN");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getSoftDeta()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPadresHijas.json?idBita=<?>
    @RequestMapping(value = "/getPadresHijas", method = RequestMethod.GET)
    public @ResponseBody
    String getPadresHijas(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idBita = request.getParameter("idBita");
            String res = checklistPreguntasComBI.obtieneInfoPadHij(idBita);

            // System.out.println(res);
            return res;

        } catch (Exception e) {
            logger.info("getPadresHijas()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPregImperd.json?idBita=<?>
    @RequestMapping(value = "/getPregImperd", method = RequestMethod.GET)
    public ModelAndView getPregImperd(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idBita = Integer.parseInt(request.getParameter("idBita"));
            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneInfoImp(idBita);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "RESPUESTASIMP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPregImperd()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPregNoImperd.json?idBita=<?>
    @RequestMapping(value = "/getPregNoImperd", method = RequestMethod.GET)
    public ModelAndView getPregNoImperd(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idBita = Integer.parseInt(request.getParameter("idBita"));
            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneInfoNoImp(idBita);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "RESPUESTASIMP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPregNoImperd()");
            return null;
        }
    }

    // http://10.51.219.179:808ZX0/checklist/consultaCatalogosService/getCheckin.json?idBitacora=28964&idUsuario=82144&idCeco=480100&fecha=190806&latitud=45&longitud=456&bandera=1&plataforma=IOS&tiempoConexion=1
    @RequestMapping(value = "/getCheckin", method = RequestMethod.GET)
    public ModelAndView getCheckin(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idBitacora = 0;
            String idBitacoraS = request.getParameter("idBitacora");
            if (idBitacoraS != null) {
                idBitacora = Integer.parseInt(idBitacoraS);
            }
            int idUsuario = 0;
            String idUsuarioS = request.getParameter("idUsuario");
            if (idUsuarioS != null) {
                idUsuario = Integer.parseInt(idUsuarioS);
            }

            String idCeco = request.getParameter("idCeco");
            String fecha = request.getParameter("fecha");
            String latitud = request.getParameter("latitud");
            String longitud = request.getParameter("longitud");
            int bandera = 0;
            String banderaS = request.getParameter("bandera");
            String plataforma = request.getParameter("plataforma");
            int tiempoConexion = 0;
            String tipoConexion = (request.getParameter("tiempoConexion"));
            if (tipoConexion != null) {
                tiempoConexion = Integer.parseInt(tipoConexion);

            }

            if (banderaS != null) {
                bandera = Integer.parseInt(banderaS);
            }

            CheckinDTO asistencia = new CheckinDTO();

            asistencia.setIdBitacora(idBitacora);
            asistencia.setIdUsuario(idUsuario);
            asistencia.setIdCeco(idCeco);
            asistencia.setFecha(fecha);
            asistencia.setLatitud(latitud);
            asistencia.setLongitud(longitud);
            asistencia.setBandera(bandera);
            asistencia.setPlataforma(plataforma);
            asistencia.setTiempoConexion(tiempoConexion);

            List<CheckinDTO> lista = checkinBI.buscaCheckin(asistencia);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "CHECKIN");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getCheckin()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getNomChecklistGpo.json?idGrupo=<?>
    @RequestMapping(value = "/getNomChecklistGpo", method = RequestMethod.GET)
    public ModelAndView getNomChecklistGpo(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idGrupo = Integer.parseInt(request.getParameter("idGrupo"));

            List<ConsultaDTO> lista = consultaBI.obtieneConsultaChecklist(idGrupo);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "CHECKLIST_GPO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getNomChecklistGpo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getBitacora.json?idCeco=<?>&idChecklist=<?>
    @RequestMapping(value = "/getBitacora", method = RequestMethod.GET)
    public ModelAndView getBitacora(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idCeco = Integer.parseInt(request.getParameter("idCeco"));
            int idChecklist = Integer.parseInt(request.getParameter("idChecklist"));

            List<ConsultaDTO> lista = consultaBI.obtieneConsultaBitacora(idCeco, idChecklist);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "BITACORA");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getBitacora()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getConsultaCeco.json?idGrupo=<?>
    @RequestMapping(value = "/getConsultaCeco", method = RequestMethod.GET)
    public ModelAndView getConsultaCeco(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idGrupo = Integer.parseInt(request.getParameter("idGrupo"));

            List<ConsultaDTO> lista = consultaBI.obtieneConsultaCeco(idGrupo);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "CHECK_CECO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getConsultaCeco()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPreguntasBitPad.json?idBita=<?>
    @RequestMapping(value = "/getPreguntasBitPad", method = RequestMethod.GET)
    public ModelAndView getPreguntasBitPad(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idBita = request.getParameter("idBita");
            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneDatosGenales(idBita);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "RESPUESTAS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getVersionesExp.json?idVers=<?>
    @RequestMapping(value = "/getVersionesExp", method = RequestMethod.GET)
    public ModelAndView getVersionesExp(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idVers = Integer.parseInt(request.getParameter("idVers"));
            List<VersionExpanDTO> lista = versionExpanBI.obtieneDatos(idVers);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "VERSIONEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getVersionesExp()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getVersionesExpDetalle.json?
    @RequestMapping(value = "/getVersionesExpDetalle", method = RequestMethod.GET)
    public ModelAndView getVersionesExpDetalle(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<VersionExpanDTO> lista = versionExpanBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "VERSIONEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getVersionesExpDetalle()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getAsignacionExp.json?version=<?>
    @RequestMapping(value = "/getAsignacionExp", method = RequestMethod.GET)
    public ModelAndView getAsignacionExp(HttpServletRequest request, HttpServletResponse response) {
        try {
            int version = Integer.parseInt(request.getParameter("version"));

            List<AsignaExpDTO> lista = asignaExpBI.obtieneDatos(version);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ASIGNAEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getAsignacionExp()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getAsignacionExpDetalle.json?
    @RequestMapping(value = "/getAsignacionExpDetalle", method = RequestMethod.GET)
    public ModelAndView getAsignacionExpDetalle(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<AsignaExpDTO> lista = asignaExpBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ASIGNAEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getAsignacionExpDetalle()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getdetalleProtoBI.json?idGrupo=321
    @RequestMapping(value = "/getdetalleProtoBI", method = RequestMethod.GET)
    public ModelAndView getdetalleProtoBI(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idGrupo = Integer.parseInt(request.getParameter("idGrupo"));
            DetalleProtoDTO detalle = new DetalleProtoDTO();
            detalle.setIdOrdeGrupo(idGrupo);

            List<DetalleProtoDTO> lista = detalleProtoBI.ObtieneDetalleProto(detalle);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "UsuarioProtocolo");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getdetalleProtoBI()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getNombreGrupo.json?idGrupo=321&idUsuario=82144
    @RequestMapping(value = "/getNombreGrupo", method = RequestMethod.GET)
    public ModelAndView getNombreGrupo(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idGrupo = Integer.parseInt(request.getParameter("idGrupo"));
            int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
            DetalleProtoDTO detalle = new DetalleProtoDTO();
            detalle.setIdOrdeGrupo(idGrupo);
            detalle.setIdUsuario(idUsuario);

            List<DetalleProtoDTO> lista = detalleProtoBI.ObtieneNombreGrupo(detalle);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "idNombreChek");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getNombreGrupo()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getNombreCeco.json?idUsuario=82144&idGrupo=321
    @RequestMapping(value = "/getNombreCeco", method = RequestMethod.GET)
    public ModelAndView getNombreCeco(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
            int idGrupo = Integer.parseInt(request.getParameter("idGrupo"));
            DetalleProtoDTO detalle = new DetalleProtoDTO();
            detalle.setIdUsuario(idUsuario);
            detalle.setIdOrdeGrupo(idGrupo);

            List<DetalleProtoDTO> lista = detalleProtoBI.ObtieneNombreCeco(detalle);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "idNombreCeco");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getBitacoraCerrada.json?idUsuario=73606&idCeco=488361&idCheklist=1
    @RequestMapping(value = "/getBitacoraCerrada", method = RequestMethod.GET)
    public ModelAndView getBitacoraCerrada(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
            String idCeco = request.getParameter("idCeco");
            int idCheklist = Integer.parseInt(request.getParameter("idCheklist"));
            DetalleProtoDTO detalle = new DetalleProtoDTO();
            detalle.setIdUsuario(idUsuario);
            detalle.setIdCeco(idCeco);
            detalle.setIdCheklist(idCheklist);

            List<DetalleProtoDTO> lista = detalleProtoBI.ObtieneBitacoraCerrada(detalle);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "bitacoraCerrada");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getBitacoraCerrada()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getControlAcceso.json?idUsuario=73433&idCeco=480100&fdInicio=20190807&fdTermino=20190807
    @RequestMapping(value = "/getControlAcceso", method = RequestMethod.GET)
    public ModelAndView getControlAcceso(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idUsuario = 0;
            String idUsuarios = request.getParameter("idUsuario");
            if (idUsuarios != null) {
                idUsuario = Integer.parseInt(idUsuarios);
            }
            /*
             * int idCeo=0; if(request.getParameter("idCeco") != null ) { idCco =
             * Integer.parseInt(request.getParameter("idCeco")); }
             */

            String idCeco = request.getParameter("idCeco");
            String fdInicio = request.getParameter("fdInicio");
            String fdTermino = request.getParameter("fdTermino");

            DetalleProtoDTO detalle = new DetalleProtoDTO();
            detalle.setIdUsuario(idUsuario);
            detalle.setIdCeco(idCeco);
            detalle.setFdInicio(fdInicio);
            detalle.setFdTermino(fdTermino);
            List<DetalleProtoDTO> lista = detalleProtoBI.ObtieneControlAcceso(detalle);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "controlAcceso");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getControlAcceso()");
            return null;

        }

    }

    // http://localhost:8080/checklist/consultaCatalogosService/getVersionesGralDetalle.json?
    @RequestMapping(value = "/getVersionesGralDetalle", method = RequestMethod.GET)
    public ModelAndView getVersionesGralDetalle(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<VersionChecklistGralDTO> lista = versionChecklistGralBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "VERSIONGRAL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getVersionesGralDetalle()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getVersionesGral.json?idVers=<?>
    @RequestMapping(value = "/getVersionesGral", method = RequestMethod.GET)
    public ModelAndView getVersionesGral(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idVers = Integer.parseInt(request.getParameter("idVers"));
            List<VersionChecklistGralDTO> lista = versionChecklistGralBI.obtieneDatos(idVers);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "VERSIONGRAL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getVersionesGral()");
            return null;
        }
    }

    // solo mandarle un parametro del que sea
    // http://localhost:8080/checklist/consultaCatalogosService/getCecosTipsuc.json?ceco=<?>&status=<?>&tipoSuc=<?>
    @RequestMapping(value = "/getCecosTipsuc", method = RequestMethod.GET)
    public ModelAndView getCecosTipsuc(HttpServletRequest request, HttpServletResponse response) {
        try {
            String ceco = request.getParameter("ceco");
            String status = request.getParameter("status");
            String tipoSuc = request.getParameter("tipoSuc");

            List<SoporteExpDTO> lista = soporteExpBI.obtieneDatos(ceco, status, tipoSuc);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "SOPEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getCecosTipsuc()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCheckUsuAsig.json?usuario=<?>&ceco=<?>
    @RequestMapping(value = "/getCheckUsuAsig", method = RequestMethod.GET)
    public ModelAndView getCheckUsuAsig(HttpServletRequest request, HttpServletResponse response) {
        try {

            String usuario = request.getParameter("usuario");
            String ceco = request.getParameter("ceco");
            // String aux2 = request.getParameter("aux2");
            List<AsignaExpDTO> lista = asignaExpBI.obtieneDatosCheckUsua(usuario, ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "SOPASIGN");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getCheckUsuAsig()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCheckAsig.json?usuario=<?>&ceco=<?>&aux=<?>
    @RequestMapping(value = "/getCheckAsig", method = RequestMethod.GET)
    public ModelAndView getCheckAsig(HttpServletRequest request, HttpServletResponse response) {
        try {
            String usuario = request.getParameter("usuario");

            List<AsignaExpDTO> lista = asignaExpBI.obtieneDatosAsignaVer(usuario);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "SOPASIGN");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getCheckAsig()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getFiltro1.json?ceco=<?>&idRecorrido=<?>&nombreCeco=<?>&fechaIni=<?>&fechaFin=<?>
    @RequestMapping(value = "/getFiltro1", method = RequestMethod.GET)
    public ModelAndView getFiltro1(HttpServletRequest request, HttpServletResponse response) {
        try {
            String ceco = request.getParameter("ceco");
            String nombreCeco = request.getParameter("nombreCeco");
            String fechaIni = request.getParameter("fechaIni");
            String fechaFin = request.getParameter("fechaFin");
            String idRecorrido = request.getParameter("idRecorrido");

            RepoFilExpDTO re = new RepoFilExpDTO();

            re.setCeco(ceco);
            re.setNombreCeco(nombreCeco);
            re.setFechaIni(fechaIni);
            re.setFechaFin(fechaFin);
            re.setIdRecorrido(idRecorrido);

            List<RepoFilExpDTO> lista = repoFilExpBI.obtieneDatosGen(ceco, nombreCeco, fechaIni, fechaFin, idRecorrido);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "filtro1");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getFiltro1()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getFiltro2.json?ceco=<?>&idRecorrido=<?>
    @RequestMapping(value = "/getFiltro2", method = RequestMethod.GET)
    public ModelAndView getFiltro2(HttpServletRequest request, HttpServletResponse response) {
        try {
            String ceco = request.getParameter("ceco");
            String idRecorrido = request.getParameter("idRecorrido");

            RepoFilExpDTO re = new RepoFilExpDTO();

            re.setCeco(ceco);
            re.setIdRecorrido(idRecorrido);

            List<RepoFilExpDTO> lista = repoFilExpBI.obtieneInfo(ceco, idRecorrido);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "filtro2");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getFiltro2()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPregCeco.json?ceco=<?>
    @RequestMapping(value = "/getPregCeco", method = RequestMethod.GET)
    public ModelAndView getPregCeco(HttpServletRequest request, HttpServletResponse response) {
        try {
            String ceco = request.getParameter("ceco");

            RepoFilExpDTO re = new RepoFilExpDTO();

            re.setCeco(ceco);

            List<RepoFilExpDTO> lista = repoFilExpBI.obtienePreguntas(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "PREGUN");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPregCeco()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getFiltro3.json?bita1=<?>&pregFilt=<?>&bita2=<?>
    @RequestMapping(value = "/getFiltro3", method = RequestMethod.GET)
    public ModelAndView getFiltro3(HttpServletRequest request, HttpServletResponse response) {
        try {
            String bita1 = request.getParameter("bita1");
            String bita2 = request.getParameter("bita2");
            String pregFilt = request.getParameter("pregFilt");

            RepoFilExpDTO re = new RepoFilExpDTO();

            re.setBita1(bita1);
            re.setBita2(bita2);
            re.setPregFilt(pregFilt);

            List<RepoFilExpDTO> lista = repoFilExpBI.obtieneInfofiltro(bita1, bita2, pregFilt);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "filtro2");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getFiltro3()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPregCecoHijas.json?ceco=<?>
    @RequestMapping(value = "/getPregCecoHijas", method = RequestMethod.GET)
    public ModelAndView getPregCecoHijas(HttpServletRequest request, HttpServletResponse response) {
        try {
            String ceco = request.getParameter("ceco");

            RepoFilExpDTO re = new RepoFilExpDTO();

            re.setCeco(ceco);

            List<RepoFilExpDTO> lista = repoFilExpBI.obtienePreguntasHijas(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "PREGUN");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPregCecoHijas()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getdashExp.json?ceco=<?>&idRecorrido=<?>&aux=<?>
    @RequestMapping(value = "/getdashExp", method = RequestMethod.GET)
    public ModelAndView getdashExp(HttpServletRequest request, HttpServletResponse response) {
        try {
            String ceco = request.getParameter("ceco");
            String idRecorrido = request.getParameter("idRecorrido");
            String aux = request.getParameter("aux");

            RepoFilExpDTO re = new RepoFilExpDTO();

            re.setCeco(ceco);

            List<RepoFilExpDTO> lista = repoFilExpBI.obtienedash(ceco, idRecorrido, aux);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "DASHEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getdashExp()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getdashRespPadExp.json?ceco=<?>
    @RequestMapping(value = "/getdashRespPadExp", method = RequestMethod.GET)
    public ModelAndView getdashRespPadExp(HttpServletRequest request, HttpServletResponse response) {
        try {
            String ceco = request.getParameter("ceco");

            RepoFilExpDTO re = new RepoFilExpDTO();

            re.setCeco(ceco);

            List<RepoFilExpDTO> lista = repoFilExpBI.obtienedash2(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "DASHEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getdashRespPadExp()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getdashRespHijExp.json?bita1=<?>
    @RequestMapping(value = "/getdashRespHijExp", method = RequestMethod.GET)
    public ModelAndView getdashRespHijExp(HttpServletRequest request, HttpServletResponse response) {
        try {
            String bita1 = request.getParameter("bita1");

            RepoFilExpDTO re = new RepoFilExpDTO();

            re.setCeco(bita1);

            List<RepoFilExpDTO> lista = repoFilExpBI.obtienedash3(bita1);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "DASHEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getdashRespHijExp()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPregConcat.json?idBita=<?>
    @RequestMapping(value = "/getPregConcat", method = RequestMethod.GET)
    public @ResponseBody
    String getPregConcat(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idBita = request.getParameter("idBita");
            String res = checklistPreguntasComBI.obtieneDatosImpconc(idBita);

            // System.out.println(res);
            return res;

        } catch (Exception e) {
            logger.info("getPregConcat()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPregImpComb.json?idBita=<?>
    @RequestMapping(value = "/getPregImpComb", method = RequestMethod.GET)
    public @ResponseBody
    String getPregImpComb(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idBita = request.getParameter("idBita");
            String res = checklistPreguntasComBI.obtieneDatosImpconcpru(idBita);

            // System.out.println(res);
            return res;

        } catch (Exception e) {
            logger.info("getPregImpComb()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getContactoExtceco.json?ceco=<?>
    @RequestMapping(value = "/getContactoExtceco", method = RequestMethod.GET)
    public ModelAndView getContactoExtceco(HttpServletRequest request, HttpServletResponse response) {
        try {
            String ceco = request.getParameter("ceco");

            EmpContacExtDTO cont = new EmpContacExtDTO();

            cont.setCeco(ceco);

            List<EmpContacExtDTO> lista = empContacExtBI.obtieneDatos(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "CONTACTOEXT");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getContactoExtceco()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getContactoDetalle.json?
    @RequestMapping(value = "/getContactoDetalle", method = RequestMethod.GET)
    public ModelAndView getContactoDetalle(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<EmpContacExtDTO> lista = empContacExtBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "CONTACTOEXT");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getContactoDetalle()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getSucubicaTabla.json?pais=<?>
    @RequestMapping(value = "/getSucubicaTabla", method = RequestMethod.GET)
    public ModelAndView getSucubicaTabla(HttpServletRequest request, HttpServletResponse response) {
        try {
            String pais = request.getParameter("pais");

            SucUbicacionDTO suc = new SucUbicacionDTO();

            suc.setPais(pais);

            List<SucUbicacionDTO> lista = sucUbicacionBI.obtieneDatos(pais);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "SUCUBICA");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getSucubicaTabla()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getDashBoard.json?idUsuario=82144
    @RequestMapping(value = "/getDashBoard", method = RequestMethod.GET)
    public ModelAndView getDashBoard(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");

            ConsultaDashBoardDTO a1 = new ConsultaDashBoardDTO();
            a1.setIdUsuario(idUsuario);

            List<ConsultaDashBoardDTO> lista2 = consultaDashBoardBI.obtieneDashBoard(a1);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista2);
            mv.addObject("tipo", "GETDASHBOARD");
            mv.addObject("res", lista2);
            return mv;
        } catch (Exception e) {
            logger.info("getDashBoard()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getAsistencia.json?idUsuario=191312
    @RequestMapping(value = "/getAsistencia", method = RequestMethod.GET)
    public ModelAndView getAsistencia(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");

            ConsultaAsistenciaDTO a1 = new ConsultaAsistenciaDTO();
            a1.setIdUsuario(idUsuario);

            List<ConsultaAsistenciaDTO> lista3 = consultaAsistenciaBI.obtieneAsistencia(a1);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista3);
            mv.addObject("tipo", "CalculoAsistencia");
            mv.addObject("res", lista3);
            return mv;
        } catch (Exception e) {
            logger.info("getAsistencia()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getFechas.json?fechaInicial=01082019&fechaFinal=20082019
    @RequestMapping(value = "/getFechas", method = RequestMethod.GET)
    public ModelAndView getFechas(HttpServletRequest request, HttpServletResponse response) {
        try {
            String fechaInicial = request.getParameter("fechaInicial");
            String fechaFinal = request.getParameter("fechaFinal");

            ConsultaFechasDTO a1 = new ConsultaFechasDTO();
            a1.setFechaInicial(fechaInicial);
            a1.setFechaFinal(fechaFinal);

            List<ConsultaFechasDTO> lista4 = consultaFechasBI.obtieneFecha(a1);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista4);
            mv.addObject("tipo", "ConsultaFecha");
            mv.addObject("res", lista4);
            return mv;
        } catch (Exception e) {
            logger.info("getFechas()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getTerritorios.json
    @RequestMapping(value = "/getTerritorios", method = RequestMethod.GET)
    public ModelAndView getTerritorios(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<ConsultaDistribucionDTO> lista = consultaDistribucionBI.ObtieneTerritorio();

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Obtiene Territorio");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getTerritorios()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getRepoFechasExp.json?&fechaIni=<?>&fechaFin=<?>
    @RequestMapping(value = "/getRepoFechasExp", method = RequestMethod.GET)
    public ModelAndView getRepoFechasExp(HttpServletRequest request, HttpServletResponse response) {
        try {

            String fechaIni = request.getParameter("fechaIni");
            String fechaFin = request.getParameter("fechaFin");

            RepoFilExpDTO re = new RepoFilExpDTO();

            re.setFechaIni(fechaIni);
            re.setFechaFin(fechaFin);

            List<RepoFilExpDTO> lista = repoFilExpBI.repoFechas(fechaIni, fechaFin);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "REPOFECHAEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getRepoFechasExp()");
            return null;
        }
    }

    // http://10.51.218.144:8080/checklist/consultaCatalogosService/getSupervisor.json?idUsuario=73433
    @RequestMapping(value = "/getSupervisor", method = RequestMethod.GET)
    public ModelAndView getSupervisor(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idUser = 0;
            String idUsuario = request.getParameter("idUsuario");
            if (idUsuario != null) {
                idUser = Integer.parseInt(idUsuario);
            }
            SupervisorDTO supervisor = new SupervisorDTO();

            supervisor.setIdUsuario(Integer.parseInt(idUsuario));

            List<SupervisorDTO> lista = supervisorBI.buscaSupervisor(supervisor);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "SUPERVISOR");
            mv.addObject("lista", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getSupervisor()");
            return null;
        }
    }

    // http://10.51.218.144:8080/checklist/consultaCatalogosService/getAsistenciaSup.json?idUsuario=73433&fechaInicio=<?>&fechaFin=<?>
    @RequestMapping(value = "/getAsistenciaSup", method = RequestMethod.GET)
    public ModelAndView getAsistenciaSup(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idUser = 0;
            String fechaI = "";
            String fechaF = "";
            String idUsuario = request.getParameter("idUsuario");
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");
            if (idUsuario != null) {
                idUser = Integer.parseInt(idUsuario);
            }

            if (fechaInicio != null) {
                fechaI = fechaInicio;
            }

            if (fechaFin != null) {
                fechaF = fechaFin;
            }

            AsistenciaSupervisorDTO asistenciaSup = new AsistenciaSupervisorDTO();

            asistenciaSup.setIdUsuario(Integer.parseInt(idUsuario));
            asistenciaSup.setFechaInicio(fechaI);
            asistenciaSup.setFechaFin(fechaF);

            List<AsistenciaSupervisorDTO> lista = asistenciaSupervisorBI.buscaAsistencia(asistenciaSup);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ASISTENCIASUP");
            mv.addObject("lista", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getAsistenciaSup()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getnivelZona.json?territorio=236737
    @RequestMapping(value = "/getnivelZona", method = RequestMethod.GET)
    public ModelAndView getnivelZona(HttpServletRequest request, HttpServletResponse response) {
        try {

            String territorio = request.getParameter("territorio");

            ConsultaDistribucionDTO a1 = new ConsultaDistribucionDTO();
            a1.setTerritorio(territorio);

            List<ConsultaDistribucionDTO> lista = consultaDistribucionBI.ObtieneNivelZona(a1);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Obtiene Zonas");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getnivelZona()");
            return null;
        }

    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getnivelRegion.json?territorio=236737&zona=238347
    @RequestMapping(value = "/getnivelRegion", method = RequestMethod.GET)
    public ModelAndView getnivelRegion(HttpServletRequest request, HttpServletResponse response) {
        try {

            String territorio = request.getParameter("territorio");
            String zona = request.getParameter("zona");

            ConsultaDistribucionDTO a1 = new ConsultaDistribucionDTO();
            a1.setTerritorio(territorio);
            a1.setZona(zona);

            List<ConsultaDistribucionDTO> lista = consultaDistribucionBI.ObtieneNivelRegion(a1);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Obtiene Zonas");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getnivelRegion()");
            return null;
        }

    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getnivelGerente.json?territorio=236737&zona=238347&region=236093
    @RequestMapping(value = "/getnivelGerente", method = RequestMethod.GET)
    public ModelAndView getnivelGerente(HttpServletRequest request, HttpServletResponse response) {
        try {

            String territorio = request.getParameter("territorio");
            String zona = request.getParameter("zona");
            String region = request.getParameter("region");

            ConsultaDistribucionDTO a1 = new ConsultaDistribucionDTO();
            a1.setTerritorio(territorio);
            a1.setZona(zona);
            a1.setRegion(region);

            List<ConsultaDistribucionDTO> lista = consultaDistribucionBI.ObtieneNivelGerente(a1);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Obtiene Zonas");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getnivelGerente()");
            return null;
        }

    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getnivelSucursal.json?territorio=236737&zona=238347&region=236093&gerente=489800
    @RequestMapping(value = "/getnivelSucursal", method = RequestMethod.GET)
    public ModelAndView getnivelSucursal(HttpServletRequest request, HttpServletResponse response) {
        try {

            String territorio = request.getParameter("territorio");
            String zona = request.getParameter("zona");
            String region = request.getParameter("region");
            String gerente = request.getParameter("gerente");

            ConsultaDistribucionDTO a1 = new ConsultaDistribucionDTO();
            a1.setTerritorio(territorio);
            a1.setZona(zona);
            a1.setRegion(region);
            a1.setGerente(gerente);

            List<ConsultaDistribucionDTO> lista = consultaDistribucionBI.ObtieneNivelSucursal(a1);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Obtiene Zonas");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getnivelSucursal()");
            return null;
        }

    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getEstructura.json?idCeco=480100
    @RequestMapping(value = "/getEstructura", method = RequestMethod.GET)
    public ModelAndView getEstructura(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idCeco = request.getParameter("idCeco");

            ConsultaDistribucionDTO a1 = new ConsultaDistribucionDTO();
            a1.setIdCeco(idCeco);

            List<ConsultaDistribucionDTO> lista = consultaDistribucionBI.ObtieneEstructura(a1);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Obtiene Estructura");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getEstructura()");
            return null;
        }

    }

    //http://10.51.219.179:8080/checklist/consultaCatalogosService/getVisitas.json?idCeco=480100&protocolo=415
    @RequestMapping(value = "/getVisitas", method = RequestMethod.GET)
    public ModelAndView getVisitas(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idCeco = request.getParameter("idCeco");
            String protocolo = request.getParameter("protocolo");

            ConsultaVisitasDTO a1 = new ConsultaVisitasDTO();
            a1.setIdCeco(idCeco);
            a1.setProtocolo(protocolo);

            List<ConsultaVisitasDTO> lista = consultaVisitasBI.obtieneVisitas(a1);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Obtiene Visitas");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getVisitas()");
            return null;
        }

    }

    //http://10.51.219.179:8080/checklist/consultaCatalogosService/getpromoCalif.json?idCeco=480100&protocolo=415
    @RequestMapping(value = "/getpromoCalif", method = RequestMethod.GET)
    public ModelAndView getpromoCalif(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idCeco = request.getParameter("idCeco");
            String protocolo = request.getParameter("protocolo");

            ConsultaVisitasDTO a1 = new ConsultaVisitasDTO();
            a1.setIdCeco(idCeco);
            a1.setProtocolo(protocolo);

            List<ConsultaVisitasDTO> lista = consultaVisitasBI.ObtienePromCalif(a1);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "promedioCalif");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getpromoCalif()");
            return null;
        }

    }

    // http://10.51.218.144:8080/checklist/consultaCatalogosService/getConsultaSucursal.json?fechaInicio=<?>&fechaFin=<?>&idSucursal=<?>&idProtocolo=<?>
    @RequestMapping(value = "/getConsultaSucursal", method = RequestMethod.GET)
    public ModelAndView getConsultaSucursal(HttpServletRequest request, HttpServletResponse response) {
        try {
            String fechaDeInicio = null;
            String fechaIni = request.getParameter("fechaInicio");
            if (fechaIni != null) {
                fechaDeInicio = fechaIni;
            }

            String fechaDeFin = null;
            String fechaFinal = request.getParameter("fechaFin");
            if (fechaIni != null) {
                fechaDeFin = fechaFinal;
            }

            int idCeco = 0;
            String idSucursal = request.getParameter("idSucursal");
            if (idSucursal != null) {
                idCeco = Integer.parseInt(idSucursal);
            }

            int idChecklist = 0;
            String idCheck = request.getParameter("idProtocolo");
            if (idCheck != null) {
                idChecklist = Integer.parseInt(idCheck);
            } else {
                idChecklist = 0;
            }

            ConsultaSupervisionDTO consulta = new ConsultaSupervisionDTO();

            consulta.setIdSucursal(Integer.parseInt(idSucursal));
            consulta.setFechaInicio(fechaDeInicio);
            consulta.setFechaFin(fechaDeFin);
            consulta.setIdProtocolo(idChecklist);

            List<ConsultaSupervisionDTO> lista = consultaSupervisionBI.buscaSucursal(consulta);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "CON_SUCURSAL");
            mv.addObject("lista", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getConsultaSucursal()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/gethallazgosBita.json?&ceco=<?>
    @RequestMapping(value = "/gethallazgosBita", method = RequestMethod.GET)
    public ModelAndView gethallazgosBita(HttpServletRequest request, HttpServletResponse response) {
        try {

            int ceco = Integer.parseInt(request.getParameter("ceco"));

            List<HallazgosExpDTO> lista = hallazgosExpBI.obtieneDatos(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("zgosBita()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/gethallazgosDeta.json?
    @RequestMapping(value = "/gethallazgosDeta", method = RequestMethod.GET)
    public ModelAndView gethallazgosDeta(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<HallazgosExpDTO> lista = hallazgosExpBI.obtieneInfo();

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("zgosDeta()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/gethallazgosEviDeta.json?
    @RequestMapping(value = "/gethallazgosEviDeta", method = RequestMethod.GET)
    public ModelAndView gethallazgosEviDeta(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<HallazgosEviExpDTO> lista = hallazgosEviExpBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSEVIEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("zgosEviDeta()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/gethallazgosEviBita.json?bitGral=
    @RequestMapping(value = "/gethallazgosEviBita", method = RequestMethod.GET)
    public ModelAndView gethallazgosEviBita(HttpServletRequest request, HttpServletResponse response) {
        try {

            int bitGral = Integer.parseInt(request.getParameter("bitGral"));

            List<HallazgosEviExpDTO> lista = hallazgosEviExpBI.obtieneDatosBita(bitGral);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSEVIEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("zgosEviBita()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/gethallazgosEviResp.json?ceco=
    @RequestMapping(value = "/gethallazgosEviResp", method = RequestMethod.GET)
    public ModelAndView gethallazgosEviResp(HttpServletRequest request, HttpServletResponse response) {
        try {

            int ceco = Integer.parseInt(request.getParameter("ceco"));

            List<HallazgosEviExpDTO> lista = hallazgosEviExpBI.obtieneDatosBita(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSEVIEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("zgosEviResp()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getBitacoras.json?idBitacora=2&idCheckUsua=3&fechaInicio=20191009&fechaFin=20191009
    @RequestMapping(value = "/getBitacoras", method = RequestMethod.GET)
    public ModelAndView getBitacoras(HttpServletRequest request, HttpServletResponse response) {

        int idBitacora = Integer.parseInt(request.getParameter("idBitacora"));
        int idCheckUsua = Integer.parseInt(request.getParameter("idCheckUsua"));
        String fechaInicio = request.getParameter("fechaInicio");
        String fechaFin = request.getParameter("fechafin");

        BitacoraAdministradorDTO consulta = new BitacoraAdministradorDTO();

        consulta.setIdBitacora(idBitacora);
        consulta.setIdCheckUsua(idCheckUsua);
        consulta.setFechaInicio(fechaInicio);
        consulta.setFechaFin(fechaFin);

        List<BitacoraAdministradorDTO> lista = bitacoraAdministradorBI.buscaBitacoraCompleto(consulta);

        //System.out.println(lista);
        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "GETCOMPLETO");
        mv.addObject("res", lista);

        return mv;
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getBitaco.json?idCheckUsua=124794&idBitacora=28875&fechaInicio=20190806&fechaFin=20190806
    @RequestMapping(value = "/getBitaco", method = RequestMethod.GET)
    public ModelAndView getBitaco(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idCheckUsua = Integer.parseInt(request.getParameter("idCheckUsua"));
            int idBitacora = Integer.parseInt(request.getParameter("idBitacora"));
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");

            List<BitacoraAdministradorDTO> lista = bitacoraAdministradorBI.buscaBitacorasPorCampo(idCheckUsua, idBitacora, fechaInicio, fechaFin);

            //System.out.println(lista);
            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTADO BITACORA");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info("getBitaco()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPregTotGrupoExt.json?idBita=<?>
    @RequestMapping(value = "/getPregTotGrupoExt", method = RequestMethod.GET)
    public ModelAndView getPregTotGrupoExt(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idBita = request.getParameter("idBita");

            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneInfoContExt(idBita);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "TOTPREG");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPregTotGrupoExt()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getPregTotConteoHij.json?idBita=<?>

    @RequestMapping(value = "/getPregTotConteoHij", method = RequestMethod.GET)
    public ModelAndView getPregTotConteoHij(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idBita = request.getParameter("idBita");

            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneInfoContHij(idBita);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "TOTPREG");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPregTotConteoHij()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/gethallazgoRepo.json?fechaIni=<?>&fechaFin=
    @RequestMapping(value = "/gethallazgoRepo", method = RequestMethod.GET)
    public ModelAndView gethallazgoRepo(HttpServletRequest request, HttpServletResponse response) {
        try {
            String fechaIni = request.getParameter("fechaIni");
            String fechaFin = request.getParameter("fechaFin");

            List<ChecklistHallazgosRepoDTO> lista = hallazgosRepoBI.obtieneDatosFecha(fechaIni, fechaFin);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "RESPUESTASIMP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("zgoRepo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/gethallazgoRepoListas.json?bitgral=
    @RequestMapping(value = "/gethallazgoRepoListas", method = RequestMethod.GET)
    public @ResponseBody
    String gethallazgoRepoListas(HttpServletRequest request, HttpServletResponse response) {

        try {
            //String bitgral="";

            String bitgral = request.getParameter("bitgral");

            String res = hallazgosRepoBI.cursores(bitgral);
            return res;
        } catch (Exception e) {
            logger.info("zgoRepoListas()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getParamet.json?claveParametro=cargaDistribucion
    @RequestMapping(value = "/getParamet", method = RequestMethod.GET)
    public ModelAndView getParamet(HttpServletRequest request, HttpServletResponse response) {
        try {

            String claveParametro = request.getParameter("claveParametro");
            ParametroDTO a1 = new ParametroDTO();
            a1.setclaveParametro(claveParametro);
            int res = parametrobi.getParametro(a1);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PARAMETROS");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info("getParamet()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getCecoDist.json?ceco=485487&territorio=236737&zona=236279&region=236122&gerente=485487
    @RequestMapping(value = "/getCecoDist", method = RequestMethod.GET)
    public ModelAndView getCecoDist(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = request.getParameter("ceco");
            String territorio = request.getParameter("territorio");
            String zona = request.getParameter("zona");
            String region = request.getParameter("region");
            String gerente = request.getParameter("gerente");

            DistribCecoDTO distribCeco = new DistribCecoDTO();

            distribCeco.setCeco(ceco);
            distribCeco.setTerritorio(territorio);
            distribCeco.setZona(zona);
            distribCeco.setRegion(region);
            distribCeco.setGerente(gerente);

            List<DistribCecoDTO> lista = distribCecoBI.obtieneCeco(distribCeco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "DISTRIBCECOS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getCecoDist()");
            return null;
        }

    }
    // http://localhost:8080/checklist/consultaCatalogosService/getConteoTab.json?

    @RequestMapping(value = "/getConteoTab", method = RequestMethod.GET)
    public ModelAndView getConteoTab(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<ConteoTablasDTO> lista = sucUbicacionBI.obtieneInfoTab();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "CONTEOTAB");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getConteoTab()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getBooks.json
    @RequestMapping(value = "/getBooks", method = RequestMethod.GET)
    public ModelAndView getBooks(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<AdmHandbookDTO> lista = admHandbookBI.obtieneTodos();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "BOOKS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getBooks()");
            return null;
        }
    }

    //http://10.51.219.179:8080/checklist/consultaCatalogosService/getidBook.json?idBook=1
    @RequestMapping(value = "/getidBook", method = RequestMethod.GET)
    public ModelAndView getidBook(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idBook = request.getParameter("idBook");

            AdmHandbookDTO a1 = new AdmHandbookDTO();
            a1.setIdBook(idBook);

            List<AdmHandbookDTO> lista = admHandbookBI.obtieneIdBook(a1);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "BOOKS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getidBook()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getHallaXCecoRecorrido.json?ceco=<?>

    @RequestMapping(value = "/getHallaXCecoRecorrido", method = RequestMethod.GET)
    public ModelAndView getHallaXCecoRecorrido(HttpServletRequest request, HttpServletResponse response) {
        try {

            int ceco = Integer.parseInt(request.getParameter("ceco"));

            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneInfoHallaz(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "hallaceco");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("XCecoRecorrido()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getHallaXCecoTab.json?ceco=<?>
    @RequestMapping(value = "/getHallaXCecoTab", method = RequestMethod.GET)
    public ModelAndView getHallaXCecoTab(HttpServletRequest request, HttpServletResponse response) {
        try {

            int ceco = Integer.parseInt(request.getParameter("ceco"));

            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneInfoHallazTab(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "hallaceco");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("XCecoTab()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/gethallazgosHistoDeta.json?
    @RequestMapping(value = "/gethallazgosHistoDeta", method = RequestMethod.GET)
    public ModelAndView gethallazgosHistoDeta(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<HallazgosExpDTO> lista = hallazgosExpBI.obtieneHistorico();

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("XCecoTab()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getHallazgoHisBita.json?bitaGral=

    @RequestMapping(value = "/getHallazgoHisBita", method = RequestMethod.GET)
    public ModelAndView getHallazgoHisBita(HttpServletRequest request, HttpServletResponse response) {
        try {

            int bitaGral = Integer.parseInt(request.getParameter("bitaGral"));
            List<HallazgosExpDTO> lista = hallazgosExpBI.obtieneHistBita(bitaGral);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("zgoHisBita()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getHallazgoHisFolio.json?idFolio=
    @RequestMapping(value = "/getHallazgoHisFolio", method = RequestMethod.GET)
    public ModelAndView getHallazgoHisFolio(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idFolio = Integer.parseInt(request.getParameter("idFolio"));
            List<HallazgosExpDTO> lista = hallazgosExpBI.obtieneHistFolio(idFolio);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("zgoHisFolio()");
            return null;
        }
    }

    //http://10.51.219.179:8080/checklist/consultaCatalogosService/consultaProtocolo.json?idProtocolo=<?>
    @RequestMapping(value = "/consultaProtocolo", method = RequestMethod.GET)
    public ModelAndView consultaProtocolo(HttpServletRequest request, HttpServletResponse response) {
        try {
            if (request.getParameter("idProtocolo") != null) {
                String idProtocolo = request.getParameter("idProtocolo");
                if (idProtocolo != null && !idProtocolo.equals("null")) {
                    List<ProtocoloDTO> respuesta = protocoloBI.consultaProtocolo(Integer.valueOf(idProtocolo));
                    ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
                    mv.addObject("tipo", "Consulta  Protocolo");
                    mv.addObject("res", respuesta);

                    return mv;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.info("consultaProtocolo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getRepoActa.json?bita1=<?>
    @RequestMapping(value = "/getRepoActa", method = RequestMethod.GET)
    public ModelAndView getRepoActa(HttpServletRequest request, HttpServletResponse response) {
        try {
            String bita1 = request.getParameter("bita1");
            RepoFilExpDTO re = new RepoFilExpDTO();
            re.setBita1(bita1);
            List<RepoFilExpDTO> lista = repoFilExpBI.obtieneInfofActa(bita1);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "filtro2");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getRepoActa()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/getProtocoloByIdCheck.json?idChecklist=<?>
    @RequestMapping(value = "/getProtocoloByIdCheck", method = RequestMethod.GET)
    public ModelAndView getProtocoloByIdCheck(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idChecklist = request.getParameter("idChecklist");
            List<ProtocoloByCheckDTO> respuesta = protocoloByCheckBI.obtieneProtocolo(Integer.parseInt(idChecklist));
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Consulta  Protocolo By Id Checklist");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("getProtocoloByIdCheck()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPregActa.json?bita1=<?>
    @RequestMapping(value = "/getPregActa", method = RequestMethod.GET)
    public ModelAndView getPregActa(HttpServletRequest request, HttpServletResponse response) {
        try {
            String bita1 = request.getParameter("bita1");

            RepoFilExpDTO re = new RepoFilExpDTO();

            re.setBita1(bita1);

            List<RepoFilExpDTO> lista = repoFilExpBI.obtienePreguntasActa(bita1);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "PREGUN");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPregActa()");
            return null;
        }
    }

    // conteo preguntas
    // http://localhost:8080/checklist/consultaCatalogosService/getTotPregGrupOCC.json?idBita=<?>
    @RequestMapping(value = "/getTotPregGrupOCC", method = RequestMethod.GET)
    public ModelAndView getTotPregGrupOCC(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idBita = request.getParameter("idBita");

            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneInfoOCC(idBita);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "TOTPREG");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getTotPregGrupOCC()");
            return null;
        }
    }

    // conteo preguntas
    // http://localhost:8080/checklist/consultaCatalogosService/getTotPregGrupOCCTotgral.json?idBita=<?>
    @RequestMapping(value = "/getTotPregGrupOCCTotgral", method = RequestMethod.GET)
    public ModelAndView getTotPregGrupOCCTotgral(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idBita = request.getParameter("idBita");

            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneComplemOCC(idBita);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "TOTPREG");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getTotPregGrupOCCTotgral()");
            return null;
        }
    }

    // conteo preguntas
    // http://localhost:8080/checklist/consultaCatalogosService/getTotPregGrupOCCTotSiNa.json?idBita=<?>
    @RequestMapping(value = "/getTotPregGrupOCCTotSiNa", method = RequestMethod.GET)
    public ModelAndView getTotPregGrupOCCTotSiNa(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idBita = request.getParameter("idBita");

            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneComplemSiNaOCC(idBita);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "TOTPREG");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getTotPregGrupOCCTotSiNa()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getNegoDeta.json?

    @RequestMapping(value = "/getNegoDeta", method = RequestMethod.GET)
    public ModelAndView getNegoDeta(HttpServletRequest request, HttpServletResponse response) {
        try {
            String negocio = request.getParameter("negocio");
            List<ZonaNegoExpDTO> lista = zonaNegoBI.obtieneInfoNego(negocio);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "NegoZona");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getNegoDeta()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getNego.json?idNego=
    // sino se le manda el numero de id trae todo lo de la tabla
    @RequestMapping(value = "/getNego", method = RequestMethod.GET)
    public ModelAndView getNego(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idNego = request.getParameter("idNego");

            List<ZonaNegoExpDTO> lista = zonaNegoBI.obtieneDatosNego(idNego);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "NegoZona");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getNego()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getZonaDeta.json?
    @RequestMapping(value = "/getZonaDeta", method = RequestMethod.GET)
    public ModelAndView getZonaDeta(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<ZonaNegoExpDTO> lista = zonaNegoBI.obtieneInfoZona();

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "NegoZona");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getZonaDeta()");
            return null;
        }
    }
    // sino se le manda el numero de id trae todo lo de la tabla
    // http://localhost:8080/checklist/consultaCatalogosService/getZona.json?idZona=

    @RequestMapping(value = "/getZona", method = RequestMethod.GET)
    public ModelAndView getZona(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idZona = request.getParameter("idZona");

            List<ZonaNegoExpDTO> lista = zonaNegoBI.obtieneDatosZona(idZona);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "NegoZona");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getZona()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getRelaNegoZona.json?
    @RequestMapping(value = "/getRelaNegoZona", method = RequestMethod.GET)
    public ModelAndView getRelaNegoZona(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<ZonaNegoExpDTO> lista = zonaNegoBI.obtieneInfoRela();

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "NegoZona");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getRelaNegoZona()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getCecoProtocolo.json
    @RequestMapping(value = "/getCecoProtocolo", method = RequestMethod.GET)
    public ModelAndView getCecoProtocolo(HttpServletRequest request, HttpServletResponse response) {
        try {

            List<ConsultaCecoProtocoloDTO> respuesta = consultaCecoProtocoloBI.obtieneConsultaCeco();
            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "GETNOMSUCU");
            mv.addObject("res", respuesta);
            return mv;
        } catch (Exception e) {
            logger.info("getCecoProtocolo()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getCecoBitacora.json?idCeco=480100&fechaInicio=20191101&fechaTermino=20191105
    @RequestMapping(value = "/getCecoBitacora", method = RequestMethod.GET)
    public ModelAndView getCecoBitacora(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idCeco = Integer.parseInt(request.getParameter("idCeco"));
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaTermino = request.getParameter("fechaTermino");

            ConsultaCecoProtocoloDTO consultaCeco = new ConsultaCecoProtocoloDTO();
            consultaCeco.setIdCeco(idCeco);
            consultaCeco.setFechaInicio(fechaInicio);
            consultaCeco.setFechaTermino(fechaTermino);

            List<ConsultaCecoProtocoloDTO> respuesta = consultaCecoProtocoloBI.obtieneConsultaBitacora(consultaCeco);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "GETCECOBITACORA");
            mv.addObject("res", respuesta);
            return mv;
        } catch (Exception e) {
            logger.info("getCecoBitacora()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getNomCheck.json?idCeco=480100&fechaTermino=06/11/2019
    @RequestMapping(value = "/getNomCheck", method = RequestMethod.GET)
    public ModelAndView getNomCheck(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idCeco = Integer.parseInt(request.getParameter("idCeco"));
            String fechaTermino = request.getParameter("fechaTermino");

            ConsultaCecoProtocoloDTO consultaCeco = new ConsultaCecoProtocoloDTO();
            consultaCeco.setIdCeco(idCeco);
            consultaCeco.setFechaTermino(fechaTermino);

            List<ConsultaCecoProtocoloDTO> respuesta = consultaCecoProtocoloBI.obtieneConsultaChecklist(consultaCeco);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "GETNOMCHECK");
            mv.addObject("res", respuesta);
            return mv;
        } catch (Exception e) {
            logger.info("getNomCheck()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/getDetalleCeco.json?idCeco=<?>&fechaInicio=<?>&fechaFin=<?>&idChecklist=<?>
    @RequestMapping(value = "/getDetalleCeco", method = RequestMethod.GET)
    public ModelAndView getDetalleCeco(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCeco = request.getParameter("idCeco");
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");
            String idChecklist = request.getParameter("idChecklist");
            List<ReporteImgDTO> respuesta = reporteImgBI.obtieneDetalleCeco(idCeco, fechaInicio, fechaFin, idChecklist);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Consulta  Detalle Ceco");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("getDetalleCeco()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/getProtocoloZona.json?idGrupo=<?>
    @RequestMapping(value = "/getProtocoloZona", method = RequestMethod.GET)
    public ModelAndView getProtocoloZona(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idGrupo = request.getParameter("idGrupo");
            List<ReporteImgDTO> respuesta = reporteImgBI.obtieneProtocolosZonas(Integer.parseInt(idGrupo));
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Consulta  Protocolos/Zonas");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("getProtocoloZona()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/obtieneSucursales.json
    @RequestMapping(value = "/obtieneSucursales", method = RequestMethod.GET)
    public ModelAndView obtieneSucursales(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<ReporteImgDTO> respuesta = reporteImgBI.obtieneSucursales();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Consulta  Sucursales");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("obtieneSucursales()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/getCursorPreguntas.json?idProtocolo=21&fechaInicio=01/10/2019&fechaFin=31/10/2019
    @RequestMapping(value = "/getCursorPreguntas", method = RequestMethod.GET)
    public ModelAndView getCursorPreguntas(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idProtocolo = request.getParameter("idProtocolo");
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");
            List<ReporteMedPregDTO> respuesta = reporteMedicionBI.getCursor(Integer.parseInt(idProtocolo), fechaInicio, fechaFin);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Consulta  Preguntas");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("getCursorPreguntas()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/getAcervo.json?idBitacora=70&idChecklist=21
    @RequestMapping(value = "/getAcervo", method = RequestMethod.GET)
    public ModelAndView getAcervo(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idBitacora = request.getParameter("idBitacora");
            String idChecklist = request.getParameter("idChecklist");
            List<ReporteImgDTO> respuesta = reporteImgBI.getAcervo(idBitacora, idChecklist);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Consulta  Acervo");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("getAcervo()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getDetalle.json?idBitacora=<?>
    @RequestMapping(value = "/getDetalle", method = RequestMethod.GET)
    public ModelAndView getDetalle(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idBitacora = Integer.parseInt(request.getParameter("idBitacora"));
            ConsultaCecoProtocoloDTO consultaDetalle = new ConsultaCecoProtocoloDTO();
            consultaDetalle.setIdBitacora(idBitacora);
            List<ConsultaCecoProtocoloDTO> lista = consultaCecoProtocoloBI.obtieneDetalle(consultaDetalle);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "GETDETALLE");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getDetalle()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getAsegurador.json
    @RequestMapping(value = "/getAsegurador", method = RequestMethod.GET)
    public ModelAndView getAsegurador(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<ConsultaAseguradorDTO> lista = consultaAseguradorBI.obtieneConsultaAsegurador();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "GETASEGURADOR");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getAsegurador()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getAseguradorSucursal.json?idUsuario=73433
    @RequestMapping(value = "/getAseguradorSucursal", method = RequestMethod.GET)
    public ModelAndView getAseguradorSucursal(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));

            ConsultaAseguradorDTO consultaAseguradorSucursal = new ConsultaAseguradorDTO();

            consultaAseguradorSucursal.setIdUsuario(idUsuario);
            List<ConsultaAseguradorDTO> lista = consultaAseguradorBI.obtieneConsultaSucursal(consultaAseguradorSucursal);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "GETASEGURADORSUCURSAL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getAseguradorSucursal()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getAseguradorBitacora.json?idUsuario=73601&idCeco=999995
    @RequestMapping(value = "/getAseguradorBitacora", method = RequestMethod.GET)
    public ModelAndView getAseguradorBitacora(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
            int idCeco = Integer.parseInt(request.getParameter("idCeco"));
            ConsultaAseguradorDTO consultaAseguradorBitacora = new ConsultaAseguradorDTO();
            consultaAseguradorBitacora.setIdUsuario(idUsuario);
            consultaAseguradorBitacora.setIdCeco(idCeco);
            List<ConsultaAseguradorDTO> lista = consultaAseguradorBI.obtieneConsultaBitaCerr(consultaAseguradorBitacora);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "GETASEGURADORBITACORA");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getAseguradorBitacora()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getAsegProto.json?idCeco=480100&fechaTermino=2019/11/06
    @RequestMapping(value = "/getAsegProto", method = RequestMethod.GET)
    public ModelAndView getAsegProto(HttpServletRequest request, HttpServletResponse response) {
        try {

            String fechaTermino = request.getParameter("fechaTermino");
            int idCeco = Integer.parseInt(request.getParameter("idCeco"));

            ConsultaAseguradorDTO consultaAsegProto = new ConsultaAseguradorDTO();
            consultaAsegProto.setFechaTermino(fechaTermino);
            consultaAsegProto.setIdCeco(idCeco);

            List<ConsultaAseguradorDTO> lista = consultaAseguradorBI.obtieneProtocolo(consultaAsegProto);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "GETASEGPROTO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getAsegProto()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getDetalleBitacora.json?idBitacora=<?>
    @RequestMapping(value = "/getDetalleBitacora", method = RequestMethod.GET)
    public ModelAndView getDetalleBitacora(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idBitacora = Integer.parseInt(request.getParameter("idBitacora"));
            ConsultaAseguradorDTO consultaDetalleBit = new ConsultaAseguradorDTO();
            consultaDetalleBit.setIdBitacora(idBitacora);
            List<ConsultaAseguradorDTO> lista = consultaAseguradorBI.obtieneDetalleBitacora(consultaDetalleBit);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "GETDETALLEBITACORA");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getDetalleBitacora()");
            return null;
        }
    }
    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getidGerente.json?idUsuario=73433

    @RequestMapping(value = "/getidGerente", method = RequestMethod.GET)
    public ModelAndView getidGerente(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
            AltaGerenteDTO getIdGerente = new AltaGerenteDTO();
            getIdGerente.setIdUsuario(idUsuario);
            List<AltaGerenteDTO> lista = altaGerenteBI.obtieneIdGerente(getIdGerente);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "GETIDGERENTE");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getidGerente()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/getReporteZonas.json?idProtocolo=?&fechaInicio=?&fechaFin=?
    @RequestMapping(value = "/getReporteZonas", method = RequestMethod.GET)
    public ModelAndView getReporteZonas(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idProtocolo = request.getParameter("idProtocolo");
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");
            List<RepMedicionDTO> respuesta = repMedicionBI.ReporteMedZonas(Integer.parseInt(idProtocolo), fechaInicio, fechaFin);
            if (respuesta != null) {
                ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
                mv.addObject("tipo", "Reporte Medicion Zonas");
                mv.addObject("res", respuesta);

                return mv;
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.info("getReporteZonas()");
            return null;
        }
    }

    /**
     * ************************************************************** Inicia
     * Administrador de perfiles Supervision
     * *************************************************************
     */
    /* Nota: si no se envía valor en fechaInicio, se considera la fecha del día en el que se haga el registro como
     fecha inicio de la asignación de ceco a empleado.
     Si no se envía valor en fechaFin, se considera la fecha 31/12/9999 como fecha fin de la asignación de ceco a empleado.
     */
    //http://10.51.218.72:8080/checklist/consultaCatalogosService/insertAsignacion.json?idUsuario=?&idPerfil=?&idCeco=?&idGerente=?&activo=?&fechaInicio=?&fechaFin=?
    @RequestMapping(value = "/insertAsignacion", method = RequestMethod.GET)
    public ModelAndView insertAsignacion(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");
            String idCeco = request.getParameter("idCeco");
            String idGerente = request.getParameter("idGerente");
            String activo = request.getParameter("activo");
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");
            int respuesta = asignacionesSupBI.insertaAsignacion(Integer.parseInt(idUsuario), Integer.parseInt(idPerfil), idCeco, Integer.parseInt(idGerente),
                    Integer.parseInt(activo), fechaInicio, fechaFin);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Inserta Asignacion");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("insertAsignacion()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/updateAsignacion.json?idUsuario=?&idPerfil=?&idCeco=?&idGerente=?&activo=?&fechaInicio=?&fechaFin=?
    @RequestMapping(value = "/updateAsignacion", method = RequestMethod.GET)
    public ModelAndView updateAsignacion(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");
            String idCeco = request.getParameter("idCeco");
            String idGerente = request.getParameter("idGerente");
            String activo = request.getParameter("activo");
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");
            int respuesta = asignacionesSupBI.updateAsignacion(Integer.parseInt(idUsuario), Integer.parseInt(idPerfil), idCeco, Integer.parseInt(idGerente),
                    Integer.parseInt(activo), fechaInicio, fechaFin);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualiza Asignacion");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("updateAsignacion()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/deleteAsignacion.json?idUsuario=?&idCeco=?
    @RequestMapping(value = "/deleteAsignacion", method = RequestMethod.GET)
    public ModelAndView deleteAsignacion(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idCeco = request.getParameter("idCeco");
            int respuesta = asignacionesSupBI.deleteAsignacion(Integer.parseInt(idUsuario), idCeco);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Elimina Asignacion");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("deleteAsignacion()");
            return null;
        }
    }

    /*Todos los parámetros de entrada son valores opcionales, si no se envía valor  en alguno de ellos para filtrar consulta,
     se mostrarán todos los registros de la tabla*/
    //http://10.51.218.72:8080/checklist/consultaCatalogosService/getAsignacion.json?idUsuario=?&idPerfil=?&idUsuario=?&idCeco=?&idGerente=?&activo=?&fechaInicio=?&fechaFin=?
    @RequestMapping(value = "/getAsignacion", method = RequestMethod.GET)
    public ModelAndView getAsignacion(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");
            String idCeco = request.getParameter("idCeco");
            String idGerente = request.getParameter("idGerente");
            String activo = request.getParameter("activo");
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");
            List<AsignacionesSupDTO> respuesta = asignacionesSupBI.getAsignaciones(idUsuario, idPerfil, idCeco, idGerente, activo, fechaInicio, fechaFin);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Get Asignaciones");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("getAsignacion()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/updateStatus.json
    @RequestMapping(value = "/updateStatus", method = RequestMethod.GET)
    public ModelAndView updateStatus(HttpServletRequest request, HttpServletResponse response) {
        try {
            int respuesta = asignacionesSupBI.updateStatus();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Update Status");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("updateStatus()");
            return null;
        }
    }

    /* idGerente ID de jefe asignado, si es gerente enviar null, en activo, si se quiere eliminar el registro este valor
     debe ser 0, si se agrega una nueva asignación el valor debe ser 1*/
    //http://10.51.218.72:8080/checklist/consultaCatalogosService/mergeAsignacion.json?idUsuario=?&idPerfil=?&idUsuario=?&idCeco=?&idGerente=?&activo=?&fechaInicio=?&fechaFin=?
    @RequestMapping(value = "/mergeAsignacion", method = RequestMethod.GET)
    public ModelAndView mergeAsignacion(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");
            String idCeco = request.getParameter("idCeco");
            String idGerente = request.getParameter("idGerente");
            String activo = request.getParameter("activo");
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");
            int respuesta = asignacionesSupBI.mergeAsignacion(Integer.parseInt(idUsuario), Integer.parseInt(idPerfil), idCeco, idGerente,
                    Integer.parseInt(activo), fechaInicio, fechaFin);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Merge Asignacion");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("mergeAsignacion()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/insertPerfil.json?idPerfil=?&descPerfil=?&activo=?
    @RequestMapping(value = "/insertPerfil", method = RequestMethod.GET)
    public ModelAndView insertPerfil(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idPerfil = request.getParameter("idPerfil");
            String descPerfil = request.getParameter("descPerfil");
            String activo = request.getParameter("activo");
            int respuesta = perfilesSupBI.insertaPerfil(Integer.parseInt(idPerfil), descPerfil, activo);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Inserta Perfil");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("insertPerfil()");
            return null;
        }
    }

    //Solo enviar los valores a actualizar, los demas enviarlos en null
    //http://10.51.218.72:8080/checklist/consultaCatalogosService/updatePerfil.json?idPerfil=?&descPerfil=?&activo=?
    @RequestMapping(value = "/updatePerfil", method = RequestMethod.GET)
    public ModelAndView updatePerfil(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idPerfil = request.getParameter("idPerfil");
            String descPerfil = request.getParameter("descPerfil");
            String activo = request.getParameter("activo");
            int respuesta = perfilesSupBI.updatePerfil(Integer.parseInt(idPerfil), descPerfil, activo);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualiza Perfil");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("updatePerfil()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/deletePerfil.json?idPerfil=?
    @RequestMapping(value = "/deletePerfil", method = RequestMethod.GET)
    public ModelAndView deletePerfil(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idPerfil = request.getParameter("idPerfil");
            int respuesta = perfilesSupBI.deletePerfil(Integer.parseInt(idPerfil));
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Elimina Perfil");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("deletePerfil()");
            return null;
        }
    }

    //el idPerfil es un valor opcional, si no se envía valor se mostrarán todos los registros de la tabla
    //http://10.51.218.72:8080/checklist/consultaCatalogosService/getPerfil.json?idPerfil=?
    @RequestMapping(value = "/getPerfil", method = RequestMethod.GET)
    public ModelAndView getPerfil(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idPerfil = request.getParameter("idPerfil");
            List<PerfilesSupDTO> respuesta = perfilesSupBI.getPerfil(idPerfil);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Get Perfiles");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("getPerfil()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/insertMenu.json?idMenu=?&descMenu=?&activo=?
    @RequestMapping(value = "/insertMenu", method = RequestMethod.GET)
    public ModelAndView insertMenu(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idMenu = request.getParameter("idMenu");
            String descMenu = request.getParameter("descMenu");
            String activo = request.getParameter("activo");
            int respuesta = menusSupBI.insertaMenu(Integer.parseInt(idMenu), descMenu, Integer.parseInt(activo));
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Inserta Menu");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("insertMenu()");
            return null;
        }
    }

    //Solo enviar los valores a actualizar, los demas enviarlos en null
    //http://10.51.218.72:8080/checklist/consultaCatalogosService/updateMenu.json?idMenu=?&descMenu=?&activo=?
    @RequestMapping(value = "/updateMenu", method = RequestMethod.GET)
    public ModelAndView updateMenu(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idMenu = request.getParameter("idMenu");
            String descMenu = request.getParameter("descMenu");
            String activo = request.getParameter("activo");
            int respuesta = menusSupBI.updateMenu(Integer.parseInt(idMenu), descMenu, activo);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualiza Menu");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("updateMenu()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/deleteMenu.json?idMenu=?
    @RequestMapping(value = "/deleteMenu", method = RequestMethod.GET)
    public ModelAndView deleteMenu(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idMenu = request.getParameter("idMenu");
            int respuesta = menusSupBI.deleteMenu(Integer.parseInt(idMenu));
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Elimina Menu");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("deleteMenu()");
            return null;
        }
    }

    //el idMenu es un valor opcional, si no se envía valor se mostrarán todos los registros de la tabla
    //http://10.51.218.72:8080/checklist/consultaCatalogosService/getMenus.json?idMenu=?
    @RequestMapping(value = "/getMenus", method = RequestMethod.GET)
    public ModelAndView getMenus(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idMenu = request.getParameter("idMenu");
            List<MenusSupDTO> respuesta = menusSupBI.getMenus(idMenu);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Get Menus");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("getMenus()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/insertRol.json?idUsuario=?&idPerfil=?&idMenu=?&activo=?
    @RequestMapping(value = "/insertRol", method = RequestMethod.GET)
    public ModelAndView insertRol(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");
            String idMenu = request.getParameter("idMenu");
            String activo = request.getParameter("activo");
            int respuesta = rolesSupBI.insertRol(Integer.parseInt(idUsuario), Integer.parseInt(idPerfil), Integer.parseInt(idMenu), Integer.parseInt(activo));
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Inserta Rol");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("insertRol()");
            return null;
        }
    }

    //activo es el unico valor que puede ir null
    //http://10.51.218.72:8080/checklist/consultaCatalogosService/updateRol.json?idUsuario=?&idPerfil=?&idMenu=?&activo=?
    @RequestMapping(value = "/updateRol", method = RequestMethod.GET)
    public ModelAndView updateRol(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");
            String idMenu = request.getParameter("idMenu");
            String activo = request.getParameter("activo");
            int respuesta = rolesSupBI.updateRol(Integer.parseInt(idUsuario), Integer.parseInt(idPerfil), Integer.parseInt(idMenu), activo);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualiza Rol");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("updateRol()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/deleteRol.json?idUsuario=?&idPerfil=?&idMenu=?
    @RequestMapping(value = "/deleteRol", method = RequestMethod.GET)
    public ModelAndView deleteRol(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");
            String idMenu = request.getParameter("idMenu");
            int respuesta = rolesSupBI.deleteRol(Integer.parseInt(idUsuario), Integer.parseInt(idPerfil), Integer.parseInt(idMenu));
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Elimina Rol");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("deleteRol()");
            return null;
        }
    }

    //el idUsuario, idPerfil y idMenu son un valor opcional, si no se envía valor, se mostrarán todos los registros de la tabla
    //http://10.51.218.72:8080/checklist/consultaCatalogosService/getRoles.json?idUsuario=?&idPerfil=?&idMenu=?
    @RequestMapping(value = "/getRoles", method = RequestMethod.GET)
    public ModelAndView getRoles(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");
            String idMenu = request.getParameter("idMenu");
            List<RolesSupDTO> respuesta = rolesSupBI.getRoles(idUsuario, idPerfil, idMenu);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Get Roles");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("getRoles()");
            return null;
        }
    }

    //Todos los campos son obligatorios
    //http://10.51.218.72:8080/checklist/consultaCatalogosService/mergeRoles.json?idUsuario=?&idPerfil=?&idMenu=?&activo=?
    @RequestMapping(value = "/mergeRoles", method = RequestMethod.GET)
    public ModelAndView mergeRoles(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");
            String idMenu = request.getParameter("idMenu");
            String activo = request.getParameter("activo");
            int respuesta = rolesSupBI.mergeRol(Integer.parseInt(idUsuario), Integer.parseInt(idPerfil), Integer.parseInt(idMenu), Integer.parseInt(activo));
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Merge Roles");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("mergeRoles()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/getEmpleado.json?idUsuario=?
    @RequestMapping(value = "/getEmpleado", method = RequestMethod.GET)
    public ModelAndView getEmpleado(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            List<AdminSupDTO> respuesta = adminSupBI.busquedaEmpleado(Integer.parseInt(idUsuario));
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Get Empleado");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("getEmpleado()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/getDetalleMenu.json?idUsuario=?
    @RequestMapping(value = "/getDetalleMenu", method = RequestMethod.GET)
    public ModelAndView getDetalleMenu(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            Map<String, Object> respuesta = adminSupBI.detalleMenu(Integer.parseInt(idUsuario));
            List<AdminSupDTO> detalle = null;
            List<AdminSupDTO> menus = null;
            Iterator<Map.Entry<String, Object>> enMap = respuesta.entrySet().iterator();
            while (enMap.hasNext()) {
                Map.Entry<String, Object> enMaps = enMap.next();
                String nomH = enMaps.getKey();
                if (nomH == "detalle") {
                    detalle = (List<AdminSupDTO>) enMaps.getValue();
                }
                if (nomH == "menus") {
                    menus = (List<AdminSupDTO>) enMaps.getValue();
                }
            }

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "GetDetalleMenu");
            mv.addObject("res", detalle);
            mv.addObject("res2", menus);

            return mv;
        } catch (Exception e) {
            logger.info("getDetalleMenu()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/getPersonalAsig.json?idUsuario=?
    @RequestMapping(value = "/getPersonalAsig", method = RequestMethod.GET)
    public ModelAndView getPersonalAsig(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            Map<String, Object> respuesta = adminSupBI.personalAsig(Integer.parseInt(idUsuario));
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Get Personal Asignado");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("getPersonalAsig()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/getCecosAsig.json?idUsuario=?
    @RequestMapping(value = "/getCecosAsig", method = RequestMethod.GET)
    public ModelAndView getCecosAsig(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            Map<String, Object> respuesta = adminSupBI.cecosAsig(Integer.parseInt(idUsuario));
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Get Cecos Asignados");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("getCecosAsig()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/buscaCeco.json?ceco=?
    @RequestMapping(value = "/buscaCeco", method = RequestMethod.GET)
    public ModelAndView buscaCeco(HttpServletRequest request, HttpServletResponse response) {
        try {
            String ceco = request.getParameter("ceco");
            List<AdminSupDTO> respuesta = adminSupBI.busquedaCeco(ceco);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Busqueda Ceco");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info("buscaCeco()");
            return null;
        }
    }

    /**
     * ************************************************************** Fin
     * Administrador de perfiles Supervision
     * ***************************************************************
     */
    // sino se le manda el numero de id trae todo lo de la tabla
    // http://localhost:8080/checklist/consultaCatalogosService/getAdicionalCeco.json?ceco=
    @RequestMapping(value = "/getAdicionalCeco", method = RequestMethod.GET)
    public ModelAndView getAdicionalCeco(HttpServletRequest request, HttpServletResponse response) {
        try {
            String ceco = request.getParameter("ceco");

            List<AdicionalesDTO> lista = adicionalesBI.obtieneDatos(ceco);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ADICION");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getAdicionalCeco()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getAdicionalBita.json?bita=&idPreg=

    @RequestMapping(value = "/getAdicionalBita", method = RequestMethod.GET)
    public ModelAndView getAdicionalBita(HttpServletRequest request, HttpServletResponse response) {
        try {
            String bita = request.getParameter("bita");
            String idPreg = request.getParameter("idPreg");

            List<AdicionalesDTO> lista = adicionalesBI.obtieneDatosBita(bita, idPreg);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ADICION");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getAdicionalBita()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getAdicionalDeta.json?
    @RequestMapping(value = "/getAdicionalDeta", method = RequestMethod.GET)
    public ModelAndView getAdicionalDeta(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<AdicionalesDTO> lista = adicionalesBI.obtieneInfo();

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ADICION");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getAdicionalDeta()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getAdicionalBitaHis.json?bita=
    @RequestMapping(value = "/getAdicionalBitaHis", method = RequestMethod.GET)
    public ModelAndView getAdicionalBitaHis(HttpServletRequest request, HttpServletResponse response) {
        try {
            String bita = request.getParameter("bita");

            List<AdicionalesDTO> lista = adicionalesBI.obtieneDatosHist(bita);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ADICION");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getAdicionalBitaHis()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getAdicionalDetaHist.json?
    @RequestMapping(value = "/getAdicionalDetaHist", method = RequestMethod.GET)
    public ModelAndView getAdicionalDetaHist(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<AdicionalesDTO> lista = adicionalesBI.obtieneInfoHist();

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ADICION");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getAdicionalDetaHist()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getContGenExp.json?bita=&clasifica=

    @RequestMapping(value = "/getContGenExp", method = RequestMethod.GET)
    public @ResponseBody
    String getContGenExp(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {

            String bita = request.getParameter("bita");
            String clasifica = request.getParameter("clasifica");

            String res = conteoGenExpBI.obtieneDatos(bita, clasifica);

            return res;

        } catch (Exception e) {
            logger.info("getContGenExp()");
            return null;
        }

    }
    // http://localhost:8080/checklist/consultaCatalogosService/getSucCheckNego.json?ceco=

    @RequestMapping(value = "/getSucCheckNego", method = RequestMethod.GET)
    public ModelAndView getSucCheckNego(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = "null";
            ceco = request.getParameter("ceco");

            List<SucursalChecklistDTO> res = sucursalChecklistBI.obtieneDatosSuc(ceco);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(res);
            mv.addObject("tipo", "SUCCHEK");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info("getSucCheckNego()");
            return null;
        }

    }
    // http://localhost:8080/checklist/consultaCatalogosService/gethallaXCecoRecorridoNvo.json?ceco=<?>

    @RequestMapping(value = "/gethallaXCecoRecorridoNvo", method = RequestMethod.GET)
    public ModelAndView gethallaXCecoRecorridoNvo(HttpServletRequest request, HttpServletResponse response) {
        try {

            int ceco = Integer.parseInt(request.getParameter("ceco"));

            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneInfoHallazNvo(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "hallaceco");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("XCecoRecorridoNvo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/gethallaXCecoTabNvo.json?ceco=<?>
    @RequestMapping(value = "/gethallaXCecoTabNvo", method = RequestMethod.GET)
    public ModelAndView gethallaXCecoTabNvo(HttpServletRequest request, HttpServletResponse response) {
        try {

            int ceco = Integer.parseInt(request.getParameter("ceco"));

            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneInfoHallazTabNvo(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "hallaceco");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("XCecoTabNvo()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getidGerentes.json?idGerente=331952
    @RequestMapping(value = "/getidGerentes", method = RequestMethod.GET)
    public ModelAndView getidGerentes(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idGerente = Integer.parseInt(request.getParameter("idGerente"));
            AltaGerenteDTO getIdGerentes = new AltaGerenteDTO();
            getIdGerentes.setIdGerente(idGerente);
            List<AltaGerenteDTO> lista = altaGerenteBI.obtieneIdUsuarios(getIdGerentes);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "GETIDUSUARIOS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getidGerentes()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getinfDisSup.json?idGerente=331952&idUsuario=176361
    @RequestMapping(value = "/getinfDisSup", method = RequestMethod.GET)
    public ModelAndView getinfDisSup(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idGerente2 = request.getParameter("idGerente");
            String idUsuario2 = request.getParameter("idUsuario");

            List<AltaGerenteDTO> lista = altaGerenteBI.obtienefullTable(idUsuario2, idGerente2);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "GETINFORMACION");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getinfDisSup()");
            return null;
        }
    }

    //http://10.51.219.179:8080/checklist/consultaCatalogosService/getMedicionPreguntas.json?idProtocolo=21
    @RequestMapping(value = "/getMedicionPreguntas", method = RequestMethod.GET)
    public ModelAndView getMedicionPreguntas(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idProtocolo = Integer.parseInt(request.getParameter("idProtocolo"));
            RepMedicionDTO a1 = new RepMedicionDTO();
            a1.setIdProtocolo(idProtocolo);
            List<RepMedicionDTO> resp = repMedicionBI.ReportePreguntas(a1);
            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "GETPREGMEDICION");
            mv.addObject("res", resp);

            return mv;
        } catch (Exception e) {
            logger.info("getMedicionPreguntas()");
            return null;
        }
    }

    //http://10.51.218.72:8080/checklist/consultaCatalogosService/getReporteProtocolos.json?idProtocolo=?&fechaInicio=?&fechaFin=?
    @RequestMapping(value = "/getReporteProtocolos", method = RequestMethod.GET)
    public ModelAndView getReporteProtocolos(HttpServletRequest request, HttpServletResponse response) {
        try {
            long startTime = System.currentTimeMillis();
            String idProtocolo = request.getParameter("idProtocolo");
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");
            List<RepMedicionDTO> respuesta = repMedicionBI.ReporteMedZonas(Integer.parseInt(idProtocolo), fechaInicio, fechaFin);

            long endTime = System.currentTimeMillis() - startTime;

            System.out.println("Tiempo reporte en Base: " + (double) endTime / 1000);

            long startTime2 = System.currentTimeMillis();
            //logger.info("RESPUESTA: "+respuesta);

            RepMedicionDTO r = new RepMedicionDTO();
            r.setIdProtocolo(Integer.parseInt(idProtocolo));
            List<RepMedicionDTO> pregunta = repMedicionBI.ReportePreguntas(r);

            /*List<RepMedicionDTO> respuesta2 = new ArrayList<RepMedicionDTO>();
             for(int i=0;i<500;i++) {
             for(RepMedicionDTO aux:respuesta) {
             respuesta2.add(aux);
             }
             }
             respuesta=respuesta2;*/
            String[] arr = new String[pregunta.size()];
            List<String> res = new ArrayList<String>();
            if (respuesta != null) {
                for (RepMedicionDTO reporte : respuesta) {
                    arr = new String[pregunta.size()];

                    for (int i = 0; i < arr.length; i++) {
                        arr[i] = "";
                    }

                    int apuntador = 0;
                    for (RepMedicionDTO aux : pregunta) {
                        String[] arrTemp = reporte.getDescRespuesta().split("#####");

                        for (int i = 0; i < arrTemp.length; i++) {
                            String[] resp = arrTemp[i].split("====");
                            int idPreg = Integer.parseInt(resp[0]);
                            if (idPreg == aux.getIdPregunta()) {
                                arr[apuntador] = resp[1];

                            }
                        }
                        apuntador++;
                    }

                    res.add(Arrays.toString(arr));
                }

                //logger.info("RES: "+res);
                ModelAndView mv = new ModelAndView("muestraServicios");
                mv.addObject("tipo", "RepMedProtocolos");
                mv.addObject("preg", pregunta);
                mv.addObject("resp", respuesta);
                mv.addObject("respSplit", res);
                long endTime2 = System.currentTimeMillis() - startTime2;

                System.out.println("Tiempo reporte listo para WEB: " + (double) endTime2 / 1000);

                return mv;
            } else {
                return null;
            }

        } catch (Exception e) {
            logger.info("getReporteProtocolos()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/gethallazgoRepoListasNvo.json?bitgral=

    @RequestMapping(value = "/gethallazgoRepoListasNvo", method = RequestMethod.GET)
    public @ResponseBody
    String gethallazgoRepoListasNvo(HttpServletRequest request, HttpServletResponse response) {

        try {
            //String bitgral="";

            String bitgral = request.getParameter("bitgral");

            String res = hallazgosRepoBI.cursoresNvo(bitgral);
            return res;
        } catch (Exception e) {
            logger.info("zgoRepoListasNvo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService//getBitaNego.json?bita=<?>
    @RequestMapping(value = "/getBitaNego", method = RequestMethod.GET)
    public ModelAndView getBitaNego(HttpServletRequest request, HttpServletResponse response) {
        try {

            String bita = request.getParameter("bita");

            List<ChecklistHallazgosRepoDTO> lista = hallazgosRepoBI.obtieneDatosNego(bita);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "NEGOBIT");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getBitaNego()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getRepoFechasExpNvo.json?&fechaIni=<?>&fechaFin=<?>
    @RequestMapping(value = "/getRepoFechasExpNvo", method = RequestMethod.GET)
    public ModelAndView getRepoFechasExpNvo(HttpServletRequest request, HttpServletResponse response) {
        try {

            String fechaIni = request.getParameter("fechaIni");
            String fechaFin = request.getParameter("fechaFin");

            RepoFilExpDTO re = new RepoFilExpDTO();

            re.setFechaIni(fechaIni);
            re.setFechaFin(fechaFin);

            List<RepoFilExpDTO> lista = repoFilExpBI.repoFechasNvo(fechaIni, fechaFin);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "REPOFECHAEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getRepoFechasExpNvo()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getDatosRepoSema.json?bitaGral=

    @RequestMapping(value = "/getDatosRepoSema", method = RequestMethod.GET)
    public @ResponseBody
    String getDatosRepoSema(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {
            String bitaGral = request.getParameter("bitaGral");

            String res = conteoGenExpBI.obtieneDatosRepoSem(bitaGral);

            return res;

        } catch (Exception e) {
            logger.info("getDatosRepoSema()");
            return null;
        }

    }

    // http://localhost:8080/checklist/consultaCatalogosService/getComentariosRepAsg.json?ceco=<?>&fecha=<?>&anio=<?>&mes=<?>
    @RequestMapping(value = "/getComentariosRepAsg", method = RequestMethod.GET)
    public @ResponseBody
    String getComentariosRepAsg(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {

            String ceco = request.getParameter("ceco");
            String fecha = request.getParameter("fecha");
            Integer anio = (request.getParameter("anio") != null ? Integer.parseInt(request.getParameter("anio")) : null);
            Integer mes = (request.getParameter("mes") != null ? Integer.parseInt(request.getParameter("mes")) : null);

            List<ComentariosRepAsgDTO> res = comentariosRepAsgBI.getComentarios(ceco, fecha, anio, mes);

            Gson gson = null;
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.serializeNulls();
            gson = gsonBuilder.create();
            String jsonSalida = gson.toJson(res);

            return jsonSalida;

        } catch (Exception e) {
            logger.info("getComentariosRepAsg()");
            return null;
        }

    }
    // http://localhost:8080/checklist/consultaCatalogosService/gethallazgosCecoTransf.json?&ceco=<?>&proyecto&fase=

    @RequestMapping(value = "/gethallazgosCecoTransf", method = RequestMethod.GET)
    public ModelAndView gethallazgosCecoTransf(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = request.getParameter("ceco");
            String fase = request.getParameter("fase");
            String proyecto = request.getParameter("proyecto");

            List<HallazgosTransfDTO> lista = hallazgosTransfBI.obtieneDatos(ceco, proyecto, fase);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSTRANSF");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("zgosCecoTransf()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getHallazgoHisBitaTransf.json?bita=

    @RequestMapping(value = "/getHallazgoHisBitaTransf", method = RequestMethod.GET)
    public ModelAndView getHallazgoHisBitaTransf(HttpServletRequest request, HttpServletResponse response) {
        try {

            String bita = request.getParameter("bita");
            List<HallazgosTransfDTO> lista = hallazgosTransfBI.obtieneHistBita(bita);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSTRANSF");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("zgoHisBitaTransf()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getHallazgoHisFolioTransf.json?idFolio=
    @RequestMapping(value = "/getHallazgoHisFolioTransf", method = RequestMethod.GET)
    public ModelAndView getHallazgoHisFolioTransf(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idFolio = Integer.parseInt(request.getParameter("idFolio"));
            List<HallazgosTransfDTO> lista = hallazgosTransfBI.obtieneHistFolio(idFolio);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSTRANSF");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("zgoHisFolioTransf()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/gethallazgosEviBitaTransf.json?proyecto=&fase=&ceco=

    @RequestMapping(value = "/gethallazgosEviBitaTransf", method = RequestMethod.GET)
    public ModelAndView gethallazgosEviBitaTransf(HttpServletRequest request, HttpServletResponse response) {
        try {

            int proyecto = Integer.parseInt(request.getParameter("proyecto"));
            int fase = Integer.parseInt(request.getParameter("fase"));
            int ceco = Integer.parseInt(request.getParameter("ceco"));

            List<HallazgosTransfDTO> lista = hallazgosTransfBI.obtieneDatosBita(proyecto, fase, ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSEVIEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("gethallazgosEviBitaTransf()");
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/consultaCatalogosService/getInforme.json
    @RequestMapping(value = "/getInforme", method = RequestMethod.GET)
    public ModelAndView getInforme(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<AdmInformProtocolosDTO> lista = admInformProtocolosBI.obtieneTodos();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "INFORME");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getInforme()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCursoresTransf.json?usuario=
    @RequestMapping(value = "/getCursoresTransf", method = RequestMethod.GET)
    public @ResponseBody
    String getCursoresTransf(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {

            String usuario = "";
            usuario = request.getParameter("usuario");
            String res = asignaTransfBI.obtieneDatosCursores(usuario);

            return res;

        } catch (Exception e) {
            logger.info("getCursoresTransf()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getFasesTrans.json?proyecto=
    @RequestMapping(value = "/getFasesTrans", method = RequestMethod.GET)
    public ModelAndView getFasesTrans(HttpServletRequest request, HttpServletResponse response) {
        try {

            String proyecto = request.getParameter("proyecto");

            List<FaseTransfDTO> lista = faseTransfBI.obtieneDatos(proyecto);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "FASEAGRU");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getFasesTrans()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getFasesAgrupa.json?idAgrupa=
    @RequestMapping(value = "/getFasesAgrupa", method = RequestMethod.GET)
    public ModelAndView getFasesAgrupa(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idAgrupa = request.getParameter("idAgrupa");

            List<FaseTransfDTO> lista = faseTransfBI.obtieneDatosAgrupa(idAgrupa);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "FASEAGRU");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getFasesAgrupa()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getAsignacionTransf.json?idProyecto=
    @RequestMapping(value = "/getAsignacionTransf", method = RequestMethod.GET)
    public ModelAndView getAsignacionTransf(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idProyecto = null;
            idProyecto = request.getParameter("idProyecto");

            List<AsignaTransfDTO> lista = asignaTransfBI.obtieneDatos(idProyecto);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ASIGNTRANSF");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getAsignacionTransf()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCursoresTransfCeco.json?ceco=
    @RequestMapping(value = "/getCursoresTransfCeco", method = RequestMethod.GET)
    public @ResponseBody
    String getCursoresTransfCeco(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {

            String ceco = "";
            ceco = request.getParameter("ceco");
            String res = asignaTransfBI.obtieneDatosCursoresCeco(ceco);

            return res;

        } catch (Exception e) {
            logger.info("getCursoresTransfCeco()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/gethallazgosRepoTransf.json?ceco=&proyecto=&fase=
    @RequestMapping(value = "/gethallazgosRepoTransf", method = RequestMethod.GET)
    public ModelAndView gethallazgosRepoTransf(HttpServletRequest request, HttpServletResponse response) {
        try {

            String proyecto = request.getParameter("proyecto");
            String fase = "";
            fase = request.getParameter("fase");
            String ceco = request.getParameter("ceco");

            List<HallazgosTransfDTO> lista = hallazgosTransfBI.obtieneDatosRepo(ceco, proyecto, fase);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSTRANSREPO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getProyecto.json?proyecto=
    @RequestMapping(value = "/getProyecto", method = RequestMethod.GET)
    public ModelAndView getProyecto(HttpServletRequest request, HttpServletResponse response) {
        try {

            String proyecto = request.getParameter("proyecto");

            List<ProyectFaseTransfDTO> lista = proyectFaseTransfBI.obtieneDatosProyec(proyecto);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "PROYECTO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getFasesTrans()");
            return null;
        }
    }
    //localhost:8080/checklist/consultaCatalogosService/getDashGenericoNvo.json?bita=<?>&parametro=

    @RequestMapping(value = "/getDashGenericoNvo", method = RequestMethod.GET)
    public ModelAndView getDashGenericoNvo(HttpServletRequest request, HttpServletResponse response) {
        try {
            String bita = request.getParameter("bita");
            String parametro = null;
            parametro = request.getParameter("parametro");

            List<RepoFilExpDTO> lista = repoFilExpBI.obtieneInfoDashboardGenerico(bita, parametro);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "DASHEXPREPONVO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("dashNuevo");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getOffline.json?idUsu=<?>&idCheck=

    @RequestMapping(value = "/getOffline", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getOffline(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idUsu = Integer.parseInt(request.getParameter("idUsu"));
            int idCheck = Integer.parseInt(request.getParameter("idCheck"));
            Map<String, Object> res = checksOfflineBI.obtieneChecksNuevo3(idUsu, idCheck);

            // System.out.println(res);
            return res;

        } catch (Exception e) {
            logger.info("getResp()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCatalogoFirmas.json?idCatalogoFirmas=
    @RequestMapping(value = "/getCatalogoFirmas", method = RequestMethod.GET)
    public ModelAndView getCatalogoFirmas(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idCatalogoFirmas = null;
            idCatalogoFirmas = request.getParameter("idCatalogoFirmas");

            List<AdmFirmasCatDTO> lista = firmasCatalogoBI.obtieneDatosFirmaCat(idCatalogoFirmas);
            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "FIRMASCATALO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("CATFIRMAS");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getAgrupaFirmas.json?idAgrupaFirmas=
    @RequestMapping(value = "/getAgrupaFirmas", method = RequestMethod.GET)
    public ModelAndView getAgrupaFirmas(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idAgrupaFirmas = null;
            idAgrupaFirmas = request.getParameter("idAgrupaFirmas");

            List<AdmFirmasCatDTO> lista = firmasCatalogoBI.obtieneDatosFirmaAgrupa(idAgrupaFirmas);
            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "FIRMASCATALO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getFasesTrans()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getFragment.json?idFragment=
    @RequestMapping(value = "/getFragment", method = RequestMethod.GET)
    public ModelAndView getFragment(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idFragment = null;
            idFragment = request.getParameter("idFragment");

            List<FragmentMenuDTO> lista = fragmentMenuBI.obtieneDatosFragment(idFragment);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "FRAGMENU");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getFasesTrans()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getMenuOpcPerfil.json?idMenuOpcionesPerf=

    @RequestMapping(value = "/getMenuOpcPerfil", method = RequestMethod.GET)
    public ModelAndView getMenuOpcPerfil(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idMenuOpcionesPerf = null;
            idMenuOpcionesPerf = request.getParameter("idMenuOpcionesPerf");

            List<FragmentMenuDTO> lista = fragmentMenuBI.obtieneDatosMenu(idMenuOpcionesPerf);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "FRAGMENU");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getFasesTrans()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getConfProyecto.json?idConfProyecto=
    @RequestMapping(value = "/getConfProyecto", method = RequestMethod.GET)
    public ModelAndView getConfProyecto(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idConfProyecto = null;
            idConfProyecto = request.getParameter("idConfProyecto");

            List<FragmentMenuDTO> lista = fragmentMenuBI.obtieneDatosConf(idConfProyecto);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "FRAGMENU");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getFasesTrans()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getFiltro1CecoFaseProy.json?ceco=<?>&fase=<?>&proyecto=&nombreCeco=<?>&idRecorrido=&fechaIni=<?>&fechaFin=<?>

    @RequestMapping(value = "/getFiltro1CecoFaseProy", method = RequestMethod.GET)
    public ModelAndView getFiltro1CecoFaseProy(HttpServletRequest request, HttpServletResponse response) {
        try {
            String ceco = request.getParameter("ceco");
            String fase = request.getParameter("fase");
            String proyecto = request.getParameter("proyecto");
            String nombreCeco = request.getParameter("nombreCeco");
            String fechaIni = request.getParameter("fechaIni");
            String fechaFin = request.getParameter("fechaFin");
            String idRecorrido = request.getParameter("idRecorrido");

            RepoFilExpDTO re = new RepoFilExpDTO();

            re.setCeco(ceco);
            re.setFase(fase);
            re.setProyecto(proyecto);
            re.setNombreCeco(nombreCeco);
            re.setFechaIni(fechaIni);
            re.setFechaFin(fechaFin);
            re.setIdRecorrido(idRecorrido);

            List<RepoFilExpDTO> lista = repoFilExpBI.obtieneDatosFiltro1(ceco, fase, proyecto, nombreCeco, idRecorrido, fechaIni, fechaFin);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "filtro1CecoFase");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getFiltro1CecoFaseProy()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getFiltro1Cursores.json?ceco=<?>&fase=<?>&proyecto=&nombreCeco=<?>&idRecorrido=&fechaIni=<?>&fechaFin=<?>
    @RequestMapping(value = "/getFiltro1Cursores", method = RequestMethod.GET)
    public @ResponseBody
    String getFiltro1Cursores(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {
            String ceco = request.getParameter("ceco");
            String fase = request.getParameter("fase");
            String proyecto = request.getParameter("proyecto");
            String nombreCeco = request.getParameter("nombreCeco");
            String fechaIni = request.getParameter("fechaIni");
            String fechaFin = request.getParameter("fechaFin");
            String idRecorrido = request.getParameter("idRecorrido");

            RepoFilExpDTO re = new RepoFilExpDTO();

            re.setCeco(ceco);
            re.setFase(fase);
            re.setProyecto(proyecto);
            re.setNombreCeco(nombreCeco);
            re.setFechaIni(fechaIni);
            re.setFechaFin(fechaFin);
            re.setIdRecorrido(idRecorrido);
            String res = repoFilExpBI.obtieneDatosFiltro1Fase(ceco, fase, proyecto, nombreCeco, idRecorrido, fechaIni, fechaFin);

            return res;

        } catch (Exception e) {
            logger.info("getFiltro1Cursores()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getSoftNvo.json?ceco=
    @RequestMapping(value = "/getSoftNvo", method = RequestMethod.GET)
    public ModelAndView getSoftNvo(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = null;
            ceco = request.getParameter("ceco");

            List<SoftNvoDTO> lista = softNvoBI.obtieneDatosSoftN(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "SOFTOPNVO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getSoftNvo()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getSoftCalculo.json?idSoft=

    @RequestMapping(value = "/getSoftCalculo", method = RequestMethod.GET)
    public ModelAndView getSoftCalculo(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idSoft = null;
            idSoft = request.getParameter("idSoft");

            List<SoftNvoDTO> lista = softNvoBI.obtieneDatosCalculosActa(idSoft);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "CALCULOSSOFT");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getSoftCalculo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getActorHallzgo.json?idActor=
    @RequestMapping(value = "/getActorHallzgo", method = RequestMethod.GET)
    public ModelAndView getActorHallzgo(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idActor = null;
            idActor = request.getParameter("idActor");

            List<ActorEdoHallaDTO> lista = actorEdoHallaBI.obtieneDatosActorHalla(idActor);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ACTOREDOHALLA");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getActorHallzgo()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getEdoGpoHallazgos.json?idEdoHalla=

    @RequestMapping(value = "/getEdoGpoHallazgos", method = RequestMethod.GET)
    public ModelAndView getEdoGpoHallazgos(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idEdoHalla = null;
            idEdoHalla = request.getParameter("idEdoHalla");

            List<ActorEdoHallaDTO> lista = actorEdoHallaBI.obtieneDatosConfEdoHalla(idEdoHalla);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ACTOREDOHALLA");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getEdoGpoHallazgos()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getConfActorHalla.json?idConfig=
    @RequestMapping(value = "/getConfActorHalla", method = RequestMethod.GET)
    public ModelAndView getConfActorHalla(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idConfig = null;
            idConfig = request.getParameter("idConfig");

            List<ConfiguraActorDTO> lista = configuraActorBI.obtieneDatosConfActor(idConfig);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "CONFACTORHALLA");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getConfActorHalla()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getSoftNvoDeta.json?ceco=

    @RequestMapping(value = "/getSoftNvoDeta", method = RequestMethod.GET)
    public ModelAndView getSoftNvoDeta(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = null;
            ceco = request.getParameter("ceco");

            List<SoftNvoDTO> lista = softNvoBI.obtieneDatosSoftNDeta(ceco);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "SOFTOPNVO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getdetallesoftnvo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getDashNvoCecoFase.json?ceco=&proyecto=&fase=
    @RequestMapping(value = "/getDashNvoCecoFase", method = RequestMethod.GET)
    public ModelAndView getDashNvoCecoFase(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = null;
            ceco = request.getParameter("ceco");
            String fase = null;
            fase = request.getParameter("fase");
            String proyecto = null;
            proyecto = request.getParameter("proyecto");

            List<RepoFilExpDTO> lista = repoFilExpBI.obtienedashCecoProyecto(ceco, fase, proyecto);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "DASHEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getdetallesoftnvo()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getHallazgoHisCecoFaseProy.json?ceco=&proyecto=&fase=
    @RequestMapping(value = "/getHallazgoHisCecoFaseProy", method = RequestMethod.GET)
    public ModelAndView getHallazgoHisCecoFaseProy(HttpServletRequest request, HttpServletResponse response) {
        try {

            String proyecto = request.getParameter("proyecto");
            String fase = request.getParameter("fase");
            String ceco = request.getParameter("ceco");

            List<HallazgosTransfDTO> lista = hallazgosTransfBI.obtieneHistoricoHallazgoDash(ceco, proyecto, fase);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSTRANSF");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("HALLAZGOHisTransf()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getHallazgoExpansCecoFaseProy.json?ceco=&proyecto=&fase=

    @RequestMapping(value = "/getHallazgoExpansCecoFaseProy", method = RequestMethod.GET)
    public ModelAndView getHallazgoExpansCecoFaseProy(HttpServletRequest request, HttpServletResponse response) {
        try {

            String proyecto = request.getParameter("proyecto");
            String fase = request.getParameter("fase");
            String ceco = request.getParameter("ceco");

            List<HallazgosTransfDTO> lista = hallazgosTransfBI.obtieneHallazgosExpansion(ceco, proyecto, fase);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSTRANSF");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("HALLAZGODASHBOARD()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getHallazgoExpansCecoFaseProyIni.json?ceco=&proyecto=&fase=

    @RequestMapping(value = "/getHallazgoExpansCecoFaseProyIni", method = RequestMethod.GET)
    public ModelAndView getHallazgoExpansCecoFaseProyIni(HttpServletRequest request, HttpServletResponse response) {
        try {

            String proyecto = request.getParameter("proyecto");
            String fase = request.getParameter("fase");
            String ceco = request.getParameter("ceco");

            List<HallazgosTransfDTO> lista = hallazgosTransfBI.obtieneHallazgosExpansionIni(ceco, proyecto, fase);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSTRANSF");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("HALLAZGOS INICIALES()");
            return null;
        }
    }

    // http://10.51.218.72:8080/checklist/consultaCatalogosService/getHallazgoExpansCecoFaseProyService.json?ceco=&proyecto=&fase=
    @RequestMapping(value = "/getHallazgoExpansCecoFaseProyService", method = RequestMethod.GET)
    public ModelAndView getHallazgoExpansCecoFaseProyService(HttpServletRequest request, HttpServletResponse response) {
        try {
            String proyecto = request.getParameter("proyecto");
            String fase = request.getParameter("fase");
            String ceco = request.getParameter("ceco");

            List<HallazgosTransfDTO> lista = hallazgosTransfBI.obtieneHallazgosExpansion(ceco, proyecto, fase);
            String json = new Gson().toJson(lista);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Hallazgos expansion");
            mv.addObject("res", json);
            return mv;
        } catch (Exception e) {
            logger.info(e.getMessage());
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getSoftCecoFaseProyecto.json?ceco=&fase=&proyecto=&checklist=

    @RequestMapping(value = "/getSoftCecoFaseProyecto", method = RequestMethod.GET)
    public ModelAndView getSoftCecoFaseProyecto(HttpServletRequest request, HttpServletResponse response) {
        try {

            String proyecto = request.getParameter("proyecto");
            String fase = request.getParameter("fase");
            String ceco = request.getParameter("ceco");
            String checklist = request.getParameter("checklist");

            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtieneSoftCecoFaseProyecto(ceco, fase, proyecto, checklist);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "RESPUESTASSOFT");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("Respuestas Soft)");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getSoftCompleto.json?ceco=&fase=&proyecto=&checklist=
    @RequestMapping(value = "/getSoftCompleto", method = RequestMethod.GET)
    public @ResponseBody
    String getSoftCompleto(HttpServletRequest request, HttpServletResponse response) {
        try {

            String proyecto = request.getParameter("proyecto");
            String fase = request.getParameter("fase");
            String ceco = request.getParameter("ceco");
            String checklist = request.getParameter("checklist");

            String res = checklistPreguntasComBI.obtieneSoftCecoFaseProyectoCom(ceco + "", fase + "", proyecto + "", checklist + "");

            return res;

        } catch (Exception e) {
            logger.info("getResp()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getNegocioCecoFaseProyecto.json?ceco=<?>&fase=<?>&proyecto=<?>
    @RequestMapping(value = "/getNegocioCecoFaseProyecto", method = RequestMethod.GET)
    public ModelAndView getNegocioCecoFaseProyecto(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = request.getParameter("ceco");
            String fase = request.getParameter("fase");
            String proyecto = request.getParameter("proyecto");

            List<ChecklistHallazgosRepoDTO> lista = hallazgosRepoBI.obtieneDatosNegoExp(ceco, fase, proyecto);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "NEGOBIT");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getBitaNegoCecoFaseProyecto()");
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaCatalogosService/getSucCecoFaseProyecto.json?ceco=<?>&fase=<?>&proyecto=<?>

    @RequestMapping(value = "/getSucCecoFaseProyecto", method = RequestMethod.GET)
    public ModelAndView getSucCecoFaseProyecto(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = request.getParameter("ceco");
            String fase = request.getParameter("fase");
            String proyecto = request.getParameter("proyecto");

            List<SucursalChecklistDTO> res = sucursalChecklistBI.obtieneDatosSucCecoFaseProyecto(ceco, fase, proyecto);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(res);
            mv.addObject("tipo", "SUCCHEK");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info("getSucCheckNego()");
            return null;
        }

    }
    // http://localhost:8080/checklist/consultaCatalogosService/getFirmaCheckCecoFaseProyecto.json?ceco=<?>&fase=<?>&proyecto=<?>

    @RequestMapping(value = "/getFirmaCheckCecoFaseProyecto", method = RequestMethod.GET)
    public ModelAndView getFirmaCheckCecoFaseProyecto(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = request.getParameter("ceco");
            String fase = request.getParameter("fase");
            String proyecto = request.getParameter("proyecto");

            List<FirmaCheckDTO> lista = firmaCheckBI.obtieneDatosFirmaCecoFaseProyecto(ceco, fase, proyecto);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "FIRMACHECK");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getFirmaCheckCeco()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPregNoImperdCecoFaseProy.json?ceco=<?>&fase=<?>&proyecto=<?>
    @RequestMapping(value = "/getPregNoImperdCecoFaseProy", method = RequestMethod.GET)
    public ModelAndView getPregNoImperdCecoFaseProy(HttpServletRequest request, HttpServletResponse response) {
        try {
            String ceco = request.getParameter("ceco");
            String fase = request.getParameter("fase");
            String proyecto = request.getParameter("proyecto");

            List<ChecklistPreguntasComDTO> lista = checklistPreguntasComBI.obtienePregNoImpCecoFaseProyecto(ceco, fase, proyecto);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "RESPUESTASIMP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPregNoImperd()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getContGenCecoFaseProyecto.json?ceco=<?>&fase=<?>&proyecto=&clasifica=
    @RequestMapping(value = "/getContGenCecoFaseProyecto", method = RequestMethod.GET)
    public @ResponseBody
    String getContGenCecoFaseProyecto(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {

            String ceco = request.getParameter("ceco");
            String fase = request.getParameter("fase");
            String proyecto = request.getParameter("proyecto");
            String clasifica = request.getParameter("clasifica");

            String res = conteoGenExpBI.obtieneDatosConteoGral(ceco, fase, proyecto, clasifica);

            return res;

        } catch (Exception e) {
            logger.info("getContGenCecoFaseProyecto()");
            return null;
        }

    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPregNoCoreExpansion.json?ceco=<?>&fase=<?>&proyecto=<?>
    @RequestMapping(value = "/getPregNoCoreExpansion", method = RequestMethod.GET)
    public ModelAndView getPregNoCoreExpansion(HttpServletRequest request, HttpServletResponse response) {
        try {
            String ceco = request.getParameter("ceco");
            String fase = request.getParameter("fase");
            String proyecto = request.getParameter("proyecto");

            List<ReporteChecksExpDTO> lista = reporteChecksExpBI.obtienePregNoImpCecoFaseProyecto(ceco, fase, proyecto);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "REPOCHEKSEXP");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPregNoImperd()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getRepoActaCecoFaseProyecto.json?ceco=<?>&fase=<?>&proyecto=<?>
    @RequestMapping(value = "/getRepoActaCecoFaseProyecto", method = RequestMethod.GET)
    public ModelAndView getRepoActaCecoFaseProyecto(HttpServletRequest request, HttpServletResponse response) {
        try {
            String ceco = request.getParameter("ceco");
            String fase = request.getParameter("fase");
            String proyecto = request.getParameter("proyecto");

            List<RepoFilExpDTO> lista = repoFilExpBI.obtieneActaCecoFaseProyecto(ceco, fase, proyecto);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ActaCecoFaseProy");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getRepoActa()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getPregActaCecoFaseProyecto.json?ceco=<?>&fase=<?>&proyecto=<?>
    @RequestMapping(value = "/getPregActaCecoFaseProyecto", method = RequestMethod.GET)
    public ModelAndView getPregActaCecoFaseProyecto(HttpServletRequest request, HttpServletResponse response) {
        try {
            String ceco = request.getParameter("ceco");
            String fase = request.getParameter("fase");
            String proyecto = request.getParameter("proyecto");

            List<RepoFilExpDTO> lista = repoFilExpBI.obtienePregActaCecoFaseProyecto(ceco, fase, proyecto);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "PREGUN");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("getPregActa()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getMatrizHallazgo.json?tipoProyecto=
    @RequestMapping(value = "/getMatrizHallazgo", method = RequestMethod.GET)
    public ModelAndView getMatrizHallazgo(HttpServletRequest request, HttpServletResponse response) {
        try {

            String tipoProyecto = null;

            tipoProyecto = request.getParameter("tipoProyecto");

            List<HallazgosTransfDTO> lista = hallagosMatrizBI.obtieneHallazgoMatriz(tipoProyecto);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "MatrizHallazgos");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("matrizHalla()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getHallazgoCompletoTransf.json?ceco=<?>&fase=<?>&proyecto=<?>
    @RequestMapping(value = "/getHallazgoCompletoTransf", method = RequestMethod.GET)
    public ModelAndView getHallazgoCompletoTransf(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = request.getParameter("ceco");
            String fase = request.getParameter("fase");
            String proyecto = request.getParameter("proyecto");

            List<HallazgosTransfDTO> lista = soporteHallazgosTransfBI.obtieneHallazgosSoporte(ceco, fase, proyecto);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSTRANSF");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("HALLazgosConFase()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getOrdenFaseFirmas.json?ceco=<?>&fase=<?>&proyecto=<?>
    @RequestMapping(value = "/getOrdenFaseFirmas", method = RequestMethod.GET)
    public @ResponseBody
    String getOrdenFaseFirmas(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = request.getParameter("ceco");
            String fase = request.getParameter("fase");
            String proyecto = request.getParameter("proyecto");

            String res = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(ceco, fase, proyecto);

            return res;

        } catch (Exception e) {
            logger.info("OrdenFaseFirmas()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getAdicionalesCecoFaseProyecto.json?ceco=<?>&fase=<?>&proyecto=<?>&parametro=
    @RequestMapping(value = "/getAdicionalesCecoFaseProyecto", method = RequestMethod.GET)
    public ModelAndView getAdicionalesCecoFaseProyecto(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = request.getParameter("ceco");
            String fase = request.getParameter("fase");
            String proyecto = request.getParameter("proyecto");
            int parametro = Integer.parseInt(request.getParameter("parametro"));

            List<HallazgosTransfDTO> lista = soporteHallazgosTransfBI.obtieneAdicionalesCecoFaseProyecto(ceco, fase, proyecto, parametro);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "HALLAZGOSTRANSF");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("HALLazgosConFase()");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getConteoHallazgosGral.json?nego=&fechaInicio=&fechaFin=AAAAMMDD
    @RequestMapping(value = "/getConteoHallazgosGral", method = RequestMethod.GET)
    public @ResponseBody
    String getConteoHallazgosGral(HttpServletRequest request, HttpServletResponse response) {
        try {

            String nego = request.getParameter("nego");
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");

            String res = reporteHallazgosBI.conteoGralHallazgos(nego, fechaInicio, fechaFin);

            return res;
        } catch (Exception e) {
            logger.info("ConteoHallazgos GRAL");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getConteoHallazgosCeco.json?ceco=&fechaInicio=&fechaFin=AAAAMMDD
    @RequestMapping(value = "/getConteoHallazgosCeco", method = RequestMethod.GET)
    public ModelAndView getConteoHallazgosCeco(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = request.getParameter("ceco");
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");

            List<ReporteHallazgosDTO> lista = reporteHallazgosBI.conteoCecoHallazgos(ceco, fechaInicio, fechaFin);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ConteoHallazgos");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("ConteoHallazgos CECO");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getConteoHallazgosFase.json?ceco=&fechaInicio=&fechaFin=AAAAMMDD
    @RequestMapping(value = "/getConteoHallazgosFase", method = RequestMethod.GET)
    public ModelAndView getConteoHallazgosFase(HttpServletRequest request, HttpServletResponse response) {
        try {

            String ceco = request.getParameter("ceco");
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");

            List<ReporteHallazgosDTO> lista = reporteHallazgosBI.conteoFaseHallazgos(ceco, fechaInicio, fechaFin);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ConteoHallazgos");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("ConteoHallazgos FASE");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCecosHallazgosFecha.json?negocio=&fechaInicio=&fechaFin=AAAAMMDD
    @RequestMapping(value = "/getCecosHallazgosFecha", method = RequestMethod.GET)
    public ModelAndView getCecosHallazgosFecha(HttpServletRequest request, HttpServletResponse response) {
        try {

            String negocio = request.getParameter("negocio");
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");

            List<ReporteHallazgosDTO> lista = reporteHallazgosBI.obtieneCecosFechas(negocio, fechaInicio, fechaFin);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ConteoHallazgos");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("ConteoHallazgos FASE");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getCheckInAsistencia.json?fechaInicio=&fechaFin=AAAAMMDD&idUsuario=
    @RequestMapping(value = "/getCheckInAsistencia", method = RequestMethod.GET)
    public ModelAndView getCheckInAsistencia(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idUsuario = request.getParameter("idUsuario");
            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");

            List<CheckInAsistenciaDTO> lista = checkInAsistenciaBI.obtieneDatosAsistencia(fechaInicio, fechaFin, idUsuario);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "CheckInAsistencia");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("CheckIn Asistencia");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getProgramacionMtto.json?idUsuario=
    @RequestMapping(value = "/getProgramacionMtto", method = RequestMethod.GET)
    public ModelAndView getProgramacionMtto(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idUsuario = request.getParameter("idUsuario");

            List<ProgramacionMttoDTO> lista = checkInAsistenciaBI.obtieneProgramaciones(idUsuario);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "ProgramacionesMtto");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(" ProgramacionesMtto");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaCatalogosService/getDatosInforme.json?idUsuario=&ceco=&idInforme=
    @RequestMapping(value = "/getDatosInforme", method = RequestMethod.GET)
    public ModelAndView getDatosInforme(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idUsuario = null;
            String ceco = null;
            String idInforme = null;

            idUsuario = request.getParameter("idUsuario");
            ceco = request.getParameter("ceco");
            idInforme = request.getParameter("idInforme");

            List<DatosInformeDTO> lista = datosInformeBI.obtieneDatosDatosInforme(ceco, idInforme, idUsuario);

            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "DatosInforme");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(" DatosInforme");
            return null;
        }
    }

}
