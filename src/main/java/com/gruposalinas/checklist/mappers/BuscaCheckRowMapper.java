package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.Coment7SDTO;


public class BuscaCheckRowMapper implements RowMapper<Coment7SDTO>{


	public Coment7SDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		Coment7SDTO coment = new Coment7SDTO();

		coment.setIdCheckUsua(rs.getInt("FIID_CHECK_USUA"));
		coment.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		coment.setIdUsuario(rs.getInt("FIID_USUARIO"));
		coment.setIdBitacora(rs.getInt("FIID_BITACORA"));
		return coment;
	}
	

}
