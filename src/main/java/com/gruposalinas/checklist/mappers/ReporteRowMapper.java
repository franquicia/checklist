package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteDTO;

public class ReporteRowMapper implements RowMapper<ReporteDTO> {

	public ReporteDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteDTO reporteDTO = new ReporteDTO();
		
		reporteDTO.setIdChecklist(rs.getString("FIID_CHECKLIST"));
		reporteDTO.setIdUsuario(rs.getString("FIID_USUARIO"));
		reporteDTO.setNombreUsuario(rs.getString("FCNOMBRE"));
		reporteDTO.setNombreSucursal(rs.getString("FIIDNU_SUCURSAL"));
		reporteDTO.setNombreCeco(rs.getString("FCNOMBRECC"));
		reporteDTO.setIdCeco(rs.getString("FCID_CECO"));
		reporteDTO.setIdRegional(rs.getString("FIREGIONAL"));
		reporteDTO.setRegional(rs.getString("FCREGIONAL"));
		reporteDTO.setIdZona(rs.getString("FIZONA"));
		reporteDTO.setZona(rs.getString("FCZONA"));
		reporteDTO.setIdTerritorio(rs.getString("FITERRITORIO"));
		reporteDTO.setTerritorio(rs.getString("FCTERRITORIO"));
		reporteDTO.setFechaRespuesta(rs.getString("FDFECHA_RES"));
		reporteDTO.setSemana(rs.getString("SEMANA"));
		reporteDTO.setNombreModuloP(rs.getString("FCMODPADRE"));
		reporteDTO.setNombreMod(rs.getString("NombreModulo"));
		reporteDTO.setIdPregunta(rs.getString("FIID_PREGUNTA"));
		reporteDTO.setDesPregunta(rs.getString("FCDESCRIPCION"));
		reporteDTO.setRespuesta(rs.getString("FCRESPUESTA"));
		reporteDTO.setObservaciones(rs.getString("FCOBSERVACIONES"));
		reporteDTO.setCompromisos(rs.getString("COMPROMISO"));
		reporteDTO.setIdResposable(rs.getString("FIID_RESPONSABLE"));
		reporteDTO.setNombreResponsable(rs.getString("NOMBRESPONSABLE"));
		reporteDTO.setFechaCompromiso(rs.getString("FDFECHA_COMP"));
		reporteDTO.setEstatus(rs.getString("FIESTATUS"));
		reporteDTO.setNombreChecklist(rs.getString("NCHECKLIST"));
		reporteDTO.setMetodoChecklist(rs.getString("METODO"));
		
		/**/
		return reporteDTO;
		
	}
}
