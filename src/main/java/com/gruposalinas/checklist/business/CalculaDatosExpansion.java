package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.gruposalinas.checklist.domain.ConteoGenExpDTO;
import com.gruposalinas.checklist.domain.ExpansionContadorDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;

public class CalculaDatosExpansion {

	private static Logger logger = LogManager.getLogger(CalculaDatosExpansion.class);

	private String res;
	private String bitacora;
	private String clasifica;
	
	private List<ExpansionContadorDTO> clasificaciones;
	private ExpansionContadorDTO datosExpansion;

	private JSONObject json;
	private JSONArray preguntas;
	private JSONArray respuestas;
	private JSONArray checks;
	
	private int error = 0;

	private ConteoGenExpBI conteoGenExpBI;
	
	public CalculaDatosExpansion(List<ExpansionContadorDTO> clasificaciones) {
		this.clasificaciones=clasificaciones;
		datosExpansion = new ExpansionContadorDTO("TOTAL");
		calcularClasificaciones();
		imprimirDatos();
	}

	//erick prueba

	public CalculaDatosExpansion(String json,String clasifica) {
		datosExpansion = new ExpansionContadorDTO(clasifica);
		crearConexionListas2(json);
		datosExpansion.crearArrayParametros();  //Se generan Arreglos para conteos de cheklist
		iniciarValidacionConteoPonderacion();
	}
	private void crearConexionListas2(String json2) {
		res = json2;

		try {
			json = new JSONObject(res.toString());
			checks = json.getJSONArray("listaChecks").getJSONArray(0);
			respuestas = json.getJSONArray("listaRespuestas").getJSONArray(0);
			preguntas = json.getJSONArray("listaPreg").getJSONArray(0);
		} catch (Exception e) {
			error = -1;
			logger.info("Algo ocurrió al parsear json conteo... " + e.getMessage());
		}

		try {
			for (int i = 0; i < checks.length(); i++) {
				//logger.info(checks.getJSONObject(i));
				ConteoGenExpDTO check = new ConteoGenExpDTO();
				check.setIdCheck(Integer.parseInt(checks.getJSONObject(i).getString("idcheck")));
				check.setPondTotal(Double.parseDouble(checks.getJSONObject(i).getString("pontot")));
				check.setVersion(Integer.parseInt(checks.getJSONObject(i).getString("version")));
				check.setNomCheck(checks.getJSONObject(i).getString("nomcheck"));
				check.setClasifica(checks.getJSONObject(i).getString("clasifica"));

				//logger.info(check);
				datosExpansion.getListaChecks().add(check);
			}
		} catch (Exception e) {
			error = -1;
			logger.info("Algo ocurrió al generar lista checks para conteo... " + e.getMessage());
		}
		try {
			for (int i = 0; i < respuestas.length(); i++) {
				//logger.info(respuestas.getJSONObject(i));
				ConteoGenExpDTO respuesta = new ConteoGenExpDTO();
				respuesta.setBitaGral(Integer.parseInt(respuestas.getJSONObject(i).getString("bitagral")));
				respuesta.setBitacora(Integer.parseInt(respuestas.getJSONObject(i).getString("bita")));
				respuesta.setIdResp(Integer.parseInt(respuestas.getJSONObject(i).getString("idresp")));
				respuesta.setIdCheck(Integer.parseInt(respuestas.getJSONObject(i).getString("idcheck")));
				respuesta.setIdPreg(Integer.parseInt(respuestas.getJSONObject(i).getString("idpreg")));
				respuesta.setCritica(Integer.parseInt(respuestas.getJSONObject(i).getString("critica")));
				respuesta.setPonderacion(Double.parseDouble(respuestas.getJSONObject(i).getString("ponderacion")));
				respuesta.setObs(respuestas.getJSONObject(i).getString("obs"));
				respuesta.setPosible(respuestas.getJSONObject(i).getString("posible"));
				respuesta.setPregunta(respuestas.getJSONObject(i).getString("pregunta"));
				respuesta.setNomCheck(respuestas.getJSONObject(i).getString("nomcheck"));
				respuesta.setClasifica(respuestas.getJSONObject(i).getString("clasifica"));

				//logger.info(respuesta);
				datosExpansion.getListaRespuestas().add(respuesta);
			}
		} catch (Exception e) {
			error = -1;
			logger.info("Algo ocurrió al generar lista respuestas para conteo... " + e.getMessage());
		}
		try {
			for (int i = 0; i < preguntas.length(); i++) {
				ConteoGenExpDTO pregunta = new ConteoGenExpDTO();
				//logger.info(preguntas.getJSONObject(i));
				pregunta.setIdPreg(Integer.parseInt(preguntas.getJSONObject(i).getString("idpreg")));
				pregunta.setIdModulo(Integer.parseInt(preguntas.getJSONObject(i).getString("idmodulo")));
				pregunta.setPregunta(preguntas.getJSONObject(i).getString("pregunta"));
				pregunta.setCritica(Integer.parseInt(preguntas.getJSONObject(i).getString("critica")));
				pregunta.setArea(preguntas.getJSONObject(i).getString("area"));
				pregunta.setTipoPreg(Integer.parseInt(preguntas.getJSONObject(i).getString("tipopreg")));
				pregunta.setPregPadre(Integer.parseInt(preguntas.getJSONObject(i).getString("pregpadre")));
				pregunta.setIdCheck(Integer.parseInt(preguntas.getJSONObject(i).getString("idcheck")));
				pregunta.setNomCheck(preguntas.getJSONObject(i).getString("nomcheck"));
				pregunta.setClasifica(preguntas.getJSONObject(i).getString("clasifica"));
				pregunta.setPondTotal(Double.parseDouble(preguntas.getJSONObject(i).getString("pontot")));
				pregunta.setVersion(Integer.parseInt(preguntas.getJSONObject(i).getString("version")));

				//logger.info(pregunta);
				datosExpansion.getListaPreg().add(pregunta);
			}
		} catch (Exception e) {
			error = -1;
			logger.info("Algo ocurrió al generar lista preguntas para conteo... " + e.getMessage());
		}

	}

	//erick prueba

	public CalculaDatosExpansion(String bitacora, String clasifica,int calcular) {
		this.bitacora = bitacora;
		this.clasifica = clasifica;
		datosExpansion = new ExpansionContadorDTO(clasifica);


		crearConexionListas();
		if(calcular==1) {
			if(error == 0) {
				datosExpansion.crearArrayParametros();  //Se generan Arreglos para conteos de cheklist
				iniciarValidacionConteoPonderacion();
			}
		} else {
			if(error == 0) {
				datosExpansion.crearArrayParametros();  //Se generan Arreglos para conteos de cheklist
			}
		}
		

	}

	private void calcularClasificaciones() {
		if(clasificaciones!=null && clasificaciones.size()>0) {
			for(int i = 0; i<clasificaciones.size(); i++) {
				datosExpansion.getListaPreg().addAll(clasificaciones.get(i).getListaPreg());
				datosExpansion.getListaRespuestas().addAll(clasificaciones.get(i).getListaRespuestas());
				datosExpansion.getListaChecks().addAll(clasificaciones.get(i).getListaChecks());
				datosExpansion.getListaPregHallazgosClasif().addAll(clasificaciones.get(i).getListaPregHallazgosClasif());
				datosExpansion.getListaPregImperdonablesClasif().addAll(clasificaciones.get(i).getListaPregImperdonablesClasif());
				datosExpansion.getListaPregAdicionalesClasif().addAll(clasificaciones.get(i).getListaPregAdicionalesClasif());
				datosExpansion.getListaChecksDesHabilitados().addAll(clasificaciones.get(i).getListaChecksDesHabilitados());
				
				datosExpansion.setCountItemClasifSi(datosExpansion.getCountItemClasifSi()+clasificaciones.get(i).getCountItemClasifSi());
				datosExpansion.setCountItemClasifNo(datosExpansion.getCountItemClasifNo()+clasificaciones.get(i).getCountItemClasifNo());
				datosExpansion.setCountItemClasifNA(datosExpansion.getCountItemClasifNA()+clasificaciones.get(i).getCountItemClasifNA());
				datosExpansion.setCountItemClasifRe(datosExpansion.getCountItemClasifRe()+clasificaciones.get(i).getCountItemClasifRe());
				datosExpansion.setCountItemClasifTo(datosExpansion.getCountItemClasifTo()+clasificaciones.get(i).getCountItemClasifTo());
				datosExpansion.setCountItemClasifImp(datosExpansion.getCountItemClasifImp()+clasificaciones.get(i).getCountItemClasifImp());
				
	
				datosExpansion.setPondClasifNa(datosExpansion.getPondClasifNa()+clasificaciones.get(i).getPondClasifNa());
				datosExpansion.setPonderacionClasifMaximaReal(datosExpansion.getPonderacionClasifMaximaReal()+clasificaciones.get(i).getPonderacionClasifMaximaReal());
				datosExpansion.setPonderacionClasifMaximaCalculada(datosExpansion.getPonderacionClasifMaximaCalculada()+clasificaciones.get(i).getPonderacionClasifMaximaCalculada());
				datosExpansion.setPonderacionClasifObtenidaReal(datosExpansion.getPonderacionClasifObtenidaReal()+clasificaciones.get(i).getPonderacionClasifObtenidaReal());
				datosExpansion.setPonderacionClasifObtenidaCalculada(datosExpansion.getPonderacionClasifObtenidaCalculada()+clasificaciones.get(i).getPonderacionClasifObtenidaCalculada());
				
			}
			datosExpansion.setPorcentajeClasifReSi((datosExpansion.getCountItemClasifSi()>0 && datosExpansion.getCountItemClasifRe()>0)?datosExpansion.getCountItemClasifSi()*100.0/datosExpansion.getCountItemClasifRe():0);
			datosExpansion.setPorcentajeClasifReNo((datosExpansion.getCountItemClasifNo()>0 && datosExpansion.getCountItemClasifRe()>0)?datosExpansion.getCountItemClasifNo()*100.0/datosExpansion.getCountItemClasifRe():0);

			datosExpansion.setPorcentajeClasifToSi((datosExpansion.getCountItemClasifSi()>0 && datosExpansion.getCountItemClasifTo()>0)?datosExpansion.getCountItemClasifSi()*100.0/datosExpansion.getCountItemClasifTo():0);
			datosExpansion.setPorcentajeClasifToNo((datosExpansion.getCountItemClasifNo()>0 && datosExpansion.getCountItemClasifTo()>0)?datosExpansion.getCountItemClasifNo()*100.0/datosExpansion.getCountItemClasifTo():0);
			datosExpansion.setPorcentajeClasifToNA((datosExpansion.getCountItemClasifNA()>0 && datosExpansion.getCountItemClasifTo()>0)?datosExpansion.getCountItemClasifNA()*100.0/datosExpansion.getCountItemClasifTo():0);

			datosExpansion.setCalificacionClasifReal((datosExpansion.getPonderacionClasifObtenidaReal()>0 && datosExpansion.getPonderacionClasifMaximaReal()>0)?datosExpansion.getPonderacionClasifObtenidaReal()*100.0/datosExpansion.getPonderacionClasifMaximaReal():0);
			datosExpansion.setCalificacionClasifCalculada((datosExpansion.getPonderacionClasifObtenidaCalculada()>0 && datosExpansion.getPonderacionClasifMaximaCalculada()>0)?datosExpansion.getPonderacionClasifObtenidaCalculada()*100.0/datosExpansion.getPonderacionClasifMaximaCalculada():0);
			
		}
		
		
	}

	private void crearConexionListas() {
		conteoGenExpBI = (ConteoGenExpBI) FRQAppContextProvider.getApplicationContext()
				.getBean("conteoGenExpBI");
		res = conteoGenExpBI.obtieneDatos(bitacora, clasifica);

		try {
			json = new JSONObject(res.toString());
			checks = json.getJSONArray("listaChecks").getJSONArray(0);
			respuestas = json.getJSONArray("listaRespuestas").getJSONArray(0);
			preguntas = json.getJSONArray("listaPreg").getJSONArray(0);
		} catch (Exception e) {
			error = -1;
			logger.info("Algo ocurrió al parsear json conteo... " + e.getMessage());
		}

		try {
			for (int i = 0; i < checks.length(); i++) {
				//logger.info(checks.getJSONObject(i));
				ConteoGenExpDTO check = new ConteoGenExpDTO();
				check.setIdCheck(Integer.parseInt(checks.getJSONObject(i).getString("idcheck")));
				check.setPondTotal(Double.parseDouble(checks.getJSONObject(i).getString("pontot")));
				check.setVersion(Integer.parseInt(checks.getJSONObject(i).getString("version")));
				check.setNomCheck(checks.getJSONObject(i).getString("nomcheck"));
				check.setClasifica(checks.getJSONObject(i).getString("clasifica"));

				//logger.info(check);
				datosExpansion.getListaChecks().add(check);
			}
		} catch (Exception e) {
			error = -1;
			logger.info("Algo ocurrió al generar lista checks para conteo... " + e.getMessage());
		}
		try {
			for (int i = 0; i < respuestas.length(); i++) {
				//logger.info(respuestas.getJSONObject(i));
				ConteoGenExpDTO respuesta = new ConteoGenExpDTO();
				respuesta.setBitaGral(Integer.parseInt(respuestas.getJSONObject(i).getString("bitagral")));
				respuesta.setBitacora(Integer.parseInt(respuestas.getJSONObject(i).getString("bita")));
				respuesta.setIdResp(Integer.parseInt(respuestas.getJSONObject(i).getString("idresp")));
				respuesta.setIdCheck(Integer.parseInt(respuestas.getJSONObject(i).getString("idcheck")));
				respuesta.setIdPreg(Integer.parseInt(respuestas.getJSONObject(i).getString("idpreg")));
				respuesta.setCritica(Integer.parseInt(respuestas.getJSONObject(i).getString("critica")));
				respuesta.setPonderacion(Double.parseDouble(respuestas.getJSONObject(i).getString("ponderacion")));
				respuesta.setObs(respuestas.getJSONObject(i).getString("obs"));
				respuesta.setPosible(respuestas.getJSONObject(i).getString("posible"));
				respuesta.setPregunta(respuestas.getJSONObject(i).getString("pregunta"));
				respuesta.setNomCheck(respuestas.getJSONObject(i).getString("nomcheck"));
				respuesta.setClasifica(respuestas.getJSONObject(i).getString("clasifica"));

				//logger.info(respuesta);
				datosExpansion.getListaRespuestas().add(respuesta);
			}
		} catch (Exception e) {
			error = -1;
			logger.info("Algo ocurrió al generar lista respuestas para conteo... " + e.getMessage());
		}
		try {
			for (int i = 0; i < preguntas.length(); i++) {
				ConteoGenExpDTO pregunta = new ConteoGenExpDTO();
				//logger.info(preguntas.getJSONObject(i));
				pregunta.setIdPreg(Integer.parseInt(preguntas.getJSONObject(i).getString("idpreg")));
				pregunta.setIdModulo(Integer.parseInt(preguntas.getJSONObject(i).getString("idmodulo")));
				pregunta.setPregunta(preguntas.getJSONObject(i).getString("pregunta"));
				pregunta.setCritica(Integer.parseInt(preguntas.getJSONObject(i).getString("critica")));
				pregunta.setArea(preguntas.getJSONObject(i).getString("area"));
				pregunta.setTipoPreg(Integer.parseInt(preguntas.getJSONObject(i).getString("tipopreg")));
				pregunta.setPregPadre(Integer.parseInt(preguntas.getJSONObject(i).getString("pregpadre")));
				pregunta.setIdCheck(Integer.parseInt(preguntas.getJSONObject(i).getString("idcheck")));
				pregunta.setNomCheck(preguntas.getJSONObject(i).getString("nomcheck"));
				pregunta.setClasifica(preguntas.getJSONObject(i).getString("clasifica"));
				pregunta.setPondTotal(Double.parseDouble(preguntas.getJSONObject(i).getString("pontot")));
				pregunta.setVersion(Integer.parseInt(preguntas.getJSONObject(i).getString("version")));

				//logger.info(pregunta);
				datosExpansion.getListaPreg().add(pregunta);
			}
		} catch (Exception e) {
			error = -1;
			logger.info("Algo ocurrió al generar lista preguntas para conteo... " + e.getMessage());
		}

	}
	
	public void iniciarValidacionConteoPonderacion() {
		if (datosExpansion.getListaChecks()!=null && datosExpansion.getListaChecks().size()>0) {
			if(datosExpansion.getListaPreg()!=null && datosExpansion.getListaPreg().size()>0) {
				if(datosExpansion.getListaRespuestas()!=null && datosExpansion.getListaRespuestas().size()>0) {
					iniciarConteoPonderacion();
				}
				else {
					contarSinRespuesta();
				}
				imprimirDatos();
			}

		}
		
	}



	private void imprimirDatos() {

		logger.info("===========================================================");
		logger.info("Clasificacion=="+datosExpansion.getClasif());
		logger.info("ItemsSi=="+datosExpansion.getCountItemClasifSi());
		logger.info("ItemsNo=="+datosExpansion.getCountItemClasifNo());
		logger.info("ItemsNA=="+datosExpansion.getCountItemClasifNA());
		logger.info("ItemRevisados=="+datosExpansion.getCountItemClasifRe());
		logger.info("ItemTotales=="+datosExpansion.getCountItemClasifTo());
		logger.info("ItemImperd=="+datosExpansion.getCountItemClasifImp());
		logger.info("===========================================================");
		logger.info("PorcentajeReSi=="+datosExpansion.getPorcentajeClasifReSi());
		logger.info("PorcentajeReNo=="+datosExpansion.getPorcentajeClasifReNo());
		logger.info("PorcentajeToSi=="+datosExpansion.getPorcentajeClasifToSi());
		logger.info("PorcentajeToNo=="+datosExpansion.getPorcentajeClasifToNo());
		logger.info("PorcentajeToNA=="+datosExpansion.getPorcentajeClasifToNA());
		logger.info("===========================================================");
		logger.info("PonderacionNA=="+datosExpansion.getPondClasifNa());
		logger.info("PonderacionMaximaReal=="+datosExpansion.getPonderacionClasifMaximaReal());
		logger.info("PonderacionObtenidaReal=="+datosExpansion.getPonderacionClasifObtenidaReal());
		logger.info("PonderacionMaximaCalculada=="+datosExpansion.getPonderacionClasifMaximaCalculada());
		logger.info("PonderacionObtenidaCalculada=="+datosExpansion.getPonderacionClasifObtenidaCalculada());
		logger.info("===========================================================");
		logger.info("CalificacionReal=="+datosExpansion.getCalificacionClasifReal());
		logger.info("CalificacionCalculada=="+datosExpansion.getCalificacionClasifCalculada());
		
	}

	private void contarSinRespuesta() {
		for(int i = 0; i<datosExpansion.getListaChecks().size();i++) {
			//------SE EVALUAN PONDERACIONES NA
			datosExpansion.getPondCheckNa()[i]=datosExpansion.getListaChecks().get(i).getPondTotal();
			datosExpansion.setPondClasifNa(datosExpansion.getPondClasifNa()+datosExpansion.getListaChecks().get(i).getPondTotal());
			//------SE EVALUAN PONDERACION MAXIMA REAL
			datosExpansion.getPonderacionCheckMaximaReal()[i]=datosExpansion.getListaChecks().get(i).getPondTotal();
			datosExpansion.setPonderacionClasifMaximaReal(datosExpansion.getPonderacionClasifMaximaReal()+datosExpansion.getListaChecks().get(i).getPondTotal());
			
			for(int j = 0; j<datosExpansion.getListaPreg().size(); j++) {
				
				if (datosExpansion.getListaChecks().get(i).getIdCheck() == datosExpansion.getListaPreg().get(j).getIdCheck()) {
					//------SE EVALUAN ITEMS SIN RESPUESTA (NA)
					datosExpansion.getCountItemCheckNA()[i]=datosExpansion.getCountItemCheckNA()[i]+1;
					datosExpansion.setCountItemClasifNA(datosExpansion.getCountItemClasifNA()+1);
				}
			}
			//------SE EVALUAN PONDERACION OBTENIDA REAL
			datosExpansion.getPonderacionCheckObtenidaReal()[i]=0;
			datosExpansion.setPonderacionClasifObtenidaReal(0);
			
			//------SE EVALUAN PORCENTAJE REVISADA Y TOTAL (SI, NO, NA) PARA CHECKLIST
			datosExpansion.getPorcentajeCheckReSi()[i] = 0;
			datosExpansion.getPorcentajeCheckReNo()[i] = 0;
			datosExpansion.getPorcentajeCheckToSi()[i] = 0;
			datosExpansion.getPorcentajeCheckToNo()[i] = 0;
			datosExpansion.getPorcentajeCheckToNA()[i] = 100;
			
			//------SE EVALUAN PONDERACION OBTENIDA CALCULADA (OBTENIDA_REAL- PONDERACION_NA) PARA CHECKLIST
			datosExpansion.getPonderacionCheckObtenidaCalculada()[i]=datosExpansion.getPonderacionCheckObtenidaReal()[i];
			//------SE EVALUAN PONDERACION MAXIMA CALCULADA (PONDERCION_MAXIMA_REAL-PONDERACION_NA) PARA CHECKLIST
			datosExpansion.getPonderacionCheckMaximaCalculada()[i]=datosExpansion.getPonderacionCheckMaximaReal()[i]-datosExpansion.getPondCheckNa()[i];

			//------SE EVALUAN CALIFICACION CALCULADA Y REAL PARA CHECKLIST
			datosExpansion.getCalificacionCheckReal()[i]=0;
			datosExpansion.getCalificacionCheckCalculada()[i]=0;
			
			
		}
		//------SE EVALUAN PORCENTAJE REVISADA Y TOTAL (SI, NO, NA) PARA CLASIFICACION
		datosExpansion.setPorcentajeClasifReSi(0);
		datosExpansion.setPorcentajeClasifReNo(0);
		datosExpansion.setPorcentajeClasifToSi(0);
		datosExpansion.setPorcentajeClasifToNo(0);
		datosExpansion.setPorcentajeClasifToNA(100);

		//------SE EVALUAN PONDERACION OBTENIDA CALCULADA (OBTENIDA_REAL- PONDERACION_NA) PARA CLASIFICACION
		datosExpansion.setPonderacionClasifObtenidaCalculada(datosExpansion.getPonderacionClasifObtenidaReal());
		//------SE EVALUAN PONDERACION MAXIMA CALCULADA (PONDERCION_MAXIMA_REAL-PONDERACION_NA) PARA CLASIFICACION
		datosExpansion.setPonderacionClasifMaximaCalculada(datosExpansion.getPonderacionClasifMaximaReal()-datosExpansion.getPondClasifNa());
		
		//------SE EVALUAN CALIFICACION CALCULADA Y REAL PARA CLASIFICACION
		datosExpansion.setCalificacionClasifReal(0);
		datosExpansion.setCalificacionClasifCalculada(0);
		
	}

	private void iniciarConteoPonderacion() {
		boolean encontroRespuesta = false;
		boolean checkListHabilitado = false;
		
		for(int i = 0; i<datosExpansion.getListaChecks().size();i++) {
			checkListHabilitado = false;
			List<ConteoGenExpDTO> listaPregHallazgosCheck = new ArrayList<>();
			List<ConteoGenExpDTO> listaPregImperdonablesCheck = new ArrayList<>();
			List<ConteoGenExpDTO> listaPregAdicionalesCheck = new ArrayList<>();
			logger.info(datosExpansion.getListaChecks().get(i).getNomCheck());
			datosExpansion.getCheck()[i] = datosExpansion.getListaChecks().get(i).getNomCheck();
			datosExpansion.getIdCheck()[i] = datosExpansion.getListaChecks().get(i).getIdCheck();
			
			//------SE EVALUAN PONDERACION MAXIMA REAL
			datosExpansion.getPonderacionCheckMaximaReal()[i]=datosExpansion.getListaChecks().get(i).getPondTotal();
			datosExpansion.setPonderacionClasifMaximaReal(datosExpansion.getPonderacionClasifMaximaReal()+datosExpansion.getListaChecks().get(i).getPondTotal());
			
			int contarPreguntas = 0;
			
			for(int j = 0; j<datosExpansion.getListaPreg().size(); j++) {
				
				if(datosExpansion.getListaChecks().get(i).getIdCheck() == datosExpansion.getListaPreg().get(j).getIdCheck()) {
					contarPreguntas++;
					for(int k = 0; k<datosExpansion.getListaRespuestas().size(); k++) {
						if(datosExpansion.getListaPreg().get(j).getIdPreg() == datosExpansion.getListaRespuestas().get(k).getIdPreg()) {
							checkListHabilitado=true;
							
							//------SE EVALUAN ITEMS NO CONSIDERADOS
							if(datosExpansion.getListaRespuestas().get(k).getPosible().equalsIgnoreCase("N/A")) {
								encontroRespuesta=true;
								//------SE EVALUAN PONDERACIONES NA

								datosExpansion.getCountItemCheckNA()[i]=datosExpansion.getCountItemCheckNA()[i] +1;
								datosExpansion.getCountItemCheckTo()[i]=datosExpansion.getCountItemCheckTo()[i] +1;
								datosExpansion.getPondCheckNa()[i]=     datosExpansion.getPondCheckNa()[i]      +datosExpansion.getListaRespuestas().get(k).getPonderacion();

								datosExpansion.setCountItemClasifNA(    datosExpansion.getCountItemClasifNA()   +1);
								datosExpansion.setCountItemClasifTo(    datosExpansion.getCountItemClasifTo()   +1);
								datosExpansion.setPondClasifNa(         datosExpansion.getPondClasifNa()        +datosExpansion.getListaRespuestas().get(k).getPonderacion());
								if(datosExpansion.getListaPreg().get(j).getPregPadre()==0) {
									for(int a=0; a<datosExpansion.getListaPreg().size();a++) {
										if(datosExpansion.getListaPreg().get(a).getPregPadre()==datosExpansion.getListaPreg().get(j).getIdPreg()) {
											datosExpansion.getCountItemCheckNA()[i]=datosExpansion.getCountItemCheckNA()[i] +1;
											datosExpansion.getCountItemCheckTo()[i]=datosExpansion.getCountItemCheckTo()[i] +1;

											datosExpansion.setCountItemClasifNA(    datosExpansion.getCountItemClasifNA()   +1);
											datosExpansion.setCountItemClasifTo(    datosExpansion.getCountItemClasifTo()   +1);
										}
									}
								}
							}
							//------SE EVALUAN ITEMS APROBADOS
							if(datosExpansion.getListaRespuestas().get(k).getPosible().equalsIgnoreCase("SI")) {
								encontroRespuesta=true;
								
								//------SE EVALUAN PONDERACION OBTENIDA REAL
								datosExpansion.getPonderacionCheckObtenidaReal()[i]=datosExpansion.getPonderacionCheckObtenidaReal()[i]+datosExpansion.getListaRespuestas().get(k).getPonderacion();
								datosExpansion.getCountItemCheckfSi()[i]=datosExpansion.getCountItemCheckfSi()[i]+1;
								datosExpansion.getCountItemCheckRe()[i] =datosExpansion.getCountItemCheckRe()[i] +1;
								datosExpansion.getCountItemCheckTo()[i] =datosExpansion.getCountItemCheckTo()[i] +1;
								
								datosExpansion.setPonderacionClasifObtenidaReal(datosExpansion.getPonderacionClasifObtenidaReal()+datosExpansion.getListaRespuestas().get(k).getPonderacion());
								datosExpansion.setCountItemClasifSi(     datosExpansion.getCountItemClasifSi()   +1);
								datosExpansion.setCountItemClasifRe(     datosExpansion.getCountItemClasifRe()   +1);
								datosExpansion.setCountItemClasifTo(     datosExpansion.getCountItemClasifTo()   +1);
								
								//------SE EVALUAN ADICIONALES
								if(datosExpansion.getListaPreg().get(j).getCritica()==1 &&
								   datosExpansion.getListaRespuestas().get(k).getObs() !=null &&
								   !datosExpansion.getListaRespuestas().get(k).getObs().isEmpty()) {
									ConteoGenExpDTO adicionales = new ConteoGenExpDTO();
									adicionales.setIdCheck  (datosExpansion.getListaChecks().     get(i).getIdCheck());
									adicionales.setClasifica(datosExpansion.getListaChecks().     get(i).getClasifica());
									adicionales.setBitacora (datosExpansion.getListaRespuestas(). get(k).getBitacora());
									adicionales.setIdPreg   (datosExpansion.getListaPreg().       get(j).getIdPreg());
									adicionales.setObs      (datosExpansion.getListaRespuestas(). get(k).getObs());
									adicionales.setPregunta (datosExpansion.getListaPreg().       get(j).getPregunta());
									listaPregAdicionalesCheck.add(adicionales);
									datosExpansion.getListaPregAdicionalesClasif().add(adicionales);
								}
							}
							//------SE EVALUAN HALLAZGOS
							if(datosExpansion.getListaRespuestas().get(k).getPosible().equalsIgnoreCase("NO")) {

								encontroRespuesta=true;
								datosExpansion.getCountItemCheckfNo()[i]=datosExpansion.getCountItemCheckfNo()[i]+1;
								datosExpansion.getCountItemCheckRe()[i] =datosExpansion.getCountItemCheckRe()[i] +1;
								datosExpansion.getCountItemCheckTo()[i] =datosExpansion.getCountItemCheckTo()[i] +1;
								
								datosExpansion.setCountItemClasifNo(		datosExpansion.getCountItemClasifNo()   +1);
								datosExpansion.setCountItemClasifRe(		datosExpansion.getCountItemClasifRe()   +1);
								datosExpansion.setCountItemClasifTo(		datosExpansion.getCountItemClasifTo()   +1);
								
								ConteoGenExpDTO hallazgos = new ConteoGenExpDTO();
								hallazgos.setIdCheck  (datosExpansion.getListaChecks().     get(i).getIdCheck());
								hallazgos.setClasifica(datosExpansion.getListaChecks().     get(i).getClasifica());
								hallazgos.setBitacora (datosExpansion.getListaRespuestas(). get(k).getBitacora());
								hallazgos.setIdPreg   (datosExpansion.getListaPreg().       get(j).getIdPreg());
								hallazgos.setObs      (datosExpansion.getListaRespuestas(). get(k).getObs());
								hallazgos.setPregunta (datosExpansion.getListaPreg().       get(j).getPregunta());
								listaPregHallazgosCheck.add(hallazgos);
								datosExpansion.getListaPregHallazgosClasif().add(hallazgos);
								
								//------SE EVALUAN IMPERDONABLES
								if(datosExpansion.getListaPreg().get(j).getCritica()==1) { 
									datosExpansion.getCountItemCheckImp()[i]=datosExpansion.getCountItemCheckImp()[i]+1;
									datosExpansion.setCountItemClasifImp(datosExpansion.getCountItemClasifImp()+1);
									ConteoGenExpDTO imperdonable = new ConteoGenExpDTO();
									imperdonable.setIdCheck  (datosExpansion.getListaChecks().     get(i).getIdCheck());
									imperdonable.setClasifica(datosExpansion.getListaChecks().     get(i).getClasifica());
									imperdonable.setBitacora (datosExpansion.getListaRespuestas(). get(k).getBitacora());
									imperdonable.setIdPreg   (datosExpansion.getListaPreg().       get(j).getIdPreg());
									imperdonable.setObs      (datosExpansion.getListaRespuestas(). get(k).getObs());
									imperdonable.setPregunta (datosExpansion.getListaPreg().       get(j).getPregunta());
									listaPregImperdonablesCheck.add(imperdonable);
									datosExpansion.getListaPregImperdonablesClasif().add(imperdonable);
								}

							}
						}
						if(encontroRespuesta) { break;}
					}
					//------SE EVALUAN CHECKLIST SIN RESPUESTAS O DESHABLITADOS (NO CONSIDERADOS)
					if(!encontroRespuesta) {
						encontroRespuesta=false;
						//logger.info("SR "+datosExpansion.getListaPreg().get(j).getPregPadre());
						//datosExpansion.getCountItemCheckNA()[i]=datosExpansion.getCountItemCheckNA()[i] +1;
						//datosExpansion.getCountItemCheckTo()[i]=datosExpansion.getCountItemCheckTo()[i] +1;

						//datosExpansion.setCountItemClasifNA(    datosExpansion.getCountItemClasifNA()   +1);
						//datosExpansion.setCountItemClasifTo(    datosExpansion.getCountItemClasifTo()   +1);
					}
					else {
						encontroRespuesta = false;
					}
				}
			}
			//------SE SE AGREGA LISTA DE CHECKLIST DESHABILITADOS PARA LA CLASIFICACION
			if(!checkListHabilitado) {
				datosExpansion.getListaChecksDesHabilitados().add(datosExpansion.getListaChecks().get(i));
				//Se agrega info NA
				datosExpansion.getCountItemCheckNA()[i]=datosExpansion.getCountItemCheckNA()[i] +contarPreguntas;
				datosExpansion.getCountItemCheckTo()[i]=datosExpansion.getCountItemCheckTo()[i] +contarPreguntas;
				datosExpansion.getPondCheckNa()[i]=     datosExpansion.getPondCheckNa()[i]      +datosExpansion.getListaChecks().get(i).getPondTotal();

				datosExpansion.setCountItemClasifNA(    datosExpansion.getCountItemClasifNA()   +contarPreguntas);
				datosExpansion.setCountItemClasifTo(    datosExpansion.getCountItemClasifTo()   +contarPreguntas);
				datosExpansion.setPondClasifNa(         datosExpansion.getPondClasifNa()        +datosExpansion.getListaChecks().get(i).getPondTotal());
				
			}
			//------SE SE AGREGA LISTA PARA CHECKLIST CORRESPONDIENTE
			datosExpansion.getListaPregHallazgosCheck()[i] = new ArrayList<>();
			datosExpansion.getListaPregImperdonablesCheck()[i] = new ArrayList<>();
			datosExpansion.getListaPregAdicionalesCheck()[i] = new ArrayList<>();
			datosExpansion.getListaPregHallazgosCheck()[i].add(listaPregHallazgosCheck);
			datosExpansion.getListaPregImperdonablesCheck()[i].add(listaPregImperdonablesCheck);
			datosExpansion.getListaPregAdicionalesCheck()[i].add(listaPregAdicionalesCheck);
			
			//------SE EVALUAN PORCENTAJE REVISADA Y TOTAL (SI, NO, NA) PARA CHECKLIST
			datosExpansion.getPorcentajeCheckReSi()[i] = (datosExpansion.getCountItemCheckfSi()[i]>0 && datosExpansion.getCountItemCheckRe()[i]>0)?datosExpansion.getCountItemCheckfSi()[i] * 100.0 /datosExpansion.getCountItemCheckRe()[i]:0;
			datosExpansion.getPorcentajeCheckReNo()[i] = (datosExpansion.getCountItemCheckfNo()[i]>0 && datosExpansion.getCountItemCheckRe()[i]>0)?datosExpansion.getCountItemCheckfNo()[i] * 100.0 /datosExpansion.getCountItemCheckRe()[i]:0;
			datosExpansion.getPorcentajeCheckToSi()[i] = (datosExpansion.getCountItemCheckfSi()[i]>0 && datosExpansion.getCountItemCheckTo()[i]>0)?datosExpansion.getCountItemCheckfSi()[i] * 100.0 /datosExpansion.getCountItemCheckTo()[i]:0;
			datosExpansion.getPorcentajeCheckToNo()[i] = (datosExpansion.getCountItemCheckfNo()[i]>0 && datosExpansion.getCountItemCheckTo()[i]>0)?datosExpansion.getCountItemCheckfNo()[i] * 100.0 /datosExpansion.getCountItemCheckTo()[i]:0;
			datosExpansion.getPorcentajeCheckToNA()[i] =  (datosExpansion.getCountItemCheckNA()[i]>0 && datosExpansion.getCountItemCheckTo()[i]>0)?datosExpansion.getCountItemCheckNA()[i]  * 100.0 /datosExpansion.getCountItemCheckTo()[i]:0;
			
			//------SE EVALUAN PONDERACION OBTENIDA CALCULADA (OBTENIDA_REAL- PONDERACION_NA) PARA CHECKLIST
			datosExpansion.getPonderacionCheckObtenidaCalculada()[i]=(datosExpansion.getPonderacionCheckObtenidaReal()[i]>0)?datosExpansion.getPonderacionCheckObtenidaReal()[i]:0;
			//------SE EVALUAN PONDERACION MAXIMA CALCULADA (PONDERCION_MAXIMA_REAL-PONDERACION_NA) PARA CHECKLIST
			datosExpansion.getPonderacionCheckMaximaCalculada()[i]=(datosExpansion.getPonderacionCheckMaximaReal()[i]-datosExpansion.getPondCheckNa()[i]>0)?datosExpansion.getPonderacionCheckMaximaReal()[i]-datosExpansion.getPondCheckNa()[i]:0;

			//------SE EVALUAN CALIFICACION CALCULADA Y REAL PARA CHECKLIST
			datosExpansion.getCalificacionCheckReal()[i]= (datosExpansion.getPonderacionCheckObtenidaReal()[i]>0 && datosExpansion.getPonderacionCheckMaximaReal()[i]>0)?datosExpansion.getPonderacionCheckObtenidaReal()[i]*100.0 /datosExpansion.getPonderacionCheckMaximaReal()[i]:0;
			datosExpansion.getCalificacionCheckCalculada()[i]=(datosExpansion.getPonderacionCheckObtenidaCalculada()[i]>0 && datosExpansion.getPonderacionCheckMaximaCalculada()[i]>0)?datosExpansion.getPonderacionCheckObtenidaCalculada()[i]*100.0 /datosExpansion.getPonderacionCheckMaximaCalculada()[i]:0;
			
		}
		//------SE EVALUAN PORCENTAJE REVISADA Y TOTAL (SI, NO, NA) PARA CLASIFICACION
		datosExpansion.setPorcentajeClasifReSi((datosExpansion.getCountItemClasifSi()>0 && datosExpansion.getCountItemClasifRe()>0)?datosExpansion.getCountItemClasifSi() * 100.0 /datosExpansion.getCountItemClasifRe() :0);
		datosExpansion.setPorcentajeClasifReNo((datosExpansion.getCountItemClasifNo()>0 && datosExpansion.getCountItemClasifRe()>0)?datosExpansion.getCountItemClasifNo() * 100.0 /datosExpansion.getCountItemClasifRe() :0);
		datosExpansion.setPorcentajeClasifToSi((datosExpansion.getCountItemClasifSi()>0 && datosExpansion.getCountItemClasifTo()>0)?datosExpansion.getCountItemClasifSi() * 100.0 /datosExpansion.getCountItemClasifTo() :0);
		datosExpansion.setPorcentajeClasifToNo((datosExpansion.getCountItemClasifNo()>0 && datosExpansion.getCountItemClasifTo()>0)?datosExpansion.getCountItemClasifNo() * 100.0 /datosExpansion.getCountItemClasifTo() :0);
		datosExpansion.setPorcentajeClasifToNA((datosExpansion.getCountItemClasifNA()>0 && datosExpansion.getCountItemClasifTo()>0)?datosExpansion.getCountItemClasifNA() * 100.0 /datosExpansion.getCountItemClasifTo() :0);

		//------SE EVALUAN PONDERACION OBTENIDA CALCULADA (OBTENIDA_REAL- PONDERACION_NA) PARA CLASIFICACION
		datosExpansion.setPonderacionClasifObtenidaCalculada((datosExpansion.getPonderacionClasifObtenidaReal()>0)?datosExpansion.getPonderacionClasifObtenidaReal():0);
		//------SE EVALUAN PONDERACION MAXIMA CALCULADA (PONDERCION_MAXIMA_REAL-PONDERACION_NA) PARA CLASIFICACION
		datosExpansion.setPonderacionClasifMaximaCalculada((datosExpansion.getPonderacionClasifMaximaReal()-datosExpansion.getPondClasifNa()>0)?datosExpansion.getPonderacionClasifMaximaReal()-datosExpansion.getPondClasifNa():0);
		
		//------SE EVALUAN CALIFICACION CALCULADA Y REAL PARA CLASIFICACION
		datosExpansion.setCalificacionClasifReal((datosExpansion.getPonderacionClasifObtenidaReal()>0 && datosExpansion.getPonderacionClasifMaximaReal()>0)?datosExpansion.getPonderacionClasifObtenidaReal() * 100.0 /datosExpansion.getPonderacionClasifMaximaReal():0);
		datosExpansion.setCalificacionClasifCalculada((datosExpansion.getPonderacionClasifObtenidaCalculada()>0 && datosExpansion.getPonderacionClasifMaximaCalculada()>0)?datosExpansion.getPonderacionClasifObtenidaCalculada() * 100.0 /datosExpansion.getPonderacionClasifMaximaCalculada():0);
	}

	public List<ExpansionContadorDTO> getClasificaciones() {
		return clasificaciones;
	}


	public void setClasificaciones(List<ExpansionContadorDTO> clasificaciones) {
		this.clasificaciones = clasificaciones;
	}


	public ExpansionContadorDTO getDatosExpansion() {
		return datosExpansion;
	}


	public void setDatosExpansion(ExpansionContadorDTO datosExpansion) {
		this.datosExpansion = datosExpansion;
	}



}
