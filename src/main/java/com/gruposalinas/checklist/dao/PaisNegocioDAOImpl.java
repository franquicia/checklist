package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.PaisNegocioDTO;
import com.gruposalinas.checklist.mappers.PaisNegocioRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PaisNegocioDAOImpl extends DefaultDAO implements PaisNegocioDAO {

    private static Logger logger = LogManager.getLogger(PaisNegocioDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneNegocioPais;
    private DefaultJdbcCall jdbcInsertaNegocioPais;
    private DefaultJdbcCall jdbcEliminaNegocioPais;

    public void init() {

        jdbcObtieneNegocioPais = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEG_PAIS")
                .withProcedureName("SPCONSULTA")
                .returningResultSet("RCL_CONSULTA", new PaisNegocioRowMapper());

        jdbcInsertaNegocioPais = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEG_PAIS")
                .withProcedureName("SPINSERTA");

        jdbcEliminaNegocioPais = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEG_PAIS")
                .withProcedureName("SPELIMINA");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PaisNegocioDTO> obtienePaisNegocio(String negocio, String pais) throws Exception {
        Map<String, Object> out = null;
        List<PaisNegocioDTO> negociopaises = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NEGOCIO", negocio).addValue("PA_PAIS", pais);

        out = jdbcObtieneNegocioPais.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMNEG_PAIS.SPCONSULTA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        negociopaises = (List<PaisNegocioDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al consultar los Negocio-Pais ");
        }

        return negociopaises;
    }

    @Override
    public boolean insertaPaisNegocio(int negocio, int pais) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NEGOCIO", negocio).addValue("PA_PAIS", pais);

        out = jdbcInsertaNegocioPais.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMNEG_PAIS.SPINSERTA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 1) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public boolean eliminaPaisNegocio(int negocio, int pais) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NEGOCIO", negocio).addValue("PA_PAIS", pais);

        out = jdbcEliminaNegocioPais.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMNEG_PAIS.SPELIMINA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 1) {
            return true;
        } else {
            return false;
        }
    }

}
