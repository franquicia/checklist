package com.gruposalinas.checklist.domain;

public class OrdenGrupoDTO {
	private int idOrdenGrupo;
	private int idChecklist;
	private int orden;
	private int idGrupo;
	private int commit;
	
	public int getIdOrdenGrupo() {
		return idOrdenGrupo;
	}
	public void setIdOrdenGrupo(int idOrdenGrupo) {
		this.idOrdenGrupo = idOrdenGrupo;
	}
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	public int getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(int idGrupo) {
		this.idGrupo = idGrupo;
	}
	public int getCommit() {
		return commit;
	}
	public void setCommit(int commit) {
		this.commit = commit;
	}
	@Override
	public String toString() {
		return "OrdenGrupoDTO [idOrdenGrupo=" + idOrdenGrupo + ", idChecklist=" + idChecklist + ", orden=" + orden
				+ ", idGrupo=" + idGrupo + ", commit=" + commit + "]";
	}
	
	

}
