package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ReporteIndicadorDTO;
import com.gruposalinas.checklist.mappers.ReporteIndicadorRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ReporteIndicadorDAOImpl extends DefaultDAO implements ReporteIndicadorDAO {

    private static Logger logger = LogManager.getLogger(ReporteIndicadorDAO.class);

    private DefaultJdbcCall jdbcInsertaIndicador;
    private DefaultJdbcCall jdbcConsultaPorcentaje;
    private DefaultJdbcCall jdbcActualizaIndicador;
    private DefaultJdbcCall jdbcInsertaPendientes;
    private DefaultJdbcCall jdbcConsultaPendientes;

    public void init() {

        jdbcInsertaIndicador = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPINDICADOR")
                .withProcedureName("SP_INS_INDICADOR");

        jdbcConsultaPorcentaje = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPINDICADOR")
                .withProcedureName("SP_CONSULTA_R");

        jdbcActualizaIndicador = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPINDICADOR")
                .withProcedureName("SP_UPDATE_R");

        jdbcInsertaPendientes = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPINDICADOR")
                .withProcedureName("SP_CARGA_PND");

        jdbcConsultaPendientes = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPINDICADOR")
                .withProcedureName("SP_PENDIENTES")
                .returningResultSet("RCL_PENDIENTES", new ReporteIndicadorRowMapper());
    }

    @Override
    public boolean insertaIndicador(ReporteIndicadorDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", bean.getIdCeco())
                .addValue("PA_NOMBRECECO", bean.getNombreCeco())
                .addValue("PA_INDICADOR", bean.getIndicador())
                .addValue("PA_VALOR", bean.getValor());

        out = jdbcInsertaIndicador.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAREPINDICADOR.SP_INS_INDICADOR}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();
        //logger.info(error);
        if (error == 0) {
            logger.info("Algo ocurrió al insertar el Indicador");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public String consultaPorcentaje(String idCeco, int idIndicador) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        String porcentaje = "";

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCECO", idCeco)
                .addValue("PA_INDICADOR", idIndicador);

        out = jdbcConsultaPorcentaje.execute(in);

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        String Return = (String) out.get("PA_PORCENTAJE");
        porcentaje = Return + "";

        //logger.info(error);
        if (error == 0) {
            logger.info("Algo ocurrió al consultar el Indicador");
        } else {
            return porcentaje;
        }

        return porcentaje;
    }

    @Override
    public boolean actualizaIndicador(ReporteIndicadorDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCECO", bean.getIdCeco())
                .addValue("PA_NOMBRECC", bean.getNombreCeco())
                .addValue("PA_INDICADOR", bean.getIndicador())
                .addValue("PA_VALOR", bean.getValor());

        out = jdbcActualizaIndicador.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAREPINDICADOR.SP_INS_INDICADOR}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al ACTUALIZAR el Indicador");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean insertaPendientes(String idCeco, int indicador) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCECO", idCeco)
                .addValue("PA_INDICADOR", indicador);

        out = jdbcInsertaPendientes.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAREPINDICADOR.SP_CARGA_PND}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar el Ceco Pendiente");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public List<ReporteIndicadorDTO> consultaPendientes(String idCeco, String idIndicador) throws Exception {

        Map<String, Object> out = null;

        List<ReporteIndicadorDTO> listaCecos = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCECO", idCeco).addValue("PA_INDICADOR", idIndicador);

        out = jdbcConsultaPendientes.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAREPINDICADOR.SP_PENDIENTES}");

        listaCecos = (List<ReporteIndicadorDTO>) out.get("RCL_PENDIENTES");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener los Cecos Pendientes");
        } else {
            return listaCecos;
        }

        return null;
    }

}
