package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ConsultaAseguradorDTO;

public class ConsultaAseguradorBitacoraRowMapper implements RowMapper<ConsultaAseguradorDTO> {

	public ConsultaAseguradorDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConsultaAseguradorDTO consultaAseguradorBitacoraDTO = new ConsultaAseguradorDTO();
		consultaAseguradorBitacoraDTO.setFechaTermino(rs.getString("FDTERMINO"));
		consultaAseguradorBitacoraDTO.setIdCeco(rs.getInt("FCID_CECO"));
		
		return consultaAseguradorBitacoraDTO;
		
		
	}

}
