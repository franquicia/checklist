package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.VersionDTO;


public interface VersionDAO {
	
	public List<VersionDTO> obtieneVersion(String version, String sistema, String so) throws Exception;
	
	public List<String> obtieneUltVersion (String so) throws Exception;
	
	public int insertaVersion(VersionDTO bean) throws Exception;
	
	public boolean actualizaVersion(VersionDTO bean) throws Exception;
	
	public boolean eliminaVersion(int idVersion) throws Exception;

}
