package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.SoporteExpDAO;
import com.gruposalinas.checklist.domain.SoporteExpDTO;

public class SoporteExpBI {

	private static Logger logger = LogManager.getLogger(SoporteExpBI.class);

private List<SoporteExpDTO> listafila;
	
	@Autowired
	SoporteExpDAO soporteExpDAO;
		
	


	public List<SoporteExpDTO> obtieneDatos( String ceco ,String status, String tipoSuc) {
		
		try {
			listafila =  soporteExpDAO.obtieneDatos(ceco,status,tipoSuc);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
			
		}
		
		return listafila;			
	}
	

	public boolean actualiza(SoporteExpDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = soporteExpDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public boolean actualizaTipoSuc(SoporteExpDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = soporteExpDAO.actualizaTipoSuc(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public boolean actualizaAbol(SoporteExpDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = soporteExpDAO.actualizaAbol(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	public boolean actualizaPreg(SoporteExpDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = soporteExpDAO.actualizaPreg(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public boolean actualizaFechaSoftNvo(SoporteExpDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = soporteExpDAO.actualizaFechaSoftNvo(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
}