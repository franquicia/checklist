package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AsignaExpDTO;
import com.gruposalinas.checklist.mappers.AsignaExpAsigRowMapper;
import com.gruposalinas.checklist.mappers.AsignaExpCheckuURowMapper;
import com.gruposalinas.checklist.mappers.AsignaExpRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AsignaExpDAOImpl extends DefaultDAO implements AsignaExpDAO {

    private static Logger logger = LogManager.getLogger(AsignaExpDAOImpl.class);

    DefaultJdbcCall jdbcInsertaAsign;
    DefaultJdbcCall jdbcEliminaAsign;
    DefaultJdbcCall jdbcCargaNuevo;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneVersion;
    DefaultJdbcCall jbdcActualizaAsign;
    DefaultJdbcCall jbdcActualizaAsignCheckU;
    DefaultJdbcCall jdbcObtieneCheckusua;
    DefaultJdbcCall jdbcObtieneAsigna;
    DefaultJdbcCall jdbcCargaAsigna;

    public void init() {

        jdbcInsertaAsign = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNEXP")
                .withProcedureName("SP_INS_ASIG");

        jdbcEliminaAsign = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNEXP")
                .withProcedureName("SP_DEL_ASIGUSU");
        //carga cuando no existe en la tabla check_usua
        jdbcCargaNuevo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNEXP")
                .withProcedureName("SP_CARGAPROCESO");

        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNEXP")
                .withProcedureName("SP_SEL_DETALLE")
                .returningResultSet("RCL_ASIG", new AsignaExpRowMapper());

        jdbcObtieneVersion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNEXP")
                .withProcedureName("SP_SEL_ASIG")
                .returningResultSet("RCL_ASIG", new AsignaExpRowMapper());

        jbdcActualizaAsign = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNEXP")
                .withProcedureName("SP_ACT_ASIG");

        //modifica el ceco que le mande a activo 1 todos los checklist de la version
        jbdcActualizaAsignCheckU = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNEXP")
                .withProcedureName("SP_ACT_ASIGCEC");

        jdbcObtieneCheckusua = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNEXP")
                .withProcedureName("SP_SEL_ASIGCU")
                .returningResultSet("RCL_ASIG", new AsignaExpCheckuURowMapper());

        jdbcObtieneAsigna = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNEXP")
                .withProcedureName("SP_SEL_ASIGAS")
                .returningResultSet("RCL_ASIG", new AsignaExpAsigRowMapper());

        jdbcCargaAsigna = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNEXP")
                .withProcedureName("SP_CARGAASIG");

    }

    public int inserta(AsignaExpDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_USU", bean.getUsuario())
                .addValue("PA_VERSION", bean.getVersion())
                .addValue("PA_USUASIG", bean.getUsuario_asig())
                .addValue("PA_OBSERV", bean.getObs());

        out = jdbcInsertaAsign.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAASIGNEXP.SP_INS_ASIG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        //BigDecimal idTipoReturn = (BigDecimal) out.get("PA_FIIDESTADO");
        //idEmpFij = idTipoReturn.intValue();
        if (error != 1) {
            logger.info("Algo ocurrió al insertar el ceco");
            error = 0;
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean elimina(String ceco, String usuario_asign) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_USUASIG", usuario_asign)
                .addValue("PA_CECO", ceco);

        out = jdbcEliminaAsign.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAASIGNEXP.SP_DEL_ASIGUSU}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + usuario_asign + ")");
        } else {
            return true;
        }

        return false;

    }

    public boolean cargaNuevo() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaNuevo.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PAASIGNEXP.SP_DEL_ASIGUSU}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al las asignaciones ");
        } else {
            return true;
        }

        return false;

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<AsignaExpDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<AsignaExpDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneTodo.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAASIGNEXP.SP_SEL_DETALLE}");

        listaFijo = (List<AsignaExpDTO>) out.get("RCL_ASIG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los Empleados Fijos");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //parametro

    @SuppressWarnings("unchecked")
    public List<AsignaExpDTO> obtieneDatos(int version) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AsignaExpDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDVERS", version);

        out = jdbcObtieneVersion.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAASIGNEXP.SP_SEL_ASIG}");
        //lleva el nombre del cursor del procedure
        lista = (List<AsignaExpDTO>) out.get("RCL_ASIG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la version (" + version + ")");
        }
        return lista;

    }

    public boolean actualiza(AsignaExpDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_USU", bean.getUsuario())
                .addValue("PA_VERSION", bean.getVersion())
                .addValue("PA_USUASIG", bean.getUsuario_asig())
                .addValue("PA_OBSERV", bean.getObs())
                .addValue("PA_STATUS", bean.getIdestatus());

        out = jbdcActualizaAsign.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAASIGNEXP.SP_ACT_ASIG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado de la version  id( " + bean.getVersion() + ")");
        } else {
            return true;
        }
        return false;

    }

    //actualiza Check_usua
    public boolean actualizaCheckU(AsignaExpDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_USU", bean.getUsuario());

        out = jbdcActualizaAsignCheckU.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAASIGNEXP.SP_ACT_ASIG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar CHECKUSUA( " + bean.getCeco() + ")");
        } else {
            return true;
        }
        return false;

    }

    @SuppressWarnings("unchecked")
    public List<AsignaExpDTO> obtieneDatosCheckUsua(String usuario, String ceco) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AsignaExpDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_USUARIO", usuario)
                .addValue("PA_CECO", ceco);
        //.addValue("PA_CHECK", aux2);

        out = jdbcObtieneCheckusua.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAASIGNEXP.SP_SEL_ASIG}");
        //lleva el nombre del cursor del procedure
        lista = (List<AsignaExpDTO>) out.get("RCL_ASIG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los datos ");
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<AsignaExpDTO> obtieneDatosAsignaVer(String usuario) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AsignaExpDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_USUARIO", usuario);

        out = jdbcObtieneAsigna.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAASIGNEXP.SP_SEL_ASIG}");
        //lleva el nombre del cursor del procedure
        lista = (List<AsignaExpDTO>) out.get("RCL_ASIG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la version (" + usuario + ")");
        }
        return lista;

    }

    public boolean CargaAsig() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaAsigna.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAASIGNEXP.SP_CARGAASIG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la carga de Asigna");
        } else {
            return true;
        }
        return false;

    }

}
