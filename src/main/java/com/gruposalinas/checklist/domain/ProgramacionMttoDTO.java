package com.gruposalinas.checklist.domain;

public class ProgramacionMttoDTO {
	


		private int idProgramacion;
		private int idCeco;
		private int idProyecto;
		private int idUsuario;
		private int status;
		private String fechaProgramacion;
		private String fechaProgramacionInicial;
		private String comentarioReprogramacion;
		private String negocio;
		private String tickets;
		private String tipoVisita;
		private String cecoSuperior;
		private String nombreCeco;
		private String nombreProyecto;
		
		
		public String getNombreCeco() {
			return nombreCeco;
		}
		public void setNombreCeco(String nombreCeco) {
			this.nombreCeco = nombreCeco;
		}
		public String getNombreProyecto() {
			return nombreProyecto;
		}
		public void setNombreProyecto(String nombreProyecto) {
			this.nombreProyecto = nombreProyecto;
		}
		public int getIdProgramacion() {
			return idProgramacion;
		}
		public void setIdProgramacion(int idProgramacion) {
			this.idProgramacion = idProgramacion;
		}
		public int getIdCeco() {
			return idCeco;
		}
		public void setIdCeco(int idCeco) {
			this.idCeco = idCeco;
		}
		public int getIdProyecto() {
			return idProyecto;
		}
		public void setIdProyecto(int idProyecto) {
			this.idProyecto = idProyecto;
		}
		public int getIdUsuario() {
			return idUsuario;
		}
		public void setIdUsuario(int idUsuario) {
			this.idUsuario = idUsuario;
		}
		public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		public String getFechaProgramacion() {
			return fechaProgramacion;
		}
		public void setFechaProgramacion(String fechaProgramacion) {
			this.fechaProgramacion = fechaProgramacion;
		}
		public String getfechaProgramacionInicial() {
			return fechaProgramacionInicial;
		}
		public void setfechaProgramacionInicial(String fechaProgramacionInicial) {
			this.fechaProgramacionInicial = fechaProgramacionInicial;
		}
		public String getComentarioReprogramacion() {
			return comentarioReprogramacion;
		}
		public void setComentarioReprogramacion(String comentarioReprogramacion) {
			this.comentarioReprogramacion = comentarioReprogramacion;
		}
		public String getNegocio() {
			return negocio;
		}
		public void setNegocio(String negocio) {
			this.negocio = negocio;
		}
		public String getTickets() {
			return tickets;
		}
		public void setTickets(String tickets) {
			this.tickets = tickets;
		}
		public String getTipoVisita() {
			return tipoVisita;
		}
		public void setTipoVisita(String tipoVisita) {
			this.tipoVisita = tipoVisita;
		}
		public String getCecoSuperior() {
			return cecoSuperior;
		}
		public void setCecoSuperior(String cecoSuperior) {
			this.cecoSuperior = cecoSuperior;
		}
		@Override
		public String toString() {
			return "ProgramacionMttoDTO [idProgramacion=" + idProgramacion + ", idCeco=" + idCeco + ", idProyecto="
					+ idProyecto + ", idUsuario=" + idUsuario + ", status=" + status + ", fechaProgramacion="
					+ fechaProgramacion + ", fechaProgramacionInicial=" + fechaProgramacionInicial + ", comentarioReprogramacion="
					+ comentarioReprogramacion + ", negocio=" + negocio + ", tickets=" + tickets + ", tipoVisita="
					+ tipoVisita + ", cecoSuperior=" + cecoSuperior + ", nombreCeco=" + nombreCeco + ", nombreProyecto="
					+ nombreProyecto + "]";
		}
		
		
	
		
	}