package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.Coment7SDTO;


public interface Correo7SDAO {
	
	public List<Coment7SDTO> buscaComentarios(int idBitacora) throws Exception;
	
	public boolean validaCehcklist(int idBitacora) throws Exception;
	
	public double CalifCheck7S(int idBitacora) throws Exception;
	
	public List<Coment7SDTO> ObtieneChecklit(int idBitacora) throws Exception;
}
