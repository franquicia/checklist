<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">

<head>

<script src="../js/expancion/jquery-3.3.1.js"></script>
<script src="../js/expancion/jquery-ui-1.12.1.js"></script>

<script type="text/javascript">
var urlServer = '${urlServer}';
var comentarioAnterior = '${comentarios}';

$(document).ready(
		function() {
			$('#owl-carousel2').on(
					'initialized.owl.carousel changed.owl.carousel',
					function(e) {
						if (!e.namespace) {
							return;
						}
						var carousel = e.relatedTarget;
						$('#txtMdl2').html(
								" Item "
										+ '<span class="imgNum">'
										+ (carousel.relative(carousel
												.current()) + 1)
										+ '</span> de '
										+ carousel.items().length);
					}).owlCarousel({
				loop : false,
				margin : 10,
				nav : true,
				items : 1,
				dots : false,
			});

			$('.owl-stage-outer').zoomify({

				  // animation duration
				  duration: 200,

				  // easing effect
				  easing:   'linear',

				  // zoom scale
				  // 1 = fullscreen
				  scale:    0.9

				});

			$('#owl-carousel1').on(
					'initialized.owl.carousel changed.owl.carousel',
					function(e) {
						if (!e.namespace) {
							return;
						}
						var carousel = e.relatedTarget;
						$('#txtMdl1').html(
								" Item "
										+ '<span class="imgNum">'
										+ (carousel.relative(carousel
												.current()) + 1)
										+ '</span> de '
										+ carousel.items().length);
					}).owlCarousel({
				loop : false,
				margin : 10,
				nav : true,
				items : 1,
				dots : false,
			});

			$('.owl-stage-outer').zoomify({

				  // animation duration
				  duration: 200,

				  // easing effect
				  easing:   'linear',

				  // zoom scale
				  // 1 = fullscreen
				  scale:    0.9

				});

			
			$('#cuadro2').hide();	
			$('#form1').hide();
			
			$('#decisionSiBtn').prop( "disabled", true );
			
		    $("#imgInp").change(function(){
			    
		         if(validarExtension(this)) {

		             if(validarPeso(this)) {
			             
		 		        readURL(this);
		 	    	} else {
		 	    		 	alertify.alert('AVISO', 'El tamaño del archivo es mas grande del permitido');
						//alert("El tamaño del archivo es mas grande del permitido");

			 	    	}
		 		} else {
		 			alertify.alert('AVISO', 'El archivo debe ser una imagen');
					//alert("El archivo debe ser una imagen");
		 				
			 		}	
			    


			    });
			/*
			$('#filechooser').change(function(ev) {
				cargaHallazgo(ev);
			    // your code
			});*/

		});

function validarExtension(datos) {

	 var extensionesValidas = ".png, .gif, .jpeg, .jpg";
	var ruta = datos.value;
	var extension = ruta.substring(ruta.lastIndexOf('.') + 1).toLowerCase();
	var extensionValida = extensionesValidas.indexOf(extension);

	if(extensionValida < 0) {
            return false;
        } else {
            return true;
        }
    }

   // Validacion de peso del fichero en kbs

    function validarPeso(datos) {
        var pesoPermitido = 1024;
        

        if (datos.files && datos.files[0]) {

	    var pesoFichero = datos.files[0].size/1024;

	    if(pesoFichero > pesoPermitido) {
	        return false;
	    } else {
	        return true;
	    }
	}
    }


function aceptarHallazgo(idCeco,idResp,status,respuesta,ruta,fechaAutorizacion,fechaFin,comentario){

	alertify.confirm('AVISO', 'Deseas Dar Por Atendido el Hallazgo', 
			function(){
				updateHallazgo(idCeco,idResp,"3","SI",ruta,fechaAutorizacion,fechaFin,comentario);
		 		alertify.success('Ok');
		 	}, 
		 	function(){ 
			 	alertify.error('Cancelado');
			}
	);

	/*
    var opcion = confirm("Deseas Dar Por Atendido el Hallazgo");
    if (opcion == true) {
        updateHallazgo(idCeco,idResp,"3","SI",ruta,fechaAutorizacion,fechaFin,comentario);
	} else {
        alert("Cancelado");
	}
	*/
}

function divSiBtnAction(){
	/*
	//CUADRO 1 ES NO	
		$('#cuadro1').hide();		
		$('#cuadro2').show();
		$('#form1').show();

		$('#decisionSiBtn').prop( "disabled", false );
		

		$('#divSiBtn').css("background-color", "green");	
		$('#divNoBtn').css("background-color", "gray");	
		
		$('#comentario').prop('readonly', false);
		$('#comentario').val('');
		$('#comentario').prop('placeholder', 'COMENTARIO');
		*/
	//CUADRO 2 ES SI		
	$('#cuadro1').show();		
	$('#cuadro2').hide();	
	$('#form1').hide();	

	$('#decisionSiBtn').prop( "disabled", true );
	

	$('#divSiBtn').css("background-color", "gray");	
	$('#divNoBtn').css("background-color", "red");	

	$('#comentario').prop('readonly', true);
	$('#comentario').val('');
	$('#comentario').val(comentarioAnterior);
		
}

function divNoBtnAction(){

	//CUADRO 2 ES SI			
	$('#cuadro1').show();		
	$('#cuadro2').hide();	
	$('#form1').hide();	

	$('#decisionSiBtn').prop( "disabled", true );
	

	$('#divSiBtn').css("background-color", "gray");	
	$('#divNoBtn').css("background-color", "red");	

	$('#comentario').prop('readonly', true);
	$('#comentario').val('');
	$('#comentario').val(comentarioAnterior);
	
	
}

function regresar() {
	alertify.confirm('AVISO', 'Deseas Regesar a la pantalla anterior', 
			function(){
				window.history.back();
		 		alertify.success('Ok');
		 	}, 
		 	function(){ 
			 	alertify.error('Cancelado');
			}
	);
}

function actualizarHallazgo(ceco,idHallazgo,comentarioAnt,fechaFin,idResp){
	//ceco,idHallazgo,comentarioAnt,fechaFin
	if ($("#comentario").val().length <= 0){
		alertify.error('El campo comentario es obligatorio');
		//alert("El campo comentario es obligatorio");
		return
		}

	if ($("#evidenciaCargada").attr("src").includes("fotoBig")){
		alertify.error('La imagen es obligatoria');
		//alert("La imagen es obligatoria");
		return
		}
	alertify.confirm('AVISO', 'Deseas Dar Por Atendido el Hallazgo', 
			function(){
				//actualizarHallazgo(${ceco},${idHallazgo},'${comentarios}','${fechaFin}','${idResp}');
				//var idHallazgo = ${idHallazgo};
				//var comentarioAnt = '${comentarios}';
				//var fechaFin = '${fechaFin}'
				            
				var observacionNueva = $("#comentario").val();
				var img64="";
				img64 = document.getElementById("evidenciaCargada").src;
				
				var respuesta = "SI";    	
				var status = "3";
				$('#img64').val(img64);
				$('#ceco').val(ceco);   
				$('#observacionNueva').val(observacionNueva);   
				$('#respuesta').val(respuesta);   
				$('#status').val(status);   
				$('#comentarioAnt').val(comentarioAnt);   
				$('#idHallazgo').val(idHallazgo);   
				$('#fechaFin').val(fechaFin);  
				$('#idResp').val(idResp);  
				$("#formularioDatos").submit();

		 		alertify.success('Ok');
		 	}, 
		 	function(){ 
			 	alertify.error('Cancelado');
			}
	);
		 
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#evidenciaCargada').attr('src', e.target.result);
            $('#img64').val(e.target.result);   
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}


</script>


<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />

<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/secciones.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/estiloVentana.css" media="screen">


<link rel="stylesheet" type="text/css" href="../css/expancion/modal.css">
<link rel="stylesheet" type="text/css" href="../css/expancion/index.css">

<!-- CARRUSEL -->
<link rel="stylesheet" type="text/css" href="../css/expancion/zoomify.css">
<link rel="stylesheet" href="../css/expancion/owl.theme.defaultHallazgos.css">
<link rel="stylesheet" href="../css/expancion/owl.carouselHallazgos.css">

<script src="../js/expancion/owl.carousel.js"></script>
<script src="../js/expancion/zoomify.js"></script>
<!-- CARRUSEL -->

<script src="../js/expancion/alertify.js"></script>
<link rel="stylesheet" href="../css/expancion/alertify.css">

<style type="text/css">

.divContainer{
margin-left: auto;
margin-right: auto;
display: block;
width:50%;
height:70%;
background-color:#E8E8E8;
}

.divContenido{
	border-style: solid 1px;
	margin-top:2px;
	margin-left:auto;
	margin-right: auto;
	display: block;
	width:99%;
	height:9.5%;
}

.divImg{
	margin-top:2px;
	margin-left:auto;
	margin-right: auto;
	display: block;
	width:99%;
	height:70%;
	background-color:white; 
}

.divAccion{
margin-top:10px;
margin-left: auto;
margin-right: auto;
display: block;
width:50%;
height:5%;
}

.divContenido{
	margin-top:2px;
	margin-left:auto;
	margin-right: auto;
	display: block;
	width:99%;
	height:9.5%;
	background-color:white; 
}

.textoItem{
	text-align:center;
	margin-top:2px;
	width:100%;
	height:100%;
}

.btnRespuesta{
	margin-top:2px;
	width: 250px;
    height: 50px;
	margin-left:auto;
	margin-right: auto;
	display: auto;	
}

#divSiBtn{
    margin-left: 0%;
    margin-right: auto;
	float:left;
  	width:45px;
  	height:45px;
  	background-color:gray;
  	border-radius: 25px;
}

#divNoBtn{
	margin-left: auto;
    margin-right: 0%;
    float:right;
  	width:45px;
  	height:45px;
  	background-color:red;
  	border-radius: 25px;
}

.btnDecision{
	margin-top:2px;
	/*width: 300px; erick*/
    height: 50px;
	margin-left:auto;
	margin-right: auto;
	display: auto;	
}

#decisionSi{
	 margin-left: 0%;
    margin-right: auto;
	float:left;
  	width:45px;
  	height:45px;
}

#decisionNo{
	margin-left: auto;
    margin-right: 0%;
    float:right;
  	width:45px;
  	height:45px;
}

#divSiBtn:hover,#divNoBtn:hover,#decisionSi:hover,#decisionNo:hover {
  transform: scale(1.1); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
}
</style>
</head>

<body style="height: auto;">

	<div class="header h90">
		<span class="titulagsBold spBold fz146">ATENCIÓN DE ADIOCIONALES</span>
	</div>
	
	<div class="divContainer">
		
		<div class="divContenido">
			
			<div class="textoItem">
				<label style="font-size:18px;">${pregunta}</label>
				
			</div>
		
		</div>
	
		<div class="divContenido">
				<div class="btnRespuesta">
						<div id="divSiBtn" onclick="divSiBtnAction();">
							<label style="color:white; text-align:center; margin-left:auto; margin-right: auto; display: block; font-size:20px;"><b>SI</b></label>
						</div>
					  	
					  	<div id="divNoBtn" onclick="divNoBtnAction();">
					  		<label style="color:white; text-align:center; margin-left:auto; margin-right: auto; display: block; font-size:20px;"><b>NO</b></label>
					  	</div>
				</div>
		</div>
			
		<div class="divImg">
							${carrusel}
		</div>
		<div class="divContenido" style="padding-bottom:1px;">
			<textarea id="comentario" style="width:99%; height:95%; resize: none;" readonly maxlength="150">${comentarios}</textarea>
		</div>

		<div class="divAccion">
			<div class="btnDecision">
						<div id="decisionSi">
					  		<button id="decisionSiBtn" style="background-color:gray; color:white; text-align:center; margin-left:auto; margin-right: auto; display: block; font-size:20px;" onclick="actualizarHallazgo(${ceco},${idHallazgo},'${comentarios}','${fechaFin}','${idResp}');">Atender</button>
						</div>
					  	
					  	<div id="decisionNo">
					  		<button id="decisionNoBtn" style="background-color:gray; color:white; text-align:center; margin-left:auto; margin-right: auto; display: block; font-size:20px;" onclick="regresar();">Regresar</button>
					  	</div>
				</div>
				
				
	<form id="form1" runat="server">
        <input type='file' id="imgInp" />
    </form>
    
 
    <form id="formularioDatos" action="actualizaHallazgoImagen.htm" method="post">
    
    	<input type="hidden" id="ceco" value="" name="ceco">
    	<input type="hidden" id="observacionNueva" value="" name="observacionNueva">
    	<input type="hidden" id="img64" value="" name="img64">
    	<input type="hidden" id="respuesta" value="" name="respuesta">
    	<input type="hidden" id="status" value="" name="status">
    	<input type="hidden" id="comentarioAnt" value="" name="comentarioAnt">
    	<input type="hidden" id="idHallazgo" value="" name="idHallazgo">
    	<input type="hidden" id="fechaFin" value="" name="fechaFin">
    	<input type="hidden" id="idResp" value="" name="idResp">
    	

    </form>
    </div>
		
		
		
	<div class="clear"></div>
</body>
</html>