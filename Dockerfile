# Base image
FROM jboss/wildfly:24.0.0.Final

#User
USER root

# Maintainer
LABEL org.opencontainers.image.authors="cescobarh@elektra.com.mx"

# Enviroments
ENV DS_DB_HOST enyhostdb
ENV DS_DB_NAME dbname
ENV DS_DB_PORT 518
ENV DS_GTN_PL_MIS 0
ENV DS_GTN_PL_MAS 100
ENV DS_GTN_DB_USER dbuser
ENV DS_GTN_DB_PASSWORD dbpassword

ENV DS_FRQ_PL_MIS 0
ENV DS_FRQ_PL_MAS 100
ENV DS_FRQ_DB_USER dbuser
ENV DS_FRQ_DB_PASSWORD dbpassword

ENV DS_MODFRQ_PL_MIS 0
ENV DS_MODFRQ_PL_MAS 100
ENV DS_MODFRQ_DB_USER dbuser
ENV DS_MODFRQ_DB_PASSWORD dbpassword

#RUN yum -y install nfs-utils

#RUN mkdir /mnt/efs

# Create user admin with password OPsURhUKI2xaMxCqX65
RUN /opt/jboss/wildfly/bin/add-user.sh admin OPsURhUKI2xaMxCqX65 --silent

# Add custom configuration file
ADD resources/wildfly/modules/system/layers/base/org/postgresql /opt/jboss/wildfly/modules/system/layers/base/org/postgresql
ADD resources/wildfly/standalone/configuration/standalone.xml /opt/jboss/wildfly/standalone/configuration/
ADD resources/wildfly/bin/jboss-cli.xml /opt/jboss/wildfly/bin/

RUN rm -rf /etc/localtime
RUN ln -s /usr/share/zoneinfo/America/Mexico_City /etc/localtime

#RUN chmod 777 /opt/jboss/wildfly/bin/start.sh

# Add example.war to deployments
ADD target/checklist.war /opt/jboss/wildfly/standalone/deployments/

VOLUME /franquicia

VOLUME /logs

VOLUME /opt/jboss/wildfly/standalone/log

# JBoss ports
EXPOSE 8181 9999

HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD curl --fail http://localhost:8181/checklist/actuator/health || exit 1
# Run
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0", "-c", "standalone.xml"]
