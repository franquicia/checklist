package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.mappers.EmpFijoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class EmpFijoDAOImpl extends DefaultDAO implements EmpFijoDAO {

    private static Logger logger = LogManager.getLogger(EmpFijoDAOImpl.class);

    DefaultJdbcCall jdbcInsertaEmpFijo;
    DefaultJdbcCall jdbcEliminaEmpFijo;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneEmpFijo;
    DefaultJdbcCall jbdcActualizaEmpFijo;

    public void init() {

        jdbcInsertaEmpFijo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMEMPFIJO")
                .withProcedureName("SP_INS_EMP");

        jdbcEliminaEmpFijo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMEMPFIJO")
                .withProcedureName("SP_DEL_EMP");

        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMEMPFIJO")
                .withProcedureName("SP_SEL_EMPLEADOS")
                .returningResultSet("RCL_EMP", new EmpFijoRowMapper());

        jdbcObtieneEmpFijo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMEMPFIJO")
                .withProcedureName("SP_SEL_EMP")
                .returningResultSet("RCL_EMP", new EmpFijoRowMapper());

        jbdcActualizaEmpFijo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMEMPFIJO")
                .withProcedureName("SP_ACT_EMP");

    }

    public int inserta(EmpFijoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDEMP", bean.getIdUsuario())
                .addValue("PA_IDACT", bean.getIdActivo());

        out = jdbcInsertaEmpFijo.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMEMPFIJO.SP_INS_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_FIIDESTADO");
        idEmpFij = idTipoReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar el Empleado");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean elimina(String idUsuario) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDEMP", idUsuario);

        out = jdbcEliminaEmpFijo.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMEMPFIJO.SP_DEL_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al borrar el usuario(" + idUsuario + ")");
        } else {
            return true;
        }

        return false;

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<EmpFijoDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<EmpFijoDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneTodo.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMEMPFIJO.SP_SEL_EMPLEADOS}");

        listaFijo = (List<EmpFijoDTO>) out.get("RCL_EMP");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener los Empleados Fijos");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //parametro

    @SuppressWarnings("unchecked")
    public List<EmpFijoDTO> obtieneDatos(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<EmpFijoDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDEMP", idUsuario);

        out = jdbcObtieneEmpFijo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMEMPFIJO.SP_SEL_EMP}");
        //lleva el nombre del cursor del procedure
        lista = (List<EmpFijoDTO>) out.get("RCL_EMP");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener el empleado(" + idUsuario + ")");
        }
        return lista;

    }

    public boolean actualiza(EmpFijoDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIIDEMP", bean.getIdEmpFijo())
                .addValue("PA_IDEMP", bean.getIdUsuario())
                .addValue("PA_IDACT", bean.getIdActivo());

        out = jbdcActualizaEmpFijo.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMEMPFIJO.SP_ACT_EMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdEmpFijo() + ")");
        } else {
            return true;
        }
        return false;

    }

}
