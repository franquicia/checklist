package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ConsultaAsistenciaDAO;
import com.gruposalinas.checklist.domain.ConsultaAsistenciaDTO;


public class ConsultaAsistenciaBI {
		
	private static Logger logger = LogManager.getLogger(ConsultaAsistenciaDTO.class);
	
	@Autowired
	ConsultaAsistenciaDAO consultaAsistenciaDAO;
	
	List<ConsultaAsistenciaDTO> listaConsulta = null;

	public List<ConsultaAsistenciaDTO> obtieneAsistencia(ConsultaAsistenciaDTO bean){
	
		try {
			listaConsulta = consultaAsistenciaDAO.obtieneAsistencia(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
			logger.info(""+e);
		}			
		
		return listaConsulta;
	
	}
	
	
}