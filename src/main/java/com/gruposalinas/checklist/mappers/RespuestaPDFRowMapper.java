package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.RespuestaPdfDTO;

public class RespuestaPDFRowMapper implements RowMapper<RespuestaPdfDTO> {

	@Override
	public RespuestaPdfDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		RespuestaPdfDTO respuestaPdfDTO = new RespuestaPdfDTO();
		
		respuestaPdfDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		respuestaPdfDTO.setPregunta(rs.getString("PREGUNTA"));
		respuestaPdfDTO.setOrden(rs.getInt("FIORDEN_CHECK"));
		respuestaPdfDTO.setPreguntaPadre(rs.getInt("FIID_PREG_PADRE"));
		respuestaPdfDTO.setRespuesta(rs.getString("RESPUESTA"));
		respuestaPdfDTO.setCompromiso(rs.getString("FCDESCRIPCION"));
		respuestaPdfDTO.setResponsable(rs.getInt("FIID_RESPONSABLE"));
		respuestaPdfDTO.setFechaCompromiso(rs.getString("FECHA_COMP"));
		respuestaPdfDTO.setEvidencia(rs.getString("FCRUTA"));
		
		
		return respuestaPdfDTO;
	}

}
