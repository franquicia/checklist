package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.PreguntaDTO;

public interface PreguntaDAO {

	public List<PreguntaDTO> obtienePregunta() throws Exception;
	
	public List<PreguntaDTO> obtienePregunta(int idPregunta) throws Exception;
	
	public int insertaPregunta(PreguntaDTO bean) throws Exception;
	
	//imperdonables
	public int insertaPreguntaCom(PreguntaDTO bean) throws Exception;
	
	//imperdonables
	public boolean actualizaPreguntaCom(PreguntaDTO bean) throws Exception;
	
	public boolean actualizaPregunta(PreguntaDTO bean) throws Exception;
	
	public boolean eliminaPregunta(int idPregunta) throws Exception;
	
}
