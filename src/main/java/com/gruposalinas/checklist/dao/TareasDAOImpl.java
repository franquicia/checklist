package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.TareaDTO;
import com.gruposalinas.checklist.mappers.TareasRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class TareasDAOImpl extends DefaultDAO implements TareasDAO {

    private static Logger logger = LogManager.getLogger(TareasDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaTarea;
    private DefaultJdbcCall jdbcActualizaTarea;
    private DefaultJdbcCall jdbcEliminaTarea;
    private DefaultJdbcCall jdbcSeleccionaTarea;
    private DefaultJdbcCall jdbcSeleccionaActivas;

    private List<TareaDTO> listatareas;
    private List<TareaDTO> listatareasActivas;

    public void init() {

        jdbcInsertaTarea = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMTAREAS")
                .withProcedureName("SPINSERTATAREA");

        jdbcActualizaTarea = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMTAREAS")
                .withProcedureName("SPACTUALIZATAREA");

        jdbcEliminaTarea = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMTAREAS")
                .withProcedureName("SPELIMINATAREA");

        jdbcSeleccionaTarea = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMTAREAS")
                .withProcedureName("SPOBTIENETAREA")
                .returningResultSet("RCL_TAREAS", new TareasRowMapper());

        jdbcSeleccionaActivas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMTAREAS")
                .withProcedureName("SPOBTIENEACTIVA")
                .returningResultSet("RCL_TAREAS", new TareasRowMapper());

    }

    @Override
    public boolean insertaTarea(TareaDTO bean) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CVETAREA", bean.getCveTarea())
                .addValue("PA_ACTIVO", bean.getActivo())
                .addValue("PA_FHTAREA", bean.getStrFechaTarea());

        out = jdbcInsertaTarea.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMTAREAS.SPINSERTATAREA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al insertar la Tarea : " + bean.getCveTarea());
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaTarea(TareaDTO bean) throws Exception {

        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CVETAREA", bean.getCveTarea())
                .addValue("PA_FHTAREA", bean.getStrFechaTarea())
                .addValue("PA_ACTIVO", bean.getActivo())
                .addValue("PA_IDTAREA", bean.getIdTarea());

        out = jdbcActualizaTarea.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMTAREAS.SPACTUALIZATAREA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al actualizar la Tarea ");
        } else {
            return true;
        }

        return false;

    }

    @Override
    public boolean eliminaTarea(int idTarea) throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDTAREA", idTarea);

        out = jdbcEliminaTarea.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMTAREAS.SPELIMINATAREA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo paso al elimnar la Tarea ");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TareaDTO> obtieneTareas() throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        out = jdbcSeleccionaTarea.execute();

        //logger.info("Funcion ejecutada: {checklist.PAADMTAREAS.SPOBTIENETAREA}");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCCION");
        respuesta = resultado.intValue();

        listatareas = (List<TareaDTO>) out.get("RCL_TAREAS");

        if (respuesta == 0) {
            logger.info("Algo paso al consular las tareas ");
        }

        return listatareas;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TareaDTO> obtieneActivas() throws Exception {
        int respuesta = 0;

        Map<String, Object> out = null;

        out = jdbcSeleccionaActivas.execute();

        logger.info("Funcion ejecutada: {checklist.PAADMTAREAS.SPOBTIENEACTIVA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCCION");
        respuesta = resultado.intValue();

        listatareasActivas = (List<TareaDTO>) out.get("RCL_TAREAS");

        if (respuesta == 0) {
            logger.info("Algo paso al consultar las tareas ");
        }

        return listatareasActivas;
    }
}
