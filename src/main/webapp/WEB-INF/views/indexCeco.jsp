<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="../css/bootstrap/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/jQuery/jquery-ui.css">

	<script	src="../js/jquery/jquery-2.2.4.min.js"></script>
	<script	src="../js/bootstrap.min.js"></script>
	<script	src="../js/jquery/jquery-ui.js"></script>
	
	<script src="../js/script-ceco-alta.js"></script>

<title>Soporte - CECO</title>
</head>
<body>

	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuSoporte.jsp" flush="true">
		<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuSoporte.jsp" />
	</tiles:insertTemplate>
	
	<br>
	<c:choose>
		<c:when test="${(paso=='1') && (alta=='si')}">
			<div class="container">
				<c:url value = "/soporte/altaCeco.htm" var = "altaCecoUrl" />
				<form:form method="POST" action="${altaCecoUrl}" model="command" name="form1" id="form1">
					<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
						<form:input type="text" class="form-control" id="activo1" placeholder="Activo" path="activo" />
						<form:input type="text" class="form-control" id="idCanal1" placeholder="Canal" path="idCanal" />
						<form:input type="text" class="form-control" id="idCeco1" placeholder="CECO" path="idCeco" />
						<form:input type="text" class="form-control" id="descCeco1" placeholder="Descripción CECO" path="descCeco" />
						<form:input type="text" class="form-control" id="idCecoSuperior1" placeholder="CECO Superior" path="idCecoSuperior" />
						<form:input type="text" class="form-control" id="calle1" placeholder="Calle" path="calle" />
						<form:input type="text" class="form-control" id="ciudad1" placeholder="Ciudad" path="ciudad" />
						<form:input type="text" class="form-control" id="idEstado1" placeholder="Estado" path="idEstado" />
						<form:input type="text" class="form-control" id="idPais1" placeholder="País" path="idPais" />
						<form:input type="text" class="form-control" id="cp1" placeholder="Código Postal" path="cp" />
						<form:input type="text" class="form-control" id="fechaModifico1" placeholder="Fecha modifico" path="fechaModifico" autocomplete="off" readonly="true" />
						<form:input type="text" class="form-control" id="idNegocio1" placeholder="Negocio" path="idNegocio" />
						<form:input type="text" class="form-control" id="idNivel1" placeholder="Nivel" path="idNivel" />
						<form:input type="text" class="form-control" id="nombreContacto1" placeholder="Nombre" path="nombreContacto" />
						<form:input type="text" class="form-control" id="puestoContacto1" placeholder="Puesto" path="puestoContacto" />
						<form:input type="text" class="form-control" id="telefonoContacto1" placeholder="Teléfono" path="telefonoContacto" />
						<form:input type="text" class="form-control" id="faxContacto1" placeholder="FAX" path="faxContacto" />
						<form:input type="text" class="form-control" id="usuarioModifico1" placeholder="Usuario modifico" path="usuarioModifico" />
						<input type="submit" class="btn btn-default btn-lg" value="Agregar CECO" onclick="return validaAlta();" />
					</div>
				</form:form>
			</div>
		</c:when>

		<c:when test="${(paso=='2') && (alta=='si')}">
			<c:choose>
				<c:when test="${res eq false}">
					<div class="container">
						<div class="alert alert-danger" role="alert">
							<p class="text-center"> 
								<h4>El CECO no fue creado correctamente!</h4>
								<br>Presione el botón de regresar para intentarlo nuevamente.
							<p>
							<br>
							<c:url value="/soporte/altaCeco.htm" var="altaCecoUrl" />
							<form:form method="GET" action="${altaCecoUrl}" model="command" name="form-regresar1">
								<input type="submit" class="btn btn-danger btn-lg" value="Regresar" />
							</form:form>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="container">
						<div class="alert alert-success" role="alert">
							<p class="text-center">
								<h4>CECO creado correctamente!</h4>
								<br>Presione el botón de regresar para insertar otro CECO.
							<p>
							<br>
							<c:url value="/soporte/altaCeco.htm" var="altaCecoUrl" />
							<form:form method="GET" action="${altaCecoUrl}" model="command" name="form-regresar2">
								<input type="submit" class="btn btn-success btn-lg" value="Regresar" />
							</form:form>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</c:when>
	</c:choose>
</body>
</html>