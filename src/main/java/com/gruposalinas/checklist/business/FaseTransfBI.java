package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.FaseTransfDAO;
import com.gruposalinas.checklist.domain.FaseTransfDTO;

public class FaseTransfBI {

	private static Logger logger = LogManager.getLogger(FaseTransfBI.class);

private List<FaseTransfDTO> listafila;
	
	@Autowired
	FaseTransfDAO faseTransfDAO;
		
	
	public int inserta(FaseTransfDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = faseTransfDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean elimina(String idTab){
		boolean respuesta = false;
		
		try {
			respuesta = faseTransfDAO.elimina(idTab);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	
	
	public List<FaseTransfDTO> obtieneDatos(String fase) {

		try {
			listafila = faseTransfDAO.obtieneDatos(fase);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}
	
	
	public boolean actualiza(FaseTransfDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = faseTransfDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
//agrupador	
	public int insertaAgrupa(FaseTransfDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = faseTransfDAO.insertaAgrupa(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaAgrupa(String idTab){
		boolean respuesta = false;
		
		try {
			respuesta = faseTransfDAO.eliminaAgrupa(idTab);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	
	
	public List<FaseTransfDTO> obtieneDatosAgrupa(String fase) {

		try {
			listafila = faseTransfDAO.obtieneDatosAgrupa(fase);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}
	
	
	public boolean actualizaAgrupa(FaseTransfDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = faseTransfDAO.actualizaAgrupa(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	



}