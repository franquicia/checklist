package com.gruposalinas.checklist.domain;

public class RepMedicionDTO {
		
	//Entrada
	private int idProtocolo;
	private String fechaInicio;
	private String fechaFin;
	//Cursor
	private String fecha;
	private String empleado;
	private int idEmpleado;
	private String puesto;
	private String pais;
	private String territorio;
	private String zona;
	private String regional;
	private String sucursal;
	private String nomSucursal;
	private String canal;
	private double ponderacion;
	private double calificacion;
	private int contSi;
	private int contNo;
	private int contImp;
	private String descPregunta;
	private String descRespuesta;
	private int idPregunta;
	private String Pregunta;
	private int numSerie;
	private int idCheckList;
	
	public int getIdProtocolo() {
		return idProtocolo;
	}
	public void setIdProtocolo(int idProtocolo) {
		this.idProtocolo = idProtocolo;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getEmpleado() {
		return empleado;
	}
	public void setEmpleado(String empleado) {
		this.empleado = empleado;
	}
	public int getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(int numEmpleado) {
		this.idEmpleado = numEmpleado;
	}
	public String getPuesto() {
		return puesto;
	}
	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getTerritorio() {
		return territorio;
	}
	public void setTerritorio(String territorio) {
		this.territorio = territorio;
	}
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	public String getRegional() {
		return regional;
	}
	public void setRegional(String regional) {
		this.regional = regional;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getNomSucursal() {
		return nomSucursal;
	}
	public void setNomSucursal(String nomSucursal) {
		this.nomSucursal = nomSucursal;
	}
	public String getCanal() {
		return canal;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
	public double getPonderacion() {
		return ponderacion;
	}
	public void setPonderacion(double ponderacion) {
		this.ponderacion = ponderacion;
	}
	public double getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(double calificacion) {
		this.calificacion = calificacion;
	}
	public int getContSi() {
		return contSi;
	}
	public void setContSi(int contSi) {
		this.contSi = contSi;
	}
	public int getContNo() {
		return contNo;
	}
	public void setContNo(int contNo) {
		this.contNo = contNo;
	}
	public int getContImp() {
		return contImp;
	}
	public void setContImp(int contImp) {
		this.contImp = contImp;
	}
	public String getDescPregunta() {
		return descPregunta;
	}
	public void setDescPregunta(String descPregunta) {
		this.descPregunta = descPregunta;
	}
	public String getDescRespuesta() {
		return descRespuesta;
	}
	public void setDescRespuesta(String descRespuesta) {
		this.descRespuesta = descRespuesta;
	}
	
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public int getNumSerie() {
		return numSerie;
	}
	public void setNumSerie(int numSerie) {
		this.numSerie = numSerie;
	}
	public int getIdCheckList() {
		return idCheckList;
	}
	public void setIdCheckList(int idCheckList) {
		this.idCheckList = idCheckList;
	}	
	public String getPregunta() {
		return Pregunta;
	}
	public void setPregunta(String pregunta) {
		Pregunta = pregunta;
	}
	@Override
	public String toString() {
		return "RepMedicionDTO [idProtocolo=" + idProtocolo + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin
				+ ", fecha=" + fecha + ", empleado=" + empleado + ", numEmpleado=" + idEmpleado + ", puesto=" + puesto
				+ ", pais=" + pais + ", territorio=" + territorio + ", zona=" + zona + ", regional=" + regional
				+ ", sucursal=" + sucursal + ", nomSucursal=" + nomSucursal + ", canal=" + canal + ", ponderacion="
				+ ponderacion + ", calificacion=" + calificacion + ", contSi=" + contSi + ", contNo=" + contNo
				+ ", contImp=" + contImp + ", descPregunta=" + descPregunta + ", descRespuesta=" + descRespuesta + "]";
	}
	
	
	
}
