package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.TipoChecklistDTO;

public class CheckOfflineRowMapper  implements RowMapper<ChecklistDTO> {
	
	public ChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistDTO checklistDTO = new ChecklistDTO();
		TipoChecklistDTO tipochecklist = new TipoChecklistDTO(); 
		
		tipochecklist.setIdTipoCheck(rs.getInt("FIID_TIPO_CHECK"));
		tipochecklist.setDescTipo(rs.getString("FCDESCRIPCION"));
		checklistDTO.setValorIni(rs.getString("FCVALOR_INI"));
		checklistDTO.setValorFin("FCVALOR_FIN");
	    
		checklistDTO.setIdTipoChecklist(tipochecklist);
		checklistDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		checklistDTO.setNombreCheck(rs.getString("FCNOMBRE"));
		checklistDTO.setFecha_inicio(rs.getString("FDFECHA_INICIO"));
		checklistDTO.setFecha_fin(rs.getString("FDFECHA_FIN"));
		
		return checklistDTO;
	}
}
