package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.TransformacionDTO;


public interface TransformacionDAO {

		//automatizado
	  	public int inserta(TransformacionDTO bean)throws Exception;
	  	
	  	//manual
		public int insertaMan(TransformacionDTO bean)throws Exception;
		
		public boolean elimina(String ceco,String recorrido)throws Exception;
		
		public List<TransformacionDTO> obtieneDatos(String ceco) throws Exception; 
		
		public boolean actualiza(TransformacionDTO bean)throws Exception;
		
	}