package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ConsultaFechasDAO;

import com.gruposalinas.checklist.domain.ConsultaFechasDTO;


public class ConsultaFechasBI {
		
	private static Logger logger = LogManager.getLogger(ConsultaFechasDTO.class);
	
	@Autowired
	ConsultaFechasDAO consultaFechasDAO;
	
	List<ConsultaFechasDTO> listaConsulta = null;

	public List<ConsultaFechasDTO> obtieneFecha(ConsultaFechasDTO bean){
	
		try {
			listaConsulta = consultaFechasDAO.obtieneFecha(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
			logger.info(""+e);
		}			
		
		return listaConsulta;
	
	}
	
	public boolean insertaFecha(ConsultaFechasDTO bean){
		
		boolean respuesta = false;
		
		try {
			respuesta = consultaFechasDAO.InsertaFecha(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar la asignacion");
				
		}
		
		return respuesta;
	}
	
public boolean ActualizaFecha(ConsultaFechasDTO bean){
		
		boolean respuesta = false;
		
		try {
			respuesta = consultaFechasDAO.ActualizaFecha(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar dia festivo");
				
		}
		
		return respuesta;

}

public boolean EliminaFecha(String fechaInicial,String fechaFinal){
	
	boolean respuesta = false;
	
	try {
		respuesta = consultaFechasDAO.EliminaFecha(fechaInicial,fechaFinal);
	} catch (Exception e) {
		logger.info("No fue posible eliminar fecha"+e);
			
	}
	
	return respuesta;
}

}