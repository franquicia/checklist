<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>Detalle del protocolo</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervision/estilos.css">
  <!-- Tablas/Paginado -->
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervision/paginador.css">
  <script type="text/javascript">
    	

    		function fechas(foto){
        		 
    			 document.getElementById('fAntes').src = "http://10.53.33.82"+foto;
    			 //document.getElementById('fAntes').src = "http://10.53.33.82/franquicia/imagenes/2019/95170_3447277_13795_ORDEN_1_2019092016213.jpg";
				//alert("Ruta evidencia: "+ foto);
			        } 
    	</script>
</head>

<body ng-app="app" ng-controller="validarForm">

  <div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="${pageContext.request.contextPath}/img/supervision/btn_hamburguer.svg" alt="Men� principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="inicio.htm"><img src="${pageContext.request.contextPath}/img/supervision/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.htm" class="selMenu">
          <div>Salir</div>
          <div><img src="${pageContext.request.contextPath}/img/supervision/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="indexSupervision.htm">Reporte de Supervisores</a> 
		</div>
    	Detalle del protocolo
    </div>

	<!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervision/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Men� Principal<br>
          </div>
        </div>
        <%-- <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la p�gina">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> --%>
        <div id="menu">
        	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuAseguramiento.jsp" flush="true">
				<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuAseguramiento.jsp" />
			</tiles:insertTemplate>
          <!-- <ul class="l-navegacion nivel1">
          		<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Supervisor</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>B�squeda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>B�squeda Supervisor</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>
				<li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>s
			</ul> -->
        </div>
      </div>
    </div>

    <!-- Contenido -->
    <div class="contSecc">
    
    <div class="liga">
		<a href="#" onclick="javascript:window.history.back()" class="liga">
		<img style="height: 15px; width: 15px;" src="${pageContext.request.contextPath}/img/supervision/arrowmleft.png">
		<span>P�GINA ANTERIOR</span>
		</a>
		</div>
    
		<div class="titSec"></div>
		<div class="gris">
		
			<div class="datProt">
				<div>
		
				<c:url value="/central/busquedaSucursal.htm" var="enviaDatos" />
				<form:form method="POST" action="${enviaDatos}" model="command" name="formDatos" id="formDatos">
					<div><strong>Sucursal:</strong> <a href="#" onclick="javascript:enviarDatos(${idSucursal});" class="liga">${sucursal}</a></div>
					<input id="idSucursal" name="idSucursal" type="hidden" value="" >
					<input id="nomSucursal" name="nomSucursal" type="hidden" value="${sucursal}" >
					<input id="nomZona" name="nomZona" type="hidden" value="${zona}" >
					<input id="nomRegion" name="nomRegion" type="hidden" value="${region}" >
					<input id="nomTerritorio" name="nomTerritorio" type="hidden" value="${territorio}" >
					<input id="idProtocolo" name="idProtocolo" type="hidden" value="${idProtocolo}" >
					<input id="idTerritorio" name="idTerritorio" type="hidden" value="${idTerritorio}" >
					<input id="idZona" name="idZona" type="hidden" value="${idZona}" >
					<input id="idRegion" name="idRegion" type="hidden" value="${idRegion}" >
				</form:form>
					<table>
						<tbody>
							<table>
								<tr>
									<td><strong>Zona: </strong></td>
									<td>${zona}</td>
								</tr>
								<tr>
									<td><strong>Regi�n: </strong></td>
									<td>${region}</td>
								</tr>
								<tr>
									<td><strong>Territorio: </strong></td>
									<td>${territorio}</td>
								</tr>
							</table>
						</tbody>
					</table>
				</div>
		
			
					<%-- <div class="flexJusDer">
						<div>
							Protocolo<br>
							<c:url value="/central/indexSupervision.htm" var="archivoPasivo" />
							<form:form method="POST" action="${archivoPasivo}" model="command" name="formProtocolo" id="formProtocolo">
								<select id="protocolo">
								  	<option value="">Selecciona el Protocolo</option>
								  	<option value="">Opci�n 2</option>
								  	<option value="">Opci�n 3</option>
								</select>	
							</form:form>				
						</div>
						
						<div>
							Zona de Sucursal<br>
							<c:url value="/central/indexSupervision.htm" var="archivoPasivo" />
							<form:form method="POST" action="${archivoPasivo}" model="command" name="formZona" id="formZona">
							
								<select id="zona">
								  	<option value="">Selecciona el Zona</option>
								  	<option value="">Opci�n 2</option>
								  	<option value="">Opci�n 3</option>
								</select>
							</form:form>
						</div>
					</div> --%>
				</div>
		</div>
		
		<%-- <c:choose>
		<c:when test="${paso == 1}"> --%>
		<div class="titSec">${nomProtocolo}</div>
		<div class="gris">
			<table id="registro" class="tblPaginador tblGeneral ">
	            <thead>
	              <tr>
	                <th></th>
	                <th></th>
	                <th></th>
	                <th></th>
	              </tr>
	            </thead>
	            <tbody>
	               <c:set var="nFilters" value="${0}" scope="request"/>
						<c:forEach var="modulos" items="${modulos}">
						
							<tr>
				                <td class="tLeft" ><b><c:out value="${modulos.nombre}"/></b></td>
				                 <td></td>
					             <td></td>
					             <td></td>
				            </tr>
				            
				            <c:set var="nomMod" value="mods${nFilters}" scope="request"/>
										
										
										<c:forEach var="list" items="${lista}">
										
										
											<c:choose>
												<c:when test="${list.idModulo == modulos.idModulo}">
												
													<tr>
										                <td class="tLeft"><c:out value="${list.pregunta}" /></td>
										                <td>
										                		<c:choose>
																	<c:when test="${list.idPosible == '4'}">
																		<c:out value="${list.respuestaAbierta}" />
																	</c:when>
																	<c:otherwise>
																		<c:out value="${list.posible}" />
																	</c:otherwise>
																</c:choose>
										                </td>
										                <td><c:out value="${list.ponderacion}" />%</td>
										                <td>
										                		<c:choose>
																	<c:when test="${empty list.ruta}">
																		NO APLICA
																		
																	</c:when>
																	<c:otherwise>
																		<a href="#" class="modal01_view" class="modal01_view" onclick='fechas("${list.ruta}")'><img src="${pageContext.request.contextPath}/img/supervision/imgIco.svg"></a>
																	</c:otherwise>
																</c:choose>
										                
										                </td>
										             </tr>			
												</c:when>
											</c:choose>
											
										</c:forEach>
						
						
						</c:forEach> 
						
	            </tbody>
	        </table>
		</div>
		
		<%-- </c:when>
		</c:choose> --%>
		
	</div>	<!-- Fin conSecc -->
	
	<!-- Footer -->
	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualizaci�n Febrero 2019</div>
	</div>
</div><!-- Fin page -->
</body>
</html>

<!-- Modal -->
<div id="modal01" class="modal">
  <div class="cuadro cuadroG">
    <a href="#" class="btnCerrar simplemodal-close"><img src="${pageContext.request.contextPath}/img/supervision/icoCerrar.svg"></a>
    <img id="fAntes" class="imgodal" src="${pageContext.request.contextPath}/img/supervision/Modex.png">
  </div>
  <div class="clear"></div><br>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/custom-file-input.js"></script><!--Unpload -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/funciones.js"></script>
<!-- Tablas/Paginado -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/paginador.js"></script> 
<script>
$(document).ready(function() {
    $('#registro').DataTable({searching: false, paging: true, info: true, ordering: false, pageLength : 15, lengthMenu: [[5], [5]]});
} );
</script>

<script type="text/javascript">

function enviarDatos(id) {
	document.getElementById('idSucursal').value = id;
	form = document.getElementById("formDatos");
	/* alert("ID: " + 
	document.getElementById('nomSucursal').value+", "+
	document.getElementById('nomZona').value+", "+
	document.getElementById('nomRegion').value+", "+
	document.getElementById('nomTerritorio').value); */
	form.submit();
}

</script>

