<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>Asignaci�n de Ceco</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supPI2/estilos.css">
  <!-- Tablas/Paginado -->
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supPI2/paginador.css">
</head>

<body ng-app="app" ng-controller="validarForm" onpaste="return false">

  <div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="${pageContext.request.contextPath}/img/supervisionPI2/btn_hamburguer.svg" alt="Men� principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="inicio.htm"><img src="${pageContext.request.contextPath}/img/supervisionPI2/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.html" class="selMenu">
          <div>Salir</div>
          <div><img src="${pageContext.request.contextPath}/img/supervisionPI2/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="#">Administrador</a> 
		</div>
    	Asignaci�n de Ceco
    </div>

  <!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervisionPI2/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Men� Principal<br>
          </div>
        </div>
        <%-- <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la p�gina">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> --%>
        <div id="menu">
          <ul class="l-navegacion nivel1">
				<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
				<li>
					<a href="#"><div>Administrador</div></a>
					<ul class="nivel2" style="height: 350px; overflow-y: auto;">
			            <li><a href="asignaciones.htm"><div>Asignaci�n de Perfiles</div></a></li>
			            <li><a href="asignaPersonal.htm"><div>Asignaci�n de Personal</div></a></li>
			            <li><a href="asignaCecos.htm"><div>Asignaci�n de Cecos</div></a></li>
			            <!-- <li><a href="asignaCecosMasiva.htm"><div>Asignaci�n de Cecos Masiva</div></a></li> -->
		          	</ul>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Asegurador</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>B&uacute;squeda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>B&uacute;squeda Asegurador</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galer&iacute;a de Evidencias</div></a>
				</li>
			</ul>
        </div>
      </div>
    </div>

  <!-- Contenido -->
  <div class="contSecc">
  
  	<div class="liga">
			<a href="inicio.htm" class="liga">
			<img style="height: 15px; width: 15px;" src="${pageContext.request.contextPath}/img/supervision/arrowmleft.png">
			<span>P&Aacute;GINA PRINCIPAL</span>
			</a>
	</div>
  
  	<c:url value="/central/asignaCecosMasiva.htm" var="asigna" />
	<form:form method="GET" action="${asigna}" model="command" name="formAsignaMasiva" id="formAsignaMasiva">
    	<div class="btnAsignar"><a href="#" class="btnA btnG btnGrande" onclick="asignacionMasiva();">Asignaci�n de Ceco Masivo</a></div>
    </form:form>
    
		<div class="titSec">Selecciona al Socio por medio de su n�mero de empleado para asignar o desasignar una sucursal.</div>
    <div class="gris">
      <div class="w33">
        <div class="divCol1">
        
         <c:url value="/central/asignaCecos.htm" var="envia" />
		<form:form method="POST" action="${envia}" model="command" name="form" id="form"> 
          <div class="col1">
            Buscar:<br>
            <div class="pRelative">
                <input type="text" id="busca" value="${numSocio}" placeholder="N�mero de Socio" maxlength="6" class="buscarNNSocio" onKeypress="return validaNumericos(event);">
                <input type="submit" class="buscar" onclick="resultado();">
		        <input type="hidden" id="numSocio" name="numSocio" value="" />
            </div>
          </div>
        </form:form>
        
        </div>
      </div>
    </div>
    
    <c:choose>    
    	<c:when test="${paso==1}">    
		    <div class="divResultadosCeco">
		      <c:forEach var="listaDetalle" items="${listaDetalle}">
				      <div class="w100">
				        <div><span class="bold">Socio:</span> ${listaDetalle.socio}</div>
				        <div><span class="bold">No. de Socio:</span> ${listaDetalle.numSocio}</div>
				        <div><span class="bold">Puesto:</span> ${listaDetalle.puesto}</div>
				      </div>
				      </c:forEach>
				      
		      <div class="titSec">Asignaci�n de Sucursales</div>
		      <div class="gris">
		        <table id="registro" class="tblPaginador tblGeneral tblAsigCeco">
		            <thead>
		              <tr>
		                <th>Sucursal</th>
		                <th>Ceco</th>
		                <th>Zona</th>
		                <th>Eliminar</th>
		              </tr>
		            </thead>
		            
		          <c:url value="/central/asignaCecosEliminar.htm" var="elimina" />
				  <form:form method="POST" action="${elimina}" model="command" name="formElimina" id="formElimina">
		            <tbody>
				            	<c:forEach var="listaCecos" items="${listaCecos}">
				            		<tr>
				            		<td>${listaCecos.sucursal}</td>
				            		<td>${listaCecos.ceco}</td>
				            		<td>${listaCecos.zona}</td>
				            		<td><a href="#" onclick="desasigna(${listaCecos.ceco});"><img src="${pageContext.request.contextPath}/img/supervisionPI2/delete.svg"></a></td>
				            		</tr>
				            	</c:forEach>
		             
		            </tbody>		            
				   <input type="hidden" id="idSocio" name="idSocio" value=""/>
				   <input type="hidden" id="idCeco" name="idCeco" value=""/>
				 </form:form>
				 
		        </table>
		        <div class="titCol">Selecciona el Ceco para asignarlo.</div><br>
		        <div class="divCol2">
		        
		        <c:url value="/central/asignaCecosBusqueda.htm" var="busqueda" />
				<form:form method="POST" action="${busqueda}" model="command" name="formBusca" id="formBusca">
		          <div class="col2" style="margin-left: 17px;">
		            Buscar:<br>
		            <div class="pRelative">
		                <input type="text" id="buscaCeco" placeholder="N�mero de Ceco" class="buscarNumCeco" onKeypress="return validaNumericos(event);" style="width: 500px;">
		                <input type="submit" class="buscar" onclick="busqueda();">
		                <input type="hidden" id="idSucu" name="idSucu" value="" />
		                <input type="hidden" id="nSocio" name="nSocio" value="" />
		            </div>
		          </div>
		        </form:form>  
		          
		         <c:choose>
		         <c:when test="${resultado==1}">
		         	<c:url value="/central/asignaCecosAgregar.htm" var="agrega" />
					<form:form method="POST" action="${agrega}" model="command" name="formAgrega" id="formAgrega">
					
		         	<c:forEach var="sucursal" items="${sucursal}">
			         	<div class="col2"><br>		         	
				            <div class="divResultadoBusquedaP">
				              <div class="bold" style="margin-left: 15px;">Resultado de la b�squeda:</div>
				              <div class="divAgregarPersonal" style="margin-left: 10px;">${sucursal.ceco} - ${sucursal.sucursal}<a href="#" onclick="agregarCeco(${sucursal.ceco});"><img src="${pageContext.request.contextPath}/img/supervisionPI2/icoAgregar3.svg"></a></div>
				            </div>			            
			          	</div>
			        </c:forEach>
			                	<input type="hidden" id="idSucursal" name="idSucursal" value="" />
			                	<input type="hidden" id="idUsuario" name="idUsuario" value="" />
		          	
		          	</form:form>
		         </c:when>
		          
		          <c:when test="${resultado==0}">
		         	<div class="col2"><br>
			            <div class="divResultadoBusquedaP">
			              <div class="bold">No se encontraron resultados.</div>
			            </div>
		          	</div>
		          </c:when>
		         
		          <c:when test="${resultado==2}">
		          </c:when>
		         
		         </c:choose>
		          
		        </div>
		      </div>
		      <div class="btnCenter">
		        <a href="#" class="btn btnA modalFinalizado_view">Guardar</a>
		      </div>
		    </div>
    </c:when>
    
    <c:when test="${paso==0}">
    
    <div class="divResultadosCeco">
      <c:forEach var="listaDetalle" items="${listaDetalle}">
		      <div class="w100">
		        <div><span class="bold">Socio:</span> ${listaDetalle.socio}</div>
		        <div><span class="bold">No. de Socio:</span> ${listaDetalle.numSocio}</div>
		        <div><span class="bold">Puesto:</span> ${listaDetalle.puesto}</div>
		      </div>
		      </c:forEach>
		      
      <div class="titSec">Asignaci�n de Sucursales</div>
      <div class="gris">
        <table id="registro" class="tblPaginador tblGeneral tblAsigCeco">
            <thead>
              <tr>
                <th>Sucursal</th>
                <th>Ceco</th>
                <th>Zona</th>
                <th>Eliminar</th>
              </tr>
            </thead>
            <tbody>
		            	<c:forEach var="lineaDirecta" items="${listaCecos}">
		            		<tr>
		            		<td>${listaCecos.sucursal}</td>
		            		<td>${listaCecos.ceco}</td>
		            		<td>${listaCecos.zona}</td>
		            		<td><a href="#" onclick="desasigna(${listaCecos.ceco});"><img src="${pageContext.request.contextPath}/img/supervisionPI2/delete.svg"></a></td>
		            		</tr>
		                	<input type="hidden" id="idSocio" name="idSocio" value="" />
		                	<input type="hidden" id="idceco" name="idceco" value="" />
		            	</c:forEach>
             
            </tbody>
        </table>
        <div class="titCol">Selecciona el Ceco para asignarlo.</div>
		        <div class="divCol2">
		        
		        <c:url value="/central/asignaCecosBusqueda.htm" var="busqueda" />
				<form:form method="POST" action="${busqueda}" model="command" name="formBusca2" id="formBusca2">
		          <div class="col2" style="margin-left: 17px;">
		            Buscar:<br>
		            <div class="pRelative">
		                <input type="text" id="buscaCeco2" placeholder="N�mero de Ceco" class="buscarNumCeco" onKeypress="return validaNumericos(event);" style="width: 500px;">
		                <input type="submit" class="buscar" onclick="busqueda2();">
		                <input type="hidden" id="idSucu2" name="idSucu2" value="" />
		                <input type="hidden" id="nSocio2" name="nSocio2" value="" />
		            </div>
		          </div>
		        </form:form>  
		          
		         <c:choose>
		         <c:when test="${resultado==1}">
		         	<c:url value="/central/asignaCecosAgregar.htm" var="agrega" />
					<form:form method="POST" action="${agrega}" model="command" name="formAgrega2" id="formAgrega2">
					
			          <c:forEach var="sucursal" items="${sucursal}">
				          <div class="col2"><br>
				            
					            <div class="divResultadoBusquedaP" >			            
					              <div class="bold" style="margin-left: 15px;">Resultado de la b�squeda:</div>
					              <div class="divAgregarPersonal" style="margin-left: 10px;">${sucursal.ceco} - ${sucursal.sucursal} <a href="#" onclick="agregarCeco2(${sucursal.ceco});"><img src="${pageContext.request.contextPath}/img/supervisionPI2/icoAgregar3.svg"></a>
				                	<input type="hidden" id="idSucursal2" name="idSucursal2" value="" />
				                	<input type="hidden" id="idUsuario2" name="idUsuario2" value="" />
					              </div>			              
					            </div>		           
				          </div>
			           </c:forEach>
			          
		          	</form:form>
		          
		         </c:when>
		          
		          <c:when test="${resultado==0}">
		         	<div class="col2"><br>
			            <div class="divResultadoBusquedaP">
			              <div class="bold">No se encontraron resultados.</div>
			            </div>
		          	</div>
		          </c:when>
		         
		          <c:when test="${resultado==2}">
		          </c:when>
		         
		         </c:choose>
		          
		        </div>
      </div>
      <div class="btnCenter">
        <a href="#" class="btn btnA modalFinalizado_view">Guardar</a>
      </div>
    </div>
    </c:when>
    
    </c:choose>
	</div>	<!-- Fin conSecc -->
	
	<!-- Footer -->
	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualizaci�n Noviembre 2019</div>
	</div>
</div><!-- Fin page -->

</body>
</html>

<!--Modal-->
<div id="modalFinalizado" class="modal">
  <div class="cuadro cuadroM">
    <a href="#" class="simplemodal-close btnCerrar"><img src="${pageContext.request.contextPath}/img/supervisionPI2/icoCerrar.svg"></a>
    <div class="titModal">Finalizado</div><br>

    <div class="w94Modal">
      <div class="clear"></div>
      <div class="tCenter">Se ha guardado con �xito las sucursales asignadas</div>
      <div>&nbsp;</div>
    </div>
    <br>
  </div>
  <div class="clear"></div><br>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/supPI2/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supPI2/custom-file-input.js"></script><!--Unpload -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/funciones.js"></script>

<!-- Tablas/Paginado -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/paginador.js"></script> 
<script>
$(document).ready(function() {
    $('#registro').DataTable();
} );
</script>

<script type="text/javascript">

function validaNumericos(event) {
    if(event.charCode >= 48 && event.charCode <= 57){
      return true;
     }
     return false;        
}

function resultado() {
	document.getElementById("numSocio").value = document.getElementById("busca").value;
	form = document.getElementById("form");
	form.submit();
}

function busqueda() {
	document.getElementById("nSocio").value = document.getElementById("busca").value;
	document.getElementById("idSucu").value = document.getElementById("buscaCeco").value;
	form = document.getElementById("formBusca");
	form.submit();
}

function agregarCeco(ceco) {
	document.getElementById("idUsuario").value = document.getElementById("busca").value;
	document.getElementById("idSucursal").value = ceco;
	form = document.getElementById("formAgrega");
	form.submit();
}

function desasigna(ceco) {
	document.getElementById("idCeco").value = ceco ;
	document.getElementById("idSocio").value = document.getElementById("busca").value;
	document.getElementById("idCeco").value = ceco;
	form = document.getElementById("formElimina");
	form.submit();
}

function asignacionMasiva() {
	form = document.getElementById("formAsignaMasiva");
	form.submit();
}

function busqueda2() {
	document.getElementById("nSocio2").value = document.getElementById("busca").value;
	document.getElementById("idSucu2").value = document.getElementById("buscaCeco2").value;
	form = document.getElementById("formBusca2");
	form.submit();
}

function agregarCeco2(ceco) {
	document.getElementById("idUsuario2").value = document.getElementById("busca").value;
	document.getElementById("idSucursal2").value = ceco;
	form = document.getElementById("formAgrega2");
	form.submit();
}


</script>
