/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.CecoPasoDTO;
import com.gruposalinas.checklist.domain.ListaTablaEKT;
import java.util.List;

/**
 *
 * @author kramireza
 */
public interface CecoWSDAO {

    public List<CecoPasoDTO> buscaCeco(String idCeco) throws Exception;

    public boolean insertaCeco(ListaTablaEKT bean) throws Exception;
    
    public boolean mergeOriginal() throws Exception;

    public boolean actualizaCeco(ListaTablaEKT bean) throws Exception;

    public boolean depuraTabla(String nombreTabla) throws Exception;

}
