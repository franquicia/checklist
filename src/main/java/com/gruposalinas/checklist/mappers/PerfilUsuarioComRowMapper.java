package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.PerfilUsuarioDTO;

public class PerfilUsuarioComRowMapper implements RowMapper<PerfilUsuarioDTO> {

	@Override
	public PerfilUsuarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		PerfilUsuarioDTO perfilUsuarioDTO = new PerfilUsuarioDTO();
		
		perfilUsuarioDTO.setIdPerfil(rs.getInt("FIIDPERFIL"));
		perfilUsuarioDTO.setIdUsuario(rs.getInt("FIIDUSUARIO"));
		
		return perfilUsuarioDTO;
	}

	
}
