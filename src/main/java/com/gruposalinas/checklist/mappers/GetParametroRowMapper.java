package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ParametroDTO;

public class GetParametroRowMapper implements RowMapper<ParametroDTO> {

	public ParametroDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ParametroDTO parametrosDTO = new ParametroDTO();
		parametrosDTO.setValorParametro(rs.getInt("FCVALOR"));
		return parametrosDTO;
	}
}
