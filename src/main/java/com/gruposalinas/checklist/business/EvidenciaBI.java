package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.EvidenciaDAO;
import com.gruposalinas.checklist.domain.EvidenciaDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class EvidenciaBI {

	private static Logger logger = LogManager.getLogger(EvidenciaBI.class);

	@Autowired
    EvidenciaDAO evidenciaDAO;
	
	List<EvidenciaDTO> listaEvidencias = null;
	List<EvidenciaDTO> listaEvidencia = null;
	

	public List<EvidenciaDTO> obtieneEvidencia(){
				
		try{
			listaEvidencias = evidenciaDAO.obtieneEvidencia();	
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Evidencia");
					
		}
				
		return listaEvidencias;
	}
	
	public List<EvidenciaDTO> obtieneEvidencia(int idEvidencia){
				
		try{
			listaEvidencia = evidenciaDAO.obtieneEvidencia(idEvidencia);	
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Evidencia");
			
		}
				
		return listaEvidencia;
	}	
	
	public int insertaEvidencia( EvidenciaDTO evidencia ) {
		
		int idEvidencia = 0;
		
		try {		
			idEvidencia = evidenciaDAO.insertaEvidencia(evidencia);
		} catch (Exception e) {
			logger.info("No fue posible insertar la Evidencia");
			
		}
		
		return idEvidencia;		
	}
	

	public boolean actualizaEvidencia (EvidenciaDTO evidencia){
		
		boolean respuesta = false;
				
		try {
			respuesta = evidenciaDAO.actualizaEvidencia(evidencia);
		} catch (Exception e) {
			logger.info("No fue posible actualizar la Evidencia");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaEvidencia(int idEvidencia){
		
		boolean respuesta = false;
		
		try{
			respuesta = evidenciaDAO.eliminaEvidencia(idEvidencia);
		}catch(Exception e){
			logger.info("No fue posible borrar la Evidencia");
			
		}
		
		return respuesta;
	}
	
}
