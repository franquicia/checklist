package com.gruposalinas.checklist.dao;

import java.util.List;
import com.gruposalinas.checklist.domain.PerfilesSupDTO;

public interface PerfilesSupDAO {
	
	public int insertPerfil(int idPerfil, String descPerfil, String activo) throws Exception;
	public int updatePerfil(int idPerfil, String descPerfil, String activo) throws Exception;
	public int deletePerfil(int idPerfil) throws Exception;
	public List<PerfilesSupDTO> getPerfil(String idPerfil) throws Exception;

}
