package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteSistemasDTO;

public class ReporteRespuestasSupervisionSistRowMapper implements RowMapper<ReporteSistemasDTO> {

	public ReporteSistemasDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteSistemasDTO reporteCecosSistemas = new ReporteSistemasDTO();
		
		reporteCecosSistemas.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		reporteCecosSistemas.setPregunta(rs.getString("PREGUNTA"));
		reporteCecosSistemas.setPonderacion(rs.getString("PONDERACION"));
		reporteCecosSistemas.setRespuesta(rs.getString("RESPUESTA"));
		reporteCecosSistemas.setIdrespuesta(rs.getInt("FIID_RESPUESTA"));
		
		return reporteCecosSistemas;
	}
}

