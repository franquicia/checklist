package com.gruposalinas.checklist.domain;

public class SucursalChecklistDTO {
	


		private int idSucursal;
		private int idEco;
		private String nombre;
		private String direccion;
		private String region;
		private String territorio;
		private String zona;
		private int statusSuc;
		private int idImag;
		private String nombreImg;
		private String ruta;
		private String observ;
		private String periodo;
		private int apert;
		private int recorrido;
		private int aux;
		private String aux2;
		private String negocio;
		private int idNego;
		
		
		public int getIdSucursal() {
			return idSucursal;
		}
		public void setIdSucursal(int idSucursal) {
			this.idSucursal = idSucursal;
		}
		public int getIdEco() {
			return idEco;
		}
		public void setIdEco(int idEco) {
			this.idEco = idEco;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getDireccion() {
			return direccion;
		}
		public void setDireccion(String direccion) {
			this.direccion = direccion;
		}
		public String getRegion() {
			return region;
		}
		public void setRegion(String region) {
			this.region = region;
		}
		public String getTerritorio() {
			return territorio;
		}
		public void setTerritorio(String territorio) {
			this.territorio = territorio;
		}
		public int getStatusSuc() {
			return statusSuc;
		}
		public void setStatusSuc(int statusSuc) {
			this.statusSuc = statusSuc;
		}
		public int getIdImag() {
			return idImag;
		}
		public void setIdImag(int idImag) {
			this.idImag = idImag;
		}
		public String getNombreImg() {
			return nombreImg;
		}
		public void setNombreImg(String nombreImg) {
			this.nombreImg = nombreImg;
		}
		public String getRuta() {
			return ruta;
		}
		public void setRuta(String ruta) {
			this.ruta = ruta;
		}
		public String getObserv() {
			return observ;
		}
		public void setObserv(String observ) {
			this.observ = observ;
		}
		
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		
		public String getZona() {
			return zona;
		}
		public void setZona(String zona) {
			this.zona = zona;
		}
		
		public int getApert() {
			return apert;
		}
		public void setApert(int apert) {
			this.apert = apert;
		}
		public int getRecorrido() {
			return recorrido;
		}
		public void setRecorrido(int recorrido) {
			this.recorrido = recorrido;
		}
		public int getAux() {
			return aux;
		}
		public void setAux(int aux) {
			this.aux = aux;
		}
		public String getAux2() {
			return aux2;
		}
		public void setAux2(String aux2) {
			this.aux2 = aux2;
		}
		
		public String getNegocio() {
			return negocio;
		}
		public void setNegocio(String negocio) {
			this.negocio = negocio;
		}
		public int getIdNego() {
			return idNego;
		}
		public void setIdNego(int idNego) {
			this.idNego = idNego;
		}
		@Override
		public String toString() {
			return "SucursalChecklistDTO [idSucursal=" + idSucursal + ", idEco=" + idEco + ", nombre=" + nombre
					+ ", direccion=" + direccion + ", region=" + region + ", territorio=" + territorio + ", zona="
					+ zona + ", statusSuc=" + statusSuc + ", idImag=" + idImag + ", nombreImg=" + nombreImg + ", ruta="
					+ ruta + ", observ=" + observ + ", periodo=" + periodo + ", apert=" + apert + ", recorrido="
					+ recorrido + ", aux=" + aux + ", aux2=" + aux2 + ", negocio=" + negocio + ", idNego=" + idNego
					+ "]";
		}
		
		
		
	}