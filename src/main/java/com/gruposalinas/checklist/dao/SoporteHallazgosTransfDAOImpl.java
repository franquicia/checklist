package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.FaseFirmasDTO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;
import com.gruposalinas.checklist.mappers.AdicionalesTransfRowMapper;
import com.gruposalinas.checklist.mappers.FaseFirmasRowMapper;
import com.gruposalinas.checklist.mappers.FaseOrdenRowMapper;
import com.gruposalinas.checklist.mappers.HallazgosTransfNvoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class SoporteHallazgosTransfDAOImpl extends DefaultDAO implements SoporteHallazgosTransfDAO {

    private static Logger logger = LogManager.getLogger(SoporteHallazgosTransfDAOImpl.class);

//Se implemento la consulta ya con la fase y no la version
    DefaultJdbcCall jdbcObtieneHallazgos;

    //se le manda fecha formato YYYYMMDD Y SE LE PUEDE MANDAR LA FASE sino se le manda toma la maxima del ceco que le mandes
    DefaultJdbcCall jbdcActualizaFaseHallazgo;
    //se le manda ceco o bitacora y depura en historico y hallazgo
    DefaultJdbcCall jbdcDepuraHallazgoBitacoraCeco;

    DefaultJdbcCall jbdcObtieneOrdenFaseFirmas;
    DefaultJdbcCall jbdcObtieneAdicionales;

    public void init() {

        //Se implemento la consulta ya con la fase y no la version
        jdbcObtieneHallazgos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLASOP")
                .withProcedureName("SP_SEL_HALLA")
                .returningResultSet("RCL_HALLAZ", new HallazgosTransfNvoRowMapper());

        //se le manda fecha formato YYYYMMDD Y SE LE PUEDE MANDAR LA FASE sino se le manda toma la maxima del ceco que le mandes
        jbdcActualizaFaseHallazgo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLASOP")
                .withProcedureName("SP_ACT_FASE");

        //se le manda ceco o bitacora y depura en historico y hallazgo
        jbdcDepuraHallazgoBitacoraCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLASOP")
                .withProcedureName("SP_DEL_HALLA");

        //Se trae orden fase y firmas
        jbdcObtieneOrdenFaseFirmas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLASOP")
                .withProcedureName("SP_SEL_ORDFASE")
                .returningResultSet("RCL_ORDEN", new FaseOrdenRowMapper())
                .returningResultSet("RCL_FIRMAS", new FaseFirmasRowMapper());

        //Obtiene Adicionales por ceco fase y proyecto
        jbdcObtieneAdicionales = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLASOP")
                .withProcedureName("SP_SEL_ADIC")
                .returningResultSet("RCL_ADIC", new AdicionalesTransfRowMapper());

    }

    //Se implemento la consulta ya con la fase y no la version
    @SuppressWarnings("unchecked")
    public List<HallazgosTransfDTO> obtieneHallazgosSoporte(String ceco, String fase, String proyecto) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto);

        out = jdbcObtieneHallazgos.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAHALLASOP.SP_SEL_HALLA}");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosTransfDTO>) out.get("RCL_HALLAZ");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos del ceco(" + ceco + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;

    }

    //se le manda fecha formato YYYYMMDD Y SE LE PUEDE MANDAR LA FASE sino se le manda toma la maxima del ceco que le mandes
    public boolean actualizaFaseHallazgos(HallazgosTransfDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_FECHA", bean.getPeriodo())
                .addValue("PA_FASE", bean.getFase());

        out = jbdcActualizaFaseHallazgo.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAHALLASOP.SP_ACT_FASE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar CECO del  id( " + bean.getCeco() + ")");
        } else {
            return true;
        }
        return false;

    }

    //se le manda ceco o bitacora y depura en historico y hallazgo
    public boolean eliminaHallazgosEHistorico(String bita, String ceco) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITACORA", bita)
                .addValue("PA_CECO", ceco);

        out = jbdcDepuraHallazgoBitacoraCeco.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAHALLASOP.SP_DEL_HALLA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el hallazgos(" + bita + " ceco " + ceco + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> obtieneOrdenFaseFirmas(String ceco, String fase, String proyecto) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<FaseFirmasDTO> listaOrdenFase = null;
        List<FaseFirmasDTO> listaFirmas = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto);

        out = jbdcObtieneOrdenFaseFirmas.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAHALLASOP.SP_SEL_ORDFASE}");

        listaOrdenFase = (List<FaseFirmasDTO>) out.get("RCL_ORDEN");
        listaFirmas = (List<FaseFirmasDTO>) out.get("RCL_FIRMAS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener info");
        } else {
            lista = new HashMap<String, Object>();
            lista.put("listaOrdenFase", listaOrdenFase);
            lista.put("listaFirmas", listaFirmas);

            return lista;

        }
        return lista;
    }

    //Se implemento la consulta ya con la fase y no la version trae adicionales de la tabla hallazgos
    @SuppressWarnings("unchecked")
    public List<HallazgosTransfDTO> obtieneAdicionalesCecoFaseProyecto(String ceco, String fase, String proyecto, int parametro) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto)
                .addValue("PA_PARAM", parametro);

        out = jbdcObtieneAdicionales.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAHALLASOP.SP_SEL_ADICIONALES}");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosTransfDTO>) out.get("RCL_ADIC");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos del ceco(" + ceco + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;

    }

}
