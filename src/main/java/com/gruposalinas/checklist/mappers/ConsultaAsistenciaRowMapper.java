package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ConsultaAsistenciaDTO;

public class ConsultaAsistenciaRowMapper implements RowMapper<ConsultaAsistenciaDTO> {

	public ConsultaAsistenciaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ConsultaAsistenciaDTO  consultaAsistenciaDTO = new ConsultaAsistenciaDTO();
		
		consultaAsistenciaDTO.setFimes(rs.getString("FIMES"));
		consultaAsistenciaDTO.setDiasHabiles(rs.getString("FIDIAS_HABILES"));
		consultaAsistenciaDTO.setAsistencias(rs.getString("FIASISTENCIAS"));
		consultaAsistenciaDTO.setPorcentaje(rs.getString("FIPORCENTAJE"));
		
		
		

		

		return consultaAsistenciaDTO;
	}

}
