package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistDTO;

public class ChecklistComboRowMapper implements RowMapper<ChecklistDTO> {

	public ChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistDTO checklistgeneral = new ChecklistDTO();
		
		checklistgeneral.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		checklistgeneral.setNombreCheck(rs.getString("FCNOMBRE"));
		
		return checklistgeneral;
	}
}
