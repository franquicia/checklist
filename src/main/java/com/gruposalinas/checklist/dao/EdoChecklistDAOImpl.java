package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.EdoChecklistDTO;
import com.gruposalinas.checklist.mappers.EdoChecklistRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class EdoChecklistDAOImpl extends DefaultDAO implements EdoChecklistDAO {

    private static Logger logger = LogManager.getLogger(EdoChecklistDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneEdoChecklist;
    DefaultJdbcCall jdbcInsertaEdoChecklits;
    DefaultJdbcCall jbdcActualizaEdoChecklist;
    DefaultJdbcCall jdbcEliminaEdoChecklist;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_EDOCKL")
                .withProcedureName("SP_SEL_G_EDOCKL")
                .returningResultSet("RCL_EDOCKL", new EdoChecklistRowMapper());

        jdbcObtieneEdoChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_EDOCKL")
                .withProcedureName("SP_SEL_EDOCKL")
                .returningResultSet("RCL_EDOCKL", new EdoChecklistRowMapper());
        jdbcInsertaEdoChecklits = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_EDOCKL")
                .withProcedureName("SP_INS_EDOCKL");

        jbdcActualizaEdoChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_EDOCKL")
                .withProcedureName("SP_ACT_EDOCKL");

        jdbcEliminaEdoChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_EDOCKL")
                .withProcedureName("SP_DEL_EDOCKL");
    }

    @SuppressWarnings("unchecked")
    public List<EdoChecklistDTO> obtieneEdoChecklist() throws Exception {
        Map<String, Object> out = null;
        List<EdoChecklistDTO> listaEdoChecklist = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_EDOCKL.SP_SEL_G_EDOCKL}");

        listaEdoChecklist = (List<EdoChecklistDTO>) out.get("RCL_EDOCKL");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Estado del Checklist");
        } else {
            return listaEdoChecklist;
        }

        return listaEdoChecklist;

    }

    @SuppressWarnings("unchecked")
    public List<EdoChecklistDTO> obtieneEdoChecklist(int idEdoCK) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<EdoChecklistDTO> listaEdoChecklist = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDESTADO", idEdoCK);

        out = jdbcObtieneEdoChecklist.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADMEDOCKL.SP_SEL_EDOCKL}");

        listaEdoChecklist = (List<EdoChecklistDTO>) out.get("RCL_EDOCKL");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Estado del Checklist con id(" + idEdoCK + ")");
        } else {
            return listaEdoChecklist;
        }

        return null;
    }

    public int insertaEdoChecklist(EdoChecklistDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEdoCK = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_DESCRIPCION", bean.getDescripcion());

        out = jdbcInsertaEdoChecklits.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADMEDOCKL.SP_INS_EDOCKL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_FIIDESTADO");
        idEdoCK = idTipoReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Estado del Checklist");
        } else {
            return idEdoCK;
        }

        return idEdoCK;
    }

    public boolean actualizaEdoChecklist(EdoChecklistDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDESTADO", bean.getIdEdochecklist())
                .addValue("PA_DESCRIPCION", bean.getDescripcion());

        out = jbdcActualizaEdoChecklist.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADMEDOCKL.SP_ACT_EDOCKL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Estado del Checklist  id( " + bean.getIdEdochecklist() + ")");
        } else {
            return true;
        }
        return false;

    }

    public boolean eliminaEdoCheckList(int idEdoCK) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDESTADO", idEdoCK);

        out = jdbcEliminaEdoChecklist.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADMEDOCKL.SP_DEL_EDOCKL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el estado Checklist id(" + idEdoCK + ")");
        } else {
            return true;
        }

        return false;

    }
}
