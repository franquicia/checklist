package com.gruposalinas.checklist.domain;

import java.util.List;

public class RegistroChecklistDTO {

	private ChecklistDTO listaChecklist;
	private List<PreguntaDTO> listaPregunta;
	private List<ChecklistPreguntaDTO> listaCheckpreg;
	private List<ArbolDecisionDTO> listaArbol;
	
	public ChecklistDTO getListaChecklist() {
		return listaChecklist;
	}
	public void setListaChecklist(ChecklistDTO listaChecklist) {
		this.listaChecklist = listaChecklist;
	}
	public List<PreguntaDTO> getListaPregunta() {
		return listaPregunta;
	}
	public void setListaPregunta(List<PreguntaDTO> listaPregunta) {
		this.listaPregunta = listaPregunta;
	}
	public List<ChecklistPreguntaDTO> getListaCheckpreg() {
		return listaCheckpreg;
	}
	public void setListaCheckpreg(List<ChecklistPreguntaDTO> listaCheckpreg) {
		this.listaCheckpreg = listaCheckpreg;
	}
	public List<ArbolDecisionDTO> getListaArbol() {
		return listaArbol;
	}
	public void setListaArbol(List<ArbolDecisionDTO> listaArbol) {
		this.listaArbol = listaArbol;
	}
	
	
	
}
