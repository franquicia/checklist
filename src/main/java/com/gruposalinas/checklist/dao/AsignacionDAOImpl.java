package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AsignacionDTO;
import com.gruposalinas.checklist.mappers.AsignacionRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AsignacionDAOImpl extends DefaultDAO implements AsignacionDAO {

    private Logger logger = LogManager.getLogger(AsignacionDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneAsignaciones;
    private DefaultJdbcCall jdbcInsertaAsignacion;
    private DefaultJdbcCall jdbcActualizaAsignacion;
    private DefaultJdbcCall jdbcEliminaAsignacion;
    private DefaultJdbcCall jdbcEjecutaAsignacion;

    public void init() {
        jdbcObtieneAsignaciones = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMASIGNACION")
                .withProcedureName("SPCONSULTA")
                .returningResultSet("RCL_CONSULTA", new AsignacionRowMapper());

        jdbcInsertaAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMASIGNACION")
                .withProcedureName("SPINSERTA");

        jdbcActualizaAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMASIGNACION")
                .withProcedureName("SPACTUALIZA");

        jdbcEliminaAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMASIGNACION")
                .withProcedureName("SPELIMINA");

        jdbcEjecutaAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMASIGNACION")
                .withProcedureName("SPREASIGNA");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<AsignacionDTO> obtieneAsignaciones(String idChecklist, String idCeco, String idPuesto, String activo)
            throws Exception {

        List<AsignacionDTO> consulta = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECKLIST", idChecklist).addValue("PA_CECO", idCeco).addValue("PA_PUESTO", idPuesto).addValue("PA_ACTIVO", activo);

        out = jdbcObtieneAsignaciones.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMASIGNACION.SPCONSULTA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        consulta = (List<AsignacionDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al consultar las asignaciones");
        }

        return consulta;
    }

    @Override
    public boolean insertaAsignacion(AsignacionDTO asignacion) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", asignacion.getIdChecklist())
                .addValue("PA_CECO", asignacion.getCeco())
                .addValue("PA_PUESTO", asignacion.getIdPuesto())
                .addValue("PA_ACTIVO", asignacion.getActivo());

        out = jdbcInsertaAsignacion.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMASIGNACION.SPINSERTA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al insertar la asignacion");
            return false;
        }

        return true;
    }

    @Override
    public boolean actualizaAsignacion(AsignacionDTO asignacion) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", asignacion.getIdChecklist())
                .addValue("PA_CECO", asignacion.getCeco())
                .addValue("PA_PUESTO", asignacion.getIdPuesto())
                .addValue("PA_ACTIVO", asignacion.getActivo());

        out = jdbcActualizaAsignacion.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMASIGNACION.SPACTUALIZA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al actualizar la asignacion");
            return false;
        }

        return true;
    }

    @Override
    public boolean eliminaAsignacion(AsignacionDTO asignacion) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", asignacion.getIdChecklist())
                .addValue("PA_CECO", asignacion.getCeco())
                .addValue("PA_PUESTO", asignacion.getIdPuesto());

        out = jdbcEliminaAsignacion.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMASIGNACION.SPELIMINA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al eliminar la asignacion");
            return false;
        }

        return true;
    }

    @Override
    public Map<String, Object> ejecutaAsignacion() throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> respuesta = new HashMap<String, Object>();
        int ejecucion = 0;

        out = jdbcEjecutaAsignacion.execute();

        logger.info("Funcion ejecutada:{checklist.PAADMASIGNACION.SPREASIGNA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        respuesta.put("respuestaString", out.get("PA_RESPUESTA"));

        if (ejecucion == 0) {
            respuesta.put("respuestaBoolean", false);
            logger.info("Algo ocurrió al ejecutar la asignacion");
        } else {
            respuesta.put("respuestaBoolean", true);
        }

        logger.info(respuesta);

        return respuesta;
    }

}
