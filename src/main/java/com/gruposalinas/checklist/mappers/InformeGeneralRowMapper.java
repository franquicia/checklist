package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.InformeGeneralDTO;

public class InformeGeneralRowMapper implements RowMapper<InformeGeneralDTO>   {

	@Override
	public InformeGeneralDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		InformeGeneralDTO informeGeneralDTO = new InformeGeneralDTO();
		
                informeGeneralDTO.setIdCeco(rs.getString("FCID_CECO"));
                informeGeneralDTO.setFecha(rs.getString("FDFECHA"));
		informeGeneralDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
                informeGeneralDTO.setRutaInforme(rs.getString("FCRUTAINFORME"));
		informeGeneralDTO.setIdEstatus(rs.getInt("FISTATUS"));
                informeGeneralDTO.setNumeroBitacora(rs.getInt("FINUMBITACORA"));
                
                
		return informeGeneralDTO;
	}

}
