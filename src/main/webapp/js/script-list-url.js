	$(document).ready(function() {
		var tt = document.getElementById('tableURL');
		tt.style.display = 'none';
	});

	function checkExecute() {
		var loading = $( "#loadingDialog" ).dialog({
			autoOpen: false,
			modal: true,
			dialogClass: "no-close",
		    hide: {
		    	effect: "blind",
		    	duration: 1000
		    }
		});

		var textArea = document.getElementById("urls").value;
		
		if (textArea == null | textArea == '') {
			alert("Debe insertar las URLs antes de continuar!");
		}
		else {
			var ce = confirm("¿Está seguro que quiere realizar la validación de la lista de URLs?");

			if (ce == true) {
				loading.dialog( "open" );

				$('#table').bootstrapTable('removeAll');
				
				var t = document.getElementById('tableURL');
				t.style.display = 'none';
				
				if (t.style.display == 'none') {
					t.style.display = 'block';
				}
				else if (t.style.display != 'none') {
					t.style.display = 'none';
				}

				var listURL = $('#urls').val();
				var arrayURL = listURL.split("\n");

				var errorCheckbox = document.getElementById("checkbox");

				var keepSaving = true;

				if (errorCheckbox.checked) {
					$.each(arrayURL, function(index, value) {
						if (keepSaving) {
							$.ajax({
								url : value,
								async : false,
								type : 'GET',
							    success : function(data, statusText, xhr) {
									insertRow(xhr, value);
								},
								error : function(xhr) {
									if (xhr.status > 400) {
										keepSaving = false;
									}
								}
							});
				    	}
				    	else {
				    		return false;
				    	}
					});

					if (!keepSaving) {
						alert("Ocurrio un error al realizar el análisis de la lista de URL y se detuvo el proceso!");
					}
				}
				else {
					$.each(arrayURL, function(index, value) {
						$.ajax({
							url : value,
							async : false,
							type : 'GET',
						    success : function(data, statusText, xhr) {
						    	insertRow(xhr, value);
						    },
							error : function(xhr) {
								insertRow(xhr, value);
							}
						});
					});
				}
			}
			else {
				return false;
			}
			
			$('#loadingDialog').dialog("close");

		}
	}

	function insertRow(xhr, value) {
		$('#table').bootstrapTable('insertRow', {
            index: 0,
            row: {
            	cUrl: value,
            	cStatus: xhr.status + " " + xhr.statusText,
            	cContentType: xhr.getResponseHeader("Content-Type"),
            	cResponseBody: xhr.responseText
            }
        });
	}

    function rowStyle(row, index) {
		var classes = ['active', 'success', 'info', 'warning', 'danger'];

		var cStatus = row.cStatus;

		if (cStatus.substring(0,3) == '200') {
			return {
				classes: 'success'
			};
		}
		else if (cStatus.substring(0,3) == '404') {
			return {
				classes: 'danger'
			};
		}
		else {
			return {
				classes: 'warning'
			};
		}
    }
