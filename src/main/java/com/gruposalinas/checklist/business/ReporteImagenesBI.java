package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ReporteImagenesDAO;
import com.gruposalinas.checklist.domain.ConteoxPreguntaDTO;
import com.gruposalinas.checklist.domain.HorasRespuestaDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.NumeroTiendasDTO;
import com.gruposalinas.checklist.domain.ReporteImagenesDTO;
import com.gruposalinas.checklist.domain.ReportesConteoDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class ReporteImagenesBI {
	
	private Logger logger = LogManager.getLogger(ReporteImagenesBI.class);
	
	@Autowired
	ReporteImagenesDAO reporteImagenesDAO;
	
	
	public Map<String, Object> obtieneModulosTiendas(String ceco, String checklist) {
		
		Map<String, Object> respuesta = new  HashMap<String, Object>();
		
		try {
			respuesta = reporteImagenesDAO.obtieneModulosTiendas(ceco, checklist);
		} catch (Exception e) {
			logger.info("No fue posible obtener las tiendas y/o modulos");
				
		}
		
		return respuesta;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> obtieneRespuestas(int idCeco, int idChecklist, String fecha){
	//public List<ReporteImagenesDTO> obtieneRespuestas(int idCeco, int idChecklist){	
		
		List<ReporteImagenesDTO> res = new ArrayList<ReporteImagenesDTO>();
		List<ReporteImagenesDTO> res2 = new ArrayList<ReporteImagenesDTO>();
		
		List<Object> modulosPreguntas = new ArrayList<Object>();
		int contHijos =0;
		try {
	
			res = reporteImagenesDAO.obtieneRespuestas(idCeco, idChecklist, fecha);
			
			for(int i=0 ; i < res.size() ; i++){
				
				ReporteImagenesDTO respuesta1 = res.get(i);
				ReporteImagenesDTO aux = null;
				for(int j = i+1;j<res.size();j++){
					
					ReporteImagenesDTO respuesta2 = res.get(j);
					
					if(respuesta1.getIdPregunta() == respuesta2.getIdPreguntaPadre()){
						
						aux = new ReporteImagenesDTO();
						aux.setIdPregunta(respuesta1.getIdPregunta());
						aux.setIdModulo(respuesta1.getIdModulo());
						aux.setImagenPrincipal(respuesta1.getImagenPrincipal());
						aux.setImagenSecundario(respuesta2.getImagenPrincipal());
						aux.setImagenReporte(respuesta2.getImagenReporte());
						aux.setTextoAbiertaImagen(respuesta2.getTextoAbiertaImagen());
						aux.setObservaciones(respuesta2.getObservaciones());
						res2.add(aux);
						
						contHijos ++;
					}

				}
				
				if (contHijos == 0 && respuesta1.getIdPreguntaPadre()==0){
					aux = new  ReporteImagenesDTO();
					aux.setIdPregunta(respuesta1.getIdPregunta());
					aux.setIdModulo(respuesta1.getIdModulo());
					aux.setImagenPrincipal(respuesta1.getImagenPrincipal());
					res2.add(aux);
				}
					
				contHijos = 0;
			}
			
			
			List<ModuloDTO> modulos = null;
			modulos = (List<ModuloDTO>) reporteImagenesDAO.obtieneModulosTiendas("", String.valueOf(idChecklist)).get("modulos");
			List<ReporteImagenesDTO> listaImagenes = null;
			
			
			for(int i=0; i<modulos.size() ; i++){
				listaImagenes = new ArrayList<ReporteImagenesDTO>();
				

				if(modulos.get(i).getIdModulo() == 0 ){					
					String [] datos = reporteImagenesDAO.primerRespuesta(idCeco, idChecklist, fecha);
		
					if(datos[0] != null && datos[1] != null){
					
						ReporteImagenesDTO primera = new  ReporteImagenesDTO();
						primera.setIdModulo(0);
						primera.setImagenPrincipal(datos[0]);
						primera.setTextoAbiertaImagen(datos[1]);
						
						listaImagenes.add(primera);
					}
	
				}
				
				for(int j = 0; j < res2.size(); j++){
										
					if(modulos.get(i).getIdModulo() == res2.get(j).getIdModulo() ){
						if(modulos.get(i).getIdModulo() == 96){
							
							String [] datos = reporteImagenesDAO.primerRespuesta(idCeco, idChecklist, fecha);
							
							if(!res2.get(i).getImagenPrincipal().contains("circulo_rojo.png")){
								if(datos[0] != null && datos[1] != null){								
									res2.get(i).setImagenPrincipal(datos[0]);																								
								}	
							}
							res2.get(i).setTextoAbiertaImagen(datos[1]);
							
						}
						listaImagenes.add(res2.get(j));
					}
					
				}
				
				modulosPreguntas.add(listaImagenes);
				
								
			}
			
		} catch (Exception e) {
			logger.info("No fue posible obtener las respuestas para el checklist "+idChecklist +" y/o ceco "+idCeco);
				
		}
		
		
		return modulosPreguntas;
		
		
	}
	
	public List<NumeroTiendasDTO> obtieneNumeroTiendas(String ceco, int tipo) {
		List<NumeroTiendasDTO> response = null;
		try {
			response = reporteImagenesDAO.obtieneNumTiendas(ceco, tipo);
		} catch (Exception e) {
			logger.info("No fue posible obtener el numero de tiendas");
		}
		return response;
	}
	
	@SuppressWarnings({ "unchecked", "unused", "rawtypes" })
	public String obtieneRespuestasRegion(String ceco, int idCheck, String fecha){

		
		String response = null;
		JsonObject principal = new JsonObject();

		try {
			Map data = reporteImagenesDAO.obtieneListasDatos(idCheck, ceco, fecha);
			List<ConteoxPreguntaDTO> conteo = (List<ConteoxPreguntaDTO>) data.get("conteo");
			List<NumeroTiendasDTO> tiendas = (List<NumeroTiendasDTO>) data.get("tiendas");
			List<HorasRespuestaDTO> horas = (List<HorasRespuestaDTO>) data.get("horas");
			List<ReportesConteoDTO> reportes = (List<ReportesConteoDTO>) data.get("reportes");
			
			
			
			int totalSucursalesRegion = tiendas.get(0).getNumTiendas();
			
			/*INICIA LOGICA ARMADO RESPUESTA*/
			JsonArray array = new JsonArray();
			JsonObject hijo = null;
			
			int totalHoras = horas.size();
			int totalHoraCorrecta = 0;
			int totalHoraIncorrecta = 0;
			int totalTiendas = 0;
			String imagen = null;
			
			Iterator<HorasRespuestaDTO> it = horas.iterator();
			
			while (it.hasNext()){
				HorasRespuestaDTO o = it.next();
				if ( Integer.parseInt(o.getHora()) < 830 )
					totalHoraCorrecta++;
				else
					totalHoraIncorrecta++;
			}
			
			double porcentajeHoras = 0;
			if (totalHoras > 0){
				porcentajeHoras = (totalHoraIncorrecta * 100) / totalHoras;
				    if (totalHoraCorrecta == totalSucursalesRegion)
				    	imagen = "palomita";
				    else if ( porcentajeHoras > 0 && porcentajeHoras < 20 )
				    	imagen = "amarillo";
				    else if ( porcentajeHoras > 19 )
				    	imagen = "rojo";
				    else
				    	imagen = "verde";
			} 
			
		    hijo = new JsonObject();
		    hijo.addProperty("totalTiendas", totalHoras);
		    hijo.addProperty("imagen", imagen);
		    
		    array.add(hijo);
			
			int i = 0;
			int cont = 0;
			while(cont < conteo.size() ){
			    totalTiendas = 0;
			    int totalSi = 0;
				int totalNo = 0;
				boolean bandera = false;
				ConteoxPreguntaDTO ant = conteo.get(cont);
				
				
				
			    while( cont < conteo.size() && ant.getIdPregunta() == conteo.get(cont).getIdPregunta() ){
					totalTiendas += conteo.get(cont).getTiendas();
					if (conteo.get(cont).getRespuesta() == 1)
						totalSi =  conteo.get(cont).getTiendas();
					else
						totalNo =  conteo.get(cont).getTiendas();

					cont++;
					bandera = true;
				}
			    
			    if (!bandera){
			    	totalTiendas = ant.getTiendas();
			    	if (conteo.get(cont).getRespuesta() == 1)
						totalSi =  conteo.get(cont).getTiendas();
					else
						totalNo =  conteo.get(cont).getTiendas();
			    }
			    double porcentajeTiendas = (totalNo * 100) / totalTiendas;
			    if (totalSi == totalSucursalesRegion)
			    	imagen = "palomita";
			    else if ( porcentajeTiendas > 0 && porcentajeTiendas < 20 )
			    	imagen = "amarillo";
			    else if ( porcentajeTiendas > 19 )
			    	imagen = "rojo";
			    else
			    	imagen = "verde";
			    
			    hijo = new JsonObject();
			    hijo.addProperty("totalTiendas", totalTiendas);
			    hijo.addProperty("imagen", imagen);
			    
			    array.add(hijo);
			    
			}
			
			for (int j = array.size() ; j < 8 ; j++)
				array.add(new JsonObject());
			
			principal.add("modulo", array);
			
			JsonArray rep =  new JsonArray();
			JsonObject jsonOb = null;
			
			Iterator<ReportesConteoDTO> itr = reportes.iterator();
			
			while(itr.hasNext()){
				jsonOb = new JsonObject();
				ReportesConteoDTO o = (ReportesConteoDTO) itr.next();
				jsonOb.addProperty("conteo",o.getConteo());
				jsonOb.addProperty("idModulo",o.getIdModulo());
				rep.add(jsonOb);
			}
			principal.add("reportes", rep);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return principal.toString();
	}

	public List<ConteoxPreguntaDTO> obtieneConteoxModulo(int checklist, String ceco, String fecha){
		List<ConteoxPreguntaDTO> lista = new ArrayList<ConteoxPreguntaDTO>();
		
		try {
			lista = reporteImagenesDAO.obtieneConteoxModulo(checklist, ceco, fecha);
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		return lista;
		
	}
	
	public List<NumeroTiendasDTO> obtieneTotalTiendasRegion(int checklist, String ceco, String fecha){
		List<NumeroTiendasDTO> lista = new ArrayList<NumeroTiendasDTO>();
		
		try {
			lista = reporteImagenesDAO.obtieneTotalTiendasRegion(checklist, ceco, fecha);
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		return lista;
	}
	
	public List<HorasRespuestaDTO> obtieneConteoxHora(int checklist, String ceco, String fecha){
		List<HorasRespuestaDTO> lista = new ArrayList<HorasRespuestaDTO>();
		
		try {
			lista = reporteImagenesDAO.obtieneConteoxHora(checklist, ceco, fecha);
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		return lista;
		
	}
	
	public List<ReportesConteoDTO> obtieneReportesConteo(int checklist, String ceco, String fecha){
		List<ReportesConteoDTO> lista = new ArrayList<ReportesConteoDTO>();
		
		try {
			lista = reporteImagenesDAO.obtieneReportesConteo(checklist, ceco, fecha);
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		return lista;
		
	}
	
}
