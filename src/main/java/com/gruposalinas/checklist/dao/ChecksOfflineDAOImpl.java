package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.BitacoraMovilDTO;
import com.gruposalinas.checklist.domain.CheckUsuaMovilDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.domain.ChecklistUsuarioDTO;
import com.gruposalinas.checklist.domain.HorarioDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.PlantillaDTO;
import com.gruposalinas.checklist.domain.PreguntaZonaDTO;
import com.gruposalinas.checklist.domain.TipoArchivoDTO;
import com.gruposalinas.checklist.domain.TipoChecklistDTO;
import com.gruposalinas.checklist.domain.TipoPreguntaDTO;
import com.gruposalinas.checklist.domain.Usuario_ADTO;
import com.gruposalinas.checklist.mappers.ArbolDecisionRowMapper;
import com.gruposalinas.checklist.mappers.ArbolDecisionRowMapper3;
import com.gruposalinas.checklist.mappers.BitacoraOfflineRowMapper;
import com.gruposalinas.checklist.mappers.CheckPregOfflineRowMapper;
import com.gruposalinas.checklist.mappers.CheckPregOfflineRowMapper3;
import com.gruposalinas.checklist.mappers.CheckUOfflineRowMapper;
import com.gruposalinas.checklist.mappers.CheckUsuaOffRowMapper;
import com.gruposalinas.checklist.mappers.ChecklistOfflineRowMapper;
import com.gruposalinas.checklist.mappers.ChecklistOfflineRowMapper3;
import com.gruposalinas.checklist.mappers.ChecksOfflineCompletosRowMapper;
import com.gruposalinas.checklist.mappers.ChecksOfflineRowMapper;
import com.gruposalinas.checklist.mappers.ElementoPlantillaRowMapper;
import com.gruposalinas.checklist.mappers.ElementoPlantillaRowMapper3;
import com.gruposalinas.checklist.mappers.HorarioRowMapper;
import com.gruposalinas.checklist.mappers.HorarioRowMapper3;
import com.gruposalinas.checklist.mappers.ModuloOfflineRowMapper;
import com.gruposalinas.checklist.mappers.ModuloOfflineRowMapper3;
import com.gruposalinas.checklist.mappers.PlatillaRowMapper;
import com.gruposalinas.checklist.mappers.PlatillaRowMapper3;
import com.gruposalinas.checklist.mappers.PreguntaZonaMapper;
import com.gruposalinas.checklist.mappers.TipoArchivoRowMapper;
import com.gruposalinas.checklist.mappers.TipoArchivoRowMapper3;
import com.gruposalinas.checklist.mappers.TipoChecklistRowMapper;
import com.gruposalinas.checklist.mappers.TipoChecklistRowMapper3;
import com.gruposalinas.checklist.mappers.TipoPreguntaRowMapper;
import com.gruposalinas.checklist.mappers.TipoPreguntaRowMapper3;
import com.gruposalinas.checklist.mappers.UsuarioSucOfflineRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ChecksOfflineDAOImpl extends DefaultDAO implements ChecksOfflineDAO {

    private static Logger logger = LogManager.getLogger(ChecklistDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneChecks;
    private DefaultJdbcCall jdbcObtieneChecksNuevo;
    private DefaultJdbcCall jdbcObtieneChecksNuevo3;
    private DefaultJdbcCall jdbcObtieneChecksNuevo4;
    private DefaultJdbcCall jdbcObtieneChecksBitacora;

    public void init() {

        jdbcObtieneChecks = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECKOFFLINE")
                .withProcedureName("SPOBTIENECHECKS")
                .returningResultSet("RCL_CHECKS", new ChecksOfflineRowMapper()).
                returningResultSet("RCL_COMPLETOS", new ChecksOfflineCompletosRowMapper())
                .returningResultSet("RCL_USUARIO_SUC", new UsuarioSucOfflineRowMapper());

        jdbcObtieneChecksNuevo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAOFFLINE_NV")
                .withProcedureName("SPOBTIENECHECKS_N")
                .returningResultSet("RCL_CHECK", new ChecklistOfflineRowMapper())
                .returningResultSet("RCL_TIPOCHECK", new TipoChecklistRowMapper())
                .returningResultSet("RCL_HORARIOS", new HorarioRowMapper())
                .returningResultSet("RCL_CHECK_USUA", new CheckUsuaOffRowMapper())
                .returningResultSet("RCL_MODULOS", new ModuloOfflineRowMapper())
                .returningResultSet("RCL_TIPO_PREG", new TipoPreguntaRowMapper())
                .returningResultSet("RCL_CHECK_PREG", new CheckPregOfflineRowMapper())
                .returningResultSet("RCL_ARBOL", new ArbolDecisionRowMapper())
                .returningResultSet("RCL_PLANTILLA", new PlatillaRowMapper())
                .returningResultSet("RCL_ELEM_P", new ElementoPlantillaRowMapper())
                .returningResultSet("RCL_BITACORA", new BitacoraOfflineRowMapper())
                .returningResultSet("RCL_USUARIO_SUC", new UsuarioSucOfflineRowMapper())
                .returningResultSet("RCL_TIPO_AR", new TipoArchivoRowMapper())
                .returningResultSet("RCL_CHECK_IN", new CheckUOfflineRowMapper());
        jdbcObtieneChecksNuevo3 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAOFFLINEV3")
                .withProcedureName("SPOBTIENECHECKS_N")
                .returningResultSet("RCL_CHECK", new ChecklistOfflineRowMapper3())
                .returningResultSet("RCL_TIPOCHECK", new TipoChecklistRowMapper3())
                .returningResultSet("RCL_HORARIOS", new HorarioRowMapper3())
                //.returningResultSet("RCL_CHECK_USUA", new  CheckUsuaOffRowMapper())
                .returningResultSet("RCL_MODULOS", new ModuloOfflineRowMapper3())
                .returningResultSet("RCL_TIPO_PREG", new TipoPreguntaRowMapper3())
                .returningResultSet("RCL_CHECK_PREG", new CheckPregOfflineRowMapper3())
                .returningResultSet("RCL_PLANTILLA", new PlatillaRowMapper3())
                .returningResultSet("RCL_ELEM_P", new ElementoPlantillaRowMapper3())
                .returningResultSet("RCL_ARBOL", new ArbolDecisionRowMapper3())
                //.returningResultSet("RCL_BITACORA", new  BitacoraOfflineRowMapper())
                //.returningResultSet("RCL_USUARIO_SUC", new  UsuarioSucOfflineRowMapper())
                .returningResultSet("RCL_TIPO_AR", new TipoArchivoRowMapper3())
                .returningResultSet("RCL_PREGZONA", new PreguntaZonaMapper());
        //.returningResultSet("RCL_CHECK_IN", new  CheckUOfflineRowMapper());

        jdbcObtieneChecksNuevo4 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAOFFLINE4")
                .withProcedureName("SPOBTIENECHECKS_N")
                .returningResultSet("RCL_CHECK", new ChecklistOfflineRowMapper())
                .returningResultSet("RCL_TIPOCHECK", new TipoChecklistRowMapper())
                .returningResultSet("RCL_HORARIOS", new HorarioRowMapper())
                .returningResultSet("RCL_CHECK_USUA", new CheckUsuaOffRowMapper())
                .returningResultSet("RCL_MODULOS", new ModuloOfflineRowMapper())
                .returningResultSet("RCL_TIPO_PREG", new TipoPreguntaRowMapper())
                .returningResultSet("RCL_CHECK_PREG", new CheckPregOfflineRowMapper())
                .returningResultSet("RCL_ARBOL", new ArbolDecisionRowMapper())
                .returningResultSet("RCL_PLANTILLA", new PlatillaRowMapper())
                .returningResultSet("RCL_ELEM_P", new ElementoPlantillaRowMapper())
                .returningResultSet("RCL_BITACORA", new BitacoraOfflineRowMapper())
                .returningResultSet("RCL_USUARIO_SUC", new UsuarioSucOfflineRowMapper())
                .returningResultSet("RCL_TIPO_AR", new TipoArchivoRowMapper())
                .returningResultSet("RCL_CHECK_IN", new CheckUOfflineRowMapper());

        jdbcObtieneChecksBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAOFFLINEV3")
                .withProcedureName("SPGETBITACORAS")
                .returningResultSet("RCL_BITACORA", new BitacoraOfflineRowMapper());

    }

    @Override
    public Map<String, Object> obtieneChecks(int idUsuario) throws Exception {

        Map<String, Object> out = null;

        Map<String, Object> listas = null;

        int ejecuccion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", idUsuario);

        out = jdbcObtieneChecks.execute(in);

        logger.info("Funcion ejecutada: {checklist.PACHECKOFFLINE.SPOBTIENECHECKS}");

        BigDecimal valueReturn = (BigDecimal) out.get("PA_EJECUCCION");
        ejecuccion = valueReturn.intValue();

        if (ejecuccion == 0) {
            logger.info("Algo ocurrió al obtener los checklist para modo Offline ");
        } else {
            listas = new HashMap<String, Object>();

            listas.put("checklists", out.get("RCL_CHECKS"));
            listas.put("checklistsCompletos", out.get("RCL_COMPLETOS"));
            listas.put("listaUsuarios", out.get("RCL_USUARIO_SUC"));
        }

        return listas;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> obtieneChecksNuevo(int idUsuario) throws Exception {

        Map<String, Object> out = null;

        Map<String, Object> listas = null;
        List<ChecklistDTO> listaChecklist = null;
        List<TipoChecklistDTO> ListaTipoCheck = null;
        List<HorarioDTO> ListaHorario = null;
        List<ChecklistUsuarioDTO> ListaCheckUsuarios = null;
        List<ModuloDTO> listaModulos = null;
        List<TipoPreguntaDTO> listaTipoPregunta = null;
        List<ChecklistPreguntaDTO> listaCheckPregunta = null;
        List<ArbolDecisionDTO> listaArbol = null;
        List<BitacoraDTO> listaBitacora = null;
        List<Usuario_ADTO> listaUsuarios = null;
        List<TipoArchivoDTO> listaTipoArchivo = null;
        List<ChecklistUsuarioDTO> listaCheckUsuaInactivos = null;
        List<PlantillaDTO> listaPlantilla = null;
        List<PlantillaDTO> listaElementoP = null;

        int ejecuccion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", idUsuario);

        logger.info("");
        out = jdbcObtieneChecksNuevo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAOFFLINE_NV.SPOBTIENECHECKS_N}");

        listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");
        ListaTipoCheck = (List<TipoChecklistDTO>) out.get("RCL_TIPOCHECK");
        ListaHorario = (List<HorarioDTO>) out.get("RCL_HORARIOS");
        ListaCheckUsuarios = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_USUA");
        listaModulos = (List<ModuloDTO>) out.get("RCL_MODULOS");
        listaTipoPregunta = (List<TipoPreguntaDTO>) out.get("RCL_TIPO_PREG");
        listaCheckPregunta = (List<ChecklistPreguntaDTO>) out.get("RCL_CHECK_PREG");
        listaArbol = (List<ArbolDecisionDTO>) out.get("RCL_ARBOL");
        listaBitacora = (List<BitacoraDTO>) out.get("RCL_BITACORA");
        listaUsuarios = (List<Usuario_ADTO>) out.get("RCL_USUARIO_SUC");
        listaTipoArchivo = (List<TipoArchivoDTO>) out.get("RCL_TIPO_AR");
        listaCheckUsuaInactivos = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_IN");
        listaPlantilla = (List<PlantillaDTO>) out.get("RCL_PLANTILLA");
        listaElementoP = (List<PlantillaDTO>) out.get("RCL_ELEM_P");

        BigDecimal valueReturn = (BigDecimal) out.get("PA_EJECUCCION");
        ejecuccion = valueReturn.intValue();

        if (ejecuccion == 0) {
            logger.info("Algo ocurrió al obtener los checklist para modo Offline ");
        } else {
            listas = new HashMap<String, Object>();

            listas.put("listaChecklist", listaChecklist);
            listas.put("ListaTipoCheck", ListaTipoCheck);
            listas.put("ListaHorario", ListaHorario);
            listas.put("ListaCheckUsuarios", ListaCheckUsuarios);
            listas.put("listaModulos", listaModulos);
            listas.put("listaTipoPregunta", listaTipoPregunta);
            listas.put("listaCheckPregunta", listaCheckPregunta);
            listas.put("listaArbol", listaArbol);
            listas.put("listaBitacora", listaBitacora);
            listas.put("listaUsuarios", listaUsuarios);
            listas.put("listaTipoArchivo", listaTipoArchivo);
            listas.put("listaCheckUsuaInactivos", listaCheckUsuaInactivos);
            listas.put("listaPlantilla", listaPlantilla);
            listas.put("listaElementoP", listaElementoP);
        }

        return listas;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> obtieneChecksNuevo3(int idUsuario, int idChecklist) throws Exception {

        Map<String, Object> out = null;

        Map<String, Object> listas = null;
        List<ChecklistDTO> listaChecklist = null;
        List<TipoChecklistDTO> ListaTipoCheck = null;
        List<HorarioDTO> ListaHorario = null;
        List<ChecklistUsuarioDTO> ListaCheckUsuarios = null;
        List<ModuloDTO> listaModulos = null;
        List<TipoPreguntaDTO> listaTipoPregunta = null;
        List<ChecklistPreguntaDTO> listaCheckPregunta = null;
        List<ArbolDecisionDTO> listaArbol = null;
        List<BitacoraDTO> listaBitacora = null;
        List<Usuario_ADTO> listaUsuarios = null;
        List<TipoArchivoDTO> listaTipoArchivo = null;
        List<ChecklistUsuarioDTO> listaCheckUsuaInactivos = null;
        List<PreguntaZonaDTO> listaPreguntaZona = null;
        List<PlantillaDTO> listaPlantilla = null;
        List<PlantillaDTO> listaElementoP = null;

        int ejecuccion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", idUsuario)
                .addValue("PA_IDCHECKLIST", idChecklist);

        logger.info("");
        out = jdbcObtieneChecksNuevo3.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAOFFLINE_NV.SPOBTIENECHECKS_N}");

        listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");
        ListaTipoCheck = (List<TipoChecklistDTO>) out.get("RCL_TIPOCHECK");
        ListaHorario = (List<HorarioDTO>) out.get("RCL_HORARIOS");
        //ListaCheckUsuarios = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_USUA");
        listaModulos = (List<ModuloDTO>) out.get("RCL_MODULOS");
        listaTipoPregunta = (List<TipoPreguntaDTO>) out.get("RCL_TIPO_PREG");
        listaCheckPregunta = (List<ChecklistPreguntaDTO>) out.get("RCL_CHECK_PREG");
        listaArbol = (List<ArbolDecisionDTO>) out.get("RCL_ARBOL");
        //listaBitacora = (List<BitacoraDTO>) out.get("RCL_BITACORA");
        //listaUsuarios = (List<Usuario_ADTO>) out.get("RCL_USUARIO_SUC");
        listaTipoArchivo = (List<TipoArchivoDTO>) out.get("RCL_TIPO_AR");
        //listaCheckUsuaInactivos = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_IN");
        listaPlantilla = (List<PlantillaDTO>) out.get("RCL_PLANTILLA");
        listaElementoP = (List<PlantillaDTO>) out.get("RCL_ELEM_P");
        listaPreguntaZona = (List<PreguntaZonaDTO>) out.get("RCL_PREGZONA");
        //BigDecimal valueReturn = (BigDecimal) out.get("PA_EJECUCCION");
        //ejecuccion = valueReturn.intValue();
        ejecuccion = 1;

        if (ejecuccion == 0) {
            logger.info("Algo ocurrió al obtener los checklist para modo Offline ");
        } else {
            listas = new HashMap<String, Object>();
            ArrayList<BitacoraMovilDTO> listaNuevaBit = new ArrayList<BitacoraMovilDTO>();
            ArrayList<CheckUsuaMovilDTO> listaNuevaCheckUsua = new ArrayList<CheckUsuaMovilDTO>();

            if (listaBitacora != null) {
                for (BitacoraDTO item : listaBitacora) {
                    BitacoraMovilDTO bit = new BitacoraMovilDTO();
                    bit.setIdBitacora(item.getIdBitacora());
                    bit.setIdCheckUsua(item.getIdCheckUsua());
                    bit.setFechaInicio(item.getFechaInicio());
                    listaNuevaBit.add(bit);

                }
            }
            if (ListaCheckUsuarios != null) {
                for (ChecklistUsuarioDTO item : ListaCheckUsuarios) {
                    CheckUsuaMovilDTO chk = new CheckUsuaMovilDTO();
                    chk.setIdCheckUsuario(item.getIdCheckUsuario());
                    chk.setIdChecklist(item.getIdChecklist());
                    chk.setIdUsuario(item.getIdUsuario());
                    chk.setFechaIni(item.getFechaIni());
                    chk.setIdCeco(item.getIdCeco());
                    chk.setNombreCeco(item.getNombreCeco());
                    chk.setNombreCheck(item.getNombreCheck());
                    chk.setLatitud(item.getLatitud());
                    chk.setLongitud(item.getLongitud());
                    chk.setZona(item.getZona());
                    listaNuevaCheckUsua.add(chk);

                }

            }

            listas.put("listaChecklist", listaChecklist);
            listas.put("ListaTipoCheck", ListaTipoCheck);
            listas.put("ListaHorario", ListaHorario);
            //listas.put("ListaCheckUsuarios",listaNuevaCheckUsua);
            listas.put("listaModulos", listaModulos);
            listas.put("listaTipoPregunta", listaTipoPregunta);
            listas.put("listaCheckPregunta", listaCheckPregunta);
            listas.put("listaArbol", listaArbol);
            //listas.put("listaBitacora",listaNuevaBit);
            //listas.put("listaUsuarios",listaUsuarios);
            listas.put("listaTipoArchivo", listaTipoArchivo);
            //listas.put("listaCheckUsuaInactivos",listaCheckUsuaInactivos);
            listas.put("listaPlantilla", listaPlantilla);
            listas.put("listaElementoP", listaElementoP);
            listas.put("listaPreguntaZona", listaPreguntaZona);
        }

        return listas;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> getBitacoras(int idUsuario, int idChecklist) throws Exception {

        Map<String, Object> out = null;

        Map<String, Object> listas = null;
        List<ChecklistDTO> listaChecklist = null;
        List<TipoChecklistDTO> ListaTipoCheck = null;
        List<HorarioDTO> ListaHorario = null;
        List<ChecklistUsuarioDTO> ListaCheckUsuarios = null;
        List<ModuloDTO> listaModulos = null;
        List<TipoPreguntaDTO> listaTipoPregunta = null;
        List<ChecklistPreguntaDTO> listaCheckPregunta = null;
        List<ArbolDecisionDTO> listaArbol = null;
        List<BitacoraDTO> listaBitacora = null;
        List<Usuario_ADTO> listaUsuarios = null;
        List<TipoArchivoDTO> listaTipoArchivo = null;
        List<ChecklistUsuarioDTO> listaCheckUsuaInactivos = null;
        List<PlantillaDTO> listaPlantilla = null;
        List<PlantillaDTO> listaElementoP = null;

        int ejecuccion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", idUsuario)
                .addValue("PA_IDCHECKLIST", idChecklist);

        logger.info("");
        out = jdbcObtieneChecksBitacora.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAOFFLINE_NV.SPOBTIENECHECKS_N}");

        //listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");
        //ListaTipoCheck = (List<TipoChecklistDTO>) out.get("RCL_TIPOCHECK");
        //ListaHorario = (List<HorarioDTO>) out.get("RCL_HORARIOS");
        //ListaCheckUsuarios = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_USUA");
        //listaModulos = (List<ModuloDTO>) out.get("RCL_MODULOS");
        //listaTipoPregunta = (List<TipoPreguntaDTO>) out.get("RCL_TIPO_PREG");
        //listaCheckPregunta = (List<ChecklistPreguntaDTO>) out.get("RCL_CHECK_PREG");
        //listaArbol = (List<ArbolDecisionDTO>) out.get("RCL_ARBOL");
        listaBitacora = (List<BitacoraDTO>) out.get("RCL_BITACORA");
        //listaUsuarios = (List<Usuario_ADTO>) out.get("RCL_USUARIO_SUC");
        //listaTipoArchivo = (List<TipoArchivoDTO>) out.get("RCL_TIPO_AR");
        //listaCheckUsuaInactivos = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_IN");
        //listaPlantilla = (List<PlantillaDTO>) out.get("RCL_PLANTILLA");
        //listaElementoP = (List<PlantillaDTO>) out.get("RCL_ELEM_P");

        //BigDecimal valueReturn = (BigDecimal) out.get("PA_EJECUCCION");
        //ejecuccion = valueReturn.intValue();
        listas = new HashMap<String, Object>();
        ArrayList<BitacoraMovilDTO> listaNuevaBit = new ArrayList<BitacoraMovilDTO>();

        if (listaBitacora != null) {
            for (BitacoraDTO item : listaBitacora) {
                BitacoraMovilDTO bit = new BitacoraMovilDTO();
                bit.setIdBitacora(item.getIdBitacora());
                bit.setIdCheckUsua(item.getIdCheckUsua());
                bit.setFechaInicio(item.getFechaInicio());
                listaNuevaBit.add(bit);

            }
        }

        //listas.put("listaChecklist",listaChecklist);
        //listas.put("ListaTipoCheck",ListaTipoCheck);
        //listas.put("ListaHorario",ListaHorario);
        //listas.put("ListaCheckUsuarios",listaNuevaCheckUsua);
        //listas.put("listaModulos",listaModulos);
        //listas.put("listaTipoPregunta",listaTipoPregunta);
        //listas.put("listaCheckPregunta",listaCheckPregunta);
        //listas.put("listaArbol",listaArbol);
        listas.put("listaBitacora", listaNuevaBit);
        //listas.put("listaUsuarios",listaUsuarios);
        //listas.put("listaTipoArchivo",listaTipoArchivo);
        //listas.put("listaCheckUsuaInactivos",listaCheckUsuaInactivos);
        //listas.put("listaPlantilla",listaPlantilla);
        //listas.put("listaElementoP",listaElementoP);

        return listas;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> obtieneChecksNuevo4(int idUsuario) throws Exception {

        Map<String, Object> out = null;

        Map<String, Object> listas = null;
        List<ChecklistDTO> listaChecklist = null;
        List<TipoChecklistDTO> ListaTipoCheck = null;
        List<HorarioDTO> ListaHorario = null;
        List<ChecklistUsuarioDTO> ListaCheckUsuarios = null;
        List<ModuloDTO> listaModulos = null;
        List<TipoPreguntaDTO> listaTipoPregunta = null;
        List<ChecklistPreguntaDTO> listaCheckPregunta = null;
        List<ArbolDecisionDTO> listaArbol = null;
        List<BitacoraDTO> listaBitacora = null;
        List<Usuario_ADTO> listaUsuarios = null;
        List<TipoArchivoDTO> listaTipoArchivo = null;
        List<ChecklistUsuarioDTO> listaCheckUsuaInactivos = null;
        List<PlantillaDTO> listaPlantilla = null;
        List<PlantillaDTO> listaElementoP = null;

        int ejecuccion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", idUsuario);

        logger.info("");
        out = jdbcObtieneChecksNuevo4.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAOFFLINE_NV.SPOBTIENECHECKS_N}");

        listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");
        ListaTipoCheck = (List<TipoChecklistDTO>) out.get("RCL_TIPOCHECK");
        ListaHorario = (List<HorarioDTO>) out.get("RCL_HORARIOS");
        ListaCheckUsuarios = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_USUA");
        listaModulos = (List<ModuloDTO>) out.get("RCL_MODULOS");
        listaTipoPregunta = (List<TipoPreguntaDTO>) out.get("RCL_TIPO_PREG");
        listaCheckPregunta = (List<ChecklistPreguntaDTO>) out.get("RCL_CHECK_PREG");
        listaArbol = (List<ArbolDecisionDTO>) out.get("RCL_ARBOL");
        listaBitacora = (List<BitacoraDTO>) out.get("RCL_BITACORA");
        listaUsuarios = (List<Usuario_ADTO>) out.get("RCL_USUARIO_SUC");
        listaTipoArchivo = (List<TipoArchivoDTO>) out.get("RCL_TIPO_AR");
        listaCheckUsuaInactivos = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_IN");
        listaPlantilla = (List<PlantillaDTO>) out.get("RCL_PLANTILLA");
        listaElementoP = (List<PlantillaDTO>) out.get("RCL_ELEM_P");

        BigDecimal valueReturn = (BigDecimal) out.get("PA_EJECUCCION");
        ejecuccion = valueReturn.intValue();

        if (ejecuccion == 0) {
            logger.info("Algo ocurrió al obtener los checklist para modo Offline ");
        } else {
            listas = new HashMap<String, Object>();

            listas.put("listaChecklist", listaChecklist);
            listas.put("ListaTipoCheck", ListaTipoCheck);
            listas.put("ListaHorario", ListaHorario);
            listas.put("ListaCheckUsuarios", ListaCheckUsuarios);
            listas.put("listaModulos", listaModulos);
            listas.put("listaTipoPregunta", listaTipoPregunta);
            listas.put("listaCheckPregunta", listaCheckPregunta);
            listas.put("listaArbol", listaArbol);
            listas.put("listaBitacora", listaBitacora);
            listas.put("listaUsuarios", listaUsuarios);
            listas.put("listaTipoArchivo", listaTipoArchivo);
            listas.put("listaCheckUsuaInactivos", listaCheckUsuaInactivos);
            listas.put("listaPlantilla", listaPlantilla);
            listas.put("listaElementoP", listaElementoP);
        }

        return listas;
    }

}
