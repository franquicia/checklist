package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.DatosInformeDTO;
import com.gruposalinas.checklist.mappers.DatosInformeRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class DatosInformeDAOImpl extends DefaultDAO implements DatosInformeDAO {

    private static Logger logger = LogManager.getLogger(DatosInformeDAOImpl.class);

    DefaultJdbcCall jdbcInsertaDatosInforme;
    DefaultJdbcCall jdbcEliminaDatosInforme;
    DefaultJdbcCall jdbcObtieneTodoDatosInfo;
    DefaultJdbcCall jbdcActualizaFirma;

    public void init() {

        jdbcInsertaDatosInforme = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_DATOSINFO")
                .withProcedureName("SP_INS_DATINF");

        jdbcEliminaDatosInforme = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_DATOSINFO")
                .withProcedureName("SP_DEL_DATINF");

        jdbcObtieneTodoDatosInfo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_DATOSINFO")
                .withProcedureName("SP_SEL_DATINF")
                .returningResultSet("RCL_INFORM", new DatosInformeRowMapper());

        jbdcActualizaFirma = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_DATOSINFO")
                .withProcedureName("SP_ACT_DATINF");

    }

    public int insertaDatosInforme(DatosInformeDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getIdCeco())
                .addValue("PA_FECHA", bean.getFecha())
                .addValue("PA_USU", bean.getIdUsuario())
                .addValue("PA_PROTOCOLO", bean.getIdProtocolo())
                //.addValue("PA_DATO", bean.getResponsable())
                .addValue("PA_IMPERD", bean.getImperdonables())
                .addValue("PA_TOTRESP", bean.getTotalRespuestas())
                .addValue("PA_CAL", bean.getCalificacion());

        out = jdbcInsertaDatosInforme.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PA_DATOSINFO.SP_INS_DATOS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_DATO");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió La DatosInforme ");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean eliminaDatosInforme(String idInforme) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_DATO", idInforme);

        out = jdbcEliminaDatosInforme.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PA_DATOSINFO.SP_DEL_DATOS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar informe(" + idInforme + ")");
        } else {
            return true;
        }

        return false;

    }

    @SuppressWarnings("unchecked")
    public List<DatosInformeDTO> obtieneDatosDatosInforme(String ceco, String idInforme, String idUsuario) throws Exception {
        Map<String, Object> out = null;
        int error = 1;
        List<DatosInformeDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_IDFORM", idInforme)
                .addValue("PA_USU", idUsuario);

        out = jdbcObtieneTodoDatosInfo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PA_DATOSINFO.SP_SEL_FIRMACE}");
        //lleva el nombre del cursor del procedure
        lista = (List<DatosInformeDTO>) out.get("RCL_INFORM");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las firmas del(" + ceco + ")");
        }
        return lista;

    }

    public boolean actualizaDatosInforme(DatosInformeDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getIdCeco())
                .addValue("PA_FECHA", bean.getFecha())
                .addValue("PA_USU", bean.getIdUsuario())
                .addValue("PA_PROTOCOLO", bean.getIdProtocolo())
                .addValue("PA_DATO", bean.getIdInforme())
                .addValue("PA_IMPERD", bean.getImperdonables())
                .addValue("PA_TOTRESP", bean.getTotalRespuestas())
                .addValue("PA_CAL", bean.getCalificacion());

        out = jbdcActualizaFirma.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PA_DATOSINFO.SP_ACT_FIRMA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado de la DatosInforme con  id( " + bean.getIdInforme() + ")");
        } else {
            return true;
        }
        return false;

    }

}
