<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>Asignaci�n de Ceco Masiva</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supPI2/estilos.css">
</head>

<body ng-app="app" ng-controller="validarForm">

  <div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="${pageContext.request.contextPath}/img/supervisionPI2/btn_hamburguer.svg" alt="Men� principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="inicio.htm"><img src="${pageContext.request.contextPath}/img/supervisionPI2/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.html" class="selMenu">
          <div>Salir</div>
          <div><img src="${pageContext.request.contextPath}/img/supervisionPI2/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="#">Administrador</a> 
		</div>
    	Asignaci�n de Ceco Masiva
    </div>

  <!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervisionPI2/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Men� Principal<br>
          </div>
        </div>
        <%-- <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la p�gina">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> --%>
        <div id="menu">
          <ul class="l-navegacion nivel1">
				<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
				<li>
					<a href="#"><div>Administrador</div></a>
					<ul class="nivel2" style="height: 350px; overflow-y: auto;">
			            <li><a href="asignaciones.htm"><div>Asignaci�n de Perfiles</div></a></li>
			            <li><a href="asignaPersonal.htm"><div>Asignaci�n de Personal</div></a></li>
			            <li><a href="asignaCecos.htm"><div>Asignaci�n de Cecos</div></a></li>
			            <!-- <li><a href="asignaCecosMasiva.htm"><div>Asignaci�n de Cecos Masiva</div></a></li> -->
		          	</ul>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Asegurador</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>B&uacute;squeda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>B&uacute;squeda Asegurador</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galer&iacute;a de Evidencias</div></a>
				</li>
			</ul>
        </div>
      </div>
    </div>

  <!-- Contenido -->
  <div class="contSecc">
  
  	<div class="liga">
			<a href="#" onclick="javascript:window.history.back()" class="liga">
			<img style="height: 15px; width: 15px;" src="${pageContext.request.contextPath}/img/supervision/arrowmleft.png">
			<span>P&Aacute;GINA ANTERIOR</span>
			</a>
	</div><br><br>
  
	<div class="titSec">Selecciona el archivo que desea subir para ser procesado.</div>
	
    <div class="gris">
      <div class="w70">
      	<c:url value="/central/asignaCecosMasiva.htm" var="masivo" />
		<form:form method="POST" action="${masivo}" model="command" name="formMasivo" id="formMasivo" enctype="multipart/form-data">
		        <div class="divCol2">
			          <div class="col2">
			            Buscar:<br>
			              <input type="file" id="uploadedFile" name="uploadedFile" class="inputfile carga">
			              <label for="uploadedFile" class="btnAgregaFoto "><span>Adjuntar documento </span></label>
			          </div>
		          <div class="col2">
		            <br>
		            <a href="#" onclick="return archivo()" class="btn btnA">Agregar Archivo</a>
		          </div>
        		</div>
         </form:form>
         <br>
         	<strong><p class="help-block">Tama�o M�ximo 100MB.</p></strong>
  
<c:choose>
    	<c:when test="${exito==0}">
    		<script type="text/javascript">
			    	window.onload = function(){
		    				$('#popUp').text('No se ha podido realizar la asignaci�n, favor de validar el archivo.');
							$('#modalFinalizado').modal();
							return false;
			    	};
		    </script>
    	</c:when>
    	<c:when test="${exito==1}">
	        <script type="text/javascript">
			    	window.onload = function(){
			    			$('#popUp').text('Se realizo la asignaci�n correctamente.');
							$('#modalFinalizado').modal();
							return false;
			    	};
		    </script>
    	</c:when>
    	<c:when test="${exito==2}">
    		<script type="text/javascript">
			    	window.onload = function(){
				    		$('#popUp').text('Solo se asignaron los protocolos.');
							$('#modalFinalizado').modal();
							return false;
			    	};
		    </script>
    	</c:when>   
    	<c:when test="${archivo==size}">
    		<script type="text/javascript">
			    	window.onload = function(){
				    		$('#popUp').text('El archivo pesa m�s de 100MB.');
							$('#modalFinalizado').modal();
							return false;
			    	};
		    </script>
    	</c:when>
    	<c:when test="${archivo==reintentar}">
    		<script type="text/javascript">
			    	window.onload = function(){
				    		$('#popUp').text('Existe un error con el archivo, favor de reintentar.');
							$('#modalFinalizado').modal();
							return false;
			    	};
		    </script>
    	</c:when>
    	
</c:choose>

      </div>
    </div>

	<!-- Footer -->
	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualizaci�n Noviembre 2019</div>
	</div>
</div><!-- Fin page -->

</body>
</html>

<!--Modal-->
<div id="modalFinalizado" class="modal">
  <div class="cuadro cuadroM">
    <a href="#" class="simplemodal-close btnCerrar"><img src="${pageContext.request.contextPath}/img/supervisionPI2/icoCerrar.svg"></a>
    <div class="titModal">Asignaci�n Masiva.</div><br>

    <div class="w94Modal">
      <div class="clear"></div>
      <div class="tCenter">
      	<p id="popUp">texto</p>
      </div>
    </div>
    <br>
  </div>
  <div class="clear"></div><br>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/supPI2/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supPI2/custom-file-input.js"></script><!--Unpload -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supPI2/funciones.js"></script>

<script type="text/javascript">

function archivo(){	
	var archivo = document.getElementById("uploadedFile").files[0];
	var arch = document.getElementById("uploadedFile").files[0].name;
		if (!arch.includes(".xlsx") || !arch.includes(".xls")) {
			alert("El archivo no es Excel, debe contener la extensi�n .xlsx o .xls")
		} else {
			if (archivo) {
				if (archivo.size > (1024 * 1024 * 100)) {
					alert("El archivo pesa m�s de 100MB!");
				} else {
					form = document.getElementById("formMasivo");
					form.submit();
				}
			} else {
				alert("Archivo no encontrado, favor de seleccionar un archivo.");
			}
		}
	}
</script>
