package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.CecoIndicadoresDTO;


public class CecoIndicadoresRowMapper implements RowMapper<CecoIndicadoresDTO>{

	public CecoIndicadoresDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
	    
		CecoIndicadoresDTO cecoIndDTO = new CecoIndicadoresDTO();
		
		cecoIndDTO.setIdCeco(rs.getString("FCID_CECO"));
		cecoIndDTO.setNombreCeco(rs.getString("FCNOMBRE"));
		cecoIndDTO.setNegocio(rs.getInt("FIID_NEGOCIO"));
		cecoIndDTO.setNivel(rs.getString("NIVEL"));
		
		return cecoIndDTO;
}

}
