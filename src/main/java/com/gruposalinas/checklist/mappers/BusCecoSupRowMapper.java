package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.Admin2SupDTO;
import com.gruposalinas.checklist.domain.AsignacionesSupDTO;

public class BusCecoSupRowMapper implements RowMapper<Admin2SupDTO> {

	public Admin2SupDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		Admin2SupDTO asignaDTO = new Admin2SupDTO();
		
		asignaDTO.setSucursal(rs.getString("Sucursal"));
		asignaDTO.setCeco(rs.getString("Ceco"));
		asignaDTO.setZona(rs.getString("Zona"));
		
		return asignaDTO;
		
		
	}

}
