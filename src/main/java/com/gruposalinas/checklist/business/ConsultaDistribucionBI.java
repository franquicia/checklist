package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ConsultaDistribucionDAO;
import com.gruposalinas.checklist.domain.ConsultaDistribucionDTO;

public class ConsultaDistribucionBI {
		
	private static Logger logger = LogManager.getLogger(DetalleProtoBI.class);
	
	@Autowired
	ConsultaDistribucionDAO consultaDistribucionDAO;
	
	List<ConsultaDistribucionDTO> listaConsulta = null;

	public List<ConsultaDistribucionDTO> ObtieneTerritorio(){
	
		try {
			listaConsulta = consultaDistribucionDAO.ObtieneTerritorio();
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
			logger.info(""+e);
		}			
		
		return listaConsulta;
	
	}
	
	public List<ConsultaDistribucionDTO> ObtieneNivelZona(ConsultaDistribucionDTO bean){
		
		try {
			listaConsulta = consultaDistribucionDAO.ObtieneNivelZona(bean);
		}
			catch(Exception e) {
				logger.info("No fue posible consultar el StoreProcedure");
				logger.info(""+e);
			}
			return listaConsulta;
		}
	
	
	public List<ConsultaDistribucionDTO> ObtieneNivelRegion(ConsultaDistribucionDTO bean){
		
		try {
			listaConsulta = consultaDistribucionDAO.ObtieneNivelRegion(bean);
		}
			catch(Exception e) {
				logger.info("No fue posible consultar el StoreProcedure");
				logger.info(""+e);
			}
			return listaConsulta;
		}
	
public List<ConsultaDistribucionDTO> ObtieneNivelGerente(ConsultaDistribucionDTO bean){
		
		try {
			listaConsulta = consultaDistribucionDAO.ObtieneNivelGerente(bean);
		}
			catch(Exception e) {
				logger.info("No fue posible consultar el StoreProcedure");
				logger.info(""+e);
			}
			return listaConsulta;
		}

public List<ConsultaDistribucionDTO> ObtieneNivelSucursal(ConsultaDistribucionDTO bean){
	
	try {
		listaConsulta = consultaDistribucionDAO.ObtieneNivelSucursal(bean);
	}
		catch(Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
			logger.info(""+e);
		}
		return listaConsulta;
	}
	
public List<ConsultaDistribucionDTO> ObtieneEstructura(ConsultaDistribucionDTO bean){
	
	try {
		listaConsulta = consultaDistribucionDAO.ObtieneEstructura(bean);
	}
		catch(Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
			logger.info(""+e);
		}
		return listaConsulta;
	}
	

	}

