package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.GeografiaDTO;


public class GeografiaRowMapper implements RowMapper<GeografiaDTO> {

	public GeografiaDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		GeografiaDTO geoDTO = new GeografiaDTO();
		
		geoDTO.setIdCeco(rs.getString("FCID_CECO"));
		geoDTO.setIdRegion(rs.getString("FCID_REGION"));
		geoDTO.setRegion(rs.getString("FCREGION"));
		geoDTO.setIdZona(rs.getString("FCID_ZONA"));
		geoDTO.setZona(rs.getString("FCZONA"));
		geoDTO.setIdTerritorio(rs.getString("FCID_TERRITORIO"));
		geoDTO.setTerritorio(rs.getString("FCTERRITORIO"));
		
		
		return geoDTO;
	}

}
