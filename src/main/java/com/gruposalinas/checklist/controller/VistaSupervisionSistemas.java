package com.gruposalinas.checklist.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.AsignacionCheckBI;
import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.business.IncidentesBI;
import com.gruposalinas.checklist.business.ReporteSupervisionSistemasBI;
import com.gruposalinas.checklist.domain.AsignacionCheckDTO;
import com.gruposalinas.checklist.domain.ReporteSistemasDTO;
import com.gruposalinas.checklist.domain.ReporteSupervisionSistemasDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.servicios.servidor.CorreosLotus;

@Controller
public class VistaSupervisionSistemas {
	private static Logger logger = LogManager.getLogger(VistaSupervisionSistemas.class);
	
	@Autowired
	ReporteSupervisionSistemasBI reporteSupervisionBI;
	
	@Autowired
	CecoBI cecoBI;
	
	@Autowired
	AsignacionCheckBI asignacionCheckBI;
	
	@Autowired
	IncidentesBI incidenteBi;
	
	int opcReporte = 0;
	int opcCeco [] = {230034,264000,230001};
	int tipoSis [] = {0,1,2};
	
	
	// ******************************** INICIA LA VISTA REPORTE SISTEMAS ******************************** //
	

	//10.51.210.239:8080/checklist/central/vistaSupervisionSistemasImagen.htm
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"central/vistaSupervisionSistemas.htm"}, method = RequestMethod.GET)
	public ModelAndView vistaSupervisionSistemas(HttpServletRequest request, HttpServletResponse response) throws Exception{
		logger.info("...ENTRO AL CONTROLLER vistaSupervisionSistemas...");
		
		this.opcReporte = 0;
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaSupervisionSistemas.htm");
		}else{
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
			if(idUsuario>0){
				mv = new ModelAndView("vistaSistemas");
				mv.addObject("idUsuario", idUsuario);
				mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
				Map<String, Object> map = new HashMap<String, Object>();
				
				List<ReporteSistemasDTO> listareporteTotales = new ArrayList<ReporteSistemasDTO>();
				List<ReporteSupervisionSistemasDTO> listareporteSistemas = new ArrayList<ReporteSupervisionSistemasDTO>();
				List<ReporteSupervisionSistemasDTO> listareporteSistemasAux = new ArrayList<ReporteSupervisionSistemasDTO>();
	
				try {
					logger.info("Ceco... " + this.opcCeco[0]);	
					map = reporteSupervisionBI.reporteSistemas(this.opcCeco[0]);
					logger.info("Map... " + map);						
					
					if(map != null) {
						listareporteTotales = (List<ReporteSistemasDTO>) map.get("listaReporteTotales");
						listareporteSistemasAux  = (List<ReporteSupervisionSistemasDTO>) map.get("listaReporteSistemas");
						
						listareporteSistemas = filtra(listareporteSistemasAux);
						
						logger.info("Size listareporteTotales:"+listareporteTotales);
						logger.info("Size listareporteTotales:"+listareporteTotales.size());
						mv.addObject("listareporteTotales", listareporteTotales);
						
						logger.info("Size listareporteSistemas:"+listareporteSistemas);
						logger.info("Size listareporteSistemas:"+listareporteSistemas.size());
						mv.addObject("listareporteSistemas", listareporteSistemas);
						
						if(listareporteTotales.size()==0 || listareporteSistemas.size()==0)
							mv = null;
					}
								
				} catch (Exception e) {
					logger.info("Ocurrio algo... "+e);
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaSupervisionSistemas...");
				mv = new ModelAndView("http404");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaSupervisionSistemas...");
		return mv;
		
	}
		

	//10.51.210.239:8080/checklist/central/vistaSupervisionSistemasV2.htm
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"central/vistaSupervisionSistemasV2.htm"}, method = RequestMethod.GET)
	public ModelAndView vistaSupervisionSistemasV2(HttpServletRequest request, HttpServletResponse response) throws Exception{
		logger.info("...ENTRO AL CONTROLLER vistaSupervisionSistemasv2...");
		
		this.opcReporte = 0;
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaSupervisionSistemas.htm");
		}else{
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
			if(idUsuario>0){
				mv = new ModelAndView("vistaSistemasV2");
				mv.addObject("idUsuario", idUsuario);
				mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
				Map<String, Object> map = new HashMap<String, Object>();
				
				List<ReporteSupervisionSistemasDTO> listaReporteSistemasLAM = new ArrayList<ReporteSupervisionSistemasDTO>();
				List<ReporteSupervisionSistemasDTO> listaReporteSistemas = new ArrayList<ReporteSupervisionSistemasDTO>();
				List<ReporteSupervisionSistemasDTO> listaReporteSistemasAux = new ArrayList<ReporteSupervisionSistemasDTO>();
	
				try {
					logger.info("Ceco... " + this.opcCeco[0]);	
					map = reporteSupervisionBI.reporteSistemas2(this.opcCeco[0]);
					logger.info("Map... " + map);						
					
					if(map != null) {
						
						Calendar now = Calendar.getInstance();
						String[] meses = new String[]{"Enero","Febebro","Marzo","Abril",	"Mayo","Junio",	"Julio",	"Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
						mv.addObject("fecha", meses[(now.get(Calendar.MONTH))]);
						
						listaReporteSistemasLAM = (List<ReporteSupervisionSistemasDTO>) map.get("listaReporteLAM");
						listaReporteSistemasAux  = (List<ReporteSupervisionSistemasDTO>) map.get("listaReporteSistemas");
						
						listaReporteSistemas = filtra(listaReporteSistemasAux);
						
						logger.info("Size listaReporteSistemasLAM:"+listaReporteSistemasLAM);
						logger.info("Size listaReporteSistemasLAM:"+listaReporteSistemasLAM.size());
						mv.addObject("listaReporteSistemasLAM", listaReporteSistemasLAM);
						
						logger.info("Size listaReporteSistemas:"+listaReporteSistemas);
						logger.info("Size listaReporteSistemas:"+listaReporteSistemas.size());
						mv.addObject("listaReporteSistemas", listaReporteSistemas);
						
						int totalMeta = 0;
						int totalVisitas = 0;
						int totalRecursos = 0;
						for (int i = 0; i < listaReporteSistemas.size(); i++) {
							ReporteSupervisionSistemasDTO obj = listaReporteSistemas.get(i);
							totalMeta += obj.getAsignados();
							totalVisitas += obj.getTerminados();
							totalRecursos += obj.getRecursos();
						}
						
						logger.info("totalMeta: " + totalMeta);						
						mv.addObject("totalMeta", totalMeta);
						logger.info("totalVisitas: " + totalVisitas);
						mv.addObject("totalVisitas", totalVisitas);
						logger.info("totalRecursos: " + totalRecursos);
						mv.addObject("totalRecursos", totalRecursos);
						
						if(listaReporteSistemasLAM.size()==0 || listaReporteSistemas.size()==0)
							mv = null;
					}
								
				} catch (Exception e) {
					logger.info("Ocurrio algo... "+e);
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaSupervisionSistemasV2...");
				mv = new ModelAndView("http404");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaSupervisionSistemasV2...");
		return mv;
		
	}
		
	// ******************************** FINALIZA LA VISTA REPORTE SISTEMAS ******************************** //
	
	public List<ReporteSupervisionSistemasDTO> filtra (List<ReporteSupervisionSistemasDTO> lista) {
		
		List<ReporteSupervisionSistemasDTO> listaReporteSistemas = null;
		if(lista != null) {
			listaReporteSistemas = lista;
			for(int i =0; i<listaReporteSistemas.size(); i++) {
				if(listaReporteSistemas.get(i).getNombreCeco().contains("SISTEMAS")) {
					listaReporteSistemas.get(i).setNombreCeco(listaReporteSistemas.get(i).getNombreCeco().replace("SISTEMAS ", ""));
				}
			}
		}
		return listaReporteSistemas;
	}
	
	// ******************************** INICIA LA VISTA REPORTE SISTEMAS SOPORTE ******************************** //
	
	
	//10.51.210.239:8080/checklist/central/vistaSupervisionSistemas.htm
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"central/vistaSupervisionSistemasSoporte.htm"}, method = RequestMethod.GET)
	public ModelAndView vistaSupervisionSistemasSoporte(HttpServletRequest request, HttpServletResponse response) throws Exception{
		logger.info("...ENTRO AL CONTROLLER vistaSupervisionSistemasSoporte...");
		
		this.opcReporte = 1;
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaSupervisionSistemas.htm");
		}else{
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
			if(idUsuario>0){
				mv = new ModelAndView("vistaSoporte");
				mv.addObject("idUsuario", idUsuario);
				mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
				Map<String, Object> map = new HashMap<String, Object>();
				
				List<ReporteSistemasDTO> listareporteTotales = new ArrayList<ReporteSistemasDTO>();
				List<ReporteSupervisionSistemasDTO> listareporteSistemas = new ArrayList<ReporteSupervisionSistemasDTO>();
	
				try {
					logger.info("Ceco... " + this.opcCeco[1]);	
					map = reporteSupervisionBI.reporteSistemas(this.opcCeco[1]);
					logger.info("Map... " + map);						
					
					if(map != null) {
						listareporteTotales = (List<ReporteSistemasDTO>) map.get("listaReporteTotales");
						listareporteSistemas  = (List<ReporteSupervisionSistemasDTO>) map.get("listaReporteSistemas");
						
						logger.info("Size listareporteTotales:"+listareporteTotales.size());
						mv.addObject("listareporteTotales", listareporteTotales);
						
						logger.info("Size listareporteSistemas:"+listareporteSistemas.size());
						mv.addObject("listareporteSistemas", listareporteSistemas);
						
						if(listareporteTotales.size()==0 || listareporteSistemas.size()==0)
							mv = null;
					}
								
				} catch (Exception e) {
					logger.info("Ocurrio algo... "+e);
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaSupervisionSistemasSoporte...");
				mv = new ModelAndView("http404");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaSupervisionSistemasSoporte...");
		return mv;
	}
	

	// ******************************** FINALIZA LA VISTA REPORTE SISTEMAS SOPORTE ******************************** //
	
	
	
	
	// ******************************** INICIA LA VISTA REPORTE SISTEMAS IMAGEN ******************************** //
	
	
	//10.51.210.239:8080/checklist/central/vistaSupervisionSistemasImagen.htm
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"central/vistaSupervisionSistemasImagen.htm"}, method = RequestMethod.GET)
	public ModelAndView vistaSupervisionSistemasImagen(HttpServletRequest request, HttpServletResponse response) throws Exception{
		logger.info("...ENTRO AL CONTROLLER vistaSupervisionSistemasImagen...");
		
		this.opcReporte = 2;
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaSupervisionSistemas.htm");
		}else{
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
			if(idUsuario>0){
				mv = new ModelAndView("vistaSistemas");
				mv.addObject("idUsuario", idUsuario);
				mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
				Map<String, Object> map = new HashMap<String, Object>();
				
				List<ReporteSistemasDTO> listareporteTotales = new ArrayList<ReporteSistemasDTO>();
				List<ReporteSupervisionSistemasDTO> listareporteSistemas = new ArrayList<ReporteSupervisionSistemasDTO>();
	
				try {
					logger.info("Ceco... " + this.opcCeco[2]);	
					map = reporteSupervisionBI.reporteSistemas(this.opcCeco[2]);
					logger.info("Map... " + map);						
					
					if(map != null) {
						listareporteTotales = (List<ReporteSistemasDTO>) map.get("listaReporteTotales");
						listareporteSistemas  = (List<ReporteSupervisionSistemasDTO>) map.get("listaReporteSistemas");
						
						logger.info("Size listareporteTotales:"+listareporteTotales.size());
						mv.addObject("listareporteTotales", listareporteTotales);
						
						logger.info("Size listareporteSistemas:"+listareporteSistemas.size());
						mv.addObject("listareporteSistemas", listareporteSistemas);

						if(listareporteTotales.size()==0 || listareporteSistemas.size()==0)
							mv = null;
					}
								
				} catch (Exception e) {
					logger.info("Ocurrio algo... "+e);
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaSupervisionSistemasImagen...");
				mv = new ModelAndView("http404");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaSupervisionSistemasImagen...");
		return mv;
		
	}
	
	
	
	// ******************************** FINALIZA LA VISTA REPORTE SISTEMAS IMAGEN ******************************** //
	
	
	
	
	
	
	
	
	
	// ******************************** INICIA LA VISTA PROGRAMA VISITAS ******************************** //
	

	//10.51.210.239:8080/checklist/central/vistaProgramaVisitas.htm
	@RequestMapping(value = {"central/vistaProgramaVisitas.htm"}, method = RequestMethod.GET)
	public ModelAndView vistaProgramaVisitas(HttpServletRequest request, HttpServletResponse response) throws Exception{
		logger.info("...ENTRO AL CONTROLLER vistaProgramaVisitas...");
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaProgramaVisitas.htm");
		}else{
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
			if(idUsuario>0){
				mv = new ModelAndView("vistaProgramaVisitas");
				mv.addObject("idUsuario", idUsuario);
				mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());	
			}else{
				logger.info("...SALE DEL CONTROLLER vistaProgramaVisitas...");
				mv = new ModelAndView("http404");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaProgramaVisitas...");
		return mv;
		
	}
	
	
	// ******************************** FINALIZA LA VISTA REPORTE SISTEMAS ******************************** //
	
	
	
	
	
	
	// ******************************** INICIA LA VISTA PROGRAMA VISITAS ******************************** //
	

	//10.51.210.239:8080/checklist/central/vistaReporteIncidencias.htm
	@RequestMapping(value = {"central/vistaReporteIncidencias.htm"}, method = RequestMethod.GET)
	public ModelAndView reporteCaptacion(HttpServletRequest request, HttpServletResponse response) throws Exception{
		logger.info("...ENTRO AL CONTROLLER vistaReporteCaptacion...");

		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaReporteIncidencias.htm");
		}else{
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
			if(idUsuario>0){
				mv = new ModelAndView("vistaReporteCaptacion");
				mv.addObject("idUsuario", idUsuario);
				mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
			}else{
				logger.info("...SALE DEL CONTROLLER vistaReporteCaptacion...");
				mv = new ModelAndView("http404");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaReporteCaptacion...");
		return mv;
		
	}
	
	
	// ******************************** FINALIZA LA VISTA REPORTE SISTEMAS ******************************** //
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	@RequestMapping(value = {"central/vistaFiltroSupervision.htm"}, method = RequestMethod.GET)
	public ModelAndView vistaFiltroSupervision(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "000000") String idCeco,
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "General") String nombreCeco)throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaFiltroSupervision...");
		ModelAndView mv = null;
		int idUsuario=0;
		
		String area = "";
		String tipo = "";
		if(idCeco.contains("000000")) {
			area=""+this.opcCeco[this.opcReporte];
			tipo = "" + this.tipoSis[this.opcReporte];
		}else {
			area=idCeco;
			tipo ="" + this.tipoSis[this.opcReporte];
		}
		
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaSupervisionSistemas.htm");
		}else{
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
		
			if(idUsuario>0){
								
				logger.info("EL ID CECO DE ESTA PETICION ES "+ area);
				mv = new ModelAndView("vistaFiltroSistemas");
				
				mv.addObject("area", area);
				mv.addObject("tipo", tipo);
				mv.addObject("nombreCeco", nombreCeco);
				mv.addObject("idUsuario", idUsuario);
				mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
				
				//FILTRO SUCURSALES
				List<ReporteSupervisionSistemasDTO> listaFiltros = new ArrayList<ReporteSupervisionSistemasDTO>();
				listaFiltros = reporteSupervisionBI.filtroCecos(Integer.parseInt(tipo));
				mv.addObject("listaFiltros", listaFiltros);
				
				logger.info("listaFiltros : " + listaFiltros);
				
				//REPORTES CICAPS
				logger.info("tipo : " + tipo);
				logger.info("area : " + area);
				List<ReporteSupervisionSistemasDTO> listaReportesSicap = new ArrayList<ReporteSupervisionSistemasDTO>();
				listaReportesSicap = reporteSupervisionBI.reporteSisCap("", "", tipo, "",area);
				
				mv.addObject("listaReportesSicap", listaReportesSicap);
				logger.info("listaFiltrosSucursales : " + listaReportesSicap);
				logger.info("listaFiltrosSucursales : " + listaReportesSicap.size());

				/*
				//FILTRO SUCURSALES
				List<ReporteSupervisionSistemasDTO> listaFiltrosSucursales = new ArrayList<ReporteSupervisionSistemasDTO>();
				listaFiltrosSucursales = reporteSupervisionBI.filtroSucursal("");
				mv.addObject("listaFiltrosSucursales", listaFiltrosSucursales);
				
				logger.info("listaFiltrosSucursales : " + listaFiltrosSucursales.size());
				logger.info("listaFiltrosSucursales : " + listaFiltrosSucursales.toString());
				*/
				
			}else{
				logger.info("...SALE DEL CONTROLLER vistaFiltroSupervision...");
				mv = new ModelAndView("http404");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaFiltroSupervision...");
		return mv;
	}
	
	
	@RequestMapping(value = {"central/vistaFiltroSoporte.htm"}, method = RequestMethod.GET)
	public ModelAndView vistaFiltroSoporte(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "000000") String idCeco,
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "General") String nombreCeco)throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaFiltroSoporte...");
		ModelAndView mv = null;
		int idUsuario=0;
		
		String area = "";
		String tipo = "";
		if(idCeco.contains("000000")) {
			area=""+this.opcCeco[this.opcReporte];
			tipo = "" + this.tipoSis[this.opcReporte];
		}else {
			area=idCeco;
			tipo ="" + this.tipoSis[this.opcReporte];
		}
		
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaSupervisionSistemasSoporte.htm");
		}else{
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
		
			if(idUsuario>0){
								
				logger.info("EL ID CECO DE ESTA PETICION ES "+ area);
				mv = new ModelAndView("vistaFiltroSoporte");
				
				mv.addObject("area", area);
				mv.addObject("tipo", tipo);
				mv.addObject("nombreCeco", nombreCeco);
				mv.addObject("idUsuario", idUsuario);
				mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
				
				//FILTRO SUCURSALES
				List<ReporteSupervisionSistemasDTO> listaFiltros = new ArrayList<ReporteSupervisionSistemasDTO>();
				listaFiltros = reporteSupervisionBI.filtroCecos(Integer.parseInt(tipo));
				mv.addObject("listaFiltros", listaFiltros);
				
				
				//REPORTES CICAPS
				List<ReporteSupervisionSistemasDTO> listaReportesSicap = new ArrayList<ReporteSupervisionSistemasDTO>();
				listaReportesSicap = reporteSupervisionBI.reporteSisCap("", "", tipo, "",area);
				mv.addObject("listaReportesSicap", listaReportesSicap);
				
				logger.info("listaFiltrosSucursales : " + listaReportesSicap.size());
				
				/*logger.info("listaFiltrosSucursales : " + listaReportesSicap.toString());

				
				//FILTRO SUCURSALES
				List<ReporteSupervisionSistemasDTO> listaFiltrosSucursales = new ArrayList<ReporteSupervisionSistemasDTO>();
				listaFiltrosSucursales = reporteSupervisionBI.filtroSucursal("");
				mv.addObject("listaFiltrosSucursales", listaFiltrosSucursales);
				
				logger.info("listaFiltrosSucursales : " + listaFiltrosSucursales.size());
				logger.info("listaFiltrosSucursales : " + listaFiltrosSucursales.toString());*/
				
			}else{
				logger.info("...SALE DEL CONTROLLER vistaFiltroSoporte...");
				mv = new ModelAndView("http404");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaFiltroSoporte...");
		return mv;
	}
	
	
	
	
	
	@RequestMapping(value = "/ajaxSupervisionSisCap", method = RequestMethod.GET)
	public @ResponseBody List<ReporteSupervisionSistemasDTO> ajaxFiltroSucursal(
			@RequestParam(value = "fInicio", required = false) String fInicio,
			@RequestParam(value = "fFin", required = false) String fFin,
			@RequestParam(value = "checklist", required = true, defaultValue = "") String checklist,
			@RequestParam(value = "sucursal", required = true, defaultValue = "") String sucursal,
			@RequestParam(value = "area", required = true, defaultValue = "0") String area, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxSupervisionSisCap...");
		
		area = ""+this.opcCeco[this.opcReporte];
		
		logger.info("fInicio... "+fInicio);
		logger.info("fFin... "+fFin);
		logger.info("checklist... "+checklist);
		logger.info("sucursal... "+sucursal);
		if(sucursal.length() > 4){
			sucursal = sucursal.substring(0, 4);
			logger.info("valor recortado... "+sucursal);
		}
		
		logger.info("area... "+area);
		
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());
		logger.info("USUARIO "+ idUsuario);
		
		
		List<ReporteSupervisionSistemasDTO> listRetorno= new ArrayList<ReporteSupervisionSistemasDTO>();
		listRetorno = reporteSupervisionBI.reporteSisCap(fInicio, fFin, checklist, sucursal, area);
		
		logger.info("listRetorno.size() "+listRetorno.size());
		logger.info("listRetorno "+listRetorno);

		logger.info("...SALE DEL AJAX ajaxSupervisionSisCap...");
		return listRetorno;
	}
	
	//Busqueda de usuarios. Eliminar la busqueda de sucursales de la anterior
	@RequestMapping(value = "/ajaxAutocompletaUsuario", method = RequestMethod.GET)
	public @ResponseBody String[] ajaxAutocompletaUsuario(
			@RequestParam(value = "term", required = true, defaultValue = "") String term, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxAutocompletaUsuario...");
		
		logger.info("term... "+term);
		
		List<ReporteSupervisionSistemasDTO> listaFiltrosUsuarios = new ArrayList<ReporteSupervisionSistemasDTO>();
		listaFiltrosUsuarios = reporteSupervisionBI.filtroUsuario(term);
		logger.info("listRetorno.size() "+listaFiltrosUsuarios.size());
		logger.info("listRetorno "+listaFiltrosUsuarios);
		
		
		String[] usuarios = new String[listaFiltrosUsuarios.size()];
		for(int i =0; i<listaFiltrosUsuarios.size(); i++){
			usuarios[i] = listaFiltrosUsuarios.get(i).getUsuario();
			//logger.info("ceco Usuario:"+ listaFiltrosUsuarios.get(i).getIdCecos());
			//logger.info("Nombre Usuario:"+usuarios[i]);
		}
	
		//logger.info("usuario.length "+usuarios.length);
		//logger.info("usuarios "+usuarios);

		logger.info("...SALE DEL AJAX ajaxAutocompletaUsuario...");
		return usuarios;
	}
			
	//Busqueda de sucursales. Eliminar la busqueda de sucursales de la anterior
	@RequestMapping(value = "/ajaxAutocompletaSucursal", method = RequestMethod.GET)
	public @ResponseBody String[] ajaxAutocompletaSucursal(
			@RequestParam(value = "term", required = true, defaultValue = "") String term, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxAutocompletaSucursal...");
		
		logger.info("term... "+term);
		
		List<ReporteSupervisionSistemasDTO> listaFiltrosSucursales = new ArrayList<ReporteSupervisionSistemasDTO>();
		listaFiltrosSucursales = reporteSupervisionBI.filtroSucursal(term);
		logger.info("listRetorno.size() "+listaFiltrosSucursales.size());
		logger.info("listRetorno "+listaFiltrosSucursales);
		
		//Vaciarlo en otra clase que tenga la siguiente forma
//		clase con un atributo que se llame label que sea de tipo String[] y dentro de ella debe
		//guardar todo lo que retorne el 
	
		
		String[] sucursales = new String[listaFiltrosSucursales.size()];
		for(int i =0; i<listaFiltrosSucursales.size(); i++){
			sucursales[i] = listaFiltrosSucursales.get(i).getSucursal();
			logger.info("Ceco Sucursal:"+ listaFiltrosSucursales.get(i).getIdCecos());
			logger.info("Nombre Sucursal:"+sucursales[i]);
		}
	
		//logger.info("sucursales.length "+sucursales.length);
		//logger.info("sucursales "+sucursales);
		

		logger.info("...SALE DEL AJAX ajaxAutocompletaSucursal...");
		return sucursales;
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/ajaxSupervisionEvidencia", method = RequestMethod.GET)
	public @ResponseBody List<List> ajaxEvidencia(
			@RequestParam(value = "idBitacora", required = true, defaultValue = "0") int idBitacora, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxSupervisionEvidencia...");
		
		logger.info("fInicio... "+idBitacora);
		
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario=Integer.parseInt(userSession.getIdUsuario());
		logger.info("USUARIO "+ idUsuario);
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		List<ReporteSupervisionSistemasDTO> listaPreguntas = new ArrayList<ReporteSupervisionSistemasDTO>();
		List<ReporteSupervisionSistemasDTO> listaEvaluacionFechas = new ArrayList<ReporteSupervisionSistemasDTO>();
		try{
			map = reporteSupervisionBI.obtenerRespuestas(idBitacora);
			logger.info("Tamaño map de respuestas y evidencia: "+map.size());
			if (map != null) {
				listaPreguntas = (List<ReporteSupervisionSistemasDTO>) map.get("listaRespuestas");
				logger.info("listaPreguntas.size() "+listaPreguntas.size());
				logger.info("listaPreguntas "+listaPreguntas);
				listaEvaluacionFechas = (List<ReporteSupervisionSistemasDTO>) map.get("listaEvaluacionFechas");
				logger.info("mapa datos "+map.keySet());
				logger.info("listaEvaluacionFechas.size() "+listaEvaluacionFechas.size());
				logger.info("listaEvaluacionFechas "+listaEvaluacionFechas);
			}
		} catch (Exception e) {
			logger.info("Ocurrio algo... "+e);
			e.printStackTrace();
		}
		
		List<ReporteSupervisionSistemasDTO> listaEvidencias= new ArrayList<ReporteSupervisionSistemasDTO>();
		listaEvidencias = reporteSupervisionBI.obtenerEvidencia(idBitacora);
		logger.info("listaEvidencias.size() "+listaEvidencias.size());
		logger.info("listaEvidencias "+listaEvidencias);
		
		List<List> listaRetorno= new  ArrayList<List>();
		listaRetorno.add(listaEvidencias);
		listaRetorno.add(listaPreguntas);
		listaRetorno.add(listaEvaluacionFechas);
		logger.info("listaRetorno.size() "+listaRetorno.size());
		logger.info("listaRetorno "+listaRetorno);
		
		logger.info("...SALE DEL AJAX ajaxSupervisionEvidencia...");
		return listaRetorno;
	}
	
	
	@RequestMapping(value = "/ajaxConsultaAsignacion", method = RequestMethod.GET)
	public @ResponseBody List<AsignacionCheckDTO > ajaxConsultaAsignacion(
			@RequestParam(value = "idChecklist", required = true, defaultValue = "null") String idChecklist,
			@RequestParam(value = "idCeco", required = true, defaultValue = "null") String idCeco,
			@RequestParam(value = "idUsuario", required = true, defaultValue = "null") String idUsuario,
			@RequestParam(value = "asigna", required = true, defaultValue = "null") String asigna, HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxConsultaAsignacion...");
		
		logger.info("idChecklist... "+idChecklist);
		logger.info("idCeco... "+idCeco);
		logger.info("idUsuario... "+idUsuario);
		logger.info("asigna... "+asigna);
		
		List<AsignacionCheckDTO > re = null;
		
		if(idChecklist.equals("null"))
			idChecklist=null;
		if(idCeco.equals("null"))
			idCeco=null;
		if(idUsuario.equals("null"))
			idUsuario=null;
		if(asigna.equals("null"))
			asigna=null;
		
		try {
			re = asignacionCheckBI.obtieneAsignaciones(idChecklist, idCeco, idUsuario,asigna);
		} catch (Exception e) {
			logger.info("Ocurrio algo.. " + e);
		}
	
		logger.info("consulta " + re);
		
		logger.info("...SALE DEL AJAX ajaxConsultaAsignacion...");
		return re;
	}
	
	@RequestMapping(value = "/ajaxInsertaAsignacion", method = RequestMethod.GET)
	public @ResponseBody String ajaxInsertaAsignacion(
			@RequestParam(value = "idUsuario", required = true, defaultValue = "0") int idUsuario,
			@RequestParam(value = "nomUsuario", required = true, defaultValue = "null") String nomUsuario,
			@RequestParam(value = "idSucursal", required = true, defaultValue = "null") String idSucursal,
			@RequestParam(value = "nomSucursal", required = true, defaultValue = "null") String nomSucursal,
			@RequestParam(value = "fecha", required = true, defaultValue = "null") String fecha,
			@RequestParam(value = "bunker", required = true, defaultValue = "0") int bunker,
			@RequestParam(value = "horaBunker", required = true, defaultValue = "null") String horaBunker,
			@RequestParam(value = "horaBunkerSalida", required = true, defaultValue = "null") String horaBunkerSalida,
			@RequestParam(value = "justificacion", required = true, defaultValue = "null") String justificacion,
			@RequestParam(value = "idChecklist", required = true, defaultValue = "0") int idChecklist,
			@RequestParam(value = "asigna", required = true, defaultValue = "0") int asigna,HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxInsertaAsignacion...");
		
		String justifica =new String(justificacion.getBytes("ISO-8859-1"),"UTF-8");
		
		logger.info("idUsuario... "+idUsuario);
		logger.info("nomUsuario... "+nomUsuario);
		logger.info("idSucursal... "+idSucursal);
		logger.info("nomSucursal... "+nomSucursal);
		logger.info("fecha... "+fecha);
		logger.info("bunker... "+bunker);
		logger.info("horaBunker... "+horaBunker);
		logger.info("horaBunkerSalida... "+horaBunkerSalida);
		logger.info("justificacion... "+justifica);
		logger.info("idChecklist... "+idChecklist);
		logger.info("asigna... "+asigna);
		
		String mailContactoMaria = FRQConstantes.mailContactoMaria;
		String mailContactoJorge = FRQConstantes.mailContactoJorge;
		String mailContactoGustavo = FRQConstantes.mailContactoGustavo;
				
		String correosD="";
		String correosC="";
		
		if(bunker==1) {
			correosD = mailContactoMaria +"," + mailContactoJorge +"," + mailContactoGustavo;
		}else {
			correosD = mailContactoMaria;
		}
		logger.info("correosD... " + correosD);
		
		CorreosLotus correosLotus= new CorreosLotus();

		try {
			correosC=correosLotus.validaUsuarioLotusCorreos(""+asigna);	
			logger.info("correoCopia - " + correosC);
			if(correosC.contains("Ocurrio algo"))
				correosC="jsepulveda@elektra.com.mx";
			
		} catch (Exception e) {
			logger.info("Algo ocurrió al consultar el correoLotus " + e);
			correosC="jsepulveda@elektra.com.mx";
		}
		
		boolean re = false;
		AsignacionCheckDTO asignacion =  new AsignacionCheckDTO();
		
		asignacion.setIdUsuario(idUsuario);
		asignacion.setNombreUsuario(nomUsuario);
		asignacion.setIdSucursal(idSucursal);
		asignacion.setNombreSucursal(nomSucursal);
		asignacion.setFecha(fecha);
		asignacion.setBunker(bunker);
		if(bunker==1) {
			asignacion.setHoraBunker(horaBunker);
			asignacion.setHoraBunkerSalida(horaBunkerSalida);
		}else {
			asignacion.setHoraBunker(null);
			asignacion.setHoraBunkerSalida(null);
		}
		asignacion.setJustificacion(justifica);
		asignacion.setIdChecklist(idChecklist);
		asignacion.setAsigna(asigna);
		
		asignacion.setEnvio("0");
		asignacion.setCorreoD(correosD);
		asignacion.setCorreoC(correosC);
		
		try {
			re = asignacionCheckBI.insertaAsignacion(asignacion);
		} catch (Exception e) {
			logger.info("Ocurrio algo..");
		}
	
		logger.info("insertar " + re);
		
		logger.info("...SALE DEL AJAX ajaxInsertaAsignacion...");
		return ""+re;
	}
	
	@RequestMapping(value = "/ajaxActualizaAsignacion", method = RequestMethod.GET)
	public @ResponseBody String ajaxActualizaAsignacion(
			@RequestParam(value = "idAsigna", required = true, defaultValue = "0") int idAsigna,
			@RequestParam(value = "idUsuario", required = true, defaultValue = "0") int idUsuario,
			@RequestParam(value = "nomUsuario", required = true, defaultValue = "null") String nomUsuario,
			@RequestParam(value = "idSucursal", required = true, defaultValue = "null") String idSucursal,
			@RequestParam(value = "nomSucursal", required = true, defaultValue = "null") String nomSucursal,
			@RequestParam(value = "fecha", required = true, defaultValue = "null") String fecha,
			@RequestParam(value = "bunker", required = true, defaultValue = "0") int bunker,
			@RequestParam(value = "horaBunker", required = true, defaultValue = "null") String horaBunker,
			@RequestParam(value = "horaBunkerSalida", required = true, defaultValue = "null") String horaBunkerSalida,
			@RequestParam(value = "justificacion", required = true, defaultValue = "null") String justificacion,
			@RequestParam(value = "idChecklist", required = true, defaultValue = "0") int idChecklist,
			@RequestParam(value = "asigna", required = true, defaultValue = "0") int asigna,HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxActualizaAsignacion...");
		
		String justifica =new String(justificacion.getBytes("ISO-8859-1"),"UTF-8");
		
		logger.info("idAsigna... "+idAsigna);
		logger.info("idUsuario... "+idUsuario);
		logger.info("nomUsuario... "+nomUsuario);
		logger.info("idSucursal... "+idSucursal);
		logger.info("nomSucursal... "+nomSucursal);
		logger.info("fecha... "+fecha);
		logger.info("bunker... "+bunker);
		logger.info("horaBunker... "+horaBunker);
		logger.info("horaBunkerSalida... "+horaBunkerSalida);
		logger.info("justificacion... "+justifica);
		logger.info("idChecklist... "+idChecklist);
		logger.info("asigna... "+asigna);
		
		String mailContactoMaria = FRQConstantes.mailContactoMaria;
		String mailContactoJorge = FRQConstantes.mailContactoJorge;
		String mailContactoGustavo = FRQConstantes.mailContactoGustavo;
		
		String correosD="";
		String correosC="";
		
		if(bunker==1) {
			correosD = mailContactoMaria +"," + mailContactoJorge +"," + mailContactoGustavo;
		}else {
			correosD = mailContactoMaria;
		}
		logger.info("correosD... " + correosD);
		
		CorreosLotus correosLotus= new CorreosLotus();

		try {
			correosC=correosLotus.validaUsuarioLotusCorreos(""+asigna);	
			logger.info("correoCopia - " + correosC);
			if(correosC.contains("Ocurrio algo") || correosC.contains(""))
				correosC="jsepulveda@elektra.com.mx";
		} catch (Exception e) {
			logger.info("Algo ocurrió al consultar el correoLotus " + e);
			correosC="jsepulveda@elektra.com.mx";
		}
		
		boolean re = false;
		AsignacionCheckDTO asignacion =  new AsignacionCheckDTO();
		
		asignacion.setIdAsigna(idAsigna);
		asignacion.setIdUsuario(idUsuario);
		asignacion.setNombreUsuario(nomUsuario);
		asignacion.setIdSucursal(idSucursal);
		asignacion.setNombreSucursal(nomSucursal);
		asignacion.setFecha(fecha);
		asignacion.setBunker(bunker);
		if(bunker==1) {
			asignacion.setHoraBunker(horaBunker);
			asignacion.setHoraBunkerSalida(horaBunkerSalida);
		}else {
			asignacion.setHoraBunker(null);
			asignacion.setHoraBunkerSalida(null);
		}
		asignacion.setJustificacion(justifica);
		asignacion.setIdChecklist(idChecklist);
		asignacion.setAsigna(asigna);
		
		asignacion.setEnvio("0");
		asignacion.setCorreoD(correosD);
		asignacion.setCorreoC(correosC);
		
		try {
			re = asignacionCheckBI.actualizaAsignacion(asignacion);
		} catch (Exception e) {
			logger.info("Ocurrio algo..");
		}
	
		logger.info("actualización " + re);
		
		logger.info("...SALE DEL AJAX ajaxActualizaAsignacion...");
		return ""+re;
	}
	
	
	
	@RequestMapping(value = "/ajaxEliminaAsignacion", method = RequestMethod.GET)
	public @ResponseBody boolean ajaxEliminaAsignacion(
			@RequestParam(value = "idAsigna", required = true, defaultValue = "0") int idAsigna,HttpServletRequest request) throws Exception {
		
		logger.info("...ENTRO AL AJAX ajaxEliminaAsignacion...");
		
		logger.info("asigna... "+idAsigna);
		
		boolean re = false; 
		AsignacionCheckDTO asignacion =  new AsignacionCheckDTO();
		asignacion.setIdAsigna(idAsigna);
		
		try {
			re = asignacionCheckBI.eliminarAsignacion(asignacion);
		} catch (Exception e) {
			logger.info("Ocurrio algo..");
		}
	
		logger.info("elimina " + re);
		
		logger.info("...SALE DEL AJAX ajaxEliminaAsignacion...");
		return re;
	}
	
	
	//RESPALDO PARA NIVELES TERRITORIALES
	//10.51.210.239:8080/checklist/central/vistaSupervisionSistemasNegocio.htm
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"central/vistaSupervisionSistemasNegocio.htm"}, method = RequestMethod.GET)
	public ModelAndView vistaSupervisionSistemasNegocio(HttpServletRequest request, HttpServletResponse response) throws Exception{
		logger.info("...ENTRO AL CONTROLLER vistaSupervisionSistemasNegocio...");
		
		ModelAndView mv = null;
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaSupervisionSistemas.htm");
		}else{
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
			if(idUsuario>0){
				mv = new ModelAndView("vistaSistemas");
				mv.addObject("idUsuario", idUsuario);
				mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
				Map<String, Object> map = new HashMap<String, Object>();
				
				List<ReporteSistemasDTO> listareporteTotales = new ArrayList<ReporteSistemasDTO>();
				List<ReporteSupervisionSistemasDTO> listareporteSistemas = new ArrayList<ReporteSupervisionSistemasDTO>();
				
				int cecos[] = {236737,236736,236738};
				
				//int cecos[] = {236738};
	
				List<ReporteSistemasDTO> listaAux1;
				List<ReporteSupervisionSistemasDTO> listaAux2;
				ReporteSistemasDTO objTotal = new ReporteSistemasDTO();
				int zonas = 0;
				
				try {
					//map = reporteSupervisionBI.reporteSistemas(230034);
					
					for (int i = 0; i < cecos.length; i++) {
						map = reporteSupervisionBI.reporteSistemas(cecos[i]);
						logger.info("Map... " + map);						
						
						if(map != null) {
							zonas++;
							//logger.info("Tamaño map de totales y reportes por direccion: "+map.size());
							listaAux1 = (List<ReporteSistemasDTO>) map.get("listaReporteTotales");
							
							for (int j = 0; j < listaAux1.size(); j++) {
								ReporteSistemasDTO objAux = listaAux1.get(j);
								objTotal.setTotales(objAux.getTotales() + objTotal.getTotales());
								objTotal.setActuales(objAux.getActuales() + objTotal.getActuales());
							}
							listaAux2  = (List<ReporteSupervisionSistemasDTO>) map.get("listaReporteSistemas");
							for (int j = 0; j < listaAux2.size(); j++) {
								ReporteSupervisionSistemasDTO objAux = listaAux2.get(j);
								listareporteSistemas.add(objAux);
							}
						}
					}					
					
					if (zonas == 3) {
						listareporteTotales.add(objTotal);
						logger.info("Size listareporteTotales:"+listareporteTotales.size());
						mv.addObject("listareporteTotales", listareporteTotales);
						
						logger.info("Size listareporteSistemas:"+listareporteSistemas.size());
						mv.addObject("listareporteSistemas", listareporteSistemas);
					}else {
						logger.info("...No cargo las 3 zonas...");
						mv = new ModelAndView("http404");
					}
				} catch (Exception e) {
					logger.info("Ocurrio algo... "+e);
				}
			}else{
				logger.info("...SALE DEL CONTROLLER vistaSupervisionSistemasNegocio...");
				mv = new ModelAndView("http404");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaSupervisionSistemasNegocio...");
		return mv;
	}
	
	@RequestMapping(value = {"central/vistaFiltroSupervisionNegocio.htm"}, method = RequestMethod.GET)
	public ModelAndView vistaFiltroSupervisionSistemasNegocio(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idCeco", required = true, defaultValue = "000000") String area,
			@RequestParam(value = "nombreCeco", required = true, defaultValue = "General") String nombreCeco)throws Exception{
		
		logger.info("...ENTRO AL CONTROLLER vistaFiltroSupervision...");
		ModelAndView mv = null;
		
		int tipo = this.tipoSis[this.opcReporte];
		
		int idUsuario=0;
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		if(userSession==null){
			response.sendRedirect("vistaSupervisionSistemas.htm");
		}else{
			idUsuario=Integer.parseInt(userSession.getIdUsuario());
			logger.info("USUARIO "+ idUsuario);
		
			if(idUsuario>0){
				logger.info("EL ID CECO DE ESTA PETICION ES "+ area);
				mv = new ModelAndView("vistaFiltroSistemas");
				
				mv.addObject("area", area);
				mv.addObject("nombreCeco", nombreCeco);
				mv.addObject("idUsuario", idUsuario);
				mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
				
				//FILTRO SUCURSALES
				List<ReporteSupervisionSistemasDTO> listaFiltros = new ArrayList<ReporteSupervisionSistemasDTO>();
				listaFiltros = reporteSupervisionBI.filtroCecos(tipo);
				mv.addObject("listaFiltros", listaFiltros);
				
				
				/*//REPORTES CICAPS
				List<ReporteSupervisionSistemasDTO> listaReportesSicap = new ArrayList<ReporteSupervisionSistemasDTO>();
				listaReportesSicap = reporteSupervisionBI.reporteSisCap("", "", "", "",area);
				mv.addObject("listaReportesSicap", listaReportesSicap);
				*/
				
				//REPORTES CICAPS
				List<ReporteSupervisionSistemasDTO> listaReportesSicap = new ArrayList<ReporteSupervisionSistemasDTO>();
				
				if(area.equals("000000")) {
					
					//listaReportesSicap = reporteSupervisionBI.reporteSisCap("", "", "", "",area);
					
					int cecos[] = {236737,236736,236738};
					List<ReporteSupervisionSistemasDTO> listaAux;
					
					for (int i = 0; i < cecos.length; i++) {
						listaAux = reporteSupervisionBI.reporteSisCap("", "", "", "",""+cecos[i]);
						if(listaAux != null) {
							for (int j = 0; j < listaAux.size(); j++) {
								ReporteSupervisionSistemasDTO objAux = listaAux.get(j);
								listaReportesSicap.add(objAux);	
							}
						}
					}
					logger.info("listaReportesSicap.size() : " + listaReportesSicap.size());
					mv.addObject("listaReportesSicap", listaReportesSicap);
				}else {
					listaReportesSicap = reporteSupervisionBI.reporteSisCap("", "", "", "",area);
					mv.addObject("listaReportesSicap", listaReportesSicap);
				}
				
				
				//FILTRO SUCURSALES
				List<ReporteSupervisionSistemasDTO> listaFiltrosSucursales = new ArrayList<ReporteSupervisionSistemasDTO>();
				listaFiltrosSucursales = reporteSupervisionBI.filtroSucursal("");
				mv.addObject("listaFiltrosSucursales", listaFiltrosSucursales);
				logger.info("listaFiltrosSucursales : " + listaFiltrosSucursales.size());
				logger.info("listaFiltrosSucursales : " + listaFiltrosSucursales.toString());
				
			}else{
				logger.info("...SALE DEL CONTROLLER vistaFiltroSupervision...");
				mv = new ModelAndView("http404");
			}
		}
		logger.info("...SALE DEL CONTROLLER vistaFiltroSupervision...");
		return mv;
	}
	
}