/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ComentariosRepAsgDTO;
import com.gruposalinas.checklist.mappers.ChecksOfflineCompletosRowMapper;
import com.gruposalinas.checklist.mappers.ChecksOfflineRowMapper;
import com.gruposalinas.checklist.mappers.UsuarioSucOfflineRowMapper;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author leodan1991
 */
public interface ComentariosRepAsgDAO {

    List<ComentariosRepAsgDTO> getComentarios(String ceco, String fecha, Integer anio, Integer mes) throws Exception;
    Boolean setComentarios(String ceco, String fecha,String comentario) throws Exception ;
    Boolean updateComentarios(String ceco, String fecha, String comentario) throws Exception ;
    Boolean deleteComentarios(String ceco, String fecha, String comentario) throws Exception ;

}
