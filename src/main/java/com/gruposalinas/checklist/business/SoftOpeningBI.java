package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.SoftOpeningDAO;
import com.gruposalinas.checklist.domain.SoftOpeningDTO;

public class SoftOpeningBI {

	private static Logger logger = LogManager.getLogger(SoftOpeningBI.class);

private List<SoftOpeningDTO> listafila;
	
	@Autowired
	SoftOpeningDAO softOpeningDAO;
		
	
	public int inserta(SoftOpeningDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = softOpeningDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	public int inserta2(int bitacora){
		int respuesta = 1;
		
		try {
			respuesta = softOpeningDAO.inserta2(bitacora);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	public int insertaMan(SoftOpeningDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = softOpeningDAO.insertaMan(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean elimina(String ceco,String recorrido){
		boolean respuesta = false;
		
		try {
			respuesta = softOpeningDAO.elimina(ceco,recorrido);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<SoftOpeningDTO> obtieneDatos( int ceco) {
		
		try {
			listafila =  softOpeningDAO.obtieneDatos(ceco);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<SoftOpeningDTO> obtieneInfo() {

		try {
			listafila = softOpeningDAO.obtieneInfo();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de la tabla Filas");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	
	public boolean actualiza(SoftOpeningDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = softOpeningDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	

}