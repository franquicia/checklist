package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.MovilInfoDTO;

public class MovilInfoRowMapper implements RowMapper<MovilInfoDTO> {

	public MovilInfoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		MovilInfoDTO movilInfo = new MovilInfoDTO();
		
		movilInfo.setIdMovil(rs.getInt("FIIDMOVIL"));
		movilInfo.setIdUsuario(rs.getInt("FIID_USUARIO"));
		movilInfo.setFecha(rs.getString("FDFECHA"));
		movilInfo.setSo(rs.getString("FCSO"));
		movilInfo.setVersion(rs.getString("FCVERSION"));
		movilInfo.setVersionApp(rs.getString("FCVERSIONAPP"));
		movilInfo.setModelo(rs.getString("FCMODELO"));
		movilInfo.setFabricante(rs.getString("FCFABRICANTE"));
		movilInfo.setNumMovil(rs.getString("FINUMMOVIL"));
		movilInfo.setTipoCon(rs.getString("FCTIPOCON"));
		movilInfo.setIdentificador(rs.getString("FCIDENTIFICADOR"));
		movilInfo.setToken(rs.getString("FCTOKEN"));
		
		return movilInfo;
	}
}
