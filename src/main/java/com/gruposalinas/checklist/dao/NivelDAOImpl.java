package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.NivelDTO;
import com.gruposalinas.checklist.mappers.NivelRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class NivelDAOImpl extends DefaultDAO implements NivelDAO {

    public static Logger logger = LogManager.getLogger(NivelDTO.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneNivel;
    DefaultJdbcCall jdbcInsertaNivel;
    DefaultJdbcCall jdbcActualizaNivel;
    DefaultJdbcCall jdbcEliminaNivel;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_NIVEL")
                .withProcedureName("SP_SEL_G_NIVEL")
                .returningResultSet("RCL_NIVEL", new NivelRowMapper());

        jdbcObtieneNivel = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_NIVEL")
                .withProcedureName("SP_SEL_NIVEL")
                .returningResultSet("RCL_NIVEL", new NivelRowMapper());

        jdbcInsertaNivel = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_NIVEL")
                .withProcedureName("SP_INS_NIVEL");

        jdbcActualizaNivel = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_NIVEL")
                .withProcedureName("SP_ACT_NIVEL");

        jdbcEliminaNivel = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_NIVEL")
                .withProcedureName("SP_DEL_NIVEL");
    }

    @SuppressWarnings("unchecked")
    public List<NivelDTO> obtieneNivel() throws Exception {
        Map<String, Object> out = null;
        List<NivelDTO> listaNivel = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_NIVEL.SP_SEL_G_NIVEL}");

        listaNivel = (List<NivelDTO>) out.get("RCL_NIVEL");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Niveles");
        } else {
            return listaNivel;
        }

        return listaNivel;

    }

    @SuppressWarnings("unchecked")
    public List<NivelDTO> obtieneNivel(int idNivel) throws Exception {
        Map<String, Object> out = null;
        List<NivelDTO> listaNivel = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_NIVEL", idNivel);

        out = jdbcObtieneNivel.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_NIVEL.SP_SEL_NIVEL}");

        listaNivel = (List<NivelDTO>) out.get("RCL_NIVEL");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Nivel");
        } else {
            return listaNivel;
        }

        return listaNivel;
    }

    public int insertaNivel(NivelDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idNivel = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NEGOCIO", bean.getIdNegocio())
                .addValue("PA_CODIGO", bean.getCodigo())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_ACTIVO", bean.getActivo());

        out = jdbcInsertaNivel.execute(in);

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idNivelReturn = (BigDecimal) out.get("PA_FIIDNIVEL");
        idNivel = idNivelReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Nivel");
        } else {
            return idNivel;
        }

        return idNivel;

    }

    public boolean actualizaNivel(NivelDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDNIVEL", bean.getIdNivel())
                .addValue("PA_FIIDNEGOCIO", bean.getIdNegocio())
                .addValue("PA_CODIGO", bean.getCodigo())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_ACTIVO", bean.getActivo());

        out = jdbcActualizaNivel.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_NIVEL.SP_ACT_NIVEL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el Nivel id( " + bean.getIdNivel() + ")");
        } else {
            return true;
        }

        return false;

    }

    public boolean eliminaNivel(int idNivel) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDNIVEL", idNivel);

        out = jdbcEliminaNivel.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_NIVEL.SP_DEL_NIVEL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el Nivel id(" + idNivel + ")");
        } else {
            return true;
        }

        return false;

    }
}
