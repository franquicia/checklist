package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ModuloDTO;

public class ModuloSingleRowMapper implements RowMapper<ModuloDTO> {

	@Override
	public ModuloDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		ModuloDTO moduloDTO = new ModuloDTO();
		
		moduloDTO.setIdModulo(rs.getInt("FIID_MODULO"));
		moduloDTO.setNombre(rs.getString("FCNOMBRE"));
		return moduloDTO;
	}
	
	

}
