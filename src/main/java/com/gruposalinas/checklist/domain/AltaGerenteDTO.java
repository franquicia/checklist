package com.gruposalinas.checklist.domain;

public class AltaGerenteDTO {
	
	private int idGerente;
	private String idGerente2;
	private String nomGerente;
	private int idUsuario;
	private String idUsuario2;
	private String nomUsuario;
	private String Zona;
	private String Region;
	private int  idPerfil;
	private int idSecuencia;
	public int getIdPerfil() {
		return idPerfil;
	}
	public void setIdPerfil(int idPerfil) {
		this.idPerfil = idPerfil;
	}
	public int getIdGerente() {
		return idGerente;
	}
	public void setIdGerente(int idGerente) {
		this.idGerente = idGerente;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public String getIdGerente2() {
		return idGerente2;
	}
	public void setIdGerente2(String idGerente2) {
		this.idGerente2 = idGerente2;
	}
	public String getIdUsuario2() {
		return idUsuario2;
	}
	public void setIdUsuario2(String idUsuario2) {
		this.idUsuario2 = idUsuario2;
	}
	public String getZona() {
		return Zona;
	}
	public void setZona(String zona) {
		Zona = zona;
	}
	public String getRegion() {
		return Region;
	}
	public void setRegion(String region) {
		Region = region;
	}
	public String getNomGerente() {
		return nomGerente;
	}
	public void setNomGerente(String nomGerente) {
		this.nomGerente = nomGerente;
	}
	public String getNomUsuario() {
		return nomUsuario;
	}
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}
	public int getIdSecuencia() {
		return idSecuencia;
	}
	public void setIdSecuencia(int idSecuencia) {
		this.idSecuencia = idSecuencia;
	}
	@Override
	public String toString() {
		return "AltaGerenteDTO [idGerente=" + idGerente + ", idGerente2=" + idGerente2 + ", nomGerente=" + nomGerente
				+ ", idUsuario=" + idUsuario + ", idUsuario2=" + idUsuario2 + ", nomUsuario=" + nomUsuario + ", Zona="
				+ Zona + ", Region=" + Region + ", idPerfil=" + idPerfil + ", idSecuencia=" + idSecuencia + "]";
	}
	
	
}
