package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.CompromisoCecoDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.domain.PuestoDTO;
import com.gruposalinas.checklist.domain.ReporteDTO;
import com.gruposalinas.checklist.domain.VistaCDTO;

public interface ReporteDAO {

	public List<ReporteDTO> buscaReporte (String idCheck, String idUsuario, String nombreUsuario, String nombreSucursal, String fechaRespuesta, String nombreCheck, String metodoCheck) throws Exception;
	
	public List<ReporteDTO>  buscaReporteURL (String idCategoria, String idTipo, String idPuesto, String idUsuario, String idCeco, String idSucursal, int idNivel,String fecha, String idPais) throws Exception;

	public List<ReporteDTO>  ComparaImagen (int idRuta,int idPregunta, int idCheckUsuario, String fecha) throws Exception;
	
	public List<ReporteDTO> ReporteRegional (String fechaIni, String fechaFin) throws Exception;
	
	public List<ReporteDTO> ReporteCheckUsua (String idCheck, String idPuesto) throws Exception;
	
	public Map<String, Object> ReporteGeneral (int idChecklist, String fechaInicio, String fechaFin) throws Exception;
	
	public Map<String, Object> ReporteGeneralCompromisos (int idChecklist, String fechaInicio, String fechaFin) throws Exception;
	
	public Map<String, Object> ReporteProtocolos (int idChecklist, String fechaInicio, String fechaFin) throws Exception;
	
	public int ReporteCalificacion(int idBitacora, int bandera) throws Exception;
	
	
	/***************REPORTE REGIONAL************************/
	
	public List<ReporteDTO> ReporteSupReg (String nivelTerritorio, String nivelZona, String nivelRegion, String region, String idCecoSup, String pais, String negocio, String fechaI, String fechaF, String idChecklist)  throws Exception;
	
	public Map<String, Object> ReporteDetSupReg(int idChecklist, String idCeco, String pais, String negocio, String fechaI, String fechaF) throws Exception;
	
	public Map<String, Object> ReporteDetSupReg_versBorra(int idChecklist, String idCeco, String pais, String negocio, String fechaI, String fechaF) throws Exception;
		
	public Map<String, Object> ReporteOperacion (int idChecklist, String fechaInicio, String fechaFin, String pais, String negocio, String territorio, String zona, String region, String ceco) throws Exception;
	
	public List<ReporteDTO> ReporteRegSup (int idCheck, int idPregunta, String fechaI,String fechaFin, String pais, String negocio,  String nivelRegion, String idCeco) throws Exception;
	
	/*************DETALLE REPORTE REGIONAL******************/
	
	public Map<String, Object> ReporteDetRegional(int idBitacora) throws Exception;
	
	public List<CompromisoDTO> ReporteDetPreguntasRegional(int idBitacora) throws Exception;
	
	public List<ReporteDTO> ReporteDetReg(int idCheck, int idCeco, String fechaIni, String fechaFin) throws Exception;
	

	/*************REPORTE VISTA CUMPLIMIENTO******************/
	public Map<String, Object> ReporteVistaCumplimiento(int idUsuario, String idCeco, int pais, int canal, String fecha) throws Exception;
	
	public List<VistaCDTO> ReporteVistaC(int idUsuario, int idCeco, int idChecklist, int pais, int canal, String fecha, int bandera) throws Exception;
	
	public List<CompromisoCecoDTO> CecoInfo(int idUsuario) throws Exception;
	
	public Map<String, Object> ReporteDetVC(int idCeco, int idChecklist, int idUsuario, String fecha) throws Exception;
	
	public List<VistaCDTO>  ReporteChecklist(int idCeco, int idChecklist) throws Exception;
	
	
	/*************CARGA REPORTE******************/
	
	public boolean CargaInicialHistorico() throws Exception;
	
	public boolean CargaCecosHistorico() throws Exception;
	
	public boolean CargaGeografiaHistorico() throws Exception;
}
