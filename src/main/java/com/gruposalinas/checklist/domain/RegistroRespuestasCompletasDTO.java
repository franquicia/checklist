package com.gruposalinas.checklist.domain;

import java.util.List;


public class RegistroRespuestasCompletasDTO {

	private List<RegistroRespuestaDTO> listaRespuestaCompleta;

	public List<RegistroRespuestaDTO> getListaRespuestaCompleta() {
		return listaRespuestaCompleta;
	}

	public void setListaRespuestaCompleta(List<RegistroRespuestaDTO> listaRespuestaCompleta) {
		this.listaRespuestaCompleta = listaRespuestaCompleta;
	}
		
}
