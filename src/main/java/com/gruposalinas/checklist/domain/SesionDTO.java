package com.gruposalinas.checklist.domain;

public class SesionDTO {

	private int idSesion;
	private String navegador;
	private int idUsuario;
	private String ip;
	private String fechaLogueo;
	private String fechaActualiza;
	private int bandera;
	private String sesionNav;
	public int getIdSesion() {
		return idSesion;
	}
	public void setIdSesion(int idSesion) {
		this.idSesion = idSesion;
	}
	public String getNavegador() {
		return navegador;
	}
	public void setNavegador(String navegador) {
		this.navegador = navegador;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getFechaLogueo() {
		return fechaLogueo;
	}
	public void setFechaLogueo(String fechaLogueo) {
		this.fechaLogueo = fechaLogueo;
	}
	public String getFechaActualiza() {
		return fechaActualiza;
	}
	public void setFechaActualiza(String fechaActualiza) {
		this.fechaActualiza = fechaActualiza;
	}
	public int getBandera() {
		return bandera;
	}
	public void setBandera(int bandera) {
		this.bandera = bandera;
	}
	public String getSesionNav() {
		return sesionNav;
	}
	public void setSesionNav(String sesionNav) {
		this.sesionNav = sesionNav;
	}
	
	
}
