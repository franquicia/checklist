<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<script type="text/javascript">
	var urlServer = '';
</script>

<script	src="../css/jQuery/jQuery.js"></script>
<script	src="../js/script-consultas.js"></script>

<!-- DESABILITA EL BOTON DE ENTER EN LA VISTA -->
<script type="text/javascript">
$(document).ready(function() {
	  $(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
	});
</script>
<script>
function goBack() {
    window.history.back();
}
</script>

<!-- PONE EL TIEMPO DEL MENSAJE -->
<script type="text/javascript">
$(document).ready(function() {
    setTimeout(function() {
        $(".content-Mensaje").fadeOut(1500);
    },3000);
});
</script>

<!-- EJECUTA EL CALENDARIO -->
<script type="text/javascript">
$(function() {
    $( "#fechaImagen" ).datepicker();
});
</script>

<script>
	idChecklistA=${idChecklist};
</script>

	<input type="hidden" id="idChecklist" name="idChecklist" value="${idChecklist}" />
<h2 class="w3-animate-zoom  datos-vacios content-Mensaje">${fallo}</h2>
				
	<div style="background: white; margin: 30px 24px 50px 24px; min-width: 600px;   box-shadow: 5px 5px 17px #000000;" >

		<form:form name="formulario" method="POST"	action="visualizadorController.htm" commandName="command" style="height:100%;">
	
		
			
			
			
			<div style="height: 36.79%; min-height: 248px;">
				<h2 style="position: relative; text-align: center; font:18px 'Roboto-Regular'; font-weight: 700;">Titulo del checklist: ${nombreChecklist}</h2>
				<ul class="margenUl">
				
				<div class="numero-pregunta" id="n-generar">1</div>
		
				<div style="height:11%; ">	
					<li class="li-stilo"><span class="primer-nivel">Pais</span>
					<c:set var="margen" scope="session" value="${-20}" /> 
						<div class="cajaVis" id="paisVis">
							<input type="hidden" name="pais" value="Selecciona una Opcion" id="valorPais">
							<span class="seleccionadoVis" id="Selecciona una Opcion">Elije una opcion</span>
							<ul class="listaselectVis">
								<c:forEach items="${listaPaises}" var="item">
									<li style="margin:${margen}px 0 0 0;" value="${item.idPais}"><a href="">${item.nombre}</a></li>
									<c:set var="margen" scope="session" value="${-40}" /> 
								</c:forEach>					
								</ul >
							<span class="trianguloinfVis"></span>
						</div>
					</li>
				</div>	
									
					<div style="height:11%;">	
					<li class="li-stilo margenLista"><span class="segundo-nivel">Territorio</span> 
						<div class="cajaVis" id="territorioVis">
							<input type="hidden" name="territorio" value="Selecciona una Opcion" id="valorTerritorio">
							<span class="seleccionadoVis " id="Selecciona una Opcion">Elije una opcion</span>
								<ul class="listaselectVis" id="territorioVis2"></ul>
							<span class="trianguloinfVis"></span>
						</div>
					</li>
				</div>
					
				<div style="height:12%;">	
					<li class="li-stilo margenLista"><span class="segundo-nivel">Zona</span> 
						<div class="cajaVis" id="zonaVis">
							<input type="hidden" name="zona" value="Selecciona una Opcion" id="valorZona">
							<span class="seleccionadoVis " id="Selecciona una Opcion">Elije una opcion</span>
								<ul class="listaselectVis" id="zonaVis2"></ul>
							<span class="trianguloinfVis"></span>
						</div>
					
					</li>
				</div>	
							
				<div style="height:12%;">				
					<li class="li-stilo margenLista"><span class="tercer-nivel">Región</span> 
						<div class="cajaVis" id="regionVis">
							<input type="hidden" name="region" value="Selecciona una Opcion" id="valorRegion">
							<span class="seleccionadoVis " id="Selecciona una Opcion">Elije una opcion</span>
								<ul class="listaselectVis" id="regionVis2"></ul>
							<span class="trianguloinfVis"></span>
						</div>
					
					</li>
				</div>					
			
				<div style="height:12%;">					
					<li class="li-stilo margenLista"><span class="cuarto-nivel">Sucursal</span> 
						<div class="cajaVis" id="sucursalVis">
							<input type="hidden" name="sucursal" value="Selecciona una Opcion" id="valorSucursal">
							<span class="seleccionadoVis " id="Selecciona una Opcion">Elije una opcion</span>
								<ul class="listaselectVis" id="sucursalVis2"></ul>
							<span class="trianguloinfVis"></span>
						</div>
					
					</li>
				</div>
				
										
				</ul>	
			</div>
					
				<div style="height: 32%; min-height: 210px;">
				<ul class="margenUl">
				
				<div style="height:25%; ">
					<li class="li-stilo"><span class="circulo">2</span> Puesto 
					
					<div class="cajaVis" id="puestoVis">
					<input type="hidden" name="puesto" value="Selecciona una Opcion" id="valorPuesto">
						<span class="seleccionadoVis" id="Selecciona una Opcion">Elije una opcion</span>
						<ul class="listaselectVis"  id="buscaPuesto"  style="height:auto; max-height: 250px; overflow: auto; z-index: 1;">
							<c:forEach items="${listaPuestoAsig}" var="item">
								<li value="${item.idPuesto}"><a href="">${item.descripcion}</a></li>
							</c:forEach>						
						</ul >
						<span class="trianguloinfVis"></span>
					</div>
					</li>
				</div>	
				<div style="height:22%;">	
					<li class="li-stilo"><span class="primer-nivel">Usuario</span> 
					<input class="textBoxInput" type="text" name="usuario" placeholder="Escribe el número de socio" id="buscaUsuario2" size="22"/>
				</div>
			

				</ul>	
			</div>
			


			<div style="margin-top:1.5%; height: 50px; width: 20%; float:right;">
					<button class="logEnviar-btn" style="width: 150px;" id="asignarChecklist" name="envia" >Asignar checklist</button>
			</div>
			
		</form:form>
		
		
			<div style="margin-top:1.5%; height: 50px; width: 20%; float:right;">
					<button class="logEnviar-btn" style="width: 150px; margin-right:90%;" id="asignarChecklist" name="atras"  onclick="goBack();">Atras</button>
	
			</div>
	