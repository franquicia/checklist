package com.gruposalinas.checklist.util;

import com.gruposalinas.checklist.domain.TokenDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author CÃƒÂ©sar Lizandro Vidal Romero Clase para encriptar y desencriptar
 * entre los sistemas de bienestar y la validaciÃƒÂ³n por llave maestra de
 * portales
 *
 */
@SuppressWarnings("restriction")
public class UtilCryptoGS {

    private static Logger logger = LogManager.getLogger(UtilCryptoGS.class);

    public static final String FORMAT_CHARACTER_ENCODING = "UTF-8";
    public static final String CIPHER_TRANSFORM_ALGORITHM = "AES/CBC/PKCS5Padding";
    public static final String ALGORITHM_ENCRYPT_METHOD = "AES";

    // private static final Logger logger =
    // LogManager.getLogger(UtilCryptoGS.class);
    /**
     * <summary>Cifra texto plano utilizando la clave de 128 bits AES y una
     * cadena Block Cipher y devuelve una cadena codificada en base64 </summary>
     * <param name="plainText">Texto sin formato para cifrar</param>
     * <param name="key">Clave secreta</param> <returns>Cadena codificada en
     * Base64</returns>
     *
     * @param plainText
     * @param key
     * @return
     * @throws UnsupportedEncodingException
     * @throws InvalidKeyException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    private String encrypt(String plainText, String key)
            throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        Base64.Encoder encoder = Base64.getEncoder();
        byte[] plainTextbytes = plainText.getBytes(FORMAT_CHARACTER_ENCODING);
        byte[] keyBytes = getKeyBytes(key);
        //return encoder.encode(encrypt(plainTextbytes, keyBytes, keyBytes)).replaceAll("\r\n", "");
        return encoder.encodeToString(encrypt(plainTextbytes, keyBytes, keyBytes)).replaceAll("\r\n", "");
    }

    /**
     * <summary> Descifra una cadena codificada en base64 con la clave dada
     * (clave de 128 bits AES y una cadena Block Cipher)</summary>
     * <param name="encryptedText">cadena codificada en Base64</param>
     * <param name="key">Clave secreta</param> <returns>Cadena des-encriptada
     * </returns>
     *
     * @param encryptedText
     * @param key
     * @return
     * @throws KeyException
     * @throws GeneralSecurityException
     * @throws GeneralSecurityException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws IOException
     */
    private String decrypt(String encryptedText, String key)
            throws KeyException, GeneralSecurityException, GeneralSecurityException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException, IOException {
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] cipheredBytes = decoder.decode(encryptedText);
        byte[] keyBytes = getKeyBytes(key);
        return new String(decrypt(cipheredBytes, keyBytes, keyBytes), FORMAT_CHARACTER_ENCODING).replaceAll("\n", "");
    }

    /**
     *
     * @param key
     * @return
     * @throws UnsupportedEncodingException
     */
    private byte[] getKeyBytes(String key) throws UnsupportedEncodingException {
        byte[] keyBytes = new byte[16];
        byte[] parameterKeyBytes = key.getBytes(FORMAT_CHARACTER_ENCODING);
        System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
        return keyBytes;
    }

    /**
     *
     * @param cipherText
     * @param key
     * @param initialVector
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    private byte[] decrypt(byte[] cipherText, byte[] key, byte[] initialVector)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORM_ALGORITHM);
        SecretKeySpec secretKeySpecy = new SecretKeySpec(key, ALGORITHM_ENCRYPT_METHOD);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpecy, ivParameterSpec);
        cipherText = cipher.doFinal(cipherText);
        return cipherText;
    }

    /**
     *
     * @param plainText
     * @param key
     * @param initialVector
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    private byte[] encrypt(byte[] plainText, byte[] key, byte[] initialVector)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORM_ALGORITHM);
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, ALGORITHM_ENCRYPT_METHOD);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        plainText = cipher.doFinal(plainText);
        return plainText;
    }

    public String generaToken(TokenDTO token) {
        String llaveEncripcion;

        if (token.getBandera()) {
            llaveEncripcion = token.getLlave();
        } else {
            llaveEncripcion = FRQConstantes.getLLave();
        }
        String resultado = "";
        try {
            String ipOrigenC = encrypt(token.getIpOrigen(), llaveEncripcion);
            String paginaSalidaC = encrypt(token.getUrlRedirect(), llaveEncripcion);
            String fechaC = encrypt(token.getFechaHora(), llaveEncripcion);
            resultado = ipOrigenC + ";" + token.getIdAplicacion() + ";" + paginaSalidaC + ";" + fechaC;
        } catch (Exception e) {
            logger.info("Ocurrio algo...");
        }
        return resultado;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public Map decryptMap(Map map) {
        String llaveEncripcion = FRQConstantes.getLLave();
        String fechaIntento = UtilDate.getSysDate("ddMMyyyy hh:mm:ss");
        Map<String, String> mapDesencriptado = new HashMap();
        Iterator entries = map.entrySet().iterator();

        /*Se libera hasta el 15 de Julio de 2016*/
        int contValoresDesencriptados = 0;
        int contIntentosMaximos = 0;
        boolean ban = true;
        ////System.out.println("contIntentosMaximos: " + contIntentosMaximos);
        while (ban && contValoresDesencriptados == 0 && contIntentosMaximos < 61) { //intentamos al menos 61 veces (Primer intento es con llave fija, los siguientes 60 son con llave dinamica probando 60 segundos hacÃƒÂ­a atras)
            while (entries.hasNext()) { //intentamos desencriptar cada parametro
                Map.Entry entry = (Map.Entry) entries.next();
                try {
                    String key = null;
                    String value = "";
                    try {
                        key = decrypt((String) entry.getKey(), llaveEncripcion);
                        value = decrypt(((String[]) entry.getValue())[0], llaveEncripcion);
                        contValoresDesencriptados++; //sabemos que al menos un parametro pudo ser desencriptado
                    } catch (Exception e) {
                        key = (String) entry.getKey();
                        value = ((String[]) entry.getValue())[0];
                        ban = false;
                    }

                    //logger.info( "Key: " + key);
                    //logger.info( "Value: " + value);
                    //El resultado debe de ser
                    /*19:09:08,335 INFORMACIÃƒâ€œN [STDOUT] 2016-01-11 19:09:08 INFO  UtilCryptoGS:171 - Key: employeeid
				      19:09:10,134 INFORMACIÃƒâ€œN [STDOUT] 2016-01-11 19:09:10 INFO  UtilCryptoGS:172 - Value: 664899
				      19:09:19,556 INFORMACIÃƒâ€œN [STDOUT] 2016-01-11 19:09:19 INFO  UtilCryptoGS:171 - Key: sAMAccountName
				      19:09:21,350 INFORMACIÃƒâ€œN [STDOUT] 2016-01-11 19:09:21 INFO  UtilCryptoGS:172 - Value: 664899
				      19:09:27,797 INFORMACIÃƒâ€œN [STDOUT] 2016-01-11 19:09:27 INFO  UtilCryptoGS:171 - Key: name
				      19:09:29,135 INFORMACIÃƒâ€œN [STDOUT] 2016-01-11 19:09:29 INFO  UtilCryptoGS:172 - Value: Cesar Lizandro Vidal Romero */
                    mapDesencriptado.put(key, value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                contIntentosMaximos++; //incrementamos 1 intento con la llave
                ////System.out.println("contIntentosMaximos: " + contIntentosMaximos);
                String fecha = UtilDate.dateToString(
                        DateUtils.addSeconds(
                                UtilDate.stringToDate(fechaIntento),
                                (-1 * contIntentosMaximos)
                        ),
                        "ddMMyyyy hh:mm:ss"
                );
                try {
                    llaveEncripcion = getLlaveEncripcionDinamica(encrypt(fecha, llaveEncripcion));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return mapDesencriptado;
    }

    public String getLlaveEncripcionDinamica(String fechaEncriptada) {

        String llaveEncripcion = FRQConstantes.getLLave().substring(0, 16);

        String nuevaLlaveEncripcion = llaveEncripcion + fechaEncriptada.substring(16, fechaEncriptada.length());

        return nuevaLlaveEncripcion;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public Map decryptMap2(Map map) {
        String llaveEncripcion = FRQConstantes.getLlaveEncripcionLocal();
        Map<String, String> mapDesencriptado = new HashMap();
        Iterator entries = map.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry entry = (Map.Entry) entries.next();
            try {
                String key = null;
                String value = "";
                try {
                    key = decrypt((String) entry.getKey(), llaveEncripcion);
                    value = decrypt(((String) entry.getValue()), llaveEncripcion);
                } catch (Exception e) {
                    key = (String) entry.getKey();
                    value = ((String) entry.getValue());
                }
                mapDesencriptado.put(key, value);
            } catch (Exception e) {
                logger.info("Ocurrio algo...");
            }
        }
        return mapDesencriptado;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public Map decryptMap2(Map map, String llave) {
        String llaveEncripcion = llave;
        Map<String, String> mapDesencriptado = new HashMap();
        Iterator entries = map.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry entry = (Map.Entry) entries.next();
            try {
                String key = null;
                String value = "";
                try {
                    key = decrypt((String) entry.getKey(), llaveEncripcion);
                    value = decrypt(((String) entry.getValue()), llaveEncripcion);
                } catch (Exception e) {
                    key = (String) entry.getKey();
                    value = ((String) entry.getValue());
                }
                mapDesencriptado.put(key, value);
            } catch (Exception e) {
                logger.info("Ocurrio algo...");
            }
        }
        return mapDesencriptado;
    }

    public String encryptParams(String params) throws InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        return encrypt(params, FRQConstantes.getLlaveEncripcionLocal());
    }

    public String encryptParams(String params, String llave) throws InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        return encrypt(params, llave);
    }

    public String decryptParams(String params) throws KeyException, GeneralSecurityException, IOException {
        return decrypt(params, FRQConstantes.getLlaveEncripcionLocal());
    }

    public String decryptParams(String params, String llave) throws KeyException, GeneralSecurityException, IOException {
        return decrypt(params, llave);
    }

    //@SuppressWarnings("static-access")
    public static void main(String[] args) throws KeyException, GeneralSecurityException, IOException {

        UtilDate fec = new UtilDate();
        String fecha = fec.getSysDate("ddMMyyyyHHmmss");
        //System.out.println(fecha);
        ////System.out.println("");

        //String text= "";
        ////System.out.println("resultado "+ new UtilCryptoGS().decryptParams(""));
        ////System.out.println("resultado "+ new UtilCryptoGS().decrypt(text, "eJG7R3O/Z+8a/FzQb4X7zv5Um++9tHo="));
        //boolean fechavar=new UtilDate().validaFecha("23082016204500", "22082016204500");
        ////System.out.println(fechavar);
        //if(fechavar)
        ////System.out.println(new UtilDate().diferenciaFecha("23082016204500", "22082016204500"));
        /*
		//System.out.println("");
		//System.out.println(new UtilCryptoGS().encryptParams("id=191841"));
		//System.out.println("servicio/token.json?"+(new UtilCryptoGS().encryptParams("ip=10.51.219.216&fecha="+fecha+"&idapl=666&uri=http://10.51.210.239:8080/checklist/servicios/loginRest.json")).replace("\n", ""));
		//System.out.println("servicio/token.json?"+(new UtilCryptoGS().encryptParams("ip=10.51.219.216&fecha=15092016164411&idapl=666&uri=http://10.51.210.239:8080/checklist/servicios/loginRest.json")).replace("\n", ""));
		////System.out.println("");

		URL url = new URL("http://10.51.210.239:8080/checklist/servicio/token.json?"+(new UtilCryptoGS().encryptParams("ip=10.51.219.216&fecha="+fecha+"&idapl=666&uri=http://10.51.210.239:8080/checklist/servicios/loginRest.json")).replace("\n", ""));
		BufferedReader in = null;
		try {
			  in = new BufferedReader(new InputStreamReader(url.openStream()));
			} catch(Throwable t){}

		String Text = (((in.readLine().split(",")[0]).split(":")[1]).replace('"', ' ')).replace(" ", "");

		//System.out.println("http://10.51.210.239:8080/checklist/servicios/loginRest.json?"+new UtilCryptoGS().encryptParams("id=191841")+"&token="+Text);


		//System.out.println("");
		//System.out.println(new UtilCryptoGS().encryptParams("id=191312&lat=19.30941887886347&lon=-99.18721647408643").replace("\n", ""));
		//System.out.println("servicio/token.json?"+(new UtilCryptoGS().encryptParams("ip=10.51.210.17&fecha="+fecha+"&idapl=666&uri=http://10.50.109.47:8080/checklist/servicios/checklist.json")).replace("\n", ""));
		//System.out.println("");
         */
 /*
		URL url = new URL("http://10.51.210.239:8080/checklist/servicio/token.json?idapl=666&"+(new UtilCryptoGS().encryptParams("ip=10.51.219.216&fecha="+fecha+"&uri=http://10.51.210.239:8080/checklist/servicios/checklist.json")).replace("\n", ""));
		BufferedReader in = null;
		try {
			  in = new BufferedReader(new InputStreamReader(url.openStream()));
			} catch(Throwable t){}

		String Text = (((in.readLine().split(",")[0]).split(":")[1]).replace('"', ' ')).replace(" ", "");

		//System.out.println("http://10.51.210.239:8080/checklist/servicios/checklist.json?"+new UtilCryptoGS().encryptParams("id=191312&lat=19.30941887886347&lon=-99.18721647408643")+"&token="+Text);


		/*
		//System.out.println("");
		//System.out.println(new UtilCryptoGS().encryptParams("id=191312&idCheck=21"));//produ 21 - desa 161
		//System.out.println("servicio/token.json?"+(new UtilCryptoGS().encryptParams("ip=10.51.210.17&fecha="+fecha+"&idapl=666&uri=http://10.50.109.47:8080/checklist/servicios/checklistUsuario.json")).replace("\n", ""));
		//System.out.println("");

		//System.out.println("");
		//System.out.println("");

         */
 /*

		URL url1 = new URL("http://10.51.210.239:8080/checklist/servicio/token.json?idapl=666&"+(new UtilCryptoGS().encryptParams("ip=10.51.219.216&fecha="+fecha+"&uri=http://10.51.210.239:8080/checklist/servicios/checklistUsuario")).replace("\n", ""));
		BufferedReader in1 = null;
		try {
			  in1 = new BufferedReader(new InputStreamReader(url1.openStream()));
			} catch(Throwable t){}

		String Text1 = (((in1.readLine().split(",")[0]).split(":")[1]).replace('"', ' ')).replace(" ", "");

		//System.out.println("http://10.51.210.239:8080/checklist/servicios/checklistUsuario.json?"+new UtilCryptoGS().encryptParams("id=191312&idCheck=1")+"&token="+Text1);
		/*


		//System.out.println("");
		//System.out.println(new UtilCryptoGS().encryptParams("id=191312&idBitacora=161"));//produ 21 - desa 161
		//System.out.println("servicio/token.json?"+(new UtilCryptoGS().encryptParams("ip=10.51.210.17&fecha="+fecha+"&idapl=666&uri=http://10.50.109.47:8080/checklist/servicios/bitacora.json")).replace("\n", ""));

		/*
		//System.out.println("");

		//System.out.println("servicio/token.json?"+(new UtilCryptoGS().encryptParams("ip=10.51.210.17&fecha="+fecha+"&idapl=666&uri=http://10.50.109.47:8080/checklist/servicios/respuesta.json")).replace("\n", ""));
		//System.out.println("");
		//System.out.println(new UtilCryptoGS().encryptParams("id=191312"));
		//System.out.println("");
         */
        ////System.out.println(new UtilCryptoGS().decryptParams("gRorBWziiDuolAeP/sNI8hoPDN8tmS9tBv6wjlYT6DY="));
        ////System.out.println(new UtilCryptoGS().encryptParams("{Ã¢â‚¬Å“respuestasÃ¢â‚¬ï¿½:{Ã¢â‚¬Å“idCheckUsuaÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�,Ã¢â‚¬ï¿½resPregsÃ¢â‚¬ï¿½:[{Ã¢â‚¬Å“idResPregÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�,Ã¢â‚¬ï¿½compromisoÃ¢â‚¬ï¿½:{Ã¢â‚¬Å“descripcionÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�,Ã¢â‚¬ï¿½fechaÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�},Ã¢â‚¬ï¿½rutaEvidenciaÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€� },{Ã¢â‚¬Å“idResPregÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�,Ã¢â‚¬ï¿½compromisoÃ¢â‚¬ï¿½:{Ã¢â‚¬Å“descripcionÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�,Ã¢â‚¬ï¿½fechaÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�},Ã¢â‚¬ï¿½rutaEvidenciaÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�},{Ã¢â‚¬Å“idResPregÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�,Ã¢â‚¬ï¿½compromisoÃ¢â‚¬ï¿½:{Ã¢â‚¬Å“descripcionÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�,Ã¢â‚¬ï¿½fechaÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�},Ã¢â‚¬ï¿½rutaEvidenciaÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�},{Ã¢â‚¬Å“idResPregÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�,Ã¢â‚¬ï¿½compromisoÃ¢â‚¬ï¿½:{Ã¢â‚¬Å“descripcionÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�,Ã¢â‚¬ï¿½fechaÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�},Ã¢â‚¬ï¿½rutaEvidenciaÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�}],Ã¢â‚¬ï¿½bitacoraÃ¢â‚¬ï¿½:{Ã¢â‚¬Å“idÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�,Ã¢â‚¬ï¿½latitudÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�,Ã¢â‚¬ï¿½longitudÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�,Ã¢â‚¬ï¿½fechaFinÃ¢â‚¬ï¿½:Ã¢â‚¬â€�Ã¢â‚¬â€�}}}"));
        /*

		//System.out.println("");
		//System.out.println("");


		URL url2 = new URL("http://10.51.210.239:8080/checklist/servicio/token.json?idapl=666&"+(new UtilCryptoGS().encryptParams("ip=10.51.219.216&fecha="+fecha+"&uri=http://10.51.210.239:8080/checklist/servicios/checklistOffline")).replace("\n", ""));
		BufferedReader in2 = null;
		try {
			  in2 = new BufferedReader(new InputStreamReader(url2.openStream()));
			} catch(Throwable t){}

		String Text2 = (((in2.readLine().split(",")[0]).split(":")[1]).replace('"', ' ')).replace(" ", "");

		//System.out.println("http://10.51.210.239:8080/checklist/servicios/checklistOffline.json?"+new UtilCryptoGS().encryptParams("id=191312")+"&token="+Text2);

		//System.out.println("");
		//System.out.println("");


		@SuppressWarnings("static-access")
		URL url21 = new URL("http://10.51.210.239:8080/checklist/servicio/token.json?idapl=7&"+(new StrCipher().encrypt("ip=10.51.219.216&fecha="+fecha+"&uri=http://10.51.210.239:8080/checklist/servicios/checklistOffline",FRQConstantes.getLlaveEncripcionLocalIOS())));
		BufferedReader in21 = null;
		try {
			  in21 = new BufferedReader(new InputStreamReader(url21.openStream()));
			} catch(Throwable t){}

		String Text21 = (((in21.readLine().split(",")[0]).split(":")[1]).replace('"', ' ')).replace(" ", "");

		//System.out.println("http://10.51.210.239:8080/checklist/servicios/checklistOffline.json?"+new StrCipher().encrypt("id=191312",FRQConstantes.getLlaveEncripcionLocalIOS())+"&token="+Text21);

         */
        //System.out.println(FRQConstantes.LLAVE_ENCRIPCION_LOCAL.length());
//        System.out.println(new UtilCryptoGS().decrypt("5X3k0JYErPxGNw8GrtTRLx+eDYf66Wc5gjqYOHEBwjnAPaTTGOkOOEk3/83ByGgSNABZYwwkcQ66DRHeoN6B/3o1hI4dfv4pxUOv2ueC0zJsGPXHFlVSnUDeUzTzXO28zcNDDRLmbl0qZM5ugkXN0A==", "fce099383332d9ffa34c4f8a50dda9a0="));
//ip=10.89.85.163&fecha=01102021124000&uri=http://10.89.85.163:8080/checklist/servicios/postAltaFirmaCheck.json
        System.out.println(new UtilCryptoGS().encryptParams("ip=10.89.85.163&fecha=07102021221900&uri=http://10.89.85.163:8080/checklist/servicios/postAltaFirmaCheck.json"));
        System.out.println(new UtilCryptoGS().encryptParams("{\"USERNAME\":\"BazEktWFauto\",\"MACADDRS\":\"1C:B7:96:67:F4:39\",\"CLIENTID\":\"ff3b44f0562942a09119ef8fa37075a7\",\"CALLEDID\":\"EKT-100\",\"FECHA\":\"2021-06-15 17:04:32\"}"));

        System.out.println(new UtilCryptoGS().encryptParams("username"));
        System.out.println(new UtilCryptoGS().encryptParams("BazEktWFauto"));
        System.out.println(new UtilCryptoGS().encryptParams("\"username\": \"BazEktWFauto"));

        System.out.println(new UtilCryptoGS().encryptParams("macaddr"));
        System.out.println(new UtilCryptoGS().encryptParams("1C:B7:96:67:F4:39"));
        System.out.println(new UtilCryptoGS().encryptParams("\"macaddr\": \"1C:B7:96:67:F4:39\""));

        System.out.println(new UtilCryptoGS().encryptParams("clientid"));
        System.out.println(new UtilCryptoGS().encryptParams("ff3b44f0562942a09119ef8fa37075a7"));
        System.out.println(new UtilCryptoGS().encryptParams("\"clientid\": \"ff3b44f0562942a09119ef8fa37075a7\""));

        System.out.println(new UtilCryptoGS().encryptParams("calledid"));
        System.out.println(new UtilCryptoGS().encryptParams("EKT-Maqueta"));
        System.out.println(new UtilCryptoGS().encryptParams("\"calledid\": \"EKT-Maqueta\""));

        System.out.println(new UtilCryptoGS().encryptParams("fechaevt"));
        System.out.println(new UtilCryptoGS().encryptParams("2021-06-15 17:04:32"));
        System.out.println(new UtilCryptoGS().encryptParams("\"fechaevt\": \"2021-06-15 17:04:32\""));

        System.out.println(new UtilCryptoGS().encryptParams("idUsuario=189871&tipoServicio=5"));

    }
}
