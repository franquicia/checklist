package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.AdmInformProtocolosDTO;

public interface AdmInformProtocolosDAO {

	
	public List<AdmInformProtocolosDTO> obtieneTodos() throws Exception;

	public boolean insertaInformProtocolos(AdmInformProtocolosDTO bean) throws Exception;

	public boolean actualizaInformProtocolos(AdmInformProtocolosDTO bean) throws Exception;
	
	public boolean eliminaInformProtocolos(AdmInformProtocolosDTO bean) throws Exception;

}
