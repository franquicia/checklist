package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ReporteDAO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.CompromisoCecoDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.domain.ReporteDTO;
import com.gruposalinas.checklist.domain.VistaCDTO;

public class ReporteRegBI {

	private static final Logger logger = LogManager.getLogger(ReporteRegBI.class);

	@Autowired
	ReporteDAO reporteDAO;

	List<ReporteDTO> listaReporte = null;
	List<CompromisoDTO> listaCompromisos = null;
	List<VistaCDTO> listaCeco = null;
	List<CompromisoCecoDTO> listaCeco1 = null;
	Map<String, Object> mapReporteGeneral = null;
	Map<String, Object> listaReporteReg = null;
	Map<String, Object>listaReporteVistaC = null;

	public List<ReporteDTO> buscaReporte(String idCheck, String idUsuario, String nombreUsuario, String nombreSucursal,
			String fechaRespuesta, String nombreCheck, String metodoCheck) throws Exception {

		if (idCheck.equals("NULL"))
			idCheck = null;
		if (idUsuario.equals("NULL"))
			idUsuario = null;
		if (nombreUsuario.equals("NULL"))
			nombreUsuario = null;
		if (nombreSucursal.equals("NULL"))
			nombreSucursal = null;
		if (fechaRespuesta.equals("NULL"))
			fechaRespuesta = null;
		if (nombreCheck.equals("NULL"))
			nombreCheck = null;
		if (metodoCheck.equals("NULL"))
			metodoCheck = null;

		listaReporte = reporteDAO.buscaReporte(idCheck, idUsuario, nombreUsuario, nombreSucursal, fechaRespuesta,
				nombreCheck, metodoCheck);

		return listaReporte;
	}

	public Map<String, Object> buscaReporteURL(String idCategoria, String idTipo, String idPuesto, String idUsuario,
			String idCeco, String idSucursal, int idNivel, String fecha, String idPais) throws Exception {

		Map<String, Object> mapReporte = null;
		List<ReporteDTO> listaReporte = null;
		List<ReporteDTO> listaAux = new ArrayList<ReporteDTO>();

		listaReporte = reporteDAO.buscaReporteURL(idCategoria, idTipo, idPuesto, idUsuario, idCeco, idSucursal, idNivel,
				fecha, idPais);

		if (!listaReporte.isEmpty()) {
			// //System.out.println("listaReporte " + listaReporte);
			mapReporte = new HashMap<String, Object>();
			for (int i = 0; i < listaReporte.size(); i++) {
				if (i + 1 == listaReporte.size()) {
					listaAux.add(listaReporte.get(i));
					mapReporte.put("" + listaReporte.get(i).getNombreGeografia(), listaAux);
				} else {
					if (listaReporte.get(i).getIdGeografia() != listaReporte.get(i + 1).getIdGeografia()) {
						listaAux.add(listaReporte.get(i));
						mapReporte.put("" + listaReporte.get(i).getNombreGeografia(), listaAux);
						listaAux = new ArrayList<ReporteDTO>();
					} else {
						listaAux.add(listaReporte.get(i));
					}
				}
			}
			// //System.out.println("mapReporte despues del for " + mapReporte);
			if (mapReporte.isEmpty() && (!listaReporte.isEmpty())) {
				mapReporte.put("" + listaReporte.get(0).getNombreGeografia(), listaReporte);
			}
		} else {
			mapReporte = null;
			//logger.info("Saliendo del BI " + mapReporte);
		}
		// logger.info("Saliendo del BI " + mapReporte);
		return mapReporte;
	}

	public List<ReporteDTO> ComparaImagen(int idRuta, int idPregunta, int idCheckUsuario, String fecha)
			throws Exception {
		try {
			listaReporte = reporteDAO.ComparaImagen(idRuta, idPregunta, idCheckUsuario, fecha);
		} catch (Exception e) {
			logger.info("No fue posible consultar las Imagenes" + e);
		}

		return listaReporte;
	}

	public List<ReporteDTO> ReporteRegional(String fechaIni, String fechaFin) throws Exception {
		try {
			listaReporte = reporteDAO.ReporteRegional(fechaIni, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporte;
	}

	public Map<String, Object> ReporteGeneral(int idChecklist, String fechaInicio, String fechaFin) throws Exception {
		try {
			mapReporteGeneral = reporteDAO.ReporteGeneral(idChecklist, fechaInicio, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return mapReporteGeneral;
	}

	public Map<String, Object> ReporteGeneralCompromisos(int idChecklist, String fechaInicio, String fechaFin)
			throws Exception {
		try {
			mapReporteGeneral = reporteDAO.ReporteGeneralCompromisos(idChecklist, fechaInicio, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return mapReporteGeneral;
	}

	public List<ReporteDTO> ReporteCheckUsua(String idCheck, String idPuesto) throws Exception {
		try {
			listaReporte = reporteDAO.ReporteCheckUsua(idCheck, idPuesto);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporte;
	}

	public List<ReporteDTO> ReporteSupReg(String nivelTerritorio, String nivelZona, String nivelRegion, String region,
			String idCecoSup, String pais, String negocio, String fechaI, String fechaF, String idChecklist)
			throws Exception {
		try {
			listaReporte = reporteDAO.ReporteSupReg(nivelTerritorio, nivelZona, nivelRegion, region, idCecoSup, pais,
					negocio, fechaI, fechaF, idChecklist);
			logger.info("lista de reporte... " + listaReporte);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporte;
	}

	public Map<String, Object> ReporteDetSupReg(int idChecklist, String idCeco, String pais, String negocio,
			String fechaI, String fechaF) throws Exception {
		try {
			listaReporteReg = reporteDAO.ReporteDetSupReg(idChecklist, idCeco, pais, negocio, fechaI, fechaF);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporteReg;
	}

	public Map<String, Object> ReporteDetRegional(int idBitacora) throws Exception {
		try {
			listaReporteReg = reporteDAO.ReporteDetRegional(idBitacora);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporteReg;
	}

	public Map<String, Object> ReporteDetVC(int idCeco, int idChecklist, int idUsuario, String fecha) throws Exception {
		try {
			listaReporteReg = reporteDAO.ReporteDetVC(idCeco, idChecklist, idUsuario, fecha);
		} catch (Exception e) {
			listaReporteReg = null;
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporteReg;
	}

	public Map<String, Object> ReporteDetSupReg_versBorra(int idChecklist, String idCeco, String pais, String negocio,
			String fechaI, String fechaF) throws Exception {
		try {
			listaReporteReg = reporteDAO.ReporteDetSupReg_versBorra(idChecklist, idCeco, pais, negocio, fechaI, fechaF);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporteReg;
	}

	public Map<String, Object> ReporteOperacion(int idChecklist, String fechaInicio, String fechaFin, String pais,
			String negocio, String territorio, String zona, String region, String ceco) throws Exception {
		Map<String, Object> listaReporteOpe = null;
		try {
			listaReporteOpe = reporteDAO.ReporteOperacion(idChecklist, fechaInicio, fechaFin, pais, negocio, territorio,
					zona, region, ceco);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporteOpe;

	}

	public List<ReporteDTO> ReporteRegSup(int idCheck, int idPregunta, String fechaI, String fechaFin, String pais,
			String negocio, String nivelRegion, String idCeco) throws Exception {
		try {
			listaReporte = reporteDAO.ReporteRegSup(idCheck, idPregunta, fechaI, fechaFin, pais, negocio, nivelRegion,
					idCeco);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporte;

	}

	public List<CompromisoDTO> ReporteDetPreguntasRegional(int idBitacora) throws Exception {
		try {
			listaCompromisos = reporteDAO.ReporteDetPreguntasRegional(idBitacora);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaCompromisos;

	}

	public List<ReporteDTO> ReporteDetReg(int idCheck, int idCeco, String fechaIni, String fechaFin) throws Exception {
		try {
			listaReporte = reporteDAO.ReporteDetReg(idCheck, idCeco, fechaIni, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaReporte;

	}

	public List<VistaCDTO> ReporteVistaC(int idUsuario, int idCeco, int idChecklist, int pais, int canal, String fecha,
			int bandera) throws Exception {
		try {
			listaCeco = reporteDAO.ReporteVistaC(idUsuario, idCeco, idChecklist, pais, canal, fecha, bandera);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaCeco;

	}


	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public Map<String, Object> ReporteVistaCumplimiento(int idUsuario, String idCeco, int pais, int canal, String fecha,
			int bandera) throws Exception {
		Map<String, Object> mapaCheck = null;
		try {
			mapaCheck = new HashMap<String, Object>();
			List<VistaCDTO> listaReporteCum = new ArrayList<VistaCDTO>();
			List<VistaCDTO> listaReporteCumOrdenada = new ArrayList<VistaCDTO>();
			List<VistaCDTO> listaReporteCumOrdenadaAux = null;
			List<List> listaAuxReporteCum = new ArrayList<List>();
			List<List> listaAuxReporteCumOrdenada = new ArrayList<List>();
			
			List<ChecklistDTO> listaChecklist = null;
			List<ChecklistDTO> listaChecklist1 = null;
			List<ChecklistDTO> listaChecklist2 = null;
			
			Map<String, Object> listaReporteVistaC1 = null;
			Map<String, Object> listaReporteVistaC2 = null;
			Map<String, Object> listaReporteVistaC3 = null;


			//logger.info("****************************************** EMPEZAMOS ******************************************");
			
			logger.info("listaReporteVistaC " + idUsuario + "   " + pais + "   " + canal + "   " + fecha + "   " + bandera);
			listaReporteVistaC1 = reporteDAO.ReporteVistaCumplimiento(idUsuario, "236737", pais, canal, fecha);
			listaReporteVistaC2 = reporteDAO.ReporteVistaCumplimiento(idUsuario, "236736", pais, canal, fecha);
			listaReporteVistaC3 = reporteDAO.ReporteVistaCumplimiento(idUsuario, "236738", pais, canal, fecha);
  
			
			/*logger.info("listaReporteVistaC " + listaReporteVistaC1);
			logger.info("listaReporteVistaC " + listaReporteVistaC2);
			logger.info("listaReporteVistaC " + listaReporteVistaC3);
			
			logger.info("****************************************** TERMINAMOS ******************************************");*/


			if (listaReporteVistaC1 != null) {
				/*1*/
				int porcentaje = (Integer) listaReporteVistaC1.get("porcentaje");
				int ceco = (Integer) listaReporteVistaC1.get("ceco");
				listaChecklist = (List<ChecklistDTO>) listaReporteVistaC1.get("listaCheck");
				int conteo = (Integer) listaReporteVistaC1.get("conteo");
				
				/*2*/
				int porcentaje1 = (Integer) listaReporteVistaC2.get("porcentaje");
				int ceco1 = (Integer) listaReporteVistaC2.get("ceco");
				listaChecklist1 = (List<ChecklistDTO>) listaReporteVistaC2.get("listaCheck");
				int conteo1 = (Integer) listaReporteVistaC2.get("conteo");
				
				/*3*/
				int porcentaje2 = (Integer) listaReporteVistaC3.get("porcentaje");
				int ceco2 = (Integer) listaReporteVistaC3.get("ceco");
				listaChecklist2 = (List<ChecklistDTO>) listaReporteVistaC3.get("listaCheck");
				int conteo2 = (Integer) listaReporteVistaC3.get("conteo");

				logger.info("ceco " + ceco);
				if (!listaChecklist.isEmpty()) {
					
					/*1*/
					//logger.info("listaChecklist "+listaChecklist);
					for (ChecklistDTO check : listaChecklist) {
						logger.info("check.getIdChecklist() 01 " + check.getIdChecklist());
						if(check.getIdChecklist() == 97 || check.getIdChecklist() == 99){
							List<VistaCDTO> lista = reporteDAO.ReporteVistaC(idUsuario, ceco, check.getIdChecklist(), pais, canal, fecha, bandera);
							listaAuxReporteCum.add(lista);
							logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  lista 01 "+lista);
						}
						
					}
					
					/*2*/
					//logger.info("listaChecklist1 "+listaChecklist1);
					for (ChecklistDTO check : listaChecklist1) {
						logger.info("check.getIdChecklist() " + check.getIdChecklist());
						if(check.getIdChecklist() == 97 || check.getIdChecklist() == 99 ){
							List<VistaCDTO> lista = reporteDAO.ReporteVistaC(idUsuario, ceco1, check.getIdChecklist(), pais, canal, fecha, bandera);
							listaAuxReporteCum.add(lista);
							logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  lista 02 "+lista);
						}
					}
					
					/*3*/
					//logger.info("listaChecklist2 "+listaChecklist2);
					//logger.info("listaChecklist2.size() "+listaChecklist2.size());
					for (ChecklistDTO check : listaChecklist2) {
						logger.info("check.getIdChecklist() " + check.getIdChecklist());
						if(check.getIdChecklist() == 97 || check.getIdChecklist() == 99){
							List<VistaCDTO> lista = reporteDAO.ReporteVistaC(idUsuario, ceco2, check.getIdChecklist(), pais, canal, fecha, bandera);
							listaAuxReporteCum.add(lista);
							logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  lista 03 "+lista);
						}
					}

					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCum.size() "+listaAuxReporteCum.size());
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCum "+listaAuxReporteCum);
					
					if (listaAuxReporteCum.size() > 0) {
						for (int i = 0; i < listaAuxReporteCum.get(i).size(); i++) {
							listaReporteCumOrdenadaAux = new ArrayList<VistaCDTO>();
							for (int j = 0; j < listaAuxReporteCum.size(); j++) {
								
								//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  i: "+i + "  , j: " + j);
								
								VistaCDTO vista= new VistaCDTO();
								vista=(VistaCDTO) listaAuxReporteCum.get(j).get(i);
								  
								listaReporteCum.add(vista);
								listaReporteCumOrdenadaAux.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
								
							}
							listaAuxReporteCumOrdenada.add(listaReporteCumOrdenadaAux);
						}
					}
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCumOrdenada.size(): "+listaAuxReporteCumOrdenada.size());
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCumOrdenada: "+listaAuxReporteCumOrdenada);
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCumOrdenada.get(0).size(): "+listaAuxReporteCumOrdenada.get(0).size());
					
					ArrayList<List> auxGeneral = new ArrayList<List>();
					ArrayList<VistaCDTO> auxInterno = null;
					int q = 0;
					int r = 0;
					int s = 1;
					while (r < listaAuxReporteCumOrdenada.size()){
						List <VistaCDTO> l = listaAuxReporteCumOrdenada.get(q);

							Iterator it = l.iterator();
							while(it.hasNext()){
								if (s==1){
									auxInterno = new ArrayList<VistaCDTO>();
								}
								VistaCDTO ob = new VistaCDTO();
								ob = (VistaCDTO)it.next();
								if(auxInterno!=null){
									auxInterno.add(ob);
								}	
								if (s == 2){
									auxGeneral.add(auxInterno);
									//auxInterno.clear();
									s=0;
								}
								s++;
							}
						r++;
						q++;
					}
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  auxGeneral.size(): "+auxGeneral.size());
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  auxGeneral: "+auxGeneral);
					
					if(listaAuxReporteCum.get(0).size()>1)
						listaReporteCumOrdenada = ordenaCaja(auxGeneral.size(), auxGeneral,conteo);
					else
						listaReporteCumOrdenada=listaReporteCum;
					
					mapaCheck.put("listaChecklist", listaChecklist);
					mapaCheck.put("listaSucursales", listaReporteCumOrdenada);
					mapaCheck.put("porcentaje", porcentaje);
					mapaCheck.put("conteo", conteo);
				} else {
					mapaCheck = null;
				}
			}
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
			mapaCheck = null;
		}
		return mapaCheck;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public Map<String, Object> ReporteVistaCumplimientoDos(int idUsuario, String idCeco, int pais, int canal, String fecha,
			int bandera) throws Exception {
		Map<String, Object> mapaCheck = null;
		try {
			mapaCheck = new HashMap<String, Object>();
			List<VistaCDTO> listaReporteCum = new ArrayList<VistaCDTO>();
			List<VistaCDTO> listaReporteCumOrdenada = new ArrayList<VistaCDTO>();
			List<VistaCDTO> listaReporteCumOrdenadaAux = null;
			List<List> listaAuxReporteCum = new ArrayList<List>();
			List<List> listaAuxReporteCumOrdenada = new ArrayList<List>();
			
			List<ChecklistDTO> listaChecklist = null;
			List<ChecklistDTO> listaChecklist1 = null;
			List<ChecklistDTO> listaChecklist2 = null;
			
			Map<String, Object> listaReporteVistaC1 = null;
			Map<String, Object> listaReporteVistaC2 = null;
			Map<String, Object> listaReporteVistaC3 = null;


			//logger.info("****************************************** EMPEZAMOS ******************************************");
			
			logger.info("listaReporteVistaC " + idUsuario + "   " + pais + "   " + canal + "   " + fecha + "   " + bandera);
			listaReporteVistaC1 = reporteDAO.ReporteVistaCumplimiento(idUsuario, "236737", pais, canal, fecha);
			listaReporteVistaC2 = reporteDAO.ReporteVistaCumplimiento(idUsuario, "236736", pais, canal, fecha);
			listaReporteVistaC3 = reporteDAO.ReporteVistaCumplimiento(idUsuario, "236738", pais, canal, fecha);
  
			
			/*logger.info("listaReporteVistaC " + listaReporteVistaC1);
			logger.info("listaReporteVistaC " + listaReporteVistaC2);
			logger.info("listaReporteVistaC " + listaReporteVistaC3);
			
			logger.info("****************************************** TERMINAMOS ******************************************");*/


			if (listaReporteVistaC1 != null) {
				/*1*/
				int porcentaje = (Integer) listaReporteVistaC1.get("porcentaje");
				int ceco = (Integer) listaReporteVistaC1.get("ceco");
				listaChecklist = (List<ChecklistDTO>) listaReporteVistaC1.get("listaCheck");
				int conteo = (Integer) listaReporteVistaC1.get("conteo");
				
				/*2*/
				int porcentaje1 = (Integer) listaReporteVistaC2.get("porcentaje");
				int ceco1 = (Integer) listaReporteVistaC2.get("ceco");
				listaChecklist1 = (List<ChecklistDTO>) listaReporteVistaC2.get("listaCheck");
				int conteo1 = (Integer) listaReporteVistaC2.get("conteo");
				
				/*3*/
				int porcentaje2 = (Integer) listaReporteVistaC3.get("porcentaje");
				int ceco2 = (Integer) listaReporteVistaC3.get("ceco");
				listaChecklist2 = (List<ChecklistDTO>) listaReporteVistaC3.get("listaCheck");
				int conteo2 = (Integer) listaReporteVistaC3.get("conteo");

				logger.info("ceco " + ceco);
				if (!listaChecklist.isEmpty()) {
					
					/*1*/
					//logger.info("listaChecklist "+listaChecklist);
					for (ChecklistDTO check : listaChecklist) {
						logger.info("check.getIdChecklist() 01 " + check.getIdChecklist());
						if(check.getIdChecklist() == 97 || check.getIdChecklist() == 99){
							List<VistaCDTO> lista = reporteDAO.ReporteVistaC(idUsuario, ceco, check.getIdChecklist(), pais, canal, fecha, bandera);
							listaAuxReporteCum.add(lista);
							logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  lista 01 "+lista);
						}
					}
					
					/*2*/
					//logger.info("listaChecklist1 "+listaChecklist1);
					for (ChecklistDTO check : listaChecklist1) {
						logger.info("check.getIdChecklist() " + check.getIdChecklist());
						if(check.getIdChecklist() == 97 || check.getIdChecklist() == 99){
							List<VistaCDTO> lista = reporteDAO.ReporteVistaC(idUsuario, ceco1, check.getIdChecklist(), pais, canal, fecha, bandera);
							listaAuxReporteCum.add(lista);
							logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  lista 02 "+lista);
						}
					}
					
					/*3*/
					//logger.info("listaChecklist2 "+listaChecklist2);
					//logger.info("listaChecklist2.size() "+listaChecklist2.size());
					for (ChecklistDTO check : listaChecklist2) {
						logger.info("check.getIdChecklist() " + check.getIdChecklist());
						if(check.getIdChecklist() == 97 || check.getIdChecklist() == 99 ){
							List<VistaCDTO> lista = reporteDAO.ReporteVistaC(idUsuario, ceco2, check.getIdChecklist(), pais, canal, fecha, bandera);
							listaAuxReporteCum.add(lista);
							logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  lista 03 "+lista);
						}
					}

					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCum.size() "+listaAuxReporteCum.size());
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCum "+listaAuxReporteCum);
					
					if (listaAuxReporteCum.size() > 0) {
						for (int i = 0; i < listaAuxReporteCum.get(i).size(); i++) {
							listaReporteCumOrdenadaAux = new ArrayList<VistaCDTO>();
							for (int j = 0; j < listaAuxReporteCum.size(); j++) {
								
								//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  i: "+i + "  , j: " + j);
								
								VistaCDTO vista= new VistaCDTO();
								vista=(VistaCDTO) listaAuxReporteCum.get(j).get(i);
								  
								listaReporteCum.add(vista);
								listaReporteCumOrdenadaAux.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
								
							}
							listaAuxReporteCumOrdenada.add(listaReporteCumOrdenadaAux);
						}
					}
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCumOrdenada.size(): "+listaAuxReporteCumOrdenada.size());
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCumOrdenada: "+listaAuxReporteCumOrdenada);
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCumOrdenada.get(0).size(): "+listaAuxReporteCumOrdenada.get(0).size());
					
					ArrayList<List> auxGeneral = new ArrayList<List>();
					ArrayList<VistaCDTO> auxInterno = null;
					int q = 0;
					int r = 0;
					int s = 1;
					while (r < listaAuxReporteCumOrdenada.size()){
						List <VistaCDTO> l = listaAuxReporteCumOrdenada.get(q);

							Iterator it = l.iterator();
							while(it.hasNext()){
								if (s==1){
									auxInterno = new ArrayList<VistaCDTO>();
								}
								VistaCDTO ob = new VistaCDTO();
								ob = (VistaCDTO)it.next();
								auxInterno.add(ob);
									
								if (s == 3){
									auxGeneral.add(auxInterno);
									//auxInterno.clear();
									s=0;
								}
								s++;
							}
						r++;
						q++;
					}
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  auxGeneral.size(): "+auxGeneral.size());
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  auxGeneral: "+auxGeneral);
					
					if(listaAuxReporteCum.get(0).size()>1)
						listaReporteCumOrdenada = ordenaCaja(auxGeneral.size(), auxGeneral,conteo);
					else
						listaReporteCumOrdenada=listaReporteCum;
					
					mapaCheck.put("listaChecklist", listaChecklist);
					mapaCheck.put("listaSucursales", listaReporteCumOrdenada);
					mapaCheck.put("porcentaje", porcentaje);
					mapaCheck.put("conteo", conteo);
				} else {
					mapaCheck = null;
				}
			}
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
			mapaCheck = null;
		}
		return mapaCheck;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public Map<String, Object> ReporteVistaCumplimientoTres(int idUsuario, String idCeco, int pais, int canal, String fecha,
			int bandera) throws Exception {
		Map<String, Object> mapaCheck = null;
		try {
			mapaCheck = new HashMap<String, Object>();
			List<VistaCDTO> listaReporteCum = new ArrayList<VistaCDTO>();
			List<VistaCDTO> listaReporteCumOrdenada = new ArrayList<VistaCDTO>();
			List<VistaCDTO> listaReporteCumOrdenadaAux = null;
			List<List> listaAuxReporteCum = new ArrayList<List>();
			List<List> listaAuxReporteCumOrdenada = new ArrayList<List>();
			
			List<ChecklistDTO> listaChecklist = null;
			List<ChecklistDTO> listaChecklist1 = null;
			List<ChecklistDTO> listaChecklist2 = null;
			
			Map<String, Object> listaReporteVistaC1 = null;
			Map<String, Object> listaReporteVistaC2 = null;
			Map<String, Object> listaReporteVistaC3 = null;


			logger.info("****************************************** EMPEZAMOS ******************************************");
			
			logger.info("listaReporteVistaC " + idUsuario + "   " + pais + "   " + canal + "   " + fecha + "   " + bandera);
			listaReporteVistaC1 = reporteDAO.ReporteVistaCumplimiento(idUsuario, "236737", pais, canal, fecha);
			listaReporteVistaC2 = reporteDAO.ReporteVistaCumplimiento(idUsuario, "236736", pais, canal, fecha);
			listaReporteVistaC3 = reporteDAO.ReporteVistaCumplimiento(idUsuario, "236738", pais, canal, fecha);
  
			logger.info("listaReporteVistaC1 " + listaReporteVistaC1);
			logger.info("listaReporteVistaC2 " + listaReporteVistaC2);
			logger.info("listaReporteVistaC3 " + listaReporteVistaC3);
			
			logger.info("****************************************** TERMINAMOS ******************************************");


			if (listaReporteVistaC1 != null) {
				/*1*/
				int porcentaje = (Integer) listaReporteVistaC1.get("porcentaje");
				int ceco = (Integer) listaReporteVistaC1.get("ceco");
				listaChecklist = (List<ChecklistDTO>) listaReporteVistaC1.get("listaCheck");
				int conteo = (Integer) listaReporteVistaC1.get("conteo");
				
				/*2*/
				int porcentaje1 = (Integer) listaReporteVistaC2.get("porcentaje");
				int ceco1 = (Integer) listaReporteVistaC2.get("ceco");
				listaChecklist1 = (List<ChecklistDTO>) listaReporteVistaC2.get("listaCheck");
				int conteo1 = (Integer) listaReporteVistaC2.get("conteo");
				
				/*3*/
				int porcentaje2 = (Integer) listaReporteVistaC3.get("porcentaje");
				int ceco2 = (Integer) listaReporteVistaC3.get("ceco");
				listaChecklist2 = (List<ChecklistDTO>) listaReporteVistaC3.get("listaCheck");
				int conteo2 = (Integer) listaReporteVistaC3.get("conteo");

				
				logger.info("ceco " + ceco);
				if (!listaChecklist.isEmpty()) {
					
					try {
						/*1*/
						//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*listaChecklist "+listaChecklist);
						//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*listaChecklist.size() "+listaChecklist.size());
						for (ChecklistDTO check : listaChecklist) {
							//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*check.getIdChecklist() 01 " + check.getIdChecklist());
							if(check.getIdChecklist() == 97 || check.getIdChecklist() == 99 || check.getIdChecklist() == 156){
								List<VistaCDTO> lista = reporteDAO.ReporteVistaC(idUsuario, ceco, check.getIdChecklist(), pais, canal, fecha, bandera);
								listaAuxReporteCum.add(lista);
								//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  lista 01 "+lista.size());
								//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  lista 01 "+lista);
							}
						}
						
						/*2*/
						//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*listaChecklist1 "+listaChecklist1);
						//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*listaChecklist1.size() "+listaChecklist1.size());
						for (ChecklistDTO check : listaChecklist1) {
							//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*check.getIdChecklist() " + check.getIdChecklist());
							if(check.getIdChecklist() == 97 || check.getIdChecklist() == 99 || check.getIdChecklist() == 156){
								List<VistaCDTO> lista = reporteDAO.ReporteVistaC(idUsuario, ceco1, check.getIdChecklist(), pais, canal, fecha, bandera);
								listaAuxReporteCum.add(lista);
								//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  lista 02 "+lista.size());
								//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  lista 02 "+lista);
							}
						}
						
						/*3*/
						//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*listaChecklist2 "+listaChecklist2);
						//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*listaChecklist2.size() "+listaChecklist2.size());
						for (ChecklistDTO check : listaChecklist2) {
							//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*check.getIdChecklist() " + check.getIdChecklist());
							if(check.getIdChecklist() == 97 || check.getIdChecklist() == 99 || check.getIdChecklist() == 156){
								List<VistaCDTO> lista = reporteDAO.ReporteVistaC(idUsuario, ceco2, check.getIdChecklist(), pais, canal, fecha, bandera);
								listaAuxReporteCum.add(lista);
								//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  lista 03 "+lista.size());
								//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  lista 03 "+lista);
							}
						}
	
					} catch (Exception e) {
						logger.info("No fue posible consultar la información" + e);
						mapaCheck = null;
						return mapaCheck;
					}
					logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCum.size() "+listaAuxReporteCum.size());
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCum "+listaAuxReporteCum);
					
					if (listaAuxReporteCum.size() > 0) {
						for (int i = 0; i < listaAuxReporteCum.get(i).size(); i++) {
							//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCum.size() "+listaAuxReporteCum.get(i).size());
							//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCum.size() "+listaAuxReporteCum.get(i));
							listaReporteCumOrdenadaAux = new ArrayList<VistaCDTO>();
							//logger.info("Dentro del for");
							for (int j = 0; j < listaAuxReporteCum.size(); j++) {
								
								//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  prueba 01: " + j);
								//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  prueba 01: " + listaAuxReporteCum.size());
								//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  prueba 01: " + listaAuxReporteCum);
								
								if(listaAuxReporteCum.get(i)==null)
									break;
								//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  prueba 02");
								try {

									//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  j: "+ j + "  , i: " + i);
																		
									VistaCDTO vista= new VistaCDTO();
									vista=(VistaCDTO) listaAuxReporteCum.get(j).get(i);
									//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  vista: " + vista);  
									listaReporteCum.add(vista);
									listaReporteCumOrdenadaAux.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
								} catch (Exception e) {
									//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  trono");
									break;
								}
								
								//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  prueba 03");
								
							}
							listaAuxReporteCumOrdenada.add(listaReporteCumOrdenadaAux);
							//logger.info("Sali del for");
						}
					}
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCumOrdenada.size(): "+listaAuxReporteCumOrdenada.size());
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCumOrdenada: "+listaAuxReporteCumOrdenada);
					//logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  listaAuxReporteCumOrdenada.get(0).size(): "+listaAuxReporteCumOrdenada.get(0).size());
					
					ArrayList<List> auxGeneral = new ArrayList<List>();
					ArrayList<VistaCDTO> auxInterno = null;
					int q = 0;
					int r = 0;
					int s = 1;
					while (r < listaAuxReporteCumOrdenada.size()){
						List <VistaCDTO> l = listaAuxReporteCumOrdenada.get(q);

							Iterator it = l.iterator();
							while(it.hasNext()){
								if (s==1){
									auxInterno = new ArrayList<VistaCDTO>();
								}
								VistaCDTO ob = new VistaCDTO();
								ob = (VistaCDTO)it.next();
								auxInterno.add(ob);
									
								if (s == 3){
									auxGeneral.add(auxInterno);
									//auxInterno.clear();
									s=0;
								}
								s++;
							}
						r++;
						q++;
					}
					logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  auxGeneral.size(): "+auxGeneral.size());
					logger.info("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*  auxGeneral: "+auxGeneral);
					
					if(listaAuxReporteCum.get(0).size()>1)
						listaReporteCumOrdenada = ordenaCaja(auxGeneral.size(), auxGeneral,conteo);
					else
						listaReporteCumOrdenada=listaReporteCum;
					
					mapaCheck.put("listaChecklist", listaChecklist);
					mapaCheck.put("listaSucursales", listaReporteCumOrdenada);
					mapaCheck.put("porcentaje", porcentaje);
					mapaCheck.put("conteo", conteo);
				} else {
					mapaCheck = null;
				}
			}
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
			mapaCheck = null;
		}
		return mapaCheck;
	}


	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public Map<String, Object> ReporteVistaCumplimientoSucursal(int idUsuario, String idCeco, int pais, int canal,
			String fecha, int bandera) throws Exception {
		Map<String, Object> mapaCheck = null;
		try {
			mapaCheck = new HashMap<String, Object>();
			List<VistaCDTO> listaReporteCum = new ArrayList<VistaCDTO>();
			List<VistaCDTO> listaReporteCumOrdenada = new ArrayList<VistaCDTO>();
			List<VistaCDTO> listaReporteCumOrdenadaAux = null;
			List<List> listaAuxReporteCum = new ArrayList<List>();
			List<List> listaAuxReporteCumOrdenada = new ArrayList<List>();
			List<ChecklistDTO> listaChecklist = null;

			logger.info("listaReporteVistaC " + idUsuario + "   " + pais + "   " + canal + "   " + fecha);
			listaReporteVistaC = reporteDAO.ReporteVistaCumplimiento(idUsuario, idCeco, pais, canal, fecha);
			logger.info("listaReporteVistaC " + listaReporteVistaC);

			if (listaReporteVistaC != null) {
				int porcentaje = (Integer) listaReporteVistaC.get("porcentaje");
				int ceco = (Integer) listaReporteVistaC.get("ceco");
				listaChecklist = (List<ChecklistDTO>) listaReporteVistaC.get("listaCheck");
				int conteo = (Integer) listaReporteVistaC.get("conteo");
				logger.info("idCeco " + idCeco + "   bandera " + bandera);

				if (!listaChecklist.isEmpty()) {
					for (ChecklistDTO check : listaChecklist) {
						logger.info("check.getIdChecklist() " + check.getIdChecklist());
						listaAuxReporteCum.add(reporteDAO.ReporteVistaC(idUsuario, Integer.parseInt(idCeco),
								check.getIdChecklist(), pais, canal, fecha, bandera));
					}
					if (listaAuxReporteCum.size() > 0) {
						for (int i = 0; i < listaAuxReporteCum.get(0).size(); i++) {
							listaReporteCumOrdenadaAux = new ArrayList<VistaCDTO>();
							for (int j = 0; j < listaAuxReporteCum.size(); j++) {
								//listaReporteCum.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
								listaReporteCum.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
								listaReporteCumOrdenadaAux.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
							}
							listaAuxReporteCumOrdenada.add(listaReporteCumOrdenadaAux);							
						}
						if(listaAuxReporteCum.get(0).size()>1)
							listaReporteCumOrdenada = ordenaCaja(listaAuxReporteCum.get(0).size(), listaAuxReporteCumOrdenada,conteo);
						else
							listaReporteCumOrdenada=listaReporteCum;
					}

				} else {
					listaAuxReporteCum.add(reporteDAO.ReporteVistaC(idUsuario, Integer.parseInt(idCeco), 0, pais, canal,
							fecha, bandera));
					if (listaAuxReporteCum.size() > 0) {
						for (int i = 0; i < listaAuxReporteCum.get(0).size(); i++) {
							for (int j = 0; j < listaAuxReporteCum.size(); j++) {
								//listaReporteCum.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));
								listaReporteCumOrdenada.add((VistaCDTO) listaAuxReporteCum.get(j).get(i));								
							}
						}
					}
				}
				mapaCheck.put("listaChecklist", listaChecklist);
				mapaCheck.put("listaSucursales", listaReporteCumOrdenada);
				mapaCheck.put("porcentaje", porcentaje);
				mapaCheck.put("conteo", conteo);
			}

		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
			mapaCheck = null;
		}

		return mapaCheck;
	}

	public List<CompromisoCecoDTO> CecoInfo(int idUsuario) throws Exception {
		try {
			listaCeco1 = reporteDAO.CecoInfo(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return listaCeco1;

	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<VistaCDTO> ordena(int tam, List<List> listaReporteCum,int conteo) {

		boolean flag = false;
		int i = 1;
		double porcentAnt = 0.0;
		double porcentAct = 0.0;
		List<VistaCDTO> act = null;
		List<VistaCDTO> ant = null;
		List<VistaCDTO> retorno = new ArrayList<VistaCDTO>();
	
		while (i < tam) {
			porcentAnt = getPorcentaje(listaReporteCum.get(i - 1));
			porcentAct = getPorcentaje(listaReporteCum.get(i));
				

			if (porcentAnt < porcentAct) {
				ant = listaReporteCum.get(i - 1);
				act = listaReporteCum.get(i);
				listaReporteCum.set(i - 1, act);
				listaReporteCum.set(i, ant);
				flag = true;
			}
			if (flag && (i + 1) == tam) {
				i = 0;
				flag = false;
			}
			i++;
		}
		
		
		for (int k = 0; k < listaReporteCum.size(); k++) {			
			for (int j = 0; j < listaReporteCum.get(k).size(); j++) {
				retorno.add((VistaCDTO) listaReporteCum.get(k).get(j));
			}
		}
		
		return retorno;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<VistaCDTO> ordenaCaja(int tam, List<List> listaReporteCum,int conteo) {

		boolean flag = false;
		int i = 1;
		double porcentAnt = 0.0;
		double porcentAct = 0.0;
		List<VistaCDTO> act = null;
		List<VistaCDTO> ant = null;
		List<VistaCDTO> retorno = new ArrayList<VistaCDTO>();
	
		while (i < tam) {
			porcentAnt = getPorcentajeCaja(listaReporteCum.get(i - 1));
			porcentAct = getPorcentajeCaja(listaReporteCum.get(i));
				

			if (porcentAnt < porcentAct) {
				ant = listaReporteCum.get(i - 1);
				act = listaReporteCum.get(i);
				listaReporteCum.set(i - 1, act);
				listaReporteCum.set(i, ant);
				flag = true;
			}
			if (flag && (i + 1) == tam) {
				i = 0;
				flag = false;
			}
			i++;
		}
		
		
		for (int k = 0; k < listaReporteCum.size(); k++) {			
			for (int j = 0; j < listaReporteCum.get(k).size(); j++) {
				retorno.add((VistaCDTO) listaReporteCum.get(k).get(j));
			}
		}
		
		return retorno;
	}

	@SuppressWarnings("rawtypes")
	public double getPorcentajeCaja(List<VistaCDTO> lista) {
		double res = 0.0;
		Iterator it = lista.iterator();
		while (it.hasNext()) {
			VistaCDTO obj = (VistaCDTO) it.next();
			if (obj.getIdChecklist().equals("97")){
				res += obj.getTotal();
			}
		}

		return res;
	}
	
	@SuppressWarnings("rawtypes")
	public double getPorcentaje(List<VistaCDTO> lista) {
		double res = 0.0;
		Iterator it = lista.iterator();
		while (it.hasNext()) {
			VistaCDTO obj = (VistaCDTO) it.next();
			res += obj.getTotal();
		}

		return res / lista.size();
	}

}