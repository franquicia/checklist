package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.SoporteExpDTO;
import com.gruposalinas.checklist.mappers.SoporteExpRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class SoporteExpDAOImpl extends DefaultDAO implements SoporteExpDAO {

    private static Logger logger = LogManager.getLogger(SoporteExpDAOImpl.class);

    DefaultJdbcCall jdbcObtieneVersion;
    DefaultJdbcCall jbdcActualizaStatus;
    DefaultJdbcCall jbdcActualizatiposuc;
    DefaultJdbcCall jbdcActualizaArbol;
    DefaultJdbcCall jbdcActualizaPreg;
    DefaultJdbcCall jbdcActualizFechaSoft;

    public void init() {

        jbdcActualizatiposuc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSOPEXP")
                .withProcedureName("SP_ACT_CECOTRAN");

        jdbcObtieneVersion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSOPEXP")
                .withProcedureName("SP_SEL_CECOS")
                .returningResultSet("RCL_CECO", new SoporteExpRowMapper());

        jbdcActualizaStatus = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSOPEXP")
                .withProcedureName("SP_ACT_CECOSTAT");

        jbdcActualizaArbol = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSOPEXP")
                .withProcedureName("SP_ACT_ARBOL");

        jbdcActualizaPreg = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSOPEXP")
                .withProcedureName("SP_ACT_PREG");

        jbdcActualizFechaSoft = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSOPEXP")
                .withProcedureName("SP_ACT_FECHSOFT");

    }

    @SuppressWarnings("unchecked")
    public List<SoporteExpDTO> obtieneDatos(String ceco, String status, String tipoSuc) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<SoporteExpDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_STATUS", status)
                .addValue("PA_TIPOSUC", tipoSuc);

        out = jdbcObtieneVersion.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMSOPEXP.SP_SEL_CECOS}");
        //lleva el nombre del cursor del procedure
        lista = (List<SoporteExpDTO>) out.get("RCL_CECO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los datos del (" + ceco + ")");
        }
        return lista;

    }

    public boolean actualiza(SoporteExpDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_STATUS", bean.getStatus())
                //TIPO DE PRERRECORRIDO
                .addValue("PA_PRERECO", bean.getAux2());

        out = jbdcActualizaStatus.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMSOPEXP.SP_ACT_CECOSTAT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar el ceco  id( " + bean.getCeco() + ")");
        } else {
            return true;
        }
        return false;

    }

    public boolean actualizaTipoSuc(SoporteExpDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_STATUS", bean.getTipoSuc());

        out = jbdcActualizatiposuc.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMSOPEXP.SP_ACT_CECOTRAN}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar el ceco  id( " + bean.getCeco() + ")");
        } else {
            return true;
        }
        return false;

    }

    public boolean actualizaAbol(SoporteExpDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_REQOBS", bean.getReqObserv())
                .addValue("PA_OBSERVOBLI", bean.getBandObser())
                .addValue("PA_PLANTILLA", bean.getPlantilla())
                .addValue("PA_PONDERA", bean.getPonderacion())
                .addValue("PA_ARBOL", bean.getArbol());

        out = jbdcActualizaArbol.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMSOPEXP.SP_ACT_ARBOL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar el arbol  id( " + bean.getArbol() + ")");
        } else {
            return true;
        }
        return false;

    }

    @Override
    public boolean actualizaPreg(SoporteExpDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPREG", bean.getIdPreg())
                .addValue("PA_TIPPREG", bean.getTipoPreg())
                .addValue("PA_DESCR", bean.getDescPreg())
                .addValue("PA_CRITICA", bean.getCritica())
                .addValue("PA_SLA", bean.getSla())
                .addValue("PA_AREA", bean.getArea())
                .addValue("PA_MODULO", bean.getModulo());

        out = jbdcActualizaPreg.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMSOPEXP.SP_ACT_PREG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar la preg  id( " + bean.getIdPreg() + ")");
        } else {
            return true;
        }
        return false;

    }

    public boolean actualizaFechaSoftNvo(SoporteExpDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITACORA", bean.getBitacora());

        out = jbdcActualizFechaSoft.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMSOPEXP.SP_ACT_FECHSOFT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar el ceco  id( " + bean.getBitacora() + ")");
        } else {
            return true;
        }
        return false;

    }

}
