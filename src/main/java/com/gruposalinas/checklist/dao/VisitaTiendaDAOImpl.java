package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.DatosCecoDTO;
import com.gruposalinas.checklist.domain.VisitaTiendaDTO;
import com.gruposalinas.checklist.mappers.DatosCecoRowMapper;
import com.gruposalinas.checklist.mappers.VisitaTiendaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class VisitaTiendaDAOImpl extends DefaultDAO implements VisitaTiendaDAO {

    private Logger logger = LogManager.getLogger(VisitaTiendaDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneVisitas = null;

    public void init() {

        jdbcObtieneVisitas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORVISITAS")
                .withProcedureName("SP_VISITAS")
                .returningResultSet("RCL_VISITAS", new VisitaTiendaRowMapper())
                .returningResultSet("RCL_DATOSCECO", new DatosCecoRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> obtieneVisitas(String idChecklist, String idUsuario, String nuMes, String anio) throws Exception {

        Map<String, Object> out = null;
        Map<String, Object> datosVisitas = new HashMap<String, Object>();

        List<VisitaTiendaDTO> visitas = null;
        List<DatosCecoDTO> datosCeco = null;

        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECKLIST", idChecklist).addValue("PA_USUARIO", idUsuario).addValue("PA_MES", nuMes).addValue("PA_ANIO", anio);

        out = jdbcObtieneVisitas.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAREPORVISITAS.SP_VISITAS}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        visitas = (List<VisitaTiendaDTO>) out.get("RCL_VISITAS");
        datosCeco = (List<DatosCecoDTO>) out.get("RCL_DATOSCECO");

        datosVisitas.put("visitas", visitas);
        datosVisitas.put("datosCeco", datosCeco);

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener las Visitas");
        }

        return datosVisitas;
    }

}
