package com.gruposalinas.checklist.dao;

import java.util.List;
import com.gruposalinas.checklist.domain.RepMedicionDTO;

public interface RepMedicionDAO {

	public List<RepMedicionDTO> ReporteZonas (int idProtocolo, String fechaInicio, String fechaFin) throws Exception;
	public List<RepMedicionDTO> ReportePreguntas (RepMedicionDTO bean) throws Exception;
}
