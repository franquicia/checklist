package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ConteoxPreguntaDTO;
import com.gruposalinas.checklist.domain.FiltrosCecoDTO;
import com.gruposalinas.checklist.domain.HorasRespuestaDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.NumeroTiendasDTO;
import com.gruposalinas.checklist.domain.ReporteImagenesDTO;
import com.gruposalinas.checklist.domain.ReportesConteoDTO;
import com.gruposalinas.checklist.mappers.ConteoxPreguntaRowMapper;
import com.gruposalinas.checklist.mappers.FiltrosCecosRowMapper;
import com.gruposalinas.checklist.mappers.HorasRespuestaRowMapper;
import com.gruposalinas.checklist.mappers.ModuloSingleRowMapper;
import com.gruposalinas.checklist.mappers.NumeroTiendasRowMapper;
import com.gruposalinas.checklist.mappers.ReporteImagenesRowMapper;
import com.gruposalinas.checklist.mappers.ReportesConteoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ReporteImagenesAdmDAOImpl extends DefaultDAO implements ReporteImagenesAdmDAO {

    private Logger logger = LogManager.getLogger(ReporteImagenesAdmDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneModuloTiendas;
    private DefaultJdbcCall jdbcObtieneRespuestas;
    private DefaultJdbcCall jdbcObtienHora;

    private DefaultJdbcCall jdbcObtieneNumSucursales;
    private DefaultJdbcCall jdbcObtieneConteoxPregunta;

    private DefaultJdbcCall jdbcObtieneConteoxModulo;
    private DefaultJdbcCall jdbcObtieneTiendasRegion;
    private DefaultJdbcCall jdbcObtieneConteoxHora;
    private DefaultJdbcCall jdbcObtieneReportesConteo;

    public void init() {

        jdbcObtieneConteoxModulo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOIMGADM")
                .withProcedureName("SP_CONTEORESPUESTAS")
                .returningResultSet("RCL_CONSULTA", new ConteoxPreguntaRowMapper());

        jdbcObtieneTiendasRegion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOIMGADM")
                .withProcedureName("SP_TOTALESREGION")
                .returningResultSet("RCL_CONSULTA", new NumeroTiendasRowMapper());

        jdbcObtieneConteoxHora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOIMGADM")
                .withProcedureName("SP_HORAS")
                .returningResultSet("RCL_CONSULTA", new HorasRespuestaRowMapper());

        jdbcObtieneReportesConteo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOIMGADM")
                .withProcedureName("SP_TOTALREPORTES")
                .returningResultSet("RCL_CONSULTA", new ReportesConteoRowMapper());

        jdbcObtieneModuloTiendas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPO_IMGADM")
                .withProcedureName("SP_TIENDASMOD")
                .returningResultSet("RCL_TIENDAS", new FiltrosCecosRowMapper())
                .returningResultSet("RCL_MODULOS", new ModuloSingleRowMapper());

        jdbcObtieneRespuestas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPO_IMGADM")
                .withProcedureName("SP_OBTIENE_RESP")
                .returningResultSet("RCL_RESPUESTAS", new ReporteImagenesRowMapper());

        jdbcObtienHora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPO_IMGADM")
                .withProcedureName("SP_OBTIENEHORA");

        jdbcObtieneNumSucursales = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOIMGADM")
                .withProcedureName("SP_OBTIENESUC")
                .returningResultSet("RCL_CONSULTA", new NumeroTiendasRowMapper());

        jdbcObtieneConteoxPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOIMGADM")
                .withProcedureName("SP_CONTEOXPREG")
                .returningResultSet("RCL_CONSULTA", new ConteoxPreguntaRowMapper())
                .returningResultSet("RCL_TOTALES", new NumeroTiendasRowMapper())
                .returningResultSet("RCL_HORAS", new HorasRespuestaRowMapper())
                .returningResultSet("RCL_REPORTES", new ReportesConteoRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> obtieneModulosTiendas(String ceco, String checklist) throws Exception {

        Map<String, Object> out = null;
        Map<String, Object> respuesta = new HashMap<String, Object>();
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco).addValue("PA_CHECKLIST", checklist);

        out = jdbcObtieneModuloTiendas.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAREPORTES_IMG.SP_TIENDASMOD}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener los modulos y tiendas");
        }

        List<ModuloDTO> modulosaux = (List<ModuloDTO>) out.get("RCL_MODULOS");
        List<ModuloDTO> modulos = new ArrayList<ModuloDTO>();

        int cont = 0;

        while (cont < modulosaux.size()) {

            ModuloDTO modulAnt = modulosaux.get(cont);

            while (cont < modulosaux.size() && modulAnt.getIdModulo() == modulosaux.get(cont).getIdModulo()) {
                cont++;
            }
            modulos.add(modulAnt);

        }
        respuesta.put("modulos", modulos);
        respuesta.put("tiendas", (List<FiltrosCecoDTO>) out.get("RCL_TIENDAS"));

        return respuesta;
    }

    @SuppressWarnings("unchecked")
    public List<ReporteImagenesDTO> obtieneRespuestas(int idCeco, int idChecklist, String fecha) throws Exception {

        List<ReporteImagenesDTO> respuestas = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", idCeco).addValue("PA_CHECKLIST", idChecklist).addValue("PA_FECHA", fecha);

        out = jdbcObtieneRespuestas.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAREPORTES_IMG.SP_OBTIENE_RESP}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        respuestas = (List<ReporteImagenesDTO>) out.get("RCL_RESPUESTAS");

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener los modulos y tiendas");
        }

        return respuestas;
    }

    @Override
    public String[] primerRespuesta(int idCeco, int idChecklist, String fecha) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        String[] datos = new String[2];

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", idCeco).addValue("PA_CHECKLIST", idChecklist).addValue("PA_FECHA", fecha);

        out = jdbcObtienHora.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAREPORTES_IMG.SP_OBTIENEHORA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener la primer repuesta");

        } else {

            datos[0] = (String) out.get("PA_URLIMG");
            datos[1] = (String) out.get("PA_TXTHORA");

        }

        return datos;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NumeroTiendasDTO> obtieneNumTiendas(String ceco, int tipo) throws Exception {

        List<NumeroTiendasDTO> numeroTiendas = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco).addValue("PA_TIPO", tipo);

        out = jdbcObtieneNumSucursales.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAREPORTES_IMG.SP_OBTIENE_RESP}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        numeroTiendas = (List<NumeroTiendasDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener el numero de sucursales");
        }

        return numeroTiendas;
    }

    @Override
    public Map<String, Object> obtieneListasDatos(int checklist, String ceco, String fecha) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        Map<String, Object> listas = new HashMap<String, Object>();

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco).addValue("PA_CHECKLIST", checklist).addValue("PA_FECHA", fecha);

        out = jdbcObtieneConteoxPregunta.execute(in);

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        listas.put("conteo", out.get("RCL_CONSULTA"));
        listas.put("tiendas", out.get("RCL_TOTALES"));
        listas.put("horas", out.get("RCL_HORAS"));
        listas.put("reportes", out.get("RCL_REPORTES"));

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener conteo por pregunta y ceco ");
        }

        return listas;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConteoxPreguntaDTO> obtieneConteoxModulo(int checklist, String ceco, String fecha) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;
        List<ConteoxPreguntaDTO> lista = new ArrayList<ConteoxPreguntaDTO>();

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco).addValue("PA_CHECKLIST", checklist).addValue("PA_FECHA", fecha);

        out = jdbcObtieneConteoxModulo.execute(in);

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        lista = (List<ConteoxPreguntaDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener conteo por pregunta y ceco ");
        }

        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NumeroTiendasDTO> obtieneTotalTiendasRegion(int checklist, String ceco, String fecha) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        List<NumeroTiendasDTO> lista = new ArrayList<NumeroTiendasDTO>();

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco).addValue("PA_CHECKLIST", checklist).addValue("PA_FECHA", fecha);

        out = jdbcObtieneTiendasRegion.execute(in);

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        lista = (List<NumeroTiendasDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener conteo tiendas por region ");
        }

        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<HorasRespuestaDTO> obtieneConteoxHora(int checklist, String ceco, String fecha) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        List<HorasRespuestaDTO> lista = new ArrayList<HorasRespuestaDTO>();

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco).addValue("PA_CHECKLIST", checklist).addValue("PA_FECHA", fecha);

        out = jdbcObtieneConteoxHora.execute(in);

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        lista = (List<HorasRespuestaDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener las horas de la primera respuesta ");
        }

        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReportesConteoDTO> obtieneReportesConteo(int checklist, String ceco, String fecha) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        List<ReportesConteoDTO> lista = new ArrayList<ReportesConteoDTO>();

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco).addValue("PA_CHECKLIST", checklist).addValue("PA_FECHA", fecha);

        out = jdbcObtieneReportesConteo.execute(in);

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        lista = (List<ReportesConteoDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener los reportes ");
        }

        return lista;
    }

}
