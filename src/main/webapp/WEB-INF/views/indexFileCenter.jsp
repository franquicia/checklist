<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE>

<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="../css/bootstrap/bootstrap.css">

	<script	src="../js/jquery/jquery-2.2.4.min.js"></script>
	<script	src="../js/bootstrap.min.js"></script>
<title>Soporte - File center</title>
</head>
<body>

	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuSoporte.jsp" flush="true">
		<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuSoporte.jsp" />
	</tiles:insertTemplate>

	<!-- ------------- -->
	<!-- SECOND NAVBAR -->
	<!-- ------------- -->
	<nav class="navbar navbar-default">
		<c:url value = "/soporte/fileCenter.htm" var="fileCenterUrl" />
		<form:form method="POST" action="${fileCenterUrl}" modelAttribute="command" name="form-directory" id="form-directory" class="navbar-form navbar-default">
			<div class="input-group">
				<input type="text" class="form-control" id="fileRoute" name="fileRoute" placeholder="Ruta del directorio" value="${fileRoute}">
				<div class="input-group-btn">
					<input type="hidden" id="fileAction" value="${fileAction}" />
					<input type="hidden" id="fileRoute" value="${fileRoute}" />
					<input type="submit" class="btn btn-default" value="Revisar ruta" id="searchAction" name="searchAction" onclick="return checkInput()" />
				</div>
			</div>
			<c:choose>
				<c:when test="${fileAction=='subirArchivo'}">
					<div class="input-group">
						&ensp; &ensp;
						<input type="radio" name="fileAction" value="subirArchivo" checked /> Subir archivo  &ensp; &ensp;
						<input type="radio" name="fileAction" value="eliminarArchivo" /> Eliminar archivo  &ensp; &ensp;
						<input type="radio" name="fileAction" value="renombrarArchivo" /> Renombrar archivo
					</div>
				</c:when>
				<c:when test="${fileAction=='eliminarArchivo'}">
						<div class="input-group">
							&ensp; &ensp;
							<input type="radio" name="fileAction" value="subirArchivo" /> Subir archivo  &ensp; &ensp;
							<input type="radio" name="fileAction" value="eliminarArchivo" checked /> Eliminar archivo  &ensp; &ensp;
							<input type="radio" name="fileAction" value="renombrarArchivo" /> Renombrar archivo
						</div>
				</c:when>
				<c:when test="${fileAction=='renombrarArchivo'}">
					<div class="input-group">
						&ensp; &ensp;
						<input type="radio" name="fileAction" value="subirArchivo" /> Subir archivo  &ensp; &ensp;
						<input type="radio" name="fileAction" value="eliminarArchivo" /> Eliminar archivo  &ensp; &ensp;
						<input type="radio" name="fileAction" value="renombrarArchivo" checked /> Renombrar archivo
					</div>
				</c:when>
			</c:choose>
		</form:form>
	</nav>

	<c:choose>
		<c:when test="${directorio=='noExiste'}">
			<div class="container">
				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>¡Cuidado!</strong>
					<br> El directorio que pusiste no existe.
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${fileAction=='subirArchivo'}">
					<div class="container">
						<c:url value="/soporte/uploadFiles.htm" var="uploadFilesUrl" />
						<form:form method="POST" action="${uploadFilesUrl}" modelAttribute="command" name="form-uploadFiles" id="form-uploadFiles" enctype="multipart/form-data">
							<div class="form-group">
								<label for="inputFile">Seleccionar archivo:</label>
								<input type="hidden" id="fileAction" name="fileAction" value="${fileAction}" />
								<input type="hidden" id="fileRoute" name="fileRoute" value="${fileRoute}" />
								<input type="file" id="uploadedFile" name="uploadedFile" />
				 				<input type="submit" class="btn btn-default btn-lg" value="Subir archivo" onclick="return checkInput()" />
				 				<p class="help-block">Tamaño máximo: 100MB</p>
							</div>
						</form:form>
					</div>
				</c:when>
				<c:when test="${fileAction=='eliminarArchivo'}">
					<div class="container">
						<div class="form-group">
							<c:url value = "/soporte/deleteFiles.htm" var = "deleteFilesUrl" />
							<form:form method="POST" action="${deleteFilesUrl}" modelAttribute="command" name="form-deleteFiles" id="form-deleteFiles" >
								<c:choose>
									<c:when test="${empty listaArchivos}">
										<div class="alert alert-info alert-dismissible" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<strong>¡Info!</strong>
											<br>No quedan archivos por eliminar!
										</div>
									</c:when>
									<c:otherwise>
										<input type="hidden" id="fileAction" name="fileAction" value="${fileAction}" />
										<input type="hidden" id="fileRoute" name="fileRoute" value="${fileRoute}" />
										<input type="submit" class="btn btn-default btn-lg" value="Eliminar archivo" onclick="return confirmDelete()" />
										<br> <br>
										<table class="table table-condensed">
											<tr>
												<th>Eliminar</th>
												<th>Nombre del archivo</th>
											</tr>
											<c:forEach var="file" items="${listaArchivos}">
												<tr>
													<td><input type="checkbox" name="listaArchivosEliminar" value="${file}"></td>
													<td><c:out value="${file}" /></td>
												</tr>
											</c:forEach>
										</table>
									</c:otherwise>
								</c:choose>
							</form:form>
						</div>
					</div>
				</c:when>
				<c:when test="${fileAction=='renombrarArchivo'}">
					<div class="container">
						<div class="form-group">
							<c:url value = "/soporte/renameFiles.htm" var = "renameFilesUrl" />
							<form:form method="POST" action="${renameFilesUrl}" modelAttribute="command" name="form-renameFiles" id="form-renameFiles" >
								<c:choose>
									<c:when test="${empty listaArchivos}">
										<div class="alert alert-info alert-dismissible" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<strong>¡Info!</strong>
											<br>No quedan archivos por renombrar!
										</div>
									</c:when>
									<c:otherwise>
										<input type="text" class="form-control" id="newFileName" name="newFileName" placeholder="Nuevo nombre" />
										<input type="hidden" id="fileAction" name="fileAction" value="${fileAction}" />
										<input type="hidden" id="fileRoute" name="fileRoute" value="${fileRoute}" />
										<input type="submit" class="btn btn-default btn-lg" value="Renombrar archivo" onclick="return confirmRename()" />
										<br> <br>
										<table class="table table-condensed">
											<tr>
												<th>Renombrar</th>
												<th>Nombre del archivo</th>
											</tr>
											<c:forEach var="file" items="${listaArchivos}">
												<tr>
													<td><input type="radio" id="oldFileName" name="oldFileName" value="${file}"></td>
													<td><c:out value="${file}" /></td>
												</tr>
											</c:forEach>
										</table>
									</c:otherwise>
								</c:choose>
							</form:form>
						</div>
					</div>
				</c:when>
			</c:choose>
		</c:otherwise>
	</c:choose>

	<c:choose>
		<c:when test="${archivoSubido=='si'}">
			<div class="container">
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>¡Felicidades!</strong>
					<br> El archivo ha sido subido correctamente.
				</div>
			</div>
		</c:when>
		<c:when test="${archivoSubido=='no'}">
			<div class="container">
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>¡Error!</strong>
					<br> El archivo no pudo ser subido al servidor, intenta nuevamente!
				</div>
			</div>
		</c:when>
		<c:when test="${archivoSubido=='no-compatible'}">
			<div class="container">
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>¡Error!</strong>
					<br> El archivo no pudo ser subido al servidor por que tiene una extensión no permitida!
				</div>
			</div>
		</c:when>
		<c:when test="${archivoSubido=='size'}">
			<div class="container">
				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>¡Cuidado!</strong>
					<br> El archivo que intentas subir pesa mas de 100MB.
					<br> Intenta con un archivo mas ligero.
				</div>
			</div>
		</c:when>
	</c:choose>
	
	<c:choose>
		<c:when test="${eliminado=='no'}">
			<div class="container">
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>¡Error!</strong>
					<br> El archivo no pudo ser eliminado, verifica que todo esté en orden e intenta nuevamente.
				</div>
			</div>
		</c:when>
		<c:when test="${eliminado=='si'}">
			<div class="container">
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>¡Felicidades!</strong>
					<br> El archivo ha sido eliminado correctamente!
				</div>
			</div>
		</c:when>
		<c:when test="${eliminado=='no-vacio'}">
			<div class="container">
				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>¡Cuidado!</strong>
					<br>Debe seleccionar al menos un archivo para eliminar.
				</div>
			</div>
		</c:when>
	</c:choose>

	<c:choose>
		<c:when test="${renombrado=='no'}">
			<div class="container">
				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>¡Acción no permitida!</strong>
					<br> El archivo no pudo ser renombrado, revisa que la ruta sea la correcta e intenta nuevamente.
				</div>
			</div>
		</c:when>
		<c:when test="${renombrado=='si'}">
			<div class="container">
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>¡Felicidades!</strong>
					<br> El archivo ha sido renombrado correctamente!
				</div>
			</div>
		</c:when>
	</c:choose>

	<script>
		function checkInput() {
			var tAction = document.getElementById("fileRoute").value;
			var oFile = document.getElementById("uploadedFile").files[0];

			if (tAction == '' || tAction == null) {
				alert("La ruta no puede ir vacía");
				return false;
			}
			else {
				if (oFile.size > (1024*1024*100)) {
					alert("El archivo pesa más de 100MB!");
					return false;
			 	}
				else {
					return true;
				}
			}
		}

		function confirmRename() {
			var renameFile = document.getElementById("newFileName").value;
			var renameConfirm = confirm("¿Desea renombrar el archivo seleccionado?");
			
			if (renameConfirm == true) {
				if (renameFile == '' || renameFile == null) {
					alert("El campo del nombre del archivo no puede ir vacío");
					return false;
				}
				else {
					if (document.getElementById('oldFileName').checked) {
						return true;
					}
					else {
						alert("¡Debe seleccionar un archivo antes de realizar esa acción!");
						return false;
					}
				}
			}
			else {
				return false;
			}
		}

		function confirmDelete() {
			var retVal = confirm("¿Desea eliminar los archivos seleccionados?");

			if ( retVal == true ) {
				return true;
			}
			else {
				return false;
			}
		}
	</script>

</body>
</html>