package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.OrdenGrupoDTO;


public class OrdenGrupoRowMapper implements RowMapper<OrdenGrupoDTO>{


	public OrdenGrupoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		OrdenGrupoDTO ordenGrupo = new OrdenGrupoDTO();
		
		ordenGrupo.setIdOrdenGrupo(rs.getInt("FIID_ORDEN_G"));
		ordenGrupo.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		ordenGrupo.setOrden(rs.getInt("FIORDEN"));
		ordenGrupo.setIdGrupo(rs.getInt("FIID_GRUPO"));
		
		return ordenGrupo;
	}
}
