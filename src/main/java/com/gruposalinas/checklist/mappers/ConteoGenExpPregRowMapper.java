package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ConteoGenExpDTO;

public class ConteoGenExpPregRowMapper implements RowMapper<ConteoGenExpDTO> {

	public ConteoGenExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConteoGenExpDTO con = new ConteoGenExpDTO();
		
		con.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    con.setIdModulo(rs.getInt("FIID_MODULO"));
	    con.setPregunta(rs.getString("FCDESCRIPCION"));
	    con.setCritica(rs.getInt("FICRITICA"));
	    con.setArea(rs.getString("FCAREA"));
	    con.setTipoPreg(rs.getInt("FIID_TIPO_PREG"));
	    con.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
	    con.setIdCheck(rs.getInt("FIID_CHECKLIST"));
	    con.setNomCheck(rs.getString("CHECKLIST"));
	    con.setClasifica(rs.getString("FCCLASIFICA"));
	    con.setPondTotal(rs.getDouble("FCPONDTOTAL"));
	    con.setVersion(rs.getInt("FIVERSION"));
	   
		return con;
	}

}


