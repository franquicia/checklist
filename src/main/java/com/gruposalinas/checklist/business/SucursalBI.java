package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.SucursalDAO;
import com.gruposalinas.checklist.domain.SucursalDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class SucursalBI {

	private static Logger logger = LogManager.getLogger(SucursalBI.class);
	
	@Autowired
	SucursalDAO sucursalDAO;
	
	List<SucursalDTO> listaSucursales = null;
	List<SucursalDTO> listaSucursal = null;

	public List<SucursalDTO> obtieneSucursal(){
		try{
			listaSucursales = sucursalDAO.obtieneSucursal();
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Sucursales");
						
		}
				
		return listaSucursales;
	}
	
	public List<SucursalDTO> obtieneSucursal(String idSucursal){
		try{
			listaSucursal = sucursalDAO.obtieneSucursal(idSucursal);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Sucursal");
					
		}
				
		return listaSucursal;
	}
	
	public List<SucursalDTO> obtieneSucursalPaso(String idSucursal){
		try{
			listaSucursal = sucursalDAO.obtieneSucursalPaso(idSucursal);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Sucursal");
					
		}
				
		return listaSucursal;
	}
	
	public int insertaSucursal(SucursalDTO sucursal){
		int idSucursal = 0;
		
		try{
			idSucursal = sucursalDAO.insertaSucursal(sucursal);
		} 
		catch (Exception e) {
			logger.info("No fue posible insertar la Sucursal");
			
		}
		
		return idSucursal;
	}
	
	public boolean cargaSucursales() {
		
		boolean respuesta = false;
		
		try {		
			respuesta = sucursalDAO.cargaSucursales();	
		} catch (Exception e) {
			logger.info("No fue posible realizar la Carga de las Sucursales");
			
		}
		
		return respuesta;		
	}
	
	public boolean actualizaSucursal(SucursalDTO sucursal){
		boolean respuesta = false;
		try{
			respuesta = sucursalDAO.actualizaSucursal(sucursal);
		} 
		catch (Exception e) {
			logger.info("No fue posible actualizar la Sucursal");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaSucursal(int idSucursal){
		boolean respuesta = false;
		try{
			respuesta = sucursalDAO.eliminaSucursal(idSucursal);
		}
		catch(Exception e){
			logger.info("No fue posible borrar la Sucursal");
			
		}
		
		return respuesta;
	}
	
	
	//Sucursal GCC
	
	public List<SucursalDTO> obtieneSucursalGCC(String idSucursal){
		try{
			listaSucursal = sucursalDAO.obtieneSucursalGCC(idSucursal);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Sucursal");
					
		}
				
		return listaSucursal;
	}
	
	public int insertaSucursalGCC(SucursalDTO sucursal){
		int idSucursal = 0;
		
		try{
			idSucursal = sucursalDAO.insertaSucursalGCC(sucursal);
		} 
		catch (Exception e) {
			logger.info("No fue posible insertar la Sucursal");
			
		}
		
		return idSucursal;
	}
	
	public boolean actualizaSucursalGCC(SucursalDTO sucursal){
		boolean respuesta = false;
		try{
			respuesta = sucursalDAO.actualizaSucursalGCC(sucursal);
		} 
		catch (Exception e) {
			logger.info("No fue posible actualizar la Sucursal");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaSucursalGCC(int idSucursal){
		boolean respuesta = false;
		try{
			respuesta = sucursalDAO.eliminaSucursalGCC(idSucursal);
		}
		catch(Exception e){
			logger.info("No fue posible borrar la Sucursal");
			
		}
		
		return respuesta;
	}
	
	public boolean cargaActualizaPaises() {
		
		boolean respuesta = false;
		
		try {		
			respuesta = sucursalDAO.cargaActualizaPaises();	
		} catch (Exception e) {
			logger.info("No fue posible realizar la Actualización de Paises");
			
		}
		
		return respuesta;		
	}
	
	public boolean rellenaSucursal(){
		boolean respuesta = false;
		
		try {
			respuesta = sucursalDAO.rellenaSucursal();
		} catch (Exception e) {
			logger.info("No fue posible realizar la Actualización de sucursales que inician en 0");
			
		}
		
		return respuesta;
				
	}
	
}
