package com.gruposalinas.checklist.domain;

public class AdmTipoZonaDTO {
	private String idTipoZona;
	private String idStatus;
	private String descripcion;
	
	public String getIdTipoZona() {
		return idTipoZona;
	}
	public void setIdTipoZona(String idTipoZona) {
		this.idTipoZona = idTipoZona;
	}
	public String getIdStatus() {
		return idStatus;
	}
	public void setIdStatus(String idStatus) {
		this.idStatus = idStatus;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	@Override
	public String toString() {
		return "AdmTipoZonaDTO [idTipoZona=" + idTipoZona + ", idStatus=" + idStatus + ", descripcion=" + descripcion
				+ "]";
	}
	
	
	
}


