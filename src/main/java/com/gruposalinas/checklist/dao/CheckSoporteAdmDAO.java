package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.CheckSoporteAdmDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.domain.EmpFijoDTO;


public interface CheckSoporteAdmDAO {
	
	
	public List<CheckSoporteAdmDTO> checkAutorizacion(String idGrupo) throws Exception;

	public List<CheckSoporteAdmDTO>  checkActivos(String idGrupo) throws Exception;
	
	public boolean actualiza(CheckSoporteAdmDTO bean)throws Exception;
	
	public List<CheckSoporteAdmDTO> checkVersParam(String param,String idGupo) throws Exception;
	
	public List<CheckSoporteAdmDTO> checkVersParamProd(String param,String idGupo) throws Exception;
}
