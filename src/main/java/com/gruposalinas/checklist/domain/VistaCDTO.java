package com.gruposalinas.checklist.domain;

public class VistaCDTO {

	private String idCeco;
	private String descCeco;
	private int total;
	private String idChecklist;
	private String numSuc;
	private int asignados;
	private int terminados;
	
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getDescCeco() {
		return descCeco;
	}
	public void setDescCeco(String descCeco) {
		this.descCeco = descCeco;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(String idChecklist) {
		this.idChecklist = idChecklist;
	}
	public String getNumSuc() {
		return numSuc;
	}
	public void setNumSuc(String numSuc) {
		this.numSuc = numSuc;
	}
	public int getAsignados() {
		return asignados;
	}
	public void setAsignados(int asignados) {
		this.asignados = asignados;
	}
	public int getTerminados() {
		return terminados;
	}
	public void setTerminados(int terminados) {
		this.terminados = terminados;
	}
	@Override
	public String toString() {
		return "VistaCDTO [idCeco=" + idCeco + ", descCeco=" + descCeco + ", total=" + total + ", idChecklist="
				+ idChecklist + ", numSuc=" + numSuc + ", asignados=" + asignados + ", terminados=" + terminados + "]";
	}
	
	
	
}
