package com.gruposalinas.checklist.business;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.CenterTextMode;
import org.jfree.chart.plot.RingPlot;
import org.jfree.data.general.DefaultPieDataset;
import org.springframework.core.io.Resource;

import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.ExpansionContadorDTO;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;
import com.gruposalinas.checklist.domain.RepoFilExpDTO;
import com.gruposalinas.checklist.domain.ZonaNegoExpDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.util.CleanPath;
import com.gruposalinas.checklist.util.UtilObject;
import com.gruposalinas.checklist.util.UtilResource;
import com.itextpdf.text.Document;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

public class ExpansionActa2BI {

	private static final Logger logger = LogManager.getLogger(ExpansionActa2BI.class);
	private String direccion=FRQConstantes.getRutaImagen(); 

	private String idBitacora;
	private String negocio;
	private DecimalFormat formatDecimales;
	private DecimalFormat formatEnteros;
	
	private ExpansionContadorDTO calculoTotal = null;
	private List<ExpansionContadorDTO> calculoZonas = null;
	private List<ZonaNegoExpDTO> listafilaZonas = null;
	
	private List<String> imperdonables = null;
	
	private String[] imgPond;	
	private String[] clasificacion;	
	private String[] detalleClasif;	
	private String[] calificacionClasif;	
	private String[] cuadroGrafica;	
	private File []  graficaFile;
	private String[] generaHTMLDetalleClasif;
	private List<RepoFilExpDTO> [] pregListClasif;
	private String paginaIndice;
	private String cadenaImperdonables;
	private String generaImagenBaner;
	private String colorOperable;
	private double preCalificacionNum;
	
	private String operable;
	private String colorTexto;
	private double numeroDeCalificacion;
	private int numImperdonablesRecorrido;
	private String totalItems;
	private String itemsPorRevisar;
	private String itemsAprobados;
	private String itemsNoConsiderados;
	
	private int colorBaner = 0; // 0=blanco
	private String imgBaner = "";
	
	public ExpansionActa2BI() {}
	
	public ExpansionActa2BI(String idBitacora,String negocio,boolean obtenerCalculos) {
		formatDecimales = new DecimalFormat("###.##");
		formatEnteros = new DecimalFormat("###.##");
		
		this.idBitacora=idBitacora;
		this.negocio=negocio;
		obtieneDatos(negocio,idBitacora, obtenerCalculos);
	}

	
	public void obtieneDatos(String negocio, String idBitacora, boolean generaGrafica) {
		ZonaNegoBI zonaNego = (ZonaNegoBI) FRQAppContextProvider.getApplicationContext()
				.getBean("zonaNegoBI");
		calculoZonas = new ArrayList<>();

		listafilaZonas  = zonaNego.obtieneInfoNego(negocio);
		if(listafilaZonas!= null && listafilaZonas.size()>0) {
			clasificacion = new String[listafilaZonas.size()+1];
			detalleClasif = new String[listafilaZonas.size()+1];
			imgPond = new String[listafilaZonas.size()+1];
			graficaFile = new File[listafilaZonas.size()+1];
			cuadroGrafica = new String[listafilaZonas.size()+1];
			generaHTMLDetalleClasif = new String[listafilaZonas.size()];
			calificacionClasif = new String[listafilaZonas.size()];
			pregListClasif = new ArrayList[listafilaZonas.size()];
			
			for(int i=0; i<listafilaZonas.size(); i++) {
				CalculaDatosExpansion calculaDatos = new CalculaDatosExpansion(idBitacora,listafilaZonas.get(i).getClasifica(),1); 
				calculoZonas.add(calculaDatos.getDatosExpansion());
				
				imgPond[i]=calculaGraficos(calculaDatos.getDatosExpansion());
				clasificacion[i]=calculoZonas.get(i).getClasif();
				detalleClasif[i]=listafilaZonas.get(i).getDescZona();
				pregListClasif[i]= new ArrayList<>();
				calificacionClasif[i]=formatDecimales.format(calculoZonas.get(i).getCalificacionClasifCalculada());
				
			}
			
			CalculaDatosExpansion total = new CalculaDatosExpansion(calculoZonas); 
			calculoTotal = total.getDatosExpansion();
			preCalificacionNum=calculoTotal.getCalificacionClasifCalculada();
			totalItems=""+calculoTotal.getCountItemClasifRe();
			itemsPorRevisar=""+calculoTotal.getCountItemClasifNo();
			itemsAprobados=""+calculoTotal.getCountItemClasifSi();
			itemsNoConsiderados=""+calculoTotal.getCountItemClasifNA();
			imgPond[listafilaZonas.size()]=calculaGraficos(calculoTotal);
			clasificacion[listafilaZonas.size()]="TOTAL";
			detalleClasif[listafilaZonas.size()]="TOTALES";
			

			obtieneDatosAperturables(calculoTotal);
			
			if(generaGrafica) {
				paginaIndice = paginaIndice(clasificacion, detalleClasif);
				cadenaImperdonables = cadenaImperdonables(idBitacora);
				generaImagenBaner = generaImagenBaner(colorBaner);
				imgBaner = generaImagenBaner;
				obtenGrafica(calculoZonas,calculoTotal);
				cadenaTodasPreguntas(pregListClasif);
			}
		}
		
	}
	
	public String generaImagenBaner() {
		String imagenes ="";
		String ruta1="";
		String ruta2="";
		
		if(colorBaner==1) {//http://10.53.33.82/franquicia/firmaChecklist/BAZ_TRANS_NEGRO.png
			ruta1 = direccion+"/franquicia/firmaChecklist/EKT_TRANS_NEGRO.png";
			ruta2 = direccion+"/franquicia/firmaChecklist/BAZ_TRANS_NEGRO.png";
		}
		if(colorBaner==0) {
			ruta1 = direccion+"/franquicia/firmaChecklist/EKT_TRANS_BLANCO.png";
			ruta2 = direccion+"/franquicia/firmaChecklist/BAZ_TRANS_BLANCO.png";
		}
		
		 imagenes="<img src=\""+ruta1+"\" style=\"height: 25%;\"></img> &nbsp;&nbsp;" + 
				  "<img src=\""+ruta2+"\" style=\"height: 25%;\"></img>";
		 return imagenes;
	}
	
	public String calculaGraficos(ExpansionContadorDTO zonaCalculo) {
		String imgPond = "";
		imgPond = getLogoExpansion(zonaCalculo.getCalificacionClasifCalculada(), zonaCalculo.getCountItemClasifImp());
		
		return imgPond;
	}
	
	
	private String getLogoExpansion(double num, int imperdonable) {
		if (imperdonable > 0) {
			return "vt01.png";
		} else {
			if (num >= 90) {
				return "vt03.png";
			} else if (num >= 70) {
				return "vt04.png";
			} else if (num >= 51) {
				return "vt02.png";
			} else {
				return "vt01.png";
			}
		}
	}
	
	
	public void obtieneDatosAperturables(ExpansionContadorDTO zonaCalculo) {
		numeroDeCalificacion = (zonaCalculo.getCalificacionClasifCalculada());
		numImperdonablesRecorrido = zonaCalculo.getCountItemClasifImp();
		colorTexto = "white";

		if (numImperdonablesRecorrido > 0) {
			// Tiene imperdonables el color es NEGRO, NO OPERABLE Y SIN RECEPCIÓN, letra
			// blanca
			operable = "NO OPERABLE ";
			operable += "";
			colorOperable = "#000";
			colorTexto = "white";
			colorBaner = 0; //blanco
		} else {

			if (numeroDeCalificacion >= 100) {
				operable = "OPERABLE ";
				operable += " (Con recepción)";
				// VERDE
				colorOperable = "#46ad35";
				colorTexto = "white";
				colorBaner = 0; //blanco
			} else if (numeroDeCalificacion >= 70 && numeroDeCalificacion <= 99) {
				operable = "OPERABLE ";
				operable += " (Sin recepción)";
				// AMARILLO
				colorOperable = "#F4C431";
				colorTexto = "black";
				colorBaner = 1; //negro
			} else if (numeroDeCalificacion >= 50 && numeroDeCalificacion <= 69) {
				operable = "NO OPERABLE ";
				operable += " ";
				// ROJO
				colorOperable = "#FE003D";
				colorTexto = "white";
				colorBaner = 0; //blanco
			} else if (numeroDeCalificacion <= 49) {
				operable = "NO OPERABLE ";
				operable += "";
				// NEGRO
				colorOperable = "#000";
				colorTexto = "white";
				colorBaner = 0; //blanco
			}
		}
	}
	
	
	public String generaImagenBaner(int colorBaner) {
		String imagenes ="";
		String ruta1="";
		String ruta2="";
		
		if(colorBaner==1) {//http://10.53.33.82/franquicia/firmaChecklist/BAZ_TRANS_NEGRO.png
			ruta1 = direccion+"/franquicia/firmaChecklist/EKT_TRANS_NEGRO.png";
			ruta2 = direccion+"/franquicia/firmaChecklist/BAZ_TRANS_NEGRO.png";
		}
		if(colorBaner==0) {
			ruta1 = direccion+"/franquicia/firmaChecklist/EKT_TRANS_BLANCO.png";
			ruta2 = direccion+"/franquicia/firmaChecklist/BAZ_TRANS_BLANCO.png";
		}
		
		 imagenes="<img src=\""+ruta1+"\" style=\"height: 25%;\"></img> &nbsp;&nbsp;" + 
				  "<img src=\""+ruta2+"\" style=\"height: 25%;\"></img>";
		 return imagenes;
	}
	
	public void obtenGrafica(List<ExpansionContadorDTO> zonasCalculo, ExpansionContadorDTO zonasCalculoTot) {

		if(zonasCalculo!=null && zonasCalculo.size()>0 ) {
			for(int i=0; i<zonasCalculo.size();i++) {
				String imgPonde="";
				int posicion = 0;
				for(int j=0; j<clasificacion.length; j++) {
					if(clasificacion[j].equalsIgnoreCase(zonasCalculo.get(i).getClasif())){
						imgPonde=imgPond[j];
						posicion=j;
						break;
					}
				}
				if(zonasCalculo.get(i).getCountItemClasifSi()==0 && zonasCalculo.get(i).getCountItemClasifNo()==0) {
					graficaFile[posicion] = grafica(1, 0, 100);
					imgPonde = getLogoExpansion(100, 0);
				}
				else {
					
					graficaFile[posicion]  = grafica(1, new Double(formatDecimales.format(zonasCalculo.get(i).getPorcentajeClasifReSi())), new Double(formatDecimales.format(zonasCalculo.get(i).getPorcentajeClasifReNo())));
				}
		
				cuadroGrafica[posicion] = generaGrafica(imgPonde,zonasCalculo.get(i).getClasif(),
						detalleClasif[posicion],
						formatDecimales.format(zonasCalculo.get(i).getPonderacionClasifMaximaCalculada()),
						graficaFile[posicion],formatDecimales.format(zonasCalculo.get(i).getCalificacionClasifCalculada()),
						formatEnteros.format(zonasCalculo.get(i).getCountItemClasifSi()),
						formatEnteros.format(zonasCalculo.get(i).getPorcentajeClasifReSi()),
						formatEnteros.format(zonasCalculo.get(i).getCountItemClasifNo()),
						formatEnteros.format(zonasCalculo.get(i).getPorcentajeClasifReNo()));			
			}
		}
		if(zonasCalculoTot!=null) {
			String imgPonde = imgPond[listafilaZonas.size()];
			if(zonasCalculoTot.getCountItemClasifSi()==0 && zonasCalculoTot.getCountItemClasifNo()==0) {
				graficaFile[listafilaZonas.size()] = grafica(1, 0, 100);
				imgPonde = getLogoExpansion(100, 0);
			}
			else {
				graficaFile[listafilaZonas.size()]  = grafica(1, new Double(formatDecimales.format(zonasCalculoTot.getPorcentajeClasifReSi())), new Double(formatDecimales.format(zonasCalculoTot.getPorcentajeClasifReNo())));
			}
			cuadroGrafica[listafilaZonas.size()] = generaGrafica(imgPonde,zonasCalculoTot.getClasif(),
					detalleClasif[listafilaZonas.size()],
					formatDecimales.format(zonasCalculoTot.getPonderacionClasifMaximaCalculada()),
					graficaFile[listafilaZonas.size()],formatDecimales.format(zonasCalculoTot.getCalificacionClasifCalculada()),
					formatEnteros.format(zonasCalculoTot.getCountItemClasifSi()),
					formatEnteros.format(zonasCalculoTot.getPorcentajeClasifReSi()),
					formatEnteros.format(zonasCalculoTot.getCountItemClasifNo()),
					formatEnteros.format(zonasCalculoTot.getPorcentajeClasifReNo()));			
		}
		
	}
	
	private String generaGrafica(String logo, String clasif,String detalleClasif, String totalPon, File grafica,
			String califCant,String resSi,String siPor,String resNo,String noPor) {
		String clasifs = "";
		String clasiff = "";
		int totales=0;
		String direccionImg=direccion+"/franquicia/firmaChecklist/";
	
		clasifs = clasiffImg(clasif);
		
		String srtCuadroGrafica="";
		Resource portada = UtilResource.getResourceFromInternet("contenedorGrafica.html");
		try {
			srtCuadroGrafica = UtilObject.inputStreamAsString(portada.getInputStream());
			srtCuadroGrafica = srtCuadroGrafica.replace("@imagenClasifImg",direccionImg+clasifs);
			srtCuadroGrafica = srtCuadroGrafica.replace("@detalleClasif",detalleClasif);
			srtCuadroGrafica = srtCuadroGrafica.replace("@rutaPondClasifImg",direccionImg+logo);
			srtCuadroGrafica = srtCuadroGrafica.replace("@totalPondClasif",totalPon);
			srtCuadroGrafica = srtCuadroGrafica.replace("@rutaGraficaImg",grafica.getAbsolutePath());
			srtCuadroGrafica = srtCuadroGrafica.replace("@direccionItemsAprovados02",direccionImg+"ItemsAprovados02.png");
			srtCuadroGrafica = srtCuadroGrafica.replace("@direccionItemsAtender02",direccionImg+"ItemsAtender02.png");

			srtCuadroGrafica = srtCuadroGrafica.replace("@countItemSi",resSi);
			srtCuadroGrafica = srtCuadroGrafica.replace("@countItemNo",resNo);
			srtCuadroGrafica = srtCuadroGrafica.replace("@pondItemSi",siPor);
			srtCuadroGrafica = srtCuadroGrafica.replace("@pondItemNo",noPor);
			srtCuadroGrafica = srtCuadroGrafica.replace("@califClasif",califCant);
					
			
			
			
		} catch (Exception e) {
			logger.info("No se pudo extraer o sustituir algun dato para generar contenedorGrafi " + e.getMessage());
		}
		
/*
		if(clasif.equalsIgnoreCase("EKT")) {
			clasiff = "Elektra &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		}
		if(clasif.equalsIgnoreCase("AREAS")) {
			clasiff = "Áreas comunes &nbsp;";
		}
		if(clasif.equalsIgnoreCase("BAZ")) {
			clasiff = "Banco Azteca &nbsp;&nbsp;";
		}
		if(clasif.equalsIgnoreCase("GENERALES")) {
			clasiff = "Generales &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		}
		if(clasif.equalsIgnoreCase("PP")) {
			clasiff = "Presta Prenda";
		}
		if(clasif.equalsIgnoreCase("CYC")) {
			clasiff = "Credito Y Cobranza";
		}
		if(clasif.equalsIgnoreCase("TOTAL")) {
			clasiff = "Totales &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			totales=1;
		}
		
		
		String cuadroGrafica="";

		if(totales==0)
			cuadroGrafica+="<div style='width:100%; border: .25px solid #F0EFF0;'>" +
			// Titulo
			"<br></br>" + "<div>" + "<b><font size=\"1\" >"+
			
			"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='"+direccion+"/franquicia/firmaChecklist/"+clasifs+"' width='15' />"+
			"&nbsp; "+clasiff

			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" /*height=10' height='10' */
/*
			+ "<img src='"+direccion+"/franquicia/firmaChecklist/" + logo + "' width='35'/>"
			+ "<br></br>"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "Total: " + formatDecimales.format((Double.parseDouble(totalPon))) + "</font></b>" + "</div>";
		else {	
			cuadroGrafica+="<div style='width:100%; background-color:#F0EFF0; border: 1px;'>" +
			// Titulo
			"<br></br>" + "<div>" + "<b><font size=\"1\" >"+
			
			"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
			"&nbsp; "+clasiff

			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" /*height=10' height='10' */
/*
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "<img src='"+direccion+"/franquicia/firmaChecklist/" + logo + "' width='35'/>"
			+ "<br></br>"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "Total: " + formatDecimales.format((Double.parseDouble(totalPon))) + "</font></b>" + "</div>";
		}	
		cuadroGrafica+=
				// Div Imagen
				"<div>" + "<div style='float: left; '>" + "<img src='" + grafica.getAbsolutePath()
				+ "' width='130px' height='90px' />" + "</div>" +

				// Mini tabla
				
				"<br></br><br></br>"
				
				+"<div style='float: right; width: 20px; bottom:0; padding-top: 0px; '>" +

				"<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
				+ "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"
				+ "<th style='text-align:center; font-size: 5px;'>items</th>"
				+ "<th style='text-align:center; font-size: 5px;' >items %</th>"
				+ "<th style='text-align:center; font-size: 5px;'>Calificación</th>" + "</tr>" + "<tr>"
				+ "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='"+direccion+"/franquicia/firmaChecklist/ItemsAprovados02.png' width='10' height='10'/> </td>"
				+ "<td style='font-size: 10px;'>" + resSi + "</td>" + "<td style='font-size: 10px;'>" + siPor + "</td>"
				+ "<td rowspan='2' style='background-color:#F0EFF0; font-size: 10px;'>"
				+ califCant + "</td>" + "</tr>" + "<tr>"
				+ "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='"+direccion+"/franquicia/firmaChecklist/ItemsAtender02.png' width='10' height='10'/> </td>"
				+ "<td style='font-size: 10px;'>" + resNo + "</td>" + "<td style='font-size: 10px;'>" + noPor + "</td>" + "</tr>" + "</table>" +

				"</div>" +

				"</div>" +
				" </div>";
*/
		return srtCuadroGrafica;
	}
	
	
	//proceso para generar pdf imperdonables
	public String cadenaImperdonables(String idBitacora) { 
		imperdonables = new ArrayList<>();
		ChecklistPreguntasComBI checklistPreguntasComBI = (ChecklistPreguntasComBI) FRQAppContextProvider
				.getApplicationContext().getBean("checklistPreguntasComBI");

		List<ChecklistPreguntasComDTO> chklistPreguntasImp  = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> listaResult  = new ArrayList<ChecklistPreguntasComDTO>();
		listaResult.clear();


		chklistPreguntasImp = checklistPreguntasComBI.obtieneInfoImp(Integer.parseInt(idBitacora));
		for (int i = 0; i < chklistPreguntasImp.size() ; i++) {
			if (chklistPreguntasImp.get(i).getIdcritica() == 1 ) {
				ChecklistPreguntasComDTO data = chklistPreguntasImp.get(i);
				data.setPregunta(chklistPreguntasImp.get(i).getPregunta());
				imperdonables.add(data.getPregunta());
				listaResult.add(data);
			}
		}

		String imperdonablesHtml = "<html><body>";
		imperdonablesHtml  += "<div style='position: fixed; top: 10px; right: 30px; left: 30px; bottom: 10px; background: white; border-radius: 30px; border-style: solid;  border-color: gray; border-width: 1px;  padding: 20px;  text-align: right;  overflow-y: scroll;'>";
		imperdonablesHtml += "<div style = 'background-color: #ECEBEB;float:right; width:100%;'><img src='"+direccion+"/franquicia/firmaChecklist/LogoElektra.png' height='30px' style='float: right;'></img>&nbsp;&nbsp;<img src='"+direccion+"/franquicia/firmaChecklist/LogoBAZ.png' height='30px' style='float: right;'></img></div> <br></br> <br></br> <br></br> <div style='text-align: left;background-color: #ECEBEB; padding-top: .5px; padding-bottom: .5px;' ><p > <b>&nbsp;&nbsp;Detalles Imperdonables</b>  | Entrega de Sucursal</p></div> <br></br>"; 
		imperdonablesHtml += " <div style='background-color: #C82663; padding-top: .5px; padding-bottom: .5px;  text-align: center;'><h1 style='color: white'>"+((listaResult!=null && listaResult.size()>0)?listaResult.size():0)+"</h1></div> <br></br>";
		if(listaResult!=null && listaResult.size()>0) {
			imperdonablesHtml += "<table  style='width: 100%; text-align: left'>";
			for (int i = 0; i < listaResult.size(); i++) {
			      imperdonablesHtml += "<tr>";
			      imperdonablesHtml += "<td style='text-align: left; padding-left: 10px; border-bottom: 1px solid gray; '>" + (i + 1) + ".- " + listaResult.get(i).getPregunta() + "</td>";
			      imperdonablesHtml += "<td style='border-bottom: 1px solid gray;'><div width='60'height='60' ><img src='"+ direccion+listaResult.get(i).getRuta() + "' width='60'height='60'></img></div></td>";
			      //imperdonablesHtml += "<td style='border-bottom: 1px solid gray;'><img src='http://10.53.33.82//franquicia/ImagSucursal/48999416333706082019.jpg' width='60'height='60'/></td>";
			      imperdonablesHtml += "</tr>";
		    }
			imperdonablesHtml += " </table>";
		}
	    imperdonablesHtml += "</div>";
	    imperdonablesHtml += "</body></html>";
	    logger.info(imperdonablesHtml);

		    
		return imperdonablesHtml;

	}

	
	//Proceso para generar pdf a detalle
		
	public void cadenaTodasPreguntas(List<RepoFilExpDTO> [] pregListClasif) {
			RepoFilExpBI repoFilExpBI = (RepoFilExpBI) FRQAppContextProvider
					.getApplicationContext().getBean("repoFilExpBI");
			
			List<RepoFilExpDTO> todasPreguntas = new ArrayList<RepoFilExpDTO>();
			List<RepoFilExpDTO> preguntas = new ArrayList<RepoFilExpDTO>();

			todasPreguntas = repoFilExpBI.obtieneInfofActa(idBitacora);
			preguntas = repoFilExpBI.obtienePreguntasActa(idBitacora);

			for (int i = 0; i < todasPreguntas.size(); i++) {
				if (todasPreguntas.get(i).getPosible().equalsIgnoreCase("SI")) {
					RepoFilExpDTO data2 = todasPreguntas.get(i);
					data2.setPregunta(todasPreguntas.get(i).getPregunta());
					data2.setCalif(todasPreguntas.get(i).getCalif());
					data2.setPosible(todasPreguntas.get(i).getPosible());
					data2.setRuta(todasPreguntas.get(i).getRuta());
					data2.setObserv(todasPreguntas.get(i).getObserv());
					for(int a = 0; a<pregListClasif.length;a++) {
						if(clasificacion[a].equalsIgnoreCase(data2.getClasif())) {
							pregListClasif[a].add(data2);
						}
					}
				} else {
					RepoFilExpDTO data = todasPreguntas.get(i);
					data.setPregunta(todasPreguntas.get(i).getPregunta());
					for(int a = 0; a<pregListClasif.length;a++) {
						if(clasificacion[a].equalsIgnoreCase(data.getClasif())) {
							pregListClasif[a].add(data);
						}
					}
				}
			}
			geteraDetalleClasif(pregListClasif);
			
		}
	
	public void geteraDetalleClasif(List<RepoFilExpDTO> [] pregListClasif) {
		for(int a = 0; a<pregListClasif.length;a++) {
			generaHTMLDetalleClasif[a]=generaHTMLDetalleClasif(detalleClasif[a],pregListClasif[a],imgPond[a],clasiffImg(clasificacion[a]),calificacionClasif[a]);
		}
	}
	
	private String generaHTMLDetalleClasif(String clasif,List<RepoFilExpDTO> preguntas,String logo,String clasifImg,String calificacion) {

		
		String zonaHtml = "<html><body>";
		zonaHtml += "<div style='position: fixed; top: 10px; right: 30px; left: 30px; bottom: 10px; background: white; border-radius: 30px; border-style: solid;  border-color: gray; border-width: 1px;  padding: 20px;  text-align: right; overflow-y: scroll;'>";
		zonaHtml += "<div style = 'background-color: #ECEBEB; width:100%;'><img src='http://10.53.33.82/franquicia/firmaChecklist/LogoElektra.png' height='30px' style='float: right;'></img>&nbsp;&nbsp;<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoBAZ.png' height='30px' style='float: right;'></img></div> <br></br> <br></br> <br></br>";
		zonaHtml += "<div><table><tr><td><img src='http://10.53.33.82/franquicia/firmaChecklist/"+clasifImg+"' height='50' align='left'/></td><td> <p style='text-align: left; padding-top: 10px'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+clasif+"</b></p></td><td><p style='text-align: center; padding-top: 10px; '>Calififcacion:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
				 + calificacion
				 + "</p></td><td><img src='http://10.53.33.82/franquicia/firmaChecklist/"+logo+"' width='50'height='25' align='rigth' style='padding-left: 30px;padding-top: 10px;'/></td></tr></table></div>";
		zonaHtml += "<div style='text-align: left; background-color: #ECEBEB;padding-top: .5px;padding-bottom: .5px;' ><table style=' width: 100%;  '> <tr><td><p > <b>&nbsp;&nbsp;Detalles de Calificacion</b>  | Entrega de Sucursal</p></td><td><p></p></td></tr></table> </div> <br>  </br>";

		zonaHtml += "<div style='text-align: left;'>";
		
		if(preguntas != null && preguntas.size()>0) {
			zonaHtml += "<table style='width: 100%'><tr><th>Item</th><th>Respuesta</th><th>Foto</th></tr>";
			for (int i = 0; i < preguntas.size(); i++) {
	
				zonaHtml += "<tr>";
				zonaHtml += "<td style='font-size: 13; width:60%;'>" + (i + 1) + ".-" + preguntas.get(i).getPregunta() + "<br></br>" + "Comenatrio Adicional: "+ ((preguntas.get(i).getObserv() != null) ? preguntas.get(i).getObserv() : " ")
						+ "</td>";
				zonaHtml += "<td style='font-size: 13'>" + preguntas.get(i).getPosible() + "</td>";
				//zonaHtml += "<td ><img src='" + preguntas.get(i).getRuta() + "' width='60'height='60'/></td>";
				zonaHtml += "<td ><div  width='60'height='60'><img src='" + preguntas.get(i).getRuta() + "' width='60'height='60'/></div></td>";
				zonaHtml += "</tr>";
	
			}
			zonaHtml += "</table>";
		} else {
			zonaHtml += "<table style='width: 100%'><tr><th>Item</th><th>Respuesta</th><th>Foto</th></tr>";
			zonaHtml += "<tr><td></td><td></td><td></td></tr>";
			zonaHtml += "</table>";
		}

		zonaHtml += "  </div></div>";
		zonaHtml += "</body></html>";
		logger.info(zonaHtml);
		return zonaHtml;
	}
	
	private File grafica(int tipo, double aprobados, double porAtender) {
		// Tipo 1 Backround Blanco - Tipo 2 Backroung Gris
		Color gris = new Color(240, 239, 240);

		DefaultPieDataset dataset = new DefaultPieDataset();
		dataset.setValue("Items Aprobados", aprobados);
		dataset.setValue("Items Por Atender", porAtender);

		RingPlot plot = new RingPlot(dataset);

		Color vrd = new Color(227, 227, 227);
		Color rojo = new Color(142, 142, 142);

		plot.setSectionPaint("Items Aprobados", vrd);
		plot.setSectionPaint("Items Por Atender", rojo);

		JFreeChart chart = new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, plot, true);

		// Fondo de la Gráfica
		if (tipo == 1) {
			chart.getPlot().setBackgroundPaint(Color.white);
		} else {
			chart.getPlot().setBackgroundPaint(gris);
		}

		// plot.setCenterTextMode(CenterTextMode.VALUE);
		Font font1 = new Font(null, Font.BOLD, 45);
		plot.setCenterTextMode(CenterTextMode.FIXED);
		plot.setCenterText(aprobados + "%");

		plot.setCenterTextFont(font1);
		plot.setCenterTextColor(Color.BLACK);

		plot.setOutlineVisible(false);
		plot.setSectionDepth(0.35);
		// plot.setSectionOutlinesVisible(true);
		// plot.setSimpleLabels(true);
		plot.setOuterSeparatorExtension(0);
		plot.setInnerSeparatorExtension(0);

		Font font = new Font("FONT", 0, 22);
		plot.setLabelFont(font);

		chart.getLegend().setFrame(BlockBorder.NONE);
		// chart.getLegend().setPosition(RectangleEdge.);

		// FONDO DE LA GRÁFICA
		if (tipo == 1) {
			chart.setBackgroundPaint(java.awt.Color.white);
			plot.setLabelBackgroundPaint(Color.white);
		} else {
			chart.setBackgroundPaint(gris);
			plot.setLabelBackgroundPaint(gris);

		}

		return setGraficaTemp(chart);
	}
	
	private String clasiffImg(String clasiff) {
		String clasifs="";
		if(clasiff.equalsIgnoreCase("EKT")) {
			clasifs = "zonaElektra.png";
		}
		if(clasiff.equalsIgnoreCase("AREAS")) {
			clasifs = "zonaAreasComunes.png";
		}
		if(clasiff.equalsIgnoreCase("BAZ")) {
			clasifs = "zonaBAZ.png";
		}
		if(clasiff.equalsIgnoreCase("GENERALES")) {
			clasifs = "zonaGenerales.png";
		}
		if(clasiff.equalsIgnoreCase("PP")) {
			clasifs = "zonaPrestaPrenda.png";
		}
		if(clasiff.equalsIgnoreCase("CYC")) {
			clasifs = "zonaBAZ.png";
		}
		return clasifs;
	}
	
	
	private File setGraficaTemp(JFreeChart chart) {

		File temp = null;
		// create a temp file
		try {
			temp = File.createTempFile("grafica", ".png");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		BufferedImage chartImage = chart.createBufferedImage(600, 400, null);

		try {
			ImageIO.write(chartImage, "png", temp);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return temp;

	}
	
	public String paginaIndice( String[] clasificacion, String[] detalleClasif) {
		String retorno = "<html><head></head><body><div><br></br>" + 
				"<div style=\"background-color: #000; padding-top: 20px; padding-bottom: 40px;\">" + 
				"  <div style=\"width: 55%;float: left;\">" + 
				"  <font style=\"text-align: center;padding: 10px;\" size=\"3\">" + 
				"  <b style=\"color: white;\">&nbsp;&nbsp;Acta de Entrega de Sucursal</b></font>" + 
				"  </div><div style=\"text-align: right;width: 45%;float: left;\">" + 
				"      <img src=\""+direccion+"/franquicia/firmaChecklist/EKT_TRANS_BLANCO.png\" height=\"30px\"> </img>&nbsp;&nbsp;" + 
				"      <img src=\""+direccion+"/franquicia/firmaChecklist/BAZ_TRANS_BLANCO.png\" height=\"30px\"> </img>" + 
				"  </div>" + 
				"</div>" + 
				"  <br></br><br></br>" + 
				"<div style=\"margin-left: 10%;\">" + 
				"  <table cellspacing=\"10\" cellpadding=\"10\">" + 
				"    <thead>\n" + 
				"      <tr><th><b>Índice</b></th></tr>" + 
				"    </thead><tbody>" + 
				"      <tr>" + 
				"        <td>1. Detalles Generales de la Entrega de Sucursal </td>" + 
				"      </tr><tr>" + 
				"        <td>2. Detalles de Imperdonables</td>" + 
				"      </tr>";
				for(int i=0; i<(clasificacion.length-1);i++) {
					retorno +="<tr>" + 
							"        <td>"+(i+3)+". Detalles de Calificación "+detalleClasif[i]+"</td>" + 
							"</tr>";
				}
				retorno +=
				"    </tbody>" + 
				"  </table>" + 
				"</div> <br></br></div></body></html>";
		logger.info(retorno);
		return retorno;
	}
	
	public String generaFirma(FirmaCheckDTO firma) {
		String firmaImg="";

		firmaImg+="<img style=\"width: 45px; height: 45px;\" src=\""+direccion+firma.getRuta()+"\"/>";
		
		return firmaImg;
	}
	
	public String mergePDF(List<String> rutas){
		String rutaMerge = "";
		try {
            List<InputStream> pdfs = new ArrayList<InputStream>();
            if(rutas!=null && rutas.size()>0) rutaMerge = ""+rutas.get(0).replace(rutas.get(0).split("/")[rutas.get(0).split("/").length - 1], "nuevo.PDF");
            for(int i=0; i<rutas.size();i++) {
            	 	File archivo = new File(CleanPath.cleanString(rutas.get(i)));
                 if(archivo.exists() && archivo.length()>0){
                	 	pdfs.add(new FileInputStream(rutas.get(i)));
                 }else {
                	 	logger.info("No se pudo agregar Archivo: "+rutas.get(i));
                 }
            		
            }
            
            OutputStream output = new FileOutputStream(rutaMerge);
            concatPDFs(pdfs, output, true);
        } catch (Exception e) {
        		logger.info("No se pudo eliminar generar el merge"+e.getMessage());
        }
		//Se eliminan Archivos temporales
		try {
			for(int i=0;i<graficaFile.length;i++) {
				File adjunto = new File(graficaFile[i].getAbsolutePath());
				adjunto.delete();
			}
			for(int i=0;i<rutas.size();i++) {
				File adjunto = new File(rutas.get(i));
				adjunto.delete();
			}
		} catch (Exception e) {
			logger.info("No se pudo eliminar temporales"+e.getMessage());
		}
			
		return rutaMerge;
	}
	
	public static void concatPDFs(List<InputStream> streamOfPDFFiles,
            OutputStream outputStream, boolean paginate) {
 
        Document document = new Document();
        try {
            List<InputStream> pdfs = streamOfPDFFiles;
            List<PdfReader> readers = new ArrayList<PdfReader>();
            int totalPages = 0;
            Iterator<InputStream> iteratorPDFs = pdfs.iterator();
 
            while (iteratorPDFs.hasNext()) {
                InputStream pdf = iteratorPDFs.next();
                PdfReader pdfReader = new PdfReader(pdf);
                readers.add(pdfReader);
                totalPages += pdfReader.getNumberOfPages();
            }
 
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);
 
            document.open();
            PdfContentByte cb = writer.getDirectContent();
 
            PdfImportedPage page;
            int currentPageNumber = 0;
            int pageOfCurrentReaderPDF = 0;
            Iterator<PdfReader> iteratorPDFReader = readers.iterator();
 
            while (iteratorPDFReader.hasNext()) {
                PdfReader pdfReader = iteratorPDFReader.next();
 
                while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {
 
                    Rectangle rectangle = pdfReader.getPageSizeWithRotation(1);
                    document.setPageSize(rectangle);
                    document.newPage();
                    
                    pageOfCurrentReaderPDF++;
                    currentPageNumber++;
                    page = writer.getImportedPage(pdfReader,
                            pageOfCurrentReaderPDF);
                    
                    switch (rectangle.getRotation()) {
                    case 0:
                        cb.addTemplate(page, 1f, 0, 0, 1f, 0, 0);
                        break;
                    case 90:
                        cb.addTemplate(page, 0, -1f, 1f, 0, 0, pdfReader
                                .getPageSizeWithRotation(1).getHeight());
                        break;
                    case 180:
                        cb.addTemplate(page, -1f, 0, 0, -1f, 0, 0);
                        break;
                    case 270:
                        cb.addTemplate(page, 0, 1.0F, -1.0F, 0, pdfReader
                                .getPageSizeWithRotation(1).getWidth(), 0);
                        break;
                    default:
                        break;
                    }
                    if (paginate) {
                        cb.beginText();
                        cb.getPdfDocument().getPageSize();
                        //PdfContentByte.
                        cb.endText();
                    }
                    
                    
                }
                pageOfCurrentReaderPDF = 0;
            }
            
            outputStream.flush();
            document.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (document.isOpen())
                document.close();
            try {
                if (outputStream != null)
                    outputStream.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(String idBitacora) {
		this.idBitacora = idBitacora;
	}

	public String getNegocio() {
		return negocio;
	}

	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}

	public DecimalFormat getFormatDecimales() {
		return formatDecimales;
	}

	public void setFormatDecimales(DecimalFormat formatDecimales) {
		this.formatDecimales = formatDecimales;
	}

	public DecimalFormat getFormatEnteros() {
		return formatEnteros;
	}

	public void setFormatEnteros(DecimalFormat formatEnteros) {
		this.formatEnteros = formatEnteros;
	}

	public ExpansionContadorDTO getCalculoTotal() {
		return calculoTotal;
	}

	public void setCalculoTotal(ExpansionContadorDTO calculoTotal) {
		this.calculoTotal = calculoTotal;
	}

	public List<ExpansionContadorDTO> getCalculoZonas() {
		return calculoZonas;
	}

	public void setCalculoZonas(List<ExpansionContadorDTO> calculoZonas) {
		this.calculoZonas = calculoZonas;
	}

	public List<ZonaNegoExpDTO> getListafilaZonas() {
		return listafilaZonas;
	}

	public void setListafilaZonas(List<ZonaNegoExpDTO> listafilaZonas) {
		this.listafilaZonas = listafilaZonas;
	}

	public List<String> getImperdonables() {
		return imperdonables;
	}

	public void setImperdonables(List<String> imperdonables) {
		this.imperdonables = imperdonables;
	}

	public String[] getImgPond() {
		return imgPond;
	}

	public void setImgPond(String[] imgPond) {
		this.imgPond = imgPond;
	}

	public String[] getClasificacion() {
		return clasificacion;
	}

	public void setClasificacion(String[] clasificacion) {
		this.clasificacion = clasificacion;
	}

	public String[] getDetalleClasif() {
		return detalleClasif;
	}

	public void setDetalleClasif(String[] detalleClasif) {
		this.detalleClasif = detalleClasif;
	}

	public String[] getCuadroGrafica() {
		return cuadroGrafica;
	}

	public void setCuadroGrafica(String[] cuadroGrafica) {
		this.cuadroGrafica = cuadroGrafica;
	}

	public File[] getGraficaFile() {
		return graficaFile;
	}

	public void setGraficaFile(File[] graficaFile) {
		this.graficaFile = graficaFile;
	}

	public String[] getGeneraHTMLDetalleClasif() {
		return generaHTMLDetalleClasif;
	}

	public void setGeneraHTMLDetalleClasif(String[] generaHTMLDetalleClasif) {
		this.generaHTMLDetalleClasif = generaHTMLDetalleClasif;
	}

	public List<RepoFilExpDTO>[] getPregListClasif() {
		return pregListClasif;
	}

	public void setPregListClasif(List<RepoFilExpDTO>[] pregListClasif) {
		this.pregListClasif = pregListClasif;
	}

	public String getPaginaIndice() {
		return paginaIndice;
	}

	public void setPaginaIndice(String paginaIndice) {
		this.paginaIndice = paginaIndice;
	}

	public String getCadenaImperdonables() {
		return cadenaImperdonables;
	}

	public void setCadenaImperdonables(String cadenaImperdonables) {
		this.cadenaImperdonables = cadenaImperdonables;
	}

	public String getGeneraImagenBaner() {
		return generaImagenBaner;
	}

	public void setGeneraImagenBaner(String generaImagenBaner) {
		this.generaImagenBaner = generaImagenBaner;
	}

	public String getColorOperable() {
		return colorOperable;
	}

	public void setColorOperable(String colorOperable) {
		this.colorOperable = colorOperable;
	}

	public double getPreCalificacionNum() {
		return preCalificacionNum;
	}

	public void setPreCalificacionNum(double preCalificacionNum) {
		this.preCalificacionNum = preCalificacionNum;
	}

	public String getOperable() {
		return operable;
	}

	public void setOperable(String operable) {
		this.operable = operable;
	}

	public String getColorTexto() {
		return colorTexto;
	}

	public void setColorTexto(String colorTexto) {
		this.colorTexto = colorTexto;
	}

	public double getNumeroDeCalificacion() {
		return numeroDeCalificacion;
	}

	public void setNumeroDeCalificacion(double numeroDeCalificacion) {
		this.numeroDeCalificacion = numeroDeCalificacion;
	}

	public int getNumImperdonablesRecorrido() {
		return numImperdonablesRecorrido;
	}

	public void setNumImperdonablesRecorrido(int numImperdonablesRecorrido) {
		this.numImperdonablesRecorrido = numImperdonablesRecorrido;
	}

	public String getTotalItems() {
		return totalItems;
	}

	public void setTotalItems(String totalItems) {
		this.totalItems = totalItems;
	}

	public String getItemsPorRevisar() {
		return itemsPorRevisar;
	}

	public void setItemsPorRevisar(String itemsPorRevisar) {
		this.itemsPorRevisar = itemsPorRevisar;
	}

	public String getItemsAprobados() {
		return itemsAprobados;
	}

	public void setItemsAprobados(String itemsAprobados) {
		this.itemsAprobados = itemsAprobados;
	}

	public String getItemsNoConsiderados() {
		return itemsNoConsiderados;
	}

	public void setItemsNoConsiderados(String itemsNoConsiderados) {
		this.itemsNoConsiderados = itemsNoConsiderados;
	}

	public int getColorBaner() {
		return colorBaner;
	}

	public void setColorBaner(int colorBaner) {
		this.colorBaner = colorBaner;
	}

	public String getImgBaner() {
		return imgBaner;
	}

	public void setImgBaner(String imgBaner) {
		this.imgBaner = imgBaner;
	}
	

	

	
	
}
