package com.gruposalinas.checklist.domain;

public class FiltrosCecoDTO {

	private String idCeco;
	private String nombreCeco;
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	
	 
}
