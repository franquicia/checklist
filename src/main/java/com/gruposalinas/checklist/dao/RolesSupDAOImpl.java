package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.RolesSupDTO;
import com.gruposalinas.checklist.mappers.RolesSupRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class RolesSupDAOImpl extends DefaultDAO implements RolesSupDAO {

    private static Logger logger = LogManager.getLogger(RolesSupDAOImpl.class);

    DefaultJdbcCall jdbcInsertRoles;
    DefaultJdbcCall jdbcUpdateRoles;
    DefaultJdbcCall jdbcDeleteRoles;
    DefaultJdbcCall jdbcGetRoles;
    DefaultJdbcCall jdbcMergeRoles;

    public void init() {

        jdbcInsertRoles = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMROLESPERFIL")
                .withProcedureName("SPINSROLPERFIL");

        jdbcUpdateRoles = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMROLESPERFIL")
                .withProcedureName("SPACTROLPERFIL");

        jdbcDeleteRoles = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMROLESPERFIL")
                .withProcedureName("SPDELROLPERFIL");

        jdbcGetRoles = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMROLESPERFIL")
                .withProcedureName("SPGETROLPERFIL")
                .returningResultSet("PA_CONSULTA", new RolesSupRowMapper());

        jdbcMergeRoles = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMROLESPERFIL")
                .withProcedureName("SPMERGEROLPRFIL");
    }

    @Override
    public int insertRoles(int idUsuario, int idPerfil, int idMenu, int activo) throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario)
                .addValue("PA_FIPERFIL_ID", idPerfil)
                .addValue("PA_FIMENU_ID", idMenu)
                .addValue("PA_FIACTIVO", activo);

        out = jdbcInsertRoles.execute(in);
        logger.info("Función ejecutada: FRANQUICIA.PADMROLESPERFIL.SPINSROLPERFIL");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al insertar el rol");
        }
        return respuesta;
    }

    @Override
    public int updateRoles(int idUsuario, int idPerfil, int idMenu, String activo) throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario)
                .addValue("PA_FIPERFIL_ID", idPerfil)
                .addValue("PA_FIMENU_ID", idMenu)
                .addValue("PA_FIACTIVO", activo);

        out = jdbcUpdateRoles.execute(in);
        logger.info("Función ejecutada: FRANQUICIA.PADMROLESPERFIL.SPACTROLPERFIL");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al actualizar el rol");
        }
        return respuesta;
    }

    @Override
    public int deleteRoles(int idUsuario, int idPerfil, int idMenu) throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario)
                .addValue("PA_FIPERFIL_ID", idPerfil)
                .addValue("PA_FIMENU_ID", idMenu);

        out = jdbcDeleteRoles.execute(in);
        logger.info("Función ejecutada: FRANQUICIA.PADMROLESPERFIL.SPDELROLPERFIL");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al eliminar el rol");
        }
        return respuesta;
    }

    @Override
    public List<RolesSupDTO> getRoles(String idUsuario, String idPerfil, String idMenu) throws Exception {
        Map<String, Object> out = null;
        List<RolesSupDTO> respuesta = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario)
                .addValue("PA_FIPERFIL_ID", idPerfil)
                .addValue("PA_FIMENU_ID", idMenu);

        out = jdbcGetRoles.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADMROLESPERFIL.SPGETROLPERFIL");

        respuesta = (List<RolesSupDTO>) out.get("PA_CONSULTA");

        if (respuesta.size() == 0) {
            logger.info("Algo ocurrió al obtener los roles");
        }
        return respuesta;
    }

    @Override
    public int mergeRoles(int idUsuario, int idPerfil, int idMenu, int activo) throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario)
                .addValue("PA_FIPERFIL_ID", idPerfil)
                .addValue("PA_FIMENU_ID", idMenu)
                .addValue("PA_FIACTIVO", activo);

        out = jdbcMergeRoles.execute(in);
        logger.info("Función ejecutada: FRANQUICIA.PADMROLESPERFIL.SPMERGEROLPRFIL");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al realizar el merge del rol");
        }
        return respuesta;
    }

}
