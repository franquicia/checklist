package com.gruposalinas.checklist.business;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.AsignacionNewDAO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class AsignacionNewBI {
	
	@Autowired
	AsignacionNewDAO asignacionNewDAO;
	
	private Logger logger = LogManager.getLogger(AsignacionNewBI.class);
	
	public Map<String, Object> ejecutaAsignaciones(){
		
		Map<String, Object> respuesta = null;
		
		try {
			respuesta = asignacionNewDAO.ejecutaAsignaciones();
		} catch (Exception e) {
		 	logger.info("Ocurrio algo al ejecutar ejecutaAsignaciones ejecutaAsignaciones()");
				
		}
		
		return respuesta;		
	}
	
	public Map<String, Object>  asignacionEspecial(String userNew, String userOld){		
		
		Map<String, Object> respuesta = null;
		
		try {
			respuesta = asignacionNewDAO.asignacionEspecial(userNew, userOld);
		} catch (Exception e) {
			logger.info("Ocurrio algo al ejecutar asignacionEspecial asignacionEspecial()");
				
		}
		
		return respuesta;
	}
	
	public Map<String, Object> ejecutaAsignacion(String ceco, int puesto, int checklist){
		
		Map<String, Object> respuesta = null;
		
		try {
			respuesta = asignacionNewDAO.ejecutaAsignacion(ceco, puesto, checklist);
		} catch (Exception e) {
			logger.info("Ocurrio algo al ejecutar ejecutaAsignacion ejecutaAsignacion()");
				
		}
		
		return respuesta;
	}
	

}
