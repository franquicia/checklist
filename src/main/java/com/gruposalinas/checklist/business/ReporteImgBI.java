package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ReporteImgDAO;
import com.gruposalinas.checklist.domain.ReporteImgDTO;

public class ReporteImgBI {
		
	private static Logger logger = LogManager.getLogger(ReporteImgBI.class);
	
	@Autowired
	ReporteImgDAO consultaDAO;
	
	List<ReporteImgDTO> listaConsulta = null;

	public List<ReporteImgDTO> obtieneDetalleCeco(String idCeco, String fechaInicio, String fechaFin, String idChecklist){
	
		try {
			listaConsulta = consultaDAO.obtieneDetalleCeco(idCeco,fechaInicio,fechaFin,idChecklist);		
			} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}			
		
		return listaConsulta;
	
	}
	
	List<ReporteImgDTO> listaConsulta2 = null;

	public List<ReporteImgDTO> obtieneProtocolosZonas(int idGrupo){
		
		try {
			listaConsulta2 = consultaDAO.obtieneProtocolosZonas(idGrupo);
		} catch (Exception e) {
			logger.info(e);
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}			
		
		return listaConsulta2;
	
		}
	
	List<ReporteImgDTO> listaConsulta3 = null;

	public List<ReporteImgDTO> obtieneSucursales(){
		
		try {
			listaConsulta3 = consultaDAO.obtieneSucursales();
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}			
		
		return listaConsulta3;
	
		}
	
	List<ReporteImgDTO> listaImg = null;

	public List<ReporteImgDTO> getAcervo(String idBitacora, String idChecklist){
	
		try {
			listaImg = consultaDAO.obtieneAcervo(Integer.parseInt(idBitacora), Integer.parseInt(idChecklist));
			} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}			
		
		return listaImg;
	
	}
}
