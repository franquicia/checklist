package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ModuloDTO;

public class ModuloOfflineRowMapper implements RowMapper<ModuloDTO> {
	
	public ModuloDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ModuloDTO moduloDTO = new ModuloDTO();
		
		
		moduloDTO.setIdModulo(rs.getInt("FIID_MODULO"));
		moduloDTO.setNombre(rs.getString("FCNOMBRE"));
		moduloDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		moduloDTO.setIdModuloPadre(rs.getInt("FIID_MOD_PADRE"));
		moduloDTO.setNombrePadre(rs.getString("FCNOMBRE_PADRE"));
		
		return moduloDTO;
		
	}

}
