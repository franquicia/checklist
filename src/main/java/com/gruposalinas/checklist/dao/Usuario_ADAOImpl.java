package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.Usuario_ADTO;
import com.gruposalinas.checklist.mappers.UsuarioPasoRowMapper;
import com.gruposalinas.checklist.mappers.Usuario_ARowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class Usuario_ADAOImpl extends DefaultDAO implements Usuario_ADAO {

    private static Logger logger = LogManager.getLogger(Usuario_ADAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneUsuario;
    DefaultJdbcCall jdbcObtieneTodosPaso;
    DefaultJdbcCall jdbcObtieneUsuarioPaso;
    DefaultJdbcCall jdbcCargaUsuario;
    DefaultJdbcCall jdbcCargaPerfilUsuario;
    DefaultJdbcCall jdbcInsertaUsuario;
    DefaultJdbcCall jdbcActualizaUsuario;
    DefaultJdbcCall jdbcActualizaUsuarioCeco;
    DefaultJdbcCall jdbcEliminaUsuario;
    DefaultJdbcCall jdbcBajaUsuario;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_USUARIO")
                .withProcedureName("SP_SEL_G_USUARIO")
                .returningResultSet("RCL_USUARIO", new Usuario_ARowMapper());

        jdbcObtieneUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_USUARIO")
                .withProcedureName("SP_SEL_USUARIO")
                .returningResultSet("RCL_USUARIO", new Usuario_ARowMapper());

        jdbcObtieneTodosPaso = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PATABPASO")
                .withProcedureName("SP_SEL_G_USUARIOS")
                .returningResultSet("RCL_PUSUARIOS", new UsuarioPasoRowMapper());

        jdbcObtieneUsuarioPaso = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PATABPASO")
                .withProcedureName("SP_SEL_USUARIOS")
                .returningResultSet("RCL_PUSUARIOS", new UsuarioPasoRowMapper());

        jdbcCargaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAUSUARIO")
                .withProcedureName("SP_CARGA_USUARIOS");

        jdbcCargaPerfilUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAUSUARIO")
                .withProcedureName("SP_CARGA_PERFIL_USUA");

        jdbcInsertaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_USUARIO")
                .withProcedureName("SP_INS_USUARIO");

        jdbcActualizaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_USUARIO")
                .withProcedureName("SP_ACT_USUARIO");

        jdbcActualizaUsuarioCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_USUARIO")
                .withProcedureName("SP_ACT_USUARIO_CEC");

        jdbcEliminaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_USUARIO")
                .withProcedureName("SP_DEL_USUARIO");

        jdbcBajaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAUSUARIO")
                .withProcedureName("SP_BAJA_USUARIOS");
    }

    @SuppressWarnings("unchecked")
    public List<Usuario_ADTO> obtieneUsuario() throws Exception {
        Map<String, Object> out = null;
        List<Usuario_ADTO> listaUsuario = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_USUARIO.SP_SEL_G_USUARIO}");
        listaUsuario = (List<Usuario_ADTO>) out.get("RCL_USUARIO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener los Usuarios");
        } else {
            return listaUsuario;
        }

        return listaUsuario;
    }

    @SuppressWarnings("unchecked")
    public List<Usuario_ADTO> obtieneUsuario(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        List<Usuario_ADTO> listaUsuario = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario);

        out = jdbcObtieneUsuario.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_USUARIO.SP_SEL_USUARIO}");
        listaUsuario = (List<Usuario_ADTO>) out.get("RCL_USUARIO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener el Usuario con id(" + idUsuario + ")");
        } else {
            return listaUsuario;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<Usuario_ADTO> obtieneUsuarioPaso() throws Exception {
        Map<String, Object> out = null;
        List<Usuario_ADTO> listaUsuario = null;
        int error = 0;

        out = jdbcObtieneTodosPaso.execute();

        //logger.info("Funcion ejecutada: {checklist.PATABPASO.SP_SEL_G_USUARIOS}");
        listaUsuario = (List<Usuario_ADTO>) out.get("RCL_PUSUARIOS");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener los Usuarios");
        } else {
            return listaUsuario;
        }

        return listaUsuario;
    }

    @SuppressWarnings("unchecked")
    public List<Usuario_ADTO> obtieneUsuarioPaso(String idUsuario, String idPuesto, String idCeco) throws Exception {
        Map<String, Object> out = null;
        List<Usuario_ADTO> listaUsuario = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USUARIO", idUsuario)
                .addValue("PA_PUESTO", idPuesto).addValue("PA_IDCECO", idCeco);

        out = jdbcObtieneUsuarioPaso.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PATABPASO.SP_SEL_USUARIOS}");
        listaUsuario = (List<Usuario_ADTO>) out.get("RCL_PUSUARIOS");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener el Usuario con id(" + idUsuario + ")");
        } else {
            return listaUsuario;
        }

        return null;
    }

    public boolean cargaUsuarios() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaUsuario.execute();

        //logger.info("Funcion ejecutada: {checklist.PAUSUARIO.SP_CARGA_USUARIOS}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar la Carga de Usuarios");
        } else {
            return true;
        }

        return false;
    }

    public boolean cargaPerfilUsuarios() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaPerfilUsuario.execute();

        //logger.info("Funcion ejecutada: {checklist.PAUSUARIO.SP_CARGA_PERFIL_USUA}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar la Carga de Usuarios");
        } else {
            return true;
        }

        return false;
    }

    public boolean insertaUsuario(Usuario_ADTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", bean.getIdUsuario())
                .addValue("PA_IDPUESTO", bean.getIdPuesto())
                .addValue("PA_IDCECO", bean.getIdCeco())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_ACTIVO", bean.getActivo())
                .addValue("PA_FECHA", bean.getFecha());

        out = jdbcInsertaUsuario.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_USUARIO.SP_INS_USUARIO}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar el Usuario");
        } else {
            return true;
        }

        return false;
    }

    public boolean actualizaUsuario(Usuario_ADTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", bean.getIdUsuario())
                .addValue("PA_IDPUESTO", bean.getIdPuesto())
                .addValue("PA_IDCECO", bean.getIdCeco())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_ACTIVO", bean.getActivo())
                .addValue("PA_FECHA", bean.getFecha());

        out = jdbcActualizaUsuario.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_USUARIO.SP_ACT_USUARIO}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar el Usuario  id( " + bean.getIdUsuario() + ")");
        } else {
            return true;
        }

        return false;

    }

    public boolean actualizaUsuarioCeco(Usuario_ADTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", bean.getIdUsuario())
                .addValue("PA_IDCECO", bean.getIdCeco());

        out = jdbcActualizaUsuarioCeco.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_USUARIO.SP_ACT_USUARIO_CEC}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar el Usuario  id( " + bean.getIdUsuario() + ")");
        } else {
            return true;
        }

        return false;

    }

    public boolean eliminaUsuario(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_USUARIO", idUsuario);

        out = jdbcEliminaUsuario.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_USUARIO.SP_DEL_USUARIO}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al borrar el Usuario id(" + idUsuario + ")");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean bajaUsuarios() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcBajaUsuario.execute();

        //logger.info("Funcion ejecutada: {checklist.PAUSUARIO.SP_BAJA_USUARIOS}");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al cambiar el estatus de los usuarios");
        } else {
            return true;
        }

        return false;
    }
}
