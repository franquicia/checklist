package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.CanalDTO;

public class CanalRowMapper implements RowMapper<CanalDTO>{

	public CanalDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
	    
		CanalDTO canalDTO = new CanalDTO();
		
		canalDTO.setIdCanal(rs.getInt("FIID_CANAL"));
		canalDTO.setDescrpicion(rs.getString("FCDESCRIPCION"));
		canalDTO.setActivo(rs.getInt("FIACTIVO"));
		
		return canalDTO;
}
	
	

}
