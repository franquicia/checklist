package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.ReporteChecksExpDTO;

public class ReporteChecklistsExpRowMapper implements RowMapper<ReporteChecksExpDTO> {

	public ReporteChecksExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteChecksExpDTO rep = new ReporteChecksExpDTO();
		
		
		rep.setBitacora(rs.getInt("FIID_BITACORA"));
		rep.setIdcheck(rs.getInt("FIID_CHECKLIST"));
		rep.setCheckusua(rs.getInt("FIID_CHECK_USUA"));
		rep.setUsuario(rs.getInt("FIID_USUARIO"));
		rep.setCeco(rs.getString("FCID_CECO"));
		rep.setFecha(rs.getString("FDTERMINO"));
		
	
		return rep;
	}

}


