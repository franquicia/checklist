package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.EvidenciaDTO;

public interface EvidenciaDAO {
	
	public List<EvidenciaDTO> obtieneEvidencia() throws Exception;
	
	public List<EvidenciaDTO> obtieneEvidencia(int idEvidencia) throws Exception;
	
	public int insertaEvidencia(EvidenciaDTO bean) throws Exception;
	
	public boolean actualizaEvidencia(EvidenciaDTO bean) throws Exception;
	
	public boolean eliminaEvidencia(int idEvidencia) throws Exception;

}
