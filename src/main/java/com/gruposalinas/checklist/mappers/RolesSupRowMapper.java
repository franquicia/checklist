package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.RolesSupDTO;

public class RolesSupRowMapper implements RowMapper<RolesSupDTO> {

	public RolesSupDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		RolesSupDTO rolesDTO = new RolesSupDTO();
		
		rolesDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
		rolesDTO.setIdPerfil(rs.getInt("FIPERFIL_ID"));
		rolesDTO.setIdMenu(rs.getInt("FIMENU_ID"));
		rolesDTO.setActivo(rs.getInt("FIACTIVO"));
		rolesDTO.setUsuaModificado(rs.getString("FCUSUARIO_MOD"));
		rolesDTO.setFechaModificacion(rs.getString("FDFECHA_MOD"));
		
		return rolesDTO;
		
		
	}

}
