package com.gruposalinas.checklist.domain;

public class AdmInformProtocolosDTO {
	private String idsec;
	private String ceco;
	private String usuario;
	private String fecha;
	private String ruta;
	private String lider;
	private String firmaLider;
	private String numLider;
	private String firmaFrq;
	private String usuarioMod;
	private String fechaMod;
	private String retroAlimentacion;
	private String idProtocolo;
	public String getIdsec() {
		return idsec;
	}
	public void setIdsec(String idsec) {
		this.idsec = idsec;
	}
	public String getCeco() {
		return ceco;
	}
	public void setCeco(String ceco) {
		this.ceco = ceco;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	
	
	public String getLider() {
		return lider;
	}
	public void setLider(String lider) {
		this.lider = lider;
	}
	
	public String getFirmaLider() {
		return firmaLider;
	}
	
	public String getNumLider() {
		return numLider;
	}
	public void setNumLider(String numLider) {
		this.numLider = numLider;
	}
	public String getFirmaFrq() {
		return firmaFrq;
	}
	public void setFirmaFrq(String firmaFrq) {
		this.firmaFrq = firmaFrq;
	}
	public void setFirmaLider(String firmaLider) {
		this.firmaLider = firmaLider;
	}
	public String getUsuarioMod() {
		return usuarioMod;
	}
	public void setUsuarioMod(String usuarioMod) {
		this.usuarioMod = usuarioMod;
	}
	public String getFechaMod() {
		return fechaMod;
	}
	public void setFechaMod(String fechaMod) {
		this.fechaMod = fechaMod;
	}
	public String getRetroAlimentacion() {
		return retroAlimentacion;
	}
	public void setRetroAlimentacion(String retroAlimentacion) {
		this.retroAlimentacion = retroAlimentacion;
	}
	public String getIdProtocolo() {
		return idProtocolo;
	}
	public void setIdProtocolo(String idProtocolo) {
		this.idProtocolo = idProtocolo;
	}
}
