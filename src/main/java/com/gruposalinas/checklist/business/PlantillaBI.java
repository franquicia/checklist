package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.AppDAO;
import com.gruposalinas.checklist.dao.PlantillaDAO;
import com.gruposalinas.checklist.domain.AppPerfilDTO;
import com.gruposalinas.checklist.domain.PlantillaDTO;

public class PlantillaBI {

	private static Logger logger = LogManager.getLogger(AppBI.class);

	List<PlantillaDTO> listaPlantilla = null;
	
	@Autowired
	PlantillaDAO plantillaDAO;
	

	public int insertaPlantilla(PlantillaDTO bean){
		
		int idPlantilla = 0;
		
		try {
			idPlantilla = plantillaDAO.insertaPlantilla(bean);	
		} catch (Exception e) {
			logger.info("No fue posible insertar");
		}
		
		return idPlantilla;
		
	}
		
	public boolean actualizaPlantilla(PlantillaDTO bean) {
		
		boolean respuesta = false ;
		
		try {
			respuesta = plantillaDAO.actualizaPlantilla(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualiza");
		}
		
		return respuesta;
	}
	
	public boolean eliminaPlantilla(int idPlantilla) {
		
		boolean respuesta = false ;
		
		try {
			respuesta = plantillaDAO.eliminaPlantilla(idPlantilla);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
		}
		
		return respuesta;		
	}
	
	public List<PlantillaDTO> obtienePlantilla(String idPlantilla) {
		
		try {
			listaPlantilla = plantillaDAO.obtienePlantilla(idPlantilla);
			//System.out.println(listaPlantilla);
		} catch (Exception e) {
			logger.info("No fue posible consultar");
		}
				
		return listaPlantilla;		
	}


	public int insertaElementoP(PlantillaDTO bean) {
		
		int idCanal = 0;
		
		try {
			idCanal = plantillaDAO.insertaElementoP(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar");
		}
		
		return idCanal;
		
	}
		
	public boolean actualizaElementoP(PlantillaDTO bean) {
		
		boolean respuesta = false ;
		
		try {
			respuesta = plantillaDAO.actualizaElementoP(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
		}
		
		return respuesta;
	}
	
	public boolean eliminaElementoP(int idElemento) {
		
		boolean respuesta = false ;
		
		try {
			respuesta = plantillaDAO.eliminaElementoP(idElemento);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
		}
		
		return respuesta;		
	}
	
	public List<PlantillaDTO> obtieneElementoP(String idPlantilla, String idElemento, String tipo) {
		
		try {
			listaPlantilla = plantillaDAO.obtieneElementoP(idPlantilla, idElemento, tipo);
		} catch (Exception e) {
			logger.info("No fue posible consultar");
		}
				
		return listaPlantilla;		
	}
	

}
