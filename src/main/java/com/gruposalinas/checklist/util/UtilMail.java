package com.gruposalinas.checklist.util;

import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.checklist.business.LoginBI;
import com.gruposalinas.checklist.business.SoporteHallazgosTransfBI;
import com.gruposalinas.checklist.business.SucursalChecklistBI;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import com.gruposalinas.checklist.resources.FRQConstantes;

public class UtilMail {

	private static Logger logger = LogManager.getLogger(UtilMail.class);

	public static boolean enviaCorreo(String destinatario, List<String> copiados, String asunto, String cuerpoCorreo,
			File adjunto) {

		/*
		 * logger.info("DESTINTATARIO ---- " + destinatario);
		 * logger.info("COPIADOS ---- " + copiados); logger.info("ASUNTO ---- " +
		 * asunto);
		 */

		// logger.info("CUERPO CORREO ---- " + cuerpoCorreo);
		// log("LLEGANDO A METOD ENVIA MAIL.....");
		String mensaje = "";
		try {
			mensaje = cuerpoCorreo;
			// Propiedades de la conexion
			String host = FRQConstantes.CTE_HOST_CORREO;
			String from = FRQConstantes.CTE_REMITENTE_SG;
			Properties props = new Properties();
			props.put("mail.smtp.host", host);
			try {
				Session session = Session.getInstance(props, null);
				MimeMessage msg = new MimeMessage(session);
				// Se setea la direccion de la que viene el correo
				msg.setFrom(new InternetAddress(from, "Sistemas - Checklist."));
				// Se setea para quién va el correo
				msg.setRecipients(Message.RecipientType.TO, destinatario);
				// Se setea para quienes se copia el correo
				String copia = "";

				for (String dest : copiados) {
					copia += dest + ",";
				}
				if (copia != null && copia != "") {
					msg.setRecipients(Message.RecipientType.CC, copia);
				}
				// Se setea el Asunto del correo
				msg.setSubject(MimeUtility.encodeText(asunto, "utf-8", "B"));
				// Se setea el mensaje y el tipo de texto
				// creates body part for the message
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(cuerpoCorreo, "text/html");
				msg.setContent(mensaje, "text/html; charset=iso-8859-1");
				// Adjuntar documento
				// JavaMail 1.4
				if (adjunto != null) {
					Multipart multipart = new MimeMultipart();
					// creates body part for the attachment
					MimeBodyPart attachPart = new MimeBodyPart();
					// code to add attachment...will be revealed later
					// adds parts to the multipart
					String attachFile = adjunto.getAbsolutePath();
					// multipart.addBodyPart(attachPart);
					multipart.addBodyPart(messageBodyPart);
					attachPart.attachFile(attachFile);
					// attachPart.attachFile(adjunto.getName().substring(0, 10));
					multipart.addBodyPart(attachPart);
					// sets the multipart as message's content
					msg.setContent(multipart);
				}
				// Se envía el correo
				Transport.send(msg);
				if (adjunto != null) {
					adjunto.delete();
					adjunto.deleteOnExit();
				}

			} catch (Exception e) {
				logger.info("Algo paso CATCH ANIDADO... " + e);
				e.printStackTrace();
				return false;
			}
		} catch (Exception e) {
			logger.info("Algo paso CATCH GENERAL... " + e);
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean enviaCorreosVisitas(List<String> destinatarios, List<String> copiados, String asunto,
			String cuerpoCorreo, File adjunto) {

		String mensaje = "";
		try {
			mensaje = cuerpoCorreo;
			// Propiedades de la conexion
			String host = FRQConstantes.CTE_HOST_CORREO; // 10.63.200.79
			String from = FRQConstantes.CTE_REMITENTE_VISITAS_SG;
			Properties props = new Properties();
			props.put("mail.smtp.host", host);

			try {
				Session session = Session.getInstance(props, null);
				MimeMessage msg = new MimeMessage(session);
				// Se setea la direccion de la que viene el correo
				msg.setFrom(new InternetAddress(from, "Visita de Sistemas - Banco Azteca"));

				// Se setea para quién va el correo
				String destinatario = "";

				for (String dest : destinatarios) {
					destinatario += dest + ",";
				}
				logger.info("destinatario... " + destinatario);
				if (destinatario != null && destinatario != "") {
					msg.setRecipients(Message.RecipientType.TO, destinatario);
				}

				// Se setea para quienes se copia el correo
				String copia = "";

				for (String copy : copiados) {
					copia += copy + ",";
				}
				logger.info("copia... " + copia);
				if (copia != null && copia != "") {
					msg.setRecipients(Message.RecipientType.BCC, copia);
				}

				// Se setea el Asunto del correo
				msg.setSubject(MimeUtility.encodeText(asunto, "utf-8", "B"));

				// Se setea el mensaje y el tipo de texto
				// creates body part for the message
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(cuerpoCorreo, "text/html");
				msg.setContent(mensaje, "text/html; charset=iso-8859-1");
				// Adjuntar documento
				// JavaMail 1.4
				if (adjunto != null) {
					Multipart multipart = new MimeMultipart();
					// creates body part for the attachment
					MimeBodyPart attachPart = new MimeBodyPart();
					// code to add attachment...will be revealed later
					// adds parts to the multipart
					String attachFile = adjunto.getAbsolutePath();
					// multipart.addBodyPart(attachPart);
					multipart.addBodyPart(messageBodyPart);
					attachPart.attachFile(attachFile);
					// attachPart.attachFile(adjunto.getName().substring(0, 10));
					multipart.addBodyPart(attachPart);
					// sets the multipart as message's content
					msg.setContent(multipart);
				}
				// Se envía el correo

				Transport.send(msg);

				if (adjunto != null) {
					adjunto.delete();
					adjunto.deleteOnExit();
				}

			} catch (Exception e) {
				logger.info("Algo paso CATCH ANIDADO... " + e);
				e.printStackTrace();
				return false;
			}
		} catch (Exception e) {
			logger.info("Algo paso CATCH GENERAL... " + e);
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean enviaCorreoExpansion(String destinatario, List<String> copiados, String asunto,
			String cuerpoCorreo, File adjunto, int ceco, int fase, int proyecto) {

		String mensaje = "";
		mensaje += "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><html>" + "<style> </style>";
		mensaje += "<body><a href=http://10.53.33.83/checklist/expansionCoreServices/generaHallazgos.json?ceco="+ceco+"&proyecto="+proyecto+"&fase="+fase+">Descarga de hallazgos</a></body></html>";
		

		try {
			// Propiedades de la conexion
			String host = FRQConstantes.CTE_HOST_CORREO;
			String from = FRQConstantes.CTE_REMITENTE_EXPANSION_SG;
			Properties props = new Properties();
			props.put("mail.smtp.host", host);
			try {
				Session session = Session.getInstance(props, null);
				MimeMessage msg = new MimeMessage(session);
				// Se setea la direccion de la que viene el correo
				msg.setFrom(new InternetAddress(from, "Checklist - Expansión"));
				// Se setea para quién va el correo
				msg.setRecipients(Message.RecipientType.TO, destinatario);
				// Se setea para quienes se copia el correo
				String copia = "";

				for (String dest : copiados) {
					copia += dest + ",";
				}
				if (copia != null && copia != "") {
					msg.setRecipients(Message.RecipientType.BCC, copia);
				}
				// Se setea el Asunto del correo
				msg.setSubject(MimeUtility.encodeText(asunto, "utf-8", "B"));
				// Se setea el mensaje y el tipo de texto
				// creates body part for the message
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(mensaje, "text/html");
				msg.setContent(mensaje, "text/html; charset=iso-8859-1");
				// Adjuntar documento
				// JavaMail 1.4
				if (adjunto != null) {
					Multipart multipart = new MimeMultipart();
					// creates body part for the attachment
					MimeBodyPart attachPart = new MimeBodyPart();
					// code to add attachment...will be revealed later
					// adds parts to the multipart
					String attachFile = adjunto.getAbsolutePath();
					// multipart.addBodyPart(attachPart);
					multipart.addBodyPart(messageBodyPart);
					attachPart.attachFile(attachFile);
					// attachPart.attachFile(adjunto.getName().substring(0, 10));
					multipart.addBodyPart(attachPart);
					// sets the multipart as message's content
					msg.setContent(multipart);
				}
				// Se envía el correo
				Transport.send(msg);
				if (adjunto != null) {
					adjunto.delete();
					adjunto.deleteOnExit();
				}

			} catch (Exception e) {
				logger.info("Algo paso CATCH ANIDADO... " + e);
				e.printStackTrace();
				return false;
			}
		} catch (Exception e) {
			logger.info("Algo paso CATCH GENERAL... " + e);
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean enviaCorreoAlexander(String destinatario, List<String> copiados, String asunto,
			String cuerpoCorreo, File adjunto) {

		/*
		 * logger.info("DESTINTATARIO ---- " + destinatario);
		 * logger.info("COPIADOS ---- " + copiados); logger.info("ASUNTO ---- " +
		 * asunto);
		 */

		// logger.info("CUERPO CORREO ---- " + cuerpoCorreo);
		// log("LLEGANDO A METOD ENVIA MAIL.....");
		String mensaje = "";
		try {
			mensaje = cuerpoCorreo;
			// Propiedades de la conexion
			String host = FRQConstantes.CTE_HOST_CORREO;
			String from = FRQConstantes.CTE_REMITENTE_SG;
			Properties props = new Properties();
			props.put("mail.smtp.host", host);
			try {
				Session session = Session.getInstance(props, null);
				MimeMessage msg = new MimeMessage(session);
				// Se setea la direccion de la que viene el correo
				msg.setFrom(new InternetAddress(from, "Checklist - Expansión"));
				// Se setea para quién va el correo
				msg.setRecipients(Message.RecipientType.TO, destinatario);
				// Se setea para quienes se copia el correo
				String copia = "";

				for (String dest : copiados) {
					copia += dest + ",";
				}
				if (copia != null && copia != "") {
					msg.setRecipients(Message.RecipientType.BCC, copia);
				}
				// Se setea el Asunto del correo
				msg.setSubject(MimeUtility.encodeText(asunto, "utf-8", "B"));
				// Se setea el mensaje y el tipo de texto
				// creates body part for the message
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(cuerpoCorreo, "text/html");
				msg.setContent(mensaje, "text/html; charset=iso-8859-1");
				// Adjuntar documento
				// JavaMail 1.4
				if (adjunto != null) {
					Multipart multipart = new MimeMultipart();
					// creates body part for the attachment
					MimeBodyPart attachPart = new MimeBodyPart();
					// code to add attachment...will be revealed later
					// adds parts to the multipart
					String attachFile = adjunto.getAbsolutePath();
					// multipart.addBodyPart(attachPart);
					multipart.addBodyPart(messageBodyPart);
					attachPart.attachFile(attachFile);
					// attachPart.attachFile(adjunto.getName().substring(0, 10));
					multipart.addBodyPart(attachPart);
					// sets the multipart as message's content
					msg.setContent(multipart);
				}
				// Se envía el correo
				Transport.send(msg);
				if (adjunto != null) {
					adjunto.delete();
					adjunto.deleteOnExit();
				}

			} catch (Exception e) {
				logger.info("Algo paso CATCH ANIDADO... " + e);
				e.printStackTrace();
				return false;
			}
		} catch (Exception e) {
			logger.info("Algo paso CATCH GENERAL... " + e);
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean enviaCorreoTransformacion(List<String> destinatarios, List<String> copiados, String asunto,
			String cuerpoCorreo, File adjunto) {

		String mensaje = "";
		try {
			mensaje = cuerpoCorreo;
			// Propiedades de la conexion
			String host = FRQConstantes.CTE_HOST_CORREO;
			String from = FRQConstantes.CTE_REMITENTE_EXPANSION_SG;
			Properties props = new Properties();
			props.put("mail.smtp.host", host);
			try {
				Session session = Session.getInstance(props, null);
				MimeMessage msg = new MimeMessage(session);
				// Se setea la direccion de la que viene el correo
				msg.setFrom(new InternetAddress(from, "Protocolo - Transformación"));

				// Se setea para quién va el correo
				String destinatario = "";

				for (String dest : destinatarios) {
					destinatario += dest + ",";
				}

				if (destinatario != null && destinatario != "") {
					msg.setRecipients(Message.RecipientType.TO, destinatario);
				}

				// Se setea para quién va el correo
				msg.setRecipients(Message.RecipientType.TO, destinatario);
				// Se setea para quienes se copia el correo
				String copia = "";

				for (String dest : copiados) {
					copia += dest + ",";
				}
				if (copia != null && copia != "") {
					msg.setRecipients(Message.RecipientType.BCC, copia);
				}
				// Se setea el Asunto del correo
				msg.setSubject(MimeUtility.encodeText(asunto, "utf-8", "B"));
				// Se setea el mensaje y el tipo de texto
				// creates body part for the message
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(cuerpoCorreo, "text/html");
				msg.setContent(mensaje, "text/html; charset=iso-8859-1");
				// Adjuntar documento
				// JavaMail 1.4
				if (adjunto != null) {
					Multipart multipart = new MimeMultipart();
					// creates body part for the attachment
					MimeBodyPart attachPart = new MimeBodyPart();
					// code to add attachment...will be revealed later
					// adds parts to the multipart
					String attachFile = adjunto.getAbsolutePath();
					// multipart.addBodyPart(attachPart);
					multipart.addBodyPart(messageBodyPart);
					attachPart.attachFile(attachFile);
					// attachPart.attachFile(adjunto.getName().substring(0, 10));
					multipart.addBodyPart(attachPart);
					// sets the multipart as message's content
					msg.setContent(multipart);
				}
				// Se envía el correo
				Transport.send(msg);
				if (adjunto != null) {
					adjunto.delete();
					adjunto.deleteOnExit();
				}

			} catch (Exception e) {
				logger.info("Algo paso CATCH ANIDADO... " + e);
				e.printStackTrace();
				return false;
			}
		} catch (Exception e) {
			logger.info("Algo paso CATCH GENERAL... " + e);
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static void enviaCorreoMergeCecos(String destinatario, String asunto, String cuerpoCorreo) {

		String mensaje = "";
		try {
			mensaje = cuerpoCorreo;
			// Propiedades de la conexion
			String host = FRQConstantes.CTE_HOST_CORREO;
			String from = FRQConstantes.CTE_REMITENTE_CARGA_CECOS;
			Properties props = new Properties();
			props.put("mail.smtp.host", host);
			try {
				Session session = Session.getInstance(props, null);
				MimeMessage msg = new MimeMessage(session);
				// Se setea la direccion de la que viene el correo
				msg.setFrom(new InternetAddress(from, "Carga - Cecos."));
				// Se setea para quién va el correo
				msg.setRecipients(Message.RecipientType.TO, destinatario);
				// Se setea para quienes se copia el correo

				// Se setea el Asunto del correo
				msg.setSubject(MimeUtility.encodeText(asunto, "utf-8", "B"));
				// Se setea el mensaje y el tipo de texto
				// creates body part for the message
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(cuerpoCorreo, "text/html");
				msg.setContent(mensaje, "text/html; charset=iso-8859-1");
				// Se envía el correo
				Transport.send(msg);

			} catch (Exception e) {
				logger.info("Algo paso CATCH ANIDADO... " + e);
				e.printStackTrace();
			}
		} catch (Exception e) {
			logger.info("Algo paso CATCH GENERAL... " + e);
			e.printStackTrace();
		}
	}

	public static boolean enviaCorreoExpansionNotificacion(List<String> destinatarios, List<String> copiados,
			String asunto, String cuerpoCorreo, File adjunto, int idCeco, int idFase, int idProyecto) {

		// Obtenemos el nombre de la fase
		String nombreFase = "";
		String nombreCeco = "";
		String mensaje = "";

		try {
			SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
					.getApplicationContext().getBean("soporteHallazgosTransfBI");
			String jsonString = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(idCeco + "", idFase + "",
					idProyecto + "");
			JsonParser parser = new JsonParser();
			JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
			JsonArray lista = jsonObj.getAsJsonArray("listaOrdenFase");
			if (lista != null && lista.size() > 0) {
				JsonObject obj = (JsonObject) lista.get(0).getAsJsonArray().get(0);
				nombreFase = obj.get("nombreFase").getAsString();
			}
		} catch (Exception e) {
			logger.info("AP al obtener el nombre de la Fase para el correo " + e);
		}
		
		try {
			SucursalChecklistBI sucursalChecklistBI = (SucursalChecklistBI) FRQAppContextProvider
					.getApplicationContext().getBean("sucursalChecklistBI");
			
			nombreCeco = sucursalChecklistBI.obtieneDatosSuc(idCeco+"").get(0).getNombre();
				
		} catch (Exception e) {
			logger.info("AP al obtener el nombre del ceco para el correo " + e);
		}
		
		mensaje += "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><html>" + "<style> </style>";
		mensaje += "<body><h3>Los hallazgos de la sucursal <strong>" + idCeco + "-" + nombreCeco
				+ "</strong> correspondientes a la Fase: <strong>" + nombreFase
				+ "</strong> se atendieron por completo</h3></body></html>";

		try {
			// Propiedades de la conexion
			String host = FRQConstantes.CTE_HOST_CORREO;
			String from = FRQConstantes.CTE_REMITENTE_EXPANSION_SG;
			Properties props = new Properties();
			props.put("mail.smtp.host", host);
			try {
				Session session = Session.getInstance(props, null);
				MimeMessage msg = new MimeMessage(session);
				// Se setea la direccion de la que viene el correo
				msg.setFrom(new InternetAddress(from, "Protocolo - Infraestructura"));

				// Se setea para quién va el correo
				String destinatario = "";

				for (String dest : destinatarios) {
					destinatario += dest + ",";
				}

				if (destinatario != null && destinatario != "") {
					msg.setRecipients(Message.RecipientType.TO, destinatario);
				}

				// Se setea para quién va el correo
				msg.setRecipients(Message.RecipientType.TO, destinatario);
				// Se setea para quienes se copia el correo
				String copia = "";

				for (String dest : copiados) {
					copia += dest + ",";
				}
				if (copia != null && copia != "") {
					msg.setRecipients(Message.RecipientType.BCC, copia);
				}
				// Se setea el Asunto del correo
				msg.setSubject(MimeUtility.encodeText(asunto, "utf-8", "B"));
				// Se setea el mensaje y el tipo de texto
				// creates body part for the message
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(cuerpoCorreo, "text/html");
				msg.setContent(mensaje, "text/html; charset=iso-8859-1");
				// Adjuntar documento
				// JavaMail 1.4
				if (adjunto != null) {
					Multipart multipart = new MimeMultipart();
					// creates body part for the attachment
					MimeBodyPart attachPart = new MimeBodyPart();
					// code to add attachment...will be revealed later
					// adds parts to the multipart
					String attachFile = adjunto.getAbsolutePath();
					// multipart.addBodyPart(attachPart);
					multipart.addBodyPart(messageBodyPart);
					attachPart.attachFile(attachFile);
					// attachPart.attachFile(adjunto.getName().substring(0, 10));
					multipart.addBodyPart(attachPart);
					// sets the multipart as message's content
					msg.setContent(multipart);
				}
				// Se envía el correo
				Transport.send(msg);
				if (adjunto != null) {
					adjunto.delete();
					adjunto.deleteOnExit();
				}

			} catch (Exception e) {
				logger.info("Algo paso CATCH ANIDADO... " + e);
				e.printStackTrace();
				return false;
			}
		} catch (Exception e) {
			logger.info("Algo paso CATCH GENERAL... " + e);
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static boolean enviaCorreoCeroHallazgos(List<String> destinatarios, List<String> copiados,
			String asunto, String cuerpoCorreo, File adjunto, int idCeco, int idFase, int idProyecto) {

		// Obtenemos el nombre de la fase
		String nombreFase = "";
		String nombreCeco = "";
		String mensaje = "";

		try {
			SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
					.getApplicationContext().getBean("soporteHallazgosTransfBI");
			String jsonString = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(idCeco + "", idFase + "",
					idProyecto + "");
			JsonParser parser = new JsonParser();
			JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
			JsonArray lista = jsonObj.getAsJsonArray("listaOrdenFase");
			if (lista != null && lista.size() > 0) {
				JsonObject obj = (JsonObject) lista.get(0).getAsJsonArray().get(0);
				nombreFase = obj.get("nombreFase").getAsString();
			}
		} catch (Exception e) {
			logger.info("AP al obtener el nombre de la Fase para el correo " + e);
		}
		
		try {
			SucursalChecklistBI sucursalChecklistBI = (SucursalChecklistBI) FRQAppContextProvider
					.getApplicationContext().getBean("sucursalChecklistBI");
			
			nombreCeco = sucursalChecklistBI.obtieneDatosSuc(idCeco+"").get(0).getNombre();
				
		} catch (Exception e) {
			logger.info("AP al obtener el nombre del ceco para el correo " + e);
		}
		
		mensaje += "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><html>" + "<style> </style>";
		mensaje += "<body><h3>Los hallazgos de la sucursal <strong>" + idCeco + "-" + nombreCeco
				+ "</strong> correspondientes a la Fase: <strong>" + nombreFase
				+ "</strong> se atendieron por completo</h3></body></html>";

		try {
			// Propiedades de la conexion
			String host = FRQConstantes.CTE_HOST_CORREO;
			String from = FRQConstantes.CTE_REMITENTE_EXPANSION_SG;
			Properties props = new Properties();
			props.put("mail.smtp.host", host);
			try {
				Session session = Session.getInstance(props, null);
				MimeMessage msg = new MimeMessage(session);
				// Se setea la direccion de la que viene el correo
				msg.setFrom(new InternetAddress(from, "Protocolo - Infraestructura"));

				// Se setea para quién va el correo
				String destinatario = "";

				for (String dest : destinatarios) {
					destinatario += dest + ",";
				}

				if (destinatario != null && destinatario != "") {
					msg.setRecipients(Message.RecipientType.TO, destinatario);
				}

				// Se setea para quién va el correo
				msg.setRecipients(Message.RecipientType.TO, destinatario);
				// Se setea para quienes se copia el correo
				String copia = "";

				for (String dest : copiados) {
					copia += dest + ",";
				}
				if (copia != null && copia != "") {
					msg.setRecipients(Message.RecipientType.BCC, copia);
				}
				// Se setea el Asunto del correo
				msg.setSubject(MimeUtility.encodeText(asunto, "utf-8", "B"));
				// Se setea el mensaje y el tipo de texto
				// creates body part for the message
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(cuerpoCorreo, "text/html");
				msg.setContent(mensaje, "text/html; charset=iso-8859-1");
				// Adjuntar documento
				// JavaMail 1.4
				if (adjunto != null) {
					Multipart multipart = new MimeMultipart();
					// creates body part for the attachment
					MimeBodyPart attachPart = new MimeBodyPart();
					// code to add attachment...will be revealed later
					// adds parts to the multipart
					String attachFile = adjunto.getAbsolutePath();
					// multipart.addBodyPart(attachPart);
					multipart.addBodyPart(messageBodyPart);
					attachPart.attachFile(attachFile);
					// attachPart.attachFile(adjunto.getName().substring(0, 10));
					multipart.addBodyPart(attachPart);
					// sets the multipart as message's content
					msg.setContent(multipart);
				}
				// Se envía el correo
				Transport.send(msg);
				if (adjunto != null) {
					adjunto.delete();
					adjunto.deleteOnExit();
				}

			} catch (Exception e) {
				logger.info("Algo paso CATCH ANIDADO... " + e);
				e.printStackTrace();
				return false;
			}
		} catch (Exception e) {
			logger.info("Algo paso CATCH GENERAL... " + e);
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
