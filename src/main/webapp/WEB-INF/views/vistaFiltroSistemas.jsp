<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="es">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
<title>Sistemas Captaci�n</title>
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/secciones.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/paginador.css" media="screen">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/calendario.css" media="screen">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/estiloVentana.css" media="screen">

<style type="text/css">
#tags {
	width: 100%;
	border: none;
	border-bottom: 1px solid #d7d7d7;
}
</style>

<script type="text/javascript">
	var idCecoBusquedaL = '${area}';
	var selCheckL = '${tipo}';
</script>

</head>

<body>

	<div class="page">
		<div class="clear"></div>

		<div class="title" id="title">
			<div class="wrapper">
				<div class="h2">
					<a href="/checklist/central/vistaSupervisionSistemas.htm">P�gina
						Principal </a> / <a> Supervisi�n Sistemas</a>
				</div>
				<h1>
					<c:out value="${nombreCeco}" />
				</h1>
			</div>			
		</div>

		<div class="clear"></div>
		
		<!-- Contenido -->
		<div class="contHome">
			<form id="miForm" name="miForm" action="" method="get"
				onsubmit="return valida(this)">
				<table class="tblBusquedas">
					<tbody>
						<tr>
							<td>Per�odo:</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>							
							<td style="font-size:12px;">Desde</td>
							<td>Hasta</td>
							<td>Cheklist</td>
							<td>Sucursales</td>
						</tr>
						<tr>							
							<td><input type="text" placeholder="Fecha"
								class="datapicker1 date" id="datepicker2"></td>
							<td><input type="text" placeholder="Fecha"
								class="datapicker1 date" id="datepicker3"></td>
							<td><select class="normal_select" id="selCheck">
									<option value="">Seleccionar checklist</option>
									<c:if test="${fn:length(listaFiltros) > 0}">
										<c:forEach var="k" begin="0" end="${(fn:length(listaFiltros))-1}">
											<option value="${listaFiltros[k].idCecos}">${listaFiltros[k].nombreCeco}</option>
										</c:forEach>
									</c:if>
							</select></td>
							<td>
								<div class="ui-widget buscar1">
									<input id="tags" class="buscar1" placeholder="Buscar sucursal">
									<input type="submit" value="" class="btnbuscar" />
								</div>							
							</td>							
						</tr>						
					</tbody>
				</table>
			</form>
			<div class="titulo titFecha">
				Resultados de la b�squeda<span id="periodoBusqueda"></span>
			</div>
			<div class="titulo titChecklist">Resultados de la b�squeda por
				Cheklist - Caja</div>
			<div class="titulo titSucursal">
				<div>
					Sucursal:<span id="nombreSucursal"> BA San Miguel Allende</span>
				</div>
				<div>
					Evaluado por:<span id="nombreEvaluador"> Alberto Antonio
						Ch�vez Cedillo</span>
				</div>
				<div>
					Fecha:<span id="fechaEvaluacion"> 19/02/2018</span>
				</div>
			</div>
			<div id="paginationdemo" class="demo">
				<!-- Pagina 1 -->
				<div id="p1" class="pagedemo _current" style="display: block;">

					<table class="tblGeneral" id="busqueda">
						<tbody>
							<c:if test="${fn:length(listaReportesSicap) > 0}">
								<c:forEach var="i" begin="0" end="${(fn:length(listaReportesSicap))-1}">
									<tr style='cursor: pointer; height: 50px;'
										onclick='detalleBitacora("${listaReportesSicap[i].idBitacora}","${listaReportesSicap[i].sucursal}")'>
										<td>${listaReportesSicap[i].finicio}</td>
										<td>${listaReportesSicap[i].evaluado}</td>
										<td>${listaReportesSicap[i].sucursal}</td>
										<td>${listaReportesSicap[i].region}</td>
										<td>${listaReportesSicap[i].checklist}</td>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
				</div>
				<div id="p2" class="pagedemo">Page 2</div>
				<div id="p3" class="pagedemo">Page 3</div>

				<div id="demo5" class="jPaginate">
					<div class="jPag-control-back">
						<a class="jPag-first">First</a> <span class="jPag-sprevious">�</span>
					</div>
					<div style="overflow: hidden; width: 186px;">
						<ul class="jPag-pages" style="width: 273px;">
							<li><span class="jPag-current">1</span></li>
							<li><a>2</a></li>
							<li><a>3</a></li>
					</div>
					<div class="jPag-control-front" style="left: 266px;">
						<span class="jPag-snext">�</span> <a class="jPag-last">Last</a>
					</div>

				</div>
				<!-- <a href="#" class="btnG">Exportar </a>  -->
			</div>
			<div class="clear"></div>
			<div class="divSucursal">
				<div class="colS1">
					<table class="tblGeneral p4" id=tablaPreguntas style="border: none !important;">
						<tbody>
					</table>
					<div class="clear"></div>
					<br>
					<textarea placeholder="Comentarios adicionales"></textarea>
				</div>
				<div class="colS2" id="contenedorEvidencia"
					style="width: 49%; height: 450px; overflow: scroll;">				
				</div>
			</div>

			<div class="clear"></div>
			<div class="aviso"></div>

		</div>

		<div class="clear"></div>
		<div class="footer1">
			<table class="tblFooter1">
				<tbody>
					<tr>
						<td style="width: 200px;">Sistema Reportes</td>
						<td style="width: 200px;"><a href="#" class="btnRojo">Malas Pr&aacute;cticas</a></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="footer">
			<table class="tblFooter">
				<tbody>
					<tr>
						<td style="width: 200px;">Banco Azteca S.A. Instituci&oacute;n de Banca M&uacute;ltiple</td>
						<td style="width: 200px;">Derechos Reservados 2014 (T&eacute;rminos y Condiciones de uso
							del Portal.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>
<script type="text/javascript" src="../js/supervisionSistemas/funciones.js"></script> 
<script type="text/javascript">

$(function() {
	$("#demo5").paginate({
		count 		: 3,
		start 		: 1,
		display     : 3,
		border					: false,
		border_color			: false,
		text_color  			: '#8a8a8a',
		background_color    	: 'transparent',	
		border_hover_color		: 'transparent',
		text_hover_color  		: '#000',
		background_hover_color	: 'transparent', 
		images					: true,
		mouse					: 'press',
		onChange     			: function(page){
									$('._current','#paginationdemo').removeClass('_current').hide();
									$('#p'+page).addClass('_current').show();
								  }
	});
	
	<c:if test="${fn:length(listaReportesSicap) > 0}">
		carga();	
		$(".titFecha").show();
		$("#paginationdemo").show();
	</c:if>
	<c:if test="${fn:length(listaReportesSicap)== 0}">
		$(".aviso").text('No existen resultados para esa directiva');
	</c:if>
	
	$( "#tags" ).autocomplete({
	      source: '../ajaxFiltroAutocompletaSucursal.json'
	    });
	
});
</script>