package com.gruposalinas.checklist.domain;

public class AdmHandbookDTO {
	private String idBook;
	private String descripcion;
	private String ruta;
	private String activo;
	private String padre;
	private String tipo;
	private String usuarioMod;
	private String fechaMod;
	private String fecha;
	public String getIdBook() {
		return idBook;
	}
	public void setIdBook(String idBook) {
		this.idBook = idBook;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getActivo() {
		return activo;
	}
	public void setActivo(String activo) {
		this.activo = activo;
	}
	public String getPadre() {
		return padre;
	}
	public void setPadre(String padre) {
		this.padre = padre;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
	public String getUsuarioMod() {
		return usuarioMod;
	}
	public void setUsuarioMod(String usuarioMod) {
		this.usuarioMod = usuarioMod;
	}
	public String getFechaMod() {
		return fechaMod;
	}
	public void setFechaMod(String fechaMod) {
		this.fechaMod = fechaMod;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	@Override
	public String toString() {
		return "AdmHandbookDTO [idBook=" + idBook + ", descripcion=" + descripcion + ", ruta=" + ruta + ", activo="
				+ activo + ", padre=" + padre + ", tipo=" + tipo + ", usuarioMod=" + usuarioMod + ", fechaMod="
				+ fechaMod + ", fecha=" + fecha + "]";
	}
	
	
	
}
