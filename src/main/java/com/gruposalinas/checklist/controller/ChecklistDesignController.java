package com.gruposalinas.checklist.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.business.ChecklistBI;
import com.gruposalinas.checklist.business.ChecklistTBI;
import com.gruposalinas.checklist.business.CorreoBI;
import com.gruposalinas.checklist.business.HorarioBI;
import com.gruposalinas.checklist.business.ModuloBI;
import com.gruposalinas.checklist.business.PaisBI;
import com.gruposalinas.checklist.business.PuestoBI;
import com.gruposalinas.checklist.business.TipoChecklistBI;
import com.gruposalinas.checklist.business.TipoPreguntaBI;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;

@Controller
public class ChecklistDesignController {

	@Autowired
	TipoChecklistBI tipoChecklistBI;
	@Autowired
	ModuloBI modulo;
	@Autowired
	HorarioBI horario;
	@Autowired
	TipoPreguntaBI tipoPregunta;
	//@Autowired
	List<PosiblesRespuestas>posiblesRespuestas;
	
	@Autowired
	ChecklistBI checklist;
	@Autowired
	PuestoBI puestoBI;
	@Autowired
	CecoBI cecoBI;
	@Autowired
	PaisBI paisBI;
	@Autowired
	ChecklistTBI checkT;
	@Autowired
	CorreoBI correobi;
	
	private static final Logger logger = LogManager.getLogger(ChecklistDesignController.class);

	@RequestMapping(value={"tienda/checklistDesign.htm","central/checklistDesign.htm"},method=RequestMethod.GET)
	public ModelAndView checkListDesign(HttpServletRequest requqest,HttpServletResponse response,Model model)
			throws ServletException, IOException{

		
		ModelAndView mv = new ModelAndView("checklistDesign");
		mv.addObject("listaTipoCheck", tipoChecklistBI.obtieneTipoChecklist());
		mv.addObject("listaModulos",modulo.obtieneModulo());
		mv.addObject("horarios", horario.obtieneHorario());
		mv.addObject("tiposPregunta",tipoPregunta.obtieneTipoPregunta());
		
		return mv;
		
	}
	
	@RequestMapping(value = "/ajaxTiposPregunta", method = RequestMethod.GET)
	public @ResponseBody List<PosiblesRespuestas> getTiposPregunta(HttpServletRequest request) {
		
		posiblesRespuestas=new ArrayList<PosiblesRespuestas>();
		posiblesRespuestas.add(new PosiblesRespuestas(101,30,1,"SI"));
		posiblesRespuestas.add(new PosiblesRespuestas(102,30,2,"NO"));

		posiblesRespuestas.add(new PosiblesRespuestas(103,31,1,"SI"));
		posiblesRespuestas.add(new PosiblesRespuestas(104,31,2,"NO"));
		posiblesRespuestas.add(new PosiblesRespuestas(105,31,3,"N/A"));
		
		posiblesRespuestas.add(new PosiblesRespuestas(106,32,4,"ABIERTA NUMERICA"));
		posiblesRespuestas.add(new PosiblesRespuestas(107,33,5,"ABIERTA TEXTO"));
		posiblesRespuestas.add(new PosiblesRespuestas(108,34,6,"ABIERTA FECHA"));
	
		return posiblesRespuestas;
	}
	class PosiblesRespuestas{

		private int id;
		private int idTipoPreg;
		private int idPosibleResp;
		private String respuesta;

		public PosiblesRespuestas(int id, int idTipoPreg, int idPosibleResp, String respuesta) {
			super();
			this.id = id;
			this.idTipoPreg = idTipoPreg;
			this.idPosibleResp = idPosibleResp;
			this.respuesta = respuesta;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getIdTipoPreg() {
			return idTipoPreg;
		}
		public void setIdTipoPreg(int idTipoPreg) {
			this.idTipoPreg = idTipoPreg;
		}
		public int getIdPosibleResp() {
			return idPosibleResp;
		}
		public void setIdPosibleResp(int idPosibleResp) {
			this.idPosibleResp = idPosibleResp;
		}
		public String getRespuesta() {
			return respuesta;
		}
		public void setRespuesta(String respuesta) {
			this.respuesta = respuesta;
		}
		

		
	}
	
	
	
	@SuppressWarnings("unused")
	@RequestMapping(value = "/ajaxInsertarChecklist", method = RequestMethod.POST)
	public @ResponseBody List<PosiblesRespuestas> guardarChecklist(HttpServletRequest request,HttpServletResponse response) {
		
		JSONObject json;
		JSONArray jsonPreguntas;
		JSONArray jsonArbol;
		
		 StringBuilder sb = new StringBuilder();
		 BufferedReader reader=null;
		    try {
			     reader= request.getReader();
		        String line;
		        while ((line = reader.readLine()) != null) {
		            sb.append(line).append('\n');
		        }
		        
		    }catch(Exception e){
		    	logger.info("Algo ocurrió... "+e.getMessage());
		    }
		    finally {
		    	if(reader!=null){
		    		try {
						reader.close();
					} catch (Exception e) {
				    	//System.out.println("Algo ocurrió... "+e.getMessage());
					}
		    	}
		    }
		    //System.out.println(sb.toString());

	        try {
				json=new JSONObject(sb.toString());
				jsonPreguntas=json.getJSONArray("preguntas");
				jsonArbol=json.getJSONArray("arbol");
				
				

		        
				
	        } catch (Exception e) {

		    	//System.out.println("Algo ocurrió... "+e.getMessage());
			}
		
		
		return null;
	}
	/*******************************************************************/

	//
	@RequestMapping(value={"tienda/menuEdicionChecklist","central/menuEdicionChecklist.htm"},method=RequestMethod.GET)
	public ModelAndView menuChecklist(HttpServletRequest requqest,HttpServletResponse response,Model model)
			throws ServletException, IOException{

		
		ModelAndView mv = new ModelAndView("menuEdicionChecklist");
		
		return mv;
		
	}

	//primera vista
	@RequestMapping(value={"tienda/checklistDesignF1.htm","central/checklistDesignF1.htm"},method=RequestMethod.POST)
	public ModelAndView checkListDesignF1(
			@RequestParam(value = "idChecklist", required = true, defaultValue = "null") String idChecklist,
			HttpServletRequest request,HttpServletResponse response,Model model)
			throws ServletException, IOException{

		////System.out.println("ENTRO AL CHECH");
		ModelAndView mv = new ModelAndView("checklistDesignF1");
		////System.out.println(idChecklist);
		
		int idTipoChecklist;
		int idHorario;
		ChecklistDTO checklistTmp = null;
						
		if (checkT.buscaChecklist(Integer.parseInt(idChecklist)).size() != 0 )
		checklistTmp = checkT.buscaChecklist(Integer.parseInt(idChecklist)).get(0);
		else if(checklist.buscaChecklist(Integer.parseInt(idChecklist)).size() != 0)
			checklistTmp=checklist.buscaChecklist(Integer.parseInt(idChecklist)).get(0);
		
		if (checklistTmp != null){
			idTipoChecklist=checklistTmp.getIdTipoChecklist().getIdTipoCheck();
			idHorario=checklistTmp.getIdHorario();
			
			mv.addObject("idChecklist",idChecklist);
			//mv.addObject("checklist",checklist.buscaChecklist(Integer.parseInt(idChecklist)).get(0));
			mv.addObject("tipoChecklist", tipoChecklistBI.obtieneTipoChecklist(idTipoChecklist).get(0) );

			mv.addObject("horario",horario.obtienehorario(idHorario).get(0));
			
			String fechaInicioTmp[]= checklistTmp.getFecha_inicio().split("-");
			checklistTmp.setFecha_inicio(fechaInicioTmp[2].split(" ")[0]+"/"+fechaInicioTmp[1]+"/"+fechaInicioTmp[0]);
			
			if( checklistTmp.getFecha_fin()!=null){
				String fechaFinTmp[]= checklistTmp.getFecha_fin().split("-");
				checklistTmp.setFecha_fin(fechaFinTmp[2].split(" ")[0]+"/"+fechaFinTmp[1]+"/"+fechaFinTmp[0]);
				
			}
		
			mv.addObject("periodicidad",checklistTmp.getPeriodicidad());
			mv.addObject("diaSemana", checklistTmp.getDia());
			
			mv.addObject("checklist",checklistTmp);
			////System.out.println("CheckDesignController" + checklistTmp.getNumVersion());
	
		mv.addObject("listaTipoCheck", tipoChecklistBI.obtieneTipoChecklist());
		mv.addObject("listaModulos",modulo.obtieneModulo());
		mv.addObject("horarios", horario.obtieneHorario());
		}
		else {
			mv.addObject("checklist",null);
			mv.addObject("idChecklist",0);
			mv.addObject("listaTipoCheck", tipoChecklistBI.obtieneTipoChecklist());
			mv.addObject("listaModulos",modulo.obtieneModulo());
			mv.addObject("horarios", horario.obtieneHorario());
			
		}
		
		//Periodicidad en codigo duro
		List<String> periodicidad = new ArrayList<String>();
		periodicidad.add("Diario");
		periodicidad.add("Semanal");
		periodicidad.add("Mensual");
		mv.addObject("periodicidad",periodicidad);
		
		//Dia semana en codigo duro
		List<String> diaSemana = new ArrayList<String>();
		diaSemana.add("Lunes");
		diaSemana.add("Martes");
		diaSemana.add("Miercoles");
		diaSemana.add("Jueves");
		diaSemana.add("Viernes");
		diaSemana.add("Sabado");
		diaSemana.add("Domingo");
		mv.addObject("diaSemana",diaSemana);

		
		return mv;
		
	}
	

	//primera vista
	@RequestMapping(value={"tienda/checklistDesignF2.htm","central/checklistDesignF2.htm"},method=RequestMethod.POST)
	public ModelAndView checkListDesignF2(
			@RequestParam(value = "idChecklist", required = true, defaultValue = "null") String idChecklist,
			@RequestParam(value = "nombreChecklist", required = true, defaultValue = "null") String nombreChecklist,
			
			HttpServletRequest request,HttpServletResponse response,Model model)
			throws ServletException, IOException{
		
		
		////System.out.println("ID CHECKLIST: "+idChecklist);
		ModelAndView mv = new ModelAndView("checklistDesignF2");
		mv.addObject("tiposPregunta",tipoPregunta.obtieneTipoPregunta());

		mv.addObject("idChecklist",idChecklist);
		//mv.addObject("nombreChecklist",nombreChecklist);
		//se obtiene el nombre del checklist
		if(checkT.buscaChecklist(Integer.parseInt(idChecklist)).size()!=0){
			mv.addObject("nombreChecklist",checkT.buscaChecklist(Integer.parseInt(idChecklist)).get(0).getNombreCheck());
			mv.addObject("checklist", checkT.buscaChecklist(Integer.parseInt(idChecklist)).get(0));
		}
		else{
			mv.addObject("nombreChecklist",checklist.buscaChecklist(Integer.parseInt(idChecklist)).get(0).getNombreCheck());
			mv.addObject("checklist", checklist.buscaChecklist(Integer.parseInt(idChecklist)).get(0));
		}
		
		return mv;
	}
	

	//primera vista
	@RequestMapping(value={"tienda/checklistDesignF3.htm","central/checklistDesignF3.htm"},method=RequestMethod.POST)
	public ModelAndView checkListDesignF3(
			@RequestParam(value = "idChecklist", required = true, defaultValue = "null") String idChecklist,
			@RequestParam(value = "preguntasJSON", required = true, defaultValue = "null") String preguntasJSON,
			@RequestParam(value = "nombreChecklist", required = true, defaultValue = "null") String nombreChecklist,
			@RequestParam(value = "arbolAdmin", required = true, defaultValue = "null") String arbolAdmin,
			
			HttpServletRequest request,HttpServletResponse response,Model model)
			throws ServletException, IOException{
		
		
		//System.out.println("PREGUNTAS JSON PARAM: "+request.getParameter("preguntasJSON"));
		//System.out.println("JSON 2: "+ preguntasJSON);
		//System.out.println("ID CHECKLIST	: "+ idChecklist);
		
		ModelAndView mv = new ModelAndView("checklistDesignF3");
		mv.addObject("preguntasJSON", preguntasJSON);
		mv.addObject("idChecklist", idChecklist);

		//mv.addObject("nombreChecklist", nombreChecklist);

		mv.addObject("arbolAdmin", arbolAdmin);
		//se obtiene el nombre del checklist
		if(checkT.buscaChecklist(Integer.parseInt(idChecklist)).size()!=0){
			mv.addObject("nombreChecklist",checkT.buscaChecklist(Integer.parseInt(idChecklist)).get(0).getNombreCheck());
			mv.addObject("checklist",checkT.buscaChecklist(Integer.parseInt(idChecklist)).get(0));
		}
		else if (checklist.buscaChecklist(Integer.parseInt(idChecklist)).size()!=0){
			mv.addObject("nombreChecklist",checklist.buscaChecklist(Integer.parseInt(idChecklist)).get(0).getNombreCheck());
			mv.addObject("checklist",checklist.buscaChecklist(Integer.parseInt(idChecklist)).get(0));
		}
		return mv; 
	}
	//  
	@RequestMapping(value={"tienda/asignacionChecklist.htm","central/asignacionChecklist.htm"},method=RequestMethod.POST)
	public ModelAndView asignacionChecklist(HttpServletRequest request,HttpServletResponse response,Model model,
			@RequestParam(value = "idChecklist", required = true, defaultValue = "null") String idChecklist,
			@RequestParam(value = "origen", required = true, defaultValue = "false") String origen)
			throws ServletException, IOException{

		ModelAndView mv = new ModelAndView("asignacionChecklist_V2");
		
		//se obtiene el nombre del checklist
			if(checklist.buscaChecklist(Integer.parseInt(idChecklist)).size()!=0){
			mv.addObject("nombreChecklist",checklist.buscaChecklist(Integer.parseInt(idChecklist)).get(0).getNombreCheck());
		}
			
		mv.addObject("listaTipoCheck", tipoChecklistBI.obtieneTipoChecklist());
		//mv.addObject("listaPuesto", puestoBI.obtienePuestoGeo());
		mv.addObject("listaPaises", paisBI.obtienePais());
		
		//mv.addObject("listaTerritorio", );
		mv.addObject("listaPuestoAsig", puestoBI.obtienePuesto());
		mv.addObject("idChecklist",idChecklist);
		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		//664899
		if (origen.contains("true") == true){
			try {
			String nombreCheck = request.getParameter("continuar");
			logger.info("at sendMailAutorizacion()");
			correobi.sendMailAutorizacion("189871", userSession,nombreCheck);
			}
			catch (Exception e){
				logger.info(e.getMessage());
			}
		}
		logger.info("ASIGNACION CHECKLIST");
		
		return mv;
		
	}
	
}