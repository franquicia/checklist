package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.AsignaTransfDTO;

public class ProyectosCecoRowMapper implements RowMapper<AsignaTransfDTO> {

	public AsignaTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AsignaTransfDTO AsignaTransfDTO = new AsignaTransfDTO();
		
		AsignaTransfDTO.setProyecto(rs.getInt("FIID_PROYECTO"));
	    AsignaTransfDTO.setIdestatus(rs.getInt("FIIDACTIVO"));
	    AsignaTransfDTO.setNombProy(rs.getString("NOMBRE_PROYECTO"));
	    AsignaTransfDTO.setObsProyecto(rs.getString("NEGOCIO"));
	    AsignaTransfDTO.setTipoProyecto(rs.getInt("FIIDTIPOPROY"));
	    AsignaTransfDTO.setEdoCargaHallazgo(rs.getInt("FIID_EDOCARGAINI"));
	    AsignaTransfDTO.setStatusMaximo(rs.getInt("FCEDOHALLAFIN"));
	    AsignaTransfDTO.setPeriodo(rs.getString("FCPERIODO"));
	    AsignaTransfDTO.setCeco(rs.getInt("FCID_CECO"));
	    AsignaTransfDTO.setAgrupador(rs.getInt("FIID_AGRUPA"));
	    
		return AsignaTransfDTO;
	}

}


