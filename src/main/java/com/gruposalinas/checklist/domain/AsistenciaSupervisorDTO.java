package com.gruposalinas.checklist.domain;

public class AsistenciaSupervisorDTO {
	
	private int idUsuario;
	private String nomUsuario;
	private String fechaInicio;
	private String fechaFin;
	private String fecha;
	private String entrada;
	private String salida;
	private int idSucursal;
	private String nomSucursal;
	
	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNomUsuario() {
		return nomUsuario;
	}

	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getEntrada() {
		return entrada;
	}

	public void setEntrada(String entrada) {
		this.entrada = entrada;
	}

	public String getSalida() {
		return salida;
	}

	public void setSalida(String salida) {
		this.salida = salida;
	}

	public int getIdSucursal() {
		return idSucursal;
	}

	public void setIdSucursal(int idSucursal) {
		this.idSucursal = idSucursal;
	}

	public String getNomSucursal() {
		return nomSucursal;
	}

	public void setNomSucursal(String nomSucursal) {
		this.nomSucursal = nomSucursal;
	}

	@Override
	public String toString() {
		return "SupervisorDTO [idUsuario=" + idUsuario
				+ ", nomUsuario=" + nomUsuario
				+ ", fechaInicio=" + fechaInicio
				+ ", fechaFin=" + fechaFin
				+ ", fecha=" + fecha
				+ ", entrada=" + entrada
				+ ", salida=" + salida
				+ ", idSucursal=" + idSucursal
				+ ", nomSucursal=" + nomSucursal + "]";

	
}
}
