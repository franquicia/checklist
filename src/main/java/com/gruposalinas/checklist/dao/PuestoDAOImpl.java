package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.PuestoDTO;
import com.gruposalinas.checklist.domain.PuestoUsuarioDTO;
import com.gruposalinas.checklist.mappers.PuestoRowMapper;
import com.gruposalinas.checklist.mappers.PuestoUsuarioRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PuestoDAOImpl extends DefaultDAO implements PuestoDAO {

    private static Logger logger = LogManager.getLogger(PuestoDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtienePuesto;
    DefaultJdbcCall jdbcObtienePuestoUsu;
    DefaultJdbcCall jdbcObtienePuestoGeo;
    DefaultJdbcCall jdbcInsertaPuesto;
    DefaultJdbcCall jdbcActualizaPuesto;
    DefaultJdbcCall jdbcEliminaPuesto;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PUESTO")
                .withProcedureName("SP_SEL_G_PUESTO")
                .returningResultSet("RCL_PUESTO", new PuestoRowMapper());

        jdbcObtienePuesto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PUESTO")
                .withProcedureName("SP_SEL_PUESTO")
                .returningResultSet("RCL_PUESTO", new PuestoRowMapper());

        jdbcObtienePuestoGeo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PUESTO")
                .withProcedureName("SP_SEL_PUESTOGEO")
                .returningResultSet("RCL_PUESTO", new PuestoRowMapper());

        jdbcInsertaPuesto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PUESTO")
                .withProcedureName("SP_INS_PUESTO");

        jdbcActualizaPuesto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PUESTO")
                .withProcedureName("SP_ACT_PUESTO");

        jdbcEliminaPuesto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PUESTO")
                .withProcedureName("SP_DEL_PUESTO");

        jdbcObtienePuestoUsu = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PUESTO")
                .withProcedureName("SP_SEL_P_SOCIO")
                .returningResultSet("RCL_PUESTO", new PuestoUsuarioRowMapper());
    }

    @SuppressWarnings("unchecked")
    public List<PuestoDTO> obtienePuesto() throws Exception {
        Map<String, Object> out = null;
        List<PuestoDTO> listaPuesto = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funcion ejecutada: {checklist.PA_ADM_PUESTO.SP_SEL_G_PUESTO}");

        listaPuesto = (List<PuestoDTO>) out.get("RCL_PUESTO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Puestos");
        } else {
            return listaPuesto;
        }

        return listaPuesto;
    }

    @SuppressWarnings("unchecked")
    public List<PuestoDTO> obtienePuesto(int idPuesto) throws Exception {
        Map<String, Object> out = null;
        List<PuestoDTO> listaPuesto = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_PUESTO", idPuesto);

        out = jdbcObtienePuesto.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_PUESTO.SP_SEL_PUESTO}");

        listaPuesto = (List<PuestoDTO>) out.get("RCL_PUESTO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Puesto con id(" + idPuesto + ")");
        } else {
            return listaPuesto;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<PuestoDTO> obtienePuestoGeo() throws Exception {
        Map<String, Object> out = null;
        List<PuestoDTO> listaPuesto = null;
        int error = 0;

        out = jdbcObtienePuestoGeo.execute();

        logger.info("Funcion ejecutada: {checklist.PA_ADM_PUESTO.SP_SEL_PUESTOGEO}");

        listaPuesto = (List<PuestoDTO>) out.get("RCL_PUESTO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Puestos");
        } else {
            return listaPuesto;
        }

        return listaPuesto;
    }

    public int insertaPuesto(PuestoDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idPuesto = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPUESTO", bean.getIdPuesto())
                .addValue("PA_NIVEL", bean.getIdNivel())
                .addValue("PA_NEGOCIO", bean.getIdNegocio())
                .addValue("PA_TPUESTO", bean.getIdTipoPuesto())
                .addValue("PA_CANAL", bean.getIdCanal())
                .addValue("PA_CODIGO", bean.getCodigo())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_SUBNEGOCIO", bean.getIdSubnegocio())
                .addValue("PA_ACTIVO", bean.getActivo());

        out = jdbcInsertaPuesto.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_PUESTO.SP_INS_PUESTO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_FIIDPUESTO");
        idPuesto = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Puesto");
        } else {
            return idPuesto;
        }

        return idPuesto;
    }

    public boolean actualizaPuesto(PuestoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_PUESTO", bean.getIdPuesto())
                .addValue("PA_NIVEL", bean.getIdNivel())
                .addValue("PA_NEGOCIO", bean.getIdNegocio())
                .addValue("PA_TPUESTO", bean.getIdTipoPuesto())
                .addValue("PA_CANAL", bean.getIdCanal())
                .addValue("PA_CODIGO", bean.getCodigo())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_SUBNEGOCIO", bean.getIdSubnegocio())
                .addValue("PA_ACTIVO", bean.getActivo());

        out = jdbcActualizaPuesto.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_PUESTO.SP_ACT_PUESTO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el Puesto  id( " + bean.getIdPuesto() + ")");
        } else {
            return true;
        }

        return false;

    }

    public boolean eliminaPuesto(int idPuesto) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_PUESTO", idPuesto);

        out = jdbcEliminaPuesto.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_PUESTO.SP_DEL_PUESTO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el Puesto id(" + idPuesto + ")");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public List<PuestoUsuarioDTO> obtienePuestoUsuario(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        List<PuestoUsuarioDTO> listaPuesto = null;
        int error = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USUARIO", idUsuario);

        out = jdbcObtienePuestoUsu.execute(in);
        logger.info("Funcion ejecutada: {checklist.PA_ADM_PUESTO.SP_SEL_P_SOCIO}");

        listaPuesto = (List<PuestoUsuarioDTO>) out.get("RCL_PUESTO");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener los Puestos");
        } else {
            return listaPuesto;
        }

        return listaPuesto;
    }
}
