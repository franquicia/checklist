<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css"
	href="../css/expancion/detalleExpancion.css">
<link rel="stylesheet" type="text/css"
	href="../css/expancion/jquery.dataTablesImperdonables.css">

<script src="../js/expancion/jquery-3.3.1.js"></script>
<script src="../js/expancion/jquery.dataTables.js"></script>
<script src="../js/expancion/tableExpand.js"></script>
<style type="text/css"></style>
<title>DETALLES IMPERDONABLES</title>
</head>
<body style="max-width: 1500;   margin: auto;">
	<div class="marginTotal">
		<div class="left">
			<div class="centrar">
				<img alt="elektra" src="../images/expancion/LogoElektra.svg"
					class="imagenBanner"> <img alt="baz"
					src="../images/expancion/LogoBAZ.svg" class="imagenBanner">
			</div>
		</div>
		<div class="main">
			<h1 style="color: #6B696E;" class="fuenteBold">
				Entrega de Sucursales / <strong class="fuenteBold">DETALLES
					DE CALIFICACIÓN</strong>
			</h1>
			<br />

		</div>
		<div class="right">
			<div class="centrar">
				<img alt="elektra" src="../images/expancion/LogoElektra.svg"
					class="imagenBanner"> <img alt="baz"
					src="../images/expancion/LogoBAZ.svg" class="imagenBanner">
			</div>
		</div>

	</div>


	<div class="marginTotal">
		<div style="width: 60%;" class="centrar">

			<div
				style="padding-bottom: 2%; padding-top: 2%; margin-top: 2%; margin-bottom: 2%; color:gray">
				<h5>${titulo}</h5>
			</div>
		</div>

	</div>


	<div class="marginTotal">
		<div style="width: 80%; margin: auto;">
			<table class="tablaChecklist" cellspacing=50;>

				<tbody>
					<c:if test="${listaAreas != null }">
						<c:set var="count" value="0" scope="page" />
						<c:forEach var="valor" items="${listaAreas}">
							<tr>
								<c:set var="count" value="${count + 1}" scope="page" />
								<td> 
								
								<a style="color:gray;" href="detalleGrupoChecklist.htm?idUsuario=189871&idBitacora=${idBitacora}&opcion=${count}">${valor}</a>
								
								
								</td>
							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
		</div>
	</div>
	
</body>
</html>