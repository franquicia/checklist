package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.LlaveDTO;
import com.gruposalinas.checklist.mappers.LlaveRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class LlaveDAOImpl extends DefaultDAO implements LlaveDAO {

    private static Logger logger = LogManager.getLogger(LlaveDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaLlave;
    private DefaultJdbcCall jdbcActualizaLlave;
    private DefaultJdbcCall jdbcEliminaLlave;
    private DefaultJdbcCall jdbcBuscaLlaves;
    private DefaultJdbcCall jdbcBuscaLlave;

    public void init() {

        jdbcInsertaLlave = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_LLAVE")
                .withProcedureName("SP_INS_LLAVE");

        jdbcActualizaLlave = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_LLAVE")
                .withProcedureName("SP_ACT_LLAVE");

        jdbcEliminaLlave = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_LLAVE")
                .withProcedureName("SP_DEL_LLAVE");

        jdbcBuscaLlaves = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_LLAVE")
                .withProcedureName("SP_SEL_G_LLAVE")
                .returningResultSet("RCL_LLAVE", new LlaveRowMapper());

        jdbcBuscaLlave = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_LLAVE")
                .withProcedureName("SP_SEL_LLAVE")
                .returningResultSet("RCL_LLAVE", new LlaveRowMapper());
    }

    public boolean insertaLllave(LlaveDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDLLAVE", bean.getIdLlave())
                .addValue("PA_FCLLAVE", bean.getLlave())
                .addValue("PA_FCDES", bean.getDescripcion());

        logger.info("Funcion ejecutada:{checklist.PA_ADM_LLAVE.SP_INS_LLAVE}");

        out = jdbcInsertaLlave.execute(in);

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar Bitacora");
        } else {
            return true;
        }

        return false;
    }

    public boolean actualizaLlave(LlaveDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_LLAVE", bean.getIdLlave())
                .addValue("PA_FCLLAVE", bean.getLlave())
                .addValue("PA_FCDES", bean.getDescripcion());

        logger.info("Funcion ejecutada:{checklist.PA_ADM_LLAVE.SP_ACT_LLAVE}");

        out = jdbcActualizaLlave.execute(in);

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar Bitacora");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaLlave(int idLlave) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_LLAVE", idLlave);

        logger.info("Funcion ejecutada:{checklist.PA_ADM_LLAVE.SP_DEL_LLAVE}");

        out = jdbcEliminaLlave.execute(in);

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar Bitacora");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<LlaveDTO> obtieneLlave() throws Exception {
        Map<String, Object> out = null;

        List<LlaveDTO> listaLlave = null;
        int error = 0;

        out = jdbcBuscaLlaves.execute();

        logger.info("Funcion ejecutada: {checklist.PA_ADM_LLAVE.SP_SEL_G_LLAVE}");

        listaLlave = (List<LlaveDTO>) out.get("RCL_LLAVE");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener las Llaves");
        } else {
            return listaLlave;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<LlaveDTO> obtieneLlave(int idLlave) throws Exception {
        Map<String, Object> out = null;

        List<LlaveDTO> listaLlave = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_LLAVE", idLlave);

        out = jdbcBuscaLlave.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_LLAVE.SP_SEL_LLAVE}");

        listaLlave = (List<LlaveDTO>) out.get("RCL_LLAVE");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener la Llave");
        } else {
            return listaLlave;
        }

        return null;
    }

}
