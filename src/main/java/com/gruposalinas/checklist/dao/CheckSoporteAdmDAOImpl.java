package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.CheckSoporteAdmDTO;
import com.gruposalinas.checklist.mappers.ChecklistSopAdmRowMapper;
import com.gruposalinas.checklist.mappers.ChecklistVerSopAdmRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class CheckSoporteAdmDAOImpl extends DefaultDAO implements CheckSoporteAdmDAO {

    private static final Logger logger = LogManager.getLogger(CheckSoporteAdmDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneChecklist;

    private DefaultJdbcCall jdbcObtieneChecklistActivos;

    private DefaultJdbcCall jdbcActualizaStatus;

    private DefaultJdbcCall jdbcObtieneChecklistVers;

    private DefaultJdbcCall jdbcObtieneChecklistVersProd;

    public void init() {

        jdbcObtieneChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_SOPADM")
                .withProcedureName("SP_CHECK_GRUP")
                .returningResultSet("RCL_CHECK", new ChecklistSopAdmRowMapper());

        jdbcObtieneChecklistActivos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_SOPADM")
                .withProcedureName("SP_CHECK_ACTIV")
                .returningResultSet("RCL_CHECK", new ChecklistSopAdmRowMapper());

        jdbcActualizaStatus = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_SOPADM")
                .withProcedureName("SP_ACT_CHECK");

        jdbcObtieneChecklistVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_SOPADM")
                .withProcedureName("SP_SEL_VER")
                .returningResultSet("RCL_CHECK", new ChecklistVerSopAdmRowMapper());

        jdbcObtieneChecklistVersProd = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_SOPADM")
                .withProcedureName("SP_SEL_VERPRO")
                .returningResultSet("RCL_CHECK", new ChecklistVerSopAdmRowMapper());
    }

    @SuppressWarnings("unchecked")
    public List<CheckSoporteAdmDTO> checkAutorizacion(String idGrupo) throws Exception {
        Map<String, Object> out = null;
        List<CheckSoporteAdmDTO> listaChecklist = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_GRUP", idGrupo);

        out = jdbcObtieneChecklist.execute(in);
        logger.info("Funci�n ejecutada:{checklist.PA_SOPADM.SP_CHECK_GRUP}");

        listaChecklist = (List<CheckSoporteAdmDTO>) out.get("RCL_CHECK");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("Algo ocurrió en el Store Procedure");
        }

        return listaChecklist;
    }

    @SuppressWarnings("unchecked")
    public List<CheckSoporteAdmDTO> checkActivos(String idGrupo) throws Exception {
        Map<String, Object> out = null;
        List<CheckSoporteAdmDTO> listaChecklist = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_GRUP", idGrupo);

        out = jdbcObtieneChecklistActivos.execute(in);
        logger.info("Funci�n ejecutada:{checklist.PA_SOPADM.SP_CHECK_ACTIV}");

        listaChecklist = (List<CheckSoporteAdmDTO>) out.get("RCL_CHECK");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("Algo ocurrió en el Store Procedure");
        }

        return listaChecklist;
    }

    @Override
    public boolean actualiza(CheckSoporteAdmDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECK", bean.getIdChecklist())
                .addValue("PA_ESTATUS", bean.getEstatus());

        out = jdbcActualizaStatus.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PA_SOPADM.SP_ACT_CHECK}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 0) {
            logger.info("Algo ocurrió al actualizar Estado del  check( " + bean.getIdChecklist() + ")");
        } else {
            return true;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    public List<CheckSoporteAdmDTO> checkVersParam(String param, String idGupo) throws Exception {
        Map<String, Object> out = null;
        List<CheckSoporteAdmDTO> listaChecklist = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PARAM", param)
                .addValue("PA_GRUP", idGupo);

        out = jdbcObtieneChecklistVers.execute(in);
        logger.info("Funci�n ejecutada:{checklist.PA_SOPADM.SP_SEL_VER}");

        listaChecklist = (List<CheckSoporteAdmDTO>) out.get("RCL_CHECK");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("Algo ocurrió en el Store Procedure");
        }

        return listaChecklist;
    }

    @SuppressWarnings("unchecked")
    public List<CheckSoporteAdmDTO> checkVersParamProd(String param, String idGupo) throws Exception {
        Map<String, Object> out = null;
        List<CheckSoporteAdmDTO> listaChecklist = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PARAM", param)
                .addValue("PA_GRUP", idGupo);

        out = jdbcObtieneChecklistVersProd.execute(in);
        logger.info("Funci�n ejecutada:{checklist.PA_SOPADM.SP_SEL_VER}");

        listaChecklist = (List<CheckSoporteAdmDTO>) out.get("RCL_CHECK");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("Algo ocurrió en el Store Procedure");
        }

        return listaChecklist;
    }
}
