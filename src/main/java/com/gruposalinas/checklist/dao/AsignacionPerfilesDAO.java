package com.gruposalinas.checklist.dao;

public interface AsignacionPerfilesDAO {
	

	public boolean actualizarPerfilesGestion() throws Exception ;	
	public boolean asignacionPerfiles() throws Exception ;
	public boolean altaPerfilesSistemas() throws Exception ;
	public boolean altaPerfilesScouting() throws Exception ;
	public boolean altaPerfilesPrestaPrenda() throws Exception ;
	public boolean altaPerfilesDescargaGestion() throws Exception ;
	
}
