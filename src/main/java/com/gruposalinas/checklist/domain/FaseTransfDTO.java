package com.gruposalinas.checklist.domain;

public class FaseTransfDTO {
	


		private int idCheck;
		private int fase;
		private int idTab;
		private int idactivo;
		private String aux;
		private String obs;
		private String periodo;
		private String nego;
		private String fechaLibera;
		private String protocolo;
		private int  ordenFase;
		private int idAgrupa;
		private int proyecto;
		private int version;
		private String nombreProyecto;
		private int faseActual;
		private String banderaFaseActual;
		
		public int getIdCheck() {
			return idCheck;
		}
		public void setIdCheck(int idCheck) {
			this.idCheck = idCheck;
		}
		public int getFase() {
			return fase;
		}
		public void setFase(int fase) {
			this.fase = fase;
		}
		public int getIdTab() {
			return idTab;
		}
		public void setIdTab(int idTab) {
			this.idTab = idTab;
		}
		public int getIdactivo() {
			return idactivo;
		}
		public void setIdactivo(int idactivo) {
			this.idactivo = idactivo;
		}
		public String getAux() {
			return aux;
		}
		public void setAux(String aux) {
			this.aux = aux;
		}
		public String getObs() {
			return obs;
		}
		public void setObs(String obs) {
			this.obs = obs;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		
		public String getNego() {
			return nego;
		}
		public void setNego(String nego) {
			this.nego = nego;
		}
		
		public String getFechaLibera() {
			return fechaLibera;
		}
		public void setFechaLibera(String fechaLibera) {
			this.fechaLibera = fechaLibera;
		}
		
		public String getProtocolo() {
			return protocolo;
		}
		public void setProtocolo(String protocolo) {
			this.protocolo = protocolo;
		}
		
		public int getOrdenFase() {
			return ordenFase;
		}
		public void setOrdenFase(int ordenFase) {
			this.ordenFase = ordenFase;
		}
		public int getIdAgrupa() {
			return idAgrupa;
		}
		public void setIdAgrupa(int idAgrupa) {
			this.idAgrupa = idAgrupa;
		}
		
		public int getProyecto() {
			return proyecto;
		}
		public void setProyecto(int proyecto) {
			this.proyecto = proyecto;
		}
		
		public int getVersion() {
			return version;
		}
		public void setVersion(int version) {
			this.version = version;
		}
		
		public String getNombreProyecto() {
			return nombreProyecto;
		}
		public void setNombreProyecto(String nombreProyecto) {
			this.nombreProyecto = nombreProyecto;
		}
		public int getFaseActual() {
			return faseActual;
		}
		public void setFaseActual(int faseActual) {
			this.faseActual = faseActual;
		}
		public String getBanderaFaseActual() {
			return banderaFaseActual;
		}
		public void setBanderaFaseActual(String banderaFaseActual) {
			this.banderaFaseActual = banderaFaseActual;
		}
		@Override
		public String toString() {
			return "FaseTransfDTO [idCheck=" + idCheck + ", fase=" + fase + ", idTab=" + idTab + ", idactivo="
					+ idactivo + ", aux=" + aux + ", obs=" + obs + ", periodo=" + periodo + ", nego=" + nego
					+ ", fechaLibera=" + fechaLibera + ", protocolo=" + protocolo + ", ordenFase=" + ordenFase
					+ ", idAgrupa=" + idAgrupa + ", proyecto=" + proyecto + ", version=" + version + ", nombreProyecto="
					+ nombreProyecto + ", faseActual=" + faseActual + ", banderaFaseActual=" + banderaFaseActual + "]";
		}
		
		
	
	}