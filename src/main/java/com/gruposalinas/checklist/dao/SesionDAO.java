package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.SesionDTO;

public interface SesionDAO {

	public List<SesionDTO> buscaSesion(String navegador, String idUsuario, String ip, String fechaLogueo, String fechaActualiza, String bandera) throws Exception;
	
	public int insertaSesion(SesionDTO bean) throws Exception;
	
	public boolean actualizaSesion(SesionDTO bean) throws Exception;
	
	public boolean eliminaSesion(int idSesion) throws Exception;
	
	public List<SesionDTO> buscaSesion(String idUsuario, String idSesion) throws Exception;
	
	public boolean eliminaSesiones(String fechaInicio, String FechaFin) throws Exception;
	

}
