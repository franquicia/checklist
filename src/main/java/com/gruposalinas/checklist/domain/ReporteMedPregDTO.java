package com.gruposalinas.checklist.domain;

public class ReporteMedPregDTO {
	
	private int idChecklist;
	private int idPregunta;
	private String descPregunta;
	private int tipoPreg;
	private int estatus;
	private int idModulo;
	private int idPregPadre;
	private int idCritica;
	private int numSerie;
	
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public String getDescPregunta() {
		return descPregunta;
	}
	public void setDescPregunta(String descPregunta) {
		this.descPregunta = descPregunta;
	}
	public int getTipoPreg() {
		return tipoPreg;
	}
	public void setTipoPreg(int tipoPreg) {
		this.tipoPreg = tipoPreg;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public int getIdModulo() {
		return idModulo;
	}
	public void setIdModulo(int idModulo) {
		this.idModulo = idModulo;
	}
	public int getIdPregPadre() {
		return idPregPadre;
	}
	public void setIdPregPadre(int idPregPadre) {
		this.idPregPadre = idPregPadre;
	}
	public int getIdCritica() {
		return idCritica;
	}
	public void setIdCritica(int idCritica) {
		this.idCritica = idCritica;
	}	
	public int getNumSerie() {
		return numSerie;
	}
	public void setNumSerie(int numSerie) {
		this.numSerie = numSerie;
	}
	
	@Override
	public String toString() {
		return "ReporteMedicionDTO [idChecklist=" + idChecklist + 
				", idPregunta=" + idPregunta + 
				", descPregunta=" + descPregunta + 
				", tipoPreg=" + tipoPreg + 
				", estatus=" + estatus + 
				", idModulo=" + idModulo + 
				", idPregPadre=" + idPregPadre +
				", idCritica=" + idCritica +
				", numSerie=" + numSerie +
				"]";
	}
		
}
