package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.FragmentMenuDTO;

public class FragmentConfigProyRowMapper implements RowMapper<FragmentMenuDTO> {

	public FragmentMenuDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		FragmentMenuDTO FragmentMenuDTO = new FragmentMenuDTO();
		
		FragmentMenuDTO.setIdConfigProyecto(rs.getInt("FIIDCONFPRO"));
		FragmentMenuDTO.setIdConfigMenu(rs.getInt("FIID_CONFIG"));
	    FragmentMenuDTO.setIdFragment(rs.getInt("FIID_FRAGM"));
		FragmentMenuDTO.setOrdenFragment(rs.getInt("FIORDENFRAG"));
	    FragmentMenuDTO.setIdAgrupaFirma(rs.getInt("FIID_FIRM"));
		FragmentMenuDTO.setParametro(rs.getString("FCPARAMETRO"));
		FragmentMenuDTO.setAux(rs.getString("FCAUX"));
		FragmentMenuDTO.setPeriodo(rs.getString("FCPERIODO"));
	
		return FragmentMenuDTO;
	}

}


