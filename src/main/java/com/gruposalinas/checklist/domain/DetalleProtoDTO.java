package com.gruposalinas.checklist.domain;

public class DetalleProtoDTO {
	private int idOrdeGrupo;
	private int idUsuario;
	private String nombreUsuario;
	private int idCheklist;
	private String idCeco;
	private String nomCeco;
	private String nomCheklist;
	private int idBitacora;
	private String fdfecha;
	private String fdInicio;
	private String fdTermino;
	private String fcLatitud;
	private String fcLongitud;
	private int bandera;
	private String plataforma;
	private int tiempoConexion;
	
	public String getFcLatitud() {
		return fcLatitud;
	}
	public void setFcLatitud(String fcLatitud) {
		this.fcLatitud = fcLatitud;
	}
	public String getFcLongitud() {
		return fcLongitud;
	}
	public void setFcLongitud(String fcLongitud) {
		this.fcLongitud = fcLongitud;
	}
	public int getIdOrdeGrupo() {
		return idOrdeGrupo;
	}
	public int getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	
	public String getFdfecha() {
		return fdfecha;
	}
	public void setFdfecha(String fdfecha) {
		this.fdfecha = fdfecha;
	}
	public String getFdInicio() {
		return fdInicio;
	}
	public void setFdInicio(String fdInicio) {
		this.fdInicio = fdInicio;
	}
	public String getFdTermino() {
		return fdTermino;
	}
	public void setFdTermino(String fdTermino) {
		this.fdTermino = fdTermino;
	}
	public void setIdOrdeGrupo(int idOrdeGrupo) {
		this.idOrdeGrupo = idOrdeGrupo;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public int getIdCheklist() {
		return idCheklist;
	}
	public void setIdCheklist(int idCheklist) {
		this.idCheklist = idCheklist;
	}
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getNomCeco() {
		return nomCeco;
	}
	public void setNomCeco(String nomCeco) {
		this.nomCeco = nomCeco;
	}
	
	
	public String getNomCheklist() {
		return nomCheklist;
	}
	public void setNomCheklist(String nomCheklist) {
		this.nomCheklist = nomCheklist;
	}
	public int getBandera() {
		return bandera;
	}
	public void setBandera(int bandera) {
		this.bandera = bandera;
	}
	
	
	public String getPlataforma() {
		return plataforma;
	}
	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}
	

	public int getTiempoConexion() {
		return tiempoConexion;
	}
	public void setTiempoConexion(int tiempoConexion) {
		this.tiempoConexion = tiempoConexion;
	}
	@Override
	public String toString() {
		return "DetalleProtoDTO [idOrdeGrupo=" + idOrdeGrupo + ", idUsuario=" + idUsuario + ", nombreUsuario="
				+ nombreUsuario + ", idCheklist=" + idCheklist + ", idCeco=" + idCeco + ", nomCeco=" + nomCeco
				+ ", nomCheklist=" + nomCheklist + ", idBitacora=" + idBitacora + ", fdfecha=" + fdfecha + ", fdInicio="
				+ fdInicio + ", fdTermino=" + fdTermino + ", fcLatitud=" + fcLatitud + ", fcLongitud=" + fcLongitud
				+ ", bandera=" + bandera + ", plataforma=" + plataforma + ", tiempoConexion=" + tiempoConexion + "]";
	}


}

