/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.dao;

/**
 *
 * @author leodan1991
 */

import java.util.ArrayList;
import java.util.List;
import com.gruposalinas.checklist.domain.AdmPregZonaDTO;

public interface AdmPregZonasTempDAO {
	
	public ArrayList<AdmPregZonaDTO> obtienePregZonaById(AdmPregZonaDTO bean) throws Exception;
	
	public boolean insertaPregZona(AdmPregZonaDTO bean) throws Exception;

	public boolean actualizaPregZona(AdmPregZonaDTO bean) throws Exception;
	
	public boolean eliminaPregZona(AdmPregZonaDTO bean) throws Exception;
}
