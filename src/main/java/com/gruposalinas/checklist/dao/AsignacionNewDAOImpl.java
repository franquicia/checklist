package com.gruposalinas.checklist.dao;

import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AsignacionNewDAOImpl extends DefaultDAO implements AsignacionNewDAO {

    private Logger logger = LogManager.getLogger(AsignacionNewDAOImpl.class);

    private DefaultJdbcCall jdbcEjecutaAsignaciones;
    private DefaultJdbcCall jdbcEjecutaAsignacion;
    private DefaultJdbcCall jdbcEjecutaAsignacionEspecial;

    public void init() {

        jdbcEjecutaAsignaciones = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNACION_NW")
                .withProcedureName("SP_REASIGNA");

        jdbcEjecutaAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNACION_NW")
                .withProcedureName("SP_ASIGNA_CHECK");

        jdbcEjecutaAsignacionEspecial = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNACION_NW")
                .withProcedureName("SP_ASIGNA_ESP");

    }

    @SuppressWarnings("unused")
    @Override
    public Map<String, Object> ejecutaAsignaciones() throws Exception {

        Map<String, Object> out = null;
        Map<String, Object> respuesta = new HashMap<String, Object>();
        int errores = 0;
        int ejecucion = 0;

        out = jdbcEjecutaAsignaciones.execute();

        logger.info("Funcion ejecutada:{checklist.PAASIGNACION_NW.SP_REASIGNA}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        respuesta.put("respuestaString", out.get("PA_RESPUESTA"));

        if (ejecucion == 0) {
            respuesta.put("respuestaBoolean", false);
            logger.info("Algo ocurrió al ejecutar la asignacion");
        } else {
            respuesta.put("respuestaBoolean", true);
        }

        return respuesta;
    }

    @Override
    public Map<String, Object> asignacionEspecial(String userNew, String userOld) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> respuesta = new HashMap<String, Object>();
        int ejecucion = 0;
        int errores = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_USRNEW", userNew)
                .addValue("PA_USROLD", userOld);

        out = jdbcEjecutaAsignacionEspecial.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAASIGNACION_NW.SP_ASIGNA_ESP}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        BigDecimal returnErrores = (BigDecimal) out.get("PA_ERRORES");
        errores = returnErrores.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrio al ejecutar asignaicon especial");
        }

        respuesta.put("ejecucion", ejecucion);
        respuesta.put("errores", errores);

        return respuesta;
    }

    @Override
    public Map<String, Object> ejecutaAsignacion(String ceco, int puesto, int checklist) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> respuesta = new HashMap<String, Object>();
        int ejecucion = 0;
        int errores = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_PUESTO", puesto)
                .addValue("PA_IDCHECK", checklist);

        out = jdbcEjecutaAsignacion.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAASIGNACION_NW.SP_ASIGNA_CHECK}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        BigDecimal returnErrores = (BigDecimal) out.get("PA_ERRORES");
        errores = returnErrores.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrio al ejecutar asignaicon especial");
        }

        respuesta.put("ejecucion", ejecucion);
        respuesta.put("errores", errores);

        return respuesta;
    }

}
