package com.gruposalinas.checklist.business;



import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.CargaCheckListDAO;
import com.gruposalinas.checklist.dao.DistribCecoDAO;
import com.gruposalinas.checklist.domain.CargaCheckListDTO;
import com.gruposalinas.checklist.domain.ConsultaDashBoardDTO;
import com.gruposalinas.checklist.domain.DistribCecoDTO;


public class DistribCecoBI {
	
	private Logger logger = LogManager.getLogger(ChecklistNegocioBI.class);
	
	@Autowired
	DistribCecoDAO distribCecoDAO;
	
	List<DistribCecoDTO> listaCeco = null;
	
	
	public int insertaDistrib(){
		int ejecucion = 0;
		
		try {
			ejecucion = distribCecoDAO.insertaDistrib();
		} catch (Exception e) {
			logger.info("No fue posible insertar la distribucion");
				
		}
		
		return ejecucion;
	}
	
	public List<DistribCecoDTO> obtieneCeco(DistribCecoDTO bean){
		
		try {
			listaCeco = distribCecoDAO.obtieneCeco(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure");
			logger.info(""+e);
		}			
		
		return listaCeco;
	
	}
	
	public boolean EliminaDistrib(String ceco){
		
		boolean respuesta = false;
		
		try {
			respuesta = distribCecoDAO.EliminaDistrib(ceco)	;	
			} catch (Exception e) {
			logger.info("No fue posible eliminar ceco"+e);
				
		}
		
		return respuesta;
	}
	

}
