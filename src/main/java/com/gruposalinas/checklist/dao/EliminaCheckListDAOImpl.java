package com.gruposalinas.checklist.dao;

import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EliminaCheckListDAOImpl extends DefaultDAO implements EliminaCheckListDAO {

    private static Logger logger = LogManager.getLogger(EliminaCheckListDAOImpl.class);

    DefaultJdbcCall jdbEliminaCheckList;

    public void init() {

        jdbEliminaCheckList = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_INSCHECKLST")
                .withProcedureName("SPDESCHECK");
        //.returningResultSet("CU_PUESTO", new CargaCheckListRowMapper());

    }

    public boolean eliminaCheckList() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        //.addValue("PA_EJECUCION", bean.getEjecucion());
        out = jdbEliminaCheckList.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_INSCHECKLST.SPDESCHECK}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al desactivar el check");
        } else {
            return true;

        }

        return false;
    }

}
