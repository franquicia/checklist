package com.gruposalinas.checklist.servicios.servidor;

import com.gruposalinas.checklist.util.Commons;
import com.gruposalinas.checklist.util.Unzipping;
import com.gruposalinas.checklist.util.UtilString;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Base64;
import java.util.Calendar;
import javax.imageio.ImageIO;
import javax.xml.ws.soap.MTOM;
import org.apache.axiom.om.OMElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sun.misc.BASE64Decoder;

@SuppressWarnings("restriction")
public class ServicioImagen {

    private static Logger logger = LogManager.getLogger(ServicioImagen.class);

    @SuppressWarnings({"static-access"})
    public boolean enviarImagen(String img, String nombreImg, String extension) {
        boolean respuesta = false;
        File r = null;
        String rootPath = File.listRoots()[0].getAbsolutePath();
        Calendar cal = Calendar.getInstance();

        //DEFINIR RUTA
        String rutaImagen = "";
        String rutaImagenPreview = "";

        //1.- :::  VERIFICAR EN DONDE ESTOY PARA GUARDAR EN ( /Sociounico/checklist/imagenes/ - /Sociounico/checklist/preview/ ) O EN EL NUEVO
        //COMENTO ESTO POR QUE AUN NO SE APLICA LA VERSION DE checklist
        try {
            if (!this.verificaServidor(InetAddress.getLocalHost().getHostAddress().toString())) {
                rutaImagen = "/Sociounico/franquicia/imagenes/";
                rutaImagenPreview = "/Sociounico/franquicia/preview/";
            } else {
                //DONDE GUARDO CUANDO ESTE EN checklist
                rutaImagen = "/franquicia/imagenes/";
                rutaImagenPreview = "/franquicia/preview/";
            }
        } catch (UnknownHostException e1) {
            // TODO Auto-generated catch block
            //e1.printStackTrace();

            rutaImagen = "/franquicia/imagenes/";
            rutaImagenPreview = "/franquicia/preview/";
        }

        String ruta = rootPath + rutaImagen + cal.getInstance().get(Calendar.YEAR) + "/";
        String rutaPreview = rootPath + rutaImagenPreview + cal.getInstance().get(Calendar.YEAR) + "/";
        // //System.out.println(cal.getTime().toString());
        ////System.out.println(File.listRoots()[0].getAbsolutePath());
        r = new File(ruta);

        logger.info("RUTA: " + ruta);
        if (r.mkdirs()) {
            logger.info("SE HA CREADA LA CARPETA");
        } else {
            logger.info("EL DIRECTORIO YA EXISTE");
        }

        FileOutputStream fos = null;
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] imgDecodificada = decoder.decodeBuffer(img);
            byte[] nomDecodificado = decoder.decodeBuffer(nombreImg);
            byte[] extDecodificada = decoder.decodeBuffer(extension);
            // //System.out.println(ruta+new String(imgDecodificada)+"."+new
            // String(extDecodificada));
            String nomDecodificadoStr = UtilString.cleanFilename(new String(nomDecodificado));
            String extDecodificadaStr = UtilString.cleanFilename(new String(extDecodificada));

            String rutaTotal = ruta + nomDecodificadoStr + "." + extDecodificadaStr;

            String rutalimpia = rutaTotal;

            FileOutputStream fileOutput = new FileOutputStream(rutalimpia);

            try {
                BufferedOutputStream bufferOutput = new BufferedOutputStream(fileOutput);
                // se escribe el archivo decodificado
                bufferOutput.write(imgDecodificada);
                bufferOutput.close();

                //**********************SE GENERA EL PREVIEW*****************
                if (!extDecodificadaStr.equalsIgnoreCase("MP4")) {
                    // procedimiento para escalar la imagen
                    //BufferedImage imgS = ImageIO.read(new File(ruta + nomDecodificadoStr + "." + extDecodificadaStr));
                    BufferedImage imgS = ImageIO.read(new File(rutalimpia));
                    // int scaleX=(int)(img.getWidth()*factor);
                    // int scaleY=(int)(img.getHeight()*factor);
                    int scaleX;
                    int scaleY;
                    if (imgS.getHeight() > imgS.getWidth()) {
                        scaleX = 120;
                        scaleY = 160;
                    } else {
                        scaleX = 160;
                        scaleY = 120;
                    }
                    Image imgEscala = imgS.getScaledInstance(scaleX, scaleY, Image.SCALE_SMOOTH);
                    BufferedImage bufferImg = new BufferedImage(scaleX, scaleY, BufferedImage.TYPE_INT_RGB);
                    bufferImg.getGraphics().drawImage(imgEscala, 0, 0, null);
                    File imgSalida = new File(rutaPreview);

                    if (imgSalida.mkdirs()) {
                        logger.info("SE HA CREADO LA ESTRUCTURA PARA LAS IMAGENES DE PREVIEW");
                    } else {
                        logger.info("LA ESTRUCTURA DE IMAGES PREVIEW YA EXISTE");
                    }

                    String rutaTotal2 = rutaPreview + nomDecodificadoStr + "." + extDecodificadaStr;

                    String rutalimpia2 = rutaTotal2;

                    //fos = new FileOutputStream(rutaPreview + nomDecodificadoStr + "." + extDecodificadaStr);
                    fos = new FileOutputStream(rutalimpia2);

                    try {
                        ImageIO.write(bufferImg, "JPG", fos);
                    } finally {
                        if (fos != null) {
                            fos.close();
                        }
                    }

                }
            } finally {
                fileOutput.close();
            }
            respuesta = true;

            //AL FINALIZAR DE GUARDAR LA IMAGEN SE INICIA EL PROCESO DE REENVIO, TRUE SI ESTOY EN checklist
            //SE COMENTA POR QUE AUN NO ESTA IMPLEMENTADO
            if (!this.verificaServidor(InetAddress.getLocalHost().getHostAddress().toString())) {
                ///logger.info("se envia a otro servidor");
                this.enviarImagenFrq(img, nombreImg, extension);
            }
            //FIN DE PROCESO DE REENVIO
        } catch (Exception e) {
            // //System.out.println("ERROR: "+e.getMessage());
            //logger.info("Algo paso " + e);
            //e.printStackTrace();
            respuesta = false;
        }
        return respuesta;

    }

    /* ******************************  METODOS PARA EL REPLICADO DE IMAGENES ****************************** */
    //TRUE CUANDO ESTOY EN checklist
    public boolean verificaServidor(String ipActual) {
        boolean resp = false;
        String[] ipFrq = {"10.53.33.74", "10.53.33.75", "10.53.33.76", "10.53.33.77"};

        for (String data : ipFrq) {
            if (data.contains(ipActual)) {
                return true;
            }
        }
        return resp;
    }

    public String codificarBase64(byte[] cadena) {
        return Base64.getEncoder().encodeToString(cadena);
    }

    /* METODO PARA ENVIAR LA IMAGEN */
    @SuppressWarnings("unused")
    public String enviarImagenFrq(String img, String nombreImagen, String extension) throws IOException {
        // boolean estatus = false;
        String estatus = "-";
        String imagen;
        String nombreImg;
        String ext;

        BufferedInputStream buffers = null;
        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        try {
            // se codifica en base 64
            imagen = img;
            nombreImg = nombreImagen;
            ext = extension;

            String xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                    + "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">"
                    + "<soap:Body>" + "<enviarImagen xmlns=\"http://servidor.servicios.checklist.gruposalinas.com\">"
                    + "<img>" + imagen + "</img>" + "<nombreImg>" + nombreImg + "</nombreImg>" + "<extension>" + ext
                    + "</extension>"
                    + "</enviarImagen>" + "</soap:Body>" + "</soap:Envelope>";

            //SOLO PONDRE EL WSDL DE checklist POR QUE AQUI ENTRA SOLO SI NO ESTA EN
            //2.- :::  DEFINIR EL WSDL DE checklist
            URL url = new URL("http://10.53.33.83:80/checklist/services/ServicioImagen?wsdl");
            rc = (HttpURLConnection) url.openConnection();

            //SUSTITUIR POR checklist
            //3.- :::  DEFINIR EL HOST DE checklist
            rc.setRequestProperty("Host", "10.53.33.83");
            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            wr = new OutputStreamWriter(rc.getOutputStream());
            wr.write(xml, 0, xml.length());
            wr.flush();

            rd = new BufferedReader(new InputStreamReader(rc.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                if (line.contains("true")) {
                    logger.info("ESTATUS ENVIO: SE HA ENVIADO CORRECTAMENTE LA FOTO");
                    estatus = "SUCCESS";
                } else {
                    estatus = line;
                }
            }
            rd.close();
            rc.disconnect();
        } catch (Exception e) {
            //logger.info("Algo paso al enviar imagen en ServicioImagen " + e.toString());
            //e.printStackTrace();
            estatus = e.getMessage();
        } finally {
            if (rd != null) {
                safeClose(rd);
            }
            if (rc != null) {
                rc.disconnect();
            }
            if (wr != null) {
                wr.close();
            }
        }
        return estatus;
    }

    /* ****************************** FIN METODOS PARA REPLICADO DE IMAGENES ****************************** */

    public static void safeClose(BufferedReader rd) {

        if (rd != null) {
            try {
                rd.close();
            } catch (IOException e) {
                //e.printStackTrace();
            }
        }

    }

    public static String cleanString(String aString) {
        if (aString == null) {
            return null;
        }
        String cleanString = "";
        for (int i = 0; i < aString.length(); ++i) {
            cleanString += cleanChar(aString.charAt(i));
        }
        return cleanString;
    }

    private static char cleanChar(char aChar) {

        // 0 - 9
        for (int i = 48; i < 58; ++i) {
            if (aChar == i) {
                return (char) i;
            }
        }

        // 'A' - 'Z'
        for (int i = 65; i < 91; ++i) {
            if (aChar == i) {
                return (char) i;
            }
        }

        // 'a' - 'z'
        for (int i = 97; i < 123; ++i) {
            if (aChar == i) {
                return (char) i;
            }
        }

        // other valid characters
        switch (aChar) {
            case '/':
                return '/';
            case '.':
                return '.';
            case '-':
                return '-';
            case '_':
                return '_';
            case ' ':
                return ' ';
            case ':':
                return ':';
        }
        return '%';
    }

    public String guardarArchivo(String img, String nombreImg, String extension, String parteArchivo, boolean finArchivos) {

        String respuesta = "false";
        File r = null;
        String rootPath = File.listRoots()[0].getAbsolutePath();
        Calendar cal = Calendar.getInstance();

        //DEFINIR RUTA
        String rutaImagen = "";
        String rutaImagenPreview = "";

        //1.- :::  VERIFICAR EN DONDE ESTOY PARA GUARDAR EN ( /Sociounico/checklist/imagenes/ - /Sociounico/checklist/preview/ ) O EN EL NUEVO
        //COMENTO ESTO POR QUE AUN NO SE APLICA LA VERSION DE checklist
        try {
            if (!this.verificaServidor(InetAddress.getLocalHost().getHostAddress().toString())) {
                rutaImagen = "/Sociounico/franquicia/videosPartes/";
                rutaImagenPreview = "/Sociounico/franquicia/preview/";

            } else {
                //DONDE GUARDO CUANDO ESTE EN checklist
                rutaImagen = "/franquicia/videosPartes/";
                rutaImagenPreview = "/franquicia/preview/";

            }
        } catch (UnknownHostException e1) {
            // TODO Auto-generated catch block
            //e1.printStackTrace();

            rutaImagen = "/franquicia/videosPartes/";
            rutaImagenPreview = "/franquicia/preview/";

        }

        //String ruta = rootPath + rutaImagen + cal.getInstance().get(Calendar.YEAR) + "/";
        String ruta = rootPath + rutaImagen + cal.getInstance().get(Calendar.YEAR) + "/";
        //String rutaPreview = rootPath + rutaImagenPreview + cal.getInstance().get(Calendar.YEAR) + "/";
        String rutaPreview = rootPath + rutaImagenPreview + cal.getInstance().get(Calendar.YEAR) + "/";
        // //System.out.println(cal.getTime().toString());
        ////System.out.println(File.listRoots()[0].getAbsolutePath());
        r = new File(ruta);

        logger.info("RUTA: " + ruta);
        if (r.mkdirs()) {
            logger.info("SE HA CREADA LA CARPETA");
        } else {
            logger.info("EL DIRECTORIO YA EXISTE");
        }
        FileOutputStream fos = null;
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] imgDecodificada = decoder.decodeBuffer(img);

            byte[] nomDecodificado = decoder.decodeBuffer(nombreImg);
            byte[] extDecodificada = decoder.decodeBuffer(extension);
            // //System.out.println(ruta+new String(imgDecodificada)+"."+new
            // String(extDecodificada));
            String nomDecodificadoStr = UtilString.cleanFilename(new String(nomDecodificado));

            String extDecodificadaStr = UtilString.cleanFilename(new String(extDecodificada));

            String rutaTotal = ruta + nomDecodificadoStr + "." + extDecodificadaStr;
            logger.info("Ruta total " + rutaTotal);

            String rutalimpia = rutaTotal;

            logger.info("Ruta Limpia " + rutalimpia);

            FileOutputStream fileOutput = new FileOutputStream(rutalimpia);

            try {
                BufferedOutputStream bufferOutput = new BufferedOutputStream(fileOutput);
                // se escribe el archivo decodificado
                bufferOutput.write(imgDecodificada);
                bufferOutput.close();

                //System.out.println("Final archivos: "+finArchivos);
                if (finArchivos) {
                    //System.out.println("ENTRE A FINAL");
                    String primerArchivo = rutaTotal.replace(parteArchivo, "_001");

                    if (Commons.validateFirstPart(primerArchivo)) {
                        Unzipping unzipping = new Unzipping(primerArchivo);
                        respuesta = "unzipCorrect";
                    } else {
                        //System.out.println("Wrong file!");
                    }
                }
                //**********************SE GENERA EL PREVIEW*****************
                if (!extDecodificadaStr.equalsIgnoreCase("ZIP")) {
                    // procedimiento para escalar la imagen
                    //BufferedImage imgS = ImageIO.read(new File(ruta + nomDecodificadoStr + "." + extDecodificadaStr));
                    BufferedImage imgS = ImageIO.read(new File(rutalimpia));
                    // int scaleX=(int)(img.getWidth()*factor);
                    // int scaleY=(int)(img.getHeight()*factor);
                    int scaleX;
                    int scaleY;
                    if (imgS.getHeight() > imgS.getWidth()) {
                        scaleX = 120;
                        scaleY = 160;
                    } else {
                        scaleX = 160;
                        scaleY = 120;
                    }
                    Image imgEscala = imgS.getScaledInstance(scaleX, scaleY, Image.SCALE_SMOOTH);
                    BufferedImage bufferImg = new BufferedImage(scaleX, scaleY, BufferedImage.TYPE_INT_RGB);
                    bufferImg.getGraphics().drawImage(imgEscala, 0, 0, null);
                    File imgSalida = new File(rutaPreview);

                    if (imgSalida.mkdirs()) {
                        logger.info("SE HA CREADO LA ESTRUCTURA PARA LAS IMAGENES DE PREVIEW");
                    } else {
                        logger.info("LA ESTRUCTURA DE IMAGES PREVIEW YA EXISTE");
                    }

                    String rutaTotal2 = rutaPreview + nomDecodificadoStr + "." + extDecodificadaStr;

                    String rutalimpia2 = rutaTotal2;

                    //fos = new FileOutputStream(rutaPreview + nomDecodificadoStr + "." + extDecodificadaStr);
                    fos = new FileOutputStream(rutalimpia2);

                    try {
                        ImageIO.write(bufferImg, "JPG", fos);
                    } finally {
                        if (fos != null) {
                            fos.close();
                        }
                    }

                }
            } finally {
                fileOutput.close();
                logger.info("Cerrando archivo ");
            }

            if (!respuesta.equals("unzipCorrect")) {
                respuesta = "true";
            }

            //AL FINALIZAR DE GUARDAR LA IMAGEN SE INICIA EL PROCESO DE REENVIO, TRUE SI ESTOY EN checklist
            //SE COMENTA POR QUE AUN NO ESTA IMPLEMENTADO
            if (!this.verificaServidor(InetAddress.getLocalHost().getHostAddress().toString())) {
                ///logger.info("se envia a otro servidor");
                this.enviarImagenFrq(img, nombreImg, extension);
            }
            //FIN DE PROCESO DE REENVIO
        } catch (Exception e) {
            // //System.out.println("ERROR: "+e.getMessage());
            logger.info("Algo paso " + e);
            //e.printStackTrace();
            respuesta = "false";
        }
        return respuesta;

    }

    @SuppressWarnings({"static-access"})
    @MTOM(threshold = 1073741824)
    public boolean enviarImagen2(OMElement img, String nombreImg, String extension) {

        return false;
    }
}
