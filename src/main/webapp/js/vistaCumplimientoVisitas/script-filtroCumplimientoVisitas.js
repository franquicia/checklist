$body = $("body");
$(document).on({
    ajaxStart: function() {$body.addClass("loading");},
    ajaxStop: function() {$body.removeClass("loading");}    
});

$(window).load(function() {
	fecha();
});

function myFunction() {
	$body.removeClass("loading");
	location.reload();
}

function nobackbutton(){
	window.location.hash="no-back-button";
	window.location.hash="Again-No-back-button" //chrome
	window.onhashchange=function(){window.location.hash="";}
}

var porcentajeL=0;

function fecha(){
	
	document.getElementById("idPais").innerHTML =nombreCecoL;	
	document.getElementById("idDiaActual").innerHTML = "";
	document.getElementById("idTotalDias").innerHTML = "";
	
	
	var hoy = new Date();
	var dd = hoy.getDate();
	var mm = hoy.getMonth()+1;
	var yy = hoy.getFullYear();

	var meses= new Array ('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre');
	
	var mesInicial=seleccionMesL;
	var anoInicial=seleccionAnoL;				
	var diasMes= new Date(anoInicial || new Date().getFullYear(), mesInicial, 0).getDate();
	var ddF = dd;
	var porcentaje=0;
	var por2="";
	var f = new Date();
	
	if((f.getMonth()+1)==mesInicial && f.getFullYear()==anoInicial){
		porcentaje=((f.getDate()-1)*100)/diasMes;
	}else{
		ddF=diasMes;
		porcentaje=100;	
	}

	por2=""+porcentaje;
	var porcenI= por2.split(".");
	porcentajeL=porcenI[0];
	document.getElementById("idDiaActual").innerHTML = ddF+" de "+meses[mesInicial-1]+" es de "+ porcentajeL+"%";
	document.getElementById("idTotalDias").innerHTML = diasMes;
	document.getElementById("idBarPor").innerHTML = porcentajeL+"%";
	document.getElementById("idBarPor").style.width = porcentajeL+"%";
	
}
var nombreCecoPasaL="";

function filtraZona(idCeco,nombreCeco,seleccionAno,seleccionMes,seleccionCanal,seleccionPais){

	setTimeout(myFunction, 20000);
	$body.addClass("loading");
	nombreCecoPasaL=nombreCecoL+"/"+lugarPaisL+"/"+nombreCeco;
	
	$("#formTerri").append("<input type='hidden' name='porcentajeG' id='porcentajeG' value='"+porcentajeG+"' />");
	$("#formTerri").append("<input type='hidden' name='banderaCecoPadre' id='banderaCecoPadre' value='"+banderaCecoPadreL+"' />");
	$("#formTerri").append("<input type='hidden' name='nivelPerfil' id='nivelPerfil' value='"+nivelPerfilL+"' />");
	$("#formTerri").append("<input type='hidden' name='idCeco' id='idCeco' value='"+idCeco+"' />");
	$("#formTerri").append("<input type='hidden' name='idCecoPadre' id='idCecoPadre' value='"+idCecoPadreL+"' />");
	$("#formTerri").append("<input type='hidden' name='nombreCeco' id='nombreCeco' value='"+nombreCecoPasaL+"' />");
	$("#formTerri").append("<input type='hidden' name='seleccionAno' id='seleccionAno' value='"+seleccionAno+"' />");
	$("#formTerri").append("<input type='hidden' name='seleccionMes' id='seleccionMes' value='"+seleccionMes+"' />");
	$("#formTerri").append("<input type='hidden' name='seleccionCanal' id='seleccionCanal' value='"+seleccionCanal+"' />");
	$("#formTerri").append("<input type='hidden' name='seleccionPais' id='seleccionPais' value='"+seleccionPais+"' />");
	
	document.getElementById("formTerri").submit();
	
	return true;
}

function filtraZonaDes(idCeco,nombreCeco,seleccionAno,seleccionMes,seleccionCanal,seleccionPais){
	
	setTimeout(myFunction, 20000);
	$body.addClass("loading");
	nombreCecoPasaL=nombreCecoL+"/"+lugarPaisL+"/"+nombreCeco;
	
	$("#formTerri").append("<input type='hidden' name='porcentajeG' id='porcentajeG' value='"+porcentajeG+"' />");
	$("#formTerri").append("<input type='hidden' name='banderaCecoPadre' id='banderaCecoPadre' value='"+banderaCecoPadreL+"' />");
	$("#formTerri").append("<input type='hidden' name='nivelPerfil' id='nivelPerfil' value='"+nivelPerfilL+"' />");
	$("#formTerri").append("<input type='hidden' name='idCeco' id='idCeco' value='"+idCeco+"' />");
	$("#formTerri").append("<input type='hidden' name='idCecoPadre' id='idCecoPadre' value='"+idCecoPadreL+"' />");
	$("#formTerri").append("<input type='hidden' name='nombreCeco' id='nombreCeco' value='"+nombreCecoPasaL+"' />");
	$("#formTerri").append("<input type='hidden' name='seleccionAno' id='seleccionAno' value='"+seleccionAno+"' />");
	$("#formTerri").append("<input type='hidden' name='seleccionMes' id='seleccionMes' value='"+seleccionMes+"' />");
	$("#formTerri").append("<input type='hidden' name='seleccionCanal' id='seleccionCanal' value='"+seleccionCanal+"' />");
	$("#formTerri").append("<input type='hidden' name='seleccionPais' id='seleccionPais' value='"+seleccionPais+"' />");
	
	document.getElementById("formTerri").submit();

	return true;
}

var valida=-1;
function muestraZonas(numOp,idCeco,seleccionAno,seleccionMes,seleccionCanal,seleccionPais){
	
	if(valida!=numOp){
		valida=numOp;	
		
		document.getElementById("idLlenaZonas"+numOp).innerHTML="";
		//alert(" "+numOp+" "+idCeco+" "+seleccionAno+" "+seleccionMes+" "+seleccionCanal+" "+seleccionPais);
		
		
		$.ajax({
			type : "GET",
			url :'../ajaxFiltroSucursal.json?idCeco='+idCeco+'&banderaCecoPadre='+0+'&seleccionAno='+seleccionAno
							+ '&seleccionMes='+seleccionMes+'&seleccionCanal='+seleccionCanal+'&seleccionPais='+seleccionPais+'&valorPila='+0,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {

				if (json == null) {
					//alert('Sin Datos');
					$("#idLlenaZonas"+numOp).append("<span class='Gris1'>Sin Datos</span>");
				}else{					

					var adjunta="";
					var adjuntaOtro="";
					var adjuntaNomCheck="";
				
					for (var a = 1; a < Object.keys(json).length; a++) {
						adjunta="";						
						for (var b = 0; b < Object.keys(json[a]).length; b++) {	
							var aux=0;
							var div = "<div class='radio amarilloN' style='color:black;'>";
							if(json[a][b].total >= porcentajeG){
								div = "<div class='radio verdeN'>";
								aux=1;
							}
							if(json[a][b].total < (porcentajeG-5)){
								div = "<div class='radio rojo'>";
								//div = "<div class='radio amarilloN' style='color:black;'>";
								aux=1;
							}
							
							if(aux!=1){
								adjunta+="<td style='width: "+64/conteoL+"%;'>" +div+	
										"<div class='radio2'>" +
										""+json[a][b].total+"%" +
										"</div>"+
										"</div>" +
										"</td>";
								//alert("Paso");
							}
							else{
								adjunta+="<td style='width: "+64/conteoL+"%;'>" +div+	
										"<div class='radio1'>" +
											""+json[a][b].total+"%" +
										"</div>"+
										"</div>" +
										"</td>";
							}
						}
						

						adjuntaOtro+="<tr class='pointer' onclick='filtraZona("+json[a][0].idCeco+",&apos;"+json[a][0].descCeco+"&apos;,"+seleccionAno+","+seleccionMes+","+seleccionCanal+","+seleccionPais+");'>"+
										"<td>"+json[a][0].descCeco+"<span>"+json[a][0].numSuc+" suc</span></td>"+
										adjunta+"" +
									"</tr>";							
					}
					//alert(Object.keys(json[0]).length);
					if(Object.keys(json[0]).length>0){
						for (var c = 0; c < Object.keys(json[0]).length; c++) {
							if(c==0)
								adjuntaNomCheck="<td><span>Zona</span></td>";							
							adjuntaNomCheck+="<td><span class='centrarSpan'>"+json[0][c].nombreCheck+"</span></td>";
						}
					}else{
						adjuntaNomCheck="<td colspan='4'><span>Zona</span></td>";
					}					
					
					$("#idLlenaZonas"+numOp).append("" +
							"<table class='bloquer subnivel' style='max-width: 800px; min-width: 350px;'>"+
								"<tbody>"+
									"<tr>"+
										adjuntaNomCheck+
										//(Object.keys(json[0]).length<4 ? "<td colspan='4'><span>Zona</span></td>":"<td colspan='"+(Object.keys(json[0]).length)+(1)+"'><span>Zona</span></td>")+										
									"</tr>" +
									adjuntaOtro+
								"</tbody>" +
							"</table>");
				
				} 				
			},
			error : function() {
				//alert('Favor de actualizar la pagina');
			}
		});	
		
	}
}

function muestraZonasUno(numOp,idCeco,seleccionAno,seleccionMes,seleccionCanal,seleccionPais){
		
	if(valida!=numOp){
		valida=numOp;	
		
		document.getElementById("idLlenaZonas"+numOp).innerHTML="";
		//alert(" "+numOp+" "+idCeco+" "+seleccionAno+" "+seleccionMes+" "+seleccionCanal+" "+seleccionPais);
		
		$.ajax({
			type : "GET",
			url :'../ajaxFiltroSucursal.json?idCeco='+idCeco+'&banderaCecoPadre='+0+'&seleccionAno='+seleccionAno
							+ '&seleccionMes='+seleccionMes+'&seleccionCanal='+seleccionCanal+'&seleccionPais='+seleccionPais+'&valorPila='+0,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {

				if (json == null) {
					//alert('Sin Datos');
					$("#idLlenaZonas"+numOp).append("<span class='Gris1'>Sin Datos</span>");
				}else{					

					var adjunta="";
					var adjuntaOtro="";
					var adjuntaNomCheck="";
					
					for (var a = 1; a < Object.keys(json).length; a++) {
						
						adjunta="";
						
						for (var b = 0; b < Object.keys(json[a]).length; b++) {
							var div = "<div class='radio amarilloN' style='color:black;'>"+"<div class='radio1'>"+json[a][b].total;
							if(json[a][b].total >= porcentajeG)
								div = "<div class='radio verdeN'>"+"<div class='radio1'>"+json[a][b].total;
							if(json[a][b].total < (porcentajeG-5))
								div = "<div class='radio rojo'>"+"<div class='radio1'>"+json[a][b].total;
							
							adjunta+="<td style='width: "+64/conteoL+"%;'>"+div+"%</div></div>"+	
									"</td>";							
						}

						adjuntaOtro+="<tr class='pointer' onclick='filtraZona("+json[a][0].idCeco+",&apos;"+json[a][0].descCeco+"&apos;,"+seleccionAno+","+seleccionMes+","+seleccionCanal+","+seleccionPais+");'>"+
										"<td>"+json[a][0].descCeco+"<span>"+json[a][0].numSuc+" suc</span></td>"+
										adjunta+"" +
									"</tr>";
							
					}
					if(Object.keys(json[0]).length>0){
						for (var c = 0; c < Object.keys(json[0]).length; c++) {
							if(c==0)
								adjuntaNomCheck="<td><span>Zona</span></td>";							
							adjuntaNomCheck+="<td><span class='centrarSpan'>"+json[0][c].nombreCheck+"</span></td>";
						}
					}else{
						adjuntaNomCheck="<td colspan='4'><span>Zona</span></td>";
					}	
					
					$("#idLlenaZonas"+numOp).append("" +
							"<table class='bloquer subnivel'>"+
								"<tbody>"+
									"<tr>"+
										adjuntaNomCheck+
										//(Object.keys(json[0]).length<4 ? "<td colspan='4'><span>Zona</span></td>":"<td colspan='"+Object.keys(json[0]).length+"'><span>Zona</span></td>")+										
									"</tr>" +
									adjuntaOtro+
								"</tbody>" +
							"</table>");
				
				} 	
				//oculta();
			},
			error : function() {
				//alert('Favor de actualizar la pagina');
			}
		});	
		
	}
}

function oculta(){
	jQuery(function() {
		$(".subSeccion").hide();

		var allPanels = $('.accordion3 > dd').hide();

		jQuery('.accordion3 > dt').on('click', function() {
			$this = $(this);
			//the target panel content
			$target = $this.next();
			
			//alert('this '+ JSON.stringify($target));
			
			
			jQuery('.accordion3 > dt').removeClass('accordion3-active');
			if ($target.hasClass("in")) {
				$this.removeClass('accordion3-active');
				$target.slideUp();
				$target.removeClass("in");
				$(".subSeccion").hide();
				
			} else {
				$this.addClass('accordion3-active');
				jQuery('.accordion3 > dd').removeClass("in");
				$target.addClass("in");
				$(".subSeccion").show();
				
				jQuery('.accordion3 > dd').slideUp();
				$target.slideDown();
			}
		})

	})
}

var lugarPaisL="";

function cambiaNombre(nombre){
	lugarPaisL=nombre;
	document.getElementById("idOpcTerr").innerHTML = "";
	document.getElementById("idOpcTerr").innerHTML = nombre;
}

