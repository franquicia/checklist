package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.SoporteExpDTO;

public class SoporteExpRowMapper implements RowMapper<SoporteExpDTO> {

	public SoporteExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		SoporteExpDTO SoporteExpDTO = new SoporteExpDTO();
		
		SoporteExpDTO.setCeco(rs.getString("FCID_CECO"));
		SoporteExpDTO.setPais(rs.getInt("FIID_PAIS"));
		SoporteExpDTO.setNegocio(rs.getInt("FIID_NEGOCIO"));
		SoporteExpDTO.setNombCeco(rs.getString("FCNOMBRE"));
		SoporteExpDTO.setCecoSup(rs.getInt("FICECO_SUPERIOR"));
		SoporteExpDTO.setStatus(rs.getString("FIACTIVO"));
		SoporteExpDTO.setTipoSuc(rs.getString("FCTIPOSUC"));
		SoporteExpDTO.setUsuario(rs.getString("FCUSUARIO_MOD"));
		SoporteExpDTO.setFecha(rs.getString("FDFECHA_MOD"));

	
		return SoporteExpDTO;
	}

}


