package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.EmpContacExtDAO;
import com.gruposalinas.checklist.domain.EmpContacExtDTO;

public class EmpContacExtBI {

	private static Logger logger = LogManager.getLogger(EmpContacExtBI.class);

private List<EmpContacExtDTO> listafila;
	
	@Autowired
	EmpContacExtDAO empContacExtDAO;
		
	
	public int inserta(EmpContacExtDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = empContacExtDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean elimina(String idTab){
		boolean respuesta = false;
		
		try {
			respuesta = empContacExtDAO.elimina(idTab);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<EmpContacExtDTO> obtieneDatos( String ceco) {
		
		try {
			listafila =  empContacExtDAO.obtieneDatos(ceco);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<EmpContacExtDTO> obtieneInfo() {

		try {
			listafila = empContacExtDAO.obtieneInfo();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de Contacto Externo");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	
	public boolean actualiza(EmpContacExtDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = empContacExtDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	

}