package com.gruposalinas.checklist.dao;


public class SearchHistoryDAO {

	/*
	@Autowired
	public MongoTemplate mongoTemplate;

	public static final String COLLECTION_NAME = "searchHistory";

	public void addSearchHistory(SearchHistory searchHistory) {

		searchHistory.setId(UUID.randomUUID().toString());
		mongoTemplate.insert(searchHistory, COLLECTION_NAME);

	}

	public List<SearchHistory> listSearchHistory() {
		return mongoTemplate.findAll(SearchHistory.class, COLLECTION_NAME);
	}

	public void deleteSearchHistory(SearchHistory searchHistory) {
		mongoTemplate.remove(searchHistory, COLLECTION_NAME);
	}

	public void updateSearchHistory(SearchHistory searchHistory) {
		mongoTemplate.insert(searchHistory, COLLECTION_NAME);
	}

	public List<DBObject> getSearchHistory(String find) {

		DBObject findCommand = new BasicDBObject("$text", new BasicDBObject("$search", find));

		DBObject projectCommand = new BasicDBObject("score", new BasicDBObject("$meta", "textScore"));

		DBObject sortCommand = new BasicDBObject("score", new BasicDBObject("$meta", "textScore"));
		DBCursor result = mongoTemplate.getCollection(COLLECTION_NAME).find(findCommand, projectCommand)
				.sort(sortCommand);

		return result.toArray();

	}

	public List<DBObject> findHistory(String find) {
		
		DBObject findCommand = new BasicDBObject("$text", new BasicDBObject("$search", find));

		DBObject projectCommand = new BasicDBObject("score", new BasicDBObject("$meta", "textScore"));

		DBObject sortCommand = new BasicDBObject("score", new BasicDBObject("$meta", "textScore"));
		DBCursor result = mongoTemplate.getCollection(COLLECTION_NAME).find(findCommand, projectCommand)
				.sort(sortCommand).limit(10);

		return result.toArray();

	}
	
	public Iterable<DBObject> groupByFileName() {

		BasicDBObject $groupId = new BasicDBObject("_id", "$fileName");

		$groupId = $groupId.append("count", new BasicDBObject("$sum", 1));

		$groupId = $groupId.append("avgScore", new BasicDBObject("$avg", "$score"));

		BasicDBObject $group = new BasicDBObject("$group", $groupId);

		Iterable<DBObject> output = (Iterable<DBObject>) mongoTemplate.getCollection(COLLECTION_NAME)
				.aggregate(Arrays.asList(

						(DBObject) new BasicDBObject("$sort", new BasicDBObject("count", -1).append("avgScore", -1)),

						(DBObject) $group

				)).results();

		return output;

	}*/

}
