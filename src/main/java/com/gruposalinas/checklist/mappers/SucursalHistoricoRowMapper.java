package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.SucursalHistDTO;


public class SucursalHistoricoRowMapper implements RowMapper<SucursalHistDTO> {

	public SucursalHistDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		SucursalHistDTO sucursalDTO = new SucursalHistDTO();
		
		sucursalDTO.setIdHistoricoSuc(rs.getInt("FIID_SUCURSALH"));
		sucursalDTO.setIdPais(rs.getInt("FIID_PAIS"));
		sucursalDTO.setNumSucursal(rs.getString("FIIDNU_SUCURSAL"));
		sucursalDTO.setNombreCC(rs.getString("FCNOMBRECC"));
		sucursalDTO.setLongitud(rs.getString("FCLONGITUD"));
		sucursalDTO.setLatitud(rs.getString("FCLATITUD"));
		sucursalDTO.setFecha(rs.getString("FDFECHA_MOD"));
		sucursalDTO.setTipoSucursal(rs.getInt("FITIPOSUC"));
		
		return sucursalDTO;
	}

}
