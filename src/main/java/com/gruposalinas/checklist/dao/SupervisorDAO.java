package com.gruposalinas.checklist.dao;

import java.util.List;
import com.gruposalinas.checklist.domain.SupervisorDTO;



public interface SupervisorDAO {
	
	public List<SupervisorDTO> obtieneSupervisor(SupervisorDTO bean) throws Exception;
	
	
}
