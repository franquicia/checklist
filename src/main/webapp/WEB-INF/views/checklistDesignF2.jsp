<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script src="/checklist/js/editText/editText.js" type="text/javascript" ></script>
<form:form name="formPreguntas" method="POST" action="checklistDesignF3.htm">


	
	<div id="definicionPregunta" class="seccionD" style="height:30%;">
	
		<input type="hidden" id="nombreChecklist" name="nombreChecklist" value="${nombreChecklist}" />
		<input type="hidden" id="arbolAdmin" name="arbolAdmin" value="0" />
		<input type="hidden" id="numVersion" name="numVersion" value="${checklist.numVersion}" />
		
		<h2>Titulo del checklist: ${nombreChecklist}</h2>
			
		<ul>
			<li>
				
				<input type="text" 
					id="preguntaText"
					placeholder="Pregunta" class="textBoxEstilo"
					 /> 

				<div id="modulo" class="caja"
					style="margin-left: 10px; margin-bottom: 10px; width: 200px;">
					<span class="seleccionado" id="-1">Categoria</span>
					<ul class="listaselect">
						<li value="-1"><a href="#">Categoria</a></li>
						<li value="-2"><a href="#">Agregar nuevo</a></li>

					</ul>
					<span class="trianguloinf"></span>
				</div>

				<div class="tooltip" style="width: 25px;">
					<span class="tooltiptext">Editar</span>
					<button class='botonEditar'
						onclick="return activarEdicionModulo();"
						style="position: relative; right: 0%; margin: 0px;"></button>
				</div>

				<div class="tooltip" style="width: 25px; margin: 5px;">
					<span class="tooltiptext">Eliminar</span>
					<button class='botonEliminar' onclick="return eliminarModulo();"
						style="position: relative; right: 0%; margin: 0px;"></button>
				</div>

				<div id="tipoPreg" class="caja"
					style="margin-left: 10px; margin-bottom: 10px;">
					<span class="seleccionado" id="-1">Tipo de pregunta</span>
					<ul class="listaselect">
						<li value="-1"><a href="#">Tipo de pregunta</a></li>
						<c:forEach items="${tiposPregunta}" var="item">
							<li value="${item.idTipoPregunta }"><a href="#">${item.descripcion}</a></li>
						</c:forEach>
					</ul>
					<span class="trianguloinf"></span>
				</div>



				<button id="agregarPregunta" class="botonAgregar"
					style="">+</button> 
					
					
					<br>

				<input type="text" id="nuevoModulo" class="textBoxEstilo"
				placeholder="Nombre de la categoria" onfocus="cambiarFondo(3);"
				style="width: 20%; display: none;" />

				<div id="moduloPadre" class="caja"
					style="margin-left: 10px; margin-bottom: 10px; display: none;">
					<span class="seleccionado" id="0">Sin modulo padre</span>
					<ul class="listaselect">
						<li value="0"><a href="#">Sin modulo padre</a></li>

					</ul>
					<span class="trianguloinf"></span>
				</div>
				<button id="agregarNuevoModulo" class="botonAgregar"
					style="display: none;" onclick="return agregarNuevoMod();">+</button>


			</li>

			<!-- 
			<li>
				<div style="position: relative;">
				<h3 style="text-align: left;">Ayuda de la pregunta:</h3><br>
				<textarea id="textoAyuda" rows="8" cols="40"></textarea>
				</div>
			</li>
			-->			

		</ul>
			

	</div>
	
	
		<div id="vistaPrevia" class="seccionVP">
			<!-- lista donde se mostran las preguntas de la vista previa -->
			<h3>Preguntas</h3>
			<ul id="preguntas" style="list-style-type: decimal;">
			</ul>

		</div>


	<input type="hidden" id="idChecklist" name="idChecklist" value="${idChecklist}" />
	<input type="hidden" id="preguntasJSON" name="preguntasJSON" value="" />
	
	<div style="margin-top:1.5%; height: 50px; width: 20%; float:right;">
		<button class="logEnviar-btn" style="width: 150px;" id="continuar" name="continuar" >Continuar</button>
	</div>
	
	</form:form>
	
	
	<form:form  name="formAtras" method="POST" action="checklistDesignF1.htm">
	
		<input type="hidden" id="idChecklistAtras" name="idChecklist" value="${idChecklist }" />
		<div style="margin-top:1.5%; height: 50px; width: 20%; float:right;">
			<button class="logEnviar-btn" style="width: 150px; " id="asignarChecklist" name="atras" >Atras</button>
		</div>
	</form:form>


	
			

