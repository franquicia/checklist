package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.MenusSupDTO;

public class MenusSupRowMapper implements RowMapper<MenusSupDTO> {

	public MenusSupDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		MenusSupDTO menuDTO = new MenusSupDTO();
		
		menuDTO.setIdMenu(rs.getInt("FIMENU_ID"));
		menuDTO.setDescMenu(rs.getString("FCDESCRIPCION"));
		menuDTO.setActivo(rs.getInt("FIACTIVO"));
		menuDTO.setUsuaModificado(rs.getString("FCUSUARIO_MOD"));
		menuDTO.setFechaModificacion(rs.getString("FDFECHA_MOD"));
		
		return menuDTO;
		
		
	}

}
