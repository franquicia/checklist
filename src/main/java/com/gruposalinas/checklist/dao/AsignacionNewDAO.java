package com.gruposalinas.checklist.dao;

import java.util.Map;

public interface AsignacionNewDAO {
	
	public Map<String,Object> ejecutaAsignaciones() throws Exception;
	
	public Map<String, Object> asignacionEspecial(String userNew, String userOld)throws Exception;
	
	public Map<String, Object> ejecutaAsignacion(String ceco, int puesto, int checklist) throws Exception;

}
