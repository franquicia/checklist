package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.CanalDTO;
import com.gruposalinas.checklist.mappers.CanalRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class CanalDAOImpl extends DefaultDAO implements CanalDAO {

    private static Logger logger = LogManager.getLogger(CanalDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaCanal;
    private DefaultJdbcCall jdbcActualizaCanal;
    private DefaultJdbcCall jdbcEliminaCanal;
    private DefaultJdbcCall jdbcBuscaCanal;
    private DefaultJdbcCall jdbcBuscaCanales;

    public void init() {

        jdbcInsertaCanal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CANAL")
                .withProcedureName("SP_INS_CANAL");

        jdbcActualizaCanal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CANAL")
                .withProcedureName("SP_ACT_CANAL");

        jdbcEliminaCanal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CANAL")
                .withProcedureName("SP_DEL_CANAL");

        jdbcBuscaCanal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CANAL")
                .withProcedureName("SP_SEL_CANAL")
                .returningResultSet("RCL_CANAL", new CanalRowMapper());

        jdbcBuscaCanales = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CANAL")
                .withProcedureName("SP_SEL_G_CAN")
                .returningResultSet("RCL_CANAL", new CanalRowMapper());

    }

    public int insertaCanal(CanalDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        int idCanal = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_DESCRIPCION", bean.getDescrpicion())
                .addValue("PA_ACTIVO", bean.getActivo());

        out = jdbcInsertaCanal.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_CANAL.SP_INS_CANAL}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_FIID_CANAL");
        idCanal = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar Canal");
        } else {
            return idCanal;
        }

        return idCanal;
    }

    public boolean actualizaCanal(CanalDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_CANAL", bean.getIdCanal())
                .addValue("PA_DESCRIPCION", bean.getDescrpicion())
                .addValue("PA_ACTIVO", bean.getActivo());

        out = jdbcActualizaCanal.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_CANAL.SP_ACT_CANAL}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Canal con id : " + bean.getIdCanal());
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaCanal(int idCanal) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_CANAL", idCanal);

        out = jdbcEliminaCanal.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_CANAL.SP_DEL_CANAL}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al eliminar Canal con id: " + idCanal);
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<CanalDTO> buscaCanal(int idCanal) throws Exception {

        Map<String, Object> out = null;

        List<CanalDTO> listaCanal = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_CANAL", idCanal);

        out = jdbcBuscaCanal.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_CANAL.SP_SEL_CANAL}");

        listaCanal = (List<CanalDTO>) out.get("RCL_CANAL");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Canal  con id: " + idCanal);
        } else {
            return listaCanal;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<CanalDTO> buscaCanales(int activo) throws Exception {

        Map<String, Object> out = null;

        List<CanalDTO> listaCanales = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ACTIVO", activo);

        out = jdbcBuscaCanales.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_CANAL.SP_SEL_G_CAN}");

        listaCanales = (List<CanalDTO>) out.get("RCL_CANAL");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Canales");
        } else {
            return listaCanales;
        }

        return null;

    }

}
