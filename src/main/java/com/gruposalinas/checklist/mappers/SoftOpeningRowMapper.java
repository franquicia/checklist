package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.SoftOpeningDTO;

public class SoftOpeningRowMapper implements RowMapper<SoftOpeningDTO> {

	public SoftOpeningDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		SoftOpeningDTO SoftOpeningDTO = new SoftOpeningDTO();
		
		SoftOpeningDTO.setCeco(rs.getInt("FCID_CECO"));
		SoftOpeningDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
	    SoftOpeningDTO.setBitacora(rs.getInt("FIID_BITGRAL"));
	    SoftOpeningDTO.setAux(rs.getString("FCAUX"));
	    SoftOpeningDTO.setRecorrido(rs.getInt("FCRECORRIDO"));
	    SoftOpeningDTO.setFechafin(rs.getString("FDFECHA_FIN"));
	    SoftOpeningDTO.setFechaini(rs.getString("FDFECHA_INICIO"));
	    SoftOpeningDTO.setPeriodo(rs.getString("FCPERIODO"));
		
		return SoftOpeningDTO;
	}

}


