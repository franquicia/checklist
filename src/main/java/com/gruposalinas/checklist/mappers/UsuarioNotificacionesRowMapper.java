package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.MovilInfoDTO;


public class UsuarioNotificacionesRowMapper implements RowMapper<MovilInfoDTO> {
	public MovilInfoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		MovilInfoDTO movilDTO = new MovilInfoDTO();
		
		movilDTO.setIdMovil(rs.getInt("FIIDMOVIL"));
		movilDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
		movilDTO.setNombreU(rs.getString("FCNOMBRE"));
		movilDTO.setIdPuesto(rs.getString("FIID_PUESTO"));
		movilDTO.setFecha(rs.getString("FDFECHA"));
		movilDTO.setSo(rs.getString("FCSO"));
		movilDTO.setVersion(rs.getString("FCVERSION"));
		movilDTO.setModelo(rs.getString("FCMODELO"));
		movilDTO.setFabricante(rs.getString("FCFABRICANTE"));
		movilDTO.setNumMovil(rs.getString("FINUMMOVIL"));
		movilDTO.setTipoCon(rs.getString("FCTIPOCON"));
		movilDTO.setIdentificador(rs.getString("FCIDENTIFICADOR"));
		movilDTO.setVersionApp(rs.getString("FCVERSIONAPP"));
		movilDTO.setToken(rs.getString("FCTOKEN"));
		
		
		return movilDTO;
	}
}
