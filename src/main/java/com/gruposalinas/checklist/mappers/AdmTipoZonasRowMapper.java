package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.AdmTipoZonaDTO;


public class AdmTipoZonasRowMapper implements RowMapper<AdmTipoZonaDTO> {

	public AdmTipoZonaDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AdmTipoZonaDTO admZonasDTO = new AdmTipoZonaDTO();

		admZonasDTO.setIdTipoZona(String.valueOf(rs.getInt("FIIDTIPOZO")));		
		admZonasDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
		admZonasDTO.setIdStatus(String.valueOf(rs.getInt("FIIDESTATUS")));
		
		return admZonasDTO;
	}
}
