<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>Reporte Supervisi&oacute;n</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervision/estilos.css">
</head>

<body ng-app="app" ng-controller="validarForm">



  <div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="${pageContext.request.contextPath}/img/supervision/btn_hamburguer.svg" alt="Men� principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="inicio.htm"><img src="${pageContext.request.contextPath}/img/supervision/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.htm" class="selMenu">
          <div>Salir</div>
          <div><img src="${pageContext.request.contextPath}/img/supervision/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="reporteUsuario.htm">Reporte Asegurador</a>
		</div>
    	Consulta
    </div>

	<!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervision/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Men� Principal<br>
          </div>
        </div>
        <%-- <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la p�gina">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> --%>
        <div id="menu">
        	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuAseguramiento.jsp" flush="true">
				<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuAseguramiento.jsp" />
			</tiles:insertTemplate>
          <!-- <ul class="l-navegacion nivel1">
          		<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Asegurador</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>B�squeda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>B�squeda Asegurador</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galer�a de Evidencias</div></a>
				</li>
			</ul> -->
        </div>
      </div>
    </div>

    <!-- Contenido -->
    <div class="contSecc">
    
    <div class="liga">
		<a href="#" onclick="javascript:history.go(-2)" class="liga">
		<img style="height: 15px; width: 15px;" src="${pageContext.request.contextPath}/img/supervision/arrowmleft.png">
		<span>P�GINA ANTERIOR</span>
		</a>
		</div>
		<br>
		
		<div class="titSec"></div>
		<div class="gris">
		
			<div class="datProt">
				<div>
					<div><strong>Asegurador:</strong> ${nomUsuario}</a></div>
					<div><strong>N�mero de empleado:</strong> ${idUsuario}</a></div>
					<table>
						<tbody>
							<table>
								<tr>
									<td><strong>Sucursal: </strong>${nomCeco}</td>
								</tr>
							</table>
						</tbody>
					</table>
				</div>
				</div>
		</div>
		
		<br>
		
    
    <input id="buscar" maxlength="50" type="text" class="form-control" placeholder="B�squeda de Fecha" />
    
	<c:choose>
		<c:when test="${datos == 1}">
		
	
		<div class="titSec">Selecciona la fecha que desea consultar.</div>
		<div class="gris">
			<div class="divCol3">
					<table id="tablaUserBit" class="tblGeneral">
						<thead>
							<tr>
								<th>Fecha en que se realizo el protocolo</th>
							</tr>
						</thead>
						<tbody>
						
						<c:forEach var="lista" items="${lista}">
								<tr>
								<c:url value="/central/reporteUsuProtocolo.htm" var="archivoPasivo" />
								<form:form method="POST" action="${archivoPasivo}" model="command" name="form1" id="form1">
									<td>
										<u><a href="#" onclick="javascript:getIdCeco('${lista.fechaTermino}')" class="txtVerde">
										<c:out value="${lista.fechaTermino}"></c:out></a></u>
									</td>
										<input id="idUsuario" name="idUsuario" type="hidden" value="${idUsuario}" />
										<input id="nomUsuario" name="nomUsuario" type="hidden" value="${nomUsuario}" />
										<input id="idCeco" name="idCeco" type="hidden" value="${idCeco}" />
										<input id="nomCeco" name="nomCeco" type="hidden" value="${nomCeco}" />
										<input id="fechaTermino" name="fechaTermino" type="hidden" value="" />
								</form:form>
								</tr>
						</c:forEach>							
						
						</tbody>
					</table>
			</div>	
		</div>		
		</c:when>
		
		<c:when test="${paso == 0}">
			<div class="divResultado">				
					<div class="tit tCenter"><span ><b> NO SE ENCONTRARON DETALLES PARA CONSULTA.</b></span>
						<br><br>
						<div><b>No se encontraron resultados.</b></div>					
					</div>
			</div>
		</c:when>
		</c:choose>
				
	</div>	<!-- Fin conSecc -->
	
	<!-- Footer -->
	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualizaci�n Febrero 2019</div>
	</div>
</div><!-- Fin page -->



</body>
</html>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/custom-file-input.js"></script><!--Unpload -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/funciones.js"></script>

<script>

document.querySelector("#buscar").onkeyup = function(){
    $TableFilter("#tablaUserBit", this.value);
}

$TableFilter = function(id, value){
    var rows = document.querySelectorAll(id + ' tbody tr');
    for(var i = 0; i < rows.length; i++){
        var showRow = false;
        var row = rows[i];
        row.style.display = 'none';        

        for(var x = 0; x < row.childElementCount; x++){
            if(row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1){
                showRow = true;
                break;
            }
        }        

        if(showRow){
            row.style.display = null;
        }
    }
}



</script>

<script type="text/javascript">

function getIdCeco(fecha) {
	//alert("Entra");
	document.getElementById('fechaTermino').value = fecha;
	form = document.getElementById("form1");
	//alert("fechaTermino: " + document.getElementById('fechaTermino').value);
	form.submit();
}

</script>
