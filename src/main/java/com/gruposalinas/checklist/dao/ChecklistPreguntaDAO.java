package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;

public interface ChecklistPreguntaDAO {

	public boolean insertaChecklistPregunta(ChecklistPreguntaDTO bean)throws Exception;
	
	public boolean actualizaChecklistPregunta(ChecklistPreguntaDTO bean)throws Exception;
	
	public boolean eliminaChecklistPregunta(int idChecklist, int idPregunta)throws Exception;
	
	public List<ChecklistPreguntaDTO> buscaPreguntas(int idChecklist)throws Exception;
	
	public List<ChecklistPreguntaDTO> obtienePregXcheck(int idChecklist) throws Exception;
	
	public boolean cambiaEstatus()throws Exception;
}
