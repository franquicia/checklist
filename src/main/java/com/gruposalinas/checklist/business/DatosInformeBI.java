package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.DatosInformeDAO;
import com.gruposalinas.checklist.domain.DatosInformeDTO;

public class DatosInformeBI {

	private static Logger logger = LogManager.getLogger(DatosInformeBI.class);

private List<DatosInformeDTO> listafila;
	
	@Autowired
	DatosInformeDAO datosInformeDAO;
		
	
	public int insertaDatosInforme(DatosInformeDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = datosInformeDAO.insertaDatosInforme(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaDatosInforme(String idInforme){
		boolean respuesta = false;
		
		try {
			respuesta = datosInformeDAO.eliminaDatosInforme(idInforme);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<DatosInformeDTO> obtieneDatosDatosInforme( String ceco, String idInforme, String idUsuario) {
		
		try {
			listafila =  datosInformeDAO.obtieneDatosDatosInforme(ceco,idInforme,idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	

	
	public boolean actualizaDatosInforme(DatosInformeDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = datosInformeDAO.actualizaDatosInforme(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	

}