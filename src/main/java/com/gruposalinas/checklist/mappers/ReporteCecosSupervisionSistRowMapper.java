package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteSupervisionSistemasDTO;

public class ReporteCecosSupervisionSistRowMapper implements RowMapper<ReporteSupervisionSistemasDTO> {

	public ReporteSupervisionSistemasDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteSupervisionSistemasDTO reporteCecosSistemas = new ReporteSupervisionSistemasDTO();
		
		reporteCecosSistemas.setIdCecos(rs.getInt("FCID_CECO"));
		reporteCecosSistemas.setNombreCeco(rs.getString("FCNOMBRE"));
		reporteCecosSistemas.setTerminados(rs.getInt("TERMINADOS"));
		reporteCecosSistemas.setAsignados(rs.getInt("ASIGNADOS"));
		return reporteCecosSistemas;
	}
}
