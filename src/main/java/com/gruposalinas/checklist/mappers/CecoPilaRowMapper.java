package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.CecoPilaDTO;

public class CecoPilaRowMapper implements RowMapper<CecoPilaDTO> {

	public CecoPilaDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		CecoPilaDTO ceco = new CecoPilaDTO();
		
		ceco.setIdCeco(rs.getInt("FCID_CECO"));
		ceco.setNombreCeco(rs.getString("FCNOMBRE"));
		ceco.setAsignados(rs.getInt("ASIGNADOS"));
		ceco.setTotal(rs.getInt("TERMINADOS"));
		
		return ceco;
	}
}
