package com.gruposalinas.checklist.business;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.AsignacionDAO;
import com.gruposalinas.checklist.domain.AsignacionDTO;
import com.gruposalinas.checklist.util.UtilFRQ;



public class AsignacionBI {
	
	@Autowired
	AsignacionDAO asignacionDAO;
	
	private Logger logger = LogManager.getLogger(AsignacionBI.class);
	
	
	public List<AsignacionDTO> obtieneAsignaciones(String idChecklist, String idCeco, String idPuesto, String activo){
		
		List<AsignacionDTO > respuesta = null;
				
		try {
			respuesta = asignacionDAO.obtieneAsignaciones(idChecklist, idCeco, idPuesto, activo);
		} catch (Exception e) {
			logger.info("No fue posible consultar las asignaciones");
				
		}
		
		return respuesta;
	}
	
	public boolean insertaAsignacion(AsignacionDTO asignacion){
		
		boolean respuesta = false;
		
		try {
			respuesta = asignacionDAO.insertaAsignacion(asignacion);
		} catch (Exception e) {
			logger.info("No fue posible insertar la asignacion");
				
		}
		
		return respuesta;
	}
	
	public boolean actualizaAsignacion(AsignacionDTO asignacion){
		
		boolean respuesta = false;
		
		try {
			respuesta = asignacionDAO.actualizaAsignacion(asignacion);
		} catch (Exception e) {
			logger.info("No fue posible actualizar la asignacion");
				
		}
		
		return respuesta;
	}
	
	public boolean eliminarAsignacion(AsignacionDTO asignacion){
		
		boolean respuesta = false;
		
		try {
			respuesta = asignacionDAO.eliminaAsignacion(asignacion);
		} catch (Exception e) {
			logger.info("No fue posible eliminar la asignacion");
				
		}
		
		return respuesta;
	}
	

	public Map<String, Object> ejecutaAsignacion(){
		
		Map<String, Object> respuesta = null ;
		
		try {
			respuesta = asignacionDAO.ejecutaAsignacion();
		} catch (Exception e) {
			logger.info("No fue posible eliminar la asignacion");
				
		}
		
		return respuesta;
	}

}
