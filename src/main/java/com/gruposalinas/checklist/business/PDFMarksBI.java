package com.gruposalinas.checklist.business;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;

import com.gruposalinas.checklist.util.UtilFRQ;

public class PDFMarksBI {
	private static Logger logger = LogManager.getLogger(PDFMarksBI.class);
	
	@SuppressWarnings("unused")
	public File getMarks(String fileName, String tagName, HttpServletRequest request) throws Exception {
	    PDDocument pdDoc = null;
	    File file = new File(fileName);
	    File nf = null;
	    
	    Path currentRelativePath = Paths.get("");
	    String s = currentRelativePath.toAbsolutePath().toString();
	    ////System.out.println("Current relative path is: " + s);
	    
	    if (!file.isFile()) {
	      //System.err.println("File " + fileName + " does not exist.");
	      return null;
	    }
	    try {
	    	FileInputStream inputStream = new FileInputStream(file);
	    	
	    	try {
				tagName = new String(tagName.getBytes("ISO-8859-1"), "UTF-8");
	    		PDFParser parser = new PDFParser(inputStream);
			    parser.parse();
			    pdDoc = new PDDocument(parser.getDocument());
			    PDFTextAnnotator pdfAnnotator = new PDFTextAnnotator("UTF-8"); // create new annotator
			    pdfAnnotator.setLineSeparator(" "); // kinda depends on what you want to match
			    pdfAnnotator.initialize(pdDoc);
			    String f = tagName;
			    pdfAnnotator.highlight(pdDoc, f);

			    /*:::::::MODIFICADO:::::*/
			    pdDoc.save("documentoCreado.pdf");
			    nf = new File("documentoCreado.pdf");


			      if (parser.getDocument() != null) 
			        parser.getDocument().close();
			      if (pdDoc != null) 
			        pdDoc.close();
		       	return nf;
	    		
	    	} catch(FileNotFoundException e) {
				logger.info("Ocurrio algo...");
	    	} finally {
	    		inputStream.close();
	    	}
	    } catch (Exception e) {
			logger.info("Ocurrio algo...");
	    }
	return null;
	}
}
