package com.gruposalinas.checklist.business;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.business.TokenBI;
import com.gruposalinas.checklist.domain.TokenDTO;
import com.gruposalinas.checklist.util.UtilCryptoGS;
import com.gruposalinas.checklist.util.UtilDate;

public class UtilValidaPOST {
	
	@Autowired
	TokenBI tokenBI;

	//LrmW7p+t0tWWlzvO5N3vg==&token=NPzdX12JnNntYOg/L4sVdw==;idapl=666;wXx4sjQVD6vMlhTKD5wMvEv7cRzitqW8bV/9ONqCQ+wK2sj5qEJOC0uEuJS9KuuuTQMB53cGJ511uCEj1a+AI2FWUL4lQxz/oaFJLBr/XlY=;1fa11gvBzGYggTGGKOvKBm9OFykI15WIXPcrtQaxFfE=
	@SuppressWarnings({ "static-access", "unchecked", "rawtypes" })
	public boolean getDesifrTokenPOST(String uri)throws IOException, KeyException, GeneralSecurityException {

		UtilCryptoGS cifra = new UtilCryptoGS();
		UtilDate fec = new UtilDate();
		String fecha = fec.getSysDate("ddMMyyyyHHmmss");
		boolean respuesta=false;

		UtilCryptoGS desci = new UtilCryptoGS();
		String uriinf = uri.split("&")[0];
		String urlt = uri.split("&")[1];
		String tk = urlt.replace("token=", "");
		String urides = (cifra.decryptParams(uriinf));
		int uriuser = Integer.parseInt(urides.split("=")[1]);

		String[] a = tk.split(";");
		Map map = new HashMap();

		map.put(a[0].replace(" ", "+"), a[0].replace(" ", "+"));
		map.put(a[1].replace(" ", "+"), a[1].replace(" ", "+"));
		map.put(a[2].replace(" ", "+"), a[2].replace(" ", "+"));
		map.put(a[3].replace(" ", "+"), a[3].replace(" ", "+"));

		String desencriptafinal = desci.decryptMap(map).toString();
		String[] vari = desencriptafinal.split(",");
		String[] valores = new String[4];
		valores[0] = vari[0].substring(0, (vari[0].length()) / 2);
		valores[1] = vari[1].substring(0, (vari[1].length()) / 2);
		valores[2] = vari[2].substring(0, (vari[2].length()) / 2);
		valores[3] = vari[3].substring(0, (vari[3].length()) / 2);

		String variipfinal = "", variidaplfinal = "", variurlfinal = "";

		for (String valor : valores) {
			if (valor.contains("uri")) {
				variurlfinal = "uri=" + valor.split("=")[1];
			}
			if (valor.contains("idapl")) {
				variidaplfinal = "idapl=" + valor.split("=")[1];
			}
			if (valor.contains("ip")) {
				variipfinal = "ip=" + valor.split("=")[1];
			}
		}

		String time = fecha;
		String aux = time.substring(0, 8);
		String ahh = time.substring(8, 10);
		String amm = time.substring(10, 12);
		String ass = time.substring(12, 14);

		boolean compara = false;
		int valseg = 0, rompe = 0;
		int hh = Integer.parseInt(ahh);
		int mm = Integer.parseInt(amm);
		int seg = Integer.parseInt(ass);

		for (int i = 2; i > 0; i--) {
			for (int j = seg; j >= 0; j--) {
				valseg++;
				if (seg < 10) {
					ass = "0" + seg;
				} else {
					ass = "" + seg;
				}
				if (mm < 10) {
					amm = "0" + mm;
				} else {
					amm = "" + mm;
				}
				if (hh < 10) {
					ahh = "0" + hh;
				} else {
					ahh = "" + hh;
				}
				time = aux + ahh + amm + ass;
				TokenDTO token = new TokenDTO();
				token.setFechaHora("fecha=" + time);
				token.setIpOrigen(variipfinal);
				token.setIdAplicacion(variidaplfinal);
				token.setUrlRedirect(variurlfinal);
				token.setToken("hola");

				String str1 = new UtilCryptoGS().generaToken(token);
				str1 = str1.replace(" ", "+");
				str1 = str1.replace("\n", "");
				tk = tk.replace(" ", "+");
				tk = tk.replace("\n", "");
				if (tk.equals(str1)) {
					compara = true;
					rompe++;
					break;
				} else {
					if (valseg >= 60) {
						compara = false;
						break;
					}
				}
				if (seg == 0) {
					if (mm == 0) {
						if (hh == 0) {
							hh = 23;
						} else {
							hh--;
						}
						mm = 59;
					} else {
						mm--;
					}
					seg = 59;
				} else {
					seg--;
				}
			}
			if (rompe == 1)
				break;
		}

		
		if (compara) {
			boolean urlbase = tokenBI.verificaToken(tk, uriuser);
			if (urlbase) {
				respuesta=true;
			} else {
				//System.out.println("error 1");
				respuesta=false;
			}
			if (tk != null)
				respuesta=true;
		} else {
			//System.out.println("error 2");
			respuesta=false;
		}
		return respuesta;
	}
}
