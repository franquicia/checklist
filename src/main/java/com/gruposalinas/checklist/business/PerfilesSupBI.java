package com.gruposalinas.checklist.business;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.PerfilesSupDAO;
import com.gruposalinas.checklist.domain.PerfilesSupDTO;

public class PerfilesSupBI {
		
	private static Logger logger = LogManager.getLogger(PerfilesSupBI.class);
	
	@Autowired
	PerfilesSupDAO perfilesSupDAO;
	List<PerfilesSupDTO> listaConsulta = null;
	int respuesta = 0;

	public int insertaPerfil(int idPerfil, String descPerfil, String activo){	
		try {
			respuesta = perfilesSupDAO.insertPerfil(idPerfil, descPerfil, activo);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}			
		return respuesta;
	}

	public int updatePerfil(int idPerfil, String descPerfil, String activo){		
		try {
			respuesta = perfilesSupDAO.updatePerfil(idPerfil, descPerfil, activo);
		} catch (Exception e) {
			logger.info(e);
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}			
		return respuesta;
	}

	public int deletePerfil(int idPerfil){
		try {
			respuesta = perfilesSupDAO.deletePerfil(idPerfil);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}
		return respuesta;
	}
	
public List<PerfilesSupDTO> getPerfil(String idPerfil){		
		try {
			listaConsulta = perfilesSupDAO.getPerfil(idPerfil);
		} catch (Exception e) {
			logger.info("No fue posible consultar el StoreProcedure: "+e);
		}
		return listaConsulta;
	}

}
