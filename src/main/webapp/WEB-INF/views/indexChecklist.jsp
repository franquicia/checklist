<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="../css/bootstrap/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/jQuery/jquery-ui.css">

	<script	src="../js/jquery/jquery-2.2.4.min.js"></script>
	<script	src="../js/bootstrap.min.js"></script>
	<script	src="../js/jquery/jquery-ui.js"></script>

	<script	src="../js/script-checklist-alta-baja.js"></script>

<title>Soporte - Checklist</title>
</head>
<body>

	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuSoporte.jsp" flush="true">
		<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuSoporte.jsp" />
	</tiles:insertTemplate>

	<br>
	<c:choose>
		<c:when test="${(paso=='1') && (alta=='si')}">
			<div class="container">
				<c:url value = "/soporte/altaUserChecklist.htm" var = "altaUserChecklistUrl" />
				<form:form method="POST" action="${altaUserChecklistUrl}" model="command" name="form1" id="form1">
					<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
						<select id="activo" name="activo" class="form-control">
						  <option value="1">Activo</option>
						  <option value="0">Inactivo</option>
						</select>
						<form:input type="text" class="form-control" id="fechaIni1" placeholder="Fecha inicio" path="fechaIni" autocomplete="off" readonly="true" />
						<form:input type="text" class="form-control" id="fechaResp1" placeholder="Fecha resp" path="fechaResp" autocomplete="off" readonly="true" />
						<form:input type="text" class="form-control" id="idCeco1" placeholder="Ceco" path="idCeco" />
						<form:select id="idChecklist" name="idChecklist" path="idChecklist" class="form-control">
							<form:options items="${listaChecklist}" />
						</form:select>
						<form:input type="text" class="form-control" id="idUsuario1" placeholder="Usuario" path="idUsuario" />
						<input type="submit" class="btn btn-default btn-lg" value="Agregar" onclick="return validaAlta();"/>
					</div>
				</form:form>
			</div>
		</c:when>
		
		<c:when test="${(paso=='2') && (alta=='si')}">
			<c:choose>
				<c:when test="${res==0}">
					<div class="container">
						<div class="alert alert-danger" role="alert">
							<p class="text-center"> 
								<h4>El usuario no fue creado correctamente!</h4>
								<br>Presione el botón de regresar para intentarlo nuevamente.
							<p>
							<br>
							<c:url value="/soporte/altaUserChecklist.htm" var="altaUserChecklistUrl" />
							<form:form method="GET" action="${altaUserChecklistUrl}" model="command" name="form-regresar2">
								<input type="submit" class="btn btn-danger btn-lg" value="Regresar" />
							</form:form>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="container">
						<div class="alert alert-success" role="alert">
							<p class="text-center"> 
								<h4>Usuario dado de alta!</h4>
								<br>Presione el botón de regresar para dar de alta otro usuario.
							<p>
							<br>
							<div class="container">
								<c:url value="/soporte/altaUserChecklist.htm" var="altaUserChecklistUrl" />
								<form:form method="GET" action="${altaUserChecklistUrl}" model="command" name="form-regresar">
									<input type="submit" class="btn btn-success btn-lg" value="Regresar" />
								</form:form>
							</div>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</c:when>

		<c:when test="${(paso=='1') && (alta=='no')}">
			<div class="container">
				<c:url value = "/soporte/bajaUserChecklist.htm" var = "bajaUserChecklistUrl" />
				<form:form method="POST" action="${bajaUserChecklistUrl}" model="command" name="form2" id="form2">
					<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
						<form:input type="text" class="form-control" id="idCheckUsuario2" name="idCheckUsuario2" placeholder="ID Checklist Usuario" path="idCheckUsuario" />
						<input type="submit" class="btn btn-default btn-lg" value="Eliminar" onclick="return validaBaja();"/>
					</div>
				</form:form>
			</div>
		</c:when>
		<c:when test="${(paso=='2') && (alta=='no')}">
			<c:choose>
				<c:when test="${res=='0'}">
					<div class="container">
						<div class="alert alert-danger" role="alert">
							<p class="text-center"> 
								<h4>El usuario no fue eliminado correctamente!</h4>
								<br>Presione el botón de regresar para intentarlo nuevamente.
							<p>
							<br>
							<c:url value="/soporte/bajaUserChecklist.htm" var="bajaUserChecklistUrl" />
							<form:form method="GET" action="${bajaUserChecklistUrl}" model="command" name="form-regresar2">
								<input type="submit" class="btn btn-danger btn-lg" value="Regresar" />
							</form:form>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="container">
						<div class="alert alert-success" role="alert">
							<p class="text-center">
								<h4>Usuario eliminado correctamente!</h4>
								<br>Presione el botón de regresar para eliminar otro usuario.
							<p>
							<br>
							<c:url value="/soporte/bajaUserChecklist.htm" var="bajaUserChecklistUrl" />
							<form:form method="GET" action="${bajaUserChecklistUrl}" model="command" name="form-regresar2">
								<input type="submit" class="btn btn-success btn-lg" value="Regresar" />
							</form:form>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</c:when>
	</c:choose>
	
	<script>
		$("#activo1").val("");
		$("#fechaResp1").val("");
		$("#fechaResp1").val("");
		$("#idCeco1").val("");
		$("#idChecklist1").val("");
		$("#idUsuario1").val("");
		$("#activo1").focus();
	
		$("#idCheckUsuario2").val("");
		$("#idCheckUsuario2").focus();
	</script>

</body>
</html>