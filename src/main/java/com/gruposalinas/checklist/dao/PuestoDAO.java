package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.PuestoDTO;
import com.gruposalinas.checklist.domain.PuestoUsuarioDTO;

public interface PuestoDAO {

	
	public List<PuestoDTO> obtienePuesto() throws Exception;
	
	public List<PuestoDTO> obtienePuestoGeo() throws Exception;
	
	public List<PuestoDTO> obtienePuesto(int idPuesto) throws Exception;
	
	public int insertaPuesto (PuestoDTO bean) throws Exception;
	
	public boolean actualizaPuesto(PuestoDTO bean) throws Exception;
	
	public boolean eliminaPuesto(int idPuesto) throws Exception;
	
	public List<PuestoUsuarioDTO> obtienePuestoUsuario(int idUsuario) throws Exception;
	
}
