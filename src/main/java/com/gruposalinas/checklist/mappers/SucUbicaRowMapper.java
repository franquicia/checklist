package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.SucUbicacionDTO;

public class SucUbicaRowMapper implements RowMapper<SucUbicacionDTO> {

	public SucUbicacionDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		SucUbicacionDTO suc= new SucUbicacionDTO();
		

	    suc.setCeco(rs.getInt("FCID_CECO"));
	    suc.setCalles(rs.getString("FCCALLE"));
	    suc.setCiudad(rs.getString("FCCIUDAD"));
	    suc.setCodigoPos(rs.getString("FCCP"));
	    suc.setNumSuc(rs.getInt("FIIDNU_SUCURSAL"));
	    suc.setNombCeco(rs.getString("CECO"));
	    suc.setIdcanal(rs.getInt("FIID_CANAL"));
	    suc.setCanal(rs.getString("CANAL"));
	    suc.setIdpais(rs.getInt("FIID_PAIS"));
	    suc.setPais(rs.getString("PAIS"));
	    suc.setLongitud(rs.getDouble("FCLONGITUD"));
	    suc.setLatitud(rs.getDouble("FCLATITUD"));
	   
		
	 
		return suc;
	}

}
