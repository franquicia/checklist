package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.BitacoraCheckUsuAdmDAO;

import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.ChecklistUsuarioDTO;


public class BitacoraCheckUsuAdmBI {
	
	private static Logger logger = LogManager.getLogger(BitacoraCheckUsuAdmBI.class);
	
	@Autowired
	BitacoraCheckUsuAdmDAO bitacoraCheckUsuAdmDAO;
	
	List<BitacoraDTO> listaBitacora = null;
	List<BitacoraDTO> listaBitacoraAll = null ;

	public int insertaBitacora(BitacoraDTO bean){
		
		int idBitacora = 0 ;
		
		try{
			
			idBitacora=bitacoraCheckUsuAdmDAO.insertaBitacora(bean);
			
		}catch(Exception e){
			logger.info("No fue posible insertar la Bitacora "+ e);
		}
		
		return idBitacora;
				
	}
	
	public boolean actualizaBitacora(BitacoraDTO bean){
		
		boolean respuesta = false;
		
		try{
			respuesta = bitacoraCheckUsuAdmDAO.actualizaBitacora(bean);
		}catch(Exception e){
			logger.info("No fue posible actualizar la Bitacora "+e);
		}
		
		return respuesta;
		
	}
	
	public boolean eliminaBitacora(int idBitacora) {
	
		boolean respuesta = false;
		
		try{
			respuesta = bitacoraCheckUsuAdmDAO.eliminaBitacora(idBitacora);
		}catch(Exception e){
			logger.info("No fue posible eliminar la Bitacora "+e);
		}
		
		return respuesta;
		
	}
	
	public List<BitacoraDTO> buscaBitacora(){
		
		try {
			listaBitacoraAll = bitacoraCheckUsuAdmDAO.buscaBitacora();
		} catch (Exception e) {
			logger.info("No fue posible consultar la Bitacora "+ e);
		}
		
		return listaBitacoraAll;
		
	}
	
	public List<BitacoraDTO> buscaBitacora(String checkUsua, String idBitacora, String fechaI, String fechaF){
		
		try {
			listaBitacora = bitacoraCheckUsuAdmDAO.buscaBitacora(checkUsua, idBitacora, fechaI, fechaF);
		} catch (Exception e) {
			logger.info("No fue posible consultar la Bitacora "+ e);
		}
		
		return listaBitacora;
		
	}
	
	public List<BitacoraDTO> buscaBitacoraCerradas(String idChecklist) {
		
		try {
			listaBitacora = bitacoraCheckUsuAdmDAO.buscaBitacoraCerradas(idChecklist);
		} catch (Exception e) {
			logger.info("No fue posible consultar la Bitacora "+ e);
		}
		
		return listaBitacora;
		
	}
	public List<BitacoraDTO> buscaBitacoraCerradasR(String idChecklist) {
		
		try {
			listaBitacora = bitacoraCheckUsuAdmDAO.buscaBitacoraCerradasR(idChecklist);
		} catch (Exception e) {
			logger.info("No fue posible consultar la Bitacora "+ e);
		}
		
		return listaBitacora;
		
	}
	
	public int verificaBitacora(int checkUsua){
		
		int idBitacora = 0;
					
		try {
			idBitacora = bitacoraCheckUsuAdmDAO.verificaIdBitacora(checkUsua);
		} catch (Exception e) {
			logger.info("No fue posible obtener el id de la Bitacora " + e);
		}
		
		return idBitacora;
		
	}
	
	public boolean cierraBitacora(){
		
		boolean respuesta = false;
		
		try{
			respuesta = bitacoraCheckUsuAdmDAO.cierraBitacora();
		}catch(Exception e){
			logger.info("No fue posible terminar las Bitacoras "+ e);
		}
		
		return respuesta;
		
	}
	
	//check usua
	List<ChecklistUsuarioDTO> listaCheckUsuario;
	List<ChecklistUsuarioDTO> listaCheckU;
	
	
	public List<ChecklistUsuarioDTO> obtieneCheckUsuario(int idCheckU){
		try{
			listaCheckUsuario = bitacoraCheckUsuAdmDAO.obtieneCheckUsuario(idCheckU);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Checklist Usuario");
						
		}
				
		return listaCheckUsuario;
	}
	
	public List<ChecklistUsuarioDTO> obtieneCheckU(String idUsuario){		
		try{
			listaCheckUsuario = bitacoraCheckUsuAdmDAO.obtieneCheckU(idUsuario);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Checklist Usuario");
						
		}
				
		return listaCheckUsuario;
	}
	
	public List<ChecklistUsuarioDTO> obtieneCheckUsua(String idUsuario, String ceco, String fechaInicio){		
		try{
			listaCheckUsuario = bitacoraCheckUsuAdmDAO.obtieneCheckUsua(idUsuario, ceco, fechaInicio);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Checklist Usuario");
						
		}
				
		return listaCheckUsuario;
	}
	
	public int insertaCheckUsuario (ChecklistUsuarioDTO checkUsuario){
		int idcheckU = 0;
		
		try{
			idcheckU = bitacoraCheckUsuAdmDAO.insertaCheckUsuario(checkUsuario);
		}catch (Exception e) {
			logger.info("No fue posible insertar el Checklist Usuario");
			
		}
		
		return idcheckU;
	}
	
	public boolean actualizaChecklistUsuario(ChecklistUsuarioDTO checkUsuario){
		boolean respuesta = false;
		try{
			respuesta =  bitacoraCheckUsuAdmDAO.actualizaCheckUsuario(checkUsuario);
		}
		catch (Exception e) {
			logger.info("No fue posible actualizar el  Checklist Usuario");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaChecklistUsuario(int idCheckU){
		boolean respuesta = false;
		try{
			respuesta = bitacoraCheckUsuAdmDAO.eliminaCheckUsuario(idCheckU);
		}
		catch(Exception e){
			logger.info("No fue posible borrar el Checklist Usuario");
			
		}
		
		return respuesta;
	}
	
	public boolean desactivaChecklist(int idChecklist){
		
		boolean respuesta = false;
		
		try {
			respuesta= bitacoraCheckUsuAdmDAO.desactivaChecklist(idChecklist);
		} catch (Exception e) {
			logger.info("No fue posible desactivar los Checklist-Usuarios");
			
		}
		
		return respuesta;
		
	}
	
	public boolean desactivaCheckUsua(int idCeco,int idChecklist, int idUsu){
		
		boolean respuesta = false;
		
		try {
			respuesta= bitacoraCheckUsuAdmDAO.desactivaCheckUsua(idCeco, idChecklist,idUsu);
		} catch (Exception e) {
			logger.info("No fue posible desactivar los Checklist-Usuarios");
			
		}
		
		return respuesta;
		
	}
	
	public boolean insertaCheckusuaEspecial(int idUsuario, int idChecklist, String ceco){
		
		boolean respuesta = false;
		
		try {
			respuesta= bitacoraCheckUsuAdmDAO.insertaCheckusuaEspecial(idUsuario, idChecklist, ceco);
		} catch (Exception e) {
			logger.info("No fue posible insertar el Checklist-Usuario");
			
		}
		
		return respuesta;
		
	}
	
}
