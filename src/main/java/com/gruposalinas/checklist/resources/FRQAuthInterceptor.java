package com.gruposalinas.checklist.resources;

import com.gruposalinas.checklist.domain.PerfilUsuarioDTO;
import com.gruposalinas.checklist.domain.RecursoPerfilDTO;
import com.gruposalinas.checklist.domain.TokenDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.util.StrCipher;
import com.gruposalinas.checklist.util.UtilCryptoGS;
import com.gruposalinas.checklist.util.UtilDate;
import com.gruposalinas.checklist.util.UtilFRQ;
import java.io.IOException;
import java.lang.reflect.Method;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class FRQAuthInterceptor implements HandlerInterceptor {

    private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);
    @SuppressWarnings("rawtypes")
    public Map map;

    /*
	 * Caso 1.- NO existe el usuario en sesion, es la primera vez que se manda a
	 * llamar la petición, Se solicita el logueo con llave maestra, se pide el
	 * token y se redirecciona hacía el portal de llave el portal de llave
	 * validara si existe o no usuario logueado, si existe el usuario entonces
	 * redirecciona de regreso al sistema, si no hay sesión entonces lo manda al
	 * logueo de llave
	 *
	 * Caso 2.- NO existe en sesion el usuario, pero es posible que vengan los
	 * parametros encriptados como resultado del proceso de logueo por llave
	 * maestra
	 *
     */
    public void emptyMap() {
        this.map = null;
    }

    @SuppressWarnings({"rawtypes", "static-access"})
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        boolean flag = true;
        UsuarioDTO user = null;
        long startTime = System.currentTimeMillis();
        request.setAttribute("startTime", startTime);
        user = (UsuarioDTO) request.getSession().getAttribute("user");
        Map mapDesencriptado = null;
        map = request.getParameterMap(); // obtenemos los parametros del request
        boolean flagEmptyMap = map.isEmpty(); // Si no tiene parametros

        String uri = request.getRequestURI();
        String uriFinal = request.getQueryString() != null ? uri + "?" + request.getQueryString() : uri + "";
        //logger.info("uri: "+uri);
        //logger.info("uriTotal: "+uriFinal);

        if (uri.contains("/checklist/actuator")) {
            return flag;
        }
        if (uri.contains("/checklist/api-local")) {
            return flag;
        }

        if (uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idapl")
                || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idapl")
                || uriFinal.contains("central/vistaCumplimientoMovil.htm?idapl")
                || uriFinal.contains("central/vistaCumplimientoVisitasMovil.htm?idapl")
                || uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idUsuario")
                || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idUsuario")
                || uriFinal.contains("central/vistaCumplimientoMovil.htm?idUsuario")
                || uriFinal.contains("central/vistaFiltroCumplimientoVisitasReg.htm?idUsuario")
                || uriFinal.contains("central/vistaCumplimientoVisitasMovil.htm?idUsuario")
                || uriFinal.contains("central/vistaFiltroCumplimientoVisitas.htm")
                || uriFinal.contains("central/vistaTerritCumplimientoVisitas.htm")
                || uriFinal.contains("central/vistaDetalleCumplimientoVisitas.htm")
                || uriFinal.contains("central/store.htm")
                || uriFinal.contains("central/storeIndex.htm")
                || uriFinal.contains("central/verificaUsuarioTienda.json")
                || uriFinal.contains("central/principal.htm")
                || uriFinal.contains("central/adminChecklist.htm")
                || uriFinal.contains("central/adminChecklist.json")
                || uriFinal.contains("consultaChecklistAdminService/postAltaProtocolos.json")
                || uriFinal.contains("consultaChecklistAdminService/postLeerExcell.json")
                || uriFinal.contains("consultaChecklistAdminService")
                || uriFinal.contains("catalogosAdmService")
                || (uriFinal.contains("/checklist/soporte/") && uriFinal.contains(".htm?idUsuario"))
                || (uriFinal.contains("/central/vistaProgramaVisitas.htm?idUsuario"))
                || (uriFinal.contains("/central/vistaSupervisionSistemas.htm?idUsuario"))
                || (uriFinal.contains("/consultaChecklistAdminService/"))
                || (uriFinal.contains("ajaxAutocompletaSucursal.json"))
                || (uriFinal.contains("/consultaHallazgoService/"))
                || (uriFinal.contains("/checklistServices/altaUsuCheckEsp2.json")) || (uriFinal.contains("/checklistServices/altaChecklistGpo.json"))
                || (uriFinal.contains("/central/vistaSupervisionSistemasV2.htm?idUsuario"))
                || (uriFinal.contains("central/inicio") && uriFinal.contains(".htm?idUsuario"))
                || (uriFinal.contains("central/seleccionFiltroExpansion") && uriFinal.contains(".htm?idUsuario"))
                || (uriFinal.contains("central/seleccionSucursalHallazgos") && uriFinal.contains(".htm?idUsuario"))) {

            user = null;
        }

        if (user == null) { // No existe el usuario en sessión

            if (uri.contains("/checklist/servicios/") || uri.contains("Core") || uri.contains("/checklist/serviciosDataCenter/") || uri.contains("/checklist/services/") || uri.contains("/checklist/reportes/")
                    || uri.contains("/checklist/errores/") || uri.contains("/checklist/servicioInfoResponse/") || uri.contains("/checklist/servicio/")
                    || uri.contains("/checklist/static/") || uri.contains("tienda/inicio.htm") || (uriFinal.contains("/checklist/soporte/") && uriFinal.contains(".htm?idUsuario"))
                    || uri.contains("checklist/mensajes.htm") || uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idapl")
                    || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idapl") || uriFinal.contains("central/vistaCumplimientoMovil.htm?idapl")
                    || uriFinal.contains("central/vistaCumplimientoVisitasMovil.htm?idapl")
                    || uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idUsuario") || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idUsuario")
                    || uriFinal.contains("central/vistaFiltroCumplimientoVisitasReg.htm?idUsuario")
                    || uriFinal.contains("central/vistaChecklist.htm?idUsuario")
                    || uriFinal.contains("central/vistaTiempoReal.htm?idUsuario")
                    || uriFinal.contains("central/vistaCumplimientoMovil.htm?idUsuario") || uriFinal.contains("central/vistaCumplimientoVisitasMovil.htm?idUsuario") || uriFinal.contains("ejecutaServiciosEncriptados")
                    || uriFinal.contains("central/vistaFiltroCumplimientoVisitas.htm")
                    || uriFinal.contains("central/vistaTerritCumplimientoVisitas.htm")
                    || uriFinal.contains("central/vistaDetalleCumplimientoVisitas.htm")
                    || (uriFinal.contains("/checklistServices/altaUsuCheckEsp2.json")) || (uriFinal.contains("/checklistServices/altaChecklistGpo.json"))
                    || uriFinal.contains("central/vistaProgramaVisitas.htm?idUsuario")
                    || uriFinal.contains("central/adminChecklist.htm")
                    || uriFinal.contains("central/adminChecklist.json")
                    || uriFinal.contains("consultaChecklistAdminService/postAltaProtocolos.json")
                    || uriFinal.contains("consultaChecklistAdminService/postLeerExcell.json")
                    || uriFinal.contains("consultaChecklistAdminService")
                    || uriFinal.contains("catalogosAdmService")
                    || (uriFinal.contains("/consultaChecklistAdminService/"))
                    || (uriFinal.contains("ajaxAutocompletaSucursal.json"))
                    || uriFinal.contains("central/vistaSupervisionSistemas.htm?idUsuario")
                    || uriFinal.contains("central/vistaSupervisionSistemasV2.htm?idUsuario")
                    || (uriFinal.contains("/consultaHallazgoService/"))
                    || uriFinal.contains("central/store.htm") || uriFinal.contains("central/storeIndex.htm") || uriFinal.contains("central/verificaUsuarioTienda.json")
                    || uriFinal.contains("central/principal.htm")
                    || uri.contains("cargaServices/cargaPuntoContacto.json")
                    || uri.contains("cargaServices/cargaPuntoContactoByCeco.json")
                    || (uriFinal.contains("central/inicio") && uriFinal.contains(".htm?idUsuario"))
                    || (uriFinal.contains("central/seleccionFiltroExpansion") && uriFinal.contains(".htm?idUsuario"))
                    || (uriFinal.contains("central/seleccionSucursalHallazgos") && uriFinal.contains(".htm?idUsuario"))) {

                // Aqui van url conocidas que queremos que redireccione aunque no haya sesion
                //logger.info("VAMOS A LOGUEAR SIN LLAVE 0: ");
                String params = request.getQueryString() != null ? "?" + request.getQueryString() : "";
                if (uri.contains("tienda/inicio.htm") && params.contains("nombre")) {
                    flag = true;
                    String urlServer = FRQConstantes.getURLServer();
                    request.getSession().setAttribute("urlServer", urlServer);
                } else if (uri.contains("/checklist/servicios") || uri.contains("Core") || uri.contains("/checklist/serviciosDataCenter/") || uri.contains("/checklist/services/") || uri.contains("/checklist/reportes/")
                        || uri.contains("/checklist/errores/") || uri.contains("/checklist/servicioInfoResponse/") || uri.contains("/checklist/servicio/")
                        || uri.contains("/checklist/static/") || uri.contains("/checklist/mensajes.htm") || (uriFinal.contains("/checklist/soporte/") && uriFinal.contains(".htm?idUsuario"))
                        || uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idapl") || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idapl")
                        || uriFinal.contains("central/vistaCumplimientoMovil.htm?idapl") || uriFinal.contains("central/vistaCumplimientoVisitasMovil.htm?idapl")
                        || uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idUsuario") || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idUsuario")
                        || uriFinal.contains("central/vistaFiltroCumplimientoVisitasReg.htm?idUsuario")
                        || uriFinal.contains("central/vistaChecklist.htm?idUsuario") || uriFinal.contains("ejecutaServiciosEncriptados")
                        || uriFinal.contains("central/vistaTiempoReal.htm?idUsuario")
                        || uriFinal.contains("central/vistaCumplimientoMovil.htm?idUsuario") || uriFinal.contains("central/vistaCumplimientoVisitasMovil.htm?idUsuario")
                        || uriFinal.contains("central/vistaFiltroCumplimientoVisitas.htm")
                        || uriFinal.contains("central/vistaTerritCumplimientoVisitas.htm")
                        || uriFinal.contains("central/vistaDetalleCumplimientoVisitas.htm")
                        || (uriFinal.contains("/checklistServices/altaUsuCheckEsp2.json")) || (uriFinal.contains("/checklistServices/altaChecklistGpo.json"))
                        || uriFinal.contains("central/vistaProgramaVisitas.htm?idUsuario")
                        || uriFinal.contains("central/adminChecklist.htm")
                        || uriFinal.contains("central/adminChecklist.json")
                        || uriFinal.contains("consultaChecklistAdminService/postAltaProtocolos.json")
                        || uriFinal.contains("consultaChecklistAdminService/postLeerExcell.json")
                        || uriFinal.contains("consultaChecklistAdminService")
                        || uriFinal.contains("catalogosAdmService")
                        || (uriFinal.contains("/consultaChecklistAdminService/"))
                        || (uriFinal.contains("ajaxAutocompletaSucursal.json"))
                        || uriFinal.contains("central/vistaSupervisionSistemas.htm?idUsuario")
                        || uriFinal.contains("central/vistaSupervisionSistemasV2.htm?idUsuario")
                        || (uriFinal.contains("/consultaHallazgoService/")
                        || uriFinal.contains("central/store.htm") || uriFinal.contains("central/storeIndex.htm") || uriFinal.contains("central/verificaUsuarioTienda.json")
                        || uriFinal.contains("central/principal.htm"))
                        || uri.contains("cargaServices/cargaPuntoContacto.json")
                        || uri.contains("cargaServices/cargaPuntoContactoByCeco.json")
                        || (uriFinal.contains("central/inicio") && uriFinal.contains(".htm?idUsuario"))
                        || (uriFinal.contains("central/seleccionFiltroExpansion") && uriFinal.contains(".htm?idUsuario"))
                        || (uriFinal.contains("central/seleccionSucursalHallazgos") && uriFinal.contains(".htm?idUsuario"))) {

                    //logger.info("VAMOS A LOGUEAR SIN LLAVE 1: ");
                    if (uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idapl") || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idapl")
                            || uriFinal.contains("central/vistaCumplimientoMovil.htm?idapl") || uriFinal.contains("central/vistaCumplimientoVisitasMovil.htm?idapl")
                            || uriFinal.contains("central/reporteOnlineAperturaMovil.htm?idUsuario") || uriFinal.contains("central/reporteOnlineCierreMovil.htm?idUsuario")
                            || uriFinal.contains("central/vistaFiltroCumplimientoVisitasReg.htm?idUsuario")
                            || uriFinal.contains("central/vistaChecklist.htm?idUsuario")
                            || uriFinal.contains("central/vistaTiempoReal.htm?idUsuario")
                            || uriFinal.contains("central/vistaCumplimientoMovil.htm?idUsuario")
                            || uriFinal.contains("central/vistaCumplimientoVisitasMovil.htm?idUsuario")
                            || uriFinal.contains("central/vistaProgramaVisitas.htm?idUsuario")
                            || uriFinal.contains("central/vistaSupervisionSistemas.htm?idUsuario")
                            || uriFinal.contains("central/vistaSupervisionSistemasV2.htm?idUsuario")
                            || (uriFinal.contains("/checklist/soporte/") && uriFinal.contains(".htm?idUsuario"))
                            //PARA CAMBIO DE LLAVE MAESTRA
                            || (uriFinal.contains("central/inicio") && uriFinal.contains(".htm?idUsuario"))
                            || (uriFinal.contains("central/seleccionFiltroExpansion") && uriFinal.contains(".htm?idUsuario"))
                            || (uriFinal.contains("central/seleccionSucursalHallazgos") && uriFinal.contains(".htm?idUsuario"))) {

                        logger.info("request.getSession.. " + request.getSession());

                        HttpSession session = request.getSession(false);
                        logger.info("session.. " + session.getAttribute("user"));
                        session.invalidate();
                        logger.info("session.. " + session);

                        String uriMovilCompleta = request.getQueryString();
                        logger.info("uriMovilCompleta.. " + uriMovilCompleta);
                        String idUser = "";
                        if (uriFinal.contains("?idUsuario")) {
                            idUser = request.getParameter("idUsuario");
                            setInfoInicial(idUser, "Administrador", request);
                        } else {
                            UtilCryptoGS cifra = new UtilCryptoGS();
                            StrCipher cifraIOS = new StrCipher();
                            int idLlave = Integer.parseInt(request.getParameter("idapl"));
                            logger.info("idLlave.. " + idLlave);
                            String uriMovil = request.getQueryString();
                            uriMovil = uriMovil.split("&")[1];
                            logger.info("uriMovil.. " + uriMovil);
                            String urides = "";
                            if (idLlave == 7) {
                                urides = (cifraIOS.decrypt(uriMovil, FRQConstantes.getLlaveEncripcionLocalIOS()));
                            } else if (idLlave == 666) {
                                urides = (cifra.decryptParams(uriMovil));
                            }
                            logger.info("urides.. " + urides);
                            idUser = urides.split("=")[1];
                        }
                        logger.info("idUser.. " + idUser);
                        UsuarioDTO userSession = new UsuarioDTO();
                        userSession.setIdUsuario(idUser);

                        session = request.getSession();
                        session.setAttribute("user", userSession);

                        logger.info("session.. " + session);
                        logger.info("session.. " + session.getAttribute("user"));

                    }
                    flag = true;
                    String urlServer = FRQConstantes.getURLServer();
                    request.getSession().setAttribute("urlServer", urlServer);
                } else if (uri.contains("checklist/mensajes.htm")) {
                    flag = true;
                    String urlServer = FRQConstantes.getURLServer();
                    request.getSession().setAttribute("urlServer", urlServer);
                } else {
                    //flag = false;
                    //response.sendRedirect("redirecciona.htm");

                    //logger.info("flag:"+flagEmptyMap);
                    /*VALIDA LA SESION*/
                    if (flagEmptyMap) { // Si esta vació
                        flag = getTokenAndRedirect(request, response);
                    } else if (!flagEmptyMap) { // No esta vacío es valido, debemos sacar el usuario y loguearlo en el sistema
                        UtilCryptoGS utilCrypto = new UtilCryptoGS();
                        //logger.info("map: "+map);
                        mapDesencriptado = utilCrypto.decryptMap(map); // Desencriptamos el mapa
                        //logger.info("mapDesencriptado: "+mapDesencriptado);
                        String idUser = "";
                        String nomUser = "";
                        try {
                            idUser = (String) mapDesencriptado.get("employeeid");
                            nomUser = (String) mapDesencriptado.get("name");
                            if (nomUser != null) {
                                nomUser = WordUtils.capitalize(nomUser.toLowerCase());
                            }
                            logger.info("Valor idUser: " + idUser + " valor nomUser: " + nomUser);
                        } catch (Exception e) {
                            logger.info("Ocurrio algo preHandle()");
                        }

                        if (idUser != null) {
                            setInfoInicial(idUser, nomUser, request);
                        } else {
                            flag = getTokenAndRedirect(request, response);
                        }
                    }
                    /*VALIDA LA SESION*/
                }
            } else if (uri.contains("central")) {
                /**
                 * *****************************************************************************************************************************************
                 */
                // Este código se comenta porque no alcanzamos el servidor de
                // llave maestra desde producción y no nos autorizaron los
                // permisos de firewall
                //logger.info("flag 1:"+flagEmptyMap);
                /*VALIDA LA SESION*/
                if (flagEmptyMap) { // Si esta vació
                    flag = getTokenAndRedirect(request, response);
                } else if (!flagEmptyMap) { // No esta vacío es valido, debemos sacar el usuario y loguearlo en el sistema
                    UtilCryptoGS utilCrypto = new UtilCryptoGS();
                    //logger.info("map 1: "+map);
                    mapDesencriptado = utilCrypto.decryptMap(map); // Desencriptamos el mapa
                    //logger.info("mapDesencriptado 1: "+mapDesencriptado);
                    String idUser = "";
                    String nomUser = "";
                    try {
                        idUser = (String) mapDesencriptado.get("employeeid");
                        nomUser = (String) mapDesencriptado.get("name");
                        if (nomUser != null) {
                            nomUser = WordUtils.capitalize(nomUser.toLowerCase());
                        }
                        logger.info("Valor idUser 1: " + idUser + " valor nomUser 1: " + nomUser);
                    } catch (Exception e) {
                        logger.info("Ocurrio algo preHandle()");
                    }
                    //nomUser = WordUtils.capitalize(nomUser.toLowerCase());
                    if (idUser != null) {
                        setInfoInicial(idUser, nomUser, request);
                    } else {
                        flag = getTokenAndRedirect(request, response);
                    }
                }
                /*VALIDA LA SESION*/

                /**
                 * **********************************************************************************************************************************************************
                 */
                /*
				 * Este segmento de código sustituye temporalmente a la llave
				 * maestra
                 */
                // response.sendRedirect( FRQConstantes.getURLServer() +
                // "/"+FRQConstantes.ALIAS_SERVER+"/login.htm" );
                // flag = false;
                /**
                 * **********************************************************************************************************************************************************
                 */
            } else {
                //flag = false;
                //response.sendRedirect("redirecciona.htm");

                //logger.info("flag 2:"+flagEmptyMap);
                /*VALIDA LA SESION*/
                if (flagEmptyMap) { // Si esta vació
                    flag = getTokenAndRedirect(request, response);
                } else if (!flagEmptyMap) { // No esta vacío es valido, debemos sacar el usuario y loguearlo en el sistema
                    UtilCryptoGS utilCrypto = new UtilCryptoGS();
                    //logger.info("map 2: "+map);
                    mapDesencriptado = utilCrypto.decryptMap(map); // Desencriptamos el mapa
                    //logger.info("mapDesencriptado 2: "+mapDesencriptado);
                    String idUser = "";
                    String nomUser = "";
                    try {
                        idUser = (String) mapDesencriptado.get("employeeid");
                        nomUser = (String) mapDesencriptado.get("name");
                        logger.info("Valor idUser 2: " + idUser + " valor nomUser 2: " + nomUser);
                    } catch (Exception e) {
                        logger.info("Ocurrio algo preHandle()");
                    }
                    if (idUser != null) {
                        setInfoInicial(idUser, nomUser, request);
                    } else {
                        flag = getTokenAndRedirect(request, response);
                    }
                }
                /*VALIDA LA SESION*/
            }
        }
        return flag;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {
        // Se ejecuta siempre y cuando el preHandle devuelve true
        long startTime = (Long) request.getAttribute("startTime");
        long endTime = System.currentTimeMillis();
        long executeTime = endTime - startTime;
        String path = request.getRequestURI();
        String clean = request.getRequestURI().replace('\n', '_').replace('\r', '_');
        UsuarioDTO userSession = null;
        RecursoPerfilDTO perfil = null;
        String usuario = "notLogged";
        String alertMsg = "";

        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }

        if (request != null) {
            try {
                HttpSession session = request.getSession();
                logger.info("session de posHandle: " + session);
                if (session != null && session.getAttribute("user") != null) {
                    //logger.info("session de posHandle: ENTRE");
                    userSession = (UsuarioDTO) request.getSession().getAttribute("user");
                    usuario = userSession.getIdUsuario();
                    //logger.info("session de posHandle: ENTRE VALOR: " +usuario);

                }

            } catch (Exception e) {
                logger.info("Ocurrio algo: postHandle()");
            }

        }

        String cleanedUser = usuario.replace('\n', '_').replace('\r', '_');
        String cleanedIpAddress = ipAddress.replace('\n', '_').replace('\r', '_');

        if (!usuario.equals(cleanedUser) || !ipAddress.equals(cleanedIpAddress) || !path.equals(clean)) {
            alertMsg = " (PRECAUCIÓN AL LEER EL LOG)";
        }

        logger.info("ipClient: " + cleanedIpAddress + " - User: " + cleanedUser + " - checklist - path: " + clean
                + " - Tiempo de ejecucion: " + executeTime + " ms" + " - " + alertMsg);

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
    }

    @SuppressWarnings("unused")
    public boolean getTokenAndRedirect(HttpServletRequest request, HttpServletResponse response) throws IOException, KeyException, GeneralSecurityException {
        //logger.info("1.- **********************  INICIA getTokenAndRedirect  **********************");
        String params = request.getQueryString() != null ? "?" + request.getQueryString() : "";
        UtilCryptoGS utilCrypto = new UtilCryptoGS();
        TokenDTO token = new TokenDTO();
        token.setBandera(false);
        token.setIpOrigen(FRQConstantes.getIpOrigen());
        token.setIdAplicacion(FRQConstantes.getIdApp());
        //token.setUrlRedirect(FRQConstantes.getURLServer() + request.getRequestURI() + params);
        token.setUrlRedirect(FRQConstantes.getURLServer() + request.getRequestURI());
        token.setFechaHora(UtilDate.getSysDate("ddMMyyyy hh:mm:ss"));
        token.setCadenaOriginal(utilCrypto.generaToken(token));

        try {
            //logger.info("1.- ENTRO AL TRY DE GENERATOKEN");
            /**
             * Se sustituye la versión generada por axis*
             */
            /**
             * * Mandamos a llamar el web-service para generar el token **
             */
            //logger.info("Cadena original... " + token.getCadenaOriginal());
            String ret = UtilFRQ.conectMasterKey(token.getCadenaOriginal());
            //logger.info("IMPRIME EL RET ------ " + ret);
            if (!ret.equals("error")) {
                token.setToken(ret);
                //logger.info("1.- ENTRO EN EL IF DE ER EN EL TRY");
            }

        } catch (Exception ex) {
            logger.info("Ocurrio algo: getTokenAndRedirect");
        }
        /**
         * * Termina llamado al web-service para generar el token **
         */
        //logger.info("1.- TERMINO DE INVOCACION SERVICIO PARA GENERAR TOKEN");

        String path = request.getRequestURI();
        String clean = request.getRequestURI().replace('\n', '_').replace('\r', '_');
        UsuarioDTO userSession = null;
        String usuario = "notLogged";
        String alertMsg = "";

        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        //logger.info("1.- path:	" + path);
        //logger.info("1.- clean:	" + clean);
        //logger.info("1.- ipAddress:	" + ipAddress);
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
            //logger.info("1.- ipAddress == null:	" + request.getRemoteAddr());
        }

        if (request != null) {
            //logger.info("1.- request != null");
            HttpSession session = request.getSession();
            //logger.info("1.- session : " + session);
            //logger.info("1.- session.getAttribute('user') : " + session.getAttribute("user"));
            if (session != null && session.getAttribute("user") != null) {
                //logger.info("session != null && session.getAttribute(user) != null");
                userSession = (UsuarioDTO) request.getSession().getAttribute("user");
                usuario = userSession.getIdUsuario();
                logger.info("ASIGNACION DE USUARIO: " + usuario);
            }
        }
        String cleanedUser = usuario.replace('\n', '_').replace('\r', '_');
        String cleanedIpAddress = ipAddress.replace('\n', '_').replace('\r', '_');

        if (!usuario.equals(cleanedUser) || !ipAddress.equals(cleanedIpAddress) || !path.equals(clean)) {
            alertMsg = " (PRECAUCIÓN AL LEER EL LOG)";
        }

        logger.info("1.- IP CLIENT: " + cleanedIpAddress + " - USER: " + cleanedUser + " - checklist - PATH: " + clean + " - " + alertMsg);
        logger.info("1.- DATOS DE REDIRECCION. response.sendRedirect: " + FRQConstantes.getUrlLoginLlave() + token.getToken() + "&id=" + FRQConstantes.getIdApp());

        response.sendRedirect(FRQConstantes.getUrlLoginLlave() + token.getToken() + "&id=" + FRQConstantes.getIdApp()); // Redireccionamos

        return false;

    }

    @SuppressWarnings("unchecked")
    public void setInfoInicial(String idUser, String nomUser, HttpServletRequest request) {
        //logger.info("**********************  INICIA setInfoInicial  **********************");
        boolean isAdmin = true;
        /* Informacion del usuario */
 /*if (idUser.equals("99999999")) {
			// idUser = "232531";
			//idUser = "176361";
		}*/
        UsuarioDTO userSession = new UsuarioDTO();
        //userSession.setIdUsuario(""+189870);
        userSession.setIdUsuario(idUser);
        userSession.setAdmin(isAdmin);
        userSession.setNombre(nomUser);

        RecursoPerfilDTO perfilAdmin = null;
        Class<?> c5;
        try {
            Object object5 = FRQAppContextProvider.getApplicationContext().getBean("recursoPerfilBI");
            c5 = object5.getClass();
            Method method5 = c5.getDeclaredMethod("obtienePermisos", new Class[]{int.class, int.class});
            perfilAdmin = (RecursoPerfilDTO) method5.invoke(object5, 1, Integer.parseInt(idUser));
        } catch (Exception e) {
            logger.info("Ocurrio algo: setInfoInicial()" + e.getStackTrace());
        }

        RecursoPerfilDTO perfilReportes = null;
        /*LLAMADO DE PERFILADO REPORTERIA*/
        try {
            Object object5 = FRQAppContextProvider.getApplicationContext().getBean("recursoPerfilBI");
            c5 = object5.getClass();
            Method method5 = c5.getDeclaredMethod("obtienePermisos", new Class[]{int.class, int.class});
            perfilReportes = (RecursoPerfilDTO) method5.invoke(object5, 4, Integer.parseInt(idUser));
        } catch (Exception e) {
            logger.info("Ocurrio algo: setInfoInicial()");
        }

        List<PerfilUsuarioDTO> perfilUsuario = null;
        Class<?> c6;
        try {
            Object object6 = FRQAppContextProvider.getApplicationContext().getBean("perfilusuarioBI");
            c6 = object6.getClass();
            Method method5 = c6.getDeclaredMethod("obtienePerfiles", new Class[]{String.class, String.class});
            perfilUsuario = (List<PerfilUsuarioDTO>) method5.invoke(object6, idUser, null);
        } catch (Exception e) {
            logger.info("Ocurrio algo : setInfoInicial()");
        }

        int perSop = 0, perSis = 0, perIma = 0;
        int perSup = 0;
        for (int i = 0; i < perfilUsuario.size(); i++) {
            if (perfilUsuario.get(i).getIdPerfil() == 105) {
                perSop = 1;
            } else if (perfilUsuario.get(i).getIdPerfil() == 106) {
                perSis = 1;
            } else if (perfilUsuario.get(i).getIdPerfil() == 107) {
                perIma = 1;
            } else if (perfilUsuario.get(i).getIdPerfil() == 231) {
                perSup = 1;
            } else if (perfilUsuario.get(i).getIdPerfil() == 227) {
                perSup = 2;
            } else if (perfilUsuario.get(i).getIdPerfil() == 235) {
                perSup = 3;
            } else if (perfilUsuario.get(i).getIdPerfil() == 285) {
                perSup = 4;
            } else if (perfilUsuario.get(i).getIdPerfil() == 346) {
                perSup = 5;
            }
            logger.info("perSup: " + perSup);
        }

        request.getSession().setAttribute("perfilSoporte", perSop);
        request.getSession().setAttribute("perfilSistemas", perSis);
        request.getSession().setAttribute("perfilImagen", perIma);
        request.getSession().setAttribute("perfilSupervision", perSup);
        logger.info("Perfil Supervision: " + perSup);

        if (perfilAdmin != null) {
            request.getSession().setAttribute("perfilAdmin", 1);
        } else {
            request.getSession().setAttribute("perfilAdmin", 0);
        }

        if (perfilReportes != null) {
            request.getSession().setAttribute("perfilReportes", 1);
        } else {
            request.getSession().setAttribute("perfilReportes", 0);
        }

        request.getSession().setAttribute("user", userSession);
        request.getSession().setAttribute("nombre", nomUser);
        request.getSession().setAttribute("nomsucursal", "Central");

        /*
		 * En las siguientes líneas se debe poner información necesaria que
		 * siempre se debe cargar cuando el usuario se loguea
         */
        String servidorApache = FRQConstantes.getURLApacheServer();
        request.getSession().setAttribute("servidorApache", servidorApache);
    }
}
