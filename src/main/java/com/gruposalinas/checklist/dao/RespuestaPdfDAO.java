package com.gruposalinas.checklist.dao;


import java.util.Map;


public interface RespuestaPdfDAO  {

	public Map<String, Object> obtieneRespuestas(int usuario, int checklist, String ceco) throws Exception;
	
	//Metodos propios
	public int enviaCompromisos(int idUsuario, int idBitacora) throws Exception;

	public Map<String, String> obtieneCecoPorBitacora(int idUsuario , int idBitacora);
	
	
}
