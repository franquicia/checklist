package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;

public class ChecklistPregComEvidRowMapper implements RowMapper<ChecklistPreguntasComDTO> {

	public ChecklistPreguntasComDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistPreguntasComDTO com = new ChecklistPreguntasComDTO();

        com.setIdBita(rs.getInt("FIID_BITACORA"));
	    com.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    com.setIdArbol(rs.getInt("FIID_ARBOL_DES"));
	    com.setRuta(rs.getString("RUTA"));
	    com.setIdPlantilla(rs.getInt("FIID_PLANTILLA"));
	    com.setIdElem(rs.getInt("FIID_ELEM_PL"));
	    com.setIdOrden(rs.getInt("FIORDEN"));
	    com.setTipoEvi(rs.getInt("FIID_TIPO"));
	   
	   
		return com;
	}

}
