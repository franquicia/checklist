package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.HorarioDTO;
import com.gruposalinas.checklist.mappers.HorarioRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class HorarioDAOImpl extends DefaultDAO implements HorarioDAO {

    private static Logger logger = LogManager.getLogger(HorarioDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneHorario;
    DefaultJdbcCall jdbcInsertaHorario;
    DefaultJdbcCall jdbcActualizaHorario;
    DefaultJdbcCall jdbcEliminaHorario;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_HORARIOS")
                .withProcedureName("SP_SEL_G_HORARIO")
                .returningResultSet("RCL_HORARIO", new HorarioRowMapper());

        jdbcObtieneHorario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_HORARIOS")
                .withProcedureName("SP_SEL_HORARIO")
                .returningResultSet("RCL_HORARIO", new HorarioRowMapper());

        jdbcInsertaHorario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_HORARIOS")
                .withProcedureName("SP_INS_HORARIO");

        jdbcActualizaHorario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_HORARIOS")
                .withProcedureName("SP_ACT_HORARIO");

        jdbcEliminaHorario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_HORARIOS")
                .withProcedureName("SP_DEL_HORARIO");
    }

    @SuppressWarnings("unchecked")
    public List<HorarioDTO> obtieneHorario() throws Exception {
        Map<String, Object> out = null;
        List<HorarioDTO> listaHorario = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_HORARIOS.SP_SEL_G_HORARIO}");

        listaHorario = (List<HorarioDTO>) out.get("RCL_HORARIO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Horarios");
        } else {
            return listaHorario;
        }

        return listaHorario;
    }

    @SuppressWarnings("unchecked")
    public List<HorarioDTO> obtenerHorario(int idHorario) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HorarioDTO> listaHorario = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_HORARIO", idHorario);

        out = jdbcObtieneHorario.execute(in);
        logger.info("Funcion ejecutada: {checklist.PA_ADM_HORARIOS.SP_SEL_HORARIO}");

        listaHorario = (List<HorarioDTO>) out.get("RCL_HORARIO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el horario con id(" + idHorario + ")");
        } else {
            return listaHorario;
        }

        return null;

    }

    public int insertaHorario(HorarioDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idHorario = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CVEHORARIO", bean.getCveHorario())
                .addValue("PA_VALOR_INI", bean.getValorIni())
                .addValue("PA_VALOR_FIN", bean.getValorFin());

        out = jdbcInsertaHorario.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_HORARIOS.SP_INS_HORARIO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDHORARIO");
        idHorario = idTipoReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Horario");
        } else {
            return idHorario;
        }

        return idHorario;

    }

    public boolean actualizaHorario(HorarioDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDHORARIO", bean.getIdHorario())
                .addValue("PA_CVEHORARIO", bean.getCveHorario())
                .addValue("PA_VALOR_INI", bean.getValorIni())
                .addValue("PA_VALOR_FIN", bean.getValorFin());

        out = jdbcActualizaHorario.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_HORARIOS.SP_ACT_HORARIO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el Horario  id( " + bean.getIdHorario() + ")");
        } else {
            return true;
        }

        return false;

    }

    public boolean eliminaHorario(int idHorario) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDHORARIO", idHorario);

        out = jdbcEliminaHorario.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_HORARIOS.SP_DEL_HORARIO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el Horario id(" + idHorario + ")");
        } else {
            return true;
        }

        return false;
    }
}
