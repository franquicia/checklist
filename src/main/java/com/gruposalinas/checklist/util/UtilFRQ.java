package com.gruposalinas.checklist.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.OutputStream;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;

public class UtilFRQ {
	
	private static final Logger logger = LogManager.getLogger(UtilFRQ.class);

	public static boolean conectKeyMaster(String nEmpleado, String llaveMa, HttpServletRequest request) throws IOException {
		HttpURLConnection rc = null;
		BufferedReader read = null;
		OutputStreamWriter outStr = null;
		try {

			URL url = new URL(FRQConstantes.MASTERKEY_URL);
			rc = (HttpURLConnection) url.openConnection();

			rc.setRequestMethod("POST");
			rc.setDoOutput(true);
			rc.setDoInput(true);
			rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

			String nEmp = nEmpleado.trim();
			nEmpleado = "" + nEmp;

			nEmpleado = StrCipher.encrypt(nEmpleado, FRQConstantes.MASTEREY_ENCRYPT);
			llaveMa = StrCipher.encrypt(llaveMa, FRQConstantes.MASTEREY_ENCRYPT);

			String idPortalLM = "7";

			String reqStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><gsc_llave xmlns=\"http://gruposalinas.com.mx/\">" + "<Param1>" + nEmpleado + "</Param1>"
					+ "<Param2>" + llaveMa + "</Param2>" + "<Param3>" + idPortalLM + "</Param3>" + "</gsc_llave></soap:Body></soap:Envelope>";

			// several more definitions to request
			
			int len = reqStr.length();
			// rc.setRequestProperty( "SOAPAction:",
			// "http://10.64.197.16/wsAppLoginParam/service.asmx");
			rc.setRequestProperty("Content-Length", Integer.toString(len));
			rc.setRequestProperty("Connection:", "Keep-Alive"); //con puntos
			rc.connect();
			
			OutputStream outputStream = rc.getOutputStream();
			 
			try{
				outStr = new OutputStreamWriter(outputStream);
				outStr.write(reqStr, 0, len);
				outStr.flush();
				// //System.out.println(reqStr);
			}finally{
				outputStream.close();
			}
			
			read = null;
			
			InputStream inputStream = rc.getInputStream();
			try {
				read = new BufferedReader(new InputStreamReader(inputStream));
			} catch (Exception exception) {
				// if something wrong instead of the output, read the error
				read = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
			}

			// read server response
			//StringBuilder sb = new StringBuilder();
			Boolean validaLlave = false;
			String line;
			while ((line = read.readLine()) != null) {
				// //System.out.println(line);
				if (line.contains("Respuesta=\"[OK]\"")) {
					validaLlave = true;
					break;
				} else if (line.contains("Respuesta=\"[Error]")) {
					validaLlave = false;
					break;
				} else {
					validaLlave = false;
				}
			}

			inputStream.close();
			return validaLlave;
			
		} catch (Exception e) {

			logger.info("Ocurrio algo...");
			return false;
		} finally {
			if (rc != null) {
				rc.disconnect();
			}
			if(read!=null){
				read.close();
			}
			if(outStr!=null){
				outStr.close();
			}
			rc = null;
			read = null;
			outStr = null;
		}
	}
	
	public static String conectMasterKey(String cadenaOriginal) throws IOException{
		HttpURLConnection rc = null;
		String reqStr = "";
		OutputStreamWriter outStr = null;
		BufferedReader read = null;
		String receivedToken = "error";
		try {
			URL url = new URL(FRQConstantes.getUrlWsToken());
			rc = (HttpURLConnection) url.openConnection();
			
			rc.setRequestMethod("POST");
			rc.setDoOutput(true);
			rc.setDoInput(true);
			rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
			
			reqStr = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tem=\"http://tempuri.org/\"><soap:Header/><soap:Body><tem:recibepeticion><tem:wcadenaoriginal>";
			reqStr += cadenaOriginal;
			reqStr += "</tem:wcadenaoriginal></tem:recibepeticion></soap:Body></soap:Envelope>";
			
			int len = reqStr.length();
			
			rc.setRequestProperty("Content-Length", Integer.toString(len));
			rc.setRequestProperty("Connection", "Keep-Alive");
			rc.connect();
			
			OutputStream outputStream = rc.getOutputStream();
			try {
				outStr = new OutputStreamWriter(outputStream);
				outStr.write(reqStr, 0, len);
				outStr.flush();
			} finally {
				outputStream.close();
			}
			
			read = null;
			InputStream inputStream = rc.getInputStream();
			try {
				read = new BufferedReader(new InputStreamReader(inputStream));
			} catch (Exception exception) {
				// if something wrong instead of the output, read the error
				read = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
			}
			
			
			Boolean validaLlave = false;
			String line;
		
			while ((line = read.readLine()) != null) {
				// //System.out.println(line);
				if (line.contains("OK")) {
					validaLlave = true;
					break;
				} else if (line.contains("Respuesta=\"[Error]")) {
					validaLlave = false;
					break;
				} else {
					validaLlave = false;
				}
			}
			
			inputStream.close();

			if(validaLlave){
				String[] partes = line.split("<string>");
				int pos = partes[2].indexOf("</string>");
				receivedToken = partes[2].substring(0, pos);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info("Ocurrio algo...");
		}
		finally{
			if(read!=null){
				read.close();
			}
		}
		
		return receivedToken;
	}
	
	public static String validaURLRedirect(String url){
		String valido = null;
		String[] validURL = {
					"loginRest.json",
					"checklist.json",
					"checklistUsuario.json"};
		
		for(int i = 0; i< validURL.length; i++){
			if (url.equals(validURL[i])){
				valido = validURL[i];
				break;
			}
		}
		
		return valido;
	}
	
	public static long getStartTime(){
		return System.currentTimeMillis();
	}
	
	public static void printTotalTime(long startTime, String log){
		long endTime = System.currentTimeMillis();
		logger.info("Funcion ejecutada:"+log+" - Tiempo de ejecucion: "+(endTime - startTime)+" ms");
	}
	
	public static void printErrorLog(HttpServletRequest request, Exception e){
		UsuarioDTO userSession = null;
		String usuario = "notLogged";
		String ipAddress = "USR proceso";
		String alertMsg = "";
		if(request != null){
			ipAddress= request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			userSession = (UsuarioDTO) request.getSession().getAttribute("user");
			usuario = userSession.getIdUsuario();
		}
		
		String cleanedUser	= usuario.replace( '\n', '_' ).replace( '\r', '_' );
		String cleanedIpAddress = ipAddress.replace( '\n', '_' ).replace( '\r', '_' );
		
		if (!usuario.equals(cleanedUser) || !ipAddress.equals(cleanedIpAddress)) {
			alertMsg = " (PRECAUCIÓN AL LEER EL LOG)";
		}
		
		logger.info("ipClient: " + cleanedIpAddress + " - User: " + cleanedUser + " - checklist - " + e.getMessage() + alertMsg);
	}
}
