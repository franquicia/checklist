package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.NumeroTiendasDTO;

public class NumeroTiendasRowMapper implements RowMapper<NumeroTiendasDTO> {

	@Override
	public NumeroTiendasDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		NumeroTiendasDTO numeroTiendasDTO = new NumeroTiendasDTO();
		
		numeroTiendasDTO.setCeco(rs.getString("FCID_CECO"));
		numeroTiendasDTO.setNombreCeco(rs.getString("FCNOMBRE"));
		numeroTiendasDTO.setNumTiendas(rs.getInt("TIENDAS"));
		
		return numeroTiendasDTO;
	}

	
	
	

}
