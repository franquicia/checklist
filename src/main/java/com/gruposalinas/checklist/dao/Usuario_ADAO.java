package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.Usuario_ADTO;


public interface Usuario_ADAO {

	
	public List<Usuario_ADTO> obtieneUsuario() throws Exception;
	
	public List<Usuario_ADTO> obtieneUsuario(int idUsuario) throws Exception;
	
    public List<Usuario_ADTO> obtieneUsuarioPaso() throws Exception;
	
	public List<Usuario_ADTO> obtieneUsuarioPaso(String idUsuario, String idPuesto, String idCeco) throws Exception;
	
	public boolean insertaUsuario(Usuario_ADTO bean) throws Exception;
	
	public boolean actualizaUsuario(Usuario_ADTO bean) throws Exception;
	
	public boolean actualizaUsuarioCeco(Usuario_ADTO bean) throws Exception;
	
	public boolean eliminaUsuario(int idUsuario) throws Exception;
	
	public boolean cargaUsuarios () throws Exception;
	
	public boolean cargaPerfilUsuarios () throws Exception;
	
	public boolean bajaUsuarios () throws Exception;
}
