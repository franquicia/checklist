package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.HallazgosEviExpDTO;
import com.gruposalinas.checklist.mappers.HallazgosEviExpBitaRowMapper;
import com.gruposalinas.checklist.mappers.HallazgosEviExpRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class HallazgosEviExpDAOImpl extends DefaultDAO implements HallazgosEviExpDAO {

    private static Logger logger = LogManager.getLogger(HallazgosEviExpDAOImpl.class);

    DefaultJdbcCall jdbcInsertaHallazgoEvi;
    DefaultJdbcCall jdbcEliminaHallazgoEvi;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneHallazgo;
    DefaultJdbcCall jdbcObtieneHallazgoBita;
    DefaultJdbcCall jbdcActualizaHallazgoEvi;

    public void init() {

        jdbcInsertaHallazgoEvi = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEVIHALLAZ")
                .withProcedureName("SP_INS_EVIHALLAZ");

        jdbcEliminaHallazgoEvi = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEVIHALLAZ")
                .withProcedureName("SP_DEL_EVIHALLAZ");

        //TRAE_TODO
        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEVIHALLAZ")
                .withProcedureName("SP_DETA_HALLAZ")
                .returningResultSet("RCL_EVIHALLAZ", new HallazgosEviExpRowMapper());

        //RESPUESTA
        jdbcObtieneHallazgo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEVIHALLAZ")
                .withProcedureName("SP_DETA_RESP")
                .returningResultSet("RCL_EVIHALLAZ", new HallazgosEviExpRowMapper());

        //BITACORA
        jdbcObtieneHallazgoBita = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEVIHALLAZ")
                .withProcedureName("SP_SEL_EVIXBITA")
                .returningResultSet("RCL_EVIHALLAZ", new HallazgosEviExpBitaRowMapper());

        jbdcActualizaHallazgoEvi = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEVIHALLAZ")
                .withProcedureName("SP_ACT_EVID");

    }

    public int inserta(HallazgosEviExpDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDHALLAZ", bean.getIdHallazgo())
                .addValue("PA_IDRESP", bean.getIdResp())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_NUMEVID", bean.getTotEvi());

        out = jdbcInsertaHallazgoEvi.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAEVIHALLAZ.SP_INS_EVIHALLAZ}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDEVI");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el EviHallazgo");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean elimina(String idResp) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDRESP", idResp);

        out = jdbcEliminaHallazgoEvi.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAEVIHALLAZ.SP_DEL_EVIHALLAZ}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + idResp + ")");
        } else {
            return true;
        }

        return false;

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<HallazgosEviExpDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<HallazgosEviExpDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneTodo.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAEVIHALLAZ.SP_DETA_HALLAZ}");

        listaFijo = (List<HallazgosEviExpDTO>) out.get("RCL_EVIHALLAZ");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los Empleados Fijos");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //parametro

    @SuppressWarnings("unchecked")
    public List<HallazgosEviExpDTO> obtieneDatos(int idResp) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosEviExpDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_RESPUESTA", idResp);

        out = jdbcObtieneHallazgo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAEVIHALLAZ.SP_DETA_RESP}");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosEviExpDTO>) out.get("RCL_EVIHALLAZ");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los datos de la bitagral(" + idResp + ")");
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<HallazgosEviExpDTO> obtieneDatosBita(int bitGral) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosEviExpDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bitGral);

        out = jdbcObtieneHallazgoBita.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAEVIHALLAZ.SP_DETA_RESP}");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosEviExpDTO>) out.get("RCL_EVIHALLAZ");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos de la bitagral(" + bitGral + ")");
        }
        if (error == -1) {
            return null;
        }
        return lista;

    }

    public boolean actualiza(HallazgosEviExpDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDEVI", bean.getIdEvi())
                .addValue("PA_IDHALLAZ", bean.getIdHallazgo())
                .addValue("PA_IDRESP", bean.getIdResp())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_NUMEVID", bean.getTotEvi());

        out = jbdcActualizaHallazgoEvi.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAEVIHALLAZ.SP_ACT_EVID}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar id( " + bean.getIdHallazgo() + ")");
        } else {
            return true;
        }
        return false;

    }

}
