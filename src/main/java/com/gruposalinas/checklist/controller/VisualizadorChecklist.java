package com.gruposalinas.checklist.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.business.ChecklistBI;
import com.gruposalinas.checklist.business.PaisBI;
import com.gruposalinas.checklist.business.PuestoBI;
import com.gruposalinas.checklist.business.ReporteBI;
import com.gruposalinas.checklist.business.TipoChecklistBI;
import com.gruposalinas.checklist.business.Usuario_ABI;
import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ReporteDTO;
import com.gruposalinas.checklist.domain.Usuario_ADTO;
import com.gruposalinas.checklist.resources.FRQConstantes;

@Controller
public class VisualizadorChecklist {

	private static Logger logger = LogManager.getLogger(VisualizadorChecklist.class);
	
	@Autowired
	TipoChecklistBI tipoChecklistBI;
	
	@Autowired
	PuestoBI puestoBI;
	
	@Autowired
	CecoBI cecoBI;
	
	@Autowired
	ChecklistBI checklistBI;
	
	@Autowired
	Usuario_ABI usuario_ABI;
	
	@Autowired
	ReporteBI reporteBI;
	
	@Autowired
	PaisBI paisBI;
	
	
	private Map<String, Object> listaURL= null;
	private int aux;
	private boolean bandera=false;
	private String categoria=null,tipo=null, puesto=null, usuario=null,costos=null,pais=null,
			territorio=null, zona=null, region=null, sucursal=null, auxCeco=null,fecha=null;
	
	
	@RequestMapping(value = "central/visualizador.htm", method=RequestMethod.GET)
	public ModelAndView vista(HttpServletRequest request){
		ModelAndView mv = null;
		if(request.getQueryString()==null){
			mv = new ModelAndView("visualizadorChecklist", "command", new TipoChecklistBI());
			mv.addObject("listaTipoCheck", tipoChecklistBI.obtieneTipoChecklist());
			mv.addObject("listaPuesto", puestoBI.obtienePuestoGeo());
			mv.addObject("listaPaises", paisBI.obtienePaisVisualizador());
			if(this.bandera==true){
				mv.addObject("fallo", "NO EXISTEN IMAGENES CON LOS DATOS INGRESADOS");
				this.bandera=false;
			}				
		}else{
			this.bandera=true;
			mv=new ModelAndView("redirect:/central/visualizador.htm");	
		}
		
		return mv;
	}
	@RequestMapping(value = "central/vistaComparador.htm", method=RequestMethod.GET)
	public ModelAndView vista2(){
		
		/*//System.out.println("tamaño map ima.. "+this.listaURL.size());
		//System.out.println("map ima.. "+this.listaURL);
		//System.out.println("lista ima.. "+this.listaURL.values());
		*/
		ModelAndView mv = new ModelAndView("comparador");
		//
		mv.addObject("rutaHostImagen", FRQConstantes.getRutaImagen());
		mv.addObject("mapFotos", this.listaURL);
		mv.addObject("tamObj", this.listaURL.size());
				
		mv.addObject("categoria", this.categoria);
		mv.addObject("tipo", this.tipo);
		mv.addObject("puesto", this.puesto);
		mv.addObject("usuario", this.usuario);
		mv.addObject("costos", this.costos);
		mv.addObject("auxCeco", this.auxCeco);
		mv.addObject("aux", this.aux);
		mv.addObject("fecha", this.fecha);
		
		return mv;
	}
	//AUTOCOMPLETAR
	@RequestMapping(value = "/getUsuarios", method = RequestMethod.GET)
	public @ResponseBody List<Usuario_ADTO> getUsuarios(@RequestParam String tagId, HttpServletRequest request) {
		if (tagId.length() > 3) {
			return usuario_ABI.obtieneUsuario(Integer.parseInt(tagId));
		} else {
			return null;
		}
	}

	
	// AJAX CONTROLLER METHODS
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/ajaxMapa", method = RequestMethod.GET)
	public @ResponseBody List<ReporteDTO> getMapa(
			@RequestParam(value = "categoria", required = true, defaultValue = "null") String categoria,
			@RequestParam(value = "tipo", required = true, defaultValue = "null") String tipo,
			@RequestParam(value = "puesto", required = true, defaultValue = "null") String puesto,
			@RequestParam(value = "usuario", required = true, defaultValue = "null") String usuario,
			@RequestParam(value = "costos", required = true, defaultValue = "null") String costos,
			@RequestParam(value = "auxCeco", required = true, defaultValue = "null") String auxCeco,
			@RequestParam(value = "aux", required = true, defaultValue = "4") int aux,
			@RequestParam(value = "fecha", required = true, defaultValue = "null") String fecha,
			@RequestParam(value = "pais", required = true, defaultValue = "null") String pais,
			@RequestParam(value = "keyMapa", required = true, defaultValue = "null") String keyMapa , HttpServletRequest request) throws NumberFormatException, Exception {
	
		
		if(categoria.equals("null"))
			categoria=null;
		if(tipo.equals("null"))
			tipo=null;
		if(puesto.equals("null"))
			puesto=null;
		if(usuario.equals("null"))
			usuario=null;
		if(costos.equals("null"))
			costos=null;
		if(auxCeco.equals("null"))
			auxCeco=null;	
		if(fecha.equals("null"))
			fecha=null;	
		if(pais.equals("null"))
			pais=null;	
	
		List<ReporteDTO> listaRep=null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		map=reporteBI.buscaReporteURL(categoria, tipo, puesto,usuario, costos, auxCeco, aux,fecha, pais);
		if(map!=null){
			Iterator<Map.Entry<String, Object>> enMap = map.entrySet().iterator();
			while (enMap.hasNext()) {
				Map.Entry<String, Object> enMaps = enMap.next();
				String nomH = enMaps.getKey();
				if (nomH.equals(keyMapa)) {
					listaRep = (List<ReporteDTO>) enMaps.getValue();
				}
			}
		}else{
			listaRep=null;
		}
		return listaRep;
	}
	
	@RequestMapping(value = "/ajaxCompara", method = RequestMethod.GET)
	public @ResponseBody List<ReporteDTO> getCompara(
			@RequestParam(value = "idRuta", required = true, defaultValue = "0") int idRuta,
			@RequestParam(value = "idPregunta", required = true, defaultValue = "0") int idPregunta,
			@RequestParam(value = "idCheckUsuario", required = true, defaultValue = "0") int idCheckUsuario,
			@RequestParam(value = "fechaImagen", required = true, defaultValue = "null") String fechaImagen, HttpServletRequest request) throws NumberFormatException, Exception {
		return reporteBI.ComparaImagen(idRuta, idPregunta, idCheckUsuario, fechaImagen);
	}
	@RequestMapping(value = "/ajaxCategoria", method = RequestMethod.GET)
	public @ResponseBody List<ChecklistDTO> getCategoria(
			@RequestParam(value = "varCateg", required = true, defaultValue = "varCateg") String varCateg, HttpServletRequest request) {
		return checklistBI.buscaTChecklist(Integer.parseInt(varCateg));
	}
	@RequestMapping(value = "/ajaxPais", method = RequestMethod.GET)
	public @ResponseBody List<CecoDTO> getPais(
			@RequestParam(value = "varPais", required = true, defaultValue = "varPais") String varPais, HttpServletRequest request) {
		return cecoBI.buscaTerritorios(Integer.parseInt(varPais));
	}
	@RequestMapping(value = "/ajaxTerritorio", method = RequestMethod.GET)
	public @ResponseBody List<CecoDTO> getTerritorio(
			@RequestParam(value = "varTerri", required = true, defaultValue = "varTerri") String varTerri, HttpServletRequest request) {
		return cecoBI.buscaCecosPasoP(Integer.parseInt(varTerri));
	}
	@RequestMapping(value = "/ajaxZona", method = RequestMethod.GET)
	public @ResponseBody List<CecoDTO> getZona(
			@RequestParam(value = "varZona", required = true, defaultValue = "varZona") String varZona, HttpServletRequest request) {	
		return cecoBI.buscaCecosPasoP(Integer.parseInt(varZona));
	}
	@RequestMapping(value = "/ajaxRegion", method = RequestMethod.GET)
	public @ResponseBody List<CecoDTO> getRegion(
			@RequestParam(value = "varRegio", required = true, defaultValue = "varRegio") String varRegio, HttpServletRequest request) {	
		return cecoBI.buscaCecosPasoP(Integer.parseInt(varRegio));
	}
	@RequestMapping(value = "/ajaxUsuario", method = RequestMethod.GET)
	public @ResponseBody List<Usuario_ADTO> getUsuario(
			@RequestParam(value = "varUsuario", required = true, defaultValue = "varUsuario") String varUsuario, HttpServletRequest request) {
		return usuario_ABI.obtieneUsuario(Integer.parseInt(varUsuario));
	}
	
	
	@RequestMapping(value = "central/visualizadorController.htm", method = RequestMethod.POST)
	public String showForm(@ModelAttribute(value = "visualizadorChecklist") ModelMap model,	HttpServletRequest request) throws Exception {
		
		String retorno="";
		if (request.getParameter("envia").equals("update")) {
		
			//this.categoria=request.getParameter("categoria");
			//this.tipo=request.getParameter("tipo");
			//this.puesto=request.getParameter("puesto");
			//this.fecha=request.getParameter("fecha");
			
		
			if(request.getParameter("categoria").equals("Selecciona una Opcion"))
				this.categoria=null;
			else
				this.categoria=request.getParameter("categoria");
			
			if(request.getParameter("tipo").equals("Selecciona una Opcion"))
				this.tipo=null;
			else
				this.tipo=request.getParameter("tipo");	
			
			if(request.getParameter("puesto").equals("Selecciona una Opcion"))
				this.puesto=null;
			else
				this.puesto=request.getParameter("puesto");
			
			if(request.getParameter("usuario").equals(""))
				this.usuario=null;
			else
				this.usuario=request.getParameter("usuario");
				
			if(request.getParameter("costos").equals(""))
				this.costos=null;
			else
				this.costos=request.getParameter("costos");
			
			if(request.getParameter("fecha").equals(""))
				this.fecha=null;
			else
				this.fecha=request.getParameter("fecha");
	
			
			if(!request.getParameter("pais").equals("Selecciona una Opcion")){
				this.pais=request.getParameter("pais");
				//this.auxCeco=this.pais;
			}
			
			this.aux=4;		
			if(!request.getParameter("territorio").equals("Selecciona una Opcion")){
				this.aux=3;
				this.territorio=request.getParameter("territorio");
				this.auxCeco=this.territorio;
				////System.out.println("imprime valor territorio:  "+request.getParameter("territorio"));
				if(!request.getParameter("zona").equals("Selecciona una Opcion")){
					this.aux=2;
					this.zona=request.getParameter("zona");
					this.auxCeco=this.zona;
					////System.out.println("imprime valor zona:  "+request.getParameter("zona"));
					if(!request.getParameter("region").equals("Selecciona una Opcion")){
						this.aux=1;
						this.region=request.getParameter("region");
						this.auxCeco=this.region;
						////System.out.println("imprime valor region:  "+request.getParameter("region"));
						if(!request.getParameter("sucursal").equals("Selecciona una Opcion")){
							this.aux=1;
							this.sucursal=request.getParameter("sucursal");
							this.auxCeco=this.sucursal;
							////System.out.println("imprime valor sucursal:  "+request.getParameter("sucursal"));
						}	
					}	
				}
					
			}else{
				this.auxCeco=null;
			}
			
			if(this.costos!=null && this.aux>1)
				this.aux=1;
				
			////System.out.println("imprime valor categoria:  "+this.categoria);
			////System.out.println("imprime valor tipo:  "+this.tipo);
			////System.out.println("imprime valor puesto:  "+ this.puesto);
			////System.out.println("imprime valor usuario:  "+this.usuario);
			////System.out.println("imprime valor costos:  "+this.costos);
			////System.out.println("imprime valor auxCeco:  "+this.auxCeco);
			////System.out.println("imprime valor aux:  "+this.aux);
			////System.out.println("imprime valor fecha:  "+this.fecha);
			////System.out.println("imprime valor pais:  "+this.pais);
				
			try {
				this.listaURL=reporteBI.buscaReporteURL(this.categoria, this.tipo, 
						this.puesto, this.usuario, this.costos, this.auxCeco, this.aux,this.fecha, this.pais);
			} catch (Exception e) {
				logger.info(e);
				e.getMessage();
			}
			
			if(this.listaURL!=null)
				retorno= "redirect:/central/vistaComparador.htm";
			else
				retorno= "redirect:/central/visualizador.htm?fallo=1";			
		}
		return retorno;
	}
}
