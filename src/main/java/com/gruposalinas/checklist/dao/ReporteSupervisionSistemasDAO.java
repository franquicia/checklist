package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.ReporteSupervisionSistemasDTO;

public interface ReporteSupervisionSistemasDAO {
	public Map<String, Object> reporteSistemas(int idCeco) throws Exception;

	public List<ReporteSupervisionSistemasDTO> reporteSisCap(String inicio, String fin, String checklist,
			String sucursal, String area) throws Exception;

	public List<ReporteSupervisionSistemasDTO> filtroCheck(int idReporte) throws Exception;

	public List<ReporteSupervisionSistemasDTO> filtroSucursal(String sucursal) throws Exception;

	public List<ReporteSupervisionSistemasDTO> filtroUsuario(String usuario) throws Exception;
	
	public Map<String, Object> obtenerRespuestas(int idBitacora) throws Exception;

	public List<ReporteSupervisionSistemasDTO> obtenerEvidencia(int idBitacora) throws Exception;
	
	public Map<String, Object> reporteSistemas2(int idCeco) throws Exception;
}
