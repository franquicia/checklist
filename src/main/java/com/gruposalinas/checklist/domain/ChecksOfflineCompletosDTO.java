package com.gruposalinas.checklist.domain;

public class ChecksOfflineCompletosDTO {
	
	private int idCheckUsua;
	private int idChecklist;
	private int idTipoPreg;
	private String cveTipoPregunta;
	private String descripcionTipo;
	private int idPregunta;
	private int ordepregunta;
	private int preguntaPadre;
	private int idArbolDes;
	private String pregunta;
	private String respuesta;
	private int idModulo;
	private String nombreModulo;
	private int idModuloPadre;
	private String moduloPadre;
	private int estatusEvidencia;
	private int siguienteOrden;
	private int reqAccion;
	private int reqObs;
	private int obliga;
	private String etiqueta;
        private int idProtocolo;
	
	public int getIdCheckUsua() {
		return idCheckUsua;
	}
	public void setIdCheckUsua(int idCheckUsua) {
		this.idCheckUsua = idCheckUsua;
	}
	public int getIdTipoPreg() {
		return idTipoPreg;
	}
	public void setIdTipoPreg(int idTipoPreg) {
		this.idTipoPreg = idTipoPreg;
	}
	public String getCveTipoPregunta() {
		return cveTipoPregunta;
	}
	public void setCveTipoPregunta(String cveTipoPregunta) {
		this.cveTipoPregunta = cveTipoPregunta;
	}
	public String getDescripcionTipo() {
		return descripcionTipo;
	}
	public void setDescripcionTipo(String descripcionTipo) {
		this.descripcionTipo = descripcionTipo;
	}
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public int getOrdepregunta() {
		return ordepregunta;
	}
	public void setOrdepregunta(int ordepregunta) {
		this.ordepregunta = ordepregunta;
	}
	public int getIdArbolDes() {
		return idArbolDes;
	}
	public void setIdArbolDes(int idArbolDes) {
		this.idArbolDes = idArbolDes;
	}
	public String getPregunta() {
		return pregunta;
	}
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public int getIdModulo() {
		return idModulo;
	}
	public void setIdModulo(int idModulo) {
		this.idModulo = idModulo;
	}
	public String getNombreModulo() {
		return nombreModulo;
	}
	public void setNombreModulo(String nombreModulo) {
		this.nombreModulo = nombreModulo;
	}
	public int getIdModuloPadre() {
		return idModuloPadre;
	}
	public void setIdModuloPadre(int idModuloPadre) {
		this.idModuloPadre = idModuloPadre;
	}
	public String getModuloPadre() {
		return moduloPadre;
	}
	public void setModuloPadre(String moduloPadre) {
		this.moduloPadre = moduloPadre;
	}
	public int getEstatusEvidencia() {
		return estatusEvidencia;
	}
	public void setEstatusEvidencia(int estatusEvidencia) {
		this.estatusEvidencia = estatusEvidencia;
	}
	public int getSiguienteOrden() {
		return siguienteOrden;
	}
	public void setSiguienteOrden(int siguienteOrden) {
		this.siguienteOrden = siguienteOrden;
	}
	public int getReqAccion() {
		return reqAccion;
	}
	public void setReqAccion(int reqAccion) {
		this.reqAccion = reqAccion;
	}
	public int getReqObs() {
		return reqObs;
	}
	public void setReqObs(int reqObs) {
		this.reqObs = reqObs;
	}
	public int getObliga() {
		return obliga;
	}
	public void setObliga(int obliga) {
		this.obliga = obliga;
	}
	public String getEtiqueta() {
		return etiqueta;
	}
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public int getPreguntaPadre() {
		return preguntaPadre;
	}
	public void setPreguntaPadre(int preguntaPadre) {
		this.preguntaPadre = preguntaPadre;
	}

    public int getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(int idProtocolo) {
        this.idProtocolo = idProtocolo;
    }

        
}
