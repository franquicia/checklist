package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.ActorEdoHallaDTO;


public interface ActorEdoHallaDAO {

	  public int insertaActorHalla(ActorEdoHallaDTO bean)throws Exception;
		
		public boolean eliminaActorHalla(int idFirma)throws Exception;
		
		public List<ActorEdoHallaDTO> obtieneDatosActorHalla(String idFirma) throws Exception; 
		
		
		public boolean actualizaActorHalla(ActorEdoHallaDTO bean)throws Exception;
		 
		public int insertaConfEdoHalla(ActorEdoHallaDTO bean)throws Exception;
		
		public boolean eliminaConfEdoHalla(int idFirma)throws Exception;
		
		public List<ActorEdoHallaDTO> obtieneDatosConfEdoHalla(String idFirma) throws Exception; 
			
		public boolean actualizaConfEdoHalla(ActorEdoHallaDTO bean)throws Exception;
		
	}