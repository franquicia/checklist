$body = $("body");

$(document).on({
	ajaxStart:function(){$(".modal").css("display","true") },
	ajaxStop:function(){$(".modal").css("display","none")}
});
function init(archive){
	/* set up XMLHttpRequest */  
	var url = "/checklist/files/2007.xlsx"
	var oReq = new XMLHttpRequest();
	var mexico;

	oReq.open("GET", url, true);
	oReq.responseType = "arraybuffer";

	oReq.onload = function(e) {
	  var arraybuffer = oReq.response;

		  /* convert data to binary string */
		  var data = new Uint8Array(arraybuffer);
		  var arr = new Array();
		  var bandera = true;
		  
		  for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
		  var bstr = arr.join("");
	
		  /* Call XLSX */
		  var workbook = XLSX.read(bstr, {type:"binary"});
		  
		  //var json_data = workbook.Sheets.Hoja1;
		  var json_data = workbook;
		  //alert (JSON.stringify(json_data));
		  
		  var sheets = json_data.Sheets;
		  var objectsLength = Object.keys(sheets).length;
	
		 var mexico = sheets.MX;
		  //alert (JSON.stringify(json_data));
		  if (mexico == undefined)
			  bandera = false;
		  
		  var salvador = sheets.ElSalvador;
		  if (salvador == undefined)
			  bandera = false;
		  
		  var guatemala = sheets.Guatemala;
		  if (guatemala == undefined)
			  bandera = false;
		  
		  var honduras = sheets.Honduras;
		  if (honduras == undefined)
			  bandera = false;
		  
		  var panama = sheets.Panamá;
		  if (panama == undefined)
			  bandera = false;
		  
		  var peru = sheets.Perú;
		  if (peru == undefined)
			  bandera = false;
				
	  if (bandera){
		  		 desactivaChecklist()
				  getInfo(mexico);
				  getInfo(salvador); 
				  getInfo(guatemala);
				  getInfo(honduras);
				  getInfo(panama);
				  getInfo(peru);
	  }
	  else{
		  alert ("Verifica el formato del archivo");
		  window.location.href = "/checklist/central/cargaExcelForm.htm";
	  }
	}
	oReq.send();
}

function desactivaChecklist(){
	$.ajax({
		type : "GET",
		url : "http://10.51.210.239:8080/checklist/checklistServices/desactivaCheckusua2.json?idChecklist=40",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
		},
		error : function() {
		},
		async: false
	})
}

function getInfo(pais){
	var result = [];
	var i = 0; 
	var j = 1;
	var cc, nombreCc,usuario,nombreUsuario = "";
	
	for(var i in pais)
		  result.push([pais[i]]);
	 
	for (var i = 5; i < result.length-1; i++){
		if (j == 4){
			ejecutaServicio(result[i-3][0].v,result[i-2][0].v,result[i-1][0].v,result[i][0].v);
			j=1;			
			i++;
		}
		j++;
	}
}

//Debe ser asincrono
function ejecutaServicio(cc, nombreCc,usuario,nombreUsuario){
	$.ajax({
		type : "GET",
		url : "http://10.51.210.239:8080/checklist/checklistServices/altaUsuCheckEsp2.json?idChecklist=40&idCeco="+cc+"&idUsu="+usuario,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			//console.log(cadena);	  
			}, 
		error : function() {
			//console.log(cadena);	
		},
		async: true
	});
}	