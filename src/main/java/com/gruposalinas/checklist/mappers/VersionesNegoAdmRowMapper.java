package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.VersionExpanDTO;

public class VersionesNegoAdmRowMapper implements RowMapper<VersionExpanDTO> {

	public VersionExpanDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		VersionExpanDTO VersionExpanDTO = new VersionExpanDTO();
		
		VersionExpanDTO.setIdTab(rs.getInt("FIID_NEGVER"));
		VersionExpanDTO.setIdVers(rs.getInt("FIVERSION"));
		VersionExpanDTO.setIdactivo(rs.getInt("FIIDACTIVO"));
	    VersionExpanDTO.setNego(rs.getString("FCNEGO"));
	    VersionExpanDTO.setObs(rs.getString("FCOBSERVACION"));
		VersionExpanDTO.setFechaLibera(rs.getString("FDLIBERACION"));
	    VersionExpanDTO.setPeriodo(rs.getString("FCPERIODO"));

		return VersionExpanDTO;
	}

}


