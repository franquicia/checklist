package com.gruposalinas.checklist.domain;

public class ReporteImagenesDTO {
	
	private int idPregunta;
	private int idModulo;
	private int idPreguntaPadre;
	private String textoAbiertaImagen;
	private String imagenPrincipal;
	private String imagenSecundario;
	private String reporte;
	private String imagenReporte;
	private String observaciones;
	
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public int getIdModulo() {
		return idModulo;
	}
	public void setIdModulo(int idModulo) {
		this.idModulo = idModulo;
	}
	public int getIdPreguntaPadre() {
		return idPreguntaPadre;
	}
	public void setIdPreguntaPadre(int idPreguntaPadre) {
		this.idPreguntaPadre = idPreguntaPadre;
	}
	public String getTextoAbiertaImagen() {
		return textoAbiertaImagen;
	}
	public void setTextoAbiertaImagen(String textoAbiertaImagen) {
		this.textoAbiertaImagen = textoAbiertaImagen;
	}
	public String getImagenPrincipal() {
		return imagenPrincipal;
	}
	public void setImagenPrincipal(String imagenPrincipal) {
		this.imagenPrincipal = imagenPrincipal;
	}
	public String getImagenSecundario() {
		return imagenSecundario;
	}
	public void setImagenSecundario(String imagenSecundario) {
		this.imagenSecundario = imagenSecundario;
	}
	public String getReporte() {
		return reporte;
	}
	public void setReporte(String reporte) {
		this.reporte = reporte;
	}
	public String getImagenReporte() {
		return imagenReporte;
	}
	public void setImagenReporte(String imagenReporte) {
		this.imagenReporte = imagenReporte;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	

}
