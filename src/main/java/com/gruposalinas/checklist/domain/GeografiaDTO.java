package com.gruposalinas.checklist.domain;

public class GeografiaDTO {
	private String idCeco;
	private String idRegion;
	private String region;
	private String idZona;
	private String zona;
	private String idTerritorio;
	private String territorio;
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getIdRegion() {
		return idRegion;
	}
	public void setIdRegion(String idRegion) {
		this.idRegion = idRegion;
	}
	public String getIdZona() {
		return idZona;
	}
	public void setIdZona(String idZona) {
		this.idZona = idZona;
	}
	public String getIdTerritorio() {
		return idTerritorio;
	}
	public void setIdTerritorio(String idTerritorio) {
		this.idTerritorio = idTerritorio;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	public String getTerritorio() {
		return territorio;
	}
	public void setTerritorio(String territorio) {
		this.territorio = territorio;
	}
	

	
	
}
