<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="../css/bootstrap/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap-table/bootstrap-table.min.css">

	<script	src="../js/jquery/jquery-2.2.4.min.js"></script>
	<script	src="../js/bootstrap.min.js"></script>
	<script	src="../js/bootstrap-table.min.js"></script>

	
	<title>Soporte - Modifica Estatus Checklist</title>
		
	<script type="text/javascript">
		function estatus(){
			var radios = document.getElementsByTagName('input');
			var value;
			var flag=false;
			for (var i = 0; i < radios.length; i++) {
			    if (radios[i].type === 'radio' && radios[i].checked) {
			        // get value, set checked flag or do whatever you need to
			        value = radios[i].value;
			        flag=true;
			    }
			}
			if(!flag){
				alert("Debes seleccionar un id de Checklist!!!");
				return false;
			}else{
				document.getElementById("idChecklist").value=value;
				return true;
			}
		}
	</script>
	<script src="../js/bootstrap-table-es-MX.min.js"></script>
	<% response.addHeader("X-Frame-Options", "SAMEORIGIN"); %>
</head>
<body>

	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuSoporte.jsp" flush="true">
		<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuSoporte.jsp" />
	</tiles:insertTemplate>

	<div id="container">
		<div id="container">
			<br>
				<p class="text-center lead"><strong>Modifica Estaus Checklist</strong></p>	
			<br>
		</div>
		<c:choose>
			<c:when test="${paso=='tabla'}">
				<div class="container">
				<c:url value = "/soporte/moficaChecklistEstatus.htm" var = "cambiaEstatus" />
				<form:form method="POST" action="${cambiaEstatus}" model="command" name="form1" id="form1">
					<table	class="table table-striped table-bordered table-hover table-condensed" 
							data-toggle="table" 
							data-sort-name="idCheckUsuario" 
							data-sort-order="asc"
							data-pagination="true"
							data-pagination-loop="true"
							data-page-size="25"
							data-height="540"
							data-smart-display="true"
							data-pagination-v-align="top"
							data-silent-sort="false"
							data-search="true"
							data-show-refresh="true"
							data-show-toggle="true"
							data-show-columns="true"
							data-show-pagination-switch="true">
						<thead>
							<tr>
								<th data-formatter="counter">#</th>
								<th data-field="idChecklist" data-sortable="true" data-searchable="true">ID Checklist</th>
								<th data-field="nombreCheck" data-sortable="true" >Nombre Checklist</th>
								<th data-field="vigente" data-sortable="true" data-searchable="true">Estatus</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="list" items="${list}">
								<tr>
									<td></td>
									<td ><INPUT TYPE="radio" id="command" name="command" value="${list.idChecklist}"/>   <c:out value="${list.idChecklist}"></c:out></td>
									<td><c:out value="${list.nombreCheck}"></c:out></td>
									<td>
										<c:if test="${list.vigente == '1'}">
										    	Activo
										</c:if>
										<c:if test="${list.vigente != '1'}">
										    	Inactivo
										</c:if>  
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<br>
					<input id="idChecklist" name="idChecklist" type="hidden" value=""/>
					<input type="submit" id="Recuperar" name="Recuperar" style="margin-left: auto;margin-right: auto;height: 40px; width: 250px; text-align: center; font-size: 25px;" class="btn btn-default btn-lg" value="Cambiar estatus" onclick="return estatus(); return false;" />
				</form:form>
				</div>
			</c:when>
			<c:when test="${paso=='OK'}">
				<c:choose>
				<c:when test="${res==0}">
					<div class="container">
						<div class="alert alert-danger" role="alert">
							<p class="text-center"> 
								<h4>No fue posible cambiar el estatus del Checklist!</h4>
								<br>Presione el botón de regresar para intentarlo nuevamente.
							<p>
							<br>
							<c:url value = "/soporte/moficaChecklistEstatus.htm" var = "cambiaEstatus" />
							<form:form method="GET" action="${cambiaEstatus}" model="command" name="form-regresar2">
								<input type="submit" class="btn btn-danger btn-lg" value="Regresar" />
							</form:form>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="container">
						<div class="alert alert-success" role="alert">
							<p class="text-center"> 
								<h4>El estatus del Checklist fue cambiado Exitosamente!</h4>
								<br>Presione el botón de regresar para cambiar el estatus de otro Checklist.
							<p>
							<br>
							<div class="container">
								<c:url value = "/soporte/moficaChecklistEstatus.htm" var = "cambiaEstatus" />
								<form:form method="GET" action="${cambiaEstatus}" model="command" name="form-regresar2">
									<input type="submit" class="btn btn-danger btn-lg" value="Regresar" />
								</form:form>
							</div>
						</div>
					</div>
				</c:otherwise>
				</c:choose>
			</c:when>
		</c:choose>

	</div>
	
	<br>
	<br>

	<script>
		function counter(value, row, index) {
			return index;
		}
	</script>
</body>
</html>