package com.gruposalinas.checklist.business;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
//import org.jboss.xnio.log.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.checklist.domain.AdmPregZonaDTO;
import com.gruposalinas.checklist.domain.AdmTipoZonaDTO;
import com.gruposalinas.checklist.domain.AdminZonaDTO;
import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.domain.ChecklistLayoutDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.PosiblesDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.ProtocoloDTO;
import com.gruposalinas.checklist.domain.AdmPregZonaDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;

public class ChecklistAdminValidaBI {

    @Autowired
    PosiblesAdmBI posiblesbi;

    @Autowired

    ArbolRespAdiEviAdmBI arbolDecisionAdmbi;

    @Autowired
    ArbolDecisionBI arbolDecisionbi;

    @Autowired
    PreguntaAdmBI preguntaAdmValidaBI;
    @Autowired
    ChecklistProtocoloBI checklistProtocoloBI;
    @Autowired
    ReporteImagenesBI reporteImagenesBI;

    @Autowired
    ChecklistPreguntaBI checklistPreguntabi;
    @Autowired
    ChecklistProtocoloAdmBI checklistProtocoloAdmBI;
    @Autowired
    ReporteImagenesAdmBI reporteImagenesAdmBI;
    @Autowired
    ArbolRespAdiEviAdmBI arbolRespAdiEviAdmBI;

    @Autowired
    ProtocoloBI protocoloBI;
    @Autowired
    AdmTipoZonaBI admTipoZonaBI;
    @Autowired
    AdmZonasBI admZonasBI;
    @Autowired
    AdmPregZonasBI admPregZonasBI;

    @Autowired
    ArbolDecisionTBI arbolDecisionAdmbiT;

    private static final Logger logger = LogManager.getLogger(ChecklistAdminBI.class);

    /* MÓDULOS */
    public ArrayList<ModuloDTO> getListaModulos(ArrayList<ChecklistLayoutDTO> listaData) {

        ArrayList<ModuloDTO> listaModulos = new ArrayList<ModuloDTO>();

        try {

            String seccionActual = "";
            boolean existe = false;
            for (ChecklistLayoutDTO objLista : listaData) {

                if (!objLista.getSeccionProtocolo().equalsIgnoreCase(seccionActual)) {

                    // VALIDAR SECCIONES SALTADAS
                    existe = false;
                    seccionActual = objLista.getSeccionProtocolo();

                    ModuloDTO modulo = new ModuloDTO();
                    modulo.setIdModulo(0);
                    modulo.setNombre(objLista.getSeccionProtocolo());
                    modulo.setIdModuloPadre(0);
                    modulo.setNombrePadre("");
                    modulo.setNumVersion("0");
                    modulo.setTipoCambio("");
                    modulo.setIdChecklist(0);

                    for (ModuloDTO obj : listaModulos) {

                        if (objLista.getSeccionProtocolo().compareTo(obj.getNombre()) == 0) {

                            existe = true;
                            break;
                        }

                    }

                    if (!existe) {
                        listaModulos.add(modulo);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listaModulos;

    }

    public ArrayList<ArrayList<ModuloDTO>> insertaModulos(ArrayList<ModuloDTO> listaModulos) {

        ArrayList<ArrayList<ModuloDTO>> response = new ArrayList<ArrayList<ModuloDTO>>();
        ArrayList<ModuloDTO> listaModulosCorrectos = new ArrayList<ModuloDTO>();

        try {
            ArrayList<ArrayList<ModuloDTO>> modulosResponse = setIdModulos(listaModulos);

            listaModulosCorrectos = modulosResponse.get(1);

            if (listaModulosCorrectos.size() == listaModulos.size()) {

                response.add(0, new ArrayList<ModuloDTO>());
                response.add(1, listaModulosCorrectos);

                return response;

            } else {

                modulosResponse = setIdModulos(modulosResponse.get(0));

                for (ModuloDTO obj : modulosResponse.get(1)) {
                    listaModulosCorrectos.add(obj);
                }

                if (listaModulosCorrectos.size() == listaModulos.size()) {

                    response.add(0, new ArrayList<ModuloDTO>());
                    response.add(1, listaModulosCorrectos);

                    return response;

                } else {

                    response.add(0, modulosResponse.get(0));
                    response.add(1, listaModulosCorrectos);

                    return response;
                }

            }
        } catch (Exception e) {
            System.out.println("ChecklistAdminBI " + e);
            return null;
        }

    }

    public ArrayList<ArrayList<ModuloDTO>> setIdModulos(ArrayList<ModuloDTO> listaModulos) {

        ArrayList<ArrayList<ModuloDTO>> response = new ArrayList<ArrayList<ModuloDTO>>();

        try {
            PreguntaAdmBI modulobi = (PreguntaAdmBI) FRQAppContextProvider.getApplicationContext().getBean("preguntaAdmBI");

            int idMod = 0;

            ArrayList<ModuloDTO> listaModulosCorrectos = new ArrayList<ModuloDTO>();
            ArrayList<ModuloDTO> listaModulosIncorrectos = new ArrayList<ModuloDTO>();

            for (int i = 0; i < listaModulos.size(); i++) {
                //System.out.println("i == " + i);
                //System.out.println("size listaModulos == " + listaModulos.size());

                idMod = 0;

                idMod = modulobi.insertaModulo(listaModulos.get(i));

                if (idMod != 0) {

                    listaModulos.get(i).setIdModulo(idMod);
                    listaModulosCorrectos.add(listaModulos.get(i));
                } else {

                    listaModulosIncorrectos.add(listaModulos.get(i));
                }
            }

            response.add(0, listaModulosIncorrectos);
            response.add(1, listaModulosCorrectos);

        } catch (Exception e) {
            System.out.println("AP al insertar modulo");
            System.out.println(e.getMessage());
            System.out.println("AP :" + e);
        }

        return response;

    }

    /* FIN MÓDULOS */

 /* PREGUNTAS */
    public ArrayList<PreguntaDTO> getListaPreguntas(ArrayList<ChecklistLayoutDTO> listaData, ArrayList<ModuloDTO> listaModulos, int tipoCarga) {

        ArrayList<PreguntaDTO> listaPreguntas = new ArrayList<PreguntaDTO>();

        try {

            for (ChecklistLayoutDTO objLista : listaData) {

                PreguntaDTO pregunta = new PreguntaDTO();

                // Se agrega a PreguntaDTO
                pregunta.setIdConsecutivo(objLista.getIdConsecutivo());
                pregunta.setIdPreguntaPadre(objLista.getIdPreguntaPadre());

                // COMÚNES
                pregunta.setPregunta(objLista.getPregunta());
                pregunta.setCritica(objLista.getFactorCritico());
                pregunta.setIdPregunta(objLista.getIdConsecutivoProtocolo());
                // Tipo de Respuesta
                pregunta.setIdTipo(objLista.getTipoRespuesta());

                // Para Aseguramiento
                pregunta.setCodigo(objLista.getCodigoChecklist());
                pregunta.setDetalle(objLista.getDefinicion());

                // Para Infraestructura
                pregunta.setSla(objLista.getSla());
                // Área Responsables
                if (tipoCarga == 1) {
                    pregunta.setArea(objLista.getResponsable2());
                } else {
                    pregunta.setArea(objLista.getZona());
                }
                listaPreguntas.add(pregunta);

                // Se Agrega el Módulo a la Pregunta
                for (ModuloDTO moduloObj : listaModulos) {

                    if (moduloObj.getNombre().compareTo(objLista.getSeccionProtocolo()) == 0) {

                        pregunta.setIdModulo(moduloObj.getIdModulo());

                    }

                }

            }

        } catch (Exception e) {

            System.out.println("ChecklistAdminBI " + e);
            return listaPreguntas;

        }

        return listaPreguntas;

    }

    public ArrayList<ArrayList<PreguntaDTO>> insertaPreguntas(ArrayList<PreguntaDTO> listaPreguntas, int tipoCarga) {

        ArrayList<ArrayList<PreguntaDTO>> response = new ArrayList<ArrayList<PreguntaDTO>>();
        ArrayList<PreguntaDTO> listaPreguntasCorrectas = new ArrayList<PreguntaDTO>();

        try {
            ArrayList<ArrayList<PreguntaDTO>> preguntasResponse = setIdPreguntas(listaPreguntas, tipoCarga);

            listaPreguntasCorrectas = preguntasResponse.get(1);

            if (listaPreguntasCorrectas.size() == listaPreguntas.size()) {

                response.add(0, new ArrayList<PreguntaDTO>());
                response.add(1, listaPreguntasCorrectas);

                return response;

            } else {

                preguntasResponse = setIdPreguntas(preguntasResponse.get(0), tipoCarga);

                for (PreguntaDTO obj : preguntasResponse.get(1)) {
                    listaPreguntasCorrectas.add(obj);
                }

                if (listaPreguntasCorrectas.size() == listaPreguntas.size()) {

                    response.add(0, new ArrayList<PreguntaDTO>());
                    response.add(1, listaPreguntasCorrectas);

                    return response;

                } else {

                    response.set(0, preguntasResponse.get(0));
                    response.set(1, listaPreguntasCorrectas);

                    return response;
                }

            }
        } catch (Exception e) {
            System.out.println("AP ChecklistAdminBI ALTA PREGUNTA " + e.getMessage());
            return null;
        }

    }

    public ArrayList<ArrayList<PreguntaDTO>> setIdPreguntas(ArrayList<PreguntaDTO> listaPreguntas, int tipoCarga) {

        ArrayList<ArrayList<PreguntaDTO>> response = new ArrayList<ArrayList<PreguntaDTO>>();

        try {
            PreguntaAdmBI preguntabi = (PreguntaAdmBI) FRQAppContextProvider.getApplicationContext().getBean("preguntaAdmBI");

            int idPregunta = 0;

            ArrayList<PreguntaDTO> listaPreguntasCorrectos = new ArrayList<PreguntaDTO>();
            ArrayList<PreguntaDTO> listaPreguntasIncorrectos = new ArrayList<PreguntaDTO>();

            for (int i = 0; i < listaPreguntas.size(); i++) {
                //System.out.println("i == " + i);
                //System.out.println("size listaPreguntas == " + listaPreguntas.size());

                idPregunta = 0;

                idPregunta = preguntabi.insertaPreguntaCom(listaPreguntas.get(i));

                if (idPregunta != 0) {

                    listaPreguntas.get(i).setIdPregunta(idPregunta);

                    listaPreguntasCorrectos.add(listaPreguntas.get(i));

                    //listaPreguntasCorrectos.add(listaPreguntas.get(i));
                    //Se agrega el id pregunta e id zona en la tabla de Preg-Zona
                    /*AdmTipoZonaDTO tipoZona = new AdmTipoZonaDTO();
                    tipoZona.setIdTipoZona(tipoCarga + "");
                    ArrayList<AdmTipoZonaDTO> tipoZonaComp = admTipoZonaBI.obtieneTipoZonaById(tipoZona);
                    if (tipoZonaComp.get(0).getDescripcion().toUpperCase().contains("ASEGURAMIENTO")) {
                        //Se obtiene el catalogo de zonas para aseguramiento
                        AdminZonaDTO zona = new AdminZonaDTO();
                        zona.setIdTipo(tipoCarga + "");
                        ArrayList<AdminZonaDTO> zonaComp = admZonasBI.obtieneZonaById(zona);

                        //Se recorre el catalogo de zonas de aseguramiento
                        for (AdminZonaDTO zonaDTO : zonaComp) {
                            if (listaPreguntas.get(i).getArea().toUpperCase().contains(zonaDTO.getDescripcion().toUpperCase())) {
                                //logger.info("Preg: " + listaPreguntas.get(i).getArea());
                                AdmPregZonaDTO pregZona = new AdmPregZonaDTO();
                                pregZona.setIdPreg(idPregunta + "");
                                pregZona.setIdZona(zonaDTO.getIdZona());
                                boolean respuesta = admPregZonasBI.insertaPregZona(pregZona);
                                if (respuesta) {
                                    listaPreguntasCorrectos.add(listaPreguntas.get(i));
                                    break;
                                }
                            } else {
                                logger.info("La zona no se encuentra en el catalogo de zonas de Aseguramiento");
                            }
                        }
                    } else if (tipoZonaComp.get(0).getDescripcion().toUpperCase().contains("INFRAESTRUCTURA")) {
                        //Se obtiene el catalogo de zonas para infraestructura
                        AdminZonaDTO zona = new AdminZonaDTO();
                        zona.setIdTipo(tipoCarga + "");
                        ArrayList<AdminZonaDTO> zonaComp = admZonasBI.obtieneZonaById(zona);

                        //Se recorre el catalogo de zonas de infraestructura
                        for (AdminZonaDTO zonaDTO : zonaComp) {
                            if (listaPreguntas.get(i).getArea().toUpperCase()
                                    .contains(zonaDTO.getDescripcion().toUpperCase())) {
                                //logger.info("Preg: " + listaPreguntas.get(i).getArea());
                                AdmPregZonaDTO pregZona = new AdmPregZonaDTO();
                                pregZona.setIdPreg(idPregunta + "");
                                pregZona.setIdZona(zonaDTO.getIdZona());
                                boolean respuesta = admPregZonasBI.insertaPregZona(pregZona);
                                if (respuesta) {
                                    listaPreguntasCorrectos.add(listaPreguntas.get(i));
                                    break;
                                }
                            } else {
                                logger.info("La zona no se encuentra en el catalogo de zonas de Infraestructura");
                            }
                        }
                    } else if (tipoZonaComp.get(0).getDescripcion().toUpperCase().contains("TRANSFORMACIÓN")) {
                        //Se obtiene el catalogo de zonas para transformacion
                        AdminZonaDTO zona = new AdminZonaDTO();
                        zona.setIdTipo(tipoCarga + "");
                        ArrayList<AdminZonaDTO> zonaComp = admZonasBI.obtieneZonaById(zona);

                        //Se recorre el catalogo de zonas de transformaciones
                        for (AdminZonaDTO zonaDTO : zonaComp) {
                            if (listaPreguntas.get(i).getArea().toUpperCase()
                                    .contains(zonaDTO.getDescripcion().toUpperCase())) {
                                //logger.info("Preg: " + listaPreguntas.get(i).getArea());
                                AdmPregZonaDTO pregZona = new AdmPregZonaDTO();
                                pregZona.setIdPreg(idPregunta + "");
                                pregZona.setIdZona(zonaDTO.getIdZona());
                                boolean respuesta = admPregZonasBI.insertaPregZona(pregZona);
                                if (respuesta) {
                                    listaPreguntasCorrectos.add(listaPreguntas.get(i));
                                    break;
                                }
                            } else {
                                logger.info("La zona no se encuentra en el catalogo de zonas de Transformación");
                            }
                        }
                    }*/
                } else {

                    listaPreguntasIncorrectos.add(listaPreguntas.get(i));
                }
            }

            response.add(0, listaPreguntasIncorrectos);
            response.add(1, listaPreguntasCorrectos);

        } catch (Exception e) {
            System.out.println("AP al insertar PREGUNTA: " + e.getMessage());
            System.out.println(e.getMessage());
            System.out.println("AP :" + e);
        }

        return response;
    }

    /* FIN PREGUNTAS */
 /* METODOS PARA ALTA PROTOCOLO */
    public ArrayList<ChecklistProtocoloDTO> getListaProtocolos(ArrayList<ChecklistLayoutDTO> listaData, int tipoNegocio) {

        // Infraestructura == 1 - Supervisión 2
        ArrayList<ChecklistProtocoloDTO> listaProtocolos = new ArrayList<ChecklistProtocoloDTO>();

        ArrayList<ChecklistLayoutDTO> listaProtocolosData = new ArrayList<ChecklistLayoutDTO>();

        if (tipoNegocio == 1) {
            // Los módulos para EXPANSIÓN(1) son protocolos
            listaProtocolosData = getListaProtocolosExpansion(listaData);
            listaProtocolos = setObjetoProtocolo(listaData, listaProtocolosData, 1);

        } else {
            // Para SUPERVISIÓN (2) los protocolos son la columna protocolo
            listaProtocolosData = getListaProtocolosSupervision(listaData);
            listaProtocolos = setObjetoProtocolo(listaData, listaProtocolosData, 2);
        }

        return listaProtocolos;
    }

    public ArrayList<ChecklistProtocoloDTO> setObjetoProtocolo(ArrayList<ChecklistLayoutDTO> listaData, ArrayList<ChecklistLayoutDTO> listaProtocolosData, int tipoNegocio) {

        ArrayList<ChecklistProtocoloDTO> listaProtocolos = new ArrayList<ChecklistProtocoloDTO>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String fechaHoy = sdf.format(new Date());

        for (ChecklistLayoutDTO obj : listaProtocolosData) {

            //CONDICIÓN
            String nombreCheck = "";
            String ordenGrupo = "";
            String idUsuario = "";
            double ponderacionTot = 0;
            String clasifica = "";
            String idProtocolo = "0";

            //GENÉRICOS
            String idTipoChecklist = "61";
            String idHorario = "23";
            String vigente = "1";
            String fecha_inicio = fechaHoy;
            String fecha_fin = null;
            String idEstado = "21";
            String dia = "";
            String periodo = "1";
            String version = "1";
            int commit = 1;
            String negocio = "";

            if (tipoNegocio == 1) {

                // EXPANSIÓN
                //Obtenemos de listaData
                nombreCheck = obj.getSeccionProtocolo();
                //**(Infraestrcutura 321 - Aseguramiento 323)**
                ordenGrupo = "327";
                //Se debe obtener de la sesión
                idUsuario = "189871";
                //**LEER EXCEL PARA EXPANSIÓN CAMPO ZONA**
                clasifica = obj.getZona();
                //**LEER EXCEL PARA ASEGURAMIENTO CAMPO PROTOCOLO 0**
                idProtocolo = "0";
                //**CALCULAR CON EXCEL**
                ponderacionTot = getPonderacionProtocolo(listaData, obj, tipoNegocio);

                //GENÉRICOS
                idTipoChecklist = "61";
                idHorario = "21";
                vigente = "1";
                fecha_inicio = fechaHoy;
                fecha_fin = "";
                idEstado = "21";
                dia = "";
                periodo = "1";
                version = "1";
                commit = 1;
                negocio = obj.getNegocio();
                //Genéricos

            } else {
                // SUPERVISIÓN

                // EXPANSIÓN
                //Obtenemos de listaData
                nombreCheck = obj.getNombreProtocolo();
                //**(Infraestrcutura 321 - Aseguramiento 323)**
                ordenGrupo = "326";
                //Se debe obtener de la sesión
                idUsuario = "189871";
                //**LEER EXCEL PARA EXPANSIÓN CAMPO ZONA**
                clasifica = "";
                //**LEER EXCEL PARA ASEGURAMIENTO CAMPO PROTOCOLO 0**
                idProtocolo = "0";
                //**CALCULAR CON EXCEL**
                ponderacionTot = getPonderacionProtocolo(listaData, obj, tipoNegocio);

                //GENÉRICOS
                idTipoChecklist = "61";
                idHorario = "21";
                vigente = "1";
                fecha_inicio = fechaHoy;
                fecha_fin = "";
                idEstado = "21";
                dia = "";
                periodo = "1";
                version = "1";
                commit = 1;
                negocio = obj.getNegocio();

                List<ProtocoloDTO> respuesta = protocoloBI.getProtocolos();

                String numProtocolo = obj.getNombreProtocolo().trim().split(" ")[0];

                int idProt = 0;
                for (ProtocoloDTO aux : respuesta) {
                    if (aux.getObservaciones().trim().equals(numProtocolo)) {
                        idProt = aux.getIdProtocolo();
                    }
                }

                idProtocolo = "" + idProt;
            }

            ChecklistProtocoloDTO checklistProtocolo = new ChecklistProtocoloDTO();
            checklistProtocolo.setIdTipoChecklist(Integer.parseInt(idTipoChecklist));
            checklistProtocolo.setNombreCheck(nombreCheck);
            checklistProtocolo.setIdHorario(Integer.parseInt(idHorario));
            checklistProtocolo.setVigente(Integer.parseInt(vigente));
            checklistProtocolo.setFecha_inicio(fecha_inicio);
            checklistProtocolo.setFecha_fin(fecha_fin);
            checklistProtocolo.setIdEstado(Integer.parseInt(idEstado));
            checklistProtocolo.setIdUsuario(idUsuario);
            checklistProtocolo.setDia(dia);
            checklistProtocolo.setPeriodo(periodo);
            checklistProtocolo.setVersion(version);
            checklistProtocolo.setOrdenGrupo(ordenGrupo);
            checklistProtocolo.setCommit(commit);
            checklistProtocolo.setPonderacionTot(ponderacionTot);
            checklistProtocolo.setClasifica(clasifica);
            checklistProtocolo.setIdProtocolo(idProtocolo);
            checklistProtocolo.setNegocio(negocio);
            //set Negocio

            listaProtocolos.add(checklistProtocolo);

        }

        return listaProtocolos;

    }

    public double getPonderacionProtocolo(ArrayList<ChecklistLayoutDTO> listaData, ChecklistLayoutDTO protocolo, int tipoNegocio) {

        double ponderacion = 0.0;

        try {

            for (ChecklistLayoutDTO obj : listaData) {

                if (tipoNegocio == 1 && (obj.getSeccionProtocolo().compareTo(protocolo.getSeccionProtocolo()) == 0)) {

                    ponderacion += obj.getPonderacion();

                    System.out.println(obj.getNombreProtocolo());
                    System.out.println(protocolo.getNombreProtocolo());
                } else if (tipoNegocio == 2 && (obj.getNombreProtocolo().compareTo(protocolo.getNombreProtocolo()) == 0)) {

                    ponderacion += obj.getPonderacion();

                }
            }
        } catch (Exception e) {
            System.out.println("AP getPonderacionProtocolo " + e);
            ponderacion = -1;
        }

        return ponderacion;

    }

    public ArrayList<ChecklistLayoutDTO> getListaProtocolosExpansion(ArrayList<ChecklistLayoutDTO> lista) {

        ArrayList<ChecklistLayoutDTO> listaProtocolos = new ArrayList<ChecklistLayoutDTO>();

        String seccionActual = "";
        boolean existe = false;
        System.out.println(lista.size());

        for (ChecklistLayoutDTO objLista : lista) {

            if (!objLista.getSeccionProtocolo().equalsIgnoreCase(seccionActual)) {

                existe = false;
                seccionActual = objLista.getSeccionProtocolo();

                ChecklistLayoutDTO objCheck = objLista;

                for (ChecklistLayoutDTO obj : listaProtocolos) {

                    if (objLista.getSeccionProtocolo().compareTo(obj.getSeccionProtocolo()) == 0) {

                        existe = true;
                        break;
                    }

                }

                if (!existe) {
                    listaProtocolos.add(objCheck);
                }
            }
        }

        return listaProtocolos;

    }

    public ArrayList<ChecklistLayoutDTO> getListaProtocolosSupervision(ArrayList<ChecklistLayoutDTO> lista) {

        ArrayList<ChecklistLayoutDTO> listaProtocolos = new ArrayList<ChecklistLayoutDTO>();

        String seccionActual = "";
        boolean existe = false;
        System.out.println(lista.size());

        for (ChecklistLayoutDTO objLista : lista) {

            if (!objLista.getNombreProtocolo().equalsIgnoreCase(seccionActual)) {

                existe = false;
                seccionActual = objLista.getNombreProtocolo();

                ChecklistLayoutDTO objCheck = objLista;

                for (ChecklistLayoutDTO obj : listaProtocolos) {

                    if (objLista.getNombreProtocolo().compareTo(obj.getNombreProtocolo()) == 0) {

                        existe = true;
                        break;
                    }

                }

                if (!existe) {
                    listaProtocolos.add(objCheck);
                }
            }
        }

        return listaProtocolos;

    }

    public ArrayList<ArrayList<ChecklistProtocoloDTO>> insertaInfoProtocolos(ArrayList<ChecklistProtocoloDTO> listaInfoProtocolos, String Usuario) {

        ArrayList<ArrayList<ChecklistProtocoloDTO>> response = new ArrayList<ArrayList<ChecklistProtocoloDTO>>();
        ArrayList<ChecklistProtocoloDTO> listaProtocolosCorrectas = new ArrayList<ChecklistProtocoloDTO>();

        try {
            ArrayList<ArrayList<ChecklistProtocoloDTO>> protocolosResponse = setIdProtocolos(listaInfoProtocolos);

            listaProtocolosCorrectas = protocolosResponse.get(1);

            if (listaProtocolosCorrectas.size() == listaInfoProtocolos.size()) {

                response.add(0, new ArrayList<ChecklistProtocoloDTO>());
                response.add(1, listaProtocolosCorrectas);

                return response;

            } else {

                protocolosResponse = setIdProtocolos(protocolosResponse.get(0));

                for (ChecklistProtocoloDTO obj : protocolosResponse.get(1)) {
                    listaProtocolosCorrectas.add(obj);
                }

                if (listaProtocolosCorrectas.size() == listaInfoProtocolos.size()) {

                    response.add(0, new ArrayList<ChecklistProtocoloDTO>());
                    response.add(1, listaProtocolosCorrectas);

                    return response;

                } else {

                    response.set(0, protocolosResponse.get(0));
                    response.set(1, listaProtocolosCorrectas);

                    return response;
                }

            }
        } catch (Exception e) {
            System.out.println("AP ChecklistAdminBI ALTA PROTOCOLO " + e);
            return null;
        }

    }

    public ArrayList<ArrayList<ChecklistProtocoloDTO>> setIdProtocolos(ArrayList<ChecklistProtocoloDTO> listaChecklist) {

        ArrayList<ArrayList<ChecklistProtocoloDTO>> response = new ArrayList<ArrayList<ChecklistProtocoloDTO>>();

        try {

            PreguntaAdmBI checklistProtocoloBI = (PreguntaAdmBI) FRQAppContextProvider.getApplicationContext().getBean("preguntaAdmBI");
            //ChecklistProtocoloBI checklistProtocoloBI = (ChecklistProtocoloBI) FRQAppContextProvider.getApplicationContext().getBean("checklistProtocoloBI");

            int idProtocolo = 0;

            ArrayList<ChecklistProtocoloDTO> listaPreguntasCorrectos = new ArrayList<ChecklistProtocoloDTO>();
            ArrayList<ChecklistProtocoloDTO> listaPreguntasIncorrectos = new ArrayList<ChecklistProtocoloDTO>();

            for (int i = 0; i < listaChecklist.size(); i++) {
                //System.out.println("i == " + i);
                //System.out.println("size listaChecklist == " + listaChecklist.size());

                idProtocolo = 0;

                //se obtiene el nombre del protocolo para buscarlo en el catalogo de protocolos
                String nomProtocoloFile = (listaChecklist != null && listaChecklist.size() > 0 ? listaChecklist.get(i).getNombreCheck() : null);
                int idProtocoloChk = 0;
                List<ProtocoloDTO> protocolosCat = protocoloBI.getProtocolos();

                //System.out.println("PROTOCOLO QUE SE ASIGNARA AL CHK: " + protocolosCat);
                if (protocolosCat != null) {
                    for (ProtocoloDTO p : protocolosCat) {
                        if (p.getDescripcion().toUpperCase().contains(nomProtocoloFile.toUpperCase())) {
                            idProtocoloChk = p.getIdProtocolo();
                            //System.out.println("PROTOCOLO QUE SE ASIGNARA AL CHK: " + idProtocoloChk);
                            //break;

                        }
                    }
                }

                //System.out.println("ITEM:  " + listaChecklist.get(i));
                listaChecklist.get(i).setIdProtocolo(idProtocoloChk + "");

                idProtocolo = checklistProtocoloBI.insertaChecklist(listaChecklist.get(i));

                if (idProtocolo != 0) {

                    listaChecklist.get(i).setIdChecklist(idProtocolo);

                    listaPreguntasCorrectos.add(listaChecklist.get(i));

                } else {

                    listaPreguntasIncorrectos.add(listaChecklist.get(i));
                }
            }

            response.add(0, listaPreguntasIncorrectos);
            response.add(1, listaPreguntasCorrectos);

        } catch (Exception e) {
            System.out.println("AP al insertar PROTOCOLO");
            System.out.println(e.getMessage());
            System.out.println("AP :" + e);
        }

        return response;
    }

    /* FIN METODOS ALTA PROTOCOLO */

 /* SET INFO LAYOUT*/
    public ArrayList<ChecklistLayoutDTO> setInfoLayout(ArrayList<ChecklistLayoutDTO> listaLayout,
            ArrayList<PreguntaDTO> listaPreguntas, ArrayList<ChecklistProtocoloDTO> listaProtocolos, int tipoNegocio) {

        ArrayList<ChecklistLayoutDTO> response = new ArrayList<ChecklistLayoutDTO>();

        // Pregunta necesita idChecklist
        for (ChecklistProtocoloDTO objProtocolo : listaProtocolos) {

            //System.out.println("Checklist --- " + objProtocolo.getNombreCheck());
            // SE AGREGA INFO DE PROTOCOLO A OBJETO LAYOUT
            for (int i = 0; i < listaLayout.size(); i++) {

                if (tipoNegocio == 1) {

                    if (objProtocolo.getNombreCheck().compareTo(listaLayout.get(i).getSeccionProtocolo()) == 0) {

                        listaLayout.get(i).setChecklistProtocoloDTO(objProtocolo);

                    }

                } else {

                    if (objProtocolo.getNombreCheck().compareTo(listaLayout.get(i).getNombreProtocolo()) == 0) {

                        listaLayout.get(i).setChecklistProtocoloDTO(objProtocolo);

                        response.add(listaLayout.get(i));

                    }
                }
            }

        }

        // SE AGREGA INFO DE PREGUNTA A OBJETO LAYOUT
        for (PreguntaDTO objPregunta : listaPreguntas) {

            for (int i = 0; i < listaLayout.size(); i++) {

                if (objPregunta.getPregunta().compareTo(listaLayout.get(i).getPregunta()) == 0 && objPregunta.getIdConsecutivo() == listaLayout.get(i).getIdConsecutivo()) {

                    listaLayout.get(i).setPreguntaDTO(objPregunta);

                }
            }
        }

        return listaLayout;
    }

    /* FIN SET INFO LAYOUT */
 /* INSERTA CHECKUSUAS  */
    public ArrayList<ArrayList<ChecklistLayoutDTO>> insertaCheckusua(ArrayList<ChecklistLayoutDTO> listaCheckusu) {

        ArrayList<ArrayList<ChecklistLayoutDTO>> response = new ArrayList<ArrayList<ChecklistLayoutDTO>>();
        ArrayList<ChecklistLayoutDTO> listaModulosCorrectos = new ArrayList<ChecklistLayoutDTO>();
        ArrayList<ChecklistLayoutDTO> listaModulosIncorrectos = new ArrayList<ChecklistLayoutDTO>();

        try {
            ArrayList<ArrayList<ChecklistLayoutDTO>> checkusuaResponse = setCheckUsuaCheckusua(listaCheckusu);

            listaModulosCorrectos = checkusuaResponse.get(1);

            if (listaModulosCorrectos.size() == listaCheckusu.size()) {

                response.add(0, new ArrayList<ChecklistLayoutDTO>());
                response.add(1, listaModulosCorrectos);

                return response;

            } else {

                checkusuaResponse = setCheckUsuaCheckusua(checkusuaResponse.get(0));

                for (ChecklistLayoutDTO obj : checkusuaResponse.get(1)) {
                    listaModulosCorrectos.add(obj);
                }

                if (listaModulosCorrectos.size() == listaCheckusu.size()) {

                    response.add(0, new ArrayList<ChecklistLayoutDTO>());
                    response.add(1, listaModulosCorrectos);

                    return response;

                } else {

                    response.add(0, listaModulosIncorrectos);
                    response.add(1, listaModulosCorrectos);

                    return response;
                }

            }
        } catch (Exception e) {
            System.out.println("ChecklistAdminBI " + e);
            return null;
        }

    }

    public ArrayList<ArrayList<ChecklistLayoutDTO>> setCheckUsuaCheckusua(ArrayList<ChecklistLayoutDTO> infoLayout) {

        ArrayList<ArrayList<ChecklistLayoutDTO>> response = new ArrayList<ArrayList<ChecklistLayoutDTO>>();

        ArrayList<ChecklistLayoutDTO> listaCheckusuasCorrectos = new ArrayList<ChecklistLayoutDTO>();
        ArrayList<ChecklistLayoutDTO> listaCheckusuasIncorrectos = new ArrayList<ChecklistLayoutDTO>();

        //ChecklistPreguntaBI checklistPreguntabi = (ChecklistPreguntaBI) FRQAppContextProvider.getApplicationContext().getBean("checklistPreguntaBI");
        PreguntaAdmBI checklistPreguntabi = (PreguntaAdmBI) FRQAppContextProvider.getApplicationContext().getBean("preguntaAdmBI");

        try {
            boolean respuesta = false;

            for (int i = 0; i < infoLayout.size(); i++) {

                //System.out.println("i == " + i);
                //System.out.println("size listaChecusuas == " + infoLayout.size());
                respuesta = false;

                int idCheck = infoLayout.get(i).getChecklistProtocoloDTO().getIdChecklist();
                int ordenPreg = infoLayout.get(i).getIdConsecutivoProtocolo();
                int idPreg = infoLayout.get(i).getPreguntaDTO().getIdPregunta();
                int idPadre = buscaPadre(infoLayout, infoLayout.get(i).getChecklistProtocoloDTO().getIdChecklist(), infoLayout.get(i).getIdPreguntaPadre());
                int commit = 1;

                ChecklistPreguntaDTO pregDTO = new ChecklistPreguntaDTO();
                pregDTO.setIdChecklist(idCheck);
                pregDTO.setOrdenPregunta(ordenPreg);
                pregDTO.setIdPregunta(idPreg);
                pregDTO.setCommit(commit);
                pregDTO.setPregPadre(idPadre);

                respuesta = checklistPreguntabi.insertaChecklistPregunta(pregDTO);

                if (respuesta) {

                    listaCheckusuasCorrectos.add(infoLayout.get(i));

                } else {

                    listaCheckusuasIncorrectos.add(infoLayout.get(i));
                }
            }

            response.add(0, listaCheckusuasIncorrectos);
            response.add(1, listaCheckusuasCorrectos);

        } catch (Exception e) {
            logger.info("AP al obtener CHECKUSUAS" + e);
        }

        return response;

    }

    /* FIN INSERTA CHECKUSUA */
    //GENÉRICO
    public ArrayList<ChecklistLayoutDTO> llenaLista() {

        ArrayList<ChecklistLayoutDTO> lista = new ArrayList<ChecklistLayoutDTO>();

        for (int i = 0; i < 10; i++) {

            ChecklistLayoutDTO obj = new ChecklistLayoutDTO();

            obj.setIdConsecutivo(i);
            obj.setCodigoChecklist(i);
            obj.setNegocio("negocio" + i);
            obj.setNombreProtocolo("nombreProtocolo");

            obj.setSeccionProtocolo("seccionProtocolo");
            /*if (i%2==0)
				obj.setSeccionProtocolo("seccionProtocolo");
			else
				obj.setSeccionProtocolo("seccionProtocolo"+i);*/

            obj.setSubSeccionProtocolo("subSeccionProtocolo" + i);
            obj.setIdConsecutivoProtocolo(i);
            obj.setFactorCritico((int) (Math.random() * 4 + 1));
            obj.setPregunta("pregunta" + i);
            obj.setIdPreguntaPadre(0);
            obj.setTipoRespuesta(21);
            obj.setOpcionesCombo("opcionesCombo");
            obj.setCumplimiento("cumplimiento");
            obj.setPonderacion(i);
            obj.setRequiereComentarios("requiereComentarios");
            obj.setRequiereEvidencia("requiereEvidencia");
            obj.setTipoEvidencia("tipoEvidencia");
            obj.setPlantillaSi("plantillaSi");
            obj.setPlantillaNo("plantillaNo");
            obj.setResponsable1("responsable1" + i);
            obj.setResponsable2("responsable2" + i);
            obj.setZona("zona");
            obj.setDefinicion("definicion" + i);
            obj.setDisciplina("disciplina" + i);
            obj.setSla("" + i);

            lista.add(obj);

        }

        return lista;

    }

    public ArrayList<ChecklistLayoutDTO> leerExcell(File file) {
        ArrayList<ChecklistLayoutDTO> lista = new ArrayList<ChecklistLayoutDTO>();
        try {

            // leer archivo excel
            XSSFWorkbook worbook = new XSSFWorkbook(file);
            // obtener la hoja que se va leer
            XSSFSheet sheet = worbook.getSheetAt(0);
            int filas = 0;
            boolean pregError = false;
            for (Row row : sheet) {
                ChecklistLayoutDTO obj = new ChecklistLayoutDTO();
                filas++;
                int column = 0;
                for (Cell cell : row) {
                    column++;

                    String format = "";
                    DataFormatter formatter = new DataFormatter();
                    // calcula el resultado de la formula

                    // obtiene el valor y se le da formato, en este caso String
                    switch (cell.getCellTypeEnum()) {
                        case STRING:
                            // logger.info(cell.getRichStringCellValue().getString());
                            format = formatter.formatCellValue(cell);
                            //logger.info(format);
                            break;
                        case NUMERIC:
                            if (DateUtil.isCellDateFormatted(cell)) {
                                // logger.info(cell.getDateCellValue());
                                format = formatter.formatCellValue(cell);
                                //logger.info(format);
                            } else {
                                // logger.info(cell.getNumericCellValue());
                                format = formatter.formatCellValue(cell);
                                //logger.info(format);
                            }
                            break;
                        case BOOLEAN:
                            // logger.info(cell.getBooleanCellValue());
                            format = formatter.formatCellValue(cell);
                            //logger.info(format);
                            break;
                        case FORMULA:
                            // logger.info(cell.getCellFormula());
                            XSSFFormulaEvaluator evaluator = worbook.getCreationHelper().createFormulaEvaluator();
                            CellReference cellRef = new CellReference(cell.getColumnIndex(), cell.getRowIndex());
                            CellValue cellValue = evaluator.evaluate(cell);
                            logger.info("Cell Value: " + cellValue.formatAsString());

                            format = formatter.formatCellValue(cell);
                            format = cellValue.formatAsString();
                            //logger.info(format);
                            break;
                        case BLANK:
                            logger.info("");
                            format = "";
                            break;
                        default:
                            logger.info("*");
                            format = "";
                    } // Cierra switch

                    if (column == 1 && format.equals("")) {
                        break;
                    }

                    if (filas != 1) {
                        int dato = 0;
                        switch (column) {
                            case 1:

                                if (format.contains(".")) {
                                    dato = (int) Double.parseDouble(format);
                                } else {
                                    dato = Integer.parseInt(format);
                                }

                                obj.setIdConsecutivo(dato);
                                break;
                            case 2:
                                if (format.contains(".")) {
                                    dato = (int) Double.parseDouble(format);
                                } else {
                                    dato = Integer.parseInt(format);
                                }
                                obj.setCodigoChecklist(dato);
                                break;
                            case 3:
                                obj.setNegocio(format);
                                break;
                            case 4:
                                obj.setNombreProtocolo(format);
                                break;
                            case 5:
                                obj.setSeccionProtocolo(format);
                                break;
                            case 6:
                                obj.setSubSeccionProtocolo(format);
                                break;
                            case 7:
                                if (format.contains(".")) {
                                    dato = (int) Double.parseDouble(format);
                                } else {
                                    dato = Integer.parseInt(format);
                                }
                                obj.setIdConsecutivoProtocolo(dato);
                                break;
                            case 8:
                                int critico = 0;
                                if (format.toLowerCase().contains("imperdonable")) {
                                    critico = 1;
                                }
                                if (format.toLowerCase().contains("crítico")) {
                                    critico = 2;
                                }
                                if (format.toLowerCase().contains("alto")) {
                                    critico = 2;
                                }
                                if (format.toLowerCase().contains("medio")) {
                                    critico = 3;
                                }
                                if (format.toLowerCase().contains("bajo")) {
                                    critico = 4;
                                }
                                obj.setFactorCritico(critico);
                                break;
                            case 9:
                                if (format == null || format.equals("")) {
                                    pregError = true;
                                }
                                obj.setPregunta(format);
                                break;
                            case 10:
                                if (!format.equals("")) {
                                    if (format.contains(".")) {
                                        dato = (int) Double.parseDouble(format);
                                    } else {
                                        dato = Integer.parseInt(format);
                                    }
                                }
                                obj.setIdPreguntaPadre(dato);
                                break;
                            case 11:
                                int tipoPreg = 0;
                                if (format.toLowerCase().equals("si-no")) {
                                    tipoPreg = 21;
                                }
                                if (format.toLowerCase().equals("si-no-n/e")) {
                                    tipoPreg = 26;
                                }
                                if (format.toLowerCase().equals("si - no - ne")) {
                                    tipoPreg = 26;
                                }
                                if (format.toLowerCase().equals("si-no-na")) {
                                    tipoPreg = 26;
                                }
                                if (format.toLowerCase().equals("ok")) {
                                    tipoPreg = 28;
                                    obj.setOpcionesCombo("OK");
                                    obj.setFlagVF(2);
                                }
                                if (format.toLowerCase().equals("v-f")) {
                                    tipoPreg = 28;
                                    obj.setFlagVF(1);
                                }

                                if (format.toLowerCase().equals("verdadero - falso")) {
                                    tipoPreg = 28;
                                    obj.setFlagVF(1);
                                }

                                if (format.toLowerCase().equals("texto")) {
                                    tipoPreg = 24;
                                }
                                if (format.toLowerCase().equals("numérica")) {
                                    tipoPreg = 23;
                                }
                                if (format.toLowerCase().equals("combo")) {
                                    tipoPreg = 28;
                                    obj.setFlagVF(0);
                                }

                                obj.setTipoRespuesta(tipoPreg);
                                break;
                            case 12:
                                obj.setOpcionesCombo(format);
                                break;
                            case 13:
                                obj.setCumplimiento(format);
                                break;
                            case 14:
                                if (format.contains("%")) {
                                    format = format.replace("%", "");
                                }
                                if (format.contains(",")) {
                                    format = format.replace(",", ".");
                                }
                                if (format.contains("NA")) {
                                    format = format.replace("NA", "0.0");
                                }
                                obj.setPonderacion(Double.parseDouble(format));
                                break;
                            case 15:
                                int comentarios = 0;
                                if (format.toLowerCase().contains("si")) {
                                    comentarios = 1;
                                }
                                obj.setRequiereComentarios("" + comentarios);
                                break;
                            case 16:
                                int evid = 0;
                                if (format.toLowerCase().contains("si")) {
                                    evid = 1;
                                }
                                obj.setRequiereEvidencia("" + evid);
                                break;
                            case 17:
                                int tipoEvid = 0;
                                if (format.toLowerCase().contains("foto")) {
                                    tipoEvid = 1;
                                }
                                if (format.toLowerCase().contains("video")) {
                                    tipoEvid = 2;
                                }
                                obj.setTipoEvidencia("" + tipoEvid);
                                break;
                            case 18:
                                obj.setPlantillaSi(format);
                                break;
                            case 19:
                                obj.setPlantillaNo(format);
                                break;
                            case 20:
                                obj.setResponsable1(format);
                                break;
                            case 21:
                                obj.setResponsable2(format);
                                break;
                            case 22:
                                obj.setZona(format);
                                break;
                            case 23:
                                obj.setDefinicion(format);
                                break;
                            case 24:
                                obj.setDisciplina(format);
                                break;
                            case 25:
                                obj.setSla(format);
                                break;
                            default:
                                break;

                        }
                    }

                } // Cierra for cell

                if (filas != 1) {

                    if (obj.getPregunta() != null) {
                        lista.add(obj);
                    }

                }

            } // Cierra for row

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return lista;
    }

    public ArrayList<ChecklistLayoutDTO> getListaArbol(ArrayList<ChecklistLayoutDTO> listaData, int tipoCarga) {

        ArrayList<ChecklistLayoutDTO> response = new ArrayList<ChecklistLayoutDTO>();

        try {
            int pos = 0;
            for (ChecklistLayoutDTO layout : listaData) {

                ArrayList<ArbolDecisionDTO> arbol = new ArrayList<ArbolDecisionDTO>();

                //---------------- Tipo evidencia y plantilla ---------------------
                int tipoEvidSi = 0;
                int obligaSi = 0;
                int plantillaSi = 0;
                if (!layout.getRequiereEvidencia().equals("0")) {
                    if (layout.getTipoEvidencia().equals("2")) {
                        tipoEvidSi = 3;
                    } else {
                        switch (layout.getPlantillaSi().trim()) {
                            case "A":
                                tipoEvidSi = 1;
                                obligaSi = 0;
                                break;
                            case "B":
                                tipoEvidSi = 1;
                                obligaSi = 1;
                                break;
                            case "C":
                                tipoEvidSi = 5;
                                obligaSi = 0;
                                plantillaSi = 5;
                                break;
                            case "D":
                                tipoEvidSi = 5;
                                obligaSi = 0;
                                plantillaSi = 1;
                                break;
                            default:

                        }

                    }

                }

                int tipoEvidNo = 0;
                int obligaNo = 0;
                int plantillaNo = 0;
                if (!layout.getRequiereEvidencia().equals("0")) {
                    if (layout.getTipoEvidencia().equals("2")) {
                        tipoEvidNo = 3;
                    } else {
                        switch (layout.getPlantillaNo().trim()) {
                            case "A":
                                tipoEvidNo = 1;
                                obligaNo = 0;
                                break;
                            case "B":
                                tipoEvidNo = 1;
                                obligaNo = 1;
                                break;
                            case "C":
                                tipoEvidNo = 5;
                                obligaNo = 0;
                                plantillaNo = 5;
                                break;
                            case "D":
                                tipoEvidNo = 5;
                                obligaNo = 0;
                                plantillaNo = 1;
                                break;
                            default:

                        }

                    }

                }
                //------------------------------------------------
                //---------------- Salto preguntas --------------------
                boolean padre = false;
                int saltoSi = 0;
                int saltoNo = 0;
                if (pos + 1 != listaData.size()) {
                    saltoSi = listaData.get(pos + 1).getIdConsecutivoProtocolo();

                    if (saltoSi == 1) {
                        saltoSi = 0;
                    }

                    if (listaData.get(pos).getIdPreguntaPadre() == 0) {
                        if (listaData.get(pos + 1).getIdPreguntaPadre() != 0) {
                            //System.out.println("Entro a verificar el salto de preguntas");
                            //Busca posicion para salto
                            for (int i = pos + 1; i < listaData.size(); i++) {
                                if (listaData.get(i).getIdPreguntaPadre() == 0) {
                                    if (pos + 1 == listaData.size()) {
                                        saltoNo = 0;
                                    } else {
                                        saltoNo = listaData.get(i).getIdConsecutivoProtocolo();

                                    }
                                    padre = true;
                                    break;
                                }
                                
                                if((tipoCarga == 1) && (pos + 1 == (listaData.size()-1)))  {
                                	 padre = true;
                                     break;
                                }
                            }

                        } else {
                            saltoNo = listaData.get(pos + 1).getIdConsecutivoProtocolo();
                        }
                    } else {
                        saltoNo = listaData.get(pos + 1).getIdConsecutivoProtocolo();
                    }
                }

                if (saltoNo == 1) {
                    saltoNo = 0;
                }
                //------------------------------------------------------

                //---------------- Obtiene ponderaciones --------------------
                double pondNa = 0.0;
                if (padre == true) {
                    pondNa = listaData.get(pos).getPonderacion();
                    if (listaData.get(pos).getIdPreguntaPadre() == 0) {
                        if (listaData.get(pos + 1).getIdPreguntaPadre() != 0) {
                            //Busca posicion para salto
                            //System.out.println("Entro a verificar la ponderación de preguntas padre");
                            for (int i = pos + 1; i < listaData.size(); i++) {
                                if (listaData.get(i).getIdPreguntaPadre() != 0) {
                                    pondNa = pondNa + listaData.get(i).getPonderacion();

                                } else {
                                    break;
                                }
                            }

                        }
                    }

                }
                //------------------------------------------------------

                //---------------- Tipo SI-NO --------------------
                if (layout.getPreguntaDTO().getIdTipo() == 21) {
                    ArbolDecisionDTO arbolSi = new ArbolDecisionDTO();

                    double pondSi = layout.getPonderacion();
                    double pondNo = 0;
                    if (padre == true) {
                        if (layout.getCumplimiento().toLowerCase().trim().equals("si")) {
                            if (tipoCarga == 1) {
                                pondNo = 0.0;
                            } else {
                                pondNo = pondNa;
                            }
                        } else {
                            pondSi = pondNa;
                            int saltoTempNo = saltoNo;
                            int saltoTempSi = saltoSi;
                            saltoSi = saltoTempNo;
                            saltoNo = saltoTempSi;
                        }
                    }

                    if (tipoEvidSi == 0) {
                        arbolSi.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolSi.setEstatusEvidencia(tipoEvidSi);//Depende lista entrada
                    }
                    arbolSi.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolSi.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolSi.setOrdenCheckRespuesta(saltoSi);//Depende lista entrada
                    arbolSi.setRespuesta("1");
                    arbolSi.setReqAccion(0);
                    arbolSi.setCommit(1);
                    arbolSi.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolSi.setReqOblig(obligaSi);//Depende de la plantilla
                    arbolSi.setDescEvidencia("Tomar Evidencia");
                    arbolSi.setPonderacion("" + pondSi);//Depende de la lista Data
                    arbolSi.setIdPlantilla(plantillaSi);//Depende Lista data
                    arbolSi.setIdProtocolo(0);//Aplica solo Zonas

                    ArbolDecisionDTO arbolNo = new ArbolDecisionDTO();
                    if (tipoEvidNo == 0) {
                        arbolNo.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolNo.setEstatusEvidencia(tipoEvidNo);//Depende lista entrada
                    }
                    arbolNo.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolNo.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolNo.setOrdenCheckRespuesta(saltoNo);//Depende lista entrada
                    arbolNo.setRespuesta("2");
                    arbolNo.setReqAccion(0);
                    arbolNo.setCommit(1);
                    arbolNo.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolNo.setReqOblig(obligaNo);//
                    arbolNo.setDescEvidencia("Tomar Evidencia");
                    arbolNo.setPonderacion("" + pondNo);//Depende de la lista Data

                    arbolNo.setIdPlantilla(plantillaNo);//Depende Lista data
                    arbolNo.setIdProtocolo(0);//Aplica solo Zonas

                    arbol.add(arbolSi);
                    arbol.add(arbolNo);
                }
                //------------------------------------------------------

                //---------------- Tipo SI-NO-NE o SI-NO-NA --------------------
                if (layout.getPreguntaDTO().getIdTipo() == 26) {

                    double pondSi = layout.getPonderacion();
                    double pondNo = 0;

                    ArbolDecisionDTO arbolSi = new ArbolDecisionDTO();
                    if (tipoEvidSi == 0) {
                        arbolSi.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolSi.setEstatusEvidencia(tipoEvidSi);//Depende lista entrada
                    }
                    arbolSi.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolSi.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolSi.setOrdenCheckRespuesta(saltoSi);//Depende lista entrada
                    arbolSi.setRespuesta("1");
                    arbolSi.setReqAccion(0);
                    arbolSi.setCommit(1);
                    arbolSi.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolSi.setReqOblig(obligaSi);//Depende de la plantilla
                    arbolSi.setDescEvidencia("Tomar Evidencia");
                    arbolSi.setPonderacion("" + layout.getPonderacion());//Depende de la lista Data
                    arbolSi.setIdPlantilla(plantillaSi);//Depende Lista data
                    arbolSi.setIdProtocolo(0);//Aplica solo Zonas

                    ArbolDecisionDTO arbolNo = new ArbolDecisionDTO();
                    if (tipoEvidNo == 0) {
                        arbolNo.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolNo.setEstatusEvidencia(tipoEvidNo);//Depende lista entrada
                    }
                    arbolNo.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolNo.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolNo.setOrdenCheckRespuesta(saltoNo);//Depende lista entrada
                    arbolNo.setRespuesta("2");
                    arbolNo.setReqAccion(0);
                    arbolNo.setCommit(1);
                    arbolNo.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolNo.setReqOblig(obligaNo);//
                    arbolNo.setDescEvidencia("Tomar Evidencia");
                    arbolNo.setPonderacion("0");//Depende de la lista Data
                    arbolNo.setIdPlantilla(plantillaNo);//Depende Lista data
                    arbolNo.setIdProtocolo(0);//Aplica solo Zonas

                    ArbolDecisionDTO arbolNa = new ArbolDecisionDTO();
                    arbolNa.setEstatusEvidencia(0);//Depende lista entrada
                    arbolNa.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolNa.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolNa.setOrdenCheckRespuesta(saltoNo);//Depende lista entrada
                    arbolNa.setRespuesta("3");
                    arbolNa.setReqAccion(0);
                    arbolNa.setCommit(1);
                    arbolNa.setReqObservacion(0);//Depende lista entrada
                    arbolNa.setReqOblig(0);//
                    arbolNa.setDescEvidencia("Tomar Evidencia");
                    if (padre == true) {
                        arbolNa.setPonderacion("" + pondNa);//Depende de la lista Data
                    } else {
                        arbolNa.setPonderacion("" + pondSi);//Depende de la lista Data
                    }
                    arbolNa.setIdPlantilla(0);//Depende Lista data
                    arbolNa.setIdProtocolo(0);//Aplica solo Zonas

                    arbol.add(arbolSi);
                    arbol.add(arbolNo);
                    arbol.add(arbolNa);
                }
                //------------------------------------------------------

                //---------------- Tipo Texto --------------------
                if (layout.getPreguntaDTO().getIdTipo() == 24) {
                    ArbolDecisionDTO arbolSi = new ArbolDecisionDTO();
                    if (tipoEvidSi == 0) {
                        arbolSi.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolSi.setEstatusEvidencia(tipoEvidSi);//Depende lista entrada
                    }
                    arbolSi.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolSi.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolSi.setOrdenCheckRespuesta(saltoSi);//Depende lista entrada
                    arbolSi.setRespuesta("4");
                    arbolSi.setReqAccion(0);
                    arbolSi.setCommit(1);
                    arbolSi.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolSi.setReqOblig(obligaSi);//Depende de la plantilla
                    arbolSi.setDescEvidencia("Tomar Evidencia");
                    arbolSi.setPonderacion("" + layout.getPonderacion());//Depende de la lista Data
                    arbolSi.setIdPlantilla(plantillaSi);//Depende Lista data
                    arbolSi.setIdProtocolo(0);//Aplica solo Zonas

                    arbol.add(arbolSi);
                }
                //------------------------------------------------------

                //---------------- Tipo Numerica --------------------
                if (layout.getPreguntaDTO().getIdTipo() == 23) {
                    ArbolDecisionDTO arbolSi = new ArbolDecisionDTO();
                    if (tipoEvidSi == 0) {
                        arbolSi.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolSi.setEstatusEvidencia(tipoEvidSi);//Depende lista entrada
                    }
                    arbolSi.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolSi.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolSi.setOrdenCheckRespuesta(saltoSi);//Depende lista entrada
                    arbolSi.setRespuesta("4");
                    arbolSi.setReqAccion(0);
                    arbolSi.setCommit(1);
                    arbolSi.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolSi.setReqOblig(obligaSi);//Depende de la plantilla
                    arbolSi.setDescEvidencia("Tomar Evidencia");
                    arbolSi.setPonderacion("" + layout.getPonderacion());//Depende de la lista Data
                    arbolSi.setIdPlantilla(plantillaSi);//Depende Lista data
                    arbolSi.setIdProtocolo(0);//Aplica solo Zonas

                    arbol.add(arbolSi);
                }
                //------------------------------------------------------

                //---------------- Tipo V-F --------------------
                if (layout.getPreguntaDTO().getIdTipo() == 28 && layout.getFlagVF() == 1) {
                    ArbolDecisionDTO arbolSi = new ArbolDecisionDTO();
                    if (tipoEvidSi == 0) {
                        arbolSi.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolSi.setEstatusEvidencia(tipoEvidSi);//Depende lista entrada
                    }
                    arbolSi.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolSi.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolSi.setOrdenCheckRespuesta(saltoSi);//Depende lista entrada
                    arbolSi.setRespuesta("432");
                    arbolSi.setReqAccion(0);
                    arbolSi.setCommit(1);
                    arbolSi.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolSi.setReqOblig(obligaSi);//Depende de la plantilla
                    arbolSi.setDescEvidencia("Tomar Evidencia");
                    arbolSi.setPonderacion("" + layout.getPonderacion());//Depende de la lista Data
                    arbolSi.setIdPlantilla(plantillaSi);//Depende Lista data
                    arbolSi.setIdProtocolo(0);//Aplica solo Zonas

                    ArbolDecisionDTO arbolNo = new ArbolDecisionDTO();
                    if (tipoEvidNo == 0) {
                        arbolNo.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolNo.setEstatusEvidencia(tipoEvidNo);//Depende lista entrada
                    }
                    arbolNo.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolNo.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolNo.setOrdenCheckRespuesta(saltoNo);//Depende lista entrada
                    arbolNo.setRespuesta("433");
                    arbolNo.setReqAccion(0);
                    arbolNo.setCommit(1);
                    arbolNo.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolNo.setReqOblig(obligaNo);//
                    arbolNo.setDescEvidencia("Tomar Evidencia");
                    if (padre == true && tipoCarga == 2) {
                        arbolNo.setPonderacion("" + pondNa);//Depende de la lista Data
                    } else {
                        arbolNo.setPonderacion("0");//Depende de la lista Data
                    }
                    arbolNo.setIdPlantilla(plantillaNo);//Depende Lista data
                    arbolNo.setIdProtocolo(0);//Aplica solo Zonas

                    arbol.add(arbolSi);
                    arbol.add(arbolNo);
                }
                //------------------------------------------------------

                //---------------- Tipo OK --------------------
                if (layout.getPreguntaDTO().getIdTipo() == 28 && layout.getFlagVF() == 2) {
                    ArbolDecisionDTO arbolSi = new ArbolDecisionDTO();
                    if (tipoEvidSi == 0) {
                        arbolSi.setEstatusEvidencia(0);//Depende lista entrada
                    } else {
                        arbolSi.setEstatusEvidencia(tipoEvidSi);//Depende lista entrada
                    }
                    arbolSi.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                    arbolSi.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                    arbolSi.setOrdenCheckRespuesta(saltoSi);//Depende lista entrada
                    arbolSi.setRespuesta("485");
                    arbolSi.setReqAccion(0);
                    arbolSi.setCommit(1);
                    arbolSi.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                    arbolSi.setReqOblig(obligaSi);//Depende de la plantilla
                    arbolSi.setDescEvidencia("Tomar Evidencia");
                    arbolSi.setPonderacion("" + layout.getPonderacion());//Depende de la lista Data
                    arbolSi.setIdPlantilla(plantillaSi);//Depende Lista data
                    arbolSi.setIdProtocolo(0);//Aplica solo Zonas

                    arbol.add(arbolSi);
                }
                //------------------------------------------------------

                //---------------- Tipo Combo --------------------
                if (layout.getPreguntaDTO().getIdTipo() == 28 && layout.getFlagVF() == 0) {

                    String[] opcCombo = layout.getOpcionesCombo().split(",");

                    for (String opc : opcCombo) {
                        int idCombo = 0;
                        for (int i = 0; i < 3; i++) {
                            idCombo = getOpcionCombo(opc);
                            if (idCombo != 0) {
                                break;
                            }
                        }
                        if (idCombo != 0) {
                            ArbolDecisionDTO arbolSi = new ArbolDecisionDTO();
                            if (tipoEvidSi == 0) {
                                arbolSi.setEstatusEvidencia(0);//Depende lista entrada
                            } else {
                                arbolSi.setEstatusEvidencia(tipoEvidSi);//Depende lista entrada
                            }
                            arbolSi.setIdCheckList(layout.getChecklistProtocoloDTO().getIdChecklist());
                            arbolSi.setIdPregunta(layout.getPreguntaDTO().getIdPregunta());
                            arbolSi.setOrdenCheckRespuesta(saltoSi);//Depende lista entrada
                            arbolSi.setRespuesta("" + idCombo);
                            arbolSi.setReqAccion(0);
                            arbolSi.setCommit(1);
                            arbolSi.setReqObservacion(Integer.parseInt(layout.getRequiereComentarios()));//Depende lista entrada
                            arbolSi.setReqOblig(obligaSi);//Depende de la plantilla
                            arbolSi.setDescEvidencia("Tomar Evidencia");
                            arbolSi.setPonderacion("" + layout.getPonderacion());//Depende de la lista Data
                            arbolSi.setIdPlantilla(plantillaSi);//Depende Lista data
                            arbolSi.setIdProtocolo(0);//Aplica solo Zonas

                            arbol.add(arbolSi);
                        } else {
                            //Algo ocurrio al generar el combo
                            System.out.println("Algo ocurrio al generar el combo");
                        }
                    }
                }
                //------------------------------------------------------

                pos++;
                layout.setArbolDTO(arbol);

                response.add(layout);
            }

        } catch (Exception e) {

            System.out.println("ChecklistAdminBI " + e);
            e.printStackTrace();
            return response;

        }

        return response;

    }

    public int getOpcionCombo(String opc) {
        int response = 0;

        try {
            //inserta Opcion combo
            PosiblesDTO posTmp = new PosiblesDTO();
            posTmp.setDescripcion(opc);

            int res = posiblesbi.insertaPosible(opc);
            response = res;

        } catch (Exception e) {
            e.printStackTrace();
            response = 0;
        }

        return response;
    }

    public ArrayList<ChecklistLayoutDTO> altaArbol(ArrayList<ChecklistLayoutDTO> listaData, int tipoCarga) {

        ArrayList<ChecklistLayoutDTO> response = new ArrayList<ChecklistLayoutDTO>();

        try {
            int pos = 0;
            for (ChecklistLayoutDTO layout : listaData) {
                ArrayList<ArbolDecisionDTO> arbolDes = new ArrayList<ArbolDecisionDTO>();
                for (ArbolDecisionDTO arbol : layout.getArbolDTO()) {

                    int idArbol = 0;
                    for (int i = 0; i < 3; i++) {
                        idArbol = ejecutaArbol(arbol);
                        if (idArbol != 0) {
                            arbol.setIdArbolDesicion(idArbol);
                            break;
                        }
                    }
                    if (idArbol != 0) {
                        arbolDes.add(arbol);
                    } else {
                        layout.setFlagErrorArbol(1);
                        break;
                    }
                }
                layout.setArbolDTO(arbolDes);
                response.add(layout);
            }

        } catch (Exception e) {

            System.out.println("ChecklistAdminBI " + e);
            return response;

        }

        return response;

    }

    public int ejecutaArbol(ArbolDecisionDTO arbol) {
        int response = 0;

        try {
            //inserta Opcion combo

            //System.out.println("ARBOL PARA INSERTAR: " + arbol);
            int resArbol = arbolDecisionAdmbi.insertaArbolDecision(arbol);
            response = resArbol;

        } catch (Exception e) {
            e.printStackTrace();
            response = 0;
        }

        return response;
    }

    public int buscaPadre(ArrayList<ChecklistLayoutDTO> infoLayout, int idProtocolo, int numeroPadre) {
        int response = 0;
        for (ChecklistLayoutDTO layout : infoLayout) {
            if (layout.getChecklistProtocoloDTO().getIdChecklist() == idProtocolo && numeroPadre == layout.getIdConsecutivoProtocolo()) {
                response = layout.getPreguntaDTO().getIdPregunta();
                break;
            }
        }

        return response;
    }

    public ArrayList<ChecklistLayoutDTO> leerJson(String datos) {
        ArrayList<ChecklistLayoutDTO> retorno = new ArrayList<ChecklistLayoutDTO>();
        try {
            //System.out.println("Json Entrada:  " + datos);

            int tipoCarga = 0;
            if (datos != null) {
                JSONObject rec = new JSONObject(datos);

                JSONObject generales = rec.getJSONObject("datos_generales");
                String nomProtocolo = generales.getString("nombre");
                String tipoCar = generales.getString("tipo_protocolo");

                if (tipoCar.equals("Infraestructura")) {
                    tipoCarga = 1;
                } else {
                    tipoCarga = 2;
                }
                JSONArray secciones = rec.getJSONArray("secciones");
                int contGlobal = 0;
                for (int i = 0; i < secciones.length(); i++) {
                    JSONObject generalesSeccion = secciones.getJSONObject(i).getJSONObject("datos_generales");
                    String seccion = generalesSeccion.getString("nombre");
                    String sub_seccion = generalesSeccion.getString("sub_seccion");

                    JSONArray preguntas = secciones.getJSONObject(i).getJSONArray("preguntas");

                    for (int j = 0; j < preguntas.length(); j++) {
                        contGlobal++;
                        ChecklistLayoutDTO objLayout = new ChecklistLayoutDTO();
                        objLayout.setIdConsecutivo(contGlobal);
                        objLayout.setCodigoChecklist(preguntas.getJSONObject(j).getInt("codigo"));
                        objLayout.setNegocio(preguntas.getJSONObject(j).getString("negocio"));
                        objLayout.setNombreProtocolo(nomProtocolo);
                        objLayout.setSeccionProtocolo(seccion);
                        objLayout.setSubSeccionProtocolo(sub_seccion);
                        objLayout.setIdConsecutivoProtocolo(preguntas.getJSONObject(j).getInt("no"));

                        int critico = 0;
                        String format = preguntas.getJSONObject(j).getString("factorCritico");
                        if (format.toLowerCase().contains("imperdonable")) {
                            critico = 1;
                        }
                        if (format.toLowerCase().contains("crítico")) {
                            critico = 2;
                        }
                        if (format.toLowerCase().contains("alto")) {
                            critico = 2;
                        }
                        if (format.toLowerCase().contains("medio")) {
                            critico = 3;
                        }

                        objLayout.setFactorCritico(critico);

                        objLayout.setPregunta(preguntas.getJSONObject(j).getString("pregunta"));

                        int tipoPreg = 0;

                        format = preguntas.getJSONObject(j).getString("tipoRespuesta");

                        if (format.toLowerCase().equals("si-no")) {
                            tipoPreg = 21;
                        }
                        if (format.toLowerCase().equals("si - no")) {
                            tipoPreg = 21;
                        }
                        if (format.toLowerCase().equals("si-no-n/e")) {
                            tipoPreg = 26;
                        }
                        if (format.toLowerCase().equals("si - no - ne")) {
                            tipoPreg = 26;
                        }
                        if (format.toLowerCase().equals("si-no-na")) {
                            tipoPreg = 26;
                        }
                        if (format.toLowerCase().equals("si - no - na")) {
                            tipoPreg = 26;
                        }
                        if (format.toLowerCase().equals("ok")) {
                            tipoPreg = 28;
                            objLayout.setOpcionesCombo("OK");
                            objLayout.setFlagVF(2);
                        }
                        if (format.toLowerCase().equals("v-f")) {
                            tipoPreg = 28;
                            objLayout.setFlagVF(1);
                        }
                        if (format.toLowerCase().equals("verdadero - falso")) {
                            tipoPreg = 28;
                            objLayout.setFlagVF(1);
                        }
                        if (format.toLowerCase().equals("texto")) {
                            tipoPreg = 24;
                        }
                        if (format.toLowerCase().equals("numérica")) {
                            tipoPreg = 23;
                        }
                        if (format.toLowerCase().equals("combo")) {
                            tipoPreg = 28;
                            objLayout.setFlagVF(0);
                        }

                        objLayout.setTipoRespuesta(tipoPreg);
                        objLayout.setOpcionesCombo(preguntas.getJSONObject(j).getString("opcionesCombo"));
                        objLayout.setIdPreguntaPadre(preguntas.getJSONObject(j).getInt("padre"));
                        objLayout.setCumplimiento(preguntas.getJSONObject(j).getString("cumplimiento"));

                        double valorP = 0.0;
                        format = preguntas.getJSONObject(j).getString("valorPonderacion");
                        if (format.contains("%")) {
                            format = format.replace("%", "");
                        }
                        if (format.contains(",")) {
                            format = format.replace(",", ".");
                        }
                        if (format.contains("NA")) {
                            format = format.replace("NA", "0.0");
                        }
                        if (format.contains("N/A")) {
                            format = format.replace("NA", "0.0");
                        }
                        valorP = Double.parseDouble(format);

                        objLayout.setPonderacion(valorP);

                        int comentarios = 0;
                        format = preguntas.getJSONObject(j).getString("comentario");
                        if (format.toLowerCase().contains("si")) {
                            comentarios = 1;
                        }
                        objLayout.setRequiereComentarios("" + comentarios);

                        int evid = 0;
                        format = preguntas.getJSONObject(j).getString("evidencia");
                        if (format.toLowerCase().contains("si")) {
                            evid = 1;
                        }
                        objLayout.setRequiereEvidencia("" + evid);

                        int tipoEvid = 0;
                        format = preguntas.getJSONObject(j).getString("tipoEvidencia");
                        if (format.toLowerCase().contains("foto")) {
                            tipoEvid = 1;
                        }
                        if (format.toLowerCase().contains("video")) {
                            tipoEvid = 2;
                        }
                        objLayout.setTipoEvidencia("" + tipoEvid);

                        objLayout.setPlantillaSi(preguntas.getJSONObject(j).getString("plantillaSi"));
                        objLayout.setPlantillaNo(preguntas.getJSONObject(j).getString("plantillaNo"));
                        objLayout.setResponsable1(preguntas.getJSONObject(j).getString("responsable"));
                        objLayout.setResponsable2(preguntas.getJSONObject(j).getString("responsable"));
                        objLayout.setZona(preguntas.getJSONObject(j).getString("zona"));
                        objLayout.setDefinicion(preguntas.getJSONObject(j).getString("definicion"));
                        objLayout.setDisciplina(preguntas.getJSONObject(j).getString("negocio"));
                        objLayout.setSla(preguntas.getJSONObject(j).getString("sla"));
                        retorno.add(objLayout);

                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            retorno = null;
        }

        return retorno;

    }

    public String retornaJson(ArrayList<ChecklistLayoutDTO> layout, int tipoCarga) {
        String retorno = null;
        try {
            retorno = "";
            JsonObject envoltorioJsonObj = new JsonObject();

            JsonObject generales = new JsonObject();
            int contador = 0;

            ArrayList<ModuloDTO> modulos = getListaModulos(layout);

            HashMap<String, ArrayList<ChecklistLayoutDTO>> mapa = new HashMap<String, ArrayList<ChecklistLayoutDTO>>();
            for (int i = 0; i < modulos.size(); i++) {
                modulos.get(i).setIdModulo(i);
                mapa.put("" + i, new ArrayList<ChecklistLayoutDTO>());
            }

            String Null = null;
            JsonArray secciones = new JsonArray();
            for (ChecklistLayoutDTO aux : layout) {

                if (contador == 0) {

                    generales.addProperty("no", Null);
                    generales.addProperty("nombre", aux.getNombreProtocolo());

                    if (tipoCarga == 1) {
                        generales.addProperty("tipo_protocolo", "Infraestructura");
                    } else {
                        generales.addProperty("tipo_protocolo", "Aseguramiento de calidad");
                    }

                }

                for (int i = 0; i < modulos.size(); i++) {
                    if (aux.getSeccionProtocolo().trim().equals(modulos.get(i).getNombre().trim())) {
                        mapa.get("" + i).add(aux);
                        break;
                    }
                }

                contador++;
            }

            /*
			for(int i=0;i<modulos.size();i++) {
				ArrayList<ChecklistLayoutDTO> auxSeccion=mapa.get(""+i);
				ArrayList<ModuloDTO> subModulos=getSubModulos(auxSeccion);
				
				HashMap <String, ArrayList<ChecklistLayoutDTO>> mapaSub=new HashMap<String, ArrayList<ChecklistLayoutDTO>>();
				for(int j=0;j<subModulos.size();j++) {
					subModulos.get(j).setIdModulo(j);
					mapaSub.put(""+j, new ArrayList<ChecklistLayoutDTO>());
				}
				
				for(ChecklistLayoutDTO aux:auxSeccion) {
					
					for(int j=0;j<subModulos.size();j++) {
						if(aux.getSubSeccionProtocolo().trim().equals(subModulos.get(i).getNombre().trim())) {
							mapaSub.get(""+i).add(aux);
							break;
						}
					}
					
				}
				
				for(int j=0;j<subModulos.size();j++) {
					
					ArrayList<ChecklistLayoutDTO> auxSubSeccion=mapaSub.get(""+i);
					JsonObject Seccion = new JsonObject();
					JsonObject generalesSeccion = new JsonObject();
					JsonArray preguntas=new JsonArray();
					for(ChecklistLayoutDTO aux:auxSubSeccion) {
						//Incluir subseccion al json
						
						if(contador==0) {
							
							generalesSeccion.addProperty("no", Null);
							generalesSeccion.addProperty("nombre", aux.getSeccionProtocolo());
							generalesSeccion.addProperty("sub_seccion", aux.getSubSeccionProtocolo());
							
							
						}
						//secciones.add(generalesSeccion);
						
						
						
						
						JsonObject pregunta = new JsonObject();
						pregunta.addProperty("no", aux.getIdConsecutivoProtocolo());
						pregunta.addProperty("codigo", aux.getCodigoChecklist());
						pregunta.addProperty("pregunta", aux.getPregunta());
						pregunta.addProperty("factorCritico", aux.getFactorCritico());
						pregunta.addProperty("padre", aux.getIdPreguntaPadre());
						pregunta.addProperty("tipoRespuesta", aux.getTipoRespuesta());
						pregunta.addProperty("opcionesCombo", aux.getOpcionesCombo());
						pregunta.addProperty("cumplimiento", aux.getCumplimiento());
						pregunta.addProperty("valorPonderacion", aux.getPonderacion());
						pregunta.addProperty("comentario", aux.getRequiereComentarios());
						pregunta.addProperty("evidencia", aux.getRequiereEvidencia());
						pregunta.addProperty("tipoEvidencia", aux.getTipoEvidencia());
						pregunta.addProperty("plantillaSi", aux.getPlantillaSi());
						pregunta.addProperty("plantillaNo", aux.getPlantillaNo());
						pregunta.addProperty("definicion", aux.getDefinicion());
						pregunta.addProperty("zona", aux.getZona());
						pregunta.addProperty("responsable", aux.getZona());
						pregunta.addProperty("sla", aux.getSla());
						
						preguntas.add(pregunta);
						
						
					}
					Seccion.add("datos_generales", generalesSeccion);
					Seccion.add("preguntas", preguntas);
					envoltorioJsonObj.add("secciones", Seccion);
				}
				
				
			}*/
            JsonArray Secciones = new JsonArray();
            for (int j = 0; j < modulos.size(); j++) {

                ArrayList<ChecklistLayoutDTO> auxSubSeccion = mapa.get("" + j);
                JsonObject Seccion = new JsonObject();
                JsonObject generalesSeccion = new JsonObject();
                JsonArray preguntas = new JsonArray();
                int cont = 0;
                for (ChecklistLayoutDTO aux : auxSubSeccion) {
                    //Incluir subseccion al json

                    if (cont == 0) {

                        generalesSeccion.addProperty("no", Null);
                        generalesSeccion.addProperty("nombre", aux.getSeccionProtocolo());
                        generalesSeccion.addProperty("sub_seccion", aux.getSubSeccionProtocolo());

                    }
                    //secciones.add(generalesSeccion);

                    JsonObject pregunta = new JsonObject();
                    pregunta.addProperty("no", aux.getIdConsecutivoProtocolo());
                    pregunta.addProperty("codigo", aux.getCodigoChecklist());
                    pregunta.addProperty("negocio", aux.getNegocio());
                    pregunta.addProperty("pregunta", aux.getPregunta());

                    String fCritico = "";
                    if (aux.getFactorCritico() == 1) {
                        fCritico = "Imperdonable";
                    }
                    if (aux.getFactorCritico() == 2) {
                        fCritico = "Alto";
                    }
                    if (aux.getFactorCritico() == 3) {
                        fCritico = "Medio";
                    }
                    if (aux.getFactorCritico() == 4) {
                        fCritico = "Bajo";
                    }

                    pregunta.addProperty("factorCritico", fCritico);
                    pregunta.addProperty("padre", aux.getIdPreguntaPadre());

                    String tRespuesta = "";

                    if (aux.getTipoRespuesta() == 21) {
                        tRespuesta = "SI - NO";
                    }
                    if (aux.getTipoRespuesta() == 28 && aux.getFlagVF() == 1) {
                        tRespuesta = "Verdadero - Falso";
                    }
                    if (aux.getTipoRespuesta() == 28 && aux.getFlagVF() == 0) {
                        tRespuesta = "Combo";
                    }
                    if (aux.getTipoRespuesta() == 28 && aux.getFlagVF() == 2) {
                        tRespuesta = "OK";
                    }
                    if (aux.getTipoRespuesta() == 26) {
                        tRespuesta = "SI - NO - NE";
                    }
                    if (aux.getTipoRespuesta() == 24) {
                        tRespuesta = "Texto";
                    }
                    if (aux.getTipoRespuesta() == 23) {
                        tRespuesta = "Numérica";
                    }

                    pregunta.addProperty("tipoRespuesta", tRespuesta);
                    pregunta.addProperty("opcionesCombo", aux.getOpcionesCombo());

                    pregunta.addProperty("cumplimiento", aux.getCumplimiento().toUpperCase());
                    pregunta.addProperty("valorPonderacion", aux.getPonderacion());

                    String comentario = "";
                    if (aux.getRequiereComentarios().trim().equals("1")) {
                        comentario = "SI";
                    } else {
                        comentario = "NO";
                    }

                    pregunta.addProperty("comentario", comentario);

                    String evidecia = "";
                    if (aux.getRequiereEvidencia().trim().equals("1")) {
                        evidecia = "SI";
                    } else {
                        evidecia = "NO";
                    }

                    pregunta.addProperty("evidencia", evidecia);

                    String tEvidecia = "";
                    if (aux.getTipoEvidencia().trim().equals("1")) {
                        tEvidecia = "Foto";
                    } else {
                        tEvidecia = "";
                    }
                    if (aux.getTipoEvidencia().trim().equals("2")) {
                        tEvidecia = "Video";
                    }
                    pregunta.addProperty("tipoEvidencia", tEvidecia);
                    pregunta.addProperty("plantillaSi", aux.getPlantillaSi());
                    pregunta.addProperty("plantillaNo", aux.getPlantillaNo());
                    pregunta.addProperty("definicion", aux.getDefinicion());

                    String zona = "";
                    if (tipoCarga == 1) {
                        switch (aux.getZona()) {
                            case "Generales":
                                zona = "GENERALES";
                                break;
                            case "Áreas comunes":
                                zona = "AREAS";
                                break;
                            case "BAZ":
                                zona = "BAZ";
                                break;
                            case "EKT":
                                zona = "EKT";
                                break;
                            case "Presta Prenda":
                                zona = "PP";
                                break;
                            case "Crédito y cobranza":
                                zona = "CYC";
                                break;
                            default:
                                zona = aux.getZona();
                                ;
                        }
                    } else {
                        zona = aux.getZona();
                    }
                    pregunta.addProperty("zona", zona);
                    pregunta.addProperty("responsable", aux.getResponsable2());
                    pregunta.addProperty("sla", aux.getSla());

                    preguntas.add(pregunta);
                    cont++;

                }
                Seccion.add("datos_generales", generalesSeccion);
                Seccion.add("preguntas", preguntas);
                Secciones.add(Seccion);
            }
            envoltorioJsonObj.add("secciones", Secciones);

            envoltorioJsonObj.add("datos_generales", generales);

            //logger.info("RESPUESTAS: " + envoltorioJsonObj.toString());
            retorno = envoltorioJsonObj.toString();

        } catch (Exception e) {
            e.printStackTrace();
            retorno = null;
        }
        return retorno;
    }

    public ArrayList<ModuloDTO> getSubModulos(ArrayList<ChecklistLayoutDTO> listaData) {

        ArrayList<ModuloDTO> listaModulos = new ArrayList<ModuloDTO>();

        try {

            String seccionActual = "";
            boolean existe = false;
            for (ChecklistLayoutDTO objLista : listaData) {

                if (!objLista.getSubSeccionProtocolo().equalsIgnoreCase(seccionActual)) {

                    // VALIDAR SECCIONES SALTADAS
                    existe = false;
                    seccionActual = objLista.getSubSeccionProtocolo();

                    ModuloDTO modulo = new ModuloDTO();
                    modulo.setIdModulo(0);
                    modulo.setNombre(objLista.getSubSeccionProtocolo());
                    modulo.setIdModuloPadre(0);
                    modulo.setNombrePadre("");
                    modulo.setNumVersion("0");
                    modulo.setTipoCambio("");
                    modulo.setIdChecklist(0);

                    for (ModuloDTO obj : listaModulos) {

                        if (objLista.getSubSeccionProtocolo().compareTo(obj.getNombre()) == 0) {

                            existe = true;
                            break;
                        }

                    }

                    if (!existe) {
                        listaModulos.add(modulo);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listaModulos;

    }

    public String retornaProtocoloBase(int idProtocolo, int tipoCarga) {
        String resultado = null;

        try {
            resultado = "";
            String nula = null;
            int contador = 0;
            JsonObject jsonRespuesta = new JsonObject();
            JsonObject datosGrales = new JsonObject();
            List<ModuloDTO> listaModulos = null;
            List<ChecklistPreguntaDTO> listaChecklistPregunta = null;

            Map<String, Object> res = reporteImagenesAdmBI.obtieneModulosTiendas("", String.valueOf(idProtocolo));
            listaModulos = (List<ModuloDTO>) res.get("modulos");
            //logger.info("Modulos: " + listaModulos.toString());
            listaChecklistPregunta = preguntaAdmValidaBI.obtienePregXcheck(idProtocolo);
            //logger.info("Lista Checklist Pregunta: " + listaChecklistPregunta.toString());

            ChecklistProtocoloDTO checklistProtocolo = new ChecklistProtocoloDTO();
            checklistProtocolo.setIdChecklist(idProtocolo);
            List<ChecklistProtocoloDTO> listaProtocolos = checklistProtocoloAdmBI.buscaChecklist(checklistProtocolo);
            //logger.info("Lista Checklist Protocolo: " + listaProtocolos.toString());

            for (int i = 0; i < listaModulos.size(); i++) {
                if (contador == 0) {
                    datosGrales.addProperty("no", nula);
                    if (listaProtocolos.get(i).getIdChecklist() == idProtocolo) {
                        datosGrales.addProperty("nombre", listaProtocolos.get(i).getNombreCheck());
                    }
                    if (tipoCarga == 1) {
                        datosGrales.addProperty("tipo_protocolo", "Infraestructura");
                    } else {
                        datosGrales.addProperty("tipo_protocolo", "Aseguramiento de calidad");
                    }

                }
                contador++;

            }

            JsonArray arrSecciones = new JsonArray();
            for (int j = 0; j < listaModulos.size(); j++) {
                int cont = 0;
                JsonObject seccionesGrales = new JsonObject();
                JsonObject secciones = new JsonObject();
                JsonArray arrPreguntas = new JsonArray();

                secciones.addProperty("no", nula);
                secciones.addProperty("nombre", listaModulos.get(j).getNombre());
                secciones.addProperty("sub_seccion", "");
                for (int k = 0; k < listaChecklistPregunta.size(); k++) {

                    JsonObject pregunta = new JsonObject();
                    pregunta.addProperty("no", "");
                    pregunta.addProperty("codigo", listaChecklistPregunta.get(k).getCodigo());
                    pregunta.addProperty("negocio", "");
                    pregunta.addProperty("pregunta", listaChecklistPregunta.get(k).getPregunta());
                    pregunta.addProperty("factorCritico", "");
                    pregunta.addProperty("padre", listaChecklistPregunta.get(k).getPregPadre());

                    String tRespuesta = "";

                    if (listaChecklistPregunta.get(k).getIdTipoPregunta() == 21) {
                        tRespuesta = "SI - NO";
                    } else if (listaChecklistPregunta.get(k).getIdTipoPregunta() == 28 && listaChecklistPregunta.get(k).getIdTipoPregunta() == 1) {
                        tRespuesta = "Verdadero - Falso";
                    } else if (listaChecklistPregunta.get(k).getIdTipoPregunta() == 28 && listaChecklistPregunta.get(k).getIdTipoPregunta() == 0) {
                        tRespuesta = "Combo";
                    } else if (listaChecklistPregunta.get(k).getIdTipoPregunta() == 28 && listaChecklistPregunta.get(k).getIdTipoPregunta() == 2) {
                        tRespuesta = "OK";
                    } else if (listaChecklistPregunta.get(k).getIdTipoPregunta() == 26) {
                        tRespuesta = "SI - NO - NE";
                    } else if (listaChecklistPregunta.get(k).getIdTipoPregunta() == 24) {
                        tRespuesta = "Texto";
                    } else if (listaChecklistPregunta.get(k).getIdTipoPregunta() == 23) {
                        tRespuesta = "Numérica";
                    }
                    pregunta.addProperty("tipoRespuesta", tRespuesta);
                    pregunta.addProperty("opcionesCombo", "");
                    pregunta.addProperty("cumplimiento", "");
                    pregunta.addProperty("valorPonderacion", "");
                    pregunta.addProperty("definicion", listaChecklistPregunta.get(k).getDetalle());

                    arrPreguntas.add(pregunta);
                    cont++;
                }
                seccionesGrales.add("datos_generales", secciones);
                seccionesGrales.add("preguntas", arrPreguntas);
                arrSecciones.add(seccionesGrales);
            }
            jsonRespuesta.add("secciones", arrSecciones);
            jsonRespuesta.add("datos_generales", datosGrales);

            logger.info("RESULTADO: " + jsonRespuesta.toString());
            resultado = jsonRespuesta.toString();
        } catch (Exception e) {
            e.printStackTrace();
            resultado = null;
        }

        return resultado;
    }

    public ArrayList<ChecklistLayoutDTO> retornaLayoutBase(int idProtocolo, int tipoCarga) {
        ArrayList<ChecklistLayoutDTO> lista = new ArrayList<ChecklistLayoutDTO>();
        try {

            HashMap<String, ModuloDTO> mapaMod = new HashMap<String, ModuloDTO>();

            List<ModuloDTO> listaModulos = null;
            List<ChecklistPreguntaDTO> listaChecklistPregunta = null;

            Map<String, Object> res = reporteImagenesAdmBI.obtieneModulosTiendas("", String.valueOf(idProtocolo));
            listaModulos = (List<ModuloDTO>) res.get("modulos");
            //logger.info("Modulos: " + listaModulos.toString());
            listaChecklistPregunta = preguntaAdmValidaBI.obtienePregXcheck(idProtocolo);
            //logger.info("Lista Checklist Pregunta: "+listaChecklistPregunta.toString());

            ChecklistProtocoloDTO checklistProtocolo = new ChecklistProtocoloDTO();
            checklistProtocolo.setIdChecklist(idProtocolo);
            List<ChecklistProtocoloDTO> listaProtocolos = checklistProtocoloAdmBI.buscaChecklist(checklistProtocolo);
            //logger.info("Lista Checklist Protocolo: " + listaProtocolos.toString());

            List<ArbolDecisionDTO> listaArbol = arbolDecisionAdmbiT.buscaArbolDecisionTemp(idProtocolo);

            HashMap<String, List<ArbolDecisionDTO>> mapaArbol = obtieneArbolesPreg(listaArbol);

            for (ModuloDTO mod : listaModulos) {
                mapaMod.put("" + mod.getIdModulo(), mod);
            }

            String nomProtocolo = "";
            String negocio = "";
            String zonaInfra = "";
            for (ChecklistProtocoloDTO prot : listaProtocolos) {
                nomProtocolo = prot.getNombreCheck();
                negocio = prot.getNegocio();
                zonaInfra = prot.getClasifica();
            }

            for (ChecklistPreguntaDTO preg : listaChecklistPregunta) {
                ChecklistLayoutDTO objLayout = new ChecklistLayoutDTO();

                List<ArbolDecisionDTO> listaArbolPreg = mapaArbol.get("" + preg.getIdPregunta());

                objLayout.setIdConsecutivo(preg.getOrdenPregunta());
                if (preg.getCodigo() == null) {
                    objLayout.setCodigoChecklist(0);
                } else {
                    objLayout.setCodigoChecklist(preg.getCodigo());
                }
                objLayout.setNegocio(negocio);
                objLayout.setNombreProtocolo(nomProtocolo);
                objLayout.setSeccionProtocolo(mapaMod.get("" + preg.getIdModulo()).getNombre());
                objLayout.setSubSeccionProtocolo("");
                objLayout.setIdConsecutivoProtocolo(preg.getOrdenPregunta());
                objLayout.setFactorCritico(preg.getCritica()); //Falta factor critico
                objLayout.setPregunta(preg.getPregunta());
                objLayout.setTipoRespuesta(preg.getIdTipoPregunta());
                if (preg.getIdTipoPregunta() == 28) {
                    for (ArbolDecisionDTO auxArbol : listaArbolPreg) {
                        if (auxArbol.getRespuesta().equals("432")) {
                            objLayout.setFlagVF(1);
                            objLayout.setOpcionesCombo("");
                        }

                        if (auxArbol.getRespuesta().equals("485")) {
                            objLayout.setFlagVF(2);
                            objLayout.setOpcionesCombo("");
                        }
                    }
                    if (objLayout.getFlagVF() == 0) {
                        String combo = "";
                        for (ArbolDecisionDTO auxArbol : listaArbolPreg) {
                            String desRespuestaCombo = "";

                            try {
                                            
                                posiblesbi = (PosiblesAdmBI) FRQAppContextProvider.getApplicationContext().getBean("posiblesAdmBI");

                                List<PosiblesDTO> posibles = posiblesbi.buscaPosible(Integer.parseInt(auxArbol.getRespuesta()));
                                if (posibles != null && posibles.size() > 0) {
                                    desRespuestaCombo = posibles.get(0).getDescripcion();

                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }

                            combo = combo + desRespuestaCombo + ",";
                        }
                        objLayout.setOpcionesCombo(combo.substring(0, combo.length() - 1));
                    }

                } else {
                    objLayout.setOpcionesCombo("");//Faltan opciones combo
                }
                if (preg.getPregPadre() == 0) {
                    objLayout.setIdPreguntaPadre(0);
                } else {
                    objLayout.setIdPreguntaPadre(buscaPadreLayout(listaChecklistPregunta, preg.getPregPadre()));
                }

                boolean flagCombo = false;

                if (preg.getIdTipoPregunta() == 21) {
                    objLayout.setCumplimiento("SI");
                } else if (preg.getIdTipoPregunta() == 26) {
                    objLayout.setCumplimiento("SI");
                } else if (preg.getIdTipoPregunta() == 28) {
                    for (ArbolDecisionDTO auxArbol : listaArbolPreg) {
                        if (auxArbol.getRespuesta().equals("432")) {
                            objLayout.setCumplimiento("VERDADERO");
                            flagCombo = true;
                            break;
                        }
                    }

                    if (flagCombo != true) {
                        objLayout.setCumplimiento("NA");
                    }

                } else {
                    objLayout.setCumplimiento("NA");
                }

                double ponderacion = 0.0;

                if (preg.getIdTipoPregunta() == 21 || preg.getIdTipoPregunta() == 26) {

                    for (ArbolDecisionDTO auxArbol : listaArbolPreg) {
                        if (auxArbol.getRespuesta().equals("1")) {
                            ponderacion = Double.parseDouble(auxArbol.getPonderacion());
                            objLayout.setPonderacion(ponderacion);//Faltan Ponderacion
                        }
                    }

                } else {
                    objLayout.setPonderacion(0.0);//Faltan Ponderacion
                }

                objLayout.setRequiereComentarios("" + listaArbolPreg.get(0).getReqObservacion());//Faltan Comentaios
                boolean flagReqEvidencia = false;
                String reqEvidencia = "";
                String tipoEvidencia = "";
                String pantillaSi = "";
                String pantillaNo = "";

                for (ArbolDecisionDTO auxArbol : listaArbolPreg) {
                    if (auxArbol.getEstatusEvidencia() != 0) {
                        flagReqEvidencia = true;
                        if (reqEvidencia.equals("")) {
                            reqEvidencia = "1";
                        }
                        if (auxArbol.getRespuesta().equals("1") || auxArbol.getRespuesta().equals("432")) {
                            if (auxArbol.getEstatusEvidencia() == 1) {
                                tipoEvidencia = "1";
                                if (auxArbol.getReqOblig() == 1) {
                                    pantillaSi = "B";
                                } else {
                                    pantillaSi = "A";
                                }
                            }
                            if (auxArbol.getEstatusEvidencia() == 5) {
                                tipoEvidencia = "1";

                                if (auxArbol.getIdPlantilla() == 5) {
                                    pantillaSi = "C";
                                } else {
                                    pantillaSi = "B";
                                }

                            }
                            if (auxArbol.getEstatusEvidencia() == 3) {
                                tipoEvidencia = "2";
                            }
                        }
                        if (auxArbol.getRespuesta().equals("2") || auxArbol.getRespuesta().equals("433")) {
                            if (auxArbol.getEstatusEvidencia() == 1) {
                                tipoEvidencia = "1";
                                if (auxArbol.getReqOblig() == 1) {
                                    pantillaNo = "B";
                                } else {
                                    pantillaNo = "A";
                                }
                            }
                            if (auxArbol.getEstatusEvidencia() == 5) {
                                tipoEvidencia = "1";

                                if (auxArbol.getIdPlantilla() == 5) {
                                    pantillaNo = "C";
                                } else {
                                    pantillaNo = "B";
                                }

                            }
                            if (auxArbol.getEstatusEvidencia() == 3) {
                                tipoEvidencia = "2";
                            }
                        }
                    }
                }

                objLayout.setRequiereEvidencia(reqEvidencia);//Faltan Evidencia BASE
                objLayout.setTipoEvidencia(tipoEvidencia);//Faltan TIPO Evidencia BASE
                objLayout.setPlantillaSi(pantillaSi);;//Faltan TIPO Evidencia BASE
                objLayout.setPlantillaNo(pantillaNo);;//Faltan TIPO Evidencia BASE

                objLayout.setResponsable1(preg.getResponsable());
                objLayout.setResponsable2("");

                if (tipoCarga == 1) {
                    objLayout.setZona(zonaInfra);
                } else {
                    objLayout.setZona(preg.getResponsable());
                }
                objLayout.setDefinicion(preg.getDetalle());
                objLayout.setDisciplina("");
                objLayout.setSla("" + preg.getSla());

                lista.add(objLayout);
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Ucurrio Algo al retornar el layut de base");
        }

        return lista;
    }

    public int buscaPadreLayout(List<ChecklistPreguntaDTO> objListaPreg, int idPreg) {
        int res = 0;
        try {
            for (ChecklistPreguntaDTO aux : objListaPreg) {
                if (idPreg == aux.getIdPregunta()) {
                    res = aux.getOrdenPregunta();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            res = 0;
        }
        return res;
    }

    public HashMap<String, List<ArbolDecisionDTO>> obtieneArbolesPreg(List<ArbolDecisionDTO> listaArbol) {
        HashMap<String, List<ArbolDecisionDTO>> listaArbolRetorno = new HashMap<String, List<ArbolDecisionDTO>>();
        try {
            for (ArbolDecisionDTO aux : listaArbol) {

                if (!listaArbolRetorno.containsKey("" + aux.getIdPregunta())) {
                    List<ArbolDecisionDTO> listaaux = new ArrayList<ArbolDecisionDTO>();
                    listaaux.add(aux);
                    listaArbolRetorno.put("" + aux.getIdPregunta(), listaaux);
                } else {
                    listaArbolRetorno.get("" + aux.getIdPregunta()).add(aux);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            listaArbolRetorno = null;
        }
        return listaArbolRetorno;
    }

}
