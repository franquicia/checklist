package com.gruposalinas.checklist.domain;

public class AdjuntoDTO {
	
	private String idIncidente;
	private String nombreArchivo;
	private String archivoBytes;
	private int numBytesRead;
	
	public String getIdIncidente() {
		return idIncidente;
	}
	public void setIdIncidente(String idIncidente) {
		this.idIncidente = idIncidente;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getArchivoBytes() {
		return archivoBytes;
	}
	public void setArchivoBytes(String archivoBytes) {
		this.archivoBytes = archivoBytes;
	}
	public int getNumBytesRead() {
		return numBytesRead;
	}
	public void setNumBytesRead(int numBytesRead) {
		this.numBytesRead = numBytesRead;
	}
	
	

}
