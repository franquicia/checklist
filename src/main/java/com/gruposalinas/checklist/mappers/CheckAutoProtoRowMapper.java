package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.CheckAutoProtoDTO;

public class CheckAutoProtoRowMapper implements RowMapper<CheckAutoProtoDTO> {

	public CheckAutoProtoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		CheckAutoProtoDTO chk = new CheckAutoProtoDTO();
		
		
	    chk.setIdTab(rs.getInt("FIID_AUTPRO"));
	    chk.setFiversion(rs.getInt("FIVERSION"));
		chk.setIdUsu(rs.getInt("FIID_USUARIO"));
		chk.setDetalle(rs.getString("FCDETALLE"));
		chk.setStatus(rs.getInt("FIID_STATUS"));
		chk.setUsuTipo(rs.getString("FCTIPOUSU"));
		chk.setFechaLiberacion(rs.getString("FDFECHALIBE"));
		chk.setNomUsu(rs.getString("FCNOMBRE"));
		
		return chk;
	}

}


