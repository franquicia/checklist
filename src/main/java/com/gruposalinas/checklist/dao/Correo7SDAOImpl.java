package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.Coment7SDTO;
import com.gruposalinas.checklist.mappers.BuscaCheckRowMapper;
import com.gruposalinas.checklist.mappers.Coments7sRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class Correo7SDAOImpl extends DefaultDAO implements Correo7SDAO {

    private static Logger logger = LogManager.getLogger(Correo7SDAOImpl.class);

    private DefaultJdbcCall buscaComent7S;
    private DefaultJdbcCall validaCheck7S;
    private DefaultJdbcCall califCheck7S;
    private DefaultJdbcCall buscaCheck7S;

    public void init() {

        buscaComent7S = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAKGCHECKENVIO")
                .withProcedureName("SP_SEL_BITA")
                .returningResultSet("RCL_BITA", new Coments7sRowMapper());

        validaCheck7S = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAKGCHECKENVIO")
                .withProcedureName("SP_VAL_USU");

        califCheck7S = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAKGCHECKENVIO")
                .withProcedureName("SP_SEL_CALIFICA");

        buscaCheck7S = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAKGCHECKENVIO")
                .withProcedureName("SP_VAL_USU")
                .returningResultSet("RCL_USU", new BuscaCheckRowMapper());

    }

    @Override
    public List<Coment7SDTO> buscaComentarios(int idBitacora) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<Coment7SDTO> listaComents = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDBITACORA", idBitacora);

        out = buscaComent7S.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAKGCHECKENVIO.SP_SEL_BITA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listaComents = (List<Coment7SDTO>) out.get("RCL_BITA");

        if (error == 1) {
            logger.info("Algo ocurrió");
        } else {
            return listaComents;
        }

        return null;
    }

    public boolean validaCehcklist(int idBitacora) throws Exception {
        boolean res = false;
        Map<String, Object> out = null;
        int error = 0;
        int VL = 0;

        List<Coment7SDTO> listaComents = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSU", null)
                .addValue("PA_IDBITA", idBitacora);

        out = validaCheck7S.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAKGCHECKENVIO.SP_SEL_BITA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal VLReturn = (BigDecimal) out.get("PA_VALSAL");

        VL = VLReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió");
        } else {
            if (VL == 1) {
                res = true;
            }
        }

        return res;
    }

    public double CalifCheck7S(int idBitacora) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        Double VL = 0.0;

        List<Coment7SDTO> listaComents = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDBITA", idBitacora);

        out = califCheck7S.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAKGCHECKENVIO.SP_SEL_CALIFICA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal VLReturn = (BigDecimal) out.get("PA_TOTAL");

        VL = VLReturn.doubleValue();

        if (error == 1) {
            logger.info("Algo ocurrió");
            VL = -1.0;
        }

        return VL;
    }

    @Override
    public List<Coment7SDTO> ObtieneChecklit(int idBitacora) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<Coment7SDTO> listaComents = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDUSU", null)
                .addValue("PA_IDBITA", idBitacora);

        out = buscaCheck7S.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAKGCHECKENVIO.SP_SEL_BITA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listaComents = (List<Coment7SDTO>) out.get("RCL_USU");

        if (error == 1) {
            logger.info("Algo ocurrió");
        } else {
            return listaComents;
        }

        return null;
    }

}
