package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.PreguntaCheckDTO;

public class PreguntaCheckRowMapper implements RowMapper<PreguntaCheckDTO> {

	public PreguntaCheckDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		PreguntaCheckDTO preguntaDTO = new PreguntaCheckDTO();

		preguntaDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		preguntaDTO.setEstatus(rs.getInt("FIESTATUS"));
		preguntaDTO.setDesc(rs.getString("FCDESCRIPCION"));
		preguntaDTO.setIdModulo(rs.getInt("FIID_MODULO"));
		preguntaDTO.setIdTipo(rs.getInt("FIID_TIPO_PREG"));
		preguntaDTO.setDeta(rs.getString("FCDETALLE"));
		preguntaDTO.setCritica(rs.getString("FICRITICA"));
		preguntaDTO.setSla(rs.getString("FCSLA"));
		preguntaDTO.setTipocheck(rs.getString("FIID_TIPO_CHECK"));
		preguntaDTO.setNombChec(rs.getString("FCNOMBRE"));
		preguntaDTO.setVersion(rs.getInt("FIVERSION"));
		preguntaDTO.setOrdenG(rs.getString("FIORDEN_GRUPO"));
		preguntaDTO.setPond(rs.getString("FCPONDTOTAL"));
		preguntaDTO.setClasific(rs.getString("FCCLASIFICA"));
                preguntaDTO.setResponsable(rs.getString("FCRESPONSABLE"));
                preguntaDTO.setNegocio(rs.getString("FCNEGOCIO"));
		return preguntaDTO;
	}
}



