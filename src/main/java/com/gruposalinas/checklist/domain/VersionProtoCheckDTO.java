package com.gruposalinas.checklist.domain;

public class VersionProtoCheckDTO {
	private String idProtocolo;
	private String idChecklist;
	private String fecha;
	private String vigente;
	public String getIdProtocolo() {
		return idProtocolo;
	}
	public void setIdProtocolo(String idProtocolo) {
		this.idProtocolo = idProtocolo;
	}
	public String getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(String idChecklist) {
		this.idChecklist = idChecklist;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getVigente() {
		return vigente;
	}
	public void setVigente(String vigente) {
		this.vigente = vigente;
	}
	@Override
	public String toString() {
		return "VersionProtoCheckDTO [idProtocolo=" + idProtocolo + ", idChecklist=" + idChecklist + ", fecha=" + fecha
				+ ", vigente=" + vigente + "]";
	}
	
	
}
