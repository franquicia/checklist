/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.domain;

/**
 *
 * @author leodan1991
 */
public class BitacoraMovilDTO {
    private int idBitacora;
    private int idCheckUsua;
    private String fechaInicio;

    public int getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(int idBitacora) {
        this.idBitacora = idBitacora;
    }

    public int getIdCheckUsua() {
        return idCheckUsua;
    }

    public void setIdCheckUsua(int idCheckUsua) {
        this.idCheckUsua = idCheckUsua;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }
    
    
    
    
}
