package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.BitacoraAdministradorDTO;
import com.gruposalinas.checklist.mappers.BitacoraAdministradorRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class BitacoraAdministradorDAOImpl extends DefaultDAO implements BitacoraAdministradorDAO {

    private static Logger logger = LogManager.getLogger(BitacoraAdministradorDAOImpl.class);

    private DefaultJdbcCall jdbcinsertaBitacora;
    private DefaultJdbcCall jdbcactualizaBitacora;
    private DefaultJdbcCall jdbceliminaBitacora;
    private DefaultJdbcCall jdbcbuscaBitacoraCompleto;
    private DefaultJdbcCall jdbcbuscaBitacorasPorCampo;

    public void init() {
        jdbcinsertaBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMBITACORA")
                .withProcedureName("SP_INS_BITACORA");

        jdbcactualizaBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMBITACORA")
                .withProcedureName("SP_ACT_BITACORA_S");

        jdbceliminaBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMBITACORA")
                .withProcedureName("SP_DEL_BITACORA");

        jdbcbuscaBitacoraCompleto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMBITACORA")
                .withProcedureName("SP_SEL_BITACORA")
                .returningResultSet("RCL_BITACORA", new BitacoraAdministradorRowMapper());

        jdbcbuscaBitacorasPorCampo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMBITACORA")
                .withProcedureName("SP_SEL_G_BIT")
                .returningResultSet("RCL_BITACORA", new BitacoraAdministradorRowMapper());

    }

    public int insertaBitacora(BitacoraAdministradorDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        @SuppressWarnings("unused")
        int idBitacora = 0;
        /*if (bean.getIdBitacora() != 0) {
			idBitacora = " " + bean.getIdBitacora();
		}
         */
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_CHECK_USA", bean.getIdCheckUsua())
                .addValue("PA_FIMODO", bean.getModo())
                .addValue("PA_FCID_CECO", bean.getCeco());

        out = jdbcinsertaBitacora.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMBITACORA.SP_INS_CECO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el Checkin");

        } else {
            BigDecimal bita = (BigDecimal) out.get("PA_IDBITACORA");
            idBitacora = bita.intValue();

        }

        return idBitacora;
    }

    public boolean actualizaBitacora(BitacoraAdministradorDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_BITACORA", bean.getIdBitacora())
                .addValue("PA_ID_CHECK_USA", bean.getIdCheckUsua())
                .addValue("PA_FCLONGITUD", bean.getLongitud())
                .addValue("PA_FCLATITUD", bean.getLatitud())
                .addValue("PA_FECHA_TERMINO", bean.getFechaFin())
                .addValue("PA_FIMODO", bean.getModo())
                .addValue("PA_FCID_CECO", bean.getCeco())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcactualizaBitacora.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMBITACORA.SP_ACT_BITACORA_S}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Bitacora");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaBitacora(int idBitacora) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_BITACORA", idBitacora);

        out = jdbceliminaBitacora.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMBITACORA.SP_DEL_BITACORA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al eliminar Bitacora");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<BitacoraAdministradorDTO> buscaBitacorasPorCampo(int idCheckUsua, int idBitacora, String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<BitacoraAdministradorDTO> listBitacora = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FID_CHECK_USUA", idCheckUsua)
                .addValue("PA_IDBITACORA", idBitacora)
                .addValue("PA_FECHA_I", fechaInicio)
                .addValue("PA_FECHA_F", fechaFin);

        out = jdbcbuscaBitacorasPorCampo.execute();

        logger.info("Funcion ejecutada:{checklist.PAADMBITACORA.SP_SEL_G_BIT}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listBitacora = (List<BitacoraAdministradorDTO>) out.get("RCL_BITACORA");

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Bitacora");
            return null;
        } else {
            return listBitacora;
        }
    }

    @SuppressWarnings("unchecked")
    public List<BitacoraAdministradorDTO> buscaBitacoraCompleto(BitacoraAdministradorDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<BitacoraAdministradorDTO> listBitacoras = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FID_CHECK_USUA", bean.getIdCheckUsua())
                .addValue("PA_IDBITACORA", bean.getIdBitacora())
                .addValue("PA_FECHA_I", bean.getFechaInicio())
                .addValue("PA_FECHA_F", bean.getFechaFin());

        out = jdbcbuscaBitacoraCompleto.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMBITACORA.SP_SEL_BITACORA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listBitacoras = (List<BitacoraAdministradorDTO>) out.get("RCL_BITACORA");

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Bitacora");
        } else {
            return listBitacoras;
        }

        return null;
    }

}
