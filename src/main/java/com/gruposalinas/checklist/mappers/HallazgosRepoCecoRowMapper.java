package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteHallazgosDTO;


public class HallazgosRepoCecoRowMapper implements RowMapper<ReporteHallazgosDTO> {

	public ReporteHallazgosDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		

		ReporteHallazgosDTO com = new ReporteHallazgosDTO();

    
	    com.setCeco(rs.getString("FCID_CECO"));
	    com.setNombreCeco(rs.getString("FCNOMBRE"));

		return com;
	}
}



