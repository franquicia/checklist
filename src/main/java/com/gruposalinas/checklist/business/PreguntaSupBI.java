package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.PreguntaSupDAO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.PreguntaSupDTO;

public class PreguntaSupBI {

	private static Logger logger = LogManager.getLogger(PreguntaSupBI.class);
	
	@Autowired
	PreguntaSupDAO preguntaDAO;
	
	List<PreguntaSupDTO> listaPregunta = null;
	
	public int insertaPreguntas(PreguntaSupDTO preguntas){
		int idPregunta = 0;
		try{
			idPregunta = preguntaDAO.insertaPregunta(preguntas);
		}
		catch(Exception e){
			logger.info("No fue posible insertar la Pregunta");
			
		}
		
		return idPregunta;
	}
	
	
	
	public boolean actualizaPregunta(PreguntaSupDTO pregunta){
		boolean respuesta = false;
		try{
			respuesta = preguntaDAO.actualizaPregunta(pregunta);
		}catch(Exception e){
			logger.info("No fue posible actualizar la Pregunta");
			
		}
		
		return respuesta;
	}
	
	public List<PreguntaSupDTO> obtienePregunta(String idPregunta,String idModulo,String tipoPregunta){
		try{
			listaPregunta = preguntaDAO.obtienePregunta(idPregunta, idModulo, tipoPregunta);
		}
		catch(Exception e)
		{
			logger.info("No fue posible obtener datos de la  Pregunta");
			e.printStackTrace();
		}
		return listaPregunta;
	}
	public boolean eliminaPregunta(int idPregunta){
		boolean respuesta = false;
		try{
			respuesta = preguntaDAO.eliminaPregunta(idPregunta);
		}catch(Exception e){
			logger.info("No fue posible eliminar la Pregunta");
			
		}
		
		return respuesta;
	}
}
