$body = $("body");

var tiendasApertura;
var tiendasOperacion;
var tiendasCierre;

var modulosApertura;
var modulosOperacion;
var modulosCierre;

//Reporte Actual que estoy ejecutando (Apertura,Operacion,Cierre)
var actual;
//Select en el que me encuentro (Zona,Region)
var posicionActual;
var init = false;
var regionTxt;
/*VARIABLES SELECT*/
var idNegocioGlobal;
var idCanal;
var idTerritorio;
var idZona;
var idRegion;

var idCheck;
var idGlobal = 0;

/*VARIABLES DIMENSION MATRIZ*/
var switchGlobal = 0;
var modulosLength;
var tiendasLength;

/*BANDERA INTERVAL*/
var interval = false;

var ejemploJson;
var body;

//FECHA ACTUAL
var n;
var now;

var urlImagen;
/*DIALOG*/

//VARIABLE DE ACTUALIZACION DATOS DE REPORTE
var intervaloGenerado;
var opt = {
		title: "checklist",
		// Indica si la ventana se abre de forma automática
        autoOpen: false,
	     // Indica si la ventana es modal
        modal: true,
        // Largo
        width: 300,
        // Alto
        height: 100,
        open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();},
}

function cargaInicio (urlImg, tipo){
	$("#infoContainer").hide();
	$("#popup").hide();
	urlImagen = urlImg;
	startTime();
	fechas();
	$("#fechaInicioVr").val(n);
	now = n;
	body = $("body");
	//SI SE CUMPLE ESTA CONDICION PONGO EL ACTUAL EN 
	if (tipo != "cierre"){
		idCheck="54";
		actual = "apertura";
		if (document.getElementById("apertura") != null)
		document.getElementById("apertura").style.backgroundColor = "rgba(15, 62, 17, 0.99)";
		if (document.getElementById("operacion") != null)
		document.getElementById("operacion").style.backgroundColor = "rgba(1, 96, 13, 0.81)";
		if (document.getElementById("cierre") != null)
		document.getElementById("cierre").style.backgroundColor = "rgba(1, 96, 13, 0.81)";
		$("#tituloTabla").hide(); 
		$("#dialog").dialog(opt);
		regionTxt = $("#seleccionRegion").text(); 
		idRegion = $("#seleccionRegion").val();
		//ES REGIONAL
		if (idRegion > 0){
			idCheck = "54";
			idRegion = $("#seleccionRegion").val();
			$("#mensajeEmergente").text(regionTxt);
			var regionName = $("#seleccionRegion option:selected").text(); 
			$("#txtRegion").text(regionName);
			getTiendas();
			getModulos();
			init = true;
			setApertura();
		}
		
		var canalLong = $("#seleccionCanal option").size();
		var territorioTxt = $("#seleccionTerritorio option:selected").text();
		if (canalLong == 1 && territorioTxt == ""  ){
			//SI SE CUMMPLE ESTA CONDICION TIENE UN CANAL, NO TIENE TERRITORIO, POR LO QUE DEBO LLENAR EL PAIS
			var idCanal = $("#seleccionCanal option:selected").val();
			selectCanal(idCanal);
		}
	}
	else if (tipo == "cierre"){
		$("#tituloTabla").hide();
		//document.getElementById("cierre").style.backgroundColor = "rgba(15, 62, 17, 0.99)";
		setCierre();
	}
}

//FUNCION PARA OBTENER MODULOS
function getModulos() {
	modulosApertura = new Array();
	modulosOperacion = new Array();
	modulosCierre = new Array();
	$.ajax({
		type : "GET",
		url : '../reporteOnlineService/getModulos.json?idCheck='+idCheck,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		beforeSend: function() {    
			body.addClass("loading");
	    },
	    complete: function() {    
	    	body.removeClass("loading");
	    },
		delimiter : ",",
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var i = 0;
			while (json.modulosApertura[i] != null) {
				modulosApertura.push(json.modulosApertura[i]);
				i++;
			}
				i = 0;
			while (json.modulosOperacion[i] != null) {
				modulosOperacion.push(json.modulosOperacion[i]);
				i++;
			}
			i = 0;
			while (json.modulosCierre[i] != null) {
				modulosCierre.push(json.modulosCierre[i]);
				i++;
			}
			
		},
		error : function() {
			alert('Algo paso al cargar los modulos');
			$("#dialog").dialog("close");	   
		},
		async: false
	})
	modulosLength =  modulosApertura.length;
	//tiendasLength = tiendasApertura.length;
};

//FUNCION PARA OBTENER TIENDAS
function getTiendas() {
	tiendasApertura = new Array();
	tiendasOperacion = new Array();
	tiendasCierre = new Array();
	$.ajax({
		type : "GET",
		url : '../reporteOnlineService/getTiendas.json?idCecoRegion='+idRegion,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		beforeSend: function() {    
			body.addClass("loading");
	    },
	    complete: function() {    
	    	body.removeClass("loading");
	    },
		delimiter : ",",
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var i = 0;
			while (json.tiendasApertura[i] != null) {
				tiendasApertura.push(json.tiendasApertura[i]);
				i++;
			}
				i = 0;
			while (json.tiendasOperacion[i] != null) {
				tiendasOperacion.push(json.tiendasOperacion[i]);
				i++;
			}
			i = 0;
			while (json.tiendasCierre[i] != null) {
				tiendasCierre.push(json.tiendasCierre[i]);
				i++;
			}
		},
		error : function() {
			alert('Algo paso al cargar las tiendas');
			$("#dialog").dialog("close");	   
		},
		async: false
})
};


//METODOS PARA PINTAR LA TABLA
function setApertura(){
	switchGlobal = 0;
	idCheck="54";
	actual = "apertura";
	if (document.getElementById("apertura") != null)
		document.getElementById("apertura").style.backgroundColor = "rgba(15, 62, 17, 0.99)";
		if (document.getElementById("operacion") != null)
		document.getElementById("operacion").style.backgroundColor = "rgba(1, 96, 13, 0.81)";
		if (document.getElementById("cierre") != null)
		document.getElementById("cierre").style.backgroundColor = "rgba(1, 96, 13, 0.81)";
	//if (init == true){
		$("#tableBody").empty();
		$("#encabezadoTh").empty();
		setSucursales("apertura");
		setModulos("apertura");
		setRespuestas("apertura",true);
	//}
}

function setOperacion(){
	switchGlobal = 0;
	idCheck="5";
	actual = "operacion";
	if (document.getElementById("apertura") != null)
		document.getElementById("apertura").style.backgroundColor = "rgba(1, 96, 13, 0.81)";
	if (document.getElementById("operacion") != null)
		document.getElementById("operacion").style.backgroundColor = "rgba(15, 62, 17, 0.99)";
	if (document.getElementById("cierre") != null)
		document.getElementById("cierre").style.backgroundColor = "rgba(1, 96, 13, 0.81)";
	
	//if (init == true){
	$("#tableBody").empty();
	$("#encabezadoTh").empty();
	setSucursales("operacion");
	setModulos("operacion");
	setRespuestas("operacion");
	//}
}

function setCierre(){
	switchGlobal = 0;
	idCheck="4";
	actual = "cierre";
	if (document.getElementById("apertura") != null)
		document.getElementById("apertura").style.backgroundColor = "rgba(1, 96, 13, 0.81)";
	if (document.getElementById("operacion") != null)
		document.getElementById("operacion").style.backgroundColor = "rgba(1, 96, 13, 0.81)";
	if (document.getElementById("cierre") != null)
		document.getElementById("cierre").style.backgroundColor = "rgba(15, 62, 17, 0.99)";

	//if (init == true){
	actual = "cierre";
	$("#tableBody").empty();
	$("#encabezadoTh").empty();
	setSucursales("cierre");
	setModulos("cierre");
	setRespuestas("cierre");
	//}
}

function setSucursales(tipo){
	var tienda = "";
	if (tipo == "apertura"){
		for(var i=0;i<tiendasApertura.length;i++){
			tienda += "<tr id="+tiendasApertura[i].idTienda+"></tr>";
		}
		$("#tableBody").append(tienda);
	}
	
	if (tipo == "operacion"){
		for(var i=0;i<tiendasOperacion.length;i++){
			tienda += "<tr id="+tiendasOperacion[i].idTienda+"></tr>";
		}
		$("#tableBody").append(tienda);
	}
	if (tipo == "cierre"){
		/*for(var i=0;i<tiendasCierre.length;i++){
			tienda += "<tr id="+tiendasCierre[i].idTienda+"></tr>";
		}*/
		$("#tableBody").append(tienda);
	}
}
	          
function setModulos(tipo){
	var tienda = "";
	var final = "normal";
	if(tipo == "apertura"){
		for(var i=0;i<modulosApertura.length;i++){
		
			if (i == modulosApertura.length-1)
				final = "thFinal"
			
					if (i == 0){
				tienda += "<th class='"+final+"' id="+modulosApertura[i].idModulo+">&nbsp;&nbsp;Entrada a las 8:30</th>";
			}
			else
			tienda += "<th class='"+final+"' id="+modulosApertura[i].idModulo+">"+modulosApertura[i].modulo+"</th>";

		}
		$("#encabezadoTh").append("<th>Sucursal</th>"+tienda);
	}
	if(tipo == "operacion"){
		for(var i=0;i<modulosOperacion.length;i++){
			tienda += "<th id="+modulosApertura[i].idModulo+">"+modulosApertura[i].modulo+"</th>";
		}
		$("#encabezadoTh").append("<th>Sucursal</th>"+tienda);
	}
	if(tipo == "cierre"){
		/*for(var i=0;i<modulosCierre.length;i++){
			if (i == modulosCierre.length-1)
				final = "thFinal"

				if (i == 0){
					tienda += "<th class='"+final+"' id="+modulosCierre[i].idModulo+">&nbsp;&nbsp;Entrada a las 8:30</th>";
				}
				else
					tienda += "<th class='"+final+"' id="+modulosCierre[i].idModulo+">"+modulosCierre[i].modulo+"</th>";
		}
		$("#encabezadoTh").append("<th>Sucursal</th>"+tienda);*/
	}
}

function setRespuestas(tipo , banderaLoad){
	//(alert("Entro set respuestas");
	if (tipo == "apertura"){
		var tienda = "";
		for(var i=0;i<tiendasApertura.length;i++){
			var idTienda = tiendasApertura[i].idTienda;
			idGlobal = idTienda;
		   	var respuestaService;
		   	
		   	if (banderaLoad)
		   		respuestaService = getRespuestasServiceLoad();
		   	else
		   		respuestaService = getRespuestasService();
		   	
			if (respuestaService  == "sesionExpirada"){
				break;
			}
			var objectsLength = Object.keys(respuestaService).length;
			var respuestas = "";
				respuestas += "<td class='reporte'>"+tiendasApertura[i].tienda+"</td>";

			for (var j=0;j<modulosApertura.length;j++){
				var final="normal";

				//CONSULTAR EL SERVICIOD DE OBTENER RESPUESTAS Y AGREGARLAS AL STRING respuestas como se observa abajo.
				var position = 0;
				if ( j == (modulosApertura.length-1) )
					final = "tdFinal";
				
				if (switchGlobal == 1){
					if (respuestaService[j].length > 1)
						position = 1;
					else
						position = 0;
				}
				
				if (respuestaService[j].length > 0){
					var color = "";
					if (respuestaService[j][position].imagenPrincipal == "circulo_verde.png")
						color = "green";
					if (respuestaService[j][position].imagenPrincipal == "circulo_rojo.png")
						color = "#AE2233";
				var imagenSecundaria = "";
				if (respuestaService[j][position].imagenSecundario == null)
					imagenSecundaria ="";
				else
					imagenSecundaria = "<img id='imgSecundaria' src='../images/reporteOnline/"+respuestaService[j][position].imagenSecundario+"' alt='imagenSecundaria'>";
				
				var imagenReporte = "";
				if (respuestaService[j][position].imagenReporte== null)
					imagenReporte ="";
				else
					imagenReporte = "<img id='img-inf-der' style='width:30px; height:30px;' src='../images/reporteOnline/"+respuestaService[j][position].imagenReporte+"' alt='imagenReporte'>";
				var texto = "";
					if (respuestaService[j][position].textoAbiertaImagen != null)
						texto = "<span style='color:white;'>"+respuestaService[j][position].textoAbiertaImagen+"</span>";
					else if (respuestaService[j][position].observaciones != null)
						texto = "<span style='color:white;'>"+respuestaService[j][position].observaciones+"</span>";

				//alert("FUE DIFERENTE A NULL"+respuestaService[j][0].imagenPrincipal);
				if (j == 0)
					texto = "<div id='text'><span style='color:white;'>"+respuestaService[j][position].textoAbiertaImagen+"</span></div>";

				respuestas += "<td class='reporte' id='"+final+"' ><div class='externo'  onclick='inicio("+tiendasApertura[i].idTienda+","+j+",&apos;"+now+"&apos;,"+54+","+respuestaService[j][position].idModulo+",&apos;"+tiendasApertura[i].tienda+"&apos;,&apos;"+urlImagen+"&apos;"+");'>"+
				" <div class='img-sup-izq'> </div>" +
				" <div class='img-sup-der'></div>	" +
				" <div class='circle-central' style='background:"+color+";'>" + 
				" <div class='img-central'>"+imagenSecundaria+
				"</div>"+texto +"</div>" +
				" <div class='img-inf-izq'></div>" +
				" <div class='img-inf-der'>"+imagenReporte+"</div></div></td>";
				}
				else {
				//respuestas += "<td>"+modulosApertura[j]+" en"+tiendasApertura[i]+"";
				respuestas += "<td class='reporte' id='"+final+"' ><div class='externo'>" +
						" <div class='img-sup-izq'></div>" +
						" <div class='img-sup-der'></div>	" +
						" <div class='circle-central' style='background:#f2f2f2;'>" +
						" <div class='img-central'>" +
						"</div>----</div>" +
						" <div class='img-inf-izq'></div>" +
						" <div class='img-inf-der'></div></div></td>";
				}
			}
			$("#"+idTienda).append(respuestas);
		}
	}
	
	if (tipo == "operacion"){
		var tienda = "";
		for(var i=0;i<tiendasOperacion.length;i++){
			var idTienda = tiendasOperacion[i].idTienda;
			idGlobal = idTienda;
			var respuestaService = getRespuestasService();
			var objectsLength = Object.keys(respuestaService).length;
			var respuestas = "";
			respuestas += "<td class='reporte'>"+tiendasOperacion[i].tienda+"</td>";
			for (var j=0;j<modulosOperacion.length;j++){
				//CONSULTAR EL SERVICIOD DE OBTENER RESPUESTAS Y AGREGARLAS AL STRING respuestas como se observa abajo.
				if (respuestaService[j].length > 0){
					var color = "";
					if (respuestaService[j][0].imagenPrincipal == "circulo_verde.png")
						color = "green";
					if (respuestaService[j][0].imagenPrincipal == "circulo_rojo.png")
						color = "red";
				var imagenSecundaria = "";
				if (respuestaService[j][0].imagenSecundario == null)
					imagenSecundaria ="";
				else
					imagenSecundaria = "<img id='imgSecundaria' src='../images/reporteOnline/"+respuestaService[j][0].imagenSecundario+"' alt='imagenSecundaria'>";
				
				var imagenReporte = "";
				if (respuestaService[j][position].imagenReporte== null)
					imagenReporte ="";
				else
					imagenReporte = "<img id='img-inf-der' style='width:30px; height:30px;' src='../images/reporteOnline/"+respuestaService[j][position].imagenReporte+"' alt='imagenReporte'>";
					
				//alert("FUE DIFERENTE A NULL"+respuestaService[j][0].imagenPrincipal);
				respuestas += "<td class='reporte'><div class='externo'>" +
				" <div class='img-sup-izq'> </div>" +
				" <div class='img-sup-der'></div>	" +
				" <div class='circle-central' style='background:"+color+";'>"+
				" <div class='img-central'>"+imagenSecundaria+
				"</div></div>" +
				" <div class='img-inf-izq'></div>" +
				" <div class='img-inf-der'>"+imagenReporte+"</div></div></td>";
				}
				else {
				//respuestas += "<td>"+modulosApertura[j]+" en"+tiendasApertura[i]+"";
					respuestas += "<td class='reporte'><div class='externo'>" +
					" <div class='img-sup-izq'></div>" +
					" <div class='img-sup-der'></div>	" +
					" <div class='circle-central' style='background:#f2f2f2;'>" +
					" <div class='img-central'>" +
					"</div>----</div>" +
					" <div class='img-inf-izq'></div>" +
					" <div class='img-inf-der'></div></div></td>";
				}
			}
			$("#"+idTienda).append(respuestas);
		}
		}
	
	if (tipo == "cierre"){
		muestraVentana();
		var tienda = "";
		/*
		for(var i=0;i<tiendasCierre.length;i++){
			var idTienda = tiendasCierre[i].idTienda;
			idGlobal = idTienda;
			if (respuestaService  == "sesionExpirada"){
				break;
			}
			
			var respuestaService = getRespuestasService();
			var objectsLength = Object.keys(respuestaService).length;
			var respuestas = "";
			respuestas += "<td class='reporte'>"+tiendasCierre[i].tienda+"</td>";
			//CONSULTAR EL SERVICIOD DE OBTENER RESPUESTAS Y AGREGARLAS AL STRING respuestas como se observa abajo.
			for (var j=0;j<modulosCierre.length;j++){
				//CONSULTAR EL SERVICIOD DE OBTENER RESPUESTAS Y AGREGARLAS AL STRING respuestas como se observa abajo.
				var final="normal";
				var position = 0;
				
				if ( j == (modulosCierre.length-1) )
					final = "tdFinal";				
				
				if (switchGlobal == 1){
					if (respuestaService[j].length > 1)
						position = 1;
					else
						position = 0;
				}
				
				if (respuestaService[j].length > 0){
					var color = "";
					if (respuestaService[j][position].imagenPrincipal == "circulo_verde.png")
						color = "green";
					if (respuestaService[j][position].imagenPrincipal == "circulo_rojo.png")
						color = "red";
				var imagenSecundaria = "";
				if (respuestaService[j][position].imagenSecundario == null)
					imagenSecundaria ="";
				else
					imagenSecundaria = "<img id='imgSecundaria'  src='../images/reporteOnline/"+respuestaService[j][position].imagenSecundario+"' alt='imagenSecundaria'>";
				
				var imagenReporte = "";
				if (respuestaService[j][position].imagenReporte== null)
					imagenReporte ="";
				else
					imagenReporte = "<img id='img-inf-der' style='width:30px; height:30px;' src='../images/reporteOnline/"+respuestaService[j][position].imagenReporte+"' alt='imagenReporte'>";
				var texto = "";
				if (respuestaService[j][position].textoAbiertaImagen != null)
					texto = "<span style='color:white;'>"+respuestaService[j][position].textoAbiertaImagen+"</span>";
				else if (respuestaService[j][position].observaciones != null)
					texto = "<span style='color:white;'>"+respuestaService[j][position].observaciones+"</span>";

				//alert("FUE DIFERENTE A NULL"+respuestaService[j][0].imagenPrincipal);
				respuestas += "<td class='reporte' id='"+final+"'><div class='externo'>" +
				" <div class='img-sup-izq'> </div>" +
				" <div class='img-sup-der'></div>	" +
				" <div class='circle-central' style='background:"+color+";'>"+
				" <div class='img-central'>"+imagenSecundaria+
				"</div>"+texto+"</div>" +				" <div class='img-inf-izq'></div>" +
				" <div class='img-inf-der'>"+imagenReporte+"</div></div></td>";
				}
				else {
				//respuestas += "<td>"+modulosApertura[j]+" en"+tiendasApertura[i]+"";
					respuestas += "<td class='reporte' id='"+final+"'><div class='externo'>" +
					" <div class='img-sup-izq'></div>" +
					" <div class='img-sup-der'></div>	" +
					" <div class='circle-central' style='background:#f2f2f2;'>" +
					" <div class='img-central'>" +
					"</div>----</div>" +
					" <div class='img-inf-izq'></div>" +
					" <div class='img-inf-der'></div></div></td>";
				}
			}
			$("#"+idTienda).append(respuestas);
		}*/
	}
	
		switchGlobal++;
		if (interval == false)
		 update (true , "general"); 
}

//CONSUMO DEL SERVICIO PARA OBTENER LAS RESPUESTAS
function getRespuestasService(){
	$.ajax({
		type : "GET",
		url : '../consultaChecklistService/getRespuestasReporte.json?idChecklist='+idCheck+'&idCeco='+idGlobal+'&fecha='+now,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		/*beforeSend: function() {    
			body.addClass("loading");
	    },
	    complete: function() {    
	    	body.removeClass("loading");
	    },*/
		success : function(json) {
			//Ocupo esta funcion para poder retornar el valor del servicio
			jsonG = json;
			var jsonC = JSON.stringify(json);
			//alert (jsonC);
		},
	    error: function(jqXHR, textStatus, errorThrown){
	    	 	
	    	if (jqXHR.status === 0) {

	    		    alert('¡TU SESIÓN HA EXPIRADO!. \n ACTUALIZA EL NAVEGADOR PARA CONTINUAR.');
	    		    jsonG = "sesionExpirada";

	    		  } else if (jqXHR.status == 404) {

	    		    alert('Requested page not found [404]');

	    		  } else if (jqXHR.status == 500) {

	    		    alert('InternalServerErr. [500].');

	    		  } else if (textStatus === 'parsererror') {

	    		    alert('Requested JSON parse failed.');

	    		  } else if (textStatus === 'timeout') {

	    		    alert('Time out Er.');

	    		  } else if (textStatus === 'abort') {

	    		    alert('Ajax request aborted.');

	    		  } else {

	    		    alert('Uncaught Er: ' + jqXHR.responseText);

	    		  }
	    },
		async: false		
	})
	return jsonG;
}

function getRespuestasServiceLoad(){
	$.ajax({
		type : "GET",
		url : '../consultaChecklistService/getRespuestasReporte.json?idChecklist='+idCheck+'&idCeco='+idGlobal+'&fecha='+now,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		beforeSend: function() {    
			body.addClass("loading");
	    },
	    complete: function() {    
	    	body.removeClass("loading");
	    },
		success : function(json) {
			//Ocupo esta funcion para poder retornar el valor del servicio
			jsonG = json;
			var jsonC = JSON.stringify(json);
			//alert (jsonC);
		},
	    error: function(jqXHR, textStatus, errorThrown){
	    	 	
	    	if (jqXHR.status === 0) {

	    		    alert('¡TU SESIÓN HA EXPIRADO!. \n ACTUALIZA EL NAVEGADOR PARA CONTINUAR.');
	    		    jsonG = "sesionExpirada";

	    		  } else if (jqXHR.status == 404) {

	    		    alert('Requested page not found [404]');

	    		  } else if (jqXHR.status == 500) {

	    		    alert('InternalServerEr [500].');

	    		  } else if (textStatus === 'parsererror') {

	    		    alert('Requested JSON parse failed.');

	    		  } else if (textStatus === 'timeout') {

	    		    alert('Time out Er.');

	    		  } else if (textStatus === 'abort') {

	    		    alert('Ajax request aborted.');

	    		  } else {

	    		    alert('Uncaught Er: ' + jqXHR.responseText);

	    		  }
	    },
		async: false		
	})
	return jsonG;
}

//EVENTOS COMBO
function selectCanal (idNegocio){
	$("#infoContainer").hide();
	idNegocioGlobal = idNegocio;
	$("#seleccionPais").empty();
	$.ajax({
		type : "GET",
		url : '../consultaCatalogosService/getPaisesCombo.json?idNegocio='+idNegocio,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		beforeSend: function() {    
			body.addClass("loading");
	    },
	    complete: function() {    
	    	body.removeClass("loading");;
	    },
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var objectsLength = Object.keys(json).length;
			$('#seleccionPais').append($("<option></option>").attr("value","0").text("--Seleccione--")); 
			for (var i = 0 ; i<=objectsLength-1; i++){
				$('#seleccionPais').append($("<option></option>").attr("value",json[i].idPais).text(json[i].nombre)); 
			}
		},
		error : function() {
			alert('Algo paso');
		},
		//async: false
})
};
function selectPais(idPais){
	$("#infoContainer").hide();
	$("#seleccionTerritorio").empty();
	$.ajax({
		type : "GET",
		url : '../consultaCatalogosService/getTerritoriosCombo.json?idNegocio='+idNegocioGlobal+'&idPais='+idPais,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		beforeSend: function() {    
			body.addClass("loading");
	    },
	    complete: function() {    
	    	body.removeClass("loading");;
	    },
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var objectsLength = Object.keys(json).length;
			$('#seleccionTerritorio').append($("<option></option>").attr("value","0").text("--Seleccione--")); 
			for (var i = 0 ; i<=objectsLength-1; i++){
				$('#seleccionTerritorio').append($("<option></option>").attr("value",json[i].idCeco).text(json[i].nombreCeco)); 
			}
		},
		error : function() {
			alert('Algo paso');
		},
		//async: false
})
};

function selectTerritorio(idCeco){
	$("#seleccionZona").empty();
	$("#infoContainer").hide();
	$.ajax({
		type : "GET",
		url : '../consultaCatalogosService/getCecosCombo.json?idCeco='+idCeco+'&tipoBusqueda=1',
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		beforeSend: function() {    
			body.addClass("loading");
	    },
	    complete: function() {    
	    	body.removeClass("loading");;
	    },
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var objectsLength = Object.keys(json).length;
			$('#seleccionZona').append($("<option></option>").attr("value","0").text("--Seleccione--")); 
			for (var i = 0 ; i<=objectsLength-1 ; i++){
				$('#seleccionZona').append($("<option></option>").attr("value",json[i].idCeco).text(json[i].nombreCeco)); 
			}
		},
		error : function() {
			alert('Algo paso');
		},
		//async: false
})
};


function selectZona(idCeco){
	posicionActual = "zona";
	var zonaName = $("#seleccionZona option:selected").text(); 
	$("#mensajeEmergente").text(zonaName);
	$("#txtRegion").text(zonaName);
	$("#seleccionRegion").empty();
	$.ajax({
		type : "GET",
		url : '../consultaCatalogosService/getCecosCombo.json?idCeco='+idCeco+'&tipoBusqueda=1',
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		beforeSend: function() {    
			body.addClass("loading");
	    },
	    complete: function() {    
	    	body.removeClass("loading");
	    },
		delimiter : ",",
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var objectsLength = Object.keys(json).length;
			$('#seleccionRegion').append($("<option></option>").attr("value","0").text("--Seleccione--")); 
			for (var i = 0 ; i<=objectsLength-1; i++){
				$('#seleccionRegion').append($("<option></option>").attr("value",json[i].idCeco).text(json[i].nombreCeco)); 
			}
			getZonal(idCeco);
		},
		error : function() {
			alert('Algo paso');
		},
		//async: false
})
};


function selectRegion(regionTxt){
	$("#infoContainer").hide();
	posicionActual = "region";
	update(false);
	interval = false;
	
	$("#tablaReporteContainer").empty();
	if ($("#seleccionRegion").val() != 0)
		idRegion = $("#seleccionRegion").val();
	$("#tituloTabla").show();
	var flag = false;

	if (actual == "apertura"){
	//APERTURA
	body.addClass("loading");
	$("#tableBody").empty();
	$("#encabezadoTh").empty();
	getTiendas();
	setModulos("apertura");
	setSucursales("apertura");
	setRespuestas("apertura" , true);
	setApertura();
	}
	
	if(actual == "operacion"){
	body.addClass("loading");
	//OPERACION
	$("#tableBody").empty();
	$("#encabezadoTh").empty();
	getTiendas();
	setModulos("operacion");
	setRespuestas("operacion" , true);
	setOperacion();
	}
	
	if (actual == "cierre"){
	//CIERRE
	actual = "cierre";
	$("#tableBody").empty();
	$("#encabezadoTh").empty();
	getTiendas();
	setModulos("cierre");
	setRespuestas("cierre" , true);
	setCierre();
	}

	$("#txtRegion").text("REGIÓN: "+regionTxt);
	init = false;
	//LLAMAR AL SERVICIO DE OBTENER RESPUESTAS
	
	
}

//FUNCION PARA ACTUALIZAR EL CONTENIDO EN INTERVALOD DE TIEMPO
function update (bandera , tipo) {
	interval = true;
	if (bandera && tipo == "general" ){
		intervaloGenerado =  setInterval(function() {
	    	if (switchGlobal == 2)
	    		switchGlobal = 0;
		        $("td.reporte").remove();
		        setRespuestas(actual,false);
	    }, 10000);
	}
	
	if (bandera && tipo == "zonal" ){
		intervaloGenerado =  setInterval(function() {
	    	if (switchGlobal == 2)
	    		switchGlobal = 0;
		        $("td.reporte").remove();
		        setRespuestasRegion(actual,false);
	    }, 10000);
	}
		
	if (!bandera){
		clearInterval(intervaloGenerado);
	}
}



//FUNCION PARA EL FILTRADO DE TIENDAS
function filtro() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("getText");
  filter = input.value.toUpperCase();
  table = document.getElementById("tablaReporteOnline");
  tr = table.getElementsByTagName("tr");

  for (i = 0; i < tr.length; i++) {
	  //DONDE QUIERO BUSCAR
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}
//FUNCION FECHA Y HORA
function startTime(){
	today=new Date();
	h=today.getHours();
	m=today.getMinutes();
	s=today.getSeconds();
	m=checkTime(m);
	s=checkTime(s);
	
	var dd = today.getDate();
	var mm = today.getMonth()+1;

	var yyyy = today.getFullYear();
	if(dd<10){
	    dd='0'+dd;
	} 
	if(mm<10){
	    mm='0'+mm;
	} 
	n = dd+'/'+mm+'/'+yyyy;
	
	document.getElementById('reloj').innerHTML="Fecha:	"+n+"<br> Hora:	"+h+":"+m+":"+s;
	t=setTimeout('startTime()',500);
}
	
function checkTime(i){
		if (i<10) {i="0" + i;}return i;
	}

/*FECHAS*/
function fechas() {
	$( "#fechaInicioVr" ).datepicker({
    	minDate: "1/1/2000",    	
        maxDate: new Date(),
    });
}

function cambiaFecha(){
	//zona,region
	var fecha= document.getElementById("fechaInicioVr").value;
	idRegion = $("#seleccionRegion").val();
	idZona = $("#seleccionZona").val();
	now = fecha;
	
	if (posicionActual == "region" && idRegion > 0){
		idRegion = $("#seleccionRegion").val();
		$("#mensajeEmergente").text(regionTxt);
		var regionName = $("#seleccionRegion option:selected").text(); 
		$("#txtRegion").text(regionName);
		setApertura();
	}
	else if (posicionActual == "zona" && idZona > 0){
		idRegion = $("#seleccionZona").val();
		var zonaName = $("#seleccionZona option:selected").text(); 
		$("#txtRegion").text(zonaName);
		setAperturaRegion();
		//setApertura();
	}
	$("#fechaInicioVr").val(now);

}

/*MODAL*/
	function muestraVentana(msj){
		document.getElementById("dialog").innerHTML = "En construcción";
		$( "#dialog" ).dialog({
			title : "checklist",
			width       :   300,
            height      :   150,
            modal : true,
			  dialogClass: "no-close",
			  buttons: [
			    {
			      text: "Aceptar",
			      click: function() {
			        $( this ).dialog( "close" );
			      }
			    }
			  ],
			  resizable: true,
			  draggable: true
			});
		
	      $( "#dialog" ).dialog( "open" );
	    
	}

	function getDetalle(idTienda,iArray,fecha,idCheck,idModulo,tienda){
		var method = "POST"; // Set method to post by default if not specified.
			// The rest of this code assumes you are not using a library.
		    // It can be made less wordy if you use one.
		    var form = document.createElement("form");
		    form.setAttribute("method", method);
		    form.setAttribute("action", "detalleReporteOnline.htm");
		    
		    var hiddenField = document.createElement("input");
		    hiddenField.setAttribute("type", "hidden");
		    hiddenField.setAttribute("name", "idTienda");
		    hiddenField.setAttribute("value", idTienda);
		    form.appendChild(hiddenField);
		    
		    var hiddenField1 = document.createElement("input");
		    hiddenField1.setAttribute("type", "hidden");
		    hiddenField1.setAttribute("name", "iArray");
		    hiddenField1.setAttribute("value", iArray);
		    form.appendChild(hiddenField1);
		    
		    var hiddenField2 = document.createElement("input");
		    hiddenField2.setAttribute("type", "hidden");
		    hiddenField2.setAttribute("name", "fecha");
		    hiddenField2.setAttribute("value", fecha);
		    form.appendChild(hiddenField2);
		    
		    var hiddenField3 = document.createElement("input");
		    hiddenField3.setAttribute("type", "hidden");
		    hiddenField3.setAttribute("name", "idCheck");
		    hiddenField3.setAttribute("value", idCheck);
		    form.appendChild(hiddenField3);
		    
		    var hiddenField4 = document.createElement("input");
		    hiddenField4.setAttribute("type", "hidden");
		    hiddenField4.setAttribute("name", "idModulo");
		    hiddenField4.setAttribute("value", idModulo);
		    form.appendChild(hiddenField4);
		    
		    var hiddenField5 = document.createElement("input");
		    hiddenField5.setAttribute("type", "hidden");
		    hiddenField5.setAttribute("name", "tienda");
		    hiddenField5.setAttribute("value", tienda);
		    form.appendChild(hiddenField5);
		    
		    document.body.appendChild(form);
		    form.submit();
}

/*MODAL MODAL*/	
	function mostrarVentana()
	{
	    var ventana = document.getElementById("miVentana");
	    ventana.style.marginTop = "100px";
	    ventana.style.left = ((document.body.clientWidth-350) / 2) +  "2px";
	    ventana.style.display = "block";
	}	
/*SCRIPT DEL LA OTRA CLASE!!!! ________ --***************/	

	/***************Funciones para salir de la ventana emergente******************/
	function amIclicked(e, element){
	    e = e || event;
	    var target = e.target || e.srcElement;
	    if(target.id==element.id)
	        return true;
	    else
	        return false;
	}

	function oneClick(event, element){
	    if(amIclicked(event, element)){
    		update (true,"general");
			$("#popup").hide();
			$('body').css('overflow', 'scroll');

	    }
	}
	/***************Funciones para salir de la ventana emergente******************/


/*NUEVO REPORTE ZONAL Y REGIONAL*/
	var tiendasRegionApertura;

	function getZonal(idCeco){
		if (interval){
		update(false);
		interval = false;
		}
		
		if (actual == "apertura"){
			//APERTURA
			body.addClass("loading");
			$("#tableBody").empty();
			$("#encabezadoTh").empty();
			getTiendasRegiones(idCeco);
			getModulos();
			setModulos("apertura");
			setTiendasRegiones("apertura");
			setRespuestasRegion("apertura");
			update(true , "zonal");
			}
	}

	function getTiendasRegiones(idCeco){
			tiendasRegionApertura = new Array();
			$("#infoContainer").show();

			$.ajax({
				type : "GET",
				url : '../reporteOnlineService/getTiendasRegiones.json?idCeco='+idCeco+'&tBusqueda=0',
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				paramName : "tagName",
				beforeSend: function() {    
					body.addClass("loading");
			    },
			    complete: function() {    
			    	body.removeClass("loading");
			    },
				delimiter : ",",
				success : function(json) {
					var jsonC = JSON.stringify(json);
					var i = 0;
					while (json[i] != null) {
						tiendasRegionApertura.push(json[i]);
						i++;
					}
				},
				error : function() {
					alert('Algo paso al cargar las tiendas');
					$("#dialog").dialog("close");	   
				},
				async: false
		})

	}
	
	function setTiendasRegiones(tipo){
		var tienda = "";
		if (tipo == "apertura"){
			for(var i=0;i<tiendasRegionApertura.length;i++){
				tienda += "<tr id="+tiendasRegionApertura[i].ceco+"></tr>";
			}
			$("#tableBody").append(tienda);
		}
	}
	
	function setRespuestasRegion(tipo){
		if (tipo == "apertura"){
			var tienda = "";
			var reporte = "";
			var l  = 0;
			var imagenReporte = "";
			for(var i=0;i<tiendasRegionApertura.length;i++){
				var idTienda = tiendasRegionApertura[i].ceco;
				//idGlobal = idTienda;
			   	var respuestaService;
			   	respuestaService = getRespuestasRegionService(idTienda);
			   	//alert (JSON.stringify(respuestaService));
			   	var respuestas = "";
			   	var n = tiendasRegionApertura[i].nombreCeco;
				respuestas += "<td class='reporte' onClick='clickZona("+tiendasRegionApertura[i].ceco+",&apos;"+n+"&apos;);' style='cursor:pointer;'>"+tiendasRegionApertura[i].nombreCeco+"<br>("+tiendasRegionApertura[i].numTiendas+" SUC)</td>";

				for (var j = 0;j<respuestaService.modulo.length;j++){
					reporte = "";
					imagenReporte = "";
					for (var k = 0; k < modulosApertura.length; k++ ){
				 			if (modulosApertura[j].idModulo == respuestaService.reportes[k].idModulo )
				 				if (respuestaService.reportes[k].conteo > 0)
								imagenReporte = "<img id='img-inf-der' style='width:30px; height:30px;' src='../images/reporteOnline/con_reporte.png' alt='imagenReporte'>"+respuestaService.reportes[k].conteo+"";
				 	}	
				 	
					
					var final="normal";
				   	//CONSULTAR EL SERVICIOD DE OBTENER RESPUESTAS Y AGREGARLAS AL STRING respuestas como se observa abajo.
					var position = 0;
					if ( j == (modulosApertura.length-1) )
						final = "tdFinal";
					
					/*if (switchGlobal == 1){
						if (respuestaService.modulo[j].length > 1)
							position = 1;
						else
							position = 0;
					}
					*/
					
					var texto = "";
					if (respuestaService.modulo[j].totalTiendas > 0)
						texto = "<span style='color:white;'>"+respuestaService.modulo[j].totalTiendas+"</span>";
					else if (respuestaService.modulo[j].observaciones != null)
						texto = "<span style='color:white;'>"+respuestaService.modulo[j].totalTiendas+"</span>";
					
					var imagenSecundaria = "";
					
					if (respuestaService.modulo.length > 0){
						var color = "#f2f2f2";
						if (respuestaService.modulo[j].imagen == "verde")
							color = "green";
						else if (respuestaService.modulo[j].imagen == "rojo")
							color = "#AE2233";
						else if (respuestaService.modulo[j].imagen == "amarillo")
							color = "rgba(227, 227, 0, 0.84)";
						else if (respuestaService.modulo[j].imagen == "palomita"){
							imagenSecundaria =  "<img id='img-central' style='width:110%; height:110%;' src='../images/reporteOnline/Paloma.png' alt='imagenPalomita'>";
							texto = "";
						}
				
					//SI TIENE O NO REPORTE	
					//var imagenReporte = "";
					//if (respuestaService.modulo[j].imagen == null)
						//imagenReporte ="";
					//else
						//imagenReporte = "<img id='img-inf-der' style='width:30px; height:30px;' src='#' alt='imagenReporte'>";
						
					

					respuestas += "<td class='reporte' id='"+final+"' ><div class='externo'>"+
					" <div class='img-sup-izq'> </div>" +
					" <div class='img-sup-der'></div>	" +
					" <div class='circle-central' style='background:"+color+";'>" + 
					" <div class='img-central'>"+imagenSecundaria+
					"</div>"+texto +"</div>" +
					" <div class='img-inf-izq'></div>" +
					" <div class='img-inf-der'>"+imagenReporte+"</div></div></td>";
					}
					else {
					//respuestas += "<td>"+modulosApertura[j]+" en"+tiendasApertura[i]+"";
					respuestas += "<td class='reporte' id='"+final+"' ><div class='externo'>" +
							" <div class='img-sup-izq'></div>" +
							" <div class='img-sup-der'></div>	" +
							" <div class='circle-central' style='background:#f2f2f2;'>" +
							" <div class='img-central'>" +
							"</div>----</div>" +
							" <div class='img-inf-izq'></div>" +
							" <div class='img-inf-der'></div></div></td>";
					}
	
					}
				$("#"+idTienda).append(respuestas);
			}
		}
		
	}
	
	function getRespuestasRegionService(idTienda){
		var jsonG;
		$.ajax({
			type : "GET",
			url : '../reporteOnlineService/getConteoRegional.json?idCeco='+idTienda+'&idCheck=54&fecha='+now,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			paramName : "tagName",
			delimiter : ",",
			beforeSend: function() {    
				body.addClass("loading");
		    },
		    complete: function() {    
		    	body.removeClass("loading");
		    },
			success : function(json) {
				//Ocupo esta funcion para poder retornar el valor del servicio
				jsonG = json;
				var jsonC = JSON.stringify(json);
			},
		    error: function(jqXHR, textStatus, errorThrown){
		    	 	
		    	if (jqXHR.status === 0) {

		    		    alert('¡TU SESIÓN HA EXPIRADO!. \n ACTUALIZA EL NAVEGADOR PARA CONTINUAR.');
		    		    jsonG = "sesionExpirada";

		    		  } else if (jqXHR.status == 404) {

		    		    alert('Requested page not found [404]');

		    		  } else if (jqXHR.status == 500) {

		    		    alert('InternalServerEr [500].');

		    		  } else if (textStatus === 'parsererror') {

		    		    alert('Requested JSON parse failed.');

		    		  } else if (textStatus === 'timeout') {

		    		    alert('Time out Er.');

		    		  } else if (textStatus === 'abort') {

		    		    alert('Ajax request aborted.');

		    		  } else {

		    		    alert('Uncaught Er: ' + jqXHR.responseText);

		    		  }
		    },
			async: false		
		})
		return jsonG;
	}

	//METODOS PARA PINTAR LA TABLA 
	function setAperturaRegion(){
		switchGlobal = 0;
		idCheck="54";
		actual = "apertura";
		if (document.getElementById("apertura") != null)
			document.getElementById("apertura").style.backgroundColor = "rgba(15, 62, 17, 0.99)";
		if (document.getElementById("operacion") != null)
			document.getElementById("operacion").style.backgroundColor = "rgba(1, 96, 13, 0.81)";
		if (document.getElementById("cierre") != null)
			document.getElementById("cierre").style.backgroundColor = "rgba(1, 96, 13, 0.81)";
		//if (init == true){
			$("#tableBody").empty();
			$("#encabezadoTh").empty();
			setModulos("apertura");
			setTiendasRegiones("apertura");
			setRespuestasRegion("apertura",true);
		//}
	}
	
	function clickZona(ceco, nombre){
		idRegion = ceco;
		selectRegion(nombre);
		$('#seleccionRegion option[value='+ceco+']').prop('selected', true);
	}
	
	