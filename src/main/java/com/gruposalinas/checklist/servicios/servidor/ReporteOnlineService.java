package com.gruposalinas.checklist.servicios.servidor;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.checklist.business.FiltrosCecoBI;
import com.gruposalinas.checklist.business.ReporteImagenesBI;
import com.gruposalinas.checklist.domain.FiltrosCecoDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.NumeroTiendasDTO;
import com.gruposalinas.checklist.resources.FRQAuthInterceptor;

@Controller
@RequestMapping("/reporteOnlineService")
public class ReporteOnlineService {
	
	@Autowired 
	FiltrosCecoBI filtrosCecobi;
	
	@Autowired
	ReporteImagenesBI reporteImagenesbi;
	
	private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);

	// http://localhost:8080/checklist/central/reporteOnlineService/getTiendas.json
	@RequestMapping(value = "/getTiendas", method = RequestMethod.GET)
	public @ResponseBody String getTiendas(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String idCeco = request.getParameter("idCecoRegion");
		JsonObject data = new JsonObject();

		try {
			
			List <FiltrosCecoDTO> tiendas =  filtrosCecobi.obtieneCecos(Integer.parseInt(idCeco) , 1);
			
			List<FiltrosCecoDTO> tiendasApertura = tiendas;
			Iterator<FiltrosCecoDTO> it = tiendasApertura.iterator();
			JsonArray arrayTiendasApertura = new JsonArray();
			while (it.hasNext()) {
				FiltrosCecoDTO o = it.next();
				JsonObject tienda = new JsonObject();
				tienda.addProperty("tienda", o.getNombreCeco());
				tienda.addProperty("idTienda", o.getIdCeco());
				arrayTiendasApertura.add(tienda);
			}
			data.add("tiendasApertura", arrayTiendasApertura);
			
			List<FiltrosCecoDTO> tiendasOperacion = tiendas;
			Iterator<FiltrosCecoDTO> it2 = tiendasOperacion.iterator();
			JsonArray arrayTiendasOperacion = new JsonArray();
			while (it2.hasNext()) {
				FiltrosCecoDTO o = it2.next();
				JsonObject tienda = new JsonObject();
				tienda.addProperty("tienda", o.getNombreCeco());
				tienda.addProperty("idTienda", o.getIdCeco());
				arrayTiendasOperacion.add(tienda);
			}
			data.add("tiendasOperacion", arrayTiendasOperacion);
			
			List<FiltrosCecoDTO> tiendasCierre = tiendas;
			Iterator<FiltrosCecoDTO> it1 = tiendasCierre.iterator();
			JsonArray arrayTiendasCierre = new JsonArray();
			while (it1.hasNext()) {
				FiltrosCecoDTO o = it1.next();
				JsonObject tienda = new JsonObject();
				tienda.addProperty("tienda", o.getNombreCeco());
				tienda.addProperty("idTienda", o.getIdCeco());
				arrayTiendasCierre.add(tienda);
			}
			data.add("tiendasCierre", arrayTiendasApertura);

			
		} catch (Exception e) {
			return null;
		}
		logger.info(data.toString());
		return data.toString();
	}

	// http://localhost:8080/checklist/central/reporteOnlineService/getModulos.json
	@RequestMapping(value = "/getModulos", method = RequestMethod.GET)
	public @ResponseBody String getModulos(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String idCheck = request.getParameter("idCheck");
		
		JsonObject data = new JsonObject();
		Map <String,Object> modulos =  reporteImagenesbi.obtieneModulosTiendas("0", idCheck);
		@SuppressWarnings("unchecked")
		List<ModuloDTO> listaModulos = (List<ModuloDTO>) modulos.get("modulos");
		
		try {
		List<ModuloDTO> modulosApertura = listaModulos;
		Iterator<ModuloDTO> it = modulosApertura.iterator();
		JsonArray arrayModulosApertura = new JsonArray();
		while (it.hasNext()) {
			ModuloDTO o = it.next();
			JsonObject modulo = new JsonObject();
			modulo.addProperty("modulo", o.getNombre());
			modulo.addProperty("idModulo", o.getIdModulo());
			arrayModulosApertura.add(modulo);
		}
		data.add("modulosApertura", arrayModulosApertura);
		
		List<ModuloDTO> modulosOperacion= listaModulos;
		Iterator<ModuloDTO> it1 = modulosOperacion.iterator();
		JsonArray arrayModulosOperacion = new JsonArray();
		while (it1.hasNext()) {
			ModuloDTO o = it1.next();
			JsonObject modulo = new JsonObject();
			modulo.addProperty("modulo", o.getNombre());
			modulo.addProperty("idModulo", o.getIdModulo());
			arrayModulosOperacion.add(modulo);
		}
		data.add("modulosOperacion", arrayModulosOperacion);
		
		List<ModuloDTO> modulosCierre= listaModulos;
		Iterator<ModuloDTO> it2 = modulosCierre.iterator();
		JsonArray arrayModulosCierre= new JsonArray();
		while (it2.hasNext()) {
			ModuloDTO o = it2.next();
			JsonObject modulo = new JsonObject();
			modulo.addProperty("modulo", o.getNombre());
			modulo.addProperty("idModulo", o.getIdModulo());
			arrayModulosCierre.add(modulo);
		}
		data.add("modulosCierre", arrayModulosCierre);

		
		}catch(Exception e){
			return null;
		} 

		return data.toString();
	}
	
	// http://localhost:8080/checklist/reporteOnlineService/getConteoRegional.json?idCeco=236108&idCheck=54&fecha=17/04/2017
	@RequestMapping(value="/getConteoRegional", method = RequestMethod.GET)
    public @ResponseBody String getConteoRegional(HttpServletRequest request, HttpServletResponse response, Model model){ 
		String idCeco = request.getParameter("idCeco");
		String idCheck = request.getParameter("idCheck");
		String fecha = request.getParameter("fecha");
		
		//System.out.println(idCheck);
		int i = Integer.parseInt(idCheck);
		//System.out.println(i);
		
		String respuesta = "";
		respuesta = reporteImagenesbi.obtieneRespuestasRegion(idCeco, Integer.parseInt(idCheck) ,fecha);
		//respuesta = reporteImagenesbi.obtieneRespuestasRegion(idCeco, Integer.parseInt(idCheck) ,fecha);

		return respuesta;
    }
	
	
	// http://localhost:8080/checklist/central/reporteOnlineService/getTiendasRegiones.json?idCeco=<?>&tBusqueda=<?>
	@RequestMapping(value="/getTiendasRegiones", method = RequestMethod.GET)
    public @ResponseBody List<NumeroTiendasDTO> getTiendasRegiones(HttpServletRequest request, HttpServletResponse response, Model model){ 
		String idCeco = request.getParameter("idCeco");
		String tBusqueda = request.getParameter("tBusqueda");
	
		List<NumeroTiendasDTO> respuesta = reporteImagenesbi.obtieneNumeroTiendas( idCeco, Integer.parseInt(tBusqueda));
		//respuesta = reporteImagenesbi.obtieneRespuestasRegion(idCeco, Integer.parseInt(idCheck) ,fecha);

		return respuesta;
    }
	

}