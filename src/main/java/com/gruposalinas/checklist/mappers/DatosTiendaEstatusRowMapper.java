package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.TipoChecklistDTO;

public class DatosTiendaEstatusRowMapper implements RowMapper<ChecklistDTO>{

	@Override
	public ChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ChecklistDTO checklistDTO = new ChecklistDTO();
		TipoChecklistDTO tipochecklist = new TipoChecklistDTO(); 
		
		tipochecklist.setIdTipoCheck(rs.getInt("FIID_TIPO_CHECK"));
		tipochecklist.setDescTipo(rs.getString("FCDESCRIPCION"));
		checklistDTO.setIdTipoChecklist(tipochecklist);
		checklistDTO.setIdCheckUsua(rs.getInt("FIID_CHECK_USUA"));
		checklistDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		checklistDTO.setNombreCheck(rs.getString("FCNOMBRE"));
		checklistDTO.setZona(rs.getString("FCZONA"));
		checklistDTO.setFecha_inicio(rs.getString("FDFECHA_INICIO"));
		checklistDTO.setFecha_fin(rs.getString("FDFECHA_FIN"));
		checklistDTO.setIdCeco(rs.getString("FCID_CECO"));
		checklistDTO.setNombresuc(rs.getString("FCNOMBRECC"));
		checklistDTO.setLongitud(rs.getDouble("FCLONGITUD"));
		checklistDTO.setLatitud(rs.getDouble("FCLATITUD"));
		checklistDTO.setUltimaVisita(rs.getString("ULTVISITA"));
		checklistDTO.setEstatus(rs.getInt("ESTATUS"));
		checklistDTO.setDistancia(rs.getDouble("DISTANCIA"));
		
		return checklistDTO;
	}
	

}
