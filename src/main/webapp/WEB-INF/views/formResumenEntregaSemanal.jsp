<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">

<head>

<script src="../js/expancion/jquery-3.3.1.js"></script>
<script src="../js/expancion/jquery-ui-1.12.1.js"></script>
<script src="../js/expancion/busquedaSucursal.js" type="text/javascript"></script>
<script src="../js/expancion/jquery.dataTables.js"></script>
<script src="../js/expancion/tabla.js"></script>



<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />

<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/secciones.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/calendarioCaptacion.css"
	media="screen">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/estiloVentana.css" media="screen">


<link rel="stylesheet" type="text/css"
	href="../css/expancion/tabla-filtros.css">


<!-- -->
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/secciones.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/calendarioCaptacion.css"
	media="screen">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/estiloVentana.css" media="screen">

<link rel="stylesheet" type="text/css" href="../css/expancion/modal.css">
<link rel="stylesheet" type="text/css" href="../css/expancion/index.css">

<!-- Owl Stylesheets -->
<link rel="stylesheet" href="../css/expancion/owl.carousel.css">
<link rel="stylesheet" href="../css/expancion/owl.theme.default.css">

<!-- DropkickDos -->
<link rel="stylesheet" type="text/css"
	href="../css/expancion/dropkickDos.css">
<link rel="stylesheet"
	href="../css/expancion/jquery-ui-datepicker.min.css">



</head>

<body>

	<div class="header h90">
		<span class="titulagsBold spBold fz146">RESUMEN ENTREGA SEMANAL</span> <span
			class="subSeccion tp30">Selecciona los criterios de búsqueda</span> <a
			href="" class="msucursales"> <span>MIS SUCURSALES</span>
		</a>
	</div>
	<div class="page">
		<div class="contHome top60">
			<div class="conInfo">

				<img src="../images/expancion/docsearch.svg" class="imgsearch">


				<div class="clear"></div>

				<div class="cn-v">
					

					<div class="w15c">

						<c:if test="${busqueda == 0 || fechaInicio == ''}">

							<input type="text" class="fecal vml" id="datepicker"
								value="FECHA INICIO">

						</c:if>


						<c:if test="${busqueda != 0 && fechaInicio != ''}">

							<input type="text" class="fecal vml" id="datepicker"
								value="${fechaInicioParam}">

						</c:if>
					</div>

					<div class="w1a">
						<span class="spBold">a</span>
					</div>

					<div class="w15c">

						<c:if test="${busqueda == 0 || fechaFin == ''}">

							<input type="text" class="fecal vml" id="datepickerDos"
								value="FECHA TERMINO">

						</c:if>


						<c:if test="${busqueda != 0 && fechaFin != ''}">

							<input type="text" class="fecal vml" id="datepickerDos"
								value="${fechaFinParam}">

						</c:if>

					</div>
					
					


				</div>
			</div>
			<br></br>
			<div class="w15c">
					<div>
						<!-- <a id="btnBuscar" onclick="obtieneReporte();" class="btn busc ml20"> Resumen Entrega Semanal V1.0</a> -->
						<a id="btnBuscar2" onclick="obtieneReporte2();" class="btn busc ml20"> Resumen Entrega Semanal</a>
						<br></br><a id="btnBuscar3" onclick="obtieneHallazgosSemanales();" class="btn busc ml20"> Resumen Hallazgos Semanales </a>
					</div>
			</div>
		</div>
		<br>
		<br>

</body>
<script type="text/javascript"
	src="../js/expancion/jquery.dropkickDos.js"></script>

<script type="text/javascript"
	src="../js/expancion/jquery.dropkickDos.js"></script>
<script>
            jQuery(document).ready(function($) {
                $( ".select_Dos").dropkick({
                    mobile: true
                });
            });
        </script>

<!-- Date Picker -->
<script src="../js/expancion/jquery-ui-datepicker.min.js"></script>
<script src="../js/expancion/jquery-ui-es.min.js"></script>
<script>
            $(function() {
                $("#datepicker" ).datepicker({       
                    changeMonth: false,
                    changeYear: false,
                    yearRange: "1900:today", 
                        beforeShow : function(){
                            if(!$('.date_picker').length){
                                $('#ui-datepicker-div').wrap('<span class="date_picker"></span>');
                            }
                        }
                });

                $("#datepickerDos" ).datepicker({       
                    changeMonth: false,
                    changeYear: false,
                    yearRange: "1900:today", 
                        beforeShow : function(){
                            if(!$('.date_picker').length){
                                $('#ui-datepicker-div').wrap('<span class="date_picker"></span>');
                            }
                        }
                });
            });
        </script>


<script type="text/javascript">
	function maximo(campo, limite) {
		if (campo.value.length >= limite) {
			campo.value = campo.value.substring(0, limite);
		}
	}

	function obtieneReporte(){
		
			var fechaInicio = $('#datepicker').val();
			var fechaFin = $('#datepickerDos').val();
			
			var fechaInicioParam = $('#datepicker').val();
			var fechaFinParam = $('#datepickerDos').val();
			

			if (fechaInicio.localeCompare('FECHA INICIO') == 0 || fechaInicio.localeCompare('') == 0 ){
				alert("Selecciona una fecha de inicio");
				return;
			} else {
				fechaInicio = $('#datepicker').val().split("/")[2] + $('#datepicker').val().split("/")[1]
				fechaInicio += $('#datepicker').val().split("/")[0]
			}
			
			if (fechaFin.localeCompare('FECHA TERMINO') == 0 || fechaFin.localeCompare('') == 0){
				alert("Selecciona una fecha de termino");
				return;
			} else {
				fechaFin = $('#datepickerDos').val().split("/")[2] + $('#datepickerDos').val().split("/")[1]
				fechaFin += $('#datepickerDos').val().split("/")[0]
							
				if (fechaInicio.localeCompare('FECHA INICIO') != 0 || fechaInicio.localeCompare('') != 0 ){
					if (validarFecha(fechaInicioParam,fechaFinParam) == false){
						alert("La fecha fin debe ser menor a la fecha inicio");
						return;
					}
				}

			}
			

			$(location).attr("href","resumenEntregaSemanal.htm?fechaInicio="+fechaInicio+"&fechaFin="+fechaFin);
			
		}
	function obtieneReporte2(){
		
		var fechaInicio = $('#datepicker').val();
		var fechaFin = $('#datepickerDos').val();
		
		var fechaInicioParam = $('#datepicker').val();
		var fechaFinParam = $('#datepickerDos').val();
		

		if (fechaInicio.localeCompare('FECHA INICIO') == 0 || fechaInicio.localeCompare('') == 0 ){
			alert("Selecciona una fecha de inicio");
			return;
		} else {
			fechaInicio = $('#datepicker').val().split("/")[2] + $('#datepicker').val().split("/")[1]
			fechaInicio += $('#datepicker').val().split("/")[0]
		}
		
		if (fechaFin.localeCompare('FECHA TERMINO') == 0 || fechaFin.localeCompare('') == 0){
			alert("Selecciona una fecha de termino");
			return;
		} else {
			fechaFin = $('#datepickerDos').val().split("/")[2] + $('#datepickerDos').val().split("/")[1]
			fechaFin += $('#datepickerDos').val().split("/")[0]
						
			if (fechaInicio.localeCompare('FECHA INICIO') != 0 || fechaInicio.localeCompare('') != 0 ){
				if (validarFecha(fechaInicioParam,fechaFinParam) == false){
					alert("La fecha fin debe ser menor a la fecha inicio");
					return;
				}
			}

		}
		

		$(location).attr("href","resumenEntregaSemanal2.htm?fechaInicio="+fechaInicio+"&fechaFin="+fechaFin);
		
	}

	function obtieneHallazgosSemanales(){
		
		var fechaInicio = $('#datepicker').val();
		var fechaFin = $('#datepickerDos').val();
		
		var fechaInicioParam = $('#datepicker').val();
		var fechaFinParam = $('#datepickerDos').val();
		

		if (fechaInicio.localeCompare('FECHA INICIO') == 0 || fechaInicio.localeCompare('') == 0 ){
			alert("Selecciona una fecha de inicio");
			return;
		} else {
			fechaInicio = $('#datepicker').val().split("/")[2] + $('#datepicker').val().split("/")[1]
			fechaInicio += $('#datepicker').val().split("/")[0]
		}
		
		if (fechaFin.localeCompare('FECHA TERMINO') == 0 || fechaFin.localeCompare('') == 0){
			alert("Selecciona una fecha de termino");
			return;
		} else {
			fechaFin = $('#datepickerDos').val().split("/")[2] + $('#datepickerDos').val().split("/")[1]
			fechaFin += $('#datepickerDos').val().split("/")[0]
						
			if (fechaInicio.localeCompare('FECHA INICIO') != 0 || fechaInicio.localeCompare('') != 0 ){
				if (validarFecha(fechaInicioParam,fechaFinParam) == false){
					alert("La fecha fin debe ser menor a la fecha inicio");
					return;
				}
			}

		}
		
		
		$(location).attr("href","generaHallazgosSemanal.htm?fechaInicio="+fechaInicio+"&fechaFin="+fechaFin);
		
	}
</script>
</html>