package com.gruposalinas.checklist.business;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.util.UtilFRQ;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.checklist.dao.EmpFijoDAO;
import com.gruposalinas.checklist.dao.ReporteChecksExpDAO;
import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;
import com.gruposalinas.checklist.domain.ReporteChecksExpDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import com.gruposalinas.checklist.resources.FRQConstantes;

public class ReporteChecksExpBI {

	private static Logger logger = LogManager.getLogger(ReporteChecksExpBI.class);

	private List<ReporteChecksExpDTO> listafila;
	private List<ReporteChecksExpDTO> lista;

	@Autowired
	ReporteChecksExpDAO reporteChecksExpDAO;

	@Autowired
	HallazgosTransfBI hallazgosTransfBI;
	@Autowired
	FirmaCheckBI firmaCheckBI;
	@Autowired
	SucursalChecklistBI sucursalChecklistBI;

	public ReporteChecksExpBI() {

	}

	public ReporteChecksExpBI(HallazgosTransfBI hallazgosTransfBI, FirmaCheckBI firmaCheckBI,
			SucursalChecklistBI sucursalChecklistBI) {
		this.hallazgosTransfBI = hallazgosTransfBI;
		this.firmaCheckBI = firmaCheckBI;
		this.sucursalChecklistBI = sucursalChecklistBI;
	}

	public List<ReporteChecksExpDTO> ObtieneBitacoras(String idusuario, String ceco, String idcheck) {

		try {

			listafila = reporteChecksExpDAO.ObtieneBitacoras(idusuario, ceco, idcheck);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			e.printStackTrace();
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	public List<ReporteChecksExpDTO> obtieneRespuestasNo(int bitacora) {

		try {
			lista = reporteChecksExpDAO.obtieneRespuestasNo(bitacora);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return lista;
	}

	public List<ReporteChecksExpDTO> obtienePregNoImpCecoFaseProyecto(String ceco, String fase, String proyecto) {

		try {

			listafila = reporteChecksExpDAO.obtienePregNoImpCecoFaseProyecto(ceco, fase, proyecto);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			e.printStackTrace();
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	@SuppressWarnings("null")
	public HashMap<String, Object> generaActaRecorridoWeb(String idCeco, String idProyecto, String idFase, String nombreSucursal,
			String zona, String region, String fase, String calificacion, String fecha) {

		String response = "";
		String rutaImg = "";
		int ordenFase = 0;

		List<String> docHtml = new ArrayList<String>();
		HashMap<String, Object> htmlDatosEntrega = new HashMap<String, Object>();
		HashMap<String, Object> responseMap = new HashMap<String, Object>();

		try {
			SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
					.getApplicationContext().getBean("soporteHallazgosTransfBI");
			String jsonString = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(idCeco, idFase, idProyecto);
			JsonParser parser = new JsonParser();
			JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
			JsonArray lista = jsonObj.getAsJsonArray("listaOrdenFase");

			if (lista != null && lista.size() > 0) {
				JsonObject obj = (JsonObject) lista.get(0).getAsJsonArray().get(0);
				ordenFase = obj.get("idOrdenFase").getAsInt();
			}
		} catch (Exception e) {
			logger.info("AP al obtener el nombre de la Fase para el acta " + e);
		}

		try {

			// Datos de entrega
			try {
				
				htmlDatosEntrega = generaDatosEntrega(idCeco, idFase, idProyecto, nombreSucursal, zona, region,
						fase, calificacion, fecha, ordenFase);
				
				if (htmlDatosEntrega.get("html") != null) {
					docHtml.add((String) htmlDatosEntrega.get("html"));

				}
				
			} catch (Exception e) {
				logger.info("AP al generar la hoja de datos" + e);
			}

			// Hoja Firmas
			try {
				String htmlFirmas = generaFirmasActa(idCeco, idFase, idProyecto);
				docHtml.add(htmlFirmas);
			} catch (Exception e) {
				logger.info("AP al generar la hoja de Firmas" + e);
			}

			// Imperdonables
			try {
				List<String> imperdonables = generaImperdonables(idCeco, idFase, idProyecto);
				for (String htmlImperdonables : imperdonables) {
					logger.info(htmlImperdonables);
					docHtml.add(htmlImperdonables);
				}
			} catch (Exception e) {
				logger.info("AP al generar las zonas \n" + e);
			}

			// Hallazgos
			try {
				List<String> zonas = generaZonas(idCeco, idFase, idProyecto);
				for (String htmlZona : zonas) {
					logger.info(htmlZona);
					docHtml.add(htmlZona);
				}
			} catch (Exception e) {
				logger.info("AP al generar las zonas \n" + e);
			}

			// Adicionales
			try {
				List<String> adicionales = generaAdicionales(idCeco, idFase, idProyecto, 0);
				for (String htmlZona : adicionales) {
					docHtml.add(htmlZona);
				}
			} catch (Exception e) {
				logger.info("AP al generar las zonas \n" + e);
			}

		} catch (Exception e) {
			logger.info("AP al generar el acta del recorrido");
			return null;

		}

		// Debemos enviar el correo
		logger.info("_______________________________ANTES DE ESCRIBIR PDF_______________________________");
		File adjunto = escribirPDFHtmlExpansion(docHtml);
		logger.info(adjunto.getAbsolutePath());
		logger.info("_______________________________DESPUES DE ESCRIBIR PDF_______________________________");
		
		responseMap.put("imagenSucursal", htmlDatosEntrega.get("imagenSucursal"));
		responseMap.put("arhivo", adjunto);
		
		return responseMap;

	}

	public String textosActasExpansion(int idProyecto, int idFase, String nombreFase) {

		String hora = "";
		int dia = 0;
		int mes = 0;
		int ano = 0;

		Date fechaActual = new Date();
		// Formateando la fecha:
		DateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");

		hora = formatoHora.format(fechaActual) + "";

		// Fecha actual desglosada:
		Calendar fecha = Calendar.getInstance();
		ano = fecha.get(Calendar.YEAR);
		mes = fecha.get(Calendar.MONTH) + 1;
		dia = fecha.get(Calendar.DAY_OF_MONTH);

		String texto = "<p>Siendo las " + hora + " horas del día " + dia + " del mes de " + mes + " del " + ano + ", ";

		switch (idProyecto) {
		// DAZ
		case 4:
			switch (idFase) {
			case 8:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			case 9:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Banco Azteca de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			default:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			}
			break;
		// OCC
		case 3:
			texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a OCC de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad</p>";
			break;
		// EKT NORMAL Y EKT RECONSTRUCCION 5
		case 2:
		case 5:
			switch (idFase) {
			case 4:
			case 14:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de construcción de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
				break;
			case 5:
			case 15:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de montaje y mobiliario de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Control Interno.</p>";
				break;
			case 6:
			case 16:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo el Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
				break;
			default:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de construcción de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
				break;
			}
			break;
		// SHINEADAS
		case 12:
			texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Aseguramiento de Calidad.</p>";
			break;
		// PPR
		case 13:
			switch (idFase) {
			case 37:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación  de Calidad.</p>";
				break;
			case 38:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Presta Prenda de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			default:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Presta Prenda de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			}
			break;
		// ITK
		case 14:
			switch (idFase) {
			case 39:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje  de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			case 40:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Italika de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			default:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Italika de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			}
			break;
		// TIENDA ANFOTRIONA
		case 23:
			texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Aseguramiento de Calidad.</p>";
			break;
		// MOVILIDAD
		case 24:
			texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo el Entrega a Movilidad de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
			break;
		// SIITO ALTERNO
		case 25:
			texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Aseguramiento de Calidad.</p>";
			break;
		default:
			texto += "se reúnen en el inmueble arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente del documento con el propósito de llevar a cabo "
					+ nombreFase + " de los trabajos de "
					+ "construcción ejecutados por el Despacho de Bienes Inmuebles(BI) y validados por el área de Aseguramiento de Calidad.</p>";
			break;
		}

		texto += "\n <p>Realizado el recorrido por construcción, revisados los trabajos efectuados y ejecutados con base en los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra, proyecto ejecutivo e indicaciones hechas oportunamente en el libro de bitácora. El Cliente lo recibe bajo el entendido de que esto será considerado efectivo una vez que se haya cumplido con los detalles y compromisos registrados en el Check list adjunto  y realizado en el recorrido, siendo el área responsable de la tienda quienes deberán asegurar el cumplimiento antes mencionado.</p>";

		return texto;

	}

	public String textosSuperiorActasExpansion(int idProyecto, int idFase, String nombreFase, String fecha) {

		String hora = "";
		hora = fecha.split(" ")[1];

		// Fecha actual desglosada:
		String ano = fecha.split(" ")[0].split("-")[0];
		String mes = fecha.split(" ")[0].split("-")[1];
		String dia = fecha.split(" ")[0].split("-")[2];

		String texto = "<p>Siendo las " + hora + " horas del día " + dia + " del mes de " + mes + " del " + ano + ", ";

		switch (idProyecto) {
		// DAZ
		case 4:
			switch (idFase) {
			case 8:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			case 9:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Banco Azteca de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			default:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			}
			break;
		// OCC
		case 3:
			texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a OCC de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad</p>";
			break;
		// EKT NORMAL Y EKT RECONSTRUCCION 5
		case 2:
		case 5:
			switch (idFase) {
			case 4:
			case 14:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de construcción de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
				break;
			case 5:
			case 15:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de montaje y mobiliario de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Control Interno.</p>";
				break;
			case 6:
			case 16:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo el Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
				break;
			default:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de construcción de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
				break;
			}
			break;
		// SHINEADAS
		case 12:
			texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Aseguramiento de Calidad.</p>";
			break;
		// PPR
		case 13:
			switch (idFase) {
			case 37:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación  de Calidad.</p>";
				break;
			case 38:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Presta Prenda de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			default:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Presta Prenda de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			}
			break;
		// ITK
		case 14:
			switch (idFase) {
			case 39:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje  de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			case 40:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Italika de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			default:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Italika de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			}
			break;
		// TIENDA ANFOTRIONA
		case 23:
			texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Aseguramiento de Calidad.</p>";
			break;
		// MOVILIDAD
		case 24:
			texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo el Entrega a Movilidad de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
			break;
		// SIITO ALTERNO
		case 25:
			texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Aseguramiento de Calidad.</p>";
			break;
		default:
			texto += "se reúnen en el inmueble arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente del documento con el propósito de llevar a cabo "
					+ nombreFase + " de los trabajos de "
					+ "construcción ejecutados por el Despacho de Bienes Inmuebles(BI) y validados por el área de Aseguramiento de Calidad.</p>";
			break;
		}

		return texto;

	}

	public String textosInferiorActasExpansion(int idProyecto, int idFase, String nombreFase) {

		String hora = "";
		int dia = 0;
		int mes = 0;
		int ano = 0;

		Date fechaActual = new Date();
		// Formateando la fecha:
		DateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");

		hora = formatoHora.format(fechaActual) + "";

		// Fecha actual desglosada:
		Calendar fecha = Calendar.getInstance();
		ano = fecha.get(Calendar.YEAR);
		mes = fecha.get(Calendar.MONTH) + 1;
		dia = fecha.get(Calendar.DAY_OF_MONTH);

		String texto = "<p>Siendo las " + hora + " horas del día " + dia + " del mes de " + mes + " del " + ano + ", ";

		switch (idProyecto) {
		// DAZ
		case 4:
			switch (idFase) {
			case 8:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			case 9:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Banco Azteca de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			default:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			}
			break;
		// OCC
		case 3:
			texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a OCC de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad</p>";
			break;
		// EKT NORMAL Y EKT RECONSTRUCCION 5
		case 2:
		case 5:
			switch (idFase) {
			case 4:
			case 14:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de construcción de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
				break;
			case 5:
			case 15:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de montaje y mobiliario de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Control Interno.</p>";
				break;
			case 6:
			case 16:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo el Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
				break;
			default:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de construcción de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
				break;
			}
			break;
		// SHINEADAS
		case 12:
			texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Aseguramiento de Calidad.</p>";
			break;
		// PPR
		case 13:
			switch (idFase) {
			case 37:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación  de Calidad.</p>";
				break;
			case 38:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Presta Prenda de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			default:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Presta Prenda de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			}
			break;
		// ITK
		case 14:
			switch (idFase) {
			case 39:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje  de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			case 40:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Italika de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			default:
				texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Italika de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
				break;
			}
			break;
		// TIENDA ANFOTRIONA
		case 23:
			texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Aseguramiento de Calidad.</p>";
			break;
		// MOVILIDAD
		case 24:
			texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo el Entrega a Movilidad de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
			break;
		// SIITO ALTERNO
		case 25:
			texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Aseguramiento de Calidad.</p>";
			break;
		default:
			texto += "se reúnen en el inmueble arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente del documento con el propósito de llevar a cabo "
					+ nombreFase + " de los trabajos de "
					+ "construcción ejecutados por el Despacho de Bienes Inmuebles(BI) y validados por el área de Aseguramiento de Calidad.</p>";
			break;
		}

		return texto;

	}

	public HashMap<String, Object> generaDatosEntrega(String ceco, String fase, String proyecto, String nombreSucursal, String zona,
			String region, String nombreFaseParam, String calificacion, String fecha, int ordenFase) {

		String nombreFase = "";
		String nombreCeco = "";
		String rutaImg = "";
		String imagenFachadaAdaptada = "";
		String fechaParam = fecha;
		int hallazgos = 0;
		int imperdonables = 0;
		
		HashMap responseMap = new HashMap<String, Object>(); 
		File fotoFachaParam = null;

		fecha = fecha.split(" ")[0];
		fecha = fecha.split("-")[2] + "/" + fecha.split("-")[1] + "/" + fecha.split("-")[0];

		List<FirmaCheckDTO> firmasB = new ArrayList<>();
		firmasB = firmaCheckBI.obtieneDatosFirmaCecoFaseProyecto(ceco, fase, proyecto);

		if (firmasB.get(0).getCeco() != null && firmasB.get(0).getCeco() != "") {
			try {
				List<SucursalChecklistDTO> lista = sucursalChecklistBI
						.obtieneDatos(Integer.parseInt(firmasB.get(0).getCeco()));

				if (lista != null) {
					for (SucursalChecklistDTO sucursalChecklistDTO : lista) {
						if (sucursalChecklistDTO != null && sucursalChecklistDTO.getRuta() != null
								&& !sucursalChecklistDTO.getRuta().equals("")) {
							rutaImg = sucursalChecklistDTO.getRuta();
							// break;
						}
					}
				}

			} catch (Exception e) {
				logger.info("SIN IMAGEN DE FACHADA DE SUCURSALES ");
			}
		}

		try {
			fotoFachaParam = getRutaImagenFachada(rutaImg);
			imagenFachadaAdaptada = fotoFachaParam.getAbsolutePath();

		} catch (Exception e) {

			logger.info("AP al obtener la imagen de fachada " + e);
			imagenFachadaAdaptada = rutaImg;

		}

		try {
			SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
					.getApplicationContext().getBean("soporteHallazgosTransfBI");
			String jsonString = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(ceco + "", fase + "", proyecto + "");
			JsonParser parser = new JsonParser();
			JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
			JsonArray lista = jsonObj.getAsJsonArray("listaOrdenFase");
			if (lista != null && lista.size() > 0) {
				JsonObject obj = (JsonObject) lista.get(0).getAsJsonArray().get(0);
				nombreFase = obj.get("nombreFase").getAsString();
			}
		} catch (Exception e) {
			logger.info("AP al obtener el nombre de la Fase para el correo " + e);
			return null;
		}

		try {
			SucursalChecklistBI sucursalChecklistBI = (SucursalChecklistBI) FRQAppContextProvider
					.getApplicationContext().getBean("sucursalChecklistBI");

			nombreCeco = sucursalChecklistBI.obtieneDatosSucCecoFaseProyecto(ceco + "", fase + "", proyecto + "").get(0)
					.getNombre();
			// nombreCeco = sucursalChecklistBI.obtieneDatosSuc(ceco+"").get(0).getNombre();

		} catch (Exception e) {
			logger.info("AP al obtener el nombre del ceco para el correo " + e);
			return null;

		}

		// Conteo hallazgos

		// Lista de Hallazgos
		List<HallazgosTransfDTO> listaHallazgos = new ArrayList<HallazgosTransfDTO>();
		listaHallazgos = hallazgosTransfBI.obtieneDatos(ceco, proyecto, fase);

		ArrayList<HallazgosTransfDTO> listaHallazgosImperdonables = (ArrayList<HallazgosTransfDTO>) listaHallazgos
				.stream().filter(arbol -> arbol.getArbol().equalsIgnoreCase("1")).collect(Collectors.toList());

		hallazgos = listaHallazgos.size();
		imperdonables = listaHallazgosImperdonables.size();

		String textoResultado = getTextoResultado(ordenFase, Integer.parseInt(proyecto),
				Double.parseDouble(calificacion), imperdonables);
		String colorResultado = getAperturableColor(imperdonables, "negocio", Double.parseDouble(calificacion),
				Double.parseDouble(calificacion), ordenFase, Integer.parseInt(fase));
		String colorLetraString = getAperturableColorLetra(imperdonables, "negocio", Double.parseDouble(calificacion),
				Double.parseDouble(calificacion), ordenFase, Integer.parseInt(fase));

		// BASE DOCUMENTO
		String thead = "<div style='background-color:#" + colorResultado + "; width: 100%; height:5%;'>"
				+ "<table style='width: 100%;  height:5%; border-spacing: 0px ; margin-top: 15px; style='' >"
				+ "	<tbody style='height:5%;' >" + "   	<tr>"
				+ "			<td style='text-align: center; width: 10%; background-color: ;'></td>"
				+ "			<td style='text-align: center; width: 80%; background-color: ; text-align: center; font-weight: bold; color:"+colorLetraString+";'>"
				+ textoResultado + "</td>"
				+ "			<td style='text-align: center; width: 10%; background-color: ;'></td>" + "		</tr>"
				+ "   </tbody>" + " </table>" + "</div>";

		thead += "<div style='background-color: white; width: 100%; height:30%;'>" +

				"<table style='width: 100%; height:100%; border-spacing: 0px ; margin-top: 0px; style='background-color:white;' >"
				+ "	<tbody style='background-color:white;' >" + "   	<tr>"
				+ "			<td style='text-align: center; width: 100%; height:100%; background-color: white;'>"
				+ "<img src='" + imagenFachadaAdaptada + "'alt='Lamp'; width='350px'; height='200px';></img>" + "</td>"
				+ "		</tr>" + "   </tbody>" + " </table>" + "</div>";

		thead += "<div style='background-color: white; width: 100%; height:5%;'>"
				+ "<table style='width: 100%;  height:5%; border-spacing: 0px ; margin-top: 15px; style='' >"
				+ "	<tbody style='height:5%;' >" + "   	<tr>"
				+ "			<td style='text-align: center; width: 10%; background-color: ;'></td>"
				+ "			<td style='font-size: 12; text-align: center; text-align: center; width: 80%; background-color: ; text-align: center; font-weight: bold;'><b>Acta Entrega Franquicia</b></td>"
				+ "			<td style='text-align: center; width: 10%; background-color: ;'></td>" + "		</tr>"
				+ "   </tbody>" + " </table>" + "</div>";

		String tablaDatosRecepcion = "<table style='width: 100%; border-spacing: 0px; margin-bottom: 70px;'>"
				+ "	<tbody>" + "  		<tr></tr>";

		tablaDatosRecepcion += "<tr width= '100%' height='50px' style='background-color: white; border-top: 5px solid '>"
				+ "	<td style='font-size: 12; text-align: center; width: 50%; height=50px;  background-color: white;'> <b>Sucursal :</b>"
				+ nombreSucursal + "</td>"
				+ "	<td style='font-size: 12; text-align: center; width: 50%; height=50px;  background-color: white;'> <b>No. Ecónomico:</b>"
				+ ceco + "</td>" + "</tr>";

		tablaDatosRecepcion += "<tr width= '100%' height='50px' style='background-color: white; border-top: 5px solid '>"
				+ "	<td style='font-size: 12; text-align: center; width: 50%; height=50px;  background-color: white;'> <b>Zona:</b>"
				+ zona + "</td>"
				+ "	<td style='font-size: 12; text-align: center; width: 50%; height=50px;  background-color: white;'> <b>Visita:</b>"
				+ nombreFase + "</td>" + "</tr>";

		tablaDatosRecepcion += "<tr width= '100%' height='50px' style='background-color: white; border-top: 5px solid '>"
				+ "	<td style='font-size: 12; text-align: center; width: 50%; height=50px;  background-color: white;'> <b>Región:</b>"
				+ region + "</td>"
				+ "	<td style='font-size: 12; text-align: center; width: 50%; height=50px;  background-color: white;'> <b>Fecha:</b>"
				+ fecha + "</td> " + "</tr>";

		tablaDatosRecepcion += "<tr width= '100%' height='50px' style='background-color: white; border-top: 5px solid '>"
				+ "	<td style='font-size: 12; text-align: center; width: 50%; height=50px;  background-color: white;'> <b>Calificación:</b>"
				+ calificacion + "</td>"
				+ "	<td style='font-size: 12; text-align: center; width: 50%; height=50px;  background-color: white;'> </td>"
				+ "</tr>";

		tablaDatosRecepcion += "<br></br><tr width= '100%' height='50px' style='background-color: white; border-top: 5px solid '>"
				+ "	<td style='font-size: 12; text-align: center; width: 50%; height=50px;  background-color: white;'> <b>Detalle de imperdonables</b>"
				+ "	<br></br>Cantidad: " + imperdonables + "</td>"
				+ "	<td style='font-size: 12; text-align: center; width: 50%; height=50px;  background-color: white;'> </td>"
				+ "</tr>";

		tablaDatosRecepcion += "<tr width= '100%' height='50px' style='background-color: white; border-top: 5px solid '>"
				+ "	<td style='font-size: 12; text-align: center; width: 50%; height=50px;  background-color: white;'> <b>Detalle de Hallazgos</b>"
				+ "	<br></br>Cantidad: " + hallazgos + " </td>"
				+ "	<td style='font-size: 12; text-align: center; width: 50%; height=50px;  background-color: white;'> </td>"
				+ "</tr>";

		tablaDatosRecepcion += "</tbody>" + "</table>";

		tablaDatosRecepcion += "<div style='background-color: white; width: 100%; height:30%;'>"
				+ "<table style='width: 100%;  height:10%; border-spacing: 0px ; margin-top: 0px; style='' >"
				+ "	<tbody style='height:10%;' >" + "   	<tr>"
				+ "			<td style='font-size: 12; text-align: center; text-align: center; width: 100%; background-color: white ; text-align:justify;'>"
				+ textosSuperiorActasExpansion(Integer.parseInt(proyecto), Integer.parseInt(fase), proyecto, fechaParam)
				+ "</td>" + "		</tr>" + "   </tbody>" + " </table>" + "</div>";

		// Salida
		String html = "<html>" + "	<head>" + "		<style>" + "  			table {"
				+ "        		border-collapse: separate;" + "           	width: 100%;" + "        	}"
				+ "		</style>" + "	</head>";

		html += "<body>";

		html += thead;

		html += tablaDatosRecepcion;

		html += "" + "</body>" + "</html>";
		
		responseMap.put("html", html);
		responseMap.put("imagenSucursal", fotoFachaParam);

		return responseMap;

	}

	public String generaFirmasActa(String ceco, String fase, String proyecto) {

		String htmlFirmas = "";
		String nombreFase = "";
		String nombreCeco = "";
		int ordenFase = 0;

		try {
			SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
					.getApplicationContext().getBean("soporteHallazgosTransfBI");
			String jsonString = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(ceco + "", fase + "", proyecto + "");
			JsonParser parser = new JsonParser();
			JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
			JsonArray lista = jsonObj.getAsJsonArray("listaOrdenFase");
			if (lista != null && lista.size() > 0) {
				JsonObject obj = (JsonObject) lista.get(0).getAsJsonArray().get(0);
				nombreFase = obj.get("nombreFase").getAsString();
				ordenFase = obj.get("idOrdenFase").getAsInt();
			}
		} catch (Exception e) {
			logger.info("AP al obtener el nombre de la Fase para el correo " + e);
			return "";
		}

		try {
			SucursalChecklistBI sucursalChecklistBI = (SucursalChecklistBI) FRQAppContextProvider
					.getApplicationContext().getBean("sucursalChecklistBI");

			nombreCeco = sucursalChecklistBI.obtieneDatosSucCecoFaseProyecto(ceco + "", fase + "", proyecto + "").get(0)
					.getNombre();
			// nombreCeco = sucursalChecklistBI.obtieneDatosSuc(ceco+"").get(0).getNombre();

		} catch (Exception e) {
			logger.info("AP al obtener el nombre del ceco para el correo " + e);
			return "";

		}

		htmlFirmas += "<html> <head> <meta charset='UTF-8'> </meta> <title>Firmas</title> <style>" + "body {"
				+ "background-color: #FFFFFF" + "}" +

				".tablaFirmas {" + "background-color: #FFFFFF;" + "}" +

				"table td tr {" + "text-align: center;" + "font-size: 10px;" + "}" +

				"body{" + "font-size: 12px;" + "}" +

				"b{" + "font-size: 12px;" + "}" +

				".encabezado{" + "border: 5px solid white;" + "}" +

				".contendedorFirma{" + "font-size: 12px;" + "text-align: center;" + "border: 5px solid #FFFFFF;" + "}" +

				".divBottom{" + "border-bottom: 2px solid #F0F0F0;" + "}" +

				".divFirmas{" + "border-bottom: 2px solid #F0F0F0;" + "padding-top: 5px;" +

				"}" +

				".ncabezado{" + "padding: 5px;" + "text-align: left;" + "}" + "</style>" + "</head>";

		htmlFirmas += "<body>" + "<!-- Tabla Encabezado -->" + "<div style='border-bottom: 2px solid #F0F0F0;'>"
				+ "<table style='width:100%; height:50px; ' class='encabezado'>" + "<tr>" + "<th>" + "" + "</th>" +

				"<td  rowspan='2' style='text-align: right;'>" + "<img src='' width='36' height='36' />"
				+ "&nbsp;&nbsp;&nbsp;" + "<img src='' width='45' height='30'/>" + "</td>" + "</tr>" +

				"<tr>" + "<td>" + "" + "</td>" + "</tr>" +

				"</table>" + "</div>" +

				"<div class='divFirmas'>" + "<label>FIRMAS DE ACEPTACIÓN</label>" + "</div>" +

				"<br></br><br></br>" +

				"<div>";

		htmlFirmas += "<p style='font-size: 12; text-align: justify;'>" + getTextoFirmasAceptacion(Integer.parseInt(proyecto), ordenFase) + "</p>";

		htmlFirmas += "</div>";

		htmlFirmas += "<table class='tablaFirmas' style='width:100%; align:center;'>" +

				"<tbody>";

		htmlFirmas += getTablaFirmas(ceco, fase, proyecto);

		htmlFirmas += "</tbody> </table> </body> </html>";

		return htmlFirmas;

	}

	public String getTextoFirmas(String ceco, String proyecto, String fase, String nombreFase) {

		String texto = "";
		int ordenFase = 0;

		try {

			SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
					.getApplicationContext().getBean("soporteHallazgosTransfBI");
			String jsonString = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(ceco + "", fase + "", proyecto + "");
			JsonParser parser = new JsonParser();
			JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
			JsonArray lista = jsonObj.getAsJsonArray("listaOrdenFase");

			if (lista != null && lista.size() > 0) {
				JsonObject obj = (JsonObject) lista.get(0).getAsJsonArray().get(0);
				ordenFase = obj.get("idOrdenFase").getAsInt();
			}

		} catch (Exception e) {

			logger.info("AP al obtener el nombre de la Fase para el correo " + e);

			texto = "<p>Se reúnen en el inmueble, arriba mencionado, las personas "
					+ "cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Validación "
					+ "técnica de los trabajos de construcción ejecutados por el Despacho de Bienes Inmuebles y validados por el área de "
					+ "Control Interno.</p>"
					+ "<p>Realizado el recorrido por el área de construcción, revisados los trabajos efectuados y ejecutados con base en los "
					+ "alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra, proyecto "
					+ "ejecutivo e indicaciones hechas oportunamente en el libro bitácora. El cliente revisará y autorizará el siguiente proceso "
					+ "con la condición de tener finalizados cada uno de los detalles registrados en el Checklist adjunto . Es imprescindible "
					+ "contar con la evidencia de los trabajos finalizados para continuar y programar la siguiente validación.</p>";

			return texto;

		}

		texto = textosActasExpansion(Integer.parseInt(proyecto), Integer.parseInt(fase), nombreFase);

		return texto;
	}

	public String getTablaFirmas(String ceco, String fase, String proyecto) {

		String tablaFirmas = "";
		JsonArray listaFirmas = new JsonArray();

		try {
			SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
					.getApplicationContext().getBean("soporteHallazgosTransfBI");
			String jsonString = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(ceco + "", fase + "", proyecto + "");
			JsonParser parser = new JsonParser();
			JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
			listaFirmas = (JsonArray) jsonObj.getAsJsonArray("listaFirmas").get(0);

		} catch (Exception e) {
			logger.info("AP al obtener el nombre de la Fase para el correo " + e);
			return "";
		}

		for (int i = 0; i < listaFirmas.size(); i++) {

			if (i == 0) {
				tablaFirmas += "<tr>";
			}
			if ((i != 0) && ((i) % 3 == 0)) {
				tablaFirmas += "<tr>";
			}

			JsonObject obj = (JsonObject) listaFirmas.get(i);
			String nombre = obj.get("responsable").getAsString();
			String puesto = obj.get("puesto").getAsString();
			String imagen = obj.get("ruta").getAsString();

			tablaFirmas += "<td style='width:100%;'>" +

					"<div class='contendedorFirma' style=' width:100%; height:150px;'>"
					+ "<div class='contenedorImg' style='border-bottom: 1px solid black;'>"
					+ "<img src='http://10.53.33.82" + imagen + "' alt='Lamp' width='60%' height='75%'></img>"
					+ "</div>" +

					"<div class='contenedorNombre'>" + "<b>" + nombre + "</b>" + "</div>" +

					"<div class='contenedorPuesto'>" + puesto + "</div>" +

					"</div>" +

					"</td>";

			if ((i + 1) % 3 == 0) {
				tablaFirmas += "</tr>";
			}

			if (i == listaFirmas.size() - 1 && (listaFirmas.size() % 3 != 0)) {
				tablaFirmas += "</tr>";
			}

		}

		return tablaFirmas;
	}

	public List<String> generaZonas(String ceco, String fase, String proyecto) {

		String nombreFase = "";
		String nombreCeco = "";

		List<String> zonasHtml = null;

		try {
			SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
					.getApplicationContext().getBean("soporteHallazgosTransfBI");
			String jsonString = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(ceco + "", fase + "", proyecto + "");
			JsonParser parser = new JsonParser();
			JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
			JsonArray lista = jsonObj.getAsJsonArray("listaOrdenFase");
			if (lista != null && lista.size() > 0) {
				JsonObject obj = (JsonObject) lista.get(0).getAsJsonArray().get(0);
				nombreFase = obj.get("nombreFase").getAsString();
			}
		} catch (Exception e) {
			logger.info("AP al obtener el nombre de la Fase para el correo " + e);
			return new ArrayList<String>();
		}

		try {
			SucursalChecklistBI sucursalChecklistBI = (SucursalChecklistBI) FRQAppContextProvider
					.getApplicationContext().getBean("sucursalChecklistBI");

			nombreCeco = sucursalChecklistBI.obtieneDatosSucCecoFaseProyecto(ceco + "", fase + "", proyecto + "").get(0)
					.getNombre();
			// nombreCeco = sucursalChecklistBI.obtieneDatosSuc(ceco+"").get(0).getNombre();

		} catch (Exception e) {
			logger.info("AP al obtener el nombre del ceco para el correo " + e);
			return new ArrayList<String>();

		}

		try {
			HallazgosZonasHtmlBI hallazgosZonasHtmlBI = new HallazgosZonasHtmlBI();
			zonasHtml = hallazgosZonasHtmlBI.generaHtmlZonasReporte(ceco, fase, proyecto, nombreFase, nombreCeco);

		} catch (Exception e) {

			zonasHtml = new ArrayList<>();

		}
		return zonasHtml;
	}

	public List<String> generaAdicionales(String ceco, String fase, String proyecto, int parametro) {

		String nombreFase = "";
		String nombreCeco = "";

		List<String> adicionalesHtml = null;

		try {
			SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
					.getApplicationContext().getBean("soporteHallazgosTransfBI");
			String jsonString = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(ceco + "", fase + "", proyecto + "");
			JsonParser parser = new JsonParser();
			JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
			JsonArray lista = jsonObj.getAsJsonArray("listaOrdenFase");
			if (lista != null && lista.size() > 0) {
				JsonObject obj = (JsonObject) lista.get(0).getAsJsonArray().get(0);
				nombreFase = obj.get("nombreFase").getAsString();
			}
		} catch (Exception e) {
			logger.info("AP al obtener el nombre de la Fase para el correo " + e);
			return new ArrayList<String>();
		}

		try {
			SucursalChecklistBI sucursalChecklistBI = (SucursalChecklistBI) FRQAppContextProvider
					.getApplicationContext().getBean("sucursalChecklistBI");

			nombreCeco = sucursalChecklistBI.obtieneDatosSucCecoFaseProyecto(ceco + "", fase + "", proyecto + "").get(0)
					.getNombre();
			// nombreCeco = sucursalChecklistBI.obtieneDatosSuc(ceco+"").get(0).getNombre();

		} catch (Exception e) {
			logger.info("AP al obtener el nombre del ceco para el correo " + e);
			return new ArrayList<String>();

		}

		try {
			AdicionalesZonasHtmlBI adicionalesBI = new AdicionalesZonasHtmlBI();
			adicionalesHtml = adicionalesBI.generaHtmlAdicionalesReporte(ceco, fase, proyecto, nombreFase, nombreCeco,
					0);

		} catch (Exception e) {

			adicionalesHtml = new ArrayList<>();

		}

		return adicionalesHtml;

	}

	public File escribirPDFHtmlExpansion(List<String> html) {
		File temp = null;
		try {
			// create a temp file
			temp = File.createTempFile("Acta_Entrega", ".pdf");
			FileOutputStream file = new FileOutputStream(temp);
			Document document = new Document();
			PdfWriter writer = PdfWriter.getInstance(document, file);
			document.open();

			java.util.Iterator<String> it = html.iterator();

			while (it.hasNext()) {
				String k = it.next();
				XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
				ByteArrayInputStream is = new ByteArrayInputStream(k.getBytes());
				worker.parseXHtml(writer, document, is);
				document.newPage();
			}
			document.close();
			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return temp;
	}

	public File getRutaImagenFachada(String ruta) throws IOException, ImageProcessingException {

		File fotoCorrecta = null;

		try {

			// URL url = new URL(FRQConstantes.getRutaImagen()+ruta);
			URL url = new URL("http://10.89.85.167/" + ruta);
			InputStream fis = url.openStream();

			InputStream fisData = url.openStream();
			Metadata metadata = ImageMetadataReader.readMetadata(fisData);

			File fotoPaso = File.createTempFile("fotoPaso", ".jpg");
			BufferedImage imgBuff = ImageIO.read(fis);
			ImageIO.write(imgBuff, "png", fotoPaso);

			System.out.println(fotoPaso.getAbsolutePath());

			javaxt.io.Image image = new javaxt.io.Image(fotoPaso.getAbsolutePath());
			fotoCorrecta = File.createTempFile("fotoFachada", ".jpg");

			String desc = "";
			for (Directory directory : metadata.getDirectories()) {
				for (Tag tag : directory.getTags()) {
					if (tag.getTagName().equals("Orientation")) {
						desc = tag.getDescription();
					}
				}
			}

			switch (desc) {

			case "Top, left side (Horizontal / normal)":
				break;

			case "Top, right side (Mirror horizontal)":
				break;

			case "Bottom, right side (Rotate 180)":
				image.rotate(180);
				break;

			case "Bottom, left side (Mirror vertical)":
				image.rotate(-180);
				break;

			case "Left side, top (Mirror horizontal and rotate 270 CW)":
				image.rotate(-90);
				break;

			case "Right side, top (Rotate 90 CW)":
				image.rotate(90);
				break;

			case "Right side, bottom (Mirror horizontal and rotate 90 CW)":
				image.rotate(-270);
				break;

			case "Left side, bottom (Rotate 270 CW)":
				image.rotate(270);
				break;

			default:
				break;

			}

			BufferedImage imgBuff2 = image.getBufferedImage();
			ImageIO.write(imgBuff2, "png", fotoCorrecta);

			fisData.close();
			fisData.close();
			fotoPaso.delete();
			fotoPaso.deleteOnExit();

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return fotoCorrecta;

	}

	public String getTextoResultado(int ordenFase, int idProyecto, double calificacion, int imperdonables) {

		String texto = "";

		boolean aperturable = imperdonables == 0;

		if (ordenFase < 3 && (idProyecto == 2 || idProyecto == 5)) {
			double calificacionesTotales = calificacion;
			if (calificacionesTotales >= 90) {
				texto = "CUMPLE";
			} else if (calificacionesTotales >= 51 && calificacionesTotales < 90) {
				texto = "DEBAJO DEL OBJETIVO";
			} else {
				texto = "NO CUMPLE";
			}

		} else {
			if (aperturable) {
				double calificacionesTotales = calificacion;
				if (calificacionesTotales >= 100) {
					texto = "OPERABLE  \"Con Recepción\"";
				} else {
					if (calificacionesTotales >= 90 && calificacionesTotales < 100) {
						texto = "OPERABLE  \"Sin Recepción\"";
					} else {
						if (calificacionesTotales >= 51 && calificacionesTotales < 90) {
							texto = "OPERABLE  \"Sin Recepción\"";
						} else {
							texto = "NO OPERABLE";
						}
					}
				}
			} else {
				texto = "NO OPERABLE";
			}
		}

		return texto;

	}

	public String getAperturableColor(int imperdonables, String negocio, double ponderacion, double ponderacionGeneral,
			int ordenFase, int fase) {

		String color = "";
		boolean aperturable = imperdonables == 0;

		if (ordenFase < 3 && (fase == 2 || fase == 5)) {

			double calificacionesTotales = ponderacion;

			if (calificacionesTotales >= 90) {
				color = "46ad35";
			} else {
				if (calificacionesTotales >= 51 && calificacionesTotales < 90) {
					color = "F4C431";
				} else {
					color = "FF0000";
				}
			}

		} else {

			if (aperturable) {

				double calificacionesTotales = ponderacion;
				if (calificacionesTotales >= 100) {
					color = "46ad35";
				} else {
					if (calificacionesTotales >= 90 && calificacionesTotales < 100) {
						color = "46ad35";
					} else {
						if (calificacionesTotales >= 51 && calificacionesTotales < 90) {
							color = "F4C431";
						} else {
							color = "FF0000";
						}
					}
				}
			} else {
				color = "FE0000";
			}
		}

		return color;

	}
	
	public String getAperturableColorLetra(int imperdonables, String negocio, double ponderacion, double ponderacionGeneral,
			int ordenFase, int fase) {

		String color = "";
		boolean aperturable = imperdonables == 0;

		if (ordenFase < 3 && (fase == 2 || fase == 5)) {

			double calificacionesTotales = ponderacion;

			if (calificacionesTotales >= 90) {
				color = "white";
			} else {
				if (calificacionesTotales >= 51 && calificacionesTotales < 90) {
					color = "black";
				} else {
					color = "white";
				}
			}

		} else {

			if (aperturable) {

				double calificacionesTotales = ponderacion;
				if (calificacionesTotales >= 100) {
					color = "white";
				} else {
					if (calificacionesTotales >= 90 && calificacionesTotales < 100) {
						color = "black";
					} else {
						if (calificacionesTotales >= 51 && calificacionesTotales < 90) {
							color = "white";
						} else {
							color = "white";
						}
					}
				}
			} else {
				color = "white";
			}
		}

		return color;

	}

	public String getTextoFirmasAceptacion(int idProyecto, int ordenFase) {

		String texto = "";

		switch (idProyecto) {

		// OCC
		case 3:

			switch (ordenFase) {
			case 1:
				texto = "Se realiza recorrido de obra, revisando los trabajos efectuados y ejecutados con base en los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra, proyecto ejecutivo e indicaciones anotadas oportunamente en el libro de bitácora."
						+ "<b> El cliente lo recibe, con la salvedad que esto será considerado efectivo una vez que se hayan cumplido con los detalles y compromisos registrados en el Check list adjunto  y realizado en el recorrido, siendo los Gerentes de la tienda quiénes deberán asegurar el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia de los trabajos finalizados.</b>";
				break;

			case 2:
				texto = "Se realiza recorrido de obra, revisando los trabajos efectuados y ejecutados con base en los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra, proyecto ejecutivo e indicaciones anotadas oportunamente en el libro de bitácora."
						+ "<b> El cliente lo recibe, con la salvedad que esto será considerado efectivo una vez que se hayan cumplido con los detalles y compromisos registrados en el Check list adjunto  y realizado en el recorrido, siendo los Gerentes de la tienda quiénes deberán asegurar el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia de los trabajos finalizados.</b>";
				break;
			default:
				texto = "Se realiza recorrido de obra, revisando los trabajos efectuados y ejecutados con base en los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra, proyecto ejecutivo e indicaciones anotadas oportunamente en el libro de bitácora."
						+ "<b> El cliente lo recibe, con la salvedad que esto será considerado efectivo una vez que se hayan cumplido con los detalles y compromisos registrados en el Check list adjunto  y realizado en el recorrido, siendo los Gerentes de la tienda quiénes deberán asegurar el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia de los trabajos finalizados.</b>";
				break;
			}

			break;

		// ITK
		case 14:

			switch (ordenFase) {
			case 1:
				texto = "Se realiza recorrido de obra, revisando los trabajos efectuados y ejecutados"
						+ "    con base en los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra,"
						+ "    proyecto ejecutivo e indicaciones hechas oportunamente en el libro  bitácora."
						+ "    <b> El cliente lo recibe, con la salvedad que esto será considerado efectivo una vez que se ha cumplido con los detalles y"
						+ "        compromisos registrados en el Check list adjunto y realizado en el recorrido, siendo los Gerentes de la tienda quiénes"
						+ "        deberán asegurar el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia de los trabajos"
						+ "        finalizados" + "    </b>";
				break;

			case 2:
				texto = "Se realiza recorrido de obra, revisando los trabajos efectuados y ejecutados"
						+ "    con base en los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra,"
						+ "    proyecto ejecutivo e indicaciones hechas oportunamente en el libro  bitácora."
						+ "    <b> El cliente lo recibe, con la salvedad que esto será considerado efectivo una vez que se ha cumplido con los detalles y"
						+ "        compromisos registrados en el Check list adjunto y realizado en el recorrido, siendo los Gerentes de la tienda quiénes"
						+ "        deberán asegurar el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia de los trabajos"
						+ "        finalizados." + "    </b>";
				break;
			default:
				texto = "Se realiza recorrido de obra, revisando los trabajos efectuados y ejecutados"
						+ "    con base en los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra,"
						+ "    proyecto ejecutivo e indicaciones hechas oportunamente en el libro  bitácora."
						+ "    <b> El cliente lo recibe, con la salvedad que esto será considerado efectivo una vez que se ha cumplido con los detalles y"
						+ "        compromisos registrados en el Check list adjunto y realizado en el recorrido, siendo los Gerentes de la tienda quiénes"
						+ "        deberán asegurar el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia de los trabajos"
						+ "        finalizados" + "    </b>";
				break;
			}

			break;

		// PP
		case 13:

			switch (ordenFase) {

			case 1:
				texto = "Se realiza recorrido de obra, revisando los trabajos efectuados y ejecutados"
						+ "    con base en los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra,"
						+ "    proyecto ejecutivo e indicaciones anotadas oportunamente en el libro de bitácora.\r\n"
						+ "    <b> El cliente lo recibe, con la salvedad que esto será considerado efectivo una vez que se ha cumplido con los detalles y"
						+ "        compromisos registrados en el Check list adjunto y realizado en el recorrido, siendo los Gerentes de la tienda quiénes"
						+ "        deberán asegurar el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia de los trabajos"
						+ "        finalizados." + " </b>";
				break;

			case 2:
				texto = "Se realiza recorrido de obra, revisando los trabajos efectuados y ejecutados"
						+ "    con base en los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra,"
						+ "    proyecto ejecutivo e indicaciones anotadas oportunamente en el libro de bitácora."
						+ "    <b>. El cliente lo recibe, con la salvedad que esto será considerado efectivo una vez que se ha cumplido con los detalles y"
						+ "        compromisos registrados en el Check list adjunto y realizado en el recorrido, siendo los Gerentes de la tienda quiénes"
						+ "        deberán asegurar el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia de los trabajos"
						+ "        finalizados." + "</b>";
				break;

			default:
				texto = "Se realiza recorrido de obra, revisando los trabajos efectuados y ejecutados"
						+ "    con base en los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra,"
						+ "    proyecto ejecutivo e indicaciones anotadas oportunamente en el libro de bitácora.\r\n"
						+ "    <b> El cliente lo recibe, con la salvedad que esto será considerado efectivo una vez que se ha cumplido con los detalles y"
						+ "        compromisos registrados en el Check list adjunto y realizado en el recorrido, siendo los Gerentes de la tienda quiénes"
						+ "        deberán asegurar el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia de los trabajos"
						+ "        finalizados." + " 	</b>";
				break;

			}

			break;

		case 12:
			texto = "Se realiza recorrido de obra, revisando los trabajos efectuados y ejecutados con base en los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra, proyecto ejecutivo e indicaciones anotadas oportunamente en el libro de bitácora."
					+ "    <b>El cliente lo recibe, con la salvedad que esto será considerado efectivo una vez que se hayan cumplido con los detalles y compromisos registrados en el Check list adjunto  y realizado en el recorrido, siendo los Gerentes de la tienda quiénes deberán asegurar el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia de los trabajos finalizados.</b>"; // Shinead
			break;
		case 23:
			texto = "Se realiza recorrido de obra, revisando los trabajos efectuados y ejecutados con base en los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra, proyecto ejecutivo e indicaciones anotadas oportunamente en el libro de bitácora."
					+ "    <b>El cliente lo recibe, con la salvedad que esto será considerado efectivo una vez que se hayan cumplido con los detalles y compromisos registrados en el Check list adjunto  y realizado en el recorrido, siendo los Gerentes de la tienda quiénes deberán asegurar el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia de los trabajos finalizados.</b>"; // anfitriona
			break;
		case 24:
			texto = "Realizado el recorrido por construcción, revisados los trabajos efectuados y ejecutados con base en los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra, proyecto ejecutivo e indicaciones hechas oportunamente en el libro de bitácora."
					+ "<b> El Cliente lo recibe bajo el entendido de que esto será considerado efectivo una vez que se haya cumplido con los detalles y compromisos registrados en el Check list adjunto  y realizado en el recorrido, siendo el área de movilidad  de la tienda quienes deberán asegurar el cumplimiento antes mencionado.</b>"; // Movilidad
			break;
		case 25:
			texto = "Se realiza recorrido de obra, revisando los trabajos efectuados y ejecutados con base en los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra, proyecto ejecutivo e indicaciones anotadas oportunamente en el libro de bitácora."
					+ "<b> El cliente lo recibe, con la salvedad que esto será considerado efectivo una vez que se hayan cumplido con los detalles y compromisos registrados en el Check list adjunto  y realizado en el recorrido, siendo los Gerentes de la tienda quiénes deberán asegurar el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia de los trabajos finalizados.</b>"; // Sitio
																																																																																																						// Alterno
			break;

		default:

			switch (ordenFase) {

			case 1:
				texto = "Realizado el recorrido por el área de construcción,"
						+ "    revisados los trabajos efectuados y ejecutados con base en los alcances autorizados,"
						+ "    la normatividad, especificaciones generales, especificaciones particulares de obra,"
						+ "    proyecto ejecutivo e indicaciones hechas oportunamente en el libro bitácora."
						+ "    <b>El cliente revisará y autorizará el siguiente proceso con la condición de tener finalizados"
						+ "    cada uno de los detalles registrados en el Checklist adjunto."
						+ "    Es imprescindible contar con la evidencia de los trabajos finalizados para continuar y"
						+ "    programar la siguiente validación. Las evidencias tendrán que enviarse a más tardar 4 días antes"
						+ "    de cumplirse la semana 23 del proceso de obra (considerando el día de la presente validación).</b>";
				break;
			case 2:
				texto = "Realizado el recorrido por el área de construcción,"
						+ "    revisados los trabajos efectuados y ejecutados con base en los alcances autorizados,"
						+ "    la normatividad, especificaciones generales, especificaciones particulares de obra,"
						+ "    proyecto ejecutivo e indicaciones hechas oportunamente en el libro bitácora."
						+ "    <b>El cliente revisará y autorizará el siguiente proceso con la condición de tener finalizados"
						+ "    cada uno de los detalles registrados en el Checklist adjunto."
						+ "    Es imprescindible contar con la evidencia de los trabajos finalizados para continuar y"
						+ "    programar la siguiente validación. Las evidencias tendrán que enviarse a más tardar 4 días antes"
						+ "    de cumplirse la semana 24 del proceso de obra (considerando el día de la presente validación).</b>";
				break;
			case 3:
				texto = "Realizado el recorrido por el área de construcción,"
						+ "    revisados los trabajos efectuados y ejecutados con base en los alcances autorizados,"
						+ "    la normatividad, especificaciones generales, especificaciones particulares de obra,"
						+ "    proyecto ejecutivo e indicaciones hechas oportunamente en el libro de bitácora."
						+ "    <b>El Cliente lo recibe, bajo el entendido de que esto será considerado efectivo"
						+ "    una vez que se cumplan los detalles y compromisos registrados en el Checklist adjunto y"
						+ "    realizado en el recorrido, siendo los Gerentes de la tienda quiénes deberán asegurar"
						+ "    el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia"
						+ "    de los trabajos finalizados para continuar y programar la siguiente validación."
						+ "    Las evidencias tendrán que enviarse a más tardar 4 días antes de cumplirse la semana 25"
						+ "    del proceso de obra (considerando el día de la presente validación).</b>";
				break;
			case 4:
				texto = "Realizado el recorrido por el área de construcción,"
						+ "    revisados los trabajos efectuados y ejecutados con base en los alcances autorizados,"
						+ "    la normatividad, especificaciones generales, especificaciones particulares de obra,"
						+ "    proyecto ejecutivo e indicaciones hechas oportunamente en el libro de bitácora."
						+ "    <b>El Cliente lo recibe, bajo el entendido de que esto será considerado efectivo"
						+ "    una vez que se cumplan los detalles y compromisos registrados en el Checklist adjunto y"
						+ "    realizado en el recorrido, siendo los Gerentes de la tienda quiénes deberán asegurar"
						+ "    el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia"
						+ "    de los trabajos finalizados para continuar y programar la siguiente validación."
						+ "    Las evidencias tendrán que enviarse, a más tardar 4 días antes de cumplirse la semana 26"
						+ "    del proceso de obra (considerando el día de la presente validación).</b>";
				break;
			case 5:
				texto = "Realizado el recorrido por el área de construcción,"
						+ "    revisados los trabajos efectuados y ejecutados con base en los alcances autorizados,"
						+ "    la normatividad, especificaciones generales, especificaciones particulares de obra,"
						+ "    proyecto ejecutivo e indicaciones hechas oportunamente en el libro de bitácora."
						+ "    <b>El Cliente lo recibe bajo el entenido de que esto será considerado efectivo"
						+ "    una vez que se haya cumplido con los detalles y compromisos registrados en el Checklist adjunto y"
						+ "    realizado en el recorrido, siendo los Gerentes de la tienda quienes deberán asegurar"
						+ "    el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia"
						+ "    de los trabajos finalizados para continuar y programar la siguiente validación."
						+ "    Las evidencias tendrán que enviarse, a más tardar 4 días antes de cumplirse la semana 26"
						+ "    del proceso de obra (considerando el día de la presente validación).</b>";
				break;
			default:
				texto = "Realizado el recorrido por el área de construcción,"
						+ "    revisados los trabajos efectuados y ejecutados con base en los alcances autorizados,"
						+ "    la normatividad, especificaciones generales, especificaciones particulares de obra,"
						+ "    proyecto ejecutivo e indicaciones hechas oportunamente en el libro de bitácora."
						+ "    <b>El Cliente lo recibe bajo el entenido de que esto será considerado efectivo"
						+ "    una vez que se haya cumplido con los detalles y compromisos registrados en el Checklist adjunto y"
						+ "    realizado en el recorrido, siendo los Gerentes de la tienda quienes deberán asegurar"
						+ "    el cumplimiento antes mencionado. Así mismo, es imprescindible contar con la evidencia"
						+ "    de los trabajos finalizados para continuar y programar la siguiente validación."
						+ "    Las evidencias tendrán que enviarse, a más tardar 4 días antes de cumplirse la semana 26"
						+ "    del proceso de obra (considerando el día de la presente validación).</b>";
				break;
			}

			break;
		}

		return texto;

	}
	
	public List<String> generaImperdonables(String ceco, String fase, String proyecto) {
		
		String nombreFase = "";
		String nombreCeco = "";

		List<String> zonasHtml = null;

		try {
			HallazgosZonasHtmlBI hallazgosZonasHtmlBI = new HallazgosZonasHtmlBI();
			zonasHtml = hallazgosZonasHtmlBI.generaHtmlImperdonablesReporte(ceco, fase, proyecto, nombreFase, nombreCeco);

		} catch (Exception e) {
			zonasHtml = new ArrayList<>();
		}
		return zonasHtml;
			
	}
}