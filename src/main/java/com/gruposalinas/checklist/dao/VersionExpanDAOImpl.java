package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.VersionExpanDTO;
import com.gruposalinas.checklist.mappers.VersionesExpanRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class VersionExpanDAOImpl extends DefaultDAO implements VersionExpanDAO {

    private static Logger logger = LogManager.getLogger(VersionExpanDAOImpl.class);

    DefaultJdbcCall jdbcInsertaVers;
    DefaultJdbcCall jdbcEliminaVers;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneVers;
    DefaultJdbcCall jbdcActualizaVers;
    DefaultJdbcCall jbdcActualizaVers2;

    public void init() {

        jdbcInsertaVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSEXP")
                .withProcedureName("SP_INS_VERS");

        jdbcEliminaVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSEXP")
                .withProcedureName("SP_DEL_VERS");

        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSEXP")
                .withProcedureName("SP_SEL_DETALLE")
                .returningResultSet("RCL_VERS", new VersionesExpanRowMapper());

        jdbcObtieneVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSEXP")
                .withProcedureName("SP_SEL_VERS")
                .returningResultSet("RCL_VERS", new VersionesExpanRowMapper());

        jbdcActualizaVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSEXP")
                .withProcedureName("SP_ACT_VERS");

        jbdcActualizaVers2 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSEXP")
                .withProcedureName("SP_ACT_VERS2");

    }

    public int inserta(VersionExpanDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", bean.getIdCheck())
                .addValue("PA_VERSION", bean.getIdVers())
                .addValue("PA_STATUS", bean.getIdactivo())
                .addValue("PA_AUX", bean.getAux())
                .addValue("PA_OBS", bean.getObs());

        out = jdbcInsertaVers.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMVERSEXP.SP_INS_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDTABLA");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el Empleado");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean elimina(String idTab) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDTABLA", idTab);

        out = jdbcEliminaVers.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMVERSEXP.SP_DEL_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el dato(" + idTab + ")");
        } else {
            return true;
        }

        return false;

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<VersionExpanDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<VersionExpanDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneTodo.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMVERSEXP.SP_SEL_DETALLE}");

        listaFijo = (List<VersionExpanDTO>) out.get("RCL_VERS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los  datos");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //parametro

    @SuppressWarnings("unchecked")
    public List<VersionExpanDTO> obtieneDatos(int idVers) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<VersionExpanDTO> listaN = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDVERS", idVers);

        out = jdbcObtieneVers.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMVERSEXP.SP_SEL_VERS}");
        //lleva el nombre del cursor del procedure
        listaN = (List<VersionExpanDTO>) out.get("RCL_VERS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la version(" + idVers + ")");
        }
        return listaN;

    }

    public boolean actualiza(VersionExpanDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDTABLA", bean.getIdTab())
                .addValue("PA_CHECKLIST", bean.getIdCheck())
                .addValue("PA_VERSION", bean.getIdVers())
                .addValue("PA_STATUS", bean.getIdactivo())
                .addValue("PA_AUX", bean.getAux())
                .addValue("PA_OBS", bean.getObs());

        out = jbdcActualizaVers.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMVERSEXP.SP_ACT_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar  del  id( " + bean.getIdTab() + ")");
        } else {
            return true;
        }
        return false;

    }

    public boolean actualiza2(VersionExpanDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDTABLA", bean.getIdTab())
                .addValue("PA_CHECKLIST", bean.getIdCheck())
                .addValue("PA_VERSION", bean.getIdVers());

        out = jbdcActualizaVers2.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMVERSEXP.SP_ACT_VERS2}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdTab() + ")");
        } else {
            return true;
        }
        return false;

    }

}
