package com.gruposalinas.checklist.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.DepuraBitacoraDAO;
import com.gruposalinas.checklist.domain.DepuraBitacoraDTO;



public class DepuraBitacoraBI {
	
	@Autowired
	DepuraBitacoraDAO depuraBitacoraDAO;
	
	public List<DepuraBitacoraDTO> depuraBitacorasSem(int fecha){
		List<DepuraBitacoraDTO> respuesta = null;
		try {
			respuesta=depuraBitacoraDAO.depuraBitacorasSem(fecha);
		}
		catch(Exception e) {
			e.printStackTrace();
			respuesta = null;
		}
		
		return respuesta;
	}
}
