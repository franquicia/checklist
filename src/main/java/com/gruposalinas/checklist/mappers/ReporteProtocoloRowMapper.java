package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteDTO;

public class ReporteProtocoloRowMapper implements RowMapper<ReporteDTO>   {

	@Override
	public ReporteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		ReporteDTO reporteDTO = new ReporteDTO();
		
		reporteDTO.setFecha_r(rs.getString("FDFECHA"));
		reporteDTO.setIdUsuario(rs.getString("FIID_USUARIO"));
		reporteDTO.setNombreUsuario(rs.getString("FCNOMBRE"));
		reporteDTO.setIdPuesto(rs.getInt("FIID_PUESTO"));
		reporteDTO.setIdCanal(rs.getInt("FIID_CANAL"));
		reporteDTO.setPonderacion(rs.getString("PONDERACION"));
		reporteDTO.setCanal(rs.getString("CANAL"));
		reporteDTO.setPais(rs.getString("PAIS"));
		reporteDTO.setPuesto(rs.getString("FCDESCRIPCION"));
		reporteDTO.setIdCeco(rs.getString("FCID_CECO"));
		reporteDTO.setTerritorio(rs.getString("FCTERRITORIO"));
		reporteDTO.setZona(rs.getString("FCZONA"));
		reporteDTO.setRegional(rs.getString("FCREGIONAL"));
		reporteDTO.setSucursal(rs.getString("SUCURSAL"));
		reporteDTO.setNombreSucursal(rs.getString("NOMBRECC"));
		reporteDTO.setIdPregunta(rs.getString("FIID_PREGUNTA"));
		reporteDTO.setRespuesta(rs.getString("FCRESPUESTA"));
		reporteDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		reporteDTO.setIdModulo(rs.getString("FIID_MODULO"));
		
		return reporteDTO;
	}

}
