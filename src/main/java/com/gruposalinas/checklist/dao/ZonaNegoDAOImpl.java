package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ZonaNegoExpDTO;
import com.gruposalinas.checklist.mappers.ZonaNegoDetaRowMapper;
import com.gruposalinas.checklist.mappers.ZonaNegoRelaRowMapper;
import com.gruposalinas.checklist.mappers.ZonaNegoRowMapper;
import com.gruposalinas.checklist.mappers.ZonaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ZonaNegoDAOImpl extends DefaultDAO implements ZonaNegoDAO {

    private static Logger logger = LogManager.getLogger(ZonaNegoDAOImpl.class);
//nego
    DefaultJdbcCall jdbcInsertaNego;
    DefaultJdbcCall jdbcEliminaNego;
    DefaultJdbcCall jdbcObtieneTodoNego;
    DefaultJdbcCall jdbcObtieneNego;
    DefaultJdbcCall jbdcActualizaNego;
// zona
    DefaultJdbcCall jdbcInsertaZona;
    DefaultJdbcCall jdbcEliminaZona;
    DefaultJdbcCall jdbcObtieneTodoZona;
    DefaultJdbcCall jdbcObtieneZona;
    DefaultJdbcCall jbdcActualizaZona;
//relacion nego zona
    DefaultJdbcCall jdbcInsertaRela;
    DefaultJdbcCall jdbcEliminaRela;
    DefaultJdbcCall jbdcActualizaRela;
    DefaultJdbcCall jdbcObtieneRela;

    public void init() {

        jdbcInsertaNego = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEGOEXP")
                .withProcedureName("SP_INS_NEGO");

        jdbcEliminaNego = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEGOEXP")
                .withProcedureName("SP_DEL_NEGO");

        jdbcObtieneTodoNego = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEGOEXP")
                .withProcedureName("SP_DETA_NEGO")
                .returningResultSet("RCL_NEGO", new ZonaNegoDetaRowMapper());

        jdbcObtieneNego = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEGOEXP")
                .withProcedureName("SP_SEL_NEGO")
                .returningResultSet("RCL_NEGO", new ZonaNegoRowMapper());

        jbdcActualizaNego = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEGOEXP")
                .withProcedureName("SP_ACT_NEGO");
//zonas
        jdbcInsertaZona = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEGOEXP")
                .withProcedureName("SP_INS_ZON");

        jdbcEliminaZona = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEGOEXP")
                .withProcedureName("SP_DEL_ZONA");

        jdbcObtieneTodoZona = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEGOEXP")
                .withProcedureName("SP_DETA_NEGOZONA")
                .returningResultSet("RCL_NEGO", new ZonaRowMapper());

        jdbcObtieneZona = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEGOEXP")
                .withProcedureName("SP_SEL_NEGOZONA")
                .returningResultSet("RCL_NEGO", new ZonaRowMapper());

        jbdcActualizaZona = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEGOEXP")
                .withProcedureName("SP_ACT_ZON");
        //relacion
        jdbcInsertaRela = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEGOEXP")
                .withProcedureName("SP_INS_NEGOZON");

        jdbcEliminaRela = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEGOEXP")
                .withProcedureName("SP_DEL_NEGOZON");

        jbdcActualizaRela = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEGOEXP")
                .withProcedureName("SP_ACT_NEGOZON");

        jdbcObtieneRela = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMNEGOEXP")
                .withProcedureName("SP_REL_NEGOZON")
                .returningResultSet("RCL_NEGO", new ZonaNegoRelaRowMapper());

    }

    public int insertaNego(ZonaNegoExpDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDNEGO", bean.getIdNegocio())
                //NOMENCLATURA
                .addValue("PA_DESC", bean.getNegocio())
                .addValue("PA_OBS", bean.getObs())
                //DESCRIPCION
                .addValue("PA_DETALLE", bean.getDescNego());

        out = jdbcInsertaNego.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMNEGOEXP.SP_INS_NEGO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_TAB");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el Empleado");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean eliminaNego(String idTabNegocio) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NEGO", idTabNegocio);

        out = jdbcEliminaNego.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMNEGOEXP.SP_DEL_NEGO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el negocio id(" + idTabNegocio + ")");
        } else {
            return true;
        }

        return false;

    }

    //sin parametro
    //sin parametro
    @SuppressWarnings("unchecked")
    public List<ZonaNegoExpDTO> obtieneInfoNego(String negocio) throws Exception {

        Map<String, Object> out = null;
        List<ZonaNegoExpDTO> listaFijo = null;
        int error = 0;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NEGO", negocio);
        out = jdbcObtieneTodoNego.execute(in);
        logger.info("Funci?n ejecutada: {FRANQUICIA.PAADMNEGOEXP.SP_DETA_NEGO}");
        listaFijo = (List<ZonaNegoExpDTO>) out.get("RCL_NEGO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();
        if (error != 1) {
            logger.info("Algo ocurrió negocios");
        } else {
            return listaFijo;
        }
        return listaFijo;
    }
    //parametro

    @SuppressWarnings("unchecked")
    public List<ZonaNegoExpDTO> obtieneDatosNego(String idNegocio) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ZonaNegoExpDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NEGO", idNegocio);

        out = jdbcObtieneNego.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMNEGOEXP.SP_SEL_NEGO}");
        //lleva el nombre del cursor del procedure
        lista = (List<ZonaNegoExpDTO>) out.get("RCL_NEGO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el empleado(" + idNegocio + ")");
        }
        return lista;

    }

    public boolean actualizaNego(ZonaNegoExpDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_TAB", bean.getIdTabNegocio())
                .addValue("PA_IDNEGO", bean.getIdNegocio())
                //NOMENCLATURA
                .addValue("PA_DESC", bean.getNegocio())
                .addValue("PA_OBS", bean.getObs())
                //DESCRIPCION
                .addValue("PA_DETALLE", bean.getDescNego());

        out = jbdcActualizaNego.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMNEGOEXP.SP_ACT_NEGO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar negocio ( " + bean.getIdNegocio() + ")");
        } else {
            return true;
        }
        return false;

    }

    @Override
    public int insertaZona(ZonaNegoExpDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                //NOMENCLATURA
                .addValue("PA_ZONA", bean.getZona())
                .addValue("PA_OBS", bean.getObs())
                //DESCRIPCION
                .addValue("PA_DETALLE", bean.getDescZona())
                .addValue("PA_AUX2", bean.getAux());

        out = jdbcInsertaZona.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMNEGOEXP.SP_INS_NEGO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDZONA");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar zona");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    @Override
    public boolean eliminaZona(String idZona) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDZONA", idZona);

        out = jdbcEliminaZona.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMNEGOEXP.SP_DEL_ZONA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el negocio id(" + idZona + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<ZonaNegoExpDTO> obtieneDatosZona(String idZona) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ZonaNegoExpDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ZONA", idZona);

        out = jdbcObtieneZona.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMNEGOEXP.SP_SEL_ZONA}");
        //lleva el nombre del cursor del procedure
        lista = (List<ZonaNegoExpDTO>) out.get("RCL_NEGO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el empleado(" + idZona + ")");
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<ZonaNegoExpDTO> obtieneInfoZona() throws Exception {
        Map<String, Object> out = null;
        List<ZonaNegoExpDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneTodoZona.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMNEGOEXP.SP_DETA_ZONA}");

        listaFijo = (List<ZonaNegoExpDTO>) out.get("RCL_NEGO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió negocios");
        } else {
            return listaFijo;
        }

        return listaFijo;
    }

    @Override
    public boolean actualizaZona(ZonaNegoExpDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDZONA", bean.getIdZona())
                //NOMENCLATURA
                .addValue("PA_ZONA", bean.getZona())
                .addValue("PA_OBS", bean.getObs())
                //DESCRIPCION
                .addValue("PA_DETALLE", bean.getDescZona())
                .addValue("PA_AUX2", bean.getAux());

        out = jdbcInsertaZona.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMNEGOEXP.SP_INS_NEGO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar negocio ( " + bean.getIdNegocio() + ")");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public int insertaRela(ZonaNegoExpDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDNEGO", bean.getIdNegocio())
                .addValue("PA_ZONA", bean.getIdZona());

        out = jdbcInsertaRela.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMNEGOEXP.SP_INS_NEGOZON}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDTAB");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar Relacion zonaNegocio");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    @Override
    public boolean eliminaRela(String idRela) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDTAB", idRela);

        out = jdbcEliminaRela.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMNEGOEXP.SP_DEL_NEGOZON}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el negocio id(" + idRela + ")");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaRela(ZonaNegoExpDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDTAB", bean.getIdRela())
                .addValue("PA_IDNEGO", bean.getIdNegocio())
                .addValue("PA_ZONA", bean.getIdZona());

        out = jbdcActualizaRela.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMNEGOEXP.SP_ACT_NEGOZON}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al el id relacion ( " + bean.getIdRela() + ")");
        } else {
            return true;
        }
        return false;

    }

    @SuppressWarnings("unchecked")
    public List<ZonaNegoExpDTO> obtieneInfoRela() throws Exception {
        Map<String, Object> out = null;
        List<ZonaNegoExpDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneRela.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMNEGOEXP.SP_REL_NEGOZON}");

        listaFijo = (List<ZonaNegoExpDTO>) out.get("RCL_NEGO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al traer relacion negocios_zona");
        } else {
            return listaFijo;
        }

        return listaFijo;
    }

}
