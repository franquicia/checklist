package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.ActorEdoHallaDAO;
import com.gruposalinas.checklist.domain.ActorEdoHallaDTO;

public class ActorEdoHallaBI {

	private static Logger logger = LogManager.getLogger(ActorEdoHallaBI.class);

private List<ActorEdoHallaDTO> listafila;
	
	@Autowired
	ActorEdoHallaDAO actorEdoHallaDAO;
		
	
	public int insertaActorHalla(ActorEdoHallaDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = actorEdoHallaDAO.insertaActorHalla(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaActorHalla(int idEmpFijo){
		boolean respuesta = false;
		
		try {
			respuesta = actorEdoHallaDAO.eliminaActorHalla(idEmpFijo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<ActorEdoHallaDTO> obtieneDatosActorHalla( String idUsuario) {
		
		try {
			listafila =  actorEdoHallaDAO.obtieneDatosActorHalla(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	
	
	public boolean actualizaActorHalla(ActorEdoHallaDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = actorEdoHallaDAO.actualizaActorHalla(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public int insertaConfEdoHalla(ActorEdoHallaDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = actorEdoHallaDAO.insertaConfEdoHalla(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaConfEdoHalla(int idEmpFijo){
		boolean respuesta = false;
		
		try {
			respuesta = actorEdoHallaDAO.eliminaConfEdoHalla(idEmpFijo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<ActorEdoHallaDTO> obtieneDatosConfEdoHalla( String idUsuario) {
		
		try {
			listafila =  actorEdoHallaDAO.obtieneDatosConfEdoHalla(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	
	
	public boolean actualizaConfEdoHalla(ActorEdoHallaDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = actorEdoHallaDAO.actualizaConfEdoHalla(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
}