package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.PosiblesDAO;
import com.gruposalinas.checklist.domain.PosiblesDTO;

public class PosiblesBI {

	private static Logger logger = LogManager.getLogger(PosiblesBI.class);
	
	@Autowired
	PosiblesDAO posiblesDAO;

	List<PosiblesDTO> listaPosible= null;
	
	public List<PosiblesDTO> buscaPosible() throws Exception {

		listaPosible = posiblesDAO.buscaPosible();

		return listaPosible;
	}
	
	public List<PosiblesDTO> buscaPosibleId(int idPosible) throws Exception {

		listaPosible = posiblesDAO.buscaPosibleId(idPosible);

		return listaPosible;
	}
	
	public int insertaPosible(String posible){
		
		int idPosible = 0;
		
		try {
			idPosible = posiblesDAO.insertaPosible(posible);
		} catch (Exception e) {
			logger.info("No fue posible insertar el Posible");
		}
		
		return idPosible;
		
	}
	
	public boolean actualizaPosible(PosiblesDTO bean){
		
		boolean respuesta = false ;
		
		try {
			respuesta = posiblesDAO.actualizaPosible(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar la Posible Respuesta");
		}
		
		return respuesta;
	}
	
	public boolean eliminaPosible(int idPosible){
		
		boolean respuesta = false ;
		
		try {
			respuesta = posiblesDAO.eliminaPosible(idPosible);
		} catch (Exception e) {
			logger.info("No fue posible insertar la Posible Respuesta");
		}
		
		return respuesta;		
	}

}

