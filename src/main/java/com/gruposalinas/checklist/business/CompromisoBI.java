package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.CompromisoDAO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class CompromisoBI {

	private static Logger logger = LogManager.getLogger(CompromisoBI.class);

	@Autowired
	CompromisoDAO compromisoDAO;
	
	List<CompromisoDTO> listaCompromisos = null;
	List<CompromisoDTO> listaCompromiso = null;
	

	public List<CompromisoDTO> obtieneCompromiso(){
				
		try{
			listaCompromisos = compromisoDAO.obtieneCompromiso();
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Compromiso");
					
		}
				
		return listaCompromisos;
	}
	
	public List<CompromisoDTO> obtieneCompromiso(int idCompromiso){
				
		try{
			listaCompromiso = compromisoDAO.obtieneCompromiso(idCompromiso);	
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Compromiso");
			
		}
				
		return listaCompromiso;
	}	
	
	public int insertaCompromiso(CompromisoDTO compromiso) {
		
		int idCompromiso = 0;
		
		try {		
			idCompromiso = compromisoDAO.insertaCompromiso(compromiso);		
		} catch (Exception e) {
			logger.info("No fue posible insertar el Compromiso");
			
		}
		
		return idCompromiso;		
	}
	
	public boolean actualizaCompromiso (CompromisoDTO compromiso){
		
		boolean respuesta = false;
				
		try {
			respuesta = compromisoDAO.actualizaCompromiso(compromiso);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Compromiso");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaCompromiso(int idCompromiso){
		
		boolean respuesta = false;
		
		try{
			respuesta = compromisoDAO.eliminaCompromiso(idCompromiso);
		}catch(Exception e){
			logger.info("No fue posible borrar el Compromiso");
			
		}
		
		return respuesta;
	}
}
