package com.gruposalinas.checklist.domain;

public class ReporteChecksExpDTO {
	


		private int bitacora;
		private int idRespuesta;
		private int idArbol;
		private String observ;
		private  int idPreg;
		private int idcheck;
		private String respuesta;
		private int idobserv;
		private String posible ;
		private String pregunta;
		private int imperdon;
		private int checkusua;
		private int usuario;
		private String fecha;
		private String ceco;
		
		public int getBitacora() {
			return bitacora;
		}
		public void setBitacora(int bitacora) {
			this.bitacora = bitacora;
		}
		public int getIdRespuesta() {
			return idRespuesta;
		}
		public void setIdRespuesta(int idRespuesta) {
			this.idRespuesta = idRespuesta;
		}
		public int getIdArbol() {
			return idArbol;
		}
		public void setIdArbol(int idArbol) {
			this.idArbol = idArbol;
		}
		public String getObserv() {
			return observ;
		}
		public void setObserv(String observ) {
			this.observ = observ;
		}
		public int getIdPreg() {
			return idPreg;
		}
		public void setIdPreg(int idPreg) {
			this.idPreg = idPreg;
		}
		public int getIdcheck() {
			return idcheck;
		}
		public void setIdcheck(int idcheck) {
			this.idcheck = idcheck;
		}
		public String getRespuesta() {
			return respuesta;
		}
		public void setRespuesta(String respuesta) {
			this.respuesta = respuesta;
		}
		public int getIdobserv() {
			return idobserv;
		}
		public void setIdobserv(int idobserv) {
			this.idobserv = idobserv;
		}
		public String getPosible() {
			return posible;
		}
		public void setPosible(String posible) {
			this.posible = posible;
		}
		public String getPregunta() {
			return pregunta;
		}
		public void setPregunta(String pregunta) {
			this.pregunta = pregunta;
		}
		public int getImperdon() {
			return imperdon;
		}
		public void setImperdon(int imperdon) {
			this.imperdon = imperdon;
		}
		public int getCheckusua() {
			return checkusua;
		}
		public void setCheckusua(int checkusua) {
			this.checkusua = checkusua;
		}
		public int getUsuario() {
			return usuario;
		}
		public void setUsuario(int usuario) {
			this.usuario = usuario;
		}
		public String getFecha() {
			return fecha;
		}
		public void setFecha(String fecha) {
			this.fecha = fecha;
		}
		
		
		public String getCeco() {
			return ceco;
		}
		public void setCeco(String ceco) {
			this.ceco = ceco;
		}
		@Override
		public String toString() {
			return "ReporteChecksExpDTO [bitacora=" + bitacora + ", idRespuesta=" + idRespuesta + ", idArbol=" + idArbol
					+ ", observ=" + observ + ", idPreg=" + idPreg + ", idcheck=" + idcheck + ", respuesta=" + respuesta
					+ ", idobserv=" + idobserv + ", posible=" + posible + ", pregunta=" + pregunta + ", imperdon="
					+ imperdon + ", checkusua=" + checkusua + ", usuario=" + usuario + ", fecha=" + fecha + ", ceco="
					+ ceco + "]";
		}
		
		
		
		
	}