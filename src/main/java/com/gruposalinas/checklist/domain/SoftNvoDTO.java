package com.gruposalinas.checklist.domain;

public class SoftNvoDTO {
	

		//soft
		private int idSoft;
		private int idFase;
		private String ceco;
		private int idProyecto;
		private int bitacora;
		private int idUsuario;
		private double calificacion;
		private String aux;
		private String periodo;
		private int status;
		private int idCanal;
		private int idRecorrido;
		//calculos
		private int idCalculo;
		private String clasificacion;
		private int itemSi;
		private int itemNo;
		private int itemNa;
		private double itemrevisados;
		private int itemTotales;
		private int itemImperd;
		private double porcentajeReSi;
		private double porcentajeReNo;
		private double porcentajeToSi;
		private double porcentajeToNo;
		private double porcentajeToNA;
		private double ponderacionNA;
		private double ponderacionMaximaReal;
		private double ponderacionObtenidaReal;
		private double ponderacionMaximaCalculada;
		private double ponderacionObtenidaCalculada;
		private double calificacionReal;
		private double calificacionCalculada;
		private int idProgramacion;
		
		public int getIdSoft() {
			return idSoft;
		}
		public void setIdSoft(int idSoft) {
			this.idSoft = idSoft;
		}
		public int getIdFase() {
			return idFase;
		}
		public void setIdFase(int idFase) {
			this.idFase = idFase;
		}
		public String getCeco() {
			return ceco;
		}
		public void setCeco(String ceco) {
			this.ceco = ceco;
		}
		public int getIdProyecto() {
			return idProyecto;
		}
		public void setIdProyecto(int idProyecto) {
			this.idProyecto = idProyecto;
		}
		public int getBitacora() {
			return bitacora;
		}
		public void setBitacora(int bitacora) {
			this.bitacora = bitacora;
		}
		public int getIdUsuario() {
			return idUsuario;
		}
		public void setIdUsuario(int idUsuario) {
			this.idUsuario = idUsuario;
		}
		public double getCalificacion() {
			return calificacion;
		}
		public void setCalificacion(double calificacion) {
			this.calificacion = calificacion;
		}
		public String getAux() {
			return aux;
		}
		public void setAux(String aux) {
			this.aux = aux;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		public int getIdCalculo() {
			return idCalculo;
		}
		public void setIdCalculo(int idCalculo) {
			this.idCalculo = idCalculo;
		}
		public String getClasificacion() {
			return clasificacion;
		}
		public void setClasificacion(String clasificacion) {
			this.clasificacion = clasificacion;
		}
		public int getItemSi() {
			return itemSi;
		}
		public void setItemSi(int itemSi) {
			this.itemSi = itemSi;
		}
		public int getItemNo() {
			return itemNo;
		}
		public void setItemNo(int itemNo) {
			this.itemNo = itemNo;
		}
		public int getItemNa() {
			return itemNa;
		}
		public void setItemNa(int itemNa) {
			this.itemNa = itemNa;
		}
		public double getItemrevisados() {
			return itemrevisados;
		}
		public void setItemrevisados(double itemrevisados) {
			this.itemrevisados = itemrevisados;
		}
		public int getItemTotales() {
			return itemTotales;
		}
		public void setItemTotales(int itemTotales) {
			this.itemTotales = itemTotales;
		}
		public int getItemImperd() {
			return itemImperd;
		}
		public void setItemImperd(int itemImperd) {
			this.itemImperd = itemImperd;
		}
		public double getPorcentajeReSi() {
			return porcentajeReSi;
		}
		public void setPorcentajeReSi(double porcentajeReSi) {
			this.porcentajeReSi = porcentajeReSi;
		}
		public double getPorcentajeReNo() {
			return porcentajeReNo;
		}
		public void setPorcentajeReNo(double porcentajeReNo) {
			this.porcentajeReNo = porcentajeReNo;
		}
		public double getPorcentajeToSi() {
			return porcentajeToSi;
		}
		public void setPorcentajeToSi(double porcentajeToSi) {
			this.porcentajeToSi = porcentajeToSi;
		}
		public double getPorcentajeToNo() {
			return porcentajeToNo;
		}
		public void setPorcentajeToNo(double porcentajeToNo) {
			this.porcentajeToNo = porcentajeToNo;
		}
		public double getPorcentajeToNA() {
			return porcentajeToNA;
		}
		public void setPorcentajeToNA(double porcentajeToNA) {
			this.porcentajeToNA = porcentajeToNA;
		}
		public double getPonderacionNA() {
			return ponderacionNA;
		}
		public void setPonderacionNA(double ponderacionNA) {
			this.ponderacionNA = ponderacionNA;
		}
		public double getPonderacionMaximaReal() {
			return ponderacionMaximaReal;
		}
		public void setPonderacionMaximaReal(double ponderacionMaximaReal) {
			this.ponderacionMaximaReal = ponderacionMaximaReal;
		}
		public double getPonderacionObtenidaReal() {
			return ponderacionObtenidaReal;
		}
		public void setPonderacionObtenidaReal(double ponderacionObtenidaReal) {
			this.ponderacionObtenidaReal = ponderacionObtenidaReal;
		}
		public double getPonderacionMaximaCalculada() {
			return ponderacionMaximaCalculada;
		}
		public void setPonderacionMaximaCalculada(double ponderacionMaximaCalculada) {
			this.ponderacionMaximaCalculada = ponderacionMaximaCalculada;
		}
		public double getPonderacionObtenidaCalculada() {
			return ponderacionObtenidaCalculada;
		}
		public void setPonderacionObtenidaCalculada(double ponderacionObtenidaCalculada) {
			this.ponderacionObtenidaCalculada = ponderacionObtenidaCalculada;
		}
		public double getCalificacionReal() {
			return calificacionReal;
		}
		public void setCalificacionReal(double calificacionReal) {
			this.calificacionReal = calificacionReal;
		}
		public double getCalificacionCalculada() {
			return calificacionCalculada;
		}
		public void setCalificacionCalculada(double calificacionCalculada) {
			this.calificacionCalculada = calificacionCalculada;
		}
		
		public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		
	
		public int getIdCanal() {
			return idCanal;
		}
		public void setIdCanal(int idCanal) {
			this.idCanal = idCanal;
		}
		
		public int getIdRecorrido() {
			return idRecorrido;
		}
		public void setIdRecorrido(int idRecorrido) {
			this.idRecorrido = idRecorrido;
		}
		
		public int getIdProgramacion() {
			return idProgramacion;
		}
		public void setIdProgramacion(int idProgramacion) {
			this.idProgramacion = idProgramacion;
		}
		@Override
		public String toString() {
			return "SoftNvoDTO [idSoft=" + idSoft + ", idFase=" + idFase + ", ceco=" + ceco + ", idProyecto="
					+ idProyecto + ", bitacora=" + bitacora + ", idUsuario=" + idUsuario + ", calificacion="
					+ calificacion + ", aux=" + aux + ", periodo=" + periodo + ", status=" + status + ", idCanal="
					+ idCanal + ", idRecorrido=" + idRecorrido + ", idCalculo=" + idCalculo + ", clasificacion="
					+ clasificacion + ", itemSi=" + itemSi + ", itemNo=" + itemNo + ", itemNa=" + itemNa
					+ ", itemrevisados=" + itemrevisados + ", itemTotales=" + itemTotales + ", itemImperd=" + itemImperd
					+ ", porcentajeReSi=" + porcentajeReSi + ", porcentajeReNo=" + porcentajeReNo + ", porcentajeToSi="
					+ porcentajeToSi + ", porcentajeToNo=" + porcentajeToNo + ", porcentajeToNA=" + porcentajeToNA
					+ ", ponderacionNA=" + ponderacionNA + ", ponderacionMaximaReal=" + ponderacionMaximaReal
					+ ", ponderacionObtenidaReal=" + ponderacionObtenidaReal + ", ponderacionMaximaCalculada="
					+ ponderacionMaximaCalculada + ", ponderacionObtenidaCalculada=" + ponderacionObtenidaCalculada
					+ ", calificacionReal=" + calificacionReal + ", calificacionCalculada=" + calificacionCalculada
					+ ", idProgramacion=" + idProgramacion + "]";
		}
		
		
		
		
	}