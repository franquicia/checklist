package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.HallazgosMatrizDAO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;

public class HallagosMatrizBI {

	private static Logger logger = LogManager.getLogger(HallagosMatrizBI.class);

private List<HallazgosTransfDTO> listafila;
	
	@Autowired
	HallazgosMatrizDAO hallazgosMatrizDAO;
		
	
	public int insertaHallazgoMatriz(HallazgosTransfDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = hallazgosMatrizDAO.insertaHallazgoMatriz(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaHallazgoMatriz(String idMatriz){
		boolean respuesta = false;
		
		try {
			respuesta = hallazgosMatrizDAO.eliminaHallazgoMatriz(idMatriz);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<HallazgosTransfDTO> obtieneHallazgoMatriz( String tipoProyecto) {
		
		try {
			listafila =  hallazgosMatrizDAO.obtieneHallazgoMatriz(tipoProyecto);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	

	
	public boolean actualizaHallazgoMatriz(HallazgosTransfDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = hallazgosMatrizDAO.actualizaHallazgoMatriz(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	

}