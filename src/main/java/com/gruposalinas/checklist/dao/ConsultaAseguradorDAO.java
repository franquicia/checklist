package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ConsultaAseguradorDTO;

public interface ConsultaAseguradorDAO {
	
	public List<ConsultaAseguradorDTO> obtieneConsultaAsegurador() throws Exception;
	public List<ConsultaAseguradorDTO> obtieneConsultaSucursal(ConsultaAseguradorDTO bean) throws Exception;
	public List<ConsultaAseguradorDTO> obtieneConsultaBitaCerr(ConsultaAseguradorDTO bean) throws Exception;
	public List<ConsultaAseguradorDTO> obtieneProtocolo(ConsultaAseguradorDTO bean) throws Exception;
	public List<ConsultaAseguradorDTO> obtieneDetalleBitacora(ConsultaAseguradorDTO bean) throws Exception;

}
