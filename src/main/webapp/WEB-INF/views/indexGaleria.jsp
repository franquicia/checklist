<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>Galer&iacute;a de evidencias</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervisionPI2/estilos.css">

  
</head>

<body ng-app="app" ng-controller="validarForm">

  <div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="${pageContext.request.contextPath}/img/supervisionPI2/btn_hamburguer.svg" alt="Menú principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="inicio.htm"><img src="${pageContext.request.contextPath}/img/supervisionPI2/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.html" class="selMenu">
          <div>Salir</div>
          <div><img src="${pageContext.request.contextPath}/img/supervisionPI2/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="indexGaleria.htm">Galer&iacute;a de evidencias</a> 
		</div>
    	B&uacute;squeda Ceco
    </div>

	<!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervisionPI2/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Men&uacute; Principal<br>
          </div>
        </div>
        <%-- <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la página">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> --%>
        <div id="menu">
        
        <c:if test="${menu==1}">
        	<ul class="l-navegacion nivel1">
				<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galer&iacute;a de Evidencias</div></a>
				</li>
			</ul>
        </c:if>
        <c:if test="${menu==0}">
	        <tiles:insertTemplate template="/WEB-INF/templates/templateMenuAseguramiento.jsp" flush="true">
				<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuAseguramiento.jsp" />
			</tiles:insertTemplate>
        	<!-- 
        	 <ul class="l-navegacion nivel1">
				<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Asegurador</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>B&uacute;squeda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>B&uacute;squeda Asegurador</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galer&iacute;a de Evidencias</div></a>
				</li>
				 <li>
					<a href="#"><div>Administrador</div></a>
					<ul class="nivel2" style="height: 350px; overflow-y: auto;">
			            <li><a href="asignaciones.htm"><div>Asignación de Perfiles</div></a></li>
			            <li><a href="asignaPersonal.htm"><div>Asignación de Personal</div></a></li>
			            <li><a href="asignaCecos.htm"><div>Asignación de Cecos</div></a></li>
			            <li><a href="asignaCecosMasiva.htm"><div>Asignación de Cecos Masiva</div></a></li>
		          	</ul>
				</li> 
			</ul>-->
        </c:if>
          
        </div>
      </div>
    </div>

     <!-- Contenido -->
    <div class="contSecc">
    
    	<div class="liga">
			<a href="inicio.htm" class="liga">
			<img style="height: 15px; width: 15px;" src="${pageContext.request.contextPath}/img/supervision/arrowmleft.png">
			<span>P&Aacute;GINA PRINCIPAL</span>
			</a>
		</div>
    
		<div class="titSec">Selecciona los criterios para realizar una b&uacute;squeda Ceco y poder visualizar las evidencias fotogr&aacute;ficas.</div>
		<div class="gris">
		<c:url value="/central/indexGaleriaCeco.htm" var="envia" />
		<form:form method="POST" action="${envia}" model="command" name="form" id="form">
			<div class="divCol4 flexJusCen">
				<div class="col4">
					Ceco<span class="obliga">*</span><br>
					<input id="ceco" type="text" placeholder="Ingrese el Ceco" value="${idCeco}" onclick="" maxlength="50" list="datalist" onkeyup="ac(this.value)" >
					
					<datalist id="datalist"> 
					<%-- <c:forEach var="listaCecos2" items="${listaCecos}">
									<option value="${listaCecos2}">
									</option>
					</c:forEach> --%>
					  
					<!-- This data list will be edited through javascript     -->
					</datalist>
					
					<script type="text/javascript">
						var tags = new Array();
						<c:forEach var="listaCecos2" items="${listaCecos}">
							tags.push("${listaCecos2}"); 
						</c:forEach>
						 /*list of available options*/ 
					     var n= tags.length; //length of datalist tags 

					     //alert(n + " array - "+tags);    
					  
					     function ac(value) { 
					        document.getElementById('datalist').innerHTML = ''; 
					         //setting datalist empty at the start of function 
					         //if we skip this step, same name will be repeated 
					           
					         l=value.length; 
					         //input query length 
					         
					         if(l>=3){
					        	 //alert(l);
							     for (var i = 0; i<n; i++) { 
							         if(((tags[i].toLowerCase()).indexOf(value.toLowerCase()))>-1) 
							         { 
							             //comparing if input string is existing in tags[i] string 
							  
							             var node = document.createElement("option"); 
							             var val = document.createTextNode(tags[i]); 
							              node.appendChild(val); 
							  
							               document.getElementById("datalist").appendChild(node); 
							               //alert("entro");
							                   //creating and appending new elements in data list 
							             } 
							         } 
					         }
					     } 
					</script>
					
				</div>
				<div class="col4 col4B">
					Fecha de inicio:<span class="obliga">*</span><br>
					<input type="text" readonly="readonly" placeholder="DD/MM/AAAA" class="datapicker1 date" id="datepicker" autocomplete="off" value="${fechaInicio}" maxlength="10">
				</div>
				<div class="col4 w30A"><div><strong>a</strong></div></div>
				<div class="col4 col4B">
					Fecha de fin:<span class="obliga">*</span><br>
					<input type="text" readonly="readonly" placeholder="DD/MM/AAAA" class="datapicker1 date" id="datepicker1" autocomplete="off" value="${fechaFin}" maxlength="10">
				</div>					
			</div>
			<div class="divCol4 flexJusCen">
				<div class="col4">
					Zona (&Aacute;rea de la sucursal):<br>
					<select id="Protocolo" >
						  <c:forEach var="listaProtocolos" items="${listaProtocolos}">
									<option value="${listaProtocolos.idChecklist}">
										<c:out value="${listaProtocolos.protocolo}"></c:out>
									</option>
						  </c:forEach>
					</select>
				</div>
				<div class="col4">
					Protocolo:<br>
					<select id="Checklist" >
						  <c:forEach var="listaChecklist" items="${listaChecklist}">
									<option value="${listaChecklist.idChecklist}">
										<c:out value="${listaChecklist.protocolo}"></c:out>
									</option>
						  </c:forEach>
					</select>
				</div>
			</div>
			<div class="btnCenter">
				<a href="#" class="btn btnBuscarS" onclick="return validaFechas();">Buscar</a>
			</div>
			
			<input id="idCeco" name="idCeco" type="hidden" value="${idCeco}" />
			<input id="idChecklist" name="idChecklist" type="hidden" value="${idChecklist}"/>
			<input id="fechaInicio" name="fechaInicio" type="hidden" value="${fechaInicio}"/>
			<input id="fechaFin" name="fechaFin" type="hidden" value="${fechaFin}" />
			
			</form:form>
		</div>
		
		<c:choose>
		
		<c:when test="${paso == 1}"> <!-- Solo filtros obligatorios -->
		
		<div class="divHideS current">
		
		<input id="buscar" maxlength="50" type="text" class="form-control" placeholder="B&uacute;squeda" />
		<br><br>
		
			<div class="titSec">Coincidencias de tu b&uacute;squeda</div>
			<div class="gris">
				<div class="scroll">
					<table id="resultados" class="tblGeneral tblSucursal">
						<tbody>
							<tr>
								<th>Ceco</th>
								<th>Sucursal</th>
								<th>Fechas de Coincidencia</th>
								<th>Protocolo</th>
								<th>Evidencias Fotogr&aacute;ficas</th>
							</tr>
							<c:url value="/central/visitaSeleccionada.htm" var="envioDetalle" />
								<form:form method="POST" action="${envioDetalle}" model="command" name="formEvidencia" id="formEvidencia">
								
								<input id="idCeco1" name="idCeco1" type="hidden" value=""/>
								<input id="idBitacora" name="idBitacora" type="hidden" value=""/>
								<input id="idCheck" name="idCheck" type="hidden" value=""/>
								<input id="nomChecklist" name="nomChecklist" type="hidden" value=""/>
								<input id="nomSucursal" name="nomSucursal" type="hidden" value=""/>
								<input id="fecha" name="fecha" type="hidden" value=""/>
								
							<c:forEach var="listaDatos" items="${listaDatos}">
							<tr>
											
								
									<td>${listaDatos.ceco}</td>
									<td>${listaDatos.sucursal}</td>
									<td>${listaDatos.fecha}</td>
									<td>${listaDatos.protocolo}</td>
									<td class="liga">
										<a href="#" onclick="javascript:enviarDatos('${listaDatos.idBitacora}','${listaDatos.ceco}', '${listaDatos.idChecklist}',
										'${listaDatos.sucursal}','${listaDatos.protocolo}','${listaDatos.fecha}','${paso}');">
										<img src="${pageContext.request.contextPath}/img/supervisionPI2/imgIco.svg"></a>
									</td>
									
								
												
							</tr>
							</c:forEach>
							</form:form>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		</c:when>
				
		<c:when test="${paso == 2}"> <!-- Obligatorios + protocolo seleccionado -->
		<div class="divHideSV current">	
		
		<input id="buscar" maxlength="50" type="text" class="form-control" placeholder="B&uacute;squeda" />
		<br><br>
		
			<div class="titSec">Coincidencias de tu b&uacute;squeda</div>
			<div class="gris">
				<div class="scroll">
					<table id="resultados" class="tblGeneral tblSucursalV">
						<tbody>
							<tr>
								<th>Ceco</th>
								<th>Sucursal</th>
								<th>Fechas de Coincidencia</th>
								<th>Protocolo</th>
								<th>Evidencias Fotogr&aacute;ficas</th>
							</tr>
							<c:forEach var="listaDatos" items="${listaDatos}">
							<tr>
											
								<c:url value="/central/visitaSeleccionada.htm" var="envio" />
								<form:form method="POST" action="${envio}" model="command" name="formEvidencia" id="formEvidencia">
									<td>${listaDatos.ceco}</td>
									<td>${listaDatos.sucursal}</td>
									<td>${listaDatos.fecha}</td>
									<td>${listaDatos.protocolo}</td>
									<td class="liga">
										<a href="#" onclick="javascript:enviarDatos('${listaDatos.idBitacora}','${listaDatos.ceco}', '${listaDatos.idChecklist}',
										'${listaDatos.sucursal}','${listaDatos.protocolo}','${listaDatos.fecha}','${paso}');">
										<img src="${pageContext.request.contextPath}/img/supervisionPI2/imgIco.svg"></a>
									</td>
									<input id="idCeco2" name="idCeco2" type="hidden" value=""/>
									<input id="idBitacora" name="idBitacora" type="hidden" value=""/>
									<input id="idCheck" name="idCheck" type="hidden" value=""/>
									<input id="nomChecklist" name="nomChecklist" type="hidden" value=""/>
									<input id="nomSucursal" name="nomSucursal" type="hidden" value=""/>
									<input id="fecha" name="fecha" type="hidden" value=""/>
								</form:form>
												
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		</c:when>
		
		<c:when test="${paso == 3}"> <!-- Obligatorios + zona seleccionada -->
		<div class="divHideSV current">		
		
		<input id="buscar" maxlength="50" type="text" class="form-control" placeholder="B&uacute;squeda" />
		<br><br>
		
			<div class="titSec">Coincidencias de tu b&uacute;squeda</div>
			<div class="gris">
				<div class="scroll">
					<table id="resultados" class="tblGeneral tblSucursalV">
						<tbody>
							<tr>
								<th>Ceco</th>
								<th>Sucursal</th>
								<th>Fechas de Coincidencia</th>
								<th>Zona (&Aacute;rea de la sucursal)</th>
								<th>Evidencias Fotogr&aacute;ficas</th>
							</tr>
							<c:forEach var="listaDatos" items="${listaDatos}">
							<tr>
											
								<c:url value="/central/visitaSeleccionada.htm" var="envio" />
								<form:form method="POST" action="${envio}" model="command" name="formEvidencia" id="formEvidencia">
									<td>${listaDatos.ceco}</td>
									<td>${listaDatos.sucursal}</td>
									<td>${listaDatos.fecha}</td>
									<td>${listaDatos.protocolo}</td>
									<td class="liga">
										<a href="#" onclick="javascript:enviarDatos('${listaDatos.idBitacora}','${listaDatos.ceco}', '${listaDatos.idChecklist}',
										'${listaDatos.sucursal}','${listaDatos.protocolo}','${listaDatos.fecha}','${paso}');">
										<img src="${pageContext.request.contextPath}/img/supervisionPI2/imgIco.svg"></a>
									</td>
									<input id="idCeco3" name="idCeco3" type="hidden" value=""/>
									<input id="idBitacora" name="idBitacora" type="hidden" value=""/>
									<input id="idCheck" name="idCheck" type="hidden" value=""/>
									<input id="nomChecklist" name="nomChecklist" type="hidden" value=""/>
									<input id="nomSucursal" name="nomSucursal" type="hidden" value=""/>
									<input id="fecha" name="fecha" type="hidden" value=""/>
								</form:form>
												
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		</c:when>
		
		<c:when test="${paso == 0}">
			<div class="divHideSV current">				
				<div class="titSec"><span ><b> NO SE ENCONTRARON DETALLES PARA CONSULTA.</b></span>
				<br><br>
				<div><b>No se encontraron resultados para esta consulta.</b></div>					
				</div>
			</div>
		</c:when>
	</c:choose>
				
	</div>	<!-- Fin conSecc -->
	
	<!-- Footer -->
	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualizaci&oacute;n Noviembre 2019</div>
	</div>
</div><!-- Fin page -->

</body>
</html>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervisionPI2/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervisionPI2/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervisionPI2/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervisionPI2/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/supervisionPI2/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervisionPI2/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supervisionPI2/custom-file-input.js"></script><!--Unpload -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervisionPI2/funciones.js"></script>

<script>
document.querySelector("#buscar").onkeyup = function(){
    $TableFilter("#resultados", this.value);
}

$TableFilter = function(id, value){
    var rows = document.querySelectorAll(id + ' tbody tr');
    for(var i = 0; i < rows.length; i++){
        var showRow = false;
        var row = rows[i];
        row.style.display = 'none';        

        for(var x = 0; x < row.childElementCount; x++){
            if(row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1){
                showRow = true;
                break;
            }
        }        

        if(showRow){
            row.style.display = null;
        }
    }
}
</script>

<script type="text/javascript">



function enviarDatos(idBitacora, idSucursal, idCheck, sucursal, protocolo, fecha, paso){
	//alert("Entra");
	document.getElementById("idBitacora").value = idBitacora;
	if(paso=="1"){
		document.getElementById("idCeco1").value = idSucursal;
		//alert("Entra3: "+document.getElementById("idCeco1").value);
	}else if(paso=="2"){
		document.getElementById("idCeco2").value = idSucursal;
		//alert("Entra4: "+document.getElementById("idCeco2").value);
	}else if(paso=="3"){
		document.getElementById("idCeco3").value = idSucursal;
		//alert("Entra5: "+document.getElementById("idCeco3").value); 
	}
	document.getElementById("idCheck").value = idCheck;
	document.getElementById("nomSucursal").value = sucursal;
	document.getElementById("nomChecklist").value = protocolo;
	document.getElementById("fecha").value = fecha;
	form = document.getElementById("formEvidencia");
	form.submit();
}

function validaFechas(){
	//alert("Paso");
	var f1=document.getElementById("datepicker").value;
	var f2=document.getElementById("datepicker1").value;

	var sucCompleta=document.getElementById("ceco").value;
	
	var ceco=sucCompleta.substr(0,6);

	//alert(ceco);
	var idP=document.getElementById("Protocolo").value;
	var idC=document.getElementById("Checklist").value;
	//alert("Filtro: "+sucCompleta);
 	var Fecha1 = stringToDate(f1,"dd/MM/yyyy","/");
 	var Fech2 = stringToDate(f2,"dd/MM/yyyy","/");
 	var FHoy = new Date();
	
    var flag1=false;
 	var flag2=false;



	if(sucCompleta == null){
		alert("Debes seleccionar un Ceco");
	} else {

	if (isNaN(Fecha1)){
		alert("Debes colocar fecha inicio");
		return false;
	}
	else{
		if(Fecha1<FHoy){
		flag1=true;
		}else{
			alert("La fecha inicio no puede ser mayor que hoy");
			return false;
		}
	}
	
	if (isNaN(Fech2)){
		alert("Debes colocar fecha fin");
		return false;
	}
	else{
		if(Fech2<FHoy){
			flag2=true;
			}else{
				alert("La fecha fin no puede ser mayor que hoy");
				return false;
			}
	} 

	if(flag1==true && flag2==true){
		if(Fecha1>Fech2){
			alert("La fecha de inicio no debe ser mayor que la de fin");
			return false;
		}else{

			document.getElementById('fechaInicio').value = f1;
			document.getElementById('fechaFin').value = f2;
			document.getElementById("idCeco").value = ceco;
			form = document.getElementById("form");
			
			if(idC=="0" && idP=="0"){
				document.getElementById("idChecklist").value = 0;	
				form.submit();
			} else if (idC=="0" && idP!="0"){
				document.getElementById("idChecklist").value = idP+"P";
				form.submit();
			} else if (idC!="0" && idP=="0"){
				document.getElementById("idChecklist").value = idC+"C";	
				form.submit();
			} else if (idC!="0" && idP!="0"){
				alert("Debe seleccionar Zona o Protocolo, no ambos.");
			}

		}
	  } 	
	}
	return false;	
}

function stringToDate(_date,_format,_delimiter){
            var formatLowerCase=_format.toLowerCase();
            var formatItems=formatLowerCase.split(_delimiter);
            var dateItems=_date.split(_delimiter);
            var monthIndex=formatItems.indexOf("mm");
            var dayIndex=formatItems.indexOf("dd");
            var yearIndex=formatItems.indexOf("yyyy");
            var month=parseInt(dateItems[monthIndex]);
            month-=1;
            var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
            return formatedDate;
}




</script>

