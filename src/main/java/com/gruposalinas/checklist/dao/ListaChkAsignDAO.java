package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ListaAsignChkDTO;
import java.util.List;
import java.util.Map;

public interface ListaChkAsignDAO {
	public List<ListaAsignChkDTO>getListaAsignaciones(int idUsuario) throws Exception ;

}
