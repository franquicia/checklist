package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ImagenesRespuestaDTO;
import com.gruposalinas.checklist.mappers.ImagenesRespuestaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ImagenesRespuestaDAOImpl extends DefaultDAO implements ImagenesRespuestaDAO {

    private Logger logger = LogManager.getLogger(ImagenesRespuestaDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneImagenes;
    private DefaultJdbcCall jdbcInsertaImagen;
    private DefaultJdbcCall jdbcEliminaImagen;

    public void init() {

        jdbcObtieneImagenes = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAIMAGENESRES")
                .withProcedureName("SP_CONSULTA_IMAGEN")
                .returningResultSet("RCL_CONSULTA", new ImagenesRespuestaRowMapper());

        jdbcInsertaImagen = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAIMAGENESRES")
                .withProcedureName("SP_INSERTA_IMAGEN");

        jdbcEliminaImagen = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAIMAGENESRES")
                .withProcedureName("SP_ELIMINA_IMAGEN");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ImagenesRespuestaDTO> obtieneImagenes(String idArbol, String idImagen) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        List<ImagenesRespuestaDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_ARBOL", idArbol).addValue("PA_IDIMAGEN", idImagen);

        out = jdbcObtieneImagenes.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAIMAGENESRES.SP_CONSULTA_IMAGEN}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        lista = (List<ImagenesRespuestaDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al consultar las imagenes del arbol (" + idArbol + ") y/o idImagen (" + idImagen + ")");
        }

        return lista;
    }

    @Override
    public int insertaImagen(ImagenesRespuestaDTO imagen) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        int idImagen = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_ARBOL", imagen.getIdArbol())
                .addValue("PA_URL", imagen.getUrlImg())
                .addValue("PA_DESCRIPCION", imagen.getDescripcion());

        out = jdbcInsertaImagen.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAIMAGENESRES.SP_INSERTA_IMAGEN}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        //System.out.println("returnEjecucion :" +returnEjecucion );
        BigDecimal returnId = (BigDecimal) out.get("PA_ID_IMAGEN");
        idImagen = returnId.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al insertar la imagen");
            return 0;
        }

        return idImagen;
    }

    @Override
    public boolean eliminaImagen(int idImagen) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_IMAGEN", idImagen);

        out = jdbcEliminaImagen.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAIMAGENESRES.SP_ELIMINA_IMAGEN}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al eliminar la imagen con idImagen (" + idImagen + ")");
            return false;
        }

        return true;
    }

}
