package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.AltaGerenteDTO;


public class AltaGerenteGetAllTableRowMapper implements RowMapper<AltaGerenteDTO> {

	public AltaGerenteDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AltaGerenteDTO altaGerenteGetDTO = new AltaGerenteDTO();
		
		altaGerenteGetDTO.setIdGerente(Integer.parseInt(rs.getString("FIGERENTEID")));
		altaGerenteGetDTO.setNomGerente(rs.getString("FCNOMGERENTE"));
		altaGerenteGetDTO.setIdUsuario(Integer.parseInt(rs.getString("FIUSUARIO")));
		altaGerenteGetDTO.setNomUsuario(rs.getString("FCNOMUSUARIO"));
		altaGerenteGetDTO.setZona(rs.getString("ZONA"));
		altaGerenteGetDTO.setRegion(rs.getString("REGION"));
		altaGerenteGetDTO.setIdPerfil(rs.getInt("FIID_PERFIL"));
		return altaGerenteGetDTO;
	}
}
