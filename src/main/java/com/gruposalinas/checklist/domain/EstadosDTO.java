package com.gruposalinas.checklist.domain;

public class EstadosDTO {
	
		private int idConsec;
		private int idPais;
		private int idEstado;
		private String descripcion;
		private String abreviatura;
		private String modificacion;
		
		public int getIdConsec() {
			return idConsec;
		}


		public void setIdConsec(int idConsec) {
			this.idConsec = idConsec;
		}


		public int getIdPais() {
			return idPais;
		}


		public void setIdPais(int idPais) {
			this.idPais = idPais;
		}


		public int getIdEstado() {
			return idEstado;
		}


		public void setIdEstado(int idEstado) {
			this.idEstado = idEstado;
		}


		public String getDescripcion() {
			return descripcion;
		}


		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}


		public String getAbreviatura() {
			return abreviatura;
		}


		public void setAbreviatura(String abreviatura) {
			this.abreviatura = abreviatura;
		}
		
		
		public String getModificacion() {
			return modificacion;
		}


		public void setModificacion(String modificacion) {
			this.modificacion = modificacion;
		}


		public String toString() {
			return "EstadosDTO [idConsec="+idConsec+",idPais="+idPais+",idEstado="+idEstado+",descripcion="+descripcion+",abreviatura="+abreviatura+",modificacion="+modificacion+"]";
		}


		
	}