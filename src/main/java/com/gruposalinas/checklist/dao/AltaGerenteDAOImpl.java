package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AltaGerenteDTO;
import com.gruposalinas.checklist.mappers.AltaGerenteGetAllTableRowMapper;
import com.gruposalinas.checklist.mappers.AltaGerenteRowMapper;
import com.gruposalinas.checklist.mappers.AltaGerenteUsuariosRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AltaGerenteDAOImpl extends DefaultDAO implements AltaGerenteDAO {

    private static Logger logger = LogManager.getLogger(AltaGerenteDAOImpl.class);

    DefaultJdbcCall jdbcInsertaGerente;
    DefaultJdbcCall jdbcObtieneIdGerente;
    DefaultJdbcCall jdbcObtieneIdUsuarios;
    DefaultJdbcCall jdbcActualizaDisSup;
    DefaultJdbcCall jdbcEliminaDisSup;
    DefaultJdbcCall jdbcObtieneTodos;

    public void init() {

        jdbcInsertaGerente = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAINSGERENTE")
                .withProcedureName("SPINSGERENTE");

        jdbcObtieneIdGerente = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAINSGERENTE")
                .withProcedureName("SPGETUSR")
                .returningResultSet("PA_CONSULTA", new AltaGerenteRowMapper());

        jdbcObtieneIdUsuarios = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAINSGERENTE")
                .withProcedureName("SPGETGERENTE")
                .returningResultSet("PA_CONSULTA", new AltaGerenteUsuariosRowMapper());

        jdbcActualizaDisSup = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAINSGERENTE")
                .withProcedureName("SPACTGERENTE");

        jdbcEliminaDisSup = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAINSGERENTE")
                .withProcedureName("SPDELGERENTE");

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAINSGERENTE")
                .withProcedureName("SPGETDISSUP")
                .returningResultSet("PA_CONSULTA", new AltaGerenteGetAllTableRowMapper());

    }

    public boolean insertaGerente(AltaGerenteDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIGERENTE", bean.getIdGerente())
                .addValue("PA_NOMGERENTE", bean.getNomGerente())
                .addValue("PA_FIUSUARIO", bean.getIdUsuario())
                .addValue("PA_NOMUSUARIO", bean.getNomUsuario())
                .addValue("PA_ZONA", bean.getZona())
                .addValue("PA_REGION", bean.getRegion())
                .addValue("PA_FIID_PERFIL", bean.getIdPerfil());

        out = jdbcInsertaGerente.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAINSGERENTE.SPINSGERENTE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar al gerente");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<AltaGerenteDTO> obtieneIdGerente(AltaGerenteDTO bean) throws Exception {

        Map<String, Object> out = null;
        List<AltaGerenteDTO> listId = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIUSUARIO", bean.getIdUsuario());

        out = jdbcObtieneIdGerente.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAINSGERENTE.SPGETUSR}");

        listId = (List<AltaGerenteDTO>) out.get("PA_CONSULTA");

        if (error == 0) {
            logger.info("Algo ocurrió al obtener el id");
        } else {
            return listId;
        }

        return listId;
    }

    @SuppressWarnings("unchecked")
    public List<AltaGerenteDTO> obtieneIdUsuarios(AltaGerenteDTO bean) throws Exception {

        Map<String, Object> out = null;
        List<AltaGerenteDTO> listId = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIGERENTE", bean.getIdGerente());

        out = jdbcObtieneIdUsuarios.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAINSGERENTE.SPGETGERENTE}");

        listId = (List<AltaGerenteDTO>) out.get("PA_CONSULTA");

        if (error == 0) {
            logger.info("Algo ocurrió al obtener la informacion");
        } else {
            return listId;
        }

        return listId;
    }

    public boolean actualizaDisSup(AltaGerenteDTO bean) throws Exception {

        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_SEC", bean.getIdSecuencia())
                .addValue("PA_FIGERENTE", bean.getIdGerente())
                .addValue("PA_NOMGERENTE", bean.getNomGerente())
                .addValue("PA_FIUSUARIO", bean.getIdUsuario())
                .addValue("PA_NOMUSUARIO", bean.getNomUsuario())
                .addValue("PA_ZONA", bean.getZona())
                .addValue("PA_REGION", bean.getRegion())
                .addValue("PA_FIID_PERFIL", bean.getIdPerfil());

        out = jdbcActualizaDisSup.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAINSGERENTE.SPACTGERENTE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al actualizar Informacion");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaDisSup(AltaGerenteDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIUSUARIO", bean.getIdUsuario());

        out = jdbcEliminaDisSup.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAINSGERENTE.SPDELGERENTE}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al borrar el Handbook");
        } else {
            return true;
        }

        return false;
    }

    /*@SuppressWarnings("unchecked")
@Override
public List<AltaGerenteDTO> obtienefullTable(AltaGerenteDTO bean) throws Exception {
	Map<String, Object> out = null;
	List<AltaGerenteDTO> listInformacion = null;

	SqlParameterSource in = new MapSqlParameterSource()
			.addValue("PA_FIGERENTE", bean.getIdGerente2())
			.addValue("PA_FIUSUARIO", bean.getIdUsuario2());

	out = jdbcObtieneTodos.execute(in);
	logger.info("Funcion ejecutada: {checklist.PAINSGERENTE.SPGETDISSUP}");
	listInformacion = (List<AltaGerenteDTO>) out.get("PA_CONSULTA");

	if(listInformacion.size() == 0){
		logger.info("Algo ocurrió al obtener la informacion");
	}else{
		return listInformacion;
	}
	return listInformacion;
}*/
    @SuppressWarnings("unchecked")
    @Override
    public List<AltaGerenteDTO> obtienefullTable(String idUsuario, String idGerente) throws Exception {
        Map<String, Object> out = null;
        List<AltaGerenteDTO> listInformacion = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIGERENTE", idGerente)
                .addValue("PA_FIUSUARIO", idUsuario);

        out = jdbcObtieneTodos.execute(in);
        logger.info("Funcion ejecutada: {checklist.PAINSGERENTE.SPGETDISSUP}");
        listInformacion = (List<AltaGerenteDTO>) out.get("PA_CONSULTA");

        if (listInformacion.size() == 0) {
            logger.info("Algo ocurrió al obtener la informacion");
        }

        return listInformacion;
    }

}
