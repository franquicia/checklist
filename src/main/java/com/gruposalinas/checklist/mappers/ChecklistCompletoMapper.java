package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistCompletoDTO;

public class ChecklistCompletoMapper implements RowMapper<ChecklistCompletoDTO> {

	
	public ChecklistCompletoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ChecklistCompletoDTO checklistCompleto = new ChecklistCompletoDTO();
		
		checklistCompleto.setOrdenPregunta(rs.getInt("FIORDEN_CHECK"));
		checklistCompleto.setIdArbolDesicion(rs.getInt("FIID_ARBOL_DES"));
		checklistCompleto.setRespuesta(rs.getString("FCRESPUESTA"));
		checklistCompleto.setEstatusEvidencia(rs.getInt("FIESTATUS_E"));
		checklistCompleto.setOrdenCheckRespuesta(rs.getInt("FCORDEN_RESP"));
		checklistCompleto.setIdTipoPregunta(rs.getInt("FIID_TIPO_PREG"));
		checklistCompleto.setCvePregunta(rs.getString("FCCLAVE_TIPO"));
		checklistCompleto.setDescripcionTipo(rs.getString("FCDESC_TIPO"));
		checklistCompleto.setIdModulo(rs.getInt("FIID_MODULO"));
		checklistCompleto.setNombreModulo(rs.getString("FCNOMBRE"));
		checklistCompleto.setIdModuloPadre(rs.getInt("FIID_MOD_PADRE"));
		checklistCompleto.setNombrePadre(rs.getString("FCMODPADRE"));
		checklistCompleto.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		checklistCompleto.setPregunta(rs.getString("FCDESCRIPCION"));
		checklistCompleto.setReqAccion(rs.getInt("FIREQACCION"));
		checklistCompleto.setObliga(rs.getInt("FIOBLIGA"));
		checklistCompleto.setReqObs(rs.getInt("FIREQOBS"));
		checklistCompleto.setDesEvidencia(rs.getString("FCDESCRIPCION_E"));
		return checklistCompleto;
		
	}
	

}
