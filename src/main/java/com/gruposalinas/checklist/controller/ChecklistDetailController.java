package com.gruposalinas.checklist.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ChecklistDetailController {

	@RequestMapping(value={"tienda/checklistDetail","central/checklistDetail"},method=RequestMethod.POST)
	public ModelAndView checkListDetail(HttpServletRequest requqest,HttpServletResponse response,Model model,
			@RequestParam(value = "idChecklist", required = true, defaultValue = "") String idCheck,
			@RequestParam(value = "nombreCheck", required = true, defaultValue = "") String nombreCheck)
			throws ServletException, IOException{
		ModelAndView mv = new ModelAndView("checklistDetails");
		mv.addObject("idCheck", idCheck);
		mv.addObject("nombreCheck", nombreCheck);
		return mv;
		
	}
	
	@RequestMapping(value={"tienda/checklistChangeDetail","central/checklistChangeDetail"},method=RequestMethod.POST)
	public ModelAndView checkListChangeDetail(HttpServletRequest requqest,HttpServletResponse response,Model model,
			@RequestParam(value = "idChecklist", required = true, defaultValue = "") String idCheck , 
			@RequestParam(value = "nombreCheck", required = true, defaultValue = "") String nombreCheck)
			throws ServletException, IOException{
		
		ModelAndView mv = new ModelAndView("checklistChangeDetails");
		mv.addObject("idCheck", idCheck);
		mv.addObject("nombreCheck", nombreCheck);
		return mv;
		
	}
}