package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.PosiblesTipoPreguntaDTO;
import com.gruposalinas.checklist.mappers.PosibleTipoPreguntaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PosibleTipoPreguntaDAOImpl extends DefaultDAO implements PosiblesTipoPreguntaDAO {

    private Logger logger = LogManager.getLogger(PosibleTipoPreguntaDAOImpl.class);

    private DefaultJdbcCall jdbcObtienePosibleTipoPregunta;
    private DefaultJdbcCall jdbcInsertaPosibleTipoPregunta;
    private DefaultJdbcCall jdbcActualizaPosibleTipoPregunta;
    private DefaultJdbcCall jdbcEliminaPosibleTipoPregunta;

    public void init() {

        jdbcObtienePosibleTipoPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPOSPREG")
                .withProcedureName("SP_SELXID")
                .returningResultSet("RCL_POSIBLES", new PosibleTipoPreguntaRowMapper());

        jdbcInsertaPosibleTipoPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPOSPREG")
                .withProcedureName("SP_INSPOSPREG");

        jdbcActualizaPosibleTipoPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPOSPREG")
                .withProcedureName("SP_ACTPOSPREG");

        jdbcEliminaPosibleTipoPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPOSPREG")
                .withProcedureName("SP_DELPOSPREG");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PosiblesTipoPreguntaDTO> obtienePosiblesRespuestas(String idTipoPregunta) throws Exception {

        Map<String, Object> out = null;
        List<PosiblesTipoPreguntaDTO> lista = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDTIPO", idTipoPregunta);

        out = jdbcObtienePosibleTipoPregunta.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPOSPREG.SP_SELXID}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        lista = (List<PosiblesTipoPreguntaDTO>) out.get("RCL_POSIBLES");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al consultar posibles respuestas");
        } else {
            return lista;
        }

        return null;
    }

    @Override
    public int insertaPosibleTipoPregunta(int idPosible, int idTipoPregunta) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        int idPosibleTipoPregunta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPOSIBLE", idPosible).addValue("PA_TIPPREG", idTipoPregunta);

        out = jdbcInsertaPosibleTipoPregunta.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPOSPREG.SP_INSPOSPREG}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        BigDecimal returnId = (BigDecimal) out.get("PA_IDPOSPREG");
        idPosibleTipoPregunta = returnId.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al insertar posibles respuestas por tipo de Pregunta");
        } else {
            return idPosibleTipoPregunta;
        }

        return 0;
    }

    @Override
    public boolean actualizaPosibleTipoPregunta(PosiblesTipoPreguntaDTO posibleTipoPreguntaDTO) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPOSPREG", posibleTipoPreguntaDTO.getIdPosibleTipoPregunta())
                .addValue("PA_IDPOSIBLE", posibleTipoPreguntaDTO.getIdPosibleRespuesta())
                .addValue("PA_TIPPREG", posibleTipoPreguntaDTO.getIdTipoPregunta());

        out = jdbcActualizaPosibleTipoPregunta.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPOSPREG.SP_ACTPOSPREG}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al actualizar posible tipo de Pregunta");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaPosibleTipoPregunta(int idPosibleTipoPregunta) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPOSPREG", idPosibleTipoPregunta);

        out = jdbcEliminaPosibleTipoPregunta.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPOSPREG.SP_DELPOSPREG}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al elimnar posibles tipo de Pregunta");
        } else {
            return true;
        }

        return false;
    }

}
