package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ProtocoloDTO;
import com.gruposalinas.checklist.mappers.ProtocoloRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ProtocoloDAOImpl extends DefaultDAO implements ProtocoloDAO {

    private static Logger logger = LogManager.getLogger(ProtocoloDAOImpl.class);

    DefaultJdbcCall jdbcObtieneProtocolo;
    DefaultJdbcCall jdbcInsertaProtocolo;
    DefaultJdbcCall jdbcActualizaProtocolo;
    DefaultJdbcCall jdbcEliminaProtocolo;
    DefaultJdbcCall jdbcObtieneArchiveroForm;

    public void init() {

        jdbcObtieneProtocolo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPROTOCOLO")
                .withProcedureName("SPGETPROTOCO")
                .returningResultSet("PA_CONSULTA", new ProtocoloRowMapper());

        jdbcInsertaProtocolo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPROTOCOLO")
                .withProcedureName("SPINSPROTOCOLO");

        jdbcActualizaProtocolo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPROTOCOLO")
                .withProcedureName("SPACTPROTOCOLO");

        jdbcEliminaProtocolo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPROTOCOLO")
                .withProcedureName("SPDELPROTOCOLO");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProtocoloDTO> obtieneProtocolo(int idProtocolo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ProtocoloDTO> listaProto = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PROTOCOLO", idProtocolo);

        out = jdbcObtieneProtocolo.execute(in);

        //logger.info("Función ejecutada: FRANQUICIA.PAADMPROTOCOLO.SPGETPROTOCO");
        listaProto = (List<ProtocoloDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el protocolo");
        } else {
            logger.info("Obtiene Protocolo: " + error);
        }

        return listaProto;
    }

    @Override
    public int insertaProtocolo(ProtocoloDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_PROTOCOLO", bean.getIdProtocolo())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_OBSERVACIONES", bean.getObservaciones())
                .addValue("PA_FCSTATUS", bean.getStatus())
                .addValue("PA_RETRO", bean.getStatusRetro());

        out = jdbcInsertaProtocolo.execute(in);

        //logger.info("Funcion ejecutada:{FRANQUICIA.PAADMPROTOCOLO.SPINSPROTOCOLO}");
        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el protocolo");
        } else {
            logger.info("Inserta Protocolo: " + error);
        }

        BigDecimal id = (BigDecimal) out.get("PA_FIID_PROTOCOLO");
        int idProtocolo = id.intValue();

        return idProtocolo;
    }

    @Override
    public boolean modificaProtocolo(ProtocoloDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_PROTOCOLO", bean.getIdProtocolo())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_OBSERVACIONES", bean.getObservaciones())
                .addValue("PA_FCSTATUS", bean.getStatus())
                .addValue("PA_RETRO", bean.getStatusRetro());

        out = jdbcActualizaProtocolo.execute(in);

        //logger.info("Funcion ejecutada:{FRANQUICIA.PAADMPROTOCOLO.SPACTPROTOCOLO}");
        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar el protocolo");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean eliminaProtocolo(int idProtocolo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_PROTOCOLO", idProtocolo);

        out = jdbcEliminaProtocolo.execute(in);

        //logger.info("Funcion ejecutada:{FRANQUICIA.PAADMPROTOCOLO.SPDELPROTOCOLO}");
        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();
        if (error != 1) {
            logger.info("Algo ocurrió al eliminar el protocolo");
        } else {
            return true;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProtocoloDTO> getProtocolos() throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        List<ProtocoloDTO> listaProto = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PROTOCOLO", null);

        out = jdbcObtieneProtocolo.execute(in);

        //logger.info("Función ejecutada: FRANQUICIA.PAADMPROTOCOLO.SPGETPROTOCO");
        listaProto = (List<ProtocoloDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el protocolo");
        } else {
            logger.info("Obtiene Protocolo: " + error);
        }

        return listaProto;
    }
}
