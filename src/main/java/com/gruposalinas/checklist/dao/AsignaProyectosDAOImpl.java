package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AsignaTransfDTO;
import com.gruposalinas.checklist.mappers.AsignaProyectoAdminRowMapper;
import com.gruposalinas.checklist.mappers.FasesAdministradorRowMapper;
import com.gruposalinas.checklist.mappers.ProyectosCecoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AsignaProyectosDAOImpl extends DefaultDAO implements AsignaProyectosDAO {

    private static Logger logger = LogManager.getLogger(AsignaProyectosDAOImpl.class);

    DefaultJdbcCall jdbcInsertaAsignProyecto;
    DefaultJdbcCall jdbcDesactivaAsignProyecto;
    DefaultJdbcCall jdbcObtieneProyectos;
    DefaultJdbcCall jdbcObtieneFases;
    DefaultJdbcCall jdbcObtieneAsignacionesProy;
    DefaultJdbcCall jbdcActualizaFase;
    DefaultJdbcCall jbdcObtieneDatosLiberaFase;

    public void init() {

        jdbcInsertaAsignProyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_SOPASIGPROY")
                .withProcedureName("SP_INS_ASIG");

        jdbcDesactivaAsignProyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGTRANSF")
                .withProcedureName("SP_DESACTIVAVERS");

        jdbcObtieneProyectos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_SOPASIGPROY")
                .withProcedureName("SP_SEL_PROYEC")
                .returningResultSet("RCL_PROY", new ProyectosCecoRowMapper());

        jdbcObtieneFases = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_SOPASIGPROY")
                .withProcedureName("SP_SEL_FASES")
                .returningResultSet("RCL_FASES", new FasesAdministradorRowMapper());

        jdbcObtieneAsignacionesProy = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_SOPASIGPROY")
                .withProcedureName("SP_SEL_ASIG")
                .returningResultSet("RCL_FASES", new AsignaProyectoAdminRowMapper());

        jbdcActualizaFase = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_SOPASIGPROY")
                .withProcedureName("SP_ACT_ASIG");

        jbdcObtieneDatosLiberaFase = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_SOPASIGPROY")
                .withProcedureName("SP_SEL_ASIG")
                .returningResultSet("RCL_FASES", new AsignaProyectoAdminRowMapper());

    }

//SOLO ACTUALIZA FECHA CUANDO EL STATUS EN LA TABLA DE ASIGNACION ES 1 DE LO CONTRARIO NO ACTUALIZA NADA
    public int insertaAsignacionProy(AsignaTransfDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_USU", bean.getUsuario())
                .addValue("PA_PROY", bean.getProyecto())
                .addValue("PA_USUASIG", bean.getUsuario_asig())
                .addValue("PA_OBSERV", bean.getObs())
                .addValue("PA_IDAGRUPA", bean.getAgrupador())
                .addValue("PA_FECHA", bean.getPeriodo())
                .addValue("PA_FASE", bean.getFaseActual());

        out = jdbcInsertaAsignProyecto.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PA_SOPASIGPROY.SP_INS_ASIG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        //BigDecimal idTipoReturn = (BigDecimal) out.get("PA_FIIDESTADO");
        //idEmpFij = idTipoReturn.intValue();
        if (error != 1) {
            logger.info("Algo ocurrió la Asignacion");
            error = 0;
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean desactivaAsignacionProy(String ceco, String usuario, String agrupa) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_USUASIG", usuario)
                .addValue("PA_CECO", ceco)
                .addValue("PA_AGRUPA", agrupa);

        out = jdbcDesactivaAsignProyecto.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PA_SOPASIGPROY.SP_DEL_ASIGUSU}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + usuario + ")");
        } else {
            return true;
        }

        return false;

    }

    @SuppressWarnings("unchecked")
    public List<AsignaTransfDTO> obtieneProyectosAdm(String ceco) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AsignaTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco);

        out = jdbcObtieneProyectos.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PA_SOPASIGPROY.SP_SEL_PROYECTOSCECO}");
        //lleva el nombre del cursor del procedure
        lista = (List<AsignaTransfDTO>) out.get("RCL_ASIG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la version (" + ceco + ")");
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<AsignaTransfDTO> obtieneFasesAdm(String ceco, String proyecto) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AsignaTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_PROY", proyecto);

        out = jdbcObtieneFases.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PA_SOPASIGPROY.SP_SEL_FASES}");
        //lleva el nombre del cursor del procedure
        lista = (List<AsignaTransfDTO>) out.get("RCL_ASIG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la fases del proyecto (" + proyecto + ")");
        }
        return lista;

    }

    public boolean liberaFaseProy(AsignaTransfDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_PROY", bean.getProyecto())
                .addValue("PA_USU", bean.getUsuario())
                .addValue("PA_USUASIG", bean.getUsuario_asig())
                .addValue("PA_STATUS", bean.getIdestatus());

        out = jbdcActualizaFase.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PA_SOPASIGPROY.SP_ACT_ASIG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado de la version  id( " + bean.getProyecto() + ")");
        } else {
            return true;
        }
        return false;

    }

    @SuppressWarnings("unchecked")
    public List<AsignaTransfDTO> obtieneDatosLiberaFase(String ceco, String proyecto) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AsignaTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_PROY", proyecto);

        out = jbdcObtieneDatosLiberaFase.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PA_SOPASIGPROY.SP_SEL_FASES}");
        //lleva el nombre del cursor del procedure
        lista = (List<AsignaTransfDTO>) out.get("RCL_ASIG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la fases del proyecto (" + proyecto + ")");
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<AsignaTransfDTO> obtieneAsignacionesProy(String ceco, String proyecto) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<AsignaTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_PROY", proyecto);

        out = jdbcObtieneAsignacionesProy.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PA_SOPASIGPROY.SP_SEL_ASIGNPTOYECTOS}");
        //lleva el nombre del cursor del procedure
        lista = (List<AsignaTransfDTO>) out.get("RCL_ASIG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la asignaciones del proyecto (" + proyecto + ")");
        }
        return lista;
    }

}
