package com.gruposalinas.checklist.domain;

public class SupervisorDTO {
	
	private int idUsuario;
	private String nomSupervisor;
	
	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNomSupervisor() {
		return nomSupervisor;
	}

	public void setNomSupervisor(String nomSupervisor) {
		this.nomSupervisor = nomSupervisor;
	}

	@Override
	public String toString() {
		return "SupervisorDTO [idUsuario=" + idUsuario
				+ ", nomSupervisor=" + nomSupervisor + "]";

	
}
}
