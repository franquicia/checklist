package com.gruposalinas.checklist.domain;

public class ReportesConteoDTO {
	
	private int conteo;
	private int idModulo;
	
	public int getConteo() {
		return conteo;
	}
	public void setConteo(int conteo) {
		this.conteo = conteo;
	}
	public int getIdModulo() {
		return idModulo;
	}
	public void setIdModulo(int idModulo) {
		this.idModulo = idModulo;
	}

}
