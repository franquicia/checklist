package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.HallazgosExpDTO;
import com.gruposalinas.checklist.mappers.HallazgosExpBitaDetRowMapper;
import com.gruposalinas.checklist.mappers.HallazgosExpBitaRowMapper;
import com.gruposalinas.checklist.mappers.HallazgosHistoricoDetRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class HallazgosExpDAOImpl extends DefaultDAO implements HallazgosExpDAO {

    private static Logger logger = LogManager.getLogger(HallazgosExpDAOImpl.class);

    DefaultJdbcCall jdbcInsertaHallazgo;
    DefaultJdbcCall jdbcEliminaHallazgo;
    DefaultJdbcCall jdbcEliminaHallazgoBita;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneHallazgo;
    DefaultJdbcCall jbdcActualizaHallazgo;
    DefaultJdbcCall jbdcActualizaHallazgoMov;
    DefaultJdbcCall jbdcActualizaHallazgoResp;
    DefaultJdbcCall jbdcCargaHallazgo;
    DefaultJdbcCall jbdcDepuraHallazgo;
    DefaultJdbcCall jbdcCargaHallazgoxBita;
    //HISTORICO
    DefaultJdbcCall jbdcCargaHistorico;
    DefaultJdbcCall jbdcInsertaHisto;
    DefaultJdbcCall jbdcEliminaHistbita;
    DefaultJdbcCall jbdcObtieneHistFolio;
    DefaultJdbcCall jbdcObtieneHistBita;
    DefaultJdbcCall jbdcObtieneDetaHist;

    public void init() {

        jdbcInsertaHallazgo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAZGOS")
                .withProcedureName("SP_INS_HALLAZ");

        jdbcEliminaHallazgo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAZGOS")
                .withProcedureName("SP_DEL_HALLAZ");

        jdbcEliminaHallazgoBita = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAZGOS")
                .withProcedureName("SP_DE_HALLAZ");

        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAZGOS")
                .withProcedureName("SP_DETA_HALLAZ")
                .returningResultSet("RCL_HALLAZ", new HallazgosExpBitaRowMapper());
//cambia mapper
        jdbcObtieneHallazgo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAZGOS")
                .withProcedureName("SP_SEL_HALLAZ")
                .returningResultSet("RCL_HALLAZ", new HallazgosExpBitaDetRowMapper());

        //update  web
        jbdcActualizaHallazgo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAZGOS")
                .withProcedureName("SP_ACT_HALLAZ");

        //update movil
        jbdcActualizaHallazgoMov = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAZGOS")
                .withProcedureName("SP_ACT_RESPNVO");

        jbdcActualizaHallazgoResp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAZGOS")
                .withProcedureName("SP_ACT_RESP");

        jbdcCargaHallazgo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAZGOS")
                .withProcedureName("SP_CARGA_HALLAZ");

        jbdcDepuraHallazgo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAZGOS")
                .withProcedureName("SP_DEPURA_HALLAZ");

        jbdcCargaHallazgoxBita = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEVIHALLAZ")
                .withProcedureName("SP_CARGA_HALLAZ");

        //histoorico
        jbdcCargaHistorico = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAHIST")
                .withProcedureName("SP_CARGA_HALLAZH");

        jbdcInsertaHisto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAHIST")
                .withProcedureName("SP_INSHALLA");

        jbdcEliminaHistbita = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAHIST")
                .withProcedureName("SP_DEL_HALLAZ");

        jbdcObtieneHistFolio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAHIST")
                .withProcedureName("SP_DETA_FOLIO")
                .returningResultSet("RCL_HISTORICO", new HallazgosHistoricoDetRowMapper());

        jbdcObtieneHistBita = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAHIST")
                .withProcedureName("SP_SEL_HISBITA")
                .returningResultSet("RCL_HISTORICO", new HallazgosHistoricoDetRowMapper());

        jbdcObtieneDetaHist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADHALLAHIST")
                .withProcedureName("SP_DETA_HALLAZ")
                .returningResultSet("RCL_HISTORICO", new HallazgosHistoricoDetRowMapper());

    }

    public int inserta(HallazgosExpDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDRESP", bean.getIdResp())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_IDPREG", bean.getIdPreg())
                .addValue("PA_PREGUNTA", bean.getPreg())
                .addValue("PA_POSIBLE", bean.getRespuesta())
                .addValue("PA_PREGPADRE", bean.getPregPadre())
                .addValue("PA_RESPONSABLE", bean.getResponsable())
                .addValue("PA_DISPOSITIVO", bean.getDisposi())
                .addValue("PA_AREA", bean.getArea())
                .addValue("PA_OBSERV", bean.getObs())
                .addValue("PA_BITAGRAL", bean.getBitGral())
                .addValue("PA_SLA", bean.getSla())
                .addValue("PA_AUX2", bean.getAux2());

        out = jdbcInsertaHallazgo.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADHALLAZGOS.SP_INS_HALLAZ}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDFOLIO");
        idEmpFij = idTipoReturn.intValue();

        BigDecimal idTipoRetu = (BigDecimal) out.get("PA_TABLA");
        idEmpFij = idTipoRetu.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar HALLAZGO");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean eliminaResp(String idResp) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDRESP", idResp);

        out = jdbcEliminaHallazgo.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADHALLAZGOS.SP_DEL_HALLAZ}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + idResp + ")");
        } else {
            return true;
        }

        return false;

    }

    public boolean eliminaBita(String bitGral) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITAGRAL", bitGral);

        out = jdbcEliminaHallazgoBita.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADHALLAZGOS.SP_DE_HALLAZ}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + bitGral + ")");
        } else {
            return true;
        }

        return false;
    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<HallazgosExpDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<HallazgosExpDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneTodo.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADHALLAZGOS.SP_DETA_HALLAZ}");

        listaFijo = (List<HallazgosExpDTO>) out.get("RCL_HALLAZ");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener los Empleados Fijos");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //parametro

    @SuppressWarnings("unchecked")
    public List<HallazgosExpDTO> obtieneDatos(int bitGral) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosExpDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", bitGral);

        out = jdbcObtieneHallazgo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADHALLAZGOS.SP_SEL_HALLAZ}");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosExpDTO>) out.get("RCL_HALLAZ");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos de la bitagral(" + bitGral + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;

    }
    // web

    public boolean actualiza(HallazgosExpDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDRESP", bean.getIdResp())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_POSIBLE", bean.getRespuesta())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_OBS", bean.getObs())
                .addValue("PA_OBSNUE", bean.getObsNueva())
                .addValue("PA_IDHALLA", bean.getIdHallazgo())
                .addValue("PA_FECHAAUTO", bean.getFechaAutorizo().replace("-", ""))
                .addValue("PA_FECHAFIN", bean.getFechaFin().replace("-", ""))
                .addValue("FCUSUMODIF", bean.getUsuModif());

        out = jbdcActualizaHallazgo.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADHALLAZGOS.SP_ACT_HALLAZ}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdHallazgo() + ")");
        } else {
            return true;
        }
        return false;

    }
    // mov

    public boolean actualizaMov(HallazgosExpDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDRESP", bean.getIdResp())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_POSIBLE", bean.getRespuesta())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_OBS", bean.getObs())
                .addValue("PA_OBSNUE", bean.getObsNueva())
                .addValue("PA_IDHALLA", bean.getIdHallazgo())
                .addValue("PA_FECHAAUTO", bean.getFechaAutorizo().replace("-", ""))
                .addValue("PA_FECHAFIN", bean.getFechaFin().replace("-", ""))
                .addValue("PA_USUMODIF", bean.getUsuModif())
                .addValue("PA_MOTIVRECHA", bean.getMotivrechazo());
        out = jbdcActualizaHallazgoMov.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADHALLAZGOS.SP_ACT_RESPNVO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdHallazgo() + ")");
        } else {
            return true;
        }
        return false;

    }

    @Override
    public boolean actualizaResp(HallazgosExpDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDRESP", bean.getIdResp())
                .addValue("PA_STATUS", bean.getStatus())
                .addValue("PA_POSIBLE", bean.getRespuesta())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_OBS", bean.getObs())
                .addValue("PA_OBSNUE", bean.getObsNueva())
                .addValue("PA_IDHALLA", bean.getIdHallazgo())
                .addValue("PA_FECHAAUTO", bean.getFechaAutorizo().replace("-", ""))
                .addValue("PA_FECHAFIN", bean.getFechaFin().replace("-", ""))
                .addValue("PA_USUMODIF", bean.getUsuModif())
                .addValue("PA_MOTIVRECHA", bean.getMotivrechazo());

        out = jbdcActualizaHallazgoResp.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADHALLAZGOS.SP_ACT_RESP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdHallazgo() + ")");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean cargaHallazgos(int bitGral) throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITAGRAL", bitGral);

        out = jbdcCargaHallazgo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADHALLAZGOS.SP_CARGA_HALLAZ}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener LA CARGA");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean EliminacargaHallazgos() throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        out = jbdcDepuraHallazgo.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PASUCUBICA.SP_DEPURA_HALLAZGO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las Sucursales");
        } else {
            return true;
        }

        return false;

    }

    @Override
    public boolean cargaHallazgosxBita(int bitGral) throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITAGRAL", bitGral);

        out = jbdcCargaHallazgoxBita.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAEVIHALLAZ.SP_CARGA_HALLAZ}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener LA CARGA x bita");
        } else {
            return true;
        }

        return false;
    }

//HALLAZGOS HISTORICO
    @SuppressWarnings("unchecked")
    public List<HallazgosExpDTO> obtieneHistFolio(int idFolio) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosExpDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FOLIO", idFolio);

        out = jbdcObtieneHistFolio.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADHALLAHIST.SP_DETA_FOLIO}");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosExpDTO>) out.get("RCL_HISTORICO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos de la bitagral(" + idFolio + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<HallazgosExpDTO> obtieneHistorico() throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosExpDTO> lista = null;

        out = jbdcObtieneDetaHist.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADHALLAHIST.SP_DETA_HALLAZ}");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosExpDTO>) out.get("RCL_HISTORICO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos ");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<HallazgosExpDTO> obtieneHistBita(int bitGral) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosExpDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bitGral);

        out = jbdcObtieneHistBita.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADHALLAHIST.SP_SEL_HISBITA}");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosExpDTO>) out.get("RCL_HISTORICO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos de la bitagral(" + bitGral + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;

    }

    @Override
    public boolean insertaHallazgosHist(int idHallazgo) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDHALLAZ", idHallazgo);

        out = jbdcInsertaHisto.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADHALLAHIST.SP_INSHALLA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el usuario(" + idHallazgo + ")");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargaHallazgosHistorico() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jbdcCargaHistorico.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADHALLAHIST.SP_CARGA_HALLAZ}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener LA CARGA ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaBitaHisto(String bitGral) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", bitGral);

        out = jbdcEliminaHistbita.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADHALLAHIST.SP_DEL_HALLAZ}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el usuario(" + bitGral + ")");
        } else {
            return true;
        }

        return false;
    }

}
