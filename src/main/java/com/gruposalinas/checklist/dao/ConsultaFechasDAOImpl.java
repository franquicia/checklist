package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ConsultaFechasDTO;
import com.gruposalinas.checklist.mappers.ConsultaFechasRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ConsultaFechasDAOImpl extends DefaultDAO implements ConsultaFechasDAO {

    private Logger logger = LogManager.getLogger(ConsultaDashBoardDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneFecha;
    private DefaultJdbcCall jdbcInsertaFecha;
    private DefaultJdbcCall jdbcActualizaFecha;
    private DefaultJdbcCall jdbcEliminaFecha;

    public void init() {
        jdbcObtieneFecha = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PKGADMFECHAS")
                .withProcedureName("SPGETFECHA")
                .returningResultSet("pa_CURSOR", new ConsultaFechasRowMapper());

        jdbcInsertaFecha = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PKGADMFECHAS")
                .withProcedureName("SPINSERTFECHA");

        jdbcActualizaFecha = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PKGADMFECHAS")
                .withProcedureName("SPACTDIASFESTV");

        jdbcEliminaFecha = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PKGADMFECHAS")
                .withProcedureName("SPDELFECHAS");
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaFechasDTO> obtieneFecha(ConsultaFechasDTO bean) throws Exception {
        Map<String, Object> out = null;
        List<ConsultaFechasDTO> listaCheck = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("pa_FechaIni", bean.getFechaInicial())
                .addValue("pa_FechaFin", bean.getFechaFinal());

        out = jdbcObtieneFecha.execute(in);

        logger.info("Funcion ejecutada:{checklist.PKGADMFECHAS.pa_FechaIni}");

        listaCheck = (List<ConsultaFechasDTO>) out.get("pa_CURSOR");

        //BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        //error = returnEjecucion.intValue();
        if (listaCheck == null) {
            error = 1;
        }

        if (error != 1) {
            logger.info("Algo ocurrió al consultar el procedure");
            return listaCheck;
        }
        return listaCheck;

    }

    @Override
    @SuppressWarnings("unused")
    public boolean InsertaFecha(ConsultaFechasDTO bean) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        logger.info("InsertaFecha...  " + bean);

        SqlParameterSource entra = new MapSqlParameterSource()
                .addValue("pa_FechaIni", bean.getFechaInicial())
                .addValue("pa_FechaFin", bean.getFechaFinal());

        out = jdbcInsertaFecha.execute(entra);

        logger.info("Funcion ejecutada:{checklist.PKGADMFECHAS.pa_FechaIni}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_RESEJECUCION");
        ejecucion = returnEjecucion.intValue();
        if (ejecucion == 0) {
            logger.info("Algo ocurrió al insertar fechaInicial y fechaFinal");
            return false;
        }

        return true;

    }

    @Override
    public boolean ActualizaFecha(ConsultaFechasDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FDFECHA", bean.getFecha());

        out = jdbcActualizaFecha.execute(in);

        logger.info("Funcion ejecutada:{checklist.PKGADMFECHAS.SPACTDIASFESTV.PA_FDFECHA}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_RESEJECUCION");
        error = returnEjecucion.intValue();
        if (error == 0) {
            logger.info("Algo ocurrió al actualizar DiaFestivo");
            return false;
        }

        return true;

    }

    @Override
    public boolean EliminaFecha(String fechaInicial, String fechaFinal) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("pa_FechaIni", fechaInicial)
                .addValue("pa_FechaFin", fechaFinal);

        out = jdbcEliminaFecha.execute(in);

        logger.info("Funcion ejecutada:{checklist.PKGADMFECHAS.SPDELFECHAS.pa_FechaIni}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_RESEJECUCION");
        error = returnEjecucion.intValue();
        if (error == 0) {
            logger.info("Algo ocurrió al eliminar la fecha ingresada");
            return false;
        }

        return true;
    }

}
