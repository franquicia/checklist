package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.mappers.ChecklistTRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ChecklistTDAOImpl extends DefaultDAO implements ChecklistTDAO {

    private static final Logger logger = LogManager.getLogger(ChecklistTDAOImpl.class);

    private DefaultJdbcCall jdbcBuscaChecklistTemp;
    private DefaultJdbcCall jdbcActualizaChecklistTemp;
    private DefaultJdbcCall jdbcEliminaChecklistTemp;
    private DefaultJdbcCall jdbcInsertaChecklist;
    private DefaultJdbcCall jdbcActualizaEdo;
    private DefaultJdbcCall jdbcEjecutaAutorizados;

    public void init() {
        jdbcBuscaChecklistTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECKTEMP")
                .withProcedureName("SP_SEL_CHECK_TEMP")
                .returningResultSet("RCL_CHECK", new ChecklistTRowMapper());

        jdbcActualizaChecklistTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECKTEMP")
                .withProcedureName("SP_ACT_CHECK_TEMP");

        jdbcEliminaChecklistTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECKTEMP")
                .withProcedureName("SP_DEL_CHECK_TEMP");

        jdbcInsertaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECKTEMP")
                .withProcedureName("SP_INS_CHECK_TEMP");

        jdbcActualizaEdo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCHECKTEMP")
                .withProcedureName("SP_ACTUALIZAEDO");

        jdbcEjecutaAutorizados = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECKCAMBIOSNEW")
                .withProcedureName("SPMAINHISTORICO");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ChecklistDTO> buscaChecklistTemp(int idChecklist) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ChecklistDTO> listaChecklist = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECK", idChecklist);

        out = jdbcBuscaChecklistTemp.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMCHECKTEMP.SP_SEL_CHECK_TEMP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");

        if (error == 1) {
            logger.info("Algo ocurrió al consultar el Checklist en la tabla temporal");
        } else {
            return listaChecklist;
        }

        return null;
    }

    @Override
    public boolean actualizaChecklistTemp(ChecklistDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECK", bean.getIdChecklist())
                .addValue("PA_FIID_TIPO_CHECK", bean.getIdTipoChecklist().getIdTipoCheck())
                .addValue("PA_FCNOMBRE", bean.getNombreCheck())
                .addValue("PA_IDHORARIO", bean.getIdHorario())
                .addValue("PA_VIGENTE", bean.getVigente())
                .addValue("PA_FECHA_I", bean.getFecha_inicio())
                .addValue("PA_FECHA_FIN", bean.getFecha_fin())
                .addValue("PA_FIID_ESTADO", bean.getIdEstado())
                .addValue("PA_NUM_REV", bean.getNumVersion())
                .addValue("PA_TIPO_MOD", bean.getTipoCambio());

        out = jdbcActualizaChecklistTemp.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMCHECKTEMP.SP_ACT_CHECK_TEMP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Checklist en la tabla temporal");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaChecklistTemp(int idCheckList) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECK", idCheckList);

        out = jdbcEliminaChecklistTemp.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMCHECKTEMP.SP_DEL_CHECK_TEMP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar Checklist de la tabla temporal");
        } else {
            return true;
        }

        return false;
    }

    public int insertaChecklist(ChecklistProtocoloDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        int idChecklist = 0;
        int numRevision = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_TIPO_CHECK", bean.getIdTipoChecklist())
                .addValue("PA_FCNOMBRE", bean.getNombreCheck())
                .addValue("PA_IDHORARIO", bean.getIdHorario())
                .addValue("PA_VIGENTE", bean.getVigente())
                .addValue("PA_FECHA_I", bean.getFecha_inicio())
                .addValue("PA_FECHA_FIN", bean.getFecha_fin())
                .addValue("PA_FIID_ESTADO", bean.getIdEstado())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_TIPOMODIF", 0)
                .addValue("PA_REVISION", 1)
                .addValue("PA_IDCHECKLIST", bean.getIdChecklist());

        out = jdbcInsertaChecklist.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMCHECKTEMP.SP_INS_CHECK_TEMP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal idreturn = (BigDecimal) out.get("PA_IDCHECKLIST");
        idChecklist = idreturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar Checklist temporal");
        } else {
            return idChecklist;
        }

        return 0;
    }

    @Override
    public boolean actualizaEdo(int idChecklist, int idEdo) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_CHECK", idChecklist)
                .addValue("PA_EDO", idEdo);

        out = jdbcActualizaEdo.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMCHECKTEMP.SP_ACTUALIZAEDO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el estado Checklist temporal");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean ejecutaAutorizados() throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        out = jdbcEjecutaAutorizados.execute();

        logger.info("Funcion ejecutada:{checklist.PACHECKCAMBIOSNEW.SPMAINHISTORICO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = errorReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al ejecutar el procedimiento de Historico");
        } else {
            return true;
        }

        return false;
    }

}
