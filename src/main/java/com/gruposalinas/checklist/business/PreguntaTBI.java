package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.PreguntaTDAO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class PreguntaTBI {

	private static Logger logger = LogManager.getLogger(PreguntaTBI.class);

	@Autowired
	PreguntaTDAO preguntaDAO;
	
	List<PreguntaDTO> listaPregunta = null;
	
	
	public List<PreguntaDTO> obtienePreguntaTemp(int idPregunta){
		try{
			listaPregunta = preguntaDAO.obtienePreguntaTemp(idPregunta);
		}
		catch(Exception e)
		{
			logger.info("No fue posible obtener datos de la tabla Preguntas temporal");
			
		}
		return listaPregunta;
	}
	
	public int insertaPreguntaTemp(PreguntaDTO bean){
		int respuesta = 0;
		try{
			respuesta = preguntaDAO.insertaPreguntaTemp(bean);
		}
		catch(Exception e)
		{
			logger.info("No fue posible insertar la Pregunta en la tabla temporal");
			
		}
		
		return respuesta;
	}
	
	public boolean actualizaPreguntaTemp(PreguntaDTO bean){
		boolean respuesta = false;
		try
		{
			respuesta = preguntaDAO.actualizaPreguntaTemp(bean);
		}
		catch(Exception e)
		{
			logger.info("No fue posible actualizar la Pregunta en la tabla temporal");
			
		}
		
		return respuesta;
	}
	
	public boolean eliminaPreguntaTemp(int idPregunta){
		boolean respuesta = false;
		
		try{
			respuesta = preguntaDAO.eliminaPreguntaTemp(idPregunta);
		}
		catch(Exception e)
		{
			logger.info("No fue posible eliminar la Pregunta en la tabla temporal");
			
		}
		
		return respuesta;
	}
}
