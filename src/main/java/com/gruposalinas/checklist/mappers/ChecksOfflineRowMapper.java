package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecksOfflineDTO;

public class ChecksOfflineRowMapper implements RowMapper<ChecksOfflineDTO>{

	@Override
	public ChecksOfflineDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
	    ChecksOfflineDTO checksOfflineDTO = new ChecksOfflineDTO();
	    
	    checksOfflineDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
	    checksOfflineDTO.setIdCheckUsua(rs.getInt("FIID_CHECK_USUA"));
	    checksOfflineDTO.setIdTipoCheck(rs.getInt("FIID_TIPO_CHECK"));
	    checksOfflineDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
	    checksOfflineDTO.setIdCheck(rs.getInt("FIID_CHECKLIST"));
		checksOfflineDTO.setNombreCheck(rs.getString("FCNOMBRE"));
		checksOfflineDTO.setZona(rs.getString("FCZONA"));
		checksOfflineDTO.setFechaInicio(rs.getString("FDFECHA_INICIO"));
		checksOfflineDTO.setFechaFin(rs.getString("FDFECHA_FIN"));
	    checksOfflineDTO.setIdCeco(rs.getString("FCID_CECO"));
		checksOfflineDTO.setNombreCeco(rs.getString("FCNOMBRECC"));
		checksOfflineDTO.setLongitud(rs.getString("FCLONGITUD"));
		checksOfflineDTO.setLatitud(rs.getString("FCLATITUD"));
		checksOfflineDTO.setHorarioIni(rs.getString("FCVALOR_INI"));
		checksOfflineDTO.setHorarioFin(rs.getString("FCVALOR_FIN"));
		checksOfflineDTO.setUltimaVisita(rs.getString("ULTVISITA"));
	    
		return checksOfflineDTO;
	}
	
	

}
