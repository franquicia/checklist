package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.FirmasCatDAO;
import com.gruposalinas.checklist.domain.AdmFirmasCatDTO;

public class FirmasCatalogoBI {

	private static Logger logger = LogManager.getLogger(FirmasCatalogoBI.class);

private List<AdmFirmasCatDTO> listafila;
	
	@Autowired
	FirmasCatDAO firmasCatDAO;
		
	
	public int insertaFirmaCat(AdmFirmasCatDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = firmasCatDAO.insertaFirmaCat(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaFirmaCat(int idEmpFijo){
		boolean respuesta = false;
		
		try {
			respuesta = firmasCatDAO.eliminaFirmaCat(idEmpFijo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<AdmFirmasCatDTO> obtieneDatosFirmaCat( String idUsuario) {
		
		try {
			listafila =  firmasCatDAO.obtieneDatosFirmaCat(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	
	
	public boolean actualizaFirmaCat(AdmFirmasCatDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = firmasCatDAO.actualizaFirmaCat(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public int insertaFirmaAgrupa(AdmFirmasCatDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = firmasCatDAO.insertaFirmaAgrupa(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaFirmaAgrupa(int idEmpFijo){
		boolean respuesta = false;
		
		try {
			respuesta = firmasCatDAO.eliminaFirmaAgrupa(idEmpFijo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<AdmFirmasCatDTO> obtieneDatosFirmaAgrupa( String idUsuario) {
		
		try {
			listafila =  firmasCatDAO.obtieneDatosFirmaAgrupa(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	
	
	public boolean actualizaFirmaAgrupa(AdmFirmasCatDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = firmasCatDAO.actualizaFirmaAgrupa(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
}