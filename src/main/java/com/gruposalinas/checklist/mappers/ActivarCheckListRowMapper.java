package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ActivarCheckListDTO;

public class ActivarCheckListRowMapper implements RowMapper<ActivarCheckListDTO> {

	@Override
	public ActivarCheckListDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ActivarCheckListDTO activarCheckListDTO = new ActivarCheckListDTO();
		
		activarCheckListDTO.setIdChecklist(rs.getString("FIID_CHECKLIST"));
		activarCheckListDTO.setIdCeco(rs.getString("FCID_CECO"));
		activarCheckListDTO.setActivo(rs.getString("FIACTIVO"));
		return activarCheckListDTO;
	}

}
