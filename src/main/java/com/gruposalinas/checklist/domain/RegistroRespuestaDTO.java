package com.gruposalinas.checklist.domain;

import java.util.Arrays;
import java.util.List;

public class RegistroRespuestaDTO {
	
	private int idCheckUsua;
	private List<RespuestaDTO> listaRespuestas;
	private List<EvidenciaDTO> listaEvidencia;
	private BitacoraDTO bitacora;
	private int idArbol;
	private int idPreg;
	private String observacion;
	private String respAd;
	
	
	public int getIdCheckUsua() {
		return idCheckUsua;
	}
	public void setIdCheckUsua(int idCheckUsua) {
		this.idCheckUsua = idCheckUsua;
	}
	public List<RespuestaDTO> getListaRespuestas() {
		return listaRespuestas;
	}
	public void setListaRespuestas(List<RespuestaDTO> listaRespuestas) {
		this.listaRespuestas = listaRespuestas;
	}
	
	public List<EvidenciaDTO> getListaEvidencia() {
		return listaEvidencia;
	}
	public void setListaEvidencia(List<EvidenciaDTO> listaEvidencia) {
		this.listaEvidencia = listaEvidencia;
	}
	public BitacoraDTO getBitacora() {
		return bitacora;
	}
	public void setBitacora(BitacoraDTO bitacora) {
		this.bitacora = bitacora;
	}
	public int getIdArbol() {
		return idArbol;
	}
	public void setIdArbol(int idArbol) {
		this.idArbol = idArbol;
	}
	public String getRespAd() {
		return respAd;
	}
	public void setRespAd(String respAd) {
		this.respAd = respAd;
	}
	public int getIdPreg() {
		return idPreg;
	}
	public void setIdPreg(int idPreg) {
		this.idPreg = idPreg;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	@Override
	public String toString() {
		return "RegistroRespuestaDTO [idCheckUsua=" + idCheckUsua + ", listaRespuestas=" + Arrays.toString(listaRespuestas.toArray())
				+ ", listaEvidencia=" + Arrays.toString(listaEvidencia.toArray()) + ", bitacora=" + bitacora + ", idArbol=" + idArbol + ", idPreg="
				+ idPreg + ", observacion=" + observacion + ", respAd=" + respAd + "]";
	}
	
	
	
	
	
}
