package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.FiltrosCecoDTO;

public class FiltrosCecosRowMapper implements RowMapper<FiltrosCecoDTO> {

	@Override
	public FiltrosCecoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		FiltrosCecoDTO cecoDTO = new FiltrosCecoDTO();
		
		cecoDTO.setNombreCeco(rs.getString("FCNOMBRE"));
		cecoDTO.setIdCeco(rs.getString("FCID_CECO"));
		
		return cecoDTO;
	}

}
