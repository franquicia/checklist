package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.PerfilesSupDTO;
import com.gruposalinas.checklist.mappers.PerfilesSupRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PerfilesSupDAOImpl extends DefaultDAO implements PerfilesSupDAO {

    private static Logger logger = LogManager.getLogger(PerfilesSupDAOImpl.class);

    DefaultJdbcCall jdbcInsertPerfil;
    DefaultJdbcCall jdbcUpdatePerfil;
    DefaultJdbcCall jdbcDeletePerfil;
    DefaultJdbcCall jdbcGetPerfil;
    DefaultJdbcCall jdbcUpdateStatus;

    public void init() {

        jdbcInsertPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILES")
                .withProcedureName("SPINSPERFIL");

        jdbcUpdatePerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILES")
                .withProcedureName("SPACTPERFIL");

        jdbcDeletePerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILES")
                .withProcedureName("SPDELPERFIL");

        jdbcGetPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILES")
                .withProcedureName("SPGETPERFIL")
                .returningResultSet("PA_CONSULTA", new PerfilesSupRowMapper());
    }

    @Override
    public int insertPerfil(int idPerfil, String descPerfil, String activo) throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIPERFIL_ID", idPerfil)
                .addValue("PA_FCDESCRIPCION", descPerfil)
                .addValue("PA_FIACTIVO", activo);

        out = jdbcInsertPerfil.execute(in);
        logger.info("Función ejecutada: FRANQUICIA.PAADMPERFILES.SPINSPERFIL");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al insertar el perfil");
        }
        return respuesta;
    }

    @Override
    public int updatePerfil(int idPerfil, String descPerfil, String activo) throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIPERFIL_ID", idPerfil)
                .addValue("PA_FCDESCRIPCION", descPerfil)
                .addValue("PA_FIACTIVO", activo);

        out = jdbcUpdatePerfil.execute(in);
        logger.info("Función ejecutada: FRANQUICIA.PAADMPERFILES.SPACTPERFIL");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al actualizar el perfil");
        }
        return respuesta;
    }

    @Override
    public int deletePerfil(int idPerfil) throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIPERFIL_ID", idPerfil);

        out = jdbcDeletePerfil.execute(in);
        logger.info("Función ejecutada: FRANQUICIA.PAADMPERFILES.SPDELPERFIL");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al eliminar el perfil");
        }
        return respuesta;
    }

    @Override
    public List<PerfilesSupDTO> getPerfil(String idPerfil) throws Exception {
        Map<String, Object> out = null;
        List<PerfilesSupDTO> respuesta = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIPERFIL_ID", idPerfil);

        out = jdbcGetPerfil.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PADMASIGNACION.SPGETASIGNACION");

        respuesta = (List<PerfilesSupDTO>) out.get("PA_CONSULTA");

        if (respuesta.size() == 0) {
            logger.info("Algo ocurrió al obtener los perfiles");
        }
        return respuesta;
    }

}
