package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.DatosVisitaResumenDTO;

public class DatosVisitaResumenRowMapper implements RowMapper<DatosVisitaResumenDTO> {

	@Override
	public DatosVisitaResumenDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		DatosVisitaResumenDTO datosVisitaResumenDTO = new DatosVisitaResumenDTO();
		
		datosVisitaResumenDTO.setFechaEnvio(rs.getString("FECHAENVIO"));
		datosVisitaResumenDTO.setFechaFin(rs.getString("FECHAFIN"));
		datosVisitaResumenDTO.setFechaInicio(rs.getString("FECHAINICIO"));
		datosVisitaResumenDTO.setIdCeco(rs.getString("FCID_CECO"));
		datosVisitaResumenDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		datosVisitaResumenDTO.setModo(rs.getString("FIMODO"));
		datosVisitaResumenDTO.setNombreChecklist(rs.getString("NOMBRECHECKLIST"));
		datosVisitaResumenDTO.setNombreTienda(rs.getString("FCNOMBRE"));
		
		return datosVisitaResumenDTO;
	}
	

}
