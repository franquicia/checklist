<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>B�squeda general</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervision/estilos.css">
</head>

<body ng-app="app" ng-controller="validarForm">



  <div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="${pageContext.request.contextPath}/img/supervision/btn_hamburguer.svg" alt="Men� principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="inicio.htm"><img src="${pageContext.request.contextPath}/img/supervision/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.htm" class="selMenu">
          <div>Salir</div>
          <div><img src="${pageContext.request.contextPath}/img/supervision/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="indexSupervision.htm">Perfil Supervision Generica</a> 
		</div>
    	Agregar Perfil de Supervision Generica
    </div>

	<!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervision/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Men� Principal<br>
          </div>
        </div>
        <%-- <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la p�gina">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> --%>
        <div id="menu">
          <ul class="l-navegacion nivel1">
          		<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Supervisor</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>B�squeda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>B�squeda Supervisor</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galer�a de Evidencias</div></a>
				</li>
			</ul>
        </div>
      </div>
    </div>

    <!-- Contenido -->
    <div class="contSecc">
    
		<div class="titSec">Selecciona los criterios de b�squeda por Supervisor para agregar </div>
		<div class="gris">
			
			<c:url value="/central/postPerfilSuperGenerica.htm" var="archivoPasivo" />
			<form:form method="POST" action="${archivoPasivo}" model="command" name="form" id="form">
			<div class="divCol3 flexJusCen flexEnd">
				<div class="col3">
					Supervisor:<br>
					<input id="idUsuario" type="text" placeholder="Escribe el n�mero de empleado" path="idUsuario" onkeypress="return fieldFormat(event);"/>
					<input id="numEmpleado" name="numEmpleado" type="hidden" value=""/>
				</div>
				<div class="col3 col3Buscar"><br><a class="btn btnBuscar btnResultado" onclick="return validaSupervisor();">Buscar</a></div>
			</div>
			</form:form>
			
			<br>
			<br>
			<c:choose>
			<c:when test="${paso == 1}">
		
				<div class="divResultado">
					<div class="tit tCenter">Seleccione al supervisor al que desea agregar el perfil de supervision Generica</div>
					<table class="tblGeneral">
						<thead>
							<tr>
								<th>Nombre de Supervisor</th>
								<th>N�mero de Empleado</th>
							</tr>
						</thead>
						<tbody>
						
						<c:forEach var="lista" items="${lista}">
								<tr>
								<c:url value="/central/dashboardSupervision.htm" var="dashboardSupervision" />
								<form:form method="POST" action="${dashboardSupervision}" model="command" name="formDashboard" id="formDashboard">
									<td>
										<u><a href="#" onclick="javascript:enviarIdUsuario(${lista.idUsuario})" class="txtVerde">
										<c:out value="${lista.nomSupervisor}"></c:out></a></u>
									</td>
									<input id="idUser" name="idUser" type="hidden" value=""/>
								</form:form>
									<td><c:out value="${lista.idUsuario}"></c:out>
									</td>
								</tr>
						</c:forEach>							
						
						</tbody>
					</table>
				</div>
			</c:when>
			
			<c:when test="${paso == 2}">
		
				<div class="divResultado">				
					<div class="tit tCenter"><span ><b> NO SE ENCONTRARON DETALLES PARA CONSULTA.</b></span>
						<br><br>
						<div><b>No se encontraron resultados para este empleado.</b></div>					
					</div>
				</div>
			</c:when>
			
			</c:choose>
		</div>				
	</div>	<!-- Fin conSecc -->
	
	<!-- Footer -->
	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualizaci�n Febrero 2019</div>
	</div>
</div><!-- Fin page -->



</body>
</html>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/custom-file-input.js"></script><!--Unpload -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/funciones.js"></script>


<script type="text/javascript">

function enviarIdUsuario(id) {
	document.getElementById("idUser").value = id;
	form = document.getElementById("formDashboard");
	//alert("ID: " + document.getElementById('idUser').value);
	form.submit();
}

//Valida supervisor
function validaSupervisor(){	
	var id=document.getElementById("idUsuario").value;
	if (id == "" || id == null) {	
		alert("Debe agregar el n�mero de empleado");
	} else {
		$(".btnResultado").click(function(){
			$(".divResultado").show();
		});

		document.getElementById('numEmpleado').value = id;
		form = document.getElementById("form");
		//alert("ID: " + document.getElementById('numEmpleado').value);
		form.submit();
	}
	
}

//valida campo numerico
function fieldFormat(event) {
    var e = event || window.event,
        charCode = e.keyCode || e.which,
        parts = e.target.value.split('.');
    if (charCode > 31 && (charCode != 46 && (charCode < 48 || charCode > 57)) || (parts.length > 1 && charCode == 46))
        return false;
    return true;
}

</script>
