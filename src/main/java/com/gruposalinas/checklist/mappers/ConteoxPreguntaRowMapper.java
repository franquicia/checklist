package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ConteoxPreguntaDTO;

public class ConteoxPreguntaRowMapper implements RowMapper<ConteoxPreguntaDTO>{

	@Override
	public ConteoxPreguntaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		ConteoxPreguntaDTO conteoxPreguntaDTO = new ConteoxPreguntaDTO();
		
		conteoxPreguntaDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		conteoxPreguntaDTO.setTiendas(rs.getInt("TIENDAS"));		
		conteoxPreguntaDTO.setRespuesta(rs.getInt("FCRESPUESTA"));
		conteoxPreguntaDTO.setOrden(rs.getInt("FIORDEN_CHECK"));
		
		return conteoxPreguntaDTO;
	}
	
	

}
