package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.PreguntaZonaDTO;

public class PreguntaZonaMapper implements RowMapper<PreguntaZonaDTO>{

	public PreguntaZonaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
	    
		PreguntaZonaDTO pregZon = new PreguntaZonaDTO();
		
		pregZon.setIdPregZona(rs.getInt("FIIDPRZO"));
		pregZon.setIdPregunta(rs.getInt("FIIDPREG"));
		pregZon.setIdZona(rs.getInt("FIIDZONA"));
		pregZon.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		pregZon.setIdPregPadre(rs.getInt("FIID_PREG_PADRE"));
		
	
		return pregZon;
}
	
	

}
