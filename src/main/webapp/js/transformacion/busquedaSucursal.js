
$(document).ready(function() {
	
//$("#listaSucursalesFiltro").hide();
			
$( "#btnBuscar" ).click(function() {
			
		var nomCeco = $('#sucursal').val().split(" - ")[1];
	 	var idCeco = $('#sucursal').val().split(" - ")[0];
		var tipoRecorrido = $('#tipoRecorrido option:selected').val();
		var fechaInicio = $('#datepicker').val();
		var fechaFin = $('#datepickerDos').val();
		
		var fechaInicioParam = $('#datepicker').val();
		var fechaFinParam = $('#datepickerDos').val();
		
		if ( (typeof nomCeco === "undefined") || (nomCeco.localeCompare('') == 0) ) {
		   nomCeco = ""
		}
		
		if ( (typeof idCeco === "undefined") || (idCeco.localeCompare('') == 0) ) {
			idCeco = ""
			}

		if (tipoRecorrido == 0){
			tipoRecorrido = ""
		}
		
		
		if ((typeof fechaInicio === "undefined") && fechaInicio.localeCompare('FECHA INICIO') == 0 || fechaInicio.localeCompare('') == 0 ){
			fechaInicio = ""
		} else {
			fechaInicio = $('#datepicker').val().split("/")[2] + $('#datepicker').val().split("/")[1]
			fechaInicio += $('#datepicker').val().split("/")[0]
		}
		
		if ((typeof fechaFin === "undefined") && fechaFin.localeCompare('FECHA TERMINO') == 0 || fechaFin.localeCompare('') == 0){
			fechaFin = ""
		} else {
			fechaFin = $('#datepickerDos').val().split("/")[2] + $('#datepickerDos').val().split("/")[1]
			fechaFin += $('#datepickerDos').val().split("/")[0]
						
			if (fechaInicio.localeCompare('FECHA INICIO') != 0 || fechaInicio.localeCompare('') != 0 ){
				if (validarFecha(fechaInicioParam,fechaFinParam) == false){
					alert("La fecha fin debe ser menor a la fecha inicio");
					return;
				}
			}

		}
		

		$(location).attr("href","seleccionFiltroExpansion.htm?busqueda=1&idCeco="+idCeco+"&nombreCeco="+nomCeco+"&tipoRecorrido="+tipoRecorrido+"&estatusVisita=&fechaInicio="+fechaInicio+"&fechaFin="+fechaFin+"&fechaInicioParam="+fechaInicioParam+"&fechaFinParam="+fechaFinParam);
		//<a href='filtrosPadresExpansion.htm?ceco="+ json[i].ceco +"&idRecorrido="+ json[i].idRecorrido + "&nombreCeco=" + json[i].nombreCeco + "'>"+ json[i].nombreCeco + "</a>
		
			//getSucursales(nomCeco,idCeco,tipoRecorrido,fechaInicio,fechaFin);			
			//$("#listaSucursalesFiltro").show();

	
	});

});
	
function validaCalendario(){

	var fecha=new Date();
	var diaSemana = new Date(fecha.getTime() - (24*60*60*1000)*0).getUTCDay();
	
	switch (diaSemana) {
	case 0:
		$( "#datepicker2" ).datepicker({
	   	 	minDate: 8,  
	    });
		break;
	case 1:
		$( "#datepicker2" ).datepicker({
	   	 	minDate: 7,  
	    });
		break;
	case 2:
		$( "#datepicker2" ).datepicker({
	   	 	minDate: 6,  
	    });
		break;
	case 3:
		$( "#datepicker2" ).datepicker({
	   	 	minDate: 12,  
	    });
		break;
	case 4:
		$( "#datepicker2" ).datepicker({
	   	 	minDate: 11,  
	    });
		break;
	case 5:
		$( "#datepicker2" ).datepicker({
	   	 	minDate: 10,  
	    });
		break;
	case 6:
		$( "#datepicker2" ).datepicker({
	   	 	minDate: 9,  
	    });
		break;

	default:
		break;
	}
	

}

function getSucursales(ceco,idCeco,tipoRecorrido,fechaInicio,fechaFin) {
		
	$.ajax({
		type : "GET",
		url : '../getSucursalesExpansion.json?idCeco='+idCeco+'&nombreCeco='+ceco+'&tipoRecorrido='+tipoRecorrido+'&estatusVisita=&fechaInicio='+fechaInicio+'&fechaFin='+fechaFin,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
					
			if(json != null){
				
				if(json.length > 0){
										
					document.getElementById("tabla").innerHTML="";					
					$("#tabla").append("<thead><tr><th><b>Nombre de Sucursal</b></th><th><b>CECO</b></th><th><b>Zona</b></th><th><b>Región</b></th><th><b>Tipo de Visita</b></th><th><b>Fecha</b></th><th><b>Pre Calificación</b></th></tr> </thead> <tbody>");
					
					var i = 0;
					while (i < json.length) {
												
						$("#tabla").append("" +
								"<tr>" +
								
									"<td align='center'> <a href='filtrosPadresExpansion.htm?ceco="+ json[i].ceco +"&idRecorrido="+ json[i].idRecorrido + "&nombreCeco=" + json[i].nombreCeco + "'>"+ json[i].nombreCeco + "</a></td>" +
									"<td align='center'><b>" + json[i].ceco + "</b></td>" +
									"<td align='center'><b>" + json[i].zona + "</b></td>" +
									"<td align='center'><b>" + json[i].region + "</b></td>" +
									"<td align='center'><b>" + json[i].idRecorrido + "</b></td>" +
									"<td align='center'><b>" + json[i].perido + "</b></td>" +
									"<td align='center'><b>" + json[i].precalif + "</b></td>" +
									"</tr>");						
						i++;
					}								
					}
					$("#tabla").append("</tbody>");

			}
		},
		error : function() {
		}
	});
}


function validarFecha(fInicial,fFinal){

	   var fi = new Date();
	   var ff = new Date();

	   var fechaInicial = fInicial.split("/");
	   fi.setFullYear(fechaInicial[2],fechaInicial[1],fechaInicial[0]);

	   var fechaFinal = fFinal.split("/");
	   ff.setFullYear(fechaFinal[2],fechaFinal[1],fechaFinal[0]);

	   if (ff >= fi){
	      return true;
	   	}
	   else{
	      return false;
	   }
	   //
}


function nobackbutton(){
	window.location.hash="no-back-button";
	window.location.hash="Again-No-back-button" //chrome
	window.onhashchange=function(){window.location.hash="";}
}
