package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.PaisNegocioDTO;

public class PaisNegocioRowMapper implements RowMapper<PaisNegocioDTO> {

	@Override
	public PaisNegocioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		PaisNegocioDTO paisNegocio = new PaisNegocioDTO();
		
		paisNegocio.setIdPais(rs.getInt("FIID_NEGOCIO"));
		paisNegocio.setIdNegocio(rs.getInt("FIID_PAIS"));
		
		return paisNegocio;
	}
	

}
