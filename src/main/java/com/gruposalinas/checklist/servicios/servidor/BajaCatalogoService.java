package com.gruposalinas.checklist.servicios.servidor;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.ActorEdoHallaBI;
import com.gruposalinas.checklist.business.AdicionalesBI;
import com.gruposalinas.checklist.business.AdmHandbookBI;
import com.gruposalinas.checklist.business.AdmInformProtocolosBI;
import com.gruposalinas.checklist.business.AltaGerenteBI;
import com.gruposalinas.checklist.business.AsignaExpBI;
import com.gruposalinas.checklist.business.AsignaTransfBI;
import com.gruposalinas.checklist.business.BitacoraAdministradorBI;
import com.gruposalinas.checklist.business.BitacoraGralBI;
import com.gruposalinas.checklist.business.CanalBI;
import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.business.CheckInAsistenciaBI;
import com.gruposalinas.checklist.business.CheckinBI;
import com.gruposalinas.checklist.business.ChecklistNegocioBI;
import com.gruposalinas.checklist.business.ComentariosRepAsgBI;
import com.gruposalinas.checklist.business.ConfiguraActorBI;
import com.gruposalinas.checklist.business.ConsultaFechasBI;
import com.gruposalinas.checklist.business.DatosInformeBI;
import com.gruposalinas.checklist.business.DistribCecoBI;
import com.gruposalinas.checklist.business.EmpContacExtBI;
import com.gruposalinas.checklist.business.EmpFijoBI;
import com.gruposalinas.checklist.business.EstadosBI;
import com.gruposalinas.checklist.business.FaseTransfBI;
import com.gruposalinas.checklist.business.FirmaCheckBI;
import com.gruposalinas.checklist.business.FirmasCatalogoBI;
import com.gruposalinas.checklist.business.FragmentMenuBI;
import com.gruposalinas.checklist.business.HallagosMatrizBI;
import com.gruposalinas.checklist.business.HallazgosEviExpBI;
import com.gruposalinas.checklist.business.HallazgosExpBI;
import com.gruposalinas.checklist.business.HallazgosTransfBI;
import com.gruposalinas.checklist.business.HorarioBI;
import com.gruposalinas.checklist.business.NegocioAdicionalBI;
import com.gruposalinas.checklist.business.NegocioBI;
import com.gruposalinas.checklist.business.NivelBI;
import com.gruposalinas.checklist.business.PaisBI;
import com.gruposalinas.checklist.business.PaisNegocioBI;
import com.gruposalinas.checklist.business.ParametroBI;
import com.gruposalinas.checklist.business.PerfilBI;
import com.gruposalinas.checklist.business.PerfilUsuarioBI;
import com.gruposalinas.checklist.business.PosibleTipoPreguntaBI;
import com.gruposalinas.checklist.business.PosiblesBI;
import com.gruposalinas.checklist.business.ProtocoloBI;
import com.gruposalinas.checklist.business.ProyectFaseTransfBI;
import com.gruposalinas.checklist.business.PuestoBI;
import com.gruposalinas.checklist.business.RecursoBI;
import com.gruposalinas.checklist.business.RecursoPerfilBI;
import com.gruposalinas.checklist.business.SoftNvoBI;
import com.gruposalinas.checklist.business.SoftOpeningBI;
import com.gruposalinas.checklist.business.SoporteHallazgosTransfBI;
import com.gruposalinas.checklist.business.SucursalBI;
import com.gruposalinas.checklist.business.SucursalChecklistBI;
import com.gruposalinas.checklist.business.TipoArchivoBI;
import com.gruposalinas.checklist.business.TransformacionBI;
import com.gruposalinas.checklist.business.Usuario_ABI;
import com.gruposalinas.checklist.business.VersionChecklistGralBI;
import com.gruposalinas.checklist.business.VersionExpanBI;
import com.gruposalinas.checklist.business.ZonaNegoBI;
import com.gruposalinas.checklist.domain.AdmFirmasCatDTO;
import com.gruposalinas.checklist.domain.AdmHandbookDTO;
import com.gruposalinas.checklist.domain.AdmInformProtocolosDTO;
import com.gruposalinas.checklist.domain.AltaGerenteDTO;
import com.gruposalinas.checklist.domain.ComentariosRepAsgDTO;
import com.gruposalinas.checklist.domain.ConsultaFechasDTO;
import com.gruposalinas.checklist.domain.DistribCecoDTO;
import com.gruposalinas.checklist.domain.FragmentMenuDTO;
import com.gruposalinas.checklist.domain.PerfilUsuarioDTO;
import com.gruposalinas.checklist.domain.ProtocoloDTO;
import com.gruposalinas.checklist.resources.FRQAuthInterceptor;
import org.springframework.ui.Model;

@Controller
@RequestMapping("/catalogosService")
public class BajaCatalogoService {

	@Autowired
	CanalBI canalbi;
	@Autowired
	PerfilBI perfilbi;
	@Autowired
	PuestoBI puestobi;
	@Autowired
	TipoArchivoBI tipoarchivobi;
	@Autowired
	CecoBI cecobi;
	@Autowired
	SucursalBI sucursalbi;
	@Autowired
	Usuario_ABI usuarioabi;
	@Autowired
	PaisBI paisbi;
	@Autowired
	HorarioBI horariobi;
	@Autowired
	ParametroBI parametrobi;
	@Autowired
	NegocioBI negociobi;
	@Autowired
	NivelBI nivelbi;
	@Autowired
	PerfilUsuarioBI perfilusuariobi;
	@Autowired
	RecursoPerfilBI recursoperfilbi;
	@Autowired
	RecursoBI recursobi;
	@Autowired
	PosiblesBI posiblesbi;
	@Autowired
	PosibleTipoPreguntaBI posibletipopreguntabi;
	@Autowired
	ChecklistNegocioBI checklistNegocioBI;
	@Autowired
	NegocioAdicionalBI adicionalBI;
	@Autowired
	PaisNegocioBI paisNegocioBI;
	@Autowired
	EmpFijoBI empFijoBI;
	@Autowired
	EstadosBI estadosBI;
	@Autowired
	BitacoraGralBI bitacoraGralBI;
	@Autowired
	FirmaCheckBI	firmaCheckBI;
	@Autowired
	SucursalChecklistBI sucursalChecklistBI;
	@Autowired
	CheckinBI checkinBI;
	@Autowired
	SoftOpeningBI softOpeningBI;
	@Autowired
	AsignaExpBI 	asignaExpBI;
	@Autowired
	VersionExpanBI versionExpanBI;
	@Autowired
	VersionChecklistGralBI versionChecklistGralBI;
	@Autowired
	EmpContacExtBI empContacExtBI;
	@Autowired
	ConsultaFechasBI consultaFechaBI;
	@Autowired
	HallazgosExpBI hallazgosExpBI;
	@Autowired	
	HallazgosEviExpBI hallazgosEviExpBI;
	@Autowired
	BitacoraAdministradorBI bitacoraAdministradorBI;
	@Autowired
	DistribCecoBI distribCecoBI;
	@Autowired
	AdmHandbookBI handBookBI;
	@Autowired
	ProtocoloBI protocoloBI;
	@Autowired
	ZonaNegoBI zonaNegoBI;
	@Autowired
	AdicionalesBI adicionalesBI;
	@Autowired
	AltaGerenteBI altaGerenteBI;
	@Autowired
	HallazgosTransfBI hallazgosTransfBI;
	@Autowired
	TransformacionBI transformacionBI;
	@Autowired
	FaseTransfBI faseTransfBI;   
	 @Autowired
	  AdmInformProtocolosBI admInformProtocolosBI;
	@Autowired
	ProyectFaseTransfBI proyectFaseTransfBI;
	@Autowired
	AsignaTransfBI asignaTransfBI;   
    @Autowired
    ComentariosRepAsgBI comentariosRepAsgBI;
 	@Autowired
 	FirmasCatalogoBI firmasCatalogoBI;
 	@Autowired
 	FragmentMenuBI fragmentMenuBI;
	@Autowired
	SoftNvoBI softNvoBI;
	@Autowired
	ActorEdoHallaBI actorEdoHallaBI;
	@Autowired
	ConfiguraActorBI configuraActorBI;
	@Autowired
	HallagosMatrizBI hallagosMatrizBI;
	@Autowired
	SoporteHallazgosTransfBI soporteHallazgosTransfBI;
	@Autowired
	CheckInAsistenciaBI checkInAsistenciaBI;
	@Autowired
	DatosInformeBI datosInformeBI;
	
	private static final Logger logger = LogManager.getLogger(BajaCatalogoService.class);

	// http://localhost:8080/checklist/catalogosService/eliminaCanal.json?idCanal=<?>
	@RequestMapping(value = "/eliminaCanal", method = RequestMethod.GET)
	public ModelAndView eliminaCanal(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idCanal = request.getParameter("idCanal");

			boolean res = canalbi.eliminaCanal(Integer.parseInt(idCanal));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "CANAL ELIMINADO CORRECTAMENTE");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/eliminaPerfil.json?idPerfil=<?>
	@RequestMapping(value = "/eliminaPerfil", method = RequestMethod.GET)
	public ModelAndView eliminaPerfil(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idP = request.getParameter("idPerfil");

			boolean res = perfilbi.eliminaPerfil(Integer.parseInt(idP));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "ID DE PERFIL BORRADO CORRECTAMENTE");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/eliminaPuesto.json?idPuesto=<?>
	@RequestMapping(value = "/eliminaPuesto", method = RequestMethod.GET)
	public ModelAndView eliminaPuesto(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idPuesto = request.getParameter("idPuesto");

			boolean res = puestobi.eliminaPuesto(Integer.parseInt(idPuesto));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "ID DE PUESTO BORRADO CORRECTAMENTE");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/eliminaTipoArchivo.json?idArchivo=<?>
	@RequestMapping(value = "/eliminaTipoArchivo", method = RequestMethod.GET)
	public ModelAndView eliminaTipoArchivo(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idA = request.getParameter("idArchivo");

			boolean res = tipoarchivobi.eliminaTipoArchivo(Integer.parseInt(idA));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "TIPO ARCHIVO BORRADO CORRECTAMENTE");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/eliminaCeco.json?idCeco=<?>
	@RequestMapping(value = "/eliminaCeco", method = RequestMethod.GET)
	public ModelAndView eliminaCeco(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idCeco = request.getParameter("idCeco");

			boolean res = cecobi.eliminaCeco(Integer.parseInt(idCeco));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "CECO BORRADO CORRECTAMENTE");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/eliminaSucursal.json?idSuc=<?>
	@RequestMapping(value = "/eliminaSucursal", method = RequestMethod.GET)
	public ModelAndView eliminaSucursal(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idSuc = request.getParameter("idSuc");

			boolean res = sucursalbi.eliminaSucursal(Integer.parseInt(idSuc));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "SUCURSAL ACTUALIZADA CORRECTAMENTE");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}
	
	// http://localhost:8080/checklist/catalogosService/eliminaSucursalGCC.json?idSuc=<?>
	@RequestMapping(value = "/eliminaSucursalGCC", method = RequestMethod.GET)
	public ModelAndView eliminaSucursalGCC(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idSuc = request.getParameter("idSuc");

			boolean res = sucursalbi.eliminaSucursalGCC(Integer.parseInt(idSuc));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "SUCURSAL GCC ELIMINADA CORRECTAMENTE");
			mv.addObject("res", res);
			return mv; 
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/eliminaUsuarioA.json?idUsuario=<?>
	@RequestMapping(value = "/eliminaUsuarioA", method = RequestMethod.GET)
	public ModelAndView eliminaUsuarioA(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idUsuario = request.getParameter("idUsuario");

			boolean res = usuarioabi.eliminaUsuario(Integer.parseInt(idUsuario));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "FRTA Usuario ELIMINADO CORRECTAMENTE");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/eliminaPais.json?idPais=<?>
	@RequestMapping(value = "/eliminaPais", method = RequestMethod.GET)
	public ModelAndView eliminaPais(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idPais = request.getParameter("idPais");

			boolean res = paisbi.eliminaPais(Integer.parseInt(idPais));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "PAIS ELIMINADO CORRECTAMENTE");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/eliminaHorario.json?idHorario=<?>
	@RequestMapping(value = "/eliminaHorario", method = RequestMethod.GET)
	public ModelAndView eliminaHorario(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idHorario = request.getParameter("idHorario");

			boolean res = horariobi.eliminaHorario(Integer.parseInt(idHorario));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "HORARIO ELIMINADO CORRECTAMENTE");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/eliminaParametro.json?&setClave=<?>
	@RequestMapping(value = "/eliminaParametro", method = RequestMethod.GET)
	public ModelAndView eliminaParametro(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String clave = request.getParameter("setClave");

			boolean res = parametrobi.eliminaParametro(clave);

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "PARAMETRO ELIMINADO CORRECTAMENTE");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/eliminaNegocio.json?idNegocio=<?>
	@RequestMapping(value = "/eliminaNegocio", method = RequestMethod.GET)
	public ModelAndView eliminaNegocio(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idNegocio = request.getParameter("idNegocio");

			boolean res = negociobi.eliminaNegocio(Integer.parseInt(idNegocio));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "NEGOCIO ELIMINADO CORRECTAMENTE");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosService/eliminaNivel.json?idNivel=<?>
	@RequestMapping(value = "/eliminaNivel", method = RequestMethod.GET)
	public ModelAndView eliminaNivel(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idNivel = request.getParameter("idNivel");

			boolean res = nivelbi.eliminaNivel(Integer.parseInt(idNivel));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "NIVEL ELIMINADO CORRECTAMENTE");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}
	
	// http://localhost:8080/checklist/catalogosService/eliminaCecos.json
	@RequestMapping(value = "/eliminaCecos", method = RequestMethod.GET)
	public ModelAndView eliminaCecos(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {

			int res = cecobi.eliminaCecosTrabajo();
			ModelAndView mv = new ModelAndView("muestraServicios");
			mv.addObject("tipo", "CECOS TRABAJO");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;  
		}
	}
	
	
	
	
	// http://localhost:8080/checklist/catalogosService/eliminaPerfilUsuario.json?idUsuario=<?>&idPerfil=<?>
	@RequestMapping(value = "/eliminaPerfilUsuario", method = RequestMethod.GET)
	public ModelAndView eliminaPerfilUsuario(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			
			String idUsuario = request.getParameter("idUsuario");
			String idPerfil = request.getParameter("idPerfil");

			PerfilUsuarioDTO perfilUsuarioDTO = new PerfilUsuarioDTO();
			perfilUsuarioDTO.setIdUsuario(Integer.parseInt(idUsuario));
			perfilUsuarioDTO.setIdPerfil(Integer.parseInt(idPerfil));
			
		
			boolean res = perfilusuariobi.eliminaPerfilUsaurio(perfilUsuarioDTO);
			
			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Perfil eliminado ");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;  
		}
	}
	
	
	// http://localhost:8080/checklist/catalogosService/eliminaRecursoPerfil.json?idRecurso=<?>&idPerfil=<?>
	@RequestMapping(value = "/eliminaRecursoPerfil", method = RequestMethod.GET)
	public ModelAndView eliminaRecursoPerfil(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			
			String idRecurso = request.getParameter("idRecurso");
			String idPerfil = request.getParameter("idPerfil");

			boolean res = recursoperfilbi.eliminaRecursoP(Integer.parseInt(idRecurso), Integer.parseInt(idPerfil));
			
			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Recurso Perfil eliminado ");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;  
		}
	}
	
	// http://localhost:8080/checklist/catalogosService/eliminaRecurso.json?idRecurso=<?>
	@RequestMapping(value = "/eliminaRecurso", method = RequestMethod.GET)
	public ModelAndView eliminaRecurso(HttpServletRequest request, HttpServletResponse response)
	{
		try {

			String idRecurso = request.getParameter("idRecurso");

			boolean res = recursobi.eliminaRecurso(Integer.parseInt(idRecurso));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Recurso Eliminado eliminado ");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;  
		}
	}
	
	// http://localhost:8080/checklist/catalogosService/eliminaPosible.json?idPosible=<?>
	@RequestMapping(value = "/eliminaPosible", method = RequestMethod.GET)
	public ModelAndView eliminaPosible(HttpServletRequest request, HttpServletResponse response)
	{
		try {

			String idPosible = request.getParameter("idPosible");

			boolean res = posiblesbi.eliminaPosible(Integer.parseInt(idPosible));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Posible Respuesta  eliminado ");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;  
		}
	}
	
	// http://localhost:8080/checklist/catalogosService/eliminaPosibleTipoPregunta.json?idPosibleTipoPreg=<?>
	@RequestMapping(value = "/eliminaPosibleTipoPregunta", method = RequestMethod.GET)
	public ModelAndView eliminaPosibleTipoPregunta(HttpServletRequest request, HttpServletResponse response)
	{
		try {

			String idPosibleTipoPreg = request.getParameter("idPosibleTipoPreg");

			boolean res = posibletipopreguntabi.eliminaPosibleTipoPregunta(Integer.parseInt(idPosibleTipoPreg));

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Posible Tipo Pregunta Respuesta  eliminado ");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;  
		}
	}
	
	
	
	// http://localhost:8080/checklist/catalogosService/eliminaDuplicadosCeco.json
	@RequestMapping(value = "/eliminaDuplicadosCeco", method = RequestMethod.GET)
	public ModelAndView eliminaDuplicadosCeco(HttpServletRequest request, HttpServletResponse response)
	{
		try {
	
			boolean res = cecobi.eliminaDuplicados();

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Elimina CECOS duplicados ");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;  
		}
	}
	
	
	// http://localhost:8080/checklist/catalogosService/eliminaChecklistNegocio.json?idChecklist=<?>&idNegocio=<?>
	@RequestMapping(value = "/eliminaChecklistNegocio", method = RequestMethod.GET)
	public ModelAndView eliminaChecklistNegocio(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			
			String idChecklist = request.getParameter("idChecklist");
			String idNegocio = request.getParameter("idNegocio");

			boolean res = checklistNegocioBI.eliminaChecklistNegocio(Integer.parseInt(idChecklist), Integer.parseInt(idNegocio));
			
			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Checklist-Negocio eliminado ");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;  
		}
	}
	
	// http://localhost:8080/checklist/catalogosService/eliminaNegocioAdicional.json?idUsuario=<?>&idNegocio=<?>
	@RequestMapping(value = "/eliminaNegocioAdicional", method = RequestMethod.GET)
	public ModelAndView eliminaNegocioAdicional(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			
			String idUsuario = request.getParameter("idUsuario");
			String idNegocio = request.getParameter("idNegocio");

			boolean res = adicionalBI.eliminaNegocioAdicional(Integer.parseInt(idUsuario), Integer.parseInt(idNegocio));
			
			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Negocio Adicional eliminado ");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;  
		}
	}
	
	// http://localhost:8080/checklist/catalogosService/eliminaNegocioPais.json?idNegocio=<?>&idPais=<?>
	@RequestMapping(value = "/eliminaNegocioPais", method = RequestMethod.GET)
	public ModelAndView eliminaNegocioPais(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			
			String idPais = request.getParameter("idPais");
			String idNegocio = request.getParameter("idNegocio");

			boolean res = paisNegocioBI.eliminaPaisNegocio(Integer.parseInt(idNegocio), Integer.parseInt(idPais));
			
			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Pais Neogocio eliminado ");
			mv.addObject("res", res);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;  
		}
	}
	
	// http://localhost:8080/checklist/catalogosService/eliminaEmpFijo.json?idEmpFijo=<?>
			@RequestMapping(value = "/eliminaEmpFijo", method = RequestMethod.GET)
			public ModelAndView eliminaEmpFijo(HttpServletRequest request, HttpServletResponse response)
			{
				try {
					
					String idEmpFijo = request.getParameter("idEmpFijo");

					boolean res = empFijoBI.elimina(idEmpFijo);
					
					ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
					mv.addObject("tipo", "Empleado Fijo Eliminado "+idEmpFijo);
					mv.addObject("res", res);
					return mv;
				} catch (Exception e) {
					logger.info(e);
					return null;  
				}
			}
	
			// http://localhost:8080/checklist/catalogosService/eliminaEstado.json?idEstado=<?>
						@RequestMapping(value = "/eliminaEstado", method = RequestMethod.GET)
						public ModelAndView eliminaEstado(HttpServletRequest request, HttpServletResponse response)
						{
							try {
								
								String idEstado = request.getParameter("idEstado");

								boolean res = estadosBI.elimina(idEstado);
								
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Estado Eliminado "+idEstado);
								mv.addObject("res", res);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;  
							}
						}
						
						// http://localhost:8080/checklist/catalogosService/eliminaBitaG.json?idBitaGral=<?>
						@RequestMapping(value = "/eliminaBitaG", method = RequestMethod.GET)
						public ModelAndView eliminaBitaG(HttpServletRequest request, HttpServletResponse response)
						{
							try {
								
								String idBitaGral = request.getParameter("idBitaGral");

								boolean res = bitacoraGralBI.elimina(idBitaGral);
								
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Bitacora Eliminado "+idBitaGral);
								mv.addObject("res", res);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;  
							}
						}
						// http://localhost:8080/checklist/catalogosService/eliminaBitaH.json?idBitaGral=<?>
						@RequestMapping(value = "/eliminaBitaH", method = RequestMethod.GET)
						public ModelAndView eliminaBitaH(HttpServletRequest request, HttpServletResponse response)
						{
							try {
								
								String idBitaGral = request.getParameter("idBitaGral");

								boolean res = bitacoraGralBI.eliminaH(idBitaGral);
								
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Bitacora Eliminada Hija "+idBitaGral);
								mv.addObject("res", res);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;  
							}
						}
						
						// http://localhost:8080/checklist/catalogosService/eliminafirmaCheck.json?idFirma=<?>
						@RequestMapping(value = "/eliminafirmaCheck", method = RequestMethod.GET)
						public ModelAndView eliminafirmaCheck(HttpServletRequest request, HttpServletResponse response)
						{
							try {
								
								String idFirma =request.getParameter("idFirma");

								boolean res = firmaCheckBI.elimina(idFirma);
								
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Firma Check eliminada "+idFirma);
								mv.addObject("res", res);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;  
							}
						}
						
						// http://localhost:8080/checklist/catalogosService/eliminaImagenSuc.json?idImag=<?>
						@RequestMapping(value = "/eliminaImagenSuc", method = RequestMethod.GET)
						public ModelAndView eliminaImagenSuc(HttpServletRequest request, HttpServletResponse response)
						{
							try {
								
								String idImag =request.getParameter("idImag");

								boolean res = sucursalChecklistBI.elimina(idImag);
								
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Imagen Eliminada "+idImag);
								mv.addObject("res", res);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;  
							}
						}
						
						// http://localhost:8080/checklist/catalogosService/eliminaCheckin.json?idCheckin=<?>
						@RequestMapping(value = "/eliminaCheckin", method = RequestMethod.GET)
						public ModelAndView eliminaCheckin(HttpServletRequest request, HttpServletResponse response)
						{
							try {
								
								String idCheckinS =request.getParameter("idCheckin");
								int idCheckin=0;
								if(idCheckinS!=null) {
									idCheckin=Integer.parseInt(idCheckinS);
								}
								

								boolean res = checkinBI.eliminaCheckin(idCheckin);
								
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Checkin Eliminado");
								mv.addObject("res", res);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;  
							}
						}
						// http://localhost:8080/checklist/catalogosService/eliminaSoft.json?ceco=<?>&recorrido=<?>
						@RequestMapping(value = "/eliminaSoft", method = RequestMethod.GET)
						public ModelAndView eliminaSoft(HttpServletRequest request, HttpServletResponse response)
						{
							try {
								
								String ceco =request.getParameter("ceco");
								String recorrido=request.getParameter("recorrido");

								boolean res = softOpeningBI.elimina(ceco,recorrido);
								
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Soft  Eliminada ceco: "+ceco);
								mv.addObject("res", res);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;  
							}
						}
						
						// http://localhost:8080/checklist/catalogosService/eliminaAsignacionExp.json?ceco=<?>&usuario_asig=<?>
						@RequestMapping(value = "/eliminaAsignacionExp", method = RequestMethod.GET)
						public ModelAndView eliminaAsignacionExp(HttpServletRequest request, HttpServletResponse response)
						{
							try {
								
								String usuario_asig =request.getParameter("usuario_asig");
								String ceco =request.getParameter("ceco");
						

								boolean res = asignaExpBI.elimina(ceco,usuario_asig);
								
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "  Eliminada : "+usuario_asig);
								mv.addObject("res", res);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;  
							}
						}
						
						
						// http://localhost:8080/checklist/catalogosService/eliminaVersionExp.json?idVers=<?>
						@RequestMapping(value = "/eliminaVersionExp", method = RequestMethod.GET)
						public ModelAndView eliminaVersionExp(HttpServletRequest request, HttpServletResponse response)
						{
							try {
								
								String idVers =request.getParameter("idVers");
						

								boolean res = versionExpanBI.elimina(idVers);
								
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "  Version Eliminada : "+idVers);
								mv.addObject("res", res);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;  
							}
						}
						
						// http://localhost:8080/checklist/catalogosService/eliminaVersionGral.json?idVers=<?>
						@RequestMapping(value = "/eliminaVersionGral", method = RequestMethod.GET)
						public ModelAndView eliminaVersionGral(HttpServletRequest request, HttpServletResponse response)
						{
							try {
								
								String idVers =request.getParameter("idVers");
						

								boolean res = versionChecklistGralBI.elimina(idVers);
								
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "  Version Eliminada : "+idVers);
								mv.addObject("res", res);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;  
							}
						}
						
						// http://localhost:8080/checklist/catalogosService/eliminaContactoExt.json?bitGral=<?>
						@RequestMapping(value = "/eliminaContactoExt", method = RequestMethod.GET)
						public ModelAndView eliminaContactoExt(HttpServletRequest request, HttpServletResponse response)
						{
							try {
								
								String bitGral =request.getParameter("bitGral");

								boolean res = empContacExtBI.elimina(bitGral);
								
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "  Contactos de la bitacora Gral : "+bitGral+"  Eliminado");
								mv.addObject("res", res);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;  
							}
						}
						
						// http://10.51.219.179:8080/checklist/catalogosService/eliminaFecha.json?fechaInicial=01/01/2020&fechaFinal=01/01/2020
						@RequestMapping(value = "/eliminaFecha", method = RequestMethod.GET)
						public ModelAndView eliminaFecha(HttpServletRequest request, HttpServletResponse response)
						{
							try {
								
								String fechaInicial =request.getParameter("fechaInicial");
								String fechaFinal = request.getParameter("fechaFinal");
								
						
								boolean res = consultaFechaBI.EliminaFecha(fechaInicial,fechaFinal);
								
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "FECHAS ELIMINADAS CORRECTAMENTE");
								mv.addObject("res", res);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;  
							}
						}
						
						// http://10.51.219.179:8080/checklist/catalogosService/eliminaHallazgoidResp.json?idResp=<?>
						@RequestMapping(value = "/eliminaHallazgoidResp", method = RequestMethod.GET)
						public ModelAndView eliminaHallazgoidResp(HttpServletRequest request, HttpServletResponse response)
						{
							try {
								
								String idResp =request.getParameter("idResp");
								
						
								boolean res = hallazgosExpBI.eliminaResp(idResp);
								
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "HALLAZGO DEPURADO");
								mv.addObject("res", res);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;  
							}
						}
						
						// http://10.51.219.179:8080/checklist/catalogosService/eliminaHallazgoBitaGral.json?bitGral=<?>
						@RequestMapping(value = "/eliminaHallazgoBitaGral", method = RequestMethod.GET)
						public ModelAndView eliminaHallazgoBitaGral(HttpServletRequest request, HttpServletResponse response)
						{
							try {
								
								String bitGral =request.getParameter("bitGral");
								
						
								boolean res = hallazgosExpBI.eliminaBita(bitGral);
								
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Hallazgos Depurados");
								mv.addObject("res", res);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;  
							}
						}
						
						// http://10.51.219.179:8080/checklist/catalogosService/eliminaEviHallazgoResp.json?bitGral=<?>
						@RequestMapping(value = "/eliminaEviHallazgoResp", method = RequestMethod.GET)
						public ModelAndView eliminaEviHallazgoResp(HttpServletRequest request, HttpServletResponse response)
						{
							try {
								
								String idEvi =request.getParameter("idEvi");
								
						
								boolean res = hallazgosEviExpBI.elimina(idEvi);
								
								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "evidencia Hallazgos Depurados");
								mv.addObject("res", res);
								return mv;
							} catch (Exception e) {
								logger.info(e);
								return null;  
							}
						}
						// http://localhost:8080/checklist/catalogosService/eliminaBitacoras.json?idBitacora=30733
								@RequestMapping(value = "/eliminaBitacoras", method = RequestMethod.GET)
								public ModelAndView eliminaBitacoras(HttpServletRequest request, HttpServletResponse response)
								{
									try {
										
										String idBitacora = request.getParameter("idBitacora");

										boolean res = bitacoraAdministradorBI.eliminaBitacora(Integer.parseInt(idBitacora));
										
										ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
										mv.addObject("tipo", "Bitacora Eliminado "+idBitacora);
										mv.addObject("res", res);
										return mv;
									} catch (Exception e) {
										logger.info(e);
										return null;  
									}
						
								}		
								// http://localhost:8080/checklist/catalogosService/DepuraHallazgos.json?idResp=<?>
								@RequestMapping(value = "/DepuraHallazgos", method = RequestMethod.GET)
								public ModelAndView DepuraHallazgos(HttpServletRequest request, HttpServletResponse response)
								{
									try {
										
										boolean res = hallazgosExpBI.EliminacargaHallazgos();
										
										ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
										mv.addObject("tipo", "HALLAZGOS DEPURADO");
										mv.addObject("res", res);
										return mv;
									} catch (Exception e) {
										logger.info(e);
										return null;  
									}	

								}
								
								// http://10.51.219.179:8080/checklist/catalogosService/bajaCecoDist.json?ceco=392975
								@RequestMapping(value = "/bajaCecoDist", method = RequestMethod.GET)
								public ModelAndView bajaCecoDist(HttpServletRequest request, HttpServletResponse response)
								{
									try {
										
										String ceco = request.getParameter("ceco");
									
										DistribCecoDTO d1 = new DistribCecoDTO();
										d1.setCeco(ceco);
										
										boolean res = distribCecoBI.EliminaDistrib(ceco);
										
										ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
										mv.addObject("tipo", "BAJA CECO DISTRIBUCION");
										mv.addObject("res", res);
										return mv;
									} catch (Exception e) {
										logger.info(e);
										return null;  
									}	

								}
								
								
								// http://10.51.219.179:8080/checklist/catalogosService/bajaHandbook.json?idBook=28
								@RequestMapping(value = "/bajaHandbook", method = RequestMethod.GET)
								public ModelAndView bajaHandbook(HttpServletRequest request, HttpServletResponse response)
								{
									try {
										
										String idBook = request.getParameter("idBook");
									
										AdmHandbookDTO admHandbook = new AdmHandbookDTO();
										admHandbook.setIdBook(idBook);
										
										boolean res = handBookBI.eliminaHandbook(admHandbook);
										
										ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
										mv.addObject("tipo", "BAJA ID HandBook");
										mv.addObject("res", res);
										return mv;
									} catch (Exception e) {
										logger.info(e);
										return null;  
									}	

								}
								// http://10.51.219.179:8080/checklist/catalogosService/eliminaHallazgoBitaHisto.json?bitGral=<?>
								@RequestMapping(value = "/eliminaHallazgoBitaHisto", method = RequestMethod.GET)
								public ModelAndView eliminaHallazgoBitaHisto(HttpServletRequest request, HttpServletResponse response)
								{
									try {
										
										String bitGral =request.getParameter("bitGral");
										
								
										boolean res = hallazgosExpBI.eliminaBitaHisto(bitGral);
										
										ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
										mv.addObject("tipo", "Hallazgos Historico Depurados");
										mv.addObject("res", res);
										return mv;
									} catch (Exception e) {
										logger.info(e);
										return null;  
									}
								}
								
								//http://10.51.219.179:8080/checklist/catalogosService/eliminaProtocolo.json?idProtocolo=<?>
								@RequestMapping(value="/eliminaProtocolo", method = RequestMethod.GET)
								public ModelAndView consultaProtocolo(HttpServletRequest request, HttpServletResponse response){
									try{
										String idProtocolo=request.getParameter("idProtocolo");
														
										boolean respuesta = protocoloBI.eliminaProtocolo(Integer.parseInt(idProtocolo));

										ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
										mv.addObject("tipo", "Elimina  Protocolo");
										mv.addObject("res", respuesta);
										
										return mv;
									}catch(Exception e){
										logger.info(e);
										return null;
									}
								}
								
								// http://10.51.219.179:8080/checklist/catalogosService/eliminaNego.json?idNego=<?>
								@RequestMapping(value = "/eliminaNego", method = RequestMethod.GET)
								public ModelAndView eliminaNego(HttpServletRequest request, HttpServletResponse response)
								{
									try {
										
										String idNego =request.getParameter("idNego");

										boolean res = zonaNegoBI.eliminaNego(idNego);
										
										ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
										mv.addObject("tipo", "negocio  Depurado");
										mv.addObject("res", res);
										return mv;
									} catch (Exception e) {
										logger.info(e);
										return null;  
									}
								}
								// http://10.51.219.179:8080/checklist/catalogosService/eliminaZona.json?idZona=<?>
								@RequestMapping(value = "/eliminaZona", method = RequestMethod.GET)
								public ModelAndView eliminaZona(HttpServletRequest request, HttpServletResponse response)
								{
									try {
										
										String idZona =request.getParameter("idZona");

										boolean res = zonaNegoBI.eliminaZona(idZona);
										
										ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
										mv.addObject("tipo", "zona  Depurado");
										mv.addObject("res", res);
										return mv;
									} catch (Exception e) {
										logger.info(e);
										return null;  
									}
								}
								// http://10.51.219.179:8080/checklist/catalogosService/eliminaZonaNego.json?idtab=<?>
								@RequestMapping(value = "/eliminaZonaNego", method = RequestMethod.GET)
								public ModelAndView eliminaZonaNego(HttpServletRequest request, HttpServletResponse response)
								{
									try {
										
										String idtab =request.getParameter("idtab");

										boolean res = zonaNegoBI.eliminaRela(idtab);
										
										ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
										mv.addObject("tipo", "zona  Depurado");
										mv.addObject("res", res);
										return mv;
									} catch (Exception e) {
										logger.info(e);
										return null;  
									}
								}
								// http://10.51.219.179:8080/checklist/catalogosService/eliminaAdicional.json?bita=<?>
								@RequestMapping(value = "/eliminaAdicional", method = RequestMethod.GET)
								public ModelAndView eliminaAdicional(HttpServletRequest request, HttpServletResponse response)
								{
									try {
										
										String bita =request.getParameter("bita");

										boolean res = adicionalesBI.elimina(bita);
										
										ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
										mv.addObject("tipo", "Adicional Depurado");
										mv.addObject("res", res);
										return mv;
									} catch (Exception e) {
										logger.info(e);
										return null;  
									}
								}
//http://10.51.219.179:8080/checklist/catalogosService/eliminaGerente.json?idUsuario=73433
								@RequestMapping(value="/eliminaGerente", method = RequestMethod.GET)
								public ModelAndView eliminaGerente(HttpServletRequest request, HttpServletResponse response){
									try{
										int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
											AltaGerenteDTO eliminaInf = new AltaGerenteDTO();
											eliminaInf.setIdUsuario(idUsuario);
										boolean respuesta = altaGerenteBI.eliminaDisSup(eliminaInf);

										ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
										mv.addObject("tipo", "Elimina Registro");
										mv.addObject("res", respuesta);
										
										return mv;
									}catch(Exception e){
										logger.info(e);
										return null;
									}
				}
                                                                
                                                                
                                                                
                                                                
                                        //http://10.51.219.179:8080/checklist/catalogosService/deleteComentarioAsgCalidad.json?ceco=<?>&fecha=<?>&comentario=<?>
					@RequestMapping(value="/deleteComentarioAsgCalidad", method = RequestMethod.GET)
					public  ModelAndView deleteComentarioAsgCalidad(HttpServletRequest request, HttpServletResponse response, Model model) {
						try {
                                                    							
                                                        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
                                                        

                                                        String ceco= request.getParameter("ceco");
                                                        String fecha= request.getParameter("fecha");
                                                        String comentario= request.getParameter("coementario");

							Boolean res = comentariosRepAsgBI.deleteComentarios(ceco, fecha, comentario);
                                                        
							mv.addObject("tipo", "ELIMINA_COMENTARIOS_ASIG_CALIADAD");
							mv.addObject("res", res);

							return mv;

						} catch (Exception e) {
							logger.info("Ocurrio algo " + e);
							return null;
						}

					}
					// http://10.51.219.179:8080/checklist/catalogosService/eliminaHallazgoBitaGralTransf.json?bitGral=<?>
					@RequestMapping(value = "/eliminaHallazgoBitaGralTransf", method = RequestMethod.GET)
					public ModelAndView eliminaHallazgoBitaGralTransf(HttpServletRequest request, HttpServletResponse response)
					{
						try {
							
							String bitGral =request.getParameter("bitGral");

							boolean res = hallazgosTransfBI.eliminaBita(bitGral);
							
							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "Hallazgos Trnasf Depurados");
							mv.addObject("res", res);
							return mv;
						} catch (Exception e) {
							logger.info(e);
							return null;  
						}
					}
					// http://10.51.219.179:8080/checklist/catalogosService/eliminaHallazgoBitaHistoTransf.json?bitGral=<?>
					@RequestMapping(value = "/eliminaHallazgoBitaHistoTransf", method = RequestMethod.GET)
					public ModelAndView eliminaHallazgoBitaHistoTransf(HttpServletRequest request, HttpServletResponse response)
					{
						try {
							
							String bitGral =request.getParameter("bitGral");
							boolean res = hallazgosTransfBI.eliminaBitaHisto(bitGral);
							
							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "Hallazgos Historico Depurados");
							mv.addObject("res", res);
							return mv;
						} catch (Exception e) {
							logger.info(e);
							return null;  
						}
					}
					// http://localhost:8080/checklist/catalogosService/eliminaTransf.json?ceco=<?>&recorrido=<?>
					@RequestMapping(value = "/eliminaTransf", method = RequestMethod.GET)
					public ModelAndView eliminaTransf(HttpServletRequest request, HttpServletResponse response)
					{
						try {
							
							String ceco =request.getParameter("ceco");
							String recorrido=request.getParameter("recorrido");

							boolean res = transformacionBI.elimina(ceco,recorrido);
							
							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "Transf  Eliminada ceco: "+ceco);
							mv.addObject("res", res);
							return mv;
						} catch (Exception e) {
							logger.info(e);
							return null;  
						}
					}
					// http://localhost:8080/checklist/catalogosService/eliminaFaseTransf.json?fase=<?>
					@RequestMapping(value = "/eliminaFaseTransf", method = RequestMethod.GET)
					public ModelAndView eliminaFaseTransf(HttpServletRequest request, HttpServletResponse response)
					{
						try {
							
							String fase =request.getParameter("fase");

							boolean res = faseTransfBI.elimina(fase);
							
							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "  Fase Eliminada : "+fase);
							mv.addObject("res", res);
							return mv;
						} catch (Exception e) {
							logger.info(e);
							return null;  
						}
					}
					
					// http://10.51.219.179:8080/checklist/catalogosService/bajaInforme.json?idsec=4
					@RequestMapping(value = "/bajaInforme", method = RequestMethod.GET)
					public ModelAndView bajaInforme(HttpServletRequest request, HttpServletResponse response)
					{
						try {
							
							String idsec = request.getParameter("idsec");
						
							AdmInformProtocolosDTO admInfProtocolos = new AdmInformProtocolosDTO();
							admInfProtocolos.setIdsec(idsec);
							
							boolean res = admInformProtocolosBI.eliminaHandbook(admInfProtocolos);
							
							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "BAJA ID DETALLE INFORME");
							mv.addObject("res", res);
							return mv;
						} catch (Exception e) {
							logger.info(e);
							return null;  
						}	

					}
					
					// http://localhost:8080/checklist/catalogosService/eliminaAgrupaFase.json?idAgrupa=<?>
					@RequestMapping(value = "/eliminaAgrupaFase", method = RequestMethod.GET)
					public ModelAndView eliminaAgrupaFase(HttpServletRequest request, HttpServletResponse response)
					{
						try {
							
							String idAgrupa =request.getParameter("idAgrupa");

							boolean res = faseTransfBI.eliminaAgrupa(idAgrupa);
							
							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "  FaseAgrupador Eliminada : "+idAgrupa);
							mv.addObject("res", res);
							return mv;
						} catch (Exception e) {
							logger.info(e);
							return null;  
						}
					}
					// http://localhost:8080/checklist/catalogosService/eliminaAsignacionTransf.json?ceco=<?>&usuario_asig=<?>
					@RequestMapping(value = "/eliminaAsignacionTransf", method = RequestMethod.GET)
					public ModelAndView eliminaAsignacionTransf(HttpServletRequest request, HttpServletResponse response)
					{
						try {
							
							String usuario_asig =request.getParameter("usuario_asig");
							String ceco =request.getParameter("ceco");
					

							boolean res = asignaTransfBI.elimina(ceco,usuario_asig);
							
							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "  Eliminada : "+usuario_asig);
							mv.addObject("res", res);
							return mv;
						} catch (Exception e) {
							logger.info(e);
							return null;  
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/eliminaFirmaCatalogo.json?idFirmaCatalogo=
					@RequestMapping(value="/eliminaFirmaCatalogo", method = RequestMethod.GET)
					public ModelAndView eliminaFirmaCatalogo(HttpServletRequest request, HttpServletResponse response){
						try{
							int idFirmaCatalogo=Integer.parseInt(request.getParameter("idFirmaCatalogo"));
							
							boolean respuesta = firmasCatalogoBI.eliminaFirmaCat(idFirmaCatalogo);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "baja de Firma :  "+idFirmaCatalogo);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					//idFirmaAgrupa  es la del agrupador el id de firmacatalogo este se puede repetir
					//http://localhost:8080/checklist/catalogosService/eliminaFirmaAgrupa.json?idFirmaCatalogo=
					@RequestMapping(value="/eliminaFirmaAgrupa", method = RequestMethod.GET)
					public ModelAndView eliminaFirmaAgrupa(HttpServletRequest request, HttpServletResponse response){
						try{
							int idFirmaCatalogo=Integer.parseInt(request.getParameter("idFirmaCatalogo"));
							
							boolean respuesta = firmasCatalogoBI.eliminaFirmaAgrupa(idFirmaCatalogo);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "elimina  Firma con agrupador :  "+idFirmaCatalogo);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/eliminaFragmentCatalogo.json?idFragment=
					@RequestMapping(value="/eliminaFragmentCatalogo", method = RequestMethod.GET)
					public ModelAndView eliminaFragmentCatalogo(HttpServletRequest request, HttpServletResponse response){
						try{
							
							String idFragment=request.getParameter("idFragment");

							
							boolean respuesta = fragmentMenuBI.eliminaFragment(idFragment);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "elimina Fragment :  "+idFragment);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/eliminaMenuOpcionesP.json?idConfigMenu=
					@RequestMapping(value="/eliminaMenuOpcionesP", method = RequestMethod.GET)
					public ModelAndView eliminaMenuOpcionesP(HttpServletRequest request, HttpServletResponse response){
						try{
							
							String idConfigMenu=request.getParameter("idConfigMenu");
						
							boolean respuesta = fragmentMenuBI.eliminaMenu(idConfigMenu);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "elimina :  "+idConfigMenu);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/eliminaConfigProyecto.json?idConfigProyecto=
					@RequestMapping(value="/eliminaConfigProyecto", method = RequestMethod.GET)
					public ModelAndView eliminaConfigProyecto(HttpServletRequest request, HttpServletResponse response){
						try{
							
					
							String idConfigProyecto=request.getParameter("idConfigProyecto");
							
							boolean respuesta = fragmentMenuBI.eliminaConf(idConfigProyecto);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "elimina  :  "+idConfigProyecto);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					//http://localhost:8080/checklist/catalogosService/eliminaSoftNvo.json?idSoft=
					@RequestMapping(value="/eliminaSoftNvo", method = RequestMethod.GET)
					public ModelAndView eliminaSoftNvo(HttpServletRequest request, HttpServletResponse response){
						try{
							
							int idSoft=Integer.parseInt(request.getParameter("idSoft"));
							
							boolean respuesta = softNvoBI.eliminaSoftN(idSoft);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "elimina  idsoft :  "+idSoft);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					//http://localhost:8080/checklist/catalogosService/eliminaCalculosSoft.json?idCalSoft=
					@RequestMapping(value="/eliminaCalculosSoft", method = RequestMethod.GET)
					public ModelAndView eliminaCalculosSoft(HttpServletRequest request, HttpServletResponse response){
						try{
							
							int idCalSoft=Integer.parseInt(request.getParameter("idCalSoft"));
							
							boolean respuesta = softNvoBI.eliminaCalculosActa(idCalSoft);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "elimina calculos soft :  "+idCalSoft);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/eliminaActorHallazgo.json?idActor=
					@RequestMapping(value="/eliminaActorHallazgo", method = RequestMethod.GET)
					public ModelAndView eliminaActorHallazgo(HttpServletRequest request, HttpServletResponse response){
						try{
							
							int idActor=Integer.parseInt(request.getParameter("idActor"));
							
							boolean respuesta = actorEdoHallaBI.eliminaActorHalla(idActor);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "elimina calculos soft :  "+idActor);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					//http://localhost:8080/checklist/catalogosService/eliminaGpoEdoHallazgo.json?idGpoEdo=
					@RequestMapping(value="/eliminaGpoEdoHallazgo", method = RequestMethod.GET)
					public ModelAndView eliminaGpoEdoHallazgo(HttpServletRequest request, HttpServletResponse response){
						try{
							
							int idGpoEdo=Integer.parseInt(request.getParameter("idGpoEdo"));
							
							boolean respuesta = actorEdoHallaBI.eliminaConfEdoHalla(idGpoEdo);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "elimina GpoEdoHalla  :  "+idGpoEdo);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					//http://localhost:8080/checklist/catalogosService/eliminaConfigActor.json?idConfigActor=
					@RequestMapping(value="/eliminaConfigActor", method = RequestMethod.GET)
					public ModelAndView eliminaConfigActor(HttpServletRequest request, HttpServletResponse response){
						try{
							
							String idConfigActor=request.getParameter("idConfigActor");
							
							boolean respuesta = configuraActorBI.eliminaConfActor(idConfigActor);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "elimina confActor  :  "+idConfigActor);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/eliminaMatrizHallazgo.json?idMatriz=
					@RequestMapping(value="/eliminaMatrizHallazgo", method = RequestMethod.GET)
					public ModelAndView eliminaMatrizHallazgo(HttpServletRequest request, HttpServletResponse response){
						try{
							
							String idMatriz=request.getParameter("idMatriz");
							
							boolean respuesta = hallagosMatrizBI.eliminaHallazgoMatriz(idMatriz);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "elimina id matriz  :  "+idMatriz);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/eliminaHallazgosGenerico.json?idBitacora=&ceco=			
					@RequestMapping(value="/eliminaHallazgosGenerico", method = RequestMethod.GET)
					public ModelAndView eliminaHallazgosGenerico(HttpServletRequest request, HttpServletResponse response){
						try{
							
							//elimina Hallazgos de historico y de tabla hallazgos si no le mando bitacora borra por ceco 
							String idBitacora=null;
							idBitacora=request.getParameter("idBitacora");
							String ceco=request.getParameter("ceco");
							
							boolean respuesta = soporteHallazgosTransfBI.eliminaHallazgosEHistorico(idBitacora,ceco);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "elimina id bitacora  :  "+idBitacora+" ceco--> "+ceco);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/eliminaCheckInAsistencia.json?idChekAsistencia=		
					@RequestMapping(value="/eliminaCheckInAsistencia", method = RequestMethod.GET)
					public ModelAndView eliminaCheckInAsistencia(HttpServletRequest request, HttpServletResponse response){
						try{
							
							
							String idChekAsistencia=request.getParameter("idChekAsistencia");
							
							boolean respuesta = checkInAsistenciaBI.eliminaAsistencia(idChekAsistencia);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "elimina id asistencia  :  "+idChekAsistencia);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/eliminaDatosIdInforme.json?idInforme=		
					@RequestMapping(value="/eliminaDatosIdInforme", method = RequestMethod.GET)
					public ModelAndView eliminaDatosIdInforme(HttpServletRequest request, HttpServletResponse response){
						try{
							
							
							String idInforme=request.getParameter("idInforme");
							
							boolean respuesta = datosInformeBI.eliminaDatosInforme(idInforme);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "elimina id asistencia  :  "+idInforme);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
}
