package com.gruposalinas.checklist.dao;

import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AsignacionPerfilesDAOImpl extends DefaultDAO implements AsignacionPerfilesDAO {

    private static final Logger logger = LogManager.getLogger(LoginDAOImpl.class);
    private DefaultJdbcCall jdbcAsignacionPerfil;
    private DefaultJdbcCall jdbcActualizarPerfilesGestion;
    private DefaultJdbcCall jdbcAltaPerfilSistemas;
    private DefaultJdbcCall jdbcAltaPerfilScouting;
    private DefaultJdbcCall jdbcAltaPerfilPrestaPrenda;
    private DefaultJdbcCall jdbcAltaPerfilDescargaGestion;

    public void init() {
        jdbcAsignacionPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNA_PERFILES")
                .withProcedureName("SP_ASIGNA_PERFILES");

        jdbcActualizarPerfilesGestion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAASIGNA_PERFILES")
                .withProcedureName("SP_ACTUALIZA_PERFILES_GESTION");

        jdbcAltaPerfilSistemas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAKGPERFSIST")
                .withProcedureName("SP_ASIGPER_SIST");

        jdbcAltaPerfilScouting = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAKGPERFSIST")
                .withProcedureName("SP_PERFSCOUTING");

        jdbcAltaPerfilPrestaPrenda = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAKGPERFSIST")
                .withProcedureName("SP_PERFPP");

        jdbcAltaPerfilDescargaGestion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAKGPERFSIST")
                .withProcedureName("SP_PERFAPLICAC");
    }

    @SuppressWarnings("unchecked")
    public boolean actualizarPerfilesGestion() throws Exception {
        Map<String, Object> out = null;
        boolean estatusEjecucion = false;

        try {

            out = jdbcActualizarPerfilesGestion.execute();

            logger.info("Funcion ejecutada:{FRANQUICIA.PAASIGNA_PERFILES.SP_ACTUALIZA_PERFILES_GESTION}");

            BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
            int respuesta = respuestaEjec.intValue();

            if (respuesta == 1) {
                //throw new Exception("Algo ocurrió en el SP");
                estatusEjecucion = false;

            } else {
                estatusEjecucion = true;
            }

        } catch (Exception e) {
            estatusEjecucion = false;
            e.printStackTrace();

        }

        return estatusEjecucion;
    }

    @SuppressWarnings("unchecked")
    public boolean asignacionPerfiles() throws Exception {
        Map<String, Object> out = null;
        boolean estatusEjecucion = false;

        try {

            out = jdbcAsignacionPerfil.execute();

            logger.info("Funcion ejecutada:{FRANQUICIA.PAASIGNA_PERFILES.SP_ASIGNA_PERFILES}");

            BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
            int respuesta = respuestaEjec.intValue();
            logger.info("respuesta: " + respuesta);
            if (respuesta == 1) {
                //throw new Exception("Algo ocurrió en el SP");
                estatusEjecucion = false;
            } else {
                estatusEjecucion = true;
            }

        } catch (Exception e) {
            estatusEjecucion = false;
            e.printStackTrace();

        }

        return estatusEjecucion;
    }

    @SuppressWarnings("unchecked")
    public boolean altaPerfilesSistemas() throws Exception {
        Map<String, Object> out = null;
        boolean estatusEjecucion = false;

        try {

            out = jdbcAltaPerfilSistemas.execute();

            logger.info("Funcion ejecutada:{FRANQUICIA.PAKGPERFSIST.SP_ASIGPER_SIST}");

            BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
            int respuesta = respuestaEjec.intValue();
            logger.info("respuesta: " + respuesta);
            if (respuesta == 1) {
                //throw new Exception("Algo ocurrió en el SP");
                estatusEjecucion = false;
            } else {
                estatusEjecucion = true;
            }

        } catch (Exception e) {
            estatusEjecucion = false;
            e.printStackTrace();

        }

        return estatusEjecucion;
    }

    @Override
    public boolean altaPerfilesScouting() throws Exception {
        Map<String, Object> out = null;
        boolean estatusEjecucion = false;

        try {

            out = jdbcAltaPerfilScouting.execute();

            logger.info("Funcion ejecutada:{FRANQUICIA.PAKGPERFSIST.SP_PERFSCOUTING}");

            BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
            int respuesta = respuestaEjec.intValue();
            logger.info("respuesta: " + respuesta);
            if (respuesta == 1) {
                //throw new Exception("Algo ocurrió en el SP");
                estatusEjecucion = false;
            } else {
                estatusEjecucion = true;
            }

        } catch (Exception e) {
            estatusEjecucion = false;
            e.printStackTrace();

        }

        return estatusEjecucion;
    }

    @Override
    public boolean altaPerfilesPrestaPrenda() throws Exception {
        Map<String, Object> out = null;
        boolean estatusEjecucion = false;

        try {

            out = jdbcAltaPerfilPrestaPrenda.execute();

            logger.info("Funcion ejecutada:{FRANQUICIA.PAKGPERFSIST.SP_PERFPP}");

            BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
            int respuesta = respuestaEjec.intValue();
            logger.info("respuesta: " + respuesta);
            if (respuesta == 1) {
                //throw new Exception("Algo ocurrió en el SP");
                estatusEjecucion = false;
            } else {
                estatusEjecucion = true;
            }

        } catch (Exception e) {
            estatusEjecucion = false;
            e.printStackTrace();

        }

        return estatusEjecucion;
    }

    @Override
    public boolean altaPerfilesDescargaGestion() throws Exception {
        Map<String, Object> out = null;
        boolean estatusEjecucion = false;

        try {

            out = jdbcAltaPerfilDescargaGestion.execute();

            logger.info("Funcion ejecutada:{FRANQUICIA.PAKGPERFSIST.SP_PERFAPLICAC}");

            BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
            int respuesta = respuestaEjec.intValue();
            logger.info("respuesta: " + respuesta);
            if (respuesta == 1) {
                //throw new Exception("Algo ocurrió en el SP");
                estatusEjecucion = false;
            } else {
                estatusEjecucion = true;
            }

        } catch (Exception e) {
            estatusEjecucion = false;
            e.printStackTrace();

        }

        return estatusEjecucion;
    }

}
