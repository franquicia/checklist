package com.gruposalinas.checklist.domain;

public class ReporteSupervisionSistemasDTO {
	
	private int idCecos;
	private String nombreCeco;
	private int terminados;
	private int asignados;
	private int recursos;
	private int planeado;
	private String usuario;
	
	private int idChecklist;
	private String nombreSucursal;
	
	private String finicio, ffin, evaluado, sucursal, region, zona, territorio, checklist;
	private int idBitacora;
	
	public int getIdCecos() {
		return idCecos;
	}
	public void setIdCecos(int idCecos) {
		this.idCecos = idCecos;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	public int getTerminados() {
		return terminados;
	}
	public void setTerminados(int terminados) {
		this.terminados = terminados;
	}
	public int getAsignados() {
		return asignados;
	}
	public void setAsignados(int asignados) {
		this.asignados = asignados;
	}
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public String getNombreSucursal() {
		return nombreSucursal;
	}
	public void setNombreSucursal(String nombreSucursal) {
		this.nombreSucursal = nombreSucursal;
	}
	public String getFinicio() {
		return finicio;
	}
	public void setFinicio(String finicio) {
		this.finicio = finicio;
	}
	public String getFfin() {
		return ffin;
	}
	public void setFfin(String ffin) {
		this.ffin = ffin;
	}
	public String getEvaluado() {
		return evaluado;
	}
	public void setEvaluado(String evaluado) {
		this.evaluado = evaluado;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	public String getTerritorio() {
		return territorio;
	}
	public void setTerritorio(String territorio) {
		this.territorio = territorio;
	}
	public String getChecklist() {
		return checklist;
	}
	public void setChecklist(String checklist) {
		this.checklist = checklist;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public int getRecursos(){
		return this.recursos;
	}
	
	public void setRecursos(int recursos){
		this.recursos=recursos;
	}
	
	public int getPlaneado(){
		return this.planeado;
	}
	
	public void setPlaneado(int planeado){
		this.planeado=planeado;
	}
	
	@Override
	public String toString() {
		return "ReporteSupervisionSistemasDTO [idCecos=" + idCecos + ", nombreCeco=" + nombreCeco + ", terminados="
				+ terminados + ", asignados=" + asignados + ", RECURSOS "+this.recursos+ ", PLANEADO "+this.planeado +", idChecklist=" + idChecklist + ", nombreSucursal="
				+ nombreSucursal + ", finicio=" + finicio + ", ffin=" + ffin + ", evaluado=" + evaluado + ", sucursal="
				+ sucursal + ", region=" + region + ", zona=" + zona + ", territorio=" + territorio + ", checklist="
				+ checklist + ", idBitacora=" + idBitacora + "]";
	}
	
}
