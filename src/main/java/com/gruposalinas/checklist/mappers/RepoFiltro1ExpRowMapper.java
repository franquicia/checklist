package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.RepoFilExpDTO;

public class RepoFiltro1ExpRowMapper implements RowMapper<RepoFilExpDTO> {

	public RepoFilExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		RepoFilExpDTO com = new RepoFilExpDTO();

	    com.setCeco(rs.getString("FCID_CECO"));
	    com.setNombreCeco(rs.getString("FCNOMBRE"));
	    com.setActivoCeco(rs.getInt("FIACTIVO"));
	    com.setCecoSuperior(rs.getString("FICECO_SUPERIOR"));
	    com.setDireccion(rs.getString("DIRECCION"));
	    com.setRegion(rs.getString("FCREGION"));
	    com.setTerritorio(rs.getString("FCTERRITORIO"));
	    com.setZona(rs.getString("FCZONA"));
	    com.setCalif(rs.getDouble("CALIFICACION"));
	    com.setPrecalif(rs.getDouble("PRECALIFICACION"));
	    com.setIdRecorrido(rs.getString("FCRECORRIDO"));
	    com.setBitGral(rs.getInt("FIID_BITGRAL"));
	    com.setAperturable(rs.getInt("APERTURABLE"));
	    com.setPerido(rs.getString("FCPERIODO"));
	    com.setCalif(rs.getDouble("CALIFICACION"));
	    com.setPrecalif(rs.getDouble("PRECALIFICACION"));
	    com.setNomUsu(rs.getString("LIDEROBRA"));
	    com.setAux(rs.getString("USU"));
	    com.setPondTot(rs.getDouble("POND_SI_NA"));
	    
		return com;
	}

}
