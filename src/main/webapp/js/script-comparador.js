var rutaHostImagenL = "";
var categoriaL = "";
var tipoL = "";
var puestoL = "";
var usuarioL = "";
var costosL = "";
var auxCecoL = "";
var auxL = "";
var fechaL = "";
var tamObjL = 0;
var noDivL=0;

var keyMapaL = "";
var idPreguntaL = "";
var idCheckUsuaL = "";
var idRutaL = "";
var fechaImagenL = "";
var direccionL=0;

function init(noDiv) {
	rutaHostImagenL = rutaHostImagen;
	categoriaL = categoria;
	tipoL = tipo;
	puestoL = puesto;
	usuarioL = usuario;
	costosL = costos;
	auxCecoL = auxCeco;
	auxL = aux;
	fechaL = fecha;
	tamObjL=tamObj;
		
	for (var i = 1; i <= tamObjL; i++) {
		document.getElementById("div" + i).style.display = 'none';
	}
	muestraVista(noDiv);
}

function carrucelImagen(categoria, tipo, puesto, usuario, costos, auxCeco, aux, fecha, keyMapa, idRuta) {
	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxMapa.json?categoria=' + categoria + '&tipo=' + tipo + '&puesto=' + puesto
				+ '&usuario=' + usuario + '&costos=' + costos + '&auxCeco=' + auxCeco + '&aux=' + aux + '&fecha=' + fecha + '&keyMapa='	+ keyMapa,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			document.getElementById("carruselSimple").innerHTML = "";
			var i = 0;
			if (json[i] == null) {
				alert('Algo paso');
			}
			while (json[i] != null) {
				if (json[i].idRuta == idRuta) {
					$("#carruselSimple")
						.append(
							"<div class='item active'><img src='"+ rutaHostImagenL+ json[i].ruta	+ "' alt='"	+ json[i].idRuta+ "'>"
								+ "<div class='pregCarrSimple'></div>"
								+ "<div id='"+ json[i].idPregunta+ "'></div>"
								+ "<div id='"+ json[i].idCheckUsua+ "'></div>"
								+ "<div id='"+ json[i].nombreGeografia+ "'></div>"
								+ "<div id='"+ json[i].fechaRespuesta+ "'></div>"
								+ "<div class='texto-Encarousel-p'>"
								+ "<p class='margenEspacioDoble basico'/>"
								+ "<p class='letraPregBold basico' id='desPreguntaCarousel' style='color: white;'>"
								+ json[i].desPregunta+ "</p>"
								+ "<p class='letraPregItalic basico' id='respuestaCarusel' style='color: white;'>"
								+ "Respuesta: "+json[i].respuesta+ "</p>"
								+ "<p class='letraPregRegular basico' id='nombreCecoCarousel' style='color: white;'>"
								+ json[i].nombreCeco+ " / "+ json[i].nombreUsuario
								+ "</p>" + "</div></div>");

				} else if (json[i].idRuta != idRuta) {
					$("#carruselSimple")
						.append(
							"<div class='item'><img src='"+ rutaHostImagenL+ json[i].ruta+ "' alt='"	+ json[i].idRuta+ "'>"
								+ "<div class='pregCarrSimple'></div>"
								+ "<div id='"+ json[i].idPregunta+ "'></div>"
								+ "<div id='"+ json[i].idCheckUsua+ "'></div>"
								+ "<div id='"+ json[i].nombreGeografia+ "'></div>"
								+ "<div id='"+ json[i].fechaRespuesta+ "'></div>"
								+ "<div class='texto-Encarousel-p'>"
								+ "<p class='margenEspacioDoble basico'/>"
								+ "<p class='letraPregBold basico' id='desPreguntaCarousel' style='color: white;'>"
								+ json[i].desPregunta+ "</p>"
								+ "<p class='letraPregItalic basico' id='respuestaCarusel' style='color: white;'>"
								+ "Respuesta: "+json[i].respuesta	+ "</p>"
								+ "<p class='letraPregRegular basico' id='nombreCecoCarousel' style='color: white;'>"
								+ json[i].nombreCeco+ " / "	+ json[i].nombreUsuario
								+ "</p>" + "</div></div>");
				}
				i++;
			}
		},
		error : function() {
			alert('Algo paso');
		}
	});
}


function carrucelDobleImagen(direccion) {
	direccionL=direccion;
	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxMapa.json?categoria=' + categoriaL + '&tipo=' + tipoL + '&puesto=' + puestoL + '&usuario=' + usuarioL
				+ '&costos=' + costosL + '&auxCeco=' + auxCecoL + '&aux=' + auxL + '&fecha=' + fechaL + '&keyMapa=' + keyMapaL,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			//document.getElementById("carruselSimple").innerHTML = "";
			var i = 0;
			if (json[i] == null) {
				alert('Algo paso');
			}
			while (json[i] != null) {
				if (json[i].idRuta == idRutaL) {
					if(i==0 && direccionL==-1){
						i=json.length-1;
						idRutaL=json[i].idRuta;
						idPreguntaL = json[i].idPregunta;
						idCheckUsuaL = json[i].idCheckUsua;
						fechaImagenL = json[i].fechaRespuesta;
						dobleImagen(json[i].idRuta, json[i].idPregunta,	json[i].idCheckUsua, json[i].fechaRespuesta);
					}else{
						i=i+direccionL;
						if(json[i]!= null){
							idRutaL=json[i].idRuta;
							idPreguntaL = json[i].idPregunta;
							idCheckUsuaL = json[i].idCheckUsua;
							fechaImagenL = json[i].fechaRespuesta;
							dobleImagen(json[i].idRuta, json[i].idPregunta,	json[i].idCheckUsua, json[i].fechaRespuesta);
						}else{
							i=0;
							idRutaL=json[i].idRuta;
							idPreguntaL = json[i].idPregunta;
							idCheckUsuaL = json[i].idCheckUsua;
							fechaImagenL = json[i].fechaRespuesta;
							dobleImagen(json[i].idRuta, json[i].idPregunta,	json[i].idCheckUsua, json[i].fechaRespuesta);
						}
					}
				}
				i++;
			}
		},
		error : function() {
			alert('Algo paso');
		}
	});
}

function dobleImagen(idRuta, idPregunta, idCheckUsua, fechaImagen) {
	$
	.ajax({
		type : "GET",
		url : urlServer + '../ajaxCompara.json?idRuta='+ idRuta + '&idPregunta=' + idPregunta
				+ '&idCheckUsuario=' + idCheckUsua + '&fechaImagen='+ fechaImagen,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			if (json != null) {
				if (json.length < 2) {
					if(direccionL!=0){
						carrucelDobleImagen(direccionL)
					}else{
						alert('NO EXISTE UNA FOTO ANTERIOR A ESTA ')
					}
				} else {
					$('#carouselSimple').hide();
					$('#tCom').hide();
					$('#carouserDoble').show();
					$('#selOp2').show();
					$('#selOp22').show();
					
					document.getElementById('imaDobleIzq').src = ""	+ rutaHostImagenL + json[0].ruta;
					document.getElementById('imaDobleDer').src = ""	+ rutaHostImagenL + json[1].ruta;
					document.getElementById('fechaRespuestaIzq').innerHTML = json[0].fechaRespuesta;
					document.getElementById('fechaRespuestaDer').innerHTML = json[1].fechaRespuesta;
					document.getElementById('desPreguntaIzq').innerHTML = json[0].desPregunta;
					document.getElementById('desPreguntaDer').innerHTML = json[1].desPregunta;
					document.getElementById('respuestaIzq').innerHTML = "Respuesta: "+ json[0].respuesta;
					document.getElementById('respuestaDer').innerHTML = "Respuesta: "+ json[1].respuesta;
					document.getElementById('nombreCecoIzq').innerHTML = json[0].nombreCeco	+ " / " + json[0].nombreUsuario;
					document.getElementById('nombreCecoDer').innerHTML = json[1].nombreCeco	+ " / " + json[1].nombreUsuario;
				}
			}
		},
		error : function() {
			alert('Algo paso');
		}
	});
}


function carrucelDiferenciaImagen(direccion) {
	direccionL=direccion;
	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxMapa.json?categoria=' + categoriaL + '&tipo=' + tipoL + '&puesto=' + puestoL + '&usuario=' + usuarioL
				+ '&costos=' + costosL + '&auxCeco=' + auxCecoL + '&aux=' + auxL + '&fecha=' + fechaL + '&keyMapa=' + keyMapaL,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			var i = 0;
			if (json[i] == null) {
				alert('Algo paso');
			}
			while (json[i] != null) {
				if (json[i].idRuta == idRutaL) {
					if(i==0 && direccionL==-1){
						i=json.length-1;
						idRutaL=json[i].idRuta;
						idPreguntaL = json[i].idPregunta;
						idCheckUsuaL = json[i].idCheckUsua;
						fechaImagenL = json[i].fechaRespuesta;
						comparaDiferencia(json[i].idRuta, json[i].idPregunta, json[i].idCheckUsua, json[i].fechaRespuesta);
					}else{
						i=i+direccionL;
						if(json[i]!= null){
							idRutaL=json[i].idRuta;
							idPreguntaL = json[i].idPregunta;
							idCheckUsuaL = json[i].idCheckUsua;
							fechaImagenL = json[i].fechaRespuesta;
							comparaDiferencia(json[i].idRuta, json[i].idPregunta, json[i].idCheckUsua, json[i].fechaRespuesta);
						}else{
							i=0;
							idRutaL=json[i].idRuta;
							idPreguntaL = json[i].idPregunta;
							idCheckUsuaL = json[i].idCheckUsua;
							fechaImagenL = json[i].fechaRespuesta;
							comparaDiferencia(json[i].idRuta, json[i].idPregunta, json[i].idCheckUsua, json[i].fechaRespuesta);
						}
					}
				}
				i++;
			}
		},
		error : function() {
			alert('Algo paso');
		}
	});
}

function comparaDiferencia(idRuta, idPregunta, idCheckUsua, fechaImagen) {
	$
	.ajax({
		type : "GET",
		url : urlServer + '../ajaxCompara.json?idRuta='+ idRuta + '&idPregunta=' + idPregunta
				+ '&idCheckUsuario=' + idCheckUsua + '&fechaImagen='+ fechaImagen,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			if (json != null) {
				if (json.length < 2) {
					if(direccionL!=0){
						carrucelDiferenciaImagen(direccionL)
					}else{
						alert('NO EXISTE UNA FOTO ANTERIOR A ESTA ')
					}
				} else {
					$('#carouserDoble').hide();
					$('#tCom').show();
					$('#selOp3').show();
					$('#selOp33').show();
					document.getElementById('imaCompaIzq').src = ""	+ rutaHostImagenL + json[0].ruta;
					document.getElementById('imaCompaDer').src = ""	+ rutaHostImagenL + json[1].ruta;
					document.getElementById('desPreguntaDoble').innerHTML = json[1].desPregunta;
					document.getElementById('respuestaDoble').innerHTML = "Respuesta: "	+ json[1].respuesta;
					document.getElementById('nombreCecoDoble').innerHTML = json[1].nombreCeco+ " / " + json[1].nombreUsuario;
				}				
			}
		},
		error : function() {
			alert('Algo paso');
		}
	});
}
// href="javascript:location.reload()"
function oculta(tamObj) {
	for (var i = 1; i <= tamObjL; i++) {
		document.getElementById("div" + i).style.display = 'none'
	}
}

function muestraVista(j) {
	document.getElementById("div" + j).style.display = 'inline-block';
	noDivL=j;	
}


/*function cerrarClick() {
	alert('One is clicked');
	$('#popup2').hide();
	$('#tCom').hide();
	init(noDivL);
}*/


function amIclicked(e, element){
    e = e || event;
    var target = e.target || e.srcElement;
    if(target.id==element.id)
        return true;
    else
        return false;
}

function oneClick(event, element){
    if(amIclicked(event, element)){
        $('#popup2').hide();
		$('#tCom').hide();
		init(noDivL);
    }
}


function muestraVista2(keyMapa, idRuta) {
	keyMapaL = keyMapa;
	idRutaL = idRuta;
	carrucelImagen(categoriaL, tipoL, puestoL, usuarioL, costosL, auxCecoL, auxL, fechaL, keyMapaL, idRutaL);
	$('#popup2').show();
	$('#carouselSimple').show();
	$('#carouserDoble').hide();
	$('#tCom').hide();
	$('#selOp1').show();
	$('#selOp11').show();
}

function ejeSelOp1() {
	var divDatos = document.getElementsByClassName('item active')[0];
	idRutaL = divDatos.childNodes[0].alt;
	idPreguntaL = divDatos.childNodes[2].id;
	idCheckUsuaL = divDatos.childNodes[3].id;
	keyMapaL = divDatos.childNodes[4].id;
	fechaImagenL = divDatos.childNodes[5].id;
	dobleImagen(idRutaL, idPreguntaL, idCheckUsuaL, fechaImagenL);
}
function ejeSelOp2() {
	direccionL=0;
	carrucelImagen(categoriaL, tipoL, puestoL, usuarioL, costosL, auxCecoL, auxL, fechaL, keyMapaL, idRutaL);
	$('#carouselSimple').show();
	$('#carouserDoble').hide();
	$('#selOp1').show();
	$('#selOp11').show();
}

function ejeSelOp3() {
	comparaDiferencia(idRutaL, idPreguntaL, idCheckUsuaL, fechaImagenL);
}
function ejeSelOp4() {
	dobleImagen(idRutaL, idPreguntaL, idCheckUsuaL, fechaImagenL);
}