package com.gruposalinas.checklist.domain;

public class BitacoraDTO {
	
	private int idBitacora;
	private int idCheckUsua;
	private String longitud;
	private String latitud;
	private String fechaInicio;
	private String fechaFin;
	private int commit;
	private int modo;
	private int idUsuario;
	private String usuario;
	private String preCalificacion;
	private String calificacion;
	
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public int getIdCheckUsua() {
		return idCheckUsua;
	}
	public void setIdCheckUsua(int idCheckUsua) {
		this.idCheckUsua = idCheckUsua;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public int getCommit() {
		return commit;
	}
	public void setCommit(int commit) {
		this.commit = commit;
	}
	public int getModo() {
		return modo;
	}
	public void setModo(int modo) {
		this.modo = modo;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPreCalificacion() {
		return preCalificacion;
	}
	public void setPreCalificacion(String preCalificacion) {
		this.preCalificacion = preCalificacion;
	}
	public String getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}
	
}
