package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.Correo7SDAO;
import com.gruposalinas.checklist.domain.Coment7SDTO;


public class Correo7SBI {

	private static Logger logger = LogManager.getLogger(Correo7SBI.class);

	List<Coment7SDTO> listaComentarios = null;
	
	@Autowired
	Correo7SDAO correo7SDAO;
	
	
	public List<Coment7SDTO> buscaComents7S(int idBitacora) {
		
		try {
			listaComentarios = correo7SDAO.buscaComentarios(idBitacora);
		} catch (Exception e) {
			logger.info("No fue posible consultar"+e);
		}
				
		return listaComentarios;		
	}
	
	public boolean validaCheck7S(int idBitacora) {
		boolean res=false;
		try {
			res = correo7SDAO.validaCehcklist(idBitacora);
		} catch (Exception e) {
			logger.info("No fue posible consultar"+ e);
			return false;
		}
				
		return res;		
	}

	public double califCheck7S(int idBitacora) {
		double res=0.0;
		try {
			res = correo7SDAO.CalifCheck7S(idBitacora);
		} catch (Exception e) {
			logger.info("No fue posible consultar"+ e);
			return -1.0;
		}
				
		return res;		
	}
	
	public List<Coment7SDTO> buscaCheck7S(int idBitacora) {
		try {
			listaComentarios = correo7SDAO.ObtieneChecklit(idBitacora);
		} catch (Exception e) {
			logger.info("No fue posible consultar"+e);
		}
				
		return listaComentarios;		
	}
	
}
