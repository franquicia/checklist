package com.gruposalinas.checklist.domain;

public class PreguntaZonaDTO {
	private int idPregZona;
	private int idPregunta;
	private int idZona;
	private int idChecklist;
	private int idPregPadre;
	private String zona;
	private String tipoZona;
	private int idTipoZona;
	public int getIdPregZona() {
		return idPregZona;
	}
	public void setIdPregZona(int idPregZona) {
		this.idPregZona = idPregZona;
	}
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public int getIdZona() {
		return idZona;
	}
	public void setIdZona(int idZona) {
		this.idZona = idZona;
	}
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public int getIdPregPadre() {
		return idPregPadre;
	}
	public void setIdPregPadre(int idPregPadre) {
		this.idPregPadre = idPregPadre;
	}
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	public String getTipoZona() {
		return tipoZona;
	}
	public void setTipoZona(String tipoZona) {
		this.tipoZona = tipoZona;
	}
	public int getIdTipoZona() {
		return idTipoZona;
	}
	public void setIdTipoZona(int idTipoZona) {
		this.idTipoZona = idTipoZona;
	}
	@Override
	public String toString() {
		return "PreguntaZonaDTO [idPregZona=" + idPregZona + ", idPregunta=" + idPregunta + ", idZona=" + idZona
				+ ", idChecklist=" + idChecklist + ", idPregPadre=" + idPregPadre + ", zona=" + zona + ", tipoZona="
				+ tipoZona + ", idTipoZona=" + idTipoZona + "]";
	}
	
	
	
	
}
