package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.ConsultaSupervisionDTO;

public class ConsultaSupervisionRowMapper implements RowMapper<ConsultaSupervisionDTO> {

	public ConsultaSupervisionDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConsultaSupervisionDTO consultaDTO = new ConsultaSupervisionDTO();

		consultaDTO.setIdSucursal(Integer.parseInt(rs.getString("FCID_CECO")));
		consultaDTO.setNomSucursal(rs.getString("FCSUCURSAL"));
		consultaDTO.setFechaInicio(rs.getString("FECHAINI"));
		consultaDTO.setFechaFin(rs.getString("FECHAFIN"));
		consultaDTO.setIdUsuario(Integer.parseInt(rs.getString("FCID_USUARIO")));
		consultaDTO.setNomUsuario(rs.getString("FCNOMBRE"));
		consultaDTO.setIdProtocolo(Integer.parseInt(rs.getString("FIPROTOCOLO")));
		consultaDTO.setNomProtocolo(rs.getString("FCPROTOCOLO"));
		consultaDTO.setIdBitacora(Integer.parseInt(rs.getString("FIID_BITACORA")));
	
		return consultaDTO;
	}

}
