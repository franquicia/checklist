package com.gruposalinas.checklist.domain;


public class AsignaTransfDTO {
	


		private int ceco;
		private int version;
		private int usuario;
		private int usuario_asig;
		private int idestatus;
		private String obs;
		private String periodo;
		private int aux;
		private String aux2;
		private String cec;
		private int proyecto;
		private String nombFase;
		private String nombProy;
		private int fase;
		private String obsFase;
		private String obsProyecto;
		private int agrupador;
		private int faseAct;
		private String nomCeco;		
		private int faseActual;	
		private int checklist;	
		private int idTabla;	
		private int idOrdenFase;
		private String idOrden;
		private int edoCargaHallazgo;
		private int banderaDesbloqueoFase;
		private int banderaFolio;
		private int tipoProyecto;
		private String region;
		private String zona;
		private String territorio;
		private String negocio;
		private String eco;
		private int edoCargaInicial;
		private int statusMaximo;
		private String banderaUltFase;
		private int conteoHallazgos;
		private String nombreUsuario;
		private String nombreFase;
		private int idProgramacion;
		private String fechaProgramacion;
		
		
         
		public int getCeco() {
			return ceco;
		}
		public void setCeco(int ceco) {
			this.ceco = ceco;
		}
		public int getVersion() {
			return version;
		}
		public void setVersion(int version) {
			this.version = version;
		}
		public int getUsuario() {
			return usuario;
		}
		public void setUsuario(int usuario) {
			this.usuario = usuario;
		}
		public int getUsuario_asig() {
			return usuario_asig;
		}
		public void setUsuario_asig(int usuario_asig) {
			this.usuario_asig = usuario_asig;
		}
		public int getIdestatus() {
			return idestatus;
		}
		public void setIdestatus(int idestatus) {
			this.idestatus = idestatus;
		}
		public String getObs() {
			return obs;
		}
		public void setObs(String obs) {
			this.obs = obs;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		public int getAux() {
			return aux;
		}
		public void setAux(int aux) {
			this.aux = aux;
		}
		public String getAux2() {
			return aux2;
		}
		public void setAux2(String aux2) {
			this.aux2 = aux2;
		}
		
		
		public String getCec() {
			return cec;
		}
		public void setCec(String cec) {
			this.cec = cec;
		}
		
		public int getProyecto() {
			return proyecto;
		}
		public void setProyecto(int proyecto) {
			this.proyecto = proyecto;
		}
		public String getNombFase() {
			return nombFase;
		}
		public void setNombFase(String nombFase) {
			this.nombFase = nombFase;
		}
		public String getNombProy() {
			return nombProy;
		}
		public void setNombProy(String nombProy) {
			this.nombProy = nombProy;
		}
		public int getFase() {
			return fase;
		}
		public void setFase(int fase) {
			this.fase = fase;
		}
		public String getObsFase() {
			return obsFase;
		}
		public void setObsFase(String obsFase) {
			this.obsFase = obsFase;
		}
		public String getObsProyecto() {
			return obsProyecto;
		}
		public void setObsProyecto(String obsProyecto) {
			this.obsProyecto = obsProyecto;
		}
		
		public int getAgrupador() {
			return agrupador;
		}
		public void setAgrupador(int agrupador) {
			this.agrupador = agrupador;
		}
		
		public int getFaseAct() {
			return faseAct;
		}
		public void setFaseAct(int faseAct) {
			this.faseAct = faseAct;
		}
		
		public String getNomCeco() {
			return nomCeco;
		}
		public void setNomCeco(String nomCeco) {
			this.nomCeco = nomCeco;
		}
		public int getFaseActual() {
			return faseActual;
		}
		public void setFaseActual(int faseActual) {
			this.faseActual = faseActual;
		}
		
		public int getChecklist() {
			return checklist;
		}
		public void setChecklist(int checklist) {
			this.checklist = checklist;
		}
		
		public int getIdTabla() {
			return idTabla;
		}
		public void setIdTabla(int idTabla) {
			this.idTabla = idTabla;
		}
		
		public int getIdOrdenFase() {
			return idOrdenFase;
		}
		public void setIdOrdenFase(int idOrdenFase) {
			this.idOrdenFase = idOrdenFase;
		}
		
		public String getIdOrden() {
			return idOrden;
		}
		public void setIdOrden(String idOrden) {
			this.idOrden = idOrden;
		}
		
		public int getEdoCargaHallazgo() {
			return edoCargaHallazgo;
		}
		public void setEdoCargaHallazgo(int edoCargaHallazgo) {
			this.edoCargaHallazgo = edoCargaHallazgo;
		}
		public int getBanderaDesbloqueoFase() {
			return banderaDesbloqueoFase;
		}
		public void setBanderaDesbloqueoFase(int banderaDesbloqueoFase) {
			this.banderaDesbloqueoFase = banderaDesbloqueoFase;
		}
		public int getBanderaFolio() {
			return banderaFolio;
		}
		public void setBanderaFolio(int banderaFolio) {
			this.banderaFolio = banderaFolio;
		}
		
		public int getTipoProyecto() {
			return tipoProyecto;
		}
		public void setTipoProyecto(int tipoProyecto) {
			this.tipoProyecto = tipoProyecto;
		}
		
		public String getRegion() {
			return region;
		}
		public void setRegion(String region) {
			this.region = region;
		}
		public String getZona() {
			return zona;
		}
		public void setZona(String zona) {
			this.zona = zona;
		}
		public String getTerritorio() {
			return territorio;
		}
		public void setTerritorio(String territorio) {
			this.territorio = territorio;
		}
		public String getNegocio() {
			return negocio;
		}
		public void setNegocio(String negocio) {
			this.negocio = negocio;
		}
		
		public String getEco() {
			return eco;
		}
		public void setEco(String eco) {
			this.eco = eco;
		}
		
		public int getEdoCargaInicial() {
			return edoCargaInicial;
		}
		public void setEdoCargaInicial(int edoCargaInicial) {
			this.edoCargaInicial = edoCargaInicial;
		}
		
		public int getStatusMaximo() {
			return statusMaximo;
		}
		public void setStatusMaximo(int statusMaximo) {
			this.statusMaximo = statusMaximo;
		}
		
		public String getBanderaUltFase() {
			return banderaUltFase;
		}
		public void setBanderaUltFase(String banderaUltFase) {
			this.banderaUltFase = banderaUltFase;
		}
		
		public int getConteoHallazgos() {
			return conteoHallazgos;
		}
		public void setConteoHallazgos(int conteoHallazgos) {
			this.conteoHallazgos = conteoHallazgos;
		}
		
		public String getNombreUsuario() {
			return nombreUsuario;
		}
		public void setNombreUsuario(String nombreUsuario) {
			this.nombreUsuario = nombreUsuario;
		}
		public String getNombreFase() {
			return nombreFase;
		}
		public void setNombreFase(String nombreFase) {
			this.nombreFase = nombreFase;
		}
		
		public int getIdProgramacion() {
			return idProgramacion;
		}
		public void setIdProgramacion(int idProgramacion) {
			this.idProgramacion = idProgramacion;
		}
		public String getFechaProgramacion() {
			return fechaProgramacion;
		}
		public void setFechaProgramacion(String fechaProgramacion) {
			this.fechaProgramacion = fechaProgramacion;
		}
		@Override
		public String toString() {
			return "AsignaTransfDTO [ceco=" + ceco + ", version=" + version + ", usuario=" + usuario + ", usuario_asig="
					+ usuario_asig + ", idestatus=" + idestatus + ", obs=" + obs + ", periodo=" + periodo + ", aux="
					+ aux + ", aux2=" + aux2 + ", cec=" + cec + ", proyecto=" + proyecto + ", nombFase=" + nombFase
					+ ", nombProy=" + nombProy + ", fase=" + fase + ", obsFase=" + obsFase + ", obsProyecto="
					+ obsProyecto + ", agrupador=" + agrupador + ", faseAct=" + faseAct + ", nomCeco=" + nomCeco
					+ ", faseActual=" + faseActual + ", checklist=" + checklist + ", idTabla=" + idTabla
					+ ", idOrdenFase=" + idOrdenFase + ", idOrden=" + idOrden + ", edoCargaHallazgo=" + edoCargaHallazgo
					+ ", banderaDesbloqueoFase=" + banderaDesbloqueoFase + ", banderaFolio=" + banderaFolio
					+ ", tipoProyecto=" + tipoProyecto + ", region=" + region + ", zona=" + zona + ", territorio="
					+ territorio + ", negocio=" + negocio + ", eco=" + eco + ", edoCargaInicial=" + edoCargaInicial
					+ ", statusMaximo=" + statusMaximo + ", banderaUltFase=" + banderaUltFase + ", conteoHallazgos="
					+ conteoHallazgos + ", nombreUsuario=" + nombreUsuario + ", nombreFase=" + nombreFase
					+ ", idProgramacion=" + idProgramacion + ", fechaProgramacion=" + fechaProgramacion + "]";
		}
	
		
	}