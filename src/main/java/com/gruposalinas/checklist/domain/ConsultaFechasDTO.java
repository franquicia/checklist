package com.gruposalinas.checklist.domain;

public class ConsultaFechasDTO {
	private String fechaInicial;
	private String fechaFinal;
	private String fecha;
	private String anio;
	private String mes;
	private String trimestre;
	private String semana;
	private String numeroDiaSemana;
	private String numDia;
	private String diaFestivo;
	private String usuarioMod;
	private String fechaMod;
        private String bimestre;
        
	public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getTrimestre() {
		return trimestre;
	}
	public void setTrimestre(String trimestre) {
		this.trimestre = trimestre;
	}
	public String getSemana() {
		return semana;
	}
	public void setSemana(String semana) {
		this.semana = semana;
	}
	public String getNumeroDiaSemana() {
		return numeroDiaSemana;
	}
	public void setNumeroDiaSemana(String numeroDiaSemana) {
		this.numeroDiaSemana = numeroDiaSemana;
	}
	public String getNumDia() {
		return numDia;
	}
	public void setNumDia(String numDia) {
		this.numDia = numDia;
	}
	public String getDiaFestivo() {
		return diaFestivo;
	}
	public void setDiaFestivo(String diaFestivo) {
		this.diaFestivo = diaFestivo;
	}
	public String getUsuarioMod() {
		return usuarioMod;
	}
	public void setUsuarioMod(String usuarioMod) {
		this.usuarioMod = usuarioMod;
	}
	public String getFechaMod() {
		return fechaMod;
	}
	public void setFechaMod(String fechaMod) {
		this.fechaMod = fechaMod;
	}

    public String getBimestre() {
        return bimestre;
    }

    public void setBimestre(String bimestre) {
        this.bimestre = bimestre;
    }
        
        
        
	@Override
	public String toString() {
		return "ConsultaFechasDTO [fechaInicial=" + fechaInicial + ", fechaFinal=" + fechaFinal + ", fecha=" + fecha
				+ ", anio=" + anio + ", mes=" + mes + ", trimestre=" + trimestre + ", semana=" + semana
				+ ", numeroDiaSemana=" + numeroDiaSemana + ", numDia=" + numDia + ", diaFestivo=" + diaFestivo
				+ ", usuarioMod=" + usuarioMod + ", fechaMod=" + fechaMod + "]";
	}

	










}
	
	
	
	
	
	
	

	
	
	
	

	
	
	