package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.NivelDAO;
import com.gruposalinas.checklist.domain.NivelDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class NivelBI {

	private static Logger logger = LogManager.getLogger(NivelBI.class);

	@Autowired
	NivelDAO nivelDAO;
	
	List<NivelDTO> listaniveles = null;
	List<NivelDTO> listanivel = null;
	

	public List<NivelDTO> obtieneNivel(){
				
		try{
			listaniveles = nivelDAO.obtieneNivel();
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Nivel");
					
		}
				
		return listaniveles;
	}
	
	public List<NivelDTO> obtieneNivel(int idNivel){
				
		try{
			listanivel = nivelDAO.obtieneNivel(idNivel);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Nivel");
			
		}
				
		return listanivel;
	}	
	
	public int insertaNivel(NivelDTO nivel) {
		
		int idNivel = 0;
		
		try {		
			idNivel = nivelDAO.insertaNivel(nivel);			
		} catch (Exception e) {
			logger.info("No fue posible insertar el Nivel");
			
		}
		
		return idNivel;		
	}

	public boolean actualizaNivel (NivelDTO nivel){
		
		boolean respuesta = false;
				
		try {
			respuesta = nivelDAO.actualizaNivel(nivel);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Nivel");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaNivel(int idNivel){
		
		boolean respuesta = false;
		
		try{
			respuesta = nivelDAO.eliminaNivel(idNivel);
		}catch(Exception e){
			logger.info("No fue posible borrar el Nivel");
			
		}
		
		return respuesta;
	}
}
