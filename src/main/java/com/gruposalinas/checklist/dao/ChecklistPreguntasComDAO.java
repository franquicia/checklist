package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.PreguntaCheckDTO;
import com.gruposalinas.checklist.mappers.PreguntaCheckRowMapper;

public interface ChecklistPreguntasComDAO
 {

	// obtiene respuesta Generico
	public List<ChecklistPreguntasComDTO> obtieneDatosGen(String idBita) throws Exception;
	
	// obtiene respuesta NO
	public Map<String, Object> obtieneDatos(String idBita) throws Exception;

	// preguntas conteo
	public List<ChecklistPreguntasComDTO> obtieneInfo(String idBita) throws Exception;
	
	// preguntas conteogrupo
		public List<ChecklistPreguntasComDTO> obtieneInfoCont(String idBita) throws Exception;
		
		// preguntas conteogrupo Campos Extra
		public List<ChecklistPreguntasComDTO> obtieneInfoContExt(String idBita) throws Exception;
		
		// preguntas conteogrupo hijas
		public List<ChecklistPreguntasComDTO> obtieneInfoContHij(String idBita) throws Exception;	
		
	//PREGUNTAS CHECK
		public List<PreguntaCheckDTO> obtieneInfopreg(int idCheck) throws Exception;

	// PREGUNTAS NO, IMPERDONABLES
		public List<ChecklistPreguntasComDTO> obtieneInfoImp(int idBita) throws Exception;
		
	//PREGUNTAS NO, SIN PERDONABLES
		public List<ChecklistPreguntasComDTO> obtieneInfoNoImp(int idBita) throws Exception;
		
	//PREGUNTAS padre e hija
		public Map<String, Object> obtieneInfoPadHij(String idBita) throws Exception;
		
		// obtiene respuesta REPORTE SI-NO 
		public List<ChecklistPreguntasComDTO> obtieneDatosGenales(String idBita) throws Exception;
		
	// obtiene respuesta NO IMPERDONABLE CONCATENADAS
	public Map<String, Object> obtieneDatosImpconc(String idBita) throws Exception;
	
	// obtiene respuesta NO IMPERDONABLE CONCATENADAS solo una consulta
	public Map<String, Object> obtieneDatosImpconcPru(String idBita) throws Exception;
	
	
	// PREGUNTAS NO
	public List<ChecklistPreguntasComDTO> obtieneInfoHallaz(int ceco) throws Exception;
	
	// hallazgos tabla
	public List<ChecklistPreguntasComDTO> obtieneInfoHallazTab(int ceco) throws Exception;
		
	
	//OCC
	
	// preguntas conteo
	public List<ChecklistPreguntasComDTO> obtieneInfoOCC(String idBita) throws Exception;
	
	// preguntas conteo sumagrupo tot gral
		public List<ChecklistPreguntasComDTO> obtieneComplemOCC(String idBita) throws Exception;
		
	// preguntas conteo suma si,na
	public List<ChecklistPreguntasComDTO> obtieneComplemSiNaOCC(String idBita) throws Exception;
	
	// PREGUNTAS NO NVO
		public List<ChecklistPreguntasComDTO> obtieneInfoHallazNvo(int ceco) throws Exception;
		
		// hallazgos tabla NVO
		public List<ChecklistPreguntasComDTO> obtieneInfoHallazTabNvo(int ceco) throws Exception;
			
		// TRAE SOFT CECO FASE PROYECTO
		public List<ChecklistPreguntasComDTO> obtieneSoftCecoFaseProyecto(String ceco,String fase,String proyecto,String checklist) throws Exception;
		
		// TRAE SOFT CECO FASE PROYECTO
		public Map<String, Object> obtieneSoftCecoFaseProyectoCom(String ceco,String fase,String proyecto,String checklist) throws Exception;
		
		//preguntas no imp  ceco fase proyecto 
		public List<ChecklistPreguntasComDTO> obtienePregNoImpCecoFaseProyecto(String ceco,String fase,String proyecto) throws Exception;
	
}
