package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ConteoGenExpDTO;

public class ConteoGenExpRespRowMapper implements RowMapper<ConteoGenExpDTO> {

	public ConteoGenExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConteoGenExpDTO con = new ConteoGenExpDTO();
		
	
        
		con.setBitaGral(rs.getInt("FIID_BITGRAL"));
	    con.setBitacora(rs.getInt("FIID_BITACORA"));
	    con.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    con.setObs(rs.getString("FCOBSERVACIONES"));
	    con.setFcResp(rs.getString("FCRESPUESTA"));
	    con.setCritica(rs.getInt("FICRITICA"));
	    con.setPosible(rs.getString("POSIBLE"));
	    con.setIdCheck(rs.getInt("FIID_CHECKLIST"));
	    con.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    con.setPregunta(rs.getString("FCDESCRIPCION"));
	    con.setPonderacion(rs.getDouble("FIPODENRACION"));
	    con.setNomCheck(rs.getString("CHECKLIST"));
	    con.setClasifica(rs.getString("FCCLASIFICA"));
	  
	  
		return con;
	}

}


