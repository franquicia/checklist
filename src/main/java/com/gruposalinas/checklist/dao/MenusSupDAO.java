package com.gruposalinas.checklist.dao;

import java.util.List;
import com.gruposalinas.checklist.domain.MenusSupDTO;

public interface MenusSupDAO {
	
	public int insertMenu(int idMenu, String descMenu, int activo) throws Exception;
	public int updateMenu(int idMenu, String descMenu, String activo) throws Exception;
	public int deleteMenu(int idMenu) throws Exception;
	public List<MenusSupDTO> getMenus(String idMenu) throws Exception;
	
}
