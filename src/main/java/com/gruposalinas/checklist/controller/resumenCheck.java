package com.gruposalinas.checklist.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.gruposalinas.checklist.business.ChecklistBI;
import com.gruposalinas.checklist.business.RecursoPerfilBI;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.RecursoPerfilDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;

@Controller
public class resumenCheck {

	private static Logger logger = LogManager.getLogger(resumenCheck.class);
	
	@Autowired
	ChecklistBI checklistBI;
	
	@Autowired
	RecursoPerfilBI recursoPerfilBI;
	
	@SuppressWarnings("unused")
	@RequestMapping(value = "central/resumenCheck.htm", method=RequestMethod.POST)
	public ModelAndView resumen(
			@RequestParam(value = "idUserAdmin", required = true, defaultValue = "null") String idUserAdmin,
			HttpServletRequest request){
		
		ModelAndView mv = new ModelAndView("resumenChecklist");
		if(idUserAdmin.equals("adminCheckchecklists")){
			try {
				
				UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
				////System.out.println("USUARIO "+Integer.parseInt(userSession.getIdUsuario()));
				//Map<String, Object> listas = checklistBI.buscaResumenCheck(Integer.parseInt(userSession.getIdUsuario()));
				Map<String, Object> listas = checklistBI.buscaResumenCheck();
				@SuppressWarnings("rawtypes")
				java.util.ArrayList  activos = (ArrayList) listas.get("activos");
				@SuppressWarnings("rawtypes")
				java.util.ArrayList pendientes = (ArrayList) listas.get("pendientes");
				
				listas.get("activos");
				mv.addObject("listaResumen", activos);
				mv.addObject("listaPendientes", pendientes);
				//RECORRER LAS LISTAS Y EN LA DE ACTIVOS BUSCAR LAS OCURRENCIAS CON ESTADO 26 Y 27, CAMBIAR SU ESTDO EN LA LISTA ACTIVOS Y PINTARLO EN EL FRONT
				@SuppressWarnings("unchecked")
				Iterator<ChecklistDTO> it = activos.iterator();
				while (it.hasNext()){
					ChecklistDTO o = it.next();
					@SuppressWarnings("unchecked")
					Iterator<ChecklistDTO> it2 = pendientes.iterator();
						while (it2.hasNext()){
							ChecklistDTO o2 = it2.next();
							if (o.getIdChecklist() == o2.getIdChecklist())
								if (o2.getIdEstado() == 26 || o2.getIdEstado() == 28){
									o.setIdEstado(o2.getIdEstado());
								}
						}
				}
				
				mv.addObject("urlServer2", FRQConstantes.getURLServer());
				mv.addObject("show", "null");
			} catch (Exception e) {
				e.printStackTrace();
				logger.info("Algo ocurrió... al traer Resumen " + e);
			}	
		}else{
			mv=null;
		}
		return mv;
	}
	
	
	@RequestMapping(value = "/ajaxResumenEstado", method = RequestMethod.GET)
	public @ResponseBody boolean getResumenEstado(
			@RequestParam(value = "varEstado", required = true, defaultValue = "0")int varEstado,
			@RequestParam(value = "varActivo", required = true, defaultValue = "0")int varActivo,HttpServletRequest request) throws Exception {
		////System.out.println("Entro a cambiar el Estado " + varEstado + "valor activo " + varActivo);
		return  checklistBI.actualizaCheckVigente(varEstado,varActivo);
	}
	
	
	
	@RequestMapping(value = "/ajaxAdminCheck", method = RequestMethod.GET)
	public @ResponseBody RecursoPerfilDTO getAdministrador(
			@RequestParam(value = "varAdmin", required = true, defaultValue = "") String varAdmin,
			@RequestParam(value = "opcAdmin", required = true, defaultValue = "") String opcAdmin, HttpServletRequest request) {
		
		return recursoPerfilBI.obtienePermisos(Integer.parseInt(opcAdmin), Integer.parseInt(varAdmin));
	}
	
}
