<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
<head>
	<title>Banco Azteca | SistematizaciÃ³n de actividades</title>
	<style type="text/css"> 
		
	
	</style>
	<link rel="stylesheet" href="../css/vistaCumplimientoVisitas/estilo.css"/>
	<script type="text/javascript">
		var negocioIdL = ${negocioId};	
		var nivelPerfilL = ${nivelPerfil};
		var idCecoL = ${idCeco};
		var nombreCecoL = '${nombreCeco}';
	</script>
	<meta name="viewport"
		content="width=device-width, initial-scale=1, maximum-scale=1" />
	<!-- <link rel="stylesheet" type="text/css" href="../css/tooltipster.bundle.css" />  -->
	<link rel="stylesheet" media="screen" href="../css/vistaCumplimientoVisitas/modal.css" />
</head>
<body onload="nobackbutton();">

	<div class="header">
		<div class="logo">
			<c:if test="${!opcionReporte}">
				<a href="/checklist/"><img src="../images/vistaCumplimientoVisitas/logo.svg"></a>
			</c:if>
			<c:if test="${opcionReporte}">
				<a><img src="../images/vistaCumplimientoVisitas/logo.svg"></a>
			</c:if>		
		</div>
		<div class="title">
			<div class="box tleft">
				<c:if test="${!opcionReporte}">
					<a href="javascript:window.history.back();" class="hom">
						<img src="../images/vistaCumplimientoVisitas/regresar.svg" class="imgBack">					
					</a> 
					<a href="../central/vistaCumplimientoVisitas.htm" class="active">
				 	<img src="../images/vistaCumplimientoVisitas/rev01W.svg">
				 </a> 
				</c:if>		
				<c:if test="${opcionReporte}">
					<a href="../central/vistaCumplimientoVisitasMovil.htm" class="active">
				 	<img src="../images/vistaCumplimientoVisitas/rev01W.svg">
				 </a>  
				</c:if>			
				 
				<!--<a href="#"
				class=" "><img src="../images/vistaCumplimientoVisitas/rev02W.svg"></a> <a href="#"
				class=" "><img src="../images/vistaCumplimientoVisitas/rev03W.svg"></a> <a href="#"
				class=" "><img src="../images/vistaCumplimientoVisitas/rev04W.svg"></a> <a href="#"
				class=" "><img src="../images/vistaCumplimientoVisitas/rev05W.svg"></a> -->
			</div>
		</div>
	</div>
	
		<div class="wrapper">
			
			<div class="hspacer"></div>
			<div class="goshadow">
				<h2>Cumplimiento de visitas</h2>
			</div>
			
			
			<form id="formFiltro" method="POST" action="vistaFiltroCumplimientoVisitas.htm">
				<div class="box">
					
					<h3>Periodo</h3>
					<div class="inner">
						<div class="doble ical">
							<select name="seleccionAno" id="seleccionAno" onChange="mes();">
								<option value="2016">2016</option>
								<option value="2017">2017</option>
								<option value="2018">2018</option>
								<option value="2019">2019</option>
								<option value="2020">2020</option>
							</select>
						</div>
						<div class="doble ical">
							<select name="seleccionMes" id="seleccionMes">
								<option value="1">Enero</option>
								<option value="2">Febrero</option>
								<option value="3">Marzo</option>
								<option value="4">Abril</option>
								<option value="5">Mayo</option>
								<option value="6">Junio</option>
								<option value="7">Julio</option>
								<option value="8">Agosto</option>
								<option value="9">Septiembre</option>
								<option value="10">Octubre</option>
								<option value="11">Noviembre</option>
								<option value="12">Diciembre</option>
							</select>
						</div>
					</div>
					<h3>Canal</h3>
					<div class="inner">
						<div class="doble icanal">
							
							
							
							<select name="seleccionCanal" id="seleccionCanal">
								<option value="1">SERVICIOS FINANCIEROS</option>
								<option value="2">CREDITO Y COBRANZA</option>
								<option value="3">CDT</option>
								<option value="4">MAZ</option>
								<option value="5">PRESTA PRENDA</option>
							</select>
							
							<!--
							<select name="seleccionCanal" id="seleccionCanal" onChange="canal(this.value);">
								<c:if test="${fn:length(negocio) < 1}">
										<option value="0">Canal</option>
								</c:if>
								<c:forEach var="item" items="${negocio}">
									<option value="${item.idNegocio}">${item.descripcion}</option>
								</c:forEach>
							</select>						
							-->
							
							
						</div>
					</div>
					<h3>Localizaci&oacute;n</h3>
					<div class="inner">
						<div class="doble ipais">
							<select name="seleccionPais" id="seleccionPais">
								<c:if test="${fn:length(pais) < 1}">
										<option value="0">Pa&iacute;s</option>
								</c:if>
								<c:forEach var="itemPais" items="${pais}">
									<option value="${itemPais.idPais}">${itemPais.nombre}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					  
					<c:choose>
					<c:when test="${(paso=='1')}">
		  					
							<div role="tooltip" class="tooltip top custom-tooltip">
								 <div class="tooltip-arrow"></div> 
								<div class="tooltip-inner"><b>No se ha encontrado información para el negocio y país seleccionado</b></div>
							</div>
						
					</c:when>
					</c:choose>
					
					<div class="botones">
							<a class="btn" onclick="enviaFiltro(${nivelPerfil});">Buscar</a>
					</div>
					
					
				</div>
			</form>
			
			<!-- Formularios para validacion segun el perfil -->
			<form id="formTerri" method="POST" action="vistaTerritCumplimientoVisitas.htm">
			</form>
			<!-- Formularios para validacion segun el perfil -->
			
			
			<div class="fspacer"></div>
		</div>
	

	<!-- jquery Start -->
	<script type="text/javascript" src="../js/vistaCumplimientoVisitas/jquery.js"></script>

	<!-- progressbar -->
	<script>
		function move() {
			var elem = document.getElementById("myBar");
			var width = 1;
			var id = setInterval(frame, 10);
			function frame() {
				if (width >= 100) {
					clearInterval(id);
				} else {
					width++;
					elem.style.width = width + '%';
				}
			}
		}
	</script>

	<!-- dropdown -->
	<script type="text/javascript" src="../js/vistaCumplimientoVisitas/menu.js"></script>

	<!-- dropdown -->
	<script type="text/javascript" src="../js/vistaCumplimientoVisitas/script-vistaCumplimientoVisitas.js"></script>

	<div class="modal"></div>
</body>
</html>