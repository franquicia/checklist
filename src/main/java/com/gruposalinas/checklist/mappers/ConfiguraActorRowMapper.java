package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ConfiguraActorDTO;

public class ConfiguraActorRowMapper implements RowMapper<ConfiguraActorDTO> {

	public ConfiguraActorDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConfiguraActorDTO ConfiguraActorDTO = new ConfiguraActorDTO();
		
		ConfiguraActorDTO.setIdHallazgoConf(rs.getInt("FIID_HALLACONF"));
	    ConfiguraActorDTO.setIdConfigActor(rs.getInt("FIID_CONFIGACT"));
	    ConfiguraActorDTO.setStatusActuacion(rs.getInt("FISTATUSACTU"));
	    ConfiguraActorDTO.setAccion(rs.getString("FCACCION"));
	    ConfiguraActorDTO.setStatusAccion(rs.getInt("FISTATUSACCION"));
	    ConfiguraActorDTO.setBanderaAtencion(rs.getInt("FIIDBANDATEND"));
	    ConfiguraActorDTO.setObs(rs.getString("FCOBSERVACION"));
	    ConfiguraActorDTO.setAux(rs.getString("FCAUX"));
	    ConfiguraActorDTO.setPeriodo(rs.getString("FCPERIODO"));
	   
		return ConfiguraActorDTO;
	}

}


