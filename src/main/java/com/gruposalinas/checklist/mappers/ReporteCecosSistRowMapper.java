package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteSistemasDTO;

public class ReporteCecosSistRowMapper implements RowMapper<ReporteSistemasDTO> {

	public ReporteSistemasDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteSistemasDTO reporteCecosSistemas = new ReporteSistemasDTO();
		
		reporteCecosSistemas.setIdCeco(rs.getInt("FCID_CECO"));
		reporteCecosSistemas.setNombreCeco(rs.getString("FCNOMBRE"));
		reporteCecosSistemas.setPorcentaje(rs.getInt("CONTEO"));
		reporteCecosSistemas.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		
		return reporteCecosSistemas;
	}
}

