package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.VisitasxMesDTO;

public class VisitasxMesRowMapper implements RowMapper<VisitasxMesDTO> {

	@Override
	public VisitasxMesDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		VisitasxMesDTO visitasxMesDTO = new VisitasxMesDTO();
		
		visitasxMesDTO.setVisitas(rs.getInt("VISITAS"));
		visitasxMesDTO.setUsuario(rs.getInt("FIID_USUARIO"));
		visitasxMesDTO.setCeco(rs.getString("FCID_CECO"));
		visitasxMesDTO.setNombreCeco(rs.getString("FCNOMBRE"));
		visitasxMesDTO.setMes(rs.getString("MES"));
		
		return visitasxMesDTO;
	}

}
