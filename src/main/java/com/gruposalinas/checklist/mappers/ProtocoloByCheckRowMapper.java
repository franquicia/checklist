package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.ProtocoloByCheckDTO;

public class ProtocoloByCheckRowMapper implements RowMapper<ProtocoloByCheckDTO> {

	public ProtocoloByCheckDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ProtocoloByCheckDTO protoDTO = new ProtocoloByCheckDTO();
		
		protoDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		protoDTO.setNomChecklist(rs.getString("FCNOMBRE_CHECKLIST"));
		protoDTO.setIdProtocolo(rs.getInt("FIID_PROTOCOLO"));
		protoDTO.setNomProtocolo(rs.getString("FCNOMBRE_PROTOCOLO"));
	
		return protoDTO;
	}

}
