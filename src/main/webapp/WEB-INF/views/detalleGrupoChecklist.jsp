<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css"
	href="../css/expancion/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="../css/expancion/detalleExpancion.css">
<script src="../js/expancion/jquery-3.3.1.js"></script>
<script src="../js/expancion/jquery.dataTables.js"></script>
<script src="../js/expancion/tabla.js"></script>
<style type="text/css">
</style>

<title>DETALLES HALLAZGOS</title>
</head>
<body style="max-width: 1500;   margin: auto;">
	<div class="marginTotal">
		<div class="left">
			<div class="centrar">
				<img alt="elektra" src="../images/expancion/LogoElektra.svg"
					class="imagenBanner"> <img alt="baz"
					src="../images/expancion/LogoBAZ.svg" class="imagenBanner">
			</div>
		</div>
		<div class="main">
			<h1 style="color: #6B696E;" class="fuenteBold">
				Entrega de Sucursales / <strong class="fuenteBold">DETALLES
					CHECKLIST</strong>
			</h1>
			<br />

		</div>
		<div class="right">
			<div class="centrar">
				<img alt="elektra" src="../images/expancion/LogoElektra.svg"
					class="imagenBanner"> <img alt="baz"
					src="../images/expancion/LogoBAZ.svg" class="imagenBanner">
			</div>
		</div>

	</div>


	<div class="marginTotal">
		<div style="width: 60%;" class="centrar">
			<div>
				<h2 class="fuenteBold"> ${titulo}</h2>
		
				<c:if test="${imagenDetalle != '' }">
					<img alt="imagendetalle"
						src="../images/expancion/${imagenDetalle}.svg"
						class="imagenBanner">
				</c:if>
			</div>
			<div
				style="background:white; padding-bottom: 2%; padding-top: 2%; margin-top: 2%; margin-bottom: 2%; color:gray">
				<h2 class="fuenteBold"> Calificación: ${calificacion}</h2>
			</div>
		</div>

	</div>


	<div class="marginTotal">
		<div style="width: 80%; margin: auto;">
			<table id="tabla" class="tablaChecklist">
				<thead>
					<tr>
						<th>Item</th>
						<th>Respuesta</th>
						<!-- <th>SLA Días</th> -->
					</tr>
				</thead>
				<tbody>
					<c:if test="${listaPreguntas != null }">
						<c:set var="count" value="0" scope="page" />
						<c:set var="countHija" value="0" scope="page" />
						<c:set var="indice" value="" scope="page" />
						<c:set var="pregDetalle" value="" scope="page" />
						<c:set var="pregPadre" value="" scope="page" />
						
						
						
						<c:forEach var="valor" items="${listaPreguntas}">
							<tr>
								<c:choose>
								
								  <c:when test="${valor.pregPadre == 0}">
								  	<c:set var="count" value="${count + 1}" scope="page" />
								  	<c:set var="countHija" value="0" scope="page" />
								  	<c:set var="indice" value="${count}" scope="page" />
								  	<c:set var="pregPadre" value="${valor.pregunta}" scope="page" />
								  	<c:set var="pregDetalle" value="${valor.pregunta}" scope="page" />
								  </c:when>
								  
								  <c:when test="${valor.pregPadre != 0}">
								    <c:set var="countHija" value="${countHija + 1}" scope="page" />
								    <c:set var="indice" value="${count}.${countHija}" scope="page" />
								    <c:set var="pregDetalle" value="${pregPadre} ${valor.pregunta}" scope="page" />
								  </c:when>
								  
								</c:choose>
								
								<td>${indice}.-${pregDetalle}</td>
								<td style="background-color:#BBBBBB; text-align:center; color:gray;"><bold>${valor.posible}</bold></td>
								<!-- <td>${valor.sla}</td>  -->
							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
		</div>

			<div> <a style="color:gray;" href="menuActa.htm?idBitacora=${idBitacora}"> Regresar </a></div>
		
	</div>



</body>
</html>