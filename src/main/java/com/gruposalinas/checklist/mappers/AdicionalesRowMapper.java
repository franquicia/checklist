package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.AdicionalesDTO;

public class AdicionalesRowMapper implements RowMapper<AdicionalesDTO> {

	public AdicionalesDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AdicionalesDTO ad = new AdicionalesDTO();
		
		ad.setBitGral(rs.getInt("FIID_BITGRAL"));
		ad.setRecorrido(rs.getInt("FCRECORRIDO"));
		ad.setCeco(rs.getString("FCID_CECO"));
		ad.setIdRespuesta(rs.getInt("FIID_RESPUESTA"));
		ad.setIdPreg(rs.getInt("FIID_PREGUNTA"));
		ad.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		ad.setPosible(rs.getString("FCPOSIBLE"));
		ad.setRuta(rs.getString("FCRUTA"));
		ad.setObs(rs.getString("FCOBSERVA"));
		ad.setPregunta(rs.getString("FCPREGUNTA"));
		ad.setCritica(rs.getInt("FCRITICA"));
		ad.setClasifica(rs.getString("FCCLASIFICA"));
		ad.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
		ad.setStatus(rs.getInt("FIID_STATUS"));
		ad.setUsuModif(rs.getString("FCUSUMODIF"));
		ad.setUsu(rs.getString("FCUSU"));
	    ad.setFechaAuto(rs.getString("FDAUTORIZO"));
	    ad.setFechaFin(rs.getString("FDVALIDO"));
	    ad.setFechaIni(rs.getString("FCPERIODO"));
	    ad.setAux(rs.getString("FCAUX"));
	    ad.setAux2(rs.getInt("FCAUX2"));
	    ad.setNomCeco(rs.getString("NOMCECO"));
	    ad.setEco(rs.getInt("ECO"));
	    ad.setLiderObra(rs.getString("LIDEROBRA"));
	    ad.setUsuReco(rs.getString("USU"));
	    ad.setNomCheck(rs.getString("NOMCHECK"));
	    ad.setClasifica(rs.getString("CLASIFICA"));


		return ad;
	}

}


