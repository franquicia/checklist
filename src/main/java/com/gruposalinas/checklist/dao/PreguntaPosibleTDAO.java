package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.PreguntaPosibleTDTO;


public interface PreguntaPosibleTDAO {

	public List<PreguntaPosibleTDTO> obtienePreguntasPosibleTemp(String idPregunta) throws Exception;
	
	public boolean insertaPreguntaPosibleTemp(PreguntaPosibleTDTO bean) throws Exception;
	
	public boolean eliminaPreguntaPosibleTemp(int idPregPos, int idPregunta) throws Exception;
	
}
