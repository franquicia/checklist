package com.gruposalinas.checklist.domain;

import java.util.List;

public class PreguntaCheckDTO {
	
	
	private int idPregunta;
	private int idModulo;
	private int idTipo;
	private int estatus;
	private String desc;
	private String deta;
	private String critica;
	private String sla;
	private String tipocheck;
	private String nombChec; 
	private int version;
	private String ordenG;
	private String pond;
	private String clasific;
        private String responsable;
        private String negocio;
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public int getIdModulo() {
		return idModulo;
	}
	public void setIdModulo(int idModulo) {
		this.idModulo = idModulo;
	}
	public int getIdTipo() {
		return idTipo;
	}
	public void setIdTipo(int idTipo) {
		this.idTipo = idTipo;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getDeta() {
		return deta;
	}
	public void setDeta(String deta) {
		this.deta = deta;
	}
	public String getCritica() {
		return critica;
	}
	public void setCritica(String critica) {
		this.critica = critica;
	}
	public String getSla() {
		return sla;
	}
	public void setSla(String sla) {
		this.sla = sla;
	}
	public String getTipocheck() {
		return tipocheck;
	}
	public void setTipocheck(String tipocheck) {
		this.tipocheck = tipocheck;
	}
	public String getNombChec() {
		return nombChec;
	}
	public void setNombChec(String nombChec) {
		this.nombChec = nombChec;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getOrdenG() {
		return ordenG;
	}
	public void setOrdenG(String ordenG) {
		this.ordenG = ordenG;
	}
	public String getPond() {
		return pond;
	}
	public void setPond(String pond) {
		this.pond = pond;
	}
	public String getClasific() {
		return clasific;
	}
	public void setClasific(String clasific) {
		this.clasific = clasific;
	}

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    @Override
    public String toString() {
        return "PreguntaCheckDTO{" + "idPregunta=" + idPregunta + ", idModulo=" + idModulo + ", idTipo=" + idTipo + ", estatus=" + estatus + ", desc=" + desc + ", deta=" + deta + ", critica=" + critica + ", sla=" + sla + ", tipocheck=" + tipocheck + ", nombChec=" + nombChec + ", version=" + version + ", ordenG=" + ordenG + ", pond=" + pond + ", clasific=" + clasific + ", responsable=" + responsable + ", negocio=" + negocio + '}';
    }
	
}
