package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.PosPregDTO;

public class PosPregRowMapper implements RowMapper<PosPregDTO> {
	
	public PosPregDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		PosPregDTO posPregDTO = new PosPregDTO();
		
		posPregDTO.setIdposPreg(rs.getInt("FIIDPOSIPREG"));
		posPregDTO.setIdPosible(rs.getInt("FIIDPOSIBLE"));
		posPregDTO.setTipoPreg(rs.getInt("FIID_TIPO_PREG"));
		
		return posPregDTO;
	}
}
