package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.AdmInformProtocolosDTO;


public class AdmInformProtocolosRowMapper implements RowMapper<AdmInformProtocolosDTO> {

	public AdmInformProtocolosDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AdmInformProtocolosDTO admInfProtoDTO = new AdmInformProtocolosDTO();
		
		admInfProtoDTO.setIdsec(rs.getString("FIID_SEC"));
		admInfProtoDTO.setCeco(rs.getString("FCID_CECO"));
		admInfProtoDTO.setUsuario(rs.getString("FIID_USUARIO"));
		admInfProtoDTO.setFecha(rs.getString("FDFECHA"));
		admInfProtoDTO.setRuta(rs.getString("FCRUTAINFORME"));
		admInfProtoDTO.setLider(rs.getString("FCLIDER_7S"));
		admInfProtoDTO.setFirmaLider(rs.getString("FCFIRMA_LIDER"));
		admInfProtoDTO.setNumLider(rs.getString("FINUMLIDER"));
		admInfProtoDTO.setFirmaFrq(rs.getString("FCFIRMAFRQ"));
		admInfProtoDTO.setUsuarioMod(rs.getString("FCUSUARIO_MOD"));
		admInfProtoDTO.setFechaMod(rs.getString("FDFECHA_MOD"));
		admInfProtoDTO.setRetroAlimentacion(rs.getString("FRCTRETRO"));
		admInfProtoDTO.setIdProtocolo(rs.getString("FCID_PROTOCOLO"));
		return admInfProtoDTO;
	}
}
