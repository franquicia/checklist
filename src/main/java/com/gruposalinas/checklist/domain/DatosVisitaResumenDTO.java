package com.gruposalinas.checklist.domain;

public class DatosVisitaResumenDTO {

	private String idCeco;
	private String nombreTienda;
	private int idChecklist;
	private String nombreChecklist;
	private String fechaInicio;
	private String fechaFin;
	private String fechaEnvio;
	private String modo;
	
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getNombreTienda() {
		return nombreTienda;
	}
	public void setNombreTienda(String nombreTienda) {
		this.nombreTienda = nombreTienda;
	}
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public String getNombreChecklist() {
		return nombreChecklist;
	}
	public void setNombreChecklist(String nombreChecklist) {
		this.nombreChecklist = nombreChecklist;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getFechaEnvio() {
		return fechaEnvio;
	}
	public void setFechaEnvio(String fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}
	public String getModo() {
		return modo;
	}
	public void setModo(String modo) {
		this.modo = modo;
	}
	
}
