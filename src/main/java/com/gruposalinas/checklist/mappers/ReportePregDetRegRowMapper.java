package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.CompromisoDTO;

public class ReportePregDetRegRowMapper implements RowMapper<CompromisoDTO> {

	public CompromisoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		CompromisoDTO compromisoDTO = new CompromisoDTO();
		
		compromisoDTO.setOrden(rs.getInt("FIORDEN_CHECK"));
		compromisoDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		compromisoDTO.setDesPregunta(rs.getString("PREGUNTA"));
		compromisoDTO.setDescripcion(rs.getString("RESPUESTA"));
		compromisoDTO.setFolio(rs.getString("FOLIO"));
		compromisoDTO.setAccion(rs.getString("ACCION"));
		compromisoDTO.setEvidencia(rs.getString("EVIDENCIA"));
		
		return compromisoDTO;
	}

}
