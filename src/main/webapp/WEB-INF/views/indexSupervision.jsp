<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
    
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>Inicio</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervision/estilos.css">
</head>

<body ng-app="app" ng-controller="validarForm" onpaste="return false">

  <div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="${pageContext.request.contextPath}/img/supervision/btn_hamburguer.svg" alt="Men� principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="inicio.htm"><img src="${pageContext.request.contextPath}/img/supervision/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.htm" class="selMenu">
          <div>Salir</div>
          <div><img src="${pageContext.request.contextPath}/img/supervision/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="indexSupervision.htm">Reporte de Supervisores</a> 
		</div>
    	B�squeda de Sucursal
    </div>

	<!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervision/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Men� Principal<br>
          </div>
        </div>
        <%-- <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la p�gina">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> --%>
        <div id="menu">
        	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuAseguramiento.jsp" flush="true">
				<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuAseguramiento.jsp" />
			</tiles:insertTemplate>
          <!-- <ul class="l-navegacion nivel1">
          		<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Asegurador</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>B�squeda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>B�squeda Asegurador</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galer�a de Evidencias</div></a>
				</li>
			</ul> -->
        </div>
      </div>
    </div>
    
    

    <!-- Contenido -->
    <div class="contSecc">
    
    <div class="liga">
		<a href="inicio.htm" class="liga">
		<img style="height: 15px; width: 15px;" src="${pageContext.request.contextPath}/img/supervision/arrowmleft.png">
		<span>P�GINA PRINCIPAL</span>
		</a>
		</div>
    
		<div class="titSec">Selecciona los criterios para realizar una b�squeda de Sucursal o Protocolo de una Sucursal</div>
		<div class="gris">
		
		
		
			<div class="divCol4">
				<div class="col4">
					Territorio:<br>
					<c:url value="/central/indexSupervisionZona.htm" var="selectZona" />
					<form:form method="POST" action="${selectZona}" model="command" name="formTerr" id="formTerr">
						<select required id="Territorios" onchange="javascript:getTerritorioSelected()">
							<c:forEach var="lista" items="${lista}">
									<option value="${lista.idCeco}">
										<c:out value="${lista.nombreCeco}"></c:out>
									</option>
							</c:forEach>
							<input id="terri" name="terri" type="hidden" value="${terri}" />
							<input id="cal1" name="cal1" type="hidden" value="${cal1}"/>
							<input id="cal2" name="cal2" type="hidden" value="${cal2}"/>
							<input id="pro" name="pro" type="hidden" value="${protocolo}" />
						</select>
					</form:form>
				</div>
				
				<div class="col4">
					Zona:<br>
					
					<c:url value="/central/indexSupervisionRegion.htm" var="selectRegional" />
					<form:form method="POST" action="${selectRegional}" model="command" name="formZona" id="formZona">					
						<select required id="Zonas" onchange="javascript:getZonaSelected()">
						
							<c:forEach var="listaZonas" items="${listaZonas}">
								<option value="${listaZonas.idCeco}">
									<c:out value="${listaZonas.nombreCeco}"></c:out>
								</option>
							</c:forEach>
							<input id="zona" name="zona" type="hidden" value="${zona}" />
							<input id="idTerri" name="idTerri" type="hidden" value="${terri}" />
							<input id="calen1" name="calen1" type="hidden" value="${cal1}"/>
							<input id="calen2" name="calen2" type="hidden" value="${cal2}"/>
							<input id="proto" name="proto" type="hidden" value="${protocolo}" />
							
						</select>					
					</form:form>
					
					
					
				</div>
				<div class="col4">
					Regi�n:<br>
					
					<c:url value="/central/indexSupervisionSucursal.htm" var="selectSucursal" />
					<form:form method="POST" action="${selectSucursal}" model="command" name="formRegion" id="formRegion">						
					<select required id="Region" onchange="javascript:getRegionSelected()">
						  <c:forEach var="listaRegiones" items="${listaRegiones}">
								<option value="${listaRegiones.idCeco}">
									<c:out value="${listaRegiones.nombreCeco}"></c:out>
								</option>
							</c:forEach>
							<input id="region" name="region" type="hidden" value="${region}" />
							<input id="idZona" name="idZona" type="hidden" value="${zona}" />
							<input id="idTerritorio" name="idTerritorio" type="hidden" value="${terri}" />
							<input id="calendario1" name="calendario1" type="hidden" value="${cal1}"/>
							<input id="calendario2" name="calendario2" type="hidden" value="${cal2}"/>
							<input id="protoco" name="protoco" type="hidden" value="${protocolo}" />
							
					</select>
					</form:form>
				</div>
				<div class="col4">
					Sucursal:<br>
					
					<c:url value="/central/indexSupervisionProtocolo.htm" var="selectProtocolo" />
					<form:form method="POST" action="${selectProtocolo}" model="command" name="formSucursal" id="formSucursal">					
						<select required id="Sucursal" onchange="javascript:getSucursalSelected()">
							  <c:forEach var="listaSucursales" items="${listaSucursales}">
									<option value="${listaSucursales.idCeco}">
										<c:out value="${listaSucursales.nombreCeco}"></c:out>
									</option>
								</c:forEach>
								<input id="sucursal" name="sucursal" type="hidden" value="${sucursal}" />							
								<input id="idRegiones" name="idRegiones" type="hidden" value="${region}" />
								<input id="idZonas" name="idZonas" type="hidden" value="${zona}" />
								<input id="idTerritorios" name="idTerritorios" type="hidden" value="${terri}" />
								<input id="calIni" name="calIni" type="hidden" value="${cal1}"/>
								<input id="calFin" name="calFin" type="hidden" value="${cal2}"/>
								<input id="protocolo" name="protocolo" type="hidden" value="${protocolo}" />
								
						</select>
					</form:form>
				</div>					
			</div>
			<div class="divCol4 flexJusCen">
				<div class="col4 col4B">
					Fecha de inicio:<br>
					<input required type="text" maxlength="10" placeholder="DD/MM/AAAA" value="${cal1}" class="datapicker1 date" id="datepicker" autocomplete="off">
				</div>
				<div class="col4 w30A"><div><strong>a</strong></div></div>
				<div class="col4 col4B">
					Fecha de fin:<br>
					<input required type="text" maxlength="10" placeholder="DD/MM/AAAA" value="${cal2}" class="datapicker1 date" id="datepicker1" autocomplete="off">
				</div>
				<div class="col4">
					Protocolo:<br>
					
					<c:url value="/central/indexSupervisionSeleccionados.htm" var="datosSeleccionados" />
					<form:form method="POST" action="${datosSeleccionados}" model="command" name="formProtocolo" id="formProtocolo">	
						<select id="Protocolo" onchange="javascript:getProtocoloSelected()">
							  <c:forEach var="listaProtocolos" items="${listaProtocolos}">
									<option value="${listaProtocolos.idChecklist}">
										<c:out value="${listaProtocolos.nomChecklist}"></c:out>
									</option>
								</c:forEach>
								<input id="ProtocoloSel" name="ProtocoloSel" type="hidden" value="${protocolo}" />
								<input id="SucursalSel" name="SucursalSel" type="hidden" value="${sucursal}" />							
								<input id="RegionSel" name="RegionSel" type="hidden" value="${region}" />
								<input id="ZonaSel" name="ZonaSel" type="hidden" value="${zona}" />
								<input id="TerritorioSel" name="TerritorioSel" type="hidden" value="${terri}" />
								<input id="calendarioIni" name="calendarioIni" type="hidden" value="${cal1}"/>
								<input id="calendarioFin" name="calendarioFin" type="hidden" value="${cal2}"/>
								
						</select>
					</form:form>
				</div>
			</div>
			
			<c:url value="/central/indexSupervision.htm" var="archivoPasivo" />
			<form:form method="POST" action="${archivoPasivo}" model="command" name="form" id="form">
				<div id="btnBuscar" class="btnCenter">
					<a href="#" class="btn btnBuscarS" onclick="return validaFechas();">Buscar</a>
				</div>
					<input id="fechaInicio" name="fechaInicio" type="hidden" value="${cal1}"/>
					<input id="fechaFin" name="fechaFin" type="hidden" value="${cal2}"/>
					<input id="idChecklist" name="idChecklist" type="hidden" value="${protocolo}" />
					<input id="idCeco" name="idCeco" type="hidden" value="${sucursal}" />							
					<input id="idRegion" name="RegionSel" type="hidden" value="${region}" />
					<input id="idZona" name="ZonaSel" type="hidden" value="${zona}" />
					<input id="idTerritorio" name="TerritorioSel" type="hidden" value="${terri}" /> 
			
			</form:form>
			
		</div>
		
		
		<c:choose>
						<c:when test="${paso == 1}"> <!-- Contiene todos los filtros -->
						
						<div class="divHideS current">
							
							
							<br><br>
		
		
    						<input id="buscar" maxlength="50" type="text" class="form-control" placeholder="B�squeda" />
    						
    						
							<div class="titSec">Protocolos que coinciden con tu b�squeda</div>
							<div class="gris">
								<div class="scroll">
									<table id="resultados" class="tblGeneral tblSucursal">
										<tbody>
										
										<tr>
											<th>Nombre de Sucursal</th>
											<th>Fecha de Visita</th>
											<th>Supervisor</th> 
										</tr>
											<c:forEach var="listaTabla" items="${listaTabla}">
												<tr>
												
												<c:url value="/central/detalleProtocoloSupervision.htm" var="envioDetalle" />
												<form:form method="POST" action="${envioDetalle}" model="command" name="formDetalleProtocolo" id="formDetalleProtocolo">
													<td class="liga">
														<a href="#" onclick="javascript:enviarDatos(${listaTabla.idBitacora});">${listaTabla.nomSucursal}</a>
													</td>
													<td>${listaTabla.fechaFin}</td>
													<td>${listaTabla.nomUsuario}</td>
													<input id="idBitacora" name="idBitacora" type="hidden" value=""/>
													<input id="sucursalEnv" name="sucursalEnv" type="hidden" value=""/>
													<input id="zonaEnv" name="zonaEnv" type="hidden" value=""/>													
													<input id="regionEnv" name="regionEnv" type="hidden" value=""/>
													<input id="terrEnv" name="terrEnv" type="hidden" value=""/>
													<input id="proEnv" name="proEnv" type="hidden" value="${listaTabla.nomProtocolo}"/>
													<input id="idProEnv" name="idProEnv" type="hidden" value=""/>
												</form:form>
												
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
						</c:when>
						<c:when test="${paso == 2}"> <!-- Protocolo no seleccionado -->
						
							<div class="divHideSV current">
							
							
							<br><br>
		
		
    						<input id="buscar" maxlength="50" type="text" class="form-control" placeholder="B�squeda" />
							
								<div class="titSec">Coincidencias de tu b�squeda</div>
								<div class="gris">
									<div class="scroll">
										<table id="resultados" class="tblGeneral tblSucursalV">
											<tbody>
												<tr>
												<th>Nombre de Sucursal</th>
												<th>Protocolo</th>
												<th>Fecha de Inicio</th>
												<th>Fecha de Fin</th>
												<th>Supervisor</th>
											</tr>							
											<c:forEach var="listaTabla" items="${listaTabla}">
											<tr>
											
												<c:url value="/central/detalleProtocoloSupervision.htm" var="envioDetalle" />
												<form:form method="POST" action="${envioDetalle}" model="command" name="formDetalleProtocolo" id="formDetalleProtocolo">
													<td class="liga">
														<a href="#" onclick="javascript:enviarDatos(${listaTabla.idBitacora});">${listaTabla.nomSucursal}</a>
													</td>
													<td>${listaTabla.nomProtocolo}</td>
													<td>${listaTabla.fechaInicio}</td>
													<td>${listaTabla.fechaFin}</td>
													<td>${listaTabla.nomUsuario}</td>
													<input id="idBitacora" name="idBitacora" type="hidden" value=""/>
													<input id="sucursalEnv" name="sucursalEnv" type="hidden" value=""/>
													<input id="zonaEnv" name="zonaEnv" type="hidden" value=""/>													
													<input id="regionEnv" name="regionEnv" type="hidden" value=""/>
													<input id="terrEnv" name="terrEnv" type="hidden" value=""/>
													<input id="proEnv" name="proEnv" type="hidden" value="${listaTabla.nomProtocolo}"/>
													<input id="idProEnv" name="idProEnv" type="hidden" value=""/>
												</form:form>
												
											</tr>
											</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</c:when>	
						
						<c:when test="${paso==3}">
							<div class="divHideSV current">				
								<div class="titSec"><span ><b> NO SE ENCONTRARON DETALLES PARA CONSULTA.</b></span>
									<br><br>
									<div><b>No se encontraron resultados para esta consulta.</b></div>					
								</div>
							</div>
						</c:when>
				</c:choose>
				
				<%-- <input id="fechaInicio" name="fechaInicio" type="hidden" value=""/>
				<input id="fechaFin" name="fechaFin" type="hidden" value=""/>
				<input id="ProtocoloSel" name="ProtocoloSel" type="hidden" value="${ProtocoloSel}" />
				<input id="SucursalSel" name="SucursalSel" type="hidden" value="${SucursalSel}" />							
				<input id="RegionSel" name="RegionSel" type="hidden" value="${RegionSel}" />
				<input id="ZonaSel" name="ZonaSel" type="hidden" value="${ZonaSel}" />
				<input id="TerritorioSel" name="TerritorioSel" type="hidden" value="${TerritorioSel}" /> --%>
	
	</div>	<!-- Fin conSecc -->
	
	<!-- Footer -->
	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualizaci�n Febrero 2019</div>
	</div>
</div><!-- Fin page -->


</body>
</html>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/custom-file-input.js"></script><!--Unpload -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/funciones.js"></script>


<script>

document.querySelector("#buscar").onkeyup = function(){
    $TableFilter("#resultados", this.value);
}

$TableFilter = function(id, value){
    var rows = document.querySelectorAll(id + ' tbody tr');
    for(var i = 0; i < rows.length; i++){
        var showRow = false;
        var row = rows[i];
        row.style.display = 'none';        

        for(var x = 0; x < row.childElementCount; x++){
            if(row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1){
                showRow = true;
                break;
            }
        }        

        if(showRow){
            row.style.display = null;
        }
    }
}


</script>


<script type="text/javascript">


function enviarDatos(id){
	document.getElementById("idBitacora").value = id;
	document.getElementById("sucursalEnv").value = document.getElementById("idCeco").value;
	document.getElementById("zonaEnv").value = document.getElementById("idZona").value;
	document.getElementById("regionEnv").value = document.getElementById("idRegion").value;
	document.getElementById("terrEnv").value = document.getElementById("idTerritorio").value;
	document.getElementById("idProEnv").value = document.getElementById("idChecklist").value;
	form = document.getElementById("formDetalleProtocolo");
	/*alert("ID: " + document.getElementById('idBitacora').value+", "+document.getElementById("sucursalEnv").value
			+", "+document.getElementById("zonaEnv").value+", "+document.getElementById("regionEnv").value
			+", "+document.getElementById("terrEnv").value);*/
	form.submit();
}

function validaFechas(){	
	var f1=document.getElementById("datepicker").value;
	var f2=document.getElementById("datepicker1").value;

	var filtro=document.getElementById("Sucursal").value;
	//alert("Filtro: "+filtro);
 	var Fecha1 = stringToDate(f1,"dd/MM/yyyy","/");
 	var Fech2 = stringToDate(f2,"dd/MM/yyyy","/");
 	var FHoy = new Date();
	
    var flag1=false;
 	var flag2=false;



	if(filtro == "0"){
		alert("Debes seleccionar los filtros");
	} else {

	if (isNaN(Fecha1)){
		alert("Debes colocar fecha inicio");
		return false;
	}
	else{
		if(Fecha1<FHoy){
		flag1=true;
		}else{
			alert("La fecha inicio no puede ser mayor que hoy");
			return false;
		}
	}
	
	if (isNaN(Fech2)){
		alert("Debes colocar fecha fin");
		return false;
	}
	else{
		if(Fech2<FHoy){
			flag2=true;
			}else{
				alert("La fecha fin no puede ser mayor que hoy");
				return false;
			}
	} 

	if(flag1==true && flag2==true){
		if(Fecha1>Fech2){
			alert("La fecha de inicio no debe ser mayor que la de fin");
			return false;
		}else{

			document.getElementById('fechaInicio').value = f1;
			document.getElementById('fechaFin').value = f2;
			document.getElementById("RegionSel").value = document.getElementById("idRegiones").value;
			document.getElementById("ZonaSel").value = document.getElementById("idZonas").value;
			document.getElementById("TerritorioSel").value = document.getElementById("idTerritorios").value;
			form = document.getElementById("form");
			form.submit();
		}
	  } 	
	}
	return false;	
}

function stringToDate(_date,_format,_delimiter)
{
            var formatLowerCase=_format.toLowerCase();
            var formatItems=formatLowerCase.split(_delimiter);
            var dateItems=_date.split(_delimiter);
            var monthIndex=formatItems.indexOf("mm");
            var dayIndex=formatItems.indexOf("dd");
            var yearIndex=formatItems.indexOf("yyyy");
            var month=parseInt(dateItems[monthIndex]);
            month-=1;
            var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
            return formatedDate;
}

function getTerritorioSelected() {
	var select = document.getElementById("Territorios");
    value = select.value;

    if (value==0){
		alert("Debe seleccionar alg�n territorio.");
    } else {
	form = document.getElementById("formTerr");
	document.getElementById("terri").value = value;
	document.getElementById('cal1').value = document.getElementById('datepicker').value;
	document.getElementById('cal2').value = document.getElementById('datepicker1').value;
	document.getElementById('pro').value = document.getElementById('Protocolo').value;
	//alert("Territorio: "+document.getElementById("terri").value);
	form.submit();
	}
}

function getZonaSelected() {
	var select = document.getElementById("Zonas");
    value = select.value;

    if (value==0){
		alert("Debe seleccionar alg�n territorio para continuar.");
    } else {
	form = document.getElementById("formZona");
	document.getElementById("zona").value = value;
	document.getElementById("idTerri").value = document.getElementById("terri").value;
	document.getElementById('calen1').value = document.getElementById('datepicker').value;
	document.getElementById('calen2').value = document.getElementById('datepicker1').value;
	document.getElementById('proto').value = document.getElementById('Protocolo').value;
	
	//alert("Zona: "+document.getElementById("idTerri").value+", "+document.getElementById("zona").value);
	form.submit();
    }
}

function getRegionSelected() {
	var select = document.getElementById("Region");
    value = select.value;

    if (value==0){
		alert("Debe seleccionar alg�n territorio/zona para continuar.");
    } else {
	form = document.getElementById("formRegion");
	document.getElementById("region").value = value;
	document.getElementById("idZona").value = document.getElementById("zona").value;
	document.getElementById("idTerritorio").value = document.getElementById("idTerri").value;
	document.getElementById('calendario1').value = document.getElementById('datepicker').value;
	document.getElementById('calendario2').value = document.getElementById('datepicker1').value;
	document.getElementById('protoco').value = document.getElementById('Protocolo').value;
	
	/*alert("Region: "+document.getElementById("idTerri").value+", "+document.getElementById("zona").value+", "
			+document.getElementById("region").value);*/
	form.submit();
    }
}

function getSucursalSelected() {
	var select = document.getElementById("Sucursal");
    value = select.value;

    if (value==0){
		alert("Debe seleccionar alg�n territorio/zona/regi�n para continuar.");
    } else {
	form = document.getElementById("formSucursal");
	document.getElementById("sucursal").value = value;
	document.getElementById("idZonas").value = document.getElementById("idZona").value;
	document.getElementById("idTerritorios").value = document.getElementById("idTerritorio").value;
	document.getElementById("idRegiones").value = document.getElementById("region").value;
	document.getElementById('calIni').value = document.getElementById('datepicker').value;
	document.getElementById('calFin').value = document.getElementById('datepicker1').value;
	document.getElementById('protocolo').value = document.getElementById('Protocolo').value;
	
	/*alert("Sucursal: "+document.getElementById("idTerritorio").value+", "+document.getElementById("idZona").value+", "
			+document.getElementById("region").value+", "+document.getElementById("sucursal").value);*/
	form.submit();
    }
}  

function getProtocoloSelected() {
	
	var select = document.getElementById("Protocolo");
    value = select.value;
    
	form = document.getElementById("formProtocolo");
	document.getElementById("ProtocoloSel").value = value;
	document.getElementById("SucursalSel").value = document.getElementById("sucursal").value;
	document.getElementById("RegionSel").value = document.getElementById("idRegiones").value;
	document.getElementById("ZonaSel").value = document.getElementById("idZonas").value;
	document.getElementById("TerritorioSel").value = document.getElementById("idTerritorios").value;
	document.getElementById('calendarioIni').value = document.getElementById('datepicker').value;
	document.getElementById('calendarioFin').value = document.getElementById('datepicker1').value;
	
	/*alert("Protocolo: "+document.getElementById("idTerritorios").value+", "+document.getElementById("idZonas").value+", "
			+document.getElementById("idRegiones").value+", "+document.getElementById("sucursal").value+", "
			+document.getElementById("ProtocoloSel").value);*/
	form.submit();
}

</script>

