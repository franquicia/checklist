package com.gruposalinas.checklist.business;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.checklist.domain.ChecklistHallazgosRepoDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.domain.EmpContacExtDTO;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;
import com.gruposalinas.checklist.domain.ParametroDTO;
import com.gruposalinas.checklist.domain.ReporteChecksExpDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.servicios.servidor.CorreoOutlook;
import com.gruposalinas.checklist.servicios.servidor.CorreosLotus;
import com.gruposalinas.checklist.util.UtilDate;
import com.gruposalinas.checklist.util.UtilMail;
import com.gruposalinas.checklist.util.UtilObject;
import com.gruposalinas.checklist.util.UtilResource;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.CenterTextMode;
import org.jfree.chart.plot.RingPlot;
import org.jfree.data.general.DefaultPieDataset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

public class CorreoBI {

    @Autowired
    RespuestaPdfBI respuestaPdfBI;

    @Autowired
    EmpContacExtBI empContacExtBI;

    @Autowired
    HallazgosRepoBI hallazgosRepoBI;

    @Autowired
    SucursalChecklistBI sucursalChecklistBI;

    @Autowired
    ParametroBI parametroBi;

    @Autowired
    ParametroBI parametrobi;

    private static final Logger logger = LogManager.getLogger(CorreoBI.class);

    private String titulo = "Alta de Checklist";
    private String NombreContacto = "Administracion Checklist";
    private String extContacto = "72911";
    private String celContacto = "1234567890";
    // private String mailContactoCesar = "cvidal@bancoazteca.com.mx";
    private String mailContacto = "cvidal@bancoazteca.com.mx";
    @SuppressWarnings("unused")
    private String mailPersonal = "alejandro.morales@elektra.com.mx";
    @SuppressWarnings("unused")
    private String mailContactoAdmin = "sistemas@checklist.com.mx";

    private List<String> copiados = null;

    public boolean sendMailAutorizacion(String idEmpleado, UsuarioDTO user, String nombreCheck) {

        copiados = new ArrayList<String>();
        Resource correoColab = null;
        CorreosLotus correo = null;

        try {
            // CORREO AL AUTORIZADOR
            // if (user.getIdUsuario().equals("99999999"))
            // user.setIdUsuario("664899");

            correoColab = UtilResource.getResourceFromInternet("mailSolicitud.html");

            try {
                logger.info("new CorreosLotus()");
                correo = new CorreosLotus();
                this.mailContacto = correo.validaUsuarioLotusCorreos("664899");
                logger.info("RETORNO VALIDA_USUARIO_LOTUS_CORREO" + this.mailContacto);
            } catch (Exception e) {
                logger.info(e.getMessage());
            }

            String strCorreo = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreo = strCorreo.replace("@idEmpleado", idEmpleado);
            strCorreo = strCorreo.replace("@NombreSocio", user.getNombre());
            strCorreo = strCorreo.replace("@Titulo",
                    "Tu solicitud de alta del checklist:" + nombreCheck + ", ha sido enviada para su autorización");
            strCorreo = strCorreo.replaceAll("null", "Sin Dato");
            // EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO VALIDA_USUARIO_LOTUS_CORREO
            UtilMail.enviaCorreo("cvidal@bancoazteca.com.mx", copiados, "checklist - Autorizacion: " + titulo,
                    strCorreo, null);

        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
        }

        correoColab = UtilResource.getResourceFromInternet("mailNotificacion.html");
        try {

            try {
                correo = new CorreosLotus();
                this.mailContacto = correo.validaUsuarioLotusCorreos("664899");
                logger.info("RETORNO VALIDA_USUARIO_LOTUS_CORREO" + this.mailContacto);
            } catch (Exception e) {
                logger.info(e.getMessage());
            }

            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoOwner = strCorreoOwner.replace("@NombreSocio", user.getNombre());
            strCorreoOwner = strCorreoOwner.replace("@Titulo",
                    "Tu solicitud de alta del checklist: " + nombreCheck + ", ha sido enviada para su autorización");
            strCorreoOwner = strCorreoOwner.replace("@NombreContacto", NombreContacto);
            strCorreoOwner = strCorreoOwner.replace("@extension", extContacto);
            strCorreoOwner = strCorreoOwner.replace("@telefono", celContacto);
            strCorreoOwner = strCorreoOwner.replace("@email", "");
            strCorreoOwner = strCorreoOwner.replaceAll("null", "Sin Dato");

            // EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO VALIDA_USUARIO_LOTUS_CORREO
            UtilMail.enviaCorreo("cvidal@bancoazteca.com.mx", copiados, "checklist - Solicitud: " + titulo,
                    strCorreoOwner, null);

        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
        }

        return true;
    }

    public boolean sendMailAprobacion(boolean autori, UsuarioDTO user, String nombreCheck) {
        copiados = new ArrayList<String>();
        String titulo = "";
        if (autori) {
            titulo = "Autorizada";
        } else {
            titulo = "Rechazada";
        }

        // if (user.getIdUsuario().equals("99999999"))
        // user.setIdUsuario("664899");
        CorreosLotus correo = null;

        try {
            Resource correoColab = UtilResource.getResourceFromInternet("mailAutorizacion.html");

            try {
                logger.info("new CorreosLotus()");
                correo = new CorreosLotus();
                this.mailContacto = correo.validaUsuarioLotusCorreos("664899");
                logger.info("RETORNO VALIDA_USUARIO_LOTUS_CORREO" + this.mailContacto);
            } catch (Exception e) {
                logger.info(e.getMessage());
            }

            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoOwner = strCorreoOwner.replace("@NombreSocio", user.getNombre());
            strCorreoOwner = strCorreoOwner.replace("@Titulo",
                    "Tu solicitud de Modificación del checklist:" + nombreCheck + ", ha sido " + titulo);
            strCorreoOwner = strCorreoOwner.replaceAll("null", "Sin Dato");
            // EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO VALIDA_USUARIO_LOTUS_CORREO
            UtilMail.enviaCorreo("cvidal@bancoazteca.com.mx", copiados, "Franquicia - Solicitud: " + titulo,
                    strCorreoOwner, null);

        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
        }
        return true;

    }

    public static boolean mergeCecos(String destinatario, String asunto, String cuerpoCorreo) {
        try {
            destinatario = "sistemas-franquicias@elektra.com.mx";
            logger.info("DESTINATARIO - " + destinatario);
        } catch (Exception e) {
            logger.info("Algo ocurrió al enviar correo " + e);
            destinatario = "sistemas-franquicias@elektra.com.mx";
        }
        UtilMail.enviaCorreoMergeCecos(destinatario, asunto, cuerpoCorreo);
        return true;
    }

    public boolean sendMailAcuerdos(UsuarioDTO user, List<CompromisoDTO> listaCompromiso)
            throws DocumentException, IOException {
        // copiados.add(user.getNombre());
        // logger.info("copiados- " + copiados);
        copiados = new ArrayList<String>();
        String titulo = "";
        titulo = "Acuerdos Checklist Triunfo  -  Sucursal: " + user.getNombre();
        String html = "";
        String fechaCierre = "";
        String lon = "150";
        int lonPreg = 0;

        for (int i = 0; i < listaCompromiso.size(); i++) {
            fechaCierre = listaCompromiso.get(0).getFechaCompromiso();
            lonPreg = listaCompromiso.get(i).getDesPregunta().length()
                    + listaCompromiso.get(i).getDescripcion().length();

            if (lonPreg > 500) {
                lon = "350";
            }

            html += "<tr>" + "<td style='background-color:white; width:5%; height:" + lon
                    + "px; border: #F0F0F0 25px solid; border-radius:20px;'>" + "<div style='width:100%; height:" + lon
                    + "px'>" + "<table style='width:100%; align:center;'>" + "<tr>"
                    + "<td style='border:white 1px solid; width:1300px;'>" + (i + 1) + ".- "
                    + listaCompromiso.get(i).getDesPregunta() + "</td>"
                    + "<td style='color:red; border: white 1px solid; width:75px;'></td>"// Respuesta NO
                    + "</tr>" + "<tr>" + " <td colspan='10' style='border: white 5px solid;'>ACUERDO: "
                    + listaCompromiso.get(i).getDescripcion() + "</td>" + "</tr>" + "</table>" + "</div>" + "</td>"
                    + "</tr>";
        }

        html = creaDocumento(html, false, null, fechaCierre);
        try {
            Resource correoColab = UtilResource.getResourceFromInternet("mailAcuerdos.html");

            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoOwner = strCorreoOwner.replace("@Titulo", "");
            strCorreoOwner = strCorreoOwner.replaceAll("null", "Sin Dato");
            strCorreoOwner = strCorreoOwner.replace("@Lista", html);

            CorreosLotus correosLotus = new CorreosLotus();
            String destinatario = "";
            String correoSucursal = "";

            List<String> docHtml = new ArrayList<String>();
            docHtml.add(html);

            logger.info("_______________________________ANTES DE ESCRIBIR PDF_______________________________");
            File adjunto = escribirPDFHtml(docHtml);
            logger.info("_______________________________DESPUES DE ESCRIBIR PDF_______________________________");

            try {
                destinatario = correosLotus.validaUsuarioLotusCorreos(user.getIdUsuario());
                correoSucursal = correosLotus.validaUsuarioLotusCorreos(user.getNombre());
                copiados.add(correoSucursal);
                copiados.add("amorales@elektra.com.mx");
                // copiados.add("jsepulveda@elektra.com.mx");
                logger.info("correo Sucursal - " + correoSucursal);
            } catch (Exception e) {
                logger.info("Algo ocurrió al consultar el correoLotus " + e);
                destinatario = "amorales@elektra.com.mx";
                correoSucursal = "sepulveda@elektra.com.mx";
            }

            // EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO VALIDA_USUARIO_LOTUS_CORREO
            UtilMail.enviaCorreo(destinatario, copiados, "" + titulo, strCorreoOwner, adjunto);

        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
        }
        return true;

    }

    // Va a recibir un list con el size de los documentos que se tienen que crear
    public File escribirPDFHtml(List<String> html) {
        File temp = null;
        try {
            // create a temp file
            temp = File.createTempFile("compromisos", ".pdf");
            FileOutputStream file = new FileOutputStream(temp);
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, file);
            document.open();

            java.util.Iterator<String> it = html.iterator();

            while (it.hasNext()) {
                String k = it.next();
                XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
                ByteArrayInputStream is = new ByteArrayInputStream(k.getBytes());
                worker.parseXHtml(writer, document, is);
                document.newPage();
            }
            document.close();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }

    public File escribirPDFHtmlExpansion(List<String> html) {
        File temp = null;
        try {
            // create a temp file
            temp = File.createTempFile("Acta_Entrega", ".pdf");
            FileOutputStream file = new FileOutputStream(temp);
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, file);
            document.open();

            java.util.Iterator<String> it = html.iterator();

            while (it.hasNext()) {
                String k = it.next();
                XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
                ByteArrayInputStream is = new ByteArrayInputStream(k.getBytes());
                worker.parseXHtml(writer, document, is);
                document.newPage();
            }
            document.close();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }

    public File escribirPDFHtmlTransformacion(List<String> html) {
        File temp = null;
        try {
            // create a temp file
            temp = File.createTempFile("Acta_Transformacion", ".pdf");
            FileOutputStream file = new FileOutputStream(temp);
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, file);

            PdfPageEventHelper pdfEvent = new PdfPageEventHelper();

            writer.setPageEvent(new PdfPageEventHelper() {

                Font font;
                PdfTemplate t;
                Image total;

                @Override
                public void onOpenDocument(PdfWriter writer, Document document) {
                    t = writer.getDirectContent().createTemplate(30, 16);
                    try {
                        total = Image.getInstance(t);
                        total.setRole(PdfName.ARTIFACT);
                        //font =  new Font(BaseFont.createFont(FONT, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 10);
                    } catch (DocumentException de) {
                        throw new ExceptionConverter(de);
                    }
                }

                @Override
                public void onEndPage(PdfWriter writer, Document document) {
                    PdfPTable table = new PdfPTable(3);
                    try {
                        table.setWidths(new int[]{24, 24, 2});
                        table.setTotalWidth(500);
                        table.getDefaultCell().setFixedHeight(20);
                        table.getDefaultCell().setBorder(Rectangle.BOTTOM);
                        table.addCell(new Phrase(""));
                        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table.addCell(new Phrase(String.format("Página %d de", writer.getPageNumber())));
                        PdfPCell cell = new PdfPCell(total);
                        cell.setBorder(Rectangle.BOTTOM);
                        table.addCell(cell);
                        PdfContentByte canvas = writer.getDirectContent();
                        canvas.beginMarkedContentSequence(PdfName.ARTIFACT);
                        table.writeSelectedRows(0, -1, 36, 30, canvas);
                        canvas.endMarkedContentSequence();
                    } catch (DocumentException de) {
                        throw new ExceptionConverter(de);
                    }
                }

                @Override
                public void onCloseDocument(PdfWriter writer, Document document) {
                    ColumnText.showTextAligned(t, Element.ALIGN_LEFT,
                            new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                            2, 2, 0);
                }
            });

            document.open();

            java.util.Iterator<String> it = html.iterator();

            while (it.hasNext()) {
                String k = it.next();
                XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
                ByteArrayInputStream is = new ByteArrayInputStream(k.getBytes());
                worker.parseXHtml(writer, document, is);
                document.newPage();
            }
            document.close();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }

    public boolean sendMailBuzon(UsuarioDTO user, String asunto, String idCeco, String puesto, String lugar,
            String telefono, String comentario, String copiadosParam) {

        copiados = new ArrayList<String>();
        String fechaPendientes = UtilDate.getSysDate("dd/MM/yyyy");
        String titulo = "";
        titulo = user.getNombre() + " - " + asunto;
        String arr[] = null;

        if (copiadosParam != "") {
            arr = copiadosParam.split(",");
            if (arr.length > 0) {
                for (int i = 0; i < arr.length; i++) {
                    copiados.add(arr[i]);
                }
            }
        }
        logger.info("copiados... " + copiados);

        try {
            Resource correoColab = UtilResource.getResourceFromInternet("mailBuzon.html");

            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoOwner = strCorreoOwner.replace("@Titulo", "Buzón dudas o comentarios");
            strCorreoOwner = strCorreoOwner.replaceAll("null", "Sin Dato");
            strCorreoOwner = strCorreoOwner.replaceAll("@Fecha", fechaPendientes);
            strCorreoOwner = strCorreoOwner.replaceAll("@Nombre", user.getNombre());
            strCorreoOwner = strCorreoOwner.replaceAll("@IdCeco", idCeco);
            strCorreoOwner = strCorreoOwner.replaceAll("@Puesto", puesto);
            strCorreoOwner = strCorreoOwner.replaceAll("@NoEmpleado", user.getIdUsuario());
            strCorreoOwner = strCorreoOwner.replaceAll("@Zona", lugar);
            strCorreoOwner = strCorreoOwner.replaceAll("@Telefono", telefono);
            strCorreoOwner = strCorreoOwner.replace("@Comentario", comentario);

            CorreosLotus correosLotus = new CorreosLotus();
            String destinatario = "";

            try {
                destinatario = correosLotus.validaUsuarioLotusCorreos(user.getIdUsuario());
                logger.info("correo destinatario - " + destinatario);
            } catch (Exception e) {
                logger.info("Algo ocurrió al consultar el correoLotus " + e);
                destinatario = "alejandro.morales@elektra.com.mx";
            }

            // EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO VALIDA_USUARIO_LOTUS_CORREO
            UtilMail.enviaCorreo(destinatario, copiados, "" + titulo, strCorreoOwner, null);

        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
        }
        return true;
    }

    public boolean sendMailCompromisosChecklist(UsuarioDTO user, String ceco) throws DocumentException, IOException {
        logger.info("SI ENTRO!!");
        // copiados.add(user.getNombre());
        // logger.info("copiados- " + copiados);
        copiados = new ArrayList<String>();
        String titulo = "";
        titulo = "Acuerdos Checklist Triunfo  -  Sucursal: " + user.getNombre();
        String html = "";

        logger.info("DENTRO DE SENDMAILCOMPROMISOS " + "USER ---> " + user.getIdUsuario() + " ceco: " + ceco);

        JsonArray res = respuestaPdfBI.obtieneRespuestas(Integer.parseInt(user.getIdUsuario()), ceco);

        logger.info("size () " + res.size());
        // String n+"1" = "";
        List<String> docHtml = new ArrayList<String>();
        List<String> generales = null;

        for (int i = 0; i < res.size(); i++) {
            logger.info("LO QUE TRAE MI RESPUESTA" + res.get(i).toString());
            if (!res.get(i).toString().equals("{}")) {
                String data = llenaDocumento(res.get(i));
                generales = new ArrayList<String>();
                generales.add(res.get(i).getAsJsonObject().get("nombrecheck").toString());
                generales.add(res.get(i).getAsJsonObject().get("nombreCeco").toString());
                generales.add(res.get(i).getAsJsonObject().get("fechaInicio").toString());
                generales.add(res.get(i).getAsJsonObject().get("fechaFin").toString());
                generales.add(res.get(i).getAsJsonObject().get("gerente").toString());
                generales.add(res.get(i).getAsJsonObject().get("regional").toString());
                String htmlCheck = creaDocumento(data, true, generales, "");
                html += htmlCheck;
                docHtml.add(htmlCheck);
                // System.out.println("DOCUMENTO " + i + "CREADO");
                // System.out.println(htmlCheck);
                // System.out.println("********************************************************");
            }
        }

        try {
            Resource correoColab = UtilResource.getResourceFromInternet("mailAcuerdos.html");

            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoOwner = strCorreoOwner.replace("@Titulo", "");
            strCorreoOwner = strCorreoOwner.replaceAll("null", "Sin Dato");
            strCorreoOwner = strCorreoOwner.replace("@Lista", html);

            CorreosLotus correosLotus = new CorreosLotus();
            String destinatario = "";
            String correoSucursal = "";

            logger.info("_______________________________ANTES DE ESCRIBIR PDF_______________________________");
            // Ahora se le enviara un list con el size de los checklist a escribir
            File adjunto = escribirPDFHtml(docHtml);
            logger.info("_______________________________DESPUES DE ESCRIBIR PDF_______________________________");

            try {
                destinatario = correosLotus.validaUsuarioLotusCorreos(user.getIdUsuario());
                correoSucursal = correosLotus.validaUsuarioLotusCorreos(user.getNombre());
                logger.info("DESTINATARIO ----- >>>>" + destinatario);
                logger.info("CORREO SUCURSAL ----- >>>>" + correoSucursal);
                logger.info("CECO QUE ESTOY TRAYENDO ----- >>>>" + ceco);

                copiados.add(correoSucursal);
                copiados.add("amorales@elektra.com.mx");
                // copiados.add("cvidal@bancoazteca.com.mx");
                // copiados.add("jsepulveda@elektra.com.mx");
                logger.info("correo Sucursal - " + correoSucursal);
            } catch (Exception e) {
                logger.info("Algo ocurrió al consultar el correoLotus " + e);
                destinatario = "amorales@elektra.com.mx";
                correoSucursal = "alejandro.morales@elektra.com.mx";
            }

            // EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO VALIDA_USUARIO_LOTUS_CORREO
            UtilMail.enviaCorreo(destinatario, copiados, "" + titulo, strCorreoOwner, adjunto);

        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
        }
        return true;

    }

    // Se agrega la fecha para los acuerdos, dado que necesitan este parametro
    @SuppressWarnings("unused")
    public String creaDocumento(String params, boolean bandera, List<String> generales, String fecha) {
        String html = "";
        String fechaPendientes = UtilDate.getSysDate("dd/MM/yyyy");

        if (bandera) {
            html += "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><html>" + "<style> </style>"
                    + "<body>"
                    + "<div id='generalContainer' style='width:100%; background:#F0F0F0; height:100%; margin:0 auto; border:1px solid #E4E4E4;'>"
                    + "<div id='tituloContainer' style='width:100%; height:10%;  background:#F0F0F0; margin-left:0px;'>"
                    + "<h3 id='checklist' style='background:F0F0F0; text-align:center; color:#929292; font-family:Arial, Helvetica, sans-serif; font-weight: 100;'>"
                    + generales.get(0).toString().replace("\"", "") + "<br></br>"
                    + generales.get(1).toString().replace("\"", "") + "</h3>" + "</div>";

            html += "<div id='generalesContanier' style='width:100%; height:11%; background-color:#FFFFFF; margin:0 auto; align-text:center; border:1px solid #E4E4E4; border-radius: 25px;'>"
                    + "<div id='observacionesContainer' style='width:95%; height:100%; background-color:#FFFFFF; margin:0 auto; align-text:center;'>"
                    + "<p>Fecha Inicio: " + generales.get(2).toString().replace("\"", "") + ""
                    + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                    + " Fecha Fin:  " + generales.get(3).toString().replace("\"", "") + "  </p>" + "<p>Gerente: "
                    + generales.get(4).toString().replace("\"", "") + "</p>" + "<p>Regional: "
                    + generales.get(5).toString().replace("\"", "") + "</p>" + "</div>" + "</div>" + "<br></br>"
                    + "<div id='observacionesContainer' style='width:100%; height:9%; background-color:#FFFFFF; margin:0 auto; align-text:center; border:1px solid #E4E4E4; border-radius: 25px;'>"
                    + "<div id='observacionesContainer' style='width:95%; height:100%; background-color:#FFFFFF; margin:0 auto; align-text:center;'>"
                    + " <p>Observaciones:</p>" + "</div>" + "</div>" + "<br></br>";
        } else {
            html += "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><html>" + "<style> </style>"
                    + "<body>"
                    + "<div id='generalContainer' style='width:100%; background:#F0F0F0; height:100%; margin:0 auto; border:1px solid #E4E4E4;'>"
                    + "<div id='tituloContainer' style='width:100%; height:10%;  background:#F0F0F0; margin-left:0px;'>"
                    + "<h3 id='checklist' style='background:F0F0F0; text-align:center; color:#929292; font-family:Arial, Helvetica, sans-serif; font-weight: 100;'>Acuerdos Checklist Apertura<br></br>"
                    + fecha.split(" ")[0] + "</h3>" + "</div>";
        }

        html += "<div id='detalleContainer' style='width:100%; height:6%; background-color:#FFFFFF; margin:0 auto; align-text:center; border:1px solid #E4E4E4; border-radius: 25px;'>"
                + "<div id='detalleTextoContainer' style='width:95%; height:100%; background-color:#FFFFFF; margin:0 auto; align-text:center;'>"
                + " <p style='text-align:center;'>A continuaci&oacute;n se presentan los reactivos a corregirlos, acci&oacute;n y fecha Recuerda mantener correctamente los reactivos que tuviste bien en el Check List</p>"
                + "</div>" + "</div>"
                + "<br></br>"
                + "<div id='respuestasContanier' style='width:100%; height:100%; background:#F0F0F0; margin:0 auto; align-text:center;'>"
                + "<div id='dataRespuestasContainer' style='width:100%; height:100%; background:#F0F0F0; margin:0 auto; align-text:center;'>"
                + "<table style='background-color:#F0F0F0; width:100%;'>";

        html += params;

        html += "</table></div></div></div></body></html>";

        return html;
    }

    public String llenaDocumento(JsonElement listaCompromiso) {
        String html = "";
        String interno = "";
        String motivo = "";

        logger.info("LO QUE TRAEN MIS COMPROMISOS!!!" + listaCompromiso);

        JsonArray data = listaCompromiso.getAsJsonObject().get("preguntas").getAsJsonArray();

        if (!data.toString().equals("[]")) {
            for (int i = 0; i < data.size(); i++) {
                String preguntaPadre = data.get(i).getAsJsonObject().get("preguntaPadre").toString().replace("\"", "");
                String respuestaPadre = data.get(i).getAsJsonObject().get("respuestaPadre").toString().replace("\"",
                        "");

                html += "<tr>"
                        + "<td style='background-color:white; width:5%; height:150px; border: #F0F0F0 25px solid; border-radius:20px;'>"
                        + "<div style='width:100%; height:150px; background-color:white;'>"
                        + "<table style='width:100%; height:150px; align:center; background-color:white;'>" + "<tr>"
                        + "<td style='border:white 1px solid; width:1400px; background-color:white;'>" + (i + 1) + ".-"
                        + preguntaPadre.toString().replace("\"", "") + "</td>"
                        + "<td style='color:red; border: white 1px solid; width:75px; height:15px; background-color:white;'>"
                        + respuestaPadre.toString().replace("\"", "") + "</td>"// Respuesta NO
                        + "</tr>";

                JsonArray motivos = data.get(i).getAsJsonObject().get("respuestas").getAsJsonArray();

                interno = "";
                motivo = "";
                for (int j = 0; j < motivos.size(); j++) {
                    motivo += motivos.get(j).getAsJsonObject().get("pregunta").toString().replace("\"", "") + ":"
                            + motivos.get(j).getAsJsonObject().get("respuesta").toString().replace("\"", "")
                            + "<br></br>";
                }
                html += "<tr><td colspan='10' style='border: white 5px solid; background-color:white;'>" + motivo
                        + "</td>" + "</tr>" + interno + "</table>" + "</div>" + "</td>" + "</tr>";
            }

            /*
             * //System.out.println(res.get(i).getAsJsonObject().get("idCheck")); JsonArray
             * res2 = res.get(i).getAsJsonObject().get("preguntas").getAsJsonArray();
             * //System.out.println(res2.size());
             * //System.out.println(res2.get(0).toString());
             * //System.out.println(res2.get(0).getAsJsonObject().get("preguntaPadre"));
             * //System.out.println(res2.get(0).getAsJsonObject().get("respuestas").
             * getAsJsonArray().get(i).getAsJsonObject().get("pregunta"));
             */

 /*
             * for (int i = 0; i < listaCompromiso.size(); i++) { html += "<tr>"
             * +"<td style='background-color:white; width:5%; height:150px; border: #F0F0F0 25px solid; border-radius:20px;'>"
             * + "<div style='width:100%; height:150px;'>"
             * +"<table style='width:100%; align:center;'>" +"<tr>"
             * +"<td style='border:white 1px solid; width:1300px;'>"+(i+1)+".- "
             * +listaCompromiso.get(i).getDesPregunta()+"</td>"
             * +"<td style='color:red; border: white 1px solid; width:75px;'></td>"//
             * Respuesta NO +"</tr>"
             *
             * +"<tr>" +" <td colspan='10' style='border: white 5px solid;'>ACUERDO: "
             * +listaCompromiso.get(i).getDescripcion()+"</td>" +"</tr>" +"</table>"
             * +"</div>" + "</td>" +"</tr>"; }
             */
        } else {
            html += "<tr>"
                    + "<td style='background-color:white; width:5%; height:150px; border: #F0F0F0 25px solid; border-radius:20px;'>"
                    + "<div style='width:100%; height:150px;'>" + "<table style='width:100%; align:center;'>" + "<tr>"
                    + "<td style='border:white 1px solid; width:1300px;'></td>"
                    + "<td style='color:red; border: white 1px solid; width:75px;'></td>"// Respuesta NO
                    + "</tr>" + "<tr><td colspan='10' style='border: white 5px solid;'></td>" + "</tr>" + "</table>"
                    + "</div>" + "</td>" + "</tr>";
        }
        return html;
    }

    // Este metodo manda el archivo adjunto excel con la lista de todos los que
    // esten asignados a visitas
    public boolean sendMailVisitas(List<String> listaDestintarios, List<String> listaCopiados, File temp, String bunker,
            String textoBunker) {

        Calendar calendar = Calendar.getInstance();
        int numeroSemana = calendar.get(Calendar.WEEK_OF_YEAR) + 1;
        String titulo = "Visita de Sistemas - Banco Azteca" + bunker;
        boolean ban = false;

        logger.info("entre BI... ");

        try {
            Resource correoColab = UtilResource.getResourceFromInternet("mailVisitas.html");

            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoOwner = strCorreoOwner.replace("@Titulo", "Visita de Sistemas <BR>Banco Azteca" + bunker);
            strCorreoOwner = strCorreoOwner.replace("@Contenido",
                    "Buen d&iacute;a, <BR>Se comparte el listado de sucursales " + textoBunker + " en la semana "
                    + numeroSemana + ".");
            UtilMail.enviaCorreosVisitas(listaDestintarios, listaCopiados, "" + titulo, strCorreoOwner, temp);
            ban = true;
        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
            ban = false;

            if (temp != null) {
                temp.delete();
                temp.deleteOnExit();
            }
        }
        return ban;
    }

    // este metodo manda 1 correo al asignado a la visita o a la sucursal que lo
    // recibira
    public boolean sendMailEstado(int idSocio, String nombre, String numSucursal, String sucursal, String fecha,
            String bunker, String tituloBuker) {

        String titulo = "Visita de Sistemas - Banco Azteca" + tituloBuker;
        boolean ban = false;
        List<String> destinatarios = new ArrayList<>();
        List<String> copiados = new ArrayList<>();

        String numTienda = "";

        if (numSucursal.length() > 5) {
            numTienda = numSucursal.substring(2, 6);
        } else {
            numTienda = numSucursal.substring(2, (numSucursal.length() - 1));
        }

        logger.info("entre BI... ");
        logger.info("DESTINATARIO ----- >>>>" + idSocio);
        logger.info("NO SUCURSAL ----- >>>>" + numSucursal);
        logger.info("SUCURSAL ----- >>>>" + numTienda);

        if (numTienda.length() == 4) {
            numTienda = "G" + numTienda;
        } else if (numTienda.length() == 3) {
            numTienda = "G" + "0" + numTienda;
        } else if (numTienda.length() == 2) {
            numTienda = "G" + "00" + numTienda;
        } else if (numTienda.length() == 1) {
            numTienda = "G" + "000" + numTienda;
        }

        logger.info("Numero de Negocio Final... " + numTienda);

        CorreosLotus correosLotus = new CorreosLotus();
        String correoSucursal = "";
        try {
            correoSucursal = correosLotus.validaUsuarioLotusCorreos(numTienda);
            logger.info("correoSucursal... " + correoSucursal);

            if (correoSucursal != null) {
                logger.info("correoSucursal... ");
                if (!correoSucursal.contains("@")) {
                    logger.info("correoSucursal if... ");
                    correoSucursal = correosLotus.validaUsuarioLotusCorreos(numTienda.replace("G", "MAZ"));
                }
            } else {
                logger.info("correoSucursal else... ");
                correoSucursal = correosLotus.validaUsuarioLotusCorreos(numTienda.replace("G", "MAZ"));
            }
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
        }

        try {
            String destinatario = "";

            destinatario = correosLotus.validaUsuarioLotusCorreos("" + idSocio);
            logger.info("DESTINATARIO ----- >>>>" + destinatario);
            logger.info("CORREO SUCURSAL ----- >>>>" + correoSucursal);

            destinatarios.add("alejandro.morales@elektra.com.mx");

            logger.info("destinatarios " + destinatarios.toString());
            logger.info("copiados " + copiados.toString());

            Resource correoColab = UtilResource.getResourceFromInternet("mailVisitasAsignadas.html");

            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoOwner = strCorreoOwner.replace("@Titulo", "Visita de Sistemas <BR>Banco Azteca" + tituloBuker);
            strCorreoOwner = strCorreoOwner.replace("@ContenidoUno",
                    "Buen d&iacute;a, por medio del presente se notifica que el socio" + "<strong>"
                    + nombre.split("-")[1] + "</strong> con número de socio <strong>" + idSocio + "</strong>"
                    + " realizará una visita a sucursal " + "<strong>" + sucursal + "</strong> el día <strong>"
                    + fecha.substring(0, 10) + "</strong> a las <strong>" + fecha.substring(11, 16)
                    + "</strong> hrs" + bunker + ".");
            strCorreoOwner = strCorreoOwner.replace("@ContenidoDos",
                    "El motivo de la visita es poder conocer y detectar la problemática en los sistemas que se usan en la sucursal.");

            UtilMail.enviaCorreosVisitas(destinatarios, copiados, "" + titulo, strCorreoOwner, null);

            ban = true;
        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
            ban = false;
        }
        return ban;
    }

    public boolean sendMailCalif7S(String listaDestintarios, List<String> listaCopiados, String sucursal, double calif,
            String comentarios, String fecha, String nomCeco) {

        Calendar calendar = Calendar.getInstance();
        int numeroSemana = calendar.get(Calendar.WEEK_OF_YEAR) + 1;
        String titulo = "Calificación del Protocolo de Medición 7S para la sucursal " + sucursal + "  " + nomCeco;
        boolean ban = false;

        logger.info("entre BI... ");

        try {
            Resource correoColab = UtilResource.getResourceFromInternet("mailCalif7S.html");

            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoOwner = strCorreoOwner.replace("@Titulo",
                    "Calificación del Protocolo de Medición 7S para la sucursal  " + sucursal + " " + nomCeco);
            strCorreoOwner = strCorreoOwner.replace("@Contenido",
                    "Buen d&iacute;a,<BR><B>Calificación :</B> " + calif + "%<BR><B>Fecha:</B> " + fecha + "<BR>");
            strCorreoOwner = strCorreoOwner.replace("@Comentarios", comentarios);
            UtilMail.enviaCorreo(listaDestintarios, listaCopiados, "" + titulo, strCorreoOwner, null);
            // UtilMail.enviaCorreosVisitas(listaDestintarios, listaCopiados, "" + titulo,
            // strCorreoOwner,null);
            ban = true;
        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
            ban = false;
        }
        return ban;
    }

    public boolean sendMailExpansion(String economico, String sucursal, String tipo, String territorio, String zona,
            List<String> imperdonables, List<FirmaCheckDTO> firmas, List<ReporteChecksExpDTO> incidencias,
            String aperturable, String imgFachada) throws DocumentException, IOException {
        // copiados.add(user.getNombre());
        // logger.info("copiados- " + copiados);
        copiados = new ArrayList<String>();
        String titulo = "";
        titulo = "ACTA ENTREGA FRANQUICIA RECEPCIÓN DE SUCURSAL";
        String html = "";
        String imperdonablesHtml = "";
        String firmasHtml = "";
        String incidenciasHtml = "";

        // Lleno la tabla de imperdonables
        if (imperdonables.size() == 0) {
            imperdonablesHtml += "<tr>";
            imperdonablesHtml += "<td> No hay imperdonables detectados </td>";
            imperdonablesHtml += "</tr>";

        } else {
            imperdonablesHtml += "<tr>";
            imperdonablesHtml += "<td> Se anexa listado con los imperdonables detectados:</td>";
            imperdonablesHtml += "</tr>";
            imperdonablesHtml += "<br></br>";
        }

        for (int i = 0; i < imperdonables.size(); i++) {

            imperdonablesHtml += "<tr>";
            imperdonablesHtml += "<td>" + (i + 1) + ".- " + imperdonables.get(i) + "</td>";
            imperdonablesHtml += "</tr>";

            imperdonablesHtml += "<tr>";
            imperdonablesHtml += "<td> NO </td>";
            imperdonablesHtml += "</tr>";

            /*
             * if (imperdonables.size() == 1) { imperdonablesHtml += "<tr>";
             * imperdonablesHtml += "<td>"+imperdonables.get(i)+"</td>"; imperdonablesHtml
             * += "</tr>"; } else if (imperdonables.size() > 1){
             *
             * if (i == 0 || i == 2 || i == 4|| i == 6|| i == 8|| i == 10|| i == 12|| i ==
             * 14|| i == 16 || i == 18|| i == 20|| i == 22 || i == 24) { imperdonablesHtml
             * += "<tr>"; }
             *
             * imperdonablesHtml += "<td>Hallazgo"+i+"</td>";
             *
             * if (i == 1 || i == 3 || i == 5 || i == 7 || i == 9 || i == 11 || i == 13 || i
             * == 15 || i == 17|| i == 19|| i == 21|| i == 23|| i == 25) { imperdonablesHtml
             * += "</tr>"; }
             *
             * }
             */
        }

        // Lleno la tabla de Firmas
        for (int i = 0; i < firmas.size(); i++) {
            FirmaCheckDTO obj = firmas.get(i);

            if (i == 0 || i == 3) {
                firmasHtml += "<tr>";
            }

            firmasHtml += "<td> <b>" + obj.getPuesto() + "</b><br></br>" + "Nombre: " + obj.getResponsable()
                    + "<br></br>" + "No. Empleado: "
                    + (obj.getIdUsuario() == null ? "" : (obj.getIdUsuario().equals("null") ? "" : obj.getIdUsuario()))
                    + "<br></br>" + "<img src='" + obj.getRuta() + "' width='200' height='100'/>" + "</td>";

            if (i == 2 || i == 5) {
                firmasHtml += "</tr>";
            }
        }

        // Lleno la tabla de incidencias
        if (incidencias.size() == 0) {
            incidenciasHtml += "<tr>";
            incidenciasHtml += "<td> No hay hallazgos detectados </td>";
            incidenciasHtml += "</tr>";
        } else {
            incidenciasHtml += "<tr>";
            incidenciasHtml += "<td> Se anexa listado con los hallazgos detectados: </td>";
            incidenciasHtml += "</tr>";
            incidenciasHtml += "<br></br>";
        }

        for (int i = 0; i < incidencias.size(); i++) {
            incidenciasHtml += "<tr>";
            incidenciasHtml += "<td>" + (i + 1) + ".- " + incidencias.get(i).getPregunta() + " </td>";
            incidenciasHtml += "</tr>";

            incidenciasHtml += "<tr>";
            incidenciasHtml += "<td> NO </td>";
            incidenciasHtml += "</tr>";
            /*
             * if (i == 0 || i == 2 || i == 4|| i == 6|| i == 8|| i == 10|| i == 12|| i ==
             * 14|| i == 16 || i == 18|| i == 20|| i == 22 || i == 24) { incidenciasHtml +=
             * "<tr>"; } incidenciasHtml += "<td>Incidencia"+i+"</td>"; if (i == 1 || i == 3
             * || i == 5 || i == 7 || i == 9 || i == 11 || i == 13 || i == 15 || i == 17|| i
             * == 19|| i == 21|| i == 23|| i == 25) { incidenciasHtml += "</tr>"; }
             */
        }

        // html = creaPDFExpansion(html,false,null,fechaCierre);
        html = creaPDFExpansionFinal(economico, sucursal, territorio, zona, imperdonablesHtml, firmasHtml,
                incidenciasHtml, aperturable, imgFachada);

        // try {
        // Resource correoColab =
        // UtilResource.getResourceFromInternet("mailAcuerdos.html");
        // String strCorreoOwner =
        // UtilObject.inputStreamAsString(correoColab.getInputStream());
        String strCorreoOwner = "";

        /*
         * strCorreoOwner = strCorreoOwner.replace("@Titulo",""); strCorreoOwner =
         * strCorreoOwner.replaceAll("null", "Sin Dato"); strCorreoOwner =
         * strCorreoOwner.replace("@Lista", html);
         */
        String destinatario = "";

        List<String> docHtml = new ArrayList<String>();
        docHtml.add(html);

        logger.info("_______________________________ANTES DE ESCRIBIR PDF_______________________________");
        File adjunto = escribirPDFHtml(docHtml);
        logger.info("_______________________________DESPUES DE ESCRIBIR PDF_______________________________");

        List<String> listaCopiados = new ArrayList<String>();

        for (int i = 0; i < firmas.size(); i++) {
            listaCopiados.add(firmas.get(i).getCorreo());
        }

        try {
            destinatario = "amorales@elektra.com.mx";
            copiados = listaCopiados;
            copiados.add("amorales@elektra.com.mx");
            //copiados.add("jsepulveda@elektra.com.mx");
            logger.info("DESTINATARIO - " + destinatario);
        } catch (Exception e) {
            logger.info("Algo ocurrió al consultar el correoLotus " + e);
            destinatario = "amorales@elektra.com.mx";
        }
        UtilMail.enviaCorreoExpansion(destinatario, copiados, "" + titulo, strCorreoOwner, adjunto, 0, 0, 0);

        /*
         * } catch (IOException e) { logger.info("Algo ocurrió... " + e); }
         */
        return true;

    }

    // Se agrega la fecha para los acuerdos, dado que necesitan este parametro
    @SuppressWarnings("unused")
    public String creaPDFExpansion(String params, boolean bandera, List<String> generales, String fecha) {
        String html = "";
        String fechaPendientes = UtilDate.getSysDate("dd/MM/yyyy");

        // html += "<meta http-equiv='Content-Type' content='text/html;
        // charset=UTF-8'><html>"
        html += "<html> <body>  HOLA";

        html += "<img src='http://10.53.33.82/franquicia/cedula/fotos/699806.jpg' alt='W3Schools.com' />";

        html += "</body></html>";

        return html;
    }

    @SuppressWarnings("unused")
    public String creaPDFExpansionFinal(String economico, String sucursal, String territorio, String zona,
            String imperdonablesHtml, String firmasHtml, String incidenciasHtml, String aperturable,
            String imgFachada) {
        String ip = FRQConstantes.getRutaImagen();
        String html = "";

        String hora = "";
        String minuto = "";
        String dia = "";
        String mes = "";
        String anio = "";

        String txtAperturable = "";

        Calendar c1 = Calendar.getInstance();
        Calendar c2 = new GregorianCalendar();

        String horaC = Integer.toString(c2.get(Calendar.HOUR)) + "";
        if (horaC.length() == 1) {
            horaC = "0" + horaC;
        }
        if (horaC.equals("00")) {
            horaC = "12";
        }

        String minC = Integer.toString(c2.get(Calendar.MINUTE)) + "";
        if (minC.length() == 1) {
            minC = "0" + minC;
        }

        String diaC = Integer.toString(c2.get(Calendar.DATE)) + "";
        if (diaC.length() == 1) {
            diaC = "0" + diaC;
        }

        String mesC = Integer.toString(c2.get(Calendar.MONTH)) + "";
        if (mesC.length() == 1) {
            mesC = "0" + mesC;
        }

        mesC = getNombreMes(mesC);

        hora = horaC + ":" + minC;
        dia = diaC;
        mes = mesC;
        anio = Integer.toString(c2.get(Calendar.YEAR));

        if (aperturable.contains("true")) {
            txtAperturable = "<h3 style='color: green;'>SUCURSAL OPERABLE</h3>";
        } else {
            txtAperturable = "<h3 style='color: red;'>SUCURSAL NO OPERABLE</h3>";
        }

        // html = "<!DOCTYPE html>" +
        html = "<html>" + "<head>" + "<title>Acta Entrega</title>" + "</head>" + "<body>"
                + // ABRE DIV LOGOS
                "<div>" + "<h2 align='center'>"
                + "<img src='" + ip + "/franquicia/firmaChecklist/firmalogo_elektra.png' width='125' height='35' />"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='" + ip + "/franquicia/firmaChecklist/firmalogo-banco-azteca.jpg' width='100' height='50' />"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='" + ip + "/franquicia/firmaChecklist/firmalogo_presta_prenda.png' width='125' height='50' />"
                + "</h2>" + "</div>"
                + // CIERRA DIV LOGOS
                // TÍTULO
                "<h2 align='center'>RESUMEN CAMINATA</h2>"
                + // TÍTULO
                // DIV IMAGEN SUCURSAL
                "<div style='width='600' height='300'>" + "<h2 align='center'>" + "<img src='" + imgFachada
                + "' width='600' height='200'/>" + "</h2>" + "</div>"
                + // FIN IMAGEN SUCURSAL
                // TEXTO APERTURABLE O NO
                txtAperturable
                + // FIN TEXTO APERTURABLE O NO
                // DIV DATOS DE LA SUCURSAL
                /*
                 * "<br></br>" + "<div style='width:100%; height:75px;'>" +
                 * "<table style='width:100%'>" + "<tr>" + "<td><b>Eco: </b> " + economico
                 * +"</td>" + "<td><b>Sucursal: </b>" + sucursal + "</td>" +
                 * "<td><b>Fecha: </b>"+ dia + "/" + mes + "/" + anio +"</td>"+ "</tr>" +
                 *
                 * "<tr>" + "<td><b>Tipo de proyecto: </b> Nueva</td>" +
                 * "<td><b>Territorio: </b> "+ territorio +"</td>" +
                 * "<td><b>Zona o Region: </b> " + zona + "</td>" + "</tr>" +
                 *
                 * "</table>" + "</div>" +
                 */ // FIN DIV DATOS DE LA SUCURSAL
                // DIV IMPERDONABLES
                "<div style='width:100%; height:240px;'>"
                + "<p>Siendo las " + hora + " horas del d&iacute;a " + dia + " del mes de " + mes + " del " + anio
                + ", se realiza la caminata de la sucursal con CECO 48" + economico + " y NOMBRE " + sucursal
                + " concluyendo con el siguiente diagnostico:</p>"
                + "<p>Se entregan Claves de acceso a Bunker y Guarda valores.</p>"
                + "<p>Capacitación del uso de las puertas de bunker y guarda valores.</p>"
                + "<p>Se valida el sistema de seguridad completo.</p>" + "<p>Se valida la llegada de mercancía</p>"
                + "<p>Por el recorrido anterior se determina realizar Soft Opening en 8 días una vez cerrada esta acta</p>"
                + "</div>"
                + // FIN DIV IMPERDONABLES
                // INICIO LISTA IMPERDONABLES
                "<table style='width:100%'>" + imperdonablesHtml + "</table>"
                + // FIN LISTA IMPERDONABLES
                // TEXTO ANTES FIRMAS
                "<p>Se realiza recorrido por la obra, revisando los trabajos efectuados y ejecutados con base a los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra, proyecto ejecutivo e indicaciones hechas oportunamente en el libro  bit&aacute;cora,<b> el Cliente lo recibe, con la salvedad que esto ser&aacute; considerado efectivo una vez que se ha cumplido con los detalles y compromisos registrados en el Checklist General<sup></sup> adjunto y realizado en la caminata, siendo los Gerentes de la tienda qui&eacute;nes deber&aacute;n asegurar el cumplimiento antes mencionado.</b></p>"
                + "<br></br>" + "<br></br>" + "<br></br>"
                + // FIN TEXTO ANTES FIRMAS
                // TABLA DE FIRMAS
                "<table border=1 cellspacing=0 cellpadding=2   style='width:100%'>" + firmasHtml + "</table>"
                + // FIN TABLA DE FIRMAS
                "<br></br>"
                + // TABLA DE NO
                "<table style='width:100%' >" + incidenciasHtml + "</table>"
                + // FIN TABLA DE NO
                "</body>" + "</html>";

        return html;
    }

    public String getNombreMes(String numMes) {

        switch (numMes) {

            case "00":

                return "Enero";

            case "01":

                return "Febrero";

            case "02":

                return "Marzo";

            case "03":

                return "Abril";

            case "04":

                return "Mayo";

            case "05":

                return "Junio";

            case "06":

                return "Julio";

            case "07":

                return "Agosto";

            case "08":

                return "Septiembre";

            case "09":

                return "Octubre";

            case "10":

                return "Noviembre";

            case "11":

                return "Diciembre";

        }

        return "";

    }

    // MAIL EXPANSIÓN NUEVO FORMATO
    File graficaComunes = null;
    File graficaBanco = null;
    File graficaElektra = null;
    File graficaGenerales = null;
    File graficaPresta = null;
    File graficaTotal = null;

    public boolean sendMailExpansionNuevoFormato(String idBitacora, String tipoVisita, String fecha, String economico,
            String sucursal, String tipo, String territorio, String zona, List<String> imperdonables,
            List<FirmaCheckDTO> firmas, List<ReporteChecksExpDTO> incidencias, String aperturable, String imgFachada)
            throws DocumentException, IOException {

        String nombreSucursal = "" + sucursal;
        String titulo = "ACTA ENTREGA FRANQUICIA RECEPCIÓN DE SUCURSAL " + nombreSucursal;
        String html = "";

        //Detectar la diferencia de Negocios para armar un documento diferente
        html = creaPDFExpansionNuevoFormato(idBitacora, tipoVisita, fecha, economico, sucursal, tipo, territorio, zona,
                imperdonables, firmas, incidencias, aperturable, imgFachada);

        List<String> docHtml = new ArrayList<String>();
        docHtml.add(html);

        // Debemos enviar el correo
        logger.info("_______________________________ANTES DE ESCRIBIR PDF_______________________________");
        File adjunto = escribirPDFHtmlExpansion(docHtml);
        logger.info("_______________________________DESPUES DE ESCRIBIR PDF_______________________________");

        List<String> listaCopiados = new ArrayList<String>();

        for (int i = 0; i < firmas.size(); i++) {
            listaCopiados.add(firmas.get(i).getCorreo());
        }

        String destinatario = "";
        String strCorreoOwner = "";

        try {
            destinatario = "checklistExpansion@elektra.com.mx";
            copiados = listaCopiados;
            copiados.add("alejandro.morales@elektra.com.mx");
            //copiados.add("jsepulveda@elektra.com.mx");
            logger.info("DESTINATARIO - " + destinatario);
        } catch (Exception e) {
            logger.info("Algo ocurrió al consultar el correoLotus " + e);
            destinatario = "amorales@elektra.com.mx";
        }
        UtilMail.enviaCorreoExpansion(destinatario, copiados, "" + titulo, strCorreoOwner, adjunto, 0, 0, 0);

        // System.out.println("****************RUTA*************** :"+
        // adjunto.getAbsolutePath() );
        // Eliminar Gráficas
        graficaComunes.delete();
        graficaBanco.delete();
        graficaElektra.delete();
        graficaGenerales.delete();
        graficaPresta.delete();
        graficaTotal.delete();

        adjunto.delete();

        return true;

    }

    //envioActaViejaConSandra
    public boolean sendMailExpansionNuevoFormatoConteo(String idBitacora, String tipoVisita, String fecha,
            String economico, String sucursal, String tipo, String territorio, String zona, List<String> imperdonables,
            List<FirmaCheckDTO> firmas, List<ReporteChecksExpDTO> incidencias, String aperturable, String imgFachada, String tipoNegocio)
            throws DocumentException, IOException {

        String nombreSucursal = "" + sucursal;
        String titulo = "ACTA ENTREGA FRANQUICIA RECEPCIÓN DE SUCURSAL " + nombreSucursal;
        String html = "";

        List<ChecklistHallazgosRepoDTO> lista = null;
        List<SucursalChecklistDTO> sucursal3 = null;
        int version = 0;
        try {
            lista = hallazgosRepoBI.obtieneDatosNego(idBitacora);
            if (lista != null && lista.size() > 0) {
                sucursal3 = sucursalChecklistBI.obtieneDatosSuc(lista.get(0).getCeco());

            }
        } catch (Exception e) {
        }
        version = sucursal3.get(0).getAux();

        //if(tipoNegocio.equalsIgnoreCase("EKT")) {
        if (version == 1) {
            html = creaPDFExpansionNuevoFormatoConteo(idBitacora, tipoVisita, fecha, economico, sucursal, tipo, territorio,
                    zona, imperdonables, firmas, incidencias, aperturable, imgFachada);
        } else {
            html = creaPDFExpansionNuevoFormatoConteoGeneral(idBitacora, tipoVisita, fecha, economico, sucursal, tipo, territorio,
                    zona, imperdonables, firmas, incidencias, aperturable, imgFachada, tipoNegocio);
        }

        //¿QUÉ TIPO ES?
        /*
         switch (tipoNegocio) {
         case "EKT":
         html = creaPDFExpansionNuevoFormatoConteo(idBitacora, tipoVisita, fecha, economico, sucursal, tipo, territorio,
         zona, imperdonables, firmas, incidencias, aperturable, imgFachada);
         break;
         case "OCC":
         html = creaPDFExpansionNuevoFormatoConteoOCC(idBitacora, tipoVisita, fecha, economico, sucursal, tipo, territorio,
         zona, imperdonables, firmas, incidencias, aperturable, imgFachada);
         break;
         case "DAZ":
         html = creaPDFExpansionNuevoFormatoConteo(idBitacora, tipoVisita, fecha, economico, sucursal, tipo, territorio,
         zona, imperdonables, firmas, incidencias, aperturable, imgFachada);
         break;
         default:
         break;
         }
         */
        List<String> docHtml = new ArrayList<String>();
        docHtml.add(html);

        // Debemos enviar el correo
        logger.info("_______________________________ANTES DE ESCRIBIR PDF_______________________________");
        File adjunto = escribirPDFHtmlExpansion(docHtml);
        logger.info("_______________________________DESPUES DE ESCRIBIR PDF_______________________________");

        List<String> listaCopiados = new ArrayList<String>();

        for (int i = 0; i < firmas.size(); i++) {
            listaCopiados.add(firmas.get(i).getCorreo());
        }

        String destinatario = "";
        String strCorreoOwner = "";

        try {
            destinatario = "checklistExpansion@elektra.com.mx";
            copiados = listaCopiados;
            copiados.add("alejandro.morales@elektra.com.mx");
            copiados.add("luis.gomezv@elektra.com.mx");
            copiados.add("erick.carrasco@elektra.com.mx");
            copiados.add("rafael.reyes@elektra.com.mx");
            copiados.add("jefloresh@bancoazteca.com.mx");
            copiados.add("alfredo.gordillo@elektra.com.mx");
            copiados.add("mareyesp@elektra.com.mx");
            //copiados.add("ssalasg@elektra.com.mx");
            //copiados.add("hmartinezt@elektra.com.mx");
            //copiados.add("arceballos@bancoazteca.com.mx");
            //copiados.add("jsepulveda@elektra.com.mx");
            logger.info("DESTINATARIO - " + destinatario);
        } catch (Exception e) {
            logger.info("Algo ocurrió al consultar el correoLotus " + e);
            destinatario = "alejandro.morales@elektra.com.mx";
        }
        UtilMail.enviaCorreoExpansion(destinatario, copiados, "" + titulo, strCorreoOwner, adjunto, 0, 0, 0);

        // System.out.println("****************RUTA*************** :"+
        // adjunto.getAbsolutePath() );
        // Eliminar Gráficas
        switch (tipoNegocio) {
            case "EKT":
                graficaComunes.delete();
                graficaBanco.delete();
                graficaElektra.delete();
                graficaGenerales.delete();
                graficaPresta.delete();
                graficaTotal.delete();

                adjunto.delete();

                break;
            case "CYC":
                graficaComunes.delete();
                graficaBanco.delete();
                graficaGenerales.delete();
                graficaTotal.delete();

                adjunto.delete();
                break;
            case "DAZ":
                graficaComunes.delete();
                graficaBanco.delete();
                graficaElektra.delete();
                graficaGenerales.delete();
                graficaPresta.delete();
                graficaTotal.delete();

                adjunto.delete();

                break;

            default:
                break;
        }

        return true;

    }

    //envioActaViejaSinSandra
    public boolean sendMailExpansionNuevoFormatoConteo(String idBitacora, String tipoVisita, String fecha,
            String economico, String sucursal, String tipo, String territorio, String zona, List<String> imperdonables,
            List<FirmaCheckDTO> firmas, List<ReporteChecksExpDTO> incidencias, String aperturable, String imgFachada, String tipoNegocio, int correo)
            throws DocumentException, IOException {

        String nombreSucursal = "" + sucursal;
        String titulo = "ACTA ENTREGA FRANQUICIA RECEPCIÓN DE SUCURSAL " + nombreSucursal;
        String html = "";

        List<ChecklistHallazgosRepoDTO> lista = null;
        List<SucursalChecklistDTO> sucursal3 = null;
        int version = 0;
        try {
            lista = hallazgosRepoBI.obtieneDatosNego(idBitacora);
            if (lista != null && lista.size() > 0) {
                sucursal3 = sucursalChecklistBI.obtieneDatosSuc(lista.get(0).getCeco());

            }
        } catch (Exception e) {
        }
        version = sucursal3.get(0).getAux();

        //¿QUÉ TIPO ES?
        //if(tipoNegocio.equalsIgnoreCase("EKT")) {
        /*if (version == 1) {
            html = creaPDFExpansionNuevoFormatoConteo(idBitacora, tipoVisita, fecha, economico, sucursal, tipo, territorio,
                    zona, imperdonables, firmas, incidencias, aperturable, imgFachada);
        } else {*/
        html = creaPDFExpansionNuevoFormatoConteoGeneral(idBitacora, tipoVisita, fecha, economico, sucursal, tipo, territorio,
                zona, imperdonables, firmas, incidencias, aperturable, imgFachada, tipoNegocio);
        //}
        /*
         switch (tipoNegocio) {
         case "EKT":
         html = creaPDFExpansionNuevoFormatoConteo(idBitacora, tipoVisita, fecha, economico, sucursal, tipo, territorio,
         zona, imperdonables, firmas, incidencias, aperturable, imgFachada);
         break;
         case "OCC":
         html = creaPDFExpansionNuevoFormatoConteoOCC(idBitacora, tipoVisita, fecha, economico, sucursal, tipo, territorio,
         zona, imperdonables, firmas, incidencias, aperturable, imgFachada);
         break;
         case "DAZ":
         html = creaPDFExpansionNuevoFormatoConteo(idBitacora, tipoVisita, fecha, economico, sucursal, tipo, territorio,
         zona, imperdonables, firmas, incidencias, aperturable, imgFachada);
         break;
         default:
         break;
         }
         */

        List<String> docHtml = new ArrayList<String>();
        docHtml.add(html);

        // Debemos enviar el correo
        logger.info("_______________________________ANTES DE ESCRIBIR PDF_______________________________");
        logger.info("" + docHtml);
        File adjunto = escribirPDFHtmlExpansion(docHtml);
        logger.info("_______________________________DESPUES DE ESCRIBIR PDF_______________________________");

        List<String> listaCopiados = new ArrayList<String>();

        for (int i = 0; i < firmas.size(); i++) {
            listaCopiados.add(firmas.get(i).getCorreo());
        }

        String destinatario = "";
        String strCorreoOwner = "";

        try {
            destinatario = "checklistExpansion@elektra.com.mx";
            copiados = listaCopiados;
            copiados.add("alejandro.morales@elektra.com.mx");
            copiados.add("luis.gomezv@elektra.com.mx");
            copiados.add("erick.carrasco@elektra.com.mx");
            //copiados.add("jsepulveda@elektra.com.mx");
            logger.info("DESTINATARIO - " + destinatario);
        } catch (Exception e) {
            logger.info("Algo ocurrió al consultar el correoLotus " + e);
            destinatario = "alejandro.morales@elektra.com.mx";
        }
        UtilMail.enviaCorreoExpansion(destinatario, copiados, "" + titulo, strCorreoOwner, adjunto, 0, 0, 0);

        // System.out.println("****************RUTA*************** :"+
        // adjunto.getAbsolutePath() );
        // Eliminar Gráficas
        switch (tipoNegocio) {
            case "EKT":
                graficaComunes.delete();
                graficaBanco.delete();
                graficaElektra.delete();
                graficaGenerales.delete();
                graficaPresta.delete();
                graficaTotal.delete();

                adjunto.delete();

                break;
            case "CYC":
                graficaComunes.delete();
                graficaBanco.delete();
                graficaGenerales.delete();
                graficaTotal.delete();

                adjunto.delete();
                break;
            case "DAZ":
                graficaComunes.delete();
                graficaBanco.delete();
                graficaElektra.delete();
                graficaGenerales.delete();
                graficaPresta.delete();
                graficaTotal.delete();

                adjunto.delete();

                break;

            default:
                break;
        }

        return true;

    }

    public String creaPDFExpansionNuevoFormato(String idBitacora, String tipoVisita, String fecha, String economico,
            String sucursal, String tipo, String territorio, String zona, List<String> imperdonables,
            List<FirmaCheckDTO> firmas, List<ReporteChecksExpDTO> incidencias, String aperturable, String imgFachada) {

        // Obtengo Files de Gráfica
        // Valores de PDF
        String operable = "";
        String colorOperable = "";

        if (tipoVisita.equals("0")) {
            tipoVisita = "Primer Recorrido";
        } else {
            tipoVisita = "Soft Openning";
        }

        // Imperdonables
        String numImperdonables = imperdonables.size() + "";

        DecimalFormat formatDecimales = new DecimalFormat("###.##");
        DecimalFormat formatEnteros = new DecimalFormat("###.##");

        // Obtenemos Datos
        ChecklistPreguntasComBI checklistPreguntasComBI = (ChecklistPreguntasComBI) FRQAppContextProvider
                .getApplicationContext().getBean("checklistPreguntasComBI");

        List<ChecklistPreguntasComDTO> listaGrupos = checklistPreguntasComBI.obtieneInfoCont(idBitacora);

        // AREAS
        ChecklistPreguntasComDTO areasComunesObj = listaGrupos.get(0);
        // BAZ 1
        ChecklistPreguntasComDTO bazObj = listaGrupos.get(1);
        // EKT 2
        ChecklistPreguntasComDTO ektObj = listaGrupos.get(2);
        // GENERALES 3
        ChecklistPreguntasComDTO generalesObj = listaGrupos.get(3);
        // PP 4
        ChecklistPreguntasComDTO prestaPrendaObj = listaGrupos.get(4);

        // Grafica Cómunes
        // Cálculo Total de Preguntas
        double comResSi1 = areasComunesObj.getTotPreg() - areasComunesObj.getPregNo();
        double comResNo1 = areasComunesObj.getPregNo();
        double comTotal = areasComunesObj.getTotPreg();
        double comSiPor1 = (comResSi1 / comTotal) * 100;
        double comNoPor1 = (comResNo1 / comTotal) * 100;

        // Calculo Por Ponderación
        double comPorcentajePonderacion = (areasComunesObj.getPrecalif() * 100) / areasComunesObj.getSumGrupo();
        int comunesImperdonables = areasComunesObj.getAux2();
        String logoCom = getLogoExpansion(comPorcentajePonderacion, comunesImperdonables);

        String comResSi = formatEnteros.format(comResSi1);
        String comResNo = formatEnteros.format(comResNo1);
        String comSiPor = formatDecimales.format(comSiPor1);
        String comNoPor = formatDecimales.format(comNoPor1);

        graficaComunes = grafica(1, Double.parseDouble(comSiPor), Double.parseDouble(comNoPor));

        // Grafica Banco
        double bazResSi1 = bazObj.getTotPreg() - bazObj.getPregNo();
        double bazResNo1 = bazObj.getPregNo();
        double bazTotal = bazObj.getTotPreg();
        double bazSiPor1 = (bazResSi1 / bazTotal) * 100;
        double bazNoPor1 = (bazResNo1 / bazTotal) * 100;

        // Calculo Por Ponderación
        double bazPorcentajePonderacion = (bazObj.getPrecalif() * 100) / bazObj.getSumGrupo();
        int bazImperdonables = bazObj.getAux2();
        String logoBaz = getLogoExpansion(bazPorcentajePonderacion, bazImperdonables);

        String bazResSi = formatEnteros.format(bazResSi1);
        String bazResNo = formatEnteros.format(bazResNo1);
        String bazSiPor = formatDecimales.format(bazSiPor1);
        String bazNoPor = formatDecimales.format(bazNoPor1);

        graficaBanco = grafica(1, Double.parseDouble(bazSiPor), Double.parseDouble(bazNoPor));

        // Grafica Elektra
        double ektResSi1 = ektObj.getTotPreg() - ektObj.getPregNo();
        double ektResNo1 = ektObj.getPregNo();
        double ektTotal = ektObj.getTotPreg();
        double ektSiPor1 = (ektResSi1 / ektTotal) * 100;
        double ektNoPor1 = (ektResNo1 / ektTotal) * 100;

        // Calculo Por Ponderación
        double ektPorcentajePonderacion = (ektObj.getPrecalif() * 100) / ektObj.getSumGrupo();
        int ektImperdonables = ektObj.getAux2();
        String logoEkt = getLogoExpansion(ektPorcentajePonderacion, ektImperdonables);

        String ektResSi = formatEnteros.format(ektResSi1);
        String ektResNo = formatEnteros.format(ektResNo1);
        String ektSiPor = formatDecimales.format(ektSiPor1);
        String ektNoPor = formatDecimales.format(ektNoPor1);

        graficaElektra = grafica(1, Double.parseDouble(ektSiPor), Double.parseDouble(ektNoPor));

        // Grafica Generales
        double genResSi1 = generalesObj.getTotPreg() - generalesObj.getPregNo();
        double genResNo1 = generalesObj.getPregNo();
        double genTotal = generalesObj.getTotPreg();
        double genSiPor1 = (genResSi1 / genTotal) * 100;
        double genNoPor1 = (genResNo1 / genTotal) * 100;

        // Calculo Por Ponderación
        double generalesPorcentajePonderacion = (generalesObj.getPrecalif() * 100) / generalesObj.getSumGrupo();
        int generalesImperdonables = generalesObj.getAux2();
        String logoGen = getLogoExpansion(generalesPorcentajePonderacion, generalesImperdonables);

        String genResSi = formatEnteros.format(genResSi1);
        String genResNo = formatEnteros.format(genResNo1);
        String genSiPor = formatDecimales.format(genSiPor1);
        String genNoPor = formatDecimales.format(genNoPor1);

        graficaGenerales = grafica(1, Double.parseDouble(genSiPor), Double.parseDouble(genNoPor));

        // Grafica Presta Prenda
        double prestaResSi1 = prestaPrendaObj.getTotPreg() - prestaPrendaObj.getPregNo();
        double prestaResNo1 = prestaPrendaObj.getPregNo();
        double prestaTotal = prestaPrendaObj.getTotPreg();
        double prestaSiPor1 = (prestaResSi1 / prestaTotal) * 100;
        double prestaNoPor1 = (prestaResNo1 / prestaTotal) * 100;

        // Calculo Por Ponderación
        double prestaPorcentajePonderacion = (prestaPrendaObj.getPrecalif() * 100) / prestaPrendaObj.getSumGrupo();
        int prestaImperdonables = prestaPrendaObj.getAux2();
        String logoPres = getLogoExpansion(prestaPorcentajePonderacion, prestaImperdonables);

        String prestaResSi = formatEnteros.format(prestaResSi1);
        String prestaResNo = formatEnteros.format(prestaResNo1);
        String prestaSiPor = formatDecimales.format(prestaSiPor1);
        String prestaNoPor = formatDecimales.format(prestaNoPor1);

        graficaPresta = grafica(1, Double.parseDouble(prestaSiPor), Double.parseDouble(prestaNoPor));

        // Valores Total
        double totalResNo1 = (areasComunesObj.getPregNo() + bazObj.getPregNo() + ektObj.getPregNo()
                + generalesObj.getPregNo() + prestaPrendaObj.getPregNo());
        double totalTotal = (areasComunesObj.getTotPreg() + bazObj.getTotPreg() + ektObj.getTotPreg()
                + generalesObj.getTotPreg() + prestaPrendaObj.getTotPreg());
        String totalTotalFormat = formatDecimales.format(totalTotal);

        double totalResSi1 = (totalTotal - totalResNo1);
        double totalSiPor1 = (totalResSi1 / totalTotal) * 100;
        double totalNoPor1 = (totalResNo1 / totalTotal) * 100;
        // Sup
        double totalValorTotal = 100;
        // Calificación
        double totalCalif1 = (totalSiPor1 * totalValorTotal) / 100;

        // Calculo Por Ponderación
        double totalPrecalif = (areasComunesObj.getPrecalif() + bazObj.getPrecalif() + ektObj.getPrecalif()
                + generalesObj.getPrecalif() + prestaPrendaObj.getPrecalif());
        double totalSumGrupo = (areasComunesObj.getSumGrupo() + bazObj.getSumGrupo() + ektObj.getSumGrupo()
                + generalesObj.getSumGrupo() + prestaPrendaObj.getSumGrupo());
        double tPorcentajePonderacion = (totalPrecalif * 100) / totalSumGrupo;
        int tImperdonables = (areasComunesObj.getAux2() + bazObj.getAux2() + ektObj.getAux2() + generalesObj.getAux2()
                + prestaPrendaObj.getAux2());
        String logoTotal = getLogoExpansion(tPorcentajePonderacion, tImperdonables);

        String totalCalif = formatDecimales.format(totalCalif1);
        String totalResSi = formatEnteros.format(totalResSi1);
        String totalResNo = formatEnteros.format(totalResNo1);
        String totalSiPor = formatDecimales.format(totalSiPor1);
        String totalNoPor = formatDecimales.format(totalNoPor1);

        // Calificacion y Pre Calificacion
        double preCalificacionNum = (areasComunesObj.getPrecalif() + bazObj.getPrecalif() + ektObj.getPrecalif()
                + generalesObj.getPrecalif() + prestaPrendaObj.getPrecalif());
        String preCalificacion = formatDecimales.format(preCalificacionNum);
        String calificacion = "0.0";
        String colorCalificacion = "";
        String colorLetraCalificacion = "white";

        if (imperdonables.size() == 0) {
            calificacion = preCalificacion;
            colorLetraCalificacion = "black";
        }

        colorCalificacion = getColorExpansion(Double.parseDouble(calificacion));

        graficaTotal = grafica(2, Double.parseDouble(totalSiPor), Double.parseDouble(totalNoPor));

        // Calculo de porcentajes por Zona
        // Porcentaje en relación al total Comunes
        // Calificación
        double porcentajeComTotal = (comTotal * 100) / totalTotal;
        double comCalif1 = (comResSi1 * porcentajeComTotal) / comTotal;

        comTotal = Double.parseDouble(formatDecimales.format(porcentajeComTotal));
        String comCalifAnt = formatDecimales.format(comCalif1);

        // Double comPonderacion = (Double.parseDouble((areasComunesObj.getAux()!=null ?
        // !areasComunesObj.getAux().equals("") ? areasComunesObj.getAux(): "0":"0")));
        double comPonderacion = areasComunesObj.getSumGrupo();
        Double comPreCal = areasComunesObj.getPrecalif();
        String comCalif = formatDecimales.format(comPreCal);
        String comTotalPon = formatDecimales.format(comPonderacion);

        // Fin Porcentaje en relación al total Comunes
        // Porcentaje en relación al total BAZ
        double porcentajeBazTotal = (bazTotal * 100) / totalTotal;
        double bazCalif1 = (bazResSi1 * porcentajeBazTotal) / bazTotal;

        bazTotal = Double.parseDouble(formatDecimales.format(porcentajeBazTotal));
        String bazCalifAnt = formatDecimales.format(bazCalif1);

        // Double bazPonderacion = (Double.parseDouble((bazObj.getAux()!=null ?
        // !bazObj.getAux().equals("") ? bazObj.getAux(): "0":"0")));
        double bazPonderacion = bazObj.getSumGrupo();
        Double bazPreCal = bazObj.getPrecalif();
        String bazCalif = formatDecimales.format(bazPreCal);
        String bazTotalPon = formatDecimales.format(bazPonderacion);

        // Fin Porcentaje en relación al total BAZ
        // Porcentaje en relación al total EKT
        double porcentajeEktTotal = (ektTotal * 100) / totalTotal;
        double ektCalif1 = (ektResSi1 * porcentajeEktTotal) / ektTotal;

        ektTotal = Double.parseDouble(formatDecimales.format(porcentajeEktTotal));
        String ektCalifAnt = formatDecimales.format(ektCalif1);

        // Double ektPonderacion = (Double.parseDouble((ektObj.getAux()!=null ?
        // !ektObj.getAux().equals("") ? ektObj.getAux(): "0":"0")));
        double ektPonderacion = ektObj.getSumGrupo();
        Double ektPreCal = ektObj.getPrecalif();
        String ektCalif = formatDecimales.format(ektPreCal);
        String ektTotalPon = formatDecimales.format(ektPonderacion);
        // Fin Porcentaje en relación al total EKT

        // Porcentaje en relación al total Generales
        double porcentajeGeneralesTotal = (genTotal * 100) / totalTotal;
        double genCalif1 = (genResSi1 * porcentajeGeneralesTotal) / genTotal;

        genTotal = Double.parseDouble(formatDecimales.format(porcentajeGeneralesTotal));
        String genCalifAnt = formatDecimales.format(genCalif1);

        // Double generalesPonderacion =
        // (Double.parseDouble((generalesObj.getAux()!=null ?
        // !generalesObj.getAux().equals("") ? generalesObj.getAux(): "0":"0")));
        double generalesPonderacion = generalesObj.getSumGrupo();
        Double generalesPreCal = generalesObj.getPrecalif();
        String genCalif = formatDecimales.format(generalesPreCal);
        String genTotalPon = formatDecimales.format(generalesPonderacion);
        // Fin Porcentaje en relación al total Generales

        // Porcentaje en relación al total PP
        double porcentajePrestaTotal = (prestaTotal * 100) / totalTotal;
        double prestaCalif1 = (prestaResSi1 * porcentajePrestaTotal) / prestaTotal;

        prestaTotal = Double.parseDouble(formatDecimales.format(porcentajePrestaTotal));
        String prestaCalifAnt = formatDecimales.format(prestaCalif1);

        // Double prestaPrendaPonderacion =
        // (Double.parseDouble((prestaPrendaObj.getAux()!=null ?
        // !prestaPrendaObj.getAux().equals("") ? prestaPrendaObj.getAux(): "0":"0")));
        double prestaPrendaPonderacion = prestaPrendaObj.getSumGrupo();
        Double prestaPrendaPreCal = prestaPrendaObj.getPrecalif();
        String prestaCalif = formatDecimales.format(prestaPrendaPreCal);
        String prestaTotalPon = formatDecimales.format(prestaPrendaPonderacion);
        // Fin Porcentaje en relación al total PP
        // Fin Calculo de porcentaje por Zona

        // Items
        String totalItems = formatDecimales.format((areasComunesObj.getTotPreg() + bazObj.getTotPreg()
                + ektObj.getTotPreg() + generalesObj.getTotPreg() + prestaPrendaObj.getTotPreg()));
        String itemsPorRevisar = totalResNo;
        String itemsAprobados = (Integer.parseInt(totalItems) - Integer.parseInt(itemsPorRevisar)) + "";

        // totalTotal = (Double.parseDouble((areasComunesObj.getAux()!=null ?
        // !areasComunesObj.getAux().equals("") ? areasComunesObj.getAux(): "0":"0"))) +
        // (Double.parseDouble((bazObj.getAux()!=null ? !bazObj.getAux().equals("") ?
        // bazObj.getAux(): "0":"0")))+
        // (Double.parseDouble((ektObj.getAux()!=null ? !ektObj.getAux().equals("") ?
        // ektObj.getAux(): "0":"0"))) +
        // (Double.parseDouble((generalesObj.getAux()!=null ?
        // !generalesObj.getAux().equals("") ? generalesObj.getAux(): "0":"0"))) +
        // (Double.parseDouble((prestaPrendaObj.getAux()!=null ?
        // !prestaPrendaObj.getAux().equals("") ? prestaPrendaObj.getAux(): "0":"0"))) ;
        totalTotal = areasComunesObj.getSumGrupo() + bazObj.getSumGrupo() + ektObj.getSumGrupo()
                + generalesObj.getSumGrupo() + prestaPrendaObj.getSumGrupo();

        // Obtengo los responsables con bitácoraHermana
        List<EmpContacExtDTO> listaResponsables = new ArrayList<EmpContacExtDTO>();
        String responsables = "";
        String tablaResponsables = "";

        try {

            listaResponsables = empContacExtBI.obtieneDatos(idBitacora);
        } catch (Exception e) {
            logger.info("AP al obtener los responsables de la bitacora : " + idBitacora);
        }

        if (listaResponsables != null && listaResponsables.size() > 0) {

            for (int i = 0; i < listaResponsables.size(); i++) {

                responsables += "<tr>" + "<td style='border: 0 px solid gray; text-align:center;'> <b>"
                        + listaResponsables.get(i).getArea() + "</b></td>"
                        + "<td style='border: 0 px solid gray; text-align:center;'>"
                        + listaResponsables.get(i).getNombre() + "</td>"
                        + "<td style='border: 0 px solid gray; text-align:center;'>"
                        + listaResponsables.get(i).getTelefono() + "</td>"
                        + "<td style='border: 0 px solid gray; text-align:center;'>"
                        + listaResponsables.get(i).getCompania() + "</td>" + "</tr>";
            }

            tablaResponsables += "<div style='width:100%; height:75px; background-color:white;'></div>"
                    + "<div> <h3> Detalles de Responsables de Obra </h3></div>"
                    + "<table cellspacing='5' align='center' style='width:100%; height:400px; background-color:#F0EFF0; border: 0 px solid gray;'>"
                    + "<thead>" + "<tr>" + "<th style='border: 0 px solid gray; text-align:center;'><b>Área</b></th>"
                    + "<th style='border: 0 px solid gray; text-align:center;' ><b>Responsable</b></th>"
                    + "<th style='border: 0 px solid gray; text-align:center;' ><b>Teléfono</b></th>"
                    + "<th style='border: 0 px solid gray; text-align:center;' ><b>Organización</b></th>" + "</tr>"
                    + "</thead>"
                    + "<tbody>"
                    + responsables
                    + "</tbody>"
                    + "</table>";
        }

        // Aperturable
        double numeroDeCalificacion = (preCalificacionNum) / 100;
        int numImperdonablesRecorrido = imperdonables.size();
        String colorTexto = "white";

        if (numImperdonablesRecorrido > 0) {
            // Tiene imperdonables el color es NEGRO, NO OPERABLE Y SIN RECEPCIÓN, letra
            // blanca
            operable = "NO OPERABLE ";
            operable += "";
            colorOperable = "#000101";
            colorTexto = "white";
        } else {

            if (numeroDeCalificacion >= 100) {
                operable = "OPERABLE ";
                operable += " (Con recepción)";
                // VERDE
                colorOperable = "#00b240";
                colorTexto = "white";
            } else if (numeroDeCalificacion >= 70 && numeroDeCalificacion <= 99) {
                operable = "OPERABLE ";
                operable += " (Sin recepción)";
                // AMARILLO
                colorOperable = "#ECE90C";
                colorTexto = "black";
            } else if (numeroDeCalificacion >= 50 && numeroDeCalificacion <= 69) {
                operable = "NO OPERABLE ";
                operable += " ";
                // ROJO
                colorOperable = "#B40404";
                colorTexto = "black";
            } else if (numeroDeCalificacion <= 49) {
                operable = "NO OPERABLE ";
                operable += "";
                // NEGRO
                colorOperable = "#000101";
                colorTexto = "white";
            }
        }

        String html = "";

        // html = "<!DOCTYPE html>" +
        html = "<html>" + "<head>" + "<title>Acta Entrega</title>" + "</head>" + "<style>" + "div {color:black;}"
                + "table,td,th {font-size: 10px;}" + "a:link {color: white;}" + "a:visited {color: white;}"
                + "a:hover {color: white;}" + "a:active {color: white;}" + "td,th {border: 0.25px solid gray;}"
                + "#detalle {text-align:center;}" + "#mini {text-color:blue;}" + "#mini {font-size: 9px;}" + "</style>"
                + "<body>"
                + // Nuevo Formato
                // ABRE DIV LOGOS
                "<div> <h3>" + "<label>ENTREGA DE SUCURSAL</label>"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoElektra.png' width='36' height='36' />"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoBAZ.png' width='45' height='30'/>"
                + "</h3></div>"
                + // CIERRA DIV LOGOS
                "<TABLE cellspacing='5' style=' width:100%; height:340px; text-align:center;' >"
                + "<TR>"
                + "<TD ROWSPAN='3' style=' width:25%;'> "
                + "<div style='width:'200'; height:'150';>"
                + "<div bgcolor='red' style='padding-top:5px; width:100%; background-color:" + colorOperable + "; color:"
                + colorTexto + "; font-size: 13px;'> <h3>" + operable + " </h3></div>"
                + "<img src='" + imgFachada
                + "' width='190%' height='111%'/>"
                + "</div>"
                + "</TD>"
                + "<TD colspan='2' style='text-align:left; background-color:#F0EFF0;'>"
                + "<div style='float:left; width:50%; margin:auto; padding-left:5px; padding-top:10px; padding-bottom:10px; '>"
                + "<b>Sucursal:</b>" + sucursal + "<br></br> <b>Zona: </b>" + zona + "<br></br> <b>Territorio: </b>"
                + territorio
                + "</div>"
                + "<div style='float:left; width:100%; heigth:200%; padding-left:2px; padding-top:30px;'>"
                + "<b>No Económico:</b>" + economico + "<br> </br> <b>Tipo Visita: </b>" + tipoVisita
                + "<br></br> <b>Fecha: </b>" + fecha
                + "</div>"
                + "</TD> "
                + "<TD style='background-color:black;'> <a target='_blank' style='color:white;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancionFirmas.htm?idUsuario=189871&idBitacora="
                + idBitacora + "'>Ver <br></br> Participantes</a></TD>"
                + // "<TD style='text-align:left; font-size: 10px; background-color:#F0EFF0;'>
                // <b>No Económico:</b>"+ "10029988" +"<br></br> <b>Tipo Visita: </b>"+"Soft
                // Opening"+"<br></br> <b>Fecha: </b>"+ "29/10/2019" +" </TD> " +
                "</TR>"
                + // <b></b> <br></br>
                "<TR>"
                + "<TD style='text-align:center; font-size: 12px; background-color:#F0EFF0;'><H5>Generales de la recepción</H5> Pre Calificación  <strong style='text-align:center; font-size: 15px;'>"
                + formatDecimales.format(Double.parseDouble(preCalificacion)) + "</strong> </TD> "
                + "<TD style='background-color:" + colorCalificacion + "; color:" + colorLetraCalificacion
                + "'> Calificación: <strong style='text-align:center; font-size: 15px;'>"
                + formatDecimales.format(Double.parseDouble(calificacion)) + "</strong> </TD>"
                + "<TD style='background-color:black;'> <a target='_blank' style='color:white;' href='http://10.53.33.83/checklist/central/menuActa.htm?idUsuario=189871&idBitacora="
                + idBitacora + "'>Ver Detalles</a></TD>"
                + "</TR>"
                + "<TR>"
                + "<TD colspan='2' style='text-align:center; background-color:#F0EFF0;'> <p><b>Imperdonables:</b> <strong style='text-align:center; font-size: 15px;'>"
                + numImperdonables + "</strong></p> </TD>"
                + "<TD style='background-color:black;'> <a target='_blank' style='color:white;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancionImperdonable.htm?idUsuario=189871&idBitacora="
                + idBitacora + "'>Ver Detalles</a></TD>"
                + // http://10.53.33.83/checklist/central/detalleChecklistExpancionImperdonable.htm?idBitacora=26282
                "</TR>"
                + "</TABLE>"
                + // ITEMS
                "<TABLE style=' width:100%; height:35px; text-align:center'>" + "<TR>"
                + "<TD style='text-align:center; font-size: 12px; background-color:#F0EFF0;'> <strong style='text-align:center; font-size: 15px;'>"
                + totalItems + "</strong><br></br><br></br> Items Revisados </TD>"
                + "<TD style='text-align:center; font-size: 12px; background-color:#D8D8D8;'><strong style='text-align:center; font-size: 15px;'>"
                + itemsAprobados
                + "</strong><br></br> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='20' height='20'/>&nbsp; Items Aprobados </TD>"
                + "<TD style='text-align:center; font-size: 12px; background-color:#6B696E; color:white;'><strong style='text-align:center; font-size: 15px;'>"
                + itemsPorRevisar
                + "</strong><br></br> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='20' height='20'/>&nbsp; Items Por Revisar </TD>"
                + "</TR>"
                + // ITEMS
                "</TABLE>"
                + "<div id='detalle'> Detalles de la Entrega </div>"
                + // Div items
                "<TABLE style='width:100%; text-align:center' cellspacing='5'>"
                + " <TR>"
                + // INICIO EKT
                "<TD style='width:50%; height:202px; background-color:white;'> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaElektra.png' width='25' height='25'/>"
                + "&nbsp; Elektra"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoEkt + "' width='60' height='25'/>"
                + "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format((Double.parseDouble(ektTotalPon))) + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaElektra.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;'>items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + ektResSi + "</td>" + "<td>" + ektSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>"
                + formatDecimales.format((Double.parseDouble(ektCalif))) + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + ektResNo + "</td>" + "<td>" + ektNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>" + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=3'>Ver Detalles</a>"
                + " </div>"
                + "</TD> "
                + // FIN EKT
                // INICIO COMUNES
                " <TD style='width:50%; height:202px; '> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaAreasComunes.png' width='25' height=20'/>"
                + "&nbsp; Áreas Comunes"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoCom + "' width='60' height='25'/>"
                + "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format((Double.parseDouble(comTotalPon))) + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaComunes.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;' >items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + comResSi + "</td>" + "<td>" + comSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>"
                + formatDecimales.format((Double.parseDouble(comCalif))) + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + comResNo + "</td>" + "<td>" + comNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>"
                + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=1'>Ver Detalles</a>"
                + " </div>"
                + "</TD>"
                + // FIN COMUNES
                "</TR>"
                + "<TR>"
                + // INICIO BAZ
                "<TD style='width:50%; height:202px; background-color:white;'> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaBAZ.png' width='35' height='25'/>"
                + "&nbsp; Banco Azteca"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoBaz + "' width='60' height='25'/>"
                + "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format((Double.parseDouble(bazTotalPon))) + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaBanco.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;'>items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + bazResSi + "</td>" + "<td>" + bazSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>"
                + formatDecimales.format((Double.parseDouble(bazCalif))) + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + bazResNo + "</td>" + "<td>" + bazNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>"
                + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=2'>Ver Detalles</a>"
                + " </div>"
                + "</TD> "
                + // FIN BAZ
                // INICIO GENERALES
                "<TD style='width:50%; height:202px; background-color:white;'> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaGenerales.png' width='25' height='25'/>"
                + "&nbsp; Generales"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoGen + "' width='60' height='25'/>"
                + "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format((Double.parseDouble(genTotalPon))) + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaGenerales.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;'>items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + genResSi + "</td>" + "<td>" + genSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>"
                + formatDecimales.format((Double.parseDouble(genCalif))) + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + genResNo + "</td>" + "<td>" + genNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>"
                + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=4'>Ver Detalles</a>"
                + " </div>"
                + "</TD>"
                + // FIN GENERALES
                "</TR>"
                + "<TR>"
                + // INICIO PRESTA PRENDA
                "<TD style='width:50%; height:202px; background-color:white;'> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaPrestaPrenda.png' width='35' height='25'/>"
                + "&nbsp; Presta Prenda"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoPres + "' width='60' height='25'/>"
                + "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format((Double.parseDouble(prestaTotalPon))) + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaPresta.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;'>items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + prestaResSi + "</td>" + "<td>" + prestaSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>"
                + formatDecimales.format((Double.parseDouble(prestaCalif))) + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + prestaResNo + "</td>" + "<td>" + prestaNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>"
                + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=5'>Ver Detalles</a>"
                + " </div>"
                + "</TD> "
                // FIN PRESTA PRENDA

                // INICIO TOTAL
                + "<TD style='width:50%; height:202px; background-color:#F0EFF0;'> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "TOTAL"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoTotal + "' width='60' height='25'/>"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format(((totalTotal)))
                + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaTotal.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px; '>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:#F0EFF0; border: 0 px solid #F0EFF'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;'>items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:#F0EFF0; border: 0 px solid #F0EFF'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + totalResSi + "</td>" + "<td>" + totalSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>"
                + formatDecimales.format((Double.parseDouble(preCalificacion))) + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:#F0EFF0; border: 0 px solid #F0EFF'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + totalResNo + "</td>" + "<td>" + totalNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>"
                + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=6'>Ver Detalles</a>"
                + " </div>"
                + "</TD>"
                + // FIN TOTAL
                "</TR>"
                + "</TABLE>"
                + "<table cellspacing='0' align='right'>" + "<tr align='center'>"
                + "<td style=' border: 0.25px solid white;'>Código</td>"
                + "<td style=' border: 0.25px solid white; text-align:center; background-color:black; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>pésimo</td>"
                + "<td style='border: 0.25px solid white;  text-align:center; background-color:#FE003D; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>en peligro</td>"
                + "<td style='border: 0.25px solid white;  text-align:center; background-color:#F7CA44; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>alerta</td>"
                + "<td style='border: 0.25px solid white;  text-align:center; background-color:#006240; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>bien</td>"
                + "</tr>" + "</table>"
                + // Responsables de Obra
                tablaResponsables
                + // Fin Responables de Obra
                "</body>" + "</html>";

        return html;

    }

    // creaPDFExpansionNuevoFormatoConteo
    public String creaPDFExpansionNuevoFormatoConteo(String idBitacora, String tipoVisita, String fecha,
            String economico, String sucursal, String tipo, String territorio, String zona, List<String> imperdonables,
            List<FirmaCheckDTO> firmas, List<ReporteChecksExpDTO> incidencias, String aperturable, String imgFachada) {

        // Obtengo Files de Gráfica
        // Valores de PDF
        String operable = "";
        String colorOperable = "";

        if (tipoVisita.equals("0")) {
            tipoVisita = "Primer Recorrido";
        } else {
            tipoVisita = "Soft Openning";
        }

        // Imperdonables
        String numImperdonables = imperdonables.size() + "";

        DecimalFormat formatDecimales = new DecimalFormat("###.##");
        DecimalFormat formatEnteros = new DecimalFormat("###.##");

        // AQUÍ ES EL CAMBIO DE TODOS LOS NUEVOS VALORES QUE HAY QUE CONSIDERAR
        // Obtenemos Datos
        ChecklistPreguntasComBI checklistPreguntasComBI = (ChecklistPreguntasComBI) FRQAppContextProvider
                .getApplicationContext().getBean("checklistPreguntasComBI");

        // Obtiene La información anterior
        List<ChecklistPreguntasComDTO> listaGrupos = new ArrayList<ChecklistPreguntasComDTO>();

        try {
            listaGrupos = checklistPreguntasComBI.obtieneInfoCont(idBitacora);
        } catch (Exception e) {

            logger.info("AP al obtener al obtener obtieneInfoCont de bitacora: " + idBitacora);
        }

        // AREAS
        ChecklistPreguntasComDTO areasComunesObj = listaGrupos.get(0);
        // BAZ 1
        ChecklistPreguntasComDTO bazObj = listaGrupos.get(1);
        // EKT 2
        ChecklistPreguntasComDTO ektObj = listaGrupos.get(2);
        // GENERALES 3
        ChecklistPreguntasComDTO generalesObj = listaGrupos.get(3);
        // PP 4
        ChecklistPreguntasComDTO prestaPrendaObj = listaGrupos.get(4);

        List<ChecklistPreguntasComDTO> listaGruposExt = new ArrayList<ChecklistPreguntasComDTO>();

        try {
            listaGruposExt = checklistPreguntasComBI.obtieneInfoContExt(idBitacora);
        } catch (Exception e) {

            logger.info("AP al obtener al obtener obtieneInfoContExt de bitacora: " + idBitacora);
        }

        // Nuevo paquete dividido
        // AREAS
        ChecklistPreguntasComDTO areasComunesObjExt = listaGruposExt.get(0);
        double ponderacionComExt = areasComunesObjExt.getSumGrupo();
        double totalGeneralComunes = areasComunesObjExt.getTotGral();

        // BAZ 1
        ChecklistPreguntasComDTO bazObjExt = listaGruposExt.get(1);
        double ponderacionBazExt = bazObjExt.getSumGrupo();
        double totalGeneralBaz = bazObjExt.getTotGral();

        // EKT 2
        ChecklistPreguntasComDTO ektObjExt = listaGruposExt.get(2);
        double ponderacionEktExt = ektObjExt.getSumGrupo();
        double totalGeneralEkt = ektObjExt.getTotGral();

        // GENERALES 3
        ChecklistPreguntasComDTO generalesObjExt = listaGruposExt.get(3);
        double ponderacionGenExt = generalesObjExt.getSumGrupo();
        double totalGeneralGenerales = generalesObjExt.getTotGral();

        // PP 4
        ChecklistPreguntasComDTO prestaPrendaObjExt = listaGruposExt.get(4);
        double ponderacionPrestaExt = prestaPrendaObjExt.getSumGrupo();
        double totalGeneralPresta = prestaPrendaObjExt.getTotGral();

        List<ChecklistPreguntasComDTO> listaConteosHijas = new ArrayList<ChecklistPreguntasComDTO>();

        try {
            listaConteosHijas = checklistPreguntasComBI.obtieneInfoContHij(idBitacora);
        } catch (Exception e) {
            logger.info("AP al obtener al obtener obtieneInfoContHij de bitacora: " + idBitacora);
        }

        // AREAS
        ChecklistPreguntasComDTO conteoHijasComunes = listaConteosHijas.get(0);
        double comSiHijas = conteoHijasComunes.getPregSi();
        double comNaHijas = conteoHijasComunes.getPregNa();

        // BAZ 1
        ChecklistPreguntasComDTO conteoHiijasBaz = listaConteosHijas.get(1);
        double bazSiHijas = conteoHiijasBaz.getPregSi();
        double bazNaHijas = conteoHiijasBaz.getPregNa();

        // EKT 2
        ChecklistPreguntasComDTO conteoHiijasEkt = listaConteosHijas.get(2);
        double ektSiHijas = conteoHiijasEkt.getPregSi();
        double ektNaHijas = conteoHiijasEkt.getPregNa();

        // GENERALES 3
        ChecklistPreguntasComDTO conteoHiijasGenerales = listaConteosHijas.get(3);
        double generalesSiHijas = conteoHiijasGenerales.getPregSi();
        double generalesNaHijas = conteoHiijasGenerales.getPregNa();

        // PP 4
        ChecklistPreguntasComDTO conteoHiijasPrestaPrenda = listaConteosHijas.get(4);
        double prestaPrendaSiHijas = conteoHiijasPrestaPrenda.getPregSi();
        double prestaPrendaNaHijas = conteoHiijasPrestaPrenda.getPregNa();

        // 1.- GRÁFICA COMUNES ********
        // Cálculo Total de Preguntas
        // double comResNo1 = areasComunesObj.getPregNo();
        double comResNo1 = areasComunesObj.getPregNo();
        // NA
        double comResNa1 = areasComunesObj.getPregNa();
        //
        double comGrupoCompleto = areasComunesObj.getTotPreg();
        // double comResSi1 = areasComunesObj.getTotPreg() -
        // areasComunesObj.getPregNo();
        double comResSi1 = comSiHijas + (comGrupoCompleto - comResNa1) - comResNo1;

        // double comTotal = areasComunesObj.getTotPreg();
        double comTotal = comResSi1 + comResNo1 + comSiHijas;

        // PARA LOS NA FALTA SUMAR LO DESACTIVADO
        comResNa1 += (totalGeneralComunes - comTotal);

        double comSiPor1 = 0;
        double comNoPor1 = 0;

        if (comTotal != 0) {
            comSiPor1 = (comResSi1 / (comResSi1 + comResNo1)) * 100;
            comNoPor1 = (comResNo1 / (comResSi1 + comResNo1)) * 100;
        }

        double NAComunes = (totalGeneralComunes - (areasComunesObj.getPregNo() + areasComunesObj.getPregSi()));
        NAComunes = NAComunes - (comSiHijas);
        /*double NAComunes = 0;
         if (areasComunesObj.getPregSi() != 0 && areasComunesObj.getPregNo() != 0 ) {
         NAComunes = (totalGeneralComunes - (areasComunesObj.getPregNo() + areasComunesObj.getPregSi()));
         NAComunes = NAComunes - (comSiHijas);
         }*/

        // Calculo Por Ponderación
        // double comPorcentajePonderacion = (areasComunesObj.getPrecalif() * 100) /
        // areasComunesObj.getSumGrupo();
        double comPorcentajePonderacion = (ponderacionComExt > 0 ? ((areasComunesObj.getPrecalif() * 100) / ponderacionComExt) : 0.0);

        int comunesImperdonables = areasComunesObj.getAux2();
        String logoCom = getLogoExpansion(comPorcentajePonderacion, comunesImperdonables);

        // Cantidad Tabla (Si-No)
        if (areasComunesObj.getPregSi() == 0 && areasComunesObj.getPregNo() == 0 && comSiHijas == 0) {
            comResSi1 = 0;
            comResNo1 = 0;
            comSiPor1 = 0;
            comNoPor1 = 0;
        }

        String comResSi = formatEnteros.format(comResSi1);
        String comResNo = formatEnteros.format(comResNo1);

        // Porcentaje Tabla y Gráfica
        String comSiPor = formatDecimales.format(comSiPor1);
        String comNoPor = formatDecimales.format(comNoPor1);

        if (comResSi1 == 0 && comResNo1 == 0) {
            graficaComunes = grafica(1, 0, 100);
            logoCom = getLogoExpansion(100, 0);
        } else {
            graficaComunes = grafica(1, Double.parseDouble(comSiPor), Double.parseDouble(comNoPor));
        }

        // GRÁFICA COMUNES ********
        // 2.- GRÁFICA BANCO ********
        // double bazResNo1 = bazObj.getPregNo();
        double bazResNo1 = bazObj.getPregNo();
        // NA
        double bazResNa1 = bazObj.getPregNa();
        //
        double bazGrupoCompleto = bazObj.getTotPreg();
        // double bazResSi1 = bazObj.getTotPreg() - bazObj.getPregNo();
        double bazResSi1 = bazSiHijas + (bazGrupoCompleto - bazResNa1) - bazResNo1;

        // double bazTotal = bazObj.getTotPreg();
        double bazTotal = bazResSi1 + bazResNo1 + bazSiHijas;

        // PARA LOS NA FALTA SUMAR LO DESACTIVADO
        bazResNa1 += (totalGeneralBaz - bazTotal);

        double bazSiPor1 = 0;
        double bazNoPor1 = 0;

        if (bazTotal != 0) {
            bazSiPor1 = (bazResSi1 / (bazResSi1 + bazResNo1)) * 100;
            bazNoPor1 = (bazResNo1 / (bazResSi1 + bazResNo1)) * 100;
        }

        double NABaz = (totalGeneralBaz - (bazObj.getPregNo() + bazObj.getPregSi()));
        NABaz = NABaz - (bazSiHijas);
        /*double NABaz = 0.0;
         if (bazObj.getPregSi() != 0 && bazObj.getPregNo() != 0 ) {
         NABaz = (totalGeneralBaz - (bazObj.getPregNo() + bazObj.getPregSi()));
         NABaz = NABaz - (bazSiHijas);
         }*/
        // Calculo Por Ponderación
        // double bazPorcentajePonderacion = (bazObj.getPrecalif() * 100) /
        // bazObj.getSumGrupo();
        double bazPorcentajePonderacion = (ponderacionBazExt > 0 ? ((bazObj.getPrecalif() * 100) / ponderacionBazExt) : 0.0);

        int bazImperdonables = bazObj.getAux2();
        String logoBaz = getLogoExpansion(bazPorcentajePonderacion, bazImperdonables);

        // Cantidad Tabla (Si-No)
        if (bazObj.getPregSi() == 0 && bazObj.getPregNo() == 0 && bazSiHijas == 0) {
            bazResSi1 = 0;
            bazResNo1 = 0;
            bazSiPor1 = 0;
            bazNoPor1 = 0;
        }

        String bazResSi = formatEnteros.format(bazResSi1);
        String bazResNo = formatEnteros.format(bazResNo1);

        // Porcentaje Tabla y Gráfica
        String bazSiPor = formatDecimales.format(bazSiPor1);
        String bazNoPor = formatDecimales.format(bazNoPor1);

        if (bazResSi1 == 0 && bazResNo1 == 0) {
            graficaBanco = grafica(1, 0, 100);
            logoBaz = getLogoExpansion(100, 0);

        } else {
            graficaBanco = grafica(1, Double.parseDouble(bazSiPor), Double.parseDouble(bazNoPor));
        }

        // GRÁFICA BANCO ********
        // 3.- GRÁFICA ELEKTRA ********
        // double ektResNo1 = ektObj.getPregNo();
        double ektResNo1 = ektObj.getPregNo();
        // NA
        double ektResNa1 = ektObj.getPregNa();
        // TOTAL DE GRUPO COMPLETO
        double ektGrupoCompleto = ektObj.getTotPreg();
        // double ektResSi1 = ektObj.getTotPreg() - ektObj.getPregNo();
        double ektResSi1 = ektSiHijas + (ektGrupoCompleto - ektResNa1) - ektResNo1;

        // double ektTotal = ektObj.getTotPreg();
        double ektTotal = ektResSi1 + ektResNo1 + ektSiHijas;

        // PARA LOS NA FALTA SUMAR LO DESACTIVADO
        ektResNa1 += (totalGeneralEkt - ektTotal);

        double ektSiPor1 = 0;
        double ektNoPor1 = 0;

        if (ektTotal != 0) {
            ektSiPor1 = (ektResSi1 / (ektResSi1 + ektResNo1)) * 100;
            ektNoPor1 = (ektResNo1 / (ektResSi1 + ektResNo1)) * 100;
        }

        double NAEkt = (totalGeneralEkt - (ektObj.getPregNo() + ektObj.getPregSi()));
        NAEkt = NAEkt - (ektSiHijas);
        /*double NAEkt = 0;
         if (ektObj.getPregSi() != 0 && ektObj.getPregNo() != 0 ) {
         NAEkt = (totalGeneralEkt - (ektObj.getPregNo() + ektObj.getPregSi()));
         NAEkt = NAEkt - (ektSiHijas);
         }*/

        // Calculo Por Ponderación
        // double ektPorcentajePonderacion = (ektObj.getPrecalif() * 100) /
        // ektObj.getSumGrupo();
        double ektPorcentajePonderacion = (ponderacionEktExt > 0 ? ((ektObj.getPrecalif() * 100) / ponderacionEktExt) : 0.0);

        int ektImperdonables = ektObj.getAux2();
        String logoEkt = getLogoExpansion(ektPorcentajePonderacion, ektImperdonables);

        // Cantidad Tabla (Si-No)
        if (ektObj.getPregSi() == 0 && ektObj.getPregNo() == 0 && ektSiHijas == 0) {
            ektResSi1 = 0;
            ektResNo1 = 0;
            ektSiPor1 = 0;
            ektNoPor1 = 0;
        }

        String ektResSi = formatEnteros.format(ektResSi1);
        String ektResNo = formatEnteros.format(ektResNo1);

        // Porcentaje Tabla y Gráfica
        String ektSiPor = formatDecimales.format(ektSiPor1);
        String ektNoPor = formatDecimales.format(ektNoPor1);

        if (ektResSi1 == 0 && ektResNo1 == 0) {
            graficaElektra = grafica(1, 0, 100);
            logoEkt = getLogoExpansion(100, 0);

        } else {
            graficaElektra = grafica(1, Double.parseDouble(ektSiPor), Double.parseDouble(ektNoPor));
        }

        // GRÁFICA ELEKTRA ********
        // 4.- GRÁFICA GENERALES ********
        // double genResNo1 = generalesObj.getPregNo();
        double genResNo1 = generalesObj.getPregNo();
        // NA
        double genResNa1 = generalesObj.getPregNa();
        //
        double genGrupoCompleto = generalesObj.getTotPreg();

        // double genResSi1 = generalesObj.getTotPreg() - generalesObj.getPregNo();
        double genResSi1 = generalesSiHijas + (genGrupoCompleto - genResNa1) - genResNo1;

        // double genTotal = generalesObj.getTotPreg();
        double genTotal = genResSi1 + genResNo1 + generalesSiHijas;

        // PARA LOS NA FALTA SUMAR LO DESACTIVADO
        genResNa1 += (totalGeneralGenerales - genTotal);

        double genSiPor1 = 0;
        double genNoPor1 = 0;

        if (genTotal != 0) {
            genSiPor1 = (genResSi1 / (genResSi1 + genResNo1)) * 100;
            genNoPor1 = (genResNo1 / (genResSi1 + genResNo1)) * 100;
        }

        double NAGenerales = (totalGeneralGenerales - (generalesObj.getPregNo() + generalesObj.getPregSi()));
        NAGenerales = NAGenerales - (generalesSiHijas);
        /*double NAGenerales = 0.0;
         if (generalesObj.getPregSi() != 0 && generalesObj.getPregNo() != 0 ) {
         NAGenerales = (totalGeneralBaz - (generalesObj.getPregNo() + generalesObj.getPregSi()));
         NAGenerales = NAGenerales - (generalesSiHijas);
         }*/

        // Calculo Por Ponderación
        // double generalesPorcentajePonderacion = (generalesObj.getPrecalif() * 100) /
        // generalesObj.getSumGrupo();
        double generalesPorcentajePonderacion = (ponderacionGenExt > 0 ? ((generalesObj.getPrecalif() * 100) / ponderacionGenExt) : 0.0);

        int generalesImperdonables = generalesObj.getAux2();
        String logoGen = getLogoExpansion(generalesPorcentajePonderacion, generalesImperdonables);

        // Cantidad Tabla (Si-No)
        if (generalesObj.getPregSi() == 0 && generalesObj.getPregNo() == 0 && generalesSiHijas == 0) {
            genResSi1 = 0;
            genResNo1 = 0;
            genSiPor1 = 0;
            genNoPor1 = 0;
        }

        String genResSi = formatEnteros.format(genResSi1);
        String genResNo = formatEnteros.format(genResNo1);

        // Porcentaje Tabla y Gráfica
        String genSiPor = formatDecimales.format(genSiPor1);
        String genNoPor = formatDecimales.format(genNoPor1);

        if (genResSi1 == 0 && genResNo1 == 0) {
            graficaGenerales = grafica(1, 0, 100);
            logoGen = getLogoExpansion(100, 0);

        } else {
            graficaGenerales = grafica(1, Double.parseDouble(genSiPor), Double.parseDouble(genNoPor));
        }

        // GRÁFICA GENERALES ********
        // 5.- GRÁFICA PRESTA PRENDA *******
        // double prestaResNo1 = prestaPrendaObj.getPregNo();
        double prestaResNo1 = prestaPrendaObj.getPregNo();
        // NA
        double prestaResNa1 = prestaPrendaObj.getPregNa();
        //
        double prestaGrupoCompleto = prestaPrendaObj.getTotPreg();
        // double prestaResSi1 = prestaPrendaObj.getTotPreg() -
        // prestaPrendaObj.getPregNo();
        double prestaResSi1 = prestaPrendaSiHijas + (prestaGrupoCompleto - prestaResNa1) - prestaResNo1;

        // double prestaTotal = prestaPrendaObj.getTotPreg();
        double prestaTotal = prestaResSi1 + prestaResNo1 + prestaPrendaSiHijas;

        // PARA LOS NA FALTA SUMAR LO DESACTIVADO
        prestaResNa1 += (totalGeneralPresta - prestaTotal);

        double prestaSiPor1 = 0;
        double prestaNoPor1 = 0;

        if (prestaTotal != 0) {
            prestaSiPor1 = (prestaResSi1 / (prestaResSi1 + prestaResNo1)) * 100;
            prestaNoPor1 = (prestaResNo1 / (prestaResSi1 + prestaResNo1)) * 100;
        }

        double NAPresta = (totalGeneralPresta - (prestaPrendaObj.getPregNo() + prestaPrendaObj.getPregSi()));
        NAPresta = NAPresta - (prestaPrendaSiHijas);
        /*double NAPresta = 0.0;
         if (prestaPrendaObj.getPregSi() != 0 && prestaPrendaObj.getPregNo() != 0 ) {
         NAPresta = (totalGeneralPresta - (prestaPrendaObj.getPregNo() + prestaPrendaObj.getPregSi()));
         NAPresta = NAPresta - (prestaPrendaSiHijas);
         }*/

        // Calculo Por Ponderación
        // double prestaPorcentajePonderacion = (prestaPrendaObj.getPrecalif() * 100) /
        // prestaPrendaObj.getSumGrupo();
        double prestaPorcentajePonderacion = (ponderacionPrestaExt > 0 ? ((prestaPrendaObj.getPrecalif() * 100) / ponderacionPrestaExt) : 0.0);

        int prestaImperdonables = prestaPrendaObj.getAux2();
        String logoPres = getLogoExpansion(prestaPorcentajePonderacion, prestaImperdonables);

        // Cantidad Tabla (Si-No)
        if (prestaPrendaObj.getPregSi() == 0 && prestaPrendaObj.getPregNo() == 0 && prestaPrendaSiHijas == 0) {
            prestaResSi1 = 0;
            prestaResNo1 = 0;
            prestaSiPor1 = 0;
            prestaNoPor1 = 0;
        }

        String prestaResSi = formatEnteros.format(prestaResSi1);
        String prestaResNo = formatEnteros.format(prestaResNo1);

        // Porcentaje Tabla y Gráfica
        String prestaSiPor = formatDecimales.format(prestaSiPor1);
        String prestaNoPor = formatDecimales.format(prestaNoPor1);

        if (prestaResSi1 == 0 && prestaResNo1 == 0) {
            graficaPresta = grafica(1, 0, 100);
            logoPres = getLogoExpansion(100, 0);
        } else {
            graficaPresta = grafica(1, Double.parseDouble(prestaSiPor), Double.parseDouble(prestaNoPor));
        }

        // GRÁFICA PRESTA PRENDA ********
        // 6.- VALORES TOTAL ********
        // double totalResSi1 = (totalTotal - totalResNo1);
        double totalResNo1 = (areasComunesObj.getPregNo() + bazObj.getPregNo() + ektObj.getPregNo()
                + generalesObj.getPregNo() + prestaPrendaObj.getPregNo());
        // NA
        double totalResNa1 = (areasComunesObj.getPregNa() + bazObj.getPregNa() + ektObj.getPregNa()
                + generalesObj.getPregNa() + prestaPrendaObj.getPregNa());

        double totalResSi1 = (comResSi1 + bazResSi1 + ektResSi1 + genResSi1 + prestaResSi1);

        double totalTotal = (totalResNo1 + totalResSi1);

        double totalSiPor1 = 0;
        double totalNoPor1 = 0;

        if (totalTotal != 0) {
            totalSiPor1 = (totalResSi1 / totalTotal) * 100;
            totalNoPor1 = (totalResNo1 / totalTotal) * 100;
        }

        // Calculo Por Ponderación
        // double totalPrecalif = (areasComunesObj.getPrecalif() + bazObj.getPrecalif()
        // + ektObj.getPrecalif() + generalesObj.getPrecalif() +
        // prestaPrendaObj.getPrecalif());
        double totalPrecalif = (areasComunesObj.getPonTot() + bazObj.getPonTot() + ektObj.getPonTot()
                + generalesObj.getPonTot() + prestaPrendaObj.getPonTot());
        double totalSumGrupo = (ponderacionComExt + ponderacionBazExt + ponderacionEktExt + ponderacionGenExt
                + ponderacionPrestaExt);
        double tPorcentajePonderacion = (totalPrecalif * 100) / totalSumGrupo;

        int tImperdonables = (areasComunesObj.getAux2() + bazObj.getAux2() + ektObj.getAux2() + generalesObj.getAux2()
                + prestaPrendaObj.getAux2());
        String logoTotal = getLogoExpansion(tPorcentajePonderacion, tImperdonables);

        // Cantidad Tabla (Si-No)
        String totalResSi = formatEnteros.format(totalResSi1);
        String totalResNo = formatEnteros.format(totalResNo1);

        // Porcentaje Tabla y Gráfica
        String totalSiPor = formatDecimales.format(totalSiPor1);
        String totalNoPor = formatDecimales.format(totalNoPor1);

        // ********* Calificación *********
        // double totalCalif1 = (totalSiPor1 * totalValorTotal) / 100;
        double totalCalif1 = (totalTotal > 0 ? ((totalSiPor1 * 100) / totalTotal) : 0.0);
        String totalCalif = formatDecimales.format(totalCalif1);

        // ********* Calificacion y Pre Calificacion *********
        // Calculo de porcentajes por Zona
        // Porcentaje en relación al total Comunes
        // Calificación
        double porcentajeComTotal = (totalTotal > 0 ? ((comTotal * 100) / totalTotal) : 0.0);
        double comCalif1 = (comTotal > 0 ? ((comResSi1 * porcentajeComTotal) / comTotal) : 0.0);
        comTotal = Double.parseDouble(formatDecimales.format(porcentajeComTotal));

        String comCalifAnt = formatDecimales.format(comCalif1);
        // Double comPonderacion = (Double.parseDouble((areasComunesObj.getAux()!=null ?
        // !areasComunesObj.getAux().equals("") ? areasComunesObj.getAux(): "0":"0")));

        // Cuanto vale la zona
        double comPonderacion = ponderacionComExt;
        // Calcular ponderación pondTotal (ponderacion SI)
        Double comPreCal = areasComunesObj.getPrecalif();

        String comCalif = formatDecimales.format(comPreCal);

        if (comResSi1 == 0 && comResNo1 == 0) {
            comGrupoCompleto = 0;
            comPonderacion = 0;
        }
        String comTotalPon = formatDecimales.format(comPonderacion);
        String comCalifCant = formatDecimales.format(comPonderacion > 0 ? (((comPreCal / comPonderacion) * 100)) : 0.0);
        // Fin Porcentaje en relación al total Comunes

        // Porcentaje en relación al total BAZ
        double porcentajeBazTotal = (totalTotal > 0 ? ((bazTotal * 100) / totalTotal) : 0.0);
        double bazCalif1 = (bazTotal > 0 ? ((bazResSi1 * porcentajeBazTotal) / bazTotal) : 0.0);
        bazTotal = Double.parseDouble(formatDecimales.format(porcentajeBazTotal));

        String bazCalifAnt = formatDecimales.format(bazCalif1);
        // Double bazPonderacion = (Double.parseDouble((bazObj.getAux()!=null ?
        // !bazObj.getAux().equals("") ? bazObj.getAux(): "0":"0")));

        double bazPonderacion = ponderacionBazExt;
        // Calcular ponderación pondTotal (ponderacion SI)
        Double bazPreCal = bazObj.getPrecalif();

        String bazCalif = formatDecimales.format(bazPreCal);

        if (bazResSi1 == 0 && bazResNo1 == 0) {
            bazGrupoCompleto = 0;
            bazPonderacion = 0;
        }

        String bazTotalPon = formatDecimales.format(bazPonderacion);
        String bazCalifCant = formatDecimales.format(bazPonderacion > 0 ? (((bazPreCal / bazPonderacion) * 100)) : 0.0);

        // Fin Porcentaje en relación al total BAZ
        // Porcentaje en relación al total EKT
        double porcentajeEktTotal = (totalTotal > 0 ? ((ektResSi1 * 100) / totalTotal) : 0.0);
        double ektCalif1 = (ektTotal > 0 ? ((ektResSi1 * porcentajeEktTotal) / ektTotal) : 0.0);
        ektTotal = Double.parseDouble(formatDecimales.format(porcentajeEktTotal));

        String ektCalifAnt = formatDecimales.format(ektCalif1);
        // Double ektPonderacion = (Double.parseDouble((ektObj.getAux()!=null ?
        // !ektObj.getAux().equals("") ? ektObj.getAux(): "0":"0")));

        double ektPonderacion = ponderacionEktExt;
        // Calcular ponderación pondTotal (ponderacion SI)
        Double ektPreCal = ektObj.getPrecalif();

        String ektCalif = formatDecimales.format(ektPreCal);

        if (ektResSi1 == 0 && ektResNo1 == 0) {
            ektGrupoCompleto = 0;
            ektPonderacion = 0;
        }

        String ektTotalPon = formatDecimales.format(ektPonderacion);
        String ektCalifCant = formatDecimales.format(ektPonderacion > 0 ? (((ektPreCal / ektPonderacion) * 100)) : 0.0);

        // Fin Porcentaje en relación al total EKT
        // Porcentaje en relación al total Generales
        double porcentajeGeneralesTotal = (totalTotal > 0 ? ((genTotal * 100) / totalTotal) : 0.0);
        double genCalif1 = (genTotal > 0 ? ((genResSi1 * porcentajeGeneralesTotal) / genTotal) : 0.0);
        genTotal = Double.parseDouble(formatDecimales.format(porcentajeGeneralesTotal));

        String genCalifAnt = formatDecimales.format(genCalif1);
        // Double generalesPonderacion =
        // (Double.parseDouble((generalesObj.getAux()!=null ?
        // !generalesObj.getAux().equals("") ? generalesObj.getAux(): "0":"0")));

        double generalesPonderacion = ponderacionGenExt;
        // Calcular ponderación pondTotal (ponderacion SI)
        Double generalesPreCal = generalesObj.getPrecalif();

        String genCalif = formatDecimales.format(generalesPreCal);

        if (genResSi1 == 0 && genResNo1 == 0) {
            genGrupoCompleto = 0;
            generalesPonderacion = 0;
        }

        String genTotalPon = formatDecimales.format(generalesPonderacion);
        String genCalifCant = formatDecimales.format(generalesPonderacion > 0 ? (((generalesPreCal / generalesPonderacion) * 100)) : 0.0);

        // Fin Porcentaje en relación al total Generales
        // Porcentaje en relación al total PP
        double porcentajePrestaTotal = (totalTotal > 0 ? ((prestaTotal * 100) / totalTotal) : 0.0);
        double prestaCalif1 = (prestaTotal > 0 ? ((prestaResSi1 * porcentajePrestaTotal) / prestaTotal) : 0.0);
        prestaTotal = Double.parseDouble(formatDecimales.format(porcentajePrestaTotal));

        String prestaCalifAnt = formatDecimales.format(prestaCalif1);
        // Double prestaPrendaPonderacion =
        // (Double.parseDouble((prestaPrendaObj.getAux()!=null ?
        // !prestaPrendaObj.getAux().equals("") ? prestaPrendaObj.getAux(): "0":"0")));

        double prestaPrendaPonderacion = ponderacionPrestaExt;
        // Calcular ponderación pondTotal (ponderacion SI)
        Double prestaPrendaPreCal = prestaPrendaObj.getPrecalif();

        String prestaCalif = formatDecimales.format(prestaPrendaPreCal);

        if (prestaResSi1 == 0 && prestaResNo1 == 0) {
            prestaGrupoCompleto = 0;
            prestaPrendaPonderacion = 0;
        }

        String prestaTotalPon = formatDecimales.format(prestaPrendaPonderacion);
        String prestaCalifCant = formatDecimales.format(prestaPrendaPonderacion > 0 ? (((prestaPrendaPreCal / prestaPrendaPonderacion) * 100)) : 0.0);

        // Fin Porcentaje en relación al total PP
        // Fin Calculo de porcentaje por Zona
        // Items
        String totalItems = formatDecimales.format((comGrupoCompleto - areasComunesObj.getPregNa() + comSiHijas)
                + (bazGrupoCompleto - bazObj.getPregNa() + bazSiHijas)
                + (ektGrupoCompleto - ektObj.getPregNa() + ektSiHijas)
                + (prestaGrupoCompleto - prestaPrendaObj.getPregNa() + prestaPrendaSiHijas)
                + (genGrupoCompleto - generalesObj.getPregNa() + generalesSiHijas));
        // String totalItems = formatDecimales.format(comTotal + bazTotal + ektTotal +
        // prestaTotal + genTotal);
        String itemsPorRevisar = formatDecimales.format(comResNo1 + bazResNo1 + ektResNo1 + prestaResNo1 + genResNo1);

        double itemAprobadosResta = ((((comGrupoCompleto - areasComunesObj.getPregNa()) - comResNo1) + comSiHijas)
                + (((bazGrupoCompleto - bazObj.getPregNa()) - bazResNo1) + bazSiHijas)
                + (((ektGrupoCompleto - ektObj.getPregNa()) - ektResNo1) + ektSiHijas)
                + (((prestaGrupoCompleto - prestaPrendaObj.getPregNa()) - prestaResNo1) + prestaPrendaSiHijas)
                + (((genGrupoCompleto - generalesObj.getPregNa()) - genResNo1) + generalesSiHijas));
        String itemsAprobados = formatDecimales.format(itemAprobadosResta);

        //String itemsNoConsiderados = formatDecimales.format(NAComunes + NABaz + NAEkt + NAPresta + NAGenerales);
        double sumaItemsRevisados = (comGrupoCompleto - areasComunesObj.getPregNa() + comSiHijas)
                + (bazGrupoCompleto - bazObj.getPregNa() + bazSiHijas)
                + (ektGrupoCompleto - ektObj.getPregNa() + ektSiHijas)
                + (prestaGrupoCompleto - prestaPrendaObj.getPregNa() + prestaPrendaSiHijas)
                + (genGrupoCompleto - generalesObj.getPregNa() + generalesSiHijas);

        String itemsNoConsiderados = formatDecimales.format(943 - (sumaItemsRevisados));

        // totalTotal = (Double.parseDouble((areasComunesObj.getAux()!=null ?
        // !areasComunesObj.getAux().equals("") ? areasComunesObj.getAux(): "0":"0"))) +
        // (Double.parseDouble((bazObj.getAux()!=null ? !bazObj.getAux().equals("") ?
        // bazObj.getAux(): "0":"0")))+
        // (Double.parseDouble((ektObj.getAux()!=null ? !ektObj.getAux().equals("") ?
        // ektObj.getAux(): "0":"0"))) +
        // (Double.parseDouble((generalesObj.getAux()!=null ?
        // !generalesObj.getAux().equals("") ? generalesObj.getAux(): "0":"0"))) +
        // (Double.parseDouble((prestaPrendaObj.getAux()!=null ?
        // !prestaPrendaObj.getAux().equals("") ? prestaPrendaObj.getAux(): "0":"0"))) ;
        totalTotal = comPonderacion + bazPonderacion + ektPonderacion + generalesPonderacion + prestaPrendaPonderacion;

        // --- PONDERACIÓNES ---
        double ponderacionTotalObtenida = areasComunesObj.getPrecalif() + bazObj.getPrecalif() + ektObj.getPrecalif()
                + generalesObj.getPrecalif() + prestaPrendaObj.getPrecalif();//
        double ponderacionTotalGrupos = comPonderacion + bazPonderacion + ektPonderacion + generalesPonderacion
                + prestaPrendaPonderacion;

        double ponderacionObtenida = (ponderacionTotalGrupos > 0 ? (((ponderacionTotalObtenida) * 100) / (ponderacionTotalGrupos)) : 0.0);
        double ponderacionTotal = (ponderacionTotalObtenida);
        String totalCalifCant = formatDecimales.format((totalTotal > 0 ? (((ponderacionTotal / totalTotal) * 100)) : 0.0));

        double preCalificacionNum = ponderacionObtenida;
        String preCalificacion = formatDecimales.format(preCalificacionNum);
        String calificacion = "0.0";
        String colorCalificacion = "";
        String colorLetraCalificacion = "white";

        if (imperdonables.size() == 0) {
            calificacion = preCalificacion;
            colorLetraCalificacion = "black";
        }

        colorCalificacion = getColorExpansion(Double.parseDouble(calificacion));

        if (colorCalificacion.contains("#000000")) {
            colorLetraCalificacion = "white";
        }

        graficaTotal = grafica(2, Double.parseDouble(totalSiPor), Double.parseDouble(totalNoPor));
        // VALORES TOTAL ********

        // Obtengo los responsables con bitácoraHermana
        List<EmpContacExtDTO> listaResponsables = new ArrayList<EmpContacExtDTO>();
        String responsables = "";
        String tablaResponsables = "";

        try {
            listaResponsables = empContacExtBI.obtieneDatos(idBitacora);
        } catch (Exception e) {
            logger.info("AP al obtener los responsables de la bitacora : " + idBitacora);
        }

        if (listaResponsables != null && listaResponsables.size() > 0) {

            for (int i = 0; i < listaResponsables.size(); i++) {

                responsables += "<tr>" + "<td style='border: 0 px solid gray; text-align:center;'> <b>"
                        + listaResponsables.get(i).getArea() + "</b></td>"
                        + "<td style='border: 0 px solid gray; text-align:center;'>"
                        + listaResponsables.get(i).getNombre() + "</td>"
                        + "<td style='border: 0 px solid gray; text-align:center;'>"
                        + listaResponsables.get(i).getTelefono() + "</td>"
                        + "<td style='border: 0 px solid gray; text-align:center;'>"
                        + listaResponsables.get(i).getCompania() + "</td>" + "</tr>";
            }

            tablaResponsables += "<div style='width:100%; height:75px; background-color:white;'></div>"
                    + "<div> <h3> Detalles de Responsables de Obra </h3></div>"
                    + "<table cellspacing='5' align='center' style='width:100%; height:400px; background-color:#F0EFF0; border: 0 px solid gray;'>"
                    + "<thead>" + "<tr>" + "<th style='border: 0 px solid gray; text-align:center;'><b>Área</b></th>"
                    + "<th style='border: 0 px solid gray; text-align:center;' ><b>Responsable</b></th>"
                    + "<th style='border: 0 px solid gray; text-align:center;' ><b>Teléfono</b></th>"
                    + "<th style='border: 0 px solid gray; text-align:center;' ><b>Organización</b></th>" + "</tr>"
                    + "</thead>"
                    + "<tbody>"
                    + responsables
                    + "</tbody>"
                    + "</table>";
        }

        // Aperturable
        double numeroDeCalificacion = (preCalificacionNum);
        int numImperdonablesRecorrido = imperdonables.size();
        String colorTexto = "white";

        if (numImperdonablesRecorrido > 0) {
            // Tiene imperdonables el color es NEGRO, NO OPERABLE Y SIN RECEPCIÓN, letra
            // blanca
            operable = "NO OPERABLE ";
            operable += "";
            colorOperable = "#000101";
            colorTexto = "white";
        } else {

            if (numeroDeCalificacion >= 100) {
                operable = "OPERABLE ";
                operable += " (Con recepción)";
                // VERDE
                colorOperable = "#00b240";
                colorTexto = "white";
            } else if (numeroDeCalificacion >= 70 && numeroDeCalificacion <= 99) {
                operable = "OPERABLE ";
                operable += " (Sin recepción)";
                // AMARILLO
                colorOperable = "#ECE90C";
                colorTexto = "black";
            } else if (numeroDeCalificacion >= 50 && numeroDeCalificacion <= 69) {
                operable = "NO OPERABLE ";
                operable += " ";
                // ROJO
                colorOperable = "#B40404";
                colorTexto = "black";
            } else if (numeroDeCalificacion <= 49) {
                operable = "NO OPERABLE ";
                operable += "";
                // NEGRO
                colorOperable = "#000101";
                colorTexto = "white";
            }
        }

        String cadenaDesactivados = "";
        String sucursales = "";
        /*
         * for (ChecklistPreguntasComDTO obj :
         * checklistPreguntasComBI.obtieneInfo(idBitacora)) {
         *
         * if ( obj.getPregSi() == 0 && obj.getPregNo() == 0 && obj.getPregNa() == 0 ) {
         *
         * sucursales += obj.getChecklist() + ", ";
         *
         * } }
         *
         * if (sucursales.length() > 0) {
         *
         * cadenaDesactivados =
         * "Los siguientes módulos no se consideran en el recorrido: " +
         * sucursales.substring(0,sucursales.length() - 2); ; }
         */

        String html = "";

        // html = "<!DOCTYPE html>" +
        html = "<html>" + "<head>" + "<title>Acta Entrega</title>" + "</head>" + "<style>" + "div {color:black;}"
                + "table,td,th {font-size: 10px;}" + "a:link {color: white;}" + "a:visited {color: white;}"
                + "a:hover {color: white;}" + "a:active {color: white;}" + "td,th {border: 0.25px solid gray;}"
                + "#detalle {text-align:center;}" + "#mini {text-color:blue;}" + "#mini {font-size: 9px;}" + "</style>"
                + "<body>"
                + // Nuevo Formato
                // ABRE DIV LOGOS
                "<div> <h3>" + "<label>ENTREGA DE SUCURSAL</label>"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoElektra.png' width='36' height='36' />"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoBAZ.png' width='45' height='30'/>"
                + "</h3></div>"
                + // CIERRA DIV LOGOS
                "<TABLE cellspacing='5' style=' width:100%; height:340px; text-align:center;' >"
                + "<TR>"
                + "<TD ROWSPAN='3' style=' width:25%;'> "
                + "<div style='width:'200'; height:'150';>"
                + "<div bgcolor='red' style='padding-top:5px; width:100%; background-color:" + colorOperable + "; color:"
                + colorTexto + "; font-size: 13px;'> <h3>" + operable + " </h3></div>"
                + "<img src='" + imgFachada
                + "' width='190%' height='111%'/>"
                + "</div>"
                + "</TD>"
                + "<TD colspan='2' style='text-align:left; background-color:#F0EFF0;'>"
                + "<div style='float:left; width:50%; margin:auto; padding-left:5px; padding-top:10px; padding-bottom:10px; '>"
                + "<b>Sucursal:</b>" + sucursal + "<br></br> <b>Zona: </b>" + zona + "<br></br> <b>Territorio: </b>"
                + territorio
                + "</div>"
                + "<div style='float:left; width:100%; heigth:200%; padding-left:2px; padding-top:30px;'>"
                + "<b>No Económico:</b>" + economico + "<br> </br> <b>Tipo Visita: </b>" + tipoVisita
                + "<br></br> <b>Fecha: </b>" + fecha
                + "</div>"
                + "</TD> "
                + "<TD style='background-color:black;'> <a target='_blank' style='color:white;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancionFirmas.htm?idUsuario=189871&idBitacora="
                + idBitacora + "'>Ver <br></br> Participantes</a></TD>"
                + // "<TD style='text-align:left; font-size: 10px; background-color:#F0EFF0;'>
                // <b>No Económico:</b>"+ "10029988" +"<br></br> <b>Tipo Visita: </b>"+"Soft
                // Opening"+"<br></br> <b>Fecha: </b>"+ "29/10/2019" +" </TD> " +
                "</TR>"
                + // <b></b> <br></br>
                "<TR>"
                + "<TD style='text-align:center; font-size: 12px; height:50px;'><strong style='text-align:center; font-size: 10px;'>Generales de la recepción</strong></TD> "
                + "<TD style='text-align:center; font-size: 12px; height:50px;'> Calificación <strong style='text-align:center; font-size: 15px;'>"
                + formatDecimales.format(Double.parseDouble(preCalificacion)) + "</strong> </TD> "
                + /*+ "<TD style='background-color:" + colorCalificacion + "; color:" + colorLetraCalificacion
                 + "'> Calificación: <strong style='text-align:center; font-size: 15px;'>"
                 + formatDecimales.format(Double.parseDouble(calificacion)) + "</strong> </TD>" +*/ "<TD style='background-color:black; height:50px;'> <a target='_blank' style='color:white;' href='http://10.53.33.83/checklist/central/menuActa.htm?idUsuario=189871&idBitacora="
                + idBitacora + "'>Ver Detalles</a></TD>"
                + "</TR>"
                + "<TR>"
                + "<TD colspan='2' style='text-align:center; background-color:#F0EFF0;'> <p><b>Imperdonables:</b> <strong style='text-align:center; font-size: 15px;'>"
                + numImperdonables + "</strong></p> </TD>"
                + "<TD style='background-color:black;'> <a target='_blank' style='color:white;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancionImperdonable.htm?idUsuario=189871&idBitacora="
                + idBitacora + "'>Ver Detalles</a></TD>"
                + // http://10.53.33.83/checklist/central/detalleChecklistExpancionImperdonable.htm?idBitacora=26282
                "</TR>"
                + "</TABLE>"
                + // ITEMS
                "<TABLE style=' width:100%; height:35px; text-align:center'>" + "<TR>"
                + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#F0EFF0;'> <strong style='text-align:center; font-size: 15px;'>"
                + totalItems + "</strong><br></br><br></br> Items Revisados </TD>"
                + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#F0EFF0;'> <strong style='text-align:center; font-size: 15px;'>"
                + itemsNoConsiderados + "</strong><br></br><br></br> Items No Considerados </TD>"
                + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#D8D8D8;'><strong style='text-align:center; font-size: 15px;'>"
                + itemsAprobados
                + "</strong><br></br> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='20' height='20'/>&nbsp; Items Aprobados </TD>"
                + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#6B696E; color:white;'><strong style='text-align:center; font-size: 15px;'>"
                + itemsPorRevisar
                + "</strong><br></br> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='20' height='20'/>&nbsp; Items Por Atender </TD>"
                + "</TR>"
                + // ITEMS
                "</TABLE>"
                + "<div id='detalle'> Detalles de la Entrega </div>"
                + // Div items
                "<TABLE style='width:100%; text-align:center' cellspacing='5'>"
                + " <TR>"
                + // INICIO EKT
                "<TD style='width:50%; height:202px; background-color:white;'> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaElektra.png' width='25' height='25'/>"
                + "&nbsp; Elektra"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoEkt + "' width='60' height='25'/>"
                + "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format((Double.parseDouble(ektTotalPon))) + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaElektra.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;'>items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + ektResSi + "</td>" + "<td>" + ektSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>"
                + ektCalifCant + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + ektResNo + "</td>" + "<td>" + ektNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>" + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=3'>Ver Detalles</a>"
                + " </div>"
                + "</TD> "
                + // FIN EKT
                // INICIO COMUNES
                " <TD style='width:50%; height:202px; '> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaAreasComunes.png' width='25' height=20'/>"
                + "&nbsp; Áreas Comunes"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoCom + "' width='60' height='25'/>"
                + "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format((Double.parseDouble(comTotalPon))) + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaComunes.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;' >items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + comResSi + "</td>" + "<td>" + comSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>"
                + comCalifCant + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + comResNo + "</td>" + "<td>" + comNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>"
                + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=1'>Ver Detalles</a>"
                + " </div>"
                + "</TD>"
                + // FIN COMUNES
                "</TR>"
                + "<TR>"
                + // INICIO BAZ
                "<TD style='width:50%; height:202px; background-color:white;'> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaBAZ.png' width='35' height='25'/>"
                + "&nbsp; Banco Azteca"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoBaz + "' width='60' height='25'/>"
                + "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format((Double.parseDouble(bazTotalPon))) + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaBanco.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;'>items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + bazResSi + "</td>" + "<td>" + bazSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>"
                + bazCalifCant + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + bazResNo + "</td>" + "<td>" + bazNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>"
                + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=2'>Ver Detalles</a>"
                + " </div>"
                + "</TD> "
                + // FIN BAZ
                // INICIO GENERALES
                "<TD style='width:50%; height:202px; background-color:white;'> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaGenerales.png' width='25' height='25'/>"
                + "&nbsp; Generales"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoGen + "' width='60' height='25'/>"
                + "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format((Double.parseDouble(genTotalPon))) + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaGenerales.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;'>items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + genResSi + "</td>" + "<td>" + genSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>"
                + genCalifCant + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + genResNo + "</td>" + "<td>" + genNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>"
                + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=4'>Ver Detalles</a>"
                + " </div>"
                + "</TD>"
                + // FIN GENERALES
                "</TR>"
                + "<TR>"
                + // INICIO PRESTA PRENDA
                "<TD style='width:50%; height:202px; background-color:white;'> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaPrestaPrenda.png' width='35' height='25'/>"
                + "&nbsp; Presta Prenda"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoPres + "' width='60' height='25'/>"
                + "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format((Double.parseDouble(prestaTotalPon))) + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaPresta.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;'>items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + prestaResSi + "</td>" + "<td>" + prestaSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>"
                + prestaCalifCant + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + prestaResNo + "</td>" + "<td>" + prestaNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>"
                + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=5'>Ver Detalles</a>"
                + " </div>"
                + "</TD> "
                // FIN PRESTA PRENDA

                // INICIO TOTAL
                + "<TD style='width:50%; height:202px; background-color:#F0EFF0;'> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "TOTAL"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoTotal + "' width='60' height='25'/>"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format(((totalTotal)))
                + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaTotal.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px; '>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:#F0EFF0; border: 0 px solid #F0EFF'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;'>items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:#F0EFF0; border: 0 px solid #F0EFF'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + totalResSi + "</td>" + "<td>" + totalSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>" + totalCalifCant
                + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:#F0EFF0; border: 0 px solid #F0EFF'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + totalResNo + "</td>" + "<td>" + totalNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>"
                + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=6'>Ver Detalles</a>"
                + " </div>"
                + "</TD>"
                + // FIN TOTAL
                "</TR>"
                + "</TABLE>"
                + "<table cellspacing='0' align='right'>" + "<tr align='center'>"
                + "<td style=' border: 0.25px solid white;'>Código</td>"
                + "<td style=' border: 0.25px solid white; text-align:center; background-color:black; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>pésimo</td>"
                + "<td style='border: 0.25px solid white;  text-align:center; background-color:#FE003D; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>en peligro</td>"
                + "<td style='border: 0.25px solid white;  text-align:center; background-color:#F7CA44; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>alerta</td>"
                + "<td style='border: 0.25px solid white;  text-align:center; background-color:#006240; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>bien</td>"
                + "</tr>" + "</table>"
                + "<strong style='text-align:center; font-size: 10px;'>" + cadenaDesactivados + "</strong>"
                + // Responsables de Obra
                tablaResponsables
                + // Fin Responables de Obra
                "</body>" + "</html>";

        return html;

    }
    ////
    // creaPDFExpansionNuevoFormatoConteoGeneral

    public String creaPDFExpansionNuevoFormatoConteoGeneral(String idBitacora, String tipoVisita, String fecha,
            String economico, String sucursal, String tipo, String territorio, String zona, List<String> imperdonables,
            List<FirmaCheckDTO> firmas, List<ReporteChecksExpDTO> incidencias, String aperturable, String imgFachada, String tipoNegocio) {

        // Obtengo Files de Gráfica
        // Valores de PDF
        String operable = "";
        String colorOperable = "";

        if (tipoVisita.equals("0")) {
            tipoVisita = "Primer Recorrido";
        } else {
            tipoVisita = "Soft Openning";
        }

        //Se crea metodo para calcular grafica y ponderaciones etc
        ExpansionActa2BI acta = new ExpansionActa2BI(idBitacora, tipoNegocio, true);

        // Imperdonables
        String numImperdonables = acta.getCalculoTotal().getListaPregImperdonablesClasif().size() + "";

        DecimalFormat formatDecimales = new DecimalFormat("###.##");
        DecimalFormat formatEnteros = new DecimalFormat("###.##");

        // Obtengo los responsables con bitácoraHermana
        List<EmpContacExtDTO> listaResponsables = new ArrayList<EmpContacExtDTO>();
        String responsables = "";
        String tablaResponsables = "";

        try {
            listaResponsables = empContacExtBI.obtieneDatos(idBitacora);
        } catch (Exception e) {
            logger.info("AP al obtener los responsables de la bitacora : " + idBitacora);
        }

        if (listaResponsables != null && listaResponsables.size() > 0) {

            for (int i = 0; i < listaResponsables.size(); i++) {

                responsables += "<tr>" + "<td style='border: 0 px solid gray; text-align:center;'> <b>"
                        + listaResponsables.get(i).getArea() + "</b></td>"
                        + "<td style='border: 0 px solid gray; text-align:center;'>"
                        + listaResponsables.get(i).getNombre() + "</td>"
                        + "<td style='border: 0 px solid gray; text-align:center;'>"
                        + listaResponsables.get(i).getTelefono() + "</td>"
                        + "<td style='border: 0 px solid gray; text-align:center;'>"
                        + listaResponsables.get(i).getCompania() + "</td>" + "</tr>";
            }

            tablaResponsables += "<div style='width:100%; height:75px; background-color:white;'></div>"
                    + "<div> <h3> Detalles de Responsables de Obra </h3></div>"
                    + "<table cellspacing='5' align='center' style='width:100%; height:400px; background-color:#F0EFF0; border: 0 px solid gray;'>"
                    + "<thead>" + "<tr>" + "<th style='border: 0 px solid gray; text-align:center;'><b>Área</b></th>"
                    + "<th style='border: 0 px solid gray; text-align:center;' ><b>Responsable</b></th>"
                    + "<th style='border: 0 px solid gray; text-align:center;' ><b>Teléfono</b></th>"
                    + "<th style='border: 0 px solid gray; text-align:center;' ><b>Organización</b></th>" + "</tr>"
                    + "</thead>"
                    + "<tbody>"
                    + responsables
                    + "</tbody>"
                    + "</table>";
        }

        // Aperturable
        String cadenaDesactivados = "";
        String sucursales = "";

        String html = "";

        // html = "<!DOCTYPE html>" +
        html = "<html>" + "<head>" + "<title>Acta Entrega</title>" + "</head>" + "<style>" + "div {color:black;}"
                + "table,td,th {font-size: 10px;}" + "a:link {color: white;}" + "a:visited {color: white;}"
                + "a:hover {color: white;}" + "a:active {color: white;}" //+ "td,th {border: 0.25px solid gray;}"
                + "#detalle {text-align:center;}" + "#mini {text-color:blue;}" + "#mini {font-size: 9px;}" + "</style>"
                + "<body>"
                + // Nuevo Formato
                // ABRE DIV LOGOS
                "<div> <h3>" + "<label>ENTREGA DE SUCURSAL</label>"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='" + FRQConstantes.getRutaImagen() + "/franquicia/firmaChecklist/LogoElektra.png' width='36' height='36' />"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='" + FRQConstantes.getRutaImagen() + "/franquicia/firmaChecklist/LogoBAZ.png' width='45' height='30'/>"
                + "</h3></div>"
                + // CIERRA DIV LOGOS
                "<TABLE cellspacing='5' style=' width:100%; height:340px; text-align:center;' >"
                + "<TR>"
                + "<TD ROWSPAN='3' style=' width:25%;'> "
                + "<div style='width:'200'; height:'150';>"
                + "<div bgcolor='red' style='padding-top:5px; width:100%; background-color:" + acta.getColorOperable() + "; color:"
                + acta.getColorTexto() + "; font-size: 13px;'> <h3>" + acta.getOperable() + " </h3></div>"
                + "<img src='" + imgFachada
                + "' width='190%' height='111%'/>"
                + "</div>"
                + "</TD>"
                + "<TD colspan='2' style='text-align:left; background-color:#F0EFF0;'>"
                + "<div style='float:left; width:50%; margin:auto; padding-left:5px; padding-top:10px; padding-bottom:10px; '>"
                + "<b>Sucursal:</b>" + sucursal + "<br></br> <b>Zona: </b>" + zona + "<br></br> <b>Territorio: </b>"
                + territorio
                + "</div>"
                + "<div style='float:left; width:100%; heigth:200%; padding-left:2px; padding-top:30px;'>"
                + "<b>No Económico:</b>" + economico + "<br> </br> <b>Tipo Visita: </b>" + tipoVisita
                + "<br></br> <b>Fecha: </b>" + fecha
                + "</div>"
                + "</TD> "
                + "<TD style='background-color:black;'> <a target='_blank' style='color:white;' href='" + FRQConstantes.getURLServer() + "/checklist/central/detalleChecklistExpancionFirmas.htm?idUsuario=189871&idBitacora="
                + idBitacora + "'>Ver <br></br> Participantes</a></TD>"
                + // "<TD style='text-align:left; font-size: 10px; background-color:#F0EFF0;'>
                // <b>No Económico:</b>"+ "10029988" +"<br></br> <b>Tipo Visita: </b>"+"Soft
                // Opening"+"<br></br> <b>Fecha: </b>"+ "29/10/2019" +" </TD> " +
                "</TR>"
                + // <b></b> <br></br>
                "<TR>"
                + "<TD style='text-align:center; font-size: 12px; height:50px;'><strong style='text-align:center; font-size: 10px;'>Generales de la recepción</strong></TD> "
                + "<TD style='text-align:center; font-size: 12px; height:50px;'> Calificación <strong style='text-align:center; font-size: 15px;'>"
                + formatDecimales.format(acta.getPreCalificacionNum()) + "</strong> </TD> "
                + /*+ "<TD style='background-color:" + colorCalificacion + "; color:" + colorLetraCalificacion
                 + "'> Calificación: <strong style='text-align:center; font-size: 15px;'>"
                 + formatDecimales.format(Double.parseDouble(calificacion)) + "</strong> </TD>" +*/ "<TD style='background-color:black; height:50px;'> <a target='_blank' style='color:white;' href='" + FRQConstantes.getURLServer() + "/checklist/central/menuActa.htm?idUsuario=189871&idBitacora="
                + idBitacora + "'>Ver Detalles</a></TD>"
                + "</TR>"
                + "<TR>"
                + "<TD colspan='2' style='text-align:center; background-color:#F0EFF0;'> <p><b>Imperdonables:</b> <strong style='text-align:center; font-size: 15px;'>"
                + numImperdonables + "</strong></p> </TD>"
                + "<TD style='background-color:black;'> <a target='_blank' style='color:white;' href='" + FRQConstantes.getURLServer() + "/checklist/central/detalleChecklistExpancionImperdonableGeneral.htm?idUsuario=189871&idBitacora="
                + idBitacora + "'>Ver Detalles</a></TD>"
                + // http://10.53.33.83/checklist/central/detalleChecklistExpancionImperdonable.htm?idBitacora=26282
                "</TR>"
                + "</TABLE>"
                + // ITEMS
                "<TABLE style=' width:100%; height:35px; text-align:center'>" + "<TR>"
                + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#F0EFF0;'> <strong style='text-align:center; font-size: 15px;'>"
                + acta.getTotalItems() + "</strong><br></br><br></br> Items Revisados </TD>"
                + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#F0EFF0;'> <strong style='text-align:center; font-size: 15px;'>"
                + acta.getItemsNoConsiderados() + "</strong><br></br><br></br> Items No Considerados </TD>"
                + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#D8D8D8;'><strong style='text-align:center; font-size: 15px;'>"
                + acta.getItemsAprobados()
                + "</strong><br></br> <img src='" + FRQConstantes.getRutaImagen() + "/franquicia/firmaChecklist/ItemsAprovados02.png' width='20' height='20'/>&nbsp; Items Aprobados </TD>"
                + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#6B696E; color:white;'><strong style='text-align:center; font-size: 15px;'>"
                + acta.getItemsPorRevisar()
                + "</strong><br></br> <img src='" + FRQConstantes.getRutaImagen() + "/franquicia/firmaChecklist/ItemsAtender02.png' width='20' height='20'/>&nbsp; Items Por Atender </TD>"
                + "</TR>"
                + // ITEMS
                "</TABLE>"
                + "<div id='detalle'> <br></br><br></br> Detalles de la Entrega </div>";

        String tabla1 = "";
        if (tipoNegocio.equalsIgnoreCase("OCC")) {
            tabla1 = "<tr height=\"180px\" style=\"border: 2px solid black;\">"
                    + "         <td WIDTH=\"50%\" style='border: 0.25px solid gray;'>@grafica1</td>"
                    + "         <td WIDTH=\"50%\" style='border: 0.25px solid gray;'>@grafica2</td>"
                    + "   </tr>"
                    + "   <tr height=\"180px\">"
                    + "         <td WIDTH=\"50%\" style='border: 0.25px solid gray;'>@grafica3</td>"
                    + "         <td WIDTH=\"50%\" style='border: 0.25px solid gray;'>@grafica4</td>"
                    + "   </tr>";
        } else {
            tabla1 = "<tr height=\"180px\" style=\"border: 2px solid black;\">"
                    + "         <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica1</td>"
                    + "         <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica2</td>"
                    + "         <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica3</td>"
                    + "   </tr>"
                    + "   <tr height=\"180px\" >"
                    + "         <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica4</td>"
                    + "         <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica5</td>"
                    + "         <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica6</td>"
                    + "   </tr>";
        }

        for (int i = 0; i < acta.getCuadroGrafica().length; i++) {

            tabla1 = tabla1.replace("@grafica" + (i + 1), acta.getCuadroGrafica()[i]);
            tabla1 = tabla1.replace("@verDetalle", "<a target='_blank' style='color:black;' href='" + FRQConstantes.getURLServer() + "/checklist/central/detalleChecklistExpancionGeneral.htm?idUsuario=189871&idBitacora="
                    + idBitacora + "&tipo=" + acta.getClasificacion()[i] + "'>Ver Detalles</a>");
        }
        tabla1 = tabla1.replace("@grafica5", acta.getCuadroGrafica()[acta.getCuadroGrafica().length - 1]);
        tabla1 = tabla1.replace("@grafica6", acta.getCuadroGrafica()[acta.getCuadroGrafica().length - 1]);

        tabla1 = tabla1.replace("width='50%'", "width='30%'");

        html += "<table WIDTH=\"100%\" cellpadding=\"0\" cellspacing=\"0\" ><tbody>"
                + tabla1
                + "</tbody></table>";
        logger.info(html);

        html += "<table cellspacing='0' align='right'>" + "<tr align='center'>"
                + "<td style=' border: 0.25px solid white;'>Código</td>"
                + "<td style=' border: 0.25px solid white; text-align:center; background-color:black; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>pésimo</td>"
                + "<td style='border: 0.25px solid white;  text-align:center; background-color:#FE003D; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>en peligro</td>"
                + "<td style='border: 0.25px solid white;  text-align:center; background-color:#F7CA44; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>alerta</td>"
                + "<td style='border: 0.25px solid white;  text-align:center; background-color:#006240; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>bien</td>"
                + "</tr>" + "</table>"
                + "<strong style='text-align:center; font-size: 10px;'>" + cadenaDesactivados + "</strong>"
                + // Responsables de Obra
                tablaResponsables
                + // Fin Responables de Obra
                "</body>" + "</html>";

        return html;

    }
    ////
    // creaPDFExpansionNuevoFormatoConteoOCC

    public String creaPDFExpansionNuevoFormatoConteoOCC(String idBitacora, String tipoVisita, String fecha,
            String economico, String sucursal, String tipo, String territorio, String zona, List<String> imperdonables,
            List<FirmaCheckDTO> firmas, List<ReporteChecksExpDTO> incidencias, String aperturable, String imgFachada) {

        // Obtengo Files de Gráfica
        // Valores de PDF
        String operable = "";
        String colorOperable = "";

        if (tipoVisita.equals("0")) {
            tipoVisita = "Primer Recorrido";
        } else {
            tipoVisita = "Soft Openning";
        }

        // Imperdonables
        String numImperdonables = imperdonables.size() + "";

        DecimalFormat formatDecimales = new DecimalFormat("###.##");
        DecimalFormat formatEnteros = new DecimalFormat("###.##");

        // AQUÍ ES EL CAMBIO DE TODOS LOS NUEVOS VALORES QUE HAY QUE CONSIDERAR
        // Obtenemos Datos
        ChecklistPreguntasComBI checklistPreguntasComBI = (ChecklistPreguntasComBI) FRQAppContextProvider
                .getApplicationContext().getBean("checklistPreguntasComBI");

        // Obtiene La información anterior
        List<ChecklistPreguntasComDTO> listaGrupos = new ArrayList<ChecklistPreguntasComDTO>();

        try {
            listaGrupos = checklistPreguntasComBI.obtieneInfoOCC(idBitacora);
        } catch (Exception e) {

            logger.info("AP al obtener al obtener obtieneInfoCont de bitacora: " + idBitacora);
        }

        // AREAS
        ChecklistPreguntasComDTO areasComunesObj = listaGrupos.get(0);
        // OCC 1
        ChecklistPreguntasComDTO cYcObj = listaGrupos.get(1);
        // GENERALES 3
        ChecklistPreguntasComDTO generalesObj = listaGrupos.get(2);

        List<ChecklistPreguntasComDTO> listaGruposExt = new ArrayList<ChecklistPreguntasComDTO>();

        try {
            listaGruposExt = checklistPreguntasComBI.obtieneComplemOCC(idBitacora);
        } catch (Exception e) {

            logger.info("AP al obtener al obtener obtieneInfoContExt de bitacora: " + idBitacora);
        }

        // Nuevo paquete dividido
        // AREAS 1
        ChecklistPreguntasComDTO areasComunesObjExt = listaGruposExt.get(0);
        double ponderacionComExt = areasComunesObjExt.getSumGrupo();
        double totalGeneralComunes = areasComunesObjExt.getTotGral();

        // OCC 2
        ChecklistPreguntasComDTO cYcObjExt = listaGruposExt.get(1);
        double ponderacionCyCExt = cYcObjExt.getSumGrupo();
        double totalGeneralCyC = cYcObjExt.getTotGral();
        // GENERALES 3
        ChecklistPreguntasComDTO generalesObjExt = listaGruposExt.get(2);
        double ponderacionGenExt = generalesObjExt.getSumGrupo();
        double totalGeneralGenerales = generalesObjExt.getTotGral();

        List<ChecklistPreguntasComDTO> listaConteosHijas = new ArrayList<ChecklistPreguntasComDTO>();

        try {
            listaConteosHijas = checklistPreguntasComBI.obtieneComplemSiNaOCC(idBitacora);
        } catch (Exception e) {
            logger.info("AP al obtener al obtener obtieneInfoContHij de bitacora: " + idBitacora);
        }

        // AREAS
        ChecklistPreguntasComDTO conteoHijasComunes = listaConteosHijas.get(0);
        double comSiHijas = conteoHijasComunes.getPregSi();
        double comNaHijas = conteoHijasComunes.getPregNa();

        // CYC 1
        ChecklistPreguntasComDTO conteoHiijasCyC = listaConteosHijas.get(1);
        double cYcSiHijas = conteoHiijasCyC.getPregSi();
        double cYcNaHijas = conteoHiijasCyC.getPregNa();

        // GENERALES 3
        ChecklistPreguntasComDTO conteoHiijasGenerales = listaConteosHijas.get(2);
        double generalesSiHijas = conteoHiijasGenerales.getPregSi();
        double generalesNaHijas = conteoHiijasGenerales.getPregNa();

        // 1.- GRÁFICA COMUNES ********
        // Cálculo Total de Preguntas
        // double comResNo1 = areasComunesObj.getPregNo();
        double comResNo1 = areasComunesObj.getPregNo();
        // NA
        double comResNa1 = areasComunesObj.getPregNa();
        //
        double comGrupoCompleto = areasComunesObj.getTotPreg();
        // double comResSi1 = areasComunesObj.getTotPreg() -
        // areasComunesObj.getPregNo();
        double comResSi1 = comSiHijas + (comGrupoCompleto - comResNa1) - comResNo1;

        // double comTotal = areasComunesObj.getTotPreg();
        double comTotal = comResSi1 + comResNo1 + comSiHijas;

        // PARA LOS NA FALTA SUMAR LO DESACTIVADO
        comResNa1 += (totalGeneralComunes - comTotal);

        double comSiPor1 = 0;
        double comNoPor1 = 0;

        if (comTotal != 0) {
            comSiPor1 = (comResSi1 / (comResSi1 + comResNo1)) * 100;
            comNoPor1 = (comResNo1 / (comResSi1 + comResNo1)) * 100;
        }

        double NAComunes = (totalGeneralComunes - (areasComunesObj.getPregNo() + areasComunesObj.getPregSi()));
        NAComunes = NAComunes - (comSiHijas);
        /*double NAComunes = 0;
         if (areasComunesObj.getPregSi() != 0 && areasComunesObj.getPregNo() != 0 ) {
         NAComunes = (totalGeneralComunes - (areasComunesObj.getPregNo() + areasComunesObj.getPregSi()));
         NAComunes = NAComunes - (comSiHijas);
         }*/

        // Calculo Por Ponderación
        // double comPorcentajePonderacion = (areasComunesObj.getPrecalif() * 100) /
        // areasComunesObj.getSumGrupo();
        double comPorcentajePonderacion = (ponderacionComExt > 0 ? ((areasComunesObj.getPrecalif() * 100) / ponderacionComExt) : 0.0);

        int comunesImperdonables = areasComunesObj.getAux2();
        String logoCom = getLogoExpansion(comPorcentajePonderacion, comunesImperdonables);

        // Cantidad Tabla (Si-No)
        if (areasComunesObj.getPregSi() == 0 && areasComunesObj.getPregNo() == 0 && comSiHijas == 0) {
            comResSi1 = 0;
            comResNo1 = 0;
            comSiPor1 = 0;
            comNoPor1 = 0;
        }

        String comResSi = formatEnteros.format(comResSi1);
        String comResNo = formatEnteros.format(comResNo1);

        // Porcentaje Tabla y Gráfica
        String comSiPor = formatDecimales.format(comSiPor1);
        String comNoPor = formatDecimales.format(comNoPor1);

        if (comResSi1 == 0 && comResNo1 == 0) {
            graficaComunes = grafica(1, 0, 100);
            logoCom = getLogoExpansion(100, 0);
        } else {
            graficaComunes = grafica(1, Double.parseDouble(comSiPor), Double.parseDouble(comNoPor));
        }

        // GRÁFICA COMUNES ********
        // 2.- GRÁFICA OCC ********
        // double bazResNo1 = bazObj.getPregNo();
        double cYcResNo1 = cYcObj.getPregNo();
        // NA
        double cYcResNa1 = cYcObj.getPregNa();
        //
        double cYcGrupoCompleto = cYcObj.getTotPreg();
        // double bazResSi1 = bazObj.getTotPreg() - bazObj.getPregNo();
        double cYcResSi1 = cYcSiHijas + (cYcGrupoCompleto - cYcResNa1) - cYcResNo1;

        // double bazTotal = bazObj.getTotPreg();
        double cYcTotal = cYcResSi1 + cYcResNo1 + cYcSiHijas;

        // PARA LOS NA FALTA SUMAR LO DESACTIVADO
        cYcResNa1 += (totalGeneralCyC - cYcTotal);

        double cYcSiPor1 = 0;
        double cYcNoPor1 = 0;

        if (cYcTotal != 0) {
            cYcSiPor1 = (cYcResSi1 / (cYcResSi1 + cYcResNo1)) * 100;
            cYcNoPor1 = (cYcResNo1 / (cYcResSi1 + cYcResNo1)) * 100;
        }

        double NACyC = (totalGeneralCyC - (cYcObj.getPregNo() + cYcObj.getPregSi()));
        NACyC = NACyC - (cYcSiHijas);
        /*double NABaz = 0.0;
         if (bazObj.getPregSi() != 0 && bazObj.getPregNo() != 0 ) {
         NABaz = (totalGeneralBaz - (bazObj.getPregNo() + bazObj.getPregSi()));
         NABaz = NABaz - (bazSiHijas);
         }*/
        // Calculo Por Ponderación
        // double bazPorcentajePonderacion = (bazObj.getPrecalif() * 100) /
        // bazObj.getSumGrupo();
        double cYcPorcentajePonderacion = (ponderacionCyCExt > 0 ? ((cYcObj.getPrecalif() * 100) / ponderacionCyCExt) : 0.0);

        int cYcImperdonables = cYcObj.getAux2();
        String logoCyC = getLogoExpansion(cYcPorcentajePonderacion, cYcImperdonables);

        // Cantidad Tabla (Si-No)
        if (cYcObj.getPregSi() == 0 && cYcObj.getPregNo() == 0 && cYcSiHijas == 0) {
            cYcResSi1 = 0;
            cYcResNo1 = 0;
            cYcSiPor1 = 0;
            cYcNoPor1 = 0;
        }

        String cYcResSi = formatEnteros.format(cYcResSi1);
        String cYcResNo = formatEnteros.format(cYcResNo1);

        // Porcentaje Tabla y Gráfica
        String cYcSiPor = formatDecimales.format(cYcSiPor1);
        String cYcNoPor = formatDecimales.format(cYcNoPor1);

        if (cYcResSi1 == 0 && cYcResNo1 == 0) {
            graficaBanco = grafica(1, 0, 100);
            logoCyC = getLogoExpansion(100, 0);

        } else {
            graficaBanco = grafica(1, Double.parseDouble(cYcSiPor), Double.parseDouble(cYcNoPor));
        }

        // GRÁFICA BANCO ********
        // 4.- GRÁFICA GENERALES ********
        // double genResNo1 = generalesObj.getPregNo();
        double genResNo1 = generalesObj.getPregNo();
        // NA
        double genResNa1 = generalesObj.getPregNa();
        //
        double genGrupoCompleto = generalesObj.getTotPreg();

        // double genResSi1 = generalesObj.getTotPreg() - generalesObj.getPregNo();
        double genResSi1 = generalesSiHijas + (genGrupoCompleto - genResNa1) - genResNo1;

        // double genTotal = generalesObj.getTotPreg();
        double genTotal = genResSi1 + genResNo1 + generalesSiHijas;

        // PARA LOS NA FALTA SUMAR LO DESACTIVADO
        genResNa1 += (totalGeneralGenerales - genTotal);

        double genSiPor1 = 0;
        double genNoPor1 = 0;

        if (genTotal != 0) {
            genSiPor1 = (genResSi1 / (genResSi1 + genResNo1)) * 100;
            genNoPor1 = (genResNo1 / (genResSi1 + genResNo1)) * 100;
        }

        double NAGenerales = (totalGeneralGenerales - (generalesObj.getPregNo() + generalesObj.getPregSi()));
        NAGenerales = NAGenerales - (generalesSiHijas);
        /*double NAGenerales = 0.0;
         if (generalesObj.getPregSi() != 0 && generalesObj.getPregNo() != 0 ) {
         NAGenerales = (totalGeneralBaz - (generalesObj.getPregNo() + generalesObj.getPregSi()));
         NAGenerales = NAGenerales - (generalesSiHijas);
         }*/

        // Calculo Por Ponderación
        // double generalesPorcentajePonderacion = (generalesObj.getPrecalif() * 100) /
        // generalesObj.getSumGrupo();
        double generalesPorcentajePonderacion = (ponderacionGenExt > 0 ? ((generalesObj.getPrecalif() * 100) / ponderacionGenExt) : 0.0);

        int generalesImperdonables = generalesObj.getAux2();
        String logoGen = getLogoExpansion(generalesPorcentajePonderacion, generalesImperdonables);

        // Cantidad Tabla (Si-No)
        if (generalesObj.getPregSi() == 0 && generalesObj.getPregNo() == 0 && generalesSiHijas == 0) {
            genResSi1 = 0;
            genResNo1 = 0;
            genSiPor1 = 0;
            genNoPor1 = 0;
        }

        String genResSi = formatEnteros.format(genResSi1);
        String genResNo = formatEnteros.format(genResNo1);

        // Porcentaje Tabla y Gráfica
        String genSiPor = formatDecimales.format(genSiPor1);
        String genNoPor = formatDecimales.format(genNoPor1);

        if (genResSi1 == 0 && genResNo1 == 0) {
            graficaGenerales = grafica(1, 0, 100);
            logoGen = getLogoExpansion(100, 0);

        } else {
            graficaGenerales = grafica(1, Double.parseDouble(genSiPor), Double.parseDouble(genNoPor));
        }

        // GRÁFICA GENERALES ********
        // 6.- VALORES TOTAL ********
        // double totalResSi1 = (totalTotal - totalResNo1);
        double totalResNo1 = (areasComunesObj.getPregNo() + cYcObj.getPregNo() + generalesObj.getPregNo());
        // NA
        double totalResNa1 = (areasComunesObj.getPregNa() + cYcObj.getPregNa() + generalesObj.getPregNa());

        double totalResSi1 = (comResSi1 + cYcResSi1 + genResSi1);

        double totalTotal = (totalResNo1 + totalResSi1);

        double totalSiPor1 = 0;
        double totalNoPor1 = 0;

        if (totalTotal != 0) {
            totalSiPor1 = (totalResSi1 / totalTotal) * 100;
            totalNoPor1 = (totalResNo1 / totalTotal) * 100;
        }

        // Calculo Por Ponderación
        // double totalPrecalif = (areasComunesObj.getPrecalif() + bazObj.getPrecalif()
        // + ektObj.getPrecalif() + generalesObj.getPrecalif() +
        // prestaPrendaObj.getPrecalif());
        double totalPrecalif = (areasComunesObj.getPonTot() + cYcObj.getPonTot() + generalesObj.getPonTot());
        double totalSumGrupo = (ponderacionComExt + ponderacionCyCExt + ponderacionGenExt);
        double tPorcentajePonderacion = (totalPrecalif * 100) / totalSumGrupo;

        int tImperdonables = (areasComunesObj.getAux2() + cYcObj.getAux2() + generalesObj.getAux2());
        String logoTotal = getLogoExpansion(tPorcentajePonderacion, tImperdonables);

        // Cantidad Tabla (Si-No)
        String totalResSi = formatEnteros.format(totalResSi1);
        String totalResNo = formatEnteros.format(totalResNo1);

        // Porcentaje Tabla y Gráfica
        String totalSiPor = formatDecimales.format(totalSiPor1);
        String totalNoPor = formatDecimales.format(totalNoPor1);

        // ********* Calificación *********
        // double totalCalif1 = (totalSiPor1 * totalValorTotal) / 100;
        double totalCalif1 = (totalTotal > 0 ? ((totalSiPor1 * 100) / totalTotal) : 0.0);
        String totalCalif = formatDecimales.format(totalCalif1);

        // ********* Calificacion y Pre Calificacion *********
        // Calculo de porcentajes por Zona
        // Porcentaje en relación al total Comunes
        // Calificación
        double porcentajeComTotal = (totalTotal > 0 ? ((comTotal * 100) / totalTotal) : 0.0);
        double comCalif1 = (comTotal > 0 ? ((comResSi1 * porcentajeComTotal) / comTotal) : 0.0);
        comTotal = Double.parseDouble(formatDecimales.format(porcentajeComTotal));

        String comCalifAnt = formatDecimales.format(comCalif1);
        // Double comPonderacion = (Double.parseDouble((areasComunesObj.getAux()!=null ?
        // !areasComunesObj.getAux().equals("") ? areasComunesObj.getAux(): "0":"0")));

        // Cuanto vale la zona
        double comPonderacion = ponderacionComExt;
        // Calcular ponderación pondTotal (ponderacion SI)
        Double comPreCal = areasComunesObj.getPrecalif();

        String comCalif = formatDecimales.format(comPreCal);

        if (comResSi1 == 0 && comResNo1 == 0) {
            comGrupoCompleto = 0;
            comPonderacion = 0;
        }
        String comTotalPon = formatDecimales.format(comPonderacion);
        String comCalifCant = formatDecimales.format(comPonderacion > 0 ? (((comPreCal / comPonderacion) * 100)) : 0.0);
        // Fin Porcentaje en relación al total Comunes

        // Porcentaje en relación al total OCC
        double porcentajeCyCTotal = (totalTotal > 0 ? ((cYcTotal * 100) / totalTotal) : 0.0);
        double cYcCalif1 = (cYcTotal > 0 ? ((cYcResSi1 * porcentajeCyCTotal) / cYcTotal) : 0.0);
        cYcTotal = Double.parseDouble(formatDecimales.format(porcentajeCyCTotal));

        String cYcCalifAnt = formatDecimales.format(cYcCalif1);
        // Double bazPonderacion = (Double.parseDouble((bazObj.getAux()!=null ?
        // !bazObj.getAux().equals("") ? bazObj.getAux(): "0":"0")));

        double cYcPonderacion = ponderacionCyCExt;
        // Calcular ponderación pondTotal (ponderacion SI)
        Double cYcPreCal = cYcObj.getPrecalif();

        String cYcCalif = formatDecimales.format(cYcPreCal);

        if (cYcResSi1 == 0 && cYcResNo1 == 0) {
            cYcGrupoCompleto = 0;
            cYcPonderacion = 0;
        }

        String cYcTotalPon = formatDecimales.format(cYcPonderacion);
        String cYcCalifCant = formatDecimales.format(cYcPonderacion > 0 ? (((cYcPreCal / cYcPonderacion) * 100)) : 0.0);

        // Fin Porcentaje en relación al total OCC
        // Porcentaje en relación al total Generales
        double porcentajeGeneralesTotal = (totalTotal > 0 ? ((genTotal * 100) / totalTotal) : 0.0);
        double genCalif1 = (genTotal > 0 ? ((genResSi1 * porcentajeGeneralesTotal) / genTotal) : 0.0);
        genTotal = Double.parseDouble(formatDecimales.format(porcentajeGeneralesTotal));

        String genCalifAnt = formatDecimales.format(genCalif1);
        // Double generalesPonderacion =
        // (Double.parseDouble((generalesObj.getAux()!=null ?
        // !generalesObj.getAux().equals("") ? generalesObj.getAux(): "0":"0")));

        double generalesPonderacion = ponderacionGenExt;
        // Calcular ponderación pondTotal (ponderacion SI)
        Double generalesPreCal = generalesObj.getPrecalif();

        String genCalif = formatDecimales.format(generalesPreCal);

        if (genResSi1 == 0 && genResNo1 == 0) {
            genGrupoCompleto = 0;
            generalesPonderacion = 0;
        }

        String genTotalPon = formatDecimales.format(generalesPonderacion);
        String genCalifCant = formatDecimales.format(generalesPonderacion > 0 ? (((generalesPreCal / generalesPonderacion) * 100)) : 0.0);

        // Fin Porcentaje en relación al total Generales
        // Fin Calculo de porcentaje por Zona
        // Items
        String totalItems = formatDecimales.format((comGrupoCompleto - areasComunesObj.getPregNa() + comSiHijas)
                + (cYcGrupoCompleto - cYcObj.getPregNa() + cYcSiHijas)
                + (genGrupoCompleto - generalesObj.getPregNa() + generalesSiHijas));
        // String totalItems = formatDecimales.format(comTotal + bazTotal + ektTotal +
        // prestaTotal + genTotal);
        String itemsPorRevisar = formatDecimales.format(comResNo1 + cYcResNo1 + genResNo1);

        double itemAprobadosResta = ((((comGrupoCompleto - areasComunesObj.getPregNa()) - comResNo1) + comSiHijas)
                + (((cYcGrupoCompleto - cYcObj.getPregNa()) - cYcResNo1) + cYcSiHijas)
                + (((genGrupoCompleto - generalesObj.getPregNa()) - genResNo1) + generalesSiHijas));
        String itemsAprobados = formatDecimales.format(itemAprobadosResta);

        //String itemsNoConsiderados = formatDecimales.format(NAComunes + NACyC  + NAGenerales);
        double sumaItemsRevisados = (comGrupoCompleto - areasComunesObj.getPregNa() + comSiHijas)
                + (cYcGrupoCompleto - cYcObj.getPregNa() + cYcSiHijas)
                + (genGrupoCompleto - generalesObj.getPregNa() + generalesSiHijas);

        String itemsNoConsiderados = formatDecimales.format(416 - (sumaItemsRevisados));

        // totalTotal = (Double.parseDouble((areasComunesObj.getAux()!=null ?
        // !areasComunesObj.getAux().equals("") ? areasComunesObj.getAux(): "0":"0"))) +
        // (Double.parseDouble((bazObj.getAux()!=null ? !bazObj.getAux().equals("") ?
        // bazObj.getAux(): "0":"0")))+
        // (Double.parseDouble((ektObj.getAux()!=null ? !ektObj.getAux().equals("") ?
        // ektObj.getAux(): "0":"0"))) +
        // (Double.parseDouble((generalesObj.getAux()!=null ?
        // !generalesObj.getAux().equals("") ? generalesObj.getAux(): "0":"0"))) +
        // (Double.parseDouble((prestaPrendaObj.getAux()!=null ?
        // !prestaPrendaObj.getAux().equals("") ? prestaPrendaObj.getAux(): "0":"0"))) ;
        totalTotal = comPonderacion + cYcPonderacion + generalesPonderacion;

        // --- PONDERACIÓNES ---
        double ponderacionTotalObtenida = areasComunesObj.getPrecalif() + cYcObj.getPrecalif() + generalesObj.getPrecalif();//
        double ponderacionTotalGrupos = comPonderacion + cYcPonderacion + generalesPonderacion;

        double ponderacionObtenida = (ponderacionTotalGrupos > 0 ? (((ponderacionTotalObtenida) * 100) / (ponderacionTotalGrupos)) : 0.0);
        double ponderacionTotal = (ponderacionTotalObtenida);
        String totalCalifCant = formatDecimales.format((totalTotal > 0 ? (((ponderacionTotal / totalTotal) * 100)) : 0.0));

        double preCalificacionNum = ponderacionObtenida;
        String preCalificacion = formatDecimales.format(preCalificacionNum);
        String calificacion = "0.0";
        String colorCalificacion = "";
        String colorLetraCalificacion = "white";

        if (imperdonables.size() == 0) {
            calificacion = preCalificacion;
            colorLetraCalificacion = "black";
        }

        colorCalificacion = getColorExpansion(Double.parseDouble(calificacion));

        if (colorCalificacion.contains("#000000")) {
            colorLetraCalificacion = "white";
        }

        graficaTotal = grafica(2, Double.parseDouble(totalSiPor), Double.parseDouble(totalNoPor));
        // VALORES TOTAL ********

        // Obtengo los responsables con bitácoraHermana
        List<EmpContacExtDTO> listaResponsables = new ArrayList<EmpContacExtDTO>();
        String responsables = "";
        String tablaResponsables = "";

        try {
            listaResponsables = empContacExtBI.obtieneDatos(idBitacora);
        } catch (Exception e) {
            logger.info("AP al obtener los responsables de la bitacora : " + idBitacora);
        }

        if (listaResponsables != null && listaResponsables.size() > 0) {

            for (int i = 0; i < listaResponsables.size(); i++) {

                responsables += "<tr>" + "<td style='border: 0 px solid gray; text-align:center;'> <b>"
                        + listaResponsables.get(i).getArea() + "</b></td>"
                        + "<td style='border: 0 px solid gray; text-align:center;'>"
                        + listaResponsables.get(i).getNombre() + "</td>"
                        + "<td style='border: 0 px solid gray; text-align:center;'>"
                        + listaResponsables.get(i).getTelefono() + "</td>"
                        + "<td style='border: 0 px solid gray; text-align:center;'>"
                        + listaResponsables.get(i).getCompania() + "</td>" + "</tr>";
            }

            tablaResponsables += "<div style='width:100%; height:75px; background-color:white;'></div>"
                    + "<div> <h3> Detalles de Responsables de Obra </h3></div>"
                    + "<table cellspacing='5' align='center' style='width:100%; height:400px; background-color:#F0EFF0; border: 0 px solid gray;'>"
                    + "<thead>" + "<tr>" + "<th style='border: 0 px solid gray; text-align:center;'><b>Área</b></th>"
                    + "<th style='border: 0 px solid gray; text-align:center;' ><b>Responsable</b></th>"
                    + "<th style='border: 0 px solid gray; text-align:center;' ><b>Teléfono</b></th>"
                    + "<th style='border: 0 px solid gray; text-align:center;' ><b>Organización</b></th>" + "</tr>"
                    + "</thead>"
                    + "<tbody>"
                    + responsables
                    + "</tbody>"
                    + "</table>";
        }

        // Aperturable
        double numeroDeCalificacion = (preCalificacionNum);
        int numImperdonablesRecorrido = imperdonables.size();
        String colorTexto = "white";

        if (numImperdonablesRecorrido > 0) {
            // Tiene imperdonables el color es NEGRO, NO OPERABLE Y SIN RECEPCIÓN, letra
            // blanca
            operable = "NO OPERABLE ";
            operable += "";
            colorOperable = "#000101";
            colorTexto = "white";
        } else {

            if (numeroDeCalificacion >= 100) {
                operable = "OPERABLE ";
                operable += " (Con recepción)";
                // VERDE
                colorOperable = "#00b240";
                colorTexto = "white";
            } else if (numeroDeCalificacion >= 70 && numeroDeCalificacion <= 99) {
                operable = "OPERABLE ";
                operable += " (Sin recepción)";
                // AMARILLO
                colorOperable = "#ECE90C";
                colorTexto = "black";
            } else if (numeroDeCalificacion >= 50 && numeroDeCalificacion <= 69) {
                operable = "NO OPERABLE ";
                operable += " ";
                // ROJO
                colorOperable = "#B40404";
                colorTexto = "black";
            } else if (numeroDeCalificacion <= 49) {
                operable = "NO OPERABLE ";
                operable += "";
                // NEGRO
                colorOperable = "#000101";
                colorTexto = "white";
            }
        }

        String cadenaDesactivados = "";
        String sucursales = "";
        /*
         * for (ChecklistPreguntasComDTO obj :
         * checklistPreguntasComBI.obtieneInfo(idBitacora)) {
         *
         * if ( obj.getPregSi() == 0 && obj.getPregNo() == 0 && obj.getPregNa() == 0 ) {
         *
         * sucursales += obj.getChecklist() + ", ";
         *
         * } }
         *
         * if (sucursales.length() > 0) {
         *
         * cadenaDesactivados =
         * "Los siguientes módulos no se consideran en el recorrido: " +
         * sucursales.substring(0,sucursales.length() - 2); ; }
         */

        String html = "";

        // html = "<!DOCTYPE html>" +
        html = "<html>" + "<head>" + "<title>Acta Entrega</title>" + "</head>" + "<style>" + "div {color:black;}"
                + "table,td,th {font-size: 10px;}" + "a:link {color: white;}" + "a:visited {color: white;}"
                + "a:hover {color: white;}" + "a:active {color: white;}" + "td,th {border: 0.25px solid gray;}"
                + "#detalle {text-align:center;}" + "#mini {text-color:blue;}" + "#mini {font-size: 9px;}" + "</style>"
                + "<body>"
                + // Nuevo Formato
                // ABRE DIV LOGOS
                "<div> <h3>" + "<label>ENTREGA DE SUCURSAL</label>"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoElektra.png' width='36' height='36' />"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoBAZ.png' width='45' height='30'/>"
                + "</h3></div>"
                + // CIERRA DIV LOGOS
                "<TABLE cellspacing='5' style=' width:100%; height:340px; text-align:center;' >"
                + "<TR>"
                + "<TD ROWSPAN='3' style=' width:25%;'> "
                + "<div style='width:'200'; height:'150';>"
                + "<div bgcolor='red' style='padding-top:5px; width:100%; background-color:" + colorOperable + "; color:"
                + colorTexto + "; font-size: 13px;'> <h3>" + operable + " </h3></div>"
                + "<img src='" + imgFachada
                + "' width='190%' height='111%'/>"
                + "</div>"
                + "</TD>"
                + "<TD colspan='2' style='text-align:left; background-color:#F0EFF0;'>"
                + "<div style='float:left; width:50%; margin:auto; padding-left:5px; padding-top:10px; padding-bottom:10px; '>"
                + "<b>Sucursal:</b>" + sucursal + "<br></br> <b>Zona: </b>" + zona + "<br></br> <b>Territorio: </b>"
                + territorio
                + "</div>"
                + "<div style='float:left; width:100%; heigth:200%; padding-left:2px; padding-top:30px;'>"
                + "<b>No Económico:</b>" + economico + "<br> </br> <b>Tipo Visita: </b>" + tipoVisita
                + "<br></br> <b>Fecha: </b>" + fecha
                + "</div>"
                + "</TD> "
                + "<TD style='background-color:black;'> <a target='_blank' style='color:white;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancionFirmas.htm?idUsuario=189871&idBitacora="
                + idBitacora + "'>Ver <br></br> Participantes</a></TD>"
                + // "<TD style='text-align:left; font-size: 10px; background-color:#F0EFF0;'>
                // <b>No Económico:</b>"+ "10029988" +"<br></br> <b>Tipo Visita: </b>"+"Soft
                // Opening"+"<br></br> <b>Fecha: </b>"+ "29/10/2019" +" </TD> " +
                "</TR>"
                + // <b></b> <br></br>
                "<TR>"
                + "<TD style='text-align:center; font-size: 12px; height:50px;'><strong style='text-align:center; font-size: 10px;'>Generales de la recepción</strong></TD> "
                + "<TD style='text-align:center; font-size: 12px; height:50px;'> Calificación <strong style='text-align:center; font-size: 15px;'>"
                + formatDecimales.format(Double.parseDouble(preCalificacion)) + "</strong> </TD> "
                + /*+ "<TD style='background-color:" + colorCalificacion + "; color:" + colorLetraCalificacion
                 + "'> Calificación: <strong style='text-align:center; font-size: 15px;'>"
                 + formatDecimales.format(Double.parseDouble(calificacion)) + "</strong> </TD>" +*/ "<TD style='background-color:black; height:50px;'> <a target='_blank' style='color:white;' href='http://10.53.33.83/checklist/central/menuActa.htm?idUsuario=189871&idBitacora="
                + idBitacora + "'>Ver Detalles</a></TD>"
                + "</TR>"
                + "<TR>"
                + "<TD colspan='2' style='text-align:center; background-color:#F0EFF0;'> <p><b>Imperdonables:</b> <strong style='text-align:center; font-size: 15px;'>"
                + numImperdonables + "</strong></p> </TD>"
                + "<TD style='background-color:black;'> <a target='_blank' style='color:white;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancionImperdonable.htm?idUsuario=189871&idBitacora="
                + idBitacora + "'>Ver Detalles</a></TD>"
                + // http://10.53.33.83/checklist/central/detalleChecklistExpancionImperdonable.htm?idBitacora=26282
                "</TR>"
                + "</TABLE>"
                + // ITEMS
                "<TABLE style=' width:100%; height:35px; text-align:center'>" + "<TR>"
                + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#F0EFF0;'> <strong style='text-align:center; font-size: 15px;'>"
                + totalItems + "</strong><br></br><br></br> Items Revisados </TD>"
                + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#F0EFF0;'> <strong style='text-align:center; font-size: 15px;'>"
                + itemsNoConsiderados + "</strong><br></br><br></br> Items No Considerados </TD>"
                + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#D8D8D8;'><strong style='text-align:center; font-size: 15px;'>"
                + itemsAprobados
                + "</strong><br></br> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='20' height='20'/>&nbsp; Items Aprobados </TD>"
                + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#6B696E; color:white;'><strong style='text-align:center; font-size: 15px;'>"
                + itemsPorRevisar
                + "</strong><br></br> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='20' height='20'/>&nbsp; Items Por Atender </TD>"
                + "</TR>"
                + // ITEMS
                "</TABLE>"
                + "<div id='detalle'> <br></br><br></br> Detalles de la Entrega </div>"
                + // Div items
                "<TABLE style='width:100%; text-align:center' cellspacing='5'>"
                + " <TR>"
                + // INICIO COMUNES
                " <TD style='width:50%; height:202px; '> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaAreasComunes.png' width='25' height=20'/>"
                + "&nbsp; Áreas Comunes"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoCom + "' width='60' height='25'/>"
                + "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format((Double.parseDouble(comTotalPon))) + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaComunes.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;' >items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + comResSi + "</td>" + "<td>" + comSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>"
                + comCalifCant + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + comResNo + "</td>" + "<td>" + comNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>"
                + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=1'>Ver Detalles</a>"
                + " </div>"
                + "</TD>"
                + // FIN COMUNES
                // INICIO Credito y  Cobranza
                "<TD style='width:50%; height:202px; background-color:white;'> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaBAZ.png' width='35' height='25'/>"
                + "&nbsp; Credito y Cobranza"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoCyC + "' width='60' height='25'/>"
                + "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format((Double.parseDouble(cYcTotalPon))) + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaBanco.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;'>items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + cYcResSi + "</td>" + "<td>" + cYcSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>"
                + cYcCalifCant + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + cYcResNo + "</td>" + "<td>" + cYcNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>"
                + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=6'>Ver Detalles</a>"
                + " </div>"
                + "</TD> "
                + // FIN Credito y Cobranza
                "</TR>"
                + "<TR>"
                + // INICIO GENERALES
                "<TD style='width:50%; height:202px; background-color:white;'> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaGenerales.png' width='25' height='25'/>"
                + "&nbsp; Generales"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoGen + "' width='60' height='25'/>"
                + "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format((Double.parseDouble(genTotalPon))) + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaGenerales.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;'>items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + genResSi + "</td>" + "<td>" + genSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>"
                + genCalifCant + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + genResNo + "</td>" + "<td>" + genNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>"
                + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=4'>Ver Detalles</a>"
                + " </div>"
                + "</TD>"
                + // FIN GENERALES
                // INICIO TOTAL
                "<TD style='width:50%; height:202px; background-color:#F0EFF0;'> "
                + "<div style='width:100%; height:202px;'>"
                + // Titulo
                "<br></br>" + "<div>" + "<h5 align='center'>"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "TOTAL"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/" + logoTotal + "' width='60' height='25'/>"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "Total: " + formatDecimales.format(((totalTotal)))
                + "</h5>" + "</div>"
                + // Div Imagen
                "<div>" + "<div style='float: left; width: 200px;'>" + "<img src='" + graficaTotal.getAbsolutePath()
                + "' width='180px' height='90%' />" + "</div>"
                + // Mini tabla
                "<div style='float: right; width: 80px; bottom:0; padding-top: 50px; '>"
                + "<table cellspacing='0'>" + "<tr  style='color:white; background-color:black;'>"
                + "<th style='color:white; background-color:#F0EFF0; border: 0 px solid #F0EFF'></th>"
                + "<th style='text-align:center; font-size: 8px;'>items</th>"
                + "<th style='text-align:center; font-size: 8px;'>items %</th>"
                + "<th style='text-align:center; font-size: 8px;'>Calificación</th>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:#F0EFF0; border: 0 px solid #F0EFF'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados02.png' width='15' height='15'/> </td>"
                + "<td>" + totalResSi + "</td>" + "<td>" + totalSiPor + "</td>"
                + "<td rowspan='2' style='background-color:#F0EFF0;'>" + totalCalifCant
                + "</td>" + "</tr>" + "<tr>"
                + "<td style='color:white; background-color:#F0EFF0; border: 0 px solid #F0EFF'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender02.png' width='15' height='15'/> </td>"
                + "<td>" + totalResNo + "</td>" + "<td>" + totalNoPor + "</td>" + "</tr>" + "</table>"
                + "</div>"
                + "</div>"
                + "<br></br>"
                + "<a target='_blank' style='color:black;' href='http://10.53.33.83/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="
                + idBitacora + "&tipo=6'>Ver Detalles</a>"
                + " </div>"
                + "</TD>"
                + // FIN TOTAL
                "</TR>"
                + "</TABLE>"
                + "<table cellspacing='0' align='right'>" + "<tr align='center'>"
                + "<td style=' border: 0.25px solid white;'>Código</td>"
                + "<td style=' border: 0.25px solid white; text-align:center; background-color:black; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>pésimo</td>"
                + "<td style='border: 0.25px solid white;  text-align:center; background-color:#FE003D; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>en peligro</td>"
                + "<td style='border: 0.25px solid white;  text-align:center; background-color:#F7CA44; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>alerta</td>"
                + "<td style='border: 0.25px solid white;  text-align:center; background-color:#006240; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>bien</td>"
                + "</tr>" + "</table>"
                + "<strong style='text-align:center; font-size: 10px;'>" + cadenaDesactivados + "</strong>"
                + // Responsables de Obra
                tablaResponsables
                + // Fin Responables de Obra
                "</body>" + "</html>";

        return html;

    }
    ////

    public File grafica(int tipo, double aprobados, double porAtender) {
        // Tipo 1 Backround Blanco - Tipo 2 Backroung Gris
        Color gris = new Color(240, 239, 240);

        DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("Items Aprobados", aprobados);
        dataset.setValue("Items Por Atender", porAtender);

        RingPlot plot = new RingPlot(dataset);

        Color vrd = new Color(227, 227, 227);
        Color rojo = new Color(142, 142, 142);

        plot.setSectionPaint("Items Aprobados", vrd);
        plot.setSectionPaint("Items Por Atender", rojo);

        JFreeChart chart = new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, plot, true);

        // Fondo de la Gráfica
        if (tipo == 1) {
            chart.getPlot().setBackgroundPaint(Color.white);
        } else {
            chart.getPlot().setBackgroundPaint(gris);
        }

        // plot.setCenterTextMode(CenterTextMode.VALUE);
        Font font1 = new Font(null, Font.BOLD, 45);
        plot.setCenterTextMode(CenterTextMode.FIXED);
        plot.setCenterText(aprobados + "%");

        plot.setCenterTextFont(font1);
        plot.setCenterTextColor(Color.BLACK);

        plot.setOutlineVisible(false);
        plot.setSectionDepth(0.35);
        // plot.setSectionOutlinesVisible(true);
        // plot.setSimpleLabels(true);
        plot.setOuterSeparatorExtension(0);
        plot.setInnerSeparatorExtension(0);

        Font font = new Font("FONT", 0, 22);
        plot.setLabelFont(font);

        chart.getLegend().setFrame(BlockBorder.NONE);
        // chart.getLegend().setPosition(RectangleEdge.);

        // FONDO DE LA GRÁFICA
        if (tipo == 1) {
            chart.setBackgroundPaint(java.awt.Color.white);
            plot.setLabelBackgroundPaint(Color.white);
        } else {
            chart.setBackgroundPaint(gris);
            plot.setLabelBackgroundPaint(gris);

        }

        return setGraficaTemp(chart);
    }

    public File setGraficaTemp(JFreeChart chart) {

        File temp = null;
        // create a temp file
        try {
            temp = File.createTempFile("grafica", ".png");
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        BufferedImage chartImage = chart.createBufferedImage(600, 400, null);

        try {
            ImageIO.write(chartImage, "png", temp);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return temp;

    }
    // FIN MAIL EXPANSIÓN NUEVO FORMATO

    public String getColorExpansion(double num) {

        if (num >= 90) {

            return "#006240";

        } else if (num >= 70) {

            return "#F7CA44";

        } else if (num >= 51) {

            return "#FE003D";

        } else {

            return "#000000";

        }

    }

    public String getLogoExpansion(double num, int imperdonable) {

        if (imperdonable > 0) {

            return "vt01.png";

        } else {

            if (num >= 90) {

                return "vt03.png";

            } else if (num >= 70) {

                return "vt04.png";

            } else if (num >= 51) {

                return "vt02.png";

            } else {

                return "vt01.png";

            }

        }

    }

    public String generaActaOCC(String idBitacora, String tipoVisita, String fecha,
            String economico, String sucursal, String tipo, String territorio, String zona,
            List<FirmaCheckDTO> firmas, String imgFachada, String tipoNegocio) {
        String direccion = FRQConstantes.getRutaImagen();
        String adjuntoRuta = "";
        String strCorreoAdjunto = "";
        List<String> rutas = new ArrayList<>();

        DecimalFormat formatDecimales = new DecimalFormat("###.##");
        //Se crea metodo para calcular grafica y ponderaciones etc
        ExpansionActaBI acta = new ExpansionActaBI(idBitacora, tipoNegocio);

        //Precarga de datos
        //Se obtiene numero imperdonables
        String numImperdonables = "" + acta.getNumImperdonablesRecorrido();

        // Se valida el tipo de recorrido
        if (tipoVisita.equals("0")) {
            tipoVisita = "Primer Recorrido";
        } else {

            tipoVisita = "Soft Openning";

        }

        //http://10.53.33.82/
        //imgFachada="//franquicia/ImagSucursal/ej_suc2.PNG";
        Resource correoColab = UtilResource.getResourceFromInternet("mailAdjActaExpansion.html");

        try {

            strCorreoAdjunto = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoAdjunto = strCorreoAdjunto.replace("@fachadaSuc", direccion + imgFachada);
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreSucursal", sucursal);
            strCorreoAdjunto = strCorreoAdjunto.replace("@zona", zona);
            strCorreoAdjunto = strCorreoAdjunto.replace("@region", territorio);
            strCorreoAdjunto = strCorreoAdjunto.replace("@numEco", economico);
            strCorreoAdjunto = strCorreoAdjunto.replace("@tipoVisita", tipoVisita);
            strCorreoAdjunto = strCorreoAdjunto.replace("@imagenesBaner", acta.getImgBaner());///*+acta.generaImagenBaner()*/);
            strCorreoAdjunto = strCorreoAdjunto.replace("@status", acta.getOperable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@colorTexto", acta.getColorTexto());
            strCorreoAdjunto = strCorreoAdjunto.replace("@colorOperable", acta.getColorOperable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreFirma1", firmas.get(0).getResponsable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreFirma2", firmas.get(1).getResponsable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreFirma3", firmas.get(2).getResponsable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreFirma4", firmas.get(3).getResponsable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreFirma5", firmas.get(4).getResponsable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreFirma6", firmas.get(5).getResponsable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreFirma7", firmas.get(6).getResponsable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@firma1", acta.generaFirma(firmas.get(0)));
            strCorreoAdjunto = strCorreoAdjunto.replace("@firma2", acta.generaFirma(firmas.get(1)));
            strCorreoAdjunto = strCorreoAdjunto.replace("@firma3", acta.generaFirma(firmas.get(2)));
            strCorreoAdjunto = strCorreoAdjunto.replace("@firma4", acta.generaFirma(firmas.get(3)));
            strCorreoAdjunto = strCorreoAdjunto.replace("@firma5", acta.generaFirma(firmas.get(4)));
            strCorreoAdjunto = strCorreoAdjunto.replace("@firma6", acta.generaFirma(firmas.get(5)));
            strCorreoAdjunto = strCorreoAdjunto.replace("@firma7", acta.generaFirma(firmas.get(6)));
            strCorreoAdjunto = strCorreoAdjunto.replace("@calificacionCalculda", formatDecimales.format(acta.getPreCalificacionNum()));
            strCorreoAdjunto = strCorreoAdjunto.replace("@itemRevisados", acta.getTotalItems());
            strCorreoAdjunto = strCorreoAdjunto.replace("@itemsAprovados", acta.getItemsAprobados());
            strCorreoAdjunto = strCorreoAdjunto.replace("@itemsPorAtender", acta.getItemsPorRevisar());
            strCorreoAdjunto = strCorreoAdjunto.replace("@itemsNoConsiderado", acta.getItemsNoConsiderados());
            strCorreoAdjunto = strCorreoAdjunto.replace("@imperdonables", "" + numImperdonables);
            String tabla1 = "<tr height=\"180px\" style=\"border: 2px solid black;\">\n"
                    + "             <td style=\"\">@grafica1</td>\n"
                    + "             <td style=\"\">@grafica2</td>\n"
                    + "         </tr>\n"
                    + "         <tr height=\"180px\" >\n"
                    + "             <td style=\"\">@grafica3</td>\n"
                    + "             <td style=\"\">@grafica4</td>\n"
                    + "         </tr>";

            strCorreoAdjunto = strCorreoAdjunto.replace("@tablaGraficasNegocio", tabla1);
            strCorreoAdjunto = strCorreoAdjunto.replace("@grafica1", acta.obtenGrafica("CYC"));
            strCorreoAdjunto = strCorreoAdjunto.replace("@grafica2", acta.obtenGrafica("AREAS"));
            strCorreoAdjunto = strCorreoAdjunto.replace("@grafica3", acta.obtenGrafica("GENERALES"));
            strCorreoAdjunto = strCorreoAdjunto.replace("@grafica4", acta.obtenGrafica("TOTALES"));

            logger.info(strCorreoAdjunto);

            // Se generan PDFS
            List<File> adjunto = new ArrayList<>();
            List<String> docHtml = new ArrayList<String>();

            //Pagina Indice
            docHtml = new ArrayList<String>();
            docHtml.add(acta.paginaIndiceOCC());
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath()); // se imprime ruta local de pagina Indice

            //Acta Graficos
            docHtml = new ArrayList<String>();
            docHtml.add(strCorreoAdjunto);
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath()); // se imprime ruta local de acta graficos

            //Acta Imperdonables
            docHtml = new ArrayList<String>();
            docHtml.add(acta.cadenaImperdonables(idBitacora));
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath()); // se imprime ruta local de acta Imperdonables

            //Repetir por cada clasif
            docHtml = new ArrayList<String>();
            docHtml.add(acta.obtieneHTMLDetalleClasif("CYC"));
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath());

            docHtml = new ArrayList<String>();
            docHtml.add(acta.obtieneHTMLDetalleClasif("AREAS"));
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath());

            docHtml = new ArrayList<String>();
            docHtml.add(acta.obtieneHTMLDetalleClasif("GENERALES"));
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath());

            //Se eliminan Archivos temporales
            acta.getGraficaComunes().delete();
            acta.getGraficaGenerales().delete();
            acta.getGraficaTotal().delete();
            acta.getGraficaCyC().delete();

            //Se juntan PDF's
            adjuntoRuta = acta.mergePDF(rutas);

            for (int i = 0; i < adjunto.size(); i++) {
                adjunto.get(i).delete();
            }

        } catch (IOException e) {

            logger.info("No se pudo extraer o sustituir algun dato del documento PDF " + e);

        }

        return adjuntoRuta;

    }

    public String generaActa(String idBitacora, String tipoVisita, String fecha,
            String economico, String sucursal, String tipo, String territorio, String zona,
            List<FirmaCheckDTO> firmas, String imgFachada, String tipoNegocio) {
        String direccion = FRQConstantes.getRutaImagen();
        String adjuntoRuta = "";
        String strCorreoAdjunto = "";
        List<String> rutas = new ArrayList<>();

        DecimalFormat formatDecimales = new DecimalFormat("###.##");
        //Se crea metodo para calcular grafica y ponderaciones etc
        ExpansionActaBI acta = new ExpansionActaBI(idBitacora, tipoNegocio);

        //Precarga de datos
        //Se obtiene numero imperdonables
        String numImperdonables = "" + acta.getNumImperdonablesRecorrido();

        // Se valida el tipo de recorrido
        if (tipoVisita.equals("0")) {
            tipoVisita = "Primer Recorrido";
        } else {

            tipoVisita = "Soft Openning";

        }

        //http://10.53.33.82/
        //imgFachada="//franquicia/ImagSucursal/ej_suc2.PNG";
        Resource correoColab = UtilResource.getResourceFromInternet("mailAdjActaExpansion.html");

        try {

            strCorreoAdjunto = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoAdjunto = strCorreoAdjunto.replace("@fachadaSuc", direccion + imgFachada);
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreSucursal", sucursal);
            strCorreoAdjunto = strCorreoAdjunto.replace("@zona", zona);
            strCorreoAdjunto = strCorreoAdjunto.replace("@region", territorio);
            strCorreoAdjunto = strCorreoAdjunto.replace("@numEco", economico);
            strCorreoAdjunto = strCorreoAdjunto.replace("@tipoVisita", tipoVisita);
            strCorreoAdjunto = strCorreoAdjunto.replace("@imagenesBaner", acta.getImgBaner());///*+acta.generaImagenBaner()*/);
            strCorreoAdjunto = strCorreoAdjunto.replace("@status", acta.getOperable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@colorTexto", acta.getColorTexto());
            strCorreoAdjunto = strCorreoAdjunto.replace("@colorOperable", acta.getColorOperable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreFirma1", firmas.get(0).getResponsable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreFirma2", firmas.get(1).getResponsable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreFirma3", firmas.get(2).getResponsable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreFirma4", firmas.get(3).getResponsable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreFirma5", firmas.get(4).getResponsable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreFirma6", firmas.get(5).getResponsable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreFirma7", firmas.get(6).getResponsable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@firma1", acta.generaFirma(firmas.get(0)));
            strCorreoAdjunto = strCorreoAdjunto.replace("@firma2", acta.generaFirma(firmas.get(1)));
            strCorreoAdjunto = strCorreoAdjunto.replace("@firma3", acta.generaFirma(firmas.get(2)));
            strCorreoAdjunto = strCorreoAdjunto.replace("@firma4", acta.generaFirma(firmas.get(3)));
            strCorreoAdjunto = strCorreoAdjunto.replace("@firma5", acta.generaFirma(firmas.get(4)));
            strCorreoAdjunto = strCorreoAdjunto.replace("@firma6", acta.generaFirma(firmas.get(5)));
            strCorreoAdjunto = strCorreoAdjunto.replace("@firma7", acta.generaFirma(firmas.get(6)));
            strCorreoAdjunto = strCorreoAdjunto.replace("@calificacionCalculda", formatDecimales.format(acta.getPreCalificacionNum()));
            strCorreoAdjunto = strCorreoAdjunto.replace("@itemRevisados", acta.getTotalItems());
            strCorreoAdjunto = strCorreoAdjunto.replace("@itemsAprovados", acta.getItemsAprobados());
            strCorreoAdjunto = strCorreoAdjunto.replace("@itemsPorAtender", acta.getItemsPorRevisar());
            strCorreoAdjunto = strCorreoAdjunto.replace("@itemsNoConsiderado", acta.getItemsNoConsiderados());
            strCorreoAdjunto = strCorreoAdjunto.replace("@imperdonables", "" + numImperdonables);
            String tabla = "<tr height=\"180px\" style=\"border: 2px solid black;\">\n"
                    + "             <td style=\"\">@grafica1</td>\n"
                    + "             <td style=\"\">@grafica2</td>\n"
                    + "             <td style=\"\">@grafica3</td>\n"
                    + "         </tr>\n"
                    + "         <tr height=\"180px\" >\n"
                    + "             <td style=\"\">@grafica4</td>\n"
                    + "             <td style=\"\">@grafica5</td>\n"
                    + "             <td style=\"\">@grafica6</td>\n"
                    + "         </tr>";

            strCorreoAdjunto = strCorreoAdjunto.replace("@tablaGraficasNegocio", tabla);
            strCorreoAdjunto = strCorreoAdjunto.replace("@grafica1", acta.obtenGrafica("EKT"));
            strCorreoAdjunto = strCorreoAdjunto.replace("@grafica2", acta.obtenGrafica("BAZ"));
            strCorreoAdjunto = strCorreoAdjunto.replace("@grafica3", acta.obtenGrafica("PP"));
            strCorreoAdjunto = strCorreoAdjunto.replace("@grafica4", acta.obtenGrafica("AREAS"));
            strCorreoAdjunto = strCorreoAdjunto.replace("@grafica5", acta.obtenGrafica("GENERALES"));
            strCorreoAdjunto = strCorreoAdjunto.replace("@grafica6", acta.obtenGrafica("TOTALES"));

            logger.info(strCorreoAdjunto);

            // Se generan PDFS
            List<File> adjunto = new ArrayList<>();
            List<String> docHtml = new ArrayList<String>();

            //Pagina Indice
            docHtml = new ArrayList<String>();
            docHtml.add(acta.paginaIndice());
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath()); // se imprime ruta local de pagina Indice

            //Acta Graficos
            docHtml = new ArrayList<String>();
            docHtml.add(strCorreoAdjunto);
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath()); // se imprime ruta local de acta graficos

            //Acta Imperdonables
            docHtml = new ArrayList<String>();
            docHtml.add(acta.cadenaImperdonables(idBitacora));
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath()); // se imprime ruta local de acta Imperdonables

            //Repetir por cada clasif
            docHtml = new ArrayList<String>();
            docHtml.add(acta.obtieneHTMLDetalleClasif("EKT"));
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath());

            docHtml = new ArrayList<String>();
            docHtml.add(acta.obtieneHTMLDetalleClasif("BAZ"));
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath());

            docHtml = new ArrayList<String>();
            docHtml.add(acta.obtieneHTMLDetalleClasif("PP"));
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath());

            docHtml = new ArrayList<String>();
            docHtml.add(acta.obtieneHTMLDetalleClasif("AREAS"));
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath());

            docHtml = new ArrayList<String>();
            docHtml.add(acta.obtieneHTMLDetalleClasif("GENERALES"));
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath());

            //Se eliminan Archivos temporales
            acta.getGraficaBanco().delete();
            acta.getGraficaComunes().delete();
            acta.getGraficaElektra().delete();
            acta.getGraficaGenerales().delete();
            acta.getGraficaPresta().delete();
            acta.getGraficaTotal().delete();

            //Se juntan PDF's
            adjuntoRuta = acta.mergePDF(rutas);

            for (int i = 0; i < adjunto.size(); i++) {
                adjunto.get(i).delete();
            }

        } catch (IOException e) {

            logger.info("No se pudo extraer o sustituir algun dato del documento PDF " + e);

        }

        return adjuntoRuta;

    }

    public String generaActaGeneral(String idBitacora, String tipoVisita, String fecha,
            String economico, String sucursal, String tipo, String territorio, String zona,
            List<FirmaCheckDTO> firmas, String imgFachada, String tipoNegocio) {
        String direccion = FRQConstantes.getRutaImagen();
        String adjuntoRuta = "";
        String strCorreoAdjunto = "";
        List<String> rutas = new ArrayList<>();

        DecimalFormat formatDecimales = new DecimalFormat("###.##");
        //Se crea metodo para calcular grafica y ponderaciones etc
        ExpansionActa2BI acta = new ExpansionActa2BI(idBitacora, tipoNegocio, true);

        //Precarga de datos
        //Se obtiene numero imperdonables
        String numImperdonables = "" + acta.getNumImperdonablesRecorrido();

        // Se valida el tipo de recorrido
        if (tipoVisita.equals("0")) {
            tipoVisita = "Primer Recorrido";
        } else {

            tipoVisita = "Soft Openning";

        }

        //http://10.53.33.82/
        //imgFachada="//franquicia/ImagSucursal/ej_suc2.PNG";
        Resource correoColab = UtilResource.getResourceFromInternet("mailAdjActaExpansion.html");

        try {

            strCorreoAdjunto = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoAdjunto = strCorreoAdjunto.replace("@fachadaSuc", direccion + imgFachada);
            strCorreoAdjunto = strCorreoAdjunto.replace("@nombreSucursal", sucursal);
            strCorreoAdjunto = strCorreoAdjunto.replace("@zona", zona);
            strCorreoAdjunto = strCorreoAdjunto.replace("@region", territorio);
            strCorreoAdjunto = strCorreoAdjunto.replace("@numEco", economico);
            strCorreoAdjunto = strCorreoAdjunto.replace("@tipoVisita", tipoVisita);
            strCorreoAdjunto = strCorreoAdjunto.replace("@imagenesBaner", acta.getImgBaner());///*+acta.generaImagenBaner()*/);
            strCorreoAdjunto = strCorreoAdjunto.replace("@status", acta.getOperable());
            strCorreoAdjunto = strCorreoAdjunto.replace("@colorTexto", acta.getColorTexto());
            strCorreoAdjunto = strCorreoAdjunto.replace("@colorOperable", acta.getColorOperable());
            for (int i = 0; i < firmas.size(); i++) {
                strCorreoAdjunto = strCorreoAdjunto.replace("@firma" + (i + 1), acta.generaFirma(firmas.get(i)));
                strCorreoAdjunto = strCorreoAdjunto.replace("@nombreFirma" + (i + 1), firmas.get(i).getResponsable());
            }
            strCorreoAdjunto = strCorreoAdjunto.replace("@calificacionCalculda", formatDecimales.format(acta.getPreCalificacionNum()));
            strCorreoAdjunto = strCorreoAdjunto.replace("@itemRevisados", acta.getTotalItems());
            strCorreoAdjunto = strCorreoAdjunto.replace("@itemsAprovados", acta.getItemsAprobados());
            strCorreoAdjunto = strCorreoAdjunto.replace("@itemsPorAtender", acta.getItemsPorRevisar());
            strCorreoAdjunto = strCorreoAdjunto.replace("@itemsNoConsiderado", acta.getItemsNoConsiderados());
            strCorreoAdjunto = strCorreoAdjunto.replace("@imperdonables", "" + numImperdonables);
            String tabla1 = "";
            if (tipoNegocio.equalsIgnoreCase("OCC")) {
                tabla1 = "<tr height=\"180px\" style=\"border: 2px solid black;\">"
                        + "         <td WIDTH=\"50%\" style='border: 0.25px solid gray;'>@grafica1</td>"
                        + "         <td WIDTH=\"50%\" style='border: 0.25px solid gray;'>@grafica2</td>"
                        + "   </tr>"
                        + "   <tr height=\"180px\">"
                        + "         <td WIDTH=\"50%\" style='border: 0.25px solid gray;'>@grafica3</td>"
                        + "         <td WIDTH=\"50%\" style='border: 0.25px solid gray;'>@grafica4</td>"
                        + "   </tr>";
            } else {
                tabla1 = "<tr height=\"180px\" style=\"border: 2px solid black;\">"
                        + "         <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica1</td>"
                        + "         <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica2</td>"
                        + "         <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica3</td>"
                        + "   </tr>"
                        + "   <tr height=\"180px\" >"
                        + "         <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica4</td>"
                        + "         <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica5</td>"
                        + "         <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica6</td>"
                        + "   </tr>";
            }
            /*
             if(tipoNegocio.equalsIgnoreCase("OCC")) {
             tabla1 = "<tr height=\"180px\" style=\"border: 2px solid black;\">\n" +
             "              <td style=\"\">@grafica1</td>\n" +
             "              <td style=\"\">@grafica2</td>\n" +
             "          </tr>\n" +
             "          <tr height=\"180px\" >\n" +
             "              <td style=\"\">@grafica3</td>\n" +
             "              <td style=\"\">@grafica4</td>\n" +
             "          </tr>" ;
             }
             else {
             tabla1 = "<tr height=\"180px\" style=\"border: 2px solid black;\">\n" +
             "              <td style=\"\">@grafica1</td>\n" +
             "              <td style=\"\">@grafica2</td>\n" +
             "              <td style=\"\">@grafica3</td>\n" +
             "          </tr>\n" +
             "          <tr height=\"180px\" >\n" +
             "              <td style=\"\">@grafica4</td>\n" +
             "              <td style=\"\">@grafica5</td>\n" +
             "              <td style=\"\">@grafica6</td>\n" +
             "          </tr>" ;
             }
             */

            strCorreoAdjunto = strCorreoAdjunto.replace("@tablaGraficasNegocio", tabla1);

            for (int i = 0; i < acta.getCuadroGrafica().length; i++) {

                strCorreoAdjunto = strCorreoAdjunto.replace("@grafica" + (i + 1), acta.getCuadroGrafica()[i]);
            }
            //strCorreoAdjunto = strCorreoAdjunto.replace("@grafica5",acta.getCuadroGrafica()[acta.getCuadroGrafica().length-1]);
            //strCorreoAdjunto = strCorreoAdjunto.replace("@grafica6",acta.getCuadroGrafica()[acta.getCuadroGrafica().length-1]);
            strCorreoAdjunto = strCorreoAdjunto.replace("@verDetalle", "");
            strCorreoAdjunto = strCorreoAdjunto.replace("125%", "100%");
            strCorreoAdjunto = strCorreoAdjunto.replace("30%", "10%");
            strCorreoAdjunto = strCorreoAdjunto.replace("cellspacing='5'", "cellspacing='1'");
            strCorreoAdjunto = strCorreoAdjunto.replace("HEIGHT=\"100%\"", "");
            strCorreoAdjunto = strCorreoAdjunto.replace("width='50%'", "width='30%'");
            strCorreoAdjunto = strCorreoAdjunto.replace("<span id=\"totalPondClasif\">", "<span id=\"totalPondClasif\" style='font-size: 60%;'>");//
            strCorreoAdjunto = strCorreoAdjunto.replace("<span id=\"detalleClasif\">", "<span id=\"detalleClasif\" style='font-size: 75%;'>");

            logger.info(strCorreoAdjunto);

            // Se generan PDFS
            List<File> adjunto = new ArrayList<>();
            List<String> docHtml = new ArrayList<String>();

            //Pagina Indice
            docHtml = new ArrayList<String>();
            docHtml.add(acta.getPaginaIndice());
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath()); // se imprime ruta local de pagina Indice

            //Acta Graficos
            docHtml = new ArrayList<String>();
            docHtml.add(strCorreoAdjunto);
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath()); // se imprime ruta local de acta graficos

            //Acta Imperdonables
            docHtml = new ArrayList<String>();
            docHtml.add(acta.getCadenaImperdonables());
            adjunto.add(escribirPDFHtml(docHtml));
            rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
            System.out.println(adjunto.get(adjunto.size() - 1).getAbsolutePath()); // se imprime ruta local de acta Imperdonables

            //Repetir por cada clasif
            try {
                for (int i = 0; i < acta.getGeneraHTMLDetalleClasif().length; i++) {
                    docHtml = new ArrayList<String>();
                    docHtml.add(acta.getGeneraHTMLDetalleClasif()[i]);
                    adjunto.add(escribirPDFHtml(docHtml));
                    rutas.add(adjunto.get(adjunto.size() - 1).getAbsolutePath());
                    System.out.println("Clasificacion: " + acta.getDetalleClasif()[i] + " .." + adjunto.get(adjunto.size() - 1).getAbsolutePath());
                }
            } catch (Exception e) {
                logger.info("No se pudo generar pagina a detalle clasif " + e.getMessage());
            }

            //Se juntan PDF's
            adjuntoRuta = acta.mergePDF(rutas);

        } catch (IOException e) {

            logger.info("No se pudo extraer o sustituir algun dato del documento PDF " + e);

        }

        return adjuntoRuta;

    }

    //ActaAlexanderConSandra
    public void actaEntregaSucursal(String idBitacora, String tipoVisita, String fecha,
            String economico, String sucursal, String tipo, String territorio, String zona, List<String> imperdonables,
            List<FirmaCheckDTO> firmas, List<ReporteChecksExpDTO> incidencias, String aperturable, String imgFachada, String tipoNegocio) {

        String destinatario = "";
        String strCorreo = "";
        List<String> listaCopiados = new ArrayList<String>();

        /* Se comenta para que no se envien correos a los destinatarios de la app  - erick
         for (int i = 0; i < firmas.size(); i++) {
         listaCopiados.add(firmas.get(i).getCorreo());
         }
         */
        strCorreo = generaPortadaActa(idBitacora, tipoVisita, fecha, economico, sucursal, tipo, territorio, zona, imperdonables,
                firmas, imgFachada, tipoNegocio);
        logger.info(strCorreo);

        try {
            destinatario = "alejandro.morales@elektra.com.mx";
            copiados = listaCopiados;
            copiados.add("alejandro.morales@elektra.com.mx");

            //copiados.add("luis.gomezv@elektra.com.mx");
            copiados.add("erick.carrasco@elektra.com.mx");
            //copiados.add("rafael.reyes@elektra.com.mx");
            //copiados.add("jefloresh@bancoazteca.com.mx");
            //copiados.add("alfredo.gordillo@elektra.com.mx");
            //copiados.add("mareyesp@elektra.com.mx");
            //copiados.add("ssalasg@elektra.com.mx");

            logger.info("DESTINATARIO - " + destinatario);
            logger.info("COPIADOS - " + destinatario);
            //logger.info("CUERPO - " + destinatario);

        } catch (Exception e) {
            logger.info("Algo ocurrió al consultar el correoLotus " + e);
            destinatario = "alejandro.morales@elektra.com.mx";

        }

        UtilMail.enviaCorreoAlexander(destinatario, copiados, "ACTA ENTREGA FRANQUICIA RECEPCIÓN DE SUCURSAL" + titulo,
                strCorreo, null);
        //UtilMail.enviaCorreoExpansion(destinatario, copiados, "" + titulo, strCorreoOwner, adjunto);
    }

    //ActaAlexanderSinSandra
    public void actaEntregaSucursal(String idBitacora, String tipoVisita, String fecha,
            String economico, String sucursal, String tipo, String territorio, String zona, List<String> imperdonables,
            List<FirmaCheckDTO> firmas, List<ReporteChecksExpDTO> incidencias, String aperturable, String imgFachada, String tipoNegocio, int envio) {

        String destinatario = "";
        String strCorreo = "";
        List<String> listaCopiados = new ArrayList<String>();

        /* Se comenta para que no se envien correos a los destinatarios de la app  - erick
         for (int i = 0; i < firmas.size(); i++) {
         listaCopiados.add(firmas.get(i).getCorreo());
         }
         */
        strCorreo = generaPortadaActa(idBitacora, tipoVisita, fecha, economico, sucursal, tipo, territorio, zona, imperdonables,
                firmas, imgFachada, tipoNegocio);
        logger.info(strCorreo);

        try {
            destinatario = "alejandro.morales@elektra.com.mx";
            copiados = listaCopiados;
            copiados.add("alejandro.morales@elektra.com.mx");
            copiados.add("luis.gomezv@elektra.com.mx");
            copiados.add("erick.carrasco@elektra.com.mx");

            logger.info("DESTINATARIO - " + destinatario);
            logger.info("COPIADOS - " + destinatario);
            //logger.info("CUERPO - " + destinatario);

        } catch (Exception e) {
            logger.info("Algo ocurrió al consultar el correoLotus " + e);
            destinatario = "alejandro.morales@elektra.com.mx";

        }

        UtilMail.enviaCorreoAlexander(destinatario, copiados, "ACTA ENTREGA FRANQUICIA RECEPCIÓN DE SUCURSAL" + titulo,
                strCorreo, null);
        //UtilMail.enviaCorreoExpansion(destinatario, copiados, "" + titulo, strCorreoOwner, adjunto);
    }

    public String generaPortadaActa(String idBitacora, String tipoVisita, String fecha,
            String economico, String sucursal, String tipo, String territorio, String zona, List<String> imperdonables,
            List<FirmaCheckDTO> firmas, String imgFachada, String tipoNegocio) {

        String direccion = FRQConstantes.getRutaImagen();
        String strPortada = "";
        DecimalFormat formatDecimales = new DecimalFormat("###.##");
        // Precarga de datos
        // Se obtiene numero imperdonables
        String[] fechas = fecha.split("/");
        String numImperdonables = imperdonables.size() + "";
        // Se valida el tipo de recorrido
        if (tipoVisita.equals("0")) {
            tipoVisita = "Primer Recorrido";
        } else {
            tipoVisita = "Soft Openning";
        }

        List<ChecklistHallazgosRepoDTO> lista = null;
        List<SucursalChecklistDTO> sucursal3 = null;
        int version = 0;
        try {
            lista = hallazgosRepoBI.obtieneDatosNego(idBitacora);
            if (lista != null && lista.size() > 0) {
                sucursal3 = sucursalChecklistBI.obtieneDatosSuc(lista.get(0).getCeco());

            }
        } catch (Exception e) {
        }
        version = sucursal3.get(0).getAux();

        //if(tipoNegocio.equalsIgnoreCase("EKT")) {
        if (version == 1) {
            // Se crea metodo para calcular grafica y ponderaciones etc
            ExpansionActaBI acta = new ExpansionActaBI(idBitacora, tipoNegocio);
            // http://10.53.33.82/
            //imgFachada = "//franquicia/ImagSucursal/fachada.PNG";
            Resource portada = UtilResource.getResourceFromInternet("mailActaExpansion.html");

            try {

                strPortada = UtilObject.inputStreamAsString(portada.getInputStream());
                strPortada = strPortada.replace("@colorOperable", acta.getColorOperable());
                strPortada = strPortada.replace("@nombreSucursal", sucursal);
                strPortada = strPortada.replace("@fachadaSuc", direccion + imgFachada);
                strPortada = strPortada.replace("@numEco", economico);
                strPortada = strPortada.replace("@tipoVisita", tipoVisita);
                strPortada = strPortada.replace("@territorio", territorio);
                strPortada = strPortada.replace("@zona", zona);
                strPortada = strPortada.replace("@fecha", fecha);
                strPortada = strPortada.replace("@calificacion", formatDecimales.format(acta.getPreCalificacionNum()));
                strPortada = strPortada.replace("@numImperdonables", numImperdonables);
                strPortada = strPortada.replace("@firma1", acta.generaFirma(firmas.get(0)));
                strPortada = strPortada.replace("@firma2", acta.generaFirma(firmas.get(1)));
                strPortada = strPortada.replace("@firma3", acta.generaFirma(firmas.get(2)));
                strPortada = strPortada.replace("@firma4", acta.generaFirma(firmas.get(3)));
                strPortada = strPortada.replace("@firma5", acta.generaFirma(firmas.get(4)));
                strPortada = strPortada.replace("@firma6", acta.generaFirma(firmas.get(5)));
                strPortada = strPortada.replace("@firma7", acta.generaFirma(firmas.get(6)));
                strPortada = strPortada.replace("@nombreFirma1", firmas.get(0).getResponsable());
                strPortada = strPortada.replace("@nombreFirma2", firmas.get(1).getResponsable());
                strPortada = strPortada.replace("@nombreFirma3", firmas.get(2).getResponsable());
                strPortada = strPortada.replace("@nombreFirma4", firmas.get(3).getResponsable());
                strPortada = strPortada.replace("@nombreFirma5", firmas.get(4).getResponsable());
                strPortada = strPortada.replace("@nombreFirma6", firmas.get(5).getResponsable());
                strPortada = strPortada.replace("@nombreFirma7", firmas.get(6).getResponsable());
                strPortada = strPortada.replace("@dia", ((fechas.length) == 3) ? fechas[0] : " ");
                strPortada = strPortada.replace("@mes", ((fechas.length) == 3) ? fechas[1] : " ");
                strPortada = strPortada.replace("@año", ((fechas.length) == 3) ? fechas[2] : " ");
                strPortada = strPortada.replace("@link", "<a href='" + FRQConstantes.getURLServer() + "/checklist/central/generaActaPDF.htm?idBitacora=" + idBitacora + "&tipoVisita=" + tipoVisita + "&fechaVisita=" + fecha + "&economico=" + economico + "&sucursal=" + sucursal + "&tipo=" + tipo + "&territorio=" + territorio + "&zona=" + zona + "&rutaImagen=" + imgFachada + "&tipoNegocio=" + tipoNegocio + "'>Descarga Reporte Completo</a>");

                if (imperdonables.size() > 0) {
                    String imp = "<div style=\"background-color: #ECEBEB; \">"
                            + "<table width=\"100%\">"
                            + "<tbody>";
                    for (int i = 0; i < imperdonables.size(); i++) {
                        imp += "<tr>"
                                + "<td style=\"text-align: center;\">"
                                + (i + 1) + ".- " + imperdonables.get(i)
                                + "</td>"
                                + "</tr>";
                    }

                    imp += "</tbody>"
                            + "</table>"
                            + "</div> <br></br>";
                    strPortada = strPortada.replace("@ImperdonablesTabla", imp);
                } else {
                    String impVacio = "<div style=\"background-color: #ECEBEB; \">"
                            + "<table width=\"100%\">"
                            + "<tbody>"
                            + "<tr>"
                            + "<td style=\"text-align: center;\">"
                            + "</td>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</div> <br></br>";

                    strPortada = strPortada.replace("@ImperdonablesTabla", impVacio);

                }

                System.out.println(strPortada);
                List<String> docHtml = new ArrayList<String>();
                docHtml.add(strPortada);
                File adjunto = escribirPDFHtml(docHtml);
                System.out.println(adjunto.getAbsolutePath());

            } catch (Exception e) {
                logger.info("No se pudo extraer o sustituir algun dato del documento PDF " + e);
            }
        } else {
            // Se crea metodo para calcular grafica y ponderaciones etc
            ExpansionActa2BI acta = new ExpansionActa2BI(idBitacora, tipoNegocio, false);
            // http://10.53.33.82/
            //imgFachada = "//franquicia/ImagSucursal/fachada.PNG";
            Resource portada = UtilResource.getResourceFromInternet("mailActaExpansion.html");
            numImperdonables = acta.getCalculoTotal().getListaPregImperdonablesClasif().size() + "";
            try {

                strPortada = UtilObject.inputStreamAsString(portada.getInputStream());
                strPortada = strPortada.replace("@colorOperable", acta.getColorOperable());
                strPortada = strPortada.replace("@nombreSucursal", sucursal);
                strPortada = strPortada.replace("@fachadaSuc", direccion + imgFachada);
                strPortada = strPortada.replace("@numEco", economico);
                strPortada = strPortada.replace("@tipoVisita", tipoVisita);
                strPortada = strPortada.replace("@territorio", territorio);
                strPortada = strPortada.replace("@zona", zona);
                strPortada = strPortada.replace("@fecha", fecha);
                strPortada = strPortada.replace("@calificacion", formatDecimales.format(acta.getPreCalificacionNum()));
                strPortada = strPortada.replace("@numImperdonables", numImperdonables);
                for (int i = 0; i < firmas.size(); i++) {
                    strPortada = strPortada.replace("@firma" + (i + 1), acta.generaFirma(firmas.get(i)));
                    strPortada = strPortada.replace("@nombreFirma" + (i + 1), firmas.get(i).getResponsable());
                }
                strPortada = strPortada.replace("@dia", ((fechas.length) == 3) ? fechas[0] : " ");
                strPortada = strPortada.replace("@mes", ((fechas.length) == 3) ? fechas[1] : " ");
                strPortada = strPortada.replace("@año", ((fechas.length) == 3) ? fechas[2] : " ");
                strPortada = strPortada.replace("@link", "<a href='" + FRQConstantes.getURLServer() + "/checklist/central/generaActaPDF.htm?idBitacora=" + idBitacora + "&tipoVisita=" + tipoVisita + "&fechaVisita=" + fecha + "&economico=" + economico + "&sucursal=" + sucursal + "&tipo=" + tipo + "&territorio=" + territorio + "&zona=" + zona + "&rutaImagen=" + imgFachada + "&tipoNegocio=" + tipoNegocio + "'>Descarga Reporte Completo</a>");

                if (acta.getCalculoTotal().getListaPregImperdonablesClasif().size() > 0) {
                    String imp = "<div style=\"background-color: #ECEBEB; \">"
                            + "<table width=\"100%\">"
                            + "<tbody>";
                    for (int i = 0; i < acta.getCalculoTotal().getListaPregImperdonablesClasif().size(); i++) {
                        imp += "<tr>"
                                + "<td style=\"text-align: center;\">"
                                + (i + 1) + ".- " + acta.getCalculoTotal().getListaPregImperdonablesClasif().get(i).getPregunta()
                                + "</td>"
                                + "</tr>";
                    }

                    imp += "</tbody>"
                            + "</table>"
                            + "</div> <br></br>";
                    strPortada = strPortada.replace("@ImperdonablesTabla", imp);
                } else {
                    String impVacio = "<div style=\"background-color: #ECEBEB; \">"
                            + "<table width=\"100%\">"
                            + "<tbody>"
                            + "<tr>"
                            + "<td style=\"text-align: center;\">"
                            + "</td>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</div> <br></br>";

                    strPortada = strPortada.replace("@ImperdonablesTabla", impVacio);

                }

                System.out.println(strPortada);
                List<String> docHtml = new ArrayList<String>();
                docHtml.add(strPortada);
                File adjunto = escribirPDFHtml(docHtml);
                System.out.println(adjunto.getAbsolutePath());

            } catch (Exception e) {
                logger.info("No se pudo extraer o sustituir algun dato del documento PDF " + e.getMessage());
            }
        }
        return strPortada;

    }

    public boolean sendMailToAsgCalidad(String ceco, String idUsuario, File attachedFile, boolean enviarSucural, ArrayList<String> correosAdicionales) throws DocumentException, IOException {
        logger.info("SI ENTRO!!");
        // copiados.add(user.getNombre());
        // logger.info("copiados- " + copiados);
        String titulo = "";
        titulo = "Informe de aseguramiento de calidad";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String html = "Este es el resultado de tu evaluación del día: " + sdf.format(new Date());

        try {
            Resource correoColab = UtilResource.getResourceFromInternet("mailAcuerdos.html");

            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoOwner = strCorreoOwner.replace("@Titulo", "");
            strCorreoOwner = strCorreoOwner.replaceAll("null", "Sin Dato");
            strCorreoOwner = strCorreoOwner.replace("@Lista", html);

            //CorreosLotus correosLotus = new CorreosLotus();
            CorreoOutlook correoOutlook = new CorreoOutlook();
            String destinatario = "";
            String correoSucursal = "";

            logger.info("_______________________________ANTES DE ESCRIBIR PDF_______________________________");
            // Ahora se le enviara un list con el size de los checklist a escribir
            //File adjunto = escribirPDFHtml(docHtml);
            logger.info("_______________________________DESPUES DE ESCRIBIR PDF_______________________________");

            try {

                if (enviarSucural) {
                    //destinatario = correosLotus.validaUsuarioLotusCorreos(idUsuario);
                    destinatario = "lrodriguezv@elektra.com.mx";
                    correoSucursal = correoOutlook.validaCorreoSuc(ceco);
                    System.out.println("CORREO SUC: " + correoSucursal);
                    //correoSucursal = "lrodriguezv@elektra.com.mx";

                } else {

                    destinatario = "lrodriguezv@elektra.com.mx";
                    correoSucursal = "lrodriguezv@elektra.com.mx";

                }

                System.out.println("enviarSucural" + enviarSucural);
                System.out.println("CORREO DESTINATARIO: " + destinatario);
                System.out.println("CORREO SUCURSAL: " + correoSucursal);

                //destinatario = "lrodriguezv@elektra.com.mx";
                //correoSucursal="lrodriguezv@elektra.com.mx";
                correosAdicionales.add(correoSucursal);

            } catch (Exception e) {
                logger.info("Algo ocurrió al consultar el correoLotus " + e);
                destinatario = "lrodriguezv@elektra.com.mx";
                correoSucursal = "lrodriguezv@elektra.com.mx";
            }

            // EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO VALIDA_USUARIO_LOTUS_CORREO
            UtilMail.enviaCorreo(destinatario, correosAdicionales, "" + titulo, strCorreoOwner, attachedFile);

        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
        }
        return true;

    }

    public boolean sendMailTransformacion(List<HallazgosTransfDTO> listaHallazgos, String fase, String asunto, String idCeco, String puesto, String lugar,
            String telefono, String comentario) {

        String destinatarios = "";
        String copiados = "";

        List<ParametroDTO> parametroDestinatarios = parametroBi.obtieneParametros("destinaTransATM");

        if (parametroDestinatarios != null && parametroDestinatarios.size() > 0) {

            destinatarios = parametroDestinatarios.get(0).getValor();

        } else {

            destinatarios = "";
        }

        List<String> listaDestinatarios = generaListaContactos(destinatarios);

        List<ParametroDTO> parametrocopiados = parametroBi.obtieneParametros("copiadosTransATM");

        if (parametrocopiados != null && parametroDestinatarios.size() > 0) {

            copiados = parametrocopiados.get(0).getValor();

        } else {
            copiados = "";
        }

        List<String> listaCopiados = generaListaContactos(copiados);


        /*copiados = new ArrayList<String>();
         copiados.add("jsepulveda@elektra.com.mx");
         copiados.add("erick.carrasco@elektra.com.mx");
         copiados.add("alejandro.morales@elektra.com.mx");*/
        String fechaPendientes = UtilDate.getSysDate("dd/MM/yyyy");
        String titulo = "";
        titulo = asunto;

        try {

            CorreosLotus correosLotus = new CorreosLotus();
            String destinatario = "";

            try {
                //destinatario = correosLotus.validaUsuarioLotusCorreos(user.getIdUsuario());
                destinatario = "alejandro.morales@elektra.com.mx";
                logger.info("correo destinatario - " + destinatario);
            } catch (Exception e) {
                logger.info("Algo ocurrió al consultar el correoLotus " + e);
                destinatario = "alejandro.morales@elektra.com.mx";
            }

            File adjunto = generaArchivoTransforma(listaHallazgos, fase);

            // EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO VALIDA_USUARIO_LOTUS_CORREO
            UtilMail.enviaCorreoTransformacion(listaDestinatarios, listaCopiados, "" + titulo, "", adjunto);

        } catch (Exception e) {
            logger.info("Algo ocurrió... " + e);
        }
        return true;
    }

    public File generaArchivoTransforma(List<HallazgosTransfDTO> listaHallazgos, String fase) {

        String nombreSucursal = "";
        String numeroEconimico = "";
        String fecha = "";
        String modulo = "";
        String supervisor = "";

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();

        Date date = calendar.getTime();

        fecha = sdf.format(date);

        if (listaHallazgos != null && listaHallazgos.size() > 0) {

            HallazgosTransfDTO hallazgo = listaHallazgos.get(0);

            nombreSucursal = hallazgo.getCecoNom();
            numeroEconimico = hallazgo.getCeco().substring(2, 6);
            modulo = hallazgo.getModulo();
            supervisor = hallazgo.getNomUsu();

        }

        String divHead
                = "<div style='background-color: #FAFAFA;'>"
                + "<table style=' width: 100%;'>"
                + "<tr>"
                + "<td style=' width: 85%; text-align: left;'>"
                + "<span style='padding-top: 10px; font-size: 18px;'><font face='Avenir_Next_Demi_Bold'><b>Entrega de Cajeros Automáticos Nueva Experiencia</b></font></span>"
                + "<br></br>"
                + "<span style='font-size: 16px; text-align: left;'><font face='AvenirNextLTPro-Medium'><i>Responsable:  " + supervisor + " </i></font></span>"
                + "</td>"
                + "<td style=' width: 15%; text-align: right'>"
                + "<img src='' height=50px></img>"
                + "</td>"
                + "</tr>"
                + "</table>"
                + "</div>"
                + "<div style='text-align: left; background-color: #F4C65C;padding-top: 3px;padding-bottom: 3px;'> </div>"
                + "<div style='margin-top: 50px'>"
                + "<table style=' width: 100%;'>"
                + "<tr >"
                + "<td style=' width: 30%; font-size: 16px;text-align: left' rowspan='3'><span style = ''><font face='Avenir_Next_Demi_Bold'><b>Transformaciones 2020</b></font></span> </td>"
                + "<td style='width: 70%; font-size: 16px; text-align: right'><font face='Avenir_Next_Demi_Bold'><b>Sucursal: </b></font><font face='AvenirNextLTPro-Medium'>" + nombreSucursal + "</font></td>"
                + "</tr>"
                + "<tr>"
                + "<td style='width: 70%; font-size: 16px; text-align: right'><font face='Avenir_Next_Demi_Bold'><b>No.Eco:</b></font> <font face='AvenirNextLTPro-Medium'>" + numeroEconimico + "</font></td>"
                + "</tr>"
                + "<tr>"
                + "<td style='width: 50%; text-align: right'><font face='Avenir_Next_Demi_Bold'><b>Fecha:</b> </font><font face='AvenirNextLTPro-Medium'>" + fecha + "</font> <font face='Avenir_Next_Demi_Bold'><b>Revisión:</b></font> <font face='AvenirNextLTPro-Medium'>" + fase + "</font></td>"
                + "</tr>"
                + "</table>"
                + "</div>"
                + "<div style='text-align: left; margin-top: 10px;' >"
                + "<p style='font-size: 18; text-align: left; padding-left: 25;' ><font face='Avenir_Next_Demi_Bold'><b>" + modulo + "</b> </font></p>"
                + "</div>"
                + "<div style='text-align: left; background-color: #202945 ; padding-top: 2px ; padding-bottom: 2px;'></div>";

        String theadTable = "<table style='width: 100%; border-spacing: 3px'  >"
                + "<tr style='background-color: #ADADAD; ' >"
                + "<th style='text-align: center; font-size: 13; color:white'; padding-top: 5; padding-bottom: 5;>#</th>"
                + "<th style='text-align: left; font-size: 13; color:white; padding-top: 5; padding-bottom: 5;'><font face='Avenir_Next_Demi_Bold'>CRITERIO DE MEDICIÓN</font></th>"
                + "<th style='text-align: center; font-size: 13; color:white; padding-top: 5; padding-bottom: 5;'><font face='Avenir_Next_Demi_Bold'>EVIDENCIA</font></th>"
                + "<th style='text-align: center; font-size: 13; color:white; padding-top: 5; padding-bottom: 5;'><font face='Avenir_Next_Demi_Bold'>MEDICIÓN</font></th>"
                + "<th style='text-align: center; font-size: 13; color:white'; padding-top: 5; padding-bottom: 5;><font face='Avenir_Next_Demi_Bold'>VALOR</font></th>"
                + "</tr>";

        String html = "<html>"
                + "<head>"
                + "<style>"
                + "/* Fuente Bold */\n"
                + "@font-face {\n"
                + " font-family: 'Avenir_Next_Demi_Bold';\n"
                + " src: \n"
                + "     url('/checklist/fonts/7s/eot/Avenir_Next_Demi_Bold.eot?#iefix')                 format('embedded-opentype'),\n"
                + "     url('/checklist/fonts/7s/otf/Avenir_Next_Demi_Bold.otf')                    format('opentype'),\n"
                + "     url('/checklist/fonts/7s/woff/Avenir_Next_Demi_Bold.woff')                  format('woff'),\n"
                + "     url('/checklist/fonts/7s/ttf/Avenir_Next_Demi_Bold.ttf')                    format('truetype'),\n"
                + "     url('/checklist/fonts/7s/svg/Avenir_Next_Demi_Bold.svg#Avenir_Next_Demi_Bold')      format('svg');\n"
                + " font-weight: normal;\n"
                + " font-style: normal;\n"
                + "}"
                + "@font-face {\n"
                + " font-family: 'AvenirNextLTPro-Medium';\n"
                + " src: \n"
                + "     url('/checklist/fonts/7s/eot/AvenirNextLTPro-Medium.eot?#iefix')            format('embedded-opentype'),  \n"
                + "     url('/checklist/fonts/7s/otf/AvenirNextLTPro-Medium.otf')                   format('opentype'),\n"
                + "     url('/checklist/fonts/7s/woff/AvenirNextLTPro-Medium.woff')                 format('woff'), \n"
                + "     url('/checklist/fonts/7s/ttf/AvenirNextLTPro-Medium.ttf')                   format('truetype'), \n"
                + "     url('/checklist/fonts/7s/svg/AvenirNextLTPro-Medium.svg#AvenirNextLTPro-Medium') format('svg');\n"
                + " font-weight: normal;\n"
                + " font-style: normal;\n"
                + "}"
                + "table {"
                + "border-collapse: separate;\n"
                + "width: 100%;"
                + "}"
                + "td {"
                + "text-align: left;"
                + "}"
                + "</style>"
                + "</head>"
                + "<body>"
                + "<div style=' position: fixed; right: 30px; left: 30px; bottom: 10px; background: white;   padding: 20px;  text-align: right; overflow-y: scroll;'>"
                + divHead
                + //"<div style='text-align: left;'>" +
                theadTable;

        int corte = 0;
        int totalRows = 0;
        for (HallazgosTransfDTO hallazgos : listaHallazgos) {

            //background-color: #f2f2f2;
            String colorRow = "#FAFAFA";
            corte++;

            if (corte % 2 == 0) {
                colorRow = "#EDEDED";

            }

            String nueva_ruta = "";

            if (hallazgos.getRuta() != null) {
                nueva_ruta = FRQConstantes.getRutaImagen() + hallazgos.getRuta().replace(".mp4", ".jpg");
                //nueva_ruta = "http://10.51.218.104"+hallazgos.getRuta().replace(".mp4", ".jpg");
            }

            html += "<tr width= '100%' height='180' style='background-color: " + colorRow + "; border-top: 5px solid '>"
                    + "<td style='font-size: 12; text-align: center; width:10%;'><font face='AvenirNextLTPro-Medium'>" + (totalRows + 1) + "</font></td>"
                    + "<td style='font-size: 12; width:30%; text-align: left; '><font face='AvenirNextLTPro-Medium'>" + hallazgos.getPreg() + "\n"
                    + "<br></br><br></br>" + hallazgos.getObs() + "</font></td>"
                    + "<td style='font-size: 12;text-align: center; width:30%;'>"
                    + "<div"
                    //+ "><img src='"+FRQConstantes.getRutaImagen()+hallazgos.getRuta()+"' height='150' width='150'  ></img></div>" +
                    + "><img src='" + nueva_ruta + "' height='150' width='150'  ></img></div>"
                    + "</td>"
                    + "<td style='font-size: 12; text-align: center; width:20%;'><font face='AvenirNextLTPro-Medium'>" + hallazgos.getRespuesta() + "</font></td>"
                    + "<td style='font-size: 12; text-align: center; width:10%;'><font face='AvenirNextLTPro-Medium'>" + hallazgos.getPonderacion() + "</font></td>"
                    + "</tr>";

            totalRows++;

            if (totalRows < listaHallazgos.size() && corte == 4) {

                html += "</table> <br></br><br></br><br></br><br></br>" + divHead;
                html += theadTable;

                corte = 0;

            } else if (totalRows == listaHallazgos.size()) {

                html += "</table>";

            }

        }

        if (listaHallazgos != null && listaHallazgos.size() == 0) {

            html += "</table></div> </body> </html>";
        } else {
            html += "</div> </body> </html>";
        }

        List<String> docHtml = new ArrayList<String>();

        docHtml.add(html);

        File adjunto = escribirPDFHtmlTransformacion(docHtml);

        return adjunto;

    }

    public List<String> generaListaContactos(String cadena) {

        List<String> listaContactos = new ArrayList<>();

        String[] contactos = cadena.split(",");

        if (contactos != null) {

            for (String contacto : contactos) {

                listaContactos.add(contacto);

            }

        }

        return listaContactos;

    }

//CORREO DE EXPANSIÓN CORE
    public boolean sendMailExpansionNuevoFormatoConteoCore(String idBitacora, String tipoVisita, String fecha,
            String economico, String sucursal, String tipo, String territorio, String zona, List<String> imperdonables,
            List<FirmaCheckDTO> firmas, List<ReporteChecksExpDTO> incidencias, String aperturable, String imgFachada, String tipoNegocio, int ceco, int fase, int proyecto)
            throws DocumentException, IOException {

        //http://10.51.218.72:8080/checklist/expansionCoreServices/generaHallazgos.json?ceco=480100&proyecto=3&fase=6
        //
        String nombreSucursal = "" + sucursal;
        String titulo = "ACTA ENTREGA FRANQUICIA RECEPCIÓN DE SUCURSAL " + nombreSucursal;
        String html = "";
        String htmlFirmas = "";

        List<ChecklistHallazgosRepoDTO> lista = null;
        List<SucursalChecklistDTO> sucursal3 = null;
        List<String> copiados = new ArrayList<String>();

        int version = 0;

        //*** NUEVO ***
        try {

            html = creaPDFExpansionNuevoFormatoConteoGeneralCore(idBitacora, tipoVisita, fecha, economico, sucursal, tipo, territorio,
                    zona, imperdonables, firmas, incidencias, aperturable, imgFachada, tipoNegocio, ceco, fase, proyecto);

        } catch (Exception e) {
        	e.printStackTrace();
            logger.info("AP al obtener el html creaPDFExpansionNuevoFormatoConteoGeneralCore()" + e);

        }

        try {

            //Se agrega codigo para generar las hojas por zonas
            //html += generaZonas(ceco, fase, proyecto);
            htmlFirmas += generaFirmasActa(ceco, fase, proyecto);

        } catch (Exception e) {
            logger.info("AP htmlFirmas" + htmlFirmas);
            logger.info("AP al generar la hoja de Firmas" + e);
        }

        List<String> docHtml = new ArrayList<String>();
        docHtml.add(html);

        //Se genera html de hallazgos por zona
        try {

            List<String> zonas = generaZonas(ceco + "", fase + "", proyecto + "");

            for (String htmlZona : zonas) {

                logger.info(htmlZona);
                docHtml.add(htmlZona);

            }

        } catch (Exception e) {
            logger.info("AP al generar las zonas \n" + e);
        }

        //Se genera html de adicionales por zona
        try {

            List<String> adicionales = generaAdicionales(ceco + "", fase + "", proyecto + "", 0);

            for (String htmlZona : adicionales) {

                logger.info(htmlZona);
                docHtml.add(htmlZona);

            }

        } catch (Exception e) {
            logger.info("AP al generar las zonas \n" + e);
        }

        docHtml.add(htmlFirmas);

        logger.info("____FIRMAS____");
        logger.info(htmlFirmas);

        // Debemos enviar el correo
        logger.info("_______________________________ANTES DE ESCRIBIR PDF_______________________________");
        File adjunto = escribirPDFHtmlExpansion(docHtml);
        logger.info("_______________________________DESPUES DE ESCRIBIR PDF_______________________________");

        List<String> listaCopiados = new ArrayList<String>();

        for (int i = 0; i < firmas.size(); i++) {
            listaCopiados.add(firmas.get(i).getCorreo());
        }

        try {

            List<ParametroDTO> parametroResult;
            parametroResult = parametrobi.obtieneParametros("adjuntaFirmantesCore");

            // 1 -- Solo envia a franquicia, 2 -- Franquicia + negocio , 3 Franquicia +  negocio + firmantes
            if (parametroResult.get(0).getValor().compareToIgnoreCase("1") == 0) {

                copiados.add("alejandro.morales@elektra.com.mx");
                //copiados.add("jsepulveda@elektra.com.mx");
                copiados.add("fjaramillo@elektra.com.mx");
                copiados.add("erick.carrasco@elektra.com.mx");

            } else if (parametroResult.get(0).getValor().compareToIgnoreCase("2") == 0) {

                copiados.add("alejandro.morales@elektra.com.mx");
                //copiados.add("jsepulveda@elektra.com.mx");
                copiados.add("fjaramillo@elektra.com.mx");
                copiados.add("erick.carrasco@elektra.com.mx");

                if (FRQConstantes.PRODUCCION) {
                    copiados.add("rafael.reyes@elektra.com.mx");
                    copiados.add("jefloresh@bancoazteca.com.mx");
                    copiados.add("alfredo.gordillo@elektra.com.mx");
                    copiados.add("mareyesp@elektra.com.mx");
                }

            } else if (parametroResult.get(0).getValor().compareToIgnoreCase("3") == 0) {

                if (FRQConstantes.PRODUCCION) {
                    copiados = listaCopiados;

                    copiados.add("rafael.reyes@elektra.com.mx");
                    copiados.add("jefloresh@bancoazteca.com.mx");
                    copiados.add("alfredo.gordillo@elektra.com.mx");
                    copiados.add("mareyesp@elektra.com.mx");
                }

                copiados.add("alejandro.morales@elektra.com.mx");
                //copiados.add("jsepulveda@elektra.com.mx");
                copiados.add("fjaramillo@elektra.com.mx");
                copiados.add("erick.carrasco@elektra.com.mx");
            } else {

                copiados.add("alejandro.morales@elektra.com.mx");
                //copiados.add("jsepulveda@elektra.com.mx");
                copiados.add("fjaramillo@elektra.com.mx");
                copiados.add("erick.carrasco@elektra.com.mx");
            }

        } catch (Exception e) {

            copiados.add("alejandro.morales@elektra.com.mx");
            //copiados.add("jsepulveda@elektra.com.mx");
            copiados.add("fjaramillo@elektra.com.mx");
            copiados.add("erick.carrasco@elektra.com.mx");

            logger.info("AP al obtener el parametro de listaCopiadosExpansion");
        }

        String destinatario = "";
        String strCorreoOwner = "";

        try {
            destinatario = "checklistExpansion@elektra.com.mx";
            copiados.add("alejandro.morales@elektra.com.mx");

            logger.info("DESTINATARIO - " + destinatario);
        } catch (Exception e) {
            logger.info("Algo ocurrió al consultar el correoLotus " + e);
            destinatario = "alejandro.morales@elektra.com.mx";
        }
        UtilMail.enviaCorreoExpansion(destinatario, copiados, "" + titulo, strCorreoOwner, adjunto, ceco, fase, proyecto);

        // System.out.println("****************RUTA*************** :"+
        // adjunto.getAbsolutePath() );
        // Eliminar Gráficas
        /*switch (tipoNegocio) {
        case "EKT":
            graficaComunes.delete();
            graficaBanco.delete();
            graficaElektra.delete();
            graficaGenerales.delete();
            graficaPresta.delete();
            graficaTotal.delete();

            adjunto.delete();

            break;
        case "CYC":
            graficaComunes.delete();
            graficaBanco.delete();
            graficaGenerales.delete();
            graficaTotal.delete();

            adjunto.delete();
            break;
        case "DAZ":
            graficaComunes.delete();
            graficaBanco.delete();
            graficaElektra.delete();
            graficaGenerales.delete();
            graficaPresta.delete();
            graficaTotal.delete();

            adjunto.delete();

            break;

        default:
            break;
        }*/
        return true;

    }

    public String creaPDFExpansionNuevoFormatoConteoGeneralCore(String idBitacora, String tipoVisita, String fecha,
            String economico, String sucursal, String tipo, String territorio, String zona, List<String> imperdonables,
            List<FirmaCheckDTO> firmas, List<ReporteChecksExpDTO> incidencias, String aperturable, String imgFachada, String tipoNegocio, int ceco, int fase, int proyecto) {

        // Obtengo Files de Gráfica
        // Valores de PDF
        String operable = "";
        String colorOperable = "";

        //PONER LA FASE
        /*if (tipoVisita.equals("0")) {
            tipoVisita = "Primer Recorrido";
        } else {
            tipoVisita = "Soft Openning";
        }*/
        //Obtener nombre de fase por ID
        tipoVisita = fase + "";

        //Se crea metodo para calcular grafica y ponderaciones etc
        ExpansionActa2CoreBI2 acta = new ExpansionActa2CoreBI2(idBitacora, tipoNegocio, true, ceco, fase, proyecto);

        // Imperdonables
        String numImperdonables = acta.getCalculoTotal().getListaPregImperdonablesClasif().size() + "";

        DecimalFormat formatDecimales = new DecimalFormat("###.##");
        DecimalFormat formatEnteros = new DecimalFormat("###.##");

        String tablaResponsables = "";

        // Aperturable
        String cadenaDesactivados = "";
        String sucursales = "";

        String html = "";
        String nombreFase = "";
        String jsonString = "";
        String nombreCeco = "";
        String imagenFachadaAdaptada = "";

        try {
            SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
                    .getApplicationContext().getBean("soporteHallazgosTransfBI");
            jsonString = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(ceco + "", fase + "",
                    proyecto + "");
            JsonParser parser = new JsonParser();
            JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
            JsonArray lista = jsonObj.getAsJsonArray("listaOrdenFase");
            if (lista != null && lista.size() > 0) {
                JsonObject obj = (JsonObject) lista.get(0).getAsJsonArray().get(0);
                nombreFase = obj.get("nombreFase").getAsString();
            }
        } catch (Exception e) {
            logger.info("AP al obtener el nombre de la Fase para el acta " + e);
        }

        try {
            SucursalChecklistBI sucursalChecklistBI = (SucursalChecklistBI) FRQAppContextProvider
                    .getApplicationContext().getBean("sucursalChecklistBI");

            nombreCeco = sucursalChecklistBI.obtieneDatosSucCecoFaseProyecto(ceco + "", fase + "", proyecto + "").get(0).getNombre();
            //nombreCeco = sucursalChecklistBI.obtieneDatosSuc(ceco+"").get(0).getNombre();

        } catch (Exception e) {
            logger.info("AP al obtener el nombre del ceco para el correo " + e);

        }

        try {

            File fotoFachaParam = getRutaImagenFachada(imgFachada);
            imagenFachadaAdaptada = fotoFachaParam.getAbsolutePath();

        } catch (Exception e) {

            logger.info("AP al obtener la imagen de fachada " + e);
            imagenFachadaAdaptada = imgFachada;

        }

        try {

            // html = "<!DOCTYPE html>" +
            html = "<html>" + "<head>" + "<title>Acta Entrega</title>" + "</head>" + "<style>" + "div {color:black;}"
                    + "table,td {font-size: 10px;}" + "a:link {color: white;}" + "a:visited {color: white;}"
                    + "a:hover {color: white;}" + "a:active {color: white;}" //+ "td,th {border: 0.25px solid gray;}"
                    + "#detalle {text-align:center;}" + "#mini {text-color:blue;}" + "#mini {font-size: 9px;}" + "</style>"
                    + "<body>"
                    + // Nuevo Formato
                    // ABRE DIV LOGOS
                    "<!-- Tabla Encabezado -->"
                    + "<div style='border-bottom: 2px solid #F0F0F0;'>"
                    + "<table style='width:100%; height:50px; font-size: 10px;' class='encabezado'>"
                    + "<tr>"
                    + "<th style='font-size: 15px;'>" + nombreFase + "</th>"
                    + "<td  rowspan='2' style='text-align: right;'>"
                    + "<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoElektra.png' width='36' height='36' />"
                    + "&nbsp;&nbsp;&nbsp;"
                    + "<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoBAZ.png' width='45' height='30'/>"
                    + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td style='font-size: 15px;'>" + nombreCeco + "</td>"
                    + "</tr>"
                    + "</table>"
                    + "</div>"
                    + // CIERRA DIV LOGOS
                    "<TABLE cellspacing='5' style=' width:100%; height:340px; text-align:center;' >"
                    + "<TR>"
                    + "<TD ROWSPAN='3' style=' width:25%; height:50%;'>"
                    + "<div>"
                    + getDataAperturable(acta, tipoNegocio, jsonString)
                    + "<img src='" + imagenFachadaAdaptada
                    + "'width='190%' height='111%'/>"
                    + "</div>"
                    + "</TD>"
                    + "<TD colspan='2' style='text-align:left; background-color:#F0EFF0;'>"
                    + "<div style='float:left; width:50%; margin:auto; padding-left:5px; padding-top:10px; padding-bottom:10px; '>"
                    + "<b>Sucursal:</b>" + sucursal + "<br></br><br></br><b>Ceco:</b>" + ceco + "<br> </br><br></br>  <b>Zona: </b>" + zona + "<br></br><br></br> <b>Territorio: </b>"
                    + territorio
                    + "</div>"
                    + "<div style='float:left; width:100%; heigth:200%; padding-left:2px; padding-top:30px;'>"
                    + "<b>Tipo Visita: </b>" + nombreFase
                    + "<br></br><br></br> <b>Fecha: </b>" + fecha
                    + "</div>"
                    + "</TD> "
                    + //"<TD style='background-color:black;'> <a target='_blank' style='color:white;' href='"+FRQConstantes.getURLServer()+"/checklist/central/detalleChecklistExpancionFirmas.htm?idUsuario=189871&idBitacora="
                    //+ idBitacora + "'>Ver <br></br> Participantes</a></TD>" +
                    // "<TD style='text-align:left; font-size: 10px; background-color:#F0EFF0;'>
                    // <b>No Económico:</b>"+ "10029988" +"<br></br> <b>Tipo Visita: </b>"+"Soft
                    // Opening"+"<br></br> <b>Fecha: </b>"+ "29/10/2019" +" </TD> " +
                    "</TR>";

            // <b></b> <br></br>
            //CONDICION SI ES EKT Y ES SOFT
            if (acta.getCalculoZonasGeografiaCalif() != null) {

                html += "<TR>"
                        + "<TD style='font-size: 10px; height:50px;'><strong style='text-align:right; font-size: 10px;'>Calificación Construcción: " + formatDecimales.format(acta.getPreCalificacionNum()) + "</strong></TD> "
                        + "<TD style='font-size: 10px; height:50px;'><strong style='text-align:right; font-size: 10px;'>Calificación Geografía: " + formatDecimales.format(acta.getCalculoZonasGeografiaCalif().getCalificacionClasifCalculada()) + "</strong></TD>"
                        + "</TR>";
            } else {

                html += "<TR>"
                        + "<TD style='font-size: 12px; height:50px;'><strong style='text-align:right; font-size: 15px;'>Calificación</strong></TD> "
                        + "<TD style='font-size: 12px; height:50px;'><strong style='text-align:center; font-size: 15px;'>"
                        + formatDecimales.format(acta.getPreCalificacionNum()) + "</strong> </TD> "
                        + "</TR>";
            }

            html += "<TR>"
                    + "<TD colspan='2' style='text-align:center; background-color:#F0EFF0;'> <p><b>Imperdonables:</b> <strong style='text-align:center; font-size: 15px; height:35px;'>"
                    + numImperdonables + "</strong></p> </TD>"
                    //+ "<TD style='background-color:black;'> <a target='_blank' style='color:white;' href='"+FRQConstantes.getURLServer()+"/checklist/central/detalleChecklistExpancionImperdonableGeneral.htm?idUsuario=189871&idBitacora="
                    //+ idBitacora + "'>Ver Detalles</a></TD>" +
                    // http://10.53.33.83/checklist/central/detalleChecklistExpancionImperdonable.htm?idBitacora=26282
                    + "</TR>"
                    + "</TABLE>"
                    + // ITEMS
                    "<TABLE style=' width:100%; height:35px; text-align:center'>" + "<TR>"
                    + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#F0EFF0;'> <strong style='text-align:center; font-size: 15px;'>"
                    + acta.getTotalItems() + "</strong><br></br><br></br> Items Revisados </TD>"
                    + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#F0EFF0;'> <strong style='text-align:center; font-size: 15px;'>"
                    + acta.getItemsNoConsiderados() + "</strong><br></br><br></br> Items No Considerados </TD>"
                    + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#D8D8D8;'><strong style='text-align:center; font-size: 15px;'>"
                    + acta.getItemsAprobados()
                    + "</strong><br></br> <img src='" + FRQConstantes.getRutaImagen() + "/franquicia/firmaChecklist/ItemsAprovados02.png' width='20' height='20'/>&nbsp; Items Aprobados </TD>"
                    + "<TD style='width:25%; text-align:center; font-size: 12px; background-color:#6B696E; color:white;'><strong style='text-align:center; font-size: 15px;'>"
                    + acta.getItemsPorRevisar()
                    + "</strong><br></br> <img src='" + FRQConstantes.getRutaImagen() + "/franquicia/firmaChecklist/ItemsAtender02.png' width='20' height='20'/>&nbsp; Items Por Atender </TD>"
                    + "</TR>"
                    + // ITEMS
                    "</TABLE>"
                    + "<div id='detalle'> <br></br><br></br> Detalles de la Entrega </div>";

            String tabla1 = "";
            if (tipoNegocio.equalsIgnoreCase("OCC")) {
                tabla1 = "<tr height=\"180px\" style=\"border: 2px solid black;\">"
                        + "           <td WIDTH=\"50%\" style='border: 0.25px solid gray;'>@grafica1</td>"
                        + "           <td WIDTH=\"50%\" style='border: 0.25px solid gray;'>@grafica2</td>"
                        + "     </tr>"
                        + "     <tr height=\"180px\">"
                        + "           <td WIDTH=\"50%\" style='border: 0.25px solid gray;'>@grafica3</td>"
                        + "           <td WIDTH=\"50%\" style='border: 0.25px solid gray;'>@grafica4</td>"
                        + "     </tr>";
            } else if (tipoNegocio.equalsIgnoreCase("EKT") || tipoNegocio.equalsIgnoreCase("DAZ")) {

                if (acta.getCalculoZonasGeografiaCalif() != null) {

                    tabla1 = "<tr height=\"150px\" style=\"border: 2px solid black;\">"
                            + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica1</td>"
                            + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica2</td>"
                            + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica3</td>"
                            + "     </tr>"
                            + "     <tr height=\"150px\" >"
                            + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica4</td>"
                            + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica5</td>"
                            + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica6</td>"
                            + "     </tr>"
                            + "     <tr height=\"150px\" >"
                            + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica7</td>"
                            + "     </tr>";
                } else {

                    tabla1 = "<tr height=\"180px\" style=\"border: 2px solid black;\">"
                            + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica1</td>"
                            + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica2</td>"
                            + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica3</td>"
                            + "     </tr>"
                            + "     <tr height=\"180px\" >"
                            + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica4</td>"
                            + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica5</td>"
                            + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica6</td>"
                            + "     </tr>";
                }

            } else if (tipoNegocio.equalsIgnoreCase("EKT") || tipoNegocio.equalsIgnoreCase("DAZ")) {

                tabla1 = "<tr height=\"180px\" style=\"border: 2px solid black;\">"
                        + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica1</td>"
                        + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica2</td>"
                        + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica3</td>"
                        + "     </tr>"
                        + "     <tr height=\"180px\" >"
                        + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica4</td>"
                        + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica5</td>"
                        + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica6</td>"
                        + "     </tr>";
            } else {

                tabla1 = "<tr height=\"180px\" style=\"border: 2px solid black;\">"
                        + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica1</td>"
                        + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica2</td>"
                        + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica3</td>"
                        + "     </tr>"
                        + "     <tr height=\"180px\" >"
                        + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica4</td>"
                        + "           <td WIDTH=\"33%\" style='border: 0.25px solid gray;'>@grafica5</td>"
                        + "     </tr>";

            }

            for (int i = 0; i < acta.getCuadroGrafica().length; i++) {

                if (acta.getCuadroGrafica()[i] != null) {

                    tabla1 = tabla1.replace("@grafica" + (i + 1), acta.getCuadroGrafica()[i]);
                    tabla1 = tabla1.replace("@verDetalle", "<a target='_blank' style='color:black;' href='" + FRQConstantes.getURLServer() + "/checklist/central/detalleChecklistExpancionGeneral.htm?idUsuario=189871&idBitacora="
                            + idBitacora + "&tipo=" + acta.getClasificacion()[i] + "'>Ver Detalles</a>");

                }

            }
            tabla1 = tabla1.replace("@grafica5", acta.getCuadroGrafica()[acta.getCuadroGrafica().length - 1]);
            tabla1 = tabla1.replace("@grafica6", acta.getCuadroGrafica()[acta.getCuadroGrafica().length - 1]);

            tabla1 = tabla1.replace("width='50%'", "width='30%'");

            html += "<table WIDTH=\"100%\" cellpadding=\"0\" cellspacing=\"0\" ><tbody>"
                    + tabla1
                    + "</tbody></table>";
            logger.info(html);

            html += "<table cellspacing='0' align='right'>" + "<tr align='center'>"
                    + "<td style=' border: 0.25px solid white;'>Código</td>"
                    + "<td style=' border: 0.25px solid white; text-align:center; background-color:black; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>pésimo</td>"
                    + "<td style='border: 0.25px solid white;  text-align:center; background-color:#FE003D; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>en peligro</td>"
                    + "<td style='border: 0.25px solid white;  text-align:center; background-color:#F7CA44; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>alerta</td>"
                    + "<td style='border: 0.25px solid white;  text-align:center; background-color:#006240; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>bien</td>"
                    + "</tr>" + "</table>"
                    + "<strong style='text-align:center; font-size: 10px;'>" + cadenaDesactivados + "</strong>"
                    + // Responsables de Obra
                    tablaResponsables
                    + // Fin Responables de Obra
                    "</body>" + "</html>";

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("AP al general el html" + e.getStackTrace());
            logger.info("ESTE ES MI HTML INCOMPLETO" + html);
        }

        return html;

    }

    public String getDataAperturable(ExpansionActa2CoreBI2 acta, String tipoNegocio, String jsonString) {

        String result = "<div bgcolor='red' style='padding-top:5px; width:100%; background-color:"
                + acta.getColorOperable() + "; color:" + acta.getColorTexto() + "; font-size: 13px;'> <h3>"
                + acta.getOperable() + " </h3></div>";

        try {

            if (tipoNegocio.equalsIgnoreCase("DAZ") || tipoNegocio.equalsIgnoreCase("OCC")) {
                result = "<div bgcolor='red' style='padding-top:5px; width:100%; background-color:"
                        + acta.getColorOperable() + "; color:" + acta.getColorTexto() + "; font-size: 13px;'> <h3>"
                        + acta.getOperable() + " </h3></div>";

            } else if (tipoNegocio.equalsIgnoreCase("EKT")) {

                String numFase = "";

                JsonParser parser = new JsonParser();
                JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
                JsonArray lista = jsonObj.getAsJsonArray("listaOrdenFase");

                if (lista != null && lista.size() > 0) {
                    JsonObject obj = (JsonObject) lista.get(0).getAsJsonArray().get(0);
                    numFase = obj.get("idOrdenFase").getAsString();
                }

                //PARA EKT LA FASE 1 Y 2 SE EVALUAN DE FORMA DIFERENTE
                if (numFase.equalsIgnoreCase("1") || numFase.equalsIgnoreCase("2")) {

                    if (acta.getPreCalificacionNum() >= 90) {

                        result = "<div bgcolor='red' style='padding-top:5px; width:100%; background-color:"
                                + "#46ad35" + "; color:" + "white" + "; font-size: 13px;'> <h3>"
                                + "ACEPTABLE" + " </h3></div>";

                    } else {

                        result = "<div bgcolor='red' style='padding-top:5px; width:100%; background-color:"
                                + "#FE003D" + "; color:" + "white" + "; font-size: 13px;'> <h3>"
                                + " NO ACEPTABLE" + " </h3></div>";
                    }

                } else {

                    result = "<div bgcolor='red' style='padding-top:5px; width:100%; background-color:"
                            + acta.getColorOperable() + "; color:" + acta.getColorTexto() + "; font-size: 13px;'> <h3>"
                            + acta.getOperable() + " </h3></div>";
                }

            }

        } catch (Exception e) {
            logger.info("AP al obtener la informacion para el color del negocio correspondiente");
        }

        return result;
    }

    public boolean sendMailExpansionCoreNotifica(int idCeco, int idFase, int idProyecto) {

        String destinatarios = "";
        String copiados = "";

        List<String> listaDestinatarios = new ArrayList<String>();
        List<String> listaCopiados = new ArrayList<String>();

        listaDestinatarios.add("checklistExpansion@elektra.com.mx");

        listaCopiados.add("alejandro.morales@elektra.com.mx");
        //listaCopiados.add("jsepulveda@elektra.com.mx");

        if (FRQConstantes.PRODUCCION) {
            listaCopiados.add("rafael.reyes@elektra.com.mx");
            listaCopiados.add("jefloresh@bancoazteca.com.mx");
            listaCopiados.add("alfredo.gordillo@elektra.com.mx");
            listaCopiados.add("mareyesp@elektra.com.mx");
        }

        String fechaPendientes = UtilDate.getSysDate("dd/MM/yyyy");
        String titulo = "";
        titulo = "Notificación de Hallazgos Concluidos";

        try {

            CorreosLotus correosLotus = new CorreosLotus();
            String destinatario = "";

            try {
                //destinatario = correosLotus.validaUsuarioLotusCorreos(user.getIdUsuario());
                destinatario = "checklistExpansion@elektra.com.mx";
                logger.info("correo destinatario - " + destinatario);
            } catch (Exception e) {
                logger.info("Algo ocurrió al consultar el correoLotus " + e);
                destinatario = "alejandro.morales@elektra.com.mx";
            }

            // EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO VALIDA_USUARIO_LOTUS_CORREO
            UtilMail.enviaCorreoExpansionNotificacion(listaDestinatarios, listaCopiados, "" + titulo, "", null, idCeco, idFase, idProyecto);

        } catch (Exception e) {
            logger.info("Algo ocurrió... " + e);
        }
        return true;
    }

    public String generaFirmasActa(int ceco, int fase, int proyecto) {

        String htmlFirmas = "";
        String nombreFase = "";
        String nombreCeco = "";

        try {
            SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
                    .getApplicationContext().getBean("soporteHallazgosTransfBI");
            String jsonString = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(ceco + "", fase + "",
                    proyecto + "");
            JsonParser parser = new JsonParser();
            JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
            JsonArray lista = jsonObj.getAsJsonArray("listaOrdenFase");
            if (lista != null && lista.size() > 0) {
                JsonObject obj = (JsonObject) lista.get(0).getAsJsonArray().get(0);
                nombreFase = obj.get("nombreFase").getAsString();
            }
        } catch (Exception e) {
            logger.info("AP al obtener el nombre de la Fase para el correo " + e);
            return "";
        }

        try {
            SucursalChecklistBI sucursalChecklistBI = (SucursalChecklistBI) FRQAppContextProvider
                    .getApplicationContext().getBean("sucursalChecklistBI");

            nombreCeco = sucursalChecklistBI.obtieneDatosSucCecoFaseProyecto(ceco + "", fase + "", proyecto + "").get(0).getNombre();
            //nombreCeco = sucursalChecklistBI.obtieneDatosSuc(ceco+"").get(0).getNombre();

        } catch (Exception e) {
            logger.info("AP al obtener el nombre del ceco para el correo " + e);
            return "";

        }

        htmlFirmas += "<html> <head> <meta charset='UTF-8'> </meta> <title>Firmas</title> <style>"
                + "body {"
                + "background-color: #FFFFFF"
                + "}"
                + ".tablaFirmas {"
                + "background-color: #FFFFFF;"
                + "}"
                + "table td tr {"
                + "text-align: center;"
                + "font-size: 10px;"
                + "}"
                + "body{"
                + "font-size: 12px;"
                + "}"
                + "b{"
                + "font-size: 12px;"
                + "}"
                + ".encabezado{"
                + "border: 5px solid white;"
                + "}"
                + ".contendedorFirma{"
                + "font-size: 12px;"
                + "text-align: center;"
                + "border: 5px solid #FFFFFF;"
                + "}"
                + ".divBottom{"
                + "border-bottom: 2px solid #F0F0F0;"
                + "}"
                + ".divFirmas{"
                + "border-bottom: 2px solid #F0F0F0;"
                + "padding-top: 5px;"
                + "}"
                + ".ncabezado{"
                + "padding: 5px;"
                + "text-align: left;"
                + "}"
                + "</style>"
                + "</head>";

        htmlFirmas += "<body>"
                + "<!-- Tabla Encabezado -->"
                + "<div style='border-bottom: 2px solid #F0F0F0;'>"
                + "<table style='width:100%; height:50px;' class='encabezado'>"
                + "<tr>"
                + "<th>" + nombreFase + "</th>"
                + "<td  rowspan='2' style='text-align: right;'>"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoElektra.png' width='36' height='36' />"
                + "&nbsp;&nbsp;&nbsp;"
                + "<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoBAZ.png' width='45' height='30'/>"
                + "</td>"
                + "</tr>"
                + "<tr>"
                + "<td>" + nombreCeco + "</td>"
                + "</tr>"
                + "</table>"
                + "</div>"
                + "<div class='divFirmas'>"
                + "<label>FIRMAS DE LOS PARTICIPANTES</label>"
                + "</div>"
                + "<br></br>"
                + "<div>";

        htmlFirmas += getTextoFirmas(ceco, proyecto, fase, nombreFase);

        htmlFirmas += "</div>";

        htmlFirmas += "<table class='tablaFirmas' style='width:100%; align:center;'>"
                + "<tbody>";

        htmlFirmas += getTablaFirmas(ceco, fase, proyecto);

        htmlFirmas += "</tbody> </table> </body> </html>";

        return htmlFirmas;

    }

    public String getTextoFirmas(int ceco, int proyecto, int fase, String nombreFase) {

        String texto = "";
        int ordenFase = 0;

        try {

            SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
                    .getApplicationContext().getBean("soporteHallazgosTransfBI");
            String jsonString = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(ceco + "", fase + "",
                    proyecto + "");
            JsonParser parser = new JsonParser();
            JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
            JsonArray lista = jsonObj.getAsJsonArray("listaOrdenFase");

            if (lista != null && lista.size() > 0) {
                JsonObject obj = (JsonObject) lista.get(0).getAsJsonArray().get(0);
                ordenFase = obj.get("idOrdenFase").getAsInt();
            }

        } catch (Exception e) {

            logger.info("AP al obtener el nombre de la Fase para el correo " + e);

            texto = "<p>Se reúnen en el inmueble, arriba mencionado, las personas "
                    + "cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Validación "
                    + "técnica de los trabajos de construcción ejecutados por el Despacho de Bienes Inmuebles y validados por el área de "
                    + "Control Interno.</p>"
                    + "<p>Realizado el recorrido por el área de construcción, revisados los trabajos efectuados y ejecutados con base en los "
                    + "alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra, proyecto "
                    + "ejecutivo e indicaciones hechas oportunamente en el libro bitácora. El cliente revisará y autorizará el siguiente proceso "
                    + "con la condición de tener finalizados cada uno de los detalles registrados en el Checklist adjunto . Es imprescindible "
                    + "contar con la evidencia de los trabajos finalizados para continuar y programar la siguiente validación.</p>";

            return texto;

        }

        texto = textosActasExpansion(proyecto, fase, nombreFase);

        return texto;
    }

    public String textosActasExpansion(int idProyecto, int idFase, String nombreFase) {

        String hora = "";
        int dia = 0;
        int mes = 0;
        int ano = 0;

        Date fechaActual = new Date();
        //Formateando la fecha:
        DateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");

        hora = formatoHora.format(fechaActual) + "";

        //Fecha actual desglosada:
        Calendar fecha = Calendar.getInstance();
        ano = fecha.get(Calendar.YEAR);
        mes = fecha.get(Calendar.MONTH) + 1;
        dia = fecha.get(Calendar.DAY_OF_MONTH);

        String texto = "<p>Siendo las " + hora + " horas del día " + dia + " del mes de " + mes + " del " + ano + ", ";

        switch (idProyecto) {
            //DAZ
            case 4:
                switch (idFase) {
                    case 8:
                        texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
                        break;
                    case 9:
                        texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Banco Azteca de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
                        break;
                    default:
                        texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
                        break;
                }
                break;
            //OCC
            case 3:
                texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a OCC de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad</p>";
                break;
            //EKT NORMAL Y EKT RECONSTRUCCION 5
            case 2:
            case 5:
                switch (idFase) {
                    case 4:
                    case 14:
                        texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de construcción de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
                        break;
                    case 5:
                    case 15:
                        texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de montaje y mobiliario de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Control Interno.</p>";
                        break;
                    case 6:
                    case 16:
                        texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo el Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
                        break;
                    default:
                        texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de construcción de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
                        break;
                }
                break;
            //SHINEADAS
            case 12:
                texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Aseguramiento de Calidad.</p>";
                break;
            //PPR
            case 13:
                switch (idFase) {
                    case 37:
                        texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación  de Calidad.</p>";
                        break;
                    case 38:
                        texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Presta Prenda de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
                        break;
                    default:
                        texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Presta Prenda de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
                        break;
                }
                break;
            //ITK
            case 14:
                switch (idFase) {
                    case 39:
                        texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Recepción de Construcción y montaje  de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
                        break;
                    case 40:
                        texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Italika de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
                        break;
                    default:
                        texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Italika de los trabajos de construcción ejecutados CONMA Construcción y validados por el área de Certificación de Calidad.</p>";
                        break;
                }
                break;
            //TIENDA ANFOTRIONA
            case 23:
                texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Aseguramiento de Calidad.</p>";
                break;
            //MOVILIDAD
            case 24:
                texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo el Entrega a Movilidad de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Control Interno.</p>";
                break;
            //SIITO ALTERNO
            case 25:
                texto += "se reúnen en el inmueble, arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente documento con el propósito de llevar a cabo la Entrega a Red Única de los trabajos de construcción ejecutados por CONMA Construcción y validados por el área de Aseguramiento de Calidad.</p>";
                break;
            default:
                texto += "se reúnen en el inmueble arriba mencionado, las personas cuyos nombres, cargos y firmas aparecen al calce del presente del documento con el propósito de llevar a cabo "
                        + nombreFase
                        + " de los trabajos de "
                        + "construcción ejecutados por el Despacho de Bienes Inmuebles(BI) y validados por el área de Aseguramiento de Calidad.</p>";
                break;
        }

        texto += "\n <p>Realizado el recorrido por construcción, revisados los trabajos efectuados y ejecutados con base en los alcances autorizados, la normatividad, especificaciones generales, especificaciones particulares de obra, proyecto ejecutivo e indicaciones hechas oportunamente en el libro de bitácora. El Cliente lo recibe bajo el entendido de que esto será considerado efectivo una vez que se haya cumplido con los detalles y compromisos registrados en el Check list adjunto  y realizado en el recorrido, siendo el área responsable de la tienda quienes deberán asegurar el cumplimiento antes mencionado.</p>";

        return texto;

    }

    public String getTablaFirmas(int ceco, int fase, int proyecto) {

        String tablaFirmas = "";
        JsonArray listaFirmas = new JsonArray();

        try {
            SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
                    .getApplicationContext().getBean("soporteHallazgosTransfBI");
            String jsonString = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(ceco + "", fase + "",
                    proyecto + "");
            JsonParser parser = new JsonParser();
            JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
            listaFirmas = (JsonArray) jsonObj.getAsJsonArray("listaFirmas").get(0);

        } catch (Exception e) {
            logger.info("AP al obtener el nombre de la Fase para el correo " + e);
            return "";
        }

        for (int i = 0; i < listaFirmas.size(); i++) {

            if (i == 0) {
                tablaFirmas += "<tr>";
            }
            if ((i != 0) && ((i) % 3 == 0)) {
                tablaFirmas += "<tr>";
            }

            JsonObject obj = (JsonObject) listaFirmas.get(i);
            String nombre = obj.get("responsable").getAsString();
            String puesto = obj.get("puesto").getAsString();
            String imagen = obj.get("ruta").getAsString();

            tablaFirmas += "<td style='width:100%;'>"
                    + "<div class='contendedorFirma' style=' width:100%; height:150px;'>"
                    + "<div class='contenedorImg' style='border-bottom: 1px solid black;'>"
                    + "<img src='http://10.53.33.82" + imagen + "' alt='Lamp' width='60%' height='75%'></img>"
                    + "</div>"
                    + "<div class='contenedorNombre'>"
                    + "<b>" + nombre + "</b>"
                    + "</div>"
                    + "<div class='contenedorPuesto'>"
                    + puesto
                    + "</div>"
                    + "</div>"
                    + "</td>";

            if ((i + 1) % 3 == 0) {
                tablaFirmas += "</tr>";
            }

            if (i == listaFirmas.size() - 1 && (listaFirmas.size() % 3 != 0)) {
                tablaFirmas += "</tr>";
            }

        }

        return tablaFirmas;
    }

    public List<String> generaZonas(String ceco, String fase, String proyecto) {

        String nombreFase = "";
        String nombreCeco = "";

        List<String> zonasHtml = null;

        try {
            SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
                    .getApplicationContext().getBean("soporteHallazgosTransfBI");
            String jsonString = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(ceco + "", fase + "",
                    proyecto + "");
            JsonParser parser = new JsonParser();
            JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
            JsonArray lista = jsonObj.getAsJsonArray("listaOrdenFase");
            if (lista != null && lista.size() > 0) {
                JsonObject obj = (JsonObject) lista.get(0).getAsJsonArray().get(0);
                nombreFase = obj.get("nombreFase").getAsString();
            }
        } catch (Exception e) {
            logger.info("AP al obtener el nombre de la Fase para el correo " + e);
            return new ArrayList<String>();
        }

        try {
            SucursalChecklistBI sucursalChecklistBI = (SucursalChecklistBI) FRQAppContextProvider
                    .getApplicationContext().getBean("sucursalChecklistBI");

            nombreCeco = sucursalChecklistBI.obtieneDatosSucCecoFaseProyecto(ceco + "", fase + "", proyecto + "").get(0).getNombre();
            //nombreCeco = sucursalChecklistBI.obtieneDatosSuc(ceco+"").get(0).getNombre();

        } catch (Exception e) {
            logger.info("AP al obtener el nombre del ceco para el correo " + e);
            return new ArrayList<String>();

        }

        try {
            HallazgosZonasHtmlBI hallazgosZonasHtmlBI = new HallazgosZonasHtmlBI();

            zonasHtml = hallazgosZonasHtmlBI.generaHtmlZonas(ceco, fase, proyecto, nombreFase, nombreCeco);

        } catch (Exception e) {

            zonasHtml = new ArrayList<>();

        }

        return zonasHtml;

    }

    public List<String> generaAdicionales(String ceco, String fase, String proyecto, int parametro) {

        String nombreFase = "";
        String nombreCeco = "";

        List<String> adicionalesHtml = null;

        try {
            SoporteHallazgosTransfBI soporteHallazgosTransfBI = (SoporteHallazgosTransfBI) FRQAppContextProvider
                    .getApplicationContext().getBean("soporteHallazgosTransfBI");
            String jsonString = soporteHallazgosTransfBI.obtieneOrdenFaseFirmas(ceco + "", fase + "",
                    proyecto + "");
            JsonParser parser = new JsonParser();
            JsonObject jsonObj = parser.parse(jsonString).getAsJsonObject();
            JsonArray lista = jsonObj.getAsJsonArray("listaOrdenFase");
            if (lista != null && lista.size() > 0) {
                JsonObject obj = (JsonObject) lista.get(0).getAsJsonArray().get(0);
                nombreFase = obj.get("nombreFase").getAsString();
            }
        } catch (Exception e) {
            logger.info("AP al obtener el nombre de la Fase para el correo " + e);
            return new ArrayList<String>();
        }

        try {
            SucursalChecklistBI sucursalChecklistBI = (SucursalChecklistBI) FRQAppContextProvider
                    .getApplicationContext().getBean("sucursalChecklistBI");

            nombreCeco = sucursalChecklistBI.obtieneDatosSucCecoFaseProyecto(ceco + "", fase + "", proyecto + "").get(0).getNombre();
            //nombreCeco = sucursalChecklistBI.obtieneDatosSuc(ceco+"").get(0).getNombre();

        } catch (Exception e) {
            logger.info("AP al obtener el nombre del ceco para el correo " + e);
            return new ArrayList<String>();

        }

        try {
            AdicionalesZonasHtmlBI adicionalesBI = new AdicionalesZonasHtmlBI();

            adicionalesHtml = adicionalesBI.generaHtmlAdicionales(ceco, fase, proyecto, nombreFase, nombreCeco, 0);

        } catch (Exception e) {

            adicionalesHtml = new ArrayList<>();

        }

        return adicionalesHtml;

    }

    public boolean sendMailExpansionCeroHallazgos(String idCeco, String idFase, String idProyecto, List<FirmaCheckDTO> firmasB, String nombreCeco, String nombreFase, String territorio, String zona, String region) {

        List<String> listaDestinatarios = new ArrayList<String>();
        List<String> listaCopiados = new ArrayList<String>();

        listaDestinatarios.add("checklistExpansion@elektra.com.mx");
        listaCopiados.add("alejandro.morales@elektra.com.mx");

        if (!firmasB.isEmpty()) {
            for (FirmaCheckDTO firma : firmasB) {
                listaCopiados.add(firma.getCorreo());
            }
        }

        File actaCeroHallazgos = generaArchivoCero(idCeco, idFase, idProyecto, nombreCeco, nombreFase, territorio, zona, region);

        if (FRQConstantes.PRODUCCION) {
            listaCopiados.add("jefloresh@bancoazteca.com.mx");
            listaCopiados.add("alfredo.gordillo@elektra.com.mx");
        }

        try {

            return (UtilMail.enviaCorreoCeroHallazgos(listaDestinatarios, listaCopiados, "Notificación de Hallazgos Concluidos - " + nombreCeco + ": " + nombreFase, "", actaCeroHallazgos, Integer.parseInt(idCeco), Integer.parseInt(idFase), Integer.parseInt(idProyecto)));

        } catch (Exception e) {
            logger.info("Algo ocurrió... " + e);
            return false;
        }
    }

    // Va a recibir un list con el size de los documentos que se tienen que crear
    public File escribirPDFHtml(List<String> html, String nameFile) {
        File temp = null;
        try {
            // create a temp file
            temp = File.createTempFile(nameFile, ".pdf");
            FileOutputStream file = new FileOutputStream(temp);
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, file);
            document.open();

            java.util.Iterator<String> it = html.iterator();

            while (it.hasNext()) {
                String k = it.next();
                XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
                ByteArrayInputStream is = new ByteArrayInputStream(k.getBytes());
                worker.parseXHtml(writer, document, is);
                document.newPage();
            }
            document.close();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }

    private File generaArchivoCero(String ceco, String fase, String proyecto, String nombreCeco, String nombreFase, String territorio, String zona, String region) {

        //ceco, proyecto, fase
        //480100 ,
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm");
        SimpleDateFormat sdfDia = new SimpleDateFormat("d");
        SimpleDateFormat sdfMes = new SimpleDateFormat("M");
        SimpleDateFormat sdfanio = new SimpleDateFormat("yyyy");

        Calendar calendar = Calendar.getInstance();

        Date hoy = calendar.getTime();

        String mes = "";
        switch (Integer.parseInt(sdfMes.format(hoy))) {

            case 1:
                mes = "Enero";
                break;
            case 2:
                mes = "Febrero";
                break;
            case 3:
                mes = "Marzo";
                break;
            case 4:
                mes = "Abril";
                break;
            case 5:
                mes = "Mayo";
                break;
            case 6:
                mes = "Junio";
                break;
            case 7:
                mes = "Julio";
                break;
            case 8:
                mes = "Agosto";
                break;
            case 9:
                mes = "Septiembre";
                break;
            case 10:
                mes = "Octubre";
                break;
            case 11:
                mes = "Noviembre";
                break;
            case 12:
                mes = "Diciembre";
                break;

        }

        String fechaHoy = simpleDateFormat.format(hoy);
        String hora = sdfHora.format(hoy);
        String dia = sdfDia.format(hoy);
        String anio = sdfanio.format(hoy);

        String htmlString = ""
                + "<html>"
                + "<style> @font-face {\n"
                + "      font-family: \"Avenir Next\";\n"
                + "      src: url(\"/fonts/Avenir_Next_Bold.otf\");\n"
                + "    }</style>"
                + "<body style=\"font-color: black\">"
                + "<div style=\"width: 100%;\">"
                + "<img style=\"display: block;"
                + "  margin-left: auto;"
                + "  margin-right: auto;\""
                + "  width: 40%;\" width=\"700\" height=\"175\" src=\"http://10.53.33.82/franquicia/banco_azteca.jpg\"></img>"
                + "<div style=\"margin-top: 10px ;padding-left: 50px; padding-right: 50px; padding-bottom: 20px; border-bottom: 2px solid #E8EBF5;\">"
                + "<span style=\"font-size: 24px;\">ACTA DE ENTREGA</span>"
                + "<br></br><strong>Cero Hallazgos</strong>"
                + "</div>"
                + "</div>"
                + "<div style=\"padding-right: 50px; padding-left: 50px;\">"
                + "<br></br>"
                + "<br></br>"
                + "<table style=\" padding-left: 10px; padding-right: 10px ;width: 100%; border: 0px solid #7B7B7B; background-color: #E8EBF5;font-size: 12px;\">"
                + "<tbody>"
                + "<tr>"
                + "<td>Tipo proyecto: </td>"
                + "<td><strong>" + nombreFase + "</strong></td>"
                + "<td>Número económico: </td>"
                + "<td><strong>" + ceco.substring(2) + "</strong></td>"
                + "</tr>"
                + "<tr>"
                + "<td>Sucursal: </td>"
                + "<td><strong>" + nombreCeco + "</strong></td>"
                + "<td>Zona/Región: </td>"
                + "<td><strong>" + zona + " / " + region + "</strong></td>"
                + "</tr>"
                + "<tr>"
                + "<td>Territorio: </td>"
                + "<td><strong>" + territorio + "</strong></td>"
                + "<td>Fecha de entrega: </td>"
                + "<td><strong>" + fechaHoy + "</strong></td>"
                + "</tr>"
                + "</tbody>"
                + "</table><br></br>"
                + "<div style=\"text-align:justify;\">"
                + "<p>"
                + "<strong>Objeto del contrato celebrando entre: </strong>"
                + "<br></br>"
                + "Cliente: <strong>BANCO AZTECA INSTITUCIÓN DE BANCO MÚLTIPLE.</strong>"
                + "<br></br>"
                + "Contratista: <strong>DESPACHO DE BIENES INMUEBLES.</strong>"
                + "</p>"
                + "<p style=\"text-align:justify; margin-top: 50px;\">"
                + "<strong>Siendo las " + hora + " horas del dia " + dia + " del mes de " + mes + " del " + anio + ",</strong>"
                + " se dan por concluidos los hallazgos registrados durante la entrega-recepcion de la tienda (anexos en el Check - List del Acta de entrega). Banco Azteca, a través del área de Franquicia, ha revisado y validado los trabajos ejecutados con base en los alcances"
                + " autorizados, la normativdad, especificaciones generales, especificaciones particulares de obra, proyecto ejecutivo e indicaciones hechas oportunamente en el libro de bitácora."
                + "<strong>"
                + " Se notifica al Gerente de la tienda sobre la resolución total de los hallazgos, es decir, que es una tienda con 'cero hallazgos'."
                + "</strong>"
                + "</p>"
                + "</div>"
                + "</div>"
                + "</body>"
                + "</html>";

        List<String> listaHtml = new ArrayList<String>();
        listaHtml.add(htmlString);

        File actaCeroHallazgosFile = escribirPDFHtml(listaHtml, "Acta_Cero_Hallazgos");

        return actaCeroHallazgosFile;

    }

    public File getRutaImagenFachada(String ruta) throws IOException, ImageProcessingException {

        File fotoCorrecta = null;

        try {

            URL url = new URL(FRQConstantes.getRutaImagen() + ruta);
            InputStream fis = url.openStream();

            InputStream fisData = url.openStream();
            Metadata metadata = ImageMetadataReader.readMetadata(fisData);

            File fotoPaso = File.createTempFile("fotoPaso", ".jpg");
            BufferedImage imgBuff = ImageIO.read(fis);
            ImageIO.write(imgBuff, "png", fotoPaso);

            System.out.println(fotoPaso.getAbsolutePath());

            javaxt.io.Image image = new javaxt.io.Image(fotoPaso.getAbsolutePath());
            fotoCorrecta = File.createTempFile("fotoFachada", ".jpg");

            String desc = "";
            for (Directory directory : metadata.getDirectories()) {
                for (Tag tag : directory.getTags()) {
                    if (tag.getTagName().equals("Orientation")) {
                        desc = tag.getDescription();
                    }
                }
            }

            switch (desc) {

                case "Top, left side (Horizontal / normal)":
                    break;

                case "Top, right side (Mirror horizontal)":
                    break;

                case "Bottom, right side (Rotate 180)":
                    image.rotate(180);
                    break;

                case "Bottom, left side (Mirror vertical)":
                    image.rotate(-180);
                    break;

                case "Left side, top (Mirror horizontal and rotate 270 CW)":
                    image.rotate(-90);
                    break;

                case "Right side, top (Rotate 90 CW)":
                    image.rotate(90);
                    break;

                case "Right side, bottom (Mirror horizontal and rotate 90 CW)":
                    image.rotate(-270);
                    break;

                case "Left side, bottom (Rotate 270 CW)":
                    image.rotate(270);
                    break;

                default:
                    break;

            }

            BufferedImage imgBuff2 = image.getBufferedImage();
            ImageIO.write(imgBuff2, "png", fotoCorrecta);

            fisData.close();
            fisData.close();
            fotoPaso.delete();
            fotoPaso.deleteOnExit();

        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        return fotoCorrecta;

    }
    //CORREO EXPANSIÓN CORE
}
