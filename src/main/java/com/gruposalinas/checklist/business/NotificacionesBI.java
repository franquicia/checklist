package com.gruposalinas.checklist.business;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

//import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.checklist.domain.ChecklistPilaDTO;
import com.gruposalinas.checklist.domain.MovilInfoDTO;
import com.gruposalinas.checklist.util.Content;

public class NotificacionesBI {

	static String apiKey = "AIzaSyCN-msi853al5iJrm0ihXErHXj9k2SYOzI";
	private final String FCM_SERVICE = "https://fcm.googleapis.com/fcm/send";
	@Autowired
	private NotificacionBI notificacionBI;

	private static Logger logger = LogManager.getLogger(NotificacionesBI.class);

	public void enviarNotificacionJSON(String token, String titulo, String mensaje) throws IOException {

		String json = "";

		json = "{ " + "\"notification\":{ " + "\"sound\": \"OverrideSound\" " + "\"title\":\"" + titulo + "\", "
				+ "\"text\":\"" + mensaje + "\" " + "}, " + "\"registration_ids\":[\"" + token + "\"] " + "}";
		//System.out.println("JSON: " + json);
		
		OutputStream os = null;
		BufferedReader rd = null;
		HttpURLConnection conn = null;
		try {
			URL url = new URL(FCM_SERVICE);
			
			
			

			/*System.setProperty("java.net.useSystemProxies", "true");
			System.setProperty("http.proxyHost", "10.50.8.20");
			System.setProperty("http.proxyPort", "8080");
			// System.setProperty("http.proxyUser", "B196228");
			// System.setProperty("http.proxyPassword", "Bancoazteca2345");
			System.setProperty("http.proxySet", "true");

			System.setProperty("https.proxyHost", "10.50.8.20");
			System.setProperty("https.proxyPort", "8080");
			// System.setProperty("https.proxyUser", "B196228");
			// System.setProperty("https.proxyPassword", "Bancoazteca2345");
			System.setProperty("https.proxySet", "true");*/

			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");

			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "key=" + apiKey);
			// conn.setRequestProperty("Content-Type", "text/html");

			os = conn.getOutputStream();
			os.write(json.getBytes("UTF-8"));

			//System.out.println("CODIGO: " + conn.getResponseCode());

			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			// rd=new BufferedInputStream(conn.getInputStream());

			StringBuffer response = new StringBuffer();
			String line = "";

			while ((line = rd.readLine()) != null) {
				response.append(line);
			}
			
			//System.out.println(response.toString());
			
			JsonParser parser = new JsonParser();
			JsonObject jsonObject = parser.parse(response.toString()).getAsJsonObject();
			
			
			int totSucces = jsonObject.get("success").getAsInt();
			int totFail = jsonObject.get("failure").getAsInt();
			
			
			//System.out.println("Success: "+totSucces);
			//System.out.println("Success: "+totFail);
			
			rd.close();
			conn.disconnect();
		} catch (Exception e) {
			//System.out.println("ERROR: " + e.getMessage());

			//System.out.println("ERROR: " + e);

		}
		finally{
			if(rd!=null){
				safeClose(rd);
			}
			if(conn!=null){
				conn.disconnect();
			}
			if(os!=null){
				os.close();
			}
			
		}

	}
	public static void safeClose(BufferedReader rd) {
		  if (rd != null) {
		    try {
		      rd.close();
		    } catch (IOException e) {
		    	//System.out.println("ERROR: " + e);
		    }
		  }
	}



	public static Content createContent(String tokenUsuario, String titulo, String mensaje) {

		Content c = new Content();

		c.addRegId(tokenUsuario);
		c.createData(titulo, mensaje);

		return c;
	}

	/*public boolean enviarNotificacion(String tokenUsuario, String titulo, String mensaje) throws IOException {

		boolean resultado = false;
		Content content = createContent(tokenUsuario, titulo, mensaje);

		resultado = enviarNotificacionPush(content);

		return resultado;
	}*/

	@SuppressWarnings({ "unused", "unchecked" })
	public boolean enviarReporteAndroid() {

		String mensaje = "";
		String titulo = "Mi GestiÃ³n";
		boolean respuesta = false;
		// notificacionBI=new NotificacionBI();
		List<MovilInfoDTO> listaUsuario = notificacionBI.obtieneUsuariosNoti("ANDROID");
		try {
			if (listaUsuario != null) {
				for (MovilInfoDTO usr : listaUsuario) {

					Map<String, Object> reporte = notificacionBI.NotificacionPorcentaje(usr.getIdUsuario());
					int porcentaje = (Integer) reporte.get("porcentaje");

					mensaje = "";
					mensaje += "Avance:\n";
					//mensaje += "Hoy debes estar al 25%\n";

					List<ChecklistPilaDTO> listaReporte = (List<ChecklistPilaDTO>) reporte.get("listaReporte");

					//mensaje += "Estas en: \n";
					for (ChecklistPilaDTO cPila : listaReporte) {
						mensaje += "\n";

						if (cPila.getEstatus() < 25) {
							mensaje += "ðŸ‘Ž" + cPila.getNombreCheck() + ": " + cPila.getEstatus() + "% ";

						} else {
							mensaje += "ðŸ‘�" + cPila.getNombreCheck() + ": " + cPila.getEstatus() + "% ";

						}

					}
					logger.info("MENSAJE: " + mensaje);
					logger.info("TOKEN ANDROID: " + usr.getToken());

					enviarNotificacionJSON(usr.getToken(), titulo, mensaje);
				}
			}
			respuesta = true;
		} catch (Exception e) {
			logger.info("ocurrio algo: " + e.getMessage());
			respuesta = false;

		}
		return respuesta;

	}

	@SuppressWarnings({ "unused", "unchecked" })
	public boolean enviarReporteIOS() {

		String mensaje = "";
		String titulo = "Mi GestiÃ³n";
		boolean respuesta = false;
		// notificacionBI=new NotificacionBI();
		List<MovilInfoDTO> listaUsuario = notificacionBI.obtieneUsuariosNoti("IOS");
		try {
			if (listaUsuario != null) {
				for (MovilInfoDTO usr : listaUsuario) {

					Map<String, Object> reporte = notificacionBI.NotificacionPorcentaje(usr.getIdUsuario());
					int porcentaje = (Integer) reporte.get("porcentaje");

					mensaje = "";
					mensaje += "Avance:\n";
					//mensaje += "Hoy debes estar al 25%\n";

					List<ChecklistPilaDTO> listaReporte = (List<ChecklistPilaDTO>) reporte.get("listaReporte");

					//mensaje += "Estas en:";
					for (ChecklistPilaDTO cPila : listaReporte) {
						mensaje += "\n";

						if (cPila.getEstatus() < 25) {
							mensaje += "ðŸ‘Ž" + cPila.getNombreCheck() + ": " + cPila.getEstatus() + "% ";

						} else {
							mensaje += "ðŸ‘�" + cPila.getNombreCheck() + ": " + cPila.getEstatus() + "% ";

						}

					}
					logger.info("MENSAJE: " + mensaje);
					logger.info("TOKEN IOS: " + usr.getToken());

					enviarNotificacionJSON(usr.getToken(), titulo, mensaje);
				}
			}
			respuesta = true;
		} catch (Exception e) {
			logger.info("ocurrio algo: " + e.getMessage());
			respuesta = false;

		}
		return respuesta;

	}

}
