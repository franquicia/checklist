/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.logs.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.lookup.StrLookup;

/**
 *
 * @author cescobarh
 */
@Plugin(name = "profile", category = StrLookup.CATEGORY)
public class profileLookup implements StrLookup {

    private final static Properties props = new Properties();

    public profileLookup() {
        this.readProfileProperties();
    }

    private void readProfileProperties() {
        try (InputStream input = this.getClass().getClassLoader().getResourceAsStream("profile.properties")) {
            if (input == null) {
                return;
            }
            props.load(input);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    @Override
    public String lookup(String key) {
        return props.getProperty(key);
    }

    @Override
    public String lookup(LogEvent event, String key) {
        return props.getProperty(key);
    }

}
