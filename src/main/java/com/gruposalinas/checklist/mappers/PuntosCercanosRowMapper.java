package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.PuntosCercanosDTO;
import com.gruposalinas.checklist.domain.SucursalesCercanasDTO;

public class PuntosCercanosRowMapper implements RowMapper<PuntosCercanosDTO> {

	@Override
	public PuntosCercanosDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		PuntosCercanosDTO sucursalesCercanasDTO = new PuntosCercanosDTO();
		
		sucursalesCercanasDTO.setCeco(rs.getString("CENTRO_COSTO"));
		sucursalesCercanasDTO.setCanal(rs.getInt("CANAL"));
		sucursalesCercanasDTO.setNombreCeco(rs.getString("NOMBRE_PUNTO"));
		sucursalesCercanasDTO.setNuSucursal(rs.getString("ECONOMICO"));
		sucursalesCercanasDTO.setLatitud(rs.getString("LATITUD"));
		sucursalesCercanasDTO.setLongitud(rs.getString("LONGITUD"));
		sucursalesCercanasDTO.setDistancia(rs.getDouble("DISTANCIA"));
		
		return sucursalesCercanasDTO;
		
	}
	
	

}
