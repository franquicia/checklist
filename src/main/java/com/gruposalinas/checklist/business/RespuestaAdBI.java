package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.RespuestaAdDAO;
import com.gruposalinas.checklist.domain.RespuestaAdDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class RespuestaAdBI {

	private static Logger logger = LogManager.getLogger(RespuestaBI.class);

	@Autowired
	RespuestaAdDAO respuestaAdDAO;
	
	List<RespuestaAdDTO> listaRespuestas = null;
	
	public List<RespuestaAdDTO> buscaRespADP(String idRespAD, String idResp) {
		try{
			listaRespuestas = respuestaAdDAO.buscaRespADP(idRespAD, idResp);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Respuesta Adicional");
					
		}
				
		return listaRespuestas;
	}

	public int insertaRespAD(RespuestaAdDTO bean) {
		int idRespuesta = 0;
		try{
			idRespuesta = respuestaAdDAO.insertaRespAD(bean);
		} 
		catch (Exception e) {
			logger.info("No fue posible insertar la Respuesta Adicional");
			
		}
		
		return idRespuesta;	
	}
	
	public boolean actualizaRespAD(RespuestaAdDTO bean) {
		boolean respuesta = false;
		try{
			respuesta = respuestaAdDAO.actualizaRespAD(bean);
		}
		catch (Exception e) {
			logger.info("No fue posible actualizar la Respuesta Adicional");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaRespAD(int idRespAD) {
		boolean respuesta = false;
		try{
			respuesta = respuestaAdDAO.eliminaRespAD(idRespAD);
		}
		catch(Exception e){
			logger.info("No fue posible borrar la Respuesta Adicional");
			
		}
		
		return respuesta;
	}
}
