package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.AsignaExpDTO;


public interface AsignaExpDAO {

	  public int inserta(AsignaExpDTO bean)throws Exception;
		
		public boolean elimina(String ceco,String usuario_asig)throws Exception;
		
		//carga cuando no existe en la tabla check_usua
		public boolean cargaNuevo()throws Exception;
		
		public List<AsignaExpDTO> obtieneDatos(int version) throws Exception; 
		
		public List<AsignaExpDTO> obtieneInfo() throws Exception; 
		
		public boolean actualiza(AsignaExpDTO bean)throws Exception;
		//modifica el ceco que le mande a 1 todos los checklist de la version
		public boolean actualizaCheckU(AsignaExpDTO bean)throws Exception;
		
		public List<AsignaExpDTO> obtieneDatosCheckUsua(String usuario,String ceco) throws Exception; 
		
		public List<AsignaExpDTO> obtieneDatosAsignaVer(String usuario ) throws Exception; 
		
		public boolean CargaAsig()throws Exception;
	}