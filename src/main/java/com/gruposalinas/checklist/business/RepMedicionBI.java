package com.gruposalinas.checklist.business;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.RepMedicionDAO;
import com.gruposalinas.checklist.domain.RepMedicionDTO;

public class RepMedicionBI {

	private static final Logger logger = LogManager.getLogger(RepMedicionBI.class);

	@Autowired
	RepMedicionDAO repMedicionDAO;
	List<RepMedicionDTO> reporte = null;
	List<RepMedicionDTO> reportePregunta = null;


	public List<RepMedicionDTO> ReporteMedZonas(int idProtocolo, String fechaInicio, String fechaFin)
			throws Exception {
		try {
			reporte = null;
			reporte = repMedicionDAO.ReporteZonas(idProtocolo, fechaInicio, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return reporte;
	}
	
	public List<RepMedicionDTO> ReportePreguntas(RepMedicionDTO bean)
			throws Exception {
		try {
			reportePregunta = null;
			reportePregunta = repMedicionDAO.ReportePreguntas(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información" + e);
		}

		return reportePregunta;
	}


}