package com.gruposalinas.checklist.domain;

public class LlaveDTO {
	
	private int idLlave;
	private String llave;
	private String descripcion;
	
	public int getIdLlave() {
		return idLlave;
	}
	public void setIdLlave(int idLlave) {
		this.idLlave = idLlave;
	}
	public String getLlave() {
		return llave;
	}
	public void setLlave(String llave) {
		this.llave = llave;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	

}
