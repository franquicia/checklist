package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.RecursoDTO;
import com.gruposalinas.checklist.mappers.RecursoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class RecursoDAOImpl extends DefaultDAO implements RecursoDAO {

    private static Logger logger = LogManager.getLogger(RecursoDAOImpl.class);

    private DefaultJdbcCall jdbcBuscaRecurso;
    private DefaultJdbcCall jdbcInsertaRecurso;
    private DefaultJdbcCall jdbcActualizaRecurso;
    private DefaultJdbcCall jdbcEliminaRecurso;

    public void init() {

        jdbcBuscaRecurso = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMRECURSO")
                .withProcedureName("SP_SEL_RECURSO")
                .returningResultSet("RCL_RECURSO", new RecursoRowMapper());

        jdbcInsertaRecurso = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMRECURSO")
                .withProcedureName("SP_INS_RECURSO");

        jdbcActualizaRecurso = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMRECURSO")
                .withProcedureName("SP_ACT_RECURSO");

        jdbcEliminaRecurso = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMRECURSO")
                .withProcedureName("SP_DEL_RECURSO");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<RecursoDTO> buscaRecurso(String idRecurso) throws Exception {
        Map<String, Object> out = null;

        List<RecursoDTO> listaRecurso = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_RECURSO", idRecurso);

        out = jdbcBuscaRecurso.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PAADMRECURSO.SP_SEL_RECURSO}");
        listaRecurso = (List<RecursoDTO>) out.get("RCL_RECURSO");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener el Recurso  con id: " + idRecurso);
        } else {
            return listaRecurso;
        }

        return null;
    }

    @Override
    public int insertaRecurso(RecursoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idRecurso = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NOMBRE", bean.getNombreRecurso());

        out = jdbcInsertaRecurso.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PAADMRECURSO.SP_INS_RECURSO}");
        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDRECURSO");
        idRecurso = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar el Recurso");
        } else {
            return idRecurso;
        }

        return idRecurso;
    }

    @Override
    public boolean actualizaRecurso(RecursoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDRECURSO", bean.getIdRecurso())
                .addValue("PA_NOMBRE", bean.getNombreRecurso());

        out = jdbcActualizaRecurso.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PAADMRECURSO.SP_ACT_RECURSO}");
        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar Recurso con id : " + bean.getIdRecurso());
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaRecurso(int idRecurso) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDRECURSO", idRecurso);

        out = jdbcEliminaRecurso.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PAADMRECURSO.SP_DEL_RECURSO}");
        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al eliminar Recurso con id: " + idRecurso);
        } else {
            return true;
        }

        return false;
    }

}
