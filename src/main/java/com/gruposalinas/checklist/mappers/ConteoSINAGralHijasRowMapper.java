package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;

public class ConteoSINAGralHijasRowMapper implements RowMapper<ChecklistPreguntasComDTO> {

	public ChecklistPreguntasComDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistPreguntasComDTO con = new ChecklistPreguntasComDTO();


		con.setClasif(rs.getString("FCCLASIFICA"));
		con.setIdBitaGral(rs.getInt("FIID_BITGRAL"));
	    con.setPregSi(rs.getInt("SUMA_SI"));
		con.setPregNa(rs.getInt("SUMA_NA"));
	
		return con;
	}

}
