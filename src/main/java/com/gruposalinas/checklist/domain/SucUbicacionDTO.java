package com.gruposalinas.checklist.domain;

public class SucUbicacionDTO {
	


	private int ceco;
	private String  calles;
	private String ciudad;
	private String codigoPos;
	private int numSuc;
	private String nombCeco;
	private int idcanal;
	private String canal;
	private int idpais;
	private String pais;
	private double longitud;
	private double latitud;
	public int getCeco() {
		return ceco;
	}
	public void setCeco(int ceco) {
		this.ceco = ceco;
	}
	public String getCalles() {
		return calles;
	}
	public void setCalles(String calles) {
		this.calles = calles;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getCodigoPos() {
		return codigoPos;
	}
	public void setCodigoPos(String codigoPos) {
		this.codigoPos = codigoPos;
	}
	public int getNumSuc() {
		return numSuc;
	}
	public void setNumSuc(int numSuc) {
		this.numSuc = numSuc;
	}
	public String getNombCeco() {
		return nombCeco;
	}
	public void setNombCeco(String nombCeco) {
		this.nombCeco = nombCeco;
	}
	public int getIdcanal() {
		return idcanal;
	}
	public void setIdcanal(int idcanal) {
		this.idcanal = idcanal;
	}
	public String getCanal() {
		return canal;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
	public int getIdpais() {
		return idpais;
	}
	public void setIdpais(int idpais) {
		this.idpais = idpais;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public double getLongitud() {
		return longitud;
	}
	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}
	public double getLatitud() {
		return latitud;
	}
	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}
	@Override
	public String toString() {
		return "SucUbicacionDTO [ceco=" + ceco + ", calles=" + calles + ", ciudad=" + ciudad + ", codigoPos="
				+ codigoPos + ", numSuc=" + numSuc + ", nombCeco=" + nombCeco + ", idcanal=" + idcanal + ", canal="
				+ canal + ", idpais=" + idpais + ", pais=" + pais + ", longitud=" + longitud + ", latitud="
				+ latitud + "]";
	}
	
	
}
