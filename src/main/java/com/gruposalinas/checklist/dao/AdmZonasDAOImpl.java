package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.AdminZonaDTO;
import com.gruposalinas.checklist.mappers.AdmZonasRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class AdmZonasDAOImpl extends DefaultDAO implements AdmZonasDAO {

    private static Logger logger = LogManager.getLogger(AdmZonasDAOImpl.class);

    DefaultJdbcCall jdbcObtieneZonaById;
    DefaultJdbcCall jdbcInsertaZona;
    DefaultJdbcCall jdbcActualizaZona;
    DefaultJdbcCall jdbcEliminaZona;

    public void init() {
        jdbcObtieneZonaById = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINZONA")
                .withProcedureName("SP_SEL_ZONA")
                .returningResultSet("RCL_INFO", new AdmZonasRowMapper());

        jdbcInsertaZona = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINZONA")
                .withProcedureName("SP_INS_ZONA");

        jdbcActualizaZona = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINZONA")
                .withProcedureName("SP_ACT_ZONA");

        jdbcEliminaZona = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMINZONA")
                .withProcedureName("SP_DEL_ZONA");
    }

    public boolean insertaZona(AdminZonaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        boolean resp = false;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCDESCRIPCION", bean.getDescripcion())
                .addValue("PA_FIIDESTATUS", bean.getIdStatus())
                .addValue("PA_FIIDITIPO", bean.getIdTipo());

        out = jdbcInsertaZona.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICA.PAADMINZONA.SP_INS_ZONA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar la zona");
        } else {
            resp = true;
        }

        return resp;
    }

    public boolean actualizaZona(AdminZonaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        boolean resp = false;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIIDZONA", bean.getIdZona())
                .addValue("PA_FCDESCRIPCION", bean.getDescripcion())
                .addValue("PA_FIIDESTATUS", bean.getIdStatus())
                .addValue("PA_FIIDITIPO", bean.getIdTipo());

        out = jdbcActualizaZona.execute(in);
        logger.info("Funcion ejecutada: {FRANQUICA.PAADMINZONA.SP_ACT_ZONA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al actualizar la zona: " + bean.getIdZona());
        } else {
            resp = true;
        }

        return resp;
    }

    public boolean eliminaZona(AdminZonaDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        boolean resp = false;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIIDZONA", bean.getIdZona());

        out = jdbcEliminaZona.execute(in);
        logger.info("Funcion ejecutada: {FRANQUICA.PAADMINZONA.SP_DEL_ZONA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al borrar la Zona: " + bean.getIdZona());
        } else {
            resp = true;
        }

        return resp;
    }

    @SuppressWarnings("unchecked")
    public ArrayList<AdminZonaDTO> obtieneZonaById(AdminZonaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        ArrayList<AdminZonaDTO> zona = new ArrayList<>();

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIIDZONA", bean.getIdZona());

        out = jdbcObtieneZonaById.execute(in);
        logger.info("Funcion ejecutada: {FRANQUICA.PAADMINZONA.SP_SEL_ZONA}");
        zona = (ArrayList<AdminZonaDTO>) out.get("RCL_INFO");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al consultar la zona: " + bean.getIdZona());
        }

        return zona;
    }

    @SuppressWarnings("unchecked")
    public ArrayList<AdminZonaDTO> obtieneZonas() throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        ArrayList<AdminZonaDTO> zonas = new ArrayList<>();

        out = jdbcObtieneZonaById.execute();
        logger.info("Funcion ejecutada: {FRANQUICA.PAADMINZONA.SP_SEL_ZONA}");
        zonas = (ArrayList<AdminZonaDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al consultar las zonas");
        }

        return zonas;
    }

}
