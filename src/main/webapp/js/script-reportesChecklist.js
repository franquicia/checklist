$body = $("body");
$(document).on({
    ajaxStart: function() {$body.addClass("loading");},
    ajaxStop: function() {$body.removeClass("loading");}    
});

var opcionPadre=0;
var porcentajeL=0;
$(window).load(function() {
	//fechas();
	$('#vistaPrevia').hide();
	$('#vistaPreviaReactivo').hide();
	$('#logRegresa-btn').hide();
	$('#logRegresa-btn2').hide();
	$('#idHijoOperando').hide();
	$('#idHijoVisitaTituloTabla').hide();
	$('#idHijoVisitaFinTabla').hide();
	$("#logBuscar-btn").hide();
	$('#idInfoPiePagina2').hide();
	$('#idInfoPorcentaje').hide();
	//document.getElementById("idHijoTituloGen").style.display="none";
	$('#idHijoTituloGen').hide();
	$('.cuadrado-1').hide();
	$('.cuadrado-3').hide();
	$('.cuadrado-5').hide();
	$('.cuadrado-7').hide();
	
	$('#ventanaEmergente').hide();

	//$('#idGeneral').hide();
	$('#idGeneralDetalle').hide();
	
	document.getElementById("fechaInicioVr").disabled = true;
	document.getElementById("fechaFinVr").disabled = true;

	iniciaAno();
	abreVistaDias();
	asignarEventosCombo();	
	llenadoFiltros(idUsuario,nivelAdmin,negocioId,paisId,territorioId,zonaId,regionId,negocioName,paisName,territorioName,zonaName,regionName);

	/*INICIOA CODIGO DE ACORDEON ALEX*/
	activeItem = $("#accordion li:last");
    $(activeItem).addClass('active');
 
    $("#accordion li").click(function(){
        $(activeItem).animate({width: "30px"}, {duration:300, queue:false});
        $(this).animate({width: "85%"}, {duration:300, queue:false});
        activeItem = this;
    });
	
});

function abreVistaDias() {
	
	$("#idPadreVisita").on("click",
	function clickli(e) {
		idPadreVisita(0);
	});
		
	$("#idPadreOperando").on("click",
	function clickli(e) {
		idPadreOperando(0);
	});

	$(".idVistaNombre").on("click",
	function clickli(e) {
		idVistaNombre();
	});
	
	$(".idOperandoNombre").on("click",
	function clickli(e) {
		idOperandoNombre();
	});
	
	$(".logRegresa-btn").on("click",
	function clickli(e) {
		$('#vistaPrevia').hide();
		$('#idHijoVisita').show();
		
		$('#idHijoVisitaTabla').show();
		$('.cuadrado-1').show();
		$('.cuadrado-3').hide();
		$('#idHijoVisitaTituloTabla').hide();	
		$('#idHijoVisitaFinTabla').hide();
		$('#tablaVisitaHijo').hide();
		$('#logRegresa-btn').hide();
		$('#idInfoPiePagina1').show();
		//$('#idInfoPiePagina2').show();	
	});
	
	
	$(".logRegresa-btn2").on("click",
	function clickli(e) {
		$('#vistaPreviaReactivo').show();
		$('#tablaOperando').show();
		$('.cuadrado-5').show();
		$('.cuadrado-7').hide();
		$('#tablaOperandoHijo').hide();
		$('#logRegresa-btn2').hide();
		$('#idReactivoVista').show();
	});
}


function fechas() {
	//$.datepicker.setDefaults($.datepicker.regional["es"]);
    $( "#fechaInicioVr" ).datepicker({
    }).datepicker("setDate", new Date()),cambiaFecha(0);
    
    $( "#fechaFinVr" ).datepicker({
   	 	minDate: "1/10/2016",    	
        maxDate: new Date(),
    });
    
}

function cambiaFecha(opcion){
	if(opcion==0)
		var fecha= document.getElementById("fechaInicioVr").value;
	if(opcion==1)
		var fecha= document.getElementById("fechaFinVr").value;
	
	
	
	$.ajax({
		type : "GET",
		url : urlServer + '../getCambioFecha.json?fecha='+fecha+'&opcion='+opcion,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			if(opcion==0){
				$("#fechaFinVr").val(json);
				
			}
			if(opcion==1)
				$("#fechaInicioVr").val(json);			
		},
		error : function() {
			alert('Favor de actualizar la pagina');
		}
	});
}

function idPadreVisita(valor){
	//alert('entro ');
	opcionPadre=0;
	var pai=$("#paisVis .seleccionadoVisvR").attr("id");
	if(pai=="Selecciona una Opcion")
		valor=1;
	if(valor==0)
		logBuscarbtn();
	$("#idPadreVisita").css("background", "#2cab6e"); 
	$("#idPadreOperando").css("background", "#b3b4b2");  
	$('#idHijoVisita').show();
	$('#idHijoVisitaTabla').show();
	$('.cuadrado-1').show();
	$('.cuadrado-3').hide();
	$('.cuadrado-5').hide();
	$('.cuadrado-7').hide();
	$('#idHijoVisitaTituloTabla').hide();
	$('#idHijoVisitaFinTabla').hide();
	$('#idHijoOperando').hide();
	$('#vistaPrevia').hide();
	$('#vistaPreviaReactivo').hide();
	$('#idInfoPiePagina1').show();
	$('#idInfoPorcentaje').hide();
	estOpe=0;
	$('#idHijoVisita').show();
	$('#tablaVisitaHijo').hide();
	$('#logRegresa-btn').hide();
	$('#logRegresa-btn2').hide();
	$('#idFechaRangos').show();
	$('#idFechaDia').hide();
}

function idPadreOperando(valor){
	//alert('entro');
	opcionPadre=1;
	var pai=$("#paisVis .seleccionadoVisvR").attr("id");
	if(pai=="Selecciona una Opcion")
		valor=1;
	if(valor==0)
		logBuscarbtn();
	$("#idPadreVisita").css("background", "#b3b4b2");
	$("#idPadreOperando").css("background", "#2cab6e");  
	$('#idHijoVisita').hide();
	$('#idHijoVisitaTabla').hide();
	$('.cuadrado-1').hide();
	$('.cuadrado-3').hide();
	$('.cuadrado-5').show();
	$('.cuadrado-7').hide();
	$('#idHijoVisitaTituloTabla').hide();
	$('#idHijoVisitaFinTabla').hide();
	$('#idHijoOperando').show();
	$('#vistaPrevia').hide();
	$('#vistaPreviaReactivo').show();
	$('#idInfoPiePagina1').hide();
	$('#idInfoPiePagina2').hide();
	estVis=0;
	$('#idReactivoVista').show();
	$('#tablaOperando').show();	
	$('#tablaOperandoHijo').hide();
	$('#logRegresa-btn').hide();
	$('#logRegresa-btn2').hide();
	$('#idFechaRangos').show();
	$('#idFechaDia').hide();
	$('#tablaVisitaHijo').hide();
}
function idVistaNombre(){
	$('#vistaPreviaReactivo').hide();
	$('#vistaPrevia').show();
	$('#idHijoVisita').hide();
	$('#idHijoVisitaTabla').hide();
	$('.cuadrado-1').hide();
	$('.cuadrado-3').show();
	$('#idHijoVisitaTituloTabla').show();
	$('#idHijoVisitaFinTabla').show();
	$('#tablaVisitaHijo').show();
	$('#logRegresa-btn').show();
}

function idOperandoNombre(){
	$('#vistaPrevia').hide();
	$('#tablaOperando').hide();
	$('.cuadrado-5').hide();
	$('.cuadrado-7').show();
	$('#tablaOperandoHijo').show();
	$('#logRegresa-btn2').show();
	$('#idReactivoVista').hide();
}

function logBuscarbtn(){
	
	var fechaInicio=document.getElementById("fechaInicioVr").value;
	var fechaFin=document.getElementById("fechaFinVr").value;
	

	if(fechaInicio==""){
		alert("Selecciona un perido");
	}else{

		var pai=$("#paisVis .seleccionadoVisvR").attr("id");
		var ter=$("#territorioVis .seleccionadoVisvR").attr("id");
		var zon=$("#zonaVis .seleccionadoVisvR").attr("id");
		var reg=$("#regionVis .seleccionadoVisvR").attr("id");
		
		//alert('ter' + ter);
		//alert('zon' + zon);
		//alert('reg' + reg);
	
	
		if(pai=="Selecciona una Opcion"){
			alert('Filtra hasta un Pais')
		}else if(ter=="Selecciona una Opcion"){
			
			cambiarPais();	
			var nombre=$("#paisVis .seleccionadoVisvR").text();
			var nombreDos='suc';
			var priValor=1;
			var segValor=0;
			var terValor=0;
			var cuaValor=null;
			//var id=$("#paisVis .seleccionadoVisvR").attr("id");	
			var id=230001;
			var fechaFin=document.getElementById("fechaFinVr").value;	
			
			rellenadoListaReporte(nombre,nombreDos,priValor,segValor,terValor,cuaValor,id,fechaInicio,fechaFin,0);
			
		}else if(zon=="Selecciona una Opcion"){
			
			cambiarTerritorio();	
			var nombre=$("#territorioVis .seleccionadoVisvR").text();
			var nombreDos='suc';
			var priValor=2;
			var segValor=1;
			var terValor=0;
			var cuaValor=null;
			var id=$("#territorioVis .seleccionadoVisvR").attr("id");	
			var fechaInicio=document.getElementById("fechaInicioVr").value;
			var fechaFin=document.getElementById("fechaFinVr").value;	
			
			rellenadoListaReporte(nombre,nombreDos,priValor,segValor,terValor,cuaValor,id,fechaInicio,fechaFin,0);
			
		}else if(reg=="Selecciona una Opcion"){
			
			cambiarZona();			
			var nombre=$("#zonaVis .seleccionadoVisvR").text();
			var nombreDos='suc';
			var priValor=3;
			var segValor=2;
			var terValor=1;
			var cuaValor=null;
			var id=$("#zonaVis .seleccionadoVisvR").attr("id");
			var fechaInicio=document.getElementById("fechaInicioVr").value;
			var fechaFin=document.getElementById("fechaFinVr").value;	
			
			rellenadoListaReporte(nombre,nombreDos,priValor,segValor,terValor,cuaValor,id,fechaInicio,fechaFin,1);
			
		}else{
			$("#valorRegion").val($("#regionVis .seleccionadoVisvR").attr("id"));
			
			var nombre=$("#regionVis .seleccionadoVisvR").text();
			var nombreDos='suc';
			var priValor=4;
			var segValor=3;
			var terValor=2;
			var cuaValor=1;
			var id=$("#regionVis .seleccionadoVisvR").attr("id");
			var fechaInicio=document.getElementById("fechaInicioVr").value;
			var fechaFin=document.getElementById("fechaFinVr").value;				
			
			rellenadoListaReporte(nombre,nombreDos,priValor,segValor,terValor,cuaValor,id,fechaInicio,fechaFin,2);
		}
	}
	
}


function llenadoFiltros(idUsuario,nivelAdmin,negocioId,paisId,territorioId,zonaId,regionId,negocioName,paisName,territorioName,zonaName,regionName){
	var fechaInicio=document.getElementById("fechaInicioVr").value;
	var fechaFin=document.getElementById("fechaFinVr").value;
	
	checklist();

	if(nivelAdmin==4){		
		cambiarInicioCanal(idUsuario);
	}
	if(nivelAdmin==3){
		$("#canalVis .seleccionadoVisvR").val(negocioId);
		$("#canalVis .seleccionadoVisvR").text(negocioName);
		$("#canalVis .seleccionadoVisvR").attr("id", negocioId);
		$("#valorCanal").val(negocioId);
		
		$("#paisVis .seleccionadoVisvR").val(paisId);
		$("#paisVis .seleccionadoVisvR").text(paisName);
		$("#paisVis .seleccionadoVisvR").attr("id",paisId);
		$("#valorPais").val(paisId);
		
		//cambiarCanalValor(negocioId);
		cambiarTerritorioValor(territorioId);
	}
	if(nivelAdmin==2){
		$("#canalVis .seleccionadoVisvR").val(negocioId);
		$("#canalVis .seleccionadoVisvR").text(negocioName);
		$("#canalVis .seleccionadoVisvR").attr("id", negocioId);
		$("#valorCanal").val(negocioId);
		
		$("#paisVis .seleccionadoVisvR").val(paisId);
		$("#paisVis .seleccionadoVisvR").text(paisName);
		$("#paisVis .seleccionadoVisvR").attr("id",paisId);
		$("#valorPais").val(paisId);
				
		$("#territorioVis .seleccionadoVisvR").val(territorioId);
		$("#territorioVis .seleccionadoVisvR").text(territorioName);
		$("#territorioVis .seleccionadoVisvR").attr("id",territorioId);
		$("#valorTerritorio").val(territorioId);
		
		cambiarTerritorioValor(territorioId);
				
		var nombre=$("#territorioVis .seleccionadoVisvR").text();
		var nombreDos='suc';
		var priValor=2;
		var segValor=1;
		var terValor=0;
		var cuaValor=null;
		var id=territorioId;
		
		rellenadoListaReporte(nombre,nombreDos,priValor,segValor,terValor,cuaValor,id,fechaInicio,fechaFin,0);
	}
	if(nivelAdmin==1){
		$("#canalVis .seleccionadoVisvR").val(negocioId);
		$("#canalVis .seleccionadoVisvR").text(negocioName);
		$("#canalVis .seleccionadoVisvR").attr("id", negocioId);
		$("#valorCanal").val(negocioId);
		
		$("#paisVis .seleccionadoVisvR").val(paisId);
		$("#paisVis .seleccionadoVisvR").text(paisName);
		$("#paisVis .seleccionadoVisvR").attr("id",paisId);
		$("#valorPais").val(paisId);
		
		$("#territorioVis .seleccionadoVisvR").val(territorioId);
		$("#territorioVis .seleccionadoVisvR").text(territorioName);
		$("#territorioVis .seleccionadoVisvR").attr("id",territorioId);
		$("#valorTerritorio").val(territorioId);
		
		$("#zonaVis .seleccionadoVisvR").val(zonaId);
		$("#zonaVis .seleccionadoVisvR").text(zonaName);
		$("#zonaVis .seleccionadoVisvR").attr("id",zonaId);
		$("#valorZona").val(zonaId);
		
		cambiarZonaValor(zonaId);
		
		var nombre=$("#zonaVis .seleccionadoVisvR").text();
		var nombreDos='suc';
		var priValor=3;
		var segValor=2;
		var terValor=1;
		var cuaValor=null;
		var id=zonaId;
		
		rellenadoListaReporte(nombre,nombreDos,priValor,segValor,terValor,cuaValor,id,fechaInicio,fechaFin,1);
		
	}
	if(nivelAdmin==0){
		$("#canalVis .seleccionadoVisvR").val(negocioId);
		$("#canalVis .seleccionadoVisvR").text(negocioName);
		$("#canalVis .seleccionadoVisvR").attr("id", negocioId);
		$("#valorCanal").val(negocioId);
		
		$("#paisVis .seleccionadoVisvR").val(paisId);
		$("#paisVis .seleccionadoVisvR").text(paisName);
		$("#paisVis .seleccionadoVisvR").attr("id",paisId);
		$("#valorPais").val(paisId);
		
		$("#territorioVis .seleccionadoVisvR").val(territorioId);
		$("#territorioVis .seleccionadoVisvR").text(territorioName);
		$("#territorioVis .seleccionadoVisvR").attr("id",territorioId);
		$("#valorTerritorio").val(territorioId);
		
		$("#zonaVis .seleccionadoVisvR").val(zonaId);
		$("#zonaVis .seleccionadoVisvR").text(zonaName);
		$("#zonaVis .seleccionadoVisvR").attr("id",zonaId);
		$("#valorZona").val(zonaId);
		
		$("#regionVis .seleccionadoVisvR").val(regionId);
		$("#regionVis .seleccionadoVisvR").text(regionName);
		$("#regionVis .seleccionadoVisvR").attr("id",regionId);
		$("#valorRegion").val(regionId);
						
		var nombre=$("#regionVis .seleccionadoVisvR").text();
		var nombreDos='suc';
		var priValor=4;
		var segValor=3;
		var terValor=2;
		var cuaValor=1;
		var id=regionId;
		
		rellenadoListaReporte(nombre,nombreDos,priValor,segValor,terValor,cuaValor,id,fechaInicio,fechaFin,2);		
	}
}

function vistaCeco(idCeco,nombreCeco,plan,real,avance,distintos,imagen){	
	
	idVistaNombre();
	
	//alert('entre al vista ' +idCeco +' . ' + nombreCeco +' . '+plan+' . '+real+' . '+avance+' . '+distintos);
	document.getElementById('idNombreCeco').innerHTML ="";
	$('#idNombreCeco').append("<a class='fuenteRegVis' onclick='tablaVisitaHijo("+idCeco+");'>"+nombreCeco+"</a>");
	//document.getElementById('idNombreCeco').innerHTML = nombreCeco;
	document.getElementById('idPlanCeco').innerHTML = plan;
	document.getElementById('idRealCeco').innerHTML = distintos;
	document.getElementById('idAvanceCeco').innerHTML = "";
	document.getElementById('idDistintoCeco').innerHTML = real;
	
	//alert(imagen)	
	
	if(avance>=porcentajeL){
		$('#idAvanceCeco').append("<div style='margin-left:17%; width:70%; max-width:50px; position:relative;' class='circle-text-verde'>" +
				(imagen!="null" ? "<img class='imagen-iconoProblemasVistaCeco' width='50%' height='40%' src='../images/icono_problemas.png'/>":"")+
				"<div>"+avance+"%</div></div>");	
	}
	/*if(avance<90 && avance>79){
		$('#idAvanceCeco').append("<div style='margin-left:17%; width:70%; max-width:50px; position:relative;' class='circle-text-amarillo'>" +
				(imagen!="null" ? "<img class='imagen-iconoProblemasVistaCeco' width='50%' height='40%' src='../images/icono_problemas.png'/>":"")+
				"<div>"+avance+"%</div></div>");	
	}*/
	if(avance<porcentajeL){
		$('#idAvanceCeco').append("<div style='margin-left:17%; width:70%; max-width:50px; position:relative;' class='circle-text-rojo'>" +				
				(imagen!="null" ? "<img class='imagen-iconoProblemasVistaCeco' width='50%' height='40%' src='../images/icono_problemas.png'/>":"")+
				"<div>"+avance+"%</div></div>");
	}
	
	tablaVisitaHijo(idCeco);
	
}

function vistaCecoSucursal(idCeco,nombreCeco,plan,real,avance,distintos,imagen){	
	
	idVistaNombre();
	
	//alert('entre al vista ' +idCeco +' . ' + nombreCeco +' . '+plan+' . '+real+' . '+avance+' . '+distintos);
	
	document.getElementById('idNombreCeco').innerHTML = nombreCeco;
	document.getElementById('idPlanCeco').innerHTML = plan;
	document.getElementById('idRealCeco').innerHTML = distintos;
	document.getElementById('idAvanceCeco').innerHTML = "";
	document.getElementById('idDistintoCeco').innerHTML = real;
	
	if(avance>=porcentajeL){
		$('#idAvanceCeco').append("<div style='margin-left:17%; width:70%; max-width:50px; position:relative;' class='circle-text-verde'>" +
				(imagen!="null" ? "<img class='imagen-iconoProblemasVistaCeco' width='50%' height='40%' src='../images/icono_problemas.png'/>":"")+
				"<div>"+avance+"%</div></div>");	
	}
	/*if(avance<90 && avance>79){
		$('#idAvanceCeco').append("<div style='margin-left:17%; width:70%; max-width:50px; position:relative;' class='circle-text-amarillo'>" +
				(imagen!="null" ? "<img class='imagen-iconoProblemasVistaCeco' width='50%' height='40%' src='../images/icono_problemas.png'/>":"")+
				"<div>"+avance+"%</div></div>");	
	}*/
	if(avance<porcentajeL){
		$('#idAvanceCeco').append("<div style='margin-left:17%; width:70%; max-width:50px; position:relative;' class='circle-text-rojo'>" +				
				(imagen!="null" ? "<img class='imagen-iconoProblemasVistaCeco' width='50%' height='40%' src='../images/icono_problemas.png'/>":"")+
				"<div>"+avance+"%</div></div>");
	}
	
	tablaVisitaHijoSucursal(idCeco,nombreCeco);
	
}

function vistaCecoSucursalCalendar(idPinta,tamPinta,idCecoPadre,idCeco,nombreCeco,plan,real,avance,distintos,imagen){	
	
	$("#idTotal").show();
	
	for (var h = 0; h < tamPinta; h++) {
		
		$("#"+(document.getElementById("00"+h).childNodes[2].id)).hide();
		$("#"+(document.getElementById("00"+h).childNodes[1].id)).show();
		
		if(idPinta=="00"+h){
			document.getElementById("00"+h).style.backgroundColor = "#285a43";
			document.getElementById(document.getElementById("00"+h).childNodes[0].id).style.color = "white";
			document.getElementById(document.getElementById("00"+h).childNodes[1].id).style.color = "white";
		}else{
			if((h%2)==0){
				document.getElementById("00"+h).style.backgroundColor = "white";
				document.getElementById(document.getElementById("00"+h).childNodes[0].id).style.color = "black";
				document.getElementById(document.getElementById("00"+h).childNodes[1].id).style.color = "black";
			}				
			if((h%2)==1){
				document.getElementById("00"+h).style.backgroundColor = "#f0eded ";
				document.getElementById(document.getElementById("00"+h).childNodes[0].id).style.color = "black";
				document.getElementById(document.getElementById("00"+h).childNodes[1].id).style.color = "black";
			}				
		}			
	}
	
	tablaVisitaHijoSucursalCalendar(idCecoPadre,idCeco);
}

function vistaCecoNivel(idCeco,nomCeco){
	//alert('entro al ceco '+ idCeco + ', ' + nomCeco);
	
	var pai=$("#paisVis .seleccionadoVisvR").attr("id");
	var ter=$("#territorioVis .seleccionadoVisvR").attr("id");
	var zon=$("#zonaVis .seleccionadoVisvR").attr("id");
	var reg=$("#regionVis .seleccionadoVisvR").attr("id");
	var fechaInicio=document.getElementById("fechaInicioVr").value;
	var fechaFin=document.getElementById("fechaFinVr").value;
	//alert('ter' + ter);
	//alert('zon' + zon);
	//alert('reg' + reg);


	if(pai=="Selecciona una Opcion"){
		//alert('Filtra un Pais')
	}else if(ter=="Selecciona una Opcion"){
		
		$("#territorioVis .seleccionadoVisvR").val(idCeco);
		$('#territorioVis .seleccionadoVisvR').text(nomCeco);
		$("#territorioVis .seleccionadoVisvR").attr("id",idCeco);		
		cambiarTerritorioValor(idCeco);
		
		var nombre=nomCeco;
		var nombreDos='suc';
		var priValor=2;
		var segValor=1;
		var terValor=0;
		var cuaValor=null;
		var id=idCeco;	
		var fechaInicio=document.getElementById("fechaInicioVr").value;
		var fechaFin=document.getElementById("fechaFinVr").value;	
		//alert('antes del rellenado');
		rellenadoListaReporte(nombre,nombreDos,priValor,segValor,terValor,cuaValor,id,fechaInicio,fechaFin,0);
		
	}else if(zon=="Selecciona una Opcion"){
				
		$("#zonaVis .seleccionadoVisvR").val(idCeco);
		$('#zonaVis .seleccionadoVisvR').text(nomCeco);
		$("#zonaVis .seleccionadoVisvR").attr("id",idCeco);
		cambiarZonaValor(idCeco);	
		
		var nombre=nomCeco;
		var nombreDos='suc';
		var priValor=3;
		var segValor=2;
		var terValor=1;
		var cuaValor=null;
		var id=idCeco;
		var fechaInicio=document.getElementById("fechaInicioVr").value;
		var fechaFin=document.getElementById("fechaFinVr").value;	
		
		rellenadoListaReporte(nombre,nombreDos,priValor,segValor,terValor,cuaValor,id,fechaInicio,fechaFin,1);		
		
	}else if(reg=="Selecciona una Opcion"){
				
		$("#regionVis .seleccionadoVisvR").val(idCeco);
		$('#regionVis .seleccionadoVisvR').text(nomCeco);
		$("#regionVis .seleccionadoVisvR").attr("id",idCeco);
		
		
		var nombre=nomCeco;
		var nombreDos='suc';
		var priValor=4;
		var segValor=3;
		var terValor=2;
		var cuaValor=1;
		var id=idCeco;
		var fechaInicio=document.getElementById("fechaInicioVr").value;
		var fechaFin=document.getElementById("fechaFinVr").value;	
		
		rellenadoListaReporte(nombre,nombreDos,priValor,segValor,terValor,cuaValor,id,fechaInicio,fechaFin,2);		
		
		
	}
	
}

function asignarEventosCombo() {
	$(".cajaVisvR").off();

	$(".cajaVisvR").hover(
		function(e) {
			asignarEventosCombo(); 
			
			var lista = $(this).find("ul");
			var triangulo = $(this).find("span:last-child");
			e.preventDefault();

			$(this).find("ul").toggle();
			if (lista.is(":hidden")) {
				triangulo.removeClass("triangulosupVisvR").addClass("trianguloinfVisvR");
				triangulo.parent().css('z-index', 0);
			} else {
				triangulo.removeClass("trianguloinfVisvR").addClass("triangulosupVisvR");
				triangulo.parent().css('z-index', 3000);
			}
		});

	$(".cajaVisvR").on("click","li",
		function clickli(e) {
			var texto = $(this).text();
			var seleccionadoVis = $(this).parent().prev();
			var lista = $(this).closest("ul");
			var triangulo = $(this).parent().next();
			e.preventDefault();
			e.stopPropagation();
			seleccionadoVis.text(texto);
			lista.hide();
			triangulo.removeClass("triangulosupVisvR").addClass("trianguloinfVisvR");
			triangulo.parent().css('z-index', 0);
			$(".cajaVisvR").unbind('mouseleave');
			var valor = $(this).val();
			seleccionadoVis.attr("id", valor);
			var divActivo = $(this).parents("li").find(".cajaVisvR").attr("id");

			if (divActivo == 'checklistVis'){
				//$("#logBuscar-btn").hide();
				cambiarChecklist();
				logBuscarbtn();
			}
		
			if (divActivo == 'canalVis'){
				$("#logBuscar-btn").hide();
				cambiarCanal();
			}
				
			if (divActivo == 'paisVis'){				
				var fechaInicio=document.getElementById("fechaInicioVr").value;
				
				if(fechaInicio==""){
					alert("Selecciona un perido");
				}else{				
					$("#logBuscar-btn").show();
					cambiarPais();
					
					//alert('hola pais '+ $("#paisVis .seleccionadoVisvR").text);
					var nombre=$("#paisVis .seleccionadoVisvR").text();
					var nombreDos='suc';
					var priValor=1;
					var segValor=0;
					var terValor=0;
					var cuaValor=null;
					//var id=$("#paisVis .seleccionadoVisvR").attr("id");
					//var id=230001
					var fechaInicio=document.getElementById("fechaInicioVr").value;
					var fechaFin=document.getElementById("fechaFinVr").value;		
				}
				//rellenadoListaReporte(nombre,nombreDos,priValor,segValor,terValor,cuaValor,id,fechaInicio,fechaFin,0);				
			}
				
				
			if (divActivo == 'territorioVis'){				
				var fechaInicio=document.getElementById("fechaInicioVr").value;	
				
				if(fechaInicio==""){
					alert("Selecciona un perido");
				}else{		
				$("#logBuscar-btn").show();
					cambiarTerritorio();
					
					var nombre=$("#territorioVis .seleccionadoVisvR").text();
					var nombreDos='suc';
					var priValor=2;
					var segValor=1;
					var terValor=0;
					var cuaValor=null;
					var id=$("#territorioVis .seleccionadoVisvR").attr("id");
					var fechaInicio=document.getElementById("fechaInicioVr").value;
					var fechaFin=document.getElementById("fechaFinVr").value;		
					
					rellenadoListaReporte(nombre,nombreDos,priValor,segValor,terValor,cuaValor,id,fechaInicio,fechaFin,0);
				}
			}
				

			if (divActivo == 'zonaVis'){
				var fechaInicio=document.getElementById("fechaInicioVr").value;	
				
				if(fechaInicio==""){
					alert("Selecciona un perido");
				}else{
					$("#logBuscar-btn").show();				
					cambiarZona();
				
					var nombre=$("#zonaVis .seleccionadoVisvR").text();
					var nombreDos='suc';
					var priValor=3;
					var segValor=2;
					var terValor=1;
					var cuaValor=null;
					var id=$("#zonaVis .seleccionadoVisvR").attr("id");
					var fechaInicio=document.getElementById("fechaInicioVr").value;
					var fechaFin=document.getElementById("fechaFinVr").value;		
					
					rellenadoListaReporte(nombre,nombreDos,priValor,segValor,terValor,cuaValor,id,fechaInicio,fechaFin,1);
				}
			}
				

			if (divActivo == 'regionVis'){
				var fechaInicio=document.getElementById("fechaInicioVr").value;	
				
				if(fechaInicio==""){
					alert("Selecciona un perido");
				}else{					
					$("#logBuscar-btn").show();				
					$("#valorRegion").val($("#regionVis .seleccionadoVisvR").attr("id"));
					
					var nombre=$("#regionVis .seleccionadoVisvR").text();
					var nombreDos='suc';
					var priValor=4;
					var segValor=3;
					var terValor=2;
					var cuaValor=1;
					var id=$("#regionVis .seleccionadoVisvR").attr("id");
					var fechaInicio=document.getElementById("fechaInicioVr").value;
					var fechaFin=document.getElementById("fechaFinVr").value;				
					
					rellenadoListaReporte(nombre,nombreDos,priValor,segValor,terValor,cuaValor,id,fechaInicio,fechaFin,2);
				}
			}
			
			
			if (divActivo == 'periodoAVis'){	
				document.getElementById("barraPorcent").innerHTML = "";
				document.getElementById("idPorc1").innerHTML = "";
				document.getElementById("idDia1-2").innerHTML = "";
				document.getElementById("idDia1").innerHTML = "";
				cambiarAno();				
			}
			if (divActivo == 'periodoVis'){	

				document.getElementById("barraPorcent").innerHTML = "";
				document.getElementById("idPorc1").innerHTML = "";
				document.getElementById("idDia1-2").innerHTML = "";
				document.getElementById("idDia1").innerHTML = "";
				//$("#idDia4-4").val("");
				
				var fechaInicial="";
				$("#valorPeriodo").val($("#periodoVis .seleccionadoVisvR").attr("id"));	
				var mesInicial=$("#valorPeriodo").val();
				var anoInicial=$("#valorPeriodoA").val();				
				var diasMes= new Date(anoInicial || new Date().getFullYear(), mesInicial, 0).getDate();
			
				var porcentaje=0;
				var por2="";
				var f=new Date();
				var dd ="";
				
				//alert('f.getDay() '+f.getDate());
				if((f.getMonth()+1)==mesInicial && f.getFullYear()==anoInicial){
					porcentaje=((f.getDate()-1)*100)/diasMes;
					dd = f.getDate();
					//porcentaje--;
				}else{
					porcentaje=(diasMes*100)/diasMes;
					dd = diasMes;
				}
				
				por2=""+porcentaje;
				var porcenI= por2.split(".");
				porcentajeL=porcenI[0];
				
				//porcentajeL=Math.round(porcentaje * 1000) / 1000;
				//alert('nuevo.. '+ porcentajeL)
				//$("#idPorcentajeL").val(porcentajeL);

				document.getElementById("idPorc1").innerHTML = porcentajeL+"%";
				document.getElementById("idPorc1").style.marginLeft = (porcentaje-3)+"%";
				document.getElementById("idDia1-2").innerHTML = dd;
				document.getElementById("idDia1-2").style.marginLeft = (porcentaje-3)+"%";
					
				document.getElementById("idDia1").innerHTML = diasMes;
				$("#barraPorcent").append("<div style='width:"+porcentaje+"%; height:35px; background:#285a43; margin-top: 0px;'/>");
							
				if(mesInicial<10){
					mesInicial="0"+mesInicial;
				}
				if(diasMes<10){
					diasMes="0"+diasMes;
				}
				
				fechaInicial="01/"+mesInicial+"/"+anoInicial;
				fechaFinal=diasMes+"/"+mesInicial+"/"+anoInicial;
				
				$("#fechaInicioVr").val(fechaInicial);
				$("#fechaFinVr").val(fechaFinal);				
			}
		});
}


function iniciaAno(){
	
	var hoy = new Date();
	var dd = hoy.getDate();
	var mm = hoy.getMonth()+1;
	var yy = hoy.getFullYear();
	
	//alert('mes actual '+ hoy);
	//alert('dia actual '+ dd);
	//alert('mes actual '+ mm);
	//alert('ano actual '+ yy);
	
	// Asigna el valor del li al input hidden
	$("#valorPeriodoA").val(yy);	
	// Vuelve a inicializar el valor del span de la lista a rellenar
	$("#periodoAVis .seleccionadoVisvR").val(yy);
	$("#periodoAVis .seleccionadoVisvR").text(yy);
	$("#periodoAVis .seleccionadoVisvR").attr("id",yy);

	
	document.getElementById("periodoVis2").innerHTML = "";
	$("#fechaInicioVr").val("");
	$("#fechaFinVr").val("");
	
	var dias= new Array ('ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE');
	

	for (var int = 1; int <= mm; int++) {
		$("#periodoVis2").append("<li style='margin:0 0 0 0;' value='" + int + "'><a href=''>"+dias[int-1]+"</a></li>");
	}
	
	// Asigna el valor del li al input hidden
	$("#valorPeriodo").val(mm);	
	// Vuelve a inicializar el valor del span de la lista a rellenar
	$("#periodoVis .seleccionadoVisvR").val(mm);
	$("#periodoVis .seleccionadoVisvR").text(dias[mm-1]);
	$("#periodoVis .seleccionadoVisvR").attr("id",mm);
	

	document.getElementById("barraPorcent").innerHTML = "";
	document.getElementById("idPorc1").innerHTML = "";
	document.getElementById("idDia1-2").innerHTML = "";
	document.getElementById("idDia1").innerHTML = "";
	
	
	var fechaInicial="";
	$("#valorPeriodo").val($("#periodoVis .seleccionadoVisvR").attr("id"));	
	var mesInicial=$("#valorPeriodo").val();
	var anoInicial=$("#valorPeriodoA").val();				
	var diasMes= new Date(anoInicial || new Date().getFullYear(), mesInicial, 0).getDate();

	var porcentaje=0;
	var por2="";
	var f=new Date();
	
	if((f.getMonth()+1)==mesInicial && f.getFullYear()==anoInicial){
		porcentaje=((f.getDate()-1)*100)/diasMes;
		//porcentaje--;
	}else{
		porcentaje=(diasMes*100)/diasMes;	
	}
		
	
	por2=""+porcentaje;
	var porcenI= por2.split(".");
	porcentajeL=porcenI[0];
	
	//porcentajeL=Math.round(porcentaje * 1000) / 1000;
	//alert('nuevo.. '+ porcentajeL)
	//$("#idPorcentajeL").val(porcentajeL);


	document.getElementById("idPorc1").innerHTML = porcentajeL+"%";
	document.getElementById("idPorc1").style.marginLeft = (porcentaje-3)+"%";
	document.getElementById("idDia1-2").innerHTML = dd;
	document.getElementById("idDia1-2").style.marginLeft = (porcentaje-3)+"%";
	
	document.getElementById("idDia1").innerHTML = diasMes;
	$("#barraPorcent").append("<div style='width:"+porcentaje+"%; height:35px; background:#285a43; margin-top: 0px;'/>");
	
	if(mesInicial<10){
		mesInicial="0"+mesInicial;
	}
	if(diasMes<10){
		diasMes="0"+diasMes;
	}
	
	fechaInicial="01/"+mesInicial+"/"+anoInicial;
	fechaFinal=diasMes+"/"+mesInicial+"/"+anoInicial;
	
	$("#fechaInicioVr").val(fechaInicial);
	$("#fechaFinVr").val(fechaFinal);	
	
	//alert('porcentajeL+ '+$("#idPorcentajeL").val())
}

function cambiarAno(){
	
	// Asigna el valor del li al input hidden
	$("#valorPeriodoA").val($("#periodoAVis .seleccionadoVisvR").attr("id"));	

	// Vuelve a inicializar el valor del span de la lista a rellenar
	$("#periodoVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$("#periodoVis .seleccionadoVisvR").text("Elije una opcion");
	$("#periodoVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");

	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#valorPeriodo").val("Selecciona una Opcion");
	
	var ano=$("#periodoAVis .seleccionadoVisvR").attr("id")
	
	Ano(ano);
}

function Ano(ano){

	document.getElementById("periodoVis2").innerHTML = "";
	$("#fechaInicioVr").val("");
	$("#fechaFinVr").val("");
	
	var dias= new Array ('ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE');
	var f=new Date();

	if(f.getFullYear()==ano){
		for (var int = 1; int <= (f.getMonth()+1); int++) {
			$("#periodoVis2").append("<li style='margin:0 0 0 0;' value='" + int + "'><a href=''>"+dias[int-1]+"</a></li>");
		}
	}else{
		for (var int = 1; int <= 12; int++) {
			$("#periodoVis2").append("<li style='margin:0 0 0 0;' value='" + int + "'><a href=''>"+dias[int-1]+"</a></li>");
		}
	}
}

function cambiarChecklist(){
	// Asigna el valor del li al input hidden
	$("#valorChecklist").val($("#checklistVis .seleccionadoVisvR").attr("id"));
}

function checklist() {
	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxListaCheck.json?idUsuario='+idUsuario,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			document.getElementById("checklistVis2").innerHTML = "";
			var i = 0;
			if (json[i] == null) {
				alert('No tienes Checklist');
			}else{
				var margenZona=0;
				while (json[i] != null) {
					$("#checklistVis2").append(
							"<li style='margin:"+margenZona+"px 0 0 0;' value='" + json[i].idChecklist + "'><a href=''>"
									+ json[i].nombreCheck + "</a></li>");
					i++;
					margenZona=0;
				}
				// Asigna el valor del li al input hidden
				$("#valorChecklist").val(json[0].idChecklist);	
				// Vuelve a inicializar el valor del span de la lista a rellenar
				$("#checklistVis .seleccionadoVisvR").val(json[0].idChecklist);
				$("#checklistVis .seleccionadoVisvR").text(json[0].nombreCheck);
				$("#checklistVis .seleccionadoVisvR").attr("id",json[0].idChecklist);
			}						
		},
		error : function() {
			alert('Favor de actualizar la pagina');
		}
	});
}


function cambiarInicioCanal(IdUsuario) {

	// Asigna el valor del li al input hidden
	//$("#valorCanal").val($("#canalVis .seleccionadoVisvR").attr("id"));

	// Vulve a inicializar el valor del span de la lista a rellenar
	$("#paisVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#paisVis .seleccionadoVisvR').text("Elije una opcion");
	$("#paisVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	$("#territorioVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#territorioVis .seleccionadoVisvR').text("Elije una opcion");
	$("#territorioVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	$("#zonaVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#zonaVis .seleccionadoVisvR').text("Elije una opcion");
	$("#zonaVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	$("#regionVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#regionVis .seleccionadoVisvR').text("Elije una opcion");
	$("#regionVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	
	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#valorPais").val("Selecciona una Opcion");
	$("#valorTerritorio").val("Selecciona una Opcion");
	$("#valorZona").val("Selecciona una Opcion");
	$("#valorRegion").val("Selecciona una Opcion");

	inicioCanal(IdUsuario);
}

function inicioCanal(IdUsuario) {
	//alert('lista ne ' + listaNegocioArray);
	/*ESTE CODIGO COMENTADO ES LO QUE LLENA EL CANAL Y EL PAIS DE MANERA ESTATICA
	$("#canalVis2").append(
			"<li style='margin:0 0 0 0;' value='" + negocioId + "'><a href=''>"
					+ negocioName + "</a></li>");
	
	$("#valorCanal").val(negocioId);
	$("#canalVis .seleccionadoVisvR").val(negocioId);
	$('#canalVis .seleccionadoVisvR').text(negocioName);
	$("#canalVis .seleccionadoVisvR").attr("id",negocioId);
	
	$("#paisVis2").append(
			"<li style='margin:0 0 0 0;' value='" + paisIdAux + "'><a href=''>"
					+ paisNameAux + "</a></li>");
	
	*/
	$.ajax({
		type : "GET",
		url : urlServer + '../getInicioCanal.json?IdUsuario='+IdUsuario,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			document.getElementById("canalVis2").innerHTML = "";
			document.getElementById("paisVis2").innerHTML = "";
			document.getElementById("territorioVis2").innerHTML = "";
			document.getElementById("zonaVis2").innerHTML = "";
			document.getElementById("regionVis2").innerHTML = "";
			
			var i = 0;
			var margenZona=0;
			while (json[i] != null) {
				$("#canalVis2").append(
						"<li style='margin:"+margenZona+"px 0 0 0;' value='" + json[i].idNegocio + "'><a href=''>"
								+ json[i].descripcion + "</a></li>");
				i++;
				margenZona=0;
			}
		},
		error : function() {
			alert('Favor de actualizar la pagina');
		}
	});
}

function cambiarCanal() {

	// Asigna el valor del li al input hidden
	$("#valorCanal").val($("#canalVis .seleccionadoVisvR").attr("id"));

	// Vulve a inicializar el valor del span de la lista a rellenar
	$("#paisVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#paisVis .seleccionadoVisvR').text("Elije una opcion");
	$("#paisVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	$("#territorioVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#territorioVis .seleccionadoVisvR').text("Elije una opcion");
	$("#territorioVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	$("#zonaVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#zonaVis .seleccionadoVisvR').text("Elije una opcion");
	$("#zonaVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	$("#regionVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#regionVis .seleccionadoVisvR').text("Elije una opcion");
	$("#regionVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	
	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#valorPais").val("Selecciona una Opcion");
	$("#valorTerritorio").val("Selecciona una Opcion");
	$("#valorZona").val("Selecciona una Opcion");
	$("#valorRegion").val("Selecciona una Opcion");

	var varCanal = document.getElementById("valorCanal").value;
	if (varCanal != "Selecciona una Opcion")
		canal(varCanal);
}

function cambiarCanalValor(negocioIdL) {

	// Asigna el valor del li al input hidden
	$("#valorCanal").val(negocioIdL);

	// Vulve a inicializar el valor del span de la lista a rellenar
	$("#paisVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#paisVis .seleccionadoVisvR').text("Elije una opcion");
	$("#paisVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	$("#territorioVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#territorioVis .seleccionadoVisvR').text("Elije una opcion");
	$("#territorioVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	$("#zonaVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#zonaVis .seleccionadoVisvR').text("Elije una opcion");
	$("#zonaVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	$("#regionVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#regionVis .seleccionadoVisvR').text("Elije una opcion");
	$("#regionVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");

	
	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#paisZona").val("Selecciona una Opcion");
	$("#territorioZona").val("Selecciona una Opcion");
	$("#valorZona").val("Selecciona una Opcion");
	$("#valorRegion").val("Selecciona una Opcion");

	var varCanal = document.getElementById("valorCanal").value;
	if (varCanal != "Selecciona una Opcion")
		canal(varCanal);

}

function canal(varCanal) {

	$.ajax({
		type : "GET",
		url : urlServer + '../consultaCatalogosService/getPaisesCombo.json?idNegocio='+varCanal,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			document.getElementById("paisVis2").innerHTML = "";
			document.getElementById("territorioVis2").innerHTML = "";
			document.getElementById("zonaVis2").innerHTML = "";
			document.getElementById("regionVis2").innerHTML = "";
			var jsonC = JSON.stringify(json);
			var i = 0;
			var margenZona=0;
			while (json[i] != null) {
				$("#paisVis2").append(
						"<li style='margin:"+margenZona+"px 0 0 0;' value='" + json[i].idPais + "'><a href=''>"
								+ json[i].nombre + "</a></li>");
				i++;
				margenZona=0;
			}
		},
		error : function() {
			alert('Favor de actualizar la pagina');
		}
	});
	async:false;
}

function cambiarPais() {
	
	// Asigna el valor del li al input hidden
	$("#valorPais").val($("#paisVis .seleccionadoVisvR").attr("id"));

	// Vulve a inicializar el valor del span de la lista a rellenar
	$("#territorioVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$("#territorioVis .seleccionadoVisvR").text("Elije una opcion");
	$("#territorioVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	$("#zonaVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$("#zonaVis .seleccionadoVisvR").text("Elije una opcion");
	$("#zonaVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	$("#regionVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$("#regionVis .seleccionadoVisvR").text("Elije una opcion");
	$("#regionVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");

	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#valorTerritorio").val("Selecciona una Opcion");
	$("#valorZona").val("Selecciona una Opcion");
	$("#valorRegion").val("Selecciona una Opcion");

	var varCanal = document.getElementById("valorCanal").value;
	var varPais = document.getElementById("valorPais").value;
	if (varPais != "Selecciona una Opcion")
		pais(varCanal,varPais);
}

function cambiarPaisValor(paisIdL) {
	
	// Asigna el valor del li al input hidden
	$("#valorPais").val(paisIdL);

	// Vulve a inicializar el valor del span de la lista a rellenar
	$("#territorioVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$("#territorioVis .seleccionadoVisvR").text("Elije una opcion");
	$("#territorioVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	$("#zonaVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$("#zonaVis .seleccionadoVisvR").text("Elije una opcion");
	$("#zonaVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	$("#regionVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$("#regionVis .seleccionadoVisvR").text("Elije una opcion");
	$("#regionVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");

	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#valorTerritorio").val("Selecciona una Opcion");
	$("#valorZona").val("Selecciona una Opcion");
	$("#valorRegion").val("Selecciona una Opcion");

	var varCanal = document.getElementById("valorCanal").value;
	var varPais = document.getElementById("valorPais").value;
	if (varPais != "Selecciona una Opcion")
		pais(varCanal,varPais);
}

function pais(varCanal,varPais) {
	
	$.ajax({
		type : "GET", 
		url : urlServer + '../consultaCatalogosService/getTerritoriosCombo.json?idNegocio='+varCanal+'&idPais='+varPais,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			document.getElementById("territorioVis2").innerHTML = "";
			document.getElementById("zonaVis2").innerHTML = "";
			document.getElementById("regionVis2").innerHTML = "";
			var jsonC = JSON.stringify(json);
			var i = 0;
			var margenZona=0;
			while (json[i] != null) {
				$("#territorioVis2").append(
						"<li style='margin:"+margenZona+"px 0 0 0;' value='" + json[i].idCeco + "'><a href=''>"
								+ json[i].nombreCeco + "</a></li>");
				i++;
				margenZona=0;
			}
		},
		error : function() {
			alert('Favor de actualizar la pagina');
		}
	});
	async:false;
}

function cambiarTerritorio() {
	// Asigna el valor del li al input hidden
	$("#valorTerritorio").val($("#territorioVis .seleccionadoVisvR").attr("id")); 

	// Vulve a inicializar el valor del span de la lista a rellenar
	$("#zonaVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#zonaVis .seleccionadoVisvR').text("Elije una opcion");
	$('#zonaVis .seleccionadoVisvR').attr("id","Selecciona una Opcion");
	$("#regionVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#regionVis .seleccionadoVisvR').text("Elije una opcion");
	$("#regionVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");

	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#valorZona").val("Selecciona una Opcion");
	$("#valorRegion").val("Selecciona una Opcion");

	var varTerri = document.getElementById("valorTerritorio").value;
	if (varTerri != "Selecciona una Opcion")
		territorio(varTerri);
}

function cambiarTerritorioValor(territorioIdL) {
	// Asigna el valor del li al input hidden
	$("#valorTerritorio").val(territorioIdL); 

	// Vulve a inicializar el valor del span de la lista a rellenar
	$("#zonaVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#zonaVis .seleccionadoVisvR').text("Elije una opcion");
	$('#zonaVis .seleccionadoVisvR').attr("id","Selecciona una Opcion");
	$("#regionVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#regionVis .seleccionadoVisvR').text("Elije una opcion");
	$("#regionVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");

	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#valorZona").val("Selecciona una Opcion");
	$("#valorRegion").val("Selecciona una Opcion");

	var varTerri = document.getElementById("valorTerritorio").value;
	if (varTerri != "Selecciona una Opcion")
		territorio(varTerri);
}

function territorio(varTerri) {
	$.ajax({
		type : "GET",
		url : urlServer + '../consultaCatalogosService/getCecosCombo.json?idCeco='+varTerri+'&tipoBusqueda=1',
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			document.getElementById("zonaVis2").innerHTML = "";
			document.getElementById("regionVis2").innerHTML = "";
			var jsonC = JSON.stringify(json);
			var i = 0;
			var margenZona=0;
			while (json[i] != null) {
				$("#zonaVis2").append(
						"<li style='margin:"+margenZona+"px 0 0 0;' value='" + json[i].idCeco + "'><a href=''>"
								+ json[i].nombreCeco + "</a></li>");
				i++;
				margenZona=0;
			}
		},
		error : function() {
			alert('Favor de actualizar la pagina');
		}
	});
	async:false;
}

function cambiarZona() {
	// Asigna el valor del li al input hidden
	$("#valorZona").val($("#zonaVis .seleccionadoVisvR").attr("id"));
	// Vulve a inicializar el valor del span de la lista a rellenar
	$("#regionVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$("#regionVis .seleccionadoVisvR").text("Elije una opcion");
	$("#regionVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#valorRegion").val("Selecciona una Opcion");
	
	var varZona = document.getElementById("valorZona").value;
	if (varZona != "Selecciona una Opcion")
		zona(varZona);
}

function cambiarZonaValor(zonaIdL) {
	// Asigna el valor del li al input hidden
	$("#valorZona").val(zonaIdL);
	// Vulve a inicializar el valor del span de la lista a rellenar
	$("#regionVis .seleccionadoVisvR").val("Selecciona una Opcion");
	$('#regionVis .seleccionadoVisvR').text("Elije una opcion");
	$("#regionVis .seleccionadoVisvR").attr("id","Selecciona una Opcion");
	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#valorRegion").val("Selecciona una Opcion");
	
	var varZona = document.getElementById("valorZona").value;
	if (varZona != "Selecciona una Opcion")
		zona(zonaIdL);
}

function zona(varZona) {
	$.ajax({
		type : "GET",
		url : urlServer + '../consultaCatalogosService/getCecosCombo.json?idCeco='+varZona+'&tipoBusqueda=1',
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			document.getElementById("regionVis2").innerHTML = "";
			var jsonC = JSON.stringify(json);
			var i = 0;
			var margenZona=0;
			while (json[i] != null) {
				$("#regionVis2").append(
						"<li style='margin:"+margenZona+"px 0 0 0;' value='" + json[i].idCeco + "'><a href=''>"
								+ json[i].nombreCeco + "</a></li>");
				i++;
				margenZona=0;
			}
		},
		error : function() {
			alert('Favor de actualizar la pagina');
		}
	});
	async:false;
}

function tablaVisitaHijo(idCeco){
	
	limpiaCalendar();
	//idHijoVisitaTituloTabla();
	var idCheck=$("#checklistVis .seleccionadoVisvR").attr("id");
	var fechaInicio=document.getElementById("fechaInicioVr").value;
	var fechaFin=document.getElementById("fechaFinVr").value;	
	var idCanal=$("#canalVis .seleccionadoVisvR").attr("id");	
	var idPais=$("#paisVis .seleccionadoVisvR").attr("id");	
	var tamaTiendas=0;
	document.getElementById("idHijoVisitaCentroTabla").innerHTML = "";
	
	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxReporteDetSupReg.json?varIdCeco='+idCeco+'&idCanal='+idCanal+'&idPais='+idPais+'&fechaInicio='+fechaInicio+'&fechaFin='+fechaFin+'&idCheck='+idCheck,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {			
			var i = 0;
			if (json[i] == null) {
				alert('Sin Datos');
			}				
			var length = Object.keys(json).length; 
			tamaTiendas= Object.keys(json).length; 
			while (json[i] != null) {				
			//9e9f9d
				if((i%2)==1){
					$("#idHijoVisitaCentroTabla").append("" +
							"<tr id='00"+i+"' style='background:#f0eded ; text-align: center; width:100%; font-family:Calibri; font-size: 13px'  onclick='vistaCecoSucursalCalendar(&apos;00"+i+ "&apos;,"+length+",&apos;"+idCeco+"&apos;,"+json[i][0].idCeco+",&apos;"+ json[i][0].nombreCeco+ "&apos;,"+json[i][0].plan+","+json[i][0].real+","+json[i][0].avance+","+json[i][0].distintos+",&apos;"+json[i][0].imagen+"&apos;);'>" +						
								(json[i][0].imagen==1 ? "<td id=fila"+json[i][0].idCeco+" class='columnaSucuB'>"+json[i][0].idCeco+" - "+json[i][0].nombreCeco+"<div class='icono-problema2'><img class='imagen-iconoProblemasVistaDias' width='30px' height='25px' src='../images/icono_problemas.png'/></div></td>":"<td id=fila"+json[i][0].idCeco+" class='columnaSucuB'>"+json[i][0].idCeco+" - "+json[i][0].nombreCeco+"</td>")+							 
								"<td id=filaN"+json[i][0].idCeco+" class='columnaSucuBTotal'>"+json[i][0].conteo+"</td>"+		
								"<td id=filaNOculto"+json[i][0].idCeco+" class='columnaSucuBTotalOculto'>"+json[i][0].conteo+"</td>"+		
							"</tr>");	
				}
				if((i%2)==0){
					$("#idHijoVisitaCentroTabla").append("" +
							"<tr id='00"+i+"' style='background:white; text-align: center; width:100%; font-family:Calibri; font-size: 13px'  onclick='vistaCecoSucursalCalendar(&apos;00"+i+ "&apos;,"+length+",&apos;"+idCeco+"&apos;,"+json[i][0].idCeco+",&apos;"+ json[i][0].nombreCeco+ "&apos;,"+json[i][0].plan+","+json[i][0].real+","+json[i][0].avance+","+json[i][0].distintos+",&apos;"+json[i][0].imagen+"&apos;);'>" +						
								(json[i][0].imagen==1 ? "<td id=fila"+json[i][0].idCeco+" class='columnaSucuB'>"+json[i][0].idCeco+" - "+json[i][0].nombreCeco+"<div class='icono-problema2'><img class='imagen-iconoProblemasVistaDias' width='30px' height='25px' src='../images/icono_problemas.png'/></div></td>":"<td id=fila"+json[i][0].idCeco+" class='columnaSucuB'>"+json[i][0].idCeco+" - "+json[i][0].nombreCeco+"</td>")+							 
								"<td id=filaN"+json[i][0].idCeco+" class='columnaSucuBTotal'>"+json[i][0].conteo+"</td>"+			
								"<td id=filaNOculto"+json[i][0].idCeco+" class='columnaSucuBTotalOculto'>"+json[i][0].conteo+"</td>"+	
							"</tr>");	
				}
		
				i++;
			}
			
			$.ajax({
				type : "GET",
				url : urlServer + '../ajaxReporteDetReg.json?fechaInicio='+fechaInicio+'&fechaFin='+fechaFin+'&idCecoPadre='+idCeco+'&idCheck='+idCheck,
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				delimiter : ",",
				success : function(json) {			
					var i = 0;
					if (json[i] == null) {
						alert('Sin Datos');
					}else{						
						var idTotal = 0;				
						var varIncre=1;	
						bandeNombre=true;
						for (var j = 0; j < 37; j++) {
							var cont=0;									
							if(json[i][j].observaciones!=null || json[i][j].observaciones!=""){								
								var arrayCecos=json[i][j].observaciones.split(",");
								var aux="";								
								for (var r = 0; r < arrayCecos.length; r++) {								
									if(r>0){
										if(arrayCecos[r]!=aux){											
											aux=arrayCecos[r];
											cont++;
										}
									}else{
										aux=arrayCecos[r];
										cont++;
									}
								}
							}
							
							document.getElementById('idCal'+(j+1)).innerHTML = "";
							if(json[i][j].nombreCeco!="NULL"){
								$("#idCal"+(j+1)).append(""+ (json[i][j].numero>0 ? "<div onclick='pintaLista(&apos;"+json[i][j].observaciones+"&apos;,"+length+");' class='fuenteNegraMargen'><a class='fuenteNegra'><div style='margin-left:38%; width:30px; height:30px;' class='circle-text-verde'><div>"+cont+"</div></div></a></div>":"-")+"<table><tr><td class='tableTres'>"+varIncre+"</td></tr></table>");
								varIncre++;					
							}else{
								$("#idCal"+(j+1)).append("");						
							}					
							idTotal=idTotal+json[i][j].numero;							
						}				
						document.getElementById('idTotal').innerHTML = idTotal;								
					}
				},
				error : function() {
					alert('Favor de actualizar la pagina');
				}
			});			
		},
		error : function() {
			alert('Favor de actualizar la pagina');
		}
	});
}
function pintaLista(idCecos,tamPinta){

	for (var h = 0; h < tamPinta; h++) {		
		if((h%2)==0){
			document.getElementById("00"+h).style.backgroundColor = "white";
			document.getElementById(document.getElementById("00"+h).childNodes[0].id).style.color = "black";
			document.getElementById(document.getElementById("00"+h).childNodes[1].id).style.color = "black";
			document.getElementById(document.getElementById("00"+h).childNodes[2].id).style.color = "black";
		}				
		if((h%2)==1){
			document.getElementById("00"+h).style.backgroundColor = "#f0eded ";
			document.getElementById(document.getElementById("00"+h).childNodes[0].id).style.color = "black";
			document.getElementById(document.getElementById("00"+h).childNodes[1].id).style.color = "black";
			document.getElementById(document.getElementById("00"+h).childNodes[2].id).style.color = "black";
		}					
	}
	
	
	var arrayCecos=idCecos.split(",");
	var temp=0;
	var idDivPadre="";
	for(i=0; i<arrayCecos.length; i++){
		document.getElementById("filaNOculto"+(arrayCecos[i])).innerHTML=document.getElementById("filaN"+(arrayCecos[i]));
		//alert($("#fila"+(arrayCecos[i])).parent()[0].id);
		if(i>0){
			if(arrayCecos[i]==arrayCecos[i-1]){
				temp++;
				idDivPadre=$("#fila"+(arrayCecos[i])).parent()[0].id;
				document.getElementById(idDivPadre).style.background="#285a43";
				document.getElementById("fila"+(arrayCecos[i])).style.color="white";
				$("#filaN"+(arrayCecos[i])).hide();
				$("#filaNOculto"+(arrayCecos[i])).show();
				document.getElementById("filaNOculto"+(arrayCecos[i])).style.color="white";
				document.getElementById("filaNOculto"+(arrayCecos[i])).innerHTML=temp;
				
			}else{
				temp=1;
				idDivPadre=$("#fila"+(arrayCecos[i])).parent()[0].id;
				document.getElementById(idDivPadre).style.background="#285a43";
				document.getElementById("fila"+(arrayCecos[i])).style.color="white";
				$("#filaN"+(arrayCecos[i])).hide();
				$("#filaNOculto"+(arrayCecos[i])).show();
				document.getElementById("filaNOculto"+(arrayCecos[i])).style.color="white";
				document.getElementById("filaNOculto"+(arrayCecos[i])).innerHTML=temp;
				
			}
		}else{
			temp=1;
			idDivPadre=$("#fila"+(arrayCecos[i])).parent()[0].id;
			document.getElementById(idDivPadre).style.background="#285a43";
			document.getElementById("fila"+(arrayCecos[i])).style.color="white";
			$("#filaN"+(arrayCecos[i])).hide();
			$("#filaNOculto"+(arrayCecos[i])).show();
			document.getElementById("filaNOculto"+(arrayCecos[i])).style.color="white";
			document.getElementById("filaNOculto"+(arrayCecos[i])).innerHTML=temp;
		}		
	}
	
}

function tablaVisitaHijoSucursal(idCeco,nombreCecoOp){
	//alert('entre a la tablaVisitaHijoSucursal');
	//idHijoVisitaTituloTabla();
	//alert('idCeco ' + idCeco);
	var idCheck=$("#checklistVis .seleccionadoVisvR").attr("id");
	var fechaInicio=document.getElementById("fechaInicioVr").value;
	var fechaFin=document.getElementById("fechaFinVr").value;	
	var idCecoPadre=$("#regionVis .seleccionadoVisvR").attr("id");
	var idCanal=$("#canalVis .seleccionadoVisvR").attr("id");	
	var idPais=$("#paisVis .seleccionadoVisvR").attr("id");	
	
	document.getElementById("idHijoVisitaCentroTabla").innerHTML = "";
	
	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxReporteDetSupRegSucursal.json?varIdCeco='+idCeco+'&idCanal='+idCanal+'&idPais='+idPais+
			'&fechaInicio='+fechaInicio+'&fechaFin='+fechaFin+'&idCecoPadre='+idCecoPadre+'&idCheck='+idCheck,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {			
			var i = 0;
			if (json[i] == null) {
				alert('Sin Datos');
			}else{						
				var idTotal = 0;				
				var varIncre=1;	
				bandeNombre=true;
				for (var j = 0; j < 37; j++) {
					document.getElementById('idCal'+(j+1)).innerHTML = "";
					if(json[i][j].nombreCeco!="NULL"){
						$("#idCal"+(j+1)).append(""+ (json[i][j].numero>0 ? "<div class='fuenteVerdeConLinkMargen' onclick='nuevaVistaResumen("+json[i][j].idBitacora+");'><a class='fuenteVerdeConLink'>"+json[i][j].numero+"</a></div>":"-")+"<table><tr><td class='tableTres'>"+varIncre+"</td></tr></table>");
						varIncre++;
						if(bandeNombre){
							$("#idHijoVisitaCentroTabla").append("" +
									"<tr style='background:white; text-align: center; font-family:Calibri; font-size: 13px'>" +						
										(json[0][10].imagen==1 ? "<td class='columnaSucuBFinal'>"+idCeco+" - "+nombreCecoOp+"<div class='icono-problema2'><img class='imagen-iconoProblemasVistaDias' width='30px' height='25px' src='../images/icono_problemas.png'/></div></td>":"<td class='columnaSucuBFinal'>"+idCeco+" - "+nombreCecoOp+"</td>")+							 
																	
									"</tr>");
							bandeNombre=false;
						}
					}else{
						$("#idCal"+(j+1)).append("");						
					}					
					idTotal=idTotal+json[i][j].numero;
					
					
				}				
				document.getElementById('idTotal').innerHTML = idTotal;						
			
			
				
			}
		},
		error : function() {
			alert('Favor de actualizar la pagina');
		}
	});
}


function tablaVisitaHijoSucursalCalendar(idCecoPadre,idCeco){
	
	var fechaInicio=document.getElementById("fechaInicioVr").value;
	var fechaFin=document.getElementById("fechaFinVr").value;	
	//var idCecoPadre=$("#regionVis .seleccionadoVisvR").attr("id");
	
	var idCanal=$("#canalVis .seleccionadoVisvR").attr("id");	
	var idPais=$("#paisVis .seleccionadoVisvR").attr("id");
	var idCheck=$("#checklistVis .seleccionadoVisvR").attr("id");
	
	
	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxReporteDetSupRegSucursal.json?varIdCeco='+idCeco+'&idCanal='+idCanal+'&idPais='+idPais+
			'&fechaInicio='+fechaInicio+'&fechaFin='+fechaFin+'&idCecoPadre='+idCecoPadre+'&idCheck='+idCheck,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {			
			var i = 0;
			if (json[i] == null) {
				alert('Sin Datos');
			}else{			
			
				var idTotal = 0;				
				var varIncre=1;
				//alert('json[i][j].idBitacora '+json[i][26].idBitacora);
				for (var j = 0; j < 37; j++) {
					document.getElementById('idCal'+(j+1)).innerHTML = "";
					//alert("json[i][j].nombreCeco " +json[i][j].nombreCeco); 
					
					if(json[i][j].nombreCeco!="NULL"){
						$("#idCal"+(j+1)).append(""+ (json[i][j].numero>0 ? "<div class='fuenteVerdeConLinkMargen' onclick='nuevaVistaResumen("+json[i][j].idBitacora+");'><a class='fuenteVerdeConLink'>"+json[i][j].numero+"</a></div>":"-")+"<table><tr><td class='tableTres'>"+varIncre+"</td></tr></table>");
						varIncre++;
					}else{
						$("#idCal"+(j+1)).append("");
						
					}
					
					idTotal=idTotal+json[i][j].numero;
				}				
				document.getElementById('idTotal').innerHTML = idTotal;
						
			}	

		},
		error : function() {
			alert('Favor de actualizar la pagina');
		}
		
	});
}


function tablaOperandoHijo(idPregunta,desPregunta){
	
	//alert(idPregunta);
	var bande=1;	
	var idCeco=$("#regionVis .seleccionadoVisvR").attr("id");
	var idPais=$("#paisVis .seleccionadoVisvR").attr("id");
	var idCanal=$("#canalVis .seleccionadoVisvR").attr("id");
	var nivelTerr=4;
	var nivelZona=3;
	var nivelRegion=2;	
	
	if(idCeco=="Selecciona una Opcion"){
		idCeco=$("#zonaVis .seleccionadoVisvR").attr("id");
		bande=1;
		nivelTerr=3;
		nivelZona=2;
		nivelRegion=1;
		if(idCeco=="Selecciona una Opcion"){
			idCeco=$("#territorioVis .seleccionadoVisvR").attr("id");
			bande=1;
			nivelTerr=2;
			nivelZona=1;
			nivelRegion=0;
			if(idCeco=="Selecciona una Opcion"){
				idCeco=$("#paisVis .seleccionadoVisvR").attr("id");
				bande=1;
				nivelTerr=1;
				nivelZona=0;
				nivelRegion=0;
				if(idCeco=="Selecciona una Opcion"){
					alert('Selecciona un filtrado')
					bande=0;
					nivelTerr=0;
					nivelZona=0;
					nivelRegion=0;
				}else{
					idCeco=230001;
				}
			}
		}
	}
		
	//alert(bande);
	if(bande==1){
		//alert('entro al if' + nivelTerr + ' ' + nivelZona +' ' + nivelRegion);
		idOperandoNombre();
		var fechaInicio=document.getElementById("fechaInicioVr").value;
		var fechaFin=document.getElementById("fechaFinVr").value;
		var idCheck=$("#checklistVis .seleccionadoVisvR").attr("id");

		document.getElementById('idReactivoPregunta').innerHTML = "";  
		document.getElementById("tablaOperandoHijoRelleno").innerHTML = "";
		$.ajax({
			type : "GET",
			url : urlServer + '../ajaxTablaOperandoHijo.json?varIdPregunta='+idPregunta+'&fechaInicio='+fechaInicio+'&fechaFin='+fechaFin+
				'&idCeco='+idCeco+'&idCanal='+idCanal+'&idPais='+idPais+'&nivelRegion='+nivelRegion+'&idCheck='+idCheck,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {			
				var i = 0;
				if (json[i] == null) {
					alert('Sin Datos');
				}				
				while (json[i] != null) {
					document.getElementById('idReactivoPregunta').innerHTML =desPregunta;
					if((i%2)==0){
						$("#tablaOperandoHijoRelleno").append("<tr style='background:#f0eded; text-align: center; font-size: 13px; height: 90px;'>" + 
							"<td style='width:25%; border: 1px solid black;'>" +								 
								(nivelRegion==2 ? "<a class='idOperandoNombreNoLink'>"+json[i].nombreCeco+"</a>":"<a class='idVistaNombre' onclick='nuevo("+json[i].idCeco+","+idPregunta+",&apos;"+desPregunta+"&apos;,&apos;"+json[i].nombreCeco+"&apos;);'>"+json[i].nombreCeco+"</a>")+
							"</td>" +
							"<td style='width:25%; border: 1px solid black; color:black; font-size:20px; font-family:Calibri;'>"+json[i].si+"</td>" +
							"<td style='width:25%; border: 1px solid black; color:black; font-size:20px; font-family:Calibri;'>"+json[i].no+"</td>" +
							"<td style='width:25%; border: 1px solid black; color:white; font-size:20px; font-family:Calibri; border-left: 7px solid black; border-right:7px solid black;'>" +							
								(json[i].avance>=porcentajeL ? "<div style='margin-left:40%; width:70%; max-width:60px;' class='circle-text-verde'><div>"+json[i].avance+"%</div></div>":"")+
								//(json[i].avance<90 && json[i].avance>79 ? "<div style='margin-left:40%; width:70%; max-width:60px;' class='circle-text-amarillo'><div>"+json[i].avance+"%</div></div>":"")+
								(json[i].avance<porcentajeL ? "<div style='margin-left:40%; width:70%; max-width:60px;' class='circle-text-rojo'><div>"+json[i].avance+"%</div></div>":"")+								
							"</td>");
					}
					if((i%2)==1){
						$("#tablaOperandoHijoRelleno").append("<tr style='background:white; text-align: center; font-size: 13px; height: 90px;'>" +
							"<td style='width:25%; border: 1px solid black;'>" +
								(nivelRegion==2 ? "<a class='idOperandoNombreNoLink'>"+json[i].nombreCeco+"</a>":"<a class='idVistaNombre' onclick='nuevo("+json[i].idCeco+","+idPregunta+",&apos;"+desPregunta+"&apos;,&apos;"+json[i].nombreCeco+"&apos;);'>"+json[i].nombreCeco+"</a>")+
							"</td>" +
							"<td style='width:25%; border: 1px solid black; font-size:20px; font-family:Calibri;'>"+json[i].si+"</td>" +
							"<td style='width:25%; border: 1px solid black; font-size:20px; font-family:Calibri;'>"+json[i].no+"</td>" +
							"<td style='width:25%; border: 1px solid black; font-size:20px; font-family:Calibri; border-left:7px solid black; border-right:7px solid black;'>" +
								(json[i].avance>=porcentajeL ? "<div style='margin-left:40%; width:70%; max-width:60px;' class='circle-text-verde'><div>"+json[i].avance+"%</div></div>":"")+
								//(json[i].avance<90 && json[i].avance>79 ? "<div style='margin-left:40%; width:70%; max-width:60px;' class='circle-text-amarillo'><div>"+json[i].avance+"%</div></div>":"")+
								(json[i].avance<porcentajeL ? "<div style='margin-left:40%; width:70%; max-width:60px;' class='circle-text-rojo'><div>"+json[i].avance+"%</div></div>":"")+					
							"</td>");
					}	

					i++;
				}	
			},
			error : function() {
				alert('Favor de actualizar la pagina');
			}
		});
	}
	
}

function nuevo(idCecoNuevo,idPregunta,desPregunta,nombreCeco){
	//alert('Datos nuevo() ' + idCecoNuevo+ " - " +idPregunta+" - "+desPregunta+" - "+nombreCeco)
	
	vistaCecoNivel(idCecoNuevo,nombreCeco);
	
	
	//alert(idPregunta);	
	var bande=1;	
	var idCeco=$("#regionVis .seleccionadoVisvR").attr("id");
	var idPais=$("#paisVis .seleccionadoVisvR").attr("id");
	var idCanal=$("#canalVis .seleccionadoVisvR").attr("id");
	var nivelTerr=4;
	var nivelZona=3;
	var nivelRegion=2;
	
	
	if(idCeco=="Selecciona una Opcion"){
		idCeco=$("#zonaVis .seleccionadoVisvR").attr("id");
		bande=1;
		nivelTerr=3;
		nivelZona=2;
		nivelRegion=1;
		if(idCeco=="Selecciona una Opcion"){
			idCeco=$("#territorioVis .seleccionadoVisvR").attr("id");
			bande=1;
			nivelTerr=2;
			nivelZona=1;
			nivelRegion=0;
			if(idCeco=="Selecciona una Opcion"){
				idCeco=$("#paisVis .seleccionadoVisvR").attr("id");
				bande=1;
				nivelTerr=1;
				nivelZona=0;
				nivelRegion=0;
				if(idCeco=="Selecciona una Opcion"){
					alert('Selecciona un filtrado')
					bande=0;
					nivelTerr=0;
					nivelZona=0;
					nivelRegion=0;
				}else{
					idCeco=230001;
				}
			}
			else{
				idCeco=idCecoNuevo;
			}
		}else{
			idCeco=idCecoNuevo;
		}
	}
		
	//alert(bande);
	if(bande==1){
		//alert('entro al if ' + nivelTerr + ' ' + nivelZona +' ' + nivelRegion+' ' + idCeco);
		idOperandoNombre();
		var fechaInicio=document.getElementById("fechaInicioVr").value;
		var fechaFin=document.getElementById("fechaFinVr").value;
		var idCheck=$("#checklistVis .seleccionadoVisvR").attr("id");

		document.getElementById('idReactivoPregunta').innerHTML = "";  
		document.getElementById("tablaOperandoHijoRelleno").innerHTML = "";
		$.ajax({
			type : "GET",
			url : urlServer + '../ajaxTablaOperandoHijo.json?varIdPregunta='+idPregunta+'&fechaInicio='+fechaInicio+'&fechaFin='+fechaFin+
				'&idCeco='+idCeco+'&idCanal='+idCanal+'&idPais='+idPais+'&nivelRegion='+nivelRegion+'&idCheck='+idCheck,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {			
				var i = 0;
				if (json[i] == null) {
					alert('Sin Datos');
				}				
				while (json[i] != null) {
					document.getElementById('idReactivoPregunta').innerHTML =desPregunta;
					if((i%2)==0){
						$("#tablaOperandoHijoRelleno").append("<tr style='background:#f0eded; text-align: center; font-size: 13px; height: 90px;'>" +
							"<td style='width:25%; border: 1px solid black;'>" +
								(nivelRegion==2 ? "<a class='idOperandoNombreNoLink'>"+json[i].nombreCeco+"</a>":"<a class='idVistaNombre' onclick='nuevo("+json[i].idCeco+","+idPregunta+",&apos;"+desPregunta+"&apos;,&apos;"+json[i].nombreCeco+"&apos;);'>"+json[i].nombreCeco+"</a>")+
							"</td>" +
							"<td style='width:25%; border: 1px solid black; color:black; font-size:20px; font-family:Calibri;'>"+json[i].si+"</td>" +
							"<td style='width:25%; border: 1px solid black; color:black; font-size:20px; font-family:Calibri;'>"+json[i].no+"</td>" +
							"<td style='width:25%; border: 1px solid black; color:white; font-size:20px; font-family:Calibri; border-left:7px solid black; border-right:7px solid black;'>" +							
								(json[i].avance>=porcentajeL ? "<div style='margin-left:40%; width:70%; max-width:60px;' class='circle-text-verde'><div>"+json[i].avance+"%</div></div>":"")+
								//(json[i].avance<90 && json[i].avance>79 ? "<div style='margin-left:40%; width:70%; max-width:60px;' class='circle-text-amarillo'><div>"+json[i].avance+"%</div></div>":"")+
								(json[i].avance<porcentajeL ? "<div style='margin-left:40%; width:70%; max-width:60px;' class='circle-text-rojo'><div>"+json[i].avance+"%</div></div>":"")+								
							"</td>");
					}
					if((i%2)==1){
						$("#tablaOperandoHijoRelleno").append("<tr style='background:white; text-align: center; font-size: 13px; height: 90px;'>" +
							"<td style='width:25%; border: 1px solid black;'>" +
								(nivelRegion==2 ? "<a class='idOperandoNombreNoLink'>"+json[i].nombreCeco+"</a>":"<a class='idVistaNombre' onclick='nuevo("+json[i].idCeco+","+idPregunta+",&apos;"+desPregunta+"&apos;,&apos;"+json[i].nombreCeco+"&apos;);'>"+json[i].nombreCeco+"</a>")+
							"</td>" +
							"<td style='width:25%; border: 1px solid black; font-size:20px; font-family:Calibri;'>"+json[i].si+"</td>" +
							"<td style='width:25%; border: 1px solid black; font-size:20px; font-family:Calibri;'>"+json[i].no+"</td>" +
							"<td style='width:25%; border: 1px solid black; font-size:20px; font-family:Calibri; border-left:7px solid black; border-right:7px solid black;'>" +
								(json[i].avance>=porcentajeL ? "<div style='margin-left:40%; width:70%; max-width:60px;' class='circle-text-verde'><div>"+json[i].avance+"%</div></div>":"")+
								//(json[i].avance<90 && json[i].avance>79 ? "<div style='margin-left:40%; width:70%; max-width:60px;' class='circle-text-amarillo'><div>"+json[i].avance+"%</div></div>":"")+
								(json[i].avance<porcentajeL ? "<div style='margin-left:40%; width:70%; max-width:60px;' class='circle-text-rojo'><div>"+json[i].avance+"%</div></div>":"")+					
							"</td>");
					}	

					i++;
				}	
			},
			error : function() {
				alert('Favor de actualizar la pagina');
			}
		});
	}
	
}

function rellenadoListaReporte(nombre,nombreDos,priValor,segValor,terValor,cuaValor,id,fechaInicio,fechaFin,bloqueo){
	
	//alert('nuevo rellenado '+ nombre+' '+ nombreDos+' '+priValor+' '+segValor+' '+terValor+' '+cuaValor+' '+id+' '+fechaInicio+' '+fechaFin +' ' +bloqueo);
	//alert('entro '+bloqueo)
	
	document.getElementById('idTablaVistaPrincipal').innerHTML = ""; 
	document.getElementById('idTablaOperandoPrincipal').innerHTML = ""; 
	document.getElementById('idColumna0').innerHTML = nombre;
	document.getElementById('idColumna1').innerHTML = nombre;
	document.getElementById('idColumna3').innerHTML = nombre;
	document.getElementById('idColumna4').innerHTML = nombre;
	
	var idCanal=$("#canalVis .seleccionadoVisvR").attr("id");	
	var idPais=$("#paisVis .seleccionadoVisvR").attr("id");	
	var idCheck=$("#checklistVis .seleccionadoVisvR").attr("id");
	
	//alert(idCanal+ ' - - - ' + idPais);
	
	if(opcionPadre==0){
		//alert('ejecuta la opcion 1')
		
		$.ajax({
			type : "GET", 
			url : urlServer + '../ajaxListaReporte.json?priValor='+priValor+'&segValor='+segValor+'&terValor='+
				terValor+'&cuaValor='+cuaValor+'&id='+id+'&idCanal='+idCanal+'&idPais='+idPais+'&fechaInicio='+fechaInicio+'&fechaFin='+fechaFin+'&idCheck='+idCheck,			
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {			
				var i = 0;
				if (json[i] == null) {
					alert('Sin Datos');
				} 
				//alert('porcentajeL '+porcentajeL);
				
				while (json[i] != null) {
					
					if(i%2==0){
						$("#idTablaVistaPrincipal").append("" +
							"<tr style='background:white; height: 90px; text-align: center;'>" +
								"<td style='border: 2px solid #40795c; width: 32.5%;'>" +
									(bloqueo==0 ? "<a onclick='vistaCecoNivel("+json[i].idCeco+",&apos;"+ json[i].nombreCeco+ "&apos;);' class='idVistaNombre'>"+json[i].nombreCeco+"<BR> ("+json[i].plan+" "+nombreDos+") </a>":"") +
									(bloqueo==1 ? "<a onclick='vistaCeco("+json[i].idCeco+",&apos;"+ json[i].nombreCeco+ "&apos;,"+json[i].plan+","+json[i].real+","+json[i].avance+","+json[i].distintos+",&apos;"+json[i].imagen+"&apos;);' class='idVistaNombre'>"+json[i].nombreCeco+"<BR> ("+json[i].plan+" "+nombreDos+") </a>":"") +
									(bloqueo==2 ? "<a onclick='vistaCecoSucursal("+json[i].idCeco+",&apos;"+ json[i].nombreCeco+ "&apos;,"+json[i].plan+","+json[i].real+","+json[i].avance+","+json[i].distintos+",&apos;"+json[i].imagen+"&apos;);' class='idVistaNombre'>"+json[i].nombreCeco+"<BR> ("+json[i].plan+" "+nombreDos+") </a>":"") +
								"</td>" +
								"<td style='border: 2px solid #40795c; width: 17%; color:black; font-size: 20px;'>"+json[i].plan+"</td>" +
								"<td style='border: 2px solid #40795c; width: 17%; color:black; font-size: 20px;'>"+json[i].distintos+"</td>" +
								(json[i].imagen!=null ? "<td style='border: 2px solid #40795c; width: 17%; color:black; font-size: 20px; border-left:7px solid black; border-right:7px solid black;'><div class='icono-problema'><img class='imagen-iconoProblemas' width='30px' height='25px' src='../images/icono_problemas.png'/></div>":"<td style='border: 2px solid #40795c; width: 17%; color:black; font-size: 20px; border-left:7px solid black; border-right:7px solid black;'>")+								
									(json[i].avance>=porcentajeL ? "<div style='margin-left:37%; width:70%; max-width:60px;' class='circle-text-verde'><div>"+json[i].avance+"%</div></div>":"")+							
									//(json[i].avance<90 && json[i].avance>79 ? "<div style='margin-left:37%; width:70%; max-width:60px;' class='circle-text-amarillo'><div>"+json[i].avance+"%</div></div>":"")+
									(json[i].avance<porcentajeL ? "<div style='margin-left:37%; width:70%; max-width:60px;' class='circle-text-rojo'><div>"+json[i].avance+"%</div></div>":"")+
								"</td>" +
								"<td style='width: 1.5%; background:white;'></td>"+
								"<td style='border: 2px solid #40795c; width: 15%; color:black; font-size: 20px;'>"+json[i].real+"</td>" +
							"</tr>");					
					}
					
					if(i%2==1){
						$("#idTablaVistaPrincipal").append("" +
							"<tr style='background:#f0eded ; height: 90px; text-align: center;'>" +
								"<td style='border: 2px solid #40795c; width: 32.5%;'>" +
									(bloqueo==0 ? "<a onclick='vistaCecoNivel("+json[i].idCeco+",&apos;"+ json[i].nombreCeco+ "&apos;);' class='idVistaNombre'>"+json[i].nombreCeco+"<BR> ("+json[i].plan+" "+nombreDos+") </a>":"") +
									(bloqueo==1 ? "<a onclick='vistaCeco("+json[i].idCeco+",&apos;"+ json[i].nombreCeco+ "&apos;,"+json[i].plan+","+json[i].real+","+json[i].avance+","+json[i].distintos+",&apos;"+json[i].imagen+"&apos;);' class='idVistaNombre'>"+json[i].nombreCeco+"<BR> ("+json[i].plan+" "+nombreDos+") </a>":"") +
									(bloqueo==2 ? "<a onclick='vistaCecoSucursal("+json[i].idCeco+",&apos;"+ json[i].nombreCeco+ "&apos;,"+json[i].plan+","+json[i].real+","+json[i].avance+","+json[i].distintos+",&apos;"+json[i].imagen+"&apos;);' class='idVistaNombre'>"+json[i].nombreCeco+"<BR> ("+json[i].plan+" "+nombreDos+") </a>":"") +
								"</td>" +
								"<td style='border: 2px solid #40795c; width: 17%; color:black; font-size: 20px;'>"+json[i].plan+"</td>" +
								"<td style='border: 2px solid #40795c; width: 17%; color:black; font-size: 20px;'>"+json[i].distintos+"</td>" +
									(json[i].imagen!=null ? "<td style='border: 2px solid #40795c; width: 17%; color:black; font-size: 20px; border-left:7px solid black; border-right:7px solid black;'><div class='icono-problema'><img class='imagen-iconoProblemas' width='30px' height='25px' src='../images/icono_problemas.png'/></div>":"<td style='border: 2px solid #40795c; width: 17%; color:black; font-size: 20px; border-left:7px solid black; border-right:7px solid black;'>")+								
									(json[i].avance>=porcentajeL ? "<div style='margin-left:37%; width:70%; max-width:60px;' class='circle-text-verde'><div>"+json[i].avance+"%</div></div>":"")+							
									//(json[i].avance<90 && json[i].avance>79 ? "<div style='margin-left:37%; width:70%; max-width:60px;' class='circle-text-amarillo'><div>"+json[i].avance+"%</div></div>":"")+
									(json[i].avance<porcentajeL ? "<div style='margin-left:37%; width:70%; max-width:60px;'  class='circle-text-rojo'><div>"+json[i].avance+"%</div></div>":"")+
								"</td>" +
								"<td style='width: 1.5%; background:white;'></td>"+
								"<td style='border: 2px solid #40795c; width: 15%; color:black; font-size: 20px;'>"+json[i].real+"</td>" +
							"</tr>");				
					}

					i++;
				}
				
			},
			error : function() {
				alert('Favor de actualizar la pagina');
			}
		});
	}
		
	if(opcionPadre==1){
		//alert('ejecuta el reporte 2')
		document.getElementById('idTablaOperandoPrincipal').innerHTML = ""; 
		//document.getElementById('conteoSucursales').innerHTML = ""; 
		//document.getElementById('idNumSuc').innerHTML = ""; 
		//document.getElementById('idLugar').innerHTML = ""; 
		//var idCheck=$("#checklistVis .seleccionadoVisvR").attr("id");
		var fechaInicio=document.getElementById("fechaInicioVr").value;
		var fechaFin=document.getElementById("fechaFinVr").value;
		var territorio=$("#territorioVis .seleccionadoVisvR").attr("id");
		var zona=$("#zonaVis .seleccionadoVisvR").attr("id");
		var region=$("#regionVis .seleccionadoVisvR").attr("id");
		var ceco=null;
		
		if(territorio=="Selecciona una Opcion")
			territorio=null;
		if(zona=="Selecciona una Opcion")
			zona=null;
		if(region=="Selecciona una Opcion")
			region=null;
		
		$.ajax({
			type : "GET", 
			url : urlServer + '../getListaReporteOperacion.json?fechaInicio='+fechaInicio+'&fechaFin='+fechaFin+'&idCanal='+idCanal+'&idPais='+idPais
				+'&territorio='+territorio+'&zona='+zona+'&region='+region+'&ceco='+ceco+'&idCheck='+idCheck,			
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			delimiter : ",",
			success : function(json) {			
				var i = 0;
				if (json[i] == null) {
					alert('Sin Datos');
				}
				var lista=json[0];
				var conteo=(json[1])-1;
				
				//alert('lista-.-.- '+ json[0]);
				//alert('lista-.-.- '+ json[1]);
				document.getElementById('conteoSucursales').innerHTML = nombre +": "+ conteo+" Sucursales"; 
				document.getElementById('idConSelec').innerHTML =  conteo+" Sucursales"; 
				document.getElementById('idNomSelec').innerHTML ="("+ nombre+")" ; 
			
								
				while (lista[i] != null) {
					
					if(i%2==0){
						
						$("#idTablaOperandoPrincipal").append("" +
							"<tr style='background:white; text-align: center; font-size: 13px; font-family:Calibri; font-weight:100; height:50px;'>" +
								"<td style='width: 59%; border: 1px solid black; color:black;' class='idOperandoNombre' onclick='tablaOperandoHijo("+lista[i].idPregunta+",&apos;"+lista[i].desPregunta+"&apos;)'>"+lista[i].desPregunta+"</td>" +
								"<th style='width: 1%; background: white;'/>"+
								"<td style='width: 12%; border: 1px solid black; color:#286b3f; font-size: 16px; font-family:Calibri;'>"+lista[i].si+"</td>" +
								"<td style='width: 12%; border: 1px solid black; color:#ab2b29; font-size: 16px; font-family:Calibri;'>"+lista[i].no+"</td>" +
								"<td style='background:white; width: 16%; border-left:7px solid black; border-right:7px solid black;'>" +
									(lista[i].avance>=porcentajeL  ? "<div style='margin-left: 35%; width:35%; max-width:47px;' class='circle-text-verde'><div>"+lista[i].avance+"%</div></div>":"")+							
									//(lista[i].avance<90 && lista[i].avance>79 ? "<div style='margin-left: 35%; width:35%; max-width:47px;' class='circle-text-amarillo'><div>"+lista[i].avance+"%</div></div>":"")+
									(lista[i].avance<porcentajeL ? "<div style='margin-left: 35%; width:35%; max-width:47px;' class='circle-text-rojo'><div>"+lista[i].avance+"%</div></div>":"")+
								"</td>" +
							"</tr>");
					}
					
					if(i%2==1){
					
						$("#idTablaOperandoPrincipal").append("" +
							"<tr style='background:#f0eded ; text-align: center; font-size: 13px; font-family:Calibri; font-weight:100; height:50px;'>" +
								"<td style='width: 59%; border: 1px solid black; color:black;' class='idOperandoNombre' onclick='tablaOperandoHijo("+lista[i].idPregunta+",&apos;"+lista[i].desPregunta+"&apos;)'>"+lista[i].desPregunta+"</td>" +
								"<th style='width: 1%; background: white;'/>"+
								"<td style='width: 12%; border: 1px solid black; color:#286b3f; font-size: 16px; font-family:Calibri;'>"+lista[i].si+"</td>" +
								"<td style='width: 12%; border: 1px solid black; color:#ab2b29; font-size: 16px; font-family:Calibri;'>"+lista[i].no+"</td>" +
								"<td style='background:white; width: 16%; border-left:7px solid black; border-right:7px solid black;'>" +
									(lista[i].avance>=porcentajeL  ? "<div style='margin-left: 35%; width:35%; max-width:47px;' class='circle-text-verde'><div>"+lista[i].avance+"%</div></div>":"")+							
									//(lista[i].avance<90 && lista[i].avance>79 ? "<div style='margin-left: 35%; width:35%; max-width:47px;' class='circle-text-amarillo'><div>"+lista[i].avance+"%</div></div>":"")+
									(lista[i].avance<porcentajeL ? "<div style='margin-left: 35%; width:35%; max-width:47px;' class='circle-text-rojo'><div>"+lista[i].avance+"%</div></div>":"")+
								"</td>" +
							"</tr>");
					}

					i++;
				}
				
			},
			error : function() {
				alert('Favor de actualizar la pagina');
			}
		});
	}
		

	//manda llamar la funcion
	
	if(opcionPadre==0){
		idPadreVisita(1);
	}else{
		idPadreOperando(1);
	}
	//logBuscarbtn();
	
}

function idHijoVisitaTituloTabla() {
	var fechaFin=document.getElementById("fechaFinVr").value;
	//alert('data ' + fechaCargo);
	document.getElementById('idHijoVisitaTituloTabla').innerHTML = "";  
	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxIdHijoVisitaTituloTabla.json?fecha='+fechaFin,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			var i = 0;
			
			if (json[i] != null) {
				
				$("#idHijoVisitaTituloTabla").append("" +
					"<table style='border-spacing:2px; width:100%; height:50px;'>" +
						"<tr style='text-align: center; font-size: 16px; font-family:Calibri; background:#6c6f6d; color:white; height: 50px;'>" +
							"<td style='border: 1px solid; width: 29.15%;'>SUCURSALES</td>" +
							"<td style='border: 1px solid; width: 4.5%;'>"+json[0]+"</td>" +
							"<td style='border: 1px solid; width: 4.5%;'>"+json[1]+"</td>" +
							"<td style='border: 1px solid; width: 4.5%;'>"+json[2]+"</td>" +
							"<td style='border: 1px solid; width: 4.5%;'>"+json[3]+"</td>" +
							"<td style='border: 1px solid; width: 4.5%;'>"+json[4]+"</td>" +
							"<td style='border: 1px solid; width: 4.5%;'>"+json[5]+"</td>" +
							"<td style='border: 1px solid; width: 4.5%;'>"+json[6]+"</td>" +
							"<td style='border: 1px solid; width: 4.5%;'>"+json[7]+"</td>" +
							"<td style='border: 1px solid; width: 4.5%;'>"+json[8]+"</td>" +
							"<td style='border: 1px solid; width: 4.5%;'>"+json[9]+"</td>" +
							"<td style='border: 1px solid; width: 4.5%;'>"+json[10]+"</td>" +
							"<td style='border: 1px solid; width: 4.5%;'>"+json[11]+"</td>" +
							"<td style='border: 1px solid; width: 4.5%;'>"+json[12]+"</td>" +
							"<td style='border-left:7px solid black; border-top:7px solid black; border-right: 7px solid black; width: 4.5%;'>"+json[13]+"<a class='mensajeHoy'>"+fechaFin+"</a></td>" +	
							"<td style='border: 1px solid; width: 7,85%;'>TOTAL</td>" +
						"</tr>" +
					"</table>");
		
			}
				
		},
		error : function() {
			alert('Favor de actualizar la pagina');
		}
	});
}

var estVis=0;
function infoVisita(){
	if(estVis==1){
		$('#idInfoPiePagina2').hide();
		estVis=0;
	}else{
		$('#idInfoPiePagina2').show();
		estVis=1;
	}	
}
var estOpe=0;
function infoOpera(){
	if(estOpe==1){
		$('#idInfoPorcentaje').hide();
		estOpe=0;
	}else{
		$('#idInfoPorcentaje').show();
		estOpe=1;
	}	
}


function muestraHijoTituloGen(){
	$('#idHijoTituloGen').show();
	$('#idHijoTituloGenVacio').hide();
}

function muestraHijoTituliGen2(){
	$('#idHijoTituloGen').hide();
	$('#idHijoTituloGenVacio').show();
}

function limpiaCalendar(){
	for (var j = 0; j < 37; j++) 
		document.getElementById('idCal'+(j+1)).innerHTML = "";
	document.getElementById('idTotal').innerHTML = "0";
	$("#idTotal").hide();
}


/*************************NUEVA VISTA RESUMEN*************************/

var conteoCirculoL;
var pintadoCirculo;
var tamListaDetalle;
var detalleArray;
var banderaPinta=false;

function nuevaVistaResumen(idBitacora){
	//CAMBIA A LA VISTA RESUMEN
	banderaPinta=false;
	conteoCirculoL=0;
	pintadoCirculo=0;
	tamListaDetalle=0;
	detalleArray = new Array();
	
	//LIMPIA NOMBRE SUCURSAL
	document.getElementById("sucursalContainer").innerHTML = "";	
	//LIMPIA LOS DATOS DE LA PESTAÑA FOLIOS
	document.getElementById("idTotalFolios").innerHTML = "";		
	document.getElementById('tablaFolioContenido').innerHTML = "";
	//LIMPIA LOS DATOS DE LA PESTAÑA EVIDENCIAS
	document.getElementById("idTotalEvidencia").innerHTML = "";
	document.getElementById('tablaEvidenciasContenido').innerHTML = "";
	//LIMPIA LOS DATOS DE LA PESTAÑA FOLIOS
	document.getElementById('tipoFuenteDosFolio').innerHTML = "";
	document.getElementById('tipoFuenteUnoPregunta').innerHTML = "";
	document.getElementById('tipoFuenteUnoCuadro').innerHTML = "";
	//LIMPIA LA IMAGEN
	document.getElementById('margenImagen').innerHTML = "";

	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxVistaResumen.json?idBitacora='+idBitacora,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {	
			
			if(json!=null){
				$('#idGeneral').hide();
				$('#idGeneralDetalle').show();
				
				if(json[0]!=null){
					//LLENADO DE LA OPCION FOLIOS Y EVIDENCIAS TOTALES
					document.getElementById("idTotalFolios").innerHTML = json[0][1];
					document.getElementById("idTotalEvidencia").innerHTML ="Total de Evidencias: "+ json[0][0];
				}
				if(json[3]!=null){
					//LLENADO DE LA OPCION FOLIOS
					for (var j = 0; j < Object.keys(json[3]).length; j++) {
						$("#tablaFolioContenido").append("" +
								"<tr class='renglonEvidencia'>" +
									"<td class='anchoTotalFolioIzq'>"+(j+1)+".- "+json[3][j].pregunta+"</td>" +
									"<td class='anchoTotalFolioDer'><a onclick='linkDetalle("+json[3][j].idPregunta+");' class='fuenteLinkFolio'>Ver</a></td>" +
								"</tr>");
				
					}
				}
				
				if(json[2]!=null){
					//LLENADO DE LA OPCION EVIDENCIAS	
					var r="";
					for (var j = 0; j < Object.keys(json[2]).length; j++) {
						r=rutaHostImagen+json[2][j].ruta;
						var rutaPreview=r.replace("imagenes", "preview");
						$("#tablaEvidenciasContenido").append("" +
								"<tr class='renglonEvidencia'>" +
									"<td class='anchoTotalEvidenciaIzq'>"+(j+1)+".- "+json[2][j].descPreg+"</td>" +
									"<td class='anchoTotalEvidenciaDer'><img onclick='linkDetalle("+json[2][j].idPregunta+");' src='"+rutaPreview+"' class='imagenPreviewEvidencia'></td>" +
								"</tr>");
					}
				}
				
				if(json[1]!=null){
					//NOMBRE SUCURSAL
					document.getElementById("sucursalContainer").innerHTML = json[1][0].nombresuc;					
					//LLENADO DE LA FECHA EN LA PESTAÑA FECHA Y HORA
					document.getElementById("fInicio").innerHTML ="Inicio: "+ json[1][0].fecha_inicio;
					document.getElementById("fFin").innerHTML ="Fin: "+ json[1][0].fecha_fin;
					document.getElementById("fEnvio").innerHTML ="Env&iacute;o: "+ json[1][0].fechaEnvio;
					//LLENADO DE LA HORA EN LA PESTAÑA FECHA Y HORA
					document.getElementById("hInicio").innerHTML ="Inicio: "+ json[1][0].horaInicio;
					document.getElementById("hFin").innerHTML ="Fin: "+ json[1][0].horaFin;
					document.getElementById("hModo").innerHTML ="Modo: "+ json[1][0].modo;						
				}
				
				if(json[4]!=null){
					detalleArray=json[4];
					tamListaDetalle=Object.keys(json[4]).length;
					
					
					if(tamListaDetalle>0){
						//LLENA EL PRIMER LA PRIMER PREGUNTA EN LA VISTA DETALLE
						document.getElementById("tipoFuenteDosFolio").innerHTML = json[4][0].folio;
						document.getElementById("tipoFuenteUnoPregunta").innerHTML = json[4][0].orden+".- "+json[4][0].desPregunta;
						document.getElementById("tipoFuenteUnoCuadro").innerHTML = json[4][0].descripcion;
						document.getElementById("tipoFuenteDosAccion").innerHTML = json[4][0].accion;
						
						var rutaPreview1=rutaHostImagen+""+json[4][0].evidencia;
						
						if(json[4][0].evidencia!="" || json[4][0].evidencia!=null)
							$("#margenImagen").append("<img onclick='muestraPopup(&apos;"+rutaPreview1+"&apos;);' class='tamaImagen' src='"+rutaPreview1+"'>");
						else
							$("#margenImagen").append("<img class='tamaImagenNoRequerida' src='../images/reporteOnline/evidencia_no_requerida.png'>");
				
					}
				}
				creaCirculos(tamListaDetalle);				
				banderaPinta=true;
				
			}else{
				alert('No hay datos de visita en este dia');
			}	
		}		
	});
}

function anteriorDetalle(){	
	if(pintadoCirculo==1){
		pintadoCirculo=tamListaDetalle+1;
		limpiaInicio();
	}
	
	pintaSelec(pintadoCirculo-1);
	if((pintadoCirculo%10)==0)
		quitaMas();

}

function siguienteDetalle(){
	if(pintadoCirculo==tamListaDetalle){
		pintadoCirculo=0;
		conteoCirculoL=0;
		limpiaFinal();
		//creaCirculos(pintadoCirculo);
	}
	if((pintadoCirculo%10)==0)
		agregaMas();
	pintaSelec(pintadoCirculo+1);
}

function pintaSelec(numEvidencia){
	pintadoCirculo=numEvidencia;
	document.getElementById('tipoFuenteDosFolio').innerHTML = "";
	document.getElementById('tipoFuenteUnoPregunta').innerHTML = "";
	document.getElementById('tipoFuenteUnoCuadro').innerHTML = "";
	document.getElementById('tipoFuenteDosAccion').innerHTML = "";
	document.getElementById('margenImagen').innerHTML = "";
	var rutaPreview=rutaHostImagen+""+detalleArray[pintadoCirculo-1].evidencia;
	
	pintaSelecInicial(numEvidencia)

	document.getElementById("tipoFuenteDosFolio").innerHTML = detalleArray[pintadoCirculo-1].folio;
	document.getElementById("tipoFuenteUnoPregunta").innerHTML = detalleArray[pintadoCirculo-1].orden+".- "+detalleArray[pintadoCirculo-1].desPregunta;
	document.getElementById("tipoFuenteUnoCuadro").innerHTML = detalleArray[pintadoCirculo-1].descripcion;
	document.getElementById("tipoFuenteDosAccion").innerHTML = detalleArray[pintadoCirculo-1].accion;

	if(detalleArray[pintadoCirculo-1].evidencia!=null)
		$("#margenImagen").append("<img onclick='muestraPopup(&apos;"+rutaPreview+"&apos;);' class='tamaImagen' src='"+rutaPreview+"'>");
	else
		$("#margenImagen").append("<img class='tamaImagenNoRequerida' src='../images/reporteOnline/evidencia_no_requerida.png'>");
}

function creaCirculos(tam){
	document.getElementById("celdaCirculos").innerHTML="";
	for (var i = 0; i < tam; i++) {
		conteoCirculoL=conteoCirculoL+1;
		$("#celdaCirculos").append("" +
			"<span onclick='irEvidencia("+conteoCirculoL+");' class='circulo_Black' id='circuloBlack_"+conteoCirculoL+"'>"+conteoCirculoL+"</span>");
	}
	pintaSelecInicial(1);
	for (var j =10; j < tam; j++) {
		//document.getElementById("circuloBlack_"+i).hide();
		$("#circuloBlack_"+(j+1)).hide();
	}
}

function pintaSelecInicial(numEvidencia){
	pintadoCirculo=numEvidencia;
	//if(banderaPinta){
		for (var i = 0; i < tamListaDetalle; i++) {
			document.getElementById("circuloBlack_"+(i+1)).style.background="black";
		}
		document.getElementById("circuloBlack_"+numEvidencia).style.background="#00a500";
	//}	
	
}

function irEvidencia(numEvidencia){
	pintaSelec(numEvidencia);
}


function agregaMas(){
	//FUNCIONALIDAD DE 10 EN 10
	for (var i = pintadoCirculo-10; i < pintadoCirculo; i++) {
		$("#circuloBlack_"+(i+1)).hide();
	}
	for (var i = pintadoCirculo; i < pintadoCirculo+10; i++) {
		$("#circuloBlack_"+(i+1)).show();
	}
}
function quitaMas(){
	//FUNCIONALIDAD DE 10 EN 10
	for (var i = pintadoCirculo-10; i < pintadoCirculo; i++) {
		$("#circuloBlack_"+(i+1)).show();
	}
	for (var i = pintadoCirculo; i < pintadoCirculo+10; i++) {
		$("#circuloBlack_"+(i+1)).hide();
	}
}
function limpiaInicio(){
	var resid=tamListaDetalle%10;
	var inic=tamListaDetalle-resid;
	limpiaFinal();
	for (var i = inic; i < tamListaDetalle; i++) {
		$("#circuloBlack_"+(i+1)).show();
	}
}
function limpiaFinal(){
	for (var i = 0; i < tamListaDetalle; i++) {
		$("#circuloBlack_"+(i+1)).hide();
	}
}

function linkDetalle(idPregunta){
	for (var int = 0; int < tamListaDetalle; int++) {
		//alert('Pregunta No. '+detalleArray[int].idPregunta);

		if(((int)%10)==0){
			for (var i = 0; i < tamListaDetalle; i++) {
				$("#circuloBlack_"+(i+1)).hide();
			}
			for (var i = int; i < int+10; i++) {
				$("#circuloBlack_"+(i+1)).show();
			}
		}else if((int)<10){
			for (var i = 0; i < tamListaDetalle; i++) {
				$("#circuloBlack_"+(i+1)).hide();
			}
			for (var i = 0; i < 10; i++) {
				$("#circuloBlack_"+(i+1)).show();
			}
		}
		if(idPregunta==detalleArray[int].idPregunta){
			irEvidencia(int+1)
			//alert('irEvidencia ' +(int+1))
			break;
		}
		
	}
}

function regresaNuevaVistaResumen(){
	$('#idGeneral').show();
	$('#idGeneralDetalle').hide();
}

function muestraPopup(ruta){
	document.getElementById("idImagenEmergente").src =ruta;
	$('#ventanaEmergente').css("visibility","visible");
	$('#ventanaEmergente').show();
	
}

function ocultaPopup(){
	$('#ventanaEmergente').hide();
}
/***************Funciones para salir de la ventana emergente******************/
function amIclicked(e, element){
    e = e || event;
    var target = e.target || e.srcElement;
    if(target.id==element.id)
        return true;
    else
        return false;
}

function oneClick(event, element){
    if(amIclicked(event, element)){
    	$('#ventanaEmergente').hide();
    }
}
/***************Funciones para salir de la ventana emergente******************/
