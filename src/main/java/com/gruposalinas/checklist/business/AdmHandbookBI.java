package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.AdmHandbookDAO;
import com.gruposalinas.checklist.domain.AdmHandbookDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class AdmHandbookBI {

	private static Logger logger = LogManager.getLogger(AdmHandbookBI.class);

	@Autowired
	AdmHandbookDAO admHandbookDAO;
	
	List<AdmHandbookDTO> listaBooks = null;

	

	public List<AdmHandbookDTO> obtieneTodos(){
				
		try{
			listaBooks = admHandbookDAO.obtieneTodos();
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla FRTAHANDBOOK");
					
		}
				
		return listaBooks;
	}
	
	public List<AdmHandbookDTO> obtieneIdBook(AdmHandbookDTO bean){
				
		try{
			listaBooks = admHandbookDAO.obtieneIdBook(bean);	
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla  FRTAHANDBOOK ");
			
		}
				
		return listaBooks;
	}	
	

	public boolean insertaHandbook(AdmHandbookDTO bean) {
		
		boolean respuesta = false;
		
		try {		
			respuesta = admHandbookDAO.insertaHandbook(bean);		
		} catch (Exception e) {
			logger.info("No fue posible insertar el Compromiso");
			
		}
		
		return respuesta;		
	}
	
	
	public boolean actualizaHandbook (AdmHandbookDTO bean){
		
		boolean respuesta = false;
				
		try {
			respuesta = admHandbookDAO.actualizaHandbook(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Handbook");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaHandbook(AdmHandbookDTO bean){
		
		boolean respuesta = false;
		
		try{
			respuesta = admHandbookDAO.eliminaHandbook(bean);
		}catch(Exception e){
			logger.info("No fue posible borrar el HandBook");
			
		}
		
		return respuesta;
	}

}
