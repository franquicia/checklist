package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.GeografiaDTO;


public interface GeografiaDAO {

	
	public List<GeografiaDTO> obtieneGeografia(String idCeco, String idRegion, String idZona, String idTerritorio) throws Exception;
	
	public boolean insertaGeografia(GeografiaDTO bean) throws Exception;
	
	public boolean actualizaGeografia(GeografiaDTO bean) throws Exception;
	
	public boolean eliminaGeografia(int idCeco) throws Exception;
}
