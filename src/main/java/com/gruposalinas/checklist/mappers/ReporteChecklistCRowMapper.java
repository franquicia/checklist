package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.VistaCDTO;

public class ReporteChecklistCRowMapper implements RowMapper<VistaCDTO> {

	public VistaCDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		VistaCDTO reporte = new VistaCDTO();
		
		reporte.setIdCeco(rs.getString("FCID_CECO"));
		reporte.setDescCeco(rs.getString("FCNOMBRE"));
		reporte.setTotal(rs.getInt("CONTEO"));
		reporte.setIdChecklist(rs.getString("FIID_CHECKLIST"));
		reporte.setNumSuc(rs.getString("NOSUCURSAL"));
		
		return reporte;
	}

}
