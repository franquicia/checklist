package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.RepMedicionDTO;

public class RepMedicionPregRowMapper implements RowMapper<RepMedicionDTO> {

	public RepMedicionDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		RepMedicionDTO reportePregDTO = new RepMedicionDTO();
		reportePregDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		reportePregDTO.setPregunta(rs.getString("FCPREGUNTA"));
		reportePregDTO.setNumSerie(rs.getInt("FINUMSERIE"));
		reportePregDTO.setIdCheckList(rs.getInt("FIID_CHECKLIST"));
		
		return reportePregDTO;
		
	}
}
