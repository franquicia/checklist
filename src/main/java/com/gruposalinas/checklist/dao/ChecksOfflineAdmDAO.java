package com.gruposalinas.checklist.dao;

import java.util.Map;

public interface ChecksOfflineAdmDAO {
	
	public Map<String, Object> obtieneChecks(int idUsuario) throws Exception;
	
	public Map<String, Object> obtieneChecksNuevo(int idUsuario) throws Exception;
	
	public Map<String, Object> obtieneChecksNuevo3(int idUsuario,int idChecklist)  throws Exception;
	
	public Map<String, Object> obtieneChecksNuevo4(int idUsuario) throws Exception;
        
        Map<String, Object> getBitacoras(int idUsuario,int idChecklist) throws Exception ;

}
