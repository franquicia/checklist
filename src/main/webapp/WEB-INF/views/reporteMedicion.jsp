<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>B�squeda general</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervision/estilos.css">
</head>

<body ng-app="app" ng-controller="validarForm" onpaste="return false">



  <div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="${pageContext.request.contextPath}/img/supervision/btn_hamburguer.svg" alt="Men� principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="inicio.htm"><img src="${pageContext.request.contextPath}/img/supervision/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.htm" class="selMenu">
          <div>Salir</div>
          <div><img src="${pageContext.request.contextPath}/img/supervision/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="indexSupervision.htm">Reporte de Supervisores</a> 
		</div>
    	Descarga base de protocolos.
    </div>

	<!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervision/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Men� Principal<br>
          </div>
        </div>
        <%-- <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la p�gina">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> --%>
        <div id="menu">
          <ul class="l-navegacion nivel1">
          		<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Asegurador</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>B�squeda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedSupervisorSupervision.htm"><div>B�squeda Asegurador</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galer�a de Evidencias</div></a>
				</li>
			</ul>
        </div>
      </div>
    </div>

    <!-- Contenido -->
    <div class="contSecc">
    
	<div class="liga">
		<a href="inicio.htm" class="liga">
		<img style="height: 15px; width: 15px;" src="${pageContext.request.contextPath}/img/supervision/arrowmleft.png">
		<span>P�GINA PRINCIPAL</span>
		</a>
		</div>
	
		<div class="titSec">Selecciona el protocolo que deseas descargar.</div>
		<div class="gris">
			
		  
		  <form:form method="POST" model="command" name="form" id="form">
			
			<div class="divCol4 flexJusCen">
				<div class="col4 col4B">
					Fecha de inicio:<br>
					<input required type="text" maxlength="10" placeholder="DD/MM/AAAA" value="" class="datapicker1 date" id="datepicker" autocomplete="off">
				</div>
				<div class="col4 w30A"><div><strong>a</strong></div></div>
				<div class="col4 col4B">
					Fecha de fin:<br>
					<input required type="text" maxlength="10" placeholder="DD/MM/AAAA" value="" class="datapicker1 date" id="datepicker1" autocomplete="off">
				</div>
				<div class="col4 col4B">
					Protocolo:<br>
					<select id="Protocolo" >
						  <c:forEach var="listaProtocolos" items="${listaProtocolos}">
									<option value="${listaProtocolos.idChecklist}">
										<c:out value="${listaProtocolos.nomChecklist}"></c:out>
									</option>
						  </c:forEach>
					</select>
				</div>
			</div>
			<div class="btnCenter">
				<a href="#" class="btn btnBuscarS"  onclick="return validaFechas();">Descargar</a>
			</div>
			
				<input id="protocolo" name="protocolo" type="hidden" value="" />
				<input id="calendarioIni" name="calendarioIni" type="hidden" value=""/>
				<input id="calendarioFin" name="calendarioFin" type="hidden" value=""/>
			
			</form:form>		
				
			</div>
						
		</div>				
	</div>	<!-- Fin conSecc -->
	
	<!-- Footer -->
	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualizaci�n Febrero 2019</div>
	</div>
</div><!-- Fin page -->



</body>
</html>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/custom-file-input.js"></script><!--Unpload -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/funciones.js"></script>


<script type="text/javascript">


function validaFechas(){	
	var f1=document.getElementById("datepicker").value;
	var f2=document.getElementById("datepicker1").value;
	var p=document.getElementById("Protocolo").value;
	
 	var Fecha1 = stringToDate(f1,"dd/MM/yyyy","/");
 	var Fech2 = stringToDate(f2,"dd/MM/yyyy","/");
 	var FHoy = new Date();
	
    var flag1=false;
 	var flag2=false;
 		
		if (isNaN(Fecha1)){
			alert("Debes colocar fecha inicio");
			return false;
		}
		else{
			if(Fecha1<FHoy){
			flag1=true;
			}else{
				alert("La fecha inicio no puede ser mayor que hoy");
				return false;
			}
		}
		
		if (isNaN(Fech2)){
			alert("Debes colocar fecha fin");
			return false;
		}
		else{
			if(Fech2<FHoy){
				flag2=true;
				}else{
					alert("La fecha fin no puede ser mayor que hoy");
					return false;
				}
		} 
	
		if(flag1==true && flag2==true){
			if(Fecha1>Fech2){
				alert("La fecha de inicio no debe ser mayor que la de fin");
				return false;
			}else{
	
				document.getElementById("calendarioIni").value=f1;
				document.getElementById("calendarioFin").value=f2;
				document.getElementById("protocolo").value=p;
				//alert(p+", "+f1+", "+f2);
				
				formProtocolos = document.getElementById("form");
				formProtocolos.action="descargaBaseProtocolos.htm";
				formProtocolos.submit();
				alert("Has descargado la base de datos de protocolos, ahora se descargara la base de protocolos por zona");
				form = document.getElementById("form");
				form.action="descargaBase.htm";
				form.submit();
				
			}
		} 

	return false;	
}

function stringToDate(_date,_format,_delimiter)
{
            var formatLowerCase=_format.toLowerCase();
            var formatItems=formatLowerCase.split(_delimiter);
            var dateItems=_date.split(_delimiter);
            var monthIndex=formatItems.indexOf("mm");
            var dayIndex=formatItems.indexOf("dd");
            var yearIndex=formatItems.indexOf("yyyy");
            var month=parseInt(dateItems[monthIndex]);
            month-=1;
            var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
            return formatedDate;
}

</script>
