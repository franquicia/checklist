package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.EdoChecklistDTO;

public class EdoChecklistRowMapper implements RowMapper<EdoChecklistDTO> {

	public EdoChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		EdoChecklistDTO edoChecklistDTO = new EdoChecklistDTO();
		
		edoChecklistDTO.setIdEdochecklist(rs.getInt("FIID_ESTADO"));
		edoChecklistDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
		
		return edoChecklistDTO;
	}
}
