package com.gruposalinas.checklist.servicios.servidor;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.business.BitacoraBI;
import com.gruposalinas.checklist.business.BitacoraGralBI;
import com.gruposalinas.checklist.business.ChecklistBI;
import com.gruposalinas.checklist.business.ChecklistPreguntasComBI;
import com.gruposalinas.checklist.business.ChecksOfflineBI;
import com.gruposalinas.checklist.business.ConsultaBI;
import com.gruposalinas.checklist.business.Correo7SBI;
import com.gruposalinas.checklist.business.CorreoBI;
import com.gruposalinas.checklist.business.FirmaCheckBI;
import com.gruposalinas.checklist.business.FirmaExpansionBI;
import com.gruposalinas.checklist.business.IncidentesBI;
import com.gruposalinas.checklist.business.LoginBI;
import com.gruposalinas.checklist.business.MovilInfoBI;
import com.gruposalinas.checklist.business.NotificacionBI;
import com.gruposalinas.checklist.business.ParametroBI;
import com.gruposalinas.checklist.business.RepoCheckPaperBI;
import com.gruposalinas.checklist.business.ReporteBI;
import com.gruposalinas.checklist.business.ReporteChecksExpBI;
import com.gruposalinas.checklist.business.RespuestaBI;
import com.gruposalinas.checklist.business.RespuestaPdfBI;
import com.gruposalinas.checklist.business.SucursalChecklistBI;
import com.gruposalinas.checklist.business.TipoCifradoBI;
import com.gruposalinas.checklist.business.Usuario_ABI;
import com.gruposalinas.checklist.business.VersionBI;
import com.gruposalinas.checklist.business.VisitaTiendaBI;
import com.gruposalinas.checklist.domain.ChecksOfflineCompletosDTO;
import com.gruposalinas.checklist.domain.AdjuntoDTO;
import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.BitacoraGralDTO;
import com.gruposalinas.checklist.domain.CanalDTO;
import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.ChecklistCompletoDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.ChecksOfflineDTO;
import com.gruposalinas.checklist.domain.Coment7SDTO;
import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.domain.ConsultaDTO;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;
import com.gruposalinas.checklist.domain.IncidenteDTO;
import com.gruposalinas.checklist.domain.LoginDTO;
import com.gruposalinas.checklist.domain.MovilInfoDTO;
import com.gruposalinas.checklist.domain.ParametroDTO;
import com.gruposalinas.checklist.domain.PerfilUsuarioDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.RegistroRespuestaDTO;
import com.gruposalinas.checklist.domain.RepoCheckPaperDTO;
import com.gruposalinas.checklist.domain.ReporteChecksExpDTO;
import com.gruposalinas.checklist.domain.ReporteDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;
import com.gruposalinas.checklist.domain.TipoCifradoDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.domain.Usuario_ADTO;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.util.StrCipher;
import com.gruposalinas.checklist.util.UtilCryptoGS;
import com.gruposalinas.checklist.util.UtilDate;
import com.gruposalinas.checklist.util.UtilMail;
import com.gruposalinas.checklist.util.UtilObject;
import com.gruposalinas.checklist.util.UtilResource;

import sun.misc.BASE64Decoder;

@Controller
@RequestMapping("/serviciosDataCenter")
public class ServicioDataCenter {

	private static Logger logger = LogManager.getLogger(ServicioDataCenter.class);


	@Autowired
	ChecklistBI checklistBI;

	@Autowired
	ConsultaBI consultaBI;

	@Autowired
	ReporteBI reportebi;

	//http://10.51.218.113:80/checklist/ejecutaServiciosEncriptados/ejecutaServicio2.json?service=serviciosDataCenter/getChecklistByGrupo.json?id=<?>&idGrupo=<?>
	@RequestMapping(value = "/getChecklistByGrupo.json", method = RequestMethod.GET) 
	public @ResponseBody List<ConsultaDTO> getChecklistByGrupo(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		List<ConsultaDTO> lista=null;

		try {

			UtilCryptoGS cifra = new UtilCryptoGS();
			String uri = request.getQueryString();
			String uriAp = request.getParameter("token");
			int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
			uri = uri.split("&")[0];

			String urides = "";

			if (idLlave == 10) {
				urides = (cifra.decryptParams(uri,FRQConstantes.getLlaveEncripcionLocalDatacenter()));
			} 

			int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);

			int idGrupo = Integer.parseInt(urides.split("&")[1].split("=")[1]);

			lista = consultaBI.obtieneConsultaChecklist(idGrupo);


		}
		catch(Exception e) {
			lista = null;
		}


		if(lista == null) {
			lista = new ArrayList<>();
		}


		return lista;
	}

    //http://10.51.218.113:80/checklist/ejecutaServiciosEncriptados/ejecutaServicio2.json?service=serviciosDataCenter/reporteGeneralComp.json?idUsuario=191312&idChecklist=1&fechaInicio=01/08/2019&fechaFin=30/08/2019
	//http://localhost:8080/checklist/servicios/reporteGeneralComp.json?idUsuario=<?>&idChecklist=<?>&fechaInicio=01/04/2017&fechaFin=07/04/2017
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/reporteGeneralComp", method = RequestMethod.GET)
	public @ResponseBody String reporteGeneralComp(HttpServletRequest request, HttpServletResponse response)throws Exception {

		Map<String, Object> mapReporteGeneral = null;
		List<PreguntaDTO> listaPreguntas = null;
		List<ReporteDTO>  listaRespuestas = null;
		List<ReporteDTO>  listaRespuestasFinal = new ArrayList<ReporteDTO>();
	
		int conteo=0;	

		JsonArray arrayProtocolo = new JsonArray();

		try{
			
			UtilCryptoGS cifra = new UtilCryptoGS();
			String uri = request.getQueryString();
			String uriAp = request.getParameter("token");
			int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
			uri = uri.split("&")[0];

			String urides = "";

			if (idLlave == 10) {
				urides = (cifra.decryptParams(uri,FRQConstantes.getLlaveEncripcionLocalDatacenter()));
			} 
			
			int idUsu = Integer.parseInt(urides.split("&")[0].split("=")[1]);
			int idChecklist = Integer.parseInt(urides.split("&")[1].split("=")[1]);
			String fechaInicio = urides.split("&")[2].split("=")[1];
			String fechaFin = urides.split("&")[3].split("=")[1];

			
			mapReporteGeneral = reportebi.ReporteGeneralCompromisos(idChecklist, fechaInicio, fechaFin);
			if(mapReporteGeneral!=null){
				Iterator<Map.Entry<String, Object>> enMap = mapReporteGeneral.entrySet().iterator();
				while (enMap.hasNext()) {
					Map.Entry<String, Object> enMaps = enMap.next();
					String nomH = enMaps.getKey();
					if (nomH == "listaPregunta") {
						listaPreguntas = (List<PreguntaDTO>) enMaps.getValue();
					}
					if (nomH == "listaRespuesta") {
						listaRespuestas = (List<ReporteDTO>) enMaps.getValue();
					}
					if (nomH == "conteo") {
						conteo = (Integer) enMaps.getValue();
					}
				}

				ServletOutputStream sos = null;
				String archivo = "";
				String columnaTitle="";


				if(listaRespuestas!=null && listaPreguntas!=null){
					if(listaRespuestas.size()>0){			

						int numBitacoras=1;
						for(int a=0; a<listaRespuestas.size()-1; a++){
							if(listaRespuestas.get(a+1)!=null){
								if(listaRespuestas.get(a).getIdBitacora()!=listaRespuestas.get(a+1).getIdBitacora()){
									numBitacoras++;
								}
							}			
						}

						int k=0;
						for (int i = 0; i < numBitacoras; i++) {	
							for (int j = 0; j < conteo; j++) {	
								//logger.info("...   No. Bita "+(i+1)+"   ...   bitacora "+listaRespuestas.get(k).getIdBitacora()+"   ....   variable Preguntas "+ listaPreguntas.get(j).getIdPregunta()+"   ...   variable Respuestas "+listaRespuestas.get(k).getIdPregunta()  +"   ...");

								if(listaPreguntas.get(j).getIdPregunta()==Integer.parseInt(listaRespuestas.get(k).getIdPregunta())){
									listaRespuestasFinal.add(listaRespuestas.get(k));
									if(k<listaRespuestas.size()-1)
										k++;
								}else{
									ReporteDTO repo= new ReporteDTO();
									repo.setDesPregunta("vacio");
									repo.setRespuesta("");
									listaRespuestasFinal.add(repo);
								}
							}
						}

						JsonObject jsonProtocolo = null;

						int inc=0;
						int auxInc=0;
						for (int i = 0; i < numBitacoras; i++) {
							auxInc=inc;
							for(int r=0; r<conteo; r++){
								ReporteDTO reporteDTO=listaRespuestasFinal.get(auxInc);
								if(reporteDTO.getFecha_r()!=null){

									jsonProtocolo = new JsonObject();

									jsonProtocolo.addProperty("fecha_respuesta", reporteDTO.getFecha_r()); 
									jsonProtocolo.addProperty("nombre_usuario",  reporteDTO.getNombreUsuario() );
									jsonProtocolo.addProperty("id_usuario",reporteDTO.getIdUsuario());
									jsonProtocolo.addProperty("puesto_usuario", reporteDTO.getPuesto());
									jsonProtocolo.addProperty("pais", reporteDTO.getPais());
									jsonProtocolo.addProperty("territorio",reporteDTO.getTerritorio() );
									jsonProtocolo.addProperty("zona", reporteDTO.getZona());
									jsonProtocolo.addProperty("regional", reporteDTO.getRegional());
									jsonProtocolo.addProperty("sucursal", reporteDTO.getSucursal());
									jsonProtocolo.addProperty("canal", reporteDTO.getCanal());
									jsonProtocolo.addProperty("ponderacion", reporteDTO.getPonderacion());

									break;
								}
								auxInc++;
							}


							JsonArray arrayPreguntas = new JsonArray();
							JsonObject jsonPregunta =  null; 

							for (int j = 0; j < conteo; j++) {	
								
								ReporteDTO reporteDTO2=listaRespuestasFinal.get(inc);
								
								PreguntaDTO pregunta = listaPreguntas.get(j);
								
								
										jsonPregunta = new JsonObject();

										jsonPregunta.addProperty("pregunta", pregunta.getPregunta());
										jsonPregunta.addProperty("respuesta", reporteDTO2.getRespuesta());
				

								arrayPreguntas.add(jsonPregunta);

								inc++;
							}

							jsonProtocolo.add("data", arrayPreguntas);

							arrayProtocolo.add(jsonProtocolo);

						}					

					}else{
						logger.info("NO EXISTEN RESPUESTAS");


						JsonObject jsonProtocolo = null; 

						String sinRespuesta = "Sin respuesta";

						jsonProtocolo = new JsonObject();

						jsonProtocolo.addProperty("fecha_respuesta", sinRespuesta); 
						jsonProtocolo.addProperty("nombre_usuario",  sinRespuesta );
						jsonProtocolo.addProperty("id_usuario",sinRespuesta);
						jsonProtocolo.addProperty("puesto_usuario", sinRespuesta);
						jsonProtocolo.addProperty("pais", sinRespuesta);
						jsonProtocolo.addProperty("territorio",sinRespuesta );
						jsonProtocolo.addProperty("zona", sinRespuesta);
						jsonProtocolo.addProperty("regional", sinRespuesta);
						jsonProtocolo.addProperty("sucursal", sinRespuesta);
						jsonProtocolo.addProperty("canal", sinRespuesta);
						jsonProtocolo.addProperty("ponderacion", sinRespuesta);


						JsonArray arrayPreguntas = new JsonArray();
						JsonObject jsonPregunta =  null;

						int inc=0;

						for (int j = 0; j < conteo; j++) {	


							jsonPregunta = new JsonObject();

							jsonPregunta.addProperty("pregunta", sinRespuesta);
							jsonPregunta.addProperty("respuesta", sinRespuesta);

							arrayPreguntas.add(jsonPregunta);

							inc++;
						}

						jsonProtocolo.add("data", arrayPreguntas);  								
						arrayProtocolo.add(jsonProtocolo);

					}
				}
				else{
					logger.info("NO EXISTEN RESPUESTAS");


					JsonObject jsonProtocolo = null; 

					String sinRespuesta = "Sin respuesta";

					jsonProtocolo = new JsonObject();

					jsonProtocolo.addProperty("fecha_respuesta", sinRespuesta); 
					jsonProtocolo.addProperty("nombre_usuario",  sinRespuesta );
					jsonProtocolo.addProperty("id_usuario",sinRespuesta);
					jsonProtocolo.addProperty("puesto_usuario", sinRespuesta);
					jsonProtocolo.addProperty("pais", sinRespuesta);
					jsonProtocolo.addProperty("territorio",sinRespuesta );
					jsonProtocolo.addProperty("zona", sinRespuesta);
					jsonProtocolo.addProperty("regional", sinRespuesta);
					jsonProtocolo.addProperty("sucursal", sinRespuesta);
					jsonProtocolo.addProperty("canal", sinRespuesta);
					jsonProtocolo.addProperty("ponderacion", sinRespuesta);


					JsonArray arrayPreguntas = new JsonArray();
					JsonObject jsonPregunta =  null;

					int inc=0;

					for (int j = 0; j < conteo; j++) {	


						jsonPregunta = new JsonObject();

						jsonPregunta.addProperty("pregunta", sinRespuesta);
						jsonPregunta.addProperty("respuesta", sinRespuesta);

						arrayPreguntas.add(jsonPregunta);

						inc++;
					}

					jsonProtocolo.add("data", arrayPreguntas);  									
					arrayProtocolo.add(jsonProtocolo);

				}


			}else{
				logger.info("LA CONSULTA DEL CHECKLIST ESTA VACIA");
				arrayProtocolo = new JsonArray();
			}					
		} catch (Exception e){
			logger.info(e);
			arrayProtocolo = new JsonArray();
		}		

		System.out.println(arrayProtocolo.toString());
		
		return arrayProtocolo.toString();
	}


}
