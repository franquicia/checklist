$(document).ready(function() {
	asignarEventosCombo();
});

function asignarEventosCombo() {
	$(".cajaVis").off();

	$(".cajaVis").hover(

			function(e) {
				asignarEventosCombo(); 
				
				var lista = $(this).find("ul");
				var triangulo = $(this).find("span:last-child");
				e.preventDefault();

				$(this).find("ul").toggle();
				if (lista.is(":hidden")) {
					triangulo.removeClass("triangulosupVis").addClass("trianguloinfVis");
					triangulo.parent().css('z-index', 0);
				} else {
					triangulo.removeClass("trianguloinfVis").addClass("triangulosupVis");
					triangulo.parent().css('z-index', 3000);
				}
			});

	$(".cajaVis").on("click","li",
			function clickli(e) {
				var texto = $(this).text();
				var seleccionadoVis = $(this).parent().prev();
				var lista = $(this).closest("ul");
				var triangulo = $(this).parent().next();
				e.preventDefault();
				e.stopPropagation();
				seleccionadoVis.text(texto);
				lista.hide();
				triangulo.removeClass("triangulosupVis").addClass("trianguloinfVis");
				triangulo.parent().css('z-index', 0);
				$(".cajaVis").unbind('mouseleave');
				var valor = $(this).val();
				seleccionadoVis.attr("id", valor);
				var divActivo = $(this).parents("li").find(".cajaVis").attr("id");

				if (divActivo == 'categoriaVis')
					cambiarCategoria()
					
				if (divActivo == 'tipoVis')
					$("#valorTipo").val($("#tipoVis .seleccionadoVis").attr("id"));

				if (divActivo == 'puestoVis')
					$("#valorPuesto").val($("#puestoVis .seleccionadoVis").attr("id"));
			
				if (divActivo == 'paisVis')
					cambiarPais()
					
				if (divActivo == 'territorioVis')
					cambiarTerritorio()

				if (divActivo == 'zonaVis')
					cambiarZona()

				if (divActivo == 'regionVis')
					cambiarRegion()

				if (divActivo == 'sucursalVis') 
					$("#valorSucursal").val($("#sucursalVis .seleccionadoVis").attr("id"));

			});
}

function cambiarCategoria() {

	// Asigna el valor del li al input hidden
	$("#valorCategoria").val($("#categoriaVis .seleccionadoVis").attr("id"));

	// Vulve a inicializar el valor del span de la lista a rellenar
	$("#tipoVis .seleccionadoVis").val("Selecciona una Opcion");
	$('#tipoVis .seleccionadoVis').text("Elije una opcion");
	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#valorTipo").val("Selecciona una Opcion");

	var varCateg = document.getElementById("valorCategoria").value;

	if (varCateg != "Selecciona una Opcion")
		categoria(varCateg);

}

function categoria(varCateg) {
	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxCategoria.json?varCateg='
				+ varCateg,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			document.getElementById("tipoVis2").innerHTML = "";
			var jsonC = JSON.stringify(json);
			var i = 0;
			// $("#tipoVis2").append("<li value='Selecciona una
			// Opcion'><a
			// href=''>Elije una opcion</a></li>");
			while (json[i] != null) {
				$("#tipoVis2").append(
						"<li value='" + json[i].idChecklist
								+ "'><a href=''>" + json[i].nombreCheck
								+ "</a></li>");
				i++;
			}
		},
		error : function() {
			alert('Algo paso');
		}
	});
}

function init() {
	$('#buscaUsuario').autocomplete({
		serviceUrl : urlServer + '../getUsuarios.json',
		paramName : "tagId",
		delimiter : ",",

		transformResult : function(response) {
			return {
				suggestions : $.map($.parseJSON(response), function(item) {
					document.forms.formulario.costos.value = item.idCeco;
				})
			};
		}
	});

	$('#buscaUsuario').keypress(function(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == '13') {
			filtraUsuario();
		}
		event.stopPropagation();
	});

}

function filtraUsuario() {
	var varUsuario = document.getElementById("buscaUsuario").value;
	formulario.costos.length = 0;
	if (varUsuario != "Elije una opcion")
		usuario(varUsuario);
}

function usuario(varUsuario) {
	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxUsuario.json?varUsuario='
				+ varUsuario,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			var jsonC = JSON.stringify(json);
			document.forms.formulario.costos.value = json[0].idCeco;
		},
		error : function() {
			alert('Algo paso');
		}
	});
}

function cambiarPais() {

	// Asigna el valor del li al input hidden
	$("#valorPais").val($("#paisVis .seleccionadoVis").attr("id"));

	// Vulve a inicializar el valor del span de la lista a rellenar
	$("#territorioVis .seleccionadoVis").val("Selecciona una Opcion");
	$('#territorioVis .seleccionadoVis').text("Elije una opcion");
	$("#zonaVis .seleccionadoVis").val("Selecciona una Opcion");
	$('#zonaVis .seleccionadoVis').text("Elije una opcion");
	$("#regionVis .seleccionadoVis").val("Selecciona una Opcion");
	$('#regionVis .seleccionadoVis').text("Elije una opcion");
	$("#sucursalVis .seleccionadoVis").val("Selecciona una Opcion");
	$('#sucursalVis .seleccionadoVis').text("Elije una opcion");
	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#valoeTerritorio").val("Selecciona una Opcion");
	$("#valorZona").val("Selecciona una Opcion");
	$("#valorRegion").val("Selecciona una Opcion");
	$("#valorSucursal").val("Selecciona una Opcion");

	var varPais = document.getElementById("valorPais").value;
	if (varPais != "Selecciona una Opcion")
		pais(varPais);

}

function pais(varPais) {
	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxPais.json?varPais='+ varPais,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			document.getElementById("territorioVis2").innerHTML = "";
			document.getElementById("zonaVis2").innerHTML = "";
			document.getElementById("regionVis2").innerHTML = "";
			document.getElementById("sucursalVis2").innerHTML = "";
			var jsonC = JSON.stringify(json);
			var i = 0;
			var margenZona=-20;
			while (json[i] != null) {
				$("#territorioVis2").append(
						"<li style='margin:"+margenZona+"px 0 0 0;' value='" + json[i].idCeco + "'><a href=''>"
								+ json[i].descCeco + "</a></li>");
				i++;
				margenZona=-40;
			}
		},
		error : function() {
			alert('Algo paso');
		}
	});
}


function cambiarTerritorio() {
	
	// Asigna el valor del li al input hidden
	$("#valorTerritorio").val($("#territorioVis .seleccionadoVis").attr("id"));

	// Vulve a inicializar el valor del span de la lista a rellenar
	$("#zonaVis .seleccionadoVis").val("Selecciona una Opcion");
	$('#zonaVis .seleccionadoVis').text("Elije una opcion");
	$("#regionVis .seleccionadoVis").val("Selecciona una Opcion");
	$('#regionVis .seleccionadoVis').text("Elije una opcion");
	$("#sucursalVis .seleccionadoVis").val("Selecciona una Opcion");
	$('#sucursalVis .seleccionadoVis').text("Elije una opcion");
	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#valorZona").val("Selecciona una Opcion");
	$("#valorRegion").val("Selecciona una Opcion");
	$("#valorSucursal").val("Selecciona una Opcion");

	var varTerri = document.getElementById("valorTerritorio").value;
	if (varTerri != "Selecciona una Opcion")
		territorio(varTerri);
}

function territorio(varTerri) {
	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxTerritorio.json?varTerri='+ varTerri,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			document.getElementById("zonaVis2").innerHTML = "";
			document.getElementById("regionVis2").innerHTML = "";
			document.getElementById("sucursalVis2").innerHTML = "";
			var jsonC = JSON.stringify(json);
			var i = 0;
			var margenZona=-20;
			while (json[i] != null) {
				$("#zonaVis2").append(
						"<li style='margin:"+margenZona+"px 0 0 0;' value='" + json[i].idCeco + "'><a href=''>"
								+ json[i].descCeco + "</a></li>");
				i++;
				margenZona=-40;
			}
		},
		error : function() {
			alert('Algo paso');
		}
	});
}




function cambiarZona() {
	// Asigna el valor del li al input hidden
	$("#valorZona").val($("#zonaVis .seleccionadoVis").attr("id"));

	// Vulve a inicializar el valor del span de la lista a rellenar
	$("#regionVis .seleccionadoVis").val("Selecciona una Opcion");
	$('#regionVis .seleccionadoVis').text("Elije una opcion");
	$("#sucursalVis .seleccionadoVis").val("Selecciona una Opcion");
	$('#sucursalVis .seleccionadoVis').text("Elije una opcion");
	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#valorRegion").val("Selecciona una Opcion");
	$("#valorSucursal").val("Selecciona una Opcion");

	var varZona = document.getElementById("valorZona").value;

	if (varZona != "Selecciona una Opcion")
		zona(varZona);

}

function zona(varZona) {
	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxZona.json?varZona=' + varZona,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			document.getElementById("regionVis2").innerHTML = "";
			document.getElementById("sucursalVis2").innerHTML = "";
			var jsonC = JSON.stringify(json);
			var i = 0;
			var margenZona=-20;
			while (json[i] != null) {
				$("#regionVis2").append(
						"<li style='margin:"+margenZona+"px 0 0 0;' value='" + json[i].idCeco + "'><a href=''>"
								+ json[i].descCeco + "</a></li>");
				i++;
				margenZona=-40;
			}
		},
		error : function() {
			alert('Algo paso');
		}
	});
}

function cambiarRegion() {

	// Asigna el valor del li al input hidden
	$("#valorRegion").val($("#regionVis .seleccionadoVis").attr("id"));

	// Vulve a inicializar el valor del span de la lista a rellenar
	$("#sucursalVis .seleccionadoVis").val("Selecciona una Opcion");
	$('#sucursalVis .seleccionadoVis').text("Elije una opcion");
	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#valorSucursal").val("Selecciona una Opcion");

	var varRegio = document.getElementById("valorRegion").value;

	if (varRegio != "Selecciona una Opcion")
		region(varRegio)

}

function region(varRegio) {
	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxRegion.json?varRegio=' + varRegio,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			document.getElementById("sucursalVis2").innerHTML = "";
			var jsonC = JSON.stringify(json);
			var i = 0;
			var margenZona=-20;
			while (json[i] != null) {
				$("#sucursalVis2").append(
						"<li style='margin:"+margenZona+"px 0 0 0;' value='" + json[i].idCeco + "'><a href=''>"
								+ json[i].descCeco + "</a></li>");
				i++;
				margenZona=-40;
			}
		},
		error : function() {
			alert('Algo paso');
		}
	});
}
