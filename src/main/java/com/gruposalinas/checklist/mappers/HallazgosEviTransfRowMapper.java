package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.HallazgosTransfDTO;

public class HallazgosEviTransfRowMapper implements RowMapper<HallazgosTransfDTO> {

	public HallazgosTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		HallazgosTransfDTO HallazgosTransfDTO = new HallazgosTransfDTO();
		
		HallazgosTransfDTO.setIdHallazgo(rs.getInt("FIID_HALLAZGO"));
	    HallazgosTransfDTO.setIdEvi(rs.getInt("FIID_EVIDENCIA"));
	    HallazgosTransfDTO.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    HallazgosTransfDTO.setRuta(rs.getString("FCRUTA"));
	    HallazgosTransfDTO.setNombre(rs.getString("FCNOMBRE"));
	    HallazgosTransfDTO.setTipoEvi(rs.getInt("FIID_TIPO"));
	    HallazgosTransfDTO.setTotEvi(rs.getInt("FIID_NUMEVI"));
	    HallazgosTransfDTO.setIdVersion(rs.getInt("IDVERSION"));
	    HallazgosTransfDTO.setStatus(rs.getInt("FCSTATUS"));
	    HallazgosTransfDTO.setPeriodo(rs.getString("FCPERIODO"));
	    HallazgosTransfDTO.setBitacora(rs.getInt("FIID_BITA"));
	
		return HallazgosTransfDTO;
	}

}


