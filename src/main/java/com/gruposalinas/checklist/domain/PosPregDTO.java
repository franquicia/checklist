package com.gruposalinas.checklist.domain;

public class PosPregDTO {
	private int idposPreg;
	private int idPosible;
	private int tipoPreg;
	public int getIdposPreg() {
		return idposPreg;
	}
	public void setIdposPreg(int idposPreg) {
		this.idposPreg = idposPreg;
	}
	public int getIdPosible() {
		return idPosible;
	}
	public void setIdPosible(int idPosible) {
		this.idPosible = idPosible;
	}
	public int getTipoPreg() {
		return tipoPreg;
	}
	public void setTipoPreg(int tipoPreg) {
		this.tipoPreg = tipoPreg;
	}
	
	

}
