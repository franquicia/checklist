package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.AsignaTransfDAO;
import com.gruposalinas.checklist.dao.FirmasCatDAO;
import com.gruposalinas.checklist.dao.HallazgosMatrizDAO;
import com.gruposalinas.checklist.domain.AdmFirmasCatDTO;
import com.gruposalinas.checklist.domain.AsignaTransfDTO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;

public class AsignaTransfBI {

	private static Logger logger = LogManager.getLogger(AsignaTransfBI.class);

private List<AsignaTransfDTO> listafila;

	@Autowired
	AsignaTransfDAO asignaTransfDAO;
	@Autowired
	FirmasCatDAO firmasCatDAO;
	@Autowired
	HallazgosMatrizDAO hallazgosMatrizDAO;
	
	public int inserta(AsignaTransfDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = asignaTransfDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	public int insertaAsignacionMtto(AsignaTransfDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = asignaTransfDAO.insertaAsignacionMtto(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	public boolean elimina(String ceco, String usuario_asig){
		boolean respuesta = false;
		
		try {
			respuesta = asignaTransfDAO.elimina(ceco,usuario_asig);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	
	public List<AsignaTransfDTO> obtieneDatos( String proyecto) {
		
		try {
			listafila =  asignaTransfDAO.obtieneDatos(proyecto);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	


	
	public boolean actualiza(AsignaTransfDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = asignaTransfDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	


public List<AsignaTransfDTO> obtieneDatosAsignaVer( String usuario) {
	
	try {
		listafila =  asignaTransfDAO.obtieneDatosAsignaVer( usuario);
	} catch (Exception e) {
		logger.info("No fue posible obtener los datos");
		UtilFRQ.printErrorLog(null, e);	
	}
	
	return listafila;			
}

public boolean CargaAsig(){
	boolean respuesta = false;
	
	try {
		respuesta = asignaTransfDAO.CargaAsig();
	} catch (Exception e) {
		logger.info("No fue posible actualizar");
		UtilFRQ.printErrorLog(null, e);	
	}
	
	return respuesta;			
}

//trae respuestas
	public String  obtieneDatosCursores(String usu) {
			JSONObject respuesta = new JSONObject();
			
		try {
			Map<String, Object> listaSucursals = null;
			listaSucursals = asignaTransfDAO.obtieneDatosCursores(usu);
			
			List<AdmFirmasCatDTO> catalogofirmas = new ArrayList<>();
			catalogofirmas = firmasCatDAO.obtieneDatosFirmaCat(null);
			
			List<AdmFirmasCatDTO> agrupaFirma = new ArrayList<>();
			agrupaFirma = firmasCatDAO.obtieneDatosFirmaAgrupa(null);
			
			
			@SuppressWarnings("unchecked")
			List<AsignaTransfDTO> listaSucursal = (List<AsignaTransfDTO>) listaSucursals.get("listaSucursal");
			@SuppressWarnings("unchecked")
			List<AsignaTransfDTO> listaProyecto = (List<AsignaTransfDTO>) listaSucursals.get("listaProyecto");
			@SuppressWarnings("unchecked")
			List<AsignaTransfDTO> listaFases = (List<AsignaTransfDTO>) listaSucursals.get("listaFases");
			@SuppressWarnings("unchecked")
			List<AsignaTransfDTO> listaGenerales = (List<AsignaTransfDTO>) listaSucursals.get("listaGenerales");
			@SuppressWarnings("unchecked")
			List<AsignaTransfDTO> listaVersiones = (List<AsignaTransfDTO>) listaSucursals.get("listaVersiones");
			@SuppressWarnings("unchecked")
			List<AsignaTransfDTO> listaConteoHallazgo = (List<AsignaTransfDTO>) listaSucursals.get("listaConteoHallazgo");
			@SuppressWarnings("unchecked")
			List<AsignaTransfDTO> listaProgramacionAsignaciones = (List<AsignaTransfDTO>) listaSucursals.get("listaProgramacionAsignaciones");
			@SuppressWarnings("unchecked")
			List<AdmFirmasCatDTO> listaFirmasCatalogo =catalogofirmas;
			@SuppressWarnings("unchecked")
			List<AdmFirmasCatDTO> listaAgrupaFirmas = agrupaFirma;

			JSONArray suc = new JSONArray();
			JSONArray proyec=new JSONArray();
			JSONArray fase = new JSONArray();
			JSONArray gene=new JSONArray();
			JSONArray version=new JSONArray();
			JSONArray catalogoFirmas=new JSONArray();
			JSONArray agrupaFirmas=new JSONArray();
			JSONArray conteoHallazgos=new JSONArray();
			JSONArray programacionAsignaciones=new JSONArray();
			//listaFirmas Catalogo
			for(int i=0; i < listaFirmasCatalogo.size();i++) {
				
				JSONObject obj =new  JSONObject();
				obj.put("idFirma", listaFirmasCatalogo.get(i).getIdFirmaCatalogo());
				obj.put("nombreFirma", listaFirmasCatalogo.get(i).getNombreFirmaCatalogo());
				obj.put("tipoProyecto", listaFirmasCatalogo.get(i).getTipoProyecto());
				
				catalogoFirmas.put(obj);
			} 
			//listaFirmas agrupador
			for(int i=0; i < listaAgrupaFirmas.size();i++) {
				
				JSONObject obj =new  JSONObject();
//id referencia de la tabla catalogo Firmas
				obj.put("idAgrupadorFirma", listaAgrupaFirmas.get(i).getIdFirmaCatalogo());
				obj.put("cargo", listaAgrupaFirmas.get(i).getCargo());
				obj.put("idObligatorio", listaAgrupaFirmas.get(i).getObligatorio());
				obj.put("orden", listaAgrupaFirmas.get(i).getOrden());
//es el id de tabla FRTAGRUPFIRMAS
				obj.put("idtabla", listaAgrupaFirmas.get(i).getIdAgrupaFirma());
				agrupaFirmas.put(obj);
			} 
			//lista sucursales
			for(int i=0; i < listaSucursal.size();i++) {
			
				JSONObject obj =new  JSONObject();
				obj.put("ceco", listaSucursal.get(i).getCeco());
				obj.put("NomCeco", listaSucursal.get(i).getNomCeco());
				obj.put("idProyecto", listaSucursal.get(i).getProyecto());
				obj.put("usuario", listaSucursal.get(i).getUsuario());
				obj.put("idAgrupador", listaSucursal.get(i).getAgrupador());
				obj.put("idStatus", listaSucursal.get(i).getIdestatus());
				obj.put("obsAsigna", listaSucursal.get(i).getObs());
				obj.put("faseActual", listaSucursal.get(i).getFaseAct());
				obj.put("negocio", listaSucursal.get(i).getNegocio());
				obj.put("zona", listaSucursal.get(i).getZona());
				obj.put("region", listaSucursal.get(i).getRegion());
				obj.put("territorio", listaSucursal.get(i).getTerritorio());
				obj.put("eco", listaSucursal.get(i).getEco());
				suc.put(obj);
			} 
			for(int i=0; i < listaProyecto.size();i++) {
				             
				JSONObject obj =new  JSONObject();
				obj.put("idProyecto", listaProyecto.get(i).getProyecto());
				obj.put("NombreProyecto", listaProyecto.get(i).getNombProy());
				obj.put("idStatus", listaProyecto.get(i).getIdestatus());
				obj.put("ObsProyecto", listaProyecto.get(i).getObsProyecto());
				obj.put("periodo", listaProyecto.get(i).getPeriodo());
				obj.put("tipoProyecto", listaProyecto.get(i).getTipoProyecto());
				obj.put("cargaInicialHallazgos", listaProyecto.get(i).getEdoCargaHallazgo());
				obj.put("statusMaximo", listaProyecto.get(i).getStatusMaximo());
				
				proyec.put(obj);		
			
			} 
			
			for(int i=0; i < listaFases.size();i++) {
				
				JSONObject obj =new  JSONObject();
				
				obj.put("idFase", listaFases.get(i).getFase());
				obj.put("idProyecto", listaFases.get(i).getProyecto());
				obj.put("idVersion", listaFases.get(i).getVersion());
				obj.put("nombreFase", listaFases.get(i).getObsFase());
				obj.put("banderaUltimaFase", listaFases.get(i).getBanderaUltFase());
				
				fase.put(obj);
			
			} 
			for(int i=0; i < listaGenerales.size();i++) {
				             
				JSONObject obj =new  JSONObject();
				
				
				obj.put("idAgrupador", listaGenerales.get(i).getAgrupador());
				obj.put("idFase", listaGenerales.get(i).getFase());
				obj.put("idOrdenFase", listaGenerales.get(i).getIdOrdenFase());
				obj.put("estatus", listaGenerales.get(i).getIdestatus());
				obj.put("observacion", listaGenerales.get(i).getObs());

				
				gene.put(obj);		
			
			} 
			for(int i=0; i < listaVersiones.size();i++) {
	             
				JSONObject obj =new  JSONObject();
				
				
				obj.put("version", listaVersiones.get(i).getVersion());
				obj.put("checklist", listaVersiones.get(i).getChecklist());

				version.put(obj);		
			
			} 
			for(int i=0; i < listaConteoHallazgo.size();i++) {
	             
				JSONObject obj =new  JSONObject();
				
				obj.put("ceco", listaConteoHallazgo.get(i).getCeco());
				obj.put("fase", listaConteoHallazgo.get(i).getFase());
				obj.put("proyecto", listaConteoHallazgo.get(i).getProyecto());
				obj.put("conteoHallazgos", listaConteoHallazgo.get(i).getConteoHallazgos());
				obj.put("nombreFase", listaConteoHallazgo.get(i).getNombFase());
				obj.put("detalleFase", listaConteoHallazgo.get(i).getObs());
				obj.put("version", listaConteoHallazgo.get(i).getVersion());

				conteoHallazgos.put(obj);		
			
			} 
			
			for(int i=0; i < listaProgramacionAsignaciones.size();i++) {
	             
				JSONObject obj =new  JSONObject();
				obj.put("idProgramacion", listaProgramacionAsignaciones.get(i).getIdProgramacion());
				obj.put("ceco", listaProgramacionAsignaciones.get(i).getCeco());
				obj.put("fase", listaProgramacionAsignaciones.get(i).getFase());
				obj.put("proyecto", listaProgramacionAsignaciones.get(i).getProyecto());
				obj.put("usuarioAsigno", listaProgramacionAsignaciones.get(i).getUsuario_asig());
				obj.put("idUsuario", listaProgramacionAsignaciones.get(i).getUsuario());
				obj.put("idAgrupador", listaProgramacionAsignaciones.get(i).getAgrupador());
				obj.put("idStatus", listaProgramacionAsignaciones.get(i).getIdestatus());
				obj.put("obsAsignacion", listaProgramacionAsignaciones.get(i).getObs());
				obj.put("faseActual", listaProgramacionAsignaciones.get(i).getFaseActual());
				obj.put("idStatus", listaProgramacionAsignaciones.get(i).getIdestatus());
				obj.put("fechaProgramacion", listaProgramacionAsignaciones.get(i).getFechaProgramacion());
				obj.put("fechaAltaTabla", listaProgramacionAsignaciones.get(i).getPeriodo());
				obj.put("nombreFase", listaProgramacionAsignaciones.get(i).getNombFase());
				obj.put("detalleFase", listaProgramacionAsignaciones.get(i).getObsFase());
				obj.put("version", listaProgramacionAsignaciones.get(i).getVersion());
				obj.put("idOrdenFase", listaProgramacionAsignaciones.get(i).getIdOrdenFase());
				obj.put("banderaFaseFin", listaProgramacionAsignaciones.get(i).getStatusMaximo());

				programacionAsignaciones.put(obj);	
			
			} 
			
			respuesta.accumulate("listaSucursal", suc);
			respuesta.accumulate("listaProyecto", proyec);
			respuesta.accumulate("listaFases", fase);
			respuesta.accumulate("listaAgrupador", gene);
			respuesta.accumulate("listaVersiones", version);
			respuesta.accumulate("listacatalogoFirmas", catalogoFirmas);
			respuesta.accumulate("listaAgrupaFirmas", agrupaFirmas);
			respuesta.accumulate("listaConteoHallazgo", conteoHallazgos);
			respuesta.accumulate("listaProgramacionAsignaciones", programacionAsignaciones);

			
	    	
		} catch (Exception e) {
			e.printStackTrace();
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta.toString();
	}
	
	//trae respuestas
		public String  obtieneDatosCursoresCeco(String ceco) {
				JSONObject respuesta = new JSONObject();
				
			try {
				Map<String, Object> listaSucursals = null;
				listaSucursals = asignaTransfDAO.obtieneDatosCursoresCeco(ceco);
				
				
				@SuppressWarnings("unchecked")
				List<AsignaTransfDTO> listaProyecto = (List<AsignaTransfDTO>) listaSucursals.get("listaProyecto");
				@SuppressWarnings("unchecked")
				List<AsignaTransfDTO> listaFases = (List<AsignaTransfDTO>) listaSucursals.get("listaFases");
				@SuppressWarnings("unchecked")
				List<AsignaTransfDTO> listaAgrupador = (List<AsignaTransfDTO>) listaSucursals.get("listaAgrupador");
				@SuppressWarnings("unchecked")
				List<AsignaTransfDTO> listaConteoHallazgo = (List<AsignaTransfDTO>) listaSucursals.get("listaConteoHallazgo");
				
				List<HallazgosTransfDTO> listaMatriz = new ArrayList<>();
				
				
				JSONArray proyec=new JSONArray();
				JSONArray fase = new JSONArray();
				JSONArray agrupador=new JSONArray();
				JSONArray matrizHallazgo=new JSONArray();
				JSONArray conteoHallazgos=new JSONArray();
				
				for(int i=0; i < listaProyecto.size();i++) {
					             
					JSONObject obj =new  JSONObject();
					obj.put("idProyecto", listaProyecto.get(i).getProyecto());
					obj.put("NombreProyecto", listaProyecto.get(i).getNombProy());
					obj.put("idStatus", listaProyecto.get(i).getIdestatus());
					obj.put("ObsProyecto", listaProyecto.get(i).getObsProyecto());
					obj.put("periodo", listaProyecto.get(i).getPeriodo());
					obj.put("tipoProyecto", listaProyecto.get(i).getTipoProyecto());
					obj.put("cargaInicialHallazgos", listaProyecto.get(i).getEdoCargaHallazgo());
					obj.put("statusMaximo", listaProyecto.get(i).getStatusMaximo());
					proyec.put(obj);		
				
				} 
				
				for(int i=0; i < listaFases.size();i++) {
					
					JSONObject obj =new  JSONObject();
					
					obj.put("idFase", listaFases.get(i).getFase());
					obj.put("idProyecto", listaFases.get(i).getProyecto());
					obj.put("idVersion", listaFases.get(i).getVersion());
					obj.put("nombreFase", listaFases.get(i).getObsFase());
					obj.put("banderaUltimaFase", listaFases.get(i).getBanderaUltFase());
					
					fase.put(obj);
				
				} 
				for(int i=0; i < listaAgrupador.size();i++) {
					             
					JSONObject obj =new  JSONObject();
					
					
					obj.put("idAgrupador", listaAgrupador.get(i).getAgrupador());
					obj.put("idFase", listaAgrupador.get(i).getFase());
					obj.put("idOrdenFase", listaAgrupador.get(i).getIdOrdenFase());
					obj.put("estatus", listaAgrupador.get(i).getIdestatus());
					obj.put("observacion", listaAgrupador.get(i).getObs());

					
					agrupador.put(obj);		
				
				} 
				
				
				try {
					
				  listaMatriz = hallazgosMatrizDAO.obtieneHallazgoMatriz(null);
				  
				} catch (Exception e) {
					logger.info("AP con la matriz " + e);
					
				}

				
				for(int i=0; i < listaMatriz.size();i++) {
		             
					JSONObject obj =new  JSONObject();
					
					
					obj.put("idMatriz", listaMatriz.get(i).getIdMatriz());
					obj.put("tipoProyecto", listaMatriz.get(i).getTipoProy());
					obj.put("statusEnvio", listaMatriz.get(i).getStatusEnvio());
					obj.put("statusHallazgo", listaMatriz.get(i).getStatusHallazgo());
					obj.put("idPerfil", listaMatriz.get(i).getIdPerfil());
					obj.put("color", listaMatriz.get(i).getColor());
					obj.put("nombreStatus", listaMatriz.get(i).getNombreStatus());

					matrizHallazgo.put(obj);		
				
				} 
				for(int i=0; i < listaConteoHallazgo.size();i++) {
		             
					JSONObject obj =new  JSONObject();
					
					obj.put("ceco", listaConteoHallazgo.get(i).getCeco());
					obj.put("fase", listaConteoHallazgo.get(i).getFase());
					obj.put("proyecto", listaConteoHallazgo.get(i).getProyecto());
					obj.put("conteoHallazgos", listaConteoHallazgo.get(i).getConteoHallazgos());
					obj.put("nombreFase", listaConteoHallazgo.get(i).getNombFase());
					obj.put("detalleFase", listaConteoHallazgo.get(i).getObs());
					obj.put("version", listaConteoHallazgo.get(i).getVersion());

					conteoHallazgos.put(obj);		
				
				} 

				respuesta.accumulate("listaProyecto", proyec);
				respuesta.accumulate("listaFases", fase);
				respuesta.accumulate("listaAgrupador", agrupador);
				respuesta.accumulate("listaMatriz", matrizHallazgo);
				respuesta.accumulate("listaConteoHallazgo", conteoHallazgos);
		
		    	
			} catch (Exception e) {
				logger.info("No fue posible obtener los datos");
				UtilFRQ.printErrorLog(null, e);
			}

			return respuesta.toString();
		}
		
		public boolean actualizaN(AsignaTransfDTO bean){
			boolean respuesta = false;
			
			try {
				respuesta = asignaTransfDAO.actualizaN(bean);
			} catch (Exception e) {
				logger.info("No fue posible actualizar");
				UtilFRQ.printErrorLog(null, e);	
			}
			
			return respuesta;			
		}

		

		
}