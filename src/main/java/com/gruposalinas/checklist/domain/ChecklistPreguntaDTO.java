package com.gruposalinas.checklist.domain;

import java.math.BigDecimal;

public class ChecklistPreguntaDTO {

    private Integer idChecklist;
    private Integer idPregunta;
    private String pregunta;
    private Integer idTipoPregunta;
    private String descTipo;
    private Integer idModulo;
    private String nombreModulo;
    private Integer ordenPregunta;
    private Integer commit;
    private Integer pregPadre;
    private String numVersion;
    private String tipoCambio;
    private Integer activo;
    private String detalle;
    private Integer critica;
    private int serie;
    private Integer sla;
    private Integer codigo;
    private String responsable;
    private Integer idZona;
    private String responsableN;
    private String negocio;

    public Integer getIdChecklist() {
        return idChecklist;
    }

    public void setIdChecklist(BigDecimal idChecklist) {
        this.idChecklist = (idChecklist == null ? null : idChecklist.intValue());
    }

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(BigDecimal idPregunta) {
        this.idPregunta = (idPregunta == null ? null : idPregunta.intValue());
    }

    public Integer getOrdenPregunta() {
        return ordenPregunta;
    }

    public void setOrdenPregunta(BigDecimal ordenPregunta) {
        this.ordenPregunta = (ordenPregunta == null ? null : ordenPregunta.intValue());
    }

    public Integer getCommit() {
        return commit;
    }

    public void setCommit(BigDecimal commit) {
        this.commit = (commit == null ? null : commit.intValue());
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public Integer getIdTipoPregunta() {
        return idTipoPregunta;
    }

    public void setIdTipoPregunta(BigDecimal idTipoPregunta) {
        this.idTipoPregunta = (idTipoPregunta == null ? null : idTipoPregunta.intValue());
    }

    public String getDescTipo() {
        return descTipo;
    }

    public void setDescTipo(String descTipo) {
        this.descTipo = descTipo;
    }

    public Integer getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(BigDecimal idModulo) {
        this.idModulo = (idModulo == null ? null : idModulo.intValue());
    }

    public String getNombreModulo() {
        return nombreModulo;
    }

    public void setNombreModulo(String nombreModulo) {
        this.nombreModulo = nombreModulo;
    }

    public Integer getPregPadre() {
        return pregPadre;
    }

    public void setPregPadre(BigDecimal pregPadre) {
        this.pregPadre = (pregPadre == null ? null : pregPadre.intValue());
    }

    public String getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(String numVersion) {
        this.numVersion = numVersion;
    }

    public String getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(String tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(BigDecimal activo) {
        this.activo = (activo == null ? null : activo.intValue());
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public Integer getCritica() {
        return critica;
    }

    public void setCritica(BigDecimal critica) {
        this.critica = (critica == null ? null : critica.intValue());
    }

    public Integer getSla() {
        return sla;
    }

    public void setSla(BigDecimal sla) {
        this.sla = (sla == null ? null : sla.intValue());
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(BigDecimal codigo) {
        this.codigo = (codigo == null ? null : codigo.intValue());
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public void setIdChecklist(Integer idChecklist) {
        this.idChecklist = idChecklist;
    }

    public void setIdPregunta(Integer idPregunta) {
        this.idPregunta = idPregunta;
    }

    public void setIdTipoPregunta(Integer idTipoPregunta) {
        this.idTipoPregunta = idTipoPregunta;
    }

    public void setIdModulo(Integer idModulo) {
        this.idModulo = idModulo;
    }

    public void setOrdenPregunta(Integer ordenPregunta) {
        this.ordenPregunta = ordenPregunta;
    }

    public void setCommit(Integer commit) {
        this.commit = commit;
    }

    public void setPregPadre(Integer pregPadre) {
        this.pregPadre = pregPadre;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

    public void setCritica(Integer critica) {
        this.critica = critica;
    }

    public int getSerie() {
        return serie;
    }

    public void setSerie(int serie) {
        this.serie = serie;
    }

    public void setSla(Integer sla) {
        this.sla = sla;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getIdZona() {
        return idZona;
    }

    public void setIdZona(int idZona) {
        this.idZona = idZona;
    }

    public String getResponsableN() {
        return responsableN;
    }

    public void setResponsableN(String responsableN) {
        this.responsableN = responsableN;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    @Override
    public String toString() {
        return "ChecklistPreguntaDTO{" + "idChecklist=" + idChecklist + ", idPregunta=" + idPregunta + ", pregunta=" + pregunta + ", idTipoPregunta=" + idTipoPregunta + ", descTipo=" + descTipo + ", idModulo=" + idModulo + ", nombreModulo=" + nombreModulo + ", ordenPregunta=" + ordenPregunta + ", commit=" + commit + ", pregPadre=" + pregPadre + ", numVersion=" + numVersion + ", tipoCambio=" + tipoCambio + ", activo=" + activo + ", detalle=" + detalle + ", critica=" + critica + ", serie=" + serie + ", sla=" + sla + ", codigo=" + codigo + ", responsable=" + responsable + ", idZona=" + idZona + ", responsableN=" + responsableN + ", negocio=" + negocio + '}';
    }

}
