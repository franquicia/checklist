package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.AppPerfilDTO;


public interface AppDAO {
	
	public List<AppPerfilDTO> buscaApp(String idApp) throws Exception;
	
	public int insertaApp(AppPerfilDTO bean) throws Exception;
	
	public boolean actualizaApp(AppPerfilDTO bean) throws Exception;
	
	public boolean eliminaApp(int idApp) throws Exception;
	
	public List<AppPerfilDTO> buscaAppPerfil(String idApp, String idUsuario) throws Exception;
	
	public int insertaAppPerfil(AppPerfilDTO bean) throws Exception;
	
	public boolean actualizaAppPerfil(AppPerfilDTO bean) throws Exception;
	
	public boolean eliminaAppPerfil(int idAppPerfil) throws Exception;

}
