package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ZonaNegoExpDTO;

public class ZonaNegoDetaRowMapper implements RowMapper<ZonaNegoExpDTO> {

	public ZonaNegoExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ZonaNegoExpDTO ne = new ZonaNegoExpDTO();
		 ne.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
		ne.setIdTabNegocio(rs.getInt("ID_NEGOCIO"));
	    //nomenclatura nego
	    ne.setNegocio(rs.getString("NEGO"));
	    //DESCRIPCION
	    ne.setDescNego(rs.getString("DESCNEGOCIO"));
	    //nomenclatura zona
	    ne.setClasifica(rs.getString("CLASIFICA"));
	    ne.setDescZona(rs.getString("DESCZONA"));
	    ne.setIdActivo(rs.getInt("FIACTIVO"));
	    ne.setObs(rs.getString("FCOBSERV"));
		ne.setIdTabNegocio(rs.getInt("ID_NEGOCIO"));
		ne.setIdTabZona(rs.getInt("ID_NEGZONA"));
		ne.setIdZona(rs.getInt("FIID_ZONA"));
	    
	  
		return ne;
	}

}


