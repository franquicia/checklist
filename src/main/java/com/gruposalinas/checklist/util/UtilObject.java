package com.gruposalinas.checklist.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.lang3.ObjectUtils;
import org.dozer.Mapper;

import com.gruposalinas.checklist.resources.FRQAppContextProvider;

public class UtilObject {
	/*  ++++PL/SQL decode++++ en Java
     * 
     * Ejemplos
     * 
     * Decode.decode(valorParaComparar, siEsValorA, devuelveValorB, siEsValorC, devuelveValorD );  //aqui devuelve un null cuando no hay coincidencia
     * Decode.decode(valorParaComparar, siEsValorA, devuelveValorB, siNoFueNingunoDevuelveDefault); //devuelve default cuando no hay coincidencia
     * NOTA: se permiten n parametros de entrada
     * 
     * PRECACUCIÓN MANEJESE CON CUIDADO
     * Este método hace una parseo de valores primitivos a Objetos
     * int a = 1 es igual a long b = 1, esto es verdadero cuando se trata de objetos primitivos
     * cuando se convierten a objetos
     * Integer a = new Integer(1) NO ES IGUAL a Long b = new Long(1), 
     * Este método hace una parseo de valores primitivos a Objetos //si esta repetido para que lo vuelvas a leer
     * 
     * Ejemplo 
     * INCORRECTO
     * long a = 1;
     * Decode.decode(a, 1, "Soy uno", 2, "Soy dos", "no fui ninguno anterior")
     * el ejemplo anterior en tiempo de ejecución nos devolvera "no fui ninguno anterior" debido a los tipos de datos
     * 
     * CORRECTO
     * long a = 1;
     * Decode.decode(a, 1L, "Soy uno", 2L, "Soy dos", "no fui ninguno anterior") //estamos indicando que 
     * 
     * int a = 1;
     * Decode.decode(a, 1, "Soy uno", 2, "Soy dos", "no fui ninguno anterior")
     * Los dos ejemplos anteriores devuelven "Soy uno"
     * 
     * Para cualquier duda con lo anterior favor de remitirse al javadoc
     * 
     * */
    public static Object decode(Object... args) {
        Object pivot = args[0];
        int max = args.length -1;
        int i = 1;
        for( ; i < max ; i += 2 ) {
            if(ObjectUtils.equals( pivot, args[i] )) {
                return args[i + 1];
            }
        }
        return i == max ? args[i] : null;
    }
    
    public static Object mapObject(Object origen, Object destino){
    	org.dozer.Mapper mapper = (Mapper) FRQAppContextProvider.getApplicationContext().getBean("Mapper");
    	return mapper.map(origen, destino.getClass());
    }
    
    public static String inputStreamAsString(InputStream stream) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }
}
