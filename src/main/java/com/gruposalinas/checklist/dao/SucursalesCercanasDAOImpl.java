package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.NegociosDentroDTO;
import com.gruposalinas.checklist.domain.PuntosCercanosDTO;
import com.gruposalinas.checklist.domain.SucursalesCercanasDTO;
import com.gruposalinas.checklist.mappers.NegociosDentroRowMapper;
import com.gruposalinas.checklist.mappers.PuntosCercanosRowMapper;
import com.gruposalinas.checklist.mappers.SucursalesCercanasRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class SucursalesCercanasDAOImpl extends DefaultDAO implements SucursalesCercanasDAO {

    private Logger logger = LogManager.getLogger(SucursalesCercanasDAOImpl.class);

    private DefaultJdbcCall jdbcObtienSucursales;
    private DefaultJdbcCall jdbcObtienPuntosCercanas;
    private DefaultJdbcCall jdbcObtienNegocioDentroSucursal;

    public void init() {

        jdbcObtienSucursales = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASUCCERCANAS")
                .withProcedureName("SP_OBTIENE_SUC")
                .returningResultSet("RCL_CONSULTA", new SucursalesCercanasRowMapper());

        jdbcObtienPuntosCercanas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPUNTOSCERCANOS")
                .withProcedureName("SP_GET_PUNTOS")
                .returningResultSet("RCL_CONSULTA", new PuntosCercanosRowMapper());

        jdbcObtienNegocioDentroSucursal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPUNTOSCERCANOS")
                .withProcedureName("SP_GET_NEG_IN")
                .returningResultSet("RCL_CONSULTA", new NegociosDentroRowMapper());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<SucursalesCercanasDTO> obtieneSucursales(String latitud, String longitud) throws Exception {

        List<SucursalesCercanasDTO> lista = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_LATITUD", latitud).addValue("PA_LONGITUD", longitud);

        out = jdbcObtienSucursales.execute(in);

        logger.info("Funcion ejecutada: {checklist.PASUCCERCANAS.SP_OBTIENE_SUC}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener las sucursales cercanas");
        } else {
            lista = (List<SucursalesCercanasDTO>) out.get("RCL_CONSULTA");
        }

        return lista;
    }

    @Override
    public List<PuntosCercanosDTO> obtieneSucursalesCercanas(String latitud, String longitud) throws Exception {

        List<PuntosCercanosDTO> lista = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_LATITUD", latitud).addValue("PA_LONGITUD", longitud);

        out = jdbcObtienPuntosCercanas.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAPUNTOSCERCANOS.SP_GET_PUNTOS}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener las sucursales cercanas");
        } else {
            lista = (List<PuntosCercanosDTO>) out.get("RCL_CONSULTA");
        }

        return lista;
    }

    @Override
    public List<NegociosDentroDTO> obtieneNegociosDentro(String economico) throws Exception {

        List<NegociosDentroDTO> lista = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ECONOMICO", economico);

        out = jdbcObtienNegocioDentroSucursal.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAPUNTOSCERCANOS.SP_GET_NEG_IN}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener las sucursales cercanas");
        } else {
            lista = (List<NegociosDentroDTO>) out.get("RCL_CONSULTA");
        }

        return lista;
    }

}
