package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.RespuestaDTO;

public class RespuestaRowMapper implements RowMapper<RespuestaDTO> {

	public RespuestaDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		RespuestaDTO respuestaDTO = new RespuestaDTO();
		
		respuestaDTO.setIdRespuesta(rs.getInt("FIID_RESPUESTA"));
		respuestaDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		respuestaDTO.setIdArboldecision(rs.getInt("FIID_ARBOL_DES"));
		respuestaDTO.setObservacion(rs.getString("FCOBSERVACIONES"));
		return respuestaDTO;
	}
}
