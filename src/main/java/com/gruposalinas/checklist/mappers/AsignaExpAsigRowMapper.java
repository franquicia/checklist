package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.AsignaExpDTO;

public class AsignaExpAsigRowMapper implements RowMapper<AsignaExpDTO> {

	public AsignaExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AsignaExpDTO AsignaExpDTO = new AsignaExpDTO();
		
		AsignaExpDTO.setCeco(rs.getInt("FCID_CECO"));
	    AsignaExpDTO.setVersion(rs.getInt("FIVERSION"));
	    AsignaExpDTO.setUsuario(rs.getInt("FCUSUASIG"));
	    AsignaExpDTO.setAux(rs.getInt("FIID_CHECKLIST"));
	    AsignaExpDTO.setPeriodo(rs.getString("FCPERIODO"));
	    
	    
		return AsignaExpDTO;
	}

}


