package com.gruposalinas.checklist.domain;

public class Admin2SupDTO {
	private String socio;
	private int numSocio;
	private String puesto;
	private String ceco;
	private String sucursal;
	private String zona;
	
	public String getSocio() {
		return socio;
	}
	public void setSocio(String socio) {
		this.socio = socio;
	}
	public int getNumSocio() {
		return numSocio;
	}
	public void setNumSocio(int numSocio) {
		this.numSocio = numSocio;
	}
	public String getPuesto() {
		return puesto;
	}
	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}
	public String getCeco() {
		return ceco;
	}
	public void setCeco(String ceco) {
		this.ceco = ceco;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	
	@Override
	public String toString() {
		return "Admin2SupDTO [socio=" + socio + ", numSocio=" + numSocio + ", puesto=" + puesto + ", ceco=" + ceco
				+ ", sucursal=" + sucursal + ", zona=" + zona + "]";
	}
	
}


