package com.gruposalinas.checklist.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.FileNotFoundException;

import com.gruposalinas.checklist.business.CargaBI;
import com.gruposalinas.checklist.domain.UploadArchive;
import com.gruposalinas.checklist.util.CleanParameter;
import com.gruposalinas.checklist.util.UtilFRQ;

@Controller

public class CargaController {

	@Autowired
	ServletContext context; 
	@Autowired
	private CargaBI cargaBI;
	@Autowired
	private CleanParameter UtilString;

	// CONTROLLER A MI FORM
	@RequestMapping(value = "/cargaForm.htm", method = RequestMethod.GET)
	public ModelAndView login(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {
		return new ModelAndView("cargaForm", "command", new UploadArchive());
	}

	// CONTROLLER ACTION DE MI FORM
	@RequestMapping(value = "archiveLoad.htm", method = RequestMethod.POST)
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "titulo", required = true, defaultValue = "") String titulo,
			@RequestParam(value = "descripcion", required = true, defaultValue = "") String descripcion,
			@RequestParam(value = "archive", required = true) MultipartFile file) throws IOException {
		UtilString.cleanParameter(titulo);
		UtilString.cleanParameter(descripcion);
		return null;
				//cargaBI.onSubmit(request, response, titulo, descripcion, file);
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleError(HttpServletRequest request, Exception ex) {
		ModelAndView mv = new ModelAndView("exception");
		return mv;
	}

	/* CONTROLLER PARA LA CARGA DE ARCHIVO EXCEL */
	@RequestMapping(value = "central/cargaExcelForm.htm", method = RequestMethod.GET)
	public ModelAndView cargaExcelForm(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {
		return new ModelAndView("cargaExcelForm", "command", new UploadArchive());
	}

	// CONTROLLER ACTION DE MI FORM
	@SuppressWarnings({ "unused", "resource" })
	@RequestMapping(value = "central/archiveExcelLoad.htm", method = RequestMethod.POST)
	public ModelAndView onSubmitExcel(HttpServletRequest request, HttpServletResponse response,
				@RequestParam(value = "titulo", required = true, defaultValue = "") String titulo,
				@RequestParam(value = "descripcion", required = true, defaultValue = "") String descripcion,
				@RequestParam(value = "archive", required = true) MultipartFile file) throws IOException, InvocationTargetException {
				UtilString.cleanParameter(titulo);
				UtilString.cleanParameter(descripcion);
				////System.out.println("TITULO --> " + titulo + "  DESCRIPCION -->" + descripcion + " FILE -->" + file.getOriginalFilename());
				ModelAndView mv = new ModelAndView("cargaExcelResponse", "command", new UploadArchive());
				File tmp = File.createTempFile(file.getOriginalFilename(), ".xlsx");
				//file.transferTo(tmp);
				File f = null;
				FileInputStream excelFile=null;
				//LEER FICHERO EXCEL
				try {
			        excelFile = new FileInputStream(tmp);
			      //Get the workbook instance for XLS file 
			        String[] fileName = file.getOriginalFilename().split("\\.");
			       if (fileName[1].equals("xls")){
			    	   ////System.out.println("ENTRO A XLS");
			    	  
			       }
			       else if (fileName[1].equals("xlsx")){
			    	   ////System.out.println("ENTRO A XLSX");
			    	   /*CARGAR COMO RECURSO NO COMO URL*/
			    	   String uploadPath = context.getRealPath("/files/2007.xlsx");
			    	   File f1 = new File(uploadPath);
				       f = f1;
				       file.transferTo(f);
			    	   //TENGO MI ARCHIVO TEMPORAL YA MI EXCEL
			    	   //tmp.delete();
			       }
				}catch (FileNotFoundException e) {
			        e.printStackTrace();
			    } catch (IOException e) {
			        e.printStackTrace();
			    }
				finally {
					    if (excelFile != null) {
					      safeClose(excelFile);
					    }
				}
				//FIN LEER FICHERO EXCEL
				if(f!=null){
					mv.addObject("archive", f.getPath());
				}
				return mv;
		}
	public static void safeClose(FileInputStream fis) {
		  if (fis != null) {
		    try {
		      fis.close();
		    } catch (IOException e) {
		    	e.printStackTrace();
		    }
		  }
		}
}

