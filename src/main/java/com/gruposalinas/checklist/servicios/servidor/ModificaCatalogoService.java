package com.gruposalinas.checklist.servicios.servidor;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.ActorEdoHallaBI;
import com.gruposalinas.checklist.business.AdicionalesBI;
import com.gruposalinas.checklist.business.AdmHandbookBI;
import com.gruposalinas.checklist.business.AdmInformProtocolosBI;
import com.gruposalinas.checklist.business.AltaGerenteBI;
import com.gruposalinas.checklist.business.AsignaExpBI;
import com.gruposalinas.checklist.business.AsignaTransfBI;
import com.gruposalinas.checklist.business.AsignacionBI;
import com.gruposalinas.checklist.business.BitacoraAdministradorBI;
import com.gruposalinas.checklist.business.BitacoraGralBI;
import com.gruposalinas.checklist.business.CanalBI;
import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.business.CecoGuadalajaraBI;
import com.gruposalinas.checklist.business.CheckInAsistenciaBI;
import com.gruposalinas.checklist.business.CheckinBI;
import com.gruposalinas.checklist.business.ChecklistBI;
import com.gruposalinas.checklist.business.ComentariosRepAsgBI;
import com.gruposalinas.checklist.business.ConfiguraActorBI;
import com.gruposalinas.checklist.business.ConsultaFechasBI;
import com.gruposalinas.checklist.business.DatosInformeBI;
import com.gruposalinas.checklist.business.EmpContacExtBI;
import com.gruposalinas.checklist.business.HorarioBI;
import com.gruposalinas.checklist.business.NegocioBI;
import com.gruposalinas.checklist.business.NivelBI;
import com.gruposalinas.checklist.business.PaisBI;
import com.gruposalinas.checklist.business.ParametroBI;
import com.gruposalinas.checklist.business.PerfilBI;
import com.gruposalinas.checklist.business.PosiblesBI;
import com.gruposalinas.checklist.business.ProtocoloBI;
import com.gruposalinas.checklist.business.ProyectFaseTransfBI;
import com.gruposalinas.checklist.business.PuestoBI;
import com.gruposalinas.checklist.business.RecursoBI;
import com.gruposalinas.checklist.business.RecursoPerfilBI;
import com.gruposalinas.checklist.business.SoftNvoBI;
import com.gruposalinas.checklist.business.SoftOpeningBI;
import com.gruposalinas.checklist.business.SoporteExpBI;
import com.gruposalinas.checklist.business.SoporteHallazgosTransfBI;
import com.gruposalinas.checklist.business.SucursalBI;
import com.gruposalinas.checklist.business.SucursalChecklistBI;
import com.gruposalinas.checklist.business.TareasBI;
import com.gruposalinas.checklist.business.TipoArchivoBI;
import com.gruposalinas.checklist.business.TransformacionBI;
import com.gruposalinas.checklist.business.Usuario_ABI;
import com.gruposalinas.checklist.business.VersionChecklistGralBI;
import com.gruposalinas.checklist.business.VersionExpanBI;
import com.gruposalinas.checklist.business.ZonaNegoBI;
import com.gruposalinas.checklist.domain.ActorEdoHallaDTO;
import com.gruposalinas.checklist.domain.AdicionalesDTO;
import com.gruposalinas.checklist.domain.AdmFirmasCatDTO;
import com.gruposalinas.checklist.domain.AdmHandbookDTO;
import com.gruposalinas.checklist.domain.AdmInformProtocolosDTO;
import com.gruposalinas.checklist.domain.AltaGerenteDTO;
import com.gruposalinas.checklist.domain.AsignaExpDTO;
import com.gruposalinas.checklist.domain.AsignaTransfDTO;
import com.gruposalinas.checklist.domain.AsignacionDTO;
import com.gruposalinas.checklist.domain.BitacoraAdministradorDTO;
import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.BitacoraGralDTO;
import com.gruposalinas.checklist.domain.CanalDTO;
import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.CheckInAsistenciaDTO;
import com.gruposalinas.checklist.domain.CheckinDTO;
import com.gruposalinas.checklist.domain.ConsultaFechasDTO;
import com.gruposalinas.checklist.domain.DatosInformeDTO;
import com.gruposalinas.checklist.domain.EmpContacExtDTO;
import com.gruposalinas.checklist.domain.HorarioDTO;
import com.gruposalinas.checklist.domain.NegocioDTO;
import com.gruposalinas.checklist.domain.NivelDTO;
import com.gruposalinas.checklist.domain.PaisDTO;
import com.gruposalinas.checklist.domain.ParametroDTO;
import com.gruposalinas.checklist.domain.PerfilDTO;
import com.gruposalinas.checklist.domain.PosiblesDTO;
import com.gruposalinas.checklist.domain.ProgramacionMttoDTO;
import com.gruposalinas.checklist.domain.ProtocoloDTO;
import com.gruposalinas.checklist.domain.ProyectFaseTransfDTO;
import com.gruposalinas.checklist.domain.PuestoDTO;
import com.gruposalinas.checklist.domain.RecursoDTO;
import com.gruposalinas.checklist.domain.RecursoPerfilDTO;
import com.gruposalinas.checklist.domain.SoftNvoDTO;
import com.gruposalinas.checklist.domain.SoftOpeningDTO;
import com.gruposalinas.checklist.domain.SoporteExpDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;
import com.gruposalinas.checklist.domain.SucursalDTO;
import com.gruposalinas.checklist.domain.TareaDTO;
import com.gruposalinas.checklist.domain.TipoArchivoDTO;
import com.gruposalinas.checklist.domain.TransformacionDTO;
import com.gruposalinas.checklist.domain.Usuario_ADTO;
import com.gruposalinas.checklist.domain.VersionChecklistGralDTO;
import com.gruposalinas.checklist.domain.VersionExpanDTO;
import com.gruposalinas.checklist.domain.ZonaNegoExpDTO;
import com.gruposalinas.checklist.resources.FRQAuthInterceptor;
import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.EstadosDTO;
import com.gruposalinas.checklist.domain.FaseTransfDTO;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;
import com.gruposalinas.checklist.domain.FragmentMenuDTO;
import com.gruposalinas.checklist.domain.HallazgosEviExpDTO;
import com.gruposalinas.checklist.domain.HallazgosExpDTO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;
import com.gruposalinas.checklist.business.EmpFijoBI;
import com.gruposalinas.checklist.business.EstadosBI;
import com.gruposalinas.checklist.business.FaseTransfBI;
import com.gruposalinas.checklist.business.FirmaCheckBI;
import com.gruposalinas.checklist.business.FirmasCatalogoBI;
import com.gruposalinas.checklist.business.FragmentMenuBI;
import com.gruposalinas.checklist.business.HallagosMatrizBI;
import com.gruposalinas.checklist.business.HallazgosEviExpBI;
import com.gruposalinas.checklist.business.HallazgosExpBI;
import com.gruposalinas.checklist.business.HallazgosTransfBI;
import com.gruposalinas.checklist.domain.ComentariosRepAsgDTO;
import com.gruposalinas.checklist.domain.ConfiguraActorDTO;

import java.util.List;
import java.util.Map;

import org.springframework.ui.Model;

@Controller
@RequestMapping("/catalogosService")
public class ModificaCatalogoService {

	@Autowired
	CanalBI canalbi;
	@Autowired
	PerfilBI perfilbi;
	@Autowired
	PuestoBI puestobi;
	@Autowired
	TipoArchivoBI tipoarchivobi;
	@Autowired
	CecoBI cecobi;
	@Autowired
	SucursalBI sucursalbi;
	@Autowired
	Usuario_ABI usuarioabi;
	@Autowired
	PaisBI paisbi;
	@Autowired
	HorarioBI horariobi;
	@Autowired
	ParametroBI parametrobi;
	@Autowired
	NegocioBI negociobi;
	@Autowired
	NivelBI nivelbi;
	@Autowired
	RecursoBI recursobi;
	@Autowired
	RecursoPerfilBI recursoperfilbi;
	@Autowired
	AsignacionBI asignacionBI;
	@Autowired
	TareasBI tareasBI;
	@Autowired
	CecoGuadalajaraBI cecoGuadalajarabi;
	@Autowired
	EmpFijoBI empFijoBI;
	@Autowired
	EstadosBI estadosBI;
	@Autowired
	BitacoraGralBI  bitacoraGralBI;
	@Autowired
	FirmaCheckBI	firmaCheckBI;
	@Autowired
	SucursalChecklistBI sucursalChecklistBI;
	@Autowired
	SoftOpeningBI softOpeningBI;
	@Autowired
	CheckinBI checkinBI;
	@Autowired
	AsignaExpBI 	asignaExpBI;
	@Autowired
	VersionExpanBI versionExpanBI;
	@Autowired
	VersionChecklistGralBI versionChecklistGralBI;
	@Autowired
	SoporteExpBI soporteExpBI;
	@Autowired
	EmpContacExtBI empContacExtBI;
	@Autowired
	ConsultaFechasBI consultaFechasBI;
	@Autowired
	HallazgosExpBI hallazgosExpBI;
	@Autowired	
	HallazgosEviExpBI hallazgosEviExpBI;
	@Autowired
	BitacoraAdministradorBI  bitacoraAdministradorBI;
	@Autowired
	ChecklistBI checklistBI;
	@Autowired
	PosiblesBI posiblesbi;
	@Autowired
	AdmHandbookBI handBookBI;
	@Autowired
	ProtocoloBI protocoloBI;
	@Autowired
	AltaGerenteBI altaGerenteBI;
	private static final Logger logger = LogManager.getLogger(ModificaCatalogoService.class);
	@Autowired
	ZonaNegoBI zonaNegoBI;
	@Autowired
	AdicionalesBI adicionalesBI;
	@Autowired
	HallazgosTransfBI hallazgosTransfBI;
	@Autowired
	TransformacionBI transformacionBI;
	@Autowired
	FaseTransfBI faseTransfBI; 
	@Autowired
	AdmInformProtocolosBI admInformProtocolosBI;
	@Autowired
	ProyectFaseTransfBI proyectFaseTransfBI;
	@Autowired
	AsignaTransfBI asignaTransfBI;  
     @Autowired
    ComentariosRepAsgBI comentariosRepAsgBI;
 	@Autowired
 	FirmasCatalogoBI firmasCatalogoBI;
 	@Autowired
 	FragmentMenuBI fragmentMenuBI;
	@Autowired
	SoftNvoBI softNvoBI;
	@Autowired
	ActorEdoHallaBI actorEdoHallaBI;
	@Autowired
	ConfiguraActorBI configuraActorBI;
	@Autowired
	HallagosMatrizBI hallagosMatrizBI;
	@Autowired
	SoporteHallazgosTransfBI soporteHallazgosTransfBI;
	@Autowired
	CheckInAsistenciaBI checkInAsistenciaBI;
	@Autowired
	DatosInformeBI datosInformeBI;
	
	// http://localhost:8080/checklist/catalogosService/updateCanal.json?idCanal=<?>&setActivo=<?>&descripcion=<?>
	@RequestMapping(value = "/updateCanal", method = RequestMethod.GET)
	public ModelAndView altaCanal(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String activo = request.getParameter("setActivo");
		String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
		String idCanal = request.getParameter("idCanal");

		CanalDTO canal = new CanalDTO();
		canal.setActivo(Integer.parseInt(activo));
		canal.setDescrpicion(descripcion);
		canal.setIdCanal(Integer.parseInt(idCanal));
		boolean res = canalbi.actualizaCanal(canal);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "CANAL MODIFICADO CORRECTAMENTE");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosService/updatePerfil.json?idPerfil=<?>&setDescripcion=<?>
	@RequestMapping(value = "/updatePerfil", method = RequestMethod.GET)
	public ModelAndView updatePerfil(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String idP = request.getParameter("idPerfil");
		String descripcion = new String(request.getParameter("setDescripcion").getBytes("ISO-8859-1"), "UTF-8");

		PerfilDTO perfil = new PerfilDTO();
		perfil.setDescripcion(descripcion);
		perfil.setIdPerfil(Integer.parseInt(idP));
		boolean res = perfilbi.actualizaPerfil(perfil);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "ID DE PERFIL ACTUALIZADO CORRECTAMENTE");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosService/updatePuesto.json?idPuesto=<?>&setActivo=<?>&setCodigo=<?>&descripcion=<?>&idCanal=<?>&idNegocio=<?>&idNivel=<?>&idSubnegocio=<?>&idTipoPuesto=<?>
	@RequestMapping(value = "/updatePuesto", method = RequestMethod.GET)
	public ModelAndView updatePuesto(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String activo = request.getParameter("setActivo");
		String codigo = request.getParameter("setCodigo");
		String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
		String idCanal = request.getParameter("idCanal");
		String idNegocio = request.getParameter("idNegocio");
		String idNivel = request.getParameter("idNivel");
		String idSubnegocio = request.getParameter("idSubnegocio");
		String idTipoPuesto = request.getParameter("idTipoPuesto");
		String idPuesto = request.getParameter("idPuesto");

		PuestoDTO puesto = new PuestoDTO();
		puesto.setActivo(Integer.parseInt(activo));
		puesto.setCodigo(codigo);
		puesto.setDescripcion(descripcion);
		puesto.setIdCanal(Integer.parseInt(idCanal));
		puesto.setIdNegocio(Integer.parseInt(idNegocio));
		puesto.setIdNivel(Integer.parseInt(idNivel));
		puesto.setIdSubnegocio(Integer.parseInt(idSubnegocio));
		puesto.setIdTipoPuesto(Integer.parseInt(idTipoPuesto));
		puesto.setIdPuesto(Integer.parseInt(idPuesto));
		boolean res = puestobi.actualizaPuesto(puesto);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "ID DE PUESTO ACTUALIZADO CORRECTAMENTE");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosService/updateTipoArchivo.json?idArchivo=<?>&setNombreTipo=<?>
	@RequestMapping(value = "/updateTipoArchivo", method = RequestMethod.GET)
	public ModelAndView updateTipoArchivo(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String nombre = new String(request.getParameter("setNombreTipo").getBytes("ISO-8859-1"), "UTF-8");
		String idA = request.getParameter("idArchivo");

		TipoArchivoDTO tipoArchivo = new TipoArchivoDTO();
		tipoArchivo.setNombreTipo(nombre);
		tipoArchivo.setIdTipoArchivo(Integer.parseInt(idA));
		boolean res = tipoarchivobi.actualizaTipoArchivo(tipoArchivo);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "ID TIPO ARCHIVO ACTUALIZADO CORRECTAMENTE");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosService/updateCeco.json?idCeco=<?>&activo=<?>&calle=<?>&ciudad=<?>&cp=<?>&descCeco=<?>&faxContacto=<?>&idCanal=<?>&idCecoSuperior=<?>&idEstado=<?>&idNegocio=<?>&idNivel=<?>&idPais=<?>&nombreContacto=<?>&puestoContacto=<?>&telefonoContacto=<?>&fecha=<?>&usuarioMod=<?>
	@RequestMapping(value = "/updateCeco", method = RequestMethod.GET)
	public ModelAndView updateCeco(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String activo = request.getParameter("activo");
		String calle = new String(request.getParameter("calle").getBytes("ISO-8859-1"), "UTF-8");
		String ciudad = new String(request.getParameter("ciudad").getBytes("ISO-8859-1"), "UTF-8");
		String cp = new String(request.getParameter("cp").getBytes("ISO-8859-1"), "UTF-8");
		String idCeco = request.getParameter("idCeco");
		String descCeco = new String(request.getParameter("descCeco").getBytes("ISO-8859-1"), "UTF-8");
		String faxContacto = new String(request.getParameter("faxContacto").getBytes("ISO-8859-1"), "UTF-8");
		String idCanal = request.getParameter("idCanal");
		String idCecoSuperior = request.getParameter("idCecoSuperior");
		String idEstado = request.getParameter("idEstado");
		String idNegocio = request.getParameter("idNegocio");
		String idNivel = request.getParameter("idNivel");
		String idPais = request.getParameter("idPais");
		String nombreContacto = new String(request.getParameter("nombreContacto").getBytes("ISO-8859-1"), "UTF-8");
		String puestoContacto = new String(request.getParameter("puestoContacto").getBytes("ISO-8859-1"), "UTF-8");
		String telefonoContacto = new String(request.getParameter("telefonoContacto").getBytes("ISO-8859-1"), "UTF-8");
		String fecha = request.getParameter("fecha");
		String usuarioMod = request.getParameter("usuarioMod");

		CecoDTO ceco = new CecoDTO();
		ceco.setActivo(Integer.parseInt(activo));
		ceco.setCalle(calle);
		ceco.setCiudad(ciudad);
		ceco.setCp(cp);
		ceco.setDescCeco(descCeco);
		ceco.setFaxContacto(faxContacto);
		ceco.setIdCanal(Integer.parseInt(idCanal));
		ceco.setIdCeco(idCeco);
		ceco.setIdCecoSuperior(Integer.parseInt(idCecoSuperior));
		ceco.setIdEstado(Integer.parseInt(idEstado));
		ceco.setIdNegocio(Integer.parseInt(idNegocio));
		ceco.setIdNivel(Integer.parseInt(idNivel));
		ceco.setIdPais(Integer.parseInt(idPais));
		ceco.setNombreContacto(nombreContacto);
		ceco.setPuestoContacto(puestoContacto);
		ceco.setTelefonoContacto(telefonoContacto);
		ceco.setFechaModifico(fecha);
		ceco.setUsuarioModifico(usuarioMod);
		boolean res = cecobi.actualizaCeco(ceco);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "CECO ACTUALIZADO CORRECTAMENTE");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosService/updateSucursal.json?idSuc=<?>&idCanal=<?>&idPais=<?>&latitud=<?>&longitud=<?>&nombreSuc=<?>&nuSuc=<?>
	@RequestMapping(value = "/updateSucursal", method = RequestMethod.GET)
	public ModelAndView updateSucursal(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String idCanal = request.getParameter("idCanal");
		String idPais = request.getParameter("idPais");
		String latitud = request.getParameter("latitud");
		String longitud = request.getParameter("longitud");
		String nombreSuc = new String(request.getParameter("nombreSuc").getBytes("ISO-8859-1"), "UTF-8");
		String nuSuc = request.getParameter("nuSuc");
		String idSuc = request.getParameter("idSuc");

		SucursalDTO sucursal = new SucursalDTO();
		sucursal.setIdCanal(Integer.parseInt(idCanal));
		sucursal.setIdPais(Integer.parseInt(idPais));
		sucursal.setLatitud(Double.parseDouble(latitud));
		sucursal.setLongitud(Double.parseDouble(longitud));
		sucursal.setNombresuc(nombreSuc);
		sucursal.setNuSucursal(nuSuc);
		sucursal.setIdSucursal(Integer.parseInt(idSuc));
		boolean res = sucursalbi.actualizaSucursal(sucursal);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "SUCURSAL ACTUALIZADA CORRECTAMENTE");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosService/updateSucursalGCC.json?idSuc=<?>&idCanal=<?>&idPais=<?>&latitud=<?>&longitud=<?>&nombreSuc=<?>&nuSuc=<?>
	@RequestMapping(value = "/updateSucursalGCC", method = RequestMethod.GET)
	public ModelAndView updateSucursalGCC(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String idCanal = request.getParameter("idCanal");
		String idPais = request.getParameter("idPais");
		String latitud = request.getParameter("latitud");
		String longitud = request.getParameter("longitud");
		String nombreSuc = new String(request.getParameter("nombreSuc").getBytes("ISO-8859-1"), "UTF-8");
		String nuSuc = request.getParameter("nuSuc");
		String idSuc = request.getParameter("idSuc");

		SucursalDTO sucursal = new SucursalDTO();
		sucursal.setIdCanal(Integer.parseInt(idCanal));
		sucursal.setIdPais(Integer.parseInt(idPais));
		sucursal.setLatitud(Double.parseDouble(latitud));
		sucursal.setLongitud(Double.parseDouble(longitud));
		sucursal.setNombresuc(nombreSuc);
		sucursal.setNuSucursal(nuSuc);
		sucursal.setIdSucursal(Integer.parseInt(idSuc));
		boolean res = sucursalbi.actualizaSucursalGCC(sucursal);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "SUCURSAL GCC ACTUALIZADA CORRECTAMENTE");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosService/updateUsuarioA.json?idUsuario=<?>&activo=<?>&fecha=<?>&idCeco=<?>&idPuesto=<?>&nombre=<?>
	@RequestMapping(value = "/updateUsuarioA", method = RequestMethod.GET)
	public ModelAndView updateUsuarioA(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String activo = request.getParameter("activo");
		String fecha = request.getParameter("fecha");
		String idCeco = request.getParameter("idCeco");
		String idPuesto = request.getParameter("idPuesto");
		String idUsuario = request.getParameter("idUsuario");
		String nombre = new String(request.getParameter("nombre").getBytes("ISO-8859-1"), "UTF-8");

		Usuario_ADTO usuarioA = new Usuario_ADTO();
		usuarioA.setActivo(Integer.parseInt(activo));
		usuarioA.setFecha(fecha);
		usuarioA.setIdCeco(idCeco);
		usuarioA.setIdPuesto(Integer.parseInt(idPuesto));
		usuarioA.setIdUsuario(Integer.parseInt(idUsuario));
		usuarioA.setNombre(nombre);
		boolean res = usuarioabi.actualizaUsuario(usuarioA);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "FRTA Usuario MODIFICADO CORRECTAMENTE");
		mv.addObject("res", res);
		return mv;
	}
	
	// http://localhost:8080/checklist/catalogosService/updateUsuarioACeco.json?idUsuario=<?>&idCeco=<?>
		@RequestMapping(value = "/updateUsuarioACeco", method = RequestMethod.GET)
		public ModelAndView updateUsuarioACeco(HttpServletRequest request, HttpServletResponse response)
				throws UnsupportedEncodingException {
			String idUsuario = request.getParameter("idUsuario");
			String idCeco = request.getParameter("idCeco");

		

			Usuario_ADTO usuarioAC = new Usuario_ADTO();
		
			usuarioAC.setIdCeco(idCeco);
			usuarioAC.setIdUsuario(Integer.parseInt(idUsuario));
		
			boolean res = usuarioabi.actualizaUsuarioCeco(usuarioAC);

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "FRTA Usuario MODIFICADO CORRECTAMENTE");
			mv.addObject("res", res);
			return mv;
		}


	// http://localhost:8080/checklist/catalogosService/updatePais.json?idPais=<?>&activo=<?>&nombre=<?>
	@RequestMapping(value = "/updatePais", method = RequestMethod.GET)
	public ModelAndView updatePais(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String activo = request.getParameter("activo");
		String nombre = new String(request.getParameter("nombre").getBytes("ISO-8859-1"), "UTF-8");
		String idPais = request.getParameter("idPais");

		PaisDTO pais = new PaisDTO();
		pais.setActivo(Integer.parseInt(activo));
		pais.setNombre(nombre);
		pais.setIdPais(Integer.parseInt(idPais));
		boolean res = paisbi.actualizaPais(pais);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "PAIS MODIFICADO CORRECTAMENTE");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosService/updateHorario.json?idHorario=<?>&cveHorario=<?>&valorIni=<?>&valorFin=<?>
	@RequestMapping(value = "/updateHorario", method = RequestMethod.GET)
	public ModelAndView updateHorario(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String cveHorario = request.getParameter("cveHorario");
		String valorIni = request.getParameter("valorIni");
		String valorFin = request.getParameter("valorFin");
		String idHorario = request.getParameter("idHorario");

		HorarioDTO horario = new HorarioDTO();
		horario.setCveHorario(cveHorario);
		horario.setValorIni(valorIni);
		horario.setValorFin(valorFin);
		horario.setIdHorario(Integer.parseInt(idHorario));
		boolean res = horariobi.actualizaHorario(horario);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "HORARIO MODIFICADO CORRECTAMENTE");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosService/updateParametro.json?setActivo=<?>&setClave=<?>&setValor=<?>
	@RequestMapping(value = "/updateParametro", method = RequestMethod.GET)
	public ModelAndView updateParametro(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String activo = request.getParameter("setActivo");
		String clave = request.getParameter("setClave");
		String valor = request.getParameter("setValor");

		ParametroDTO parametro = new ParametroDTO();
		parametro.setActivo(Integer.parseInt(activo));
		parametro.setClave(clave);
		parametro.setValor(valor);
		boolean res = parametrobi.actualizaParametro(parametro);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "PARAMETRO MODIFICADO CORRECTAMENTE");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosService/updateNegocio.json?idNegocio<?>&setActivo=<?>&descripcion=<?>
	@RequestMapping(value = "/updateNegocio", method = RequestMethod.GET)
	public ModelAndView updateNegocio(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String activo = request.getParameter("setActivo");
		String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
		String idNegocio = request.getParameter("idNegocio");

		NegocioDTO negocio = new NegocioDTO();
		negocio.setActivo(Integer.parseInt(activo));
		negocio.setDescripcion(descripcion);
		negocio.setIdNegocio(Integer.parseInt(idNegocio));
		boolean res = negociobi.actualizaNegocio(negocio);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "NEGOCIO MODIFICADO CORRECTAMENTE");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosService/updateNivel.json?idNivel=<?>&setActivo=<?>&codigo=<?>&descripcion=<?>&idNegocio=<?>
	@RequestMapping(value = "/updateNivel", method = RequestMethod.GET)
	public ModelAndView updateNivel(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String activo = request.getParameter("setActivo");
		String codigo = request.getParameter("codigo");
		String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
		String idNegocio = request.getParameter("idNegocio");
		String idNivel = request.getParameter("idNivel");

		NivelDTO nivel = new NivelDTO();
		nivel.setActivo(Integer.parseInt(activo));
		nivel.setCodigo(codigo);
		nivel.setDescripcion(descripcion);
		nivel.setIdNegocio(Integer.parseInt(idNegocio));
		nivel.setIdNivel(Integer.parseInt(idNivel));
		boolean res = nivelbi.actualizaNivel(nivel);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "NIVEL MODIFICADO CORRECTAMENTE");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosService/updateRecurso.json?idRecurso=<?>&descRecurso=<?>
	@RequestMapping(value = "/updateRecurso", method = RequestMethod.GET)
	public ModelAndView updateRecurso(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String idRecurso = request.getParameter("idRecurso");
		String descRecurso = new String(request.getParameter("descRecurso").getBytes("ISO-8859-1"), "UTF-8");

		RecursoDTO recursoDTO = new RecursoDTO();
		recursoDTO.setIdRecurso(Integer.parseInt(idRecurso));
		recursoDTO.setNombreRecurso(descRecurso);

		boolean res = recursobi.actualizaRecurso(recursoDTO);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "RECURSO MODIFICADO ");
		mv.addObject("res", res);
		return mv;

	}

	// http://localhost:8080/checklist/catalogosService/updateRecursoPerfil.json?idPerfil=<?>&idRecurso=<?>&inserta=<?>&consulta=<?>&elimina=<?>&modifca=<?>
	@RequestMapping(value = "/updateRecursoPerfil", method = RequestMethod.GET)
	public ModelAndView updateRecursoPerfil(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String idPerfil = request.getParameter("idPerfil");
		String idRecurso = request.getParameter("idRecurso");
		String consulta = request.getParameter("consulta");
		String elimina = request.getParameter("elimina");
		String inserta = request.getParameter("inserta");
		String modifica = request.getParameter("modifca");

		RecursoPerfilDTO recursoPerfilDTO = new RecursoPerfilDTO();

		recursoPerfilDTO.setIdPerfil(idPerfil);
		recursoPerfilDTO.setIdRecurso(idRecurso);
		recursoPerfilDTO.setInserta(Integer.parseInt(inserta));
		recursoPerfilDTO.setConsulta(Integer.parseInt(consulta));
		recursoPerfilDTO.setElimina(Integer.parseInt(elimina));
		recursoPerfilDTO.setModifica(Integer.parseInt(modifica));

		boolean res = recursoperfilbi.actualizaRecursoP(recursoPerfilDTO);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "RECURSO PERFIL MODIFICADO ");
		mv.addObject("res", res);
		return mv;

	}

	// http://localhost:8080/checklist/catalogosService/updateAsignaciones.json?idChecklist=<?>&idCeco=<?>&idPuesto=<?>&activo=<?>
	@RequestMapping(value = "/updateAsignaciones", method = RequestMethod.GET)
	public ModelAndView updateAsignaciones(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String idChecklist = request.getParameter("idChecklist");
		String idCeco = request.getParameter("idCeco");
		String idPuesto = request.getParameter("idPuesto");
		String activo = request.getParameter("activo");

		AsignacionDTO asignacionDTO = new AsignacionDTO();

		asignacionDTO.setIdChecklist(Integer.parseInt(idChecklist));
		asignacionDTO.setCeco(idCeco);
		asignacionDTO.setIdPuesto(Integer.parseInt(idPuesto));
		asignacionDTO.setActivo(Integer.parseInt(activo));

		boolean res = asignacionBI.actualizaAsignacion(asignacionDTO);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "ASIGNACION MODIFICADO ");
		mv.addObject("res", res);
		return mv;

	}

	// http://localhost:8080/checklist/catalogosService/updateObtieneTiendas.json?idTarea=<?>&claveTarea=<?>&fechaTarea=<?>&activo=<?>
	@RequestMapping(value = "/updateObtieneTiendas", method = RequestMethod.GET)
	public ModelAndView updateObtieneTiendas(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		
		String idTarea = request.getParameter("idTarea");
		String claveTarea = request.getParameter("claveTarea");
		String fechaTarea = request.getParameter("fechaTarea");
		String activo = request.getParameter("activo");
		
		TareaDTO tareaDTO = new TareaDTO();
		tareaDTO.setIdTarea(Integer.parseInt(idTarea));
		tareaDTO.setCveTarea(claveTarea);
		tareaDTO.setStrFechaTarea(fechaTarea);
		tareaDTO.setActivo(Integer.parseInt(activo));
		
		boolean res = tareasBI.actualizaTarea(tareaDTO);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "LISTA TAREAS MODIFICADO ");
		mv.addObject("res", res);
		return mv;
	}
	
	// http://localhost:8080/checklist/catalogosService/updateCecoGuadalajara.json
		@RequestMapping(value = "/updateCecoGuadalajara", method = RequestMethod.GET)
		public ModelAndView updateCecoGuadalajara(HttpServletRequest request, HttpServletResponse response)
				throws UnsupportedEncodingException {
			
			boolean res = cecoGuadalajarabi.actualizaCecoGuadalajara();

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Modifica Ceco Guadalajara ");
			mv.addObject("res", res);
			return mv;
		}
		
		// http://localhost:8080/checklist/catalogosService/updateEmpFijo.json?idEmpFijo=<?>&idUsuario=<?>&idActivo=<?>
		@RequestMapping(value = "/updateEmpFijo", method = RequestMethod.GET)
		public ModelAndView updateEmpFijo(HttpServletRequest request, HttpServletResponse response)
				throws UnsupportedEncodingException 
		{
		
			String idEmpFijo= request.getParameter("idEmpFijo");
			String idUsuario= request.getParameter("idUsuario");
			String idActivo= request.getParameter("idActivo");
			
			EmpFijoDTO empFijoDTO = new EmpFijoDTO();
			empFijoDTO.setIdEmpFijo(Integer.parseInt(idEmpFijo));
			empFijoDTO.setIdUsuario(Integer.parseInt(idUsuario));
			empFijoDTO.setIdActivo(Integer.parseInt(idActivo));
			
			boolean res = empFijoBI.actualiza(empFijoDTO);

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Se Modifico el id "+idEmpFijo);
			mv.addObject("res", res);
			
			return mv;
		}
		
		// http://localhost:8080/checklist/catalogosService/updateEstado.json?idConsec=<?>&idPais=<?>&idEstado=<?>&descripcion=<?>&abreviatura=<?>
				@RequestMapping(value = "/updateEstado", method = RequestMethod.GET)
				public ModelAndView updateEstado(HttpServletRequest request, HttpServletResponse response)
						throws UnsupportedEncodingException 
				{
					String idConsec=request.getParameter("idConsec");
					String idPais= request.getParameter("idPais");
					String idEstado= request.getParameter("idEstado");
					String descripcion= request.getParameter("descripcion");
					String abreviatura= request.getParameter("abreviatura");
					
					EstadosDTO estadosDTO = new EstadosDTO();
					estadosDTO.setIdConsec(Integer.parseInt(idConsec));
					estadosDTO.setIdPais(Integer.parseInt(idPais));
					estadosDTO.setIdEstado(Integer.parseInt(idEstado));
					estadosDTO.setDescripcion(descripcion);
					estadosDTO.setAbreviatura(abreviatura);
					
					boolean res = estadosBI.actualiza(estadosDTO);

					ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
					mv.addObject("tipo", "Se Modifico el id "+idEstado);
					mv.addObject("res", res);
					
					return mv;
				}
				
				//SERVICIOS PARA EXPANSION
				
				//http://localhost:8080/checklist/catalogosService/updateBitaGral.json?finicio=<?>&fin=<?>&status=<?>
				@RequestMapping(value="/updateBitaGral", method = RequestMethod.GET)
				public ModelAndView updateBitaGral(HttpServletRequest request, HttpServletResponse response){
					
						int idBitaGral=Integer.parseInt( request.getParameter("idBitaGral"));
						String finicio= request.getParameter("finicio");
						String fin= request.getParameter("fin");
						int status=Integer.parseInt( request.getParameter("status"));
						
						BitacoraGralDTO bitaG = new BitacoraGralDTO();
						bitaG.setIdBitaGral(idBitaGral);
						bitaG.setFinicio(finicio);
						bitaG.setFin(fin);
						bitaG.setStatus(status);
						
						boolean respuesta = bitacoraGralBI.actualiza(bitaG);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "modifico Bitacora Gral ");
						mv.addObject("res", respuesta);
						
						return mv;
				}
				//http://localhost:8080/checklist/catalogosService/updateBitaH.json?idBitaGral=<?>&idBita=<?>&finicio=<?>&fin=<?>&status=<?>
				@RequestMapping(value="/updateBitaH", method = RequestMethod.GET)
				public ModelAndView updateBitaH(HttpServletRequest request, HttpServletResponse response){
					
						int idBitaGral=Integer.parseInt(request.getParameter("idBitaGral"));
						int idBita=Integer.parseInt(request.getParameter("idBita"));
						String finicio= request.getParameter("finicio");
						String fin= request.getParameter("fin");
						int status=Integer.parseInt( request.getParameter("status"));
						
						
						BitacoraGralDTO bitaH = new BitacoraGralDTO();
						
						bitaH.setIdBitaGral(idBitaGral);
						bitaH.setIdBita(idBita);
						bitaH.setFinicio(finicio);
						bitaH.setFin(fin);
						bitaH.setStatus(status);
						
						
						boolean respuesta = bitacoraGralBI.actualizaH(bitaH);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "modifico Bitacora Gral_hija "+idBitaGral+" : "+idBita);
						mv.addObject("res", respuesta);
						return mv;
					
				}
				
				//http://localhost:8080/checklist/catalogosService/updateFirmaCheck.json?idFirma=<?>&bitacora=<?>&ceco=<?>&idUsuario=<?>&puesto=<?>&responsable=<?>&correo=<?>&nombre=<?>&ruta=<?>
				@RequestMapping(value="/updateFirmaCheck", method = RequestMethod.GET)
				public ModelAndView updateFirmaCheck(HttpServletRequest request, HttpServletResponse response){
					
						int idFirma=Integer.parseInt(request.getParameter("idFirma"));
						String bitacora=request.getParameter("bitacora");
						String ceco=request.getParameter("ceco");
						String idUsuario=request.getParameter("idUsuario");
						String puesto=request.getParameter("puesto");
						String responsable=request.getParameter("responsable");
						String correo=request.getParameter("correo");
						String nombre=request.getParameter("nombre");
						String ruta=request.getParameter("ruta");
						
						FirmaCheckDTO firm = new FirmaCheckDTO();
						
						firm.setIdFirma(idFirma);
						firm.setBitacora(bitacora);
						firm.setCeco(ceco);
						firm.setIdUsuario(idUsuario);
						firm.setPuesto(puesto);
						firm.setResponsable(responsable);
						firm.setCorreo(correo);
						firm.setNombre(nombre);
						firm.setRuta(ruta);
						
						boolean respuesta = firmaCheckBI.actualiza(firm);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Se modifico el id de Firma de checklist  "+idFirma);
						mv.addObject("res", respuesta);
						
						return mv;
					
				}
				
				//http://localhost:8080/checklist/catalogosService/updateImagSuc.json?idImag=<?>&idSucursal=<?>&nombre=<?>&ruta=<?>&observ=<?>
				@RequestMapping(value="/updateImagSuc", method = RequestMethod.GET)
				public ModelAndView updateImagSuc(HttpServletRequest request, HttpServletResponse response){
					
						int idImag=Integer.parseInt(request.getParameter("idImag"));
						int idSucursal=Integer.parseInt(request.getParameter("idSucursal"));
						String nombre=request.getParameter("nombre");
						String ruta=request.getParameter("ruta");
						String observ=request.getParameter("observ");
						

						SucursalChecklistDTO suci = new SucursalChecklistDTO();
						
						suci.setIdImag(idImag);
						suci.setIdSucursal(idSucursal);
						suci.setNombre(nombre);
						suci.setRuta(ruta);
						suci.setObserv(observ);
						
						boolean respuesta = sucursalChecklistBI.actualiza(suci);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "se modifico los datos de  imagen Sucursal "+idSucursal+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
				
				}
				//YYYYMMDD HH24:MI:SS:FF3 pueden ir null fechas
				//http://localhost:8080/checklist/catalogosService/updateSoftOpMan.json?ceco=<?>&idUsuario=<?>&bitacora=<?>&recorrido=<?>&fechaini=<?>&fechafin=<?>&aux=<?>
				@RequestMapping(value="/updateSoftOpMan", method = RequestMethod.GET)
				public ModelAndView updateSoftOpMan(HttpServletRequest request, HttpServletResponse response){
					
						int ceco=Integer.parseInt(request.getParameter("ceco"));
						int idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
						int bitacora=Integer.parseInt(request.getParameter("bitacora"));
						int recorrido=Integer.parseInt(request.getParameter("recorrido"));
						String fechaini=request.getParameter("fechaini");
						String fechafin=request.getParameter("fechafin");
						String aux=request.getParameter("aux");

						SoftOpeningDTO so = new SoftOpeningDTO();
						
						so.setCeco(ceco);
						so.setIdUsuario(idUsuario);
						so.setBitacora(bitacora);
						so.setRecorrido(recorrido);
						so.setFechaini(fechaini);
						so.setFechafin(fechafin);
						so.setAux(aux);
						
						boolean respuesta = softOpeningBI.actualiza(so);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "update  soft con ceco: "+ceco+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
					
				}
				
				//http://10.51.219.179:8080/checklist/catalogosService/modificaCeckin.json?idCheck=462&idBitacora=28964&idUsuario=82144&idCeco=480100&fecha=20190806&latitud=45&longitud=456&bandera=1&plataforma=ANDROID&tiempoConexion=1
				@RequestMapping(value="/modificaCeckin", method = RequestMethod.GET)
				public ModelAndView modificaCeckin(HttpServletRequest request, HttpServletResponse response){
					try{
						int idCheck=Integer.parseInt(request.getParameter("idCheck"));
						int idBitacora=Integer.parseInt(request.getParameter("idBitacora"));
						int idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
						String idCeco=request.getParameter("idCeco");
						String fecha=request.getParameter("fecha");
						String latitud=request.getParameter("latitud");
						String longitud=request.getParameter("longitud");
						int bandera=Integer.parseInt(request.getParameter("bandera"));
						String plataforma=request.getParameter("plataforma");
						int tiempoConexion=Integer.parseInt(request.getParameter("tiempoConexion"));
						

						CheckinDTO asistencia = new CheckinDTO();
						asistencia.setIdCheck(idCheck);
						asistencia.setIdBitacora(idBitacora);
						asistencia.setIdUsuario(idUsuario);
						asistencia.setIdCeco(idCeco);
						asistencia.setFecha(fecha);
						asistencia.setLatitud(latitud); 
						asistencia.setLongitud(longitud);
						asistencia.setBandera(bandera);
						asistencia.setPlataforma(plataforma);
						asistencia.setTiempoConexion(tiempoConexion);
						
						
						boolean respuesta = checkinBI.modificaCheckin(asistencia);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Modifica  checkin bitacora "+idBitacora+" : ");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/updateAsignaExp.json?ceco=<?>&version=<?>&usuario=<?>&usuario_asig=<?>&obs=<?>&idestatus=<?>
				@RequestMapping(value="/updateAsignaExp", method = RequestMethod.GET)
				public ModelAndView updateAsignaExp(HttpServletRequest request, HttpServletResponse response){
						try{
							int ceco=Integer.parseInt(request.getParameter("ceco"));
							int version=Integer.parseInt(request.getParameter("version"));
							int usuario=Integer.parseInt(request.getParameter("usuario"));
							int usuario_asig=Integer.parseInt(request.getParameter("usuario_asig"));
							String obs=request.getParameter("obs");
							int idestatus=Integer.parseInt(request.getParameter("idestatus"));


							AsignaExpDTO asig = new AsignaExpDTO();
							
							asig.setCeco(ceco);
							asig.setVersion(version);
							asig.setUsuario_asig(usuario_asig);
							asig.setUsuario(usuario);
							asig.setObs(obs);
							asig.setIdestatus(idestatus);
							
							boolean respuesta = asignaExpBI.actualiza(asig);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update   con ceco: "+ceco+" : ");
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
				
				//http://localhost:8080/checklist/catalogosService/updateVersionExp.json?idTab=<?>&idCheck=<?>&idVers=<?>&idactivo=<?>&obs=<?>&aux=<?>
				@RequestMapping(value="/updateVersionExp", method = RequestMethod.GET)
				public ModelAndView updateVersionExp(HttpServletRequest request, HttpServletResponse response){
					try{
						int idTab=Integer.parseInt(request.getParameter("idTab"));
						int idCheck=Integer.parseInt(request.getParameter("idCheck"));
						int idVers=Integer.parseInt(request.getParameter("idVers"));
						int idactivo=Integer.parseInt(request.getParameter("idactivo"));
						String obs=request.getParameter("obs");
						String aux=request.getParameter("aux");

						VersionExpanDTO ver = new VersionExpanDTO();
						
						ver.setIdTab(idTab);
						ver.setIdCheck(idCheck);
						ver.setIdVers(idVers);
						ver.setIdactivo(idactivo);
						ver.setObs(obs);
						ver.setAux(aux);
						
						boolean respuesta = versionExpanBI.actualiza(ver);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "update   checklist: "+idCheck+" : con la version:"+idVers+"");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				//http://localhost:8080/checklist/catalogosService/updateVersionExp2.json?idTab=<?>&idCheck=<?>&idVers=<?>
				@RequestMapping(value="/updateVersionExp2", method = RequestMethod.GET)
				public ModelAndView updateVersionExp2(HttpServletRequest request, HttpServletResponse response){
					try{
						int idTab=Integer.parseInt(request.getParameter("idTab"));
						int idCheck=Integer.parseInt(request.getParameter("idCheck"));
						int idVers=Integer.parseInt(request.getParameter("idVers"));

						VersionExpanDTO ver = new VersionExpanDTO();
						
						ver.setIdTab(idTab);
						ver.setIdCheck(idCheck);
						ver.setIdVers(idVers);
					
						boolean respuesta = versionExpanBI.actualiza2(ver);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "update   checklist: "+idCheck+" : con la version:"+idVers+"");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/updateVersionGral.json?idTab=<?>&idCheck=<?>&idVers=<?>&idactivo=<?>&obs=<?>&&nombrecheck=<?>&versionAnt=<?>
				@RequestMapping(value="/updateVersionGral", method = RequestMethod.GET)
				public ModelAndView updateVersionGral(HttpServletRequest request, HttpServletResponse response){
					try{
						int idTab=Integer.parseInt(request.getParameter("idTab"));
						int idCheck=Integer.parseInt(request.getParameter("idCheck"));
						int idVers=Integer.parseInt(request.getParameter("idVers"));
						int idactivo=Integer.parseInt(request.getParameter("idactivo"));
						String obs=request.getParameter("obs");
						String nombrecheck=request.getParameter("nombrecheck");
						int versionAnt =Integer.parseInt(request.getParameter("versionAnt"));

						VersionChecklistGralDTO ver = new VersionChecklistGralDTO();
						
						ver.setIdTab(idTab);
						ver.setIdCheck(idCheck);
						ver.setIdVers(idVers);
						ver.setIdactivo(idactivo);
						ver.setObs(obs);
						ver.setVersionAnt(versionAnt);
						ver.setNombrecheck(nombrecheck);
						
						
						boolean respuesta = versionChecklistGralBI.actualiza(ver);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "update   checklist: "+idCheck+" : con la version:"+idVers+"");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/updateCecoTipSuc.json?ceco=<?>&tipoSuc=<?>
				@RequestMapping(value="/updateCecoTipSuc", method = RequestMethod.GET)
				public ModelAndView updateCecoTipSuc(HttpServletRequest request, HttpServletResponse response){
					try{
						String ceco = request.getParameter("ceco");
						String tipoSuc = request.getParameter("tipoSuc");
						
						SoporteExpDTO sop = new SoporteExpDTO();
	
						sop.setCeco(ceco);
						sop.setTipoSuc(tipoSuc);
						boolean respuesta = soporteExpBI.actualizaTipoSuc(sop);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "update   ceco: "+ceco+" : con tipo de suc : "+tipoSuc+"");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosService/updateCecoStatus.json?ceco=<?>&status=<?>&prerrecorrido=<?>
				@RequestMapping(value="/updateCecoStatus", method = RequestMethod.GET)
				public ModelAndView updateCecoStatus(HttpServletRequest request, HttpServletResponse response){
					try{
						String ceco = request.getParameter("ceco");
						String status = request.getParameter("status");
						String prerrecorrido = request.getParameter("prerrecorrido");
						
						
						SoporteExpDTO sop = new SoporteExpDTO();
	
						sop.setCeco(ceco);
						sop.setStatus(status);
						sop.setAux2(prerrecorrido);
						boolean respuesta = soporteExpBI.actualiza(sop);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "update   ceco: "+ceco+" : con estatus  : "+status+"");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				//http://localhost:8080/checklist/catalogosService/updateContactoExt.json?idTab=<?>&http://10.51.218.154:8080/checklist/catalogosService/updateContactoExt.json?idTab=148&ceco=230001&bitGral=300090&idUsu=123&domicilio=av&&nombre=mario&telefono=12345678&contacto=correoele&area=nuevaconta&compania=italika&aux=modificado&aux2=modificado
				@RequestMapping(value="/updateContactoExt", method = RequestMethod.GET)
				public ModelAndView updateContactoExt(HttpServletRequest request, HttpServletResponse response){
					try{
						
						int idTab=Integer.parseInt(request.getParameter("idTab"));
						String ceco=request.getParameter("ceco");
						int bitGral=Integer.parseInt(request.getParameter("bitGral"));
						int idUsu=Integer.parseInt(request.getParameter("idUsu"));
						String domicilio=request.getParameter("domicilio");
						String nombre=request.getParameter("nombre");
						String telefono=request.getParameter("telefono");
						String contacto=request.getParameter("contacto");
						String area=request.getParameter("area");
						String compania=request.getParameter("compania");
						String aux=request.getParameter("aux");
						String aux2=request.getParameter("aux2");
						
						
						EmpContacExtDTO cont = new EmpContacExtDTO();
						
						cont.setIdTab(idTab);
						cont.setCeco(ceco);
						cont.setBitGral(bitGral);
						cont.setIdUsu(idUsu);
						cont.setDomicilio(domicilio);
						cont.setNombre(nombre);
						cont.setTelefono(telefono);
						cont.setContacto(contacto);
						cont.setArea(area);
						cont.setCompania(compania);
						cont.setAux(aux);
						cont.setAux2(aux2);
					
						
						boolean respuesta = empContacExtBI.actualiza(cont);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "update   ceco: "+ceco+" : con la bitGral: "+bitGral+"");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				// http://10.51.219.179:8080/checklist/catalogosService/updateDiaFestivo.json?fecha=01012020
				@RequestMapping(value="/updateDiaFestivo", method = RequestMethod.GET)
				public ModelAndView updateDiaFestivo(HttpServletRequest request, HttpServletResponse response){
					try{
						String fecha = request.getParameter("fecha");
					
						ConsultaFechasDTO a1 = new ConsultaFechasDTO();
	
						a1.setFecha(fecha);
						
						boolean respuesta = consultaFechasBI.ActualizaFecha(a1);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Registro Modificado");
						mv.addObject("res", respuesta);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				// http://10.51.219.179:8080/catalogosService/updateHallazgo.json?idHallazgo=&respuesta=&idResp=&status=&ruta=&obs=&fechaAut=&fechafin=&obsNueva=
				@RequestMapping(value="/updateHallazgo", method = RequestMethod.GET)
				public ModelAndView updateHallazgo(HttpServletRequest request, HttpServletResponse response){
					try{
						int idHallazgo= Integer.parseInt(request.getParameter("idHallazgo"));
						int idResp= Integer.parseInt(request.getParameter("idResp"));
						int status= Integer.parseInt(request.getParameter("status"));
						String respuesta= request.getParameter("respuesta");
						String ruta= request.getParameter("ruta");
						String obs= request.getParameter("obs");
						String obsNueva= request.getParameter("obsNueva");
						String fechaAut= request.getParameter("fechaAut");
						String fechafin= request.getParameter("fechafin");
						String usuModif=request.getParameter("usuModif");
						
						HallazgosExpDTO h = new HallazgosExpDTO();
						
						h.setIdHallazgo(idHallazgo);
						h.setIdResp(idResp);
						h.setStatus(status);
						h.setRespuesta(respuesta);
						h.setRuta(ruta);
						h.setObs(obs);
						h.setObsNueva(obsNueva);
						h.setFechaAutorizo(fechaAut);
						h.setFechaFin(fechafin);
						h.setUsuModif(usuModif);
						boolean res = hallazgosExpBI.actualiza(h);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Registro actualizado");
						mv.addObject("res", res);
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				// http://10.51.219.179:8080/checklist/catalogosService/updateHallazgoResp.json?idHallazgo=&respuesta=&idResp=&status=&ruta=&obs=&fechaAut=&fechafin=&obsNueva=&motivrechazo=
				@RequestMapping(value="/updateHallazgoResp", method = RequestMethod.GET)
				public ModelAndView updateHallazgoResp(HttpServletRequest request, HttpServletResponse response){
					try{
						
						int idHallazgo= Integer.parseInt(request.getParameter("idHallazgo"));
						int idResp= Integer.parseInt(request.getParameter("idResp"));
						int status= Integer.parseInt(request.getParameter("status"));
						String respuesta= request.getParameter("respuesta");
						String ruta= request.getParameter("ruta");
						String obs= request.getParameter("obs");
						String obsNueva= request.getParameter("obsNueva");
						String fechaAut= request.getParameter("fechaAut");
						String fechafin= request.getParameter("fechafin");
						String motivrechazo=request.getParameter("motivrechazo");
						String usuModif=request.getParameter("usuModif");
						
						HallazgosExpDTO h = new HallazgosExpDTO();
						
						h.setIdHallazgo(idHallazgo);
						h.setIdResp(idResp);
						h.setStatus(status);
						h.setRespuesta(respuesta);
						h.setRuta(ruta);
						h.setObs(obs);
						h.setObsNueva(obsNueva);
						h.setFechaAutorizo(fechaAut);
						h.setFechaFin(fechafin);
						h.setMotivrechazo(motivrechazo);
						h.setUsuModif(usuModif);
					
						boolean res = hallazgosExpBI.actualizaResp(h);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Registro actualizado");
						mv.addObject("res", res);
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				
				// http://10.51.219.179:8080/checklist/catalogosService/updateHallazgoEvi.json?idEvi=&idHallazgo=&idResp=&ruta=&nombre=&totEvi=
				@RequestMapping(value="/updateHallazgoEvi", method = RequestMethod.GET)
				public ModelAndView updateHallazgoEvi(HttpServletRequest request, HttpServletResponse response){
					try{
						
						int idEvi= Integer.parseInt(request.getParameter("idEvi"));
						int idHallazgo= Integer.parseInt(request.getParameter("idHallazgo"));
						int idResp= Integer.parseInt(request.getParameter("idResp"));
						String ruta= request.getParameter("ruta");
						String nombre= request.getParameter("nombre");
						int totEvi= Integer.parseInt(request.getParameter("totEvi"));
						
						
						HallazgosEviExpDTO he = new HallazgosEviExpDTO();
						he.setIdEvi(idEvi);
						he.setIdHallazgo(idHallazgo);
						he.setIdResp(idResp);
						he.setRuta(ruta);
						he.setNombre(nombre);
						he.setTotEvi(totEvi);
						
				
						boolean res = hallazgosEviExpBI.actualiza(he);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "Registro actualizado");
						mv.addObject("res", res);
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}

				//http://10.51.219.179:8080/checklist/catalogosService/updateBitaco.json?idBitacora=30733&idCheckUsua=85503&longitud=123456&latitud=7890&fechaFin=20190831&modo=1&ceco=1&commit=1

					@RequestMapping(value = "/updateBitaco", method = RequestMethod.GET)
					public ModelAndView updateBitaco(HttpServletRequest request, HttpServletResponse response)
							throws UnsupportedEncodingException {
					
						String idBitacora = request.getParameter("idBitacora");
						String idCheckUsua = request.getParameter("idCheckUsua");
						String longitud = request.getParameter("longitud");
						String latitud = request.getParameter("latitud");
						String fechaFin = request.getParameter("fechaFin");
						String modo=request.getParameter("modo");
						String ceco = request.getParameter("ceco");
						String commit = request.getParameter("commit");

						BitacoraAdministradorDTO bitacoraAdministrador = new BitacoraAdministradorDTO();
						
						bitacoraAdministrador.setIdBitacora(Integer.parseInt(idBitacora));
						bitacoraAdministrador.setIdCheckUsua(Integer.parseInt(idCheckUsua));
						bitacoraAdministrador.setLongitud(longitud);
						bitacoraAdministrador.setLatitud(latitud);
						bitacoraAdministrador.setFechaFin(fechaFin);
						bitacoraAdministrador.setModo(Integer.parseInt(modo));
						bitacoraAdministrador.setCeco(ceco);
						bitacoraAdministrador.setCommit(Integer.parseInt(commit));
						
						
						boolean res = bitacoraAdministradorBI.actualizaBitacora(bitacoraAdministrador);

						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", "BITACORA MODIFICADAS");
						mv.addObject("res", res);
						return mv;
					}
					
					//http://localhost:8080/checklist/catalogosService/updateCheckUsuAsignaExp.json?ceco=<?>&usuario=<?>
					@RequestMapping(value="/updateCheckUsuAsignaExp", method = RequestMethod.GET)
					public ModelAndView updateCheckUsuAsignaExp(HttpServletRequest request, HttpServletResponse response){
							try{
								int ceco=Integer.parseInt(request.getParameter("ceco"));
								int usuario=Integer.parseInt(request.getParameter("usuario"));

								AsignaExpDTO asig = new AsignaExpDTO();
								
								asig.setCeco(ceco);
								asig.setUsuario(usuario);
								
								boolean respuesta = asignaExpBI.actualizaCheckU(asig);

								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "update   con ceco:  "+ceco+" usuario: "+usuario+" ");
								mv.addObject("res", respuesta);
								
								return mv;
							}catch(Exception e){
								logger.info(e);
								return null;
							}
						}
				
					//http://localhost:8080/checklist/catalogosService/actualizaFechatermino.json?fecha=<?>&idBitacora=<?>&preCalificacion=<?>&calificacionFinal=<?>
					@RequestMapping(value="/actualizaFechatermino", method = RequestMethod.GET)
					public ModelAndView actualizaFechatermino(HttpServletRequest request, HttpServletResponse response){
							try{
								String fecha=request.getParameter("fecha");
								int idBitacora=Integer.parseInt(request.getParameter("idBitacora"));
								String preCalificacion=request.getParameter("preCalificacion");
								String calificacionFinal=request.getParameter("calificacionFinal");

								boolean respuesta = checklistBI.actualizaFechatermino(fecha, idBitacora,preCalificacion,calificacionFinal);

								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "update   con fecha termino : "+fecha+" : a la bitacora "+idBitacora+"");
								mv.addObject("res", respuesta);
								
								return mv;
							}catch(Exception e){
								logger.info(e);
								return null;
							}
						}
					
					//http://localhost:8080/checklist/catalogosService/actualizaPosible.json?idPosible=<?>&respuesta=<?>
					@RequestMapping(value="/actualizaPosible", method = RequestMethod.GET)
					public ModelAndView actualizaPosible(HttpServletRequest request, HttpServletResponse response){
						try{
							String respuesta= new String (request.getParameter("respuesta").getBytes("ISO-8859-1"), "UTF-8");
							int idPosible=Integer.parseInt(request.getParameter("idPosible"));
							PosiblesDTO pos=new PosiblesDTO();
							pos.setIdPosible(idPosible);
							
							char[] ca = { '\u0061', '\u0301' };
							char[] ce = { '\u0065', '\u0301' };
							char[] ci = { '\u0069', '\u0301' };
							char[] co = { '\u006F', '\u0301' };
							char[] cu = { '\u0075', '\u0301' };
							// mayusculas
							char[] c1 = { '\u0041', '\u0301' };
							char[] c2 = { '\u0045', '\u0301' };
							char[] c3 = { '\u0049', '\u0301' };
							char[] c4 = { '\u004F', '\u0301' };
							char[] c5 = { '\u0055', '\u0301' };
							char[] c6 = { '\u006E', '\u0303' };

							String setPregunta = request.getParameter("respuesta");
							//System.out.println("setPregunta1 " + setPregunta);
							if (setPregunta.contains(String.valueOf(ca)) || setPregunta.contains(String.valueOf(ce))
									|| setPregunta.contains(String.valueOf(ci)) || setPregunta.contains(String.valueOf(co))
									|| setPregunta.contains(String.valueOf(cu)) || setPregunta.contains(String.valueOf(c1))
									|| setPregunta.contains(String.valueOf(c2)) || setPregunta.contains(String.valueOf(c3))
									|| setPregunta.contains(String.valueOf(c4)) || setPregunta.contains(String.valueOf(c5))
									|| setPregunta.contains(String.valueOf(c6))) {

								String rr = setPregunta.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
										.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
										.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
										.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
										.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
										.replaceAll(String.valueOf(c6), "ñ");
								//System.out.println("rr " + rr);
								
								setPregunta=rr;
								pos.setDescripcion(rr);
								rr = null;
							}	
							
							
							boolean res = posiblesbi.actualizaPosible(pos);
							
							ModelAndView mv = new ModelAndView("altaModuloRes");
							mv.addObject("tipo", "ID POSIBLE ASIGNADO ");
							mv.addObject("res", res);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://10.51.219.179:8080/checklist/catalogosService/actualizaHandbook.json?idBook=28&descripcion=ALGO2&ruta=ALGO3&activo=1&padre=1&tipo=TEST3
					@RequestMapping(value="/actualizaHandbook", method = RequestMethod.GET)
					public ModelAndView actualizaHandbook(HttpServletRequest request, HttpServletResponse response){
							try{
								String idBook=request.getParameter("idBook");
								String descripcion=request.getParameter("descripcion");
								String ruta=request.getParameter("ruta");
								String activo=request.getParameter("activo");
								String padre=request.getParameter("padre");
								String tipo=request.getParameter("tipo");
								
								AdmHandbookDTO admHandbook = new AdmHandbookDTO();
								admHandbook.setIdBook(idBook);
								admHandbook.setDescripcion(descripcion);
								admHandbook.setRuta(ruta);
								admHandbook.setActivo(activo);
								admHandbook.setPadre(padre); 
								admHandbook.setTipo(tipo);

								boolean respuesta = handBookBI.actualizaHandbook(admHandbook);

								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Update Handbook");
								mv.addObject("res", respuesta);
								
								return mv;
							}catch(Exception e){
								logger.info(e);
								return null;
							}
						}
					
					//http://10.51.219.179:8080/checklist/catalogosService/actualizaProtocolo.json?idProtocolo=<?>&descripcion=<?>&observaciones=<?>&status=<?>&statusRetro=<?>
					@RequestMapping(value="/actualizaProtocolo", method = RequestMethod.GET)
					public ModelAndView actualizaProtocolo(HttpServletRequest request, HttpServletResponse response){
						try{
							String idProtocolo=request.getParameter("idProtocolo");
							String descripcion=request.getParameter("descripcion");
							String observaciones=request.getParameter("observaciones");
							String status=request.getParameter("status");
							int statusRetro=Integer.parseInt(request.getParameter("statusRetro"));
							ProtocoloDTO protocolo = new ProtocoloDTO();
							protocolo.setIdProtocolo(Integer.parseInt(idProtocolo));
							protocolo.setDescripcion(descripcion);
							protocolo.setObservaciones(observaciones);
							protocolo.setStatus(Integer.parseInt(status));
							protocolo.setStatusRetro(statusRetro);
				
							boolean respuesta = protocoloBI.modificaProtocolo(protocolo);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "Update  Protocolo");
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					
					//http://localhost:8080/checklist/catalogosService/updateArbolParam.json?reqObserv=<?>&bandObser=<?>&plantilla=&ponderacion&arbol=
					@RequestMapping(value="/updateArbolParam", method = RequestMethod.GET)
					public ModelAndView updateArbolParam(HttpServletRequest request, HttpServletResponse response){
						try{
							//si requiere observacion 
							String reqObserv = request.getParameter("reqObserv");
							// si la observacion es obligatoria
							String bandObser = request.getParameter("bandObser");
							String plantilla = request.getParameter("plantilla");
							String ponderacion = request.getParameter("ponderacion");
							String arbol = request.getParameter("arbol");
							
							SoporteExpDTO sop = new SoporteExpDTO();
		
							sop.setReqObserv(reqObserv);
							sop.setBandObser(bandObser);
							sop.setPlantilla(plantilla);
							sop.setPonderacion(ponderacion);
							sop.setArbol(arbol);
							boolean respuesta = soporteExpBI.actualizaAbol(sop);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update   arbol: "+arbol+" : ");
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					// http://10.53.33.83/checklist/catalogosService/updateNegoExp.json?idNego=&Nombcorto=&obs=&descripcion=&idTab=
					@RequestMapping(value = "/updateNegoExp", method = RequestMethod.GET)
					public ModelAndView altaNegoExp(HttpServletRequest request, HttpServletResponse response)
					{
						try {
							int idTab =Integer.parseInt(request.getParameter("idTab"));
							int idNego =Integer.parseInt(request.getParameter("idNego"));
							String Nombcorto =request.getParameter("Nombcorto");
							String obs =request.getParameter("obs");
							String descripcion =request.getParameter("obs");
							
							ZonaNegoExpDTO zn=new ZonaNegoExpDTO();
							zn.setIdTabNegocio(idTab);
							zn.setIdNegocio(idNego);
							zn.setNegocio(Nombcorto);
							zn.setObs(obs);
							zn.setDescNego(descripcion);					

							boolean res = zonaNegoBI.actualizaNego(zn);
							
							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", " Negocio actualizada: "+idNego);
							mv.addObject("res", res);
							return mv;
						} catch (Exception e) {
							logger.info(e);
							return null;  
						}
					}
					// http://10.53.33.83/checklist/catalogosService/updateZonaExp.json?Nombcorto=&obs=&descripcion=&aux=&idZona=
					@RequestMapping(value = "/updateZonaExp", method = RequestMethod.GET)
					public ModelAndView updateZonaExp(HttpServletRequest request, HttpServletResponse response)
					{
						try {
							
							int idZona =Integer.parseInt(request.getParameter("idZona"));
							String Nombcorto =request.getParameter("Nombcorto");
							String obs =request.getParameter("obs");
							String descripcion =request.getParameter("obs");
							String aux =request.getParameter("aux");
							
							ZonaNegoExpDTO zn=new ZonaNegoExpDTO();
							zn.setIdZona(idZona);
							zn.setZona(Nombcorto);
							zn.setObs(obs);
							zn.setDescZona(descripcion);
							zn.setAux(aux);
							
							boolean res = zonaNegoBI.actualizaZona(zn);
							
							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", " Zona actualizada: ");
							mv.addObject("res", res);
							return mv;
						} catch (Exception e) {
							logger.info(e);
							return null;  
						}
					}
					// http://10.53.33.83/checklist/catalogosService/updateZonaNegoExp.json?idZona=&idNego=
					@RequestMapping(value = "/updateZonaNegoExp", method = RequestMethod.GET)
					public ModelAndView updateZonaNegoExp(HttpServletRequest request, HttpServletResponse response)
					{
						try {
							int idTab =Integer.parseInt(request.getParameter("idTab"));
							ZonaNegoExpDTO zn=new ZonaNegoExpDTO();
							zn.setAux2(idTab);
							
							boolean res = zonaNegoBI.actualizaRela(zn);
							
							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", " update Relacion Zona Negocio Correcta: ");
							mv.addObject("res", res);
							return mv;
						} catch (Exception e) {
							logger.info(e);
							return null;  
						}
					}
					// http://10.53.33.83/checklist/catalogosService/updateAdicional.json?obs=&status=&idPreg=&reco=&ruta=
					@RequestMapping(value = "/updateAdicional", method = RequestMethod.GET)
					public ModelAndView updateAdicional(HttpServletRequest request, HttpServletResponse response)
					{
						try {

							String obs=request.getParameter("obs");
							int status =Integer.parseInt(request.getParameter("status"));
							int idPreg =Integer.parseInt(request.getParameter("idPreg"));
							int reco =Integer.parseInt(request.getParameter("reco"));
							String ruta =request.getParameter("ruta");
							
							AdicionalesDTO ad=new AdicionalesDTO();
							ad.setObs(obs);
							ad.setStatus(status);
							ad.setIdPreg(idPreg);
							ad.setRecorrido(reco);
							ad.setRuta(ruta);
							
							boolean res = adicionalesBI.actualiza(ad);
							
							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", " update adicional correcto: ");
							mv.addObject("res", res);
							return mv;
						} catch (Exception e) {
							logger.info(e);
							return null;  
						}
					}
					
					// http://10.51.219.179:8080/catalogosService/updateHallazgoMov.json?idHallazgo=2024&respuesta=2&idResp=3970&status=2&ruta=xx&obs=xx&fechaAut=&fechafin=&obsNueva=nuevaobs&usuModif=&motiRecha=
					@RequestMapping(value="/updateHallazgoMov", method = RequestMethod.GET)
					public ModelAndView updateHallazgoMov(HttpServletRequest request, HttpServletResponse response){
						try{
							
								int idHallazgo= Integer.parseInt(request.getParameter("idHallazgo"));
								int idResp= Integer.parseInt(request.getParameter("idResp"));
								int status= Integer.parseInt(request.getParameter("status"));
								String respuesta= request.getParameter("respuesta");
								String ruta= request.getParameter("ruta");
								String obs= request.getParameter("obs");
								String obsNueva= request.getParameter("obsNueva");
								String fechaAut= request.getParameter("fechaAut");
								String fechafin= request.getParameter("fechafin");
								String usuModif=request.getParameter("usuModif");
								String motiRecha=request.getParameter("motiRecha");
								
								HallazgosExpDTO h = new HallazgosExpDTO();
								
								h.setIdHallazgo(idHallazgo);
								h.setIdResp(idResp);
								h.setStatus(status);
								h.setRespuesta(respuesta);
								h.setRuta(ruta);
								h.setObs(obs);
								h.setObsNueva(obsNueva);
								h.setFechaAutorizo(fechaAut);
								h.setFechaFin(fechafin);
								h.setUsuModif(usuModif);
								h.setMotivrechazo(motiRecha);
								boolean res = hallazgosExpBI.actualizaMov(h);

								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Registro actualizado");
								mv.addObject("res", res);
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/updatePregParam.json?idPreg=<?>&tipoPreg=<?>&desc=&critica&sla=&area=&modulo=
					@RequestMapping(value="/updatePregParam", method = RequestMethod.GET)
					public ModelAndView updatePregParam(HttpServletRequest request, HttpServletResponse response){
						try{
							String idPreg = request.getParameter("idPreg");
							String tipoPreg = request.getParameter("tipoPreg");
							String desc = request.getParameter("desc");
							String critica = request.getParameter("critica");
							String sla = request.getParameter("sla");
							String area = request.getParameter("area");
							String modulo = request.getParameter("modulo");
							
							
							SoporteExpDTO sop = new SoporteExpDTO();
		
							sop.setIdPreg(idPreg);
							sop.setTipoPreg(tipoPreg);
							sop.setDescPreg(desc);
							sop.setCritica(critica);
							sop.setSla(sla);
							sop.setArea(area);
							sop.setModulo(modulo);
							boolean respuesta = soporteExpBI.actualizaPreg(sop);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update   pregunta: "+idPreg+" : ");
							mv.addObject("res", respuesta);
							
							return mv;

						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}

//http://10.51.219.179:8080/checklist/catalogosService/updateAltaGerente.json?idSecuencia=6&idGerente=331952&nomGerente=CARLOS&idUsuario=176361&nomUsuario=BERENICE CEDILLO RIVERO&&idPerfil=0
					@RequestMapping(value="/updateAltaGerente", method = RequestMethod.GET)
					public ModelAndView updateAltaGerente(HttpServletRequest request, HttpServletResponse response){
						try{
							int idGerente = Integer.parseInt(request.getParameter("idGerente"));
							String nomGerente = request.getParameter("nomGerente");
							int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
							String nomUsuario = request.getParameter("nomUsuario");
							String Zona = request.getParameter("Zona");
							String Region = request.getParameter("Region");
							int idPerfil = Integer.parseInt(request.getParameter("idPerfil"));
							int idSecuencia = Integer.parseInt(request.getParameter("idSecuencia"));
							AltaGerenteDTO updateInfo = new AltaGerenteDTO();
		
							updateInfo.setIdGerente(idGerente);
							updateInfo.setNomGerente(nomGerente);
							updateInfo.setIdUsuario(idUsuario);
							updateInfo.setNomUsuario(nomUsuario);
							updateInfo.setZona(Zona);
							updateInfo.setRegion(Region);
							updateInfo.setIdPerfil(idPerfil);
							updateInfo.setIdSecuencia(idSecuencia);
							
							
							boolean respuesta = altaGerenteBI.actualizaDisSup(updateInfo);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update ID: "+idGerente+" : ");
							mv.addObject("res", respuesta);
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
		}
                                        
                                        
                                        
                                        //http://10.51.219.179:8080/checklist/catalogosService/updateComentarioAsgCalidad.json?ceco=<?>&fecha=<?>&comentario=<?>
					@RequestMapping(value="/updateComentarioAsgCalidad", method = RequestMethod.GET)
					public  ModelAndView updateComentarioAsgCalidad(HttpServletRequest request, HttpServletResponse response, Model model) {
						try {
                                                    							
                                                        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
                                                        

                                                        String ceco= request.getParameter("ceco");
                                                        String fecha= request.getParameter("fecha");
                                                        String comentario= request.getParameter("coementario");

							Boolean res = comentariosRepAsgBI.updateComentarios(ceco, fecha, comentario);
                                                        
							mv.addObject("tipo", "MODIFICA_COMENTARIOS_ASIG_CALIADAD");
							mv.addObject("res", res);

							return mv;

						} catch (Exception e) {
							logger.info("Ocurrio algo " + e);
							return null;
						}

					}
					// http://10.51.219.179:8080/catalogosService/updateHallazgoTransf.json?idHallazgo=&respuesta=&idResp=&status=&ruta=&obs=&fechaAut=&fechafin=&obsNueva=
					@RequestMapping(value="/updateHallazgoTransf", method = RequestMethod.GET)
					public ModelAndView updateHallazgoTransf(HttpServletRequest request, HttpServletResponse response){
						try{
							int idHallazgo= Integer.parseInt(request.getParameter("idHallazgo"));
							int idResp= Integer.parseInt(request.getParameter("idResp"));
							int status= Integer.parseInt(request.getParameter("status"));
							String respuesta= request.getParameter("respuesta");
							String ruta= request.getParameter("ruta");
							String obs= request.getParameter("obs");
							String obsNueva= request.getParameter("obsNueva");
							String fechaAut= request.getParameter("fechaAut");
							String fechafin= request.getParameter("fechafin");
							String usuModif=request.getParameter("usuModif");
							
							HallazgosTransfDTO h = new HallazgosTransfDTO();
							
							h.setIdHallazgo(idHallazgo);
							h.setIdResp(idResp);
							h.setStatus(status);
							h.setRespuesta(respuesta);
							h.setRuta(ruta);
							h.setObs(obs);
							h.setObsNueva(obsNueva);
							h.setFechaAutorizo(fechaAut);
							h.setFechaFin(fechafin);
							h.setUsuModif(usuModif);
							boolean res = hallazgosTransfBI.actualiza(h);
					
							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "Registro actualizado");
							mv.addObject("res", res);
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					// http://10.51.219.179:8080/checklist/catalogosService/updateHallazgoRespTransf.json?idHallazgo=&respuesta=&idResp=&status=&ruta=&obs=&fechaAut=&fechafin=&obsNueva=&motivrechazo=
					@RequestMapping(value="/updateHallazgoRespTransf", method = RequestMethod.GET)
					public ModelAndView updateHallazgoRespTransf(HttpServletRequest request, HttpServletResponse response){
						try{
							
							int idHallazgo= Integer.parseInt(request.getParameter("idHallazgo"));
							int idResp= Integer.parseInt(request.getParameter("idResp"));
							int status= Integer.parseInt(request.getParameter("status"));
							String respuesta= request.getParameter("respuesta");
							String ruta= request.getParameter("ruta");
							String obs= request.getParameter("obs");
							String obsNueva= request.getParameter("obsNueva");
							String fechaAut= request.getParameter("fechaAut");
							String fechafin= request.getParameter("fechafin");
							String motivrechazo=request.getParameter("motivrechazo");
							String usuModif=request.getParameter("usuModif");
							
							HallazgosTransfDTO h = new HallazgosTransfDTO();
							
							h.setIdHallazgo(idHallazgo);
							h.setIdResp(idResp);
							h.setStatus(status);
							h.setRespuesta(respuesta);
							h.setRuta(ruta);
							h.setObs(obs);
							h.setObsNueva(obsNueva);
							h.setFechaAutorizo(fechaAut);
							h.setFechaFin(fechafin);
							h.setMotivrechazo(motivrechazo);
							h.setUsuModif(usuModif);
						
							boolean res = hallazgosTransfBI.actualizaResp(h);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "Registro actualizado");
							mv.addObject("res", res);
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					// http://10.51.219.179:8080/checklist/catalogosService/updateHallazgoMovTransf.json?idHallazgo=2024&respuesta=2&idResp=3970&status=2&ruta=xx&obs=xx&fechaAut=&fechafin=&obsNueva=&usuModif=&motiRecha=&usuario=&ceco=&tipoEvi=
					@RequestMapping(value="/updateHallazgoMovTransf", method = RequestMethod.GET)
					public ModelAndView updateHallazgoMovTransf(HttpServletRequest request, HttpServletResponse response){
						try{
							
								int idHallazgo= Integer.parseInt(request.getParameter("idHallazgo"));
								int idResp= Integer.parseInt(request.getParameter("idResp"));
								int status= Integer.parseInt(request.getParameter("status"));
								String respuesta= request.getParameter("respuesta");
								String ruta= request.getParameter("ruta");
								String obs= request.getParameter("obs");
								String obsNueva= request.getParameter("obsNueva");
								String fechaAut= request.getParameter("fechaAut");
								String fechafin= request.getParameter("fechafin");
								String usuModif=request.getParameter("usuModif");
								String motiRecha=request.getParameter("motiRecha");
								int tipoEvi= Integer.parseInt(request.getParameter("tipoEvi"));
								String ceco= request.getParameter("ceco");
								int usuario= Integer.parseInt(request.getParameter("usuario"));
								
								HallazgosTransfDTO h = new HallazgosTransfDTO();
								
								h.setIdHallazgo(idHallazgo);
								h.setIdResp(idResp);
								h.setStatus(status);
								h.setRespuesta(respuesta);
								h.setRuta(ruta);
								h.setObs(obs);
								h.setObsNueva(obsNueva);
								h.setFechaAutorizo(fechaAut);
								h.setFechaFin(fechafin);
								h.setUsuModif(usuModif);
								h.setMotivrechazo(motiRecha);
								h.setCeco(ceco);
								h.setTipoEvi(tipoEvi);
								h.setUsuario(usuario);
								
								Map<String,Object> res = hallazgosTransfBI.actualizaMov(h);

								ModelAndView mv = new ModelAndView("muestraServicios");
								mv.addObject("tipo", "ValidaActualizaHAlla");
								mv.addObject("res", res);

							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/updateTransf.json?ceco=<?>&idUsuario=<?>&bitacora=<?>&recorrido=<?>&fechaini=<?>&fechafin=<?>&aux=<?>
					@RequestMapping(value="/updateTransf", method = RequestMethod.GET)
					public ModelAndView updateTransf(HttpServletRequest request, HttpServletResponse response){
						
							int ceco=Integer.parseInt(request.getParameter("ceco"));
							int idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
							int bitacora=Integer.parseInt(request.getParameter("bitacora"));
							int recorrido=Integer.parseInt(request.getParameter("recorrido"));
							String fechaini=request.getParameter("fechaini");
							String fechafin=request.getParameter("fechafin");
							String aux=request.getParameter("aux");

							TransformacionDTO so = new TransformacionDTO();
							
							so.setCeco(ceco);
							so.setIdUsuario(idUsuario);
							so.setBitacora(bitacora);
							so.setRecorrido(recorrido);
							so.setFechaini(fechaini);
							so.setFechafin(fechafin);
							so.setAux(aux);
							
							
							boolean respuesta = transformacionBI.actualiza(so);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update  transf con ceco: "+ceco+" : ");
							mv.addObject("res", respuesta);
							
							return mv;
						
					}
					
					//http://localhost:8080/checklist/catalogosService/updateFaseTransf.json?idTabla=&idProyecto=<?>&fase=<?>&version=<?>&obs=<?>
					@RequestMapping(value="/updateFaseTransf", method = RequestMethod.GET)
					public ModelAndView updateFaseTransf(HttpServletRequest request, HttpServletResponse response){
						try{
							int idTabla=Integer.parseInt(request.getParameter("idTabla"));
							int idProyecto=Integer.parseInt(request.getParameter("idProyecto"));
							int fase=Integer.parseInt(request.getParameter("fase"));
							int version=Integer.parseInt(request.getParameter("version"));
							String obs=request.getParameter("obs");

							FaseTransfDTO ver = new FaseTransfDTO();
							ver.setIdTab(idTabla);
							ver.setProyecto(idProyecto);
							ver.setFase(fase);
							ver.setVersion(version);
							ver.setObs(obs);
							boolean respuesta = faseTransfBI.actualiza(ver);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update   proyecto: "+idProyecto+" : con la fase: "+fase+"");
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://10.51.219.179:8080/checklist/catalogosService/actualizaInforme.json?idsec=22&ceco=480100&usuario=82144&fecha=20200210 01:18:00&ruta=RUTA DE INFORME&lider=NOMBRE DEL LIDER&firmaLider=RUTA FIRMA DEL LIDER&numLider=73433&firmaFrq=FIRMA ASEGURADOR
					@RequestMapping(value="/actualizaInforme", method = RequestMethod.GET)
					public ModelAndView actualizaInforme(HttpServletRequest request, HttpServletResponse response){
							try{
								String idsec=request.getParameter("idsec");
								String ceco=request.getParameter("ceco");
								String usuario=request.getParameter("usuario");
								String fecha=request.getParameter("fecha");
								String ruta=request.getParameter("ruta");
								String lider = request.getParameter("lider");
								String firmaLider = request.getParameter("firmaLider");
								String numLider = request.getParameter("numLider");
								String firmaFrq = request.getParameter("firmaFrq");
								
								AdmInformProtocolosDTO admInfProto = new AdmInformProtocolosDTO();
								admInfProto.setIdsec(idsec);
								admInfProto.setCeco(ceco);
								admInfProto.setUsuario(usuario);
								admInfProto.setFecha(fecha);
								admInfProto.setRuta(ruta); 
								admInfProto.setLider(lider);
								admInfProto.setFirmaLider(firmaLider);
								admInfProto.setNumLider(numLider);
								admInfProto.setFirmaFrq(firmaFrq);

								boolean respuesta = admInformProtocolosBI.actualizaInformProtocolos(admInfProto);

								ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
								mv.addObject("tipo", "Update detalle Informe");
								mv.addObject("res", respuesta);
								
								return mv;
							}catch(Exception e){
								logger.info(e);
								return null;
							}
						}
					
					//http://localhost:8080/checklist/catalogosService/actualizaAsignaTransf.json?ceco=<?>&idProyecto=<?>&usuario=<?>&usuario_asig=<?>&obs=<?>&status=&idAgrupa=&idOrden=
					@RequestMapping(value="/actualizaAsignaTransf", method = RequestMethod.GET)
					public ModelAndView actualizaAsignaTransf(HttpServletRequest request, HttpServletResponse response){
						try{
							int ceco=Integer.parseInt(request.getParameter("ceco"));
							int idProyecto=0;
							if (request.getParameter("idProyecto") != null && request.getParameter("idProyecto").length() <= 0) {
								idProyecto = 0;
							} else {
								idProyecto = Integer.parseInt(request.getParameter("idProyecto"));
							}
							int usuario=0;
							if (request.getParameter("usuario") != null && request.getParameter("usuario").length() <= 0) {
								usuario = 0;
							} else {
								usuario = Integer.parseInt(request.getParameter("usuario"));
							}	
							
							int usuario_asig=0;
							if (request.getParameter("usuario_asig") != null && request.getParameter("usuario_asig").length() <= 0) {
								usuario_asig = 0;
							} else {
								usuario_asig = Integer.parseInt(request.getParameter("usuario_asig"));
							}	
							
							int status=0;
							if (request.getParameter("status") != null && request.getParameter("status").length() <= 0) {
								status = 0;
							} else {
								status = Integer.parseInt(request.getParameter("status"));
							}
							String nombreProyecto=request.getParameter("nombreProyecto");
							int idAgrupa=0;
							if (request.getParameter("idAgrupa") != null && request.getParameter("idAgrupa").length() <= 0) {
								idAgrupa = 0;
							} else {
								idAgrupa = Integer.parseInt(request.getParameter("idAgrupa"));
							}
							String idOrden = request.getParameter("idOrden");

							AsignaTransfDTO asig = new AsignaTransfDTO();
							
							asig.setCeco(ceco);
							asig.setProyecto(idProyecto);
							asig.setUsuario_asig(usuario_asig);
							asig.setUsuario(usuario);
							asig.setObs(nombreProyecto);
							asig.setIdestatus(status);
							asig.setAgrupador(idAgrupa);
							asig.setIdOrden(idOrden);
							 
							
							boolean respuesta = asignaTransfBI.actualiza(asig);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update   con ceco: "+ceco+" : ");
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/actualizaProyectoTransf.json?nombre=<?>&obs=<?>
					@RequestMapping(value="/actualizaProyectoTransf", method = RequestMethod.GET)
					public ModelAndView actualizaProyectoTransf(HttpServletRequest request, HttpServletResponse response){
						try{
							
							String nombre=request.getParameter("nombre");
							String obs=request.getParameter("obs");

							ProyectFaseTransfDTO pro = new ProyectFaseTransfDTO();
							pro.setNomProy(nombre);
							pro.setObsProy(obs);
							boolean respuesta = proyectFaseTransfBI.actualizaProyec(pro);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update   con idproyecto: "+nombre+" : ");
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/actualizaAgrupaFase.json?idTabla=&idAgrupa=<?>&fase=<?>&idactivo=<?>&obs=<?>&aux=<?>&ordenFase=
					@RequestMapping(value="/actualizaAgrupaFase", method = RequestMethod.GET)
					public ModelAndView actualizaAgrupaFase(HttpServletRequest request, HttpServletResponse response){
						try{
							int idTabla=Integer.parseInt(request.getParameter("idTabla"));
							int idAgrupa=Integer.parseInt(request.getParameter("idAgrupa"));
							int fase=Integer.parseInt(request.getParameter("fase"));
							int idactivo=Integer.parseInt(request.getParameter("idactivo"));
							int ordenFase=Integer.parseInt(request.getParameter("ordenFase"));
							String obs=request.getParameter("obs");
							String aux=request.getParameter("aux");

							FaseTransfDTO ver = new FaseTransfDTO();
							ver.setIdTab(idTabla);
							ver.setIdAgrupa(idAgrupa);
							ver.setFase(fase);
							ver.setIdactivo(idactivo);
							ver.setOrdenFase(ordenFase);
							ver.setObs(obs);
							ver.setAux(aux);
							
				
							boolean respuesta = faseTransfBI.actualizaAgrupa(ver);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "Alta   fase: "+fase+" con agrupador: "+idAgrupa+" : ");
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/actualizaAsignaTransfNvo.json?ceco=<?>&idProyecto=<?>&usuario=<?>&usuario_asig=<?>&obs=<?>&status=&idAgrupa=&idOrden=
					@RequestMapping(value="/actualizaAsignaTransfNvo", method = RequestMethod.GET)
					public ModelAndView actualizaAsignaTransfNvo(HttpServletRequest request, HttpServletResponse response){
						try{
							int ceco=Integer.parseInt(request.getParameter("ceco"));
							int idProyecto=0;
							if (request.getParameter("idProyecto") != null && request.getParameter("idProyecto").length() <= 0) {
								idProyecto = 0;
							} else {
								idProyecto = Integer.parseInt(request.getParameter("idProyecto"));
							}
							
							int desbFase=0;
							if (request.getParameter("desbFase") != null && request.getParameter("desbFase").length() <= 0) {
								desbFase = 0;
							} else {
								desbFase = Integer.parseInt(request.getParameter("desbFase"));
							}	
							
							int status=0;
							if (request.getParameter("status") != null && request.getParameter("status").length() <= 0) {
								status = 0;
							} else {
								status = Integer.parseInt(request.getParameter("status"));
							}
							String nombreProyecto=request.getParameter("nombreProyecto");
							int idAgrupa=0;
							if (request.getParameter("idAgrupa") != null && request.getParameter("idAgrupa").length() <= 0) {
								idAgrupa = 0;
							} else {
								idAgrupa = Integer.parseInt(request.getParameter("idAgrupa"));
							}
							String idOrden = request.getParameter("idOrden");

							AsignaTransfDTO asig = new AsignaTransfDTO();
							
							asig.setCeco(ceco);
							asig.setProyecto(idProyecto);
							asig.setBanderaDesbloqueoFase(desbFase);
							asig.setObs(nombreProyecto);
							asig.setIdestatus(status);
							asig.setAgrupador(idAgrupa);
							asig.setIdOrden(idOrden);
							
							boolean respuesta = asignaTransfBI.actualizaN(asig);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update   con ceco: "+ceco+" : ");
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/actualizaFirmaCatalogo.json?idFirmaCatalogo=&nombreFirma=&tipoProy&aux=
					@RequestMapping(value="/actualizaFirmaCatalogo", method = RequestMethod.GET)
					public ModelAndView actualizaFirmaCatalogo(HttpServletRequest request, HttpServletResponse response){
						try{
							int idFirmaCatalogo=Integer.parseInt(request.getParameter("idFirmaCatalogo"));
							String nombreFirma=request.getParameter("nombreFirma");
							int tipoProy=Integer.parseInt(request.getParameter("tipoProy"));
							String aux=request.getParameter("aux");
							
							AdmFirmasCatDTO firm = new AdmFirmasCatDTO();
							firm.setIdFirmaCatalogo(idFirmaCatalogo);
							firm.setNombreFirmaCatalogo(nombreFirma);
							firm.setTipoProyecto(tipoProy);
							firm.setAux2(aux);
						
							
							boolean respuesta = firmasCatalogoBI.actualizaFirmaCat(firm);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "Alta de Firma :  "+nombreFirma);
							mv.addObject("res id firma: -->", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					//idFirmaAgrupa  es la del agrupador el id de firmacatalogo este se puede repetir y idFirmaCatalogo es el id de la tabla agrupador
					//http://localhost:8080/checklist/catalogosService/actualizaFirmaAgrupa.json?idFirmaCatalogo=&idFirmaAgrupa=&cargo=&obligatorio=&ordenFirma=&aux=
					@RequestMapping(value="/actualizaFirmaAgrupa", method = RequestMethod.GET)
					public ModelAndView actualizaFirmaAgrupa(HttpServletRequest request, HttpServletResponse response){
						try{
							int idFirmaCatalogo=Integer.parseInt(request.getParameter("idFirmaCatalogo"));
							int idFirmaAgrupa=Integer.parseInt(request.getParameter("idFirmaAgrupa"));
							String cargo=request.getParameter("cargo");
							int obligatorio=Integer.parseInt(request.getParameter("obligatorio"));
							int ordenFirma=Integer.parseInt(request.getParameter("ordenFirma"));
							String aux=request.getParameter("aux");
							
							AdmFirmasCatDTO firm = new AdmFirmasCatDTO();
							firm.setIdAgrupaFirma(idFirmaAgrupa);
							firm.setIdFirmaCatalogo(idFirmaCatalogo);
							firm.setCargo(cargo);
							firm.setObligatorio(obligatorio);
							firm.setOrden(ordenFirma);
							firm.setAux2(aux);
						
							
							boolean respuesta = firmasCatalogoBI.actualizaFirmaAgrupa(firm);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "Alta de Firma con agrupador :  "+idFirmaCatalogo);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/actualizaFragmentCatalogo.json?idFragment=&nombreFragment=&parametro=&numFragment=
					@RequestMapping(value="/actualizaFragmentCatalogo", method = RequestMethod.GET)
					public ModelAndView actualizaFragmentCatalogo(HttpServletRequest request, HttpServletResponse response){
						try{
							
							int idFragment=Integer.parseInt(request.getParameter("idFragment"));
							String nombreFragment=request.getParameter("nombreFragment");
							String parametro=request.getParameter("parametro");
							int numFragment=Integer.parseInt(request.getParameter("numFragment"));
							
							FragmentMenuDTO fra = new FragmentMenuDTO();
							fra.setIdFragment(idFragment);
							fra.setNombreFragment(nombreFragment);
							fra.setParametro(parametro);
							fra.setNumFragment(numFragment);
							
							boolean respuesta = fragmentMenuBI.actualizaFragment(fra);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "Alta de Fragment :  "+nombreFragment);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/actualizaMenuOpcionesP.json?idConfigMenu=&titulo=&idPerfil=&ordenMenu=&aux=
					@RequestMapping(value="/actualizaMenuOpcionesP", method = RequestMethod.GET)
					public ModelAndView actualizaMenuOpcionesP(HttpServletRequest request, HttpServletResponse response){
						try{
							int idConfigMenu=Integer.parseInt(request.getParameter("idConfigMenu"));
							String titulo=request.getParameter("titulo");
							int idPerfil=Integer.parseInt(request.getParameter("idPerfil"));
							int ordenMenu=Integer.parseInt(request.getParameter("ordenMenu"));
							String aux=request.getParameter("aux");
							
							FragmentMenuDTO men = new FragmentMenuDTO();
							men.setIdConfigMenu(idConfigMenu);
							men.setTituloMenu(titulo);
							men.setIdPerfil(idPerfil);
							men.setOrdenMenu(ordenMenu);
							men.setAux(aux);
							
							
							boolean respuesta = fragmentMenuBI.actualizaMenu(men);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", " ACTUALIZO  menu con nombre :  "+titulo);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/actualizaConfigProyecto.json?idConfigProyecto=&idConfMenu=&idFragment=&ordenFrag=&idAgrupaFirma=&parametro=&aux=
					@RequestMapping(value="/actualizaConfigProyecto", method = RequestMethod.GET)
					public ModelAndView actualizaConfigProyecto(HttpServletRequest request, HttpServletResponse response){
						try{
							
							int idConfigProyecto=Integer.parseInt(request.getParameter("idConfigProyecto"));
							int idConfMenu=Integer.parseInt(request.getParameter("idConfMenu"));
							int idFragment=Integer.parseInt(request.getParameter("idFragment"));
							int ordenFrag=Integer.parseInt(request.getParameter("ordenFrag"));
							int idAgrupaFirma=Integer.parseInt(request.getParameter("idAgrupaFirma"));
							String parametro=request.getParameter("parametro");
							String aux=request.getParameter("aux");
							
							FragmentMenuDTO conf = new FragmentMenuDTO();
							conf.setIdConfigProyecto(idConfigProyecto);
							conf.setIdConfigMenu(idConfMenu);
							conf.setIdFragment(idFragment);
							conf.setOrdenFragment(ordenFrag);
							conf.setIdAgrupaFirma(idAgrupaFirma);
							conf.setParametro(parametro);
							conf.setAux(aux);
					
							
							boolean respuesta = fragmentMenuBI.actualizaConf(conf);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update de config con idconfiguracionMenu :  "+idConfMenu);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					//http://localhost:8080/checklist/catalogosService/actualizaSoftProyecto.json?idSoft=&recorridoId=&idFase=&idProyecto=&bitacora=&faseActual=
					@RequestMapping(value="/actualizaSoftProyecto", method = RequestMethod.GET)
					public ModelAndView actualizaSoftProyecto(HttpServletRequest request, HttpServletResponse response){
						try{
							int idSoft=Integer.parseInt(request.getParameter("idSoft"));
							String recorridoId=request.getParameter("recorridoId");
							int idFase=Integer.parseInt(request.getParameter("idFase"));
							int idProyecto=Integer.parseInt(request.getParameter("idProyecto"));
							int bitacora=Integer.parseInt(request.getParameter("bitacora"));
							int faseActual=Integer.parseInt(request.getParameter("faseActual"));
							
							SoftNvoDTO soft = new SoftNvoDTO();
							soft.setIdSoft(idSoft);
							soft.setCeco(recorridoId);
							soft.setIdFase(idFase);
							soft.setIdProyecto(idProyecto);
							soft.setBitacora(bitacora);
							soft.setIdUsuario(faseActual);
							
						
							boolean respuesta = softNvoBI.actualizaSoftN(soft);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update de proyecto: "+idProyecto+"  con idSoft :  "+idSoft);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
	//http://localhost:8080/checklist/catalogosService/actualizaCalificacionesActa.json?idCalculos=&idSoft=&clasificacion=&totalSi=&totalNo=&totalNA=&totalRevisados=&totalItems=&totImperdonables=&porcPondSi=&porcPondNo=&porcPondNA=&pondMaxReal=&pondObtCal=&calificacionCalc=
					@RequestMapping(value="/actualizaCalificacionesActa", method = RequestMethod.GET)
					public ModelAndView actualizaCalificacionesActa(HttpServletRequest request, HttpServletResponse response){
						try{
							
							int idCalculos=Integer.parseInt(request.getParameter("idCalculos"));
							int idSoft=Integer.parseInt(request.getParameter("idSoft"));
							String clasificacion=request.getParameter("clasificacion");
							int totalSi=Integer.parseInt(request.getParameter("totalSi"));
							int totalNo=Integer.parseInt(request.getParameter("totalNo"));
							int totalNA=Integer.parseInt(request.getParameter("totalNA"));
							int totalRevisados=Integer.parseInt(request.getParameter("totalRevisados"));
							int totalItems=Integer.parseInt(request.getParameter("totalItems"));
							int totImperdonables=Integer.parseInt(request.getParameter("totImperdonables"));
							double porcPondSi=Double.parseDouble(request.getParameter("porcPondSi"));
							double porcPondNo=Double.parseDouble(request.getParameter("porcPondNo"));
							double porcPondNA=Double.parseDouble(request.getParameter("porcPondNA"));
							double pondMaxReal=Double.parseDouble(request.getParameter("pondMaxReal"));
							double pondObtCal=Double.parseDouble(request.getParameter("pondObtCal"));
							double calificacionCalc=Double.parseDouble(request.getParameter("calificacionCalc"));
							
							SoftNvoDTO soft = new SoftNvoDTO();
							soft.setIdCalculo(idCalculos);
							soft.setIdSoft(idSoft);
							soft.setClasificacion(clasificacion);
							soft.setItemSi(totalSi);
							soft.setItemNo(totalNo);
							soft.setItemNa(totalNA);
							soft.setItemrevisados(totalRevisados);
							soft.setItemTotales(totalItems);
							soft.setItemImperd(totImperdonables);
							soft.setPorcentajeReSi(porcPondSi);
							soft.setPorcentajeReNo(porcPondNo);
							soft.setPorcentajeToNA(porcPondNA);
							soft.setPonderacionMaximaReal(pondMaxReal);
							soft.setPonderacionObtenidaCalculada(pondObtCal);
							soft.setCalificacionCalculada(calificacionCalc);
							
							boolean respuesta = softNvoBI.actualizaCalculosActa(soft);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update de calculo  proyecto:");
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/actualizaActorHallazgo.json?idActor=&nombre=&obs=&aux=&status=&idPerfil=&idConfig=
					@RequestMapping(value="/actualizaActorHallazgo", method = RequestMethod.GET)
					public ModelAndView actualizaActorHallazgo(HttpServletRequest request, HttpServletResponse response){
						try{
							int idActor=Integer.parseInt(request.getParameter("idActor"));
							String nombre=request.getParameter("nombre");
							String obs=request.getParameter("obs");
							String aux=request.getParameter("aux");
							int status=Integer.parseInt(request.getParameter("status"));
							int idPerfil=Integer.parseInt(request.getParameter("idPerfil"));
							int idConfig=Integer.parseInt(request.getParameter("idConfig"));
								
							ActorEdoHallaDTO actor = new ActorEdoHallaDTO();
							actor.setIdActorConfig(idActor);
							actor.setNombreActor(nombre);
							actor.setObservacion(obs);
							actor.setAux(aux);
							actor.setStatus(status);		
							actor.setIdPerfil(idPerfil);
							actor.setIdConfiguracion(idConfig);
							
							actor.setStatus(status);					
							
							boolean respuesta = actorEdoHallaBI.actualizaActorHalla(actor);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update de actot: "+idActor+"  con status :  "+status);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/actualizaGpoEdoHallazgo.json?idGpoEdoHallazgo=&nombre=&idConfig=&obs=&aux=&status=&color=
					@RequestMapping(value="/actualizaGpoEdoHallazgo", method = RequestMethod.GET)
					public ModelAndView actualizaGpoEdoHallazgo(HttpServletRequest request, HttpServletResponse response){
						try{
							int idGpoEdoHallazgo=Integer.parseInt(request.getParameter("idGpoEdoHallazgo"));
							String nombre=request.getParameter("nombre");
							int status=Integer.parseInt(request.getParameter("status"));
							int idConfig=Integer.parseInt(request.getParameter("idConfig"));
							String obs=request.getParameter("obs");
							String aux=request.getParameter("aux");
							String color=request.getParameter("color");
								
							ActorEdoHallaDTO actor = new ActorEdoHallaDTO();
							actor.setIdEdoHallazgo(idGpoEdoHallazgo);
							actor.setNombStatus(nombre);
							actor.setStatus(status);
							actor.setIdConfiguracion(idConfig);
							actor.setObservacion(obs);
							actor.setAux(aux);		
							actor.setColor(color);
						
							boolean respuesta = actorEdoHallaBI.actualizaConfEdoHalla(actor);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update de idConfig: "+idConfig+"  con status :  "+status);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					//http://localhost:8080/checklist/catalogosService/actualizaConfigActor.json?idHallazgoConf=&idConfigActor=&statusAccion=&accion=&bandAtencion=&idConfig=&aux=&statusActuacion=
					@RequestMapping(value="/actualizaConfigActor", method = RequestMethod.GET)
					public ModelAndView actualizaConfigActor(HttpServletRequest request, HttpServletResponse response){
						try{
							
							int idHallazgoConf=Integer.parseInt(request.getParameter("idHallazgoConf"));
							int idConfigActor=Integer.parseInt(request.getParameter("idConfigActor"));
							int statusAccion=Integer.parseInt(request.getParameter("statusAccion"));
							int statusActuacion=Integer.parseInt(request.getParameter("statusActuacion"));
							String accion=request.getParameter("accion");
							int bandAtencion=Integer.parseInt(request.getParameter("bandAtencion"));
							int idConfig=Integer.parseInt(request.getParameter("idConfig"));
							String obs=request.getParameter("obs");
							String aux=request.getParameter("aux");
								
							ConfiguraActorDTO actor = new ConfiguraActorDTO();
							actor.setIdHallazgoConf(idHallazgoConf);
							actor.setIdConfigActor(idConfigActor);
							actor.setStatusAccion(statusAccion);
							actor.setStatusActuacion(statusActuacion);
							actor.setAccion(accion);
							actor.setBanderaAtencion(bandAtencion);
							actor.setIdConfig(idConfig);
							actor.setObs(obs);
							actor.setAux(aux);			
		                     
							boolean respuesta = configuraActorBI.actualizaConfActor(actor);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "actualiza de idConfighalla:  "+idHallazgoConf+"  con statusAcccion :  "+statusAccion);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/actualizaProyectoTransfNvo.json?idProyecto=&nombre=<?>&obs=<?>&edoCargaInicial=&tipoProyecto=&maxHallazgo=&status=
					@RequestMapping(value="/actualizaProyectoTransfNvo", method = RequestMethod.GET)
					public ModelAndView actualizaProyectoTransfNvo(HttpServletRequest request, HttpServletResponse response){
						try{
							int idProyecto=Integer.parseInt(request.getParameter("idProyecto"));
							String nombre=request.getParameter("nombre");
							String obs=request.getParameter("obs");
							int edoCargaInicial=Integer.parseInt(request.getParameter("edoCargaInicial"));
							int tipoProyecto=Integer.parseInt(request.getParameter("tipoProyecto"));
							int maxHallazgo=Integer.parseInt(request.getParameter("maxHallazgo"));
							int status=Integer.parseInt(request.getParameter("status"));
							

							ProyectFaseTransfDTO pro = new ProyectFaseTransfDTO();
							pro.setIdProyecto(idProyecto);
							pro.setNomProy(nombre);
							pro.setObsProy(obs);
							pro.setEdoCargaIni(edoCargaInicial);
							pro.setIdTipoProyecto(tipoProyecto);
							pro.setHallazgoEdoMax(maxHallazgo);
							pro.setIdStatus(status);
							boolean respuesta = proyectFaseTransfBI.actualizaProyecNvo(pro);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update   con idproyecto: "+nombre+" : ");
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/actualizaMatrizHallazgo.json?idMatriz=&tipoProyecto=&statusEnvio=&statusHallazgo=&perfil=&color=&nombreStatus=
					@RequestMapping(value="/actualizaMatrizHallazgo", method = RequestMethod.GET)
					public ModelAndView actualizaMatrizHallazgo(HttpServletRequest request, HttpServletResponse response){
						try{
							int idMatriz=Integer.parseInt(request.getParameter("idMatriz"));
	                        String tipoProyecto=request.getParameter("tipoProyecto");
							int statusEnvio=Integer.parseInt(request.getParameter("statusEnvio"));
							int statusHallazgo=Integer.parseInt(request.getParameter("statusHallazgo"));
							int perfil=Integer.parseInt(request.getParameter("perfil"));
							String color=request.getParameter("color");
							String nombreStatus=request.getParameter("nombreStatus");
								
							HallazgosTransfDTO matriz = new HallazgosTransfDTO();
							matriz.setIdMatriz(idMatriz);
							matriz.setTipoProy(tipoProyecto);
							matriz.setStatusEnvio(statusEnvio);
							matriz.setStatusHallazgo(statusHallazgo);
							matriz.setIdPerfil(perfil);
							matriz.setColor(color);
							matriz.setNombreStatus(nombreStatus);
		                     
							boolean respuesta = hallagosMatrizBI.actualizaHallazgoMatriz(matriz);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "modifico  idmatriz: "+idMatriz);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/actualizaHallazgoFase.json?ceco=&fecha=&fase=
					@RequestMapping(value="/actualizaHallazgoFase", method = RequestMethod.GET)
					public ModelAndView actualizaHallazgoFase(HttpServletRequest request, HttpServletResponse response){
						try{
							
	                        String ceco=request.getParameter("ceco");
							String fecha=request.getParameter("fecha");
							String fase=null;
							fase=request.getParameter("fase");
								
							HallazgosTransfDTO actfase = new HallazgosTransfDTO();
							actfase.setCeco(ceco);
							actfase.setPeriodo(fecha);
							actfase.setFase(fase);
		                     
							boolean respuesta = soporteHallazgosTransfBI.actualizaFaseHallazgos(actfase);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "modifico de ceco: "+ceco+"con fase  "+fase);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					//http://localhost:8080/checklist/catalogosService/actualizaFechaSoftNvo.json?bitacora=
					@RequestMapping(value="/actualizaFechaSoftNvo", method = RequestMethod.GET)
					public ModelAndView actualizaFechaSoftNvo(HttpServletRequest request, HttpServletResponse response){
						try{
							String bitacora = request.getParameter("bitacora");
								
							SoporteExpDTO sop = new SoporteExpDTO();
		
							sop.setBitacora(bitacora);
							boolean respuesta = soporteExpBI.actualizaFechaSoftNvo(sop);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "update  softnvo con bitacora "+bitacora+"");
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/actualizaCheckInAsistencia.json?idAsistencia=&idUsuario=&latitud=&longitud=&fecha=&ruta=&observaciones=&lugar=&tipoCheckIn=&tipoProyecto=
					@RequestMapping(value="/actualizaCheckInAsistencia", method = RequestMethod.GET)
					public ModelAndView actualizaCheckInAsistencia(HttpServletRequest request, HttpServletResponse response){
						try{

							
							int idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
							double latitud=Double.parseDouble(request.getParameter("latitud"));
							double longitud=Double.parseDouble(request.getParameter("longitud"));
							String fecha=request.getParameter("fecha");
							String ruta=request.getParameter("ruta");
							String observaciones=request.getParameter("observaciones");
							String lugar=request.getParameter("lugar");
							//entrada 0 salida 1
							int tipoCheckIn=Integer.parseInt(request.getParameter("tipoCheckIn"));
							int tipoProyecto=Integer.parseInt(request.getParameter("tipoProyecto"));
							int idAsistencia=Integer.parseInt(request.getParameter("idAsistencia"));
						
							CheckInAsistenciaDTO asist = new CheckInAsistenciaDTO();
							asist.setIdUsuario(idUsuario);
							asist.setLatitud(latitud);
							asist.setLongitud(longitud);
							asist.setFecha(fecha);
							asist.setRuta(ruta);
							asist.setObservaciones(observaciones);
							asist.setLugar(lugar);
							asist.setTipoCheckIn(tipoCheckIn);
							asist.setIdTipoProyecto(tipoProyecto);
							asist.setIdAsistencia(idAsistencia);
							
							
							boolean  respuesta = checkInAsistenciaBI.actualizaAsistencia(asist);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "actualiza Asistencia de usuario: "+idUsuario+"  con tipo checkInAsistencia :  (0 entrada 1 Salida)-->"+tipoCheckIn);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					//http://localhost:8080/checklist/catalogosService/actualizaProgramacionMtto.json?idUsuario=&fechaProgramacion=&comentario=&idProgramacion=
					@RequestMapping(value="/actualizaProgramacionMtto", method = RequestMethod.GET)
					public ModelAndView actualizaProgramacionMtto(HttpServletRequest request, HttpServletResponse response){
						try{
			
							int idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
							//DDMMYY
							String fechaProgramacion =request.getParameter("fechaProgramacion");
							String comentario=request.getParameter("comentario");
							int idProgramacion=Integer.parseInt(request.getParameter("idProgramacion"));
						
	                          ProgramacionMttoDTO asist = new ProgramacionMttoDTO();
							asist.setIdUsuario(idUsuario);
							asist.setFechaProgramacion(fechaProgramacion);
							asist.setIdProgramacion(idProgramacion);
							asist.setComentarioReprogramacion(comentario);
							
							boolean  respuesta = checkInAsistenciaBI.actualizaProgramacion(asist);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "actualiza Asistencia de usuario: "+idUsuario+" con programacion-->"+idProgramacion);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					// http://10.53.33.83/checklist/catalogosService/updateAdicionalHallazgo.json?idHallazgo=&respuesta=&usuModif=&ruta=&obs=
					@RequestMapping(value="/updateAdicionalHallazgo", method = RequestMethod.GET)
					public ModelAndView updateAdicionalHallazgo(HttpServletRequest request, HttpServletResponse response){
						try{
							int idHallazgo= Integer.parseInt(request.getParameter("idHallazgo"));
							String respuesta= request.getParameter("respuesta");
							String usuModif=request.getParameter("usuModif");
							String ruta= request.getParameter("ruta");
							String obs= request.getParameter("obs");
							
							HallazgosTransfDTO h = new HallazgosTransfDTO();
							
							h.setIdHallazgo(idHallazgo);
			
							h.setRespuesta(respuesta);
							h.setRuta(ruta);
							h.setObs(obs);
							h.setUsuModif(usuModif);
							boolean res = hallazgosTransfBI.actualizaAdicionalHalla(h);
					
							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "Registro actualizado");
							mv.addObject("res", res);
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
					
					//http://localhost:8080/checklist/catalogosService/actualizaDatosInforme.json?eliminaDatosIdInforme=&idCeco=&idUsuario=&idProtocolo=&imperdonables=&calificacion=&totalRespuestas=&fecha=DDMMYY HH:MM:SS
					@RequestMapping(value="/actualizaDatosInforme", method = RequestMethod.GET)
					public ModelAndView actualizaDatosInforme(HttpServletRequest request, HttpServletResponse response){
						try{
		
							
							String fecha=request.getParameter("fecha");
							int idCeco=Integer.parseInt(request.getParameter("idCeco"));
							int idInforme=Integer.parseInt(request.getParameter("idInforme"));
							int idUsuario=Integer.parseInt(request.getParameter("idUsuario"));
							int idProtocolo=Integer.parseInt(request.getParameter("idProtocolo"));
							int imperdonables=Integer.parseInt(request.getParameter("imperdonables"));
							double calificacion=Double.parseDouble(request.getParameter("calificacion"));
							int totalRespuestas=Integer.parseInt(request.getParameter("totalRespuestas"));
							
							
							DatosInformeDTO informe = new DatosInformeDTO();
							informe.setIdCeco(idCeco);
							informe.setIdUsuario(idUsuario);
							informe.setIdProtocolo(idProtocolo);
							informe.setImperdonables(imperdonables);
							informe.setTotalRespuestas(totalRespuestas);
							informe.setCalificacion(calificacion);
							informe.setFecha(fecha);
							informe.setIdInforme(idInforme);
							
							boolean respuesta = datosInformeBI.actualizaDatosInforme(informe);

							ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
							mv.addObject("tipo", "Actualizo id informe-->"+idInforme+"  de ceco: "+idCeco+"  con usuario : "+idUsuario);
							mv.addObject("res", respuesta);
							
							return mv;
						}catch(Exception e){
							logger.info(e);
							return null;
						}
					}
}
