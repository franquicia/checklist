package com.gruposalinas.checklist.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.domain.Login;
import com.gruposalinas.checklist.domain.LoginTokenDTO;
import com.gruposalinas.checklist.domain.UploadArchive;
import com.gruposalinas.checklist.domain.UsuarioADN;
import com.gruposalinas.checklist.resources.FRQAuthInterceptor;
import com.gruposalinas.checklist.util.CleanParameter;
import com.gruposalinas.checklist.util.UtilFRQ;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Controller
public class InicioController {

	private static final Logger logger = LogManager.getLogger(InicioController.class);

	
	@Autowired
	private CleanParameter UtilString;


	@RequestMapping(value = "mensajes.htm", method = RequestMethod.GET)
	public String mensajes(HttpServletRequest request, Exception ex) {
		logger.info("ENTRO A MENSAJES.HTM");
		return "redirect:tienda/inicio.htm?"+request.getQueryString();
	}
	
	@RequestMapping(value = {"tienda/inicio.htm", "central/inicio.htm"}, method = RequestMethod.GET)
	public ModelAndView metodoRedirect(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "usuario", required = false, defaultValue = "vacio") String usuario,
			@RequestParam(value = "password", required = false, defaultValue = "") String password,
			@RequestParam(value = "numempleado", required = false, defaultValue = "") String numempleado,
			@RequestParam(value = "ws", required = false, defaultValue = "") String ws,
			@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
			@RequestParam(value = "apellidop", required = false, defaultValue = "") String apellidop,
			@RequestParam(value = "apellidom", required = false, defaultValue = "") String apellidom,
			@RequestParam(value = "sucursal", required = false, defaultValue = "") String sucursal,
			@RequestParam(value = "nomsucursal", required = false, defaultValue = "") String nomsucursal,
			@RequestParam(value = "puesto", required = false, defaultValue = "") String puesto,
			@RequestParam(value = "descpuesto", required = false, defaultValue = "") String descpuesto,
			@RequestParam(value = "SAP", required = false, defaultValue = "") String sap,
			@RequestParam(value = "pais", required = false, defaultValue = "") String pais,
			@RequestParam(value = "canal", required = false, defaultValue = "") String canal,
			@RequestParam(value = "servidor", required = false, defaultValue = "") String servidor,
			@RequestParam(value = "puestobase", required = false, defaultValue = "") String puestobase)
			throws ServletException, IOException {
		
		String salida = "mensajes";
		ModelAndView mv = new ModelAndView(salida);
		
		if ( usuario != "vacio" && request.getRequestURI().contains("tienda/inicio.htm")){
			// Creo un Usuario ADN para almacenar la informacion		
			UsuarioADN userADN = new UsuarioADN();
			userADN.setIdUsuario(usuario);
			//logger.info("Id Usuario--> " + userADN.getIdUsuario());
			userADN.setPassword(UtilString.cleanParameter(password));
			//logger.info("Password--> " + userADN.getPassword());
			userADN.setNumempleado(UtilString.cleanParameter(numempleado));
			//logger.info("Numero Empleado--> " + userADN.getNumempleado());
			userADN.setWs(UtilString.cleanParameter(ws));
			//logger.info("WS--> " + userADN.getWs());
			userADN.setNombre(UtilString.cleanParameter(nombre));
			//logger.info("Nombre--> " + userADN.getNombre());
			userADN.setLlaveMaestra(UtilString.cleanParameter(null));
			//logger.info("Llave Maestra--> " + userADN.getLlaveMaestra());
			userADN.setApellidop(UtilString.cleanParameter(apellidop));
			//logger.info("Apellidop--> " + userADN.getApellidop());
			userADN.setApellidom(UtilString.cleanParameter(apellidom));
			//logger.info("Apellidom--> " + userADN.getApellidom());
			userADN.setSucursal(UtilString.cleanParameter(sucursal));
			//logger.info("Sucursal --> " + userADN.getSucursal());
			userADN.setNomsucursal(UtilString.cleanParameter(nomsucursal));
			//logger.info("NomSucursal--> " + userADN.getNomsucursal());
			userADN.setPuesto(UtilString.cleanParameter(puesto));
			//logger.info("Puesto--> " + userADN.getPuesto());
			userADN.setDescpuesto(UtilString.cleanParameter(descpuesto));
			//logger.info("DescPuesto--> " + userADN.getDescpuesto());
			userADN.setSap(UtilString.cleanParameter(sap));
			//logger.info("SAP--> " + userADN.getSap());
			userADN.setPais(UtilString.cleanParameter(pais));
			//logger.info("Pais--> " + userADN.getPais());
			userADN.setCanal(UtilString.cleanParameter(canal));
			//logger.info("Canal--> " + userADN.getCanal());
			userADN.setServidor(UtilString.cleanParameter(servidor));
			//logger.info("Servidor--> " + userADN.getServidor());
			userADN.setPuestobase(UtilString.cleanParameter(puestobase));
			//logger.info("Puesto Base--> " + userADN.getPuestobase());
	
			//HttpSession s = sesionADN(request, userADN);
			//s.setAttribute("nombre", nombre + " " + apellidop + " " + apellidom );
			//s.setAttribute("tienda", nomsucursal);
			
			request.getSession().setAttribute("nombre", nombre);
			request.getSession().setAttribute("nomsucursal", nomsucursal);
			
			/*//System.out.println("USUARIO ADN INICIO SESION" + userADN);
			//System.out.println("USUARIO ADN NOMBRE INICIO SESION" + userADN.getNombre());
			//System.out.println("USUARIO ADN INICIO SESION" + userADN.getNumempleado());*/
		
			mv.addObject("objUserADN", sesionADN(request, userADN));
			//mv.addObject("nombre", s.getAttribute("nombre"));
			//mv.addObject("nomsucursal", s.getAttribute("tienda"));
			
		}
		return mv;
	}
	

	@RequestMapping(value = {"central/principal.htm"}, method = RequestMethod.GET)
	public ModelAndView postPrincipal(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		
		ModelAndView mv = new ModelAndView("principal");
		
		return mv;
	}
	
	public HttpSession sesionADN(HttpServletRequest request, UsuarioADN userADN) {
		HttpSession session = request.getSession();
		/*//System.out.println("USUARIO ADN INICIO SESION" + userADN);
		//System.out.println("USUARIO ADN NOMBRE INICIO SESION" + userADN.getNombre());
		//System.out.println("USUARIO ADN INICIO SESION" + userADN.getNumempleado());*/
		session.setAttribute("user", userADN);
		return session;
	}
	
	@ExceptionHandler(Exception.class)
	public ModelAndView handleError(HttpServletRequest request, Exception ex) {
		ModelAndView mv = new ModelAndView("exception");
	    return mv;
	}
	
	@RequestMapping(value = {"tienda/redirecciona.htm","central/redirecciona.htm" }, method = RequestMethod.GET)
	public ModelAndView redirecciona(HttpServletRequest request, Exception ex) {
		ModelAndView mv = new ModelAndView("reinicioSesion");
	    return mv;
	}
	
	@RequestMapping(value="central/cierraSesion.htm", method = RequestMethod.GET)
	public String cierraSesionCentral(HttpServletRequest request, HttpServletResponse response) throws Exception{
		FRQAuthInterceptor o = new FRQAuthInterceptor();
		o.emptyMap();
		
		/*
		request.removeAttribute("user");
		request.removeAttribute("nombre");
		request.removeAttribute("nomSucursal");
		request.removeAttribute("urlServer");
		request.removeAttribute("perfilAdmin");
		request.removeAttribute("perfilReportes");
		request.removeAttribute("servidorApache");
		request.removeAttribute("startTime");
		*/

		HttpSession session = request.getSession(false);
		if (session != null) {
		    session.invalidate();
		}
		request.getSession().setAttribute("user", null);	
		request.getSession().invalidate();
		return "redirect:https://portal.socio.gs/Logout_azteca/cerrar_sesion.html";
		//return "http://portal.socio.gs/Logout_azteca/cerrar_sesion.html";
		//Para ocupara este cierre de sesion se tiene que crear un DNS para checklist y pueda cerrar toda la sesion
		//return "redirect:http://portal.socio.gs/logoutportal";   
	}
	
}