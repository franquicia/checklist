package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.ReporteMedicionDTO;

public class ReporteProtMedRespRowMapper2 implements RowMapper<ReporteMedicionDTO> {

	public ReporteMedicionDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteMedicionDTO reporteDTO = new ReporteMedicionDTO();
		
		//reporteDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		reporteDTO.setFecha(rs.getString("FDFECHA"));
		reporteDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
		reporteDTO.setNomUsuario(rs.getString("FCNOMBRE"));
		reporteDTO.setIdPuesto(rs.getInt("FIID_PUESTO"));
		reporteDTO.setIdCanal(rs.getInt("FIID_CANAL"));
		reporteDTO.setNomCanal(rs.getString("CANAL"));
		reporteDTO.setNomPais(rs.getString("PAIS"));
		reporteDTO.setNomPuesto(rs.getString("FCDESCRIPCION"));
		reporteDTO.setIdCeco(rs.getString("FCID_CECO"));
		reporteDTO.setNomCeco(rs.getString("FCNOMBRE_CECO"));
		reporteDTO.setTerritorio(rs.getString("FCTERRITORIO"));
		reporteDTO.setZona(rs.getString("FCZONA"));
		reporteDTO.setRegion(rs.getString("FCREGIONAL"));
		reporteDTO.setSucursal(rs.getString("SUCURSAL"));
		reporteDTO.setCalificacion(rs.getString("PONDERACION"));
		reporteDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		reporteDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		//reporteDTO.setDescPregunta(rs.getString("FCDESCRIPCION"));
		//reporteDTO.setIdRespuesta(rs.getInt("FIID_RESPUESTA"));
		reporteDTO.setDescRespuesta(rs.getString("FCRESPUESTA"));
		reporteDTO.setIdPonderacion(rs.getDouble("FIPODENRACION"));
		
		return reporteDTO;
		
	}
}
