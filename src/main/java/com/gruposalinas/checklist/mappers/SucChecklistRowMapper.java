package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;

public class SucChecklistRowMapper implements RowMapper<SucursalChecklistDTO> {

	public SucursalChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		SucursalChecklistDTO sc = new SucursalChecklistDTO();
		sc.setIdSucursal(rs.getInt("FCID_CECO"));
		sc.setNombre(rs.getString("FCNOMBRE"));
		sc.setIdEco(rs.getInt("ECO"));
		sc.setRegion(rs.getString("FCREGION"));
		sc.setZona(rs.getString("FCZONA"));
		sc.setTerritorio(rs.getString("FCTERRITORIO"));
		sc.setRuta(rs.getString("FCRUTA"));
		sc.setPeriodo(rs.getString("FCPERIODO"));
		sc.setApert(rs.getInt("FCRECORRIDO"));
		sc.setRecorrido(rs.getInt("FCAUX"));
		sc.setAux(rs.getInt("FIVERSION"));
		sc.setNegocio(rs.getString("NEGO"));
		return sc;
	}

}


