package com.gruposalinas.checklist.domain;

public class TipoChecklistDTO {
	
	private int idTipoCheck;
	private String descTipo;
	
	public int getIdTipoCheck() {
		return idTipoCheck;
	}
	public void setIdTipoCheck(int idTipoChecklist) {
		this.idTipoCheck = idTipoChecklist;
	}
	public String getDescTipo() {
		return descTipo;
	}
	public void setDescTipo(String descTipo) {
		this.descTipo = descTipo;
	}
	@Override
	public String toString() {
		return "TipoChecklistDTO [idTipoCheck=" + idTipoCheck + ", descTipo=" + descTipo + "]";
	}

	

}
