package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.SoftOpeningDTO;


public interface SoftOpeningDAO {

		//automatizado
	  	public int inserta(SoftOpeningDTO bean)throws Exception;
	  	
		public int inserta2(int  bitacora)throws Exception;
	  	
	  	//manual
		public int insertaMan(SoftOpeningDTO bean)throws Exception;
		
		public boolean elimina(String ceco,String recorrido)throws Exception;
		
		public List<SoftOpeningDTO> obtieneDatos(int ceco) throws Exception; 
		
		public List<SoftOpeningDTO> obtieneInfo() throws Exception; 
		
		public boolean actualiza(SoftOpeningDTO bean)throws Exception;
		
	}