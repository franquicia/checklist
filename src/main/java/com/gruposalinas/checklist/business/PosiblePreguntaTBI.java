package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.PreguntaPosibleTDAO;
import com.gruposalinas.checklist.domain.PreguntaPosibleTDTO;

public class PosiblePreguntaTBI {

	private static Logger logger = LogManager.getLogger(PosiblePreguntaTBI.class);
	
	@Autowired
	PreguntaPosibleTDAO posiblesPregDAO;

	List<PreguntaPosibleTDTO> listaPosiblePreg= null;
	
	public List<PreguntaPosibleTDTO> obtienePreguntasPosibleTemp(String idPregunta) throws Exception {

		try {
		listaPosiblePreg = posiblesPregDAO.obtienePreguntasPosibleTemp(idPregunta);
		} catch (Exception e) {
			logger.info("No fue posible consultar");
		}

		return listaPosiblePreg;
	}
	
	public boolean insertaPreguntaPosibleTemp(PreguntaPosibleTDTO bean){
		
		boolean respuesta = false;
		
		try {
			respuesta = posiblesPregDAO.insertaPreguntaPosibleTemp(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar");
		}
		
		return respuesta;
		
	}
	
	public boolean eliminaPreguntaPosibleTemp(int idPregPos, int idPregunta){
		
		boolean respuesta = false ;
		
		try {
			respuesta = posiblesPregDAO.eliminaPreguntaPosibleTemp(idPregPos, idPregunta);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
		}
		
		return respuesta;		
	}

	
}
