package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.LoginDTO;

public class LoginRowMapperN implements RowMapper<LoginDTO> {
	
	public LoginDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		LoginDTO loginDTO  = new LoginDTO();
		
		loginDTO.setNoEmpleado(rs.getInt("FIID_USUARIO"));
		loginDTO.setNombreEmpelado(rs.getString("FCNOMBRE"));
		loginDTO.setIdCeco(""+(rs.getInt("FCID_CECO")));		
		loginDTO.setDescCeco(rs.getString("FCNOM_CECO"));
		loginDTO.setIdPuesto(rs.getInt("FIID_PUESTO"));
		loginDTO.setDescPuesto(rs.getString("FCDESCRIPCION"));
		loginDTO.setIdperfil(rs.getInt("FIID_PERFIL"));
		loginDTO.setIdPais(rs.getInt("FIID_PAIS"));
		loginDTO.setIdTerritorio(rs.getInt("FCID_TERRITORIO"));
		loginDTO.setZona(rs.getInt("FCID_ZONA"));
		loginDTO.setRegion(rs.getInt("FCID_REGION"));
		
		return loginDTO;
	}

}
