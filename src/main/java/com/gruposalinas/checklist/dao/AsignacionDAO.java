package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.AsignacionDTO;

public interface AsignacionDAO {
	
	public List<AsignacionDTO> obtieneAsignaciones(String idChecklist, String idCeco, String idPuesto, String activo) throws Exception;
	
	public boolean insertaAsignacion(AsignacionDTO asignacion) throws Exception;
	
	public boolean actualizaAsignacion(AsignacionDTO asignacion)throws Exception;
	
	public boolean eliminaAsignacion(AsignacionDTO asignacion )throws Exception;
	
	public Map<String, Object> ejecutaAsignacion()throws Exception;

}
