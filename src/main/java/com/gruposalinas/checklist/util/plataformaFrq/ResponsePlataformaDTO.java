/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.util.plataformaFrq;

import java.util.ArrayList;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 *
 * @author leodan1991
 */
public class ResponsePlataformaDTO<T> {
    
    
    @JsonProperty("status_code")
    private int statusCode;
    @JsonProperty("data")
    private T data;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    
    
    

    @Override
    public String toString() {
        return "ResponsePlataformaDTO{" + "statusCode=" + statusCode + ", data=" + data + '}';
    }
    
    
    
}
