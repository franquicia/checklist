$(document).ready(function() {
	asignarEventosCombo();
});

function asignarEventosCombo() {
	$(".cajaVis").off();

	$(".cajaVis").hover(

			function(e) {
				asignarEventosCombo(); 
				
				var lista = $(this).find("ul");
				var triangulo = $(this).find("span:last-child");
				e.preventDefault();

				$(this).find("ul").toggle();
				if (lista.is(":hidden")) {
					triangulo.removeClass("triangulosupVis").addClass("trianguloinfVis");
					triangulo.parent().css('z-index', 0);
				} else {
					triangulo.removeClass("trianguloinfVis").addClass("triangulosupVis");
					triangulo.parent().css('z-index', 3000);
				}
			});

	$(".cajaVis").on("click","li",
			function clickli(e) {
				var texto = $(this).text();
				var seleccionadoVis = $(this).parent().prev();
				var lista = $(this).closest("ul");
				var triangulo = $(this).parent().next();
				e.preventDefault();
				e.stopPropagation();
				seleccionadoVis.text(texto);
				lista.hide();
				triangulo.removeClass("triangulosupVis").addClass("trianguloinfVis");
				triangulo.parent().css('z-index', 0);
				$(".cajaVis").unbind('mouseleave');
				var valor = $(this).val();
				seleccionadoVis.attr("id", valor);
				var divActivo = $(this).parents("li").find(".cajaVis").attr("id");

				if (divActivo == 'categoriaVis')
					cambiarCategoria()
					
				if (divActivo == 'tipoVis')
					$("#valorTipo").val($("#tipoVis .seleccionadoVis").attr("id"));

				

			});
}

function cambiarCategoria() {
	// Asigna el valor del li al input hidden
	$("#valorCategoria").val($("#categoriaVis .seleccionadoVis").attr("id"));
	// Vulve a inicializar el valor del span de la lista a rellenar
	$("#tipoVis .seleccionadoVis").val("Selecciona una Opcion");
	$('#tipoVis .seleccionadoVis').text("Elije una opcion");
	// Vulve a inicializar el valor del input hidden de la lista a rellenar
	$("#valorTipo").val("Selecciona una Opcion");
	var varCateg = document.getElementById("valorCategoria").value;
	if (varCateg != "Selecciona una Opcion")
		categoria(varCateg);
}

function categoria(varCateg) {
	$.ajax({
		type : "GET",
		url : urlServer + '../ajaxCategoria.json?varCateg='
				+ varCateg,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			document.getElementById("tipoVis2").innerHTML = "";
			var jsonC = JSON.stringify(json);
			var i = 0;
			while (json[i] != null) {
				$("#tipoVis2").append(
						"<li value='" + json[i].idChecklist
								+ "'><a href=''>" + json[i].nombreCheck
								+ "</a></li>");
				i++;
			}
		},
		error : function() {
			alert('Algo paso');
		}
	});
}

function generaReporte(){
	
	var idChecklist=null;
	var fechaInicio=null;
	var fechaFin=null;
	
	if(document.getElementById("valorTipo").value!="Selecciona una Opcion"){
		idChecklist=document.getElementById("valorTipo").value;
		
		if(document.getElementById("fechaInicio").value!="" && document.getElementById("fechaFin").value!=""){
			fechaInicio=document.getElementById("fechaInicio").value;	
			fechaFin=document.getElementById("fechaFin").value;				
			
			window.location.href = urlServer+'../reporteService/reporteGeneral.json?idChecklist='+idChecklist+'&fechaInicio='+fechaInicio+'&fechaFin='+fechaFin;		
		}else{
			window.location.href = urlServer+'../reporteService/reporteGeneral.json?idChecklist='+idChecklist;		
		}		
		
	}else{
		alert(' NO EXISTEN DATOS ')		

	}
	
	}
function limpiaReporte(){
	
	$("#categoriaVis .seleccionadoVis").val("Selecciona una Opcion");
	$('#categoriaVis .seleccionadoVis').text("Elije una opcion");
	
	$("#tipoVis .seleccionadoVis").val("Selecciona una Opcion");
	$('#tipoVis .seleccionadoVis').text("Elije una opcion");
	document.getElementById("valorTipo").value="Selecciona una Opcion";
	document.getElementById("fechaInicio").value="";
	document.getElementById("fechaFin").value="";
}
