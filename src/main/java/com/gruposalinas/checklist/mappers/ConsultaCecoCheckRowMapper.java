package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ConsultaCecoCheckDTO;


public class ConsultaCecoCheckRowMapper implements RowMapper<ConsultaCecoCheckDTO> {

	//SACA EL TOTAL DE CECOS DE IN USUARIO
	public ConsultaCecoCheckDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ConsultaCecoCheckDTO consultaCecoCheckDTO = new ConsultaCecoCheckDTO();
		
		consultaCecoCheckDTO.setCeco(rs.getInt("TOTALCECOS"));
		consultaCecoCheckDTO.setChecklist(rs.getInt("FIID_CHECKLIST"));
		consultaCecoCheckDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
		

		return consultaCecoCheckDTO;
	}

}


