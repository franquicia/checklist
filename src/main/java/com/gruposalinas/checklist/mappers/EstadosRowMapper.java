package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.EstadosDTO;

public class EstadosRowMapper implements RowMapper<EstadosDTO> {

	public EstadosDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		EstadosDTO EstadosDTO = new EstadosDTO();
		
	
	    EstadosDTO.setIdConsec(rs.getInt("FICONSEC"));
	    EstadosDTO.setIdPais(rs.getInt("FIPAIS_ID")); 
	    EstadosDTO.setIdEstado(rs.getInt("FIESTADO_ID"));
	    EstadosDTO.setDescripcion(rs.getString("FCESTADO_DESC"));
	    EstadosDTO.setAbreviatura(rs.getString("FCESTADO_ABREV"));
	    EstadosDTO.setModificacion(rs.getString("ULTIMA_MODIFICACION"));
		return EstadosDTO;
	}

}

