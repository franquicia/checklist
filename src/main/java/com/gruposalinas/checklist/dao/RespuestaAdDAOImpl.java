package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.RespuestaAdDTO;
import com.gruposalinas.checklist.mappers.RespuestaAdRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class RespuestaAdDAOImpl extends DefaultDAO implements RespuestaAdDAO {

    private static Logger logger = LogManager.getLogger(RespuestaAdDAOImpl.class);

    private DefaultJdbcCall jdbcBuscaRespAD;
    private DefaultJdbcCall jdbcInsertaRespAD;
    private DefaultJdbcCall jdbcActualizaRespAD;
    private DefaultJdbcCall jdbcEliminaRespAD;

    public void init() {

        jdbcBuscaRespAD = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMRESPAD")
                .withProcedureName("SP_SEL_RESPAD")
                .returningResultSet("RCL_RESP", new RespuestaAdRowMapper());

        jdbcInsertaRespAD = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMRESPAD")
                .withProcedureName("SP_INS_RESPAD");

        jdbcActualizaRespAD = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMRESPAD")
                .withProcedureName("SP_ACT_RESPAD");

        jdbcEliminaRespAD = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMRESPAD")
                .withProcedureName("SP_DEL_RESPAD");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<RespuestaAdDTO> buscaRespADP(String idRespAD, String idResp) throws Exception {
        Map<String, Object> out = null;

        List<RespuestaAdDTO> listaRespuesta = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDRES_AD", idRespAD)
                .addValue("PA_IDRESPUESTA", idResp);

        out = jdbcBuscaRespAD.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMRESPAD.SP_SEL_RESPAD}");

        listaRespuesta = (List<RespuestaAdDTO>) out.get("RCL_RESP");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener las Respuestas Adicionales");
        } else {
            return listaRespuesta;
        }

        return null;
    }

    @Override
    public int insertaRespAD(RespuestaAdDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idResp = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDRESPUESTA", bean.getIdRespuesta())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcInsertaRespAD.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMRESPAD.SP_INS_RESPAD}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDRESP");
        idResp = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar la Respuesta Adicional");
        } else {
            return idResp;
        }

        return idResp;
    }

    @Override
    public boolean actualizaRespAD(RespuestaAdDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDRES_AD", bean.getIdRespuestaAd())
                .addValue("PA_IDRESPUESTA", bean.getIdRespuesta())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcActualizaRespAD.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMRESPAD.SP_ACT_RESPAD}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar la Respuesta Adicional");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaRespAD(int idRespAD) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDRES_AD", idRespAD);

        out = jdbcEliminaRespAD.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMRESPAD.SP_DEL_RESPAD}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al eliminar la Respuesta Adicional");
        } else {
            return true;
        }

        return false;
    }

}
