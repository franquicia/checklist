package com.gruposalinas.checklist.domain;

public class PosiblesTipoPreguntaDTO {
	
	private int idPosibleTipoPregunta;
	private int idTipoPregunta;
	private int idPosibleRespuesta;
	private String descripcionPosible;
	private String numeroRevision;
	private String tipoCambio;
	
	public int getIdPosibleTipoPregunta() {
		return idPosibleTipoPregunta;
	}
	public void setIdPosibleTipoPregunta(int idPosibleTipoPregunta) {
		this.idPosibleTipoPregunta = idPosibleTipoPregunta;
	}
	public int getIdTipoPregunta() {
		return idTipoPregunta;
	}
	public void setIdTipoPregunta(int idTipoPregunta) {
		this.idTipoPregunta = idTipoPregunta;
	}
	public int getIdPosibleRespuesta() {
		return idPosibleRespuesta;
	}
	public void setIdPosibleRespuesta(int idPosibleRespuesta) {
		this.idPosibleRespuesta = idPosibleRespuesta;
	}
	public String getDescripcionPosible() {
		return descripcionPosible;
	}
	public void setDescripcionPosible(String descripcionPosible) {
		this.descripcionPosible = descripcionPosible;
	}
	public String getNumeroRevision() {
		return numeroRevision;
	}
	public void setNumeroRevision(String numeroRevision) {
		this.numeroRevision = numeroRevision;
	}
	public String getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	

}
