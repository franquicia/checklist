package com.gruposalinas.checklist.domain;

public class RepoCheckPaperDTO {
	


		private String ceco;
		private int idCheclist;
		private String nombreCheck;
		private int chekUsua;
		private double cali;
		private String fini;
		private String ffin;
		private int bandera;
		private int semana;
		private int bitacora;
		private int idUsuario;
		private int idResp;
		private int idRespAbierta;
		private String respAb;
		private int bandResp;
		private int idPreg;
		private int pregPadre;
		private int modulo;
		private int tipoPreg;
		private String pregunta;
		private String tipopregunta;
		//detalle Checklist
		private int version;
		private int ordenGrupo;
		private String  nombreModulo;
		private int idmodPadre;
		//detalle preg
		private String claveTipo;
		private int arbol;
		private int respArbol;
		private int status;
		private float ponderacion;
		private int plantilla;
		private String posResp;
		
		
		
		public int getBitacora() {
			return bitacora;
		}
		public void setBitacora(int bitacora) {
			this.bitacora = bitacora;
		}
		public String getCeco() {
			return ceco;
			
		}
		public void setCeco(String ceco) {
			this.ceco = ceco;
		}
		public int getIdCheclist() {
			return idCheclist;
		}
		public void setIdCheclist(int idCheclist) {
			this.idCheclist = idCheclist;
		}
		public String getNombreCheck() {
			return nombreCheck;
		}
		
		public int getChekUsua() {
			return chekUsua;
		}
		public void setChekUsua(int chekUsua) {
			this.chekUsua = chekUsua;
		}
		public void setNombreCheck(String nombreCheck) {
			this.nombreCheck = nombreCheck;
		}
		public double getCali() {
			return cali;
		}
		public void setCali(double cali) {
			this.cali = cali;
		}
		public String getFini() {
			return fini;
		}
		public void setFini(String fini) {
			this.fini = fini;
		}
		public String getFfin() {
			return ffin;
		}
		public void setFfin(String ffin) {
			this.ffin = ffin;
		}
		public int getBandera() {
			return bandera;
		}
		public void setBandera(int bandera) {
			this.bandera = bandera;
		}
		public int getSemana() {
			return semana;
		}
		public void setSemana(int semana) {
			this.semana = semana;
		}
		
		
		
		public int getIdUsuario() {
			return idUsuario;
		}
		public void setIdUsuario(int idUsuario) {
			this.idUsuario = idUsuario;
		}
		
		
		public int getIdResp() {
			return idResp;
		}
		public void setIdResp(int idResp) {
			this.idResp = idResp;
		}
		public String getRespAb() {
			return respAb;
		}
		public void setRespAb(String respAb) {
			this.respAb = respAb;
		}
		public int getBandResp() {
			return bandResp;
		}
		public void setBandResp(int bandResp) {
			this.bandResp = bandResp;
		}
		public int getIdPreg() {
			return idPreg;
		}
		public void setIdPreg(int idPreg) {
			this.idPreg = idPreg;
		}
		public int getPregPadre() {
			return pregPadre;
		}
		public void setPregPadre(int pregPadre) {
			this.pregPadre = pregPadre;
		}
		public int getModulo() {
			return modulo;
		}
		public void setModulo(int modulo) {
			this.modulo = modulo;
		}
		public int getTipoPreg() {
			return tipoPreg;
		}
		public void setTipoPreg(int tipoPreg) {
			this.tipoPreg = tipoPreg;
		}
		public String getPregunta() {
			return pregunta;
		}
		public void setPregunta(String pregunta) {
			this.pregunta = pregunta;
		}
		public String getTipopregunta() {
			return tipopregunta;
		}
		public void setTipopregunta(String tipopregunta) {
			this.tipopregunta = tipopregunta;
		}
		
		
		public int getVersion() {
			return version;
		}
		public void setVersion(int version) {
			this.version = version;
		}
		public int getOrdenGrupo() {
			return ordenGrupo;
		}
		public void setOrdenGrupo(int ordenGrupo) {
			this.ordenGrupo = ordenGrupo;
		}
		public String getNombreModulo() {
			return nombreModulo;
		}
		public void setNombreModulo(String nombreModulo) {
			this.nombreModulo = nombreModulo;
		}
		public int getIdmodPadre() {
			return idmodPadre;
		}
		public void setIdmodPadre(int idmodPadre) {
			this.idmodPadre = idmodPadre;
		}
		
		public String getClaveTipo() {
			return claveTipo;
		}
		public void setClaveTipo(String claveTipo) {
			this.claveTipo = claveTipo;
		}
		
		public int getIdRespAbierta() {
			return idRespAbierta;
		}
		public void setIdRespAbierta(int idRespAbierta) {
			this.idRespAbierta = idRespAbierta;
		}
		public int getArbol() {
			return arbol;
		}
		public void setArbol(int arbol) {
			this.arbol = arbol;
		}
		public int getRespArbol() {
			return respArbol;
		}
		public void setRespArbol(int respArbol) {
			this.respArbol = respArbol;
		}
		public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		public float getPonderacion() {
			return ponderacion;
		}
		public void setPonderacion(float ponderacion) {
			this.ponderacion = ponderacion;
		}
		public int getPlantilla() {
			return plantilla;
		}
		public void setPlantilla(int plantilla) {
			this.plantilla = plantilla;
		}
		public String getPosResp() {
			return posResp;
		}
		public void setPosResp(String posResp) {
			this.posResp = posResp;
		}
		@Override
		public String toString() {
			return "RepoCheckPaperDTO [ceco=" + ceco + ", idCheclist=" + idCheclist + ", nombreCheck=" + nombreCheck
					+ ", chekUsua=" + chekUsua + ", cali=" + cali + ", fini=" + fini + ", ffin=" + ffin + ", bandera="
					+ bandera + ", semana=" + semana + ", bitacora=" + bitacora + ", idUsuario=" + idUsuario
					+ ", idResp=" + idResp + ", idRespAbierta=" + idRespAbierta + ", respAb=" + respAb + ", bandResp="
					+ bandResp + ", idPreg=" + idPreg + ", pregPadre=" + pregPadre + ", modulo=" + modulo
					+ ", tipoPreg=" + tipoPreg + ", pregunta=" + pregunta + ", tipopregunta=" + tipopregunta
					+ ", version=" + version + ", ordenGrupo=" + ordenGrupo + ", nombreModulo=" + nombreModulo
					+ ", idmodPadre=" + idmodPadre + ", claveTipo=" + claveTipo + ", arbol=" + arbol + ", respArbol="
					+ respArbol + ", status=" + status + ", ponderacion=" + ponderacion + ", plantilla=" + plantilla
					+ ", posResp=" + posResp + "]";
		}
		
		
	
		
	}