package com.gruposalinas.checklist.domain;

public class ChecklistUsuarioDTO {
	
	private int idCheckUsuario;
	private int idChecklist;
	private int idUsuario;
	private int idCeco;
	private String nombreU;
	private String nombreCeco;
	private int idBitacora;
	private String respuestas;
	private String compromisos;
	private String respuestas_Ad;
	private int activo;
	private String fechaIni;
	private String fechaFin;
	private String fechaResp;
	private String ultimaVisita;
	private String fechaModificacion;
	private String nombreCheck;
	private int estatus;
	private String latitud;
	private String longitud;
	private String zona;
	
	
	public int getIdCheckUsuario() {
		return idCheckUsuario;
	}
	public void setIdCheckUsuario(int idCheckUsuario) {
		this.idCheckUsuario = idCheckUsuario;
	}
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(int idCeco) {
		this.idCeco = idCeco;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	public String getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}
	public String getFechaResp() {
		return fechaResp;
	}
	public void setFechaResp(String fechaResp) {
		this.fechaResp = fechaResp;
	}
	public String getNombreU() {
		return nombreU;
	}
	public void setNombreU(String nombreU) {
		this.nombreU = nombreU;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getRespuestas() {
		return respuestas;
	}
	public void setRespuestas(String respuestas) {
		this.respuestas = respuestas;
	}
	public String getCompromisos() {
		return compromisos;
	}
	public void setCompromisos(String compromisos) {
		this.compromisos = compromisos;
	}
	public String getRespuestas_Ad() {
		return respuestas_Ad;
	}
	public void setRespuestas_Ad(String respuestas_Ad) {
		this.respuestas_Ad = respuestas_Ad;
	}
	public String getUltimaVisita() {
		return ultimaVisita;
	}
	public void setUltimaVisita(String ultimaVisita) {
		this.ultimaVisita = ultimaVisita;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public String getNombreCheck() {
		return nombreCheck;
	}
	public void setNombreCheck(String nombreCheck) {
		this.nombreCheck = nombreCheck;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}

}
