package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.TipoArchivoDTO;
import com.gruposalinas.checklist.mappers.TipoArchivoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class TipoArchivoDAOImpl extends DefaultDAO implements TipoArchivoDAO {

    private static Logger logger = LogManager.getLogger(TipoArchivoDAOImpl.class);

    DefaultJdbcCall jdbcInsertaTipoArchivo;
    DefaultJdbcCall jdbcEliminaTipoArchivo;
    DefaultJdbcCall jdbcActualizaTipoArchivo;
    DefaultJdbcCall jdbcObtieneTipoArchivo;
    DefaultJdbcCall jdbcObtieneTodos;

    public void init() {

        jdbcInsertaTipoArchivo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_ARCHIVOS")
                .withProcedureName("SP_INS_ARCHIVOS");

        jdbcEliminaTipoArchivo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_ARCHIVOS")
                .withProcedureName("SP_DEL_ARCHIVOS");

        jdbcActualizaTipoArchivo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_ARCHIVOS")
                .withProcedureName("SP_ACT_ARCHIVOS");

        jdbcObtieneTipoArchivo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_ARCHIVOS")
                .withProcedureName("SP_SEL_ARCHIVOS")
                .returningResultSet("RCL_ARC", new TipoArchivoRowMapper());

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_ARCHIVOS")
                .withProcedureName("SP_SEL_G_ARC")
                .returningResultSet("RCL_ARC", new TipoArchivoRowMapper());
    }

    public int insertaTipoArchivo(TipoArchivoDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idTipo = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NOMBRE", bean.getNombreTipo());

        out = jdbcInsertaTipoArchivo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_ARCHIVOS.SP_INS_ARCHIVOS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDARCHIVO");
        idTipo = idTipoReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar el Tipo de Archivo");
        } else {
            return idTipo;
        }

        return idTipo;
    }

    public boolean eliminaTipoArchivo(int idTipo) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_TIPO", idTipo);

        out = jdbcEliminaTipoArchivo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_ARCHIVOS.SP_DEL_ARCHIVOS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al borrar el Tipo archivo id(" + idTipo + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean actualizaTipoArchivo(TipoArchivoDTO bean) throws Exception {

        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_TIPO", bean.getIdTipoArchivo())
                .addValue("PA_NOMBRE", bean.getNombreTipo());

        out = jdbcActualizaTipoArchivo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_ARCHIVOS.SP_ACT_ARCHIVOS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar Tipo Archivo  id( " + bean.getIdTipoArchivo() + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<TipoArchivoDTO> obtieneTipoArchivo() throws Exception {

        Map<String, Object> out = null;
        List<TipoArchivoDTO> listaTipoArchivo = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funcion ejecutada: {checklist.PA_ADM_ARCHIVOS.SP_SEL_G_ARC}");

        listaTipoArchivo = (List<TipoArchivoDTO>) out.get("RCL_ARC");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener los Tipo Archivo");
        } else {
            return listaTipoArchivo;
        }

        return listaTipoArchivo;
    }

    @SuppressWarnings("unchecked")
    public List<TipoArchivoDTO> obtieneTipoArchivo(int idTipo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<TipoArchivoDTO> listaTipoArchivo = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_TIPO", idTipo);

        out = jdbcObtieneTipoArchivo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_ARCHIVOS.SP_SEL_ARCHIVOS}");

        listaTipoArchivo = (List<TipoArchivoDTO>) out.get("RCL_ARC");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener los Tipo Archivo con id(" + idTipo + ")");
        } else {
            return listaTipoArchivo;
        }

        return null;
    }

}
