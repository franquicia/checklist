package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.ReporteSupervisionSistemasDTO;

public class ReporteSupervisionSisCapRowMapper implements RowMapper<ReporteSupervisionSistemasDTO> {

	public ReporteSupervisionSistemasDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteSupervisionSistemasDTO reporteCecosSistemas = new ReporteSupervisionSistemasDTO();
		
		reporteCecosSistemas.setFinicio(rs.getString("FINICIO"));
		reporteCecosSistemas.setFfin(rs.getString("FFIN"));
		reporteCecosSistemas.setEvaluado(rs.getString("EVALUADO"));
		reporteCecosSistemas.setSucursal(rs.getString("SUCURSAL"));
		reporteCecosSistemas.setRegion(rs.getString("REGION"));
		reporteCecosSistemas.setZona(rs.getString("ZONA"));
		reporteCecosSistemas.setTerritorio(rs.getString("TERRITORIO"));
		reporteCecosSistemas.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		reporteCecosSistemas.setChecklist(rs.getString("CHECKLIST"));
		reporteCecosSistemas.setIdBitacora(rs.getInt("FIID_BITACORA"));
		
		return reporteCecosSistemas;
	}
}

