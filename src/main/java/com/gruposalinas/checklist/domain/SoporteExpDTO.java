package com.gruposalinas.checklist.domain;



public class SoporteExpDTO {
	


		private String ceco;
		private String status;
		private String tipoSuc;
		private int pais;
		private String nombCeco;
		private int cecoSup;
		private int negocio;
		private String usuario;
		private String fecha;
		private int aux;
		private String aux2;
		private String reqObserv;
		private String bandObser;
		private String plantilla;
		private String ponderacion;
		private String arbol;
		private String idPreg;
		private String tipoPreg;
		private String descPreg;
		private String critica;
		private String sla;
		private String area;
		private String modulo;
		private String bitacora;

		public String getCeco() {
			return ceco;
		}
		public void setCeco(String ceco) {
			this.ceco = ceco;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getTipoSuc() {
			return tipoSuc;
		}
		public void setTipoSuc(String tipoSuc) {
			this.tipoSuc = tipoSuc;
		}
		public int getPais() {
			return pais;
		}
		public void setPais(int pais) {
			this.pais = pais;
		}
		public String getNombCeco() {
			return nombCeco;
		}
		public void setNombCeco(String nombCeco) {
			this.nombCeco = nombCeco;
		}
		public int getCecoSup() {
			return cecoSup;
		}
		public void setCecoSup(int cecoSup) {
			this.cecoSup = cecoSup;
		}
		public int getNegocio() {
			return negocio;
		}
		public void setNegocio(int negocio) {
			this.negocio = negocio;
		}
		public String getUsuario() {
			return usuario;
		}
		public void setUsuario(String usuario) {
			this.usuario = usuario;
		}
		public String getFecha() {
			return fecha;
		}
		public void setFecha(String fecha) {
			this.fecha = fecha;
		}
		public int getAux() {
			return aux;
		}
		public void setAux(int aux) {
			this.aux = aux;
		}
		public String getAux2() {
			return aux2;
		}
		public void setAux2(String aux2) {
			this.aux2 = aux2;
		}
		
		
		public String getReqObserv() {
			return reqObserv;
		}
		public void setReqObserv(String reqObserv) {
			this.reqObserv = reqObserv;
		}
		public String getBandObser() {
			return bandObser;
		}
		public void setBandObser(String bandObser) {
			this.bandObser = bandObser;
		}
		public String getPlantilla() {
			return plantilla;
		}
		public void setPlantilla(String plantilla) {
			this.plantilla = plantilla;
		}
		public String getPonderacion() {
			return ponderacion;
		}
		public void setPonderacion(String ponderacion) {
			this.ponderacion = ponderacion;
		}
		public String getArbol() {
			return arbol;
		}
		public void setArbol(String arbol) {
			this.arbol = arbol;
		}
		
		public String getIdPreg() {
			return idPreg;
		}
		public void setIdPreg(String idPreg) {
			this.idPreg = idPreg;
		}
		public String getTipoPreg() {
			return tipoPreg;
		}
		public void setTipoPreg(String tipoPreg) {
			this.tipoPreg = tipoPreg;
		}
		public String getDescPreg() {
			return descPreg;
		}
		public void setDescPreg(String descPreg) {
			this.descPreg = descPreg;
		}
		public String getCritica() {
			return critica;
		}
		public void setCritica(String critica) {
			this.critica = critica;
		}
		public String getSla() {
			return sla;
		}
		public void setSla(String sla) {
			this.sla = sla;
		}
		public String getArea() {
			return area;
		}
		public void setArea(String area) {
			this.area = area;
		}
		public String getModulo() {
			return modulo;
		}
		public void setModulo(String modulo) {
			this.modulo = modulo;
		}
		
		public String getBitacora() {
			return bitacora;
		}
		public void setBitacora(String bitacora) {
			this.bitacora = bitacora;
		}
		@Override
		public String toString() {
			return "SoporteExpDTO [ceco=" + ceco + ", status=" + status + ", tipoSuc=" + tipoSuc + ", pais=" + pais
					+ ", nombCeco=" + nombCeco + ", cecoSup=" + cecoSup + ", negocio=" + negocio + ", usuario="
					+ usuario + ", fecha=" + fecha + ", aux=" + aux + ", aux2=" + aux2 + ", reqObserv=" + reqObserv
					+ ", bandObser=" + bandObser + ", plantilla=" + plantilla + ", ponderacion=" + ponderacion
					+ ", arbol=" + arbol + ", idPreg=" + idPreg + ", tipoPreg=" + tipoPreg + ", descPreg=" + descPreg
					+ ", critica=" + critica + ", sla=" + sla + ", area=" + area + ", modulo=" + modulo + ", bitacora="
					+ bitacora + "]";
		}
		
		
	}