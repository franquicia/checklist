package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ParametroDTO;
import com.gruposalinas.checklist.mappers.ParametroRowMapper;
import com.gruposalinas.checklist.util.UtilString;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ParametroDAOImpl extends DefaultDAO implements ParametroDAO {

    private static Logger logger = LogManager.getLogger(ParametroDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneParametro;
    DefaultJdbcCall jdbcGetParametro;
    DefaultJdbcCall jdbcInsertaParametro;
    DefaultJdbcCall jdbcActualizaParametro;
    DefaultJdbcCall jdbcEliminaParametro;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PARAM")
                .withProcedureName("SP_SEL_G_PARAM")
                .returningResultSet("RCL_PARAM", new ParametroRowMapper());

        jdbcObtieneParametro = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PARAM")
                .withProcedureName("SP_SEL_PARAM")
                .returningResultSet("RCL_PARAM", new ParametroRowMapper());

        jdbcInsertaParametro = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PARAM")
                .withProcedureName("SP_INS_PARAM");

        jdbcActualizaParametro = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PARAM")
                .withProcedureName("SP_ACT_PARAM");

        jdbcEliminaParametro = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PARAM")
                .withProcedureName("SP_DEL_PARAM");

        jdbcGetParametro = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PARAM")
                .withProcedureName("SPGETPARAMETRO");
    }

    @SuppressWarnings("unchecked")
    public List<ParametroDTO> obtieneParametro() throws Exception {
        Map<String, Object> out = null;
        List<ParametroDTO> listaParametro = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_PARAM.SP_SEL_G_PARAM}");
        listaParametro = (List<ParametroDTO>) out.get("RCL_PARAM");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Parametros");
        } else {
            return listaParametro;
        }

        return listaParametro;

    }

    @SuppressWarnings("unchecked")
    public List<ParametroDTO> obtieneParametro(String clave) throws Exception {
        Map<String, Object> out = null;
        List<ParametroDTO> listaParametro = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CVE_PARAM", clave);

        out = jdbcObtieneParametro.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_PARAM.SP_SEL_PARAM}");
        listaParametro = (List<ParametroDTO>) out.get("RCL_PARAM");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Parametro con la clave (" + clave + ")");
        } else {
            return listaParametro;
        }

        return null;
    }

    public boolean insertaParametro(ParametroDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CVE_PARAM", bean.getClave())
                .addValue("PA_VAL_PARAM", bean.getValor())
                .addValue("PA_ACTIVO_PARAM", bean.getActivo());

        out = jdbcInsertaParametro.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_PARAM.SP_INS_PARAM}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Parametro");
        } else {
            return true;
        }

        return false;

    }

    public boolean actualizaParametro(ParametroDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CVE_PARAM", bean.getClave())
                .addValue("PA_VAL_PARAM", bean.getValor())
                .addValue("PA_ACTIVO_PARAM", bean.getActivo());

        out = jdbcActualizaParametro.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_PARAM.SP_ACT_PARAM}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            String clave = UtilString.cleanParameter(bean.getClave())
                    .replace('\n', '_').replace('\r', '_');
            logger.info("Algo ocurrió al actualizar el Parametro clave( " + clave + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaParametros(String clave) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CVE_PARAM", clave);

        out = jdbcEliminaParametro.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_PARAM.SP_DEL_PARAM}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            clave = UtilString.cleanParameter(clave)
                    .replace('\n', '_').replace('\r', '_');
            logger.info("Algo ocurrió al borrar el Parametro clave(" + clave + ")");
        } else {
            return true;
        }

        return false;

    }

    @Override
    public int getParametro(ParametroDTO bean) throws Exception {
        Map<String, Object> out = null;
        int res = 0;
        int error = 1;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CVE_PARAM", bean.getclaveParametro());

        out = jdbcGetParametro.execute(in);

        //logger.info("Funcion ejecutada: {checklist.PA_ADM_PARAM.SPGETPARAMETRO}");
        String valor = (String) out.get("PA_FCVALOR");
        res = Integer.parseInt(valor);

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Parametros");
        } else {
            return res;
        }

        return res;

    }
}
