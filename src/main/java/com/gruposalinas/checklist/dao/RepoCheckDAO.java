package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.RepoCheckDTO;


public interface RepoCheckDAO {

	 
		
		// obtiene checklist de usuario
		public List<RepoCheckDTO> obtieneDatos(int idUsuario) throws Exception; 
		
		//obtiene su info del usuario
		public List<RepoCheckDTO> obtieneInfo(int idUsuario) throws Exception; 
		

		
	}