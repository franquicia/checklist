package com.gruposalinas.checklist.domain;


public class SchedulerDTO {
	
	private String id;
	private String fecha;
	private String hora;
	public String minutos;
	public String segundos;
	public String dia;
	public String mes;
	public String diaSemana;
	private String cTarea;
	private String fechaTarea;
	private String nactivo;
	
	public String getDiaSemana() {
		return diaSemana;
	}
	public void setDiaSemana(String diaSemana) {
		this.diaSemana = diaSemana;
	}	
	public String getCTarea() {
		return cTarea;
	}
	public void setCTarea(String cTarea) {
		this.cTarea = cTarea;
	}
	public String getFechaTarea() {
		return fechaTarea;
	}
	public void setFechaTarea(String fechaTarea) {
		this.fechaTarea = fechaTarea;
	}
	public String getNactivo() {
		return nactivo;
	}
	public void setNactivo(String nactivo) {
		this.nactivo = nactivo;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getMinutos() {
		return minutos;
	}
	public void setMinutos(String minutos) {
		this.minutos = minutos;
	}
	public String getSegundos() {
		return segundos;
	}
	public void setSegundos(String segundos) {
		this.segundos = segundos;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
