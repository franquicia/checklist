package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;

public class ChecklistPregComRowMapper implements RowMapper<ChecklistPreguntasComDTO> {

	public ChecklistPreguntasComDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistPreguntasComDTO com = new ChecklistPreguntasComDTO();

        com.setIdBita(rs.getInt("FIID_BITACORA"));
	    com.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    com.setIdArbol(rs.getInt("FIID_ARBOL_DES"));
	    com.setObserv(rs.getString("OBSERVACIONES"));
	    com.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    com.setIdCheck(rs.getInt("FIID_CHECKLIST"));
	    com.setIdPosible(rs.getInt("FCRESPUESTA"));
	    com.setIdObserv(rs.getInt("FIREQOBS"));
	    com.setPosible(rs.getString("POSIBLE"));
	    com.setPregunta(rs.getString("PREGUNTA"));   
	    com.setIdcritica(rs.getInt("FICRITICA"));
	   // com.setRuta(rs.getString("RUTA"));
	    com.setIdcheckUsua(rs.getInt("FIID_CHECK_USUA"));
	    com.setIdUsu(rs.getInt("FIID_USUARIO"));
	    com.setCeco(rs.getString("FCID_CECO"));
	    com.setChecklist(rs.getString("CHECKLIST"));
	    com.setIdOrdenGru(rs.getInt("FIORDEN_GRUPO"));
	    com.setIdperiodicidad(rs.getInt("FCPERIODO"));
	    com.setRespuestaAbierta(rs.getString("RESP_ABIERTA"));
	    com.setIdRespAb(rs.getInt("FIID_RESPUESTA_AD"));
	    com.setBanderaRespAb(rs.getInt("EVIDEN_AD"));
	    com.setModulo(rs.getString("MODULO"));
	    com.setCalif(rs.getDouble("FICALIFICACION"));
	    com.setPrecalif(rs.getDouble("FIPRECALIF"));
	    com.setClasif(rs.getString("FCCLASIFICA"));
	    com.setPonderacion(rs.getString("FCPONDTOTAL"));
	    
	  
		return com;
	}

}
