package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.DistribCecoDTO;

public class DistribCecoRowMapper implements RowMapper<DistribCecoDTO> {

	public DistribCecoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		DistribCecoDTO distribDTO = new DistribCecoDTO();
		distribDTO.setCeco(rs.getString("FCID_CECO"));
		distribDTO.setNombre(rs.getString("FCNOMBRE"));
		distribDTO.setTerritorio(rs.getString("FCID_TERR"));
		distribDTO.setZona(rs.getString("FCID_ZONA"));
		distribDTO.setRegion(rs.getString("FCID_REGI"));
		distribDTO.setGerente(rs.getString("FCID_GERE"));
		distribDTO.setUsuario(rs.getString("FCUSUARIO_MOD"));
		distribDTO.setFecha(rs.getString("FDFECHA_MOD"));
		
		
		return distribDTO;
		
		
	}

}
