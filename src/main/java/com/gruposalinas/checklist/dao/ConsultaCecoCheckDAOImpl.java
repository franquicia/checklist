package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ConsultaCecoCheckDTO;
import com.gruposalinas.checklist.domain.ConsultaCecoCheckDTO2;
import com.gruposalinas.checklist.mappers.ConsultaCecoCheckRowMapper;
import com.gruposalinas.checklist.mappers.EmpFijoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ConsultaCecoCheckDAOImpl extends DefaultDAO implements ConsultaCecoCheckDAO {

    private static Logger logger = LogManager.getLogger(ConsultaCecoCheckDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneCecoChecklist;
    DefaultJdbcCall jdbcObtieneDatos;
    DefaultJdbcCall jdbcObtieneDato;

    public void init() {

        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAUSUCECOCHECLS")
                .withProcedureName("VAL_CECOCHECK")
                .returningResultSet("CU_CHECKL", new EmpFijoRowMapper())
                .returningResultSet("CU_CECOS", new ConsultaCecoCheckRowMapper());

        jdbcObtieneCecoChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAUSUCECOCHECLS")
                .withProcedureName("USU_CECCHEK")
                .returningResultSet("CU_CHECKL", new EmpFijoRowMapper())
                .returningResultSet("CU_CECOS", new ConsultaCecoCheckRowMapper());

        jdbcObtieneDatos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAUSUCECOCHECLS")
                .withProcedureName("USU_CECCHEK")
                .returningResultSet("CU_CHECKL", new EmpFijoRowMapper())
                .returningResultSet("CU_CECOS", new ConsultaCecoCheckRowMapper());

        jdbcObtieneDato = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAUSUCECOCHECLS")
                .withProcedureName("USU_CECCHEK")
                .returningResultSet("CU_CHECKL", new EmpFijoRowMapper())
                .returningResultSet("CU_CECOS", new ConsultaCecoCheckRowMapper());

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<ConsultaCecoCheckDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<ConsultaCecoCheckDTO> listaFijo = null;
        List<ConsultaCecoCheckDTO2> listaCeco = null;
        int error = 0;

        out = jdbcObtieneTodo.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAUSUCECOCHECLS.VAL_CECOCHECK}");

        listaFijo = (List<ConsultaCecoCheckDTO>) out.get("CU_CHECKL");
        listaCeco = (List<ConsultaCecoCheckDTO2>) out.get("CU_CECOS");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener los ");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //parametro

    @SuppressWarnings("unchecked")
    public List<ConsultaCecoCheckDTO> obtieneDatos(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ConsultaCecoCheckDTO> lista = null;
        List<ConsultaCecoCheckDTO2> listaCeco = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSU", idUsuario);

        out = jdbcObtieneCecoChecklist.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAUSUCECOCHECLS.SP_SEL_EMP}");
        //lleva el nombre del cursor del procedure
        lista = (List<ConsultaCecoCheckDTO>) out.get("CU_CHECKL");
        listaCeco = (List<ConsultaCecoCheckDTO2>) out.get("CU_CECOS");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener el empleado(" + idUsuario + ")");
        }
        return lista;

    }

//con parametro map
    @Override
    public Map<String, Object> obtieneDat(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> dat = new HashMap<String, Object>();
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSU", idUsuario);

        out = jdbcObtieneDato.execute(in);

        logger.info("Funcion Ejecutada:{checklist.PAUSUCECOCHECLS.USU_CECCHEK}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            dat.put("Ceco", out.get("CU_CHECKL"));
            dat.put("Checklist", out.get("CU_CECOS"));
        } else {
            dat.put("Ceco", new ArrayList<ConsultaCecoCheckDTO>());
            dat.put("Checklist", new ArrayList<ConsultaCecoCheckDTO2>());
        }
        return dat;
    }

    @Override
    public Map<String, Object> obtieneDato() throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> dat = new HashMap<String, Object>();
        int ejecucion = 0;

        out = jdbcObtieneDatos.execute();

        logger.info("funcion Ejecutada :{checklist.PAUSUCECOCHECLS.VAL_CECOCHECK ");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            dat.put("Ceco", out.get("CU_CHECKL"));
            dat.put("Checklist", out.get("CU_CECOS"));
        } else {
            dat.put("Ceco", new ArrayList<ConsultaCecoCheckDTO>());
            dat.put("Checklist", new ArrayList<ConsultaCecoCheckDTO2>());
        }

        return dat;
    }

}
