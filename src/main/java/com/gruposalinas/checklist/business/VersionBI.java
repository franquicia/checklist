package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.VersionDAO;
import com.gruposalinas.checklist.domain.VersionDTO;


public class VersionBI {
	private static Logger logger = LogManager.getLogger(PeriodoBI.class);
	@Autowired
	VersionDAO versionDAO;
	
	List<VersionDTO> listaVersion = null;
	List<String> listaVersionFecha = null;
	

	
	public List<VersionDTO> obtieneVersion(String version, String sistema, String so)  {
				
		try{
			listaVersion = versionDAO.obtieneVersion(version, sistema, so);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la Versión");
		}
				
		return listaVersion;
	}	
	
	public  List<String> obtieneUltVersion(String so) {
    
		try{
			listaVersionFecha = versionDAO.obtieneUltVersion(so);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la Versión");
		}
				
		return listaVersionFecha;
	}	
	
	public int  insertaVersion(VersionDTO bean) {
		
		int idGrupo = 0;  
		
		try {		
			idGrupo = versionDAO.insertaVersion(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar la Versión");
		}
		
		return idGrupo;		
	}

	public boolean actualizaVersion(VersionDTO bean) throws Exception {
		
		boolean respuesta = false;
				
		try {
			respuesta = versionDAO.actualizaVersion(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar la Versión");
		}
							
		return respuesta;
	}
	
	public  boolean eliminaVersion(int idVersion) {
		
		boolean respuesta = false;
		
		try{
			respuesta = versionDAO.eliminaVersion(idVersion);
		}catch(Exception e){
			logger.info("No fue posible borrar la Versión");
		}
		
		return respuesta;
	}
}
