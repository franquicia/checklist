package com.gruposalinas.checklist.domain;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class MisTareasHeadDTO {
	@SerializedName("IdTarea") private String idTarea;
	@SerializedName("Titulo") private String titulo;
	@SerializedName("Inicio") private String inicio;
	@SerializedName("Fin") private String fin;
	@SerializedName("Puestos") private ArrayList<Puestos> puestos;

	public String getIdTarea() {
		return idTarea;
	}

	public void setIdTarea(String idTarea) {
		this.idTarea = idTarea;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getInicio() {
		return inicio;
	}

	public void setInicio(String inicio) {
		this.inicio = inicio;
	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	public ArrayList<Puestos> getPuestos() {
		return puestos;
	}

	public void setPuestos(ArrayList<Puestos> puestos) {
		this.puestos = puestos;
	}

	class Puestos {
		@SerializedName("IdPuesto") private String idPuesto;
		@SerializedName("DescPuesto") private String descPuesto;
		
		public String getIdPuesto() {
			return idPuesto;
		}
		public void setIdPuesto(String idPuesto) {
			this.idPuesto = idPuesto;
		}
		public String getDescPuesto() {
			return descPuesto;
		}
		public void setDescPuesto(String descPuesto) {
			this.descPuesto = descPuesto;
		}
	}
}
