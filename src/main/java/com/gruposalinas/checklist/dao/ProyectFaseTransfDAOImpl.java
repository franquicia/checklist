package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ProyectFaseTransfDTO;
import com.gruposalinas.checklist.mappers.ProyectFaseTransfRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ProyectFaseTransfDAOImpl extends DefaultDAO implements ProyectFaseTransfDAO {

    private static Logger logger = LogManager.getLogger(ProyectFaseTransfDAOImpl.class);

    DefaultJdbcCall jdbcInsertaProyecto;
    DefaultJdbcCall jdbcEliminaProyecto;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneProyecto;
    DefaultJdbcCall jbdcActualizaProyecto;
    DefaultJdbcCall jbdcActualizaProyectoNvo;
//fase
    DefaultJdbcCall jdbcInsertaFase;
    DefaultJdbcCall jdbcEliminaFase;
    DefaultJdbcCall jdbcObtieneTodoFase;
    DefaultJdbcCall jdbcObtieneFase;
    DefaultJdbcCall jbdcActualizaFase;

    public void init() {

        jdbcInsertaProyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPROYFASE")
                .withProcedureName("SP_INS_PROY");

        jdbcEliminaProyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPROYFASE")
                .withProcedureName("SP_DEL_PROY");

        jdbcObtieneProyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPROYFASE")
                .withProcedureName("SP_SEL_PROY")
                .returningResultSet("RCL_PROYE", new ProyectFaseTransfRowMapper());

        jbdcActualizaProyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPROYFASE")
                .withProcedureName("SP_ACT_PROY");

        jbdcActualizaProyectoNvo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPROYFASE")
                .withProcedureName("SP_ACT_PROYNVO");

    }

    @Override
    public int insertaProyec(ProyectFaseTransfDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NOMBRE", bean.getNomProy())
                .addValue("PA_OBS", bean.getObsProy())
                .addValue("PA_TIPOPROY", bean.getIdTipoProyecto())
                .addValue("PA_EDOCARGA", bean.getEdoCargaIni())
                .addValue("PA_HALLAMAX", bean.getHallazgoEdoMax());

        out = jdbcInsertaProyecto.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMPROYFASE.SP_INS_PROY}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_PROY");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el Empleado");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    @Override
    public boolean eliminaProyec(String idProyecto) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_PROY", idProyecto);

        out = jdbcEliminaProyecto.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMPROYFASE.SP_DEL_PROYECTO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el proyecto id: (" + idProyecto + ")");
        } else {
            return true;
        }

        return false;

    }

    @SuppressWarnings("unchecked")
    public List<ProyectFaseTransfDTO> obtieneDatosProyec(String idProyecto) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ProyectFaseTransfDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROY", idProyecto);

        out = jdbcObtieneProyecto.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMPROYFASE.SP_SEL_PROYECTO}");
        //lleva el nombre del cursor del procedure
        lista = (List<ProyectFaseTransfDTO>) out.get("RCL_PROYE");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el proyecto con id: (" + idProyecto + ")");
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<ProyectFaseTransfDTO> obtieneInfoProyec() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean actualizaProyec(ProyectFaseTransfDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROY", bean.getIdProyecto())
                .addValue("PA_NOMBRE", bean.getNomProy())
                .addValue("PA_OBS", bean.getObsProy())
                .addValue("PA_STATUS", bean.getIdStatus());

        out = jbdcActualizaProyecto.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMPROYFASE.SP_ACT_PROY}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdProyecto() + ")");
        } else {
            return true;
        }
        return false;

    }

    @Override
    public boolean actualizaProyecNvo(ProyectFaseTransfDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PROY", bean.getIdProyecto())
                .addValue("PA_NOMBRE", bean.getNomProy())
                .addValue("PA_OBS", bean.getObsProy())
                .addValue("PA_STATUS", bean.getIdStatus())
                .addValue("PA_EDOCARGA", bean.getEdoCargaIni())
                .addValue("PA_TIPOPROY", bean.getIdTipoProyecto())
                .addValue("PA_HALLAMAX", bean.getHallazgoEdoMax());

        out = jbdcActualizaProyectoNvo.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMPROYFASE.SP_ACT_PROYNVO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdProyecto() + ")");
        } else {
            return true;
        }
        return false;

    }

}
