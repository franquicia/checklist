package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.PaisDTO;

public class FiltrosPaisRowMapper implements RowMapper<PaisDTO> {

	@Override
	public PaisDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		PaisDTO paisDTO = new PaisDTO();
		
		paisDTO.setIdPais(rs.getInt("FIID_PAIS"));
		paisDTO.setNombre(rs.getString("FCNOMBRE"));
		
		return paisDTO;
	}

}
