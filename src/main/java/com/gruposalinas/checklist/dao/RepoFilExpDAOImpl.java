package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.RepoFilExpDTO;
import com.gruposalinas.checklist.domain.SoftNvoDTO;
import com.gruposalinas.checklist.mappers.PregNoImpCecoFaaseProyectoRowMapper;
import com.gruposalinas.checklist.mappers.RepoDash2RowMapper;
import com.gruposalinas.checklist.mappers.RepoDashExpCecoFaseRowMapper;
import com.gruposalinas.checklist.mappers.RepoDashExpGenRowMapper;
import com.gruposalinas.checklist.mappers.RepoDashExpRowMapper;
import com.gruposalinas.checklist.mappers.RepoFechasExpRowMapper;
import com.gruposalinas.checklist.mappers.RepoFilExpFil3RowMapper;
import com.gruposalinas.checklist.mappers.RepoFilExpPregRowMapper;
import com.gruposalinas.checklist.mappers.RepoFilExpRowMapper;
import com.gruposalinas.checklist.mappers.RepoFiltro1CecoFaseExpRowMapper;
import com.gruposalinas.checklist.mappers.RepoFiltro1ExpRowMapper;
import com.gruposalinas.checklist.mappers.SoftCalulosRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class RepoFilExpDAOImpl extends DefaultDAO implements RepoFilExpDAO {

    private static Logger logger = LogManager.getLogger(RepoFilExpDAOImpl.class);

    DefaultJdbcCall jdbcObtieneFiltro1FaseProy;
    DefaultJdbcCall jdbcObtieneDashGenerico;
    DefaultJdbcCall jdbcObtieneFiltro1;
    DefaultJdbcCall jdbcObtienefiltro2;
    DefaultJdbcCall jdbcObtienefiltro3;
    DefaultJdbcCall jdbcObtieneDash;
    DefaultJdbcCall jdbcObtieneDash2;
    DefaultJdbcCall jdbcObtieneDash3;
    DefaultJdbcCall jdbcObtienePreguntas;
    DefaultJdbcCall jdbcObtienePreguntasHij;
    DefaultJdbcCall jdbcObtieneRepoFechas;
    DefaultJdbcCall jdbcObtieneRepoActa;
    DefaultJdbcCall jdbcObtienePregActa;
    DefaultJdbcCall jdbcObtieneRepoFechasNvo;
    DefaultJdbcCall jdbcObtieneDashNvo;

    DefaultJdbcCall jdbcObtieneRepoActaCecoFaseProyecto;
    DefaultJdbcCall jdbcObtienePregActaCecoFaseProyecto;

    public void init() {

        //tFiltro 1
        jdbcObtieneFiltro1 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAFILREPOEXP")
                .withProcedureName("SP_FILTRO1")
                .returningResultSet("RCL_PREG", new RepoFiltro1ExpRowMapper());

        //tFiltro 2
        jdbcObtienefiltro2 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAFILREPOEXP")
                .withProcedureName("SP_FILTRO2")
                .returningResultSet("RCL_PREG", new RepoFilExpRowMapper());

        // trae preguntas
        jdbcObtienePreguntas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAFILREPOEXP")
                .withProcedureName("SP_PREGUNTAS")
                .returningResultSet("RCL_PREG", new RepoFilExpPregRowMapper());

        // trae preguntas hijas
        jdbcObtienePreguntasHij = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAFILREPOEXP2")
                .withProcedureName("SP_PREGUNTAS")
                .returningResultSet("RCL_PREG", new RepoFilExpPregRowMapper());

        //tFiltro 3
        jdbcObtienefiltro3 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAFILREPOEXP2")
                .withProcedureName("SP_FILTRO3")
                .returningResultSet("RCL_PREG", new RepoFilExpFil3RowMapper());
        //dash
        jdbcObtieneDash = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADASHEXP")
                .withProcedureName("SP_DASH")
                .returningResultSet("RCL_PREG", new RepoDashExpRowMapper());
        //dash2  PREGUNTAS
        jdbcObtieneDash2 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADASHEXP")
                .withProcedureName("SP_DASH2")
                .returningResultSet("RCL_PREG", new RepoDash2RowMapper());

        //dash3  RESPUESTAS
        jdbcObtieneDash3 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADASH2EXP")
                .withProcedureName("SP_DASH2")
                .returningResultSet("RCL_PREG", new RepoFilExpFil3RowMapper());

        //REPORTE FECHAS
        jdbcObtieneRepoFechas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPODETAEXT")
                .withProcedureName("SP_SELDETALLE")
                .returningResultSet("RCL_CECOS", new RepoFechasExpRowMapper());

        //reporte acta
        jdbcObtieneRepoActa = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOACTA")
                .withProcedureName("SP_REPOACTA")
                .returningResultSet("RCL_PREG", new RepoFilExpFil3RowMapper());

        //preguntas reporte acta
        jdbcObtienePregActa = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOACTA")
                .withProcedureName("SP_PREGUNTAS")
                .returningResultSet("RCL_PREG", new RepoFilExpPregRowMapper());
        //REPORTE FECHAS
        jdbcObtieneRepoFechasNvo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOFECHAS")
                .withProcedureName("SP_SELDETALLE")
                .returningResultSet("RCL_CECOS", new RepoFechasExpRowMapper());

        //dashboard generico nuevo
        jdbcObtieneDashGenerico = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADASH2EXP")
                .withProcedureName("SP_REPOGENE")
                .returningResultSet("RCL_PREG", new RepoDashExpGenRowMapper());

        //tFiltro 1 	NUEVO EXPANSION SE AGREGO RECORRIDO
        jdbcObtieneFiltro1FaseProy = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAFILEXP")
                .withProcedureName("SP_FILTRO1")
                .returningResultSet("RCL_PREG", new RepoFiltro1CecoFaseExpRowMapper())
                .returningResultSet("RCL_SOFT", new SoftCalulosRowMapper());
        //dash ceco fase proyecto
        jdbcObtieneDashNvo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADASHEXPNVO")
                .withProcedureName("SP_SEL_DASH")
                .returningResultSet("RCL_HALLAZ", new RepoDashExpCecoFaseRowMapper());

        //reporte acta ceco fase proyecto
        jdbcObtieneRepoActaCecoFaseProyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOACTAEXP")
                .withProcedureName("SP_REPOACTA")
                .returningResultSet("RCL_PREG", new PregNoImpCecoFaaseProyectoRowMapper());

        //reporte acta ceco fase proyecto
        jdbcObtienePregActaCecoFaseProyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOACTAEXP")
                .withProcedureName("SP_PREGUNTAS")
                .returningResultSet("RCL_PREG", new RepoFilExpPregRowMapper());

    }

    // Filtro 1
    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> obtieneDatosGen(String ceco, String nombreCeco, String fechaIni, String fechaFin, String idRecorrido) throws Exception {
        Map<String, Object> out = null;
        List<RepoFilExpDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco.equalsIgnoreCase("") ? null : ceco)
                .addValue("PA_NOMBRECECO", nombreCeco.equalsIgnoreCase("") ? null : nombreCeco)
                .addValue("PA_FECHAINICIO", fechaIni.equalsIgnoreCase("") ? null : fechaIni)
                .addValue("PA_FECHAFIN", fechaFin.equalsIgnoreCase("") ? null : fechaFin)
                .addValue("PA_RECORRIDO", idRecorrido.equalsIgnoreCase("") ? null : idRecorrido);

        out = jdbcObtieneFiltro1.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAFILREPOEXP.SP_FILTRO1}");

        listaFijo = (List<RepoFilExpDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    // Filtro 2
    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> obtieneInfo(String ceco, String idRecorrido) throws Exception {
        Map<String, Object> out = null;
        List<RepoFilExpDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_RECORRIDO", idRecorrido);

        out = jdbcObtienefiltro2.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAFILREPOEXP.SP_FILTRO2}");

        listaFijo = (List<RepoFilExpDTO>) out.get("RCL_PREG");
        logger.info("*lista de areas*: " + listaFijo);

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener EL CONTEO DE PREGUNTAS POR GRUPO");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    // Preguntas
    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> obtienePreguntas(String ceco) throws Exception {
        Map<String, Object> out = null;
        List<RepoFilExpDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco);

        out = jdbcObtienePreguntas.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAFILREPOEXP.SP_PREGUNTAS}");

        listaFijo = (List<RepoFilExpDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las preguntas ");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    // Preguntas hijas
    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> obtienePreguntasHijas(String ceco) throws Exception {
        Map<String, Object> out = null;
        List<RepoFilExpDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", ceco);

        out = jdbcObtienePreguntasHij.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAFILREPOEXP2.SP_PREGUNTAS}");

        listaFijo = (List<RepoFilExpDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las preguntas ");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    //Filtro 3
    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> obtieneInfofiltro(String bita1, String bita2, String pregFilt) throws Exception {
        Map<String, Object> out = null;
        List<RepoFilExpDTO> lista = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PREGUNTA", pregFilt)
                .addValue("PA_BITA1", bita1)
                .addValue("PA_BITA2", bita2);

        out = jdbcObtienefiltro3.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAFILREPOEXP2.SP_FILTRO3}");

        lista = (List<RepoFilExpDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las preguntas ");
        } else {
            return lista;
        }

        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> obtienedash(String ceco, String idRecorrido, String aux) throws Exception {
        Map<String, Object> out = null;
        List<RepoFilExpDTO> lista = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_RECORRIDO", idRecorrido)
                .addValue("PA_TIPREPO", aux);

        out = jdbcObtieneDash.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PADASHEXP.SP_DASH}");

        lista = (List<RepoFilExpDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las preguntas ");
        } else {
            return lista;
        }

        return lista;
    }
//dash 2 preg padres

    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> obtienedash2(String ceco) throws Exception {
        Map<String, Object> out = null;
        List<RepoFilExpDTO> lista = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco);

        out = jdbcObtieneDash2.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PADASHEXP.SP_DASH2}");

        lista = (List<RepoFilExpDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las preguntas ");
        } else {
            return lista;
        }

        return lista;
    }

    //dash 2 preg hijas
    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> obtienedash3(String bita1) throws Exception {
        Map<String, Object> out = null;
        List<RepoFilExpDTO> lista = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bita1);

        out = jdbcObtieneDash3.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PADASH2EXP.SP_DASH2}");

        lista = (List<RepoFilExpDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las preguntas ");
        } else {
            return lista;
        }

        return lista;
    }

    @Override
    public String resp_preg(String bita1) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> repoFechas(String fechaIni, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        List<RepoFilExpDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FECHAINICIO", fechaIni)
                .addValue("PA_FECHAFIN", fechaFin);

        out = jdbcObtieneRepoFechas.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPODETAEXT.SP_SELDETALLE}");

        listaFijo = (List<RepoFilExpDTO>) out.get("RCL_CECOS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    //repo acta
    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> obtieneInfofActa(String bita1) throws Exception {
        Map<String, Object> out = null;
        List<RepoFilExpDTO> lista = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bita1);

        out = jdbcObtieneRepoActa.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPOACTA.SP_REPOACTA}");

        lista = (List<RepoFilExpDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos de la bitacora(" + bita1 + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;

    }
    // Preguntas

    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> obtienePreguntasActa(String bita1) throws Exception {
        Map<String, Object> out = null;
        List<RepoFilExpDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITA", bita1);

        out = jdbcObtienePregActa.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPOACTA.SP_PREGUNTAS}");

        listaFijo = (List<RepoFilExpDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos de la bitacora(" + bita1 + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return listaFijo;
    }

    //reporte fechas nuevo
    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> repoFechasNvo(String fechaIni, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        List<RepoFilExpDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FECHAINICIO", fechaIni)
                .addValue("PA_FECHAFIN", fechaFin);

        out = jdbcObtieneRepoFechasNvo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPOFECHAS.SP_SELDETALLE}");

        listaFijo = (List<RepoFilExpDTO>) out.get("RCL_CECOS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> obtieneInfoDashboardGenerico(String bita1, String parametro) throws Exception {
        Map<String, Object> out = null;
        List<RepoFilExpDTO> lista = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITA", bita1)
                .addValue("PA_PARAM", parametro);

        out = jdbcObtieneDashGenerico.execute(in);

        logger.info("Funcin ejecutada: {FRANQUICIA.PAFILREPOEXP2.SP_FILTRO3}");

        lista = (List<RepoFilExpDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las preguntas ");
        } else {
            return lista;
        }

        return lista;

    }

    @SuppressWarnings("unchecked")//WEB
    public List<RepoFilExpDTO> obtieneDatosFiltro1(String ceco, String fase, String proyecto,
            String nombreCeco, String recorrido, String fechaIni, String fechaFin) throws Exception {
        // Filtro 1
        Map<String, Object> out = null;
        List<RepoFilExpDTO> listaFijo = null;
        List<SoftNvoDTO> listaCalculos = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto)
                .addValue("PA_NOMBRECECO", nombreCeco)
                .addValue("PA_FECHAINICIO", fechaIni)
                .addValue("PA_FECHAFIN", fechaFin)
                .addValue("PA_RECORRIDO", recorrido);

        out = jdbcObtieneFiltro1FaseProy.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAFILREPOEXP.SP_FILTRO1}");

        listaFijo = (List<RepoFilExpDTO>) out.get("RCL_PREG");
        listaCalculos = (List<SoftNvoDTO>) out.get("RCL_SOFT");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    // trae preguntas
    @SuppressWarnings("unchecked")//LO OCUPA ALEX
    public Map<String, Object> obtieneDatosFiltro1Fase(String ceco, String fase, String proyecto,
            String nombreCeco, String recorrido, String fechaIni, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<RepoFilExpDTO> listaSoft = null;
        List<SoftNvoDTO> listaCalculos = null;
        int error = 0;
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco.equals("") ? null : ceco)
                .addValue("PA_FASE", fase.equals("") ? null : fase)
                .addValue("PA_PROYECTO", proyecto.equals("") ? null : proyecto)
                .addValue("PA_NOMBRECECO", nombreCeco.equals("") ? null : nombreCeco)
                .addValue("PA_FECHAINICIO", fechaIni.equals("") ? null : fechaIni)
                .addValue("PA_FECHAFIN", fechaFin.equals("") ? null : fechaFin)
                .addValue("PA_RECORRIDO", recorrido.equals("") ? null : recorrido);

        out = jdbcObtieneFiltro1FaseProy.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAFILREPOEXP.SP_FILTRO1string}");
        // lleva el nombre del cursor del procedure
        listaSoft = (List<RepoFilExpDTO>) out.get("RCL_PREG");
        listaCalculos = (List<SoftNvoDTO>) out.get("RCL_SOFT");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener datos ");
        }
        lista = new HashMap<String, Object>();
        lista.put("listaSoft", listaSoft);
        lista.put("listaCalculos", listaCalculos);

        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> obtienedashCecoProyecto(String ceco, String fase, String proyecto) throws Exception {
        // Filtro 1
        Map<String, Object> out = null;
        List<RepoFilExpDTO> listaFijo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto);

        out = jdbcObtieneDashNvo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PADASHEXPNVO.SP_DASHNVO}");

        listaFijo = (List<RepoFilExpDTO>) out.get("RCL_HALLAZ");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> obtieneActaCecoFaseProyecto(String ceco, String fase, String proyecto)
            throws Exception {
        Map<String, Object> out = null;
        List<RepoFilExpDTO> lista = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto);

        out = jdbcObtieneRepoActaCecoFaseProyecto.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPOACTAEXP.SP_REPOACTA}");

        lista = (List<RepoFilExpDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos del ceco(" + ceco + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<RepoFilExpDTO> obtienePregActaCecoFaseProyecto(String ceco, String fase, String proyecto)
            throws Exception {
        Map<String, Object> out = null;
        List<RepoFilExpDTO> lista = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto);

        out = jdbcObtienePregActaCecoFaseProyecto.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPOACTAEXP.SP_PREGUNTAS}");

        lista = (List<RepoFilExpDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos del ceco(" + ceco + ")");
        }
        if (error == -1) {
            {
                return null;
            }
        }
        return lista;
    }

}
