<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css"
	href="../css/expancion/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="../css/expancion/detalleExpancion.css">
<script src="../js/expancion/jquery-3.3.1.js"></script>
<script src="../js/expancion/jquery.dataTables.js"></script>
<script src="../js/expancion/tabla.js"></script>
<style type="text/css">
</style>

<title>DETALLES HALLAZGOS</title>
</head>
<body style="max-width: 1500;   margin: auto;">
	<div class="marginTotal">
		<div class="left">
			<div class="centrar">
				<img alt="elektra" src="../images/expancion/LogoElektra.svg"
					class="imagenBanner"> <img alt="baz"
					src="../images/expancion/LogoBAZ.svg" class="imagenBanner">
			</div>
		</div>
		<div class="main">
			<h1 style="color: #6B696E;" class="fuenteBold">
				Entrega de Sucursales / <strong class="fuenteBold">DETALLES
					HALLAZGOS</strong>
			</h1>
			<br />

		</div>
		<div class="right">
			<div class="centrar">
				<img alt="elektra" src="../images/expancion/LogoElektra.svg"
					class="imagenBanner"> <img alt="baz"
					src="../images/expancion/LogoBAZ.svg" class="imagenBanner">
			</div>
		</div>

	</div>


	<div class="marginTotal">
		<div style="width: 60%;" class="centrar">
			<div>
				<c:if test="${imagenDetalle != '' }">
					<img alt="imagendetalle"
						src="../images/expancion/${imagenDetalle}.svg"
						class="imagenBanner">
				</c:if>
			</div>
			<div
				style="background: #FE003D; padding-bottom: 2%; padding-top: 2%; margin-top: 2%; margin-bottom: 2%; color: #FFFFFF">
				<h2 class="fuenteBold">${titulo}</h2>
			</div>
		</div>

	</div>


	<div class="marginTotal">
		<div style="width: 80%; margin: auto;">
			<table id="tabla" class="tablaChecklist">
				<thead>
					<tr>
						<th>Item</th>
						<th>N/A</th>
						<th>SLA Días</th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${chklistPreguntasImp != null }">
						<c:set var="count" value="0" scope="page" />
						<c:forEach var="valor" items="${chklistPreguntasImp}">
							<tr>
								<c:set var="count" value="${count + 1}" scope="page" />
								<td>${count}.-${valor.pregunta}</td>
								<td>No</td>
								<td> <img alt="elektra" src="http://10.53.33.82${valor.ruta}"
										class="" height="200" width="200">
								 </td>
							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
		</div>
	</div>



</body>
</html>