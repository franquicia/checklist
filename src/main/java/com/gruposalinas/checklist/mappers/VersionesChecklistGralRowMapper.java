package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.VersionChecklistGralDTO;

public class VersionesChecklistGralRowMapper implements RowMapper<VersionChecklistGralDTO> {

	public VersionChecklistGralDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		VersionChecklistGralDTO VersionChecklistGralDTO = new VersionChecklistGralDTO();
		
		VersionChecklistGralDTO.setIdTab(rs.getInt("FIID_CHECVERS"));
		VersionChecklistGralDTO.setIdCheck(rs.getInt("FIID_CHECKLIST"));
		VersionChecklistGralDTO.setIdVers(rs.getInt("FIVERSION"));
	    VersionChecklistGralDTO.setVersionAnt(rs.getInt("FIVERSANT"));
		VersionChecklistGralDTO.setIdactivo(rs.getInt("FIIDACTIVO"));
	    VersionChecklistGralDTO.setNombrecheck(rs.getString("FCNOMBRECHECK"));
	    VersionChecklistGralDTO.setObs(rs.getString("FCOBSERVACION"));
	    VersionChecklistGralDTO.setPeriodo(rs.getString("FCPERIODO"));
	   
	 
		return VersionChecklistGralDTO;
	}

}


