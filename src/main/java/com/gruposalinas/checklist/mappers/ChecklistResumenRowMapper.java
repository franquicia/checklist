package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistDTO;

public class ChecklistResumenRowMapper implements RowMapper<ChecklistDTO>  {
public ChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistDTO checklistgeneral = new ChecklistDTO();
		
		checklistgeneral.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		checklistgeneral.setNombreCheck(rs.getString("FCNOMBRE"));
		checklistgeneral.setFecha_inicio(rs.getString("FDFECHA_INICIO"));
		checklistgeneral.setFechaModificacion(rs.getString("FDFECHA_MOD"));
		checklistgeneral.setUsuarios(rs.getString("USUARIOS"));
		checklistgeneral.setVigente(rs.getInt("FIVIGENTE"));
		checklistgeneral.setIdEstado(rs.getInt("FIID_ESTADO"));
		return checklistgeneral;
	}
}
