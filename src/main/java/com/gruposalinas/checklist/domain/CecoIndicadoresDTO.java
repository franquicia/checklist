package com.gruposalinas.checklist.domain;

public class CecoIndicadoresDTO {
	private String idCeco;
	private String nombreCeco;
	private int negocio;
	private String nivel;
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	public int getNegocio() {
		return negocio;
	}
	public void setNegocio(int negocio) {
		this.negocio = negocio;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	@Override
	public String toString() {
		return "CecoIndicadoresDTO [idCeco=" + idCeco + ", nombreCeco=" + nombreCeco + ", negocio=" + negocio
				+ ", nivel=" + nivel + "]";
	}

}
