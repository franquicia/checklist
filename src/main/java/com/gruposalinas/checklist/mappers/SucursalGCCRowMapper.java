package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.SucursalDTO;

public class SucursalGCCRowMapper implements RowMapper<SucursalDTO> {

	@Override
	public SucursalDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		SucursalDTO sucursalDTO = new SucursalDTO();
		
		sucursalDTO.setIdSucursal(rs.getInt("FIID_SUCURSAL"));
		sucursalDTO.setIdPais(rs.getInt("FIID_PAIS"));
		sucursalDTO.setIdCanal(rs.getInt("FIID_CANAL"));
		sucursalDTO.setNuSucursal(rs.getString("FCNU_SUCURSAL"));
		sucursalDTO.setNombresuc(rs.getString("FCNOMBRECC"));
		sucursalDTO.setLongitud(rs.getDouble("FCLONGITUD"));
		sucursalDTO.setLatitud(rs.getDouble("FCLATITUD"));
		
		return sucursalDTO;
	}
	

}
