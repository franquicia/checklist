package com.gruposalinas.checklist.domain;

public class EmpContacExtDTO {
	


		private int idTab;
		private String  ceco;
		private int bitGral;
		private int idUsu;
		private String domicilio;
		private String nombre;
		private String telefono;
		private String contacto;
		private String area;
		private String compania;
		private String aux;
		private String aux2;
		private String periodo;
		
		public int getIdTab() {
			return idTab;
		}
		public void setIdTab(int idTab) {
			this.idTab = idTab;
		}
		public String getCeco() {
			return ceco;
		}
		public void setCeco(String ceco) {
			this.ceco = ceco;
		}
		public int getBitGral() {
			return bitGral;
		}
		public void setBitGral(int bitGral) {
			this.bitGral = bitGral;
		}
		public int getIdUsu() {
			return idUsu;
		}
		public void setIdUsu(int idUsu) {
			this.idUsu = idUsu;
		}
		public String getDomicilio() {
			return domicilio;
		}
		public void setDomicilio(String domicilio) {
			this.domicilio = domicilio;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getTelefono() {
			return telefono;
		}
		public void setTelefono(String telefono) {
			this.telefono = telefono;
		}
		public String getContacto() {
			return contacto;
		}
		public void setContacto(String contacto) {
			this.contacto = contacto;
		}
		public String getArea() {
			return area;
		}
		public void setArea(String area) {
			this.area = area;
		}
		public String getCompania() {
			return compania;
		}
		public void setCompania(String compania) {
			this.compania = compania;
		}
		public String getAux() {
			return aux;
		}
		public void setAux(String aux) {
			this.aux = aux;
		}
		public String getAux2() {
			return aux2;
		}
		public void setAux2(String aux2) {
			this.aux2 = aux2;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		@Override
		public String toString() {
			return "EmpContacExtDTO [idTab=" + idTab + ", ceco=" + ceco + ", bitGral=" + bitGral + ", idUsu=" + idUsu
					+ ", domicilio=" + domicilio + ", nombre=" + nombre + ", telefono=" + telefono + ", contacto="
					+ contacto + ", area=" + area + ", compania=" + compania + ", aux=" + aux + ", aux2=" + aux2
					+ ", periodo=" + periodo + "]";
		}
		
		
	}