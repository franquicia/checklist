<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<tiles:importAttribute name="javascripts"/>
<tiles:importAttribute name="stylesheets" ignore="true" />
<head>
<title><tiles:insertAttribute name="title" ignore="true" /></title>
<c:if test="${javascripts !=null }">

	<c:forEach var="script" items="${javascripts}">
		<script src="<c:url value="${script}"/>"></script>
	</c:forEach>

</c:if>
<c:if test="${stylesheets != null }">

    <!-- stylesheets-->
    <c:forEach var="css" items="${stylesheets}">
       <link rel="stylesheet" type="text/css" href="<c:url value="${css}"/>">
    </c:forEach>

</c:if>

<!-- <link rel="stylesheet" type="text/css" href="css/demo.css"> -->
<!-- <script type="text/javascript" src="js/jquery.js"></script> -->

<!-- CSS Carousel --> 
<link rel="stylesheet" type="text/css" href="../css/header-style.css">
<link rel="stylesheet" type="text/css" href="../css/carousel.css">
<link rel="stylesheet" type="text/css" href="../css/jQuery/jQuery.css">
<!-- CSS Diseño de checklist -->

<!--
<link href="../css/checklistDesign.css"
	rel="stylesheet" type="text/css" />
-->
<link href="../css/wickedpicker.css"
	rel="stylesheet" type="text/css" />
</head>

<!--Se define la estructura de este template, el cual contendra un encabezado y un body. Su contenido se define en el tilesDef.xml -->
<header>
<div class="checklists">
		<tiles:insertAttribute name="header"/>
	</div>
</header>
<div id="menu">
		<tiles:insertAttribute name="menu"/>
</div>
<div id="menuCheck">
		<tiles:insertAttribute name="menuCheck"/>
</div>
<body>
	<div id="cuerpo" style="height: 94%;">
		<tiles:insertAttribute name="body"/>
	</div>
</body>
</html>
	


