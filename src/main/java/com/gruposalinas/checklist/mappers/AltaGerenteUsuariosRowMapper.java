package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.AltaGerenteDTO;


public class AltaGerenteUsuariosRowMapper implements RowMapper<AltaGerenteDTO> {

	public AltaGerenteDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AltaGerenteDTO altaGerenteDTO = new AltaGerenteDTO();
		
		altaGerenteDTO.setIdUsuario(rs.getInt("FIUSUARIO"));
		altaGerenteDTO.setNomUsuario(rs.getString("FCNOMUSUARIO"));
		
	
		return altaGerenteDTO;
	}
}
