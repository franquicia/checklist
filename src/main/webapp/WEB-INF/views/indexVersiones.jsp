<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="../css/bootstrap/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap-table/bootstrap-table.min.css">

	<script	src="../js/jquery/jquery-2.2.4.min.js"></script>
	<script	src="../js/bootstrap.min.js"></script>
	<script	src="../js/bootstrap-table.min.js"></script>
	
	<c:choose>
		<c:when test="${action=='insert'}">
			<title>Soporte - Agregar versión</title>
		</c:when>
		<c:when test="${action=='update'}">
			<title>Soporte - Actualizar versión</title>
		</c:when>
		<c:when test="${action=='delete'}">
			<title>Soporte - Eliminar versión</title>
		</c:when>
		<c:otherwise>
			<title>Soporte - Lista de versiones</title>
		</c:otherwise>
	</c:choose>	

</head>
<body>

	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuSoporte.jsp" flush="true">
		<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuSoporte.jsp" />
	</tiles:insertTemplate>

	<div id="container">
		<c:choose>
			<c:when test="${action=='insert'}">
				<c:choose>
					<c:when test="${inserted=='si'}">
						<div class="container">
							<br>
							<div class="alert alert-success" role="alert">
								<p class="text-center"> 
									<h4>La versión fue insertada correctamente!</h4>
									<br>Presione el botón de regresar para insertar un registro nuevo.
								<p>
								<br>
								<c:url value="/soporte/insertVersion.htm" var="insertVersionUrl" />
								<form:form method="GET" action="${insertVersionUrl}" model="command" name="formInsertBack1">
									<input type="submit" class="btn btn-success btn-lg" value="Regresar" />
								</form:form>
							</div>
						</div>
					</c:when>
					<c:when test="${inserted=='no'}">
						<div class="container">
							<br>
							<div class="alert alert-danger" role="alert">
								<p class="text-center"> 
									<h4>La versión no fue insertada correctamente!</h4>
									<br>Presione el botón de regresar para intentarlo nuevamente.
								<p>
								<br>
								<c:url value="/soporte/insertVersion.htm" var="insertVersionUrl" />
								<form:form method="GET" action="${insertVersionUrl}" model="command" name="formInsertBack2">
									<input type="submit" class="btn btn-danger btn-lg" value="Regresar" />
								</form:form>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div id="container">
							<c:url value = "/soporte/insertVersion.htm" var = "insertVersionUrl" />
							<form:form method="POST" action="${insertVersionUrl}" model="command" name="formInsert" id="formInsert">
								<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
									<form:input type="text" class="form-control" id="descripcion" placeholder="Descripción" path="descripcion" />
									<form:input type="text" class="form-control" id="version" placeholder="Versión" path="version" />
									<form:input type="text" class="form-control" id="sistema" placeholder="Sistema" path="sistema" />
									<form:input type="text" class="form-control" id="so" placeholder="Sistema Operativo" path="so" />
									<input type="submit" class="btn btn-default btn-lg" value="Agregar" onclick="return validaInsert();"/>
								</div>
							</form:form>
						</div>
					</c:otherwise>
				</c:choose>
			
			</c:when>
			<c:when test="${action=='update'}">
				<c:choose>
					<c:when test="${updated=='si'}">
						<div class="container">
							<br>
							<div class="alert alert-success" role="alert">
								<p class="text-center"> 
									<h4>La versión fue actualizada correctamente!</h4>
									<br>Presione el botón de regresar para actualizar un registro nuevo.
								<p>
								<br>
								<c:url value="/soporte/updateVersion.htm" var="updateVersionUrl" />
								<form:form method="GET" action="${updateVersionUrl}" model="command" name="formUpdateBack1">
									<input type="submit" class="btn btn-success btn-lg" value="Regresar" />
								</form:form>
							</div>
						</div>
					</c:when>
					<c:when test="${updated=='no'}">
						<div class="container">
							<br>
							<div class="alert alert-danger" role="alert">
								<p class="text-center"> 
									<h4>La versión no fue actualizada correctamente!</h4>
									<br>Presione el botón de regresar para intentarlo nuevamente.
								<p>
								<br>
								<c:url value="/soporte/updateVersion.htm" var="updateVersionUrl" />
								<form:form method="GET" action="${updateVersionUrl}" model="command" name="formUpdateBack2">
									<input type="submit" class="btn btn-danger btn-lg" value="Regresar" />
								</form:form>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div id="container">
							<c:url value = "/soporte/updateVersion.htm" var = "updateVersionUrl" />
							<form:form method="POST" action="${updateVersionUrl}" model="command" name="formUpdate" id="formUpdate">
								<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">

									<form:select id="idVersion" name="idVersion" path="idVersion" class="form-control">
										<form:options items="${list}" />
									</form:select>

									<form:input type="text" class="form-control" id="descripcion" placeholder="Descripción" path="descripcion" />
									<form:input type="text" class="form-control" id="version" placeholder="Versión" path="version" />
									<form:input type="text" class="form-control" id="sistema" placeholder="Sistema" path="sistema" />
									<form:input type="text" class="form-control" id="so" placeholder="Sistema Operativo" path="so" />
									<input type="submit" class="btn btn-default btn-lg" value="Actualizar" onclick="return validaUpdate();"/>
								</div>
							</form:form>
						</div>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:when test="${action=='delete'}">
				<c:choose>
					<c:when test="${deleted=='si'}">
						<div class="container">
							<br>
							<div class="alert alert-success" role="alert">
								<p class="text-center"> 
									<h4>La versión fue eliminada correctamente!</h4>
									<br>Presione el botón de regresar para actualizar un registro nuevo.
								<p>
								<br>
								<c:url value="/soporte/deleteVersion.htm" var="deleteVersionUrl" />
								<form:form method="GET" action="${deleteVersionUrl}" model="command" name="formDeleteBack1">
									<input type="submit" class="btn btn-success btn-lg" value="Regresar" />
								</form:form>
							</div>
						</div>
					</c:when>
					<c:when test="${deleted=='no'}">
						<div class="container">
							<br>
							<div class="alert alert-danger" role="alert">
								<p class="text-center"> 
									<h4>La versión no fue eliminada correctamente!</h4>
									<br>Presione el botón de regresar para intentarlo nuevamente.
								<p>
								<br>
								<c:url value="/soporte/deleteVersion.htm" var="deleteVersionUrl" />
								<form:form method="GET" action="${deleteVersionUrl}" model="command" name="formDeleteBack2">
									<input type="submit" class="btn btn-danger btn-lg" value="Regresar" />
								</form:form>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div id="container">
							<c:url value = "/soporte/deleteVersion.htm" var = "deleteVersionUrl" />
							<form:form method="POST" action="${deleteVersionUrl}" model="command" name="formDelete" id="formDelete">
								<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-4">
									
									<form:select id="idVersion" name="idVersion" path="idVersion" class="form-control">
										<form:options items="${list}" />
									</form:select>

									<input type="submit" class="btn btn-default btn-lg" value="Eliminar" onclick="return validaDelete();"/>
								</div>
							</form:form>
						</div>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<div id="container">
					<br>
					<p class="text-center lead"><strong>Lista de Versiones</strong></p>	
					<br>
				</div>
				
				<div class="container">
					<table	class="table table-striped table-bordered table-hover table-condensed" 
							data-toggle="table" 
							data-sort-name="idVersion" 
							data-sort-order="asc"
							data-pagination="true"
							data-pagination-loop="true"
							data-page-size="25"
							data-height="540"
							data-smart-display="true"
							data-pagination-v-align="top"
							data-silent-sort="false"
							data-search="true"
							data-show-refresh="true"
							data-show-toggle="true"
							data-show-columns="true"
							data-show-pagination-switch="true">
						<thead>
							<tr>
								<th data-field="idVersion" data-sortable="true" data-searchable="true">ID Versión</th>
								<th data-field="descripcion" data-sortable="true" data-searchable="true">Descripción</th>
								<th data-field="version" data-sortable="true">Versión</th>
								<th data-field="sistema" data-sortable="true">Sistema</th>
								<th data-field="so" data-sortable="true" >SO</th>
								<th data-field="fecha" data-sortable="true" >Fecha</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="list" items="${list}">
								<tr>
									<td><c:out value="${list.idVersion}"></c:out></td>
									<td><c:out value="${list.descripcion}"></c:out></td>
									<td><c:out value="${list.version}"></c:out></td>
									<td><c:out value="${list.sistema}"></c:out></td>
									<td><c:out value="${list.so}"></c:out></td>
									<td><c:out value="${list.fecha}"></c:out></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:otherwise>
		</c:choose>
	</div>

	<br>
	<br>

	<script>
		function validaInsert() {
			var descripcion = document.getElementById("descripcion").value;
			var version = document.getElementById("version").value;
			var sistema = document.getElementById("sistema").value;
			var so = document.getElementById("so").value;

			if ((descripcion == '' || descripcion == null) 
					&& (version == '' || version == null) 
					&& (sistema == '' || sistema == null) 
					&& (so == '' || so == null)) {
				alert("Ningún campo puede ir vacío!");
				return false;
			}
			else {
				return true;
			}
		}
		
		function validaUpdate() {
			var idVersion = document.getElementById("idVersion").value;
			var descripcion = document.getElementById("descripcion").value;
			var version = document.getElementById("version").value;
			var sistema = document.getElementById("sistema").value;
			var so = document.getElementById("so").value;

			if ((idVersion == '' || idVersion == null) 
					&& (descripcion == '' || descripcion == null) 
					&& (version == '' || version == null) 
					&& (sistema == '' || sistema == null) 
					&& (so == '' || so == null)) {
				alert("Ningún campo puede ir vacío!");
				return false;
			}
			else {
				return true;
			}
		}
		
		function validaDelete() {
			var confirmDelete = confirm("¿Está seguro que desea eliminar este registro?");
			
			if ( confirmDelete == true ) {
				return true;
			}
			else {
				return false;
			}
		}
	</script>

</body>
</html>