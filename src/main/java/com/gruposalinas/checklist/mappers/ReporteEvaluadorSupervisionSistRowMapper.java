package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteSistemasDTO;

public class ReporteEvaluadorSupervisionSistRowMapper implements RowMapper<ReporteSistemasDTO> {

	public ReporteSistemasDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteSistemasDTO reporteCecosSistemas = new ReporteSistemasDTO();
		
		reporteCecosSistemas.setEvaluador(rs.getString("FCNOMBRE"));
		reporteCecosSistemas.setFecha(rs.getString("FECHA"));
		
		return reporteCecosSistemas;
	}
}

