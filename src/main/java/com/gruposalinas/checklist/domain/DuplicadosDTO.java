package com.gruposalinas.checklist.domain;

public class DuplicadosDTO {
	private int conteo;
	private int idBitacora;
	private int idArbol;
	public int getConteo() {
		return conteo;
	}
	public void setConteo(int conteo) {
		this.conteo = conteo;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public int getIdArbol() {
		return idArbol;
	}
	public void setIdArbol(int idArbol) {
		this.idArbol = idArbol;
	}
	

	
}
