package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.AsignacionCheckDTO;


public class AsignaCheckRowMapper implements RowMapper<AsignacionCheckDTO> {

	@Override
	public AsignacionCheckDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		AsignacionCheckDTO asignacionDTO = new AsignacionCheckDTO();
		
		asignacionDTO.setIdAsigna(rs.getInt("FIID_ASIGNA"));
		asignacionDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
		asignacionDTO.setNombreSucursal(rs.getString("FCNOMBRECC"));
		asignacionDTO.setNombreUsuario(rs.getString("FCNOMBREUSU"));
		asignacionDTO.setIdSucursal(rs.getString("FINUM_SUC"));
		asignacionDTO.setFecha(rs.getString("FDFECHA"));
		asignacionDTO.setBunker(rs.getInt("FIBUNKER"));
		asignacionDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		asignacionDTO.setHoraBunker(rs.getString("FCHORABUNKER"));
		asignacionDTO.setHoraBunkerSalida(rs.getString("FCHORABUNKERSALIDA"));
		asignacionDTO.setJustificacion(rs.getString("FCJUSTIFICACION"));
		asignacionDTO.setAsigna(rs.getInt("FIASIGNA"));
		asignacionDTO.setEnvio(rs.getString("FCENVIADO"));
		asignacionDTO.setCorreoD(rs.getString("FCCORREOD"));
		asignacionDTO.setCorreoC(rs.getString("FCCORREOC"));
		
		return asignacionDTO;
	}

}
