package com.gruposalinas.checklist.domain;

public class FoliosMtoDTO {
	
	private String idFolio;
	private String idSucursal;
	private String nomSucursal;
	private String nomIncidente;
	private String fechaProgramada;
	private String nomProveedor;
	private String estado; 
	private String motivoEstado;
	private String fechaModificacion;
	private String fechaCreacion;
	
	public String getIdFolio() {
		return idFolio;
	}

	public void setIdFolio(String idFolio) {
		this.idFolio = idFolio;
	}

	public String getIdSucursal() {
		return idSucursal;
	}

	public void setIdSucursal(String idSucursal) {
		this.idSucursal = idSucursal;
	}

	public String getNomSucursal() {
		return nomSucursal;
	}

	public void setNomSucursal(String nomSucursal) {
		this.nomSucursal = nomSucursal;
	}

	public String getNomIncidente() {
		return nomIncidente;
	}

	public void setNomIncidente(String nomIncidente) {
		this.nomIncidente = nomIncidente;
	}

	public String getFechaProgramada() {
		return fechaProgramada;
	}

	public void setFechaProgramada(String fechaProgramada) {
		this.fechaProgramada = fechaProgramada;
	}

	public String getNomProveedor() {
		return nomProveedor;
	}

	public void setNomProveedor(String nomProveedor) {
		this.nomProveedor = nomProveedor;
	}
	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMotivoEstado() {
		return motivoEstado;
	}

	public void setMotivoEstado(String motivoEstado) {
		this.motivoEstado = motivoEstado;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}


	@Override
	public String toString() {
		return "FoliosMtoDTO [idFolio=" + idFolio + 
				", idSucursal=" + idSucursal + 
				", nomSucursal=" + nomSucursal + 
				", nomIncidente=" + nomIncidente + 
				", fechaProgramada=" + fechaProgramada + 
				", nomProveedor=" + nomProveedor + 
				", estado=" + estado + 
				", motivoEstado=" + motivoEstado + 
				", fechaModificacion=" + fechaModificacion + 
				", fechaCreacion=" + fechaCreacion + 
				"]";
	}
}
