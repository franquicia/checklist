package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.HallazgosTransfDTO;
import com.gruposalinas.checklist.mappers.HallazgosMatrizRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class HallazgosMatrizDAOImpl extends DefaultDAO implements HallazgosMatrizDAO {

    private static Logger logger = LogManager.getLogger(HallazgosMatrizDAOImpl.class);

    DefaultJdbcCall jdbcInsertaHallazgoMatriz;
    DefaultJdbcCall jdbcEliminaHallazgoMatriz;
    DefaultJdbcCall jdbcObtieneHallazgoMatriz;
    DefaultJdbcCall jbdcActualizaHallazgoMatriz;

    public void init() {

        jdbcInsertaHallazgoMatriz = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMATHALLA")
                .withProcedureName("SP_INS_MAT");

        jdbcEliminaHallazgoMatriz = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMATHALLA")
                .withProcedureName("SP_DEL_MAT");

        jdbcObtieneHallazgoMatriz = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMATHALLA")
                .withProcedureName("SP_CONS_MAT")
                .returningResultSet("RCL_MATRIZ", new HallazgosMatrizRowMapper());

        jbdcActualizaHallazgoMatriz = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMATHALLA")
                .withProcedureName("SP_ACT_MAT");

    }

    public int insertaHallazgoMatriz(HallazgosTransfDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idTab = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_TIPOPROY", bean.getTipoProy())
                .addValue("PA_STATUSENVIO", bean.getStatusEnvio())
                .addValue("PA_STATUSHALLA", bean.getStatusHallazgo())
                .addValue("PA_PERFIL", bean.getIdPerfil())
                .addValue("PA_COLOR", bean.getColor())
                .addValue("PA_NOMSTATUS", bean.getNombreStatus());

        out = jdbcInsertaHallazgoMatriz.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMMATHALLA.SP_INS_MAT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDMATRIZ");
        idTab = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("No se inserto");
            idTab = 0;
        }

        return idTab;
    }

    public boolean eliminaHallazgoMatriz(String idMatriz) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDMATRIZ", idMatriz);

        out = jdbcEliminaHallazgoMatriz.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMMATHALLA.SP_DEL_MAT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar (" + idMatriz + ")");
        } else {
            return true;
        }

        return false;
    }

    //parametro
    @SuppressWarnings("unchecked")
    public List<HallazgosTransfDTO> obtieneHallazgoMatriz(String tipoProyecto) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<HallazgosTransfDTO> lista = new ArrayList<>();

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_TIPOPROY", tipoProyecto);

        out = jdbcObtieneHallazgoMatriz.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMMATHALLA.SP_CONS_MAT}");
        //lleva el nombre del cursor del procedure
        lista = (List<HallazgosTransfDTO>) out.get("RCL_MATRIZ");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1 && error != -1) {
            logger.info("Algo ocurrió al obtener los datos de la matriz(" + tipoProyecto + ")");
        }
        if (error == -1) {
            {
                logger.info("NO TIENE INFORMACIÓN");
                List<HallazgosTransfDTO> listaVacia = new ArrayList<>();
                return listaVacia;
            }
        }
        return lista;

    }

    // web
    public boolean actualizaHallazgoMatriz(HallazgosTransfDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_TIPOPROY", bean.getTipoProy())
                .addValue("PA_STATUSENVIO", bean.getStatusEnvio())
                .addValue("PA_STATUSHALLA", bean.getStatusHallazgo())
                .addValue("PA_PERFIL", bean.getIdPerfil())
                .addValue("PA_COLOR", bean.getColor())
                .addValue("PA_NOMSTATUS", bean.getNombreStatus())
                .addValue("PA_IDMATRIZ", bean.getIdMatriz());

        out = jbdcActualizaHallazgoMatriz.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMMATHALLA.SP_ACT_MAT}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id matriz ( " + bean.getIdMatriz() + ")");
        } else {
            return true;
        }
        return false;

    }

}
