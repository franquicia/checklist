<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html lang="es">
<head>
<title>Banco Azteca | Sistematización de actividades</title>
<link rel="stylesheet"
	href="../css/vistaCumplimientoVisitas/estilo.css" />
<script type="text/javascript">
	var conteoL = ${conteo};
	var nivelPerfilL = ${nivelPerfil};
	var porcentajeG = ${porcentajeG};
	var idCecoL = ${idCeco};
	var idCecoPadreL = ${idCecoPadre};
	var nombreCecoL = '${nombreCeco}';
	var banderaCecoPadreL = ${banderaCecoPadre};
	var seleccionAnoL = ${seleccionAno};
	var seleccionMesL = ${seleccionMes};
	var seleccionCanalL = ${seleccionCanal};
	var seleccionPaisL = ${seleccionPais};
</script>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- <link rel="stylesheet" type="text/css"
	href="../css/vistaCumplimientoVisitas/tooltipster.bundle.css" />  -->
<link rel="stylesheet" media="screen"
	href="../css/vistaCumplimientoVisitas/modal.css">
	
</head>
<body onload="nobackbutton();">

	<div class="header" style="z-index: 10;">
		<div class="logo">
			<c:if test="${!opcionReporte}">
				<a href="/checklist/"><img src="../images/vistaCumplimientoVisitas/logo.svg"></a>
			</c:if>
			<c:if test="${opcionReporte}">
				<a><img src="../images/vistaCumplimientoVisitas/logo.svg"></a>
			</c:if>		
		</div>
		<div class="title">
			<div class="box tleft">
				<a onclick="regresaVistaAnterior(${nivelPerfil},${idCecoPadre},'${nombreCeco}',${banderaCecoPadre},${seleccionAno},${seleccionMes},${seleccionCanal},${seleccionPais});"  class="hom"><img
					src="../images/vistaCumplimientoVisitas/regresar.svg"
					class="imgBack">
				</a> 
				<c:if test="${!opcionReporte}">
					<a href="../central/vistaCumplimientoVisitas.htm" class="active">
						<img src="../images/vistaCumplimientoVisitas/rev01W.svg">
					</a>
				</c:if>	
				<c:if test="${opcionReporte}">
					<a href="../central/vistaCumplimientoVisitasMovil.htm" class="active">
						<img src="../images/vistaCumplimientoVisitas/rev01W.svg">
					</a>
				</c:if>	
				
				<!--<a href="#" class=" "><img
					src="../images/vistaCumplimientoVisitas/rev02W.svg"></a>
				<a href="#" class=" "><img
					src="../images/vistaCumplimientoVisitas/rev03W.svg"></a>
				<a href="#" class=" "><img
					src="../images/vistaCumplimientoVisitas/rev04W.svg"></a>
				<a href="#" class=" "><img
					src="../images/vistaCumplimientoVisitas/rev05W.svg"></a>-->
			</div>
		</div>
		
		<div class="wrapperFixed" style="background:red; background:#f2f2f2;">
			
			<div class="goborder">
				<h2 id="idNombreFiltro"></h2>
			</div>
						
			<table class="avance" style="max-width: 800px; min-width: 350px;">
				<thead>
					<tr>
						<th colspan="3">Objetivo al d&iacute;a <a id="idDiaActual" style="color:#9e9e9e;">0</a></th>
					</tr>
					<tr>
						<th>Día 1 0%</th>
						<th class="w100">
							<div class="w3-light-grey w3-round">
								<div class="w3-container w3-blue" style="width: 1%" id="idBarPor">&nbsp;</div>
							</div>
						</th>
						<th>Día <a id="idTotalDias" style="color:#9e9e9e;">0</a> 100%</th>
					</tr>
				</thead>
			</table>

			<table class="bloquer3" style="max-width: 800px;  min-width: 350px;">
				<thead>
					<tr>
						<th>Region</th>
						<c:forEach items="${listaChecklist}" var="item">
						    <th style="width: ${64/conteo}%;">${item.nombreCheck}</th>
						</c:forEach>
				</thead>
			</table>
	
	
		</div>
		
	</div>

	<div class="wrapper">
		<div class="hspacer"></div>
		<div class="box" id="box" style="margin-top: 185px;">
			
			<form id="formDetalle" method="POST" action="vistaDetalleCumplimientoVisitas.htm">	
			
				<dl class="accordion3">
				
					<c:forEach var="j" begin="0" end="${listaSucursalesSize-1}">
						
						<c:if test="${(j%conteo)==0}">
							<c:if test="${j!=0}">
											</tr>								
										</tbody>
									</table>
								</dt>			
								<dd>
									<div class="inner1">
										<dl class="accordion" id="idLlenaSucursales${j-conteo}">										
										</dl>				
									</div>
								</dd>
							</c:if>	
							
							<dt>
								<table class="bloquer3 goshadow1">
									<tbody>						
										<tr class="pointer" onclick="muestraSucursal(${j},${listaSucursales[j].idCeco},${seleccionAno},${seleccionMes},${seleccionCanal},${seleccionPais});">
											<td>${listaSucursales[j].descCeco} <span>${listaSucursales[j].numSuc} suc</span></td>
							
												<c:if test="${listaChecklist!='[]'}">
													<td style="width: ${64/conteo}%;">
														<c:if test="${listaSucursales[j].total<(porcentajeG-5)}">
															<div class="radio rojo">
																<div class="radio1">${listaSucursales[j].total}%</div>
															</div>
														</c:if>
														
														<c:if test="${listaSucursales[j].total>=porcentajeG}">
															<div class="radio verde">
																<div class="radio1">${listaSucursales[j].total}%</div>
															</div>
														</c:if>
														
														<c:if test="${listaSucursales[j].total>=(porcentajeG-5) && listaSucursales[j].total<(porcentajeG)}">
															<div class="radio amarilloN" style="color:black;">
																<div class="radio2">${listaSucursales[j].total}%</div>
															</div>
														</c:if>
														
													</td>			
												</c:if>
																			
						</c:if>
					
						<c:if test="${(j%conteo)!=0}">
							<c:if test="${listaChecklist!='[]'}">							
								<td style="width: ${64/conteo}%;">						
									<c:if test="${listaSucursales[j].total<porcentajeG-5}">
										<div class="radio rojo">
											<div class="radio1">${listaSucursales[j].total}%</div>
										</div>
									</c:if>
									
									<c:if test="${listaSucursales[j].total>=porcentajeG}">
										<div class="radio verde">
											<div class="radio1">${listaSucursales[j].total}%</div>
										</div>
									</c:if>
									
									<c:if test="${listaSucursales[j].total>=(porcentajeG-5) && listaSucursales[j].total<(porcentajeG)}">
										<div class="radio amarilloN" style="color:black;">
											<div class="radio2">${listaSucursales[j].total}%</div>
										</div>
									</c:if>
								</td>
							</c:if>	
						</c:if>	
					</c:forEach>
					
								</tr>								
							</tbody>
						</table>
					</dt>
								
					<dd>
						<div class="inner1">
							<dl class="accordion" id="idLlenaSucursales${listaSucursalesSize-conteo}">
								
							</dl>
						</div>
					</dd>	
				
				</dl>
			</form>
			
		</div>
		<form id="formFiltro" method="POST" action="vistaFiltroCumplimientoVisitas.htm"></form>
		<div class="fspacer"></div>
	</div>
	

	<!-- jquery Start -->
	<script type="text/javascript"
		src="../js/vistaCumplimientoVisitas/jquery.js"></script>

	<!-- progressbar -->
	<script>
		function move() {
			var elem = document.getElementById("myBar");
			var width = 1;
			var id = setInterval(frame, 10);
			function frame() {
				if (width >= 100) {
					clearInterval(id);
				} else {
					width++;
					elem.style.width = width + '%';
				}
			}
		}
	</script>

	<!-- dropdown -->
	<script type="text/javascript"
		src="../js/vistaCumplimientoVisitas/menu.js"></script>
		
	<!-- dropdown -->
	<script type="text/javascript" src="../js/vistaCumplimientoVisitas/script-territCumplimientoVisitas.js"></script>

	<!-- accordion -->
	<script type="text/javascript">
		jQuery(function() {

			$(".subSeccion").hide();

			var allPanels = $('.accordion3 > dd').hide();

			jQuery('.accordion3 > dt').on('click', function() {
				$this = $(this);
				//the target panel content
				$target = $this.next();
				

				jQuery('.accordion3 > dt').removeClass('accordion3-active');
				if ($target.hasClass("in")) {
					$this.removeClass('accordion3-active');
					$target.slideUp();
					$target.removeClass("in");
					$(".subSeccion").hide();
					$(".avance").show();
					$(".goborder").show();
					document.getElementById("box").style.marginTop = "185px";
					
				} else {
					$this.addClass('accordion3-active');
					jQuery('.accordion3 > dd').removeClass("in");
					$target.addClass("in");
					$(".subSeccion").show();
					$(".avance").hide();
					$(".goborder").hide();
					
					jQuery('.accordion3 > dd').slideUp();
					$target.slideDown();
					document.getElementById("box").style.marginTop = "60px";

				}
			});

			var allPanels = $('.accordion > dd').hide();

			jQuery('.accordion > dt').on('click', function() {
				$this = $(this);
				//the target panel content
				$target = $this.next();

				jQuery('.accordion > dt').removeClass('accordion-active');
				if ($target.hasClass("in")) {
					$this.removeClass('accordion-active');
					$target.slideUp();
					$target.removeClass("in");

				} else {
					$this.addClass('accordion-active');
					jQuery('.accordion > dd').removeClass("in");
					$target.addClass("in");
					$(".subSeccion").show();

					jQuery('.accordion > dd').slideUp();
					$target.slideDown();
				}
			});

		});
	</script>

	<div class="modal"></div>
</body>
</html>