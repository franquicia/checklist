package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ReporteSistemasDTO;


public interface ReporteSistemasDAO {
	
	public List<ReporteSistemasDTO> ReporteChecklistSistemas(int idCeco) throws Exception;
	public List<ReporteSistemasDTO> ReporteCecosSistemas(int idCeco, int idChecklist,String fecha) throws Exception;

}
