package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ConsultaCecoProtocoloDTO;




public interface ConsultaCecoProtocoloDAO {
	
	public List<ConsultaCecoProtocoloDTO> obtieneConsultaCeco() throws Exception;
	public List<ConsultaCecoProtocoloDTO> obtieneConsultaBitacora(ConsultaCecoProtocoloDTO bean) throws Exception;
	public List<ConsultaCecoProtocoloDTO> obtieneConsultaChecklist(ConsultaCecoProtocoloDTO bean) throws Exception;
	public List<ConsultaCecoProtocoloDTO> obtieneDetalle(ConsultaCecoProtocoloDTO bean) throws Exception;


}
