package com.gruposalinas.checklist.business;


import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.checklist.dao.ReporteHallazgosDAO;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.ReporteHallazgosDTO;

import com.gruposalinas.checklist.util.UtilFRQ;

public class ReporteHallazgosBI extends Thread{
	
	private static Logger logger = LogManager.getLogger(ReporteHallazgosBI.class);
	
	private List<ReporteHallazgosDTO> listafila;

		
		@Autowired
		ReporteHallazgosDAO reporteHallazgosDAO;
			
		
		
		public String conteoGralHallazgos(String nego,String fechaInicio,String fechaFin){
		
		
				JSONObject respuesta = new JSONObject();

				JSONObject obj = new JSONObject();
				try {
					Map<String, Object> listaConteoHallazgos = null;
					listaConteoHallazgos = reporteHallazgosDAO.conteoGralHallazgos(nego,fechaInicio,fechaFin);

					@SuppressWarnings("unchecked")
					List<ReporteHallazgosDTO> listaConteo = (List<ReporteHallazgosDTO>) listaConteoHallazgos.get("listaConteo");

					//JSONArray conteos = new JSONArray();
					
					for(int i=0 ;i<listaConteo.size();i++) {
						obj.put("totalGral", listaConteo.get(i).getConteoTotal());
						obj.put("porAtender", listaConteo.get(i).getConteoPorAtender());
						obj.put("porAutorizar", listaConteo.get(i).getConteoPorAutorizar());
						obj.put("rechazados", listaConteo.get(i).getConteoRechazado());
						obj.put("atendidos", listaConteo.get(i).getConteoAtendido());
						obj.put("negocio", listaConteo.get(i).getNegocio());
						
						//conteos.put(obj);
						}
					
					//respuesta.append( "listaConteo",listaConteo);
					
					
				} catch (Exception e) {
					logger.info("No fue posible obtener los datos");
					UtilFRQ.printErrorLog(null, e);
				}

				return obj.toString();	
		}
		
		public List<ReporteHallazgosDTO> conteoCecoHallazgos(String ceco,String fechaInicio,String fechaFin){
			try {
		
				listafila =  reporteHallazgosDAO.conteoCecoHallazgos( ceco, fechaInicio,fechaFin);
			} catch (Exception e) {
				logger.info("No fue posible obtener los datos");
				e.printStackTrace();
				UtilFRQ.printErrorLog(null, e);	
			}
			
			return listafila;			
		}
		
		public List<ReporteHallazgosDTO> conteoFaseHallazgos(String ceco,String fechaInicio,String fechaFin){
			try {
		
				listafila =  reporteHallazgosDAO.conteoFaseHallazgos( ceco,fechaInicio,fechaFin);
			} catch (Exception e) {
				logger.info("No fue posible obtener los datos");
				e.printStackTrace();
				UtilFRQ.printErrorLog(null, e);	
			}
			
			return listafila;			
		}
		
		public List<ReporteHallazgosDTO> obtieneCecosFechas(String negocio, String fechaInicio, String fechaFin){
			try {
		
				listafila =  reporteHallazgosDAO.obtieneCecosFechas( negocio,fechaInicio,fechaFin);
			} catch (Exception e) {
				logger.info("No fue posible obtener los datos");
				e.printStackTrace();
				UtilFRQ.printErrorLog(null, e);	
			}
			
			return listafila;			
		}
		
		public JsonArray obtieneConteosCeco(String negocio, String fechaInicio, String fechaFin){
			
		
			JsonObject jsonObject = null;
			JsonArray jsonArray = null;		
					
			
			try {
				
				List<ReporteHallazgosDTO> cecos = obtieneCecosFechas(negocio, fechaInicio, fechaFin);
				
				if(cecos != null) {
					
					jsonArray = new JsonArray();
					
					for(ReporteHallazgosDTO ceco : cecos ) {
						
						List<ReporteHallazgosDTO> conteoCeco = conteoCecoHallazgos(ceco.getCeco(), fechaInicio, fechaFin);
						
						if(conteoCeco != null && conteoCeco.size() > 0) {
							
							jsonObject = new JsonObject();
							
							jsonObject.addProperty("ceco",ceco.getCeco());
							jsonObject.addProperty("nombreCeco",ceco.getNombreCeco());
							jsonObject.addProperty("totalHallazgos", conteoCeco.get(0).getConteoTotal());
							jsonObject.addProperty("atendidos", conteoCeco.get(0).getConteoAtendido());
							jsonObject.addProperty("porAtender", conteoCeco.get(0).getConteoPorAtender());
							jsonObject.addProperty("porAutorizar", conteoCeco.get(0).getConteoPorAutorizar());
							jsonObject.addProperty("rechazadosPorAtender", conteoCeco.get(0).getConteoRechazado());
							
							jsonArray.add(jsonObject);
						
						}
						
					}	
					
				}else {
					
					jsonArray = null ;
					
				}
				
				
				
			}catch(Exception e) {
				jsonArray = null;
			}
			
					
			return jsonArray;
			
			
		}
		
		public JsonArray obtieneConteoFases ( String ceco,String fechaInicio,String fechaFin){
			
			JsonObject jsonObject = null;
			JsonArray jsonArray = null;		
			
			try {
		
				List<ReporteHallazgosDTO> conteoFase =  reporteHallazgosDAO.conteoFaseHallazgos( ceco,fechaInicio,fechaFin);
				
				if(conteoFase != null) {
					
					jsonArray =new JsonArray();
					
					for(ReporteHallazgosDTO fase : conteoFase) {
						
						
						jsonObject = new JsonObject();
							
						jsonObject.addProperty("fase",fase.getNombreFase());
						jsonObject.addProperty("totalHallazgos", fase.getConteoTotal());
						jsonObject.addProperty("atendidos", fase.getConteoAtendido());
						jsonObject.addProperty("porAtender", fase.getConteoPorAtender());
						jsonObject.addProperty("porAutorizar", fase.getConteoPorAutorizar());
						jsonObject.addProperty("rechazadosPorAtender", fase.getConteoRechazado());
							
						jsonArray.add(jsonObject);
						
					
						
						
					}
					
				}
				
				
			} catch (Exception e) {
				logger.info("No fue posible obtener los datos");
				e.printStackTrace();
				
				jsonArray = null;
					
			}
			
			return jsonArray;			
		}
		
	
}
