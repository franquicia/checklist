<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="es">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />

<script src="../js/supervisionSistemas/script_reporteIncidentes.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/grafica.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/seccionesCaptacion.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/calendarioCaptacion.css"
	media="screen">

<style type="text/css">
#tags {
	width: 100%;
	border: none;
	border-bottom: 1px solid #d7d7d7;
}
</style>

<title>Sistemas Supervisi�n</title>
</head>

<body>

	<div class="page">

		<div class="clear"></div>

		<div class="title" id="title">
			<div class="wrapper">
				<div class="h2">
					<a href="/checklist/central/vistaReporteCaptacion.htm">P�gina Principal </a> / <a>Sistemas</a>
				</div>
				<h1>Reporte de Incidencias</h1>
			</div>
		</div>

		<div class="clear"></div>



		<!-- Contenido -->
		<div class="contHome">
			<div class="titN" style="width: 95%; margin: auto;">
				<table class="tblHeader">
					<tbody>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				</table>

					<table class="tblBusquedas">
						<tbody>
							<tr>
								<td>Per�odo:</td>
								<td><input type="text" placeholder="--/--/--"
									class="datapicker1 date" id="datepicker5"></td>

								<td><input type="text" placeholder="--/--/--"
									class="datapicker1 date" id="datepicker6"></td>

								<td><select class="normal_select" id="selEstatus">
										<option value="">Elige Estatus</option>
										<option value="6000">Abierto</option>
										<option value="1000">Cerrado</option>
										<option value="9000">Desarrollo</option>
										<option value="7000">Reachazado</option>
										<option value="19000">Confirmar</option>
										<option value="13000">En L�nea</option>
										<option value="8000">Revisar</option>
										<option value="0000">En Proceso</option>
										<option value="2000">Recurrencia</option>
										<option value="15000">En Validaci�n</option>
										<option value="5000">En Atenci�n</option>
										<option value="20000">Cancelado</option>
										<option value="20001">Revisado</option>
								</select></td>

								<td rowspan="2"><input type="button" class="btnbuscarG" onclick="return busqueda(this)" /></td>

							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>Desde</td>
								<td>Hasta</td>
								<td>Estatus</td>
							</tr>
						</tbody>
					</table>


			</div>

			<div class="clear"></div>

			<br>

			<div class="home"
				style="margin-top: 40px; width: 80%; display: none;">

				<table>
					<tbody>
						<tr>
							<td style="font-size: 16px; font-weight: 700; color: #006341">Detalle
								de reporte de incidencia</td>
						</tr>
					</tbody>
				</table>

				<div class="pagedemo _current"
					style="display: block; margin-top: 20px">

					<table class="tblGeneral" id="busqueda">
						<tbody>
							<tr>
								<th>Folio</th>
								<th>Fecha</th>
								<th>Checklist</th>
								<th>M�dulo</th>
								<th>Estatus</th>
								<th>Asignar</th>
							</tr>
						</tbody>
					</table>
				</div>


				<div>
					<div class="pagedemo _current" style="display: block;">

						<table class="tblGeneral" id="busqueda">
							<tbody>
								<c:if test="${0 == 0}">
									<c:forEach var="i" begin="0" end="${0}">
										<tr style='cursor: pointer;'>
											<td>123456789</td>
											<td>15/02/2018</td>
											<td>Caja</td>
											<td>Modulo 1</td>
											<td>En Desarrollo</td>
											<td>189870 - J Jorge</td>
									</c:forEach>
								</c:if>
							</tbody>
						</table>
					</div>

				</div>


				<div style="margin-top: 30px;">

					<table class="tblDetalle">
						<tbody>
							<tr>
								<td width="30%"><img
									style="width: 100%; height: 100%; max-width: 400px; max-height: 400px"
									src="../images/android.png"></td>
								<td width="5%" style="background: white;"></td>
								<td rowspan="2" width="60%" height="100%;">
									<table style="width: 100%; height: 100%;">
										<tr style="height: 10%;">
											<td width="100%;"
												style="background: white; padding-bottom: 5px; padding-left: 10px;">
												<a
												style="font-size: 16px; font-weight: 700; color: #006341;">Bitacora</a>
											</td>
										</tr>
										<tr style="height: 40%;">
											<td>
												<div
													style="height: 70px; overflow: scroll; text-align: justify; padding-left: 25px; padding-right: 25px; padding-top: 15px; margin-bottom: 5px;">
													<a>Hay muchas variaciones de los pasajes de Lorem Ipsum
														disponibles, pero la mayor�a sufri� alteraciones en alguna
														manera, ya sea porque se le agreg� humor, o palabras
														aleatorias que no parecen ni un poco cre�bles. Si vas a
														utilizar un pasaje de Lorem Ipsum, necesit�s estar seguro
														de que no hay nada avergonzante escondido en el medio del
														texto. Todos los generadores de Lorem Ipsum que se
														encuentran en Internet tienden a repetir trozos
														predefinidos cuando sea necesario, haciendo a este el
														�nico generador verdadero (v�lido) en la Internet. Usa un
														diccionario de mas de 200 palabras provenientes del lat�n,
														combinadas con estructuras muy �tiles de sentencias, para
														generar texto de Lorem Ipsum que parezca razonable. Este
														Lorem Ipsum generado siempre estar� libre de repeticiones,
														humor agregado o palabras no caracter�sticas del lenguaje,
														etc.</a>
												</div>
											</td>
										</tr>
										<tr style="height: 10%;">
											<td width="100%;"
												style="background: white; padding-bottom: 5px; padding-top: 20px; padding-left: 10px;">
												<a
												style="font-size: 16px; font-weight: 700; color: #006341;">Historial
													de Folio</a>
											</td>
										</tr>
										<tr style="height: 40%;">
											<td>
												<div
													style="height: 100px; overflow: scroll; text-align: justify; padding-left: 25px; padding-right: 25px; padding-top: 15px; margin-bottom: 5px;">
													<a>Al contrario del pensamiento popular, el texto de
														Lorem Ipsum no es simplemente texto aleatorio. Tiene sus
														raices en una pieza cl�sica de la literatura del Latin,
														que data del a�o 45 antes de Cristo, haciendo que este
														adquiera mas de 2000 a�os de antiguedad. Richard
														McClintock, un profesor de Latin de la Universidad de
														Hampden-Sydney en Virginia, encontr� una de las palabras
														m�s oscuras de la lengua del lat�n, "consecteur", en un
														pasaje de Lorem Ipsum, y al seguir leyendo distintos
														textos del lat�n, descubri� la fuente indudable. Lorem
														Ipsum viene de las secciones 1.10.32 y 1.10.33 de "de
														Finnibus Bonorum et Malorum" (Los Extremos del Bien y El
														Mal) por Cicero, escrito en el a�o 45 antes de Cristo.
														Este libro es un tratado de teor�a de �ticas, muy popular
														durante el Renacimiento. La primera linea del Lorem Ipsum,
														"Lorem ipsum dolor sit amet..", viene de una linea en la
														secci�n 1.10.32 <br>El trozo de texto est�ndar de
														Lorem Ipsum usado desde el a�o 1500 es reproducido debajo
														para aquellos interesados. Las secciones 1.10.32 y 1.10.33
														de "de Finibus Bonorum et Malorum" por Cicero son tambi�n
														reproducidas en su forma original exacta, acompa�adas por
														versiones en Ingl�s de la traducci�n realizada en 1914 por
														H. Rackham.
													</a>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>

							<tr>
								<td width="30%">

									<table width="100%">
										<tr>
											<td width="50%;" style="text-align: left;">Patio
												Bancario <br> Primer Set
											</td>
											<td width="50%;" style="text-align: right:;">19/02/2018
												<br> 15:07:20
											</td>
										</tr>
									</table>
								</td>
								<td width="5%"></td>
							</tr>
						</tbody>
					</table>
				</div>

				<table style="margin: auto; margin-top: 4%; margin-bottom: 3%">
					<tbody>
						<tr>
							<td rowspan="2"><input value="Guardar" class="btnRegistrar"
								type="button"></td>
						</tr>
					</tbody>
				</table>


			</div>



			<div class="clear"></div>



			<div class="home" style="margin-top: 40px; width: 80%">

				<table>
					<tbody>
						<tr>
							<td style="font-size: 16px; font-weight: 700; color: #006341">
								Resultado de busqueda:
								<a style="color: #666666;" id="tituloFiltro">Motivos de Estatus</a>
							</td>
						</tr>
					</tbody>
				</table>


				<div style="margin-top: 30px;">

					<table class="tblDetalle">
						<tbody>
							<tr>
								<td rowspan="2" width="60%" height="100%;" style="background: none;">

									<table style="width: 100%; height: 100%;" id="listaSis">
										
									</table>

								</td>
								<td width="3%" style="background: white;"></td>
								<td width="30%" style="background: white;">

									<div class="grafica" id="grafica" style="margin: auto;">
									
									</div>
									
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="pagedemo _current"
					style="display: block; margin-top: 50px">

					<table class="tblGeneral" id="tituloTabla">
						<tbody>
							<tr>
								<th style="width:13%;">Estatus</th>
								<th style="width:20%;">Fecha Apertura</th>
								<th style="width:17%;">Folio</th>
								<th style="width:15%;">Mensaje Error</th>
								<th style="width:15%;">Motivo Estado</th>
								<th style="width:20%;">Responsable</th>
							</tr>
						</tbody>
					</table>
				</div>

				<div>
					<div class="pagedemo _current" style="display: block; overflow: scroll; height: 300px;">
						<table class="tblGeneral" id="cuerpoTabla">
	
						</table>
					</div>
				</div>
			</div>

			<div class="clear"></div>

		</div>

		<div class="clear"></div>

		<div class="footer1">
			<table class="tblFooter1">
				<tbody>
					<tr>
						<td>Sistema Reportes</td>
						<td><a class="btnRojo">Malas Pr�cticas</a></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="footer">
			<table class="tblFooter">
				<tbody>
					<tr>
						<td>Banco Azteca S.A. Instituci�n de Banca M�ltiple</td>
						<td>Derechos Reservados 2014 (T�rminos y Condiciones de uso
							del Portal).</td>
					</tr>
				</tbody>
			</table>
		</div>

	</div>
</body>
<script type="text/javascript">

    	document.getElementById("datepicker5").value="";
    	document.getElementById("datepicker6").value="";
    	document.getElementById("selEstatus").value="";

    	$.datepicker.setDefaults($.datepicker.regional['es']);
    	$(function() {

    		$( "#datepicker5" ).datepicker({
    			dateFormat: "yy/mm/dd",
    			minDate: "2017/01/01",    
    			maxDate: new Date()
    		});
    		
    		$( "#datepicker6" ).datepicker({
    			dateFormat: "yy/mm/dd",
    	        maxDate: new Date()
    	    });

  		$("#datepicker5").change(function(){    
  			document.getElementById("datepicker6").value="";
  			$( "#datepicker6" ).datepicker("option", "minDate", ""+$('#datepicker5').val());
	    });
    		
    	});
    	
</script>
<div id="tipoModal" class="modal"></div>

</html>

