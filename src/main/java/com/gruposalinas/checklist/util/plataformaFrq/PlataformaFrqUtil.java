/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.util.plataformaFrq;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
//import org.codehaus.jackson.annotate.JsonCreator;
//import org.codehaus.jackson.annotate.JsonProperty;
//import org.codehaus.jackson.annotate.JsonSetter;
import org.codehaus.jackson.map.ObjectMapper;
//import org.codehaus.jackson.map.annotate.JsonDeserialize;

/**
 *
 * @author leodan1991
 */
public class PlataformaFrqUtil {

    private static org.apache.logging.log4j.Logger logger = LogManager.getLogger(PlataformaFrqUtil.class);

    private final String PROTOCOL = "http://";
    private final String IP_PLATAFORMA = "10.53.33.83";
    private final String PORT_PLATAFORMA = "82";
    //private final String IP_PLATAFORMA = "10.51.218.130";
    //private final String PORT_PLATAFORMA = "8080";

    private final String MAIN_CONTEXT = "/plataforma-franquicia-core/service";
    private final String CONTEXT_TOKEN = "/security/oauth/token";
    private final String URI_BASE = PROTOCOL + IP_PLATAFORMA + ":" + PORT_PLATAFORMA;
    private final String FULL_PATH_TOKEN = PROTOCOL + IP_PLATAFORMA + ":" + PORT_PLATAFORMA + MAIN_CONTEXT + CONTEXT_TOKEN;

    /**
     * @param args the command line arguments
     */
    /*public static void main(String[] args) {
        try {

            PlataformaFrqUtil tokenTool = new PlataformaFrqUtil();
            TokenPlataforma token = tokenTool.getTokenPlataforma("196228");
            tokenTool.getProtocolosByCeco(token.getAccessToken(), "236899");
            //token.testRequest();
        } catch (Exception ex) {
            LogManager.getLogger(PlataformaFrqUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/
    public TokenPlataforma getTokenPlataforma2(String idUsuario) {
        TokenPlataforma token = null;

        try {
            DefaultHttpClient client = new DefaultHttpClient();

            HttpPost post = new HttpPost(FULL_PATH_TOKEN);

            System.out.println("URL TOKEN: " + FULL_PATH_TOKEN);

            List<NameValuePair> formDataBody = new ArrayList<>();
            formDataBody.add(new BasicNameValuePair("grant_type", "password"));
            formDataBody.add(new BasicNameValuePair("username", idUsuario));
            formDataBody.add(new BasicNameValuePair("password", idUsuario));

            String body = "grant_type: password\n"
                    + "username: " + idUsuario + "\n"
                    + "password: " + idUsuario + "\n";
            for (NameValuePair p : formDataBody) {
                System.out.println("PARAM: " + p);
            }

            StringEntity stringEntity = new StringEntity(body);

            post.setEntity(new UrlEncodedFormEntity(formDataBody, "UTF-8"));
            //post.setEntity(stringEntity);

            post.setHeader("Content-Type", "text/plain");

            //auth type
            String encoding = Base64.getEncoder().encodeToString("frontendapp:12345".getBytes("UTF-8"));
            post.setHeader("Authorization", "Basic " + encoding);

            HttpResponse response = client.execute(post);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            System.out.println(result.toString());

        } catch (MalformedURLException ex) {
            logger.error("Error", ex);
        } catch (IOException ioEx) {
            ioEx.printStackTrace();

        }

        return token;
    }

    public TokenPlataforma getTokenPlataforma(String idUsuario) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/plain");
        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("username", idUsuario)
                .addFormDataPart("password", idUsuario)
                .addFormDataPart("grant_type", "password")
                .build();

        String encodingAuth = Base64.getEncoder().encodeToString("frontendapp:12345".getBytes("UTF-8"));

        Request request = new Request.Builder().url(URI_BASE + "/plataforma-franquicia-core/service/security/oauth/token").method("POST", body).addHeader("Authorization", "Basic " + encodingAuth).build();

        //Request request = new Request.Builder().url("http://10.53.33.83:82"+"/plataforma-franquicia-core/service/security/oauth/token").method("POST", body).addHeader("Authorization", "Basic " + encodingAuth).build();
        Response response = client.newCall(request).execute();
        String jsonRequest = response.body().string();
        //System.out.println("BODY: " + jsonRequest);

        ObjectMapper mapper = new ObjectMapper();

        TokenPlataforma token = mapper
                .readValue(jsonRequest, TokenPlataforma.class);
        System.out.println("TOKEN OBTENIDO: " + token);

        return token;
    }

    public ArrayList<Integer> getProtocolosByCeco(String accessToken, String ceco) {

        ArrayList<Integer> protocolos = new ArrayList<>();
        try {

            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            Request request = new Request.Builder()
                    .url(URI_BASE + "/plataforma-franquicia-core/service/franquicia/punto/contacto/protocolo/secure/get/" + ceco)
                    .method("GET", null)
                    .addHeader("Authorization", "Bearer " + accessToken)
                    .build();
            Response response = client.newCall(request).execute();
            String jsonData = response.body().string();
            ObjectMapper mapper = new ObjectMapper();

            ResponsePlataformaDTO<ProtocoloPlataformaDTO> ref = new ResponsePlataformaDTO<>();

            ResponsePlataformaDTO dataRequest = mapper
                    .readValue(jsonData, ref.getClass());

            //System.out.println("DATA : " + dataRequest);
            ArrayList<Object> protocoloRequest = (ArrayList<Object>) dataRequest.getData();
            for (Object o : protocoloRequest) {

                LinkedHashMap<String, Integer> hashmap = (LinkedHashMap<String, Integer>) o;
                for (Entry<String, Integer> entry : hashmap.entrySet()) {
                    System.out.println("E: " + entry.getValue());
                    protocolos.add(entry.getValue());

                }

                //System.out.println("O: " + o.toString());
                //System.out.println("O: " + o.getClass().getSimpleName());

                /*ProtocoloPlataformaDTO p= mapper
                .readValue(o.toString(), ProtocoloPlataformaDTO.class);
            System.out.println("P: "+p.toString());*/
            }
        } catch (Exception e) {

        }

        return protocolos;
    }

}
