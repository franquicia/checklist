<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
	<tiles:importAttribute name="javascripts"/>
	<head>
		<title><tiles:insertAttribute name="title" ignore="true"/></title>
		<c:forEach var="script" items="${javascripts}">
	        <script src="<c:url value="${script}"/>"></script>
	    </c:forEach>
		<link rel="stylesheet" type="text/css" href="../css/carousel.css">
		<!-- <link rel="stylesheet" type="text/css" href="css/demo.css"> -->
		<!-- <script type="text/javascript" src="js/jquery.js"></script> -->
		
	</head>
    <body>
    	<div id="cabecero">
			<tiles:insertAttribute name="header" />
		</div>
		<div></div>
		<div id="cuerpo">
			<tiles:insertAttribute name="body" />
		</div>
		<div></div>
	</body>
</html>