package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ParametroDTO;

public interface ParametroDAO {

	public List<ParametroDTO> obtieneParametro() throws Exception;
	
	public List<ParametroDTO> obtieneParametro(String idParametro) throws Exception;
	
	public int getParametro(ParametroDTO bean) throws Exception;

	public boolean insertaParametro(ParametroDTO bean) throws Exception;
	
	public boolean actualizaParametro(ParametroDTO bean) throws Exception;
	
	public boolean eliminaParametros(String clave) throws Exception;
}
