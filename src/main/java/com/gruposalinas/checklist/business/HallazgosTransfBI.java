package com.gruposalinas.checklist.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.ChecklistHallazgosRepoDAO;
import com.gruposalinas.checklist.dao.HallazgosTransfDAO;
import com.gruposalinas.checklist.domain.HallazgosTransfDTO;


public class HallazgosTransfBI {

	private static Logger logger = LogManager.getLogger(HallazgosTransfBI.class);

private List<HallazgosTransfDTO> listafila;
	
	@Autowired
	HallazgosTransfDAO hallazgosTransfDAO;
	

			
	
	public int inserta(HallazgosTransfDTO bean){
		int respuesta = 0;
		
		try {
			int respuesta2 = hallazgosTransfDAO.inserta(bean);
			logger.info(respuesta2);
			respuesta = respuesta2;
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	//elimina por bitacora gral
	public boolean eliminaBita(String bitGral){
		boolean respuesta = false;
		
		try {
			respuesta = hallazgosTransfDAO.eliminaBita(bitGral);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	
	public List<HallazgosTransfDTO> obtieneDatos(String ceco,String proyecto,String fase) {
		
		try {
			listafila =  hallazgosTransfDAO.obtieneDatos(ceco,proyecto,fase);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
public List<HallazgosTransfDTO> obtieneDatosRepo(String ceco,String proyecto,String fase) {
		
		try {
			listafila =  hallazgosTransfDAO.obtieneDatosRepo(ceco,proyecto,fase);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	//historico hallazgos
	public List<HallazgosTransfDTO> obtieneHistBita( String bita) {
		
		try {
			listafila =  hallazgosTransfDAO.obtieneHistBita (bita);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	//IMAGENES 
		public List<HallazgosTransfDTO> obtieneDatosBita(int proyecto, int fase,int ceco) {
			
			try {
				listafila =  hallazgosTransfDAO.obtieneDatosBita (proyecto,fase,ceco);
			} catch (Exception e) {
				logger.info("No fue posible obtener los datos");
				UtilFRQ.printErrorLog(null, e);	
			}
			
			return listafila;			
		}
	//web
	
	public boolean actualiza(HallazgosTransfDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = hallazgosTransfDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	//movil
	public Map<String,Object> actualizaMov(HallazgosTransfDTO bean){
		Map<String,Object> respuesta=null;
		
		try {
			respuesta = hallazgosTransfDAO.actualizaMov(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public boolean actualizaResp(HallazgosTransfDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = hallazgosTransfDAO.actualizaResp(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	

		//carga hallazgos por bitacora
		public boolean cargaHallazgosxBita(int bitGral,int cargaIni){
			boolean respuesta = false;
			
			try {
				respuesta = hallazgosTransfDAO.cargaHallazgosxBita( bitGral,cargaIni);
			} catch (Exception e) {
				logger.info("No fue posible actualizar");
				UtilFRQ.printErrorLog(null, e);	
			}
			
			return respuesta;			
		}
		
	
	
	
		
	//Actualiza Papá en Automático Cuando cierro el último hijo
		public boolean actualizaHallazgo(HallazgosTransfDTO hallazgo) {
			boolean respuesta = false;
			
			try {
				
				boolean actualizaResponse = hallazgosTransfDAO.actualizaResp(hallazgo);
				
				if (actualizaResponse) {
					respuesta = true;
				} else {
					respuesta = false;
				}
				
			} catch (Exception e) {
				logger.info("AP en actualizaHallazgo() HallazgosExpBI");
				respuesta = false;
			}
			
			return respuesta;
		}
		
		//Permite Cambiar de status 3 a 1 (SI a NO)
		public boolean actualizaHallazgoSiANo(HallazgosTransfDTO hallazgo) {
			boolean respuesta = false;
			
			try {
				
				boolean actualizaResponse = hallazgosTransfDAO.actualiza(hallazgo);	
				
				if (actualizaResponse) {
					respuesta = true;
				} else {
					respuesta = false;
				}
				
			} catch (Exception e) {
				logger.info("AP en actualizaHallazgo() HallazgosExpBI");
				respuesta = false;
			}
			
			return respuesta;
		}
		
		public boolean insertaHallazgo(HallazgosTransfDTO hallazgo) {
			boolean respuesta = false;
			
			try {

				int actualizaResponse = hallazgosTransfDAO.inserta(hallazgo);				
				
				if (actualizaResponse !=  0) {
					respuesta = true;
					logger.info("");
				} else {
					respuesta = false;
				}
				
			} catch (Exception e) {
				logger.info("AP en inserta Hallazgo() HallazgosExpBI");
				respuesta = false;
			}
			
			return respuesta;
		}
	//FIN Nuevo Proceso para la carga de hallazgos desde la primer caminata

//hallasgos historico
	// elimina por bitacora gral
	public boolean eliminaBitaHisto(String bitGral) {
		boolean respuesta = false;

		try {
			respuesta = hallazgosTransfDAO.eliminaBitaHisto(bitGral);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta;
	}

	// INSERTA
	public boolean insertaHallazgosHist(int idHallazgo) {
		boolean respuesta = false;

		try {
			respuesta = hallazgosTransfDAO.insertaHallazgosHist(idHallazgo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta;
	}

	// carga historial
	public boolean cargaHallazgosHistorico() {
		boolean respuesta = false;

		try {
			respuesta = hallazgosTransfDAO.cargaHallazgosHistorico();
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta;
	}

	public List<HallazgosTransfDTO> obtieneHistFolio(int idFolio) {

		try {
			listafila = hallazgosTransfDAO.obtieneHistFolio(idFolio);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}
	
	public String generaArchivoTrans(String idCeco, String idProyecto,String idFase) {
		
		
		List<HallazgosTransfDTO> listaHallazgos =  obtieneDatos(idCeco, idProyecto, idFase);
		
		String htmlGenerado = generaHTMLDetalleTransforma("", listaHallazgos, "",  "" , "");
		
		
		return htmlGenerado;
	}

	 private String generaHTMLDetalleTransforma(String clasif,List<HallazgosTransfDTO> hallazagos,String logo,String clasifImg,String calificacion) {

			
			String zonaHtml = "<html><body>";
			zonaHtml += "<div style='position: fixed; top: 10px; right: 30px; left: 30px; bottom: 10px; background: white; border-radius: 30px; border-style: solid;  border-color: gray; border-width: 1px;  padding: 20px;  text-align: right; overflow-y: scroll;'>";
			zonaHtml += "<div style = 'background-color: #ECEBEB; width:100%;'><img src='http://10.53.33.82/franquicia/firmaChecklist/LogoElektra.png' height='30px' style='float: right;'></img>&nbsp;&nbsp;<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoBAZ.png' height='30px' style='float: right;'></img></div> <br></br> <br></br> <br></br>";
			zonaHtml += "<div><table><tr><td><img src='http://10.53.33.82/franquicia/firmaChecklist/"+clasifImg+"' height='50' align='left'/></td><td> <p style='text-align: left; padding-top: 10px'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+clasif+"</b></p></td><td><p style='text-align: center; padding-top: 10px; '>Calififcacion:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
					 + calificacion
					 + "</p></td><td><img src='http://10.53.33.82/franquicia/firmaChecklist/"+logo+"' width='50'height='25' align='rigth' style='padding-left: 30px;padding-top: 10px;'/></td></tr></table></div>";
			zonaHtml += "<div style='text-align: left; background-color: #ECEBEB;padding-top: .5px;padding-bottom: .5px;' ><table style=' width: 100%;  '> <tr><td><p > <b>&nbsp;&nbsp;Detalles de Calificacion</b>  | Entrega de Sucursal</p></td><td><p></p></td></tr></table> </div> <br>  </br>";

			zonaHtml += "<div style='text-align: left;'>";
			
			if(hallazagos != null && hallazagos.size()>0) {
				zonaHtml += "<table style='width: 100%'><tr><th>Item</th><th>Respuesta</th><th>Foto</th></tr>";
				for (int i = 0; i < hallazagos.size(); i++) {
		
					zonaHtml += "<tr>";
					zonaHtml += "<td style='font-size: 13; width:60%;'>" + (i + 1) + ".-" + hallazagos.get(i).getPreg() + "<br></br>" + "Comenatrio Adicional: "+ ((hallazagos.get(i).getObs() != null) ? hallazagos.get(i).getObs() : " ")
							+ "</td>";
					zonaHtml += "<td style='font-size: 13'>" + hallazagos.get(i).getRespuesta() + "</td>";
					//zonaHtml += "<td ><img src='" + preguntas.get(i).getRuta() + "' width='60'height='60'/></td>";
					zonaHtml += "<td ><div  width='150'height='150'><img src='http://10.51.218.104" + hallazagos.get(i).getRuta() + "' width='150'height='150'/></div></td>";
					zonaHtml += "</tr>";
		
				}
				zonaHtml += "</table>";
			} else {
				zonaHtml += "<table style='width: 100%'><tr><th>Item</th><th>Respuesta</th><th>Foto</th></tr>";
				zonaHtml += "<tr><td></td><td></td><td></td></tr>";
				zonaHtml += "</table>";
			}

			zonaHtml += "  </div></div>";
			zonaHtml += "</body></html>";
			logger.info(zonaHtml);
			return zonaHtml;
		}
		
	 public List<HallazgosTransfDTO> obtieneHistoricoHallazgoDash(String ceco,String proyecto,String fase) {
			
			try {
				listafila =  hallazgosTransfDAO.obtieneHistoricoHallazgoDash(ceco,proyecto,fase);
			} catch (Exception e) {
				logger.info("No fue posible obtener los datos");
				UtilFRQ.printErrorLog(null, e);	
			}
			
			return listafila;			
		}
	 
	 public List<HallazgosTransfDTO> obtieneHallazgosExpansion(String ceco,String proyecto,String fase) {
			
			try {
				listafila =  hallazgosTransfDAO.obtieneHallazgosExpansion(ceco,proyecto,fase);
			} catch (Exception e) {
				logger.info("No fue posible obtener los datos");
				UtilFRQ.printErrorLog(null, e);	
			}
			
			return listafila;			
		}
	 public List<HallazgosTransfDTO> obtieneHallazgosExpansionIni(String ceco,String proyecto,String fase) {
			
			try {
				listafila =  hallazgosTransfDAO.obtieneHallazgosExpansionIni(ceco,proyecto,fase);
			} catch (Exception e) {
				logger.info("No fue posible obtener los datos");
				UtilFRQ.printErrorLog(null, e);	
			}
			
			return listafila;			
		}
	 
	//carga hallazgos por bitacora nuevo flujo
			public boolean cargaHallazgosxBitaNvo(int bitGral, int ceco,int fase, int proyecto){
				boolean respuesta = false;
				
				try {
					respuesta = hallazgosTransfDAO.cargaHallazgosxBitaNvo( bitGral,ceco,fase,proyecto);
				} catch (Exception e) {
					logger.info("No fue posible actualizar");
					UtilFRQ.printErrorLog(null, e);	
				}
				
				return respuesta;
				}
			
			public boolean actualizaAdicionalHalla(HallazgosTransfDTO bean){
				boolean respuesta = false;
				
				try {
					respuesta = hallazgosTransfDAO.actualizaAdicionalHalla(bean);
				} catch (Exception e) {
					logger.info("No fue posible actualizar");
					UtilFRQ.printErrorLog(null, e);	
				}
				
				return respuesta;			
			}
}