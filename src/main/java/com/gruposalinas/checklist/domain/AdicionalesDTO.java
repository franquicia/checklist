package com.gruposalinas.checklist.domain;

public class AdicionalesDTO {
	


		private int bitGral;
		private int recorrido;
		private String ceco;
		private String nomCeco;
		private int idRespuesta;
		private int idPreg;
		private int idChecklist;
		private String posible;
		private String ruta;
		private String obs;
		private String pregunta;
		private int critica;
		private String clasifica;
		private int pregPadre;
		private int status;
		private String usuModif;
		private String usu;
		private String fechaFin;
		private String fechaIni;
		private String fechaAuto;
		private String aux;
		private int aux2;
		private String periodo;
		private int eco;
		private String liderObra;
		private String usuReco;
		private String nomCheck;

		
		public int getBitGral() {
			return bitGral;
		}
		public void setBitGral(int bitGral) {
			this.bitGral = bitGral;
		}
		public int getRecorrido() {
			return recorrido;
		}
		public void setRecorrido(int recorrido) {
			this.recorrido = recorrido;
		}
		public String getCeco() {
			return ceco;
		}
		public void setCeco(String ceco) {
			this.ceco = ceco;
		}
		public String getNomCeco() {
			return nomCeco;
		}
		public void setNomCeco(String nomCeco) {
			this.nomCeco = nomCeco;
		}
		public int getIdRespuesta() {
			return idRespuesta;
		}
		public void setIdRespuesta(int idRespuesta) {
			this.idRespuesta = idRespuesta;
		}
		public int getIdPreg() {
			return idPreg;
		}
		public void setIdPreg(int idPreg) {
			this.idPreg = idPreg;
		}
		public int getIdChecklist() {
			return idChecklist;
		}
		public void setIdChecklist(int idChecklist) {
			this.idChecklist = idChecklist;
		}
		public String getPosible() {
			return posible;
		}
		public void setPosible(String posible) {
			this.posible = posible;
		}
		public String getRuta() {
			return ruta;
		}
		public void setRuta(String ruta) {
			this.ruta = ruta;
		}
		public String getObs() {
			return obs;
		}
		public void setObs(String obs) {
			this.obs = obs;
		}
		public String getPregunta() {
			return pregunta;
		}
		public void setPregunta(String pregunta) {
			this.pregunta = pregunta;
		}
		public int getCritica() {
			return critica;
		}
		public void setCritica(int critica) {
			this.critica = critica;
		}
		public String getClasifica() {
			return clasifica;
		}
		public void setClasifica(String clasifica) {
			this.clasifica = clasifica;
		}
		public int getPregPadre() {
			return pregPadre;
		}
		public void setPregPadre(int pregPadre) {
			this.pregPadre = pregPadre;
		}
		public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		public String getUsuModif() {
			return usuModif;
		}
		public void setUsuModif(String usuModif) {
			this.usuModif = usuModif;
		}
		public String getUsu() {
			return usu;
		}
		public void setUsu(String usu) {
			this.usu = usu;
		}
		public String getFechaFin() {
			return fechaFin;
		}
		public void setFechaFin(String fechaFin) {
			this.fechaFin = fechaFin;
		}
		public String getFechaIni() {
			return fechaIni;
		}
		public void setFechaIni(String fechaIni) {
			this.fechaIni = fechaIni;
		}
		public String getFechaAuto() {
			return fechaAuto;
		}
		public void setFechaAuto(String fechaAuto) {
			this.fechaAuto = fechaAuto;
		}
		public String getAux() {
			return aux;
		}
		public void setAux(String aux) {
			this.aux = aux;
		}
		public int getAux2() {
			return aux2;
		}
		public void setAux2(int aux2) {
			this.aux2 = aux2;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		
		public int getEco() {
			return eco;
		}
		public void setEco(int eco) {
			this.eco = eco;
		}
		public String getLiderObra() {
			return liderObra;
		}
		public void setLiderObra(String liderObra) {
			this.liderObra = liderObra;
		}
		public String getUsuReco() {
			return usuReco;
		}
		public void setUsuReco(String usuReco) {
			this.usuReco = usuReco;
		}
		
		public String getNomCheck() {
			return nomCheck;
		}
		public void setNomCheck(String nomCheck) {
			this.nomCheck = nomCheck;
		}
		@Override
		public String toString() {
			return "AdicionalesDTO [bitGral=" + bitGral + ", recorrido=" + recorrido + ", ceco=" + ceco + ", nomCeco="
					+ nomCeco + ", idRespuesta=" + idRespuesta + ", idPreg=" + idPreg + ", idChecklist=" + idChecklist
					+ ", posible=" + posible + ", ruta=" + ruta + ", obs=" + obs + ", pregunta=" + pregunta
					+ ", critica=" + critica + ", clasifica=" + clasifica + ", pregPadre=" + pregPadre + ", status="
					+ status + ", usuModif=" + usuModif + ", usu=" + usu + ", fechaFin=" + fechaFin + ", fechaIni="
					+ fechaIni + ", fechaAuto=" + fechaAuto + ", aux=" + aux + ", aux2=" + aux2 + ", periodo=" + periodo
					+ ", eco=" + eco + ", liderObra=" + liderObra + ", usuReco=" + usuReco + ", nomCheck=" + nomCheck
					+ "]";
		}
		
		
		  
	}