package com.gruposalinas.checklist.domain;

public class AltaSucursalDTO {
	
	private int idSucursal;
	private int idPais;
	private int idCanal;
	private String nomPais;
	private String nomCanal;
	private String idCeco;
	private String nuSucursal;	
	private String nombresuc;
	private double latitud;
	private double longitud;
	private boolean checkGCC;
	
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	
	public String getnomPais() {
		return nomPais;
	}
	public void setnomPais(String nomPais) {
		this.nomPais = nomPais;
	}
	public String getnomCanal() {
		return nomCanal;
	}
	public void setnomCanal(String nomCanal) {
		this.nomCanal = nomCanal;
	}
	
	public int getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(int idSucursal) {
		this.idSucursal = idSucursal;
	}
	public int getIdPais() {
		return idPais;
	}
	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}
	public int getIdCanal() {
		return idCanal;
	}
	public void setIdCanal(int idCanal) {
		this.idCanal = idCanal;
	}
	public String getNombresuc() {
		return nombresuc;
	}
	public void setNombresuc(String nombresuc) {
		this.nombresuc = nombresuc;
	}
	public String getNuSucursal() {
		return nuSucursal;
	}
	public void setNuSucursal(String nuSucursal) {
		this.nuSucursal = nuSucursal;
	}
	public double getLatitud() {
		return latitud;
	}
	public void  setLatitud(double latitud) {
		this.latitud = latitud;
	}
	public double getLongitud() {
		return longitud;
	}
	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}
	public boolean getcheckGCC() {
		return checkGCC;
	}
	public void setcheckGCC(boolean checkGCC) {
		this.checkGCC = checkGCC;
	}

}
