package com.gruposalinas.checklist.domain;

public class ConsultaCecoCheckDTO2 {
	
	//consulta  el  cursor CU_CECOS


		private int idUsuario;
		private int ceco;
		private int checklist;
		private int totalceco;
		//private  int totalcheckl;
		
	
		public int getIdUsuario() {
			return idUsuario;
		}
		public void setIdUsuario(int idUsuario) {
			this.idUsuario = idUsuario;
		}
		
		
		public int getCeco() {
			return ceco;
		}
		public void setCeco(int ceco) {
			this.ceco = ceco;
		}
		public int getChecklist() {
			return checklist;
		}
		public void setChecklist(int checklist) {
			this.checklist = checklist;
		}
		public int getTotalceco() {
			return totalceco;
		}
		public void setTotalceco(int totalceco) {
			this.totalceco = totalceco;
		}
		@Override
		public String toString() {
			return "ConsultaCecoCheckDTO2 [idUsuario=" + idUsuario + ", ceco=" + ceco + ", checklist=" + checklist
					+ ", totalceco=" + totalceco + "]";
		}
		
		
		
		
		
		
		
	
	}