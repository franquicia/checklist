<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">

<head>

<script src="../js/expancion/jquery-3.3.1.js"></script>
<script src="../js/expancion/jquery-ui-1.12.1.js"></script>



<script type="text/javascript">
var urlServer = '${urlServer}';

$(document).ready(
		function() {
			$('#owl-carousel1').on(
					'initialized.owl.carousel changed.owl.carousel',
					function(e) {
						if (!e.namespace) {
							return;
						}
						var carousel = e.relatedTarget;
						$('#txtMdl1').html(
								" Item "
										+ '<span class="imgNum">'
										+ (carousel.relative(carousel
												.current()) + 1)
										+ '</span> de '
										+ carousel.items().length);
					}).owlCarousel({
				loop : false,
				margin : 10,
				nav : true,
				items : 1,
				dots : false,
			});

			$('.owl-stage-outer').zoomify({

				  // animation duration
				  duration: 200,

				  // easing effect
				  easing:   'linear',

				  // zoom scale
				  // 1 = fullscreen
				  scale:    0.9

				});
		});
</script>


<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />

<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/secciones.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/estiloVentana.css" media="screen">


<link rel="stylesheet" type="text/css" href="../css/expancion/modal.css">
<link rel="stylesheet" type="text/css" href="../css/expancion/index.css">

<!-- CARRUSEL -->
<link rel="stylesheet" type="text/css" href="../css/expancion/zoomify.css">
<link rel="stylesheet" href="../css/expancion/owl.theme.defaultHallazgos.css">
<link rel="stylesheet" href="../css/expancion/owl.carouselHallazgos.css">

<script src="../js/expancion/owl.carousel.js"></script>
<script src="../js/expancion/zoomify.js"></script>
<!-- CARRUSEL -->

<script src="../js/expancion/alertify.js"></script>
<link rel="stylesheet" href="../css/expancion/alertify.css">

<style type="text/css">


.divContainer{
margin-left: auto;
margin-right: auto;
display: block;
width:50%;
height:70%;
background-color:#E8E8E8;
}

.divContenido{
	border-style: solid 1px;
	margin-top:2px;
	margin-left:auto;
	margin-right: auto;
	display: block;
	width:99%;
	height:9.5%;
}

.divImg{
	margin-top:2px;
	margin-left:auto;
	margin-right: auto;
	display: block;
	width:99%;
	height:70%;
}

.divAccion{
margin-top:10px;
margin-left: auto;
margin-right: auto;
display: block;
width:50%;
height:5%;
}

.divContenido{
	margin-top:2px;
	margin-left:auto;
	margin-right: auto;
	display: block;
	width:99%;
	height:9.5%;
	background-color:white; 
}

.textoItem{
	text-align:center;
	margin-top:2px;
	width:100%;
	height:100%;
}

.btnRespuesta{
	margin-top:2px;
	width: 250px;
    height: 50px;
	margin-left:auto;
	margin-right: auto;
	display: auto;	
}

#divSiBtn{
    margin-left: 0%;
    margin-right: auto;
	float:left;
  	width:45px;
  	height:45px;
  	background-color:green;
  	border-radius: 25px;
}

#divNoBtn{
	margin-left: auto;
    margin-right: 0%;
    float:right;
  	width:45px;
  	height:45px;
  	background-color:gray;
  	border-radius: 25px;
}

.btnDecision{
	margin-top:2px;
	/*width: 300px; erick*/
    height: 50px;
	margin-left:auto;
	margin-right: auto;
	display: auto;	
}

#decisionSi{
	 margin-left: 0%;
    margin-right: auto;
	float:left;
  	width:45px;
  	height:45px;
}

#decisionNo{
	margin-left: auto;
    margin-right: 0%;
    float:right;
  	width:45px;
  	height:45px;
}

#divSiBtn:hover,#divNoBtn:hover,#decisionSi:hover,#decisionNo:hover {
  transform: scale(1.1); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
}

/* DIV PARA EL TRACKING */
	#contenedorTracking {
        background-color:white;
        width:75%;
        height:500px;
        margin-left: auto;
		margin-right: auto;
		display: block;
        
        }
        
	#cabecera {
        color:white;
        background-color:white;
        color:gray;
        padding:10px;
        height:10%;
        margin-left:10%;
      }

      #contenidoImg {
	    background-color: lightgray;
	    height: 70%;
	    width: 40%;
	    margin-left: 20%;
	    margin-right: auto;
	    display: block;
	}

      #linea {
            background-color: white;
		    float: left;
		    height: 499px;
		    width: 10%;
		    margin-top: -69px;
      }
      
       #respuestaBtn{
       		border-radius: 48%;
       		margin-top: 25px;
       		height: 40px;
		    width: 40px;
		    color:white;
            background-color: #323232;
            margin-left: auto;
			margin-right: auto;
			display: block;
      }
      
      #respuestaBtnDiv{
       		margin-top: -1px;
       		height: 500px;
		    width: 5px;
            background-color: #323232;
            margin-left: auto;
			margin-right: auto;
			display: block;
			
      }
      
      #respuestaBtnDivSup{
       		margin-top: -1px;
       		height: 40px;
		    width: 5px;
            background-color: #323232;
            margin-left: auto;
			margin-right: auto;
			display: block;
			
      }
      
      #respuestaBtn2{
       		border-radius: 48%;
       		height: 40px;
		    width: 40px;
		    color:white;
            background-color: #323232;
            margin-left: auto;
			margin-right: auto;
			display: block;
      }
      
      #imgEvidencia{ 
            margin-left: auto;
		    margin-right: auto;
		    display: block;
		    width: 50%;
		    height: 75%;
		    padding-top: 5%;
    }
    

	.owl-carousel .owl-stage-outer {
    width: 100%;
    height: 250px;
    position: relative;
    overflow: hidden;
    -webkit-transform: translate3d(0px, 0px, 0px);
	}
	
	.cuadrobigUnic {
    width: 85%;
    height: 100%;
    max-width: 100%;
    margin: 0 auto;
    position: relative;
    background-color: #d6d6d6;
    /* border: 1px solid #AAB7CE; */
    padding: 1px;
    position: relative;
    transition: all 0.3s ease-out;
    -moz-transition: all 0.3s ease-out;
    -webkit-transition: all 0.3s ease-out;
	}
	
	.divImg {
    margin-top: 2px;
    margin-left: auto;
    margin-right: auto;
    display: block;
    width: 99%;
    height: 70%;
	}
/* DIV PARA EL TRACKING */

</style>


<!--Definicion de un encabezado-->	

<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/estilos.css">
<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/menuNuevo.css">
<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/dropkickModificado.css">
</head>

<body style="height: auto;">

		<div class="header">
			<div class="tblHeader">
				<div class="headerMenu">
				</div>
				
				<div class="headerLogo">
					<a> <img
						src="../images/supervisionSistemas/logo-baz.svg" id="imgLogo">
					</a>
				</div>
			</div>
		</div>

		<div class="clear"></div>

	
	<div class="header h90" style="text-align:center; background-color: #d3d3d33b; border-bottom:solid 0px;">
		<h4 style="text-align:left; margin-left:15px; color:gray; line-height:100%;">Histórico</h4>
		<h3 style="text-align:left; margin-left:15px; color:black; line-height:100%;"><b>Autorización de Hallazgo</b></h3>
		<h3 style="text-align:right; margin-right:15%; color:black; line-height:100%;"><a style="text-decoration:underline; color:black;" href="#">Mis Sucursales</a></h3>
	</div>
	
	<div class="divContenido" style="margin-top: 30px; margin-left:30px;">
		<div class="textoItem" style="text-align:left;">
			<label style="font-size:15px;"><b>${pregunta}</b></label>
		</div>
	</div>
	
	<div id="contenedorTracking" style="margin-top: 30px;">
	
		${cabeceraHallazgoInicial}
      	
      	<div id="contenidoImg">
			<!-- CARRUSEL --> 
			<div class="divImg">
				${carrusel}
			<!-- CARRUSEL -->     		
      		</div>
		</div>
	
	</div>    
	  	${historialCadena}
	
</body>
</html>