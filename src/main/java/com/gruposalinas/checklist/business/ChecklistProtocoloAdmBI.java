package com.gruposalinas.checklist.business;


import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ChecklistProtocoloAdmDAO;
import com.gruposalinas.checklist.dao.ChecklistProtocoloDAO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;

public class ChecklistProtocoloAdmBI {
	
	private static final Logger logger = LogManager.getLogger(ChecklistBI.class);
	
	@Autowired
	ChecklistProtocoloAdmDAO checklistProtocoloAdmDAO;
	
	List<ChecklistProtocoloDTO> listaChecklist= null;
	
	
	public List<ChecklistProtocoloDTO> buscaChecklist(ChecklistProtocoloDTO bean){
		List<ChecklistProtocoloDTO> listaChecklist2= null;
		try{
			
			listaChecklist2 = checklistProtocoloAdmDAO.getChecklistProtocolo(bean);
		}catch(Exception e){
			logger.info("No fue posible obtener el Checklist");
			
		}
		
		return listaChecklist2;
		
	}
	
	public int insertaChecklistCom(ChecklistProtocoloDTO bean){

		int idChecklist = 0;
		try{
			idChecklist = checklistProtocoloAdmDAO.insertaChecklistCom(bean);
		}catch(Exception e){
			logger.info("No fue posible insertar el Checklist");
			logger.info("Fallos" + e);
			
		}
		
		return idChecklist;
	}
	
/*
	public boolean eliminaChecklist(int idCheckList){
		boolean respuesta= false;
		
		try {
			respuesta = checklistDAO.eliminaChecklist(idCheckList);
		} catch (Exception e) {
			logger.info("No fue posible eliminar el Checklist");
			
		}
		
		return respuesta;
		
	}
*/


	//total pond
	public boolean actualizaChecklistCom(ChecklistProtocoloDTO bean){
		boolean respuesta= false;
		
		try {
			respuesta =  checklistProtocoloAdmDAO.actualizaChecklistCom(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Checklist");
			
		}
		
		return respuesta;
		
	}


}