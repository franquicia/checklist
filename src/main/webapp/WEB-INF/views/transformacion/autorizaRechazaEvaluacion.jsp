<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">

<head>

<script src="../js/expancion/jquery-3.3.1.js"></script>
<script src="../js/expancion/jquery-ui-1.12.1.js"></script>

<script type="text/javascript">
var urlServer = '${urlServer}';

$(document).ready(
		function() {
			$('#owl-carousel2').on(
					'initialized.owl.carousel changed.owl.carousel',
					function(e) {
						if (!e.namespace) {
							return;
						}
						var carousel = e.relatedTarget;
						$('#txtMdl2').html(
								" Item "
										+ '<span class="imgNum">'
										+ (carousel.relative(carousel
												.current()) + 1)
										+ '</span> de '
										+ carousel.items().length);
					}).owlCarousel({
				loop : false,
				margin : 10,
				nav : true,
				items : 1,
				dots : false,
			});

			$('.owl-stage-outer').zoomify({

				  // animation duration
				  duration: 200,

				  // easing effect
				  easing:   'linear',

				  // zoom scale
				  // 1 = fullscreen
				  scale:    0.9

				});
		});

$(document).ready(
		function() {
			$('#owl-carousel1').on(
					'initialized.owl.carousel changed.owl.carousel',
					function(e) {
						if (!e.namespace) {
							return;
						}
						var carousel = e.relatedTarget;
						$('#txtMdl1').html(
								" Item "
										+ '<span class="imgNum">'
										+ (carousel.relative(carousel
												.current()) + 1)
										+ '</span> de '
										+ carousel.items().length);
					}).owlCarousel({
				loop : false,
				margin : 10,
				nav : true,
				items : 1,
				dots : false,
			});

			$('.owl-stage-outer').zoomify({

				  // animation duration
				  duration: 200,

				  // easing effect
				  easing:   'linear',

				  // zoom scale
				  // 1 = fullscreen
				  scale:    0.9

				});
		});
function regresar() {
	alertify.confirm('AVISO', 'Deseas regesar a la pantalla anterior', 
			function(){
				window.history.back();
		 		alertify.success('Ok');
		 	}, 
		 	function(){ 
			 	alertify.error('Cancelado');
			}
	);
}



function aceptarHallazgo(idCeco,idProyecto,idFase,tipoEvidencia,idResp,status,respuesta,ruta,fechaAutorizacion,fechaFin,comentario,idHallazgo,comentarioInicio){


	alertify.confirm('AVISO', 'Deseas Autorizar?', 
			function(){
				var status = "3";
				if(${perfil}==1){ status = "3";}
				if(${perfil}==2){ status = "4";}
				
				updateHallazgo(idCeco,idProyecto,idFase,tipoEvidencia,idResp,status,"SI",ruta,fechaAutorizacion,fechaFin,comentario,idHallazgo,${idFolio},comentarioInicio,"${motivoRechazo}");
		 		alertify.success('Ok') ;
		 	}, 
		 	function(){ 
			 	alertify.error('Cancelado');
			}
	);

}

function rechazarHallazgo(idCeco,idProyecto,idFase,tipoEvidencia,idResp,status,respuesta,ruta,fechaAutorizacion,fechaFin,comentario,idHallazgo,comentarioInicio){
//rechazarHallazgo(488361,370,0,'NO','/franquicia/imagenes/2019/73610_3_7_1710201911025415.jpg','','2019-10-17 11:26:20','atención 1','1102','2.2');
	
	alertify.prompt('AVISO',"Escribe el motivo de tu rechazo", "",
  		function(evt, value ){
  			if(value==""){
  				alertify.error('Se requiere un motivo para rechazar.');
  	  	  	} else {
  	  	  		updateHallazgo(idCeco,idProyecto,idFase,tipoEvidencia,idResp,"0","NO",ruta,"",fechaFin,comentario,idHallazgo,idHallazgo,comentarioInicio,value);
  	  	  		alertify.success('Realizando rechazo...'+value);
  	  	  	}
    			
  		},
 		function(){
    			alertify.error('No se realizo el rechazo.');
  		}
  	);
}

function updateHallazgo(idCeco,idProyecto,idFase,tipoEvidencia,idResp,status,respuesta,ruta,fechaAutorizacion,fechaFin,comentario,idHallazgo,idFolio,comentarioInicio,motivoRechazo){
	$(location).attr("href","actualizaHallazgoDirectoTransformacion.htm?ceco="+idCeco+"&idProyecto="+idProyecto+"&idFase="+idFase+"&tipoEvidencia="+tipoEvidencia+"&idResp="+idResp+"&status="+status+"&respuesta="+respuesta+"&ruta="+ruta+"&fechaAutorizacion="+fechaAutorizacion+"&fechaFin="+fechaFin+"&comentario="+comentarioInicio+"&idHallazgo="+idHallazgo+"&idFolio="+idFolio+"&comentarioInicio="+comentario+"&motivoRechazo="+motivoRechazo);
	
}

</script>


<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />

<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/secciones.css">
<link rel="stylesheet" type="text/css" href="../css/supervisionSistemas/estiloVentana.css" media="screen">

<link rel="stylesheet" type="text/css" href="../css/expancion/modal.css">
<link rel="stylesheet" type="text/css" href="../css/expancion/index.css">

<!-- CARRUSEL -->
<link rel="stylesheet" type="text/css" href="../css/expancion/zoomify.css">
<link rel="stylesheet" href="../css/expancion/owl.theme.defaultHallazgos.css">
<link rel="stylesheet" href="../css/expancion/owl.carouselHallazgos.css">

<script src="../js/expancion/owl.carousel.js"></script>
<script src="../js/expancion/zoomify.js"></script>
<!-- CARRUSEL -->

<script src="../js/expancion/alertify.js"></script>
<link rel="stylesheet" href="../css/expancion/alertify.css">

<style type="text/css">
.divContainer {
	margin-left: auto;
	margin-right: auto;
	display: block;
	width: 50%;
	height: 70%;
	background-color: #E8E8E8;
}

.divContenido {
	border-style: solid 1px;
	margin-top: 2px;
	margin-left: auto;
	margin-right: auto;
	display: block;
	width: 99%;
	height: 9.5%;
}

.divImg {
	margin-top: 2px;
	margin-left: auto;
	margin-right: auto;
	display: block;
	width: 99%;
	height: 70%;
	background-color: white;
}

.divAccion {
	margin-top: 10px;
	margin-left: auto;
	margin-right: auto;
	display: block;
	width: 50%;
	height: 5%;
}

.divContenido {
	margin-top: 2px;
	margin-left: auto;
	margin-right: auto;
	display: block;
	width: 99%;
	height: 9.5%;
	background-color: white;
}

.textoItem {
	text-align: center;
	margin-top: 2px;
	width: 100%;
	height: 100%;
}

.btnRespuesta {
	margin-top: 2px;
	width: 250px;
	height: 50px;
	margin-left: auto;
	margin-right: auto;
	display: auto;
}

#divSiBtn {
	margin-left: 0%;
	margin-right: auto;
	float: left;
	width: 45px;
	height: 45px;
	background-color: green;
	border-radius: 25px;
}

#divNoBtn {
	margin-left: auto;
	margin-right: 0%;
	float: right;
	width: 45px;
	height: 45px;
	background-color: gray;
	border-radius: 25px;
}

.btnDecision {
	margin-top: 2px;
	/*width: 300px; erick*/
	height: 50px;
	margin-left: auto;
	margin-right: auto;
	display: auto;
}

#decisionSi {
	margin-left: 0%;
	margin-right: auto;
	float: left;
	width: 45px;
	height: 45px;
}

#decisionNo {
	margin-left: auto;
	margin-right: 0%;
	float: right;
	width: 45px;
	height: 45px;
}

#divSiBtn:hover, #divNoBtn:hover, #decisionSi:hover, #decisionNo:hover {
	transform: scale(1.1);
	/* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
}
</style>


</head>

<body style="height: auto;">

	<div class="header h90">
		<span class="titulagsBold spBold fz146">${titulo}</span>
	</div>
		<div class="divContenido">

			<div class="textoItem">
				<label style="font-size: 18px;">${pregunta}</label>

			</div>

		</div>

		<div class="divContenido">
			<div class="btnRespuesta">
				<div id="divSiBtn">
					<label
						style="color: white; text-align: center; margin-left: auto; margin-right: auto; display: block; font-size: 20px;"><b>SI</b></label>
				</div>

				<div id="divNoBtn">
					<label
						style="color: white; text-align: center; margin-left: auto; margin-right: auto; display: block; font-size: 20px;"><b>NO</b></label>
				</div>
			</div>
		</div>
		<c:if test = "${carrusel3 != ''}">
			<div class="divImg">${carrusel3}</div>
			<div class="" style="padding-bottom: 1%; padding-top: 1%;">
				<span style="width: 60%; height: 95%;  resize: none; display: block; margin: auto;" >Observacion atención: </span>
				<textarea style="width: 60%; height: 95%;  resize: none; display: block; margin: auto;" readonly>${obsAtencion}</textarea>
			</div>
			<div class="" style="padding-bottom: 1%; padding-top: 1%;">
				<span style="width: 60%; height: 95%;  resize: none; display: block; margin: auto;" >Motivo Rechazo: </span>
				<textarea style="width: 60%; height: 95%;  resize: none; display: block; margin: auto;" readonly>${motivoRechazo}</textarea>
			</div>
		</c:if>

		<div class="divImg">${carrusel2}</div>
		<div class="divImg">${carrusel1}</div>
		<div class="" style="padding-bottom: 1%; padding-top: 1%;">
			<span style="width: 60%; height: 95%;  resize: none; display: block; margin: auto;" >Observacion: </span>
			<textarea style="width: 60%; height: 95%;  resize: none; display: block; margin: auto;" readonly>${comentarios}</textarea>
		</div>
		<c:if test = "${carrusel3 == '' && motivoRechazo!=null && motivoRechazo!='' }">
			<div class="" style="padding-bottom: 1%; padding-top: 1%;">
				<span style="width: 60%; height: 95%;  resize: none; display: block; margin: auto;" >Motivo Rechazo: </span>
				<textarea style="width: 60%; height: 95%;  resize: none; display: block; margin: auto;" readonly>${motivoRechazo}</textarea>
			</div>
		</c:if>
		
		<div class="btnDecision">
    			<button id="decisionNoBtn" style="background-color: red;color: white;font-size: 20px;float: left;margin-left: 20%; visibility: ${visibleBoton};" onclick="rechazarHallazgo(${ceco},${idProyecto},${idFase},${tipoArchivo},${idResp},${status},'${respuesta}','${ruta}','${fechaAutorizacion}','${fechaFin}','${comentarios}',${idHallazgo},'${obsAtencion}');">Rechazar</button>
    			<button id="decisionNoBtn" style="background-color: gray;color: white;font-size: 20px;float: left;margin-left: 20%;" onclick="regresar();">Regresar</button>
    			<button id="decisionNoBtn" style="background-color: green;color: white;font-size: 20px;float: left;margin-left: 20%; visibility: ${visibleBoton};" onclick="aceptarHallazgo(${ceco},${idProyecto},${idFase},${tipoArchivo},${idResp},${status},'${respuesta}','${obtieneRutaEvi}','${fechaAutorizacion}','${fechaFin}','${comentarios}',${idHallazgo},'${obsAtencion}');">Autorizar</button>
		</div>

		<div class="clear"></div>
</body>
</html>