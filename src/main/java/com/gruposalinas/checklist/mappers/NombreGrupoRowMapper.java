package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.DetalleProtoDTO;

public class NombreGrupoRowMapper implements RowMapper<DetalleProtoDTO> {

	public DetalleProtoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		DetalleProtoDTO detprotoDTO = new DetalleProtoDTO();
		
		detprotoDTO.setIdUsuario(rs.getInt("FIID_CHECKLIST"));
		detprotoDTO.setNomCheklist(rs.getString("FCNOMBRE"));
	return detprotoDTO;
	}
	

		

}






