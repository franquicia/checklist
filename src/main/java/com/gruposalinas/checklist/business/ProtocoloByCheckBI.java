package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.ProtocoloByCheckDAO;
import com.gruposalinas.checklist.domain.ProtocoloByCheckDTO;

public class ProtocoloByCheckBI {

	private static final Logger logger = LogManager.getLogger(ProtocoloByCheckBI.class);

	@Autowired
	ProtocoloByCheckDAO protocoloByCheckDAO;
	List<ProtocoloByCheckDTO> protocolo = null;


	public List<ProtocoloByCheckDTO> obtieneProtocolo(int idChecklist)
			throws Exception {
		try {
			protocolo = protocoloByCheckDAO.obtieneProtocolo(idChecklist);
		} catch (Exception e) {
			logger.info("No fue posible consultar la información " + e);
		}

		return protocolo;
	}

}