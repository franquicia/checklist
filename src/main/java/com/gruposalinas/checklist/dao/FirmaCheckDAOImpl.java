package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.FirmaCheckDTO;
import com.gruposalinas.checklist.mappers.FirmaCheckRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class FirmaCheckDAOImpl extends DefaultDAO implements FirmaCheckDAO {

    private static Logger logger = LogManager.getLogger(FirmaCheckDAOImpl.class);

    DefaultJdbcCall jdbcInsertaFirma;
    DefaultJdbcCall jdbcInsertaFirmaN;
    DefaultJdbcCall jdbcEliminaFirma;
    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneFirma;
    DefaultJdbcCall jbdcActualizaFirma;
    DefaultJdbcCall jdbcObtieneFirmaCecoFaseProyecto;

    public void init() {

        jdbcInsertaFirma = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFIRMACHECK")
                .withProcedureName("SP_INS_FIRMA");

        jdbcInsertaFirmaN = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFIRMACHECK")
                .withProcedureName("SP_INS_FIRMAN");

        jdbcEliminaFirma = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFIRMACHECK")
                .withProcedureName("SP_DEL_FIRMA");

        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFIRMACHECK")
                .withProcedureName("SP_SEL_FIRMAG")
                .returningResultSet("RCL_FIRMA", new FirmaCheckRowMapper());

        jdbcObtieneFirma = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFIRMACHECK")
                .withProcedureName("SP_SEL_FIRMACE")
                .returningResultSet("RCL_FIRMA", new FirmaCheckRowMapper());

        jbdcActualizaFirma = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMFIRMACHECK")
                .withProcedureName("SP_ACT_FIRMA");

        jdbcObtieneFirmaCecoFaseProyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEXPNVOFLUJO")
                .withProcedureName("SP_SEL_FIRMACE")
                .returningResultSet("RCL_FIRMA", new FirmaCheckRowMapper());
    }

    public int inserta(FirmaCheckDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITACORA", bean.getBitacora())
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_PUESTO", bean.getPuesto())
                .addValue("PA_RESPONSABLE", bean.getResponsable())
                .addValue("PA_CORREO", bean.getCorreo())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_RUTA", bean.getRuta());

        out = jdbcInsertaFirma.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMFIRMACHECK.SP_INS_FIRMA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_FIRMA");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió La firma del checklist");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public int insertaN(FirmaCheckDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITACORA", bean.getBitacora())
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_PUESTO", bean.getPuesto())
                .addValue("PA_RESPONSABLE", bean.getResponsable())
                .addValue("PA_CORREO", bean.getCorreo())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_RUTA", bean.getRuta())
                .addValue("PA_AGRUPA", bean.getIdAgrupa())
                .addValue("PA_FASE", bean.getIdFase())
                .addValue("PA_PROYECTO", bean.getIdProyecto());

        out = jdbcInsertaFirmaN.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMFIRMACHECK.SP_INS_FIRMA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_FIRMA");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió La firma del checklist");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean elimina(String idFirma) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIRMA", idFirma);

        out = jdbcEliminaFirma.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMFIRMACHECK.SP_DEL_FIRMA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar la firma(" + idFirma + ")");
        } else {
            return true;
        }

        return false;

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<FirmaCheckDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<FirmaCheckDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneTodo.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMFIRMACHECK.SP_SEL_FIRMAG}");

        listaFijo = (List<FirmaCheckDTO>) out.get("RCL_FIRMA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las firmas");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //parametro

    @SuppressWarnings("unchecked")
    public List<FirmaCheckDTO> obtieneDatos(String ceco) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<FirmaCheckDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco);

        out = jdbcObtieneFirma.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMFIRMACHECK.SP_SEL_FIRMACE}");
        //lleva el nombre del cursor del procedure
        lista = (List<FirmaCheckDTO>) out.get("RCL_FIRMA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las firmas del(" + ceco + ")");
        }
        return lista;

    }

    public boolean actualiza(FirmaCheckDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIRMA", bean.getIdFirma())
                .addValue("PA_BITACORA", bean.getBitacora())
                .addValue("PA_CECO", bean.getCeco())
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_PUESTO", bean.getPuesto())
                .addValue("PA_RESPONSABLE", bean.getResponsable())
                .addValue("PA_CORREO", bean.getCorreo())
                .addValue("PA_NOMBRE", bean.getNombre())
                .addValue("PA_RUTA", bean.getRuta());

        out = jbdcActualizaFirma.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMFIRMACHECK.SP_ACT_FIRMA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado de la firma con  id( " + bean.getIdFirma() + ")");
        } else {
            return true;
        }
        return false;

    }

    @SuppressWarnings("unchecked")
    public List<FirmaCheckDTO> obtieneDatosFirmaCecoFaseProyecto(String ceco, String fase, String proyecto) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<FirmaCheckDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto);

        out = jdbcObtieneFirmaCecoFaseProyecto.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAEXPNVOFLUJO.SP_SEL_FIRMACE}");
        //lleva el nombre del cursor del procedure
        lista = (List<FirmaCheckDTO>) out.get("RCL_FIRMA");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las firmas del(" + ceco + ")");
        }
        return lista;

    }

}
