package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.SucursalDTO;

public interface SucursalDAO {
	
	public List<SucursalDTO> obtieneSucursal() throws Exception;

	public List<SucursalDTO> obtieneSucursal(String idSucursal) throws Exception;
	
	public List<SucursalDTO> obtieneSucursalPaso(String idSucursal) throws Exception;
	
	public int insertaSucursal(SucursalDTO bean) throws Exception;
	
	public boolean actualizaSucursal(SucursalDTO bean) throws Exception;
	
	public boolean eliminaSucursal(int idSucursal) throws Exception;
	
	public boolean cargaSucursales () throws Exception;

	public boolean cargaActualizaPaises () throws Exception;
	
	//GCC
	public List<SucursalDTO> obtieneSucursalGCC(String idSucursal) throws Exception;
	
	public int insertaSucursalGCC(SucursalDTO bean) throws Exception;
	
	public boolean actualizaSucursalGCC(SucursalDTO bean) throws Exception;
	
	public boolean eliminaSucursalGCC(int idSucursal) throws Exception;
	
	public boolean rellenaSucursal() throws Exception;
}
