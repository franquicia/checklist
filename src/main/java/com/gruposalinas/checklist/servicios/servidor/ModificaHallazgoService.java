package com.gruposalinas.checklist.servicios.servidor;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gruposalinas.checklist.business.HallazgosExpBI;
import com.gruposalinas.checklist.business.SucursalChecklistBI;
import com.gruposalinas.checklist.domain.HallazgosExpDTO;



@Controller
@RequestMapping("/consultaHallazgoService")
public class ModificaHallazgoService {
	private static final Logger logger = LogManager.getLogger(ModificaHallazgoService.class);
	

	@Autowired
	SucursalChecklistBI sucursalChecklistBI;
	
	@Autowired
	HallazgosExpBI hallazgosExpBI;
	
	//http://localhost:8080/checklist/consultaHallazgoService/actualizaHallazgoNew.json?ceco="+idCeco+"&idResp="+idResp+"&status="+status+"&respuesta="+respuesta+"&ruta="+ruta+"&fechaAutorizacion="+fechaAutorizacion+"&fechaFin="+fechaFin+"&comentario="+comentario+"&idHallazgo="+idHallazgo+"&comentarioInicio="+comentarioInicio+"&motivoRechazo="+motivoRechazo&img64=img64&idUsuario=idUsuario&nombreUsuario=nombreUsuario
		@RequestMapping(value = "/actualizaHallazgoNew",  method = { RequestMethod.POST, RequestMethod.GET })
		public @ResponseBody Map<String, Boolean> actualizaHallazgoNew(HttpServletRequest request, HttpServletResponse response,
				@RequestParam(value = "ceco", required = true, defaultValue = "") String ceco,
				@RequestParam(value = "idResp", required = true, defaultValue = "") String idResp,
				@RequestParam(value = "status", required = true, defaultValue = "") String status,
				@RequestParam(value = "respuesta", required = true, defaultValue = "") String respuesta,
				@RequestParam(value = "ruta", required = true, defaultValue = "") String ruta,
				@RequestParam(value = "fechaAutorizacion", required = true, defaultValue = "") String fechaAutorizacion,
				@RequestParam(value = "fechaFin", required = true, defaultValue = "") String fechaFin,
				@RequestParam(value = "idHallazgo", required = true, defaultValue = "") String idHallazgo,
				@RequestParam(value = "comentario", required = true, defaultValue = "") String comentario,
				@RequestParam(value = "comentarioInicio", required = true, defaultValue = "") String comentarioInicio,
				@RequestParam(value = "motivoRechazo", required = true, defaultValue = "") String motivoRechazo,
				@RequestParam(value = "img64", required = true, defaultValue = "") String img64,
				@RequestParam(value = "idUsuario", required = true, defaultValue = "") String idUsuario,
				@RequestParam(value = "nombreUsuario", required = true, defaultValue = "") String nombreUsuario
				) throws ServletException, IOException {

			Map<String, Boolean> hasMapRetorno = new HashMap<>();
			int retorno = 0;
			String datosUser = idUsuario +"-"+ nombreUsuario+"-DESDEWEB";
			int idRespLocal=0;
			int cecoLocal=0;
			int statusLocal=0;
			int idHallazgoLocal=0;
			if(motivoRechazo==null || motivoRechazo.equalsIgnoreCase("null")) motivoRechazo="";
			if(respuesta==null || respuesta.equalsIgnoreCase("null")) respuesta="";
			if(ruta==null || ruta.equalsIgnoreCase("null")) ruta="";
			if(fechaAutorizacion==null || fechaAutorizacion.equalsIgnoreCase("null")) fechaAutorizacion="";
			if(fechaFin==null || fechaFin.equalsIgnoreCase("null")) fechaFin="";
			if(comentario==null || comentario.equalsIgnoreCase("null")) comentario="";
			if(img64==null || img64.equalsIgnoreCase("null")) img64="";
			if(idUsuario==null || idUsuario.equalsIgnoreCase("null")) idUsuario="";
			if(nombreUsuario==null || nombreUsuario.equalsIgnoreCase("null")) nombreUsuario="";
			
			HallazgosExpDTO bean = new HallazgosExpDTO();
			try {
				idRespLocal=Integer.parseInt(idResp);
			} catch (NumberFormatException excepcion) {
			    		idRespLocal=0;
			}
			try {
				cecoLocal=Integer.parseInt(ceco);
			} catch (NumberFormatException excepcion) {
				cecoLocal=0;
			}
			try {
				statusLocal=Integer.parseInt(status);
			} catch (NumberFormatException excepcion) {
				statusLocal=0;
			}
			try {
				idHallazgoLocal=Integer.parseInt(idHallazgo);
			} catch (NumberFormatException excepcion) {
				idHallazgoLocal=0;
			}

			
			bean.setIdResp(idRespLocal);
			bean.setStatus(statusLocal);
			bean.setRespuesta(respuesta);
			bean.setRuta(ruta);
			bean.setFechaAutorizo(fechaAutorizacion.replaceAll("-", ""));
			bean.setFechaFin(fechaFin.replaceAll("-", ""));
			
			// SE ACTUALIZA CON ID RESPUESTA, NUEVO ES CON ID HALLAZGO
			bean.setIdHallazgo(idHallazgoLocal);
			bean.setIdResp(idRespLocal);
			// SE ACTUALIZA MOTIVO RECGAZO Y USUARIO MODIF
			bean.setMotivrechazo(motivoRechazo);
			bean.setUsuModif(datosUser);
			
			//if (statusLocal == 3 || statusLocal == 1 || statusLocal == 0){
				bean.setObs(comentarioInicio);
				bean.setObsNueva(comentario);
			//}
			
			if(img64!=null && !img64.isEmpty() && !img64.equals("")) {
				img64 = img64.replaceAll(" ", "+");
				img64 = img64.split(",")[1];
				logger.info(img64);

				String rutaResponse = sucursalChecklistBI.insertaHallazgoDirecto(cecoLocal, img64);

				bean.setRuta(rutaResponse);
			}
			// GUARDA IMAGEN
			
			boolean actualizaResponse = hallazgosExpBI.actualizaResp(bean);
			
			
			hasMapRetorno.put("retorno", actualizaResponse);
			return hasMapRetorno;
		}

}
