package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ProyectFaseTransfDTO;

public class ProyectoFaseTraRowMapper implements RowMapper<ProyectFaseTransfDTO> {

	public ProyectFaseTransfDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ProyectFaseTransfDTO ProyectFaseTransfDTO = new ProyectFaseTransfDTO();
		
		ProyectFaseTransfDTO.setIdFaseProy(rs.getInt("FIID_PROYFASE"));
		ProyectFaseTransfDTO.setIdProyecto(rs.getInt("FIID_PROYECTO"));
		ProyectFaseTransfDTO.setFase(rs.getInt("FIFASE"));
	    ProyectFaseTransfDTO.setAuxFase(rs.getString("FCAUX"));
	    ProyectFaseTransfDTO.setObsFase(rs.getString("FCOBSERVACION"));
	    ProyectFaseTransfDTO.setPeriodo(rs.getString("FCPERIODO"));
	    ProyectFaseTransfDTO.setEdoCargaIni(rs.getInt("FIID_EDOCARGAINI"));
	
		return ProyectFaseTransfDTO;
	}

}


