package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.SesionDTO;

public class SesionRowMapper implements RowMapper<SesionDTO>{

	public SesionDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		SesionDTO sesionDTO = new SesionDTO();
		
		
		sesionDTO.setIdSesion(rs.getInt("FIID_SESION"));
		sesionDTO.setNavegador(rs.getString("FCNAVEGADOR"));
		sesionDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
		sesionDTO.setIp(rs.getString("FCIP"));
		sesionDTO.setFechaLogueo(rs.getString("FD_LOGUEO"));
		sesionDTO.setFechaActualiza(rs.getString("FD_ACTUAL"));
		sesionDTO.setBandera(rs.getInt("FIBANDERA"));
		sesionDTO.setSesionNav(rs.getString("FCSESIONNAV"));
		
		return sesionDTO;
	}

}
