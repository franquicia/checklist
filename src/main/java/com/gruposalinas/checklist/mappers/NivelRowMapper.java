package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.NivelDTO;

public class NivelRowMapper implements RowMapper<NivelDTO>{

	public NivelDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		NivelDTO nivelDTO = new NivelDTO();
		
		nivelDTO.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
		nivelDTO.setIdNivel(rs.getInt("FIID_NIVEL"));
		nivelDTO.setCodigo(rs.getString("FCCODIGO"));
		nivelDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
		
		return nivelDTO;
	}
}
