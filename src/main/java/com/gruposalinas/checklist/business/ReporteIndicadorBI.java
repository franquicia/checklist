package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ReporteIndicadorDAO;
import com.gruposalinas.checklist.domain.RecursoDTO;
import com.gruposalinas.checklist.domain.ReporteIndicadorDTO;
import com.gruposalinas.checklist.util.UtilFRQ;


public class ReporteIndicadorBI {
	private static Logger logger = LogManager.getLogger(ReporteIndicadorBI.class);
	
	@Autowired
	ReporteIndicadorDAO reporteIndicadorDAO;
	List<ReporteIndicadorDTO> listaCecosPendientes = null;
	
	public boolean insertaIndicador(ReporteIndicadorDTO bean) {
		
		boolean respuesta = false ;
		
		try {
			respuesta = reporteIndicadorDAO.insertaIndicador(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar el Indicador");
		}
		
		return respuesta;
	}
	
	public String consultaPorcentaje(String idCeco, int idIndicador){
		String respuesta = "";
		try{
			respuesta = reporteIndicadorDAO.consultaPorcentaje(idCeco, idIndicador);
		}
		catch(Exception e)
		{
			logger.info("No fue posible consultar el porcentaje");
		}
		
		return respuesta;
	}
	
	public boolean actualizaIndicador(ReporteIndicadorDTO bean) {
		
		boolean respuesta = false ;
		
		try {
			respuesta = reporteIndicadorDAO.actualizaIndicador(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Indicador");
		}
		
		return respuesta;
	}
	public boolean insertaPendientes (String idCeco, int indicador) {
		
		boolean respuesta = false ;
		
		try {
			respuesta = reporteIndicadorDAO.insertaPendientes(idCeco, indicador);
		} catch (Exception e) {
			logger.info("No fue posible insertar el Ceco Pendiente");
		}
		
		return respuesta;
	}
	
	public List<ReporteIndicadorDTO> consultaPendientes(String idCeco, String idIndicador)  {

		try {
			listaCecosPendientes = reporteIndicadorDAO.consultaPendientes(idCeco, idIndicador);
			
		} catch (Exception e) {
			logger.info("No fue posible consultar el Ceco Pendiente");
		}

		return listaCecosPendientes;
	}
}
