package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.EmpContacExtDTO;


public interface EmpContacExtDAO {

	  public int inserta(EmpContacExtDTO bean)throws Exception;
		
		public boolean elimina(String idTab)throws Exception;
		
		public List<EmpContacExtDTO> obtieneDatos(String ceco) throws Exception; 
		
		public List<EmpContacExtDTO> obtieneInfo() throws Exception; 
		
		public boolean actualiza(EmpContacExtDTO bean)throws Exception;
		
	}