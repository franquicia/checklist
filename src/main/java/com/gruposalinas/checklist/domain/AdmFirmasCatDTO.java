package com.gruposalinas.checklist.domain;

public class AdmFirmasCatDTO {
	


		private int idFirmaCatalogo;
		private String nombreFirmaCatalogo;
		private int tipoProyecto;
		private int idAgrupaFirma;
		private String cargo;
		private int obligatorio;
		private int orden;
		private int aux;
		private String aux2;
		private String periodo;
		
		public int getIdFirmaCatalogo() {
			return idFirmaCatalogo;
		}
		public void setIdFirmaCatalogo(int idFirmaCatalogo) {
			this.idFirmaCatalogo = idFirmaCatalogo;
		}
		public String getNombreFirmaCatalogo() {
			return nombreFirmaCatalogo;
		}
		public void setNombreFirmaCatalogo(String nombreFirmaCatalogo) {
			this.nombreFirmaCatalogo = nombreFirmaCatalogo;
		}
		public int getTipoProyecto() {
			return tipoProyecto;
		}
		public void setTipoProyecto(int tipoProyecto) {
			this.tipoProyecto = tipoProyecto;
		}
		public int getIdAgrupaFirma() {
			return idAgrupaFirma;
		}
		public void setIdAgrupaFirma(int idAgrupaFirma) {
			this.idAgrupaFirma = idAgrupaFirma;
		}
		public String getCargo() {
			return cargo;
		}
		public void setCargo(String cargo) {
			this.cargo = cargo;
		}
		public int getObligatorio() {
			return obligatorio;
		}
		public void setObligatorio(int obligatorio) {
			this.obligatorio = obligatorio;
		}
		public int getOrden() {
			return orden;
		}
		public void setOrden(int orden) {
			this.orden = orden;
		}
		public int getAux() {
			return aux;
		}
		public void setAux(int aux) {
			this.aux = aux;
		}
		public String getAux2() {
			return aux2;
		}
		public void setAux2(String aux2) {
			this.aux2 = aux2;
		}
		
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		@Override
		public String toString() {
			return "AdmFirmasCatDTO [idFirmaCatalogo=" + idFirmaCatalogo + ", nombreFirmaCatalogo="
					+ nombreFirmaCatalogo + ", tipoProyecto=" + tipoProyecto + ", idAgrupaFirma=" + idAgrupaFirma
					+ ", cargo=" + cargo + ", obligatorio=" + obligatorio + ", orden=" + orden + ", aux=" + aux
					+ ", aux2=" + aux2 + ", periodo=" + periodo + "]";
		}
		
		
	}