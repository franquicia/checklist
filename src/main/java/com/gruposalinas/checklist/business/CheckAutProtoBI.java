package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.CheckAutoProtoDAO;
import com.gruposalinas.checklist.domain.CheckAutoProtoDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class CheckAutProtoBI {

	private static Logger logger = LogManager.getLogger(CheckAutProtoBI.class);

	@Autowired
	CheckAutoProtoDAO checkAutoProtoDAO;
	
	List<CheckAutoProtoDTO> listaAutoriza = null;

	

	
	public List<CheckAutoProtoDTO> obtieneAutoProto(String idTab){
				
		try{
			listaAutoriza = checkAutoProtoDAO.obtieneAutoProto(idTab);	
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla autoriza: "+e.getMessage());
			
		}
				
		return listaAutoriza;
	}	
	
	public int insertaAutoProto(CheckAutoProtoDTO autoriza) {
		
		int idautoriza = 0;
		
		try {		
			idautoriza = checkAutoProtoDAO.insertaAutoProto(autoriza);		
		} catch (Exception e) {
			logger.info("No fue posible insertar el autoriza: "+e.getMessage());
			
		}
		
		return idautoriza;		
	}
	
	public boolean actualizaAutoProto (CheckAutoProtoDTO autoriza){
		
		boolean respuesta = false;
				
		try {
			respuesta = checkAutoProtoDAO.actualizaAutoProto(autoriza);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el autoriza: "+e.getMessage());
			
		}
							
		return respuesta;
	}
public boolean actualizaAutoProtoStatus (CheckAutoProtoDTO autoriza){
		
		boolean respuesta = false;
				
		try {
			respuesta = checkAutoProtoDAO.actualizaAutoProtoStatus(autoriza);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el autoriza: "+e.getMessage());
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaAutoProto(int idautoriza){
		
		boolean respuesta = false;
		
		try{
			respuesta = checkAutoProtoDAO.eliminaAutoProto(idautoriza);
		}catch(Exception e){
			logger.info("No fue posible borrar el autoriza: "+e.getMessage());
			
		}
		
		return respuesta;
	}
}
