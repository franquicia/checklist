package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.AsignaExpDAO;
import com.gruposalinas.checklist.dao.EmpFijoDAO;
import com.gruposalinas.checklist.domain.AsignaExpDTO;

public class AsignaExpBI {

	private static Logger logger = LogManager.getLogger(AsignaExpBI.class);

private List<AsignaExpDTO> listafila;

	@Autowired
	AsignaExpDAO asignaExpDAO;
		
	
	public int inserta(AsignaExpDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = asignaExpDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean elimina(String ceco, String usuario_asig){
		boolean respuesta = false;
		
		try {
			respuesta = asignaExpDAO.elimina(ceco,usuario_asig);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public boolean cargaNuevo( ){
		boolean respuesta = false;
		
		try {
			respuesta = asignaExpDAO.cargaNuevo();
		} catch (Exception e) {
			logger.info("No fue posible cargar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public List<AsignaExpDTO> obtieneDatos( int version) {
		
		try {
			listafila =  asignaExpDAO.obtieneDatos(version);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<AsignaExpDTO> obtieneInfo() {

		try {
			listafila = asignaExpDAO.obtieneInfo();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos ");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	
	public boolean actualiza(AsignaExpDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = asignaExpDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	//act check_usu ceco
	
	public boolean actualizaCheckU(AsignaExpDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = asignaExpDAO.actualizaCheckU(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
public List<AsignaExpDTO> obtieneDatosCheckUsua( String usuario,String ceco) {
	 List<AsignaExpDTO> listafilaAsig =null;
		try {
			listafilaAsig =  asignaExpDAO.obtieneDatosCheckUsua(usuario,ceco);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafilaAsig;			
	}

public List<AsignaExpDTO> obtieneDatosAsignaVer( String usuario) {
	
	try {
		listafila =  asignaExpDAO.obtieneDatosAsignaVer( usuario);
	} catch (Exception e) {
		logger.info("No fue posible obtener los datos");
		UtilFRQ.printErrorLog(null, e);	
	}
	
	return listafila;			
}

public boolean CargaAsig(){
	boolean respuesta = false;
	
	try {
		respuesta = asignaExpDAO.CargaAsig();
	} catch (Exception e) {
		logger.info("No fue posible actualizar");
		UtilFRQ.printErrorLog(null, e);	
	}
	
	return respuesta;			
}

}