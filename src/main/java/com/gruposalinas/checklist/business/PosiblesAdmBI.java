package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.PosiblesAdmDAO;
import com.gruposalinas.checklist.domain.PosiblesDTO;

public class PosiblesAdmBI {

    private static Logger logger = LogManager.getLogger(PosiblesAdmBI.class);

    @Autowired
    PosiblesAdmDAO posiblesAdmDAO;

    List<PosiblesDTO> listaPosible = null;

    public List<PosiblesDTO> buscaPosible() throws Exception {

        listaPosible = posiblesAdmDAO.buscaPosible();

        return listaPosible;
    }

    public int insertaPosible(String posible) {

        int idPosible = 0;

        try {
            idPosible = posiblesAdmDAO.insertaPosible(posible);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Posible");
        }

        return idPosible;

    }

    public boolean actualizaPosible(PosiblesDTO bean) {

        boolean respuesta = false;

        try {
            respuesta = posiblesAdmDAO.actualizaPosible(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar la Posible Respuesta");
        }

        return respuesta;
    }

    public boolean eliminaPosible(int idPosible) {

        boolean respuesta = false;

        try {
            respuesta = posiblesAdmDAO.eliminaPosible(idPosible);
        } catch (Exception e) {
            logger.info("No fue posible insertar la Posible Respuesta");
        }

        return respuesta;
    }

    public List<PosiblesDTO> buscaPosible(int idPosible) throws Exception {

        listaPosible = posiblesAdmDAO.buscaPosible(idPosible);

        return listaPosible;
    }

}
