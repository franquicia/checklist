package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.DuplicadosDTO;

public interface DuplicadosDAO {
	
	public List<DuplicadosDTO> consultaDuplicados(String idUsuario, String checkUsua, String idCheck, String idBitacora, String fechaInicio, String FechaFin, int bandera) throws Exception;
	
	public boolean eliminaDuplicadosResp (String idUsuario, String checkUsua, String idCheck, String idBitacora) throws Exception;
	
	public boolean eliminaDuplicadosNuevo (String idUsuario, String checkUsua, String idCheck, String idBitacora, String fechaInicio, String fechaFin) throws Exception;
	
	public boolean eliminaRespuestasD () throws Exception;

}
