package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.DuplicadosDTO;
import com.gruposalinas.checklist.mappers.DuplicadosRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class DuplicadosDAOImpl extends DefaultDAO implements DuplicadosDAO {

    private static Logger logger = LogManager.getLogger(DuplicadosDAOImpl.class);

    DefaultJdbcCall jdbcEliminaRespuestasD;
    DefaultJdbcCall jdbcConsultaDuplicados;
    DefaultJdbcCall jdbcEliminaResp;
    DefaultJdbcCall jdbcEliminaRespFecha;

    public void init() {

        jdbcEliminaRespuestasD = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPUESTAS")
                .withProcedureName("SPELIMINADUPLICADOS");

        jdbcConsultaDuplicados = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADUPLICADOS")
                .withProcedureName("SPCONSULTADUPLICADOS")
                .returningResultSet("RCL_CONTEO", new DuplicadosRowMapper());

        jdbcEliminaResp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADUPLICADOS")
                .withProcedureName("SPELIMINADUPLICADOS");

        jdbcEliminaRespFecha = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADUPLICADOS")
                .withProcedureName("SPELIMINADUPLI");
    }

    @Override
    public boolean eliminaRespuestasD() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcEliminaRespuestasD.execute();

        logger.info("Funcion ejecutada: {checklist.PARESPUESTAS.SPELIMINADUPLICADOS}");

        logger.info("Salio DAOImpl");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al borrar las Respuestas");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public List<DuplicadosDTO> consultaDuplicados(String idUsuario, String checkUsua, String idCheck, String idBitacora,
            String fechaInicio, String FechaFin, int bandera) throws Exception {

        List<DuplicadosDTO> consulta = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USUARIO", idUsuario)
                .addValue("PA_CHECKUSU", checkUsua)
                .addValue("PA_CHECK", idCheck)
                .addValue("PA_BITACORA", idBitacora)
                .addValue("PA_FECHAI", fechaInicio)
                .addValue("PA_FECHAF", FechaFin)
                .addValue("PA_BANDERA", bandera);

        out = jdbcConsultaDuplicados.execute(in);

        logger.info("Funcion ejecutada:{checklist.PADUPLICADOS.SPCONSULTADUPLICADOS}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        consulta = (List<DuplicadosDTO>) out.get("RCL_CONTEO");

        if (ejecucion == 1) {
            logger.info("Algo ocurrió al consultar las asignaciones");
        }

        return consulta;
    }

    @Override
    public boolean eliminaDuplicadosResp(String idUsuario, String checkUsua, String idCheck, String idBitacora)
            throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USUARIO", idUsuario)
                .addValue("PA_CHECKUSU", checkUsua)
                .addValue("PA_CHECK", idCheck)
                .addValue("PA_BITACORA", idBitacora);

        out = jdbcEliminaResp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PADUPLICADOS.SPELIMINADUPLICADOS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al borrar las Respuestas");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaDuplicadosNuevo(String idUsuario, String checkUsua, String idCheck, String idBitacora,
            String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USUARIO", idUsuario)
                .addValue("PA_CHECKUSU", checkUsua)
                .addValue("PA_CHECK", idCheck)
                .addValue("PA_BITACORA", idBitacora)
                .addValue("PA_FECHAI", fechaInicio)
                .addValue("PA_FECHAF", fechaFin);

        out = jdbcEliminaRespFecha.execute(in);

        logger.info("Funcion ejecutada: {checklist.PADUPLICADOS.SPELIMINADUPLI}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al borrar las Respuestas");
        } else {
            return true;
        }

        return false;
    }
}
