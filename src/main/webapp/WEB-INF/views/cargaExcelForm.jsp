<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ingresa Archivo Excel</title>
<style>
.error {
	color: #a93d35;
	font-style: italic;
}
</style>
</head>
<body>
<br><br>
	<div id="formArchiveContainer">
		<form:form method="POST" id="uploadForm" enctype="multipart/form-data"
			action="archiveExcelLoad.htm">
			<table>
				<tr>
					<td><label>Selecciona el archivo (Excel .xlsx):</label>
					 <form:input type="file" name="file" id="file" path="archive" /></td>
					<form:hidden path="titulo" id="titulo" value="documentoExcel" />
					<form:hidden path="descripcion" id="descripcion" value="documentoExcel"/>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="SUBIR DOCUMENTO"
						onClick="return validaFormArchive();" /></td>
				</tr>
			</table>
		</form:form>
	<div class="error" id="error">${response}</div>
	</div><br>
	<div id="linkSalirCenterContainer">
		<a id="buttonLink" href="/checklist/" title="out">Salir</a>
	</div>
</body>
</html>