package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.LlaveDAO;
import com.gruposalinas.checklist.domain.LlaveDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class LlaveBI {
	
	private static Logger logger = LogManager.getLogger(LlaveBI.class);
	
	@Autowired
	LlaveDAO llaveDAO;
	
	List<LlaveDTO> listLlave = null;
	List<LlaveDTO> listLlaves  = null;
	
	
	public boolean insertaLlave(LlaveDTO bean){

		boolean respuesta = false;
		
		try{
			respuesta = llaveDAO.insertaLllave(bean);
		}catch(Exception e){
			logger.info("No fue posible insertar las Llave");
			
		}

		return respuesta;
		
	}
	
	public boolean actualizaLlave(LlaveDTO bean){
		
		boolean respuesta = false;
		
		try {
			respuesta = llaveDAO.actualizaLlave(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar la Llave");
			
		}
		
		
		return respuesta;
	}
	
	public boolean eliminaLlave(int idLlave){
		boolean respuesta = false;
		
		try {
			respuesta = llaveDAO.eliminaLlave(idLlave);
		} catch (Exception e) {
			logger.info("No fue posible eliminar la Llave");
			
		}
		
		
		return respuesta;
	}
	
	public List<LlaveDTO> obtieneLlave(int idLlave){
		
		try {
			listLlave = llaveDAO.obtieneLlave(idLlave);
		} catch (Exception e) {
			logger.info("No fue posible obtener la Llave");
			
		}
		
		return listLlave;
		
	}
	
	public List<LlaveDTO> obtieneLlave(){
		
		try {
			listLlaves = llaveDAO.obtieneLlave();
		} catch (Exception e) {
			logger.info("No fue posible obtener la Llave");
			
		}
		
		return listLlaves;
	}
	

}
