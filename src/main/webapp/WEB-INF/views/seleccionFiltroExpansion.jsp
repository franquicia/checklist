<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">

<head>

<script src="../js/expancion/jquery-3.3.1.js"></script>
<script src="../js/expancion/jquery-ui-1.12.1.js"></script>
<script src="../js/expancion/busquedaSucursal.js" type="text/javascript"></script>
<script src="../js/expancion/jquery.dataTables.js"></script>
<script src="../js/expancion/tabla.js"></script>



<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />

<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/secciones.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/calendarioCaptacion.css"
	media="screen">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/estiloVentana.css" media="screen">


<link rel="stylesheet" type="text/css"
	href="../css/expancion/tabla-filtros.css">


<!-- -->
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/secciones.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/calendarioCaptacion.css"
	media="screen">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/estiloVentana.css" media="screen">

<link rel="stylesheet" type="text/css" href="../css/expancion/modal.css">
<link rel="stylesheet" type="text/css" href="../css/expancion/index.css">

<!-- Owl Stylesheets -->
<link rel="stylesheet" href="../css/expancion/owl.carousel.css">
<link rel="stylesheet" href="../css/expancion/owl.theme.default.css">

<!-- DropkickDos -->
<link rel="stylesheet" type="text/css"
	href="../css/expancion/dropkickDos.css">
<link rel="stylesheet"
	href="../css/expancion/jquery-ui-datepicker.min.css">



</head>

<body>

	<div class="header h90">
		<span class="titulagsBold spBold fz146">REALIZA UNA BÚSQUEDA</span> <span
			class="subSeccion tp30">Selecciona los criterios de búsqueda</span> <a
			href="" class="msucursales"> <span>MIS SUCURSALES</span>
		</a>
	</div>
	<div class="page">
		<div class="contHome top60">
			<div class="conInfo">

				<img src="../images/expancion/docsearch.svg" class="imgsearch">
				<div class="w30d">


					<table style="margin-top: 4%; width: 100%;">
						<tbody>
							<tr>
								<td class="etiquetas">
									<div class="spBold bot10a">Información sobre Sucursal</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="ui-widget buscar1">

										<c:if test="${busqueda == 0 || idCeco == ''}">
											<input class="input insuc" id="sucursal" class="buscar1"
												autocomplete="off" placeholder="CECO / NOMBRE SUCURSAL"
												style="width: 100%;">
										</c:if>


										<c:if test="${busqueda != 0 && idCeco != ''}">
											<input class="input insuc" id="sucursal" class="buscar1"
												autocomplete="off" placeholder=""
												value="${idCeco} - ${nombreCeco}" style="width: 100%;">
										</c:if>


									</div>
								</td>
							</tr>

						</tbody>
					</table>

				</div>

				<div class="clear"></div>

				<div class="cn-v">
					<div class="w30c prw3">
						<div class="spBold bot10a">Visita</div>

						<c:if test="${busqueda == 0 || tipoRecorrido == ''}">
							<select class="select_Dos" id="tipoRecorrido">
								<option value='0'>Todas</option>
								<option value='2'>Soft Opening</option>
								<option value='1'>Caminata</option>
								<option value='3'>Gran Opening</option>
							</select>
						</c:if>


						<c:if test="${busqueda != 0 && tipoRecorrido != ''}">

							<select class="select_Dos" id="tipoRecorrido">

								<c:if test="${tipoRecorrido == '0'}">
									<option value='0' selected>Todas</option>
									<option value='2'>Soft Opening</option>
									<option value='1'>Caminata</option>
									<option value='3'>Gran Opening</option>
								</c:if>

								<c:if test="${tipoRecorrido == '2'}">
									<option value='0'>Todas</option>
									<option value='2' selected>Soft Opening</option>
									<option value='1'>Caminata</option>
									<option value='3'>Gran Opening</option>
								</c:if>

								<c:if test="${tipoRecorrido == '1'}">
									<option value='0'>Todas</option>
									<option value='2'>Soft Opening</option>
									<option value='1' selected>Caminata</option>
									<option value='3'>Gran Opening</option>
								</c:if>

								<c:if test="${tipoRecorrido == '3'}">
									<option value='0'>Todas</option>
									<option value='2'>Soft Opening</option>
									<option value='1'>Caminata</option>
									<option value='3' selected>Gran Opening</option>
								</c:if>

							</select>

						</c:if>


					</div>

					<div class="w15c">

						<c:if test="${busqueda == 0 || fechaInicio == ''}">

							<input type="text" class="fecal vml" id="datepicker"
								placeholder="FECHA INICIO">

						</c:if>


						<c:if test="${busqueda != 0 && fechaInicio != ''}">

							<input type="text" class="fecal vml" id="datepicker"
								value="${fechaInicioParam}">

						</c:if>
					</div>

					<div class="w1a">
						<span class="spBold">a</span>
					</div>

					<div class="w15c">

						<c:if test="${busqueda == 0 || fechaFin == ''}">

							<input type="text" class="fecal vml" id="datepickerDos"
								placeholder="FECHA TERMINO">

						</c:if>


						<c:if test="${busqueda != 0 && fechaFin != ''}">

							<input type="text" class="fecal vml" id="datepickerDos"
								value="${fechaFinParam}">

						</c:if>

					</div>

					<div class="w15c">
						<div class="tcenter">
							<a id="btnBuscar" href="#" class="btn busc ml20">Buscar</a>
						</div>
					</div>


				</div>
			</div>
		</div>
		<br>
		<br>

		<!-- TABLA 
			<div id="listaSucursalesFiltro" class="marginTotal">
				<div style="width: 80%; margin: auto;">
					<table id="tabla" class="tablaChecklist">
						<thead>
							<tr>
								<th><b>Nombre de Sucursal</b></th>
								<th><b>CECO</b></th>
								<th><b>Zona</b></th>
								<th><b>Región</b></th>
								<th><b>Tipo de Visita</b></th>
								<th><b>Fecha</b></th>
								<th><b>Pre Calificación</b></th>
							</tr>
						</thead>
						<tbody>
							

						</tbody>
					</table>
				</div>
			</div>-->

		<div class="marginTotal">
			<div style="width: 80%; margin: auto;"
				style="width: 80%; margin: auto;">${tabla}</div>
		</div>
</body>
<script type="text/javascript"
	src="../js/expancion/jquery.dropkickDos.js"></script>

<script type="text/javascript"
	src="../js/expancion/jquery.dropkickDos.js"></script>
<script>
            jQuery(document).ready(function($) {
                $( ".select_Dos").dropkick({
                    mobile: true
                });
            });
        </script>

<!-- Date Picker -->
<script src="../js/expancion/jquery-ui-datepicker.min.js"></script>
<script src="../js/expancion/jquery-ui-es.min.js"></script>
<script>
            $(function() {
                $("#datepicker" ).datepicker({       
                    changeMonth: false,
                    changeYear: false,
                    yearRange: "1900:today", 
                        beforeShow : function(){
                            if(!$('.date_picker').length){
                                $('#ui-datepicker-div').wrap('<span class="date_picker"></span>');
                            }
                        }
                });

                $("#datepickerDos" ).datepicker({       
                    changeMonth: false,
                    changeYear: false,
                    yearRange: "1900:today", 
                        beforeShow : function(){
                            if(!$('.date_picker').length){
                                $('#ui-datepicker-div').wrap('<span class="date_picker"></span>');
                            }
                        }
                });
            });
        </script>


<script type="text/javascript">
	$("#empleado").autocomplete({
		source : '../ajaxAutocompletaUsuario.json'
	});

	$("#sucursal").autocomplete({
		source : '../ajaxAutocompletaSucursal.json'
	});

	function maximo(campo, limite) {
		if (campo.value.length >= limite) {
			campo.value = campo.value.substring(0, limite);
		}
	}

	validaCalendario();
</script>
</html>