<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="height: 98%; min-width: 1200px">
<head>
<script type="text/javascript">
	var urlServer = '${urlServer}';
</script>
<script>
var rutaHostImagen='${rutaHostImagen}';
var categoria='${categoria}';
var tipo='${tipo}';
var puesto='${puesto}';
var usuario='${usuario}';
var costos='${costos}';
var auxCeco='${auxCeco}';
var aux=${aux};	
var fecha='${fecha}';	
var tamObj='${tamObj}';
</script>
<link
	href="../css/visualizador/visualizaImagenes.css"
	rel="stylesheet" type="text/css" />
<link
	href="../css/visualizador/foundation.css"
	rel="stylesheet" type="text/css" />
<link
	href="../css/visualizador/twentytwenty.css"
	rel="stylesheet" type="text/css" />
<link
	href="../css/visualizador/comparador.css"
	rel="stylesheet" type="text/css" />


	<script src="../js/script-comparador.js"></script>
	<script	src="../js/visualizador/jquery.event.move.js"></script>
	<script	src="../js/visualizador/jquery.twentytwenty.js"></script>
	<script> $(window).load(function() {
				$(".twentytwenty-container[data-orientation!='vertical']").twentytwenty({default_offset_pct : 0.7});
			});
	</script>
	
</head>
<body onload="init(1)" style="height: 95%;">

	<!-- ...............::::::::::::::::::::::: VISTA DE IMAGENES :::::::::::::::::::::::............... -->
	<c:set var="iActivo" value="${1}"/>
	<c:set var="i" scope="session" value="0" />
	<c:forEach var="mapaDeFotos" items="${mapFotos}">
		<c:set var="i" scope="session" value="${i+1}" />

		<div class="sombra"
			style="position: relative; z-index: 0;  min-height: 674px; height: 90%; background-color: white; margin: 30px 20px 0px 16px; width: 97%; min-width: 1100px;"
			id="div${i}">

			<div class="titulo_margen" id="titulo_general">${mapaDeFotos.key}</div>
			<div class="marco" style="overflow-y: scroll;">
				<table class="ajuste">
					<tr>
						<c:set var="maximo" scope="session" value="${1}" />
						<c:forEach var="item" items="${mapaDeFotos.value}">
							<c:if test="${maximo==7}">
								<tr></tr>
								<c:set var="maximo" scope="session" value="${1}" />
							</c:if>
							<c:set var="rutaOriginal" value="${item.ruta}"/>
							<c:set var="preview" value="${rutaHostImagen}/checklist/preview${fn:substringAfter(rutaOriginal,'imagenes')}" />
							<td>																
								<div class="hovereffect" style="box-shadow: 2px 2px 4px #000;">
									<img class="img-responsive" id="${item.idRuta}"
										src="${preview}" alt="">
									<div class="overlay">
										<a href="#popup2" class="popup2-link"
											onclick="muestraVista2('${mapaDeFotos.key}','${item.idRuta}')">
											<img class="imagen_busca" id="lupa"
											src="../images/visualizador/lupa2.png"/>
											<h2>
												${item.desPregunta}
											</h2>
										</a>
									</div>
								</div>
							</td>
							<c:set var="maximo" scope="session" value="${maximo+1}" />
						</c:forEach>
					</tr>
				</table>
			</div>
			<BR>
			<div style="position: relative; width: 100%; ">
				<center>
					<c:forEach var="j" begin="1" end="${tamObj}">
						<c:if test="${iActivo==j}">
							<a onclick="oculta(${tamObj}), muestraVista(${j})"> 
								<span class="circuloVistasActivo"/>
							</a>
						</c:if>
						<c:if test="${iActivo!=j}">
							<a onclick="oculta(${tamObj}), muestraVista(${j})"> 
								<span class="circuloVistas"/>
							</a>
						</c:if>
					</c:forEach>
				</center>
			</div>
				
		<div style="  float:right;  margin-top: -17px; margin-bottom:27px;">
			<input type="button" class="logVolver-btn" id="logVolver-btn" onclick=" location.href='visualizador.htm' " value="REGRESAR"/>
		</div>	
	
		</div>
		<c:set var="iActivo" value="${iActivo+1}" />
	</c:forEach>

	<!--<c:out value="${mapFotos['TERRITORIAL NORTE']}"/><br>-->

	<!-- ...............::::::::::::::::::::::: TRIUNFO POP :::::::::::::::::::::::............... -->
	<div style="z-index: 2;" class="pop2_general" id="popup2" onclick="oneClick(event, this);">


		<!-- ...............::::::::::::::::::::::: Carrucel Simple :::::::::::::::::::::::............... -->
		<div class="popup-contenedor" id="carouselSimple"  >
			<div id="contentTriunfo2" class="carouselTriunfoDiv">

				<!-- Carousel -->
				<div id="myCarouselTriunfo2" class="carousel slide"
					data-interval="false">

					<!-- Imagenes -->
					<div class="carouselTriunfo2-inner" role="listbox" id="carruselSimple"></div>

					<!-- Controles Izquierda y Derecha -->
					
					<!--Imagenes Flecha Izquierda -->
					<a class="left carousel2-control" href="#myCarouselTriunfo2"
						role="button2" data-slide="prev"> <span class="flechas2"
						id="fAnterior2"> <img class="tamFlechas" 
							src="../images/visualizador/atras1.png"></span>
					</a>
					<!--Imagenes Flecha Derecha -->
					<a class="right carousel2-control" href="#myCarouselTriunfo2"
						role="button2" data-slide="next"> <span class="flechas2"
						id="fSiguiente2"> <img class="tamFlechas"
							src="../images/visualizador/adelante1.png"></span>
					</a>
				</div>
				<br>
			</div>
			<div class="condiciones" class="fuente">
				<div id="selOp1" class="fuenteA">
					<a class="popup2-link"> <span class="cuadrado"
						onClick="ejeSelOp1()"></span>
					</a> <p class="condicionMargenA"> ¿Deseas comparar con información anterior?</p>
				</div>
				<div id="selOp11" class="fuenteB">
					<a class="popup2-link"> <span class="cuadrado2"></span>
					</a> <p class="condicionMargenB">Comparar sobre la misma imagen</p>
				</div>
			</div>
		</div>


		<!-- ...............::::::::::::::::::::::: CARRUCEL DOBLE IMAGEN :::::::::::::::::::::::............... -->

		<div class="popup-contenedor1" id="carouserDoble">
		
			<div class="parFecha">
				<div class="fechaIzqMargen" id="fechaRespuestaIzq"></div>
				<div class="fechaDerMargen" id="fechaRespuestaDer"></div>
			</div>

			<div class="parImagenes">
				<div class="ima_izquierdaMargen">
					<div>
						<img class="ima_izquierda" id="imaDobleIzq">
					</div>
				</div>

				<div class="ima_derechaMargen">
					<div>
						<img class="ima_derecha" id="imaDobleDer">
					</div>
				</div>
			</div>

			<div class="parPreguntas">
				<div class="preguntaIzqMargen">
					<p class="margenEspacioDoble basico"/>
					<p class="letraPregBold basico" id="desPreguntaIzq"></p>
					<p class="letraPregItalic basico" id="respuestaIzq"></p>
					<p class="letraPregRegular basico" id="nombreCecoIzq"></p>
				</div>
				<div class="preguntaDerMargen">
					<p class="margenEspacioDoble basico"/>
					<p class="letraPregBold basico" id="desPreguntaDer"></p>
					<p class="letraPregItalic basico" id="respuestaDer"></p>
					<p class="letraPregRegular basico" id="nombreCecoDer"></p>
				</div>
			</div>
			<hr class="hr-uno"/>		
			
			<!-- Controles Izquierda y Derecha -->

			<!--Imagenes Flecha Izquierda -->
			<a class="left carousel2-control" onclick="carrucelDobleImagen(-1)"
				role="button2" data-slide="prev"> <span class="flechas2"
				id="fAnterior2"> <img class="tamFlechas"
					src="../images/visualizador/atras1.png"></span>
			</a>
			<!--Imagenes Flecha Derecha -->
			<a class="right carousel2-control" onclick="carrucelDobleImagen(1)"
				role="button2" data-slide="next"> <span class="flechas2"
				id="fSiguiente2"> <img class="tamFlechas"
					src="../images/visualizador/adelante1.png"></span>
			</a>
					
			<div class="condiciones" class="fuente">
				<div id="selOp2" class="fuenteA">
					<a class="popup2-link"> <span class="cuadrado"
						onClick="ejeSelOp2()">✓</span>
					</a> ¿Deseas comparar con información anterior?
				</div>
				<div id="selOp22" class="fuenteA">
					<a class="popup2-link"> <span class="cuadrado1"
						onClick=" ejeSelOp3()"></span></a> Comparar sobre la misma imagen
				</div>
			</div>
		</div>



		<!-- ...............::::::::::::::::::::::: CARRUCEL COMPARAR IMAGEN :::::::::::::::::::::::............... -->
		<div class="popup-contenedor2" id="tCom">

			<div class="margenImaCompara">
				<div class="twentytwenty-container">
					<img class="tamImaCompara" id="imaCompaIzq" /> 
					<img class="tamImaCompara" id="imaCompaDer" />
				</div>
			</div>
			<div class="preguntaMargenCompara">
				<p class="margenEspacioCompara basico"/>
				<p class="letraPregBold basico" id="desPreguntaDoble"></p>
				<p class="letraPregItalic basico" id="respuestaDoble"></p>
				<p class="letraPregRegular basico" id="nombreCecoDoble"></p>
			</div>
			<hr class="hr-dos"/>

			<!-- Controles Izquierda y Derecha -->

			<!--Imagenes Flecha Izquierda -->
			<a class="left carousel2-control" onclick="carrucelDiferenciaImagen(-1)"
				role="button2" data-slide="prev"> <span class="flechas2"
				id="fAnterior2"> <img class="tamFlechas"
					src="../images/visualizador/atras1.png"></span>
			</a>
			<!--Imagenes Flecha Derecha -->
			<a class="right carousel2-control" onclick="carrucelDiferenciaImagen(1)"
				role="button2" data-slide="next"> <span class="flechas2"
				id="fSiguiente2"> <img class="tamFlechas"
					src="../images/visualizador/adelante1.png"></span>
			</a>

			<div class="condiciones" class="fuente">
				<div id="selOp3" class="fuenteA">
					<a class="popup2-link"> <span class="cuadrado">✓</span>
					</a> ¿Deseas comparar con información anterior?
				</div>
				<div id="selOp33" class="fuenteA">
					<a class="popup2-link"> <span class="cuadrado1"
						onClick=" ejeSelOp4()">✓</span></a> Comparar sobre la misma imagen
				</div>
			</div>
		</div>
		<!--<a class="popup2-cerrar" onclick="cerrarClick()">X</a>-->
	</div>
	
	
	<!-- 
	<div class="row" style="margin-top: 2em;" >
		<div class="large-7 columns">
			<div class="twentytwenty-container">
				<img
					src="../images/carousel/diapositivas/Diapositiva1.jpg" />
				<img
					src="../images/carousel/diapositivas/Diapositiva2.jpg" />
			</div>
		</div>
	</div>
-->

	
</body>
</html>