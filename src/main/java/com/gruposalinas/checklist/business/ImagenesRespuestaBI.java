package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ImagenesRespuestaDAO;
import com.gruposalinas.checklist.domain.ImagenesRespuestaDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class ImagenesRespuestaBI {
	
	private Logger logger = LogManager.getLogger(ImagenesRespuestaBI.class);
	
	@Autowired
	ImagenesRespuestaDAO imagenesRespuestaDAO;
	
	public List<ImagenesRespuestaDTO> obtieneImagenes(String idArbol, String idImagen){
		
		List<ImagenesRespuestaDTO> lista = null;
		
		try {
			lista = imagenesRespuestaDAO.obtieneImagenes(idArbol, idImagen);
		} catch (Exception e) {
			logger.info("No fue posible consultar informacion imagenes");
				
		}
		
		return lista;
	}
	
	public int insertaImagen(ImagenesRespuestaDTO imagen){
		
		int id = 0;
		
		try {
			id = imagenesRespuestaDAO.insertaImagen(imagen);
			//System.out.println("ID :");
			
		} catch (Exception e) {
			logger.info("No fue posible insertar informacion imagenes");
				
		}
		
		return id;
		
	}
	
	@SuppressWarnings("unused")
	public boolean eliminaImagen(int idImagen){
		boolean respuesta = false;
		
		try {
			respuesta = imagenesRespuestaDAO.eliminaImagen(idImagen);
		} catch (Exception e) {
			logger.info("No fue posible eliminar informacion imagenes");
				
		}
		
		return false;
	}
	
	

}
