package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.GrupoDTO;
import com.gruposalinas.checklist.mappers.GrupoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class GrupoDAOImpl extends DefaultDAO implements GrupoDAO {

    private static Logger logger = LogManager.getLogger(GrupoDAOImpl.class);

    DefaultJdbcCall jdbcObtieneGrupo;
    DefaultJdbcCall jdbcInsertaGrupo;
    DefaultJdbcCall jdbcActualizaGrupo;
    DefaultJdbcCall jdbcEliminaGrupo;

    public void init() {

        jdbcObtieneGrupo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMGRUPO")
                .withProcedureName("SP_SEL_GRUPO")
                .returningResultSet("RCL_GRUPO", new GrupoRowMapper());

        jdbcInsertaGrupo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMGRUPO")
                .withProcedureName("SP_INS_GRUPO");

        jdbcActualizaGrupo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMGRUPO")
                .withProcedureName("SP_ACT_GRUPO");

        jdbcEliminaGrupo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMGRUPO")
                .withProcedureName("SP_DEL_GRUPO");
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<GrupoDTO> obtieneGrupo(String idGrupo) throws Exception {
        Map<String, Object> out = null;
        List<GrupoDTO> listaGrupo = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IGRUPO", idGrupo);

        out = jdbcObtieneGrupo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMGRUPO.SP_SEL_GRUPO}");

        listaGrupo = (List<GrupoDTO>) out.get("RCL_GRUPO");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Grupo");
        } else {
            return listaGrupo;
        }

        return null;
    }

    @Override
    public int insertaGrupo(GrupoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idGrupo = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcInsertaGrupo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMGRUPO.SP_INS_GRUPO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDGRUPO");
        idGrupo = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Grupo");
        } else {
            return idGrupo;
        }

        return idGrupo;
    }

    @Override
    public boolean actualizaGrupo(GrupoDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDGRUPO", bean.getIdGrupo())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcActualizaGrupo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMGRUPO.SP_ACT_GRUPO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el Grupo");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaGrupo(int idGrupo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDGRUPO", idGrupo);

        out = jdbcEliminaGrupo.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMGRUPO.SP_DEL_GRUPO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el Grupo");
        } else {
            return true;
        }

        return false;
    }

}
