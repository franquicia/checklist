package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.PreguntaCheckDTO;

public class PreguntaPregConcatRowMapper implements RowMapper<ChecklistPreguntasComDTO> {

	public ChecklistPreguntasComDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		

		ChecklistPreguntasComDTO com = new ChecklistPreguntasComDTO();

       
	    com.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    com.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
	    com.setPregunta(rs.getString("PREGUNTA"));   
	    com.setIdcritica(rs.getInt("FICRITICA"));
	    com.setIdBitaGral(rs.getInt("FIID_BITGRAL"));
		return com;
	}
}



