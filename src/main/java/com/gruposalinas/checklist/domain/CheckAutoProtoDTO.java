package com.gruposalinas.checklist.domain;

public class CheckAutoProtoDTO {
	


		private int idTab;
		private int idCheck;
		private int idUsu;
		private String detalle;
		private int status;
		private String usuTipo;
		private String fechaLiberacion;
		private String periodo;
		private String nomUsu;
		private int fiversion;
		public int getIdTab() {
			return idTab;
		}
		public void setIdTab(int idTab) {
			this.idTab = idTab;
		}
		public int getIdCheck() {
			return idCheck;
		}
		public void setIdCheck(int idCheck) {
			this.idCheck = idCheck;
		}
		public int getIdUsu() {
			return idUsu;
		}
		public void setIdUsu(int idUsu) {
			this.idUsu = idUsu;
		}
		public String getDetalle() {
			return detalle;
		}
		public void setDetalle(String detalle) {
			this.detalle = detalle;
		}
		public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		public String getUsuTipo() {
			return usuTipo;
		}
		public void setUsuTipo(String usuTipo) {
			this.usuTipo = usuTipo;
		}
		public String getFechaLiberacion() {
			return fechaLiberacion;
		}
		public void setFechaLiberacion(String fechaLiberacion) {
			this.fechaLiberacion = fechaLiberacion;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		public String getNomUsu() {
			return nomUsu;
		}
		public void setNomUsu(String nomUsu) {
			this.nomUsu = nomUsu;
		}
		
		public int getFiversion() {
			return fiversion;
		}
		public void setFiversion(int fiversion) {
			this.fiversion = fiversion;
		}
		@Override
		public String toString() {
			return "CheckAutoProtoDTO [idTab=" + idTab + ", idCheck=" + idCheck + ", idUsu=" + idUsu + ", detalle="
					+ detalle + ", status=" + status + ", usuTipo=" + usuTipo + ", fechaLiberacion=" + fechaLiberacion
					+ ", periodo=" + periodo + ", nomUsu=" + nomUsu + ", fiversion=" + fiversion + "]";
		}
		
		
	}