package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.NivelDTO;

public interface NivelDAO {
	
	public List<NivelDTO> obtieneNivel() throws Exception;
	
	public List<NivelDTO> obtieneNivel(int idNivel) throws Exception;
	
	public int insertaNivel(NivelDTO bean) throws Exception;
	
	public boolean actualizaNivel(NivelDTO bean) throws Exception;
	
	public boolean eliminaNivel(int idNivel) throws Exception;

}
