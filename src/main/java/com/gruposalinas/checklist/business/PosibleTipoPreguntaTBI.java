package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.PosibleTipoPreguntaTDAO;
import com.gruposalinas.checklist.domain.PosiblesTipoPreguntaDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class PosibleTipoPreguntaTBI {
	
	private Logger logger = LogManager.getLogger(PosibleTipoPreguntaTBI.class);
	
	@Autowired
	PosibleTipoPreguntaTDAO posibleTipoPreguntaDAO;
	public List<PosiblesTipoPreguntaDTO> obtienePosiblesRespuestasTemp(String idTipoPregunta){
		
		List<PosiblesTipoPreguntaDTO> listaRes = null;
		
		try {
			listaRes = posibleTipoPreguntaDAO.obtienePosiblesRespuestasTemp(idTipoPregunta);
		} catch (Exception e) {
			logger.info("No fue posible obtener las posibles respuestas de la tabla temporal");
			
		}
		
		return listaRes;
		
	}
	
	public boolean insertaPosibleTipoPregunta(PosiblesTipoPreguntaDTO bean){
		
		boolean res= false;
		
		try {
			res = posibleTipoPreguntaDAO.insertaPosibleTipoPregunta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar las posibles respuestas de la tabla temporal");
			
		}
		
		return res;
		
	}
	
	public boolean actualizaPosibleTipoPregunta(PosiblesTipoPreguntaDTO bean){
		
		boolean res = false;
		
		try {
			res = posibleTipoPreguntaDAO.actualizaPosibleTipoPregunta(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar las posibles respuestas de la tabla temporal");
			
		}
		
		return res;
		
	}
	
	public boolean eliminaPosibleTipoPregunta(int idPosibleTipoPregunta){
		boolean res = false;
		
		try {
			res = posibleTipoPreguntaDAO.eliminaPosibleTipoPregunta(idPosibleTipoPregunta);
		} catch (Exception e) {
			logger.info("No fue posible eliminar las posibles respuestas de la tabla temporal");
			
		}
		
		return res;
	}

}
