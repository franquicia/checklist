var idChecklist=0;
var arbolAdmin=0;
var preguntas=new Array();

var urlServer =$(location).attr('host');
var arbolDecision=new Array();
var posiblesRespuestas=new Array();
var totalArbol=0;
var regsArbolBD=0;

var numVersion;

$(document).ready(
		function(){
			numVersion = document.getElementById('numVersion').value;
			idChecklist=$("#idChecklist").val();

			arbolAdmin=$("#arbolAdmin").val();

			//se cargan las posibles respuestas
			cargarPosiblesResp();
			//si no hay preguntas se cargan de la base de datos
			cargarPreguntaPreviasService();
			//se cargan las opciones del arbol generadas previamente
			cargarArbolBDService();
			//alert("TOTAL ARBOL: "+arbolDecision.length);
			cargarValoresIniciales();
		}
);


function cargarPosiblesResp() {
	$
	.ajax({
		type : "GET",
		url : encodeURI("http://"+urlServer + '../consultaCatalogosService/getPosibleTipoPreguntaService.json?idTipoPregunta='),
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var i = 0;
			while (json[i] != null) {
				//document.forms.formulario.tipo.options[i+1]  = new Option(json[i].nombreCheck, json[i].idChecklist);
				posiblesRespuestas.push(
					{
						"id":json[i].idPosibleTipoPregunta,
						"idTipo":json[i].idTipoPregunta,
						"idPosibleRespuesta":json[i].idPosibleRespuesta,
						"respuesta":json[i].descripcionPosible
					}
				)
				i++;


				//alert("REG "+i+"--> id: "+posiblesRespuestas[posiblesRespuestas.length-1].id+"idTipo: "+posiblesRespuestas[posiblesRespuestas.length-1].idTipo+" respuesta: "+posiblesRespuestas[posiblesRespuestas.length-1].respuesta);
			}

			
			
		},
		error : function() {
			alert('Algo paso al cargar los tipos de posibles respuestas');
		},
		async: false
	});
}


function cargarValoresIniciales(){

	var preguntaArbol = "";
	for (var i = 0; i < preguntas.length; i++){
		

		preguntaArbol += "<tr>";
		preguntaArbol += "<td colspan='7' class='tituloPregunta' style='text-align: left;'>";
		preguntaArbol += ""+(i+1)+". "+ preguntas[i].descripcion + "";
		preguntaArbol += "</td>";
		preguntaArbol += "</tr>";

		var tipoPreg = preguntas[i].idTipo;
		var vPrevia = "";
		for(var j=0;j<posiblesRespuestas.length;j++){
			if(posiblesRespuestas[j].idTipo==preguntas[i].idTipo){
				vPrevia+=generarOpcionesArbol(posiblesRespuestas[j].respuesta,preguntas[i].idPregBD,posiblesRespuestas[j].idPosibleRespuesta);
				totalArbol++;
			}
		}
		
		preguntaArbol += vPrevia;
	}

		document.getElementById("preguntasArbol").innerHTML=preguntaArbol;
		quitarEventosCombo();

	
	
		asignarEventosCombo();
		

		$("#preguntasArbol").animate({scrollTop:$("#preguntasArbol")[0].scrollHeight}, 0);

		//se activan elementos de evidencia en caso de ser necesario
		for (var i = 0; i < preguntas.length; i++){
			for(var j=0;j<posiblesRespuestas.length;j++){
				if(posiblesRespuestas[j].idTipo==preguntas[i].idTipo){
					habilitarEvidencia(posiblesRespuestas[j].idPosibleRespuesta,preguntas[i].idPregBD);
				
				}
			}
		}

}


function guardarArbolGeneral(){
	for(var i=0;i<preguntas.length;i++){
		for(var j=0;j<posiblesRespuestas.length;j++){
			if(posiblesRespuestas[j].idTipo==preguntas[i].idTipo){
				agregarOpcionArbol(posiblesRespuestas[j].idPosibleRespuesta,preguntas[i].idPregBD);
			}
				
		}
		//alert("pregunta : "+preguntas[i].idPregBD+" tipo: "+preguntas[i].idTipo);
		
		
		
	}
	
	
}



$(document).ready(
		function() {
			$("#guardar").click(
					function() {
						regsArbolBD=0;
						guardarArbolGeneral();
						return false;
					})
		});



function agregarOpcionArbol(tipoRes,idPregBD){
	

	var campos=false;
	var mensaje="";
	//alert("AGREGAR! "+tipoRes);

	var evidencia = document.getElementById("evidencia_"+tipoRes+"_"+idPregBD).checked;

	var idTipoEvidencia=$("#tipoEvidencia_"+tipoRes+"_"+idPregBD+" span").attr("id");
	
	var evidenciaObliga =  document.getElementById("evidenciaObliga_"+tipoRes+"_"+idPregBD).checked;
	var observaciones =  document.getElementById("observacion_"+tipoRes+"_"+idPregBD).checked;
	var acciones =  document.getElementById("acciones_"+tipoRes+"_"+idPregBD).checked;
	
	var idPregSig=$("#pregSig_"+tipoRes+"_"+idPregBD+" span").attr("id");
	var etiquetaEvidencia=document.getElementById("etiquetaEvidencia_"+tipoRes+"_"+idPregBD).value;
	
	if(evidencia){
		evidencia=1
	}else{
		evidencia=0
	}
	
	
	if(evidenciaObliga){
		evidenciaObliga=1
	}else{
		evidenciaObliga=0
	}
	
	
	
	if(observaciones){
		observaciones=1
	}else{
		observaciones=0
	}

	if(acciones){
		acciones=1
	}else{
		acciones=0
	}
	
	
	//alert("ETIQUETA: "+etiquetaEvidencia);
	
	var res="";

	var indice=0;
	for(var i=0;i<preguntas.length;i++){
		if(preguntas[i].idPregBD==idPregBD){
			indice=i;
		}
	}
	var idPosibleRes;

	for(var j=0;j<posiblesRespuestas.length;j++){
		if(posiblesRespuestas[j].idPosibleRespuesta==tipoRes){
			res=posiblesRespuestas[j].respuesta;
			idPosibleRes=posiblesRespuestas[j].idPosibleRespuesta;
		}
		
	}

	if(idPregSig=="-1"){
		mensaje="\n*Siguiente pregunta";
		campos=true;
		arbolCompleto=false;
	}
	if(evidencia==true){

		if(idTipoEvidencia=="-1"){
			mensaje+="\n*Tipo de evidencia";
			campos=true;
			arbolCompleto=false;
		}
	}

	var arbol={
			"idPregBD":preguntas[indice].idPregBD,
			"resp":res,
			"idPosibleRes":idPosibleRes,
			
			"evidencia":evidencia,
			"idTipoEvidencia":idTipoEvidencia,
			"evidenciaObliga":evidenciaObliga,
			"observaciones":observaciones,
			"acciones":acciones,
			"idPregSig":idPregSig,
			"etiquetaEvidencia":etiquetaEvidencia,
			"idArbolBD":0,
			"completo":true
			
			
	};

	var existeBD=false;
	var indiceArbol=0;
	for(var i=0;i<arbolDecision.length;i++){
		if(arbolDecision[i].idPregBD==arbol.idPregBD && arbolDecision[i].idPosibleRes==arbol.idPosibleRes){
			existeBD=true;
			arbol.idArbolBD=arbolDecision[i].idArbolBD;
			indiceArbol=i;
		}
		
	}
	if(campos){
		//alert("Hacen falta llenar los siguientes campos de la pregunta ["+(indice+1)+"] con respuesta ["+res+"]: \n"+mensaje);

		if($("#guardar_"+arbol.idPosibleRes+"_"+arbol.idPregBD).attr("class")!="arbolNoGuardado"){

			$("#guardar_"+arbol.idPosibleRes+"_"+arbol.idPregBD).removeClass("arbolGuardado").addClass("arbolNoGuardado");
		}
		if(existeBD){
			arbolDecision[indiceArbol].completo=false;
			//alert(JSON.stringify(arbolDecision[indiceArbol]));
			
		}
		/*
		for(var i=0;i<arbolDecision.length;i++){
			if(arbolDecision[i].idPregBD==arbol.idPregBD && arbolDecision[i].idPosibleRes==arbol.idPosibleRes){
				
				arbolDecision[i].completo=false;

				alert("ARBOL: \n"+JSON.stringify(arbolDecision[i]));
			}
			
		}*/

		//$("#guardar_"+arbol.idPosibleRes+"_"+arbol.idPregBD).addClass("arbolNoGuardado");
		
		return false;
	}
	if(existeBD){
		modificarArbolService(arbol);

		$("#guardar_"+arbol.idPosibleRes+"_"+arbol.idPregBD).removeClass("arbolNoGuardado").addClass("arbolGuardado");
	}else{
		//si no existe en la base de datos se agrega
		altaArbolService(arbol);
		//se incrementa el total de registros guardados en base de datos
		//regsArbolBD++;

		$("#guardar_"+arbol.idPosibleRes+"_"+arbol.idPregBD).removeClass("arbolNoGuardado").addClass("arbolGuardado");
	}
	regsArbolBD++;
	/*
	alert("SE AGREGO UNA OPCION:  \n"

			+"\nidPreg: "+arbol.idPregBD
			+"\nresp: "+arbol.resp
			+"\nidPosibleRes: "+arbol.idPosibleRes
			+"\nevidencia: "+arbol.evidencia
			+"\nidTipoEvidencia: "+arbol.idTipoEvidencia
			+"\nevidenciaObliga: "+arbol.evidenciaObliga
			+"\nobservaciones: "+arbol.observaciones
			+"\nacciones: "+arbol.acciones
			+"\nidPregSig: "+arbol.idPregSig
			+"\netiquetaEvidencia"+arbol.etiquetaEvidencia
			+"\nidArbolBD"+arbol.idArbolBD
		);*/
	
	return false;
	
}

function obtenerArbol(idPregBDx,idPosibleResp){
	var arbolTmp=null;
	
	for(var i=0;i<arbolDecision.length;i++){
		if((arbolDecision[i].idPregBD==idPregBDx) && (parseInt(arbolDecision[i].idPosibleRes)==parseInt(idPosibleResp))){
			arbolTmp=arbolDecision[i];
			//alert("datos: "+arbolDecision[i].idPregBD+" - "+arbolDecision[i].idPosibleRes+" IN "+idPregBDx+"-"+idPosibleResp);
		}
	}
	return arbolTmp;
}

function generarOpcionesArbol(resp,idPregBD,idPosibleResp) {
	


	var vistaPreviaRespuesta = "";
	
	var arbolTmp=obtenerArbol(idPregBD,idPosibleResp);
	/* si la respuesta es si */
	vistaPreviaRespuesta += "<tr>";
	vistaPreviaRespuesta += "<td style='text-align: left;'>"+resp+"</td>";
	
	if(arbolTmp==null){
		

		//***********************validacion campo de evidencia******************************
		vistaPreviaRespuesta += "<td>"+"<input id='evidencia_"+idPosibleResp+"_"+idPregBD+"'type='checkbox' onchange='return habilitarEvidencia("+idPosibleResp+","+idPregBD+");' />"+"</td>";

		//***********************validacion tipo de evidencia******************************
		
		vistaPreviaRespuesta += 
				"<td style='width:210px;'>"
				+"<div id='tipoEvidencia_"+idPosibleResp+"_"+idPregBD+"' class='caja'  >"
				+ "<span class='seleccionado' id='-1' >Seleccione ...</span>"
				+ "<ul class='listaselect'>"
				+ "<li value='-1'><a href='#'>Seleccione ...</a></li>"
				+ "<li value='21'><a href='#'>Imagen</a></li>"
				+"</ul>"
				+ "<span class='trianguloinf'></span>" + "</div>"
				+"</td>";

		//***********************validacion evidencia obligatoria******************************
		vistaPreviaRespuesta += "<td>"+"<input id='evidenciaObliga_"+idPosibleResp+"_"+idPregBD+"' type='checkbox' disabled />"+"</td>";

		//***********************validacion observaciones******************************
		vistaPreviaRespuesta += "<td>"+"<input id='observacion_"+idPosibleResp+"_"+idPregBD+"' type='checkbox' />"+"</td>";
		
		//***********************validacion acciones******************************
		vistaPreviaRespuesta += "<td>"+"<input id='acciones_"+idPosibleResp+"_"+idPregBD+"' type='checkbox' />"+"</td>";
		
		//***********************validacion pregunta siguiente******************************
		vistaPreviaRespuesta += 
				"<td style='width:210px;'>"
				+"<div id='pregSig_"+idPosibleResp+"_"+idPregBD+"' class='caja' >"
				+ "<span class='seleccionado' id='-1'>Seleccione ...</span>"
				+ "<ul class='listaselect'>"
				+ "<li value='-1'><a href='#'>Seleccione ...</a></li>";

		for(var i=0;i<preguntas.length;i++){
			for(var j=0;j<preguntas.length;j++){
				if(preguntas[i].idPregBD==idPregBD){

					if(j>preguntas[i].id){
						vistaPreviaRespuesta+= "<li value='"+(preguntas[j].id+1)+"'><a href='#'>Pregunta "+(preguntas[j].id+1)+"</a></li>";
					}
						
				}	
			}
			
			
		}

		vistaPreviaRespuesta+= "<li value='0'><a href='#'>Fin checklist</a></li>";
		vistaPreviaRespuesta+= 
			"</ul>"+ "<span class='trianguloinf'></span>" + "</div>"+"</td>";
			
		//***********************validacion etiqueta evidencia******************************
		vistaPreviaRespuesta += "<td>"+"<input type='text' style='width:100%;' placeholder='Agrega la etiqueta' id='etiquetaEvidencia_"+idPosibleResp+"_"+idPregBD+"'/>"+"</td>";

		//***********************boton guardar******************************
		//vistaPreviaRespuesta += "<td>"+"<button id='guardar_"+idPosibleResp+"_"+idPregBD+"' onclick='return agregarOpcionArbol("+idPosibleResp+","+idPregBD+");'>Guardar</button>"+"</td>";
		vistaPreviaRespuesta += "<td>"+"<button class='arbolNoGuardado' onclick='return false;' id='guardar_"+idPosibleResp+"_"+idPregBD+"' ></button>"+"</td>";
		
		
		vistaPreviaRespuesta += "</tr>";

	}else{
		/*
		alert("SE AGREGO UNA OPCION:  \n"

				+"\nidPreg: "+arbol.idPregBD
				+"\nresp: "+arbol.resp
				+"\nidPosibleRes: "+arbol.idPosibleRes
				+"\nevidencia: "+arbol.evidencia
				+"\nidTipoEvidencia: "+arbol.idTipoEvidencia
				+"\nevidenciaObliga: "+arbol.evidenciaObliga
				+"\nobservaciones: "+arbol.observaciones
				+"\nacciones: "+arbol.acciones
				+"\nidPregSig: "+arbol.idPregSig
				+"\netiquetaEvidencia"+arbol.etiquetaEvidencia
				+"\nidArbolBD"+arbol.idArbolBD
			);*/

		//***********************validacion campo de evidencia******************************
		if(parseInt(arbolTmp.evidencia)==1){
			vistaPreviaRespuesta += "<td>"+"<input id='evidencia_"+idPosibleResp+"_"+idPregBD+"'type='checkbox' onchange='return habilitarEvidencia("+idPosibleResp+","+idPregBD+");' checked />"+"</td>";
			
		}else{
			vistaPreviaRespuesta += "<td>"+"<input id='evidencia_"+idPosibleResp+"_"+idPregBD+"'type='checkbox' onchange='return habilitarEvidencia("+idPosibleResp+","+idPregBD+");' />"+"</td>";
		}
		//***********************validacion tipo de evidencia******************************
		vistaPreviaRespuesta += 
			"<td style='width:210px;'>"
			+"<div id='tipoEvidencia_"+idPosibleResp+"_"+idPregBD+"' class='caja'  >"
			+ "<span class='seleccionado' id='21' >Imagen</span>"
			+ "<ul class='listaselect'>"
			+ "<li value='-1'><a href='#'>Seleccione ...</a></li>"
			+ "<li value='21'><a href='#'>Imagen</a></li>"
			+"</ul>"
			+ "<span class='trianguloinf'></span>" + "</div>"
			+"</td>";
		//***********************validacion evidencia obligatoria******************************
		if(parseInt(arbolTmp.evidenciaObliga)==1){
			vistaPreviaRespuesta += "<td>"+"<input id='evidenciaObliga_"+idPosibleResp+"_"+idPregBD+"' type='checkbox' disabled checked />"+"</td>";	
		}else{
			vistaPreviaRespuesta += "<td>"+"<input id='evidenciaObliga_"+idPosibleResp+"_"+idPregBD+"' type='checkbox' disabled  />"+"</td>";
		}
		//***********************validacion observaciones******************************
		if(parseInt(arbolTmp.observaciones)==1){
			vistaPreviaRespuesta += "<td>"+"<input id='observacion_"+idPosibleResp+"_"+idPregBD+"' type='checkbox' checked />"+"</td>";
		}else{
			vistaPreviaRespuesta += "<td>"+"<input id='observacion_"+idPosibleResp+"_"+idPregBD+"' type='checkbox' />"+"</td>";
		}
		//***********************validacion acciones******************************
		if(parseInt(arbolTmp.acciones)==1){
			vistaPreviaRespuesta += "<td>"+"<input id='acciones_"+idPosibleResp+"_"+idPregBD+"' type='checkbox' checked />"+"</td>";
			
		}else{
			vistaPreviaRespuesta += "<td>"+"<input id='acciones_"+idPosibleResp+"_"+idPregBD+"' type='checkbox' />"+"</td>";
						
		}
		//***********************validacion pregunta siguiente******************************

		if(parseInt(arbolAdmin)==1){

			vistaPreviaRespuesta += 
				"<td style='width:210px;'>"
				+"<div id='pregSig_"+idPosibleResp+"_"+idPregBD+"' class='caja' >"
			
			if(parseInt(arbolTmp.idPregSig)==0){
				vistaPreviaRespuesta+="<span class='seleccionado' id='0'>Fin checklist</span>"
			}else{
				vistaPreviaRespuesta+="<span class='seleccionado' id='"+arbolTmp.idPregSig+"'>Pregunta "+arbolTmp.idPregSig+"</span>";
			}

			vistaPreviaRespuesta+=
				"<ul class='listaselect'>"
				+ "<li value='-1'><a href='#'>Seleccione ...</a></li>";
				
			for(var i=0;i<preguntas.length;i++){
				for(var j=0;j<preguntas.length;j++){
					if(preguntas[i].idPregBD==idPregBD){
		
						if(j>preguntas[i].id){
							vistaPreviaRespuesta+= "<li value='"+(preguntas[j].id+1)+"'><a href='#'>Pregunta "+(preguntas[j].id+1)+"</a></li>";
						}
							
					}	
				}
				
				
			}

			vistaPreviaRespuesta+= "<li value='0'><a href='#'>Fin checklist</a></li>";
			vistaPreviaRespuesta+= 
				"</ul>"+ "<span class='trianguloinf'></span>" + "</div>"+"</td>";
		}else{
			vistaPreviaRespuesta += 
				"<td style='width:210px;'>"
				+"<div id='pregSig_"+idPosibleResp+"_"+idPregBD+"' class='caja' >"
				+ "<span class='seleccionado' id='-1'>Seleccione ...</span>"
				+ "<ul class='listaselect'>"
				+ "<li value='-1'><a href='#'>Seleccione ...</a></li>";

			for(var i=0;i<preguntas.length;i++){
				for(var j=0;j<preguntas.length;j++){
					if(preguntas[i].idPregBD==idPregBD){
	
						if(j>preguntas[i].id){
							vistaPreviaRespuesta+= "<li value='"+(preguntas[j].id+1)+"'><a href='#'>Pregunta "+(preguntas[j].id+1)+"</a></li>";
						}
							
					}	
				}
				
				
			}
	
			vistaPreviaRespuesta+= "<li value='0'><a href='#'>Fin checklist</a></li>";
			vistaPreviaRespuesta+= 
				"</ul>"+ "<span class='trianguloinf'></span>" + "</div>"+"</td>";
				
		}
		
			

		//***********************validacion etiqueta evidencia******************************
		vistaPreviaRespuesta += "<td>"+"<input type='text' style='width:100%;' value='"+arbolTmp.etiquetaEvidencia+"' placeholder='Agrega la etiqueta'  id='etiquetaEvidencia_"+idPosibleResp+"_"+idPregBD+"'/>"+"</td>";

		//***********************boton guardar******************************
		//vistaPreviaRespuesta += "<td>"+"<button id='guardar_"+idPosibleResp+"_"+idPregBD+"' onclick='return agregarOpcionArbol("+idPosibleResp+","+idPregBD+");'>Guardar</button>"+"</td>";
		if(parseInt(arbolAdmin)==1){
			vistaPreviaRespuesta += "<td>"+"<button class='arbolGuardado' onclick='return false;' id='guardar_"+idPosibleResp+"_"+idPregBD+"' ></button>"+"</td>";
			
		}else{
			vistaPreviaRespuesta += "<td>"+"<button class='arbolNoGuardado' onclick='return false;' id='guardar_"+idPosibleResp+"_"+idPregBD+"' ></button>"+"</td>";
						
		}
		vistaPreviaRespuesta += "</tr>";

		
		
		
	}


	return vistaPreviaRespuesta;

}

function habilitarEvidencia(idPosibleResp,idPregBD){

	if(document.getElementById("evidencia_"+idPosibleResp+"_"+idPregBD).checked){

		document.getElementById("evidenciaObliga_"+idPosibleResp+"_"+idPregBD).disabled=false;
		$("#tipoEvidencia_"+idPosibleResp+"_"+idPregBD).parent().css('pointer-events', "auto");
		document.getElementById("etiquetaEvidencia_"+idPosibleResp+"_"+idPregBD).disabled=false;
		
		

	}else{

		$("#tipoEvidencia_"+idPosibleResp+"_"+idPregBD+" .seleccionado").attr("id",-1);
		$("#tipoEvidencia_"+idPosibleResp+"_"+idPregBD+" .seleccionado").text("Seleccione ...");

		document.getElementById("evidenciaObliga_"+idPosibleResp+"_"+idPregBD).checked=false;

		document.getElementById("evidenciaObliga_"+idPosibleResp+"_"+idPregBD).disabled=true;
		$("#tipoEvidencia_"+idPosibleResp+"_"+idPregBD).parent().css('pointer-events', "none");
		//se limpia la etiqueta de la evidencia
		$("#etiquetaEvidencia_"+idPosibleResp+"_"+idPregBD).val("");
		
		document.getElementById("etiquetaEvidencia_"+idPosibleResp+"_"+idPregBD).disabled=true;
		



	}
	
	return false;
}
function quitarEventoCombo(idPosibleResp,idPregBD){
	

	document.getElementById("evidenciaObliga_"+idPosibleResp+"_"+idPregBD).disabled=true;

	$("#tipoEvidencia_"+idPosibleResp+"_"+idPregBD).parent().css('pointer-events', "none");
}
function quitarEventosCombo(){
	
	for(var i=0;i<preguntas.length;i++){
		for(var j=0;j<posiblesRespuestas.length;j++){
			if(preguntas[i].idTipo==posiblesRespuestas[j].idTipo){

				quitarEventoCombo(posiblesRespuestas[j].idPosibleRespuesta,preguntas[i].idPregBD);
			}
		}

		
	}
	
}

function asignarEventosCombo(){
	/*se asignan los eventos para los combos agregados en el div dinamico del arbol de deciciones del checklist*/
	$(".caja").off();

		
	$(".caja").hover(
			
			function(e){

				asignarEventosCombo(); 
				
				var lista = $(this).find("ul");
				var triangulo = $(this).find("span:last-child");
				e.preventDefault();
				
				$(this).find("ul").toggle();
				if (lista.is(":hidden") ) {
					triangulo.removeClass("triangulosup").addClass("trianguloinf");
					//lista.hide();

					triangulo.parent().css('z-index', 0);
				} else {
					//lista.hide();
					triangulo.removeClass("trianguloinf").addClass("triangulosup");
					triangulo.parent().css('z-index', 3000);
				}
				
			}
		);
		
		$(".caja").on("click","li",
				function(e){
					
					var texto = $(this).text();
					var seleccionado = $(this).parent().prev(); 
					//var seleccionado = $(this).parent().find(".seleccionado"); 
					
					
					var valor=$(this).val();
					
					var lista = $(this).closest("ul");
					var triangulo = $(this).parent().next();
					//var lista = $(this).find("ul").toggle();

					//var lista = $(this).parents("li").find(".listaselect");
					//var triangulo = $(this).find("span:last-child");
					//var oculto = $(this).parents("li").find(".caja").attr("id");
					
					e.preventDefault();
					e.stopPropagation();
					seleccionado.text(texto);
					//alert("DIV: "+oculto);
					
					//alert("VALOR: "+valor);
					//seleccionado.val(valor);
					seleccionado.attr("id",valor);
					lista.hide();
					$(".caja").unbind('mouseleave');
					triangulo.removeClass("triangulosup").addClass("trianguloinf");

				


					
				}
		);
		
}



function altaArbolService(arbolX) {
	

	// http://localhost:8080../checklistServices/arbolDesicion.json?estatusEvidencia=<?>&idCheck=<?>&idPreg=<?>&idOrdenCheckRes=<?>&setRes=<?>&reqAccion=<?>&commit=<?>&reqObs=<?>&reqOblig=<?>&etiquetaEvidencia=<?>
	
	//alert("http://"+urlServer + 
	//	'../checklistServices/arbolDesicionService.json?estatusEvidencia='+arbolX.evidencia+'&idCheck='+idChecklist+'&idPreg='+arbolX.idPregBD+'&idOrdenCheckRes='+arbolX.idPregSig+'&setRes='+arbolX.idPosibleRes+'&reqAccion='+arbolX.acciones+'&commit=1&reqObs='+arbolX.observaciones+'&reqOblig='+arbolX.evidenciaObliga+'&etiquetaEvidencia='+arbolX.etiquetaEvidencia)
	$
	.ajax({
		type : "GET",
		url :encodeURI("http://"+urlServer + 
				'../checklistServices/arbolDesicionTempService.json?idArbol=0&estatusEvidencia='+arbolX.evidencia+'&idCheck='+idChecklist+'&idPreg='+arbolX.idPregBD+'&idOrdenCheckRes='+arbolX.idPregSig+'&setRes='+arbolX.idPosibleRes+'&reqAccion='+arbolX.acciones+'&commit=1&reqObs='+arbolX.observaciones+'&reqOblig='+arbolX.evidenciaObliga+'&etiquetaEvidencia='+arbolX.etiquetaEvidencia)+'&numRevision='+numVersion+'&tipoCambio=I',
 
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			//alert("ID PREG: "+json);
			
			if(json	!=	0){
				//alert("Se agrego correctamente la información");

				arbolX.idArbolBD=json;
				
				arbolDecision.push(arbolX);
				
				//alert("id arbol insertado: "+arbolDecision[arbolDecision.length-1].idArbolBD);
				
				
					
			}else{
				alert('Algo paso al agregar la información!');
			
			}
			

		},
		error : function() {

			alert('Algo paso al agregar la información');
		
			
		},

		async: false
	});
	
}


function modificarArbolService(arbolX) {
	// http://localhost:8080../checklistServices/updateArbolDesicion.json?idArbolDesicion=<?>&estatusEvidencia=<?>&idCheck=<?>&idPreg=<?>&idOrdenCheckRes=<?>&setRes=<?>&reqAccion=<?>&reqObs=<?>&reqOblig=<?>&etiquetaEvidencia=<?>
	//alert("http://"+urlServer + 
	//	'../checklistServices/updateArbolDesicion.json?idArbolDesicion='+arbolX.idArbolBD+'&estatusEvidencia='+arbolX.evidencia+'&idCheck='+idChecklist+'&idPreg='+arbolX.idPregBD+'&idOrdenCheckRes='+arbolX.idPregSig+'&setRes='+arbolX.idPosibleRes+'&reqAccion='+arbolX.acciones+'&commit=1&reqObs='+arbolX.observaciones+'&reqOblig='+arbolX.evidenciaObliga+'&etiquetaEvidencia='+arbolX.etiquetaEvidencia)
	$
	.ajax({
		type : "GET",
		url :encodeURI('http://'+urlServer+'../checklistServices/arbolDesicionTempService.json?idArbol='+arbolX.idArbolBD+'&estatusEvidencia='+arbolX.evidencia+'&idCheck='+idChecklist+'&idPreg='+arbolX.idPregBD+'&idOrdenCheckRes='+arbolX.idPregSig+'&setRes='+arbolX.idPosibleRes+'&reqAccion='+arbolX.acciones+'&commit=1&reqObs='+arbolX.observaciones+'&reqOblig='+arbolX.evidenciaObliga+'&etiquetaEvidencia='+arbolX.etiquetaEvidencia)+'&numRevision='+numVersion+'&tipoCambio=U',
		 
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			
			if(json != 0 ){
				//alert("Se actualizo correctamente la información");
					
			}else{
				alert('Algo paso al actualizar la información');
			
			}
			

		},
		error : function() {

			alert('Algo paso al actualizar la información');
		
			
		},

		async: false
	});
	
}




$(document).ready(
		function() {
			$("#continuar").click(
					function() {
						
						//alert("regs bd: "+arbolDecision.length+" total arbol: "+totalArbol);
						//if(arbolDecision.length==totalArbol){
						if(regsArbolBD==totalArbol){
							actualizaEstadoCheck (27);
							return true;
						}else{
							alert("No has guardado todas las opciones. Restan "+(totalArbol-regsArbolBD)+" por guardar");
							return false;
						}
						
						return true;
					})
		});




function cargarArbolBDService() {
	$
	.ajax({
		type : "GET",
		url : encodeURI("http://"+urlServer + '../consultaChecklistService/getArbolDesicionService.json?idCheck='+idChecklist),
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var i = 0;
			//alert(jsonC);
			while (json[i] != null) {
				//buscarPosibleResp(json[i].respuesta);
				//alert("POSIBLE: "+json[i].respuesta);
				
				var arbolTmp={
						"idPregBD":json[i].idPregunta,
						"resp":buscarPosibleResp(json[i].respuesta),
						"idPosibleRes":json[i].respuesta,	
						"evidencia":json[i].estatusEvidencia,
						"idTipoEvidencia":21,
						"evidenciaObliga":json[i].reqOblig,
						"observaciones":json[i].reqObservacion,
						"acciones":json[i].reqAccion,
						"idPregSig":json[i].ordenCheckRespuesta,
						"etiquetaEvidencia":json[i].descEvidencia==null?"":json[i].descEvidencia,
						"idArbolBD":json[i].idArbolDesicion
												
						
				};
				if(arbolAdmin==1){
					regsArbolBD++;
				}
				//document.forms.formulario.tipo.options[i+1]  = new Option(json[i].nombreCheck, json[i].idChecklist);
				arbolDecision.push(arbolTmp);
				i++;
				//alert("REG "+i+"--> id: "+posiblesRespuestas[posiblesRespuestas.length-1].id+"idTipo: "+posiblesRespuestas[posiblesRespuestas.length-1].idTipo+" respuesta: "+posiblesRespuestas[posiblesRespuestas.length-1].respuesta);
			}

		},
		error : function() {
			alert('Algo paso al cargar el arbol');
		},
		async: false
	});
}
function buscarPosibleResp(idPosResp){

		
	var respuesta="";
	for(var i=0;i<posiblesRespuestas.length;i++){
		//alert("RESP: "+idPosResp);
		if(idPosResp==posiblesRespuestas[i].idPosibleRespuesta){
			respuesta=posiblesRespuestas[i].respuesta;
		}
	}
	
	return respuesta;
} 




function cargarPreguntaPreviasService() {
	 
	 
	$.ajax({
		type : "GET",
		url : encodeURI("http://"+urlServer + '../consultaChecklistService/getCheckPreguntaService.json?idCheck='+idChecklist),
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {
			var jsonC = JSON.stringify(json);
			var i = 0;
			while (json[i] != null) {
				var pregTmp={
						"id":preguntas.length,
						"descripcion":json[i].pregunta,
						"idTipo":json[i].idTipoPregunta,
						"tipo":json[i].descTipo,
						"moduloId":json[i].idModulo,
						"modulo":json[i].nombreModulo,
						"idPregBD":json[i].idPregunta
						};
				//document.forms.formulario.tipo.options[i+1]  = new Option(json[i].nombreCheck, json[i].idChecklist);
				preguntas.push(pregTmp);
				i++;
			 
			

				//alert("REG "+i+"--> id: "+posiblesRespuestas[posiblesRespuestas.length-1].id+"idTipo: "+posiblesRespuestas[posiblesRespuestas.length-1].idTipo+" respuesta: "+posiblesRespuestas[posiblesRespuestas.length-1].respuesta);
			}

			//alert("preguntas cargadas: "+preguntas.length);
			//alert("se ha cargado el catalogo!, total de regs: "+posiblesRespuestas.length);
		},
		error : function() {
			alert('Algo paso cargar las preguntas previas');
		},
		async: false
	});
}


function actualizaEstadoCheck (estado){
	 $.ajax({
	 		type : "GET",
	 		url : encodeURI("http://"+urlServer + '../checklistServices/updateEstadoCheck.json?idCheck='+idChecklist+'&estado='+estado),
	 		contentType : "application/json; charset=utf-8",
	 		dataType : "json",
	 		paramName : "tagName",
	 		delimiter : ",",
	 		success : function(json) {
	 			alert ("Estado Check actualizado Correctamente");
	 		},
	 		error : function() {
	 			alert('Algo paso al actualizar el estado del check');
	 		},
	 		async: false
	 	});
}

