package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.ProyectFaseTransfDTO;


public interface ProyectFaseTransfDAO {

//proyecto
	public int insertaProyec(ProyectFaseTransfDTO bean)throws Exception;
	
	public boolean eliminaProyec(String idFase)throws Exception;
	
	public List<ProyectFaseTransfDTO> obtieneDatosProyec(String proyecto) throws Exception; 
	
	public List<ProyectFaseTransfDTO> obtieneInfoProyec() throws Exception; 
	
	public boolean actualizaProyec(ProyectFaseTransfDTO bean)throws Exception;
	
	public boolean actualizaProyecNvo(ProyectFaseTransfDTO bean)throws Exception;

	
	}