package com.gruposalinas.checklist.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.gruposalinas.checklist.business.SchedulerBI;
import com.gruposalinas.checklist.business.TareasBI;
import com.gruposalinas.checklist.domain.Login;
import com.gruposalinas.checklist.domain.SchedulerDTO;
import com.gruposalinas.checklist.util.UtilDate;

@Controller
public class Scheduler {

	private static final Logger logger = LogManager.getLogger(Scheduler.class);

	@Autowired
	private SchedulerBI schedulerBI;
	@Autowired
	private TareasBI tareasBI;

	/* FORMS 2 METHODS */
	@SuppressWarnings("static-access")
	@RequestMapping(value = "central/schedulerForm.htm", method = RequestMethod.GET)
	public ModelAndView task(HttpServletRequest request) {

		if (request.getQueryString() != null) {
			if (request.getParameter("valida").equals("true")) {
				ModelAndView mv = new ModelAndView("schedulerView", "command", new SchedulerDTO());
				try {
					mv.addObject("ipHost",InetAddress.getLocalHost().getHostAddress());
					mv.addObject("fechaHost",new UtilDate().getSysDate("HH:mm:ss"));
				} catch (UnknownHostException e) {
					logger.info("Algo ocurrió... en IP Host", e);
				}
				mv.addObject("listaTareas", tareasBI.obtieneTareas());
				return mv;
			} else {
				Login login = new Login();
				return new ModelAndView("login", "command", login);
			}
		} else {
			Login login = new Login();
			return new ModelAndView("login", "command", login);
		}
	}

	@RequestMapping(value = "central/schedulerController.htm", method = RequestMethod.POST)
	public String showForm(@ModelAttribute(value = "schedulerView") SchedulerDTO scheduler, ModelMap model,
			HttpServletRequest request) throws UnknownHostException {

		if (request.getParameter("envia").equals("update")) {
			model.addAttribute("command", new SchedulerDTO());
			try {
				schedulerBI.task(scheduler, request);
			} catch (Exception e) {
				logger.info("Algo ocurrió...", e);
			}

		} else if (request.getParameter("envia").equals("delete")) {
			String identifier = scheduler.getId();
			try {
				schedulerBI.eliminaTask(Integer.parseInt(request.getParameter("id" + identifier)),
						request.getParameter("idTarea" + identifier));
			} catch (Exception e) {
				logger.info("Algo ocurrió...", e);
			}

		} else if (request.getParameter("envia").equals("guarda")) {
			model.addAttribute("command", new SchedulerDTO());
			try {
				schedulerBI.newTask(scheduler, request);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return "redirect:/central/schedulerForm.htm?valida=true";

	}
}