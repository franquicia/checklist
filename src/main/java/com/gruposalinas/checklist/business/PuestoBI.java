package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.PuestoDAO;
import com.gruposalinas.checklist.domain.PuestoDTO;
import com.gruposalinas.checklist.domain.PuestoUsuarioDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class PuestoBI {

	private static Logger logger = LogManager.getLogger(PuestoBI.class);
	
	@Autowired
	PuestoDAO puestoDAO;
	
	List<PuestoDTO> listaPuesto = null;
	
	List<PuestoDTO> listaPuestos = null;
	List<PuestoUsuarioDTO> listaPuestosU = null;
	
	public List<PuestoDTO> obtienePuesto(){
		try{
			listaPuestos = puestoDAO.obtienePuesto();
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Puesto");
					
		}
				
		return listaPuestos;
	}
	public List<PuestoDTO> obtienePuestoGeo(){
		try{
			listaPuestos = puestoDAO.obtienePuestoGeo();
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Puesto");
					
		}
				
		return listaPuestos;
	}
	
	public List<PuestoDTO> obtienePuesto(int idPuesto){
		try{
			listaPuesto = puestoDAO.obtienePuesto(idPuesto);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Puesto");
						
		}
				
		return listaPuesto;
	}
	
	
	public int insertaPuesto(PuestoDTO puesto) {
		int idPuesto=0;
		try{
			idPuesto = puestoDAO.insertaPuesto(puesto);
		}
		catch (Exception e) {
			logger.info("No fue posible insertar el Puesto");
			
		}
		
		return idPuesto;	
	}
	
	public boolean actualizaPuesto(PuestoDTO puesto){
		boolean respuesta = false;
		try{
			respuesta = puestoDAO.actualizaPuesto(puesto);
		} 
		catch (Exception e) {
			logger.info("No fue posible actualizar el Puesto");
			
		}
							
		return respuesta;
	}
	
	public boolean eliminaPuesto(int idPuesto){
		boolean respuesta = false;
		try{
			respuesta = puestoDAO.eliminaPuesto(idPuesto);
		}
		catch(Exception e){
			logger.info("No fue posible borrar el Puesto");
			
		}
		
		return respuesta;
	}
	
	public List<PuestoUsuarioDTO> obtienePuestoUsuario(int idUsuario){
		try{
			//System.out.println(idUsuario);
			listaPuestosU = puestoDAO.obtienePuestoUsuario(idUsuario);
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Puesto");
					
		}
				
		return listaPuestosU;
	}
}
