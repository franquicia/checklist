package com.gruposalinas.checklist.domain;

public class ChecklistModificacionesDTO {
	private int idChecklist;
	private String nombreCheck;
	private int idHorario;
	private int idTipoChecklist;
	private String fechaInicioCheck;
	private String fechaFinCheck;
	private String modificacionCheck;
	private int idPregunta;
	private String pregunta;
	private String modificacionPreg;	
	private int idModulo;
	private String nombreModulo;
	private int idModuloPadre;
	private String nombreModuloPadre;
	private String modificacionModulo;
	private int ordenCheck;
	private String modificacionCheckPreg;
	private int idArbol;
	private int idPosible;
	private String posibleRespuesta;
	private int estatusEvidencia;
	private int siguientePregunta;
	private int requiereAccion;
	private int requiereObsv;
	private int evidenciaObligatoria;
	private String etiquetaEvidencia;
	private String modificacionArbol;
	private int idTipoPreg;
	
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public String getNombreCheck() {
		return nombreCheck;
	}
	public void setNombreCheck(String nombreCheck) {
		this.nombreCheck = nombreCheck;
	}
	public int getIdHorario() {
		return idHorario;
	}
	public void setIdHorario(int idHorario) {
		this.idHorario = idHorario;
	}
	public int getIdTipoChecklist() {
		return idTipoChecklist;
	}
	public void setIdTipoChecklist(int idTipoChecklist) {
		this.idTipoChecklist = idTipoChecklist;
	}
	public String getFechaInicioCheck() {
		return fechaInicioCheck;
	}
	public void setFechaInicioCheck(String fechaInicioCheck) {
		this.fechaInicioCheck = fechaInicioCheck;
	}
	public String getFechaFinCheck() {
		return fechaFinCheck;
	}
	public void setFechaFinCheck(String fechaFinCheck) {
		this.fechaFinCheck = fechaFinCheck;
	}
	public String getModificacionCheck() {
		return modificacionCheck;
	}
	public void setModificacionCheck(String modificacionCheck) {
		this.modificacionCheck = modificacionCheck;
	}
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public String getPregunta() {
		return pregunta;
	}
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}
	public String getModificacionPreg() {
		return modificacionPreg;
	}
	public void setModificacionPreg(String modificacionPreg) {
		this.modificacionPreg = modificacionPreg;
	}
	public int getIdModulo() {
		return idModulo;
	}
	public void setIdModulo(int idModulo) {
		this.idModulo = idModulo;
	}
	public String getNombreModulo() {
		return nombreModulo;
	}
	public void setNombreModulo(String nombreModulo) {
		this.nombreModulo = nombreModulo;
	}
	public int getIdModuloPadre() {
		return idModuloPadre;
	}
	public void setIdModuloPadre(int idModuloPadre) {
		this.idModuloPadre = idModuloPadre;
	}
	public String getNombreModuloPadre() {
		return nombreModuloPadre;
	}
	public void setNombreModuloPadre(String nombreModuloPadre) {
		this.nombreModuloPadre = nombreModuloPadre;
	}
	public String getModificacionModulo() {
		return modificacionModulo;
	}
	public void setModificacionModulo(String modificacionModulo) {
		this.modificacionModulo = modificacionModulo;
	}
	public int getOrdenCheck() {
		return ordenCheck;
	}
	public void setOrdenCheck(int ordenCheck) {
		this.ordenCheck = ordenCheck;
	}
	public String getModificacionCheckPreg() {
		return modificacionCheckPreg;
	}
	public void setModificacionCheckPreg(String modificacionCheckPreg) {
		this.modificacionCheckPreg = modificacionCheckPreg;
	}
	public int getIdArbol() {
		return idArbol;
	}
	public void setIdArbol(int idArbol) {
		this.idArbol = idArbol;
	}
	public int getIdPosible() {
		return idPosible;
	}
	public void setIdPosible(int idPosible) {
		this.idPosible = idPosible;
	}
	public String getPosibleRespuesta() {
		return posibleRespuesta;
	}
	public void setPosibleRespuesta(String posibleRespuesta) {
		this.posibleRespuesta = posibleRespuesta;
	}
	public int getEstatusEvidencia() {
		return estatusEvidencia;
	}
	public void setEstatusEvidencia(int estatusEvidencia) {
		this.estatusEvidencia = estatusEvidencia;
	}
	public int getSiguientePregunta() {
		return siguientePregunta;
	}
	public void setSiguientePregunta(int siguientePregunta) {
		this.siguientePregunta = siguientePregunta;
	}
	public int getRequiereAccion() {
		return requiereAccion;
	}
	public void setRequiereAccion(int requiereAccion) {
		this.requiereAccion = requiereAccion;
	}
	public int getRequiereObsv() {
		return requiereObsv;
	}
	public void setRequiereObsv(int requiereObsv) {
		this.requiereObsv = requiereObsv;
	}
	public int getEvidenciaObligatoria() {
		return evidenciaObligatoria;
	}
	public void setEvidenciaObligatoria(int evidenciaObligatoria) {
		this.evidenciaObligatoria = evidenciaObligatoria;
	}
	public String getEtiquetaEvidencia() {
		return etiquetaEvidencia;
	}
	public void setEtiquetaEvidencia(String etiquetaEvidencia) {
		this.etiquetaEvidencia = etiquetaEvidencia;
	}
	public String getModificacionArbol() {
		return modificacionArbol;
	}
	public void setModificacionArbol(String modificaiconArbol) {
		this.modificacionArbol = modificaiconArbol;
	}
	public int getIdTipoPreg() {
		return idTipoPreg;
	}
	public void setIdTipoPreg(int idTipoPreg) {
		this.idTipoPreg = idTipoPreg;
	}
}
