package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.CompromisoDTO;
import com.gruposalinas.checklist.mappers.CompromisoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class CompromisoDAOImpl extends DefaultDAO implements CompromisoDAO {

    private static Logger logger = LogManager.getLogger(CompromisoDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneCompromiso;
    DefaultJdbcCall jdbcInsertaCompromiso;
    DefaultJdbcCall jdbcActualizaCompromiso;
    DefaultJdbcCall jdbcEliminaCompromiso;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_COMP")
                .withProcedureName("SP_SEL_G_COMP")
                .returningResultSet("RCL_COMP", new CompromisoRowMapper());

        jdbcObtieneCompromiso = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_COMP")
                .withProcedureName("SP_SEL_COMP")
                .returningResultSet("RCL_COMP", new CompromisoRowMapper());

        jdbcInsertaCompromiso = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_COMP")
                .withProcedureName("SP_INS_COMP");

        jdbcActualizaCompromiso = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_COMP")
                .withProcedureName("SP_ACT_COMP");

        jdbcEliminaCompromiso = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_COMP")
                .withProcedureName("SP_DEL_COMP");
    }

    @SuppressWarnings("unchecked")
    public List<CompromisoDTO> obtieneCompromiso() throws Exception {

        Map<String, Object> out = null;
        List<CompromisoDTO> listaCompromiso = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funcion ejecutada: {checklist.PA_ADM_COMP.SP_SEL_G_COMP}");

        listaCompromiso = (List<CompromisoDTO>) out.get("RCL_COMP");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener los Compromisos");
        } else {
            return listaCompromiso;
        }

        return listaCompromiso;
    }

    @SuppressWarnings("unchecked")
    public List<CompromisoDTO> obtieneCompromiso(int idCompromiso) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<CompromisoDTO> listaCompromiso = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_COMP", idCompromiso);

        out = jdbcObtieneCompromiso.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_COMP.SP_SEL_COMP}");

        listaCompromiso = (List<CompromisoDTO>) out.get("RCL_COMP");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener el Compromiso con id(" + idCompromiso + ")");
        } else {
            return listaCompromiso;
        }

        return null;
    }

    public int insertaCompromiso(CompromisoDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idCompromiso = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_RESP", bean.getIdRespuesta())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_FECHA_COMP", bean.getFechaCompromiso())
                .addValue("PA_IDRESPONSABLE", bean.getIdResponsable())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcInsertaCompromiso.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_COMP.SP_INS_COMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_FIID_COMPROMISO");
        idCompromiso = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar el Compromiso");
        } else {
            return idCompromiso;
        }

        return idCompromiso;
    }

    public boolean actualizaCompromiso(CompromisoDTO bean) throws Exception {

        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_COMP", bean.getIdCompromiso())
                .addValue("PA_FIID_RESP", bean.getIdRespuesta())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_FECHA_COMP", bean.getFechaCompromiso())
                .addValue("PA_IDRESPONSABLE", bean.getIdResponsable());

        out = jdbcActualizaCompromiso.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_COMP.SP_ACT_COMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el Compromiso  id( " + bean.getIdCompromiso() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaCompromiso(int idCompromiso) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_COMP", idCompromiso);

        out = jdbcEliminaCompromiso.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_COMP.SP_DEL_COMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el Compromiso id(" + idCompromiso + ")");
        } else {
            return true;
        }

        return false;
    }
}
