package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ListaAsignChkDTO;
import com.gruposalinas.checklist.mappers.ListAsignChkRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ListaChkAsignDAOImpl extends DefaultDAO implements ListaChkAsignDAO {

    private static Logger logger = LogManager.getLogger(ChecklistDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneAsignChk;

    public void init() {

        jdbcObtieneAsignChk = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACONSULTASIGN")
                .withProcedureName("SPGETASIGNCH")
                .returningResultSet("PA_CONSULTA", new ListAsignChkRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ListaAsignChkDTO> getListaAsignaciones(int idUsuario) throws Exception {

        Map<String, Object> out = null;

        List<ListaAsignChkDTO> listaChecklist = null;

        int ejecuccion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_USUARIO", idUsuario);

        logger.info("");
        out = jdbcObtieneAsignChk.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PACONSULTASIGN.SPGETASIGNCH}");

        listaChecklist = (List<ListaAsignChkDTO>) out.get("PA_CONSULTA");

        /*BigDecimal valueReturn = (BigDecimal) out.get("PA_EJECUCCION");
		ejecuccion = valueReturn.intValue();

		if(ejecuccion == 0){
			logger.info("Algo ocurrió al obtener los checklist para modo Offline ");
		}else{

		}*/
        return listaChecklist;
    }

}
