package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.FiltrosCecoDTO;
import com.gruposalinas.checklist.domain.NegocioDTO;
import com.gruposalinas.checklist.domain.PaisDTO;
import com.gruposalinas.checklist.mappers.ChecklistComboRowMapper;
import com.gruposalinas.checklist.mappers.FiltrosCecosRowMapper;
import com.gruposalinas.checklist.mappers.FiltrosNegocioRowMapper;
import com.gruposalinas.checklist.mappers.FiltrosPaisRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class FiltrosCecoDAOImpl extends DefaultDAO implements FiltrosCecoDAO {

    private Logger logger = LogManager.getLogger(FiltrosCecoDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneFiltros;
    private DefaultJdbcCall jdbcObtieneCecos;
    private DefaultJdbcCall jdbcObtienePaises;
    private DefaultJdbcCall jdbcObtieneCheck;
    private DefaultJdbcCall jdbcObtieneTerritorios;
    private DefaultJdbcCall jdbcObtienePaisesN;

    public void init() {

        jdbcObtieneFiltros = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAFILTROSREP")
                .withProcedureName("SPCONSULTACECOS")
                .returningResultSet("RCL_TERRITORIO", new FiltrosCecosRowMapper())
                .returningResultSet("RCL_ZONA", new FiltrosCecosRowMapper())
                .returningResultSet("RCL_REGION", new FiltrosCecosRowMapper())
                .returningResultSet("RCL_PAIS", new FiltrosPaisRowMapper())
                .returningResultSet("RCL_NEGOCIO", new FiltrosNegocioRowMapper());

        jdbcObtieneCecos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAFILTROSREP")
                .withProcedureName("SPOBTIENECECOS")
                .returningResultSet("RCL_CONSULTA", new FiltrosCecosRowMapper());

        jdbcObtienePaises = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAFILTROSREP")
                .withProcedureName("SPOBTIENEPAISES")
                .returningResultSet("RCL_PAISES", new FiltrosPaisRowMapper());

        jdbcObtienePaisesN = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAFILTROSREP")
                .withProcedureName("SPOBTPAISES")
                .returningResultSet("RCL_PAISES", new FiltrosPaisRowMapper());

        jdbcObtieneTerritorios = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAFILTROSREP")
                .withProcedureName("SPOBTIENETERR")
                .returningResultSet("RCL_TERRITORIOS", new FiltrosCecosRowMapper());

        jdbcObtieneCheck = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAFILTROSREP")
                .withProcedureName("SPOBTIENECHECKLISTS")
                .returningResultSet("RCL_CHECKLIST", new ChecklistComboRowMapper());
    }

    @Override
    public Map<String, Object> obtieneFiltros(int idUsuario) throws Exception {

        Map<String, Object> out = null;

        Map<String, Object> filtros = new HashMap<String, Object>();
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USUARIO", idUsuario);

        out = jdbcObtieneFiltros.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAFILTROSREP.SPCONSULTACECOS}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 1) {

            filtros.put("pais", out.get("RCL_PAIS"));
            filtros.put("negocio", out.get("RCL_NEGOCIO"));
            filtros.put("territorio", out.get("RCL_TERRITORIO"));
            filtros.put("zona", out.get("RCL_ZONA"));
            filtros.put("region", out.get("RCL_REGION"));

        } else {

            filtros.put("pais", new ArrayList<PaisDTO>());
            filtros.put("negocio", new ArrayList<NegocioDTO>());
            filtros.put("territorio", new ArrayList<FiltrosCecoDTO>());
            filtros.put("zona", new ArrayList<FiltrosCecoDTO>());
            filtros.put("region", new ArrayList<FiltrosCecoDTO>());
        }

        return filtros;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<FiltrosCecoDTO> obtieneCecos(int idCeco, int tipoBusqueda) throws Exception {

        Map<String, Object> out = null;
        List<FiltrosCecoDTO> cecos = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CECO", idCeco).addValue("PA_BUSQUEDA", tipoBusqueda);

        out = jdbcObtieneCecos.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAFILTROSREP.SPOBTIENECECOS}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        cecos = (List<FiltrosCecoDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {

            logger.info("Algo ocurrió al consultar los cecos ");

        }

        return cecos;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PaisDTO> obtienePaises(int negocio) throws Exception {

        Map<String, Object> out = null;
        List<PaisDTO> paises = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NEGOCIO", negocio);

        out = jdbcObtienePaises.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAFILTROSREP.SPOBTIENEPAISES}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        paises = (List<PaisDTO>) out.get("RCL_PAISES");

        if (ejecucion == 0) {

            logger.info("Algo ocurrió al consultar los paises ");

        }

        return paises;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<FiltrosCecoDTO> obtieneTerritorios(int pais, int negocio) throws Exception {
        Map<String, Object> out = null;
        List<FiltrosCecoDTO> cecos = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_PAIS", pais).addValue("PA_NEGOCIO", negocio);

        out = jdbcObtieneTerritorios.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAFILTROSREP.SPOBTIENETERR}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        cecos = (List<FiltrosCecoDTO>) out.get("RCL_TERRITORIOS");

        if (ejecucion == 0) {

            logger.info("Algo ocurrió al consultar los cecos ");

        }

        return cecos;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ChecklistDTO> obtieneChecklist(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistDTO> checklist = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USUARIO", idUsuario);

        out = jdbcObtieneCheck.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAFILTROSREP.SPOBTIENECHECKLISTS}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        checklist = (List<ChecklistDTO>) out.get("RCL_CHECKLIST");

        if (ejecucion == 0) {

            logger.info("Algo ocurrió al consultar los cecos ");

        }

        return checklist;
    }

    @Override
    public List<PaisDTO> obtienePaises(int negocio, int idUsuario) throws Exception {
        Map<String, Object> out = null;
        List<PaisDTO> paises = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NEGOCIO", negocio).addValue("PA_IDUSUARIO", idUsuario);

        out = jdbcObtienePaisesN.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAFILTROSREP.SPOBTPAISES}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        paises = (List<PaisDTO>) out.get("RCL_PAISES");

        if (ejecucion == 0) {

            logger.info("Algo ocurrió al consultar los paises ");

        }

        return paises;
    }

}
