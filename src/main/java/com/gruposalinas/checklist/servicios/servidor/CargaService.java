package com.gruposalinas.checklist.servicios.servidor;

import java.io.UnsupportedEncodingException;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.ArbolDecisionBI;
import com.gruposalinas.checklist.business.AsignaExpBI;
import com.gruposalinas.checklist.business.AsignaTransfBI;
import com.gruposalinas.checklist.business.AsignacionBI;
import com.gruposalinas.checklist.business.AsignacionNewBI;
import com.gruposalinas.checklist.business.CargaCheckListBI;
import com.gruposalinas.checklist.business.CecoBI;
import com.gruposalinas.checklist.business.ChecklistPreguntaBI;
import com.gruposalinas.checklist.business.ChecklistTBI;
import com.gruposalinas.checklist.business.DistribCecoBI;
import com.gruposalinas.checklist.business.EliminaCheckListBI;
import com.gruposalinas.checklist.business.HallazgosExpBI;
import com.gruposalinas.checklist.business.HallazgosTransfBI;
import com.gruposalinas.checklist.business.PerfilBI;
import com.gruposalinas.checklist.business.PerfilesNuevoBI;
import com.gruposalinas.checklist.business.ReporteBI;
import com.gruposalinas.checklist.business.SucUbicacionBI;
import com.gruposalinas.checklist.business.SucursalBI;
import com.gruposalinas.checklist.business.TruncTableTokenBI;
import com.gruposalinas.checklist.business.Usuario_ABI;
import com.gruposalinas.checklist.dao.AsignaExpDAO;
import com.gruposalinas.checklist.dao.SucUbicacionDAO;
import com.gruposalinas.checklist.domain.ActivarCheckListDTO;
import com.gruposalinas.checklist.domain.EliminaCheckListDTO;
import com.gruposalinas.checklist.domain.TruncTableTokenDTO;
import com.gruposalinas.checklist.domain.CargaCheckListDTO;
import com.gruposalinas.checklist.domain.DetalleProtoDTO;
import com.gruposalinas.checklist.domain.DistribCecoDTO;
import com.gruposalinas.checklist.business.ActivarCheckListBI;
import com.gruposalinas.checklist.business.AsigPuntoContactoBI;
import com.gruposalinas.checklist.business.ComentariosRepAsgBI;
import com.gruposalinas.checklist.business.DepuracionTblBI;
import com.gruposalinas.checklist.resources.FRQAuthInterceptor;
import java.util.ArrayList;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/cargaServices")
public class CargaService {

    @Autowired
    Usuario_ABI usuarioabi;
    @Autowired
    SucursalBI sucursalbi;
    @Autowired
    CecoBI cecobi;
    @Autowired
    PerfilBI perfilbi;
    @Autowired
    ArbolDecisionBI arboldecisionbi;
    @Autowired
    ChecklistPreguntaBI checklistPreguntaBI;
    @Autowired
    ChecklistTBI checkTBI;
    @Autowired
    AsignacionBI asignacionBI;
    @Autowired
    AsignacionNewBI asignacionNewBI;
    @Autowired
    ReporteBI reporteBI;
    @Autowired
    PerfilesNuevoBI perfilesNuevoBI;
    @Autowired
    ActivarCheckListBI activarCheckListBI;
    @Autowired
    SucUbicacionBI sucUbicacionBI;
    @Autowired
    CargaCheckListBI cargaCheckListBI;
    @Autowired
    EliminaCheckListBI eliminaCheckListBI;
    @Autowired
    TruncTableTokenBI truncTableTokenBI;
    @Autowired
    AsignaExpBI asignaExpBI;
    @Autowired
    DistribCecoBI distribCecoBI;
    @Autowired
    HallazgosExpBI hallazgosExpBI;

    @Autowired
    DepuracionTblBI depuracionTblBI;

    @Autowired
    ComentariosRepAsgBI comentariosRepAsgBI;
    @Autowired
    AsignaTransfBI asignaTransfBI;

    @Autowired
    AsigPuntoContactoBI asigPuntoContactoBI;
    
    @Autowired
    HallazgosTransfBI hallazgosTransfBI;

    private static final Logger logger = LogManager.getLogger(CargaService.class);

    // http://localhost:8080/checklist/cargaServices/cargaUsuarios.json
    @RequestMapping(value = "/cargaUsuarios", method = RequestMethod.GET)
    public ModelAndView cargaUsuarios(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean[] res = usuarioabi.cargaUsuarios();
            //System.out.println(res.length);
            //System.out.println(res[0] + "---" + res[1]);
            // boolean res = usuarioabi.cargaUsuarios();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA USUARIOS CORRECTA");
            mv.addObject("res", res[0] + "  --- BAJA USUARIOS  " + res[1] + "  --- CARGA PERFILES  " + res[2]);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/checklist/cargaServices/cargaGeografia.json
    @RequestMapping(value = "/cargaGeografia", method = RequestMethod.GET)
    public ModelAndView cargaGeografia(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cecobi.cargaGeografia();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA GEOGRAFIA CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaSucursales.json
    @RequestMapping(value = "/cargaSucursales", method = RequestMethod.GET)
    public ModelAndView cargaSucursales(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = sucursalbi.cargaSucursales();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA SUCURSALES CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaCecos.json
    @RequestMapping(value = "/cargaCecos", method = RequestMethod.GET)
    public ModelAndView cargaCecos(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cecobi.cargaCecos();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA CECOS CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/updateCecos.json
    @RequestMapping(value = "/updateCecos", method = RequestMethod.GET)
    public ModelAndView updateCecos(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cecobi.updateCecos();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "UPDATE CECOS CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaCecos2.json
    @RequestMapping(value = "/cargaCecos2", method = RequestMethod.GET)
    public ModelAndView cargaCecos2(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cecobi.cargaCecos2();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA CECOS2 CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaCecosGuatemala.json
    @RequestMapping(value = "/cargaCecosGuatemala.json", method = RequestMethod.GET)
    public ModelAndView cargaCecosGuatemala(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int[] res = cecobi.cargaCecosGuatemala();
            //System.out.println(res.length);
            //System.out.println(res[0] + "---" + res[1] + "---" + res[2] + "---" + res[3]);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CARGA CECOS GUATEMALA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaCecosPeru.json
    @RequestMapping(value = "/cargaCecosPeru.json", method = RequestMethod.GET)
    public ModelAndView cargaCecosPeru(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int[] res = cecobi.cargaCecosPeru();
            //System.out.println(res.length);
            //System.out.println(res[0] + "---" + res[1] + "---" + res[2] + "---" + res[3]);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CARGA CECOS PERU");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaCecosHonduras.json
    @RequestMapping(value = "/cargaCecosHonduras.json", method = RequestMethod.GET)
    public ModelAndView cargaCecosHonduras(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int[] res = cecobi.cargaCecosHonduras();
            //System.out.println(res.length);
            //System.out.println(res[0] + "---" + res[1] + "---" + res[2] + "---" + res[3]);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CARGA CECOS HONDURAS");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaCecosPanama.json
    @RequestMapping(value = "/cargaCecosPanama.json", method = RequestMethod.GET)
    public ModelAndView cargaCecosPanama(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int[] res = cecobi.cargaCecosPanama();
            //System.out.println(res.length);
            // //System.out.println(res[0] + "---" + res[1] + "---" + res[2] + "---" +
            // res[3]);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CARGA CECOS PANAMA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaCecosSalvador.json
    @RequestMapping(value = "/cargaCecosSalvador.json", method = RequestMethod.GET)
    public ModelAndView cargaCecosSalvador(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int[] res = cecobi.cargaCecosSalvador();
            //System.out.println(res.length);
            // //System.out.println(res[0] + "---" + res[1] + "---" + res[2] + "---" +
            // res[3]);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CARGA CECOS SALVADOR");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaCecosLAM.json
    @RequestMapping(value = "/cargaCecosLAM.json", method = RequestMethod.GET)
    public ModelAndView cargaCecosLAM(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            Map<String, Integer> res = cecobi.cargaCecosLAM();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CARGA CECOS LAM");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaPerfiles.json
    @RequestMapping(value = "/cargaPerfiles.json", method = RequestMethod.GET)
    public ModelAndView cargaPerfiles(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int[] res = perfilbi.agregaNuevoPerfil();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CARGA PERFILES");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaIdPosibles.json
    @RequestMapping(value = "/cargaIdPosibles.json", method = RequestMethod.GET)
    public ModelAndView cargaIdPosibles(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            boolean res = arboldecisionbi.cambiaRespuestasPorId();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA ID POSIBLES ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaEstatusCheckPreg.json
    @RequestMapping(value = "/cargaEstatusCheckPreg.json", method = RequestMethod.GET)
    public ModelAndView cargaEstatusCheckPreg(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            boolean res = checklistPreguntaBI.cambiaEstatus();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Cambia estatus ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/checklist/cargaServices/ejecutaAutorizados.json
    @RequestMapping(value = "/ejecutaAutorizados.json", method = RequestMethod.GET)
    public ModelAndView ejecutaAutorizados(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            boolean res = checkTBI.ejecutaAutorizados();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Ejecute Autorizados ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/checklist/cargaServices/ejecutaAsignaciones.json
    @RequestMapping(value = "/ejecutaAsignaciones.json", method = RequestMethod.GET)
    public ModelAndView ejecutaAsignaciones(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            Map<String, Object> res = asignacionBI.ejecutaAsignacion();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "EJECUTE ASIGNACIONES");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/checklist/cargaServices/ejecutaAsignacionesNew.json
    @RequestMapping(value = "/ejecutaAsignacionesNew.json", method = RequestMethod.GET)
    public ModelAndView ejecutaAsignacionesNew(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            Map<String, Object> res = asignacionNewBI.ejecutaAsignaciones();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "EJECUTE ASIGNACIONES");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            //System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/checklist/cargaServices/cargaInicialHistorico.json
    @RequestMapping(value = "/cargaInicialHistorico", method = RequestMethod.GET)
    public ModelAndView cargaInicialHistorico(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = reporteBI.CargaInicialHistorico();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA INICIAL DE HISTORICO CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaCecosHistorico.json
    @RequestMapping(value = "/cargaCecosHistorico", method = RequestMethod.GET)
    public ModelAndView cargaCecosHistorico(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = reporteBI.CargaCecosHistorico();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA CECOS DE HISTORICO CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaGeografiaHistorico.json
    @RequestMapping(value = "/cargaGeografiaHistorico", method = RequestMethod.GET)
    public ModelAndView cargaGeografiaHistorico(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = reporteBI.CargaGeografiaHistorico();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA GEOGRAFIA DE HISTORICO CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaActPaises.json
    @RequestMapping(value = "/cargaActPaises", method = RequestMethod.GET)
    public ModelAndView cargaActPaises(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = sucursalbi.cargaActualizaPaises();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualización de Paises");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaCCMexico.json
    @RequestMapping(value = "/cargaCCMexico", method = RequestMethod.GET)
    public ModelAndView cargaCCMexico(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cecobi.cargaCreditoCobranzaCecos();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Carga Credito y Cobranza México");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/actualizaSucursales0.json
    @RequestMapping(value = "/actualizaSucursales0", method = RequestMethod.GET)
    public ModelAndView actualizaSucursales0(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = sucursalbi.rellenaSucursal();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualizacion sucursales que inician en 0");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaCCPanama.json
    @RequestMapping(value = "/cargaCCPanama", method = RequestMethod.GET)
    public ModelAndView cargaCCPanama(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cecobi.cargaCreditoCobranzaCecosPanama();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Carga Credito y Cobranza Panama");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaPerfilesAuto.json
    @RequestMapping(value = "/cargaPerfilesAuto", method = RequestMethod.GET)
    public ModelAndView cargaPerfilesAuto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean actualizacionGestion = perfilesNuevoBI.asignacionPerfiles();
            boolean asignacion = false;
            logger.info("ACTUALIZACION GESTION: " + actualizacionGestion);

            if (actualizacionGestion) {
                asignacion = perfilesNuevoBI.actualizacionPerfilesGestion();
            }
            logger.info("ACTUALIZACION PERFILES: " + asignacion);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA DE PERFILES: ");
            mv.addObject("res", asignacion);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaPerfilesPaso1.json
    @RequestMapping(value = "/cargaPerfilesPaso1", method = RequestMethod.GET)
    public ModelAndView cargaPerfilesPaso1(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean actualizacionGestion = perfilesNuevoBI.asignacionPerfiles();
            boolean asignacion = false;
            logger.info("ACTUALIZACION GESTION: " + actualizacionGestion);

            if (actualizacionGestion) {
                asignacion = perfilesNuevoBI.actualizacionPerfilesGestion();
            }
            logger.info("ACTUALIZACION PERFILES: " + asignacion);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ASIGNA PERFILES (FRANQUICIA) ");
            mv.addObject("res", asignacion);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaPerfilesPaso2.json
    @RequestMapping(value = "/cargaPerfilesPaso2", method = RequestMethod.GET)
    public ModelAndView cargaPerfilesPaso2(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean asignacion = false;

            asignacion = perfilesNuevoBI.actualizacionPerfilesGestion();

            logger.info("ACTUALIZACION PERFILES: " + asignacion);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ASIGNA PERFILES (GESTION) ");
            mv.addObject("res", asignacion);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/altaPerfilSistemas.json
    @RequestMapping(value = "/altaPerfilSistemas", method = RequestMethod.GET)
    public ModelAndView altaPerfilSistemas(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean asignacion = false;

            asignacion = perfilesNuevoBI.altaPerfilSistemas();

            logger.info("ALTA PERFILES DE SISTEMAS: " + asignacion);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ASIGNA PERFILES SISTEMAS ");
            mv.addObject("res", asignacion);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/altaPerfilScouting.json
    @RequestMapping(value = "/altaPerfilScouting", method = RequestMethod.GET)
    public ModelAndView altaPerfilScouting(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean asignacion = false;

            asignacion = perfilesNuevoBI.altaPerfilSCouting();

            logger.info("ALTA PERFILES DE SCOUTING: " + asignacion);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ASIGNA PERFILES SCOUTING ");
            mv.addObject("res", asignacion);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/altaPerfilPrestaPrenda.json
    @RequestMapping(value = "/altaPerfilPrestaPrenda", method = RequestMethod.GET)
    public ModelAndView altaPerfilPrestaPrenda(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean asignacion = false;

            asignacion = perfilesNuevoBI.altaPerfilPrestaPrenda();

            logger.info("ALTA PERFILES DE PRESTA PRENDA: " + asignacion);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ASIGNA PERFILES PRESTA PRENDA ");
            mv.addObject("res", asignacion);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/altaPerfilDescargaGestionM.json
    @RequestMapping(value = "/altaPerfilDescargaGestionM", method = RequestMethod.GET)
    public ModelAndView altaPerfilDescargaGestionM(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean asignacion = false;

            asignacion = perfilesNuevoBI.altaPerfilDescargaGestion();

            logger.info("ALTA PERFILES QUE DESCARGAN GESTION MOVIL: " + asignacion);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ASIGNA PERFILES QUE DESCARGAN GESTION MOVIL");
            mv.addObject("res", asignacion);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/cargaCecosCreditoCobranza.json
    @RequestMapping(value = "/cargaCecosCreditoCobranza.json", method = RequestMethod.GET)

    public ModelAndView cargaCecosCreditoCobranza(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        try {

            boolean res = cecobi.cargaCreditoCobranza();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");

            mv.addObject("tipo", "CECO_CYC");

            mv.addObject("res", res);

            return mv;

        } catch (Exception e) {

            //System.out.println("DENTRO DEL CATCH");
            logger.info(e);

            return null;

        }
    }
    // http://10.51.219.179:8080/checklist/cargaServices/activarCheckList.json?idUsuario=191312&idCeco=480181&idChecklist=&activo=0

    /* Habilitar o desabilitar por usuario,ceco, S/N checklist con parametro activo.
		http://10.51.219.179:8080/checklist/cargaServices/activarCheckList.json?idUsuario=191312&idCeco=<?>&activo=<?>
		
		Habilitar o desabilitar por usuario , ceco, checklist con parametro activo.
		http://10.51.219.179:8080/checklist/cargaServices/activarCheckList.json?idUsuario=191312&idCeco=<?>&idChecklist=<?>&activo=<?>
					 
		Habilitar o desabilitar por ceco , checklist con parametro activo
		http://10.51.219.179:8080/checklist/cargaServices/activarCheckList.json?idCeco=<?>&idChecklist=<?>&activo=<?>
		
		Habilitar o desabilitar por usuario , checklist con parametro activo
		http://10.51.219.179:8080/checklist/cargaServices/activarCheckList.json?idUsuario=<?>&idChecklist=<?>&activo=<?>
		  
		Habilitar o desabilitar por usuario con parametro activo.
		http://10.51.219.179:8080/checklist/cargaServices/activarCheckList.json?idUsuario=<?>&activo=<?>
			 
		Habilitar o desabilitar por ceco con parametro activo.
		http://10.51.219.179:8080/checklist/cargaServices/activarCheckList.json?idCeco=<?>&activo=<?>
		
		Habilitar o desabilitar solo por checklist
		http://10.51.219.179:8080/checklist/cargaServices/activarCheckList.json?&idChecklist=<?>&activo=<?>	
     */
    @RequestMapping(value = "/activarCheckList", method = RequestMethod.GET)
    public ModelAndView activarCheckList(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idCeco = request.getParameter("idCeco");
            String idChecklist = request.getParameter("idChecklist");
            String activo = request.getParameter("activo");

            ActivarCheckListDTO a1 = new ActivarCheckListDTO();

            a1.setIdUsuario(idUsuario);
            a1.setIdCeco(idCeco);
            a1.setIdChecklist(idChecklist);
            a1.setActivo(activo);

            boolean res = activarCheckListBI.actualizaCheckList(a1);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ACTUALIZA CHECKLIST (FRANQUICIA) ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/checklist/cargaServices/cargaSucUbi.json
    @RequestMapping(value = "/cargaSucUbi", method = RequestMethod.GET)
    public ModelAndView cargaSucUbi(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean asignacion = false;

            asignacion = sucUbicacionBI.cargaSucUbic();

            logger.info("ALTA Carga SucUbica: " + asignacion);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Carga SucUbica");
            mv.addObject("res", asignacion);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/EliminacargaSucUbi.json
    @RequestMapping(value = "/EliminacargaSucUbi", method = RequestMethod.GET)
    public ModelAndView EliminacargaSucUbi(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean asignacion = false;

            asignacion = sucUbicacionBI.EliminacargaSucUbic();

            logger.info("baja Carga SucUbica: " + asignacion);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Baja Carga SucUbica");
            mv.addObject("res", asignacion);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/cargaServices/altaCheckList.json
    @RequestMapping(value = "/altaCheckList", method = RequestMethod.GET)
    public ModelAndView altaCheckList(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            //String idUsuario = request.getParameter("idUsuario");
            //String idCeco= request.getParameter("idCeco");

            CargaCheckListDTO a1 = new CargaCheckListDTO();
            //a1.setIdCeco(idCeco);
            //	a1.setIdUsuario(idUsuario);
            boolean res = cargaCheckListBI.insertaCheckList();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ALTA DE CHECKLIST 313 Y 219 (FRANQUICIA) ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/cargaServices/eliminaCheckListPuesto.json
    @RequestMapping(value = "/eliminaCheckListPuesto", method = RequestMethod.GET)
    public ModelAndView eliminaCheckListPuesto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            boolean res = eliminaCheckListBI.eliminaCheckList();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ElIMINA CHECKLIST POR PUESTO (FRANQUICIA) ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/cargaServices/truncateTableToken.json
    @RequestMapping(value = "/truncateTableToken", method = RequestMethod.GET)
    public ModelAndView truncateTableToken(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = false;
            res = truncTableTokenBI.truncTable();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "TRUNCATE TABLE FRANQUICIA.FRTATOKEN ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://10.51.219.179:8080/checklist/cargaServices/DistribCeco.json
    @RequestMapping(value = "/DistribCeco", method = RequestMethod.GET)
    public ModelAndView DistribCeco(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            //String idUsuario = request.getParameter("idUsuario");
            //String idCeco= request.getParameter("idCeco");

            DistribCecoDTO a1 = new DistribCecoDTO();
            //a1.setIdCeco(idCeco);
            //	a1.setIdUsuario(idUsuario);
            int res = distribCecoBI.insertaDistrib();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Distribucion de Cecos ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
    //http://localhost:8080/checklist/cargaServices/altaCargaAsignaExp.json?

    @RequestMapping(value = "/altaCargaAsignaExp", method = RequestMethod.GET)
    public ModelAndView altaCargaAsignaExp(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean respuesta = asignaExpBI.cargaNuevo();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "  carga Asignaciones ");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
    // http://10.51.219.179:8080/checklist/cargaServices/cargaHallazgoHistorico.json?

    @RequestMapping(value = "/cargaHallazgoHistorico", method = RequestMethod.GET)
    public ModelAndView cargaHallazgoHistorico(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean res = hallazgosExpBI.cargaHallazgosHistorico();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "HALLAZGOS Historicos CARGADOS ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/depuraTablas.json=nombreTabla<?>
    @RequestMapping(value = "/depuraTablas", method = RequestMethod.GET)
    public ModelAndView depuraTablas(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String nombreTabla = request.getParameter("nombreTabla");

            boolean depurar = false;

            depurar = depuracionTblBI.depuraTabla(nombreTabla);

            logger.info("DEPURACION TABLA: " + depurar);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "SE DEPURAN LAS TABLAS");
            mv.addObject("res", depurar);
            return mv;

        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/depuraBitacoras.json=fechaInicio<?>&fechaFin=<?>
    @RequestMapping(value = "/depuraBitacoras", method = RequestMethod.GET)
    public ModelAndView depuraBitacoras(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");

            ArrayList<Object> depurar = null;

            depurar = depuracionTblBI.depuraBitacora(fechaInicio, fechaFin);

            logger.info("DEPURACION DE BITACORAS: " + depurar);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "SE DEPURAN LAS BITACORAS");
            mv.addObject("res", "RES" + ((boolean) depurar.get(0)) + " TOTAL REGS: " + ((double) depurar.get(1)));
            return mv;

        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/cargaServices/depuraErrores.json=fechaInicio<?>&fechaFin=<?>
    @RequestMapping(value = "/depuraErrores", method = RequestMethod.GET)
    public ModelAndView depuraErrores(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            String fechaInicio = request.getParameter("fechaInicio");
            String fechaFin = request.getParameter("fechaFin");

            ArrayList<Object> depurar = null;

            depurar = depuracionTblBI.depuraErrores(fechaInicio, fechaFin);

            logger.info("DEPURACION DE ERRORES : " + depurar);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "SE DEPURAN LOS ERRORES");
            mv.addObject("res", "RES" + ((boolean) depurar.get(0)) + " TOTAL REGS: " + ((double) depurar.get(1)));
            return mv;

        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://10.51.219.179:8080/checklist/cargaServices/setComentarioAsgCalidad.json?ceco=<?>&fecha=<?>&comentario=<?>			
    @RequestMapping(value = "/setComentarioAsgCalidad", method = RequestMethod.GET)

    public ModelAndView setComentarioAsgCalidad(HttpServletRequest request, HttpServletResponse response, Model model) {

        try {

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");

            String ceco = request.getParameter("ceco");
            String fecha = request.getParameter("fecha");
            String comentario = request.getParameter("comentario");

            System.out.println("ceco: " + ceco + "fecha: " + fecha + "comentario: " + comentario);
            Boolean res = comentariosRepAsgBI.setComentarios(ceco, fecha, comentario);
            mv.addObject("tipo", "INSERTA_COMENTARIOS_ASIG_CALIADAD");
            mv.addObject("res", res);

            return mv;

        } catch (Exception e) {

            logger.info("Ocurrio algo " + e);
            return null;
        }

    }

    //http://localhost:8080/checklist/cargaServices/altaCargaAsignaTransf.json?
    @RequestMapping(value = "/altaCargaAsignaTransf", method = RequestMethod.GET)
    public ModelAndView altaCargaAsignaTransf(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean respuesta = asignaTransfBI.CargaAsig();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "  carga Asignaciones transf");
            mv.addObject("res", respuesta);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/checklist/cargaServices/cargaPuntoContacto.json
    @RequestMapping(value = "/cargaPuntoContacto", method = RequestMethod.GET)
    public ModelAndView cargaPuntoContacto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            asigPuntoContactoBI.asignaProtocolosUsr();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA DE PUNTOS DE CONTACTO");
            mv.addObject("res", true);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
    
    //http://localhost:8080/checklist/cargaServices/cargaPuntoContactoByCeco.json?idCeco=<?>
    @RequestMapping(value = "/cargaPuntoContactoByCeco", method = RequestMethod.GET)
    public @ResponseBody String cargaPuntoContactoByCeco(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        int cantidadUsuarios=0;
        boolean ejecucion=true;
        try {
                        
            String ceco = request.getParameter("idCeco");

            cantidadUsuarios = asigPuntoContactoBI.asignaProtocolosCeco(ceco);

        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            ejecucion=false;
            cantidadUsuarios=0;
        }
        
        String json="{"
                + "\"resultadoOk\":"+ejecucion
                + ",\"usuarios_modificados:\":"+cantidadUsuarios
                + "}";
                
        return json;
    }
    
 // http://10.51.219.179:8080/checklist/cargaServices/cargaHallazgoHistoricoTransformacion.json?

    @RequestMapping(value = "/cargaHallazgoHistoricoTransformacion", method = RequestMethod.GET)
    public ModelAndView cargaHallazgoHistoricoTransformacion(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean res = hallazgosTransfBI.cargaHallazgosHistorico();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "HALLAZGOS Historicos Transformacion CARGADOS ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }


}
