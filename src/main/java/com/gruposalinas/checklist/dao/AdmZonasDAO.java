package com.gruposalinas.checklist.dao;

import java.util.ArrayList;
import java.util.List;
import com.gruposalinas.checklist.domain.AdminZonaDTO;

public interface AdmZonasDAO {
	
	public ArrayList<AdminZonaDTO> obtieneZonaById(AdminZonaDTO bean) throws Exception;
	
	public boolean insertaZona(AdminZonaDTO bean) throws Exception;

	public boolean actualizaZona(AdminZonaDTO bean) throws Exception;
	
	public boolean eliminaZona(AdminZonaDTO bean) throws Exception;
}
