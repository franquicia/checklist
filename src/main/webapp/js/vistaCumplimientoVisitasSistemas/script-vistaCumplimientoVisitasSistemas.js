$body = $("body");
$(document).on({
	ajaxStart : function() {
		$body.addClass("loading");
	},
	ajaxStop : function() {
		$body.removeClass("loading");
	}
});

$(window).load(function() {
	iniciaAno();
});

function myFunction() {
	$body.removeClass("loading");
	location.reload();
}

function nobackbutton(){
	window.location.hash="no-back-button";
	window.location.hash="Again-No-back-button" //chrome
	window.onhashchange=function(){window.location.hash="";}
}

function porcentaje(selectMes,selectAno){
	
	var hoy = new Date();
	var dd = hoy.getDate();
	var mm = hoy.getMonth()+1;
	var yy = hoy.getFullYear();

	var meses= new Array ('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre');
	
	var mesInicial=selectMes;
	var anoInicial=selectAno;				
	var diasMes= new Date(anoInicial || new Date().getFullYear(), mesInicial, 0).getDate();
	var ddF = dd;
	var porcentaje=0;
	var por2="";
	var f = new Date();
	
	if((f.getMonth()+1)==mesInicial && f.getFullYear()==anoInicial){
		porcentaje=((f.getDate()-1)*100)/diasMes;
	}else{
		ddF=diasMes;
		porcentaje=100;	
	}
	por2=""+porcentaje;
	var porcenI= por2.split(".");
	
	return(porcenI[0]);	
}

function enviaFiltro(nivValida) {
	
	var hoy = new Date();
	var mm = hoy.getMonth() + 1;
	var yy = hoy.getFullYear();

	var selectMes = document.getElementById("seleccionMes").value;
	var selectAno = document.getElementById("seleccionAno").value;
	var valorPais= document.getElementById("seleccionPais").value;

	var x = document.getElementById("seleccionPais").selectedIndex;
	var y = document.getElementById("seleccionPais").options;
	var selectPais = y[x].text;

	if(nivValida == -1){
		alert("No tienes perfil");
	}else{
		if(valorPais==0 || selectMes==0){
			alert('Te falta llenar campos');
		}else{
			if ((selectMes > mm) && (selectAno == yy)) {
				alert('Fecha invalida');
			} else {			
				var porcentajeG=porcentaje(selectMes,selectAno);
				if (nivelPerfilL == 0 || nivelPerfilL == 1) {
					if (nivelPerfilL == 0)
						$("#formFiltro").append("<input type='hidden' name='banderaCecoPadre' id='banderaCecoPadre' value='0' />");
					else
						$("#formFiltro").append("<input type='hidden' name='banderaCecoPadre' id='banderaCecoPadre' value='1' />");
					$("#formFiltro").append("<input type='hidden' name='nivelPerfil' id='nivelPerfil' value='"+ nivelPerfilL + "' />");
					$("#formFiltro").append("<input type='hidden' name='porcentajeG' id='porcentajeG' value='"+ porcentajeG + "' />");
					$("#formFiltro").append("<input type='hidden' name='idCecoPadre' id='idCecoPadre' value='"+ idCecoL + "' />");
					$("#formFiltro").append("<input type='hidden' name='nombreCeco' id='nombreCeco' value='"+ selectPais + "' />");
					document.getElementById("formFiltro").submit();
				} else {
					redirecciona(porcentajeG,nivelPerfilL,selectMes, selectAno);
				}	
				setTimeout(myFunction, 25000);
				$body.addClass("loading");
			}				
		}	
	}	
	return true;
}

function redirecciona(porcentajeG,nivelPerfil, seleccionMes, seleccionAno) {

	var seleccionCanal = document.getElementById("seleccionCanal").value;
	var seleccionPais = document.getElementById("seleccionPais").value;

	$("#formTerri").append("<input type='hidden' name='porcentajeG' id='porcentajeG' value='"+porcentajeG+"' />");
	$("#formTerri").append("<input type='hidden' name='banderaCecoPadre' id='banderaCecoPadre' value='0' />");
	$("#formTerri").append("<input type='hidden' name='nivelPerfil' id='nivelPerfil' value='"+nivelPerfil+"' />");
	$("#formTerri").append("<input type='hidden' name='idCeco' id='idCeco' value='" + idCecoL+ "' />");
	$("#formTerri").append("<input type='hidden' name='idCecoPadre' id='idCecoPadre' value='" + idCecoL+ "' />");
	$("#formTerri").append("<input type='hidden' name='nombreCeco' id='nombreCeco' value='"+ nombreCecoL + "' />");
	$("#formTerri").append("<input type='hidden' name='seleccionAno' id='seleccionAno' value='"+ seleccionAno + "' />");
	$("#formTerri").append("<input type='hidden' name='seleccionMes' id='seleccionMes' value='"+ seleccionMes + "' />");
	$("#formTerri").append("<input type='hidden' name='seleccionCanal' id='seleccionCanal' value='"+ seleccionCanal + "' />");
	$("#formTerri").append("<input type='hidden' name='seleccionPais' id='seleccionPais' value='"+ seleccionPais + "' />");

	document.getElementById("formTerri").submit();
	return true;
}

function iniciaAno() {

	var hoy = new Date();
	var mm = hoy.getMonth() + 1;
	var yy = hoy.getFullYear();

	var selectAno = document.getElementById("seleccionAno");
	var selectMes = document.getElementById("seleccionMes");

	for (var i = 0; i < selectAno.options.length; i++) {
		if (selectAno.options[i].value == yy) {
			document.getElementById("seleccionAno").selectedIndex = i;
			break;
		}
	}
	for (var j = 0; j < selectMes.options.length; j++) {
		if (selectMes.options[j].value == mm) {
			document.getElementById("seleccionMes").selectedIndex = j;
			break;
		}
	}
}

function mes() {
	document.getElementById("seleccionMes").innerHTML = "";
	$("#seleccionMes").append("" +
		"<option value='0'>Selecciona Mes</option>"+
		"<option value='1'>Enero</option>"+
		"<option value='2'>Febrero</option>"+
		"<option value='3'>Marzo</option>"+
		"<option value='4'>Abril</option>"+
		"<option value='5'>Mayo</option>"+
		"<option value='6'>Junio</option>"+
		"<option value='7'>Julio</option>"+
		"<option value='8'>Agosto</option>"+
		"<option value='9'>Septiembre</option>"+
		"<option value='10'>Octubre</option>"+
		"<option value='11'>Noviembre</option>"+
		"<option value='12'>Diciembre</option>");
}

function canal(varCanal) {
	document.getElementById("seleccionPais").innerHTML = "";
	$.ajax({
		type : "GET",
		url : '../consultaCatalogosService/getPaisesCombo.json?idNegocio='
				+ varCanal,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		delimiter : ",",
		success : function(json) {
			var i = 0;
			if (json[i] == null) {
				alert('Sin Datos');
				$("#seleccionPais").append(
						"<option value=0>Sin Pa&iacute;s</option>");
			}
			while (json[i] != null) {
				$("#seleccionPais").append(
						"<option value=" + json[i].idPais + ">"
								+ json[i].nombre + "</option>");
				i++;
			}
		},
		error : function() {
			alert('Favor de actualizar la pagina');
		}
	});
}