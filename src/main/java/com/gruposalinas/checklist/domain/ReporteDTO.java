package com.gruposalinas.checklist.domain;

public class ReporteDTO {

	private String idChecklist;
	private String idUsuario;
	private String nombreUsuario;
	private String nombreSucursal;
	private String nombreCeco;
	private String fechaRespuesta;
	private int numero;
	private String dia;
	private String semana;
	private String nombreModuloP;
	private String nombreMod;
	private String idModulo;
	private String idPregunta;
	private String desPregunta;
	private String idArboldes;
	private String respuesta;
	private String nombreChecklist;
	private String metodoChecklist;
	
	
	private int idTipoCheck;
	private int idRuta;
	private String ruta;
	private int idPais;
	private String pais;
	private int idCanal;
	public String getPonderacion() {
		return ponderacion;
	}
	public void setPonderacion(String ponderacion) {
		this.ponderacion = ponderacion;
	}
	private String canal;
	private int idGeografia;
	private String nombreGeografia;
	private String idCeco;
	private int idCheckUsua;
	private String observaciones;
	private String compromisos;
	private String idResposable;
	private String nombreResponsable;
	private String fechaCompromiso;
	private String estatus;
	private String ponderacion;
	
	private String territorio;
	private String zona;
	private String regional;
	private String idRegional;
	private String idZona;
	private String idTerritorio;
	private String sucursal;
	private String fecha_r;
	private String pregunta_cp;
	private String monto;
	private String motivo;
	private String fecha_c;
	private String pregunta_ca;
	private String monto_ca;
	private String motivo_ca;
	private String fecha_ca;
	
	private int idPuesto;
	private String puesto;
	private int idBitacora;
	
	private int plan;
	private int real;
	private int avance;
	private int total;
	private String si;
	private String no;
	private int distintos;
	private String fondo;
	private String imagen;
	
	private int conteo;
	
	public int getConteo() {
		return conteo;
	}
	public void setConteo(int conteo) {
		this.conteo = conteo;
	}
	public String getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(String idChecklist) {
		this.idChecklist = idChecklist;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getNombreSucursal() {
		return nombreSucursal;
	}
	public void setNombreSucursal(String nombreSucursal) {
		this.nombreSucursal = nombreSucursal;
	}
	public String getFechaRespuesta() {
		return fechaRespuesta;
	}
	public void setFechaRespuesta(String fechaRespuesta) {
		this.fechaRespuesta = fechaRespuesta;
	}
	public String getNombreChecklist() {
		return nombreChecklist;
	}
	public void setNombreChecklist(String nombreChecklist) {
		this.nombreChecklist = nombreChecklist;
	}
	public String getMetodoChecklist() {
		return metodoChecklist;
	}
	public void setMetodoChecklist(String metodoChecklist) {
		this.metodoChecklist = metodoChecklist;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	public String getSemana() {
		return semana;
	}
	public void setSemana(String semana) {
		this.semana = semana;
	}
	public String getNombreModuloP() {
		return nombreModuloP;
	}
	public void setNombreModuloP(String nombreModuloP) {
		this.nombreModuloP = nombreModuloP;
	}
	public String getNombreMod() {
		return nombreMod;
	}
	public void setNombreMod(String nombreMod) {
		this.nombreMod = nombreMod;
	}
	public String getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(String idPregunta) {
		this.idPregunta = idPregunta;
	}
	public String getDesPregunta() {
		return desPregunta;
	}
	public void setDesPregunta(String desPregunta) {
		this.desPregunta = desPregunta;
	}
	public String getIdArboldes() {
		return idArboldes;
	}
	public void setIdArboldes(String idArboldes) {
		this.idArboldes = idArboldes;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getIdModulo() {
		return idModulo;
	}
	public void setIdModulo(String idModulo) {
		this.idModulo = idModulo;
	}
	public int getIdTipoCheck() {
		return idTipoCheck;
	}
	public void setIdTipoCheck(int idTipoCheck) {
		this.idTipoCheck = idTipoCheck;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public int getIdGeografia() {
		return idGeografia;
	}
	public void setIdGeografia(int idGeografia) {
		this.idGeografia = idGeografia;
	}
	public String getNombreGeografia() {
		return nombreGeografia;
	}
	public void setNombreGeografia(String nombreGeografia) {
		this.nombreGeografia = nombreGeografia;
	}
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public int getIdRuta() {
		return idRuta;
	}
	public void setIdRuta(int idRuta) {
		this.idRuta = idRuta;
	}
	public int getIdCheckUsua() {
		return idCheckUsua;
	}
	public void setIdCheckUsua(int idCheckUsua) {
		this.idCheckUsua = idCheckUsua;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getTerritorio() {
		return territorio;
	}
	public void setTerritorio(String territorio) {
		this.territorio = territorio;
	}
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	public String getRegional() {
		return regional;
	}
	public void setRegional(String regional) {
		this.regional = regional;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getFecha_r() {
		return fecha_r;
	}
	public void setFecha_r(String fecha_r) {
		this.fecha_r = fecha_r;
	}
	public String getPregunta_cp() {
		return pregunta_cp;
	}
	public void setPregunta_cp(String pregunta_cp) {
		this.pregunta_cp = pregunta_cp;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public String getFecha_c() {
		return fecha_c;
	}
	public void setFecha_c(String fecha_c) {
		this.fecha_c = fecha_c;
	}
	public String getPregunta_ca() {
		return pregunta_ca;
	}
	public void setPregunta_ca(String pregunta_ca) {
		this.pregunta_ca = pregunta_ca;
	}
	public String getMonto_ca() {
		return monto_ca;
	}
	public void setMonto_ca(String monto_ca) {
		this.monto_ca = monto_ca;
	}
	public String getMotivo_ca() {
		return motivo_ca;
	}
	public void setMotivo_ca(String motivo_ca) {
		this.motivo_ca = motivo_ca;
	}
	public String getFecha_ca() {
		return fecha_ca;
	}
	public void setFecha_ca(String fecha_ca) {
		this.fecha_ca = fecha_ca;
	}
	public String getIdRegional() {
		return idRegional;
	}
	public void setIdRegional(String idRegional) {
		this.idRegional = idRegional;
	}
	public String getIdZona() {
		return idZona;
	}
	public void setIdZona(String idZona) {
		this.idZona = idZona;
	}
	public String getIdTerritorio() {
		return idTerritorio;
	}
	public void setIdTerritorio(String idTerritorio) {
		this.idTerritorio = idTerritorio;
	}
	public int getIdPuesto() {
		return idPuesto;
	}
	public void setIdPuesto(int idPuesto) {
		this.idPuesto = idPuesto;
	}
	public String getPuesto() {
		return puesto;
	}
	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getCanal() {
		return canal;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
	public int getIdPais() {
		return idPais;
	}
	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}
	public int getIdCanal() {
		return idCanal;
	}
	public void setIdCanal(int idCanal) {
		this.idCanal = idCanal;
	}
	public int getPlan() {
		return plan;
	}
	public void setPlan(int plan) {
		this.plan = plan;
	}
	public int getReal() {
		return real;
	}
	public void setReal(int real) {
		this.real = real;
	}
	public int getAvance() {
		return avance;
	}
	public void setAvance(int avance) {
		this.avance = avance;
	}
	public int getDistintos() {
		return distintos;
	}
	public void setDistintos(int distintos) {
		this.distintos = distintos;
	}
	public String getFondo() {
		return fondo;
	}
	public void setFondo(String fondo) {
		this.fondo = fondo;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getSi() {
		return si;
	}
	public void setSi(String si) {
		this.si = si;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getCompromisos() {
		return compromisos;
	}
	public void setCompromisos(String compromisos) {
		this.compromisos = compromisos;
	}
	public String getIdResposable() {
		return idResposable;
	}
	public void setIdResposable(String idResposable) {
		this.idResposable = idResposable;
	}
	public String getFechaCompromiso() {
		return fechaCompromiso;
	}
	public void setFechaCompromiso(String fechaCompromiso) {
		this.fechaCompromiso = fechaCompromiso;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getNombreResponsable() {
		return nombreResponsable;
	}
	public void setNombreResponsable(String nombreResponsable) {
		this.nombreResponsable = nombreResponsable;
	}
	
}
