package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.SucursalHistDTO;
import com.gruposalinas.checklist.mappers.SucursalHistoricoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class SucursalHistDAOImpl extends DefaultDAO implements SucursalHistDAO {

    private static Logger logger = LogManager.getLogger(SucursalHistDAOImpl.class);

    DefaultJdbcCall jdbcObtieneSucursal;

    public void init() {
        jdbcObtieneSucursal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMHISTSUC")
                .withProcedureName("SP_CONSULTA")
                .returningResultSet("RCL_CONSULTA", new SucursalHistoricoRowMapper());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<SucursalHistDTO> obtieneSucursal(String idHistSuc, String numSuc) throws Exception {
        Map<String, Object> out = null;
        List<SucursalHistDTO> listaSucursal = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NUMSUC", numSuc).addValue("PA_SUC", idHistSuc);

        out = jdbcObtieneSucursal.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMHISTSUC.SP_CONSULTA}");

        listaSucursal = (List<SucursalHistDTO>) out.get("RCL_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo paso al obtener la Sucursal");
        } else {
            return listaSucursal;
        }

        return null;
    }

}
