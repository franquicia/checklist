package com.gruposalinas.checklist.servicios.servidor;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gruposalinas.checklist.business.AdmPregZonasBI;
import com.gruposalinas.checklist.business.AdmPregZonasTempBI;
import com.gruposalinas.checklist.business.AdmTipoZonaBI;
import com.gruposalinas.checklist.business.AdmZonasBI;
import com.gruposalinas.checklist.business.ArbolRespAdiEviAdmBI;
import com.gruposalinas.checklist.business.BitacoraCheckUsuAdmBI;
import com.gruposalinas.checklist.business.CheckAutProtoBI;
import com.gruposalinas.checklist.business.CheckSoporteAdmBI;
import com.gruposalinas.checklist.business.ChecklistAdminBI;
import com.gruposalinas.checklist.business.ChecklistAdminValidaBI;
import com.gruposalinas.checklist.business.ChecklistProtocoloBI;
import com.gruposalinas.checklist.business.ChecklistBI;
import com.gruposalinas.checklist.business.ChecksOfflineAdmBI;
import com.gruposalinas.checklist.business.ChecksOfflineBI;
import com.gruposalinas.checklist.business.LeerExcelBI;
import com.gruposalinas.checklist.business.OrdenGrupoBI;
import com.gruposalinas.checklist.business.ParametroBI;
import com.gruposalinas.checklist.business.PosiblesAdmBI;
import com.gruposalinas.checklist.business.PreguntaAdmBI;
import com.gruposalinas.checklist.business.PreguntaBI;
import com.gruposalinas.checklist.business.RepoFilExpBI;
import com.gruposalinas.checklist.business.VersionCheckAdmBI;
import com.gruposalinas.checklist.business.VersionProtoCheckBI;
import com.gruposalinas.checklist.dao.BitacoraCheckUsuAdmDAO;
import com.gruposalinas.checklist.domain.AdmPregZonaDTO;
import com.gruposalinas.checklist.domain.AdmTipoZonaDTO;
import com.gruposalinas.checklist.domain.AdminZonaDTO;
import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.CheckAutoProtoDTO;
import com.gruposalinas.checklist.domain.CheckSoporteAdmDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistLayoutDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.domain.EvidenciaDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.OrdenGrupoDTO;
import com.gruposalinas.checklist.domain.ParametroDTO;
import com.gruposalinas.checklist.domain.PosiblesDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.RespuestaAdDTO;
import com.gruposalinas.checklist.domain.RespuestaDTO;
import com.gruposalinas.checklist.domain.VersionExpanDTO;
import com.gruposalinas.checklist.domain.VersionProtoCheckDTO;
import com.gruposalinas.checklist.domain.ProtocoloDTO;
import com.gruposalinas.checklist.domain.RepoFilExpDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.test.MainTestCarga;
import com.gruposalinas.checklist.util.StrCipher;
import com.gruposalinas.checklist.util.UtilCryptoGS;

@Controller
@RequestMapping("/consultaChecklistAdminService")

public class ConsultaChecklistAdminService {

    @Autowired
    LeerExcelBI leerExcelBI;
    @Autowired
    ChecklistAdminBI checklistAdminBI;
    @Autowired
    PreguntaAdmBI preguntaAdmBI;
    @Autowired
    BitacoraCheckUsuAdmBI bitacoraCheckUsuAdmBI;
    @Autowired
    ArbolRespAdiEviAdmBI arbolRespAdiEviAdmBI;
    @Autowired
    CheckSoporteAdmBI checkSoporteAdmBI;
    @Autowired
    ChecklistProtocoloBI checklistProtocoloBI;
    @Autowired
    CheckAutProtoBI checkAutProtoBI;
    @Autowired
    PosiblesAdmBI posiblesAdmBI;
    @Autowired
    ChecksOfflineAdmBI checksOfflineAdmBI;
    @Autowired
    VersionCheckAdmBI versionCheckAdmBI;
    @Autowired
    ChecklistAdminValidaBI checklistAdminValidaBI;
    @Autowired
    ChecklistAdminValidaBI checklistValidaAdminBI;

    @Autowired
    AdmPregZonasBI admPregZonasBI;
    @Autowired
    AdmZonasBI admZonasBI;

    @Autowired
    AdmTipoZonaBI admTipoZonaBI;
    @Autowired
    OrdenGrupoBI ordenGrupoBI;

    @Autowired
    AdmPregZonasTempBI admPregZonasTempBI;
    
    @Autowired
    VersionProtoCheckBI versionProtoCheckBI;

    @Autowired
    ParametroBI parametrobi;
    private static final Logger logger = LogManager.getLogger(ConsultaChecklistAdminService.class);
    private static final int MAX_FILE_SIZE = 1024 * 1024 * 100;// 100MB

    // http://localhost:8080/checklist/consultaChecklistAdminService/getLeerExcel.json
    @RequestMapping(value = "/getLeerExcel", method = RequestMethod.GET)
    public ModelAndView getCanales(HttpServletRequest request, HttpServletResponse response) {
        try {
            boolean res = leerExcelBI.leerExcell();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", " Archivo leido correctamente : ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info("Paso Algo al leer el archivo excel");
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaChecklistAdminService/insertaModulos.json
    /*@RequestMapping(value = "/insertaModulos", method = RequestMethod.GET)
	public ModelAndView insertaModulos(HttpServletRequest request, HttpServletResponse response) {
		try {

			boolean respuesta = false;

			ArrayList<ChecklistLayoutDTO> listaEntrante = new ArrayList<ChecklistLayoutDTO>();
			listaEntrante = checklistAdminBI.llenaLista();

			// MÓDULOS
			ArrayList<ModuloDTO> listaModulos = new ArrayList<ModuloDTO>();
			listaModulos = checklistAdminBI.getListaModulos(listaEntrante);

			ArrayList<ArrayList<ModuloDTO>> responseInsertaModulos = checklistAdminBI.insertaModulos(listaModulos);

			ArrayList<ModuloDTO> listaModulosInsertados = responseInsertaModulos.get(1);
			ArrayList<ModuloDTO> listaModulosNoInsertados = responseInsertaModulos.get(0);

			// Se insertaron correctamente todos los módulos
			if (listaModulosNoInsertados.size() == 0 && (listaModulosInsertados.size() == listaModulos.size())) {
				respuesta = true;
				// No se insertaron todos los módulos correctamente
			} else {
				respuesta = false;
			}

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "Modulos Cargados correctamente : ");
			mv.addObject("res", respuesta);
			return mv;

		} catch (Exception e) {
			logger.info("AP EN LA CARGA DE MODULOS /consultaChecklistAdminService/insertaModulos" + e);
			return null;
		}
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/insertaPreguntas.json
	@RequestMapping(value = "/insertaPreguntas", method = RequestMethod.GET)
	public ModelAndView insertaPreguntas(HttpServletRequest request, HttpServletResponse response) {
		try {

			boolean respuesta = false;

			ArrayList<ChecklistLayoutDTO> listaEntrante = new ArrayList<ChecklistLayoutDTO>();

			listaEntrante = checklistAdminBI.llenaLista();

			// MÓDULOS
			ArrayList<ModuloDTO> listaModulos = new ArrayList<ModuloDTO>();
			listaModulos = checklistAdminBI.getListaModulos(listaEntrante);

			ArrayList<ArrayList<ModuloDTO>> responseInsertaModulos = checklistAdminBI.insertaModulos(listaModulos);

			ArrayList<ModuloDTO> listaModulosInsertados = responseInsertaModulos.get(1);
			ArrayList<ModuloDTO> listaModulosNoInsertados = responseInsertaModulos.get(0);

			// Se insertaron correctamente todos los módulos
			if (listaModulosNoInsertados.size() == 0 && (listaModulosInsertados.size() == listaModulos.size())) {

				// PREGUNTAS
				ArrayList<PreguntaDTO> listaPreguntas = new ArrayList<PreguntaDTO>();
				//listaPreguntas = checklistAdminBI.getListaPreguntas(listaEntrante, listaModulosInsertados,tipoCarga);

				ArrayList<ArrayList<PreguntaDTO>> responseInsertaPreguntas = checklistAdminBI
						.insertaPreguntas(listaPreguntas);
				ArrayList<PreguntaDTO> listaPreguntasInsertados = responseInsertaPreguntas.get(1);
				ArrayList<PreguntaDTO> listaPreguntasNoInsertados = responseInsertaPreguntas.get(0);

				if (listaPreguntasNoInsertados.size() == 0
						&& (listaPreguntasInsertados.size() == listaPreguntas.size())) {

					respuesta = true;

				} else {

					respuesta = false;
				}

				// No se insertaron todos los módulos correctamente
			} else {
				respuesta = false;

			}

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", " Preguntas Cargadas correctamente : ");
			mv.addObject("res", respuesta);
			return mv;

		} catch (Exception e) {
			logger.info("AP EN LA CARGA DE PREGUNTAS /consultaChecklistAdminService/insertaPreguntas" + e);
			return null;
		}
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/insertaProtocolos.json
	@RequestMapping(value = "/insertaProtocolos", method = RequestMethod.GET)
	public ModelAndView insertaProtocolos(HttpServletRequest request, HttpServletResponse response) {
		try {

			boolean respuesta = false;

			ArrayList<ChecklistLayoutDTO> listaEntrante = new ArrayList<ChecklistLayoutDTO>();

			listaEntrante = checklistAdminBI.llenaLista();

			// MÓDULOS
			ArrayList<ModuloDTO> listaModulos = new ArrayList<ModuloDTO>();
			listaModulos = checklistAdminBI.getListaModulos(listaEntrante);

			ArrayList<ArrayList<ModuloDTO>> responseInsertaModulos = checklistAdminBI.insertaModulos(listaModulos);

			ArrayList<ModuloDTO> listaModulosInsertados = responseInsertaModulos.get(1);
			ArrayList<ModuloDTO> listaModulosNoInsertados = responseInsertaModulos.get(0);

			// Se insertaron correctamente todos los módulos
			if (listaModulosNoInsertados.size() == 0 && (listaModulosInsertados.size() == listaModulos.size())) {

				// PREGUNTAS
				ArrayList<PreguntaDTO> listaPreguntas = new ArrayList<PreguntaDTO>();
				//listaPreguntas = checklistAdminBI.getListaPreguntas(listaEntrante, listaModulosInsertados);

				ArrayList<ArrayList<PreguntaDTO>> responseInsertaPreguntas = checklistAdminBI
						.insertaPreguntas(listaPreguntas);
				ArrayList<PreguntaDTO> listaPreguntasInsertados = responseInsertaPreguntas.get(1);
				ArrayList<PreguntaDTO> listaPreguntasNoInsertados = responseInsertaPreguntas.get(0);

				if (listaPreguntasNoInsertados.size() == 0
						&& (listaPreguntasInsertados.size() == listaPreguntas.size())) {

					respuesta = true;

				} else {

					respuesta = false;
				}

				// No se insertaron todos los módulos correctamente
			} else {
				respuesta = false;

			}

			// INSERCIÓN DE PROTOCOLOS
			if (respuesta) {

				ArrayList<ChecklistProtocoloDTO> listaProtocolos = checklistAdminBI.getListaProtocolos(listaEntrante,
						1);

				ArrayList<ArrayList<ChecklistProtocoloDTO>> responseInsertaProtocolos = checklistAdminBI
						.insertaInfoProtocolos(listaProtocolos, request.getParameter("user"));

				ArrayList<ChecklistProtocoloDTO> listaProtocolosInsertados = responseInsertaProtocolos.get(1);
				ArrayList<ChecklistProtocoloDTO> listaProtocolosNoInsertados = responseInsertaProtocolos.get(0);

				if (listaProtocolosNoInsertados.size() == 0
						&& (listaProtocolosInsertados.size() == listaProtocolos.size())) {

					respuesta = true;

				} else {

					respuesta = false;
				}

			} else {
				logger.info("NO SE INSERTA INFORMACIÓN DE PROTOCOLOS ");
			}

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", " Protocolos Cargadas correctamente : ");
			mv.addObject("res", respuesta);
			return mv;

		} catch (Exception e) {
			logger.info("AP EN LA CARGA DE PROTOCOLOS /consultaChecklistAdminService/insertaProtocolos" + e);
			return null;
		}
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/insertaCheckPreg.json
	@RequestMapping(value = "/insertaCheckPreg", method = RequestMethod.GET)
	public ModelAndView insertaCheckPreg(HttpServletRequest request, HttpServletResponse response) {
		try {

			boolean respuesta = false;

			ArrayList<ChecklistLayoutDTO> listaEntrante = new ArrayList<ChecklistLayoutDTO>();
			ArrayList<PreguntaDTO> listaPreguntas = new ArrayList<PreguntaDTO>();
			ArrayList<ChecklistProtocoloDTO> listaProtocolos = new ArrayList<ChecklistProtocoloDTO>();

			listaEntrante = checklistAdminBI.llenaLista();

			// MÓDULOS
			ArrayList<ModuloDTO> listaModulos = new ArrayList<ModuloDTO>();
			listaModulos = checklistAdminBI.getListaModulos(listaEntrante);

			ArrayList<ArrayList<ModuloDTO>> responseInsertaModulos = checklistAdminBI.insertaModulos(listaModulos);

			ArrayList<ModuloDTO> listaModulosInsertados = responseInsertaModulos.get(1);
			ArrayList<ModuloDTO> listaModulosNoInsertados = responseInsertaModulos.get(0);

			// Se insertaron correctamente todos los módulos
			if (listaModulosNoInsertados.size() == 0 && (listaModulosInsertados.size() == listaModulos.size())) {

				// PREGUNTAS
				//listaPreguntas = checklistAdminBI.getListaPreguntas(listaEntrante, listaModulosInsertados);

				ArrayList<ArrayList<PreguntaDTO>> responseInsertaPreguntas = checklistAdminBI
						.insertaPreguntas(listaPreguntas);
				ArrayList<PreguntaDTO> listaPreguntasInsertados = responseInsertaPreguntas.get(1);
				ArrayList<PreguntaDTO> listaPreguntasNoInsertados = responseInsertaPreguntas.get(0);

				if (listaPreguntasNoInsertados.size() == 0
						&& (listaPreguntasInsertados.size() == listaPreguntas.size())) {

					respuesta = true;

				} else {

					respuesta = false;
				}

				// No se insertaron todos los módulos correctamente
			} else {
				respuesta = false;

			}

			// INSERCIÓN DE PROTOCOLOS
			if (respuesta) {

				listaProtocolos = checklistAdminBI.getListaProtocolos(listaEntrante, 2);

				ArrayList<ArrayList<ChecklistProtocoloDTO>> responseInsertaProtocolos = checklistAdminBI
						.insertaInfoProtocolos(listaProtocolos, request.getParameter("user"));

				ArrayList<ChecklistProtocoloDTO> listaProtocolosInsertados = responseInsertaProtocolos.get(1);
				ArrayList<ChecklistProtocoloDTO> listaProtocolosNoInsertados = responseInsertaProtocolos.get(0);

				if (listaProtocolosNoInsertados.size() == 0
						&& (listaProtocolosInsertados.size() == listaProtocolos.size())) {

					respuesta = true;

				} else {

					respuesta = false;
				}

			} else {
				logger.info("NO SE INSERTA INFORMACIÓN DE PROTOCOLOS ");
			}

			// INSERCIÓN DE CHECKUSUA
			if (respuesta) {

				ArrayList<ChecklistLayoutDTO> infoLayout = checklistAdminBI.setInfoLayout(listaEntrante, listaPreguntas,
						listaProtocolos, 2);

				ArrayList<ArrayList<ChecklistLayoutDTO>> responseInsertaCheckusua = checklistAdminBI
						.insertaCheckusua(infoLayout);

				ArrayList<ChecklistLayoutDTO> listaCheckusuaInsertados = responseInsertaCheckusua.get(1);
				ArrayList<ChecklistLayoutDTO> listaCheckusuaNoinsertados = responseInsertaCheckusua.get(0);

				// Se insertaron correctamente todos los módulos
				if (listaCheckusuaNoinsertados.size() == 0 && (listaCheckusuaInsertados.size() == infoLayout.size())) {
					respuesta = true;

					// No se insertaron todos los módulos correctamente
				} else {
					respuesta = false;
				}

			} else {
				logger.info("NO SE INSERTA INFORMACIÓN DE CHECKUSUA");

			}

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", " Checkusuas Cargadas correctamente : ");
			mv.addObject("res", respuesta);
			return mv;

		} catch (Exception e) {
			logger.info("AP EN LA CARGA DE PROTOCOLOS /consultaChecklistAdminService/insertaCheckusuas -->" + e);
			return null;
		}
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/getModulos.json
	@RequestMapping(value = "/getModulos", method = RequestMethod.GET)
	public ModelAndView getModulos(HttpServletRequest request, HttpServletResponse response) {

		List<ModuloDTO> lista = preguntaAdmBI.obtieneModulo();

		ModelAndView mv = new ModelAndView("muestraServicios");
		mv.addObject("tipo", "GET MODULOS");
		mv.addObject("res", lista);

		return mv;
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/getModulos.json?idModulo=<?>
	@RequestMapping(value = "/getModulosService", method = RequestMethod.GET)
	public @ResponseBody List<ModuloDTO> getModuloService(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idModulo = request.getParameter("idModulo");

			List<ModuloDTO> lista = preguntaAdmBI.obtieneModulo();

			return lista;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/getChecklist.json
	@RequestMapping(value = "/getChecklist", method = RequestMethod.GET)
	public ModelAndView getChecklist(HttpServletRequest request, HttpServletResponse response) {

		List<ChecklistDTO> lista = preguntaAdmBI.buscaChecklist();

		ModelAndView mv = new ModelAndView("muestraServicios");
		// ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

		mv.addObject("tipo", "GET CHECKLIST");
		mv.addObject("res", lista);

		return mv;
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/getPreguntas.json
	@RequestMapping(value = "/getPreguntas", method = RequestMethod.GET)
	public ModelAndView getPreguntas(HttpServletRequest request, HttpServletResponse response) {

		List<PreguntaDTO> lista = preguntaAdmBI.obtienePregunta();

		ModelAndView mv = new ModelAndView("muestraServicios");
		mv.addObject("tipo", "GET PREGUNTAS");
		mv.addObject("res", lista);

		return mv;
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/getCheckPreguntaService.json?idCheck=<?>
	@RequestMapping(value = "/getCheckPreguntaService", method = RequestMethod.GET)
	public @ResponseBody List<ChecklistPreguntaDTO> getCheckPreguntaService(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String idCheck = request.getParameter("idCheck");

			List<ChecklistPreguntaDTO> lista = preguntaAdmBI.obtienePregXcheck(Integer.parseInt(idCheck));

			return lista;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://10.51.219.179:8080/checklist/consultaChecklistAdminService/getBitacora.json
	@RequestMapping(value = "/getBitacora", method = RequestMethod.GET)
	public ModelAndView getBitacora(HttpServletRequest request, HttpServletResponse response) {

		List<BitacoraDTO> lista = bitacoraCheckUsuAdmBI.buscaBitacora();

		// System.out.println(lista);

		ModelAndView mv = new ModelAndView("muestraServicios");
		mv.addObject("tipo", "LISTA BITACORA");
		mv.addObject("res", lista);

		return mv;
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/getBitacoraG.json?idCheckU=<?>&idBitacora=<?>&fechaI=<?>&fechaF=<?>
	@RequestMapping(value = "/getBitacoraG", method = RequestMethod.GET)
	public ModelAndView getBitacoraG(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idCheckU = request.getParameter("idCheckU");
			String idBitacora = request.getParameter("idBitacora");
			String fechaI = request.getParameter("fechaI");
			String fechaF = request.getParameter("fechaF");

			List<BitacoraDTO> lista = bitacoraCheckUsuAdmBI.buscaBitacora(idCheckU, idBitacora, fechaI, fechaF);

			// System.out.println(lista);

			ModelAndView mv = new ModelAndView("muestraServicios");
			mv.addObject("tipo", "LISTA BITACORA");
			mv.addObject("res", lista);

			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/getBitacoraCerradas.json?idChecklist=<?>
	@RequestMapping(value = "/getBitacoraCerradas", method = RequestMethod.GET)
	public ModelAndView getBitacoraCerradas(HttpServletRequest request, HttpServletResponse response) {

		String idChecklist = request.getParameter("idChecklist");

		List<BitacoraDTO> lista = bitacoraCheckUsuAdmBI.buscaBitacoraCerradas(idChecklist);

		// System.out.println(lista);

		ModelAndView mv = new ModelAndView("muestraServicios");
		mv.addObject("tipo", "BITACORA CERRADA");
		mv.addObject("res", lista);

		return mv;
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/getArbolDesicion.json?idCheck=<?>
	@RequestMapping(value = "/getArbolDesicion", method = RequestMethod.GET)
	public ModelAndView getArbolDesicion(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idCheck = request.getParameter("idCheck");

			List<ArbolDecisionDTO> lista = arbolRespAdiEviAdmBI.buscaArbolDecision(Integer.parseInt(idCheck));

			ModelAndView mv = new ModelAndView("muestraServicios");
			mv.addObject("tipo", "ARBOL DESICION");
			mv.addObject("res", lista);

			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/getArbolDesicionService.json?idCheck=<?>
	@RequestMapping(value = "/getArbolDesicionService", method = RequestMethod.GET)
	public @ResponseBody List<ArbolDecisionDTO> getArbolDesicionService(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String idCheck = request.getParameter("idCheck");
			List<ArbolDecisionDTO> lista = arbolRespAdiEviAdmBI.buscaArbolModificaciones(Integer.parseInt(idCheck));
			return lista;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/getRespuestas.json
	@RequestMapping(value = "/getRespuestas", method = RequestMethod.GET)
	public ModelAndView getRespuestas(HttpServletRequest request, HttpServletResponse response) {

		List<RespuestaDTO> lista = arbolRespAdiEviAdmBI.obtieneRespuesta();

		ModelAndView mv = new ModelAndView("muestraServicios");
		mv.addObject("tipo", "LISTA RESPUESTAS");
		mv.addObject("res", lista);

		return mv;
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/getRespuestasG.json?idRespuesta=<?>&idBitacora=<?>&idArbol=<?>
	@RequestMapping(value = "/getRespuestasG", method = RequestMethod.GET)
	public ModelAndView getRespuestasG(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idBitacora = request.getParameter("idBitacora");
			String idRespuesta = request.getParameter("idRespuesta");
			String idArbol = request.getParameter("idArbol");

			logger.info(idArbol + " " + idRespuesta + " " + idBitacora + "  dd ");

			List<RespuestaDTO> lista = arbolRespAdiEviAdmBI.obtieneRespuesta(idArbol, idRespuesta, idBitacora);

			ModelAndView mv = new ModelAndView("muestraServicios");
			mv.addObject("tipo", "LISTA RESPUESTAS");
			mv.addObject("res", lista);

			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/getRespuestasAd.json?idRespuesta=<?>&idRespuestaAd=<?>
	@RequestMapping(value = "/getRespuestasAd", method = RequestMethod.GET)
	public ModelAndView getRespuestasAd(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idRespuesta = request.getParameter("idRespuesta");
			String idRespuestaAd = request.getParameter("idRespuestaAd");

			List<RespuestaAdDTO> lista = arbolRespAdiEviAdmBI.buscaRespADP(idRespuestaAd, idRespuesta);

			ModelAndView mv = new ModelAndView("muestraServicios");
			mv.addObject("tipo", "GET RESPUESTAS ADICIONALES");
			mv.addObject("res", lista);

			return mv;
		} catch (Exception e) {
			logger.info("Ocurrio algo " + e);
			return null;
		}
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/getEvidencias.json
	@RequestMapping(value = "/getEvidencias", method = RequestMethod.GET)
	public ModelAndView getEvidencias(HttpServletRequest request, HttpServletResponse response) {

		List<EvidenciaDTO> lista = arbolRespAdiEviAdmBI.obtieneEvidencia();

		ModelAndView mv = new ModelAndView("muestraServicios");
		mv.addObject("tipo", "LISTA EVIDENCIAS");
		mv.addObject("res", lista);

		return mv;
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/getGrupoCheck.json?idGrup=
	@RequestMapping(value = "/getGrupoCheck", method = RequestMethod.GET)
	public ModelAndView getGrupoCheck(HttpServletRequest request, HttpServletResponse response) {

		String idGrup = request.getParameter("idGrup");
		List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkAutorizacion(idGrup);

		ModelAndView mv = new ModelAndView("muestraServicios");
		mv.addObject("tipo", "CHECKSOPO");
		mv.addObject("res", lista);

		return mv;
	}

	// http://localhost:8080/checklist/consultaChecklistAdminService/getChecksActiv.json?idGrup=
	@RequestMapping(value = "/getChecksActiv", method = RequestMethod.GET)
	public ModelAndView getChecksActiv(HttpServletRequest request, HttpServletResponse response) {

		String idGrup = request.getParameter("idGrup");
		List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkActivos(idGrup);

		ModelAndView mv = new ModelAndView("muestraServicios");
		mv.addObject("tipo", "CHECKSOPO");
		mv.addObject("res", lista);

		return mv;
	}*/
    // http://localhost:8080/checklist/consultaChecklistAdminService/postLeerExcell.json
    @RequestMapping(value = "/postLeerExcell", method = RequestMethod.POST)
    public @ResponseBody
    String getCatalogoProtocolos(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "uploadedFile", required = false, defaultValue = "") MultipartFile uploadedFile,
            @RequestParam(value = "tipoCarga", required = false, defaultValue = "") String tipo)
            throws KeyException, GeneralSecurityException, IOException {

        logger.info("Entra a Asigna Cecos Masiva Post");
        String salida = "adminChecklist";
        ModelAndView mv = new ModelAndView(salida, "command", null);
        String jsonSalida = null;
        int tipoCarga = 2;

        try {

            if (tipo.equals("Infraestructura")) {
                tipoCarga = 1;
            }
            String fileName = uploadedFile.getOriginalFilename();
            logger.info("Name: " + fileName);

            File tmp = new File(fileName);
            if (!tmp.exists()) {
                uploadedFile.transferTo(tmp);
            }
            String rootPath = File.listRoots()[0].getAbsolutePath();
            logger.info("rootPath: " + rootPath);
            // fileRoute="/franquicia/imagenes";

            File file = new File(fileName);
            logger.info("filePath: " + file.getAbsolutePath());

            if (file.exists()) {
                if (uploadedFile.getSize() < MAX_FILE_SIZE) {

                    boolean respuesta = false;

                    ArrayList<PreguntaDTO> listaPreguntas = new ArrayList<PreguntaDTO>();
                    ArrayList<ChecklistProtocoloDTO> listaProtocolos = new ArrayList<ChecklistProtocoloDTO>();
                    // leer archivo excel
                    String parametroResult = "0";
                    
                    try {
                	
                    	parametroResult = parametrobi.obtieneParametros("tipoLecturaExcel").get(0).getValor();
                	
                    } catch (Exception e) {
                    	
                    	logger.info("AP con el parámetro tipoLecturaExcel");
                    	parametroResult = "0";
                    	
                    }
                	
                    ArrayList<ChecklistLayoutDTO> listaEntrante = new ArrayList<ChecklistLayoutDTO>();
                    		
                    if (parametroResult.compareToIgnoreCase("0") == 0) {
                    	
                        listaEntrante = checklistAdminBI.leerExcell(file);
                        
                    } else {
                    	if(tipoCarga==2) {
                    		listaEntrante = checklistAdminBI.leerExcell(file);
                    	}else {
                    		listaEntrante = checklistAdminBI.leerExcellMejorado(file);
                    	}
                    } 

                    logger.info("El archivo: " + listaEntrante.toString());
                    jsonSalida = checklistAdminBI.retornaJson(listaEntrante, tipoCarga);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            jsonSalida = "{}";
        }

        return jsonSalida;
    }

    // http://localhost:8080/checklist/consultaChecklistAdminService/postAltaProtocolosPROD.json
    @RequestMapping(value = "/postAltaProtocolosPROD", method = RequestMethod.POST)
    public @ResponseBody
    String postAltaProtocolos(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "JSON", required = false, defaultValue = "") String JSON,
            @RequestParam(value = "tipoCarga", required = false, defaultValue = "") String tipo)
            throws KeyException, GeneralSecurityException, IOException {

        logger.info("Entra a Asigna Cecos Masiva Post");
        String salida = "adminChecklist";
        ModelAndView mv = new ModelAndView(salida, "command", null);
        String jsonSalida = null;
        int tipoCarga = 2;

        try {

            if (tipo.equals("Infraestructura")) {
                tipoCarga = 1;
            }
            boolean respuesta = false;
            ArrayList<PreguntaDTO> listaPreguntas = new ArrayList<PreguntaDTO>();
            ArrayList<ChecklistLayoutDTO> listaEntrante = checklistAdminBI.leerJson(JSON);
            ArrayList<ChecklistProtocoloDTO> listaProtocolos = new ArrayList<ChecklistProtocoloDTO>();

            logger.info("El archivo: " + listaEntrante.toString());

            // MÓDULOS
            ArrayList<ModuloDTO> listaModulos = new ArrayList<ModuloDTO>();
            listaModulos = checklistAdminBI.getListaModulos(listaEntrante);

            ArrayList<ArrayList<ModuloDTO>> responseInsertaModulos = checklistAdminBI.insertaModulos(listaModulos);

            ArrayList<ModuloDTO> listaModulosInsertados = responseInsertaModulos.get(1);
            ArrayList<ModuloDTO> listaModulosNoInsertados = responseInsertaModulos.get(0);

            // Se insertaron correctamente todos los módulos
            if (listaModulosNoInsertados.size() == 0 && (listaModulosInsertados.size() == listaModulos.size())) {

                // PREGUNTAS
                listaPreguntas = checklistAdminBI.getListaPreguntas(listaEntrante, listaModulosInsertados, tipoCarga);

                ArrayList<ArrayList<PreguntaDTO>> responseInsertaPreguntas = checklistAdminBI
                        .insertaPreguntas(listaPreguntas, tipoCarga);
                ArrayList<PreguntaDTO> listaPreguntasInsertados = responseInsertaPreguntas.get(1);
                ArrayList<PreguntaDTO> listaPreguntasNoInsertados = responseInsertaPreguntas.get(0);

                if (listaPreguntasNoInsertados.size() == 0
                        && (listaPreguntasInsertados.size() == listaPreguntas.size())) {

                    respuesta = true;

                } else {

                    respuesta = false;
                }

                // No se insertaron todos los módulos correctamente
            } else {
                respuesta = false;

            }

            // INSERCIÓN DE PROTOCOLOS
            if (respuesta) {

                listaProtocolos = checklistAdminBI.getListaProtocolos(listaEntrante, tipoCarga);

                ArrayList<ArrayList<ChecklistProtocoloDTO>> responseInsertaProtocolos = checklistAdminBI
                        .insertaInfoProtocolos(listaProtocolos, request.getParameter("user"));

                ArrayList<ChecklistProtocoloDTO> listaProtocolosInsertados = responseInsertaProtocolos.get(1);
                ArrayList<ChecklistProtocoloDTO> listaProtocolosNoInsertados = responseInsertaProtocolos.get(0);

                if (listaProtocolosNoInsertados.size() == 0
                        && (listaProtocolosInsertados.size() == listaProtocolos.size())) {

                    respuesta = true;

                } else {

                    respuesta = false;
                }

            } else {
                logger.info("NO SE INSERTA INFORMACIÓN DE PROTOCOLOS ");
            }

            // INSERCIÓN DE CHECKUSUA
            if (respuesta) {

                ArrayList<ChecklistLayoutDTO> infoLayout = checklistAdminBI.setInfoLayout(listaEntrante, listaPreguntas,
                        listaProtocolos, tipoCarga);

                logger.info("infoLayout" + infoLayout.size());

                ArrayList<ArrayList<ChecklistLayoutDTO>> responseInsertaCheckusua = checklistAdminBI
                        .insertaCheckusua(infoLayout);

                ArrayList<ChecklistLayoutDTO> listaCheckusuaInsertados = responseInsertaCheckusua.get(1);
                ArrayList<ChecklistLayoutDTO> listaCheckusuaNoinsertados = responseInsertaCheckusua.get(0);

                // Se insertaron correctamente todos los módulos
                if (listaCheckusuaNoinsertados.size() == 0 && (listaCheckusuaInsertados.size() == infoLayout.size())) {
                    respuesta = true;
                    listaEntrante = responseInsertaCheckusua.get(1);
                    // No se insertaron todos los módulos correctamente
                } else {
                    respuesta = false;

                }

            } else {
                logger.info("NO SE INSERTA INFORMACIÓN DE CHECKUSUA");

            }

            // ---------ALTA ARBOL DE DESICION-------------
            if (respuesta) {

                ArrayList<ChecklistLayoutDTO> listaArbol = checklistAdminBI.getListaArbol(listaEntrante, tipoCarga);

                // ArrayList<ChecklistLayoutDTO> listaArbolSalida =
                // checklistAdminBI.getListaArbol(listaEntrante, 2);
                ArrayList<ChecklistLayoutDTO> listaArbolSalida = checklistAdminBI.altaArbol(listaArbol, tipoCarga);

                String preguntasError = "";
                for (ChecklistLayoutDTO layout : listaArbolSalida) {
                    if (layout.getFlagErrorArbol() != 0) {
                        preguntasError = preguntasError + layout.getIdConsecutivo() + ", ";
                    }

                }
                if (preguntasError.equals("")) {
                    jsonSalida = "{\"ALTA\":true}";
                    respuesta = true;

                } else {
                    logger.info("PREGUNTAS CON ERROR AL CREAR ARBOL: " + preguntasError);
                    respuesta = false;

                    //HACEMOS ROLL BACK
                    boolean resp = arbolRespAdiEviAdmBI.depuraTabParam(listaProtocolos.get(0).getIdChecklist());
                    if (!resp) {
                        arbolRespAdiEviAdmBI.depuraTabParam(listaProtocolos.get(0).getIdChecklist());
                    }

                }

            } else {
                logger.info("NO SE INSERTA LOS ÁRBOLES ");

                //HACEMOS ROLL BACK
                boolean resp = arbolRespAdiEviAdmBI.depuraTabParam(listaProtocolos.get(0).getIdChecklist());
                if (!resp) {
                    arbolRespAdiEviAdmBI.depuraTabParam(listaProtocolos.get(0).getIdChecklist());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            jsonSalida = "{}";
        }

        return jsonSalida;
    }

    // 321(INFRAESTRUCTURA) Y 323(SUPERVISION)
    // http://localhost:8080/checklist/consultaChecklistAdminService/getGrupoProtocolosActivos.json?idGrupo=<?>
    @RequestMapping(value = "/getGrupoProtocolosActivos", method = RequestMethod.GET)
    public @ResponseBody
    String getGrupoProtocolosActivos(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "idGrupo", required = false, defaultValue = "") String idGrupo)
            throws KeyException, GeneralSecurityException, IOException {

        String json = "";

        //FALTA EL BI QUE OBTIENE LISTA DE PROTOCOLOS DE TABLAS DE PRODUCCIÓN
        List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkVersParamProd("0", idGrupo);

        try {

            Gson gson = null;
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.serializeNulls();
            gson = gsonBuilder.create();
            json = gson.toJson(lista);

        } catch (Exception e) {
            return "{ }";
        }
        return json;
    }

    // http://localhost:8080/checklist/consultaChecklistAdminService/getGrupoProtocolosRevision.json?idGrupo=<?>
    @RequestMapping(value = "/getGrupoProtocolosRevision", method = RequestMethod.GET)
    public @ResponseBody
    String getGrupoProtocolosRevision(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "idGrupo", required = false, defaultValue = "") String idGrupo)
            throws KeyException, GeneralSecurityException, IOException {

        String json = "";

        //OBTIENE LISTA DE PROTOCOLOS DE TABLAS DE VALIDACIÓN
        List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkVersParam("0", idGrupo);

        try {

            Gson gson = null;
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.serializeNulls();
            gson = gsonBuilder.create();
            json = gson.toJson(lista);

        } catch (Exception e) {
            return "{ }";
        }

        return json;
    }

    // http://localhost:8080/checklist/consultaChecklistAdminService/getOfflineValidacion.json
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getOfflineValidacion", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getOfflineValidacion(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        int idUsuario = 0;
        Map<String, Object> listasOffline = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                idUsuario = Integer.parseInt(urides.split("=")[1]);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                idUsuario = Integer.parseInt(urides.split("=")[1]);
            }

            listasOffline = checksOfflineAdmBI.obtieneChecksNuevo(idUsuario);

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
            listasOffline = null;
        }

        //logger.info("Json getOffline2: "+listasOffline);
        return listasOffline;
    }

    //http://10.51.218.72:8080/checklist/consultaChecklistAdminService/getRetornaProtocoloBase.json?idProtocolo=&tipoCarga=
    @RequestMapping(value = "/getRetornaProtocoloBase", method = RequestMethod.GET)
    public ModelAndView getRetornaProtocoloBase(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        try {
            int idProtocolo = Integer.parseInt(request.getParameter("idProtocolo"));
            int tipoCarga = Integer.parseInt(request.getParameter("tipoCarga"));
            ChecklistProtocoloDTO checklistProtocolo = new ChecklistProtocoloDTO();
            checklistProtocolo.setIdChecklist(idProtocolo);

            String lista = checklistAdminBI.retornaProtocoloBase(idProtocolo, tipoCarga);
            //List<ChecklistProtocoloDTO> lista = checklistProtocoloBI.buscaChecklist(checklistProtocolo);
            mv.addObject("tipo", "PROTOCOLO_BASE");
            mv.addObject("res", lista);

        } catch (Exception e) {
            logger.info(e);
        }
        return mv;
    }

    //http://10.51.218.72:8080/checklist/consultaChecklistAdminService/postRetornaProtocoloBase.json?idProtocolo=&tipoCarga=
    @RequestMapping(value = "/postRetornaProtocoloBase", method = RequestMethod.POST)
    public @ResponseBody
    String postRetornaProtocoloBase(HttpServletRequest request, HttpServletResponse response) {
        //ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        String resultado = null;
        try {
            int idProtocolo = Integer.parseInt(request.getParameter("idProtocolo"));
            int tipoCarga = Integer.parseInt(request.getParameter("tipoCarga"));
            ChecklistProtocoloDTO checklistProtocolo = new ChecklistProtocoloDTO();
            checklistProtocolo.setIdChecklist(idProtocolo);

            String lista = checklistAdminBI.retornaJson(checklistAdminBI.retornaLayoutBase(idProtocolo, tipoCarga), tipoCarga);;
            //List<ChecklistProtocoloDTO> lista = checklistProtocoloBI.buscaChecklist(checklistProtocolo);
            //mv.addObject("tipo", "PROTOCOLO_BASE");
            //mv.addObject("res", lista);
            resultado = lista;

        } catch (Exception e) {
            logger.info(e);
            resultado = null;
        }
        return resultado;
    }

    // http://localhost:8080/checklist/consultaChecklistAdminService/getCheckAuto.json?idCheck=<?>
    @RequestMapping(value = "/getCheckAuto", method = RequestMethod.GET)
    public ModelAndView getCheckAuto(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCheck = "";
            idCheck = request.getParameter("idCheck");

            List<CheckAutoProtoDTO> lista = checkAutProtoBI.obtieneAutoProto(idCheck);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "AUTOCHECK");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaChecklistAdminService/getPosibles.json
    @RequestMapping(value = "/getPosibles", method = RequestMethod.GET)
    public ModelAndView getPosibles(HttpServletRequest request, HttpServletResponse response) {

        try {

            List<PosiblesDTO> res = posiblesAdmBI.buscaPosible();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "POSIBLES ");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaChecklistAdminService/getVersCheckAdm.json?idVers=<?>
    @RequestMapping(value = "/getVersCheckAdm", method = RequestMethod.GET)
    public ModelAndView getVersCheckAdm(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idVers = "";
            idVers = request.getParameter("idVers");

            List<VersionExpanDTO> lista = versionCheckAdmBI.obtieneDatos(idVers);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "VERSIONEXP");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
    // http://localhost:8080/checklist/consultaChecklistAdminService/getVersNegoAdm.json?idVers=<?>

    @RequestMapping(value = "/getVersNegoAdm", method = RequestMethod.GET)
    public ModelAndView getVersNegoAdm(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idVers = "";
            idVers = request.getParameter("idVers");

            List<VersionExpanDTO> lista = versionCheckAdmBI.obtieneDatosNegoVer(idVers);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "VERSIONEXP");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/checklist/consultaChecklistAdminService/getVersChecksSop.json?param=&idGrupo=
    @RequestMapping(value = "/getVersChecksSop", method = RequestMethod.GET)
    public ModelAndView getVersChecksSop(HttpServletRequest request, HttpServletResponse response) {

        String param = request.getParameter("param");
        String idGrupo = request.getParameter("idGrupo");
        List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkVersParam(param, idGrupo);

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "VERCHECKSOP");
        mv.addObject("res", lista);

        return mv;
    }

    //http://10.51.218.72:8080/checklist/consultaChecklistAdminService/getRetornaProtocoloBaseProduccion.json?idProtocolo=&tipoCarga=
    @RequestMapping(value = "/getRetornaProtocoloBaseProduccion", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String getRetornaProtocoloBaseProduccion(HttpServletRequest request, HttpServletResponse response) {
        //ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        String resultado = null;
        try {
            int idProtocolo = Integer.parseInt(request.getParameter("idProtocolo"));
            int tipoCarga = Integer.parseInt(request.getParameter("tipoCarga"));
            
            
            ChecklistProtocoloDTO checklistProtocolo = new ChecklistProtocoloDTO();
            checklistProtocolo.setIdChecklist(idProtocolo);
            HashMap<String, String> objDatosGenerales = new HashMap();

            //INFRAESTRUCUTURA
            if (tipoCarga == 1) {
                //Obtengo lista de id´s de Checklist de esa Versión
                List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkVersParamProd(idProtocolo + "", "");
                ArrayList<String> listaId = new ArrayList<String>();

                for (CheckSoporteAdmDTO objLista : lista) {
                    listaId.add(objLista.getIdChecklist() + "");
                }

                objDatosGenerales.put("no", lista.get(0).getIdtab() + "");
                objDatosGenerales.put("nombre", lista.get(0).getNego() + "");
                objDatosGenerales.put("tipo_Protocolo", "Infraestructura");

                resultado = this.getJsonFinalProduccion(listaId, 1, objDatosGenerales);

            } else {
                //SUPERVISION

                //Obtengo lista de id´s de Checklist de esa Versión 122
                List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkVersParamProd(idProtocolo + "", "");
                ArrayList<String> listaId = new ArrayList<String>();

                for (CheckSoporteAdmDTO objLista : lista) {
                    listaId.add(objLista.getIdChecklist() + "");
                }

                objDatosGenerales.put("no", lista.get(0).getIdtab() + "");
                objDatosGenerales.put("nombre", lista.get(0).getNego() + "");
                objDatosGenerales.put("tipo_protocolo", "Aseguramiento de Calidad");

                resultado = this.getJsonFinalProduccion(listaId, 0, objDatosGenerales);
            }

        } catch (Exception e) {
            logger.info(e);
            resultado = null;
        }
        return resultado;
    }

    public String getJsonFinalProduccion(ArrayList<String> listaChecklists, int tipo, HashMap<String, String> objDatosGenerales) throws JSONException {

        JSONObject json = new JSONObject();
        JSONArray secciones = new JSONArray();

        JSONObject datosGenerales = new JSONObject();
        datosGenerales.put("no", objDatosGenerales.get("no"));
        datosGenerales.put("nombre", objDatosGenerales.get("nombre"));
        datosGenerales.put("tipo_protocolo", objDatosGenerales.get("tipo_protocolo"));
        //Consulta BI DETALLE

        if (tipo == 1) {

            for (String idCheck : listaChecklists) {
                String jsonString = checklistAdminBI.retornaJson(checklistAdminBI.retornaLayoutBase(Integer.parseInt(idCheck), 1), 1);
                JSONObject jsonObj = new JSONObject(jsonString);
                JSONObject seccion = (JSONObject) jsonObj.getJSONArray("secciones").get(0);
                secciones.put(seccion);
            }

        } else {

            for (String idCheck : listaChecklists) {
                String jsonString = checklistAdminBI.retornaJson(checklistAdminBI.retornaLayoutBase(Integer.parseInt(idCheck), 0), 2);
                logger.info(jsonString);
                JSONObject jsonObj = new JSONObject(jsonString);
                JSONArray arraySecciones = jsonObj.getJSONArray("secciones");

                for (int i = 0; i < arraySecciones.length(); i++) {

                    secciones.put(arraySecciones.get(i));

                }
            }
        }

        json.put("secciones", secciones);
        json.put("datos_generales", datosGenerales);

        return json.toString();

    }

    //http://10.51.218.72:8080/checklist/consultaChecklistAdminService/getRetornaProtocoloBaseTest.json?idProtocolo=&tipoCarga=
    @RequestMapping(value = "/getRetornaProtocoloBaseTest", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String getRetornaProtocoloBaseTest(HttpServletRequest request, HttpServletResponse response) {
        //ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        String resultado = null;
        try {
            int idProtocolo = Integer.parseInt(request.getParameter("idProtocolo"));
            int tipoCarga = Integer.parseInt(request.getParameter("tipoCarga"));
            ChecklistProtocoloDTO checklistProtocolo = new ChecklistProtocoloDTO();
            checklistProtocolo.setIdChecklist(idProtocolo);
            HashMap<String, String> objDatosGenerales = new HashMap();

            //INFRAESTRUCUTURA
            if (tipoCarga == 1) {

                //Obtengo lista de id´s de Checklist de esa Versión
                List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkVersParam(idProtocolo + "", "1");
                ArrayList<String> listaId = new ArrayList<String>();

                for (CheckSoporteAdmDTO objLista : lista) {
                    listaId.add(objLista.getIdChecklist() + "");
                }

                objDatosGenerales.put("no", lista.get(0).getIdtab() + "");
                objDatosGenerales.put("nombre", lista.get(0).getNego() + "");
                objDatosGenerales.put("tipo_protocolo", "Infraestructura");

                resultado = this.getJsonFinalTest(listaId, 1, objDatosGenerales);

            } else {
                //SUPERVISION

                //Obtengo lista de id´s de Checklist de esa Versión
                List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkVersParam(idProtocolo + "", "2");
                ArrayList<String> listaId = new ArrayList<String>();

                for (CheckSoporteAdmDTO objLista : lista) {
                    listaId.add(objLista.getIdChecklist() + "");
                }

                objDatosGenerales.put("no", lista.get(0).getIdtab() + "");
                objDatosGenerales.put("nombre", lista.get(0).getNego() + "");
                objDatosGenerales.put("tipo_Protocolo", "Aseguramiento de Calidad");

                resultado = this.getJsonFinalTest(listaId, 0, objDatosGenerales);
            }

        } catch (Exception e) {
            logger.info(e);
            resultado = null;
        }
        return resultado;
    }

    public String getJsonFinalTest(ArrayList<String> listaChecklists, int tipo, HashMap<String, String> objDatosGenerales) throws JSONException {

        JSONObject json = new JSONObject();
        JSONArray secciones = new JSONArray();

        JSONObject datosGenerales = new JSONObject();
        datosGenerales.put("no", objDatosGenerales.get("no"));
        datosGenerales.put("nombre", objDatosGenerales.get("nombre"));
        datosGenerales.put("tipo_protocolo", objDatosGenerales.get("tipo_protocolo"));

        //Consulta BI DETALLE
        if (tipo == 1) {

            for (String idCheck : listaChecklists) {
                String jsonString = checklistAdminValidaBI.retornaJson(checklistAdminValidaBI.retornaLayoutBase(Integer.parseInt(idCheck), 1), 1);
                JSONObject jsonObj = new JSONObject(jsonString);
                JSONObject seccion = (JSONObject) jsonObj.getJSONArray("secciones").get(0);
                secciones.put(seccion);

            }

        } else {

            for (String idCheck : listaChecklists) {
                String jsonString = checklistAdminValidaBI.retornaJson(checklistAdminValidaBI.retornaLayoutBase(Integer.parseInt(idCheck), 2), 2);
                JSONObject jsonObj = new JSONObject(jsonString);
                JSONArray arraySecciones = jsonObj.getJSONArray("secciones");

                for (int i = 0; i < arraySecciones.length(); i++) {

                    secciones.put(arraySecciones.get(i));

                }

            }
        }

        json.put("secciones", secciones);
        json.put("datos_generales", datosGenerales);

        return json.toString();
    }
    // http://localhost:8080/checklist/consultaChecklistAdminService/getVersChecksSopProd.json?param=&idGrupo=

    @RequestMapping(value = "/getVersChecksSopProd", method = RequestMethod.GET)
    public ModelAndView getVersChecksSopProd(HttpServletRequest request, HttpServletResponse response) {

        String param = request.getParameter("param");
        String idGrupo = request.getParameter("idGrupo");
        List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkVersParamProd(param, idGrupo);

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "VERCHECKSOP");
        mv.addObject("res", lista);

        return mv;
    }

    //http://10.51.218.72:8080/checklist/consultaChecklistAdminService/getCargaProtocolosManual.json?
    @RequestMapping(value = "/getCargaProtocolosManual", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String getCargaProtocolosManual(HttpServletRequest request, HttpServletResponse response) {
        //ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        String resultado = null;
        try {

            resultado = "" + checklistAdminBI.ejecutaCargaProd();

        } catch (Exception e) {
            logger.info(e);
            resultado = null;
        }
        return resultado;
    }

    // http://localhost:8080/checklist/consultaChecklistAdminService/postAltaProtocolos.json
    @RequestMapping(value = "/postAltaProtocolos", method = RequestMethod.POST)
    public @ResponseBody
    String postAltaProtocolosADM(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "JSON", required = false, defaultValue = "") String JSON,
            @RequestParam(value = "tipoCarga", required = false, defaultValue = "") String tipo)
            throws KeyException, GeneralSecurityException, IOException {

        logger.info("Entra a Asigna Cecos Masiva Post");
        System.out.println("CARGA PROTOCOLO DESDE PLATAFORMA");
        String salida = "adminChecklist";
        ModelAndView mv = new ModelAndView(salida, "command", null);
        String jsonSalida = null;
        int tipoCarga = 2;

        try {

            if (tipo.equals("Infraestructura")) {
                tipoCarga = 1;
            }
            boolean respuesta = false;
            ArrayList<PreguntaDTO> listaPreguntas = new ArrayList<PreguntaDTO>();
            ArrayList<ChecklistLayoutDTO> listaEntrante = checklistAdminBI.leerJson(JSON);
            ArrayList<ChecklistProtocoloDTO> listaProtocolos = new ArrayList<ChecklistProtocoloDTO>();

            logger.info("El archivo: " + listaEntrante.toString());

            // MÓDULOS
            ArrayList<ModuloDTO> listaModulos = new ArrayList<ModuloDTO>();
            listaModulos = checklistValidaAdminBI.getListaModulos(listaEntrante);

            ArrayList<ArrayList<ModuloDTO>> responseInsertaModulos = checklistValidaAdminBI.insertaModulos(listaModulos);

            ArrayList<ModuloDTO> listaModulosInsertados = responseInsertaModulos.get(1);
            ArrayList<ModuloDTO> listaModulosNoInsertados = responseInsertaModulos.get(0);

            // Se insertaron correctamente todos los módulos
            if (listaModulosNoInsertados.size() == 0 && (listaModulosInsertados.size() == listaModulos.size())) {

                // PREGUNTAS
                listaPreguntas = checklistValidaAdminBI.getListaPreguntas(listaEntrante, listaModulosInsertados, tipoCarga);

                ArrayList<ArrayList<PreguntaDTO>> responseInsertaPreguntas = checklistValidaAdminBI
                        .insertaPreguntas(listaPreguntas, tipoCarga);
                ArrayList<PreguntaDTO> listaPreguntasInsertados = responseInsertaPreguntas.get(1);
                ArrayList<PreguntaDTO> listaPreguntasNoInsertados = responseInsertaPreguntas.get(0);

                if (listaPreguntasNoInsertados.size() == 0
                        && (listaPreguntasInsertados.size() == listaPreguntas.size())) {

                    respuesta = true;

                } else {

                    respuesta = false;
                }

                // No se insertaron todos los módulos correctamente
            } else {
                respuesta = false;

            }

            //INSERCIÓN DE PROTOCOLOS
            ArrayList<ChecklistProtocoloDTO> listaProtocolosInsertados = null;
            if (respuesta) {

                listaProtocolos = checklistValidaAdminBI.getListaProtocolos(listaEntrante, tipoCarga);

                ArrayList<ArrayList<ChecklistProtocoloDTO>> responseInsertaProtocolos = checklistValidaAdminBI.insertaInfoProtocolos(listaProtocolos, request.getParameter("user"));

                listaProtocolosInsertados = responseInsertaProtocolos.get(1);
                ArrayList<ChecklistProtocoloDTO> listaProtocolosNoInsertados = responseInsertaProtocolos.get(0);

                if (listaProtocolosNoInsertados.size() == 0 && (listaProtocolosInsertados.size() == listaProtocolos.size())) {

                    respuesta = true;

                } else {

                    respuesta = false;
                }

            } else {
                logger.info("NO SE INSERTA INFORMACIÓN DE PROTOCOLOS ");
            }

            //INSERCIÓN DE CHECKUSUA
            if (respuesta) {

                ArrayList<ChecklistLayoutDTO> infoLayout = checklistValidaAdminBI.setInfoLayout(listaEntrante, listaPreguntas, listaProtocolos, tipoCarga);

                logger.info("infoLayout" + infoLayout.size());

                ArrayList<ArrayList<ChecklistLayoutDTO>> responseInsertaCheckusua = checklistValidaAdminBI.insertaCheckusua(infoLayout);

                ArrayList<ChecklistLayoutDTO> listaCheckusuaInsertados = responseInsertaCheckusua.get(1);
                ArrayList<ChecklistLayoutDTO> listaCheckusuaNoinsertados = responseInsertaCheckusua.get(0);

                // Se insertaron correctamente todos los módulos
                if (listaCheckusuaNoinsertados.size() == 0 && (listaCheckusuaInsertados.size() == infoLayout.size())) {
                    respuesta = true;
                    listaEntrante = responseInsertaCheckusua.get(1);
                    // No se insertaron todos los módulos correctamente
                } else {
                    respuesta = false;
                }

            } else {
                logger.info("NO SE INSERTA INFORMACIÓN DE CHECKUSUA");

            }

            //---------ALTA ARBOL DE DESICION-------------
            if (respuesta) {

                ArrayList<ChecklistLayoutDTO> listaArbol = checklistValidaAdminBI.getListaArbol(listaEntrante, tipoCarga);

                //ArrayList<ChecklistLayoutDTO> listaArbolSalida = checklistValidaAdminBI.getListaArbol(listaEntrante, 2);
                ArrayList<ChecklistLayoutDTO> listaArbolSalida = checklistValidaAdminBI.altaArbol(listaArbol, tipoCarga);

                String preguntasError = "";
                for (ChecklistLayoutDTO layout : listaArbolSalida) {
                    if (layout.getFlagErrorArbol() != 0) {
                        preguntasError = preguntasError + layout.getIdConsecutivo() + ", ";
                    }

                }
                if (preguntasError.equals("")) {

                    respuesta = true;

                } else {
                    logger.info("PREGUNTAS CON ERROR AL CREAR ARBOL: " + preguntasError);
                    respuesta = false;
                    boolean resp = arbolRespAdiEviAdmBI.depuraTabParam(listaProtocolos.get(0).getIdChecklist());
                    if (!resp) {
                        arbolRespAdiEviAdmBI.depuraTabParam(listaProtocolos.get(0).getIdChecklist());
                    }
                }

            } else {
                logger.info("NO SE INSERTA LOS ÁRBOLES ");
                boolean resp = arbolRespAdiEviAdmBI.depuraTabParam(listaProtocolos.get(0).getIdChecklist());
                if (!resp) {
                    arbolRespAdiEviAdmBI.depuraTabParam(listaProtocolos.get(0).getIdChecklist());
                }
            }

            /*if (respuesta) {
                //res = true;
                String nomProtocolo = listaEntrante.get(0).getNombreProtocolo();
                boolean resp = versionCheckAdmBI.cargaVesionesTabProd(nomProtocolo);
                for (ChecklistProtocoloDTO aux : listaProtocolosInsertados) {
                    OrdenGrupoDTO grupoOrden = new OrdenGrupoDTO();
                    grupoOrden.setIdGrupo(0);
                    if (tipoCarga == 1) {
                        grupoOrden.setIdGrupo(321);
                    } else {
                        grupoOrden.setIdGrupo(323);
                    }

                    grupoOrden.setIdChecklist(aux.getIdChecklist());
                    grupoOrden.setOrden(1);
                    grupoOrden.setCommit(1);

                    int result = ordenGrupoBI.insertaGrupo(grupoOrden);
                }
            }*/
            //código para agregar las zonas
            if (respuesta) {

                try {
                    //PreguntaBI preguntabi = (PreguntaBI) FRQAppContextProvider.getApplicationContext().getBean("preguntaBI");

                    for (int i = 0; i < listaPreguntas.size(); i++) {
                        int idPregunta=listaPreguntas.get(i).getIdPregunta();
                        System.out.println("ID_PREGUNTA_TEMP: "+idPregunta);

                        //listaPreguntasCorrectos.add(listaPreguntas.get(i));
                        //Se agrega el id pregunta e id zona en la tabla de Preg-Zona
                        AdmTipoZonaDTO tipoZona = new AdmTipoZonaDTO();
                        tipoZona.setIdTipoZona(tipoCarga + "");
                        ArrayList<AdmTipoZonaDTO> tipoZonaComp = admTipoZonaBI.obtieneTipoZonaById(tipoZona);
                        if (tipoZonaComp.get(0).getDescripcion().toUpperCase().contains("ASEGURAMIENTO")) {
                            //Se obtiene el catalogo de zonas para aseguramiento
                            AdminZonaDTO zona = new AdminZonaDTO();
                            zona.setIdTipo(tipoCarga + "");
                            ArrayList<AdminZonaDTO> zonaComp = admZonasBI.obtieneZonaById(zona);

                            //Se recorre el catalogo de zonas de aseguramiento
                            for (AdminZonaDTO zonaDTO : zonaComp) {
                                System.out.println("ZONA DEL LAYOUT: " + listaPreguntas.get(i).getArea());
                                System.out.println("ZONA DE CATALOGO: " + zonaDTO.getDescripcion());
                                if (listaPreguntas.get(i).getArea().toUpperCase().contains(zonaDTO.getDescripcion().toUpperCase())) {
                                    //logger.info("Preg: " + listaPreguntas.get(i).getArea());
                                    AdmPregZonaDTO pregZona = new AdmPregZonaDTO();
                                    pregZona.setIdPreg(idPregunta + "");
                                    pregZona.setIdZona(zonaDTO.getIdZona());
                                    respuesta = admPregZonasTempBI.insertaPregZona(pregZona);
                                    if (respuesta) {
                                        break;
                                    }
                                } else {
                                    logger.info("La zona no se encuentra en el catalogo de zonas");
                                }
                            }
                        } else if (tipoZonaComp.get(0).getDescripcion().toUpperCase().contains("INFRAESTRUCTURA")) {
                            //Se obtiene el catalogo de zonas para infraestructura
                            AdminZonaDTO zona = new AdminZonaDTO();
                            zona.setIdTipo(tipoCarga + "");
                            ArrayList<AdminZonaDTO> zonaComp = admZonasBI.obtieneZonaById(zona);

                            //Se recorre el catalogo de zonas de infraestructura
                            for (AdminZonaDTO zonaDTO : zonaComp) {
                                if (listaPreguntas.get(i).getArea().toUpperCase().contains(zonaDTO.getDescripcion().toUpperCase())) {
                                    //logger.info("Preg: " + listaPreguntas.get(i).getArea());
                                    AdmPregZonaDTO pregZona = new AdmPregZonaDTO();
                                    pregZona.setIdPreg(idPregunta + "");
                                    pregZona.setIdZona(zonaDTO.getIdZona());
                                    respuesta = admPregZonasTempBI.insertaPregZona(pregZona);
                                    if (respuesta) {
                                        break;
                                    }
                                } else {
                                    logger.info("La zona no se encuentra en el catalogo de zonas");
                                }
                            }
                        } else if (tipoZonaComp.get(0).getDescripcion().toUpperCase().contains("TRANSFORMACIÓN")) {
                            //Se obtiene el catalogo de zonas para transformacion
                            AdminZonaDTO zona = new AdminZonaDTO();
                            zona.setIdTipo(tipoCarga + "");
                            ArrayList<AdminZonaDTO> zonaComp = admZonasBI.obtieneZonaById(zona);

                            //Se recorre el catalogo de zonas de transformaciones
                            for (AdminZonaDTO zonaDTO : zonaComp) {
                                if (listaPreguntas.get(i).getArea().toUpperCase().contains(zonaDTO.getDescripcion().toUpperCase())) {
                                    //logger.info("Preg: " + listaPreguntas.get(i).getArea());
                                    AdmPregZonaDTO pregZona = new AdmPregZonaDTO();
                                    pregZona.setIdPreg(idPregunta + "");
                                    pregZona.setIdZona(zonaDTO.getIdZona());
                                    respuesta = admPregZonasTempBI.insertaPregZona(pregZona);
                                    if (respuesta) {
                                        break;
                                    }
                                } else {
                                    logger.info("La zona no se encuentra en el catalogo de zonas");
                                }
                            }
                        }
                    }

                    //response.add(0, listaPreguntasIncorrectos);
                    //response.add(1, listaPreguntasCorrectos);
                } catch (Exception e) {
                    logger.info("Ocurrio algo en AP al insertar PREGUNTA: " + e.getMessage());
                    //System.out.println(e.getMessage());
                    //System.out.println("AP :" + e);
                }

            }

            if (respuesta == true) {
                //listaProtocolos
                jsonSalida = "{\"ALTA\":true}";
                String nomProtocolo = listaEntrante.get(0).getNombreProtocolo();
                boolean resp = versionCheckAdmBI.cargaVesionesTab(nomProtocolo);
            } else {
                jsonSalida = "{\"ALTA\":false}";

            }

        } catch (Exception e) {
            e.printStackTrace();
            jsonSalida = "{}";
        }

        return jsonSalida;
    }
    
 // http://localhost:8080/checklist/consultaChecklistAdminService/getVersChecksModelo.json?idProtocolo=&idChecklist=

    @RequestMapping(value = "getVersChecksModelo", method = RequestMethod.GET)
    public ModelAndView getVersChecksModelo(HttpServletRequest request, HttpServletResponse response) {

        String param = request.getParameter("idProtocolo");
        String idChecklist = request.getParameter("idChecklist");
        
        VersionProtoCheckDTO bean=new VersionProtoCheckDTO();
        bean.setIdProtocolo(param);
        bean.setIdChecklist(idChecklist);
        
        // Verifica la version Vigentes del protocolo para inactivarla
        List<VersionProtoCheckDTO> lista=versionProtoCheckBI.obtieneVersiones(bean);   
        
       

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "VERSION-CHECKLIST-PROTOCOLO");
        mv.addObject("res", lista);

        return mv;
    }

}
