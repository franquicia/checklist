package com.gruposalinas.checklist.domain;

public class CanalDTO {
	
	private int idCanal;
	private String descrpicion;
	private int activo;
	
	public int getIdCanal() {
		return idCanal;
	}
	public void setIdCanal(int idCanal) {
		this.idCanal = idCanal;
	}
	public String getDescrpicion() {
		return descrpicion;
	}
	public void setDescrpicion(String descrpicion) {
		this.descrpicion = descrpicion;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	
	

}
