<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="../css/jQuery/jQuery.css">
<script src="../css/jQuery/jQuery.js"></script>
<script src="../js/script-validaciones.js" type="text/javascript"></script>

<style type="text/css">
table {
	width: 100%;
	border: 1px solid #21C0A1;
	text-align: center;
}

td {
	text-align: center;
	padding: 15px;
}

tr:nth-child(even) {
	background-color: #f2f2f2
}

th {
	background-color: #21C0A1;;
	text-align: center;
	color: white;
	width: 15%;
	padding: 13px;
}

.log-btn {
	background: #0AC986;
	text-align: center;
	dispaly: inline-block;
	width: 100px;
	font-size: 16px;
	height: 50px;
	color: #fff;
	text-decoration: none;
	border: none;
	border-radius: 4px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
}

#log-btn:hover {
	cursor: pointer;
}
</style>

</head>

<body onload="initScheduler();">
	<c:choose>
		<c:when test="${res == null}">
			<form:form method="POST" action="schedulerController.htm"
				commandName="command" onsubmit="return validaScheduler();" style="margin: 50px 20px 0 20px;">
				<table>
					<tr>
						<th>ID TAREA</th>
						<th>CVE TAREA</th>
						<th>FECHA</th>
						<th>TAREA ACTIVA</th>
						<th>OPCIONES</th>
						<th>IP ${ipHost},&nbsp;HH ${fechaHost}</th>
					
						<c:forEach items="${listaTareas}" var="item">
							<tr>
								<td><input type="hidden" name="id${item.idTarea}"
									value="${item.idTarea}" />${item.idTarea}</td>
								<td><input type="hidden" name="idTarea${item.idTarea}"
									value="${item.cveTarea}" />${item.cveTarea}</td>
								<td><input type="hidden" name="fecha${item.idTarea}"
									value="${item.strFechaTarea}" />${item.strFechaTarea}</td>
								<td><input type="hidden" name="activo${item.idTarea}"
									value="${item.activo}" /> 
								<select name="act${item.idTarea}">
										<c:choose>
											<c:when test="${item.activo == 1}">
												<option value="1">Activo</option>
												<option value="0">Inactivo</option>
											</c:when>
											<c:when test="${item.activo == 0}">
												<option value="0">Inactivo</option>
												<option value="1">Activo</option>
											</c:when>
										</c:choose>
								</select></td>
								<td><form:radiobutton path="id" value="${item.idTarea}"
										onclick="habilitaScheduler()" />Selecciona</td>
							</tr>
						</c:forEach>
					</tr>
					<tr id="tarea">
						<td>Nombre Tarea:<input type="text" name="nombreTarea"
							id="nombreTarea" />
						</td>
						<td>Estado Tarea: <select name="nTareaActiva"
							id="nTareaActiva">
								<option value="1">Activo</option>
								<option value="0">Inactivo</option>
						</select>

						</td>
						

					</tr>

					<tr>
						<td id="hora"><form:label path="hora">Horas:</form:label> <form:select
								path="hora">
								<c:forEach var="i" begin="0" end="24">
									<c:choose>
										<c:when test="${i == 0}">
											<option value="*">Todas(*)</option>
										</c:when>
										<c:when test="${i > 0 && i < 11}">
											<option value="${i-1}">0${i-1}Hrs.</option>
										</c:when>
										<c:when test="${ i > 10 && i < 25}">
											<option value="${i-1}">${i-1}Hrs.</option>
										</c:when>
									</c:choose>
								</c:forEach>
							</form:select></td>

						<td id="minutos"><form:label path="minutos">Minutos:</form:label>
							<form:select path="minutos">
								<c:forEach var="i" begin="0" end="61">
									<c:choose>
										<c:when test="${i == 0}">
											<option value="*">Todos(*)</option>
										</c:when>
										<c:when test="${i > 0 && i < 11}">
											<option value="${i-1}">0${i-1}Mins.</option>
										</c:when>
										<c:when test="${i > 10 && i < 61}">
											<option value="${i-1}">${i-1}Mins.</option>
										</c:when>
									</c:choose>
								</c:forEach>
							</form:select></td>
						<td id="segundos"><form:label path="segundos">Segundos:</form:label>
							<form:select path="segundos">
								<c:forEach var="i" begin="0" end="61">
									<c:choose>
										<c:when test="${i == 0}">
											<option value="*">Todos(*)</option>
										</c:when>
										<c:when test="${i > 0 && i < 11}">
											<option value="${i-1}">0${i-1}Segs.</option>
										</c:when>
										<c:when test="${i > 10 && i < 61}">
											<option value="${i-1}">${i-1}Segs.</option>
										</c:when>
									</c:choose>
								</c:forEach>
							</form:select></td>
						<td id=dia><form:label path="dia">D&iacute;as:</form:label> <form:select
								path="dia">
								<c:forEach var="i" begin="0" end="31">
									<c:choose>
										<c:when test="${i == 0}">
											<option value="*">Todos(*)</option>
										</c:when>
										<c:when test="${i > 0 && i < 10}">
											<option value="${i}">0${i}</option>
										</c:when>
										<c:when test="${i > 0 && i > 9 && i < 32}">
											<option value="${i}">${i}</option>
										</c:when>
									</c:choose>
								</c:forEach>
							</form:select></td>
						<td id="mes"><form:label path="mes">Mes:</form:label> <form:select
								path="mes">
								<c:forEach var="i" begin="0" end="12">
									<c:choose>
										<c:when test="${i == 0}">
											<option value="*">Todos(*)</option>
										</c:when>
										<c:when test="${i > 0 && i < 10}">
											<option value="${i}">0${i}</option>
										</c:when>
										<c:when test="${i > 0 && i > 9 && i < 13}">
											<option value="${i}">${i}</option>
										</c:when>
									</c:choose>
								</c:forEach>
							</form:select></td>
						<td id="diaSemana"><form:label path="diaSemana">D&iacute;a Semana</form:label>
							<form:select path="diaSemana">
								<c:forEach var="i" begin="0" end="7">
									<c:choose>
										<c:when test="${i == 0}">
											<option value="?">Todos(?)</option>
										</c:when>
										<c:when test="${i == 1 }">
											<option value="SUN">Domingo</option>
										</c:when>
										<c:when test="${i == 2 }">
											<option value="MON">Lunes</option>
										</c:when>
										<c:when test="${i == 3 }">
											<option value="TUE">Martes</option>
										</c:when>
										<c:when test="${i == 4 }">
											<option value="WED">Miercoles</option>
										</c:when>
										<c:when test="${i == 5 }">
											<option value="THU">Jueves</option>
										</c:when>
										<c:when test="${i == 6 }">
											<option value="FRI">Viernes</option>
										</c:when>
										<c:when test="${i == 7 }">
											<option value="SAT">S&aacute;bado</option>
										</c:when>
									</c:choose>
								</c:forEach>
							</form:select></td>
					</tr>
		
					<tr>
						<td id="h"><input type="checkbox" id="h" name="h" />Hora Recurrente</td>
						<td id="m"><input type="checkbox" id="m" name="m" />Mins. Recurrentes</td>
						<td id="s"><input type="checkbox" id="s" name="s">Segs. Recurrentes</td>
						<td id="d"><input type="checkbox" id="d" name="d" />D&iacute;a Recurrente</td>
						<td id="month"><input type="checkbox" id="month" name="month" />Mes Recurrente</td>
						<td id="ds"><input type="checkbox" id="ds" name="ds" />D&iacute;a de Semana Recurrente</td>
					</tr>
				</table>
				<br>
				<!-- <input type="submit" value="MODIFICAR"/> -->
				<center style="min-width: 700px;">
					<button type="submit" class="log-btn" id="log-btn0" name="envia"  
					value="guarda"  onclick="return validaGuardadoScheduler();">GUARDAR</button>

					<button type="button" class="log-btn" id="log-btn1" name="envia"
						value="new" onclick="nuevaTarea()">NUEVO</button>

					<button type="submit" class="log-btn" id="log-btn2" name="envia"
						value="update">MODIFICAR</button>

					<button type="submit" class="log-btn" id="log-btn3" name="envia"
						value="delete">ELIMINAR</button>
				</center>
			</form:form>
		</c:when>

	</c:choose>
</body>
