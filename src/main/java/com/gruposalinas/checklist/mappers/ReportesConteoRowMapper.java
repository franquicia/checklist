package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReportesConteoDTO;

public class ReportesConteoRowMapper implements RowMapper<ReportesConteoDTO> {

	@Override
	public ReportesConteoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
	    
		ReportesConteoDTO reportesConteoDTO = new ReportesConteoDTO();
		
		reportesConteoDTO.setConteo(rs.getInt("CONTEO"));
		reportesConteoDTO.setIdModulo(rs.getInt("FIID_MODULO"));
		
		return reportesConteoDTO;
	}
	

}
