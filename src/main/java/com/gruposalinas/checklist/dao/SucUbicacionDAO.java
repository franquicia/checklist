package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ConteoTablasDTO;
import com.gruposalinas.checklist.domain.SucUbicacionDTO;

public interface SucUbicacionDAO {

	public List<SucUbicacionDTO> obtieneDatos(String idpais) throws Exception;

	public List<SucUbicacionDTO> obtieneInfo() throws Exception;
	
	public boolean cargaSucUbic() throws Exception;
	
	public boolean EliminacargaSucUbic() throws Exception;

	public List<SucUbicacionDTO> obtieneDatosTab(String idpais) throws Exception;
	
	//Cursor Conteo Tablas check_usua,bitacoras,tokens esquemas
	public List<ConteoTablasDTO> obtieneInfoTab() throws Exception;
}
