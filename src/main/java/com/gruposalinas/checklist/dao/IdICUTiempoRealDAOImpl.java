package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.IdICUTiempoRealDTO;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class IdICUTiempoRealDAOImpl extends DefaultDAO implements IdICUTiempoRealDAO {

    private DefaultJdbcCall jdbcInsertaIdICUTiempoReal;

    public void init() {

        jdbcInsertaIdICUTiempoReal = (DefaultJdbcCall) new DefaultJdbcCall(this.getFrqJdbcTemplate());
        jdbcInsertaIdICUTiempoReal.withSchemaName("FRANQUICIA");
        jdbcInsertaIdICUTiempoReal.withCatalogName("PAADMCLIENTEICU");
        jdbcInsertaIdICUTiempoReal.withProcedureName("SPINSUSRBDIGITAL");

    }

    @Override
    public boolean inserta(IdICUTiempoRealDTO idICUTiempoRealDTO) throws Exception {
        // TODO Auto-generated method stub

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCUSERNAME", idICUTiempoRealDTO.getUsername());
        mapSqlParameterSource.addValue("PA_FDINICIO_SESION", idICUTiempoRealDTO.getFechaEvento());
        mapSqlParameterSource.addValue("PA_FCSUCURSAL", idICUTiempoRealDTO.getTienda());
        mapSqlParameterSource.addValue("PA_FCMAC", idICUTiempoRealDTO.getMac());
        mapSqlParameterSource.addValue("PA_FCCLIENTE_ID", idICUTiempoRealDTO.getIdentificadorCliente());

        Map<String, Object> out = jdbcInsertaIdICUTiempoReal.execute(mapSqlParameterSource);
        boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() != 0;

        return success;

    }

}
