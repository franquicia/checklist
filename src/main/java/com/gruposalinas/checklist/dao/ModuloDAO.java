package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ModuloDTO;

public interface ModuloDAO {

	
	public List<ModuloDTO> obtieneModulo() throws Exception;
	
	public List<ModuloDTO> obtieneModulo(int idModulo) throws Exception;
	
	public int insertaModulo(ModuloDTO bean) throws Exception;
	
	public boolean actualizaModulo (ModuloDTO bean) throws Exception;
	
	public boolean eliminaModulo(int idModulo) throws Exception;
	
}
