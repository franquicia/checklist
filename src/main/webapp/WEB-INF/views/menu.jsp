<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script src="../js/script-menu.js"></script>
<script type="text/javascript">
history.forward();
</script>

<body>
	<div id="menu">
		<div id="menuContainer">
			<div id="opcion" >			
				<div id="linkMenu">
					<ul class="nav">
						<li>
						<a id="linkMenu" href="../">
								 <img id="subir"
								src="../images/menu/triunfo.png"
								alt="subir">&nbsp;&nbsp; Sistematización de actividades
						</a>
							<ul>
								<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>¿Qu&eacute; es?</p></a></li>
								<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>&nbsp;¿Qu&eacute; tengo que hacer?</p></a></li>
								<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>¿C&oacute;mo lo tengo que hacer?</p></a></li>
								<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>¿Qu&eacute; tengo que verificar?</p></a></li>														
								
							<!-- <div id="opcionAdminForm">
								<form name="formulario" method="POST" action="/checklist/central/resumenCheck.htm" id="formActivaAdmin">							
									<li id="linkMenuHijo"><a onclick="resumen()" id="linkMenuHijo"><p>ADMINISTRADOR CHECKLIST</p></a></li>
									<input type="hidden" name="idUserAdmin" value="adminCheckchecklists">
								</form>							
							</div>-->
							
							<c:if test="${perfilAdmin==1}">
								<form name="formulario" method="POST" action="../central/resumenCheck.htm" id="formActivaAdmin">							
									<li id="linkMenuHijo"><a onclick="resumen()" id="linkMenuHijo"><p>Administrador Checklist</p></a></li>
									<input type="hidden" name="idUserAdmin" value="adminCheckchecklists">
								</form>	
							</c:if>	
							<c:if test="${perfilReportes==1}">
								<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>Reportes</p></a>
									<ul id="subMenuLinkMenuHijo">
										<li id="linkMenuHijoSub" onclick="javascript:location.href='reporteOnlineApertura.htm'"><p>¿Cómo operan mis sucursales?</li>
										<li id="linkMenuHijoSub" onclick="javascript:location.href='vistaCumplimientoVisitas.htm'"><p>¿Cómo supervisamos?</p></li>
										<li id="linkMenuHijoSub" onclick="javascript:location.href='vistaCumplimientoVisitasSistemas.htm'"><p>Reporte de Visitas</p></li>
										<li id="linkMenuHijoSub" onclick="javascript:location.href='VistaSupervisionSistemasReporteVisitas.htm'"><p>Reporte Supervision</p></li>
									</ul>
								</li>				
							</c:if>																												
							</ul>
						</li>
					</ul>
				</div>
			</div>
	
			<div id="opcion">
				<div id="opcionMenu">
					<ul class="nav">
						<li><a id="linkMenu" href="carrusel7s.htm"> <img id="subir"
								src="../images/menu/7s.png"
								alt="subir">&nbsp;&nbsp; 7S
						</a>
						</li>
					</ul>
				</div>
			</div>
		
		<!-- se crean div temporales para posibles llenados -->
			
			<div id="opcion2"></div>	
			
			
			
			<c:choose>
				<c:when test="${nomsucursal == 'Central'}">
					<div id="opcion1">
						<div id="opcionMenu">
							<div id="linkMenu2">
								<ul class="nav">
									<li><a id="linkMenu" href="#"> <img
											id="agendaCierreFuera"> &nbsp;&nbsp;Salir
									</a>
										<ul>
											<li id="linkMenuCierre"
												onclick="javascript:location.href='cierraSesion.htm'"><img
												id="agendaCierre"
												src="http://botonrojo.socio.gs/homebr/imgs/cerrar_sesion2.png"
												alt="agenda">&nbsp;&nbsp;Cerrar sesión</li>
										</ul>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</c:when>
			</c:choose>
			
		</div>
	</div>
</body>





<!--
<div id="opcion"><div id="opcionMenu"><a id="linkMenu"  href="cargaForm.htm"><img id="subir" src="images/menu/subir_archivo.png" alt="subir">&nbsp;SUBIR ARCHIVO</a></div></div>
		<div id="opcion"><div id="opcionMenu"><a id="linkMenu" href="irBuscador.htm"><img id="saber" src="images/menu/para_saber.png" alt="saber">&nbsp;PARA SABER M&Aacute;S</a></div></div>
		<div id="opcion"><div id="opcionMenu"><a id="linkMenu" href="#"><img id="reportes" src="images/menu/reportes.png" alt="reportes">REPORTES</a></div></div>
		<div id="opcion"><div id="opcionMenu"><a id="linkMenu" href="#"><img id="agenda" src="images/menu/agenda.png" alt="agenda">&nbsp;AGENDA</a></div></div>
		<div id="opcion"><div id="opcionMenu"><a id="linkMenu" href="muestraFormatoChecklist.htm"><img id="checklist" src="images/menu/checklist.png" alt="checklist">&nbsp;CHECKLIST</a></div></div>
	
  -->
