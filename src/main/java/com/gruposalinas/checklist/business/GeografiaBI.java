package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.GeografiaDAO;
import com.gruposalinas.checklist.domain.GeografiaDTO;
import com.gruposalinas.checklist.util.UtilFRQ;


public class GeografiaBI {

	private static Logger logger = LogManager.getLogger(GeografiaBI.class);

	@Autowired
	GeografiaDAO geoDAO;
	List<GeografiaDTO> listaGeo = null;
	
	public List<GeografiaDTO> obtieneGeo(String idCeco,String idRegion,String idZona,String idTerritorio){
		try{
			listaGeo = geoDAO.obtieneGeografia(idCeco, idRegion, idZona, idTerritorio);
		}
		catch(Exception e)
		{
			logger.info("No fue posible obtener datos de la tabla");
			
			
		}
		return listaGeo;
	}
	
	public boolean insertaGeo (GeografiaDTO bean){

		boolean respuesta = false;
		try{
			respuesta = geoDAO.insertaGeografia(bean);
		}
		catch(Exception e)
		{
			logger.info("No fue posible insertar");
			
		}
		return respuesta;
	}
	
	public boolean actualizaGeo(GeografiaDTO bean){
		
		boolean respuesta = false;
		
		try{
			respuesta = geoDAO.actualizaGeografia(bean);
		}
		catch(Exception e){
			logger.info("No fue posible actualizar la tabla");
			
			
		}
		
		return respuesta;
	}
	
	public boolean eliminaGeo(int idCeco)
	{
		boolean respuesta =false;
		
		try{
			respuesta = geoDAO.eliminaGeografia(idCeco);
		}
		catch(Exception e){
			logger.info("No fue posible borrar los datos");
			
			
		}
		return respuesta;
	}
	

}
