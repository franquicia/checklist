package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ConsultaFechasDTO;


public interface ConsultaFechasDAO {

	public  List<ConsultaFechasDTO> obtieneFecha(ConsultaFechasDTO bean ) throws Exception;
		public boolean InsertaFecha (ConsultaFechasDTO bean) throws Exception;
		public boolean ActualizaFecha (ConsultaFechasDTO bean) throws Exception;
		public boolean EliminaFecha(String fechaInicial,String fechaFinal) throws Exception;
	}