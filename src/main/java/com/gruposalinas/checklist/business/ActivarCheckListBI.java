package com.gruposalinas.checklist.business;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ActivarCheckListDAO;
import com.gruposalinas.checklist.domain.ActivarCheckListDTO;


public class ActivarCheckListBI {
		
	private static Logger logger = LogManager.getLogger(ActivarCheckListBI.class);
	
	@Autowired
	ActivarCheckListDAO activarchecklistDAO;
	
	
	public boolean actualizaCheckList(ActivarCheckListDTO bean) {

		boolean resultado = false;

		try {
			resultado = activarchecklistDAO.actualizaCheckList(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el CECO ");
			logger.info(""+e);
		}

		return resultado;
	}
	
}