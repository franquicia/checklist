package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.FaseFirmasDTO;

public class FaseFirmasRowMapper implements RowMapper<FaseFirmasDTO> {

	public FaseFirmasDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		FaseFirmasDTO firm = new FaseFirmasDTO();
		
	
		firm.setIdSoft(rs.getString("FIID_SOFTN"));
		firm.setCeco(rs.getString("FCID_CECO"));
		firm.setIdFase(rs.getInt("FIID_FASE"));
		firm.setIdProyecto(rs.getInt("FIID_PROYECTO"));
		firm.setPuesto(rs.getString("FCPUESTO"));
		firm.setResponsable(rs.getString("FCRESPONSABLE"));
		firm.setCorreo(rs.getString("FCCORREO"));
		firm.setNombre(rs.getString("FCNOMBRE"));
		firm.setRuta(rs.getString("FIID_RUTA"));
		firm.setIdAgrupa(rs.getInt("FIID_AGRUPFIRMA"));
		
		
        
		return firm;
	}

}


