package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ChecklistHallazgosRepoDTO;

public class BitaNegoRowMapper implements RowMapper<ChecklistHallazgosRepoDTO> {

	public ChecklistHallazgosRepoDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistHallazgosRepoDTO bit = new ChecklistHallazgosRepoDTO();
		
		bit.setCeco(rs.getString("FCID_CECO"));
		bit.setNegocio(rs.getString("NEGO"));
		
		return bit;
	}

}


