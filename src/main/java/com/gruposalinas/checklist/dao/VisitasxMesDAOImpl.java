package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.VisitasxMesDTO;
import com.gruposalinas.checklist.mappers.VisitasxMesRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class VisitasxMesDAOImpl extends DefaultDAO implements VisitasxMesDAO {

    private Logger logger = LogManager.getLogger(VisitasxMesDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneVisitas;

    public void init() {

        jdbcObtieneVisitas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAVISITASXMES")
                .withProcedureName("SP_OBTIENEVISITAS")
                .returningResultSet("RCL_CONSULTA", new VisitasxMesRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<VisitasxMesDTO> obtieneVisitas(int checklist, String fechaInicio, String fechaFin) throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;
        List<VisitasxMesDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECKLIST", checklist).addValue("PA_RANGOINICIO", fechaInicio).addValue("PA_RANGOFIN", fechaFin);

        out = jdbcObtieneVisitas.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAVISITASXMES.SP_OBTIENEVISITAS}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        lista = (List<VisitasxMesDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al consultar las visitas");
            return null;
        }

        return lista;
    }

}
