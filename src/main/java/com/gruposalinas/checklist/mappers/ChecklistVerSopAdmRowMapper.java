package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.CheckSoporteAdmDTO;

public class ChecklistVerSopAdmRowMapper implements RowMapper<CheckSoporteAdmDTO> {

	@Override
	public CheckSoporteAdmDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		CheckSoporteAdmDTO chk = new CheckSoporteAdmDTO();
		
		chk.setIdtab(rs.getInt("FIID_NEGVER"));
		chk.setVersion(rs.getString("FIVERSION"));
		chk.setVigente(rs.getInt("FIIDACTIVO"));
		chk.setNego(rs.getString("FCNEGO"));
		chk.setObs(rs.getString("FCOBSERVACION"));
		chk.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		chk.setOrdeGrupo(rs.getString("GRUPO"));
		chk.setNombreCheck(rs.getString("NOMBRECCHECK"));
		chk.setIdUsuario(rs.getInt("FIID_USUARIO"));
		chk.setNomusu(rs.getString("USUARIO"));
		chk.setFechaModificacion(rs.getString("FECHAMODIF"));
		chk.setFecha_inicio(rs.getString("FECHAINICIO"));
		chk.setFechaLibera(rs.getString("FDLIBERACION"));
		return chk;
	}
	
	
    
}
