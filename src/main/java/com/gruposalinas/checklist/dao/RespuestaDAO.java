package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.RespuestaDTO;

public interface RespuestaDAO {

	public List<RespuestaDTO> obtieneRespuesta() throws Exception;
	
	public List<RespuestaDTO> obtieneRespuesta(String idArbol, String idRespuesta, String idBitacora) throws Exception;
	
	public int insertaRespuesta (RespuestaDTO bean) throws Exception;
	
	public boolean insertaUnaRespuesta(int idCheckU, int idBitacora, int idPreg, int idArbol, String obsv, String compromiso, String evidencia, String respAd) throws Exception;

	public boolean insertaRespuestaNueva(int idCheckU, int idBitacora, int idPreg, int idArbol, String obsv, String compromiso, String evidencia, String respAd) throws Exception;
	
	public boolean actualizaRespuesta(RespuestaDTO bean) throws Exception;
	
	public boolean actualizaFechaResp(String idRespuesta, String fechaTermino, String commit) throws Exception;
	
	public boolean eliminaRespuesta (int idRespuesta) throws Exception;
	
	public boolean eliminaRespuestas (String respuestas) throws Exception;
	
	public boolean eliminaRespuestasDuplicadas () throws Exception;
	
	public boolean registraRespuestas(int idCheckUsua, BitacoraDTO bitacora, String idRespuestas, String compromisos, String evidencias)throws Exception;
}
