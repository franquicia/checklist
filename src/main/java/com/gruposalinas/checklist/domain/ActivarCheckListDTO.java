package com.gruposalinas.checklist.domain;

public class ActivarCheckListDTO {
	
	private String idUsuario;
	private String idCeco;
	private String activo;
	private String idChecklist;
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getActivo() {
		return activo;
	}
	public void setActivo(String activo) {
		this.activo = activo;
	}
	public String getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(String idChecklist) {
		this.idChecklist = idChecklist;
	}
	@Override
	public String toString() {
		return "ActivarCheckListDTO [idUsuario=" + idUsuario + ", idCeco=" + idCeco + ", activo=" + activo
				+ ", idChecklist=" + idChecklist + "]";
	}

	
}
