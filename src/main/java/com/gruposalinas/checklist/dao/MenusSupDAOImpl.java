package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.MenusSupDTO;
import com.gruposalinas.checklist.mappers.MenusSupRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class MenusSupDAOImpl extends DefaultDAO implements MenusSupDAO {

    private static Logger logger = LogManager.getLogger(MenusSupDAOImpl.class);

    DefaultJdbcCall jdbcInsertMenu;
    DefaultJdbcCall jdbcUpdateMenu;
    DefaultJdbcCall jdbcDeleteMenu;
    DefaultJdbcCall jdbcGetMenu;

    public void init() {

        jdbcInsertMenu = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMENU")
                .withProcedureName("SPINSMENU");

        jdbcUpdateMenu = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMENU")
                .withProcedureName("SPACTMENU");

        jdbcDeleteMenu = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMENU")
                .withProcedureName("SPDELMENU");

        jdbcGetMenu = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMMENU")
                .withProcedureName("SPGETMENU")
                .returningResultSet("PA_CONSULTA", new MenusSupRowMapper());
    }

    @Override
    public int insertMenu(int idMenu, String descMenu, int activo) throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIMENU_ID", idMenu)
                .addValue("PA_FCDESCRIPCION", descMenu)
                .addValue("PA_FIACTIVO", activo);

        out = jdbcInsertMenu.execute(in);
        logger.info("Función ejecutada: FRANQUICIA.PAADMMENU.SPINSMENU");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al insertar el menu");
        }
        return respuesta;
    }

    @Override
    public int updateMenu(int idMenu, String descMenu, String activo) throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIMENU_ID", idMenu)
                .addValue("PA_FCDESCRIPCION", descMenu)
                .addValue("PA_FIACTIVO", activo);

        out = jdbcUpdateMenu.execute(in);
        logger.info("Función ejecutada: FRANQUICIA.PAADMMENU.SPACTMENU");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al actualizar el menu");
        }
        return respuesta;
    }

    @Override
    public int deleteMenu(int idMenu) throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIMENU_ID", idMenu);

        out = jdbcDeleteMenu.execute(in);
        logger.info("Función ejecutada: FRANQUICIA.PAADMMENU.SPDELMENU");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        respuesta = resultado.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al eliminar el menu");
        }
        return respuesta;
    }

    @Override
    public List<MenusSupDTO> getMenus(String idMenu) throws Exception {
        Map<String, Object> out = null;
        List<MenusSupDTO> respuesta = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIMENU_ID", idMenu);

        out = jdbcGetMenu.execute(in);
        logger.info("Función ejecutada: FRANQUICIA.PAADMMENU.SPGETMENU");

        respuesta = (List<MenusSupDTO>) out.get("PA_CONSULTA");

        if (respuesta.size() == 0) {
            logger.info("Algo ocurrió al consultar los menus");
        }
        return respuesta;
    }

}
