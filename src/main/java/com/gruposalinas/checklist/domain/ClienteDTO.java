package com.gruposalinas.checklist.domain;

public class ClienteDTO {

	private String idCeco;
	private String descCeco;
	
	private String apertura;
	private String cierre;
	private String cumplimiento7s;
	private String cumplimientoPiso;
	private String cajero;
	
	public ClienteDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getDescCeco() {
		return descCeco;
	}
	public void setDescCeco(String descCeco) {
		this.descCeco = descCeco;
	}
	public String getApertura() {
		return apertura;
	}
	public void setApertura(String apertura) {
		this.apertura = apertura;
	}
	public String getCierre() {
		return cierre;
	}
	public void setCierre(String cierre) {
		this.cierre = cierre;
	}
	public String getCumplimiento7s() {
		return cumplimiento7s;
	}
	public void setCumplimiento7s(String cumplimiento7s) {
		this.cumplimiento7s = cumplimiento7s;
	}
	public String getCumplimientoPiso() {
		return cumplimientoPiso;
	}
	public void setCumplimientoPiso(String cumplimientoPiso) {
		this.cumplimientoPiso = cumplimientoPiso;
	}
	public String getCajero() {
		return cajero;
	}
	public void setCajero(String cajero) {
		this.cajero = cajero;
	}

	@Override
	public String toString() {
		return "ClienteDTO [idCeco=" + idCeco + ", descCeco=" + descCeco + ", apertura=" + apertura + ", cierre="
				+ cierre + ", cumplimiento7s=" + cumplimiento7s + ", cumplimientoPiso=" + cumplimientoPiso + ", cajero="
				+ cajero + "]";
	}
}
