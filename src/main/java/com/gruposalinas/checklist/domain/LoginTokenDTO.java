package com.gruposalinas.checklist.domain;

public class LoginTokenDTO {

	private String usuario;
	private String llave;
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getLlave() {
		return llave;
	}
	public void setLlave(String llave) {
		this.llave = llave;
	}
	
}
