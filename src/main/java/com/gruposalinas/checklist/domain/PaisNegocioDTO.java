package com.gruposalinas.checklist.domain;

public class PaisNegocioDTO {
	
	private int idPais;
	private int idNegocio;
	
	public int getIdPais() {
		return idPais;
	}
	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}
	public int getIdNegocio() {
		return idNegocio;
	}
	public void setIdNegocio(int idNegocio) {
		this.idNegocio = idNegocio;
	}

	
}
