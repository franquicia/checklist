package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.EvidenciaDTO;

public class ReporteEvidenciaRowMapper implements RowMapper<EvidenciaDTO> {

	public EvidenciaDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		EvidenciaDTO evidenciaDTO = new EvidenciaDTO();
		
		evidenciaDTO.setIdEvidencia(rs.getInt("FIIDEVIDENCIA"));
		evidenciaDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		evidenciaDTO.setDescPreg(rs.getString("FCDESCRIPCION"));
		evidenciaDTO.setIdRespuesta(rs.getInt("FIID_RESPUESTA"));
		evidenciaDTO.setIdTipo(rs.getInt("FIID_TIPO"));
		evidenciaDTO.setRuta(rs.getString("FCRUTA"));
		 
		return evidenciaDTO;
		
	}
}
