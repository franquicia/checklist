package com.gruposalinas.checklist.domain;
	
	public class CecoPilaDTO {
		
	private int idCeco;
	private String nombreCeco;
	private int total;
	private int asignados;
	public int getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(int idCeco) {
		this.idCeco = idCeco;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getAsignados() {
		return asignados;
	}
	public void setAsignados(int asignados) {
		this.asignados = asignados;
	}



}
