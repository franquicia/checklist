package com.gruposalinas.checklist.domain;

public class CecoDTO {

	private String idCeco;
	private int idPais;
	private int idNegocio;
	private int idCanal;
	private int idEstado;
	private String descCeco;
	private int idCecoSuperior;
	private int activo;
	private int idNivel;
	private String calle;
	private String ciudad;
	private String cp;
	private String nombreContacto;
	private String puestoContacto;
	private String telefonoContacto;
	private String faxContacto;
	private String usuarioModifico;
	private String fechaModifico;
	
	private String idCCSuperior;
	private String desCanal;
	private int entidadid;
	private int numEconomico;
	private String nombreEnt;
	private int entidadPadre;
	private String nombrePadre;
	private String tipoCanal;
	private String nomTipoCanal;
	private String estatusCC;
	private int tipoCC;
	private String tipoOperacion;
	private String tipoSucursal;
	private String nomTipoSucursal;
	private int idResponsable;
	private int idSie;
	private String clasfSie;
	private int gastoNegocio;
	private String gastoxNegocio;
	private String fechaApertura;
	private String fechaCierre;
	private String desPais;
	private String fechaCarga;
	private int total;
	private int asignados;
	
	private RH rh;
	
	
	public RH getRh() {
		return rh;
	}
	public void setRh(RH rh) {
		this.rh = rh;
	}
	public int getIdPais() {
		return idPais;
	}
	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}
	public int getIdNegocio() {
		return idNegocio;
	}
	public void setIdNegocio(int idNegocio) {
		this.idNegocio = idNegocio;
	}
	public int getIdCanal() {
		return idCanal;
	}
	public void setIdCanal(int idCanal) {
		this.idCanal = idCanal;
	}
	public int getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}
	public int getIdCecoSuperior() {
		return idCecoSuperior;
	}
	public void setIdCecoSuperior(int idCecoSuperior) {
		this.idCecoSuperior = idCecoSuperior;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	public int getIdNivel() {
		return idNivel;
	}
	public void setIdNivel(int idNivel) {
		this.idNivel = idNivel;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getNombreContacto() {
		return nombreContacto;
	}
	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}
	public String getPuestoContacto() {
		return puestoContacto;
	}
	public void setPuestoContacto(String puestoContacto) {
		this.puestoContacto = puestoContacto;
	}
	public String getTelefonoContacto() {
		return telefonoContacto;
	}
	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}
	public String getFaxContacto() {
		return faxContacto;
	}
	public void setFaxContacto(String faxContacto) {
		this.faxContacto = faxContacto;
	}
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco; 
	}
	public String getDescCeco() {
		return descCeco;
	}
	public void setDescCeco(String descCeco) {
		this.descCeco = descCeco;
	}
	public String getUsuarioModifico() {
		return usuarioModifico;
	}
	public void setUsuarioModifico(String usuarioModifico) {
		this.usuarioModifico = usuarioModifico;
	}
	public String getFechaModifico() {
		return fechaModifico;
	}
	public void setFechaModifico(String fechaModifico) {
		this.fechaModifico = fechaModifico;
	}
	public int getNumEconomico() {
		return numEconomico;
	}
	public void setNumEconomico(int numEconomico) {
		this.numEconomico = numEconomico;
	}
	public String getNombreEnt() {
		return nombreEnt;
	}
	public void setNombreEnt(String nombreEnt) {
		this.nombreEnt = nombreEnt;
	}
	public int getEntidadPadre() {
		return entidadPadre;
	}
	public void setEntidadPadre(int entidadPadre) {
		this.entidadPadre = entidadPadre;
	}
	public String getNombrePadre() {
		return nombrePadre;
	}
	public void setNombrePadre(String nombrePadre) {
		this.nombrePadre = nombrePadre;
	}
	public String getTipoCanal() {
		return tipoCanal;
	}
	public void setTipoCanal(String tipoCanal) {
		this.tipoCanal = tipoCanal;
	}
	public String getNomTipoCanal() {
		return nomTipoCanal;
	}
	public void setNomTipoCanal(String nomTipoCanal) {
		this.nomTipoCanal = nomTipoCanal;
	}
	public String getEstatusCC() {
		return estatusCC;
	}
	public void setEstatusCC(String estatusCC) {
		this.estatusCC = estatusCC;
	}
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public String getTipoSucursal() {
		return tipoSucursal;
	}
	public void setTipoSucursal(String tipoSucursal) {
		this.tipoSucursal = tipoSucursal;
	}
	public String getNomTipoSucursal() {
		return nomTipoSucursal;
	}
	public void setNomTipoSucursal(String nomTipoSucursal) {
		this.nomTipoSucursal = nomTipoSucursal;
	}
	public int getIdResponsable() {
		return idResponsable;
	}
	public void setIdResponsable(int idResponsable) {
		this.idResponsable = idResponsable;
	}
	public int getIdSie() {
		return idSie;
	}
	public void setIdSie(int idSie) {
		this.idSie = idSie;
	}
	public String getClasfSie() {
		return clasfSie;
	}
	public void setClasfSie(String clasfSie) {
		this.clasfSie = clasfSie;
	}
	public int getGastoNegocio() {
		return gastoNegocio;
	}
	public void setGastoNegocio(int gastoNegocio) {
		this.gastoNegocio = gastoNegocio;
	}
	public String getGastoxNegocio() {
		return gastoxNegocio;
	}
	public void setGastoxNegocio(String gastoxNegocio) {
		this.gastoxNegocio = gastoxNegocio;
	}
	public String getFechaApertura() {
		return fechaApertura;
	}
	public void setFechaApertura(String fechaApertura) {
		this.fechaApertura = fechaApertura;
	}
	public String getFechaCierre() {
		return fechaCierre;
	}
	public void setFechaCierre(String fechaCierre) {
		this.fechaCierre = fechaCierre;
	}
	public String getDesPais() {
		return desPais;
	}
	public void setDesPais(String desPais) {
		this.desPais = desPais;
	}
	public String getFechaCarga() {
		return fechaCarga;
	}
	public void setFechaCarga(String fechaCarga) {
		this.fechaCarga = fechaCarga;
	}
	public int getEntidadid() {
		return entidadid;
	}
	public void setEntidadid(int entidadid) {
		this.entidadid = entidadid;
	}
	public int getTipoCC() {
		return tipoCC;
	}
	public void setTipoCC(int tipoCC) {
		this.tipoCC = tipoCC;
	}
	public String getIdCCSuperior() {
		return idCCSuperior;
	}
	public void setIdCCSuperior(String idCCSuperior) {
		this.idCCSuperior = idCCSuperior;
	}
	public String getDesCanal() {
		return desCanal;
	}
	public void setDesCanal(String desCanal) {
		this.desCanal = desCanal;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getAsignados() {
		return asignados;
	}
	public void setAsignados(int asignados) {
		this.asignados = asignados;
	}
	@Override
	public String toString() {
		return "CecoDTO [idCeco=" + idCeco + ", idPais=" + idPais + ", idNegocio=" + idNegocio + ", idCanal=" + idCanal
				+ ", idEstado=" + idEstado + ", descCeco=" + descCeco + ", idCecoSuperior=" + idCecoSuperior
				+ ", activo=" + activo + ", idNivel=" + idNivel + ", calle=" + calle + ", ciudad=" + ciudad + ", cp="
				+ cp + ", nombreContacto=" + nombreContacto + ", puestoContacto=" + puestoContacto
				+ ", telefonoContacto=" + telefonoContacto + ", faxContacto=" + faxContacto + ", usuarioModifico="
				+ usuarioModifico + ", fechaModifico=" + fechaModifico + ", idCCSuperior=" + idCCSuperior
				+ ", desCanal=" + desCanal + ", entidadid=" + entidadid + ", numEconomico=" + numEconomico
				+ ", nombreEnt=" + nombreEnt + ", entidadPadre=" + entidadPadre + ", nombrePadre=" + nombrePadre
				+ ", tipoCanal=" + tipoCanal + ", nomTipoCanal=" + nomTipoCanal + ", estatusCC=" + estatusCC
				+ ", tipoCC=" + tipoCC + ", tipoOperacion=" + tipoOperacion + ", tipoSucursal=" + tipoSucursal
				+ ", nomTipoSucursal=" + nomTipoSucursal + ", idResponsable=" + idResponsable + ", idSie=" + idSie
				+ ", clasfSie=" + clasfSie + ", gastoNegocio=" + gastoNegocio + ", gastoxNegocio=" + gastoxNegocio
				+ ", fechaApertura=" + fechaApertura + ", fechaCierre=" + fechaCierre + ", desPais=" + desPais
				+ ", fechaCarga=" + fechaCarga + ", total=" + total + ", asignados=" + asignados + "]";
	}

	
}
