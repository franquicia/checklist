package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.ReporteHallazgosDTO;

public class ReporteHallazgosDetalleRowMapper implements RowMapper<ReporteHallazgosDTO> {

	public ReporteHallazgosDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteHallazgosDTO ReporteHallazgosDTO = new ReporteHallazgosDTO();
		
		ReporteHallazgosDTO.setConteoTotal(rs.getInt("CONTEO_GRAL"));
	    ReporteHallazgosDTO.setConteoPorAtender(rs.getInt("CONTEO_PORATENDER"));
	    ReporteHallazgosDTO.setConteoPorAutorizar(rs.getInt("CONTEO_AUTORIZAR"));
	    ReporteHallazgosDTO.setConteoAtendido(rs.getInt("CONTEO_ATENDIDO"));
	    ReporteHallazgosDTO.setConteoRechazado(rs.getInt("CONTEO_RECHAZADO"));
	    ReporteHallazgosDTO.setNegocio(rs.getString("NEGOCIO"));
	    ReporteHallazgosDTO.setCeco(rs.getString("CECO"));
	    ReporteHallazgosDTO.setNombreCeco(rs.getString("NOMBRECECO"));
	    ReporteHallazgosDTO.setFase(rs.getInt("FIID_FASE"));
	    ReporteHallazgosDTO.setNombreFase(rs.getString("NOMFASE"));
	    ReporteHallazgosDTO.setProyecto(rs.getInt("FIID_PROYECTO"));
	    ReporteHallazgosDTO.setNombreProyecto(rs.getString("NOMPROY"));
	   
	    
		return ReporteHallazgosDTO;
	}

}


