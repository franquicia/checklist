package com.gruposalinas.checklist.domain;

public class DetalleRespuestaReporteDTO {
	
	private int idModulo;
	private int idPregunta; 
	private String pregunta;
	private int idPreguntaPadre;
	private int idRepuesta;
	private String observaciones;
	private int idArbol;
	private String respuesta;
	private String resAdicional;
	private String evidencia;
	private String accion;
	private String fechaCompromiso;
	
	public int getIdModulo() {
		return idModulo;
	}
	public void setIdModulo(int idModulo) {
		this.idModulo = idModulo;
	}
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public String getPregunta() {
		return pregunta;
	}
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}
	public int getIdPreguntaPadre() {
		return idPreguntaPadre;
	}
	public void setIdPreguntaPadre(int idPreguntaPadre) {
		this.idPreguntaPadre = idPreguntaPadre;
	}
	public int getIdRepuesta() {
		return idRepuesta;
	}
	public void setIdRepuesta(int idRepuesta) {
		this.idRepuesta = idRepuesta;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public int getIdArbol() {
		return idArbol;
	}
	public void setIdArbol(int idArbol) {
		this.idArbol = idArbol;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getResAdicional() {
		return resAdicional;
	}
	public void setResAdicional(String resAdicional) {
		this.resAdicional = resAdicional;
	}
	public String getEvidencia() {
		return evidencia;
	}
	public void setEvidencia(String evidencia) {
		this.evidencia = evidencia;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public String getFechaCompromiso() {
		return fechaCompromiso;
	}
	public void setFechaCompromiso(String fechaCompromiso) {
		this.fechaCompromiso = fechaCompromiso;
	}
	
}
