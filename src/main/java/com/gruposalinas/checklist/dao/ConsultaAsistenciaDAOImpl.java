package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ConsultaAsistenciaDTO;
import com.gruposalinas.checklist.mappers.ConsultaAsistenciaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ConsultaAsistenciaDAOImpl extends DefaultDAO implements ConsultaAsistenciaDAO {

    private Logger logger = LogManager.getLogger(ConsultaAsistenciaDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneAsistencia;

    public void init() {
        jdbcObtieneAsistencia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE_SUPRV")
                .withProcedureName("SPGRAFICA_ASIST")
                .returningResultSet("PA_CONSULTA", new ConsultaAsistenciaRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ConsultaAsistenciaDTO> obtieneAsistencia(ConsultaAsistenciaDTO bean) throws Exception {
        Map<String, Object> out = null;
        List<ConsultaAsistenciaDTO> listaCheck = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_USUARIO", bean.getIdUsuario());

        out = jdbcObtieneAsistencia.execute(in);

        //logger.info("Funcion ejecutada:{checklist.PAREPORTE_SUPRV.PA_FIID_USUARIO}");
        listaCheck = (List<ConsultaAsistenciaDTO>) out.get("PA_CONSULTA");

        //BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        //error = returnEjecucion.intValue();
        if (listaCheck == null) {
            error = 1;
        }

        if (error != 1) {
            logger.info("Algo ocurrió al consultar obtieneAsistencia");
            return listaCheck;
        }
        return listaCheck;

    }

}
