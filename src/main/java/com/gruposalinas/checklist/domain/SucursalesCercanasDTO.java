package com.gruposalinas.checklist.domain;

public class SucursalesCercanasDTO {
	
	private String ceco;
	private String nombreCeco;
	private String nuSucursal;
	private String latitud;
	private String longitud;
	
	public String getCeco() {
		return ceco;
	}
	public void setCeco(String ceco) {
		this.ceco = ceco;
	}
	public String getNombreCeco() {
		return nombreCeco;
	}
	public void setNombreCeco(String nombreCeco) {
		this.nombreCeco = nombreCeco;
	}
	public String getNuSucursal() {
		return nuSucursal;
	}
	public void setNuSucursal(String nuSucursal) {
		this.nuSucursal = nuSucursal;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	
	

}
