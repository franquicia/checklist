package com.gruposalinas.checklist.business;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonObject;
import com.gruposalinas.checklist.dao.TokenDAO;
import com.gruposalinas.checklist.domain.LlaveDTO;
import com.gruposalinas.checklist.domain.TokenDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;
import com.gruposalinas.checklist.util.UtilCryptoGS;
import com.gruposalinas.checklist.util.UtilDate;
import com.gruposalinas.checklist.util.UtilFRQ;

public class TokenDataCenterBI {
	
	private static Logger logger = LogManager.getLogger(TokenDataCenterBI.class);
	
	@Autowired
	TokenDAO tokenDAO;
	
	List<TokenDTO> datosToken = null;
	List<TokenDTO> listaToken = null;
	List<TokenDTO> listaTokenF = null;
	
	
	public boolean verificaToken(String token, Integer usuario){
		
		boolean resultado = false;
		TokenDTO bean = new TokenDTO();
		
		try{
			datosToken = tokenDAO.buscaToken(token);
			if(datosToken.size()>0){

				bean.setIdToken(datosToken.get(0).getIdToken());
				bean.setToken(datosToken.get(0).getToken());
				bean.setUsuario(usuario);
				bean.setEstatus(1);
				resultado = tokenDAO.actualizaToken(bean);

			}
		}catch (Exception e){
			logger.info("Algo Ocurrió... al verificar el TOKEN");
			
		}
		
		return resultado;		
	}
	
	
	public int insertaToken(TokenDTO bean) {
		
		int idResultado = 0;
		
		try {
			idResultado = tokenDAO.insertaToken(bean);
		} catch (Exception e) {
			logger.info("Algo Ocurrió... al insertar el Token");
			
		}
		
		return idResultado;
	}
	
	public boolean actualizaToken(TokenDTO bean){
		
		boolean resultado = false;
		
		try {
			resultado = tokenDAO.actualizaToken(bean);
		} catch (Exception e) {
			logger.info("Algo Ocurrió... al actualizar el Token");
			
		}
		
		return resultado;
		
	}
	
	public boolean eliminaToken(int idToken){
		
		boolean resultado = false;
		
		try {
			resultado = tokenDAO.eliminaToken(idToken);
		} catch (Exception e) {
			logger.info("Algo Ocurrió... al borrar el Token");
			
		}
		
		return resultado;
		
	}

	public boolean eliminaTokenTodos(){
		
		boolean resultado = false;
		
		try {
			resultado = tokenDAO.eliminaTokenTodos();
		} catch (Exception e) {
			logger.info("Algo Ocurrió... al borrar el Token");
			
		}
		
		return resultado;
		
	}
	public List<TokenDTO> buscaToken(String token){
				
		try {
			datosToken = tokenDAO.buscaToken(token);
		} catch (Exception e) {
			logger.info("Algo Ocurrió... al consultar el Token" );
			
		}
		
		return datosToken;
		
	}
	
	public List<TokenDTO> buscaTokenF(String fecha){
				
		try {
			listaTokenF = tokenDAO.buscaTokenF(fecha);
		} catch (Exception e) {
			logger.info("Algo Ocurrió... al consultar el Token" );
			
		}
		
		return listaTokenF;
		
	}
	
	public List<TokenDTO> buscaToken(){
		
		try {
			listaToken = tokenDAO.buscaToken();
		} catch (Exception e) {
			logger.info("Algo Ocurrió... al consultar los Token");
			
		}
		
		return listaToken;
		
		
	}
	
	/*GENERA TOKEN*/
	@SuppressWarnings({ "static-access", "unused" })
	public String getToken(String tokenEncriptado)
			throws IOException, KeyException, GeneralSecurityException {

		UtilCryptoGS cifra = new UtilCryptoGS();
		TokenDTO token = new TokenDTO();
		String json = "";
		JsonObject object = new JsonObject();
		UtilDate fec = new UtilDate();
		String fecha = fec.getSysDate("dd/MM/yyyy");
		String fechavalida = fec.getSysDate("ddMMyyyyHHmmss");
		List<LlaveDTO> llave=null;

		String urides = cifra.decryptParams(tokenEncriptado,FRQConstantes.getLlaveEncripcionLocalDatacenter());
		String[] valores = urides.split("&");
		
		String variipfinal = "", variidaplfinal = "", variurlfinal = "", varifechafinal = "";
		
		for (String valor : valores) {
			if (valor.contains("uri")) {
				variurlfinal = "uri="+valor.split("=")[1];
			}
			if (valor.contains("idapl")) {
				variidaplfinal = "idapl="+valor.split("=")[1];
			}
			if (valor.contains("ip")) {
				variipfinal = "ip="+valor.split("=")[1];
			}
			if (valor.contains("fecha")) {
				varifechafinal = "fecha="+valor.split("=")[1];;
			}
		}
		if (fec.validaFecha(varifechafinal.split("=")[1], fechavalida)) {
			/*
			System.out.println("Id apli.. " +variidaplfinal.split("=")[1]);
			llave=llaveBI.obtieneLlave(Integer.parseInt(variidaplfinal.split("=")[1]));

			System.out.println("valor... " + llave.get(0).getLlave());
			if(llave.size()>0)
				token.setLlave(llave.get(0).getLlave());
			else
				token.setLlave(((llaveBI.obtieneLlave(666)).get(0)).toString());
			*/
			
			//System.out.println("Llave.. "+token.getLlave());
			token.setIpOrigen(variipfinal);
			token.setFechaHora(varifechafinal);
			token.setIdAplicacion(variidaplfinal);
			token.setUrlRedirect(variurlfinal);
			token.setBandera(true);
			token.setLlave(FRQConstantes.getLlaveEncripcionLocalDatacenter());
			String str = cifra.generaToken(token);
			str = str.replace("\n", "");
			str = str.replace(" ", "+");
			token.setToken(str);
			token.setUsuario(null);

			insertaToken(token);
		}else{
			
			logger.info("Hora del cliente dessincronizada");
		}
		return token.getToken();
	}
	
	public static void main(String [] args){
		
		String service = "service=servicios/checklist.json?id=191841&lat=19.30941887886347&lon=-99.18721647408643";
		String rutaService = service.split("\\?")[0].replace("service=", "");
		System.out.println("RUTA SERVICE----> " + rutaService);
		
		String params = service.replaceAll(rutaService, "").replace("service=", "").replace("?", "");
		System.out.println("PRIMEROS PARAMS:   " + params);
		
	}

}
