package com.gruposalinas.checklist.domain;

import org.springframework.http.HttpHeaders;

public class BinaryOutputWrapper {
	private byte[] data;
	private HttpHeaders headers;

	public BinaryOutputWrapper() {
		super();
	}

	public BinaryOutputWrapper(byte[] data, HttpHeaders headers) {
		super();
		this.data = data;
		this.headers = headers;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public HttpHeaders getHeaders() {
		return headers;
	}

	public void setHeaders(HttpHeaders headers) {
		this.headers = headers;
	}
	
}
