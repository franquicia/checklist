package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ChecklistActualDTO;
import com.gruposalinas.checklist.domain.ChecklistModificacionesDTO;
import com.gruposalinas.checklist.mappers.ChecklistActualRowMapper;
import com.gruposalinas.checklist.mappers.ChecklistModificacionesRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ChecklistModificacionesDAOImpl extends DefaultDAO implements ChecklistModificacionesDAO {

    private static Logger logger = LogManager.getLogger(ChecklistDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneActual;
    private DefaultJdbcCall jdbcObtieneModificaciones;
    private DefaultJdbcCall jdbcAutorizaCheck;

    public void init() {

        jdbcObtieneActual = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECKCAMBIOS")
                .withProcedureName("SPOBTIENEACTUAL")
                .returningResultSet("RCL_ACTUAL", new ChecklistActualRowMapper());

        jdbcObtieneModificaciones = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECKCAMBIOS")
                .withProcedureName("SPOBTIENECHECKS")
                .returningResultSet("RCL_ACTUAL", new ChecklistActualRowMapper())
                .returningResultSet("RCL_MODIFICA", new ChecklistModificacionesRowMapper());

        jdbcAutorizaCheck = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECKCAMBIOSNEW")
                .withProcedureName("SPAUTORIZACION");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ChecklistActualDTO> obtieneChecklistActual(int idChecklist) throws Exception {

        List<ChecklistActualDTO> listaChecklist = null;
        Map< String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECKLIST", idChecklist);

        out = jdbcObtieneActual.execute(in);

        logger.info("Funcion ejecutada:{checklist.PACHECKCAMBIOS.SPOBTIENEACTUAL}");

        BigDecimal returnEjec = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjec.intValue();

        listaChecklist = (List<ChecklistActualDTO>) out.get("RCL_ACTUAL");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al consultar el checklist actual con id (" + idChecklist + ")");
        } else {
            return listaChecklist;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> obtieneChecklist(int idChecklist) throws Exception {

        Map<String, Object> checklists = new HashMap<String, Object>();
        Map< String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECKLIST", idChecklist);

        out = jdbcObtieneModificaciones.execute(in);

        logger.info("Funcion ejecutada:{checklist.PACHECKCAMBIOS.SPOBTIENECHECKS}");

        BigDecimal returnEjec = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjec.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al consultar las modificaciones del checklist con id (" + idChecklist + ")");
        } else {

            checklists.put("checklistActual", (List<ChecklistActualDTO>) out.get("RCL_ACTUAL"));
            checklists.put("checklistModificaciones", (List<ChecklistModificacionesDTO>) out.get("RCL_MODIFICA"));

            return checklists;
        }

        return null;
    }

    @Override
    public boolean autorizaChecklist(int autoriza, int idChecklist) throws Exception {
        int ejecucion = 0;

        Map< String, Object> out = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECKLIST", idChecklist).addValue("PA_AUTORIZA", autoriza);

        out = jdbcAutorizaCheck.execute(in);

        logger.info("Funcion ejecutada:{checklist.PACHECKCAMBIOSNEW.SPAUTORIZACION");

        BigDecimal returnEje = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEje.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al autorizar las modificaciones del checklist con id (" + idChecklist + ")");
        } else {
            return true;
        }

        return false;
    }

}
