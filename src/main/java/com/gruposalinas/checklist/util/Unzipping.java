/*
This file is part of cmd-zipper.

cmd-zipper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

cmd-zipper is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

http://www.gnu.org/licenses/
*/

package com.gruposalinas.checklist.util;


import com.gruposalinas.checklist.domain.CustomFile;
import com.gruposalinas.checklist.util.Extensions;

import org.apache.commons.io.FilenameUtils;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by juanca on 4/19/17.
 */
public class Unzipping {

    private final static String REBUILT = "rebuilt";
	
    //DEFINIR RUTA
	String rutaArchivo = "";
	String rutaPartes ="";
	
	static String pathPrinc = "";
    
	public Unzipping(String path) throws IOException {
        CustomFile customFile = new CustomFile();
        File inputFile = new File(path);

        customFile.setPath(String.format("%s%s", inputFile.getParent(), File.separator));
        customFile.setFileName(inputFile.getName());
        customFile.setFileNameExtension(inputFile.getName());
        customFile.setFileExtension(FilenameUtils.getExtension(inputFile.getName()));

        String rootPath = File.listRoots()[0].getAbsolutePath();
		Calendar cal = Calendar.getInstance();

		//1.- :::  VERIFICAR EN DONDE ESTOY PARA GUARDAR EN ( /Sociounico/checklist/imagenes/ - /Sociounico/checklist/preview/ ) O EN EL NUEVO
		//COMENTO ESTO POR QUE AUN NO SE APLICA LA VERSION DE checklist
		try {
			if (!this.verificaServidor(InetAddress.getLocalHost().getHostAddress().toString())){
				rutaArchivo = File.separator+"Sociounico"+File.separator+"franquicia"+File.separator+"imagenes"+File.separator;
				rutaPartes = "/Sociounico/franquicia/videosPartes/";
			}
			else{
				//DONDE GUARDO CUANDO ESTE EN checklist
				rutaPartes = "/franquicia/videosPartes/";
				rutaArchivo = File.separator+"franquicia"+File.separator+"imagenes"+File.separator;
			}
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			rutaPartes = "/franquicia/videosPartes/";
			rutaArchivo = File.separator+"franquicia"+File.separator+"videosPartes"+File.separator;
		}
        
		pathPrinc = rootPath + rutaArchivo + cal.get(Calendar.YEAR) + File.separator;
		
        joinAndUnzipFile(inputFile, customFile);
    }

    private  void joinAndUnzipFile(File inputFile, CustomFile customFile)
            throws IOException {

        ZipInputStream is = null;
        File path = inputFile.getParentFile();

        List<InputStream> fileInputStream = new ArrayList<>();
        for (String fileName : path.list()) {
            if (fileName.startsWith(customFile.getFileName()) && fileName.endsWith(Extensions.ZIP.getExtension())) {
                fileInputStream.add(new FileInputStream(String.format("%s%s", customFile.getPath(), fileName)));
                //System.out.println(String.format("File found: %s", fileName));
            }
        }

        if (fileInputStream.size() > 0) {
            String fileNameOutput = String.format("%s%s", customFile.getPath(), customFile.getFileName());
            OutputStream os = new BufferedOutputStream(new FileOutputStream(fileNameOutput));
            try {
                //System.out.println("Please wait while the files are joined: ");
                ZipEntry ze;
                for (InputStream inputStream : fileInputStream) {
                    is = new ZipInputStream(inputStream);
                    ze = is.getNextEntry();
                    
                    //customFile.setFileName(String.format("%s_%s", REBUILT, ze.getName()));
                    customFile.setFileName(String.format("%s", ze.getName()));
                    byte[] buffer = new byte[CustomFile.BYTE_SIZE];

                    for (int readBytes; (readBytes = is.read(buffer, 0, CustomFile.BYTE_SIZE)) > -1; ) {
                        os.write(buffer, 0, readBytes);                      
                    }                   
                    is.close();
                }
            } finally {
                os.flush();
                os.close();
                is.close();
                renameFinalFile(customFile, fileNameOutput);
                deleteFiles(customFile);
            }
        } else {
            throw new FileNotFoundException("Error: The file not exist!");
        }
        //System.out.println("\nEnded process!");
    }

    private  void renameFinalFile(CustomFile customFile, String fileNameOutput) {
        File output = new File(fileNameOutput);
        
        
        //System.out.println("Original Archivo: "+fileNameOutput);
        //System.out.println("Path 1: "+customFile.getPath() + customFile.getFileName().substring(1));
        //System.out.println("Path 2: "+pathPrinc+customFile.getFileName().substring(1));
        
        boolean rename1 = output.renameTo(new File(customFile.getPath() + customFile.getFileName().substring(1)));
       
        File output2 = new File(customFile.getPath() + customFile.getFileName().substring(1));
        boolean rename2 = output2.renameTo(new File(pathPrinc+customFile.getFileName().substring(1)));
        
        //System.out.println("Primer renombre: "+rename1);
        //System.out.println("Segundo renombre: "+rename2);
    }
    
    private  void deleteFiles(CustomFile customFile) {
    	Calendar cal = Calendar.getInstance();
    	File path = new File( File.listRoots()[0].getAbsolutePath()+rutaPartes+cal.getInstance().get(Calendar.YEAR)+"/");
    	//File path = new File("C://Sociounico/franquicia/videosPartes/"+cal.getInstance().get(Calendar.YEAR)+"/");
    	
    	//System.out.println("Customfile: "+customFile.getFileNameExtension());
    	String ruta[] = customFile.getFileNameExtension().split("\\.");
    	String nombreOriginal = ruta[0];
    	
    	String nombreArchivo = nombreOriginal.substring(0,nombreOriginal.length()-4); 
    	
    	 //nombreArchivo = nombreArr[0];
    	
    	for(String fileDelete : path.list()) {
    		if(fileDelete.startsWith(nombreArchivo) && fileDelete.endsWith(Extensions.ZIP.getExtension())) {
    			//System.out.println("Archivo a elminar "+path+File.separator+fileDelete);
    			File file = new File(path+File.separator+fileDelete);
    			boolean exist = file.exists();
    			boolean delete = file.delete();
    		}
    		
    	}
    	
    }
    
	public boolean verificaServidor(String ipActual){
		boolean resp =  false;
		String[] ipFrq = {"10.53.33.74","10.53.33.75","10.53.33.76","10.53.33.77"};

		for (String data : ipFrq){
			if(data.contains(ipActual))
				return true;
		}
		return resp;
	}
  
}
