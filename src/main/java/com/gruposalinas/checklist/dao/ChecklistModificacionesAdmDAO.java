package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.ChecklistActualDTO;

public interface ChecklistModificacionesAdmDAO {
	
	public List<ChecklistActualDTO> obtieneChecklistActual(int idChecklist) throws Exception;
	
	public Map<String, Object> obtieneChecklist(int idChecklist) throws Exception;
	
	public boolean autorizaChecklist(int autoriza, int idChecklist) throws Exception;
	
	

}
