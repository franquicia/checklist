package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ImagenesRespuestaDTO;

public interface ImagenesRespuestaDAO {
	
	public List<ImagenesRespuestaDTO> obtieneImagenes(String idArbol, String idImagen)throws Exception;
	
	public int insertaImagen(ImagenesRespuestaDTO imagen)throws Exception;
	
	public boolean eliminaImagen(int idImagen)throws Exception;

}
