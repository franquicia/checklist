package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.VersionProtoCheckDTO;

public interface VersionProtoCheckDAO {
	
	public boolean insertaVersion(VersionProtoCheckDTO bean)throws Exception;
	
	public boolean actualizaVersion(VersionProtoCheckDTO bean)throws Exception;
	
	public List<VersionProtoCheckDTO> consultaVersion(VersionProtoCheckDTO bean)throws Exception;
	
	public boolean eliminaVersion(VersionProtoCheckDTO bean)throws Exception;

}
