package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.AdmFirmasCatDTO;


public interface FirmasCatDAO {

	  public int insertaFirmaCat(AdmFirmasCatDTO bean)throws Exception;
		
		public boolean eliminaFirmaCat(int idFirma)throws Exception;
		
		public List<AdmFirmasCatDTO> obtieneDatosFirmaCat(String idFirma) throws Exception; 
		
		
		public boolean actualizaFirmaCat(AdmFirmasCatDTO bean)throws Exception;
		 
		public int insertaFirmaAgrupa(AdmFirmasCatDTO bean)throws Exception;
		
		public boolean eliminaFirmaAgrupa(int idFirma)throws Exception;
		
		public List<AdmFirmasCatDTO> obtieneDatosFirmaAgrupa(String idFirma) throws Exception; 
			
		public boolean actualizaFirmaAgrupa(AdmFirmasCatDTO bean)throws Exception;
		
	}