package com.gruposalinas.checklist.mappers;

import com.gruposalinas.checklist.domain.RepoFilExpDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class RepoFiltro1CecoFaseExpRowMapper implements RowMapper<RepoFilExpDTO> {

    public RepoFilExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        RepoFilExpDTO com = new RepoFilExpDTO();

        com.setCeco(rs.getString("FCID_CECO"));
        com.setNombreCeco(rs.getString("FCNOMBRE"));
        com.setActivoCeco(rs.getInt("FIACTIVO"));
        com.setCecoSuperior(rs.getString("FICECO_SUPERIOR"));
        com.setDireccion(rs.getString("DIRECCION"));
        com.setFase(rs.getString("FIID_FASE"));
        com.setProyecto(rs.getString("FIID_PROYECTO"));
        com.setBitGral(rs.getInt("FIID_BITACORA"));
        com.setIdSoft(rs.getInt("FIID_SOFTN"));
        com.setRegion(rs.getString("FCREGION"));
        com.setTerritorio(rs.getString("FCTERRITORIO"));
        com.setZona(rs.getString("FCZONA"));
        com.setNomUsu(rs.getString("LIDEROBRA"));
        com.setPerido(rs.getString("FCPERIODO"));
        com.setIdRecorrido(rs.getString("FCRECO"));
        com.setPondTot(rs.getDouble("CALIFICACION"));
        // com.setCalif(rs.getDouble("CALIFICACION"));
        //com.setPrecalif(rs.getDouble("PRECALIFICACION"));
        // com.setAperturable(rs.getInt("APERTURABLE"));
        // com.setCalif(rs.getDouble("CALIFICACION"));
        //com.setPrecalif(rs.getDouble("PRECALIFICACION"));
        //com.setAux(rs.getString("USU"));

        return com;
    }

}
