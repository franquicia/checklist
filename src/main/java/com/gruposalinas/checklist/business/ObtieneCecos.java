/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.business;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gruposalinas.checklist.dao.CecoWSDAO;
import com.gruposalinas.checklist.domain.ListaTablaEKT;
import com.gruposalinas.checklist.domain.ParametroDTO;
import com.gruposalinas.checklist.domain.RespuestaEstructurasWSDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;
import com.gruposalinas.checklist.resources.FRQConstantes;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

/**
 *
 * @author kramireza
 */
@Component
public class ObtieneCecos {

    private static final String String = null;

    private static Logger logger = LogManager.getLogger(ObtieneCecos.class);

    @Autowired

    CecoWSDAO cecoDAO;

    public String getTokenCecos() {

        String usuario = FRQConstantes.getUsuarioWSCeco();
        String password = FRQConstantes.getPasswordWSCeco();
        String salida = null;

        BufferedReader rd = null;
        OutputStreamWriter ou = null;
        InputStreamReader inputStream = null;
        try {

            String data = URLEncoder.encode("usuario", "UTF-8") + "=" + URLEncoder.encode(usuario, "UTF-8");
            data += "&" + URLEncoder.encode("contrasena", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");
            URL url = new URL(FRQConstantes.getURLTokenCecos() + "?" + data);
            // URL url = new URL(http://10.50.109.68:8080/cecows/rest/token");
            //logger.info("url : " +url);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setUseCaches(false);

            // Obtener el estado del recurso
            int statusCode = conn.getResponseCode();

            logger.info("statusCode getTokenCecos" + statusCode);

            StringBuffer res = new StringBuffer();
            inputStream = new InputStreamReader(conn.getInputStream());
            rd = new BufferedReader(inputStream);

            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }
            salida = res.toString();
        } catch (Exception e) {
            salida = null;
        } finally {
            try {

                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                //logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }

            try {
                if (ou != null) {
                    ou.close();
                }
            } catch (Exception e) {
                //logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }
            try {
                if (inputStream != null) {
                    inputStream.close();
                }

            } catch (Exception e) {
                //logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }
        }
        return salida;
    }

    @SuppressWarnings("unused")
    public boolean getData(String token) throws IOException {
        boolean resultado = false;
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        URL urlConstant = new URL(FRQConstantes.getURLConsultaCecos());
        Map params = new LinkedHashMap<>();
        params.put("token", token);
        StringBuilder postData = new StringBuilder();
        for (Iterator it = params.entrySet().iterator(); it.hasNext();) {
            Map.Entry param = (Map.Entry) it.next();
            if (postData.length() != 0) {
                postData.append('&');
            }
            postData.append(URLEncoder.encode((String) param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
        final StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        HttpURLConnection conn = (HttpURLConnection) urlConstant.openConnection();
        RespuestaEstructurasWSDTO respuesta = new RespuestaEstructurasWSDTO();
        try {
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(50000);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            conn.setDoOutput(true);
            conn.getOutputStream().write(postDataBytes);
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            String output;
            stopWatch.stop();
            logger.info("Tiempo de conexión:" + stopWatch.getTotalTimeMillis() + " Milisegundos");
            StringBuilder stringBuilder = new StringBuilder();
            while ((output = br.readLine()) != null) {
                stringBuilder.append(output);
            }

            respuesta = gson.fromJson(stringBuilder.toString(), RespuestaEstructurasWSDTO.class);

            if (!respuesta.getListaTablaEKT().isEmpty()) {
                //PRIMERO DEPURAMOS LA TABLA ANTES DE INGRESAR LOS DATOS
                boolean depuro = cecoDAO.depuraTabla(FRQConstantes.getDepuraTabla());
                logger.info("Termina depura tabla: " + depuro);
                if (depuro) {

                    logger.info("Tamaño de arreglo:: " + respuesta.getListaTablaEKT().size());
                    for (ListaTablaEKT cecoEstruc : respuesta.getListaTablaEKT()) {
                        resultado = cecoDAO.insertaCeco(cecoEstruc);
                    }
                    logger.info("resultado ::" + resultado);
                }
            } else {
                logger.info("La lista de los cecos viene vacia.");
            }
        } catch (Exception e) {
            logger.info("Ocurrió una excepcion en la extracción de la información de los CECOS de Estructura.:" + e.getMessage());
        } finally {
            conn.disconnect();
        }
        return resultado;
    }

    @SuppressWarnings("unused")
    public boolean getMergeData() throws IOException {
        boolean resultado = false;
        try {
            //PRIMERO DEPURAMOS LA TABLA ANTES DE INGRESAR LOS DATOS
            boolean depuro = cecoDAO.depuraTabla(FRQConstantes.getDepuraTablaOriginal());
            logger.info("Termina depura tabla: " + depuro);
            if (depuro) {
                resultado = cecoDAO.mergeOriginal();
            }
            logger.info("resultado ::" + resultado);

        } catch (Exception e) {
            logger.info("Ocurrió una excepcion en la extracción de la información de los CECOS de Estructura.:" + e);
            e.printStackTrace();
        }
        return resultado;

    }

    public boolean ejecutaProcesosCecosGeneral() {
        boolean resultMerge = false;
        boolean resulData = false;
        boolean resultGeneral = false;
        String destinatario = "";
        String asunto = "";
        String cuerpoCorreo = "";
        logger.info("*************Comienza carga cecos*************");
        try {
            String tokenResult = getTokenCecos();
            if (tokenResult != null && !tokenResult.equals("")) {
                JSONObject jsonObj = new JSONObject(tokenResult);
                String token = jsonObj.getString("token");
                //        System.out.println("Token Recibido" + token);

                resulData = getData(token);
                if (resulData) {

                    resultMerge = getMergeData();
                    if (!resultMerge) {
                        logger.info("*************Error merge entre las tablas  TAPASO_CECO y FRTPPASO_CECO*************" + CorreoBI.mergeCecos(destinatario = "", asunto = "Carga de Cecos", cuerpoCorreo = "Error merge entre las tablas  TAPASO_CECO y FRTPPASO_CECO; IP " + FRQConstantes.getIpOrigen()));
                    } else {
                        resultGeneral = true;
                        logger.info("*************Proceso correcto, merge entre las tablas  TAPASO_CECO y FRTPPASO_CECO*************" + CorreoBI.mergeCecos(destinatario = "", asunto = "Carga de Cecos", cuerpoCorreo = "Termina carga de cecos exitosamente;IP  " + FRQConstantes.getIpOrigen()));
                    }
                } else {
                    logger.info("*************Error al insertar en la tabla  TAPASO_CECO*************" + CorreoBI.mergeCecos(destinatario = "", asunto = "Carga de Cecos", cuerpoCorreo = "Error al insertar en la tabla  TAPASO_CECO; IP: " + FRQConstantes.getIpOrigen()));
                }
            } else {
                logger.info("*************Error al obtener el token*************" + CorreoBI.mergeCecos(destinatario = "", asunto = "Carga de Cecos", cuerpoCorreo = "Error al obtener el token; IP " + FRQConstantes.getIpOrigen()));
            }
        } catch (Exception c) {

            logger.info("*************Error al ejecutar el metodo ejecutaProcesosCecosGeneral************" + c + CorreoBI.mergeCecos(destinatario = "", asunto = "Carga de Cecos", cuerpoCorreo = "Error al ejecutar el metodo ejecutaProcesosCecosGeneral; IP " + FRQConstantes.getIpOrigen()));
        }
        return resultGeneral;
    }

    public boolean cargaTablaPasoATablaTrabajoCeco() {
        boolean resultado = false;
        logger.info("*************Comienza merge de la tabla  FRTPPASO_CECO al FRTACECO *************");

        try {
            ParametroBI paramBI = (ParametroBI) FRQAppContextProvider.getApplicationContext()
                    .getBean("parametroBI");
            int paramTotal = Integer.parseInt(paramBI.obtieneParametros("numCargas").get(0).getValor());

            CecoBI cecoBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
            logger.info("Elimina Duplicados... " + cecoBI.eliminaDuplicados());

            for (int j = 1; j <= paramTotal; j++) {
                ParametroDTO parametroDTO = new ParametroDTO();
                parametroDTO.setClave("tipoCarga");
                parametroDTO.setValor("" + j);
                parametroDTO.setActivo(1);
                ParametroBI parametroBI = (ParametroBI) FRQAppContextProvider.getApplicationContext()
                        .getBean("parametroBI");
                logger.info("Actualiza Parametro... " + parametroBI.actualizaParametro(parametroDTO));

                cecoBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
                if (j == 1) {
                    logger.info("Carga CecoBAZ... " + cecoBI.cargaBazPasoTrabajo());
                    resultado = true;
                }
                if (j == 2) {
                    logger.info("Carga CecoGCC... " + cecoBI.cargaGCCPasoTrabajo());
                    resultado = true;
                }
                if (j == 3) {
                    logger.info("Carga CecoCanalesTerceros... " + cecoBI.cargaCanalesTercerosPasoTrabajo());
                    resultado = true;
                }
                if (j == 4) {
                    logger.info("Carga CecoEKT... " + cecoBI.cargaEKTPasoTrabajo());
                    resultado = true;
                }
                if (j == 5) {
                    logger.info("Carga Micronegocios... " + cecoBI.cargaMicronegocioPasoTrabajo());
                    resultado = true;

                }
                if (j == 6) {
                    logger.info("Carga CecoPPR... " + cecoBI.cargaPPRPasoTrabajo());
                    resultado = true;
                }

            }

        } catch (Exception e) {
            logger.info("Algo Ocurrió Carga tabla de paso a tabla de trabajo... " + e.getMessage());
            resultado = false;
        }
        return resultado;
    }
}
