package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ProtocoloDAO;
import com.gruposalinas.checklist.domain.ProtocoloDTO;

public class ProtocoloBI {
		
	private static Logger logger = LogManager.getLogger(CheckinBI.class);
	
	@Autowired
	ProtocoloDAO protocoloDAO;
	
	List<ProtocoloDTO> listaProtocolo = null;
	
	public List<ProtocoloDTO> consultaProtocolo(int idProtocolo){
		
		List<ProtocoloDTO> respuesta = null ;
		
		try {
			respuesta = protocoloDAO.obtieneProtocolo(idProtocolo);
		} catch (Exception e) {
			logger.info("No fue posible consultar el protocolo");
		}
		
		return respuesta;		
	}
	
	public int insertaProtocolo(ProtocoloDTO bean){
		
		int respuesta = 0;
		
		try {
			respuesta = protocoloDAO.insertaProtocolo(bean);			
		} catch (Exception e) {
			logger.info("No fue posible insertar el protocolo");
		}
		
		return respuesta;
		}
		
	public boolean modificaProtocolo(ProtocoloDTO bean){
		
		boolean respuesta = false ;
		
		try {
			respuesta = protocoloDAO.modificaProtocolo(bean);
		} catch (Exception e) {
			logger.info("No fue posible modificar el protocolo");
		}
		
		return respuesta;
	}
	
	public boolean eliminaProtocolo(int idProtocolo){
		
		boolean respuesta = false ;
		
		try {
			respuesta = protocoloDAO.eliminaProtocolo(idProtocolo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar el protocolo");
		}
		
		return respuesta;		
	}
	
        
	public List<ProtocoloDTO> getProtocolos(){
		
		List<ProtocoloDTO> respuesta = null ;
		
		try {
			respuesta = protocoloDAO.getProtocolos();
		} catch (Exception e) {
			logger.info("No fue posible consultar los protocolos");
		}
		
		return respuesta;		
	}
	
}
