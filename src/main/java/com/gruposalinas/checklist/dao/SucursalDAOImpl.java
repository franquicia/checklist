package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.SucursalDTO;
import com.gruposalinas.checklist.mappers.SucursalGCCRowMapper;
import com.gruposalinas.checklist.mappers.SucursalPasoRowMapper;
import com.gruposalinas.checklist.mappers.SucursalRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class SucursalDAOImpl extends DefaultDAO implements SucursalDAO {

    private static Logger logger = LogManager.getLogger(SucursalDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneSucursal;
    DefaultJdbcCall jdbcObtieneSucursalP;
    DefaultJdbcCall jdbcCargaSucursal;
    DefaultJdbcCall jdbcActualizaPais;
    DefaultJdbcCall jdbcInsertaSucursal;
    DefaultJdbcCall jdbcActualizaSucursal;
    DefaultJdbcCall jdbcEliminaSucursal;
    DefaultJdbcCall jdbcRellenaSucursal;

    //Gerencias Credito y Cobranza
    DefaultJdbcCall jdbcObtieneSucursalGCC;
    DefaultJdbcCall jdbcInsertaSucursalGCC;
    DefaultJdbcCall jdbcActualizaSucursalGCC;
    DefaultJdbcCall jdbcEliminaSucursalGCC;

    public void init() {
        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_SUCURSAL")
                .withProcedureName("SP_SEL_G_SUCURSAL")
                .returningResultSet("RCL_SUCURSAL", new SucursalRowMapper());

        jdbcObtieneSucursal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_SUCURSAL")
                .withProcedureName("SP_SEL_SUCURSAL")
                .returningResultSet("RCL_SUCURSAL", new SucursalRowMapper());

        jdbcObtieneSucursalP = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PATABPASO")
                .withProcedureName("SP_SEL_SUCURSAL")
                .returningResultSet("RCL_SUCURSAL", new SucursalPasoRowMapper());

        jdbcCargaSucursal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASUCURSAL")
                .withProcedureName("SP_CARGA_SUCURSAL");

        jdbcActualizaPais = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_SUCURSAL")
                .withProcedureName("SP_ACT_PAIS");

        jdbcInsertaSucursal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_SUCURSAL")
                .withProcedureName("SP_INS_SUCURSAL");

        jdbcActualizaSucursal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_SUCURSAL")
                .withProcedureName("SP_ACT_SUCURSAL");

        jdbcEliminaSucursal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_SUCURSAL")
                .withProcedureName("SP_DEL_SUCURSAL");

        //Gerencias Credito y Cobranza
        jdbcObtieneSucursalGCC = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSUCGCC")
                .withProcedureName("SP_CONSULTA")
                .returningResultSet("RCL_CONSULTA", new SucursalGCCRowMapper());

        jdbcInsertaSucursalGCC = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSUCGCC")
                .withProcedureName("SP_INSERTA");

        jdbcActualizaSucursalGCC = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSUCGCC")
                .withProcedureName("SP_ACTUALIZA");

        jdbcEliminaSucursalGCC = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSUCGCC")
                .withProcedureName("SP_ELIMINA");

        jdbcRellenaSucursal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASUCURSAL")
                .withProcedureName("SP_RELLENA_NUM");

    }

    @SuppressWarnings("unchecked")
    public List<SucursalDTO> obtieneSucursal() throws Exception {
        Map<String, Object> out = null;
        List<SucursalDTO> listaSucursal = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funcion ejecutada: {checklist.PA_ADM_SUCURSAL.SP_SEL_G_SUCURSAL}");

        listaSucursal = (List<SucursalDTO>) out.get("RCL_SUCURSAL");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener las Sucursales");
        } else {
            return listaSucursal;
        }

        return listaSucursal;

    }

    @SuppressWarnings("unchecked")
    public List<SucursalDTO> obtieneSucursal(String idSucursal) throws Exception {
        Map<String, Object> out = null;
        List<SucursalDTO> listaSucursal = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_SUC", idSucursal);

        out = jdbcObtieneSucursal.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_SUCURSAL.SP_SEL_SUCURSAL}");

        listaSucursal = (List<SucursalDTO>) out.get("RCL_SUCURSAL");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener la Sucursal con id(" + idSucursal + ")");
        } else {
            return listaSucursal;
        }

        return null;
    }

    public boolean cargaSucursales() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaSucursal.execute();

        logger.info("Funcion ejecutada: {checklist.PASUCURSAL.SP_CARGA_SUCURSAL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar la Carga de Sucursales");
        } else {
            return true;
        }

        return false;
    }

    public int insertaSucursal(SucursalDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idSucursal = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_SUC", bean.getIdSucursal())
                .addValue("PA_IDPAIS", bean.getIdPais())
                .addValue("PA_IDCANAL", bean.getIdCanal())
                .addValue("PA_ID_NU_SUC", bean.getNuSucursal())
                .addValue("PA_NOMBRECC", bean.getNombresuc())
                .addValue("PA_LONGITUD", bean.getLongitud())
                .addValue("PA_LATITUD", bean.getLatitud());

        out = jdbcInsertaSucursal.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_SUCURSAL.SP_INS_SUCURSAL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_ID_SUC");
        idSucursal = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar la Sucursal");
        } else {
            return idSucursal;
        }

        return idSucursal;
    }

    public boolean actualizaSucursal(SucursalDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_SUC", bean.getIdSucursal())
                .addValue("PA_IDPAIS", bean.getIdPais())
                .addValue("PA_IDCANAL", bean.getIdCanal())
                .addValue("PA_ID_NU_SUC", bean.getNuSucursal())
                .addValue("PA_NOMBRECC", bean.getNombresuc())
                .addValue("PA_LONGITUD", bean.getLongitud())
                .addValue("PA_LATITUD", bean.getLatitud());

        out = jdbcActualizaSucursal.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_SUCURSAL.SP_ACT_SUCURSAL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar Sucursal  id( " + bean.getIdSucursal() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaSucursal(int idSucursal) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_SUC", idSucursal);

        out = jdbcEliminaSucursal.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_SUCURSAL.SP_DEL_SUCURSAL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al borrar la Sucursal id(" + idSucursal + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<SucursalDTO> obtieneSucursalPaso(String idSucursal) throws Exception {
        Map<String, Object> out = null;
        List<SucursalDTO> listaSucursal = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDNUMSUC", idSucursal);

        out = jdbcObtieneSucursalP.execute(in);

        logger.info("Funcion ejecutada: {checklist.PATABPASO.SP_SEL_SUCURSAL}");

        listaSucursal = (List<SucursalDTO>) out.get("RCL_SUCURSAL");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener la Sucursal con id(" + idSucursal + ")");
        } else {
            return listaSucursal;
        }

        return null;
    }

    // Metodos para Gerencias de Credito y Cobranza
    @SuppressWarnings("unchecked")
    public List<SucursalDTO> obtieneSucursalGCC(String idSucursal) throws Exception {
        Map<String, Object> out = null;
        List<SucursalDTO> listaSucursal = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NUSUCURSAL", idSucursal);

        out = jdbcObtieneSucursalGCC.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMSUCGCC.SP_SEL_SUCURSAL}");

        listaSucursal = (List<SucursalDTO>) out.get("RCL_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener la Sucursal con id(" + idSucursal + ")");
        }

        return listaSucursal;
    }

    public int insertaSucursalGCC(SucursalDTO bean) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        int idSucursal = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_PAIS", bean.getIdPais())
                .addValue("PA_CANAL", bean.getIdCanal())
                .addValue("PA_NUSUCURSAL", bean.getNuSucursal())
                .addValue("PA_NOMBRE", bean.getNombresuc())
                .addValue("PA_LONGITUD", bean.getLongitud())
                .addValue("PA_LATITUD", bean.getLatitud());

        out = jdbcInsertaSucursalGCC.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMSUCGCC.SP_INSERTA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_ID_SUC");
        idSucursal = idReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo paso al insertar la Sucursal");
        }

        return idSucursal;
    }

    public boolean actualizaSucursalGCC(SucursalDTO bean) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDSUCURSAL", bean.getIdSucursal())
                .addValue("PA_PAIS", bean.getIdPais())
                .addValue("PA_CANAL", bean.getIdCanal())
                .addValue("PA_NUSUCURSAL", bean.getNuSucursal())
                .addValue("PA_NOMBRE", bean.getNombresuc())
                .addValue("PA_LONGITUD", bean.getLongitud())
                .addValue("PA_LATITUD", bean.getLatitud())
                .addValue("PA_NUSUSCURSAL", "");

        out = jdbcActualizaSucursalGCC.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_SUCURSAL.SP_ACT_SUCURSAL}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        if (ejecucion == 0) {
            logger.info("Algo paso al actualizar Sucursal  id( " + bean.getIdSucursal() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaSucursalGCC(int idSucursal) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDSUCURSAL", idSucursal);

        out = jdbcEliminaSucursalGCC.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMSUCGCC.SP_ELIMINA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        if (ejecucion == 0) {
            logger.info("Algo paso al borrar la Sucursal id(" + idSucursal + ")");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargaActualizaPaises() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcActualizaPais.execute();

        logger.info("Funcion ejecutada: {checklist.PA_ADM_SUCURSAL.SP_ACT_PAIS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar la Actualización de Sucursales");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean rellenaSucursal() throws Exception {

        Map<String, Object> out = null;
        int ejecucion = 0;

        out = jdbcRellenaSucursal.execute();

        logger.info("Funcion ejecutada: {checklist.PASUCURSAL.SP_RELLENA_NUM}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        if (ejecucion == 1) {
            return true;
        } else {
            logger.info("Algo paso al actualizar las sucursales que inician con 0");
            return false;
        }

    }

}
