package com.gruposalinas.checklist.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.GenericFilterBean;

public class FilterWsdl extends GenericFilterBean {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		
	    final HttpServletRequest httpRequest = (HttpServletRequest) request;
	    final HttpServletResponse httpResponse = (HttpServletResponse) response;
	    //logger.info("request lenght " + request.getContentLength());
	    //logger.info(" URL Servicio WSDL: " + httpRequest.getRequestURL());
	    //logger.info("response  buffer size" + response.getBufferSize());
	    
	    try {
	         
	    	 if(httpRequest.getMethod().compareTo("POST")==0)
		    		chain.doFilter(httpRequest,httpResponse);
	    	 } catch (IOException e) {
	    		 //logger.info("FILTER CASE 1");
	    	 }catch(ServletException e) {
	    		 //logger.info("FILTER CASE 2");
	    	 }catch(RuntimeException e) {
	    		 //logger.info("FILTER CASE 3");
	    	 }catch(Exception e) {
	    		 //logger.info("FILTER CASE 4");
	    	 }

	 
	}
	
	

}
