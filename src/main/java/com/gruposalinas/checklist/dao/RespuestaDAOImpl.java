package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.RespuestaDTO;
import com.gruposalinas.checklist.mappers.RespuestaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class RespuestaDAOImpl extends DefaultDAO implements RespuestaDAO {

    private static Logger logger = LogManager.getLogger(RespuestaDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneRespuesta;
    DefaultJdbcCall jdbcInsertaRespuesta;
    DefaultJdbcCall jdbcInsertaUnaResp;
    DefaultJdbcCall jdbcInsertaNuevaResp;
    DefaultJdbcCall jdbcActualizaFechaResp;
    DefaultJdbcCall jdbcActualizaRespuesta;
    DefaultJdbcCall jdbcEliminaRespuesta;
    DefaultJdbcCall jdbcEliminaRespuestas;
    DefaultJdbcCall jdbcEliminaRespuestasD;
    DefaultJdbcCall jdbcRegistraRespuestas;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_RESP")
                .withProcedureName("SP_SEL_G_RESP")
                .returningResultSet("RCL_RESP", new RespuestaRowMapper());

        jdbcObtieneRespuesta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_RESP")
                .withProcedureName("SP_SEL_RESP")
                .returningResultSet("RCL_RESP", new RespuestaRowMapper());

        jdbcInsertaRespuesta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_RESP")
                .withProcedureName("SP_INS_RESP");

        jdbcInsertaUnaResp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPUESTAS")
                .withProcedureName("SP_REGISTRA_PREGUNTA");

        jdbcInsertaNuevaResp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPUESTAS")
                .withProcedureName("SP_NUEVA_RESP");

        jdbcActualizaRespuesta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_RESP")
                .withProcedureName("SP_ACT_RESP");

        jdbcActualizaFechaResp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_RESP")
                .withProcedureName("SP_ACT_F_RESP");

        jdbcEliminaRespuesta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_RESP")
                .withProcedureName("SP_DEL_RESP");

        jdbcEliminaRespuestas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPUESTAS")
                .withProcedureName("SP_ELIMINA_RESP");

        jdbcEliminaRespuestasD = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PARESPUESTAS")
                .withProcedureName("SPELIMINADUPLICADOS");

        jdbcRegistraRespuestas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKLIST")
                .withProcedureName("SP_REGISTA_RPTAS");
    }

    @SuppressWarnings("unchecked")
    public List<RespuestaDTO> obtieneRespuesta() throws Exception {
        Map<String, Object> out = null;
        List<RespuestaDTO> listaRespuesta = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funcion ejecutada: {checklist.PA_ADM_RESP.SP_SEL_G_RESP}");

        listaRespuesta = (List<RespuestaDTO>) out.get("RCL_RESP");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener las Respuestas");
        } else {
            return listaRespuesta;
        }

        return listaRespuesta;

    }

    @SuppressWarnings("unchecked")
    public List<RespuestaDTO> obtieneRespuesta(String idArbol, String idRespuesta, String idBitacora) throws Exception {
        Map<String, Object> out = new HashMap<String, Object>();
        List<RespuestaDTO> listaRespuesta = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_ARBOL", idArbol)
                .addValue("PA_IDRESPUESTA", idRespuesta)
                .addValue("PA_IDBITACORA", idBitacora);

        logger.info(idArbol + " " + idRespuesta + " " + idBitacora + "  dd ");

        out = jdbcObtieneRespuesta.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_RESP.SP_SEL_RESP}");

        listaRespuesta = (List<RespuestaDTO>) out.get("RCL_RESP");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener las Respuestas");
        } else {
            return listaRespuesta;
        }

        return null;
    }

    public int insertaRespuesta(RespuestaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idRespuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_USUARIO", bean.getIdCheckUsuario())
                .addValue("PA_ID_BITACORA", bean.getIdBitacora())
                .addValue("PA_ID_ARBOL_DES", bean.getIdArboldecision())
                .addValue("PA_OBSERVACION", bean.getObservacion())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcInsertaRespuesta.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_RESP.SP_INS_RESP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDRESP");
        idRespuesta = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar la Respuesta");
        } else {
            return idRespuesta;
        }

        return idRespuesta;

    }

    public boolean actualizaRespuesta(RespuestaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_RESP", bean.getIdRespuesta())
                .addValue("PA_ID_USUARIO", bean.getIdCheckUsuario())
                .addValue("PA_ID_BITACORA", bean.getIdBitacora())
                .addValue("PA_ID_ARBOL_DES", bean.getIdArboldecision())
                .addValue("PA_OBSERVACION", bean.getObservacion());

        out = jdbcActualizaRespuesta.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_RESP.SP_ACT_RESP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar la Respuesta  id( " + bean.getIdRespuesta() + ")");
        } else {
            return true;
        }

        return false;

    }

    public boolean eliminaRespuesta(int idResp) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_RESP", idResp);

        out = jdbcEliminaRespuesta.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_RESP.SP_DEL_RESP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al borrar la Respuesta id(" + idResp + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean registraRespuestas(int idCheckUsua, BitacoraDTO bitacora, String idRespuestas, String compromisos, String evidencias) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDBITACORA", bitacora.getIdBitacora())
                .addValue("PA_LONGITUD", bitacora.getLongitud())
                .addValue("PA_LATITUD", bitacora.getLatitud())
                .addValue("PA_CHECKUSUA", idCheckUsua)
                .addValue("PA_IDRESPUESTAS", idRespuestas)
                .addValue("PA_COMPROMISOS", compromisos)
                .addValue("PA_EVIDENCIAS", evidencias);

        out = jdbcRegistraRespuestas.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_CHECKLIST.SP_REGISTA_RPTAS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar las Respuestas");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean insertaUnaRespuesta(int idCheckU, int idBitacora, int idPreg, int idArbol, String obsv, String compromiso, String evidencia, String respAd) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCHECKUSUA", idCheckU)
                .addValue("PA_IDBITACORA", idBitacora)
                .addValue("PA_IDPREGUNTA", idPreg)
                .addValue("PA_IDARBOL", idArbol)
                .addValue("PA_OBSV", obsv)
                .addValue("PA_COMPROMISO", compromiso)
                .addValue("PA_EVIDENCIA", evidencia)
                .addValue("PA_RESPUESTA_AD", respAd);

        out = jdbcInsertaUnaResp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PARESPUESTAS.SP_REGISTRA_PREGUNTA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar la Respuesta con el idArbol: " + idArbol + " ");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaRespuestas(String respuestas) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_RESP", respuestas);

        out = jdbcEliminaRespuestas.execute(in);

        logger.info("Funcion ejecutada: {checklist.PARESPUESTAS.SP_ELIMINA_RESP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al borrar las Respuestas");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaFechaResp(String idRespuesta, String fechaTermino, String commit) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDRESPUESTA", idRespuesta)
                .addValue("PA_FECHA", fechaTermino).addValue("PA_COMMIT", commit);

        out = jdbcActualizaFechaResp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_RESP.SP_ACT_F_RESP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar la Respuesta  id( " + idRespuesta + ")");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaRespuestasDuplicadas() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcEliminaRespuestasD.execute();

        logger.info("Funcion ejecutada: {checklist.PARESPUESTAS.SPELIMINADUPLICADOS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al borrar las Respuestas");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean insertaRespuestaNueva(int idCheckU, int idBitacora, int idPreg, int idArbol, String obsv,
            String compromiso, String evidencia, String respAd) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCHECKUSUA", idCheckU)
                .addValue("PA_IDBITACORA", idBitacora)
                .addValue("PA_IDPREGUNTA", idPreg)
                .addValue("PA_IDARBOL", idArbol)
                .addValue("PA_OBSV", obsv)
                .addValue("PA_COMPROMISO", compromiso)
                .addValue("PA_EVIDENCIA", evidencia)
                .addValue("PA_RESPUESTA_AD", respAd);

        out = jdbcInsertaNuevaResp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PARESPUESTAS.SP_NUEVA_RESP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar la Respuesta con el idArbol: " + idArbol + " ");
        } else {
            return true;
        }

        return false;
    }

}
