package com.gruposalinas.checklist.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.ChecklistHallazgosRepoDAO;
import com.gruposalinas.checklist.domain.ChecklistHallazgosRepoDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.HallazgosExpDTO;

public class HallazgosRepoBI {

	private static Logger logger = LogManager.getLogger(HallazgosRepoBI.class);

	private List<ChecklistHallazgosRepoDTO> listafila;
	private Map<String, Object> lista = null;

	@Autowired
	ChecklistHallazgosRepoDAO checklistHallazgosRepoDAO;

	public List<ChecklistHallazgosRepoDTO> obtieneDatosFecha(String fechaIni, String fechaFin) {

		try {

			listafila = checklistHallazgosRepoDAO.obtieneDatosFecha(fechaIni, fechaFin);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	public String cursores(String bitgral) {
		// List<ChecklistHallazgosRepoDTO> response = new
		// ArrayList<ChecklistHallazgosRepoDTO>();
		JSONObject respuesta = new JSONObject();
		try {

			Map<String, Object> listaPregs = null;
			listaPregs = checklistHallazgosRepoDAO.cursores(bitgral);
			
		
			@SuppressWarnings("unchecked")
			List<HallazgosExpDTO> listahallazgotab = (List<HallazgosExpDTO>) listaPregs.get("hallazgostabla");
			@SuppressWarnings("unchecked")
			List<HallazgosExpDTO> listaReconue = (List<HallazgosExpDTO>) listaPregs.get("listaReconue");
			@SuppressWarnings("unchecked")
			List<HallazgosExpDTO> hallazgosultreco = (List<HallazgosExpDTO>) listaPregs.get("hallazgosultreco");
			
			JSONArray hallatab = new JSONArray();
			JSONArray copia=new JSONArray();
			JSONArray reco=new JSONArray();
			
			// lista hallazgos tabla

			//for (HallazgosExpDTO aux : listahallazgotab) {
				for (int i = 0; i < listahallazgotab.size(); i++) {

					JSONObject obj = new JSONObject();
			
					obj.put("bitacoraGral", listahallazgotab.get(i).getBitGral());
					obj.put("idFolio",listahallazgotab.get(i).getIdFolio());
					obj.put("idRespuesta",listahallazgotab.get(i).getIdResp());
					obj.put("status",listahallazgotab.get(i).getStatus());
					obj.put("idPreg",listahallazgotab.get(i).getIdPreg());
					obj.put("posible",listahallazgotab.get(i).getRespuesta());
					obj.put("pregPadre",listahallazgotab.get(i).getPregPadre());
					obj.put("responsable",listahallazgotab.get(i).getResponsable());
					obj.put("obsNuev",listahallazgotab.get(i).getObsNueva());
					obj.put("obsAnt",listahallazgotab.get(i).getObs());
					obj.put("idCheck",listahallazgotab.get(i).getIdcheck());
					obj.put("ruta",listahallazgotab.get(i).getRuta());
					obj.put("bitgral",listahallazgotab.get(i).getBitGral());
					obj.put("fechaIni",listahallazgotab.get(i).getFechaIni());
					obj.put("fechaFin",listahallazgotab.get(i).getFechaFin());
					obj.put("fechaAutorizo",listahallazgotab.get(i).getFechaAutorizo());
					obj.put("ceco",listahallazgotab.get(i).getCeco());
					obj.put("reco",listahallazgotab.get(i).getRecorrido());
					obj.put("pregunta",listahallazgotab.get(i).getPreg());
					
					hallatab.put(obj);
					
				}
			//}
			
			// lista hallazgos copia
				for (int i = 0; i < listaReconue.size(); i++) {

					JSONObject obj = new JSONObject();
			

					obj.put("idFolio",listaReconue.get(i).getIdFolio());
					
					obj.put("idRespuesta",listaReconue.get(i).getIdResp());
					obj.put("bitacora",listaReconue.get(i).getBitacora());
					obj.put("obsNuev",listaReconue.get(i).getObsNueva());
					obj.put("idCheck",listaReconue.get(i).getIdcheck());
					obj.put("idPreg",listaReconue.get(i).getIdPreg());
					obj.put("posible",listaReconue.get(i).getRespuesta());
					obj.put("pregPadre",listaReconue.get(i).getPregPadre());
					obj.put("pregunta",listaReconue.get(i).getPreg());
					obj.put("bitacoraGral", listaReconue.get(i).getBitGral());
					obj.put("ceco",listaReconue.get(i).getCeco());
					obj.put("reco",listaReconue.get(i).getRecorrido());
					obj.put("ruta",listaReconue.get(i).getRuta());

					
					copia.put(obj);
					
				}
			
			// lista hallazgos bita gral
						
							for (int i = 0; i < hallazgosultreco.size(); i++) {

								JSONObject obj = new JSONObject();
						
								obj.put("bitacoraGral", hallazgosultreco.get(i).getBitGral());
								obj.put("idFolio",hallazgosultreco.get(i).getIdFolio());
								obj.put("idRespuesta",hallazgosultreco.get(i).getIdResp());
								obj.put("bitacora",hallazgosultreco.get(i).getBitacora());
								obj.put("status",hallazgosultreco.get(i).getStatus());
								obj.put("idPreg",hallazgosultreco.get(i).getIdPreg());
								obj.put("posible",hallazgosultreco.get(i).getRespuesta());
								obj.put("pregPadre",hallazgosultreco.get(i).getPregPadre());
								obj.put("responsable",hallazgosultreco.get(i).getResponsable());
								obj.put("obsNuev",hallazgosultreco.get(i).getObsNueva());
								obj.put("obsAnt",hallazgosultreco.get(i).getObs());
								obj.put("idCheck",hallazgosultreco.get(i).getIdcheck());
								obj.put("ruta",hallazgosultreco.get(i).getRuta());
								obj.put("bitgral",hallazgosultreco.get(i).getBitGral());
								obj.put("fechaIni",hallazgosultreco.get(i).getFechaIni());
								obj.put("fechaFin",hallazgosultreco.get(i).getFechaFin());
								obj.put("fechaAutorizo",hallazgosultreco.get(i).getFechaAutorizo());
								obj.put("ceco",hallazgosultreco.get(i).getCeco());
								obj.put("reco",hallazgosultreco.get(i).getRecorrido());
								obj.put("pregunta",hallazgosultreco.get(i).getPreg());
								
							
								reco.put(obj);
								
							}
						
			respuesta.append("listahallazgotab", hallatab);
			respuesta.append("listaReconue", copia);
			respuesta.append("hallazgosultreco", reco);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta.toString();
	}
	
	public String cursoresNvo(String bitgral) {
		// List<ChecklistHallazgosRepoDTO> response = new
		// ArrayList<ChecklistHallazgosRepoDTO>();
		JSONObject respuesta = new JSONObject();
		try {

			Map<String, Object> listaPregs = null;
			listaPregs = checklistHallazgosRepoDAO.cursores(bitgral);
			
		
			@SuppressWarnings("unchecked")
			List<HallazgosExpDTO> listahallazgotab = (List<HallazgosExpDTO>) listaPregs.get("hallazgostabla");
			@SuppressWarnings("unchecked")
			List<HallazgosExpDTO> listaReconue = (List<HallazgosExpDTO>) listaPregs.get("listaReconue");
			@SuppressWarnings("unchecked")
			List<HallazgosExpDTO> hallazgosultreco = (List<HallazgosExpDTO>) listaPregs.get("hallazgosultreco");
			
			JSONArray hallatab = new JSONArray();
			JSONArray copia=new JSONArray();
			JSONArray reco=new JSONArray();
			
			// lista hallazgos tabla

			//for (HallazgosExpDTO aux : listahallazgotab) {
				for (int i = 0; i < listahallazgotab.size(); i++) {

					JSONObject obj = new JSONObject();
			
					obj.put("bitacoraGral", listahallazgotab.get(i).getBitGral());
					obj.put("idFolio",listahallazgotab.get(i).getIdFolio());
					obj.put("idRespuesta",listahallazgotab.get(i).getIdResp());
					obj.put("status",listahallazgotab.get(i).getStatus());
					obj.put("idPreg",listahallazgotab.get(i).getIdPreg());
					obj.put("posible",listahallazgotab.get(i).getRespuesta());
					obj.put("pregPadre",listahallazgotab.get(i).getPregPadre());
					obj.put("responsable",listahallazgotab.get(i).getResponsable());
					obj.put("obsNuev",listahallazgotab.get(i).getObsNueva());
					obj.put("obsAnt",listahallazgotab.get(i).getObs());
					obj.put("idCheck",listahallazgotab.get(i).getIdcheck());
					obj.put("ruta",listahallazgotab.get(i).getRuta());
					obj.put("bitgral",listahallazgotab.get(i).getBitGral());
					obj.put("fechaIni",listahallazgotab.get(i).getFechaIni());
					obj.put("fechaFin",listahallazgotab.get(i).getFechaFin());
					obj.put("fechaAutorizo",listahallazgotab.get(i).getFechaAutorizo());
					obj.put("ceco",listahallazgotab.get(i).getCeco());
					obj.put("reco",listahallazgotab.get(i).getRecorrido());
					obj.put("pregunta",listahallazgotab.get(i).getPreg());
					
					hallatab.put(obj);
					
				}
			//}
			
			// lista hallazgos copia
				for (int i = 0; i < listaReconue.size(); i++) {

					JSONObject obj = new JSONObject();
			

					obj.put("idFolio",listaReconue.get(i).getIdFolio());
					
					obj.put("idRespuesta",listaReconue.get(i).getIdResp());
					obj.put("bitacora",listaReconue.get(i).getBitacora());
					obj.put("obsNuev",listaReconue.get(i).getObsNueva());
					obj.put("idCheck",listaReconue.get(i).getIdcheck());
					obj.put("idPreg",listaReconue.get(i).getIdPreg());
					obj.put("posible",listaReconue.get(i).getRespuesta());
					obj.put("pregPadre",listaReconue.get(i).getPregPadre());
					obj.put("pregunta",listaReconue.get(i).getPreg());
					obj.put("bitacoraGral", listaReconue.get(i).getBitGral());
					obj.put("ceco",listaReconue.get(i).getCeco());
					obj.put("reco",listaReconue.get(i).getRecorrido());
					obj.put("ruta",listaReconue.get(i).getRuta());

					
					copia.put(obj);
					
				}
			
			// lista hallazgos bita gral
						
							for (int i = 0; i < hallazgosultreco.size(); i++) {

								JSONObject obj = new JSONObject();
						
								obj.put("bitacoraGral", hallazgosultreco.get(i).getBitGral());
								obj.put("idFolio",hallazgosultreco.get(i).getIdFolio());
								obj.put("idRespuesta",hallazgosultreco.get(i).getIdResp());
								obj.put("bitacora",hallazgosultreco.get(i).getBitacora());
								obj.put("status",hallazgosultreco.get(i).getStatus());
								obj.put("idPreg",hallazgosultreco.get(i).getIdPreg());
								obj.put("posible",hallazgosultreco.get(i).getRespuesta());
								obj.put("pregPadre",hallazgosultreco.get(i).getPregPadre());
								obj.put("responsable",hallazgosultreco.get(i).getResponsable());
								obj.put("obsNuev",hallazgosultreco.get(i).getObsNueva());
								obj.put("obsAnt",hallazgosultreco.get(i).getObs());
								obj.put("idCheck",hallazgosultreco.get(i).getIdcheck());
								obj.put("ruta",hallazgosultreco.get(i).getRuta());
								obj.put("bitgral",hallazgosultreco.get(i).getBitGral());
								obj.put("fechaIni",hallazgosultreco.get(i).getFechaIni());
								obj.put("fechaFin",hallazgosultreco.get(i).getFechaFin());
								obj.put("fechaAutorizo",hallazgosultreco.get(i).getFechaAutorizo());
								obj.put("ceco",hallazgosultreco.get(i).getCeco());
								obj.put("reco",hallazgosultreco.get(i).getRecorrido());
								obj.put("pregunta",hallazgosultreco.get(i).getPreg());
								
							
								reco.put(obj);
								
							}
						
			respuesta.append("listahallazgotab", hallatab);
			respuesta.append("listaReconue", copia);
			respuesta.append("hallazgosultreco", reco);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta.toString();
	}
	
	public List<ChecklistHallazgosRepoDTO> obtieneDatosNego(String bita) throws Exception {

		try {

			listafila = checklistHallazgosRepoDAO.obtieneDatosNego(bita);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}
	

	public List<ChecklistHallazgosRepoDTO> obtieneDatosNegoExp(String ceco,String fase,String proyecto) throws Exception {
		try {
			listafila = checklistHallazgosRepoDAO.obtieneDatosNegoExp(ceco,fase,proyecto);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}
	
	public List<ChecklistHallazgosRepoDTO> obtieneDatosNegoCore(String bita,int ceco,int fase,int proyecto) throws Exception {

		try {

			listafila = checklistHallazgosRepoDAO.obtieneDatosNegoExp(ceco+"",fase+"",proyecto+"");
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

}