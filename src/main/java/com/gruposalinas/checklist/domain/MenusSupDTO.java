package com.gruposalinas.checklist.domain;

public class MenusSupDTO {
	
	private int idMenu;
	private String descMenu;
	private int activo;
	private String usuaModificado;
	private String fechaModificacion;
	
	public int getIdMenu() {
		return idMenu;
	}
	public void setIdMenu(int idMenu) {
		this.idMenu = idMenu;
	}
	public String getDescMenu() {
		return descMenu;
	}
	public void setDescMenu(String descMenu) {
		this.descMenu = descMenu;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	public String getUsuaModificado() {
		return usuaModificado;
	}
	public void setUsuaModificado(String usuaModificado) {
		this.usuaModificado = usuaModificado;
	}
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	@Override
	public String toString() {
		return "MenusSupDTO [idMenu=" + idMenu + ", descMenu=" + descMenu + ", activo=" + activo + ", usuaModificado="
				+ usuaModificado + ", fechaModificacion=" + fechaModificacion + "]";
	}

}


