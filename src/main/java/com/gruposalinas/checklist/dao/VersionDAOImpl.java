package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.VersionDTO;
import com.gruposalinas.checklist.mappers.VersionRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class VersionDAOImpl extends DefaultDAO implements VersionDAO {

    private static Logger logger = LogManager.getLogger(VersionDAOImpl.class);

    DefaultJdbcCall jdbcObtieneVersion;
    DefaultJdbcCall jdbcObtieneUltVersion;
    DefaultJdbcCall jdbcInsertaVersion;
    DefaultJdbcCall jdbcActualizaVersion;
    DefaultJdbcCall jdbcEliminaVersion;

    public void init() {

        jdbcObtieneVersion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSION")
                .withProcedureName("SP_SEL_VERSION")
                .returningResultSet("RCL_VERSION", new VersionRowMapper());

        jdbcObtieneUltVersion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSION")
                .withProcedureName("SP_ULT_VERSION");

        jdbcInsertaVersion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSION")
                .withProcedureName("SP_INS_VERSION");

        jdbcActualizaVersion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSION")
                .withProcedureName("SP_ACT_VERSION");

        jdbcEliminaVersion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERSION")
                .withProcedureName("SP_DEL_VERSION");
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<VersionDTO> obtieneVersion(String version, String sistema, String so) throws Exception {
        Map<String, Object> out = null;
        List<VersionDTO> listaVersion = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_VERSION", version)
                .addValue("PA_SISTEMA", sistema)
                .addValue("PA_SO", so);

        out = jdbcObtieneVersion.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMVERSION.SP_SEL_VERSION}");

        listaVersion = (List<VersionDTO>) out.get("RCL_VERSION");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener la Versión");
        } else {
            return listaVersion;
        }

        return null;
    }

    @Override
    public int insertaVersion(VersionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idVersion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_VERSION", bean.getVersion())
                .addValue("PA_SISTEMA", bean.getSistema())
                .addValue("PA_SO", bean.getSo())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcInsertaVersion.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMVERSION.SP_INS_VERSION}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDVERSION");
        idVersion = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar la Versión");
        } else {
            return idVersion;
        }

        return idVersion;
    }

    @Override
    public boolean actualizaVersion(VersionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDVERSION", bean.getIdVersion())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_VERSION", bean.getVersion())
                .addValue("PA_SISTEMA", bean.getSistema())
                .addValue("PA_SO", bean.getSo())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcActualizaVersion.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMVERSION.SP_ACT_VERSION}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar la Versión");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaVersion(int idVersion) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDVERSION", idVersion);

        out = jdbcEliminaVersion.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMVERSION.SP_DEL_VERSION}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar la Versión");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public List<String> obtieneUltVersion(String so) throws Exception {
        Map<String, Object> out = null;
        List<String> listaVersion = new ArrayList<String>();
        String version = "";
        String fecha = "";

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_SO", so);
        out = jdbcObtieneUltVersion.execute(in);
        logger.info("Funcion ejecutada: {checklist.PAADMVERSION.SP_ULT_VERSION}");

        version = (String) out.get("PA_VERSION");
        fecha = (String) out.get("PA_HORA_V");

        listaVersion.add(version);
        listaVersion.add(fecha);

        if (version.equals("0") || fecha.equals("")) {
            listaVersion = null;
        }

        return listaVersion;
    }
}
