package com.gruposalinas.checklist.domain;

public class PuestoDTO {
	
	private int idPuesto;
	private int idNivel;
	private int idNegocio;
	private int idTipoPuesto;
	private int idCanal;
	private String codigo;
	private String descripcion;
	private int idSubnegocio;
	private int activo;
	
	public int getIdPuesto() {
		return idPuesto;
	}
	public void setIdPuesto(int idPuesto) {
		this.idPuesto = idPuesto;
	}
	public int getIdNivel() {
		return idNivel;
	}
	public void setIdNivel(int idNivel) {
		this.idNivel = idNivel;
	}
	public int getIdNegocio() {
		return idNegocio;
	}
	public void setIdNegocio(int idNegocio) {
		this.idNegocio = idNegocio;
	}
	public int getIdTipoPuesto() {
		return idTipoPuesto;
	}
	public void setIdTipoPuesto(int idTipoPuesto) {
		this.idTipoPuesto = idTipoPuesto;
	}
	public int getIdCanal() {
		return idCanal;
	}
	public void setIdCanal(int idCanal) {
		this.idCanal = idCanal;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getIdSubnegocio() {
		return idSubnegocio;
	}
	public void setIdSubnegocio(int idSubnegocio) {
		this.idSubnegocio = idSubnegocio;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}

}
