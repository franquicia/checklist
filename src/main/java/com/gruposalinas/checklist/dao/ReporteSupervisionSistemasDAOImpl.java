package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ReporteSupervisionSistemasDTO;
import com.gruposalinas.checklist.mappers.FiltroUsuarioRowMapper;
import com.gruposalinas.checklist.mappers.ReporteCecosSupervisionSist2RowMapper;
import com.gruposalinas.checklist.mappers.ReporteCecosSupervisionSistRowMapper;
import com.gruposalinas.checklist.mappers.ReporteEvaluadorSupervisionSistRowMapper;
import com.gruposalinas.checklist.mappers.ReporteEvidenciaSupervisionSistRowMapper;
import com.gruposalinas.checklist.mappers.ReporteRespuestasSupervisionSistRowMapper;
import com.gruposalinas.checklist.mappers.ReporteSupervisionSisCapRowMapper;
import com.gruposalinas.checklist.mappers.ReporteSupervisionSistemasFiltroCheckRowMapper;
import com.gruposalinas.checklist.mappers.ReporteSupervisionSistemasFiltroSucursalRowMapper;
import com.gruposalinas.checklist.mappers.ReporteTotalesSupervisionSistRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ReporteSupervisionSistemasDAOImpl extends DefaultDAO implements ReporteSupervisionSistemasDAO {

    private static final Logger logger = LogManager.getLogger(ReporteSupervisionSistemasDAOImpl.class);

    private DefaultJdbcCall jdbcReporteSupervisionFiltroCheck;
    private DefaultJdbcCall jdbcReporteSupervisionFiltroSucursal;
    private DefaultJdbcCall jdbcReporteSupervisionFiltroUsuario;
    private DefaultJdbcCall jdbcReporteSisCap;
    private DefaultJdbcCall jdbcReporteSupervisionSistemas;
    private DefaultJdbcCall jdbcReporteRespuestas;
    private DefaultJdbcCall jdbcReporteEvidencia;
    private DefaultJdbcCall jdbcReporteSupervisionSistemas2;

    public void init() {
        jdbcReporteSupervisionFiltroCheck = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA").withCatalogName("PAREPSISTEMAS2").withProcedureName("SP_FILTRO_CHECK")
                .returningResultSet("RCL_CHECK", new ReporteSupervisionSistemasFiltroCheckRowMapper());

        jdbcReporteSupervisionFiltroSucursal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA").withCatalogName("PAREPSISTEMAS2").withProcedureName("SP_FILTRO_SUCURSAL")
                .returningResultSet("RCL_SUCURSAL", new ReporteSupervisionSistemasFiltroSucursalRowMapper());

        jdbcReporteSupervisionFiltroUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA").withCatalogName("PAREPSISTEMAS2").withProcedureName("SP_FILTRO_USUARIO")
                .returningResultSet("RCL_USUARIO", new FiltroUsuarioRowMapper());

        jdbcReporteSisCap = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPSISTEMAS").withProcedureName("SP_REPORTE_SISCAP")
                .returningResultSet("RCL_CECOS", new ReporteSupervisionSisCapRowMapper());

        jdbcReporteSupervisionSistemas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA").withCatalogName("PAREPSISTEMAS").withProcedureName("SP_REPORTE_SISTEMAS")
                .returningResultSet("RCL_TOTALES", new ReporteTotalesSupervisionSistRowMapper())
                .returningResultSet("RCL_PORCENTAJE", new ReporteCecosSupervisionSistRowMapper());

        jdbcReporteRespuestas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPSISTEMAS2").withProcedureName("SP_RESPUESTAS")
                .returningResultSet("PA_EVAL_FECHA", new ReporteEvaluadorSupervisionSistRowMapper())
                .returningResultSet("RCL_RESPUESTAS", new ReporteRespuestasSupervisionSistRowMapper());

        jdbcReporteEvidencia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPSISTEMAS2").withProcedureName("SP_EVIDENCIA")
                .returningResultSet("RCL_EVIDENCIA", new ReporteEvidenciaSupervisionSistRowMapper());

        jdbcReporteSupervisionSistemas2 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA").withCatalogName("PAREPSISTEMAS2").withProcedureName("SP_REPORTE_SISTEMAS2")
                .returningResultSet("RCL_LAM", new ReporteCecosSupervisionSist2RowMapper())
                .returningResultSet("RCL_PORCENTAJE", new ReporteCecosSupervisionSist2RowMapper());
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> reporteSistemas(int idCeco) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<ReporteSupervisionSistemasDTO> listaReporteTotales = null;
        List<ReporteSupervisionSistemasDTO> listaReporteSistemas = null;
        // int respuesta = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCECO", idCeco);
        out = jdbcReporteSupervisionSistemas.execute(in);
        logger.info("Funcion ejecutada:{checklist.PAREPSISTEMAS.SP_REPORTE_SISTEMAS}");
        listaReporteTotales = (List<ReporteSupervisionSistemasDTO>) out.get("RCL_TOTALES");
        listaReporteSistemas = (List<ReporteSupervisionSistemasDTO>) out.get("RCL_PORCENTAJE");
        lista = new HashMap<String, Object>();
        lista.put("listaReporteTotales", listaReporteTotales);
        lista.put("listaReporteSistemas", listaReporteSistemas);
        // BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");
        // respuesta = respuestaEjec.intValue();
        // if (respuesta == 1) {
        // throw new Exception("El Reporte de Sistemas retorno error");
        // }
        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReporteSupervisionSistemasDTO> reporteSisCap(String inicio, String fin, String checklist,
            String sucursal, String area) throws Exception {
        Map<String, Object> out = null;
        List<ReporteSupervisionSistemasDTO> listaReporteSisCap = null;
        // int respuesta = 0;
        logger.info("Antes de la prueba");
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FINICIO", inicio).addValue("PA_FFIN", fin)
                .addValue("PA_CHECK", checklist).addValue("PA_SUC", sucursal).addValue("PA_AREA", area);
        out = jdbcReporteSisCap.execute(in);
        logger.info("Funcion ejecutada:{checklist.PAREPSISTEMAS.SP_REPORTE_SISCAP}");
        listaReporteSisCap = (List<ReporteSupervisionSistemasDTO>) out.get("RCL_CECOS");
        // BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");
        // respuesta = respuestaEjec.intValue();
        // if (respuesta == 1) {
        // throw new Exception("El Reporte de Sistemas retorno error");
        // }
        return listaReporteSisCap;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReporteSupervisionSistemasDTO> filtroCheck(int idReporte) throws Exception {
        Map<String, Object> out = null;
        List<ReporteSupervisionSistemasDTO> listaChecklist = null;
        // int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_REPORTE", idReporte);
        out = jdbcReporteSupervisionFiltroCheck.execute(in);
        logger.info("Funcion ejecutada:{checklist.PAREPSISTEMAS2.SP_FILTRO_CHECK}");
        listaChecklist = (List<ReporteSupervisionSistemasDTO>) out.get("RCL_CHECK");
        // BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");
        // respuesta = respuestaEjec.intValue();
        // if (respuesta == 1) {
        // throw new Exception("El Reporte de Sistemas retorno error");
        // }
        return listaChecklist;

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReporteSupervisionSistemasDTO> filtroSucursal(String sucursal) throws Exception {
        Map<String, Object> out = null;
        List<ReporteSupervisionSistemasDTO> listaFiltroSucursal = null;
        // int respuesta = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_SUCURSAL", sucursal);
        out = jdbcReporteSupervisionFiltroSucursal.execute(in);
        logger.info("Funcion ejecutada:{checklist.PAREPSISTEMAS2.SP_FILTRO_SUCURSAL}");
        listaFiltroSucursal = (List<ReporteSupervisionSistemasDTO>) out.get("RCL_SUCURSAL");
        // BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");
        // respuesta = respuestaEjec.intValue();
        // if (respuesta == 1) {
        // throw new Exception("El Reporte de Sistemas retorno error");
        // }
        return listaFiltroSucursal;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReporteSupervisionSistemasDTO> filtroUsuario(String usuario) throws Exception {
        Map<String, Object> out = null;
        List<ReporteSupervisionSistemasDTO> listaFiltroSucursal = null;
        // int respuesta = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USUARIO", usuario);
        out = jdbcReporteSupervisionFiltroUsuario.execute(in);
        logger.info("Funcion ejecutada:{checklist.PAREPSISTEMAS2.SP_FILTRO_USUARIO}");
        listaFiltroSucursal = (List<ReporteSupervisionSistemasDTO>) out.get("RCL_USUARIO");
        // BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");
        // respuesta = respuestaEjec.intValue();
        // if (respuesta == 1) {
        // throw new Exception("El Reporte de Sistemas retorno error");
        // }
        return listaFiltroSucursal;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> obtenerRespuestas(int idBitacora) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<ReporteSupervisionSistemasDTO> listaEvaluacionFechas = null;
        List<ReporteSupervisionSistemasDTO> listaRespuestas = null;
        // int respuesta = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDBITACORA", idBitacora);
        out = jdbcReporteRespuestas.execute(in);
        logger.info("Funcion ejecutada:{checklist.PAREPSISTEMAS2.SP_RESPUESTAS}");
        listaEvaluacionFechas = (List<ReporteSupervisionSistemasDTO>) out.get("PA_EVAL_FECHA");
        listaRespuestas = (List<ReporteSupervisionSistemasDTO>) out.get("RCL_RESPUESTAS");
        lista = new HashMap<String, Object>();
        lista.put("listaEvaluacionFechas", listaEvaluacionFechas);
        lista.put("listaRespuestas", listaRespuestas);
        // BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");
        // respuesta = respuestaEjec.intValue();
        // if (respuesta == 1) {
        // throw new Exception("El Reporte de Sistemas retorno error");
        // }
        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReporteSupervisionSistemasDTO> obtenerEvidencia(int idBitacora) throws Exception {
        Map<String, Object> out = null;
        List<ReporteSupervisionSistemasDTO> listaFiltroSucursal = null;
        // int respuesta = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDBITACORA", idBitacora);
        out = jdbcReporteEvidencia.execute(in);
        logger.info("Funcion ejecutada:{checklist.PAREPSISTEMAS2.SP_EVIDENCIA}");
        listaFiltroSucursal = (List<ReporteSupervisionSistemasDTO>) out.get("RCL_EVIDENCIA");
        // BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");
        // respuesta = respuestaEjec.intValue();
        // if (respuesta == 1) {
        // throw new Exception("El Reporte de Sistemas retorno error");
        // }
        return listaFiltroSucursal;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> reporteSistemas2(int idCeco) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<ReporteSupervisionSistemasDTO> listaReporteLAM = null;
        List<ReporteSupervisionSistemasDTO> listaReporteSistemas = null;
        // int respuesta = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCECO", idCeco);
        out = jdbcReporteSupervisionSistemas2.execute(in);
        logger.info("Funcion ejecutada:{checklist.PAREPSISTEMAS.SP_REPORTE_SISTEMAS}");
        listaReporteLAM = (List<ReporteSupervisionSistemasDTO>) out.get("RCL_LAM");
        listaReporteSistemas = (List<ReporteSupervisionSistemasDTO>) out.get("RCL_PORCENTAJE");
        lista = new HashMap<String, Object>();
        lista.put("listaReporteLAM", listaReporteLAM);
        lista.put("listaReporteSistemas", listaReporteSistemas);
        // BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");
        // respuesta = respuestaEjec.intValue();
        // if (respuesta == 1) {
        // throw new Exception("El Reporte de Sistemas retorno error");
        // }
        return lista;
    }

}
