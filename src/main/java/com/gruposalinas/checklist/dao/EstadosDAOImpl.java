package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.EstadosDTO;
import com.gruposalinas.checklist.mappers.EstadosRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class EstadosDAOImpl extends DefaultDAO implements EstadosDAO {

    private static Logger logger = LogManager.getLogger(EstadosDAOImpl.class);

    DefaultJdbcCall jdbcInsertaEstados;
    DefaultJdbcCall jdbcEliminaEstados;
    DefaultJdbcCall jdbcObtieneEstados;
    DefaultJdbcCall jdbcObtieneEstado;
    DefaultJdbcCall jdbcObtieneDetalle;
    DefaultJdbcCall jbdcActualizaEstados;

    public void init() {

        jdbcInsertaEstados = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMESTADO")
                .withProcedureName("SP_INS_ESTADO");

        jdbcEliminaEstados = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMESTADO")
                .withProcedureName("SP_DEL_ESTADO");

        jdbcObtieneEstados = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMESTADO")
                .withProcedureName("SP_SEL_ESTADOS")
                .returningResultSet("RCL_ESTADO", new EstadosRowMapper());

        jdbcObtieneEstado = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMESTADO")
                .withProcedureName("SP_SEL_ESTADO")
                .returningResultSet("RCL_ESTADO", new EstadosRowMapper());

        jdbcObtieneDetalle = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMESTADO")
                .withProcedureName("SP_SEL_COMPLETO")
                .returningResultSet("RCL_ESTADO", new EstadosRowMapper());

        jbdcActualizaEstados = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMESTADO")
                .withProcedureName("SP_ACT_ESTADO");

    }

    public int inserta(EstadosDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEstado = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPAIS", bean.getIdPais())
                .addValue("PA_IDESTADO", bean.getIdEstado())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_ABREVIATURA", bean.getAbreviatura());

        out = jdbcInsertaEstados.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMESTADO.SP_INS_ESTADO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_CONSECUTIVO");
        idEstado = idTipoReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar el Estado");
        } else {
            return idEstado;
        }

        return idEstado;
    }

    public boolean elimina(String idEstado) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDESTADO", idEstado);

        out = jdbcEliminaEstados.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMESTADO.SP_DEL_ESTADO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al borrar el Estado(" + idEstado + ")");
        } else {
            return true;
        }

        return false;

    }

    //parametro
    @SuppressWarnings("unchecked")
    public List<EstadosDTO> obtieneDatos(int idEstado) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<EstadosDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDESTADO", idEstado);

        out = jdbcObtieneEstado.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMESTADO.SP_SEL_ESTADO}");
        //lleva el nombre del cursor del procedure
        lista = (List<EstadosDTO>) out.get("RCL_ESTADO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener el empleado(" + idEstado + ")");
        }
        return lista;

    }

    //sin parametro obtiene estados
    @SuppressWarnings("unchecked")
    public List<EstadosDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<EstadosDTO> listaEstados = null;
        int error = 0;

        out = jdbcObtieneEstados.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMESTADO.SP_SEL_ESTADOS}");

        listaEstados = (List<EstadosDTO>) out.get("RCL_ESTADO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener los Estados");
        } else {
            return listaEstados;
        }

        return listaEstados;

    }

    //Detalle Completo estados
    @SuppressWarnings("unchecked")
    public List<EstadosDTO> obtieneDetalle() throws Exception {
        Map<String, Object> out = null;
        List<EstadosDTO> listaDetalle = null;
        int error = 0;

        out = jdbcObtieneDetalle.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMESTADO.SP_SEL_COMPLETO}");

        listaDetalle = (List<EstadosDTO>) out.get("RCL_ESTADO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener los Estados");
        } else {
            return listaDetalle;
        }

        return listaDetalle;
    }

    public boolean actualiza(EstadosDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CONSECUTIVO", bean.getIdConsec())
                .addValue("PA_IDPAIS", bean.getIdPais())
                .addValue("PA_IDESTADO", bean.getIdEstado())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_ABREVIATURA", bean.getAbreviatura());

        out = jbdcActualizaEstados.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMESTADO.SP_ACT_ESTADO}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al actualizar Estado del id( " + bean.getIdEstado() + ")");
        } else {
            return true;
        }
        return false;

    }

}
