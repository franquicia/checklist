package com.gruposalinas.checklist.business;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.AsignacionPerfilesDAO;

public class PerfilesNuevoBI {
	
	private static Logger logger = LogManager.getLogger(PerfilesNuevoBI.class);

	@Autowired
	AsignacionPerfilesDAO asignacionPerfilesDAO;
	
	
	public boolean asignacionAutomaticaPerfiles() {
		boolean actualizacionGestion=false;
		boolean asignacion=false;
		try {
			actualizacionGestion=asignacionPerfilesDAO.actualizarPerfilesGestion();
			if(actualizacionGestion) {
				asignacion=asignacionPerfilesDAO.asignacionPerfiles();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			actualizacionGestion=false;
			asignacion=false;
		}
		
		return asignacion;
		
	}
	public boolean actualizacionPerfilesGestion() {
		boolean actualizacionGestion=false;
		try {
			actualizacionGestion=asignacionPerfilesDAO.actualizarPerfilesGestion();
			
			
		} catch (Exception e) {
			e.printStackTrace();

			actualizacionGestion=false;
		}
		
		return actualizacionGestion;
		
	}
	
	public boolean asignacionPerfiles() {
		boolean asignacion=false;
		try {
			asignacion=asignacionPerfilesDAO.asignacionPerfiles();

		} catch (Exception e) {
			e.printStackTrace();
			asignacion=false;
		}
		
		return asignacion;
	
		
	}
	
	public boolean altaPerfilSistemas() {
		boolean asignacion=false;
		try {
			asignacion=asignacionPerfilesDAO.altaPerfilesSistemas();

		} catch (Exception e) {
			e.printStackTrace();
			asignacion=false;
		}
		
		return asignacion;
	
		
	}
	
	public boolean altaPerfilSCouting() {
		boolean asignacion=false;
		try {
			asignacion=asignacionPerfilesDAO.altaPerfilesScouting();

		} catch (Exception e) {
			e.printStackTrace();
			asignacion=false;
		}
		
		return asignacion;
	
		
	}
	
	public boolean altaPerfilPrestaPrenda() {
		boolean asignacion=false;
		try {
			asignacion=asignacionPerfilesDAO.altaPerfilesPrestaPrenda();

		} catch (Exception e) {
			e.printStackTrace();
			asignacion=false;
		}
		
		return asignacion;
	
		
	}
	public boolean altaPerfilDescargaGestion() {
		boolean asignacion=false;
		try {
			asignacion=asignacionPerfilesDAO.altaPerfilesDescargaGestion();

		} catch (Exception e) {
			e.printStackTrace();
			asignacion=false;
		}
		
		return asignacion;
	
		
	}
}
