package com.gruposalinas.checklist.domain;

public class CheckInAsistenciaDTO {
	


		private int idAsistencia;
		private int idUsuario;
		private double latitud;
		private double longitud;
		private String fecha;
		private int idTipoProyecto;
		private String ruta;
		private int idDescuento;
		private String lugar;
		private int tipoCheckIn;
		private int idPerfil;
		private String periodo;
		private String observaciones;
		private String tipoAsistencia;
		
		public int getIdAsistencia() {
			return idAsistencia;
		}
		public void setIdAsistencia(int idAsistencia) {
			this.idAsistencia = idAsistencia;
		}
		public int getIdUsuario() {
			return idUsuario;
		}
		public void setIdUsuario(int idUsuario) {
			this.idUsuario = idUsuario;
		}
		public double getLatitud() {
			return latitud;
		}
		public void setLatitud(double latitud) {
			this.latitud = latitud;
		}
		public double getLongitud() {
			return longitud;
		}
		public void setLongitud(double longitud) {
			this.longitud = longitud;
		}
		public String getFecha() {
			return fecha;
		}
		public void setFecha(String fecha) {
			this.fecha = fecha;
		}
		public int getIdTipoProyecto() {
			return idTipoProyecto;
		}
		public void setIdTipoProyecto(int idTipoProyecto) {
			this.idTipoProyecto = idTipoProyecto;
		}
		public String getRuta() {
			return ruta;
		}
		public void setRuta(String ruta) {
			this.ruta = ruta;
		}
		public int getIdDescuento() {
			return idDescuento;
		}
		public void setIdDescuento(int idDescuento) {
			this.idDescuento = idDescuento;
		}
		public String getLugar() {
			return lugar;
		}
		public void setLugar(String lugar) {
			this.lugar = lugar;
		}
		public int getTipoCheckIn() {
			return tipoCheckIn;
		}
		public void setTipoCheckIn(int tipoCheckIn) {
			this.tipoCheckIn = tipoCheckIn;
		}
		public int getIdPerfil() {
			return idPerfil;
		}
		public void setIdPerfil(int idPerfil) {
			this.idPerfil = idPerfil;
		}
		public String getPeriodo() {
			return periodo;
		}
		public void setPeriodo(String periodo) {
			this.periodo = periodo;
		}
		public String getObservaciones() {
			return observaciones;
		}
		public void setObservaciones(String observaciones) {
			this.observaciones = observaciones;
		}
		
		public String getTipoAsistencia() {
			return tipoAsistencia;
		}
		public void setTipoAsistencia(String tipoAsistencia) {
			this.tipoAsistencia = tipoAsistencia;
		}
		@Override
		public String toString() {
			return "CheckInAsistenciaDTO [idAsistencia=" + idAsistencia + ", idUsuario=" + idUsuario + ", latitud="
					+ latitud + ", longitud=" + longitud + ", fecha=" + fecha + ", idTipoProyecto=" + idTipoProyecto
					+ ", ruta=" + ruta + ", idDescuento=" + idDescuento + ", lugar=" + lugar + ", tipoCheckIn="
					+ tipoCheckIn + ", idPerfil=" + idPerfil + ", periodo=" + periodo + ", observaciones="
					+ observaciones + ", tipoAsistencia=" + tipoAsistencia + "]";
		}

		
	}