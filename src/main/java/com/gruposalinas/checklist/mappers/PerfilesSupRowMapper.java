package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.PerfilesSupDTO;

public class PerfilesSupRowMapper implements RowMapper<PerfilesSupDTO> {

	public PerfilesSupDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		PerfilesSupDTO perfilDTO = new PerfilesSupDTO();
		
		perfilDTO.setIdPerfil(rs.getInt("FIPERFIL_ID"));
		perfilDTO.setDescPerfil(rs.getString("FCDESCRIPCION"));
		perfilDTO.setActivo(rs.getString("FIACTIVO"));
		perfilDTO.setUsuaModificado(rs.getString("FCUSUARIO_MOD"));
		perfilDTO.setFechaModificacion(rs.getString("FDFECHA_MOD"));
		
		return perfilDTO;
		
		
	}

}
