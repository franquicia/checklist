package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.PosiblesDTO;


public interface PosiblesAdmDAO {

	public List<PosiblesDTO> buscaPosible() throws Exception;
	
	public int insertaPosible(String  posible) throws Exception;
	
	public boolean actualizaPosible(PosiblesDTO bean) throws Exception;
	
	public boolean eliminaPosible(int idPosible) throws Exception;
        
        List<PosiblesDTO> buscaPosible(int idPosible) throws Exception ;

}
