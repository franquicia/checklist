package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.CheckInAsistenciaDTO;

public class CheckInAsistenciaRowMapper implements RowMapper<CheckInAsistenciaDTO> {

	public CheckInAsistenciaDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		CheckInAsistenciaDTO CheckInAsistenciaDTO = new CheckInAsistenciaDTO();
		
		CheckInAsistenciaDTO.setIdAsistencia(rs.getInt("FIID_CHECKASI"));
	    CheckInAsistenciaDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
	    CheckInAsistenciaDTO.setLatitud(rs.getDouble("FIID_LATITUD"));
	    CheckInAsistenciaDTO.setLongitud(rs.getDouble("FIID_LONGITUD"));
	    CheckInAsistenciaDTO.setFecha(rs.getString("FDFECHA"));
	    CheckInAsistenciaDTO.setObservaciones(rs.getString("FCOBSERVACIONES"));
	    CheckInAsistenciaDTO.setIdDescuento(rs.getInt("FCDESCUENTO"));
	    CheckInAsistenciaDTO.setRuta(rs.getString("FCRUTA"));
	    CheckInAsistenciaDTO.setLugar(rs.getString("FCLUGARASIST"));
	    CheckInAsistenciaDTO.setIdTipoProyecto(rs.getInt("FIID_TIPOPROY"));
	    CheckInAsistenciaDTO.setTipoCheckIn(rs.getInt("FIID_TIPOASIST"));
	    CheckInAsistenciaDTO.setTipoAsistencia(rs.getString("TIPOCHECKIN"));
	    
	    CheckInAsistenciaDTO.setPeriodo(rs.getString("FCPERIODO"));
	    
	   
	   
		return CheckInAsistenciaDTO;
	}

}


