<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<tiles:importAttribute name="javascripts" />
<tiles:importAttribute name="stylesheets" ignore="true" />
<head>
<title><tiles:insertAttribute name="title" ignore="true" /></title>

</head>

<!--Se define la estructura de este template, el cual contendra un encabezado y un body. Su contenido se define en el tilesDef.xml -->
<header>
	<div class="checklists">
		<tiles:insertAttribute name="header" />
	</div>
</header>
<body>
	<div id="cuerpo" style="height: 94%; background:#F2F2F2;">
		<tiles:insertAttribute name="body" />
	</div>
</body>
</html>
