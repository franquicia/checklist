package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.BitacoraDAO;
import com.gruposalinas.checklist.domain.BitacoraDTO;


public class BitacoraBI {
	
	private static Logger logger = LogManager.getLogger(BitacoraBI.class);
	
	@Autowired
	BitacoraDAO bitacoraDAO;
	
	List<BitacoraDTO> listaBitacora = null;
	List<BitacoraDTO> listaBitacoraAll = null ;

	public int insertaBitacora(BitacoraDTO bean){
		
		int idBitacora = 0 ;
		
		try{
			
			idBitacora=bitacoraDAO.insertaBitacora(bean);
			
		}catch(Exception e){
			logger.info("No fue posible insertar la Bitacora "+ e);
		}
		
		return idBitacora;
				
	}
	
	public boolean actualizaBitacora(BitacoraDTO bean){
		
		boolean respuesta = false;
		
		try{
			respuesta = bitacoraDAO.actualizaBitacora(bean);
		}catch(Exception e){
			logger.info("No fue posible actualizar la Bitacora "+e);
		}
		
		return respuesta;
		
	}
	
	public boolean eliminaBitacora(int idBitacora) {
	
		boolean respuesta = false;
		
		try{
			respuesta = bitacoraDAO.eliminaBitacora(idBitacora);
		}catch(Exception e){
			logger.info("No fue posible eliminar la Bitacora "+e);
		}
		
		return respuesta;
		
	}
	
	public List<BitacoraDTO> buscaBitacora(){
		
		try {
			listaBitacoraAll = bitacoraDAO.buscaBitacora();
		} catch (Exception e) {
			logger.info("No fue posible consultar la Bitacora "+ e);
		}
		
		return listaBitacoraAll;
		
	}
	
	public List<BitacoraDTO> buscaBitacora(String checkUsua, String idBitacora, String fechaI, String fechaF){
		
		try {
			listaBitacora = bitacoraDAO.buscaBitacora(checkUsua, idBitacora, fechaI, fechaF);
		} catch (Exception e) {
			logger.info("No fue posible consultar la Bitacora "+ e);
		}
		
		return listaBitacora;
		
	}
	
	public List<BitacoraDTO> buscaBitacoraCerradas(String idChecklist) {
		
		try {
			listaBitacora = bitacoraDAO.buscaBitacoraCerradas(idChecklist);
		} catch (Exception e) {
			logger.info("No fue posible consultar la Bitacora "+ e);
		}
		
		return listaBitacora;
		
	}
	public List<BitacoraDTO> buscaBitacoraCerradasR(String idChecklist) {
		
		try {
			listaBitacora = bitacoraDAO.buscaBitacoraCerradasR(idChecklist);
		} catch (Exception e) {
			logger.info("No fue posible consultar la Bitacora "+ e);
		}
		
		return listaBitacora;
		
	}
	
	public int verificaBitacora(int checkUsua){
		
		int idBitacora = 0;
					
		try {
			idBitacora = bitacoraDAO.verificaIdBitacora(checkUsua);
		} catch (Exception e) {
			logger.info("No fue posible obtener el id de la Bitacora " + e);
		}
		
		return idBitacora;
		
	}
	
	public boolean cierraBitacora(){
		
		boolean respuesta = false;
		
		try{
			respuesta = bitacoraDAO.cierraBitacora();
		}catch(Exception e){
			logger.info("No fue posible terminar las Bitacoras "+ e);
		}
		
		return respuesta;
		
	}
	
}
