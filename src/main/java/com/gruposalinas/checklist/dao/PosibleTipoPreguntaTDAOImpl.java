package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.PosiblesTipoPreguntaDTO;
import com.gruposalinas.checklist.mappers.PosibleTipoPregTRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PosibleTipoPreguntaTDAOImpl extends DefaultDAO implements PosibleTipoPreguntaTDAO {

    private Logger logger = LogManager.getLogger(PosibleTipoPreguntaTDAOImpl.class);

    private DefaultJdbcCall jdbcObtienePosibleTipoPregTemp;
    private DefaultJdbcCall jdbcInsertaPosibleTipoPregTemp;
    private DefaultJdbcCall jdbcActualizaPosibleTipoPregTemp;
    private DefaultJdbcCall jdbcEliminaPosibleTipoPregTemp;

    public void init() {

        jdbcObtienePosibleTipoPregTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPOSPREGTEMP")
                .withProcedureName("SP_SEL_POSTPREG_TEMP")
                .returningResultSet("RCL_POSIBLES", new PosibleTipoPregTRowMapper());

        jdbcInsertaPosibleTipoPregTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPOSPREGTEMP")
                .withProcedureName("SP_INSPOSPREG_TEMP");

        jdbcActualizaPosibleTipoPregTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPOSPREGTEMP")
                .withProcedureName("SP_ACTPOSPREG_TEMP");

        jdbcEliminaPosibleTipoPregTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPOSPREGTEMP")
                .withProcedureName("SP_DELPOSPREG_TEMP");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PosiblesTipoPreguntaDTO> obtienePosiblesRespuestasTemp(String idTipoPregunta) throws Exception {
        Map<String, Object> out = null;
        List<PosiblesTipoPreguntaDTO> lista = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDTIPO", idTipoPregunta);

        out = jdbcObtienePosibleTipoPregTemp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPOSPREGTEMP.SP_SEL_POSTPREG_TEMP}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        lista = (List<PosiblesTipoPreguntaDTO>) out.get("RCL_POSIBLES");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al consultar posibles respuestas de la tabla temporal");
        } else {
            return lista;
        }

        return null;
    }

    @Override
    public boolean insertaPosibleTipoPregunta(PosiblesTipoPreguntaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPOSIBLE", bean.getIdPosibleRespuesta())
                .addValue("PA_TIPPREG", bean.getIdTipoPregunta())
                .addValue("PA_NUM_REV", bean.getNumeroRevision())
                .addValue("PA_TIPO_MOD", bean.getTipoCambio());

        out = jdbcInsertaPosibleTipoPregTemp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPOSPREGTEMP.SP_INSPOSPREG_TEMP}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al insertar posibles respuestas por tipo de Pregunta de la tabla temporal");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaPosibleTipoPregunta(PosiblesTipoPreguntaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPOSPREG", bean.getIdPosibleTipoPregunta())
                .addValue("PA_IDPOSIBLE", bean.getIdPosibleRespuesta())
                .addValue("PA_TIPPREG", bean.getIdTipoPregunta())
                .addValue("PA_NUM_REV", bean.getNumeroRevision())
                .addValue("PA_TIPO_MOD", bean.getTipoCambio());

        out = jdbcActualizaPosibleTipoPregTemp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPOSPREGTEMP.SP_ACTPOSPREG_TEMP}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al actualizar posible tipo de Pregunta de la tabla temporal");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaPosibleTipoPregunta(int idPosibleTipoPregunta) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPOSPREG", idPosibleTipoPregunta);

        out = jdbcEliminaPosibleTipoPregTemp.execute(in);

        logger.info("Funcion ejecutada: {checklist.PAADMPOSPREGTEMP.SP_DELPOSPREG_TEMP}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al eliminar posibles tipo de Pregunta de la tabla temporal");
        } else {
            return true;
        }

        return false;
    }

}
