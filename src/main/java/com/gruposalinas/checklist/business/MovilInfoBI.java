package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.MovilInfoDAO;
import com.gruposalinas.checklist.domain.MovilInfoDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class MovilInfoBI {

	private static Logger logger = LogManager.getLogger(MovilInfoBI.class);
	
	@Autowired
	MovilInfoDAO movilInfoDAO;
	
	List<MovilInfoDTO> listaInfo = null;
	
	public List<MovilInfoDTO> obtieneInfoMovil(MovilInfoDTO bean){
		try{
			listaInfo = movilInfoDAO.obtieneInfoMovil(bean);
		}
		catch(Exception e)
		{
			logger.info("No fue posible obtener datos");
		}
		return listaInfo;
	}
	
public boolean insertaInfoMovil (MovilInfoDTO bean){
		
		boolean idMovil = false;
		try{
			idMovil = movilInfoDAO.insertaInfoMovil(bean);
		}
		catch(Exception e)
		{
			logger.info("No fue posible insertar la información del dispositivo");
		}
		return idMovil;
	}
	
	public boolean actualizaInfoMovil(MovilInfoDTO bean){
		
		boolean respuesta = false;
		
		try{
			respuesta = movilInfoDAO.actualizaInfoMovil(bean);
		}
		catch(Exception e){
			logger.info("No fue posible actualizar la tabla");
		}
		return respuesta;
	}
	
	public boolean eliminaInfoMovil(int idMovil)
	{
		boolean respuesta =false;
		
		try{
			respuesta = movilInfoDAO.eliminaInfoMovil(idMovil);
		}
		catch(Exception e){
			logger.info("No fue posible borrar la información del dispositivo");
		}
		return respuesta;
	}
}
