package com.gruposalinas.checklist.mappers;

import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.CecoDTO;

public class CecoPasoRowMapper implements RowMapper<CecoDTO>{

	public CecoDTO mapRow(java.sql.ResultSet rs, int rowNum) throws SQLException {
		
		CecoDTO cecoDTO = new CecoDTO();
		
		cecoDTO.setIdCeco(rs.getString("FCCCID"));
		cecoDTO.setEntidadid(rs.getInt("FIENTIDADID"));
		cecoDTO.setNumEconomico(rs.getInt("FINUM_ECONOMICO"));
		cecoDTO.setDescCeco(rs.getString("FCNOMBRECC"));
		cecoDTO.setNombreEnt(rs.getString("FCNOMBRE_ENT"));
		cecoDTO.setIdCCSuperior(rs.getString("FCCCID_PADRE"));
		cecoDTO.setEntidadPadre(rs.getInt("FIENTDID_PADRE"));
		cecoDTO.setNombrePadre(rs.getString("FCNOMBRECC_PAD"));
		cecoDTO.setTipoCanal(rs.getString("FCTIPOCANAL"));
		cecoDTO.setNomTipoCanal(rs.getString("FCNOMBTIPCANAL"));
		cecoDTO.setDesCanal(rs.getString("FCCANAL"));
		cecoDTO.setActivo(rs.getInt("FISTATUSCCID"));
		cecoDTO.setEstatusCC(rs.getString("FCSTATUSCC"));
		cecoDTO.setTipoCC(rs.getInt("FITIPOCC"));
		cecoDTO.setIdEstado(rs.getInt("FIESTADOID"));
		cecoDTO.setCiudad(rs.getString("FCMUNICIPIO"));
		cecoDTO.setTipoOperacion(rs.getString("FCTIPOOPERACION"));
		cecoDTO.setTipoSucursal(rs.getString("FCTIPOSUCURSAL"));
		cecoDTO.setNomTipoSucursal(rs.getString("FCNOMBRETIPOSUC"));
		cecoDTO.setCalle(rs.getString("FCCALLE"));
		cecoDTO.setIdResponsable(rs.getInt("FIRESPONSABLEID"));
		cecoDTO.setNombreContacto(rs.getString("FCRESPONSABLE"));
		cecoDTO.setCp(rs.getString("FCCP"));
		cecoDTO.setTelefonoContacto(rs.getString("FCTELEFONOS"));
		cecoDTO.setIdSie(rs.getInt("FICLASIF_SIEID"));
		cecoDTO.setClasfSie(rs.getString("FCCLASIF_SIE"));
		cecoDTO.setGastoNegocio(rs.getInt("FIGASTOXNEGID"));
		cecoDTO.setGastoxNegocio(rs.getString("FCGASTOXNEGOCIO"));
		cecoDTO.setFechaApertura(rs.getString("FDAPERTURA"));
		cecoDTO.setFechaCierre(rs.getString("FDCIERRE"));
		cecoDTO.setIdPais(rs.getInt("FIPAISID"));
		cecoDTO.setDesPais(rs.getString("FCPAIS"));
		cecoDTO.setFechaCarga(rs.getString("FDCARGA"));

		return cecoDTO;
		
	}

}
