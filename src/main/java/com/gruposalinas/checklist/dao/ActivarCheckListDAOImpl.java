package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ActivarCheckListDTO;
import com.gruposalinas.checklist.mappers.ActivarCheckListRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ActivarCheckListDAOImpl extends DefaultDAO implements ActivarCheckListDAO {

    private Logger logger = LogManager.getLogger(ActivarCheckListDAOImpl.class);

    private DefaultJdbcCall jdbcActualizaAsignacion;

    public void init() {

        jdbcActualizaAsignacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAACTCHECKLST")
                .withProcedureName("SP_ACTIVO")
                .returningResultSet("PA_CONSULTA", new ActivarCheckListRowMapper());

    }

    @SuppressWarnings("unused")
    @Override
    public boolean actualizaCheckList(ActivarCheckListDTO bean) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        /*String idChecklist = null;
		if (bean.getIdChecklist() != 0) {
			idChecklist = "" + bean.getIdChecklist();
		}
         */
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_USUARIO", bean.getIdUsuario())
                .addValue("PA_FCID_CECO", bean.getIdCeco())
                .addValue("PA_CHECKLIST", bean.getIdChecklist())
                .addValue("PA_ACTUALIZACION", bean.getActivo());

        out = jdbcActualizaAsignacion.execute(in);

        //logger.info("Funcion ejecutada:{FRANQUICIA.PA_ACTIVAR_CHECKLIST.SP_ACTIVO}");
        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = resultado.intValue();

        if (ejecucion == 1) {
            logger.info("Algo ocurrió al actualizar el estatus del checklist" + bean.getIdChecklist());
            return false;
        }

        return true;
    }

}
