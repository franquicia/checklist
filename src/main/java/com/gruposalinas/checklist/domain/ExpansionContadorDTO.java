package com.gruposalinas.checklist.domain;

import java.util.ArrayList;
import java.util.List;

public class ExpansionContadorDTO {

	private List<ConteoGenExpDTO> listaPreg;
	private List<ConteoGenExpDTO> listaRespuestas;
	private List<ConteoGenExpDTO> listaChecks;
	

	private String clasif;
	private int bitacora;
	// Por clasificacion
	// ===========================================================================

	private List<ConteoGenExpDTO> listaPregHallazgosClasif;
	private List<ConteoGenExpDTO> listaPregImperdonablesClasif;
	private List<ConteoGenExpDTO> listaPregAdicionalesClasif;

	private List<ConteoGenExpDTO> listaChecksDesHabilitados;
	
	private int countItemClasifNo; // Items Por Atender
	private int countItemClasifSi; // Items Aprobados
	private int countItemClasifNA; // Items No Considerados
	private int countItemClasifRe; // Items Revisados Si+No
	private int countItemClasifTo; // Items totales Si+No+NA
	private int countItemClasifImp; // Items Imperdonables

	// Porcentajes por items revisados (Se toman encuenta solo items Si y No)
	private double porcentajeClasifReSi; // porcentajeReSi= countItemSi*100/countItemClasifRe
	private double porcentajeClasifReNo; // porcentajeReNo= countItemNo*100/countItemClasifRe

	// Porcentajes por items totales(se toma encuenta NA)
	private double porcentajeClasifToSi; // porcentajeToSi= countItemSi*100/countItemTo
	private double porcentajeClasifToNo; // porcentajeToNo= countItemNo*100/countItemTo
	private double porcentajeClasifToNA; // porcentajeToNA= countItemNA*100/countItemTo

	private double pondClasifNa; // ponderacion calculada para restarla del NA
	private double ponderacionClasifMaximaReal; // es el valor maximo posible para su 100%
	private double ponderacionClasifMaximaCalculada; // es el valor maximo posible para su 100% restando ponderacionClasifMaximaReal-pondClasifNa
	private double ponderacionClasifObtenidaReal; // es el valor obtenido de su calculo
	private double ponderacionClasifObtenidaCalculada; // es el valor obtenido de su calculo restando ponderacionClasifObtenidaReal-pondClasifNa
	

	private double calificacionClasifReal; // es el valor calulado entre (ponderacionClasifObtenidaReal)*100/ponderacionClasifMaximaReal
	private double calificacionClasifCalculada; // es el valor calulado entre (ponderacionClasifObtenidaCalculada)*100/ponderacionClasifMaximaCalculada

	// Por clasificacion
	// ===========================================================================
	private String check[];
	private int idCheck[];
	
	private List<List<ConteoGenExpDTO>>[] listaPregHallazgosCheck;
	private List<List<ConteoGenExpDTO>>[] listaPregImperdonablesCheck;
	private List<List<ConteoGenExpDTO>>[] listaPregAdicionalesCheck;
	
	private int[] countItemCheckfNo; // Items Por Atender
	private int[] countItemCheckfSi; // Items Aprobados
	private int[] countItemCheckNA; // Items No Considerados
	private int[] countItemCheckRe; // Items Revisados Si+No
	private int[] countItemCheckTo; // Items totales Si+No+NA
	private int[] countItemCheckImp; // Items Imperdonables

	// Porcentajes por items revisados (Se toman encuenta solo items Si y No)
	private double[] porcentajeCheckReSi; // porcentajeReSi= countItemSi*100/countItemRe
	private double[] porcentajeCheckReNo; // porcentajeReNo= countItemNo*100/countItemRe

	// Porcentajes por items totales(se toma encuenta NA)
	private double[] porcentajeCheckToSi; // porcentajeToSi= countItemSi*100/countItemTo
	private double[] porcentajeCheckToNo; // porcentajeToNo= countItemNo*100/countItemTo
	private double[] porcentajeCheckToNA; // porcentajeToNA= countItemNA*100/countItemTo

	private double[] pondCheckNa; // ponderacion calculada para restarla del NA
	private double[] ponderacionCheckMaximaReal; // es el valor maximo posible para su 100%
	private double[] ponderacionCheckMaximaCalculada; // es el valor maximo posible para su 100% (ponderacionCheckMaximaReal- pondCheckNa)
	private double[] ponderacionCheckObtenidaReal; // es el valor obtenido de su calculo
	private double[] ponderacionCheckObtenidaCalculada; // es el valor obtenido de su calculo restando (ponderacionCheckObtenidaReal-pondCheckNa)
	
	private double[] calificacionCheckReal; // es el valor calulado entre (ponderacionCheckObtenidaReal)*100/ponderacionCheckMaximaReal
	private double[] calificacionCheckCalculada; // es el valor calulado entre (ponderacionCheckObtenidaCalculada)*100/ponderacionCheckMaximaCalculada

	public ExpansionContadorDTO(String clasif) {
		listaChecks = new ArrayList<>();
		listaPreg = new ArrayList<>();
		listaRespuestas = new ArrayList<>();
		
		listaChecksDesHabilitados = new ArrayList<>();
		
		listaPregHallazgosClasif = new ArrayList<>();
		listaPregImperdonablesClasif = new ArrayList<>();
		listaPregAdicionalesClasif = new ArrayList<>();
		this.clasif = clasif;

		countItemClasifNo = 0;
		countItemClasifSi = 0;
		countItemClasifNA = 0;
		countItemClasifRe = 0;
		countItemClasifTo = 0;
		countItemClasifImp = 0;

		porcentajeClasifReSi = 0;
		porcentajeClasifReNo = 0;
		porcentajeClasifToSi = 0;
		porcentajeClasifToNo = 0;
		porcentajeClasifToNA = 0;

		pondClasifNa = 0;
		ponderacionClasifMaximaReal = 0;
		ponderacionClasifMaximaCalculada = 0;
		ponderacionClasifObtenidaReal = 0;
		ponderacionClasifObtenidaCalculada = 0;
		
		calificacionClasifReal = 0;
		calificacionClasifCalculada = 0;
		

	}

	public void crearArrayParametros() {
		int size = listaChecks.size();
		if (size != 0) {
			listaPregHallazgosCheck = new ArrayList [size];
			listaPregImperdonablesCheck = new ArrayList [size];
			listaPregAdicionalesCheck = new ArrayList [size];
			check = new String[size];
			idCheck = new int[size];
			
			
			countItemCheckfNo = new int[size];
			countItemCheckfSi = new int[size];
			countItemCheckNA = new int[size];
			countItemCheckRe = new int[size];
			countItemCheckTo = new int[size];
			countItemCheckImp = new int[size];

			porcentajeCheckReSi = new double[size];
			porcentajeCheckReNo = new double[size];

			porcentajeCheckToSi = new double[size];
			porcentajeCheckToNo = new double[size];
			porcentajeCheckToNA = new double[size];

			pondCheckNa = new double[size];
			ponderacionCheckMaximaReal = new double[size];
			ponderacionCheckMaximaCalculada = new double[size];
			
			ponderacionCheckObtenidaReal = new double[size];
			ponderacionCheckObtenidaCalculada = new double[size];

			calificacionCheckReal = new double[size];
			calificacionCheckCalculada = new double[size];
		} else {
			listaPregHallazgosCheck = new ArrayList [37];
			listaPregImperdonablesCheck = new ArrayList [37];
			listaPregAdicionalesCheck = new ArrayList [37];
			check = new String[37];
			idCheck = new int[37];
			
			countItemCheckfNo = new int[37];
			countItemCheckfSi = new int[37];
			countItemCheckNA = new int[37];
			countItemCheckRe = new int[37];
			countItemCheckTo = new int[37];
			countItemCheckImp = new int[37];

			porcentajeCheckReSi = new double[37];
			porcentajeCheckReNo = new double[37];

			porcentajeCheckToSi = new double[37];
			porcentajeCheckToNo = new double[37];
			porcentajeCheckToNA = new double[37];

			pondCheckNa = new double[37];
			ponderacionCheckMaximaReal = new double[37];
			ponderacionCheckMaximaCalculada = new double[37];
			
			ponderacionCheckObtenidaReal = new double[37];
			ponderacionCheckObtenidaCalculada = new double[37];

			calificacionCheckReal = new double[37];
			calificacionCheckCalculada = new double[37];
			
		}
	}

	
	public List<ConteoGenExpDTO> getListaChecksDesHabilitados() {
		return listaChecksDesHabilitados;
	}

	public void setListaChecksDesHabilitados(List<ConteoGenExpDTO> listaChecksDesHabilitados) {
		this.listaChecksDesHabilitados = listaChecksDesHabilitados;
	}

	public List<ConteoGenExpDTO> getListaPreg() {
		return listaPreg;
	}

	public void setListaPreg(List<ConteoGenExpDTO> listaPreg) {
		this.listaPreg = listaPreg;
	}

	public List<ConteoGenExpDTO> getListaRespuestas() {
		return listaRespuestas;
	}

	public void setListaRespuestas(List<ConteoGenExpDTO> listaRespuestas) {
		this.listaRespuestas = listaRespuestas;
	}

	public List<ConteoGenExpDTO> getListaChecks() {
		return listaChecks;
	}

	public List<ConteoGenExpDTO> getListaPregImperdonablesClasif() {
		return listaPregImperdonablesClasif;
	}

	public void setListaPregImperdonablesClasif(List<ConteoGenExpDTO> listaPregImperdonablesClasif) {
		this.listaPregImperdonablesClasif = listaPregImperdonablesClasif;
	}

	public List<ConteoGenExpDTO> getListaPregAdicionalesClasif() {
		return listaPregAdicionalesClasif;
	}

	public void setListaPregAdicionalesClasif(List<ConteoGenExpDTO> listaPregAdicionalesClasif) {
		this.listaPregAdicionalesClasif = listaPregAdicionalesClasif;
	}

	public List<List<ConteoGenExpDTO>>[] getListaPregImperdonablesCheck() {
		return listaPregImperdonablesCheck;
	}

	public void setListaPregImperdonablesCheck(List<List<ConteoGenExpDTO>>[] listaPregImperdonablesCheck) {
		this.listaPregImperdonablesCheck = listaPregImperdonablesCheck;
	}

	public List<List<ConteoGenExpDTO>>[] getListaPregAdicionalesCheck() {
		return listaPregAdicionalesCheck;
	}

	public void setListaPregAdicionalesCheck(List<List<ConteoGenExpDTO>>[] listaPregAdicionalesCheck) {
		this.listaPregAdicionalesCheck = listaPregAdicionalesCheck;
	}

	public void setListaChecks(List<ConteoGenExpDTO> listaChecks) {
		this.listaChecks = listaChecks;
	}

	public List<ConteoGenExpDTO> getListaPregHallazgosClasif() {
		return listaPregHallazgosClasif;
	}

	public void setListaPregHallazgosClasif(List<ConteoGenExpDTO> listaPregHallazgosClasif) {
		this.listaPregHallazgosClasif = listaPregHallazgosClasif;
	}

	public List<List<ConteoGenExpDTO>>[] getListaPregHallazgosCheck() {
		return listaPregHallazgosCheck;
	}

	public void setListaPregHallazgosCheck(List<List<ConteoGenExpDTO>>[] listaPregHallazgosCheck) {
		this.listaPregHallazgosCheck = listaPregHallazgosCheck;
	}

	public String getClasif() {
		return clasif;
	}

	public void setClasif(String clasif) {
		this.clasif = clasif;
	}

	public int getBitacora() {
		return bitacora;
	}

	public void setBitacora(int bitacora) {
		this.bitacora = bitacora;
	}

	public int getCountItemClasifNo() {
		return countItemClasifNo;
	}

	public void setCountItemClasifNo(int countItemClasifNo) {
		this.countItemClasifNo = countItemClasifNo;
	}

	public int getCountItemClasifSi() {
		return countItemClasifSi;
	}

	public void setCountItemClasifSi(int countItemClasifSi) {
		this.countItemClasifSi = countItemClasifSi;
	}

	public int getCountItemClasifNA() {
		return countItemClasifNA;
	}

	public void setCountItemClasifNA(int countItemClasifNA) {
		this.countItemClasifNA = countItemClasifNA;
	}

	public int getCountItemClasifRe() {
		return countItemClasifRe;
	}

	public void setCountItemClasifRe(int countItemClasifRe) {
		this.countItemClasifRe = countItemClasifRe;
	}

	public int getCountItemClasifTo() {
		return countItemClasifTo;
	}

	public void setCountItemClasifTo(int countItemClasifTo) {
		this.countItemClasifTo = countItemClasifTo;
	}

	public int getCountItemClasifImp() {
		return countItemClasifImp;
	}

	public void setCountItemClasifImp(int countItemClasifImp) {
		this.countItemClasifImp = countItemClasifImp;
	}

	public double getPorcentajeClasifReSi() {
		return porcentajeClasifReSi;
	}

	public void setPorcentajeClasifReSi(double porcentajeClasifReSi) {
		this.porcentajeClasifReSi = porcentajeClasifReSi;
	}

	public double getPorcentajeClasifReNo() {
		return porcentajeClasifReNo;
	}

	public void setPorcentajeClasifReNo(double porcentajeClasifReNo) {
		this.porcentajeClasifReNo = porcentajeClasifReNo;
	}

	public double getPorcentajeClasifToSi() {
		return porcentajeClasifToSi;
	}

	public void setPorcentajeClasifToSi(double porcentajeClasifToSi) {
		this.porcentajeClasifToSi = porcentajeClasifToSi;
	}

	public double getPorcentajeClasifToNo() {
		return porcentajeClasifToNo;
	}

	public void setPorcentajeClasifToNo(double porcentajeClasifToNo) {
		this.porcentajeClasifToNo = porcentajeClasifToNo;
	}

	public double getPorcentajeClasifToNA() {
		return porcentajeClasifToNA;
	}

	public void setPorcentajeClasifToNA(double porcentajeClasifToNA) {
		this.porcentajeClasifToNA = porcentajeClasifToNA;
	}

	public double getPondClasifNa() {
		return pondClasifNa;
	}

	public void setPondClasifNa(double pondClasifNa) {
		this.pondClasifNa = pondClasifNa;
	}

	public double getPonderacionClasifMaximaReal() {
		return ponderacionClasifMaximaReal;
	}

	public void setPonderacionClasifMaximaReal(double ponderacionClasifMaximaReal) {
		this.ponderacionClasifMaximaReal = ponderacionClasifMaximaReal;
	}

	public double getPonderacionClasifMaximaCalculada() {
		return ponderacionClasifMaximaCalculada;
	}

	public void setPonderacionClasifMaximaCalculada(double ponderacionClasifMaximaCalculada) {
		this.ponderacionClasifMaximaCalculada = ponderacionClasifMaximaCalculada;
	}

	public double getPonderacionClasifObtenidaReal() {
		return ponderacionClasifObtenidaReal;
	}

	public void setPonderacionClasifObtenidaReal(double ponderacionClasifObtenidaReal) {
		this.ponderacionClasifObtenidaReal = ponderacionClasifObtenidaReal;
	}

	public double getPonderacionClasifObtenidaCalculada() {
		return ponderacionClasifObtenidaCalculada;
	}

	public void setPonderacionClasifObtenidaCalculada(double ponderacionClasifObtenidaCalculada) {
		this.ponderacionClasifObtenidaCalculada = ponderacionClasifObtenidaCalculada;
	}

	public double getCalificacionClasifReal() {
		return calificacionClasifReal;
	}

	public void setCalificacionClasifReal(double calificacionClasifReal) {
		this.calificacionClasifReal = calificacionClasifReal;
	}

	public double getCalificacionClasifCalculada() {
		return calificacionClasifCalculada;
	}

	public void setCalificacionClasifCalculada(double calificacionClasifCalculada) {
		this.calificacionClasifCalculada = calificacionClasifCalculada;
	}

	public double[] getPonderacionCheckMaximaReal() {
		return ponderacionCheckMaximaReal;
	}

	public void setPonderacionCheckMaximaReal(double[] ponderacionCheckMaximaReal) {
		this.ponderacionCheckMaximaReal = ponderacionCheckMaximaReal;
	}

	public double[] getPonderacionCheckMaximaCalculada() {
		return ponderacionCheckMaximaCalculada;
	}

	public void setPonderacionCheckMaximaCalculada(double[] ponderacionCheckMaximaCalculada) {
		this.ponderacionCheckMaximaCalculada = ponderacionCheckMaximaCalculada;
	}

	public double[] getPonderacionCheckObtenidaReal() {
		return ponderacionCheckObtenidaReal;
	}

	public void setPonderacionCheckObtenidaReal(double[] ponderacionCheckObtenidaReal) {
		this.ponderacionCheckObtenidaReal = ponderacionCheckObtenidaReal;
	}

	public double[] getPonderacionCheckObtenidaCalculada() {
		return ponderacionCheckObtenidaCalculada;
	}

	public void setPonderacionCheckObtenidaCalculada(double[] ponderacionCheckObtenidaCalculada) {
		this.ponderacionCheckObtenidaCalculada = ponderacionCheckObtenidaCalculada;
	}

	public double[] getCalificacionCheckReal() {
		return calificacionCheckReal;
	}

	public void setCalificacionCheckReal(double[] calificacionCheckReal) {
		this.calificacionCheckReal = calificacionCheckReal;
	}

	public double[] getCalificacionCheckCalculada() {
		return calificacionCheckCalculada;
	}

	public void setCalificacionCheckCalculada(double[] calificacionCheckCalculada) {
		this.calificacionCheckCalculada = calificacionCheckCalculada;
	}

	public int[] getCountItemCheckfNo() {
		return countItemCheckfNo;
	}

	public void setCountItemCheckfNo(int[] countItemCheckfNo) {
		this.countItemCheckfNo = countItemCheckfNo;
	}

	public int[] getCountItemCheckfSi() {
		return countItemCheckfSi;
	}

	public void setCountItemCheckfSi(int[] countItemCheckfSi) {
		this.countItemCheckfSi = countItemCheckfSi;
	}

	public int[] getCountItemCheckNA() {
		return countItemCheckNA;
	}

	public void setCountItemCheckNA(int[] countItemCheckNA) {
		this.countItemCheckNA = countItemCheckNA;
	}

	public int[] getCountItemCheckRe() {
		return countItemCheckRe;
	}

	public void setCountItemCheckRe(int[] countItemCheckRe) {
		this.countItemCheckRe = countItemCheckRe;
	}

	public int[] getCountItemCheckTo() {
		return countItemCheckTo;
	}

	public void setCountItemCheckTo(int[] countItemCheckTo) {
		this.countItemCheckTo = countItemCheckTo;
	}

	public int[] getCountItemCheckImp() {
		return countItemCheckImp;
	}

	public void setCountItemCheckImp(int[] countItemCheckImp) {
		this.countItemCheckImp = countItemCheckImp;
	}

	public double[] getPorcentajeCheckReSi() {
		return porcentajeCheckReSi;
	}

	public void setPorcentajeCheckReSi(double[] porcentajeCheckReSi) {
		this.porcentajeCheckReSi = porcentajeCheckReSi;
	}

	public double[] getPorcentajeCheckReNo() {
		return porcentajeCheckReNo;
	}

	public void setPorcentajeCheckReNo(double[] porcentajeCheckReNo) {
		this.porcentajeCheckReNo = porcentajeCheckReNo;
	}

	public double[] getPorcentajeCheckToSi() {
		return porcentajeCheckToSi;
	}

	public void setPorcentajeCheckToSi(double[] porcentajeCheckToSi) {
		this.porcentajeCheckToSi = porcentajeCheckToSi;
	}

	public double[] getPorcentajeCheckToNo() {
		return porcentajeCheckToNo;
	}

	public void setPorcentajeCheckToNo(double[] porcentajeCheckToNo) {
		this.porcentajeCheckToNo = porcentajeCheckToNo;
	}

	public double[] getPorcentajeCheckToNA() {
		return porcentajeCheckToNA;
	}

	public void setPorcentajeCheckToNA(double[] porcentajeCheckToNA) {
		this.porcentajeCheckToNA = porcentajeCheckToNA;
	}

	public double[] getPondCheckNa() {
		return pondCheckNa;
	}

	public void setPondCheckNa(double[] pondCheckNa) {
		this.pondCheckNa = pondCheckNa;
	}

	public String[] getCheck() {
		return check;
	}

	public void setCheck(String[] check) {
		this.check = check;
	}

	public int[] getIdCheck() {
		return idCheck;
	}

	public void setIdCheck(int[] idCheck) {
		this.idCheck = idCheck;
	}

}
