package com.gruposalinas.checklist.domain;

import java.util.ArrayList;

public class ChecklistLayoutDTO {

    private int idConsecutivo;
    private int codigoChecklist;
    private String negocio;
    private String nombreProtocolo;
    private String seccionProtocolo;
    private String subSeccionProtocolo;
    private int idConsecutivoProtocolo;
    private int factorCritico;
    private String pregunta;
    private int idPreguntaPadre;
    private int tipoRespuesta;
    private String opcionesCombo;
    private String cumplimiento;
    private double ponderacion;
    private String requiereComentarios;
    private String requiereEvidencia;
    private String tipoEvidencia;
    private String plantillaSi;
    private String plantillaNo;
    private String responsable1;
    private String responsable2;
    private String zona;
    private String definicion;
    private String disciplina;
    private String sla;
    private String negocioPregunta;

    private int flagVF;
    private int flagErrorArbol;

    //SE AGREGAN
    private PreguntaDTO preguntaDTO;
    private ChecklistProtocoloDTO checklistProtocoloDTO;
    private ArrayList<ArbolDecisionDTO> arbolDTO;

    public PreguntaDTO getPreguntaDTO() {
        return preguntaDTO;
    }

    public void setPreguntaDTO(PreguntaDTO preguntaDTO) {
        this.preguntaDTO = preguntaDTO;
    }

    public ChecklistProtocoloDTO getChecklistProtocoloDTO() {
        return checklistProtocoloDTO;
    }

    public void setChecklistProtocoloDTO(ChecklistProtocoloDTO checklistProtocoloDTO) {
        this.checklistProtocoloDTO = checklistProtocoloDTO;
    }

    public int getIdConsecutivo() {
        return idConsecutivo;
    }

    public void setIdConsecutivo(int idConsecutivo) {
        this.idConsecutivo = idConsecutivo;
    }

    public int getCodigoChecklist() {
        return codigoChecklist;
    }

    public void setCodigoChecklist(int codigoChecklist) {
        this.codigoChecklist = codigoChecklist;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    public String getNombreProtocolo() {
        return nombreProtocolo;
    }

    public void setNombreProtocolo(String nombreProtocolo) {
        this.nombreProtocolo = nombreProtocolo;
    }

    public String getSeccionProtocolo() {
        return seccionProtocolo;
    }

    public void setSeccionProtocolo(String seccionProtocolo) {
        this.seccionProtocolo = seccionProtocolo;
    }

    public String getSubSeccionProtocolo() {
        return subSeccionProtocolo;
    }

    public void setSubSeccionProtocolo(String subSeccionProtocolo) {
        this.subSeccionProtocolo = subSeccionProtocolo;
    }

    public int getIdConsecutivoProtocolo() {
        return idConsecutivoProtocolo;
    }

    public void setIdConsecutivoProtocolo(int idConsecutivoProtocolo) {
        this.idConsecutivoProtocolo = idConsecutivoProtocolo;
    }

    public int getFactorCritico() {
        return factorCritico;
    }

    public void setFactorCritico(int factorCritico) {
        this.factorCritico = factorCritico;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public int getIdPreguntaPadre() {
        return idPreguntaPadre;
    }

    public void setIdPreguntaPadre(int idPreguntaPadre) {
        this.idPreguntaPadre = idPreguntaPadre;
    }

    public int getTipoRespuesta() {
        return tipoRespuesta;
    }

    public void setTipoRespuesta(int tipoRespuesta) {
        this.tipoRespuesta = tipoRespuesta;
    }

    public String getOpcionesCombo() {
        return opcionesCombo;
    }

    public void setOpcionesCombo(String opcionesCombo) {
        this.opcionesCombo = opcionesCombo;
    }

    public String getCumplimiento() {
        return cumplimiento;
    }

    public void setCumplimiento(String cumplimiento) {
        this.cumplimiento = cumplimiento;
    }

    public double getPonderacion() {
        return ponderacion;
    }

    public void setPonderacion(double ponderacion) {
        this.ponderacion = ponderacion;
    }

    public String getRequiereComentarios() {
        return requiereComentarios;
    }

    public void setRequiereComentarios(String requiereComentarios) {
        this.requiereComentarios = requiereComentarios;
    }

    public String getRequiereEvidencia() {
        return requiereEvidencia;
    }

    public void setRequiereEvidencia(String requiereEvidencia) {
        this.requiereEvidencia = requiereEvidencia;
    }

    public String getTipoEvidencia() {
        return tipoEvidencia;
    }

    public void setTipoEvidencia(String tipoEvidencia) {
        this.tipoEvidencia = tipoEvidencia;
    }

    public String getPlantillaSi() {
        return plantillaSi;
    }

    public void setPlantillaSi(String plantillaSi) {
        this.plantillaSi = plantillaSi;
    }

    public String getPlantillaNo() {
        return plantillaNo;
    }

    public void setPlantillaNo(String plantillaNo) {
        this.plantillaNo = plantillaNo;
    }

    public String getResponsable1() {
        return responsable1;
    }

    public void setResponsable1(String responsable1) {
        this.responsable1 = responsable1;
    }

    public String getResponsable2() {
        return responsable2;
    }

    public void setResponsable2(String responsable2) {
        this.responsable2 = responsable2;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getDefinicion() {
        return definicion;
    }

    public void setDefinicion(String definicion) {
        this.definicion = definicion;
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    public String getSla() {
        return sla;
    }

    public void setSla(String sla) {
        this.sla = sla;
    }

    public String getNegocioPregunta() {
        return negocioPregunta;
    }

    public void setNegocioPregunta(String negocioPregunta) {
        this.negocioPregunta = negocioPregunta;
    }

    @Override
    public String toString() {
        return "ChecklistLayoutDTO [idConsecutivo=" + idConsecutivo + ", codigoChecklist=" + codigoChecklist
                + ", negocio=" + negocio + ", nombreProtocolo=" + nombreProtocolo + ", seccionProtocolo="
                + seccionProtocolo + ", subSeccionProtocolo=" + subSeccionProtocolo + ", idConsecutivoProtocolo="
                + idConsecutivoProtocolo + ", factorCritico=" + factorCritico + ", pregunta=" + pregunta
                + ", idPreguntaPadre=" + idPreguntaPadre + ", tipoRespuesta=" + tipoRespuesta + ", opcionesCombo="
                + opcionesCombo + ", cumplimiento=" + cumplimiento + ", ponderacion=" + ponderacion
                + ", requiereComentarios=" + requiereComentarios + ", requiereEvidencia=" + requiereEvidencia
                + ", tipoEvidencia=" + tipoEvidencia + ", plantillaSi=" + plantillaSi + ", plantillaNo=" + plantillaNo
                + ", responsable1=" + responsable1 + ", responsable2=" + responsable2 + ", zona=" + zona
                + ", definicion=" + definicion + ", disciplina=" + disciplina + ", sla=" + sla + "]";
    }

    public ArrayList<ArbolDecisionDTO> getArbolDTO() {
        return arbolDTO;
    }

    public void setArbolDTO(ArrayList<ArbolDecisionDTO> arbolDTO) {
        this.arbolDTO = arbolDTO;
    }

    public int getFlagVF() {
        return flagVF;
    }

    public void setFlagVF(int flagVF) {
        this.flagVF = flagVF;
    }

    public int getFlagErrorArbol() {
        return flagErrorArbol;
    }

    public void setFlagErrorArbol(int flagErrorArbol) {
        this.flagErrorArbol = flagErrorArbol;
    }

}
