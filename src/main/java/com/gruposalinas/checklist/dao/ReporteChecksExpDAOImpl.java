package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ReporteChecksExpDTO;
import com.gruposalinas.checklist.mappers.ReporteChecklistsExpRowMapper;
import com.gruposalinas.checklist.mappers.ReporteChecksExpRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ReporteChecksExpDAOImpl extends DefaultDAO implements ReporteChecksExpDAO {

    private static Logger logger = LogManager.getLogger(ReporteChecksExpDAOImpl.class);

    private DefaultJdbcCall jdbcObtienePreguntasNo;
    private DefaultJdbcCall jdbcObtieneeChecks;
    private DefaultJdbcCall jdbcObtienePregNoImpCecoFasePooyecto;

    public void init() {

        jdbcObtienePreguntasNo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGCHECKS")
                .withProcedureName("SP_CONS_NO")
                .returningResultSet("RCL_PREG", new ReporteChecksExpRowMapper());

        jdbcObtieneeChecks = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAPREGCHECKS")
                .withProcedureName("SP_CONSCHECK_USU")
                .returningResultSet("RCL_PREG", new ReporteChecklistsExpRowMapper());

        // PREGUNTAS NO IMP CECO FASE PROYECTOString
        jdbcObtienePregNoImpCecoFasePooyecto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEXPNVOFLUJO")
                .withProcedureName("SP_CONS_NO")
                .returningResultSet("RCL_PREG", new ReporteChecksExpRowMapper());

    }

    @SuppressWarnings("unchecked")
    public List<ReporteChecksExpDTO> ObtieneBitacoras(String idusuario, String ceco, String idcheck) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ReporteChecksExpDTO> lista = null;

        logger.info("Entro en el dao Consulta obtiene Bitacoras");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_USU", idusuario)
                .addValue("PA_CECO", ceco)
                .addValue("PA_CHECK", idcheck);

        out = jdbcObtieneeChecks.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGCHECKS.SP_CONSCHECK_USU}");
        //lleva el nombre del cursor del procedure
        lista = (List<ReporteChecksExpDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el empleado(" + idcheck + ")");
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<ReporteChecksExpDTO> obtieneRespuestasNo(int bitacora) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ReporteChecksExpDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_BITACORA", bitacora);

        out = jdbcObtienePreguntasNo.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAPREGCHECKS.SP_CONS_NO}");
        //lleva el nombre del cursor del procedure
        lista = (List<ReporteChecksExpDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener la bitacora(" + bitacora + ")");
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<ReporteChecksExpDTO> obtienePregNoImpCecoFaseProyecto(String ceco, String fase, String proyecto) throws Exception {
        Map<String, Object> out = null;
        List<ReporteChecksExpDTO> listanoimp = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FASE", fase)
                .addValue("PA_PROYECTO", proyecto);

        out = jdbcObtienePregNoImpCecoFasePooyecto.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAEXPNVOFLUJO.SP_CONS_NO}");

        listanoimp = (List<ReporteChecksExpDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion de no  imperdonables");
        } else {
            return listanoimp;
        }

        return listanoimp;
    }

}
