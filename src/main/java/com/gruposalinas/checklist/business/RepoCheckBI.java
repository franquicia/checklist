package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.EmpFijoDAO;
import com.gruposalinas.checklist.dao.RepoCheckDAO;
import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.RepoCheckDTO;

public class RepoCheckBI {

	private static Logger logger = LogManager.getLogger(RepoCheckBI.class);

private List<RepoCheckDTO> listafila;
	
	@Autowired
	RepoCheckDAO repoCheckDAO;
		
	


	// obtiene checklist de usuario
	public List<RepoCheckDTO> obtieneDatos( int idUsuario) {
		
		try {
			listafila =  repoCheckDAO.obtieneDatos(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	//obtiene su info del usuario
	public List<RepoCheckDTO> obtieneInfo(int idUsuario) {

		try {
			listafila = repoCheckDAO.obtieneInfo(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos de la tabla Filas");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	

}