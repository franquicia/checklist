/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.business;

import com.gruposalinas.checklist.dao.ComentariosRepAsgDAO;
import com.gruposalinas.checklist.domain.ComentariosRepAsgDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author leodan1991
 */
public class ComentariosRepAsgBI {

    @Autowired
    private ComentariosRepAsgDAO comentariosRepAsgDAO;

    public List<ComentariosRepAsgDTO> getComentarios(String ceco, String fecha, Integer anio, Integer mes) throws Exception {
        List<ComentariosRepAsgDTO> listaComentarios = null;
        try {
            listaComentarios = comentariosRepAsgDAO.getComentarios(ceco, fecha, anio, mes);

        } catch (Exception e) {

        }

        return listaComentarios;
    }

    public Boolean setComentarios(String ceco, String fecha, String comentario) throws Exception {
        Boolean estatus = null;
        try {

            estatus = comentariosRepAsgDAO.setComentarios(ceco, fecha, comentario);

        } catch (Exception e) {

        }

        return estatus;
    }
    
    public Boolean updateComentarios(String ceco, String fecha, String comentario) throws Exception {
        Boolean estatus = null;
        try {

            estatus = comentariosRepAsgDAO.updateComentarios(ceco, fecha, comentario);

        } catch (Exception e) {

        }

        return estatus;
    }

    public Boolean deleteComentarios(String ceco, String fecha, String comentario) throws Exception {
        Boolean estatus = null;
        try {

            estatus = comentariosRepAsgDAO.deleteComentarios(ceco, fecha, comentario);

        } catch (Exception e) {

        }

        return estatus;
    }
}
