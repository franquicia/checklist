<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
 	<script src="../js/jquery/jquery-1.8.0.min.js"></script>
 	
    <title>ACCESO</title>
    	<style type="text/css">
		
		
		
*{
  box-sizing:border-box;
  -moz-box-sizing:border-box;
  -webkit-box-sizing:border-box;
  font-family:arial;
}

body{background:url("../images/loginFondo.jpg")#FF9000}
h1{
  color:#AAA173;
  text-align:center;
  font-faimly:icon;
}

.login-form{
  width:350px;
  padding:40px 30px;
  background:rgba(235,235,205,0.7);
  border-radius:4px;
  -moz-border-radius:4px;
  -webkit-border-radius:4px;
  margin:50px auto;
}
.form-group{
  position: relative;
  margin-bottom:15px;
}
.form-control{
  width:100%;
  height:50px;
  border:none;
  padding:5px 7px 5px 15px;
  background:#fff;
  color:#666;
  border:2px solid #E0D68F;
  border-radius:4px;
  -moz-border-radius:4px;
  -webkit-border-radius:4px;
}
.form-control:focus, .form-control:focus + .fa{
  border-color:#10CE88;
  color:#10CE88;
}
.form-group .fa{
  position: absolute;
  right:15px;
  top:17px;
  color:#999;
}
.log-status.wrong-entry {
  -webkit-animation: wrong-log 0.3s;
  -moz-animation: wrong-log 0.3s;
  -ms-animation: wrong-log 0.3s;
  animation: wrong-log 0.3s;
}
.log-status.wrong-entry .form-control, .wrong-entry .form-control + .fa {
  border-color: #ed1c24;
  color: #ed1c24;
}
@keyframes wrong-log {
  0% { left: 0px;}
  20% {left: 15px;}
  40% {left: -15px;}
  60% {left: 15px;}
  80% {left: -15px;}
  100% {left: 0px;}
}
@-ms-keyframes wrong-log {
  0% { left: 0px;}
  20% {left: 15px;}
  40% {left: -15px;}
  60% {left: 15px;}
  80% {left: -15px;}
  100% {left: 0px;}
}
@-moz-keyframes wrong-log {
  0% { left: 0px;}
  20% {left: 15px;}
  40% {left: -15px;}
  60% {left: 15px;}
  80% {left: -15px;}
  100% {left: 0px;}
}
@-webkit-keyframes wrong-log {
  0% { left: 0px;}
  20% {left: 15px;}
  40% {left: -15px;}
  60% {left: 15px;}
  80% {left: -15px;}
  100% {left: 0px;}
}
.log-btn{
  background:#0AC986;
  dispaly:inline-block;
  width:100%;
  font-size:16px;
  height:50px;
  color:#fff;
  text-decoration:none;
  border:none;
  border-radius:4px;
  -moz-border-radius:4px;
  -webkit-border-radius:4px;
}

#log-btn:hover{

	cursor:pointer;	
}


.alert{
  display:none;
  font-size:12px;
  color:#f00;
  float:left;
}

		
</style>
<!-- 
	<script type="text/javascript">
	  $(document).ready(function(){
	        $('.log-btn').click(function(){
	            $('.log-status').addClass('wrong-entry');
	           $('.alert').fadeIn(1500);
	           setTimeout( "$('.alert').fadeOut(1500);",3000 );
	        });
	        $('.form-control').keypress(function(){
	            $('.log-status').removeClass('wrong-entry');
	        });
	    });
	</script>
-->
</head>

 

<div  class="login-form">
<form:form action="consultaLogin.htm" method="POST" autocomplete="off">
<h1>Login</h1>

		<br>
		<fieldset><legend>Ingresa Credenciales</legend>
	
			<input type="hidden"  value="true" name="valida"/>

			<div class="form-group ">
			<br>
			USER: 	 	<form:input type="text" class="form-control" placeholder="Username "  path="user"/><br><br>
			<i class="fa fa-user"></i>
			</div>
			
			<div class="form-group log-status">
			PASSWORD:   <form:input type="password" class="form-control" placeholder="Password"  path="pass" /><br>
			<i class="fa fa-lock"></i>
			 </div>
		</fieldset>
		${invalido}
		<br><br>
		<button type="submit"  class="log-btn" id="log-btn">Enviar</button>
</form:form>
</div>

</body>

</html>