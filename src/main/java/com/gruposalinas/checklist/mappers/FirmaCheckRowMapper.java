package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;

public class FirmaCheckRowMapper implements RowMapper<FirmaCheckDTO> {

	public FirmaCheckDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		FirmaCheckDTO firm = new FirmaCheckDTO();
		
		firm.setIdFirma(rs.getInt("FIID_FIRMCHECK"));
		firm.setBitacora(rs.getString("FIID_BITGRAL"));
		firm.setCeco(rs.getString("FCID_CECO"));
		firm.setIdUsuario(rs.getString("FIID_USUARIO"));
		firm.setPuesto(rs.getString("FCPUESTO"));
		firm.setResponsable(rs.getString("FCRESPONSABLE"));
		firm.setCorreo(rs.getString("FCCORREO"));
		firm.setNombre(rs.getString("FCNOMBRE"));
		firm.setRuta(rs.getString("FIID_RUTA"));
		firm.setPeriodo(rs.getString("FCPERIODO"));
		
		return firm;
	}

}


