package com.gruposalinas.checklist.dao;

import java.util.List;


import com.gruposalinas.checklist.domain.DetalleProtoDTO;


public interface DetalleProtoDAO {
	
	public List<DetalleProtoDTO> ObtieneDetalleProto(DetalleProtoDTO bean) throws Exception;
	public List<DetalleProtoDTO> ObtieneNombreGrupo(DetalleProtoDTO bean) throws Exception;
	public List<DetalleProtoDTO> ObtieneNombreCeco (DetalleProtoDTO bean) throws Exception;
	public List<DetalleProtoDTO> ObtieneBitacoraCerrada(DetalleProtoDTO bean) throws Exception;
	public List<DetalleProtoDTO> ObtieneControlAcceso(DetalleProtoDTO bean) throws Exception;

	


}
