package com.gruposalinas.checklist.domain;

public class Coment7SDTO {
	 private int idPregunta;
	 private int idArbolDes;
	 private int idBitacora;
	 private int idRespuesta;
	 private String descripcion;
	 private int idCheckUsua;
	 private int idChecklist;
	 private String ceco;
	 private int idUsuario;
	 
	 
	 
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdCheckUsua() {
		return idCheckUsua;
	}
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	
	public int getIdArbolDes() {
		return idArbolDes;
	}
	public void setIdArbolDes(int idArbolDes) {
		this.idArbolDes = idArbolDes;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public int getIdRespuesta() {
		return idRespuesta;
	}
	public void setIdRespuesta(int idRespuesta) {
		this.idRespuesta = idRespuesta;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public void setIdCheckUsua(int idCheckUsua) {
		this.idCheckUsua=idCheckUsua;
	}
	
	public int getIdChecklist() {
		return this.idChecklist;
	}
	
	public void setIdChecklist(int idChecklist) {
		this.idChecklist=idChecklist;
	}
	
	public String getCeco() {
		return ceco;
	}
	public void setCeco(String ceco) {
		this.ceco=ceco;
	}
}
