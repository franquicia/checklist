package com.gruposalinas.checklist.servicios.servidor;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.resources.FRQConstantes;

@Controller
@RequestMapping("/soporte")
public class ServicioArchivos {

	private static Logger logger = LogManager.getLogger(ServicioArchivos.class);

	private static final int MAX_FILE_SIZE      = 1024 * 1024 * 100; // 100MB
	
	//http://localhost:8080/checklist/soporte/fileCenter.htm
	@RequestMapping(value = "/fileCenter", method = RequestMethod.GET)
	public ModelAndView getFileCenter(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		if (FRQConstantes.PRODUCCION) {
			UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
			Integer user = Integer.parseInt(userSession.getIdUsuario());

			if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312) ||
					user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899) ||
					user.equals(643965) || user.equals(304513))) {
				return new ModelAndView("redirect:/views/http404.jsp");
			}
		}

		ModelAndView mv = new ModelAndView("indexFileCenter", "command", new FileParameters());

		mv.addObject("fileAction", "subirArchivo");

		return mv;
	}

	//http://localhost:8080/checklist/soporte/fileCenter.htm
	@RequestMapping(value = "/fileCenter", method = RequestMethod.POST)
	public ModelAndView postFileCenter(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "fileRoute", required = true) String fileRoute,
			@RequestParam(value = "fileAction", required = true) String fileAction) 
					throws IOException {

		if (FRQConstantes.PRODUCCION) {
			UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
			Integer user = Integer.parseInt(userSession.getIdUsuario());

			if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312) ||
					user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899) ||
					user.equals(643965) || user.equals(304513))) {
				return new ModelAndView("redirect:/views/http404.jsp");
			}
		}

		ModelAndView mv = new ModelAndView("indexFileCenter", "command", new FileParameters());

		if (new File(fileRoute).isDirectory()) {
			logger.info("El directorio " + fileRoute + " si existe");
			
			if (fileAction.equals("subirArchivo")) {
				//mv.addObject("fileAction", fileAction);
			}
			else if (fileAction.equals("eliminarArchivo")) {
				mv.addObject("listaArchivos", new File(fileRoute).list());
			}
			else if (fileAction.equals("renombrarArchivo")) {
				mv.addObject("listaArchivos", new File(fileRoute).list());
			}
		}
		else {
			logger.info("El directorio no existe");
			mv.addObject("directorio", "noExiste");
		}

		mv.addObject("fileRoute", fileRoute);
		mv.addObject("fileAction", fileAction);

		return mv;

	}

	//http://localhost:8080/checklist/soporte/uploadFiles.htm
	@RequestMapping(value = "/uploadFiles", method = RequestMethod.POST)
	public ModelAndView postUploadFiles(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "uploadedFile", required = true) MultipartFile uploadedFile,
			@RequestParam(value = "fileRoute", required = true) String fileRoute,
			@RequestParam(value = "fileAction", required = true) String fileAction)
					throws IOException {

		if (FRQConstantes.PRODUCCION) {
			UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
			Integer user = Integer.parseInt(userSession.getIdUsuario());

			if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312) ||
					user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899) ||
					user.equals(643965) || user.equals(304513))) {
				return new ModelAndView("redirect:/views/http404.jsp");
			}
		}

		ModelAndView mv = new ModelAndView("indexFileCenter", "command", new FileParameters());

		String rootPath = File.listRoots()[0].getAbsolutePath();
		String ruta = rootPath + fileRoute;

		File route = new File(ruta);

		if (route.exists()) {
			if (uploadedFile.getSize() < MAX_FILE_SIZE) {
				if (checkContentType(uploadedFile.getContentType())) {
					BufferedOutputStream bufferOutput=null;
					try {
		   				FileOutputStream fileOutput = new FileOutputStream(ruta + uploadedFile.getOriginalFilename());

		   				bufferOutput = new BufferedOutputStream(fileOutput);
		   				bufferOutput.write((byte []) uploadedFile.getBytes());
		   				bufferOutput.close();

		       			logger.info("Archivo " + uploadedFile.getOriginalFilename() + " fue subido exitosamente!");
		       			mv.addObject("archivoSubido", "si");
		   			}
		   			catch (Exception e) {
		       			mv.addObject("archivoSubido", "no");
		   				logger.info("ERROR: " + e.getMessage());
		   			}
					finally{
						if(bufferOutput!=null){
							bufferOutput.close();
						}
					}
				}
				else {
	       			mv.addObject("archivoSubido", "no-compatible");
					logger.info("El archivo " + uploadedFile.getOriginalFilename() + " tiene una extensión incompatible.");
				}
	       	}
	       	else {
       			mv.addObject("archivoSubido", "size");
	   			logger.info("El archivo " + uploadedFile.getOriginalFilename() + " es mayor a 100MB");
	       	}
		}
		else {
   			mv.addObject("archivoSubido", "no");
			logger.info("El directorio " + fileRoute + " no existe");
		}
		
		mv.addObject("fileAction", fileAction);
		mv.addObject("fileRoute", fileRoute);

		return mv;
		
	}

	//http://localhost:8080/checklist/soporte/deleteFiles.htm
	@RequestMapping(value = "/deleteFiles", method = RequestMethod.POST)
	public ModelAndView postListFiles(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "fileRoute", required = true) String fileRoute,
			@RequestParam(value = "fileAction", required = true) String fileAction) 
					throws ServletException, IOException {

		if (FRQConstantes.PRODUCCION) {
			UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
			Integer user = Integer.parseInt(userSession.getIdUsuario());

			if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312) ||
					user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899) ||
					user.equals(643965) || user.equals(304513))) {
				return new ModelAndView("redirect:/views/http404.jsp");
			}
		}

		ModelAndView mv = new ModelAndView("indexFileCenter", "command", new FileParameters());

		List<String> listaArchivosEliminar = new ArrayList<String>();
		List<String> auxList = new ArrayList<String>();
		List<String> fullList = new ArrayList<String>();

		String ruta = File.listRoots()[0].getAbsolutePath() + fileRoute;

		File directory = new File(ruta);

		for (String video : directory.list())
			fullList.add(video);

		if (request.getParameterValues("listaArchivosEliminar") != null) {
			for (String video : request.getParameterValues("listaArchivosEliminar"))
				listaArchivosEliminar.add(video);

			if (listaArchivosEliminar.size() > 0) {
				if (directory.exists()) {
					File deleteFile;

					for (String file : listaArchivosEliminar) {
						deleteFile = new File (ruta + file);
						
						if (deleteFile.delete()) {
							logger.info("El archivo " + file + " fue eliminado");
							auxList.add(file);
							mv.addObject("eliminado", "si");
						}
						else {
							logger.info("El archivo " + file + " no fue eliminado");
							mv.addObject("eliminado", "no");
						}
					}

					deleteFile = null;
					
					if (auxList.isEmpty()) {
						mv.addObject("listaArchivos", fullList);
					}
					else {
						fullList.removeAll(auxList);
						mv.addObject("listaArchivos", fullList);
					}
				}
				else {
					logger.info("El directorio no existe, no se pueden eliminar los vídeos");
					mv.addObject("eliminado", "no");
					mv.addObject("listaArchivos", fullList);
				}
			}
			else {
				logger.info("La lista se encuentra vacía, no se puede eliminar ningún vídeo");
				mv.addObject("eliminado", "no");
			}
		}
		else {
			mv.addObject("eliminado", "no-vacio");
			mv.addObject("listaArchivos", fullList);
		}
		
		mv.addObject("fileAction", fileAction);
		mv.addObject("fileRoute", fileRoute);

		return mv;

	}

	//http://localhost:8080/checklist/soporte/renameFiles.htm
	@RequestMapping(value = "/renameFiles", method = RequestMethod.POST)
	public ModelAndView postRenameFiles(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "fileRoute", required = true) String fileRoute,
			@RequestParam(value = "fileAction", required = true) String fileAction,
			@RequestParam(value = "oldFileName", required = true) String oldFileName,
			@RequestParam(value = "newFileName", required = true) String newFileName) 
					throws ServletException, IOException {
		
		if (FRQConstantes.PRODUCCION) {
			UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
			Integer user = Integer.parseInt(userSession.getIdUsuario());

			if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312) ||
					user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899) ||
					user.equals(643965) || user.equals(304513))) {
				return new ModelAndView("redirect:/views/http404.jsp");
			}
		}

		ModelAndView mv = new ModelAndView("indexFileCenter", "command", new FileParameters());

		File directory = new File(fileRoute);
		
		if (oldFileName != null) {
			logger.info("NOT NULL");
		}
		else {
			logger.info("NULL");
		}
		
		if (directory.isDirectory()) {
			File uploadedFile = new File(fileRoute + oldFileName);
			
			if (uploadedFile.isFile()) {
				try {
					if (uploadedFile.renameTo(new File(fileRoute + newFileName))) {
						logger.info("El archivo " + uploadedFile.getName() + " fue renombrado exitosamente");
						mv.addObject("fileAction", fileAction);
						mv.addObject("fileRoute", fileRoute);
						mv.addObject("renombrado", "si");
					}
					else {
						logger.info("El archivo " + uploadedFile.getName() + " no pudo ser renombrado");
						mv.addObject("fileAction", fileAction);
						mv.addObject("fileRoute", fileRoute);
						mv.addObject("renombrado", "no");
					}
				}
				catch (Exception e) {
					logger.info("ERROR " + e.getMessage());
				}
			}
			else {
				uploadedFile = null;
				logger.info("El archivo no existe o su nombre no es correcto.");
				mv.addObject("renombrado", "no");
			}
			
			List<String> fullList = new ArrayList<String>();
			
			for (String video : directory.list())
				fullList.add(video);
			
			mv.addObject("listaArchivos", fullList);
		}
		else {
			logger.info("El directorio no existe o no es correcto.");
			mv.addObject("renombrado", "no");
		}

		return mv;
			
	}
	
	/**
	 * audio/aac						//AAC
	 * audio/mp4						//AAC (Firefox)
	 * video/x-msvideo 					//AVI
	 * application/octet-stream			//BIN
	 * text/css							//CSS
	 * application/msword				//DOC
	 * application/epub+zip				//EPUB
	 * image/gif						//GIF
	 * text/html						//HTM / HTML
	 * image/x-icon						//ICO
	 * application/java-archive			//JAR
	 * image/jpeg						//JPG / JPEG
	 * application/javascript			//JS
	 * application/json					//JSON
	 * video/mpeg						//MPEG
	 * audio/mpeg						//MP3
	 * video/mp4						//MP4
	 * video/quicktime					//MOV
	 * image/png						//PNG
	 * application/pdf					//PDF
	 * application/vnd.ms-powerpoint	//PPT
	 * application/x-rar-compressed		//RAR
	 * application/rtf					//RTF
	 * audio/x-wav						//WAV
	 * video/x-ms-wmv					//WMV
	 * application/xhtml+xml			//XHMTL
	 * application/vnd.ms-excel			//XLS
	 * application/xml					//XML
	 * application/zip					//ZIP
	 * 
	 * application/vnd.openxmlformats-officedocument.wordprocessingml.document			//DOCX
	 * application/vnd.openxmlformats-officedocument.spreadsheetml.sheet				//XLSX
	 * application/vnd.openxmlformats-officedocument.presentationml.presentation		//PPTX
	 * 
	 */

	boolean checkContentType (String contentType) {

		String [] contentTypeList = {"audio/aac", "video/x-msvideo", "application/octet-stream", "text/css", "application/msword", "application/epub+zip", 
				"image/gif", "text/html", "image/x-icon", "application/java-archive", "image/jpeg", "application/javascript", "application/json", "video/mpeg", 
				"audio/mpeg", "video/mp4", "image/png", "application/pdf", "application/vnd.ms-powerpoint", "application/x-rar-compressed", "application/rtf",
				"audio/x-wav", "application/xhtml+xml", "application/vnd.ms-excel", "application/xml", "application/zip",
				"application/vnd.openxmlformats-officedocument.wordprocessingml.document", "audio/mp4", "video/quicktime", 
				"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "video/x-ms-wmv", 
				"application/vnd.openxmlformats-officedocument.presentationml.presentation",
				null};

		for (String string : contentTypeList) {
			if (string.equals(contentType))
				return true;
		}

		return false;
	}
}

class FileParameters {

	private MultipartFile video;
	private String fileAction;
	private String fileRoute;
	private String newFileName;
	private String oldFileName;
	
	public MultipartFile getVideo() {
		return video;
	}
	public void setVideo(MultipartFile video) {
		this.video = video;
	}
	public String getFileAction() {
		return fileAction;
	}
	public void setFileAction(String fileAction) {
		this.fileAction = fileAction;
	}
	public String getFileRoute() {
		return fileRoute;
	}
	public void setFileRoute(String fileRoute) {
		this.fileRoute = fileRoute;
	}
	public String getNewFileName() {
		return newFileName;
	}
	public void setNewFileName(String newFileName) {
		this.newFileName = newFileName;
	}
	public String getOldFileName() {
		return oldFileName;
	}
	public void setOldFileName(String oldFileName) {
		this.oldFileName = oldFileName;
	}
}