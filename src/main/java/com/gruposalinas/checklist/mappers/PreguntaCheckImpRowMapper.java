package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.PreguntaCheckDTO;

public class PreguntaCheckImpRowMapper implements RowMapper<ChecklistPreguntasComDTO> {

	public ChecklistPreguntasComDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		

		ChecklistPreguntasComDTO com = new ChecklistPreguntasComDTO();

        com.setIdBita(rs.getInt("FIID_BITACORA"));
	    com.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    com.setIdArbol(rs.getInt("FIID_ARBOL_DES"));
	    com.setObserv(rs.getString("OBSERVACIONES"));
	    com.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    com.setIdCheck(rs.getInt("FIID_CHECKLIST"));
	    com.setIdPosible(rs.getInt("FCRESPUESTA"));
	    com.setIdObserv(rs.getInt("FIREQOBS"));
	    com.setPosible(rs.getString("POSIBLE"));
	    com.setPregunta(rs.getString("PREGUNTA"));   
	    com.setIdcritica(rs.getInt("FICRITICA"));
	    com.setClasif(rs.getString("FCCLASIFICA"));
	    com.setSla(rs.getString("FCSLA"));
	    com.setPregPadre(rs.getInt("FIID_PREG_PADRE")); 
	    com.setRuta(rs.getString("FCRUTA"));
	    com.setIdBitaGral(rs.getInt("FIID_BITGRAL"));
	    com.setLiderObra(rs.getString("LIDEROBRA"));
	    com.setFechaIni(rs.getString("FDINICIO"));
	    com.setFechaFin(rs.getString("FDTERMINO"));
	    com.setIdUsu(rs.getInt("FIID_USUARIO"));
	    com.setRealizoReco(rs.getString("FCNOMBRE"));
	    com.setNombCeco(rs.getString("CECO"));
	    com.setEco(rs.getInt("ECO"));
	    com.setCeco(rs.getString("FCID_CECO"));

		return com;
	}
}



