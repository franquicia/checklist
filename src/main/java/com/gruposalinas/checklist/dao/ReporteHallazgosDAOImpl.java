package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ReporteHallazgosDTO;
import com.gruposalinas.checklist.mappers.HallazgosRepoCecoRowMapper;
import com.gruposalinas.checklist.mappers.ReporteHallazgosDetalleRowMapper;
import com.gruposalinas.checklist.mappers.ReporteHallazgosRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ReporteHallazgosDAOImpl extends DefaultDAO implements ReporteHallazgosDAO {

    private static Logger logger = LogManager.getLogger(ReporteHallazgosDAOImpl.class);

    DefaultJdbcCall jdbcObtieneConteoGenerales;
    DefaultJdbcCall jdbcObtieneConteoFase;
    DefaultJdbcCall jdbcObtieneConteoCeco;
    DefaultJdbcCall jdbcObtieneCecoFechas;

    public void init() {

        jdbcObtieneConteoGenerales = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOHALLATRA")
                .withProcedureName("SP_SEL_HALLANEGO")
                .returningResultSet("RCL_CONTEOGEN", new ReporteHallazgosRowMapper());

        jdbcObtieneConteoCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOHALLATRA")
                .withProcedureName("SP_SEL_HALLACECO")
                .returningResultSet("RCL_CONTEOCECO", new ReporteHallazgosRowMapper());

        jdbcObtieneConteoFase = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPOHALLATRA")
                .withProcedureName("SP_SEL_HALLAPROY")
                .returningResultSet("RCL_CONTEOFASE", new ReporteHallazgosDetalleRowMapper());

        jdbcObtieneCecoFechas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAHALLASOP")
                .withProcedureName("SP_SEL_CECOS")
                .returningResultSet("RCL_CECOS", new HallazgosRepoCecoRowMapper());

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public Map<String, Object> conteoGralHallazgos(String nego, String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<ReporteHallazgosDTO> listaConteo = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NEGO", nego)
                .addValue("PA_FECHAINI", fechaInicio)
                .addValue("PA_FECHAFIN", fechaFin);
        out = jdbcObtieneConteoGenerales.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPOHALLATRA.SP_SEL_CONTEOGENERALES HALLAZ}");

        listaConteo = (List<ReporteHallazgosDTO>) out.get("RCL_CONTEOGEN");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener informacion de padres e hijas");
        } else {
            lista = new HashMap<String, Object>();
            lista.put("listaConteo", listaConteo);

            return lista;

        }
        return lista;

    }
    //parametro

    @SuppressWarnings("unchecked")
    public List<ReporteHallazgosDTO> conteoCecoHallazgos(String ceco, String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ReporteHallazgosDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FECHAINI", fechaInicio)
                .addValue("PA_FECHAFIN", fechaFin);;

        out = jdbcObtieneConteoCeco.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPOHALLATRA.SP_SEL_CONTEOCECO}");
        //lleva el nombre del cursor del procedure
        lista = (List<ReporteHallazgosDTO>) out.get("RCL_CONTEOCECO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener datos del CONTEO POR ceco(" + ceco + ")");
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<ReporteHallazgosDTO> conteoFaseHallazgos(String ceco, String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ReporteHallazgosDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_FECHAINI", fechaInicio)
                .addValue("PA_FECHAFIN", fechaFin);;

        out = jdbcObtieneConteoFase.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAREPOHALLATRA.SP_SEL_CONTEOCECOFASEPROY}");
        //lleva el nombre del cursor del procedure
        lista = (List<ReporteHallazgosDTO>) out.get("RCL_CONTEOFASE");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener datos del CONTEO ceco FASE PROY(" + ceco + ")");
        }
        return lista;

    }

    @SuppressWarnings("unchecked")
    public List<ReporteHallazgosDTO> obtieneCecosFechas(String negocio, String fechaInicio, String fechaFin)
            throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ReporteHallazgosDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_NEGO", negocio)
                .addValue("PA_FECHAINI", fechaInicio)
                .addValue("PA_FECHAFIN", fechaFin);;

        out = jdbcObtieneCecoFechas.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAHALLASOP.SP_SEL_CECOS}");
        //lleva el nombre del cursor del procedure
        lista = (List<ReporteHallazgosDTO>) out.get("RCL_CECOS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener datos del CECOS DEL NEGOCIO(" + negocio + ")");
        }
        return lista;

    }

}
