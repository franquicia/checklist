package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;

import com.gruposalinas.checklist.dao.RepoCheckPaperDAO;
import com.gruposalinas.checklist.domain.RepoCheckPaperDTO;

public class RepoCheckPaperBI {

	private static Logger logger = LogManager.getLogger(RepoCheckPaperBI.class);

private List<RepoCheckPaperDTO> listafila;
	
	@Autowired
	RepoCheckPaperDAO repoCheckPaperDAO;
		
	
	
	
	
	public List<RepoCheckPaperDTO> obtieneDatos( String ceco,int idCheclist) {
		
		try {
			listafila =  repoCheckPaperDAO.obtieneDatos(ceco,idCheclist);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			e.printStackTrace();
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<RepoCheckPaperDTO> obtieneInfo(int idUsuario,int idCheclist) {

		try {
			listafila = repoCheckPaperDAO.obtieneInfo(idUsuario,idCheclist);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}

public List<RepoCheckPaperDTO> obtieneDatosPreguntas( String bitacora,int idCheclist) {
		
		try {
			listafila =  repoCheckPaperDAO.obtieneDatosPreguntas(bitacora,idCheclist);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<RepoCheckPaperDTO> obtieneInfoPreguntas(int bitacora,int idCheclist) {

		try {
			listafila = repoCheckPaperDAO.obtieneInfoPreguntas(bitacora,idCheclist);
		} catch (Exception e) {
			logger.info("No fue posible obtener datos");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}


	
	

}