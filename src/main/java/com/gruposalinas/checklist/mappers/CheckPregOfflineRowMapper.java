package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;

public class CheckPregOfflineRowMapper implements RowMapper<ChecklistPreguntaDTO> {

	public ChecklistPreguntaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ChecklistPreguntaDTO checklistPreguntaDTO = new ChecklistPreguntaDTO();
		
		checklistPreguntaDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		checklistPreguntaDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		checklistPreguntaDTO.setPregunta(rs.getString("FCDESCRIPCION"));
		checklistPreguntaDTO.setIdModulo(rs.getInt("FIID_MODULO"));
		checklistPreguntaDTO.setIdTipoPregunta(rs.getInt("FIID_TIPO_PREG"));
		checklistPreguntaDTO.setOrdenPregunta(rs.getInt("FIORDEN_CHECK"));
		checklistPreguntaDTO.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
		checklistPreguntaDTO.setActivo(rs.getInt("FIESTATUS"));
		checklistPreguntaDTO.setDetalle(rs.getString("FCDETALLE"));
		checklistPreguntaDTO.setCritica(rs.getInt("FICRITICA"));
		checklistPreguntaDTO.setSerie(rs.getInt("FINUMSERIE"));
		try {checklistPreguntaDTO.setIdZona(rs.getInt("FIIDZONA"));}catch(Exception e){checklistPreguntaDTO.setIdZona(0);}
        
		//checklistPreguntaDTO.setSerie(0);
		checklistPreguntaDTO.setSla(0);
		return checklistPreguntaDTO;
	}

}