package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.FragmentMenuDTO;

public class FragmentRowMapper implements RowMapper<FragmentMenuDTO> {

	public FragmentMenuDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		FragmentMenuDTO FragmentMenuDTO = new FragmentMenuDTO();
		
		FragmentMenuDTO.setIdFragment(rs.getInt("FIID_FRAGM"));
		FragmentMenuDTO.setNombreFragment(rs.getString("FCNOMBREFRAG"));
	    FragmentMenuDTO.setNumFragment(rs.getInt("FIID_NUMFRAG"));
		FragmentMenuDTO.setParametro(rs.getString("FCPARAMETRO"));
		FragmentMenuDTO.setPeriodo(rs.getString("FCPERIODO"));
		
	   
		return FragmentMenuDTO;
	}

}


