/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ComentariosRepAsgDTO;
import com.gruposalinas.checklist.mappers.ComentariosRepAsgRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author leodan1991
 */
public class ComentariosRepAsgDAOImpl extends DefaultDAO implements ComentariosRepAsgDAO {

    private DefaultJdbcCall jdbcObtieneComentarios;
    private DefaultJdbcCall jdbcInsertaComentario;
    private DefaultJdbcCall jdbcModificaComentario;
    private DefaultJdbcCall jdbcEliminaComentario;

    private static Logger logger = LogManager.getLogger(ComentariosRepAsgDAO.class);

    public void init() {

        jdbcObtieneComentarios = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMCOMENTARIOS")
                .withProcedureName("SPGETCOMENTARIOS")
                .returningResultSet("PA_CONSULTA", new ComentariosRepAsgRowMapper());

        jdbcInsertaComentario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMCOMENTARIOS")
                .withProcedureName("SPINSCOMENTARIOS");

        jdbcModificaComentario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMCOMENTARIOS")
                .withProcedureName("SPACTCOMENTARIOS");

        jdbcEliminaComentario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADMCOMENTARIOS")
                .withProcedureName("SPDELCOMENTARIOS");

    }

    @Override
    public List<ComentariosRepAsgDTO> getComentarios(String ceco, String fecha, Integer anio, Integer mes) throws Exception {

        List<ComentariosRepAsgDTO> listaComentarios = null;
        Map<String, Object> out = null;

        int ejecuccion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_CECO", ceco)
                .addValue("PA_FDFECHA", fecha)
                .addValue("PA_FIANIO", anio)
                .addValue("PA_FIMES", mes);

        out = jdbcObtieneComentarios.execute(in);

        logger.info("Funcion ejecutada: {franquicia.PADMCOMENTARIOS.SPGETCOMENTARIOS}");

        if (out != null) {
            listaComentarios = (List<ComentariosRepAsgDTO>) out.get("PA_CONSULTA");
        }

        return listaComentarios;
    }

    @Override
    public Boolean setComentarios(String ceco, String fecha, String comentario) throws Exception {

        Boolean resultado = null;
        Map<String, Object> out = null;

        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_CECO", ceco)
                .addValue("PA_FDFECHA", fecha)
                .addValue("PA_FCCOMENTARIO", comentario);

        out = jdbcInsertaComentario.execute(in);

        BigDecimal resultadoBD = (BigDecimal) out.get("PA_RESEJECUCION");
        ejecucion = resultadoBD.intValue();

        logger.info("Funcion ejecutada: {franquicia.PADMCOMENTARIOS.SPINSCOMENTARIOS}");

        if (ejecucion == 1) {
            resultado = true;
        } else {
            resultado = false;
        }

        return resultado;
    }

    @Override
    public Boolean updateComentarios(String ceco, String fecha, String comentario) throws Exception {

        Boolean resultado = null;
        Map<String, Object> out = null;

        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_CECO", ceco)
                .addValue("PA_FDFECHA", fecha)
                .addValue("PA_FCCOMENTARIO", comentario);

        out = jdbcModificaComentario.execute(in);

        BigDecimal resultadoBD = (BigDecimal) out.get("PA_RESEJECUCION");
        ejecucion = resultadoBD.intValue();

        logger.info("Funcion ejecutada: {franquicia.PADMCOMENTARIOS.SPINSCOMENTARIOS}");

        if (ejecucion == 1) {
            resultado = true;
        } else {
            resultado = false;
        }

        return resultado;
    }

    @Override
    public Boolean deleteComentarios(String ceco, String fecha, String comentario) throws Exception {

        Boolean resultado = null;
        Map<String, Object> out = null;

        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCID_CECO", ceco)
                .addValue("PA_FDFECHA", fecha)
                .addValue("PA_FCCOMENTARIO", comentario);

        out = jdbcEliminaComentario.execute(in);

        BigDecimal resultadoBD = (BigDecimal) out.get("PA_RESEJECUCION");
        ejecucion = resultadoBD.intValue();

        logger.info("Funcion ejecutada: {franquicia.PADMCOMENTARIOS.SPINSCOMENTARIOS}");

        if (ejecucion == 1) {
            resultado = true;
        } else {
            resultado = false;
        }

        return resultado;
    }
}
