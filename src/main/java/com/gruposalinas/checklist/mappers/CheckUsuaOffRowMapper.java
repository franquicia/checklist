package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistUsuarioDTO;

public class CheckUsuaOffRowMapper implements RowMapper<ChecklistUsuarioDTO> {

	public ChecklistUsuarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistUsuarioDTO checklistUsuarioDTO = new ChecklistUsuarioDTO();
		
		checklistUsuarioDTO.setIdCheckUsuario(rs.getInt("FIID_CHECK_USUA"));
		checklistUsuarioDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
		checklistUsuarioDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		checklistUsuarioDTO.setIdCeco(rs.getInt("FCID_CECO"));
		checklistUsuarioDTO.setNombreCeco(rs.getString("FCNOMBRE"));
		checklistUsuarioDTO.setZona(rs.getString("FCZONA"));
		checklistUsuarioDTO.setLatitud(rs.getString("FCLATITUD"));
		checklistUsuarioDTO.setLongitud(rs.getString("FCLONGITUD"));
		checklistUsuarioDTO.setUltimaVisita(rs.getString("ULTIMAVISITA"));
		
		return checklistUsuarioDTO;
	}

}
