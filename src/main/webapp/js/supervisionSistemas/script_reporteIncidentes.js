$(document).ready(function() {
	document.getElementById("tipoModal").style.display = 'block';
	cargaIncidentes("","","");	
	cargaIncidentesDetalle("","","");
});


function cargaIncidentes(fechaInicio, fechaFin , motivo) {
	
	document.getElementById("grafica").innerHTML="";
	document.getElementById("listaSis").innerHTML="";
	$.ajax({
		type : "GET",
		url : '../incidentes/getReporteMotivos.json?fechaInicio='+fechaInicio+'&fechaFin='+fechaFin+'&motivo='+motivo,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {

			if(json.totalesMotivo != null && json.totalesMotivo.length>0){
				
				$("#listaSis").append("" +
						"<tr style='height: 30%;'>"+
							"<td style='width: 50%; background: white;'>" +
									"<a style='font-size: 16px; font-weight: 700; color: black;'>Sistemas</a>"+
							"</td>"+
							"<td style='width: 50%; background: white;'>" +
								"<a	style='font-size: 16px; font-weight: 700; color: black;'>Cantidad</a>"+
							"</td>"+
						"</tr>");
				
				
				var i = 0;
				var com=0;
				var barra=0;
				while (json.totalesMotivo[i] != null) {
		
					$("#listaSis").append("" +
						"<tr style='height: "+(70/json.totalesMotivo.length)+"%;' >" +
							"<td style='width: 50%; background: white;'><a style='font-size: 16px; font-weight: 100; color: #666666;'>"+json.totalesMotivo[i].motivoEstado+"</a></td>" +
							"<td style='width: 50%; background: white;'><a style='font-size: 16px; font-weight: 700; color: "+json.totalesMotivo[i].color+";'>"+json.totalesMotivo[i].totalIncidentes+"</a></td>" +
						"</tr>");
					
					
					barra = (json.totalesMotivo[i].porcentaje*360)/100;
					if(barra>180){
						$("#grafica").append("" +
							"<div style='transform: rotate("+com+"deg);' class='recorte'>" +
								"<div style='background-color: "+json.totalesMotivo[i].color+"; transform: rotate(180deg);' class='barra'></div>" +
							"</div>");	
						com=com+180;
						
						$("#grafica").append("" +
								"<div style='transform: rotate("+com+"deg);' class='recorte'>" +
									"<div style='background-color: "+json.totalesMotivo[i].color+"; transform: rotate("+(barra-180)+"deg);' class='barra'></div>" +
								"</div>");	
						com=com+(barra-180);
						
					}else{
						$("#grafica").append("" +
							"<div style='transform: rotate("+com+"deg);' class='recorte'>" +
								"<div style='background-color: "+json.totalesMotivo[i].color+"; transform: rotate("+barra+"deg);' class='barra'></div>" +
							"</div>");	
						com=com+barra;
					}
							
					i++;
				}
				
				$("#grafica").append("<p class='center'>	Total<br> <span class='font-color'>"+(json.totalesMotivo[0].totalIncidentesGeneral)+"</span></p>");
				
			}else{
				
				$("#listaSis").append("" +
						"<tr style='height: 30%;'>"+
							"<td style='width: 50%; background: white;'>" +
									"<a style='font-size: 16px; font-weight: 700; color: black;'>Sistemas</a>"+
							"</td>"+
							"<td style='width: 50%; background: white;'>" +
								"<a	style='font-size: 16px; font-weight: 700; color: black;'>Cantidad</a>"+
							"</td>"+
						"</tr>" +
						"<tr style='height: 70%;' >" +
							"<td style='width: 50%; background: white;'><a style='font-size: 16px; font-weight: 100; color: #666666;'>Sin Dato</a></td>" +
							"<td style='width: 50%; background: white;'><a style='font-size: 16px; font-weight: 700; color: #666666;'>-</a></td>" +
						"</tr>");
				
				
				$("#grafica").append("" +
						"<div style='transform: rotate(0deg);' class='recorte'>" +
							"<div style='background-color: #F2f2f2; transform: rotate(180deg);' class='barra'></div>" +
						"</div>"+
						"<div style='transform: rotate(180deg);' class='recorte'>" +
							"<div style='background-color: #F2F2F2; transform: rotate(180deg);' class='barra'></div>" +
						"</div>" +
					"<p class='center'>	Total<br> <span class='font-color'>-</span></p>");	
			}
			
			
			
		},
		error : function() {
			
		}
	});
}

function cargaIncidentesDetalle(fechaInicio, fechaFin , motivo) {

	document.getElementById("cuerpoTabla").innerHTML="";
	$.ajax({
		type : "GET",
		url : '../incidentes/getInformesIncidentes.json?estatus=&motivo='+motivo+'&fechaInicio='+fechaInicio+'&fechaFin='+fechaFin+'&director=',
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		paramName : "tagName",
		delimiter : ",",
		success : function(json) {

			if(json.listaIncidentes != null && json.listaIncidentes.length>0){
				
				var i=0;
				while (json.listaIncidentes[i] != null) {
			
					$("#cuerpoTabla").append("" +
						"<tr>" +
							"<td style='width:13%;'>"+json.listaIncidentes[i].estatus+"</td>" +
							"<td style='width:20%;'>"+json.listaIncidentes[i].fechaApertura+"</td>" +
							"<td style='width:17%;'><a href='#'>"+json.listaIncidentes[i].folio+"</a></td>" +
							"<td style='width:15%;'>"+json.listaIncidentes[i].mensajeError+"</td>" +
							"<td style='width:15%;'>"+json.listaIncidentes[i].motivoEstado+"</td>" +
							"<td style='width:20%;'>"+json.listaIncidentes[i].programador+"</td>" +
						"</tr>");	
					i++;
				}
			}else{
				$("#cuerpoTabla").append("" +
					"<tr>" +
						"<td style='width:13%;'>-</td>" +
						"<td style='width:20%;'>-</td>" +
						"<td style='width:17%;'>-</td>" +
						"<td style='width:15%;'>-</td>" +
						"<td style='width:15%;'>-</td>" +
						"<td style='width:20%;'>-</td>" +
					"</tr>");	
			}
			document.getElementById("tipoModal").style.display = 'none';
		},
		error : function() {
			document.getElementById("tipoModal").style.display = 'none';
		}
	});
}


function busqueda(){

	var fInicio = document.getElementById("datepicker5").value;
	var fFin = document.getElementById("datepicker6").value;
	var motivo = document.getElementById("selEstatus").value;
	
	if((fInicio == "" && fFin == "" && motivo == "" ) || (fInicio != "" && fFin != "")){
		
		document.getElementById("tipoModal").style.display = 'block';
		
		var subTit = document.getElementById("selEstatus").options[document.getElementById("selEstatus").selectedIndex].text;
		if(subTit == "Elige Estatus")
			document.getElementById("tituloFiltro").innerHTML="Motivos de Estatus";
		else
			document.getElementById("tituloFiltro").innerHTML=subTit;
		
		if(fInicio != ""){
			fInicio+= " 00:00:00";
		}
		if(fFin != ""){
			fFin+= " 23:59:59";
		}
		cargaIncidentes(fInicio, fFin , motivo)
		cargaIncidentesDetalle(fInicio, fFin , motivo);
	}else{
		alert('Coloca la fecha correctamente');
	}
	
}

