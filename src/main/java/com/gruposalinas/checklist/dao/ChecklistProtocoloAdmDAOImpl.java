package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.mappers.ChecklistProtocoloRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ChecklistProtocoloAdmDAOImpl extends DefaultDAO implements ChecklistProtocoloAdmDAO {

    private static final Logger logger = LogManager.getLogger(ChecklistDAOImpl.class);

    // checklist con calificacion ponderacion
    private DefaultJdbcCall jdbcBuscaChecklist;
    private DefaultJdbcCall jdbcInsertaChecklistCom;

    private DefaultJdbcCall jdbcActualizaChecklistCom;

    /*
	private DefaultJdbcCall jdbcEliminaChecklist;
     */

    public void init() {

        jdbcBuscaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_CHECKVAL")
                .withProcedureName("SP_SEL_CHECK")
                .returningResultSet("RCL_CHECK", new ChecklistProtocoloRowMapper());

        jdbcInsertaChecklistCom = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_CHECKVAL")
                .withProcedureName("SP_INS_CHECKCOM");

        jdbcActualizaChecklistCom = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_CHECKVAL")
                .withProcedureName("SP_ACT_CHECKCOM");
        /*

		jdbcEliminaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
				.withSchemaName("FRANQUICIA")
				.withCatalogName("PAADM_CHECK")
				.withProcedureName("SP_DEL_CHECK");
         */
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistProtocoloDTO> getChecklistProtocolo(ChecklistProtocoloDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ChecklistProtocoloDTO> listaChecklist = null;

        @SuppressWarnings("unused")
        String idChecklist = null;
        if (bean.getIdChecklist() != 0) {
            idChecklist = "" + bean.getIdChecklist();
        }
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECK", bean.getIdChecklist());

        out = jdbcBuscaChecklist.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADM_CHECK.SP_SEL_CHECK}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listaChecklist = (List<ChecklistProtocoloDTO>) out.get("RCL_CHECK");

        if (error == 1) {
            logger.info("Algo ocurrió al consultar el Checklist");
            return null;
        } else {
            return listaChecklist;
        }
    }

    public int insertaChecklistCom(ChecklistProtocoloDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        int idChecklist = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_TIPO_CHECK", bean.getIdTipoChecklist())
                .addValue("PA_FCNOMBRE", bean.getNombreCheck())
                .addValue("PA_IDHORARIO", bean.getIdHorario())
                .addValue("PA_VIGENTE", bean.getVigente())
                .addValue("PA_FECHA_I", bean.getFecha_inicio())
                .addValue("PA_FECHA_FIN", bean.getFecha_fin())
                .addValue("PA_FIID_ESTADO", bean.getIdEstado())
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_DIA", bean.getDia())
                .addValue("PA_PERIODO", bean.getPeriodo())
                .addValue("PA_VERSION", bean.getVersion())
                .addValue("PA_ORDEN_G", bean.getOrdenGrupo())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_VALPOND", bean.getPonderacionTot())
                .addValue("PA_CLASIFICA", bean.getClasifica())
                .addValue("PA_PROTOCOLO", bean.getIdProtocolo())
                .addValue("PA_NEGO", bean.getNegocio());

        out = jdbcInsertaChecklistCom.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADM_CHECK.SP_INS_CHECKCOM}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal idreturn = (BigDecimal) out.get("PA_IDCHECKLIST");
        idChecklist = idreturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar Checklist");
            return idChecklist;
        } else {
            return idChecklist;
        }

    }

    public boolean actualizaChecklistCom(ChecklistProtocoloDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_TIPO_CHECK", bean.getIdTipoChecklist())
                .addValue("PA_FCNOMBRE", bean.getNombreCheck())
                .addValue("PA_IDHORARIO", bean.getIdHorario())
                .addValue("PA_VIGENTE", bean.getVigente())
                .addValue("PA_FECHA_I", bean.getFecha_inicio())
                .addValue("PA_FECHA_FIN", bean.getFecha_fin())
                .addValue("PA_FIID_ESTADO", bean.getIdEstado())
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_DIA", bean.getDia())
                .addValue("PA_PERIODO", bean.getPeriodo())
                .addValue("PA_VERSION", bean.getVersion())
                .addValue("PA_ORDEN_G", bean.getOrdenGrupo())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_VALPOND", bean.getPonderacionTot())
                .addValue("PA_CLASIFICA", bean.getClasifica())
                .addValue("PA_PROTOCOLO", bean.getIdProtocolo())
                .addValue("PA_FIID_CHECK", bean.getIdChecklist());

        out = jdbcActualizaChecklistCom.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PA_ADM_CHECK.SP_ACT_CHECKCOM}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Checklist");
        } else {
            return true;
        }

        return false;
    }

    /*
	public boolean eliminaChecklist(int idCheckList) throws Exception {

		Map<String, Object> out = null;
		int error = 0;

		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("PA_FIID_CHECK", idCheckList);

		out = jdbcEliminaChecklist.execute(in);

		logger.info("Funci�n ejecutada:{checklist.PA_ADM_CHECK.SP_DEL_CHECK}");

		BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
		error = errorReturn.intValue();

		if(error == 1){
			logger.info("Algo ocurrió al borrar Checklist el id: " + idCheckList);
		}else{
		   return true;
		}

		return false;
	}
     */
    @Override
    public boolean eliminaChecklist(ChecklistProtocoloDTO bean) throws Exception {
        // TODO Auto-generated method stub
        return false;
    }
}
