package com.gruposalinas.checklist.business;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gruposalinas.checklist.dao.ChecklistModificacionesDAO;
import com.gruposalinas.checklist.domain.ChecklistActualDTO;
import com.gruposalinas.checklist.domain.ChecklistModificacionesDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class ChecklistModificacionesBI {

	private static Logger logger = LogManager.getLogger(ChecklistModificacionesBI.class);
	
	@Autowired
	ChecklistModificacionesDAO checklistModificacionesDAO;
	
	public String obtieneChecklistActual(int idChecklist){
		
		List<ChecklistActualDTO> checklistActual = null;
		JsonObject data = new JsonObject();
		JsonObject general = new JsonObject();

		
		try {
			checklistActual = checklistModificacionesDAO.obtieneChecklistActual(idChecklist);
			
			// INICIA ARMADO DE JSON
			JsonArray arrayPregunta = new JsonArray();
			
			Iterator<ChecklistActualDTO> it = checklistActual.iterator();
			int lastQuestion = 0;
			String last = "";
			int i = 0;
			int j = 1;
			while (it.hasNext()) {
				JsonObject respuesta = new JsonObject();
				
				ChecklistActualDTO o = it.next();
				if (o.getIdPregunta() != lastQuestion){
					
					if (o.getIdPregunta() != lastQuestion & i > 0){
							
							JsonObject pregAux = new JsonObject();
							pregAux.addProperty("pregunta", last);
							pregAux.add("respuestas", arrayPregunta);
							general.add("respuesta"+j, pregAux);
							
							arrayPregunta = new JsonArray();
							lastQuestion = o.getIdPregunta();
							last = o.getPregunta();
							j++;
					}
					
					respuesta.addProperty("Respuesta", o.getPosibleRespuesta());
					respuesta.addProperty("Evidencia", o.getEstatusEvidencia());
					respuesta.addProperty("TipoEvidencia", o.getEstatusEvidencia());
					respuesta.addProperty("EvidenciaObligatoria", o.getEvidenciaObligatoria());
					respuesta.addProperty("Observaciones", o.getRequiereObsv());
					respuesta.addProperty("Accion", o.getRequiereAccion());
					respuesta.addProperty("PreguntaSiguiente", o.getSiguientePregunta());
					respuesta.addProperty("EtiquetaEvidencia", o.getEtiquetaEvidencia());
					arrayPregunta.add(respuesta);
					lastQuestion = o.getIdPregunta();
					last = o.getPregunta();
				} else {
					respuesta.addProperty("Respuesta", o.getPosibleRespuesta());
					respuesta.addProperty("Evidencia", o.getEstatusEvidencia());
					respuesta.addProperty("TipoEvidencia", o.getEstatusEvidencia());
					respuesta.addProperty("EvidenciaObligatoria", o.getEvidenciaObligatoria());
					respuesta.addProperty("Observaciones", o.getRequiereObsv());
					respuesta.addProperty("Accion", o.getRequiereAccion());
					respuesta.addProperty("PreguntaSiguiente", o.getSiguientePregunta());
					respuesta.addProperty("EtiquetaEvidencia", o.getEtiquetaEvidencia());
					
					arrayPregunta.add(respuesta);
					lastQuestion = o.getIdPregunta();
					last = o.getPregunta();
				}
				
				if (it.hasNext() == false){
					JsonObject pregAux = new JsonObject();
					pregAux.addProperty("pregunta", last);
					pregAux.add("respuestas", arrayPregunta);
					general.add("respuesta"+j, pregAux);
				}

				i++;
			}
			data.add("data", general);
			//FIN ARMADO JSON
			
		} catch (Exception e) {
			logger.info("No fue posible obtener el checklist actual con id(idChecklist) ");
				
		}
		
		return data.toString();
		
	}
	
	public String obtieneChecklist(int idChecklist) throws Exception{
		JsonObject data = new JsonObject();
		JsonObject general = new JsonObject();
		
		Map<String, Object> datosChecks = checklistModificacionesDAO.obtieneChecklist(idChecklist);
		
		@SuppressWarnings("unchecked")
		List<ChecklistModificacionesDTO> checklistModificado = (List<ChecklistModificacionesDTO>) datosChecks.get("checklistModificaciones");
		@SuppressWarnings("unchecked")
		List<ChecklistActualDTO> checklistActual =  (List<ChecklistActualDTO>) datosChecks.get("checklistActual");

		try {
			// INICIA ARMADO DE JSON
			JsonArray arrayPregunta = new JsonArray();
			
			Iterator<ChecklistActualDTO> it = checklistActual.iterator();
			int lastQuestion = 0;
			String last = "";
			int i = 0;
			int j = 1;
			int checkPreg = 0;
			while (it.hasNext()) {
				JsonObject respuesta = new JsonObject();
				
				ChecklistActualDTO o = it.next();
				if (o.getIdPregunta() != lastQuestion){
					
					if (o.getIdPregunta() != lastQuestion & i > 0){
							
							JsonObject pregAux = new JsonObject();
							pregAux.addProperty("pregunta", last);
							pregAux.addProperty("ordenCheckPreg", checkPreg);
							pregAux.add("respuestas", arrayPregunta);
							general.add("respuesta"+j, pregAux);
							
							arrayPregunta = new JsonArray();
							lastQuestion = o.getIdPregunta();
							last = o.getPregunta();
							checkPreg = o.getOrdenCheck();
							j++;
					}
					
					respuesta.addProperty("Respuesta", o.getPosibleRespuesta());
					respuesta.addProperty("Evidencia", o.getEstatusEvidencia());
					respuesta.addProperty("TipoEvidencia", o.getEstatusEvidencia());
					respuesta.addProperty("EvidenciaObligatoria", o.getEvidenciaObligatoria());
					respuesta.addProperty("Observaciones", o.getRequiereObsv());
					respuesta.addProperty("Accion", o.getRequiereAccion());
					respuesta.addProperty("PreguntaSiguiente", o.getSiguientePregunta());
					respuesta.addProperty("EtiquetaEvidencia", o.getEtiquetaEvidencia());
					arrayPregunta.add(respuesta);
					lastQuestion = o.getIdPregunta();
					checkPreg = o.getOrdenCheck();
					last = o.getPregunta();
				} else {
					respuesta.addProperty("Respuesta", o.getPosibleRespuesta());
					respuesta.addProperty("Evidencia", o.getEstatusEvidencia());
					respuesta.addProperty("TipoEvidencia", o.getEstatusEvidencia());
					respuesta.addProperty("EvidenciaObligatoria", o.getEvidenciaObligatoria());
					respuesta.addProperty("Observaciones", o.getRequiereObsv());
					respuesta.addProperty("Accion", o.getRequiereAccion());
					respuesta.addProperty("PreguntaSiguiente", o.getSiguientePregunta());
					respuesta.addProperty("EtiquetaEvidencia", o.getEtiquetaEvidencia());
					
					arrayPregunta.add(respuesta);
					lastQuestion = o.getIdPregunta();
					checkPreg = o.getOrdenCheck();
					last = o.getPregunta();
				}
				
				if (it.hasNext() == false){
					JsonObject pregAux = new JsonObject();
					pregAux.addProperty("pregunta", last);
					pregAux.addProperty("ordenCheckPreg", checkPreg);
					pregAux.add("respuestas", arrayPregunta);
					general.add("respuesta"+j, pregAux);
					checkPreg = o.getOrdenCheck();
				}

				i++;
			}
			data.add("anterior", general);
			//FIN ARMADO JSON
				
		} catch (Exception e) {
			logger.info("No fue posible obtener las modificaciones del checklist con id("+idChecklist+") ");
				
		}
		
		JsonObject generalNuevo = new JsonObject();		
		try {
			// INICIA ARMADO DE JSON
			JsonArray arrayPregunta = new JsonArray();
			
			Iterator<ChecklistModificacionesDTO> it = checklistModificado.iterator();
			int lastQuestion = 0;
			String last = "";
			int i = 0;
			int j = 1;
			int checkPreg = 0;
			while (it.hasNext()) {
				JsonObject respuesta = new JsonObject();
				
				ChecklistModificacionesDTO o = it.next();
				if (o.getIdPregunta() != lastQuestion){
					
					if (o.getIdPregunta() != lastQuestion & i > 0){
							
							JsonObject pregAux = new JsonObject();
							pregAux.addProperty("pregunta", last);
							pregAux.addProperty("ordenCheckPreg", checkPreg);
							pregAux.add("respuestas", arrayPregunta);
							generalNuevo.add("respuesta"+j, pregAux);
							
							arrayPregunta = new JsonArray();
							lastQuestion = o.getIdPregunta();
							last = o.getPregunta();
							checkPreg = o.getOrdenCheck();
							j++;
					}
					
					respuesta.addProperty("Respuesta", o.getPosibleRespuesta());
					respuesta.addProperty("Evidencia", o.getEstatusEvidencia());
					respuesta.addProperty("TipoEvidencia", o.getEstatusEvidencia());
					respuesta.addProperty("EvidenciaObligatoria", o.getEvidenciaObligatoria());
					respuesta.addProperty("Observaciones", o.getRequiereObsv());
					respuesta.addProperty("Accion", o.getRequiereAccion());
					respuesta.addProperty("PreguntaSiguiente", o.getSiguientePregunta());
					respuesta.addProperty("EtiquetaEvidencia", o.getEtiquetaEvidencia());
					
					respuesta.addProperty("ModificacionCheck", o.getModificacionCheck());
					respuesta.addProperty("ModificacionPreg", o.getModificacionPreg());
					respuesta.addProperty("ModificacionModulo", o.getModificacionModulo());
					respuesta.addProperty("ModificacionCheckPreg", o.getModificacionCheckPreg());
					respuesta.addProperty("ModificacionArbol", o.getModificacionArbol());
					
					arrayPregunta.add(respuesta);
					lastQuestion = o.getIdPregunta();
					last = o.getPregunta();
					checkPreg = o.getOrdenCheck();
				} else {
					respuesta.addProperty("Respuesta", o.getPosibleRespuesta());
					respuesta.addProperty("Evidencia", o.getEstatusEvidencia());
					respuesta.addProperty("TipoEvidencia", o.getEstatusEvidencia());
					respuesta.addProperty("EvidenciaObligatoria", o.getEvidenciaObligatoria());
					respuesta.addProperty("Observaciones", o.getRequiereObsv());
					respuesta.addProperty("Accion", o.getRequiereAccion());
					respuesta.addProperty("PreguntaSiguiente", o.getSiguientePregunta());
					respuesta.addProperty("EtiquetaEvidencia", o.getEtiquetaEvidencia());
					
					//Se agrega el tipo de modificacion realizado
					respuesta.addProperty("ModificacionCheck", o.getModificacionCheck());
					respuesta.addProperty("ModificacionPreg", o.getModificacionPreg());
					respuesta.addProperty("ModificacionModulo", o.getModificacionModulo());
					respuesta.addProperty("ModificacionCheckPreg", o.getModificacionCheckPreg());
					respuesta.addProperty("ModificacionArbol", o.getModificacionArbol());

					arrayPregunta.add(respuesta);
					lastQuestion = o.getIdPregunta();
					last = o.getPregunta();
					checkPreg = o.getOrdenCheck();

				}
				
				if (it.hasNext() == false){
					JsonObject pregAux = new JsonObject();
					pregAux.addProperty("pregunta", last);
					pregAux.addProperty("ordenCheckPreg", checkPreg);
					pregAux.add("respuestas", arrayPregunta);
					generalNuevo.add("respuesta"+j, pregAux);
				}

				i++;
			}
			data.add("nuevo", generalNuevo);
			//FIN ARMADO JSON
				
		} catch (Exception e) {
			logger.info("No fue posible obtener las modificaciones del checklist con id("+idChecklist+") ");
				
		}
		
		//Datos generales de checklist anterior
				try {
					// INICIA ARMADO DE JSON
					
					Iterator<ChecklistActualDTO> it = checklistActual.iterator();
					ChecklistActualDTO o = it.next();
					JsonObject respuesta = new JsonObject();
					respuesta.addProperty("idChecklist", o.getIdChecklist());
					respuesta.addProperty("nombreCheck", o.getNombreCheck());
					respuesta.addProperty("idHorario", o.getIdHorario());
					respuesta.addProperty("idTipoChecklist", o.getIdTipoChecklist());
					respuesta.addProperty("fechaInicio", o.getFechaInicioCheck());
					respuesta.addProperty("fechaFin", o.getFechaFinCheck());

					data.add("datosGeneralesActual", respuesta);

					//FIN ARMADO JSON
						
				} catch (Exception e) {
					logger.info("No fue posible obtener las modificaciones del checklist con id("+idChecklist+") ");
						
				}
				
		
		
		//Datos generales de checklist nuevo
		try {
			// INICIA ARMADO DE JSON
			
			Iterator<ChecklistModificacionesDTO> it = checklistModificado.iterator();
			ChecklistModificacionesDTO o = it.next();
			JsonObject respuesta = new JsonObject();
			respuesta.addProperty("idChecklist", o.getIdChecklist());
			respuesta.addProperty("nombreCheck", o.getNombreCheck());
			respuesta.addProperty("idHorario", o.getIdHorario());
			respuesta.addProperty("idTipoChecklist", o.getIdTipoChecklist());
			respuesta.addProperty("fechaInicio", o.getFechaInicioCheck());
			respuesta.addProperty("fechaFin", o.getFechaFinCheck());

			data.add("datosGeneralesNuevo", respuesta);

			//FIN ARMADO JSON
				
		} catch (Exception e) {
			logger.info("No fue posible obtener las modificaciones del checklist con id("+idChecklist+") ");
				
		}
		
		return data.toString();
	}
	
	public boolean autorizaChecklist(int autoriza, int idChecklist){
		boolean respuesta = false;
		
		try {
			respuesta = checklistModificacionesDAO.autorizaChecklist(autoriza, idChecklist);
		} catch (Exception e) {
			logger.info("No fue posible autorizar las modificaciones del checklist con id("+idChecklist+") ");
			
		}
	return respuesta;
	}
}
