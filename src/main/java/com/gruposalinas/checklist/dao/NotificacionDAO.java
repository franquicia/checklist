package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.MovilInfoDTO;


public interface NotificacionDAO {

	public Map<String, Object> notificacionPorcentaje(int idUsuario) throws Exception;
	
	public List<MovilInfoDTO> obtieneUsuariosNoti(String so) throws Exception;
}
