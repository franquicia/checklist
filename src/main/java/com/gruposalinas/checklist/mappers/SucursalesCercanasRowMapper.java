package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.SucursalesCercanasDTO;

public class SucursalesCercanasRowMapper implements RowMapper<SucursalesCercanasDTO> {

	@Override
	public SucursalesCercanasDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		SucursalesCercanasDTO sucursalesCercanasDTO = new SucursalesCercanasDTO();
		
		sucursalesCercanasDTO.setCeco(rs.getString("FCID_CECO"));
		sucursalesCercanasDTO.setNombreCeco(rs.getString("FCNOMBRE"));
		sucursalesCercanasDTO.setNuSucursal(rs.getString("FIIDNU_SUCURSAL"));
		sucursalesCercanasDTO.setLatitud(rs.getString("FCLATITUD"));
		sucursalesCercanasDTO.setLongitud(rs.getString("FCLONGITUD"));
		
		return sucursalesCercanasDTO;
	}
	
	

}
