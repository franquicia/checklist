/*$(document).ready(function() {
    $('#tabla').DataTable( {
    	 "columns": [
    		    { "type": "num" },
    		    { "type": "string" },
    		    { "type": "string" },
    		    { "type": "string" }
    		  ],
        "ordering": true
    } );
});*/


$(document).ready(function() {
jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "formatted-num-pre": function ( a ) {
        a = (a === "-" || a === "") ? 0 : a.replace( /[^\d\-\.]/g, "" );
        return parseFloat( a );
    },
 
    "formatted-num-asc": function ( a, b ) {
        return a - b;
    },
 
    "formatted-num-desc": function ( a, b ) {
        return b - a;
    }
} );

$('#tabla').DataTable( {
	
     columnDefs: [
       { type: 'formatted-num', targets: 0 }
     ],
     stateSave: true
  } );
});