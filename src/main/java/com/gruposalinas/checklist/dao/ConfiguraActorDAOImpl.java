package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ConfiguraActorDTO;
import com.gruposalinas.checklist.mappers.ConfiguraActorRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ConfiguraActorDAOImpl extends DefaultDAO implements ConfiguraActorDAO {

    private static Logger logger = LogManager.getLogger(ConfiguraActorDAOImpl.class);

    DefaultJdbcCall jdbcInsertaConfActor;
    DefaultJdbcCall jdbcEliminaConfActor;
    DefaultJdbcCall jdbcObtieneConfActor;
    DefaultJdbcCall jbdcActualizaConfActor;

    public void init() {

        jdbcInsertaConfActor = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACONFIGACTOR")
                .withProcedureName("SP_INS_CONF");

        jdbcEliminaConfActor = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACONFIGACTOR")
                .withProcedureName("SP_DEL_CONF");

        jdbcObtieneConfActor = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACONFIGACTOR")
                .withProcedureName("SP_SEL_CONF")
                .returningResultSet("RCL_CONF", new ConfiguraActorRowMapper());

        jbdcActualizaConfActor = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACONFIGACTOR")
                .withProcedureName("SP_ACT_CONF");
    }

    @Override
    public int insertaConfActor(ConfiguraActorDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCONFACT", bean.getIdConfigActor())
                .addValue("PA_STATUS", bean.getStatusActuacion())
                .addValue("PA_ACCION", bean.getAccion())
                .addValue("PA_STATUSACCION", bean.getStatusAccion())
                .addValue("PA_BANDATENC", bean.getBanderaAtencion())
                .addValue("PA_OBS", bean.getObs())
                .addValue("PA_IDCONFIG", bean.getIdConfig())
                .addValue("PA_AUX", bean.getAux());

        out = jdbcInsertaConfActor.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PACONFIGACTOR.SP_INS_CONFHALLA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDCONFHALLA");
        idEmpFij = idTipoReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el CONFHALLA");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    @Override
    public boolean eliminaConfActor(String idConfig) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCONFHALLA", idConfig);

        out = jdbcEliminaConfActor.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PACONFIGACTOR.SP_DEL_CONFHALLA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al borrar el CONFHALLA(" + idConfig + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<ConfiguraActorDTO> obtieneDatosConfActor(String idConfig) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        List<ConfiguraActorDTO> lista = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCONF", idConfig);

        out = jdbcObtieneConfActor.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PACONFIGACTOR.SP_SEL_CONFHALLA}");
        //lleva el nombre del cursor del procedure
        lista = (List<ConfiguraActorDTO>) out.get("RCL_CONF");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el ConfigHALLA(" + idConfig + ")");
        }
        return lista;
    }

    @Override
    public boolean actualizaConfActor(ConfiguraActorDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCONFHALLA", bean.getIdHallazgoConf())
                .addValue("PA_IDCONFACT", bean.getIdConfigActor())
                .addValue("PA_STATUS", bean.getStatusActuacion())
                .addValue("PA_ACCION", bean.getAccion())
                .addValue("PA_STATUSACCION", bean.getStatusAccion())
                .addValue("PA_BANDATENC", bean.getBanderaAtencion())
                .addValue("PA_OBS", bean.getObs())
                .addValue("PA_IDCONFIG", bean.getIdConfig())
                .addValue("PA_AUX", bean.getAux());

        out = jbdcActualizaConfActor.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PACONFIGACTOR.SP_ACT_CONFHALLA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar Estado del  idCONFHALLA( " + bean.getIdHallazgoConf() + ")");
        } else {
            return true;
        }
        return false;
    }

}
