package com.gruposalinas.checklist.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.ChecklistHallazgosRepoDAO;
import com.gruposalinas.checklist.dao.HallazgosExpDAO;
import com.gruposalinas.checklist.domain.HallazgosEviExpDTO;
import com.gruposalinas.checklist.domain.HallazgosExpDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;

public class HallazgosExpBI {

	private static Logger logger = LogManager.getLogger(HallazgosExpBI.class);

private List<HallazgosExpDTO> listafila;
	
	@Autowired
	HallazgosExpDAO hallazgosExpDAO;
	
	@Autowired
	ChecklistHallazgosRepoDAO checklistHallazgosRepoDAO;
			
	
	public int inserta(HallazgosExpDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = hallazgosExpDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	

//elimina respuesta		
	public boolean eliminaResp(String idResp){
		boolean respuesta = false;
		
		try {
			respuesta = hallazgosExpDAO.eliminaResp(idResp);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	//elimina por bitacora gral
	public boolean eliminaBita(String bitGral){
		boolean respuesta = false;
		
		try {
			respuesta = hallazgosExpDAO.eliminaBita(bitGral);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	
	public List<HallazgosExpDTO> obtieneDatos( int bitGral) {
		
		try {
			listafila =  hallazgosExpDAO.obtieneDatos(bitGral);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<HallazgosExpDTO> obtieneInfo() {

		try {
			listafila =  hallazgosExpDAO.obtieneInfo();
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	


	//web
	
	public boolean actualiza(HallazgosExpDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = hallazgosExpDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	//movil
	public boolean actualizaMov(HallazgosExpDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = hallazgosExpDAO.actualizaMov(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public boolean actualizaResp(HallazgosExpDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = hallazgosExpDAO.actualizaResp(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	//carga hallazgos
	public boolean cargaHallazgos(int bitGral){
		boolean respuesta = false;
		
		try {
			respuesta = hallazgosExpDAO.cargaHallazgos( bitGral);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	//depura tabla
		public boolean EliminacargaHallazgos(){
			boolean respuesta = false;
			
			try {
				respuesta = hallazgosExpDAO.EliminacargaHallazgos();
			} catch (Exception e) {
				logger.info("No fue posible eliminar");
				UtilFRQ.printErrorLog(null, e);	
			}
			
			return respuesta;			
		}

		//carga hallazgos por bitacora
		public boolean cargaHallazgosxBita(int bitGral){
			boolean respuesta = false;
			
			try {
				respuesta = hallazgosExpDAO.cargaHallazgosxBita( bitGral);
			} catch (Exception e) {
				logger.info("No fue posible actualizar");
				UtilFRQ.printErrorLog(null, e);	
			}
			
			return respuesta;			
		}
		
	//INICIO Nuevo Proceso para la carga de hallazgos desde la primer caminata
		
	public boolean cargaHallazgosUpdate(int idBitacora) {

		/*EL ID RESPUESTA ES EL MISMO QUE EL ID HALLAZGO*/
		
		boolean respuesta = false;
		boolean respuestaPaso1  = false;
		boolean respuestaPaso2  = false;
		boolean respuestaPaso3   = false;
		boolean respuestaPaso4   = false;

		Map <String,Object> listaPregs = null;
		
		List<HallazgosExpDTO> listaHallazgosTabla = new ArrayList<HallazgosExpDTO>();
		List<HallazgosExpDTO> listaHallazgosCopiaRecorrido = new ArrayList<HallazgosExpDTO>();
		List<HallazgosExpDTO> listaHallazgosNuevoRecorrido = new ArrayList<HallazgosExpDTO>();


		//PASO 1 TABLA EXISTENTE contre COPIA NUEVO RECORRIDO
		try {
			
			listaPregs = checklistHallazgosRepoDAO.cursores(idBitacora+"");
			
			// Obtengo los hallazgos de la tabla
			listaHallazgosTabla = (List<HallazgosExpDTO>) listaPregs.get("hallazgostabla");
			// Obtengo la copia de hallazgos del nuevo recorrido
			listaHallazgosCopiaRecorrido = (List<HallazgosExpDTO>) listaPregs.get("listaReconue");
			// Obtengo los hallazgos del nuevo recorrido
			listaHallazgosNuevoRecorrido = (List<HallazgosExpDTO>) listaPregs.get("hallazgosultreco");
			
			
			if (listaHallazgosTabla != null && listaHallazgosTabla.size() > 0) {
				//Estaba en NO y Pasa a SI (Se resuelve)
				respuestaPaso1 = actualizaHallazgosNoASi(listaHallazgosTabla, listaHallazgosCopiaRecorrido);
			} 
		} catch (Exception e) {
			logger.info(e);
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos PASO1 y actualizaHallazgosNoASi del CECO: ");
		}
		
		//PASO 2 TABLA EXISTENTE contra COPIA NUEVO RECORRIDO
		try {
			listaPregs = checklistHallazgosRepoDAO.cursores(idBitacora+"");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos PASO1 y actualizaHallazgosNoASi del CECO: PASO 2 ");		}
		// Obtengo los hallazgos de la tabla
		// Obtengo los hallazgos de la tabla
					listaHallazgosTabla = (List<HallazgosExpDTO>) listaPregs.get("hallazgostabla");
					// Obtengo la copia de hallazgos del nuevo recorrido
					listaHallazgosCopiaRecorrido = (List<HallazgosExpDTO>) listaPregs.get("listaReconue");
					// Obtengo los hallazgos del nuevo recorrido
					listaHallazgosNuevoRecorrido = (List<HallazgosExpDTO>) listaPregs.get("hallazgosultreco");		
		try {
			if (listaHallazgosTabla != null && listaHallazgosTabla.size() > 0) {
				//Estaba en SI y Pasa a NO
				respuestaPaso2 = actualizaHallazgosSiaNo(listaHallazgosTabla, listaHallazgosCopiaRecorrido);
			}
		} catch (Exception e) {
			logger.info(e+"");
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos PASO2 y actualizaHallazgosSiaNo del CECO: ");
		}
		
		//PASO 3 TABLA EXISTENTE contra TODOS HALLAZGOS
		/*Comparar todos los hallazgos del recorrido con la tabla e INSERTAR los que no existan*/
		try {
			listaPregs = checklistHallazgosRepoDAO.cursores(idBitacora+"");
			logger.info("ww");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos PASO1 y actualizaHallazgosNoASi del CECO: PASO 3 ");		}
		// Obtengo los hallazgos de la tabla
		// Obtengo los hallazgos de la tabla
					listaHallazgosTabla = (List<HallazgosExpDTO>) listaPregs.get("hallazgostabla");
					// Obtengo la copia de hallazgos del nuevo recorrido
					listaHallazgosCopiaRecorrido = (List<HallazgosExpDTO>) listaPregs.get("listaReconue");
					// Obtengo los hallazgos del nuevo recorrido
					listaHallazgosNuevoRecorrido = (List<HallazgosExpDTO>) listaPregs.get("hallazgosultreco");	
		try {
			if (listaHallazgosTabla != null && listaHallazgosTabla.size() > 0) {
				//Estaba en SI y Pasa a NO
				respuestaPaso3 = insertaHallazgosAgregados(listaHallazgosTabla, listaHallazgosNuevoRecorrido);
			}
		} catch (Exception e) {
			logger.info(e+"");
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos PASO3 y insertaHallazgosAgregados del CECO: ? ");
		}
		
		//PASO 4 ES PRIMER RECORRIDO INSERTO TODOS LOS NUEVOS HALLAZGOS listaHallazgosTabla.size() == 0
		try {
			if (listaHallazgosTabla.size() == 0) {
				//Estaba en SI y Pasa a NO
				respuestaPaso4 = insertaHallazgosNuevos(listaHallazgosNuevoRecorrido);
			}
		} catch (Exception e) {
			logger.info(e);
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos PASO3 y insertaHallazgosAgregados del CECO: ");
		}

		if (listaHallazgosTabla.size() == 0 && respuestaPaso4) {
			respuesta = true;
		} else if (listaHallazgosTabla.size() > 0 && respuestaPaso1 && respuestaPaso2 && respuestaPaso3) {
			respuesta = true;
		} else {
			respuesta = false;
		}
		return respuesta;

	}
	///nuevo metodo para cargar hallazgos
	public boolean cargaHallazgosUpdateNuevo(int idBitacora) {

		/*EL ID RESPUESTA ES EL MISMO QUE EL ID HALLAZGO*/
		
		boolean respuesta = false;
		boolean respuestaPaso1  = false;
		boolean respuestaPaso2  = false;
		boolean respuestaPaso3   = false;
		boolean respuestaPaso4   = false;

		Map <String,Object> listaPregs = null;
		
		List<HallazgosExpDTO> listaHallazgosTabla = new ArrayList<HallazgosExpDTO>();
		List<HallazgosExpDTO> listaHallazgosCopiaRecorrido = new ArrayList<HallazgosExpDTO>();
		List<HallazgosExpDTO> listaHallazgosNuevoRecorrido = new ArrayList<HallazgosExpDTO>();


		//PASO 1 TABLA EXISTENTE contre COPIA NUEVO RECORRIDO
		try {
			
			listaPregs = checklistHallazgosRepoDAO.cursores(idBitacora+"");
			
			// Obtengo los hallazgos de la tabla
			listaHallazgosTabla = (List<HallazgosExpDTO>) listaPregs.get("hallazgostabla");
			// Obtengo la copia de hallazgos del nuevo recorrido
			listaHallazgosCopiaRecorrido = (List<HallazgosExpDTO>) listaPregs.get("listaReconue");
			// Obtengo los hallazgos del nuevo recorrido
			listaHallazgosNuevoRecorrido = (List<HallazgosExpDTO>) listaPregs.get("hallazgosultreco");
			
			
			if (listaHallazgosTabla != null && listaHallazgosTabla.size() > 0) {
				//Estaba en NO y Pasa a SI (Se resuelve)
				respuestaPaso1 = actualizaHallazgosNoASi2(listaHallazgosTabla, listaHallazgosCopiaRecorrido);
			} 
		} catch (Exception e) {
			logger.info(e);
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos PASO1 y actualizaHallazgosNoASi del CECO: ");
		}
		
		//PASO 2 TABLA EXISTENTE contra COPIA NUEVO RECORRIDO
		try {
			listaPregs = checklistHallazgosRepoDAO.cursores(idBitacora+"");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos PASO1 y actualizaHallazgosNoASi del CECO: PASO 2 ");		}
		// Obtengo los hallazgos de la tabla
		// Obtengo los hallazgos de la tabla
					listaHallazgosTabla = (List<HallazgosExpDTO>) listaPregs.get("hallazgostabla");
					// Obtengo la copia de hallazgos del nuevo recorrido
					listaHallazgosCopiaRecorrido = (List<HallazgosExpDTO>) listaPregs.get("listaReconue");
					// Obtengo los hallazgos del nuevo recorrido
					listaHallazgosNuevoRecorrido = (List<HallazgosExpDTO>) listaPregs.get("hallazgosultreco");		
		try {
			if (listaHallazgosTabla != null && listaHallazgosTabla.size() > 0) {
				//Estaba en SI y Pasa a NO
				respuestaPaso2 = actualizaHallazgosSiaNo(listaHallazgosTabla, listaHallazgosCopiaRecorrido);
			}
		} catch (Exception e) {
			logger.info(e+"");
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos PASO2 y actualizaHallazgosSiaNo del CECO: ");
		}
		
		//PASO 3 TABLA EXISTENTE contra TODOS HALLAZGOS
		/*Comparar todos los hallazgos del recorrido con la tabla e INSERTAR los que no existan*/
		try {
			listaPregs = checklistHallazgosRepoDAO.cursores(idBitacora+"");
			logger.info("ww");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos PASO1 y actualizaHallazgosNoASi del CECO: PASO 3 ");		}
		// Obtengo los hallazgos de la tabla
		// Obtengo los hallazgos de la tabla
					listaHallazgosTabla = (List<HallazgosExpDTO>) listaPregs.get("hallazgostabla");
					// Obtengo la copia de hallazgos del nuevo recorrido
					listaHallazgosCopiaRecorrido = (List<HallazgosExpDTO>) listaPregs.get("listaReconue");
					// Obtengo los hallazgos del nuevo recorrido
					listaHallazgosNuevoRecorrido = (List<HallazgosExpDTO>) listaPregs.get("hallazgosultreco");	
		try {
			if (listaHallazgosTabla != null && listaHallazgosTabla.size() > 0) {
				//Estaba en SI y Pasa a NO
				respuestaPaso3 = insertaHallazgosAgregados2(listaHallazgosTabla, listaHallazgosNuevoRecorrido);
			}
		} catch (Exception e) {
			logger.info(e+"");
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos PASO3 y insertaHallazgosAgregados del CECO: ? ");
		}
		
		//PASO 4 ES PRIMER RECORRIDO INSERTO TODOS LOS NUEVOS HALLAZGOS listaHallazgosTabla.size() == 0
		try {
			if (listaHallazgosTabla.size() == 0) {
				//Estaba en SI y Pasa a NO
				respuestaPaso4 = cargaHallazgosxBita(idBitacora);
			}
		} catch (Exception e) {
			logger.info(e);
			logger.info(
					"AP al obtener los Hallazgos hallazgosExpBI.obtieneDatos PASO3 y insertaHallazgosAgregados del CECO: ");
		}

		if (listaHallazgosTabla.size() == 0 && respuestaPaso4) {
			respuesta = true;
		} else if (listaHallazgosTabla.size() > 0 && respuestaPaso1 && respuestaPaso2 && respuestaPaso3) {
			respuesta = true;
		} else {
			respuesta = false;
		}
		return respuesta;

	}
	/////
		
	public boolean actualizaHallazgosNoASi(List<HallazgosExpDTO> listaHallazgosTabla,
			List<HallazgosExpDTO> listaHallazgosCopiaRecorrido) {
		/*
		 * Cuando una respuesta en el NUEVO RECORRIDO es (SI) es papá, por lo tanto
		 * debemos actualizar todas sus hijas dentro de la TABLA HALLAZGOS. Actualizar
		 * en la tabla de hallazgos todas sus hijas a (SI) con la evidencia de su papá.
		 * Al cerrar las hijas cuando se cambia la última en automático el papá se
		 * atiende
		 */

		boolean respuesta = false;

		for (HallazgosExpDTO hallazgoRecorridoObj : listaHallazgosCopiaRecorrido) {

			if (hallazgoRecorridoObj.getPregPadre() != 0
					&& hallazgoRecorridoObj.getRespuesta().equalsIgnoreCase("SI")) {

				// ACTUALIZO en la tabla los hallazgos hijos
				int idPadre = hallazgoRecorridoObj.getPregPadre();

				for (HallazgosExpDTO hallazgoTablaObj : listaHallazgosTabla) {

					if (hallazgoTablaObj.getIdPreg() == hallazgoRecorridoObj.getIdPreg()) {

						// actualizarHallazgoHijo
						HallazgosExpDTO bean = new HallazgosExpDTO();
						bean.setIdResp(hallazgoTablaObj.getIdResp());
						bean.setStatus(3);
						bean.setRespuesta("SI");
						bean.setRuta(hallazgoRecorridoObj.getRuta());
						// Fecha de la acción
						String fechaAutorizacionLocal = "";
						SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
						fechaAutorizacionLocal = f.format(new Date());
						bean.setFechaAutorizo(fechaAutorizacionLocal.replaceAll("-", ""));

						// Fecha Fin
						bean.setFechaFin(fechaAutorizacionLocal.replaceAll("-", ""));
						// SE ACTUALIZA CON ID RESPUESTA, NUEVO ES CON ID HALLAZGO
						bean.setIdHallazgo(hallazgoTablaObj.getIdHallazgo());
						bean.setIdResp(hallazgoTablaObj.getIdResp());
						// SE ACTUALIZA CON ID RESPUESTA, NUEVO ES CON ID HALLAZGO

						bean.setObs(hallazgoTablaObj.getObs());
						bean.setObsNueva(hallazgoRecorridoObj.getObsNueva());
						
						//SE ACTUALIZA USUMODIF POR "ACTUALIZACION POR RECORRIDO" Y MOTIVORECHAZO EN ""
						bean.setUsuModif("ACTUALIZACION DESDE RECORRIDO");
						bean.setMotivrechazo("");

						try {

							// Estatus 1 a 3
							boolean response = actualizaHallazgo(bean);

							if (response) {

								respuesta = true;

							} else {

								logger.info(
										"Else actualizaHallazgosRecorrido() HallazgosExpBI actualizaHallazgosNoASi");
								respuesta = false;

							}

						} catch (Exception e) {

							logger.info("AP actualizaHallazgosRecorrido() HallazgosExpBI actualizaHallazgosNoASi");
							respuesta = false;

						}
					}
				}

			} else if (hallazgoRecorridoObj.getPregPadre() == 0
					&& hallazgoRecorridoObj.getRespuesta().equalsIgnoreCase("SI")) {

				for (HallazgosExpDTO hallazgoTablaObj : listaHallazgosTabla) {

					if (hallazgoRecorridoObj.getIdPreg() == hallazgoTablaObj.getPregPadre()) {

						// actualizarHallazgoHijo
						HallazgosExpDTO bean = new HallazgosExpDTO();
						bean.setIdResp(hallazgoTablaObj.getIdResp());
						bean.setStatus(3);
						bean.setRespuesta("SI");
						bean.setRuta(hallazgoRecorridoObj.getRuta());
						// Fecha de la acción
						String fechaAutorizacionLocal = "";
						SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
						fechaAutorizacionLocal = f.format(new Date());
						bean.setFechaAutorizo(fechaAutorizacionLocal.replaceAll("-", ""));

						// Fecha Fin
						bean.setFechaFin(fechaAutorizacionLocal.replaceAll("-", ""));
						// SE ACTUALIZA CON ID RESPUESTA, NUEVO ES CON ID HALLAZGO
						bean.setIdHallazgo(hallazgoTablaObj.getIdHallazgo());
						bean.setIdResp(hallazgoTablaObj.getIdResp());
						// SE ACTUALIZA CON ID RESPUESTA, NUEVO ES CON ID HALLAZGO

						bean.setObs(hallazgoTablaObj.getObs());
						bean.setObsNueva(hallazgoRecorridoObj.getObsNueva());

						try {

							// Estatus 1 a 3
							boolean response = actualizaHallazgo(bean);

							if (response) {

								respuesta = true;

							} else {

								logger.info(
										"Else actualizaHallazgosRecorrido() HallazgosExpBI actualizaHallazgosNoASi");
								respuesta = false;

							}

						} catch (Exception e) {

							logger.info("AP actualizaHallazgosRecorrido() HallazgosExpBI actualizaHallazgosNoASi");
							respuesta = false;

						}
						
					}
				}

			}

		}

		return respuesta;

	}
		///Metodo para nuevos negocios
	public boolean actualizaHallazgosNoASi2(List<HallazgosExpDTO> listaHallazgosTabla,
			List<HallazgosExpDTO> listaHallazgosCopiaRecorrido) {
		/*
		 * Cuando una respuesta en el NUEVO RECORRIDO es (SI) es papá, por lo tanto
		 * debemos actualizar todas sus hijas dentro de la TABLA HALLAZGOS. Actualizar
		 * en la tabla de hallazgos todas sus hijas a (SI) con la evidencia de su papá.
		 * Al cerrar las hijas cuando se cambia la última en automático el papá se
		 * atiende
		 */

		boolean respuesta = false;

		for (HallazgosExpDTO hallazgoRecorridoObj : listaHallazgosCopiaRecorrido) {

			if (hallazgoRecorridoObj.getRespuesta().equalsIgnoreCase("SI")) {

			

				for (HallazgosExpDTO hallazgoTablaObj : listaHallazgosTabla) {

					if (hallazgoTablaObj.getIdPreg() == hallazgoRecorridoObj.getIdPreg()) {

						// actualizarHallazgoHijo
						HallazgosExpDTO bean = new HallazgosExpDTO();
						bean.setIdResp(hallazgoTablaObj.getIdResp());
						bean.setStatus(3);
						bean.setRespuesta("SI");
						bean.setRuta(hallazgoRecorridoObj.getRuta());
						// Fecha de la acción
						String fechaAutorizacionLocal = "";
						SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
						fechaAutorizacionLocal = f.format(new Date());
						bean.setFechaAutorizo(fechaAutorizacionLocal.replaceAll("-", ""));

						// Fecha Fin
						bean.setFechaFin(fechaAutorizacionLocal.replaceAll("-", ""));
						// SE ACTUALIZA CON ID RESPUESTA, NUEVO ES CON ID HALLAZGO
						bean.setIdHallazgo(hallazgoTablaObj.getIdHallazgo());
						bean.setIdResp(hallazgoTablaObj.getIdResp());
						// SE ACTUALIZA CON ID RESPUESTA, NUEVO ES CON ID HALLAZGO

						bean.setObs(hallazgoTablaObj.getObs());
						bean.setObsNueva(hallazgoRecorridoObj.getObsNueva());
						
						//SE ACTUALIZA USUMODIF POR "ACTUALIZACION POR RECORRIDO" Y MOTIVORECHAZO EN ""
						bean.setUsuModif("ACTUALIZACION DESDE RECORRIDO");
						bean.setMotivrechazo("");

						try {

							// Estatus 1 a 3
							boolean response = actualizaHallazgo(bean);

							if (response) {

								respuesta = true;

							} else {

								logger.info(
										"Else actualizaHallazgosRecorrido() HallazgosExpBI actualizaHallazgosNoASi");
								respuesta = false;

							}

						} catch (Exception e) {

							logger.info("AP actualizaHallazgosRecorrido() HallazgosExpBI actualizaHallazgosNoASi");
							respuesta = false;

						}
					}
				}

			} 

		}

		return respuesta;

	}
	
	
	///
	public boolean actualizaHallazgosSiaNo(List<HallazgosExpDTO> listaHallazgosTabla,
			List<HallazgosExpDTO> listaHallazgosCopiaRecorrido) {
		/*
		 * Cuando una respuesta en el NUEVO RECORRIDO es (NO) es un hallazgo, debemos
		 * validar si era un SI en el recorrido previo, si es una hija que se cambio de
		 * NO a SI o si es un nuevo hallazgo
		 */
		boolean respuesta = false;

		for (HallazgosExpDTO hallazgoRecorridoObj : listaHallazgosCopiaRecorrido) {

			if (hallazgoRecorridoObj.getRespuesta().equalsIgnoreCase("NO")) {

				// Si su ID padre es 0 ó es hija se actualiza a ella misma en la tabla
				// anterior
					for (HallazgosExpDTO hallazgoTablaObj : listaHallazgosTabla) {

						// Busco el ID de mi recorrido actual en la tabla y lo ACTUALIZO
			if ((hallazgoRecorridoObj.getIdPreg() == hallazgoTablaObj.getIdPreg()) && (hallazgoTablaObj.getRespuesta().equalsIgnoreCase("SI")) ) {

							// actualizarHallazgoHijo
							HallazgosExpDTO bean = new HallazgosExpDTO();
							bean.setIdResp(hallazgoRecorridoObj.getIdResp());
							bean.setStatus(1);
							bean.setRespuesta("NO");
							bean.setRuta("");
							logger.info("Actualiza");
							// Fecha de la acción
							String fechaAutorizacionLocal = "";
							SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
							fechaAutorizacionLocal = f.format(new Date());
							bean.setFechaAutorizo(fechaAutorizacionLocal.replaceAll("-", ""));

							// Fecha Fin
							bean.setFechaFin(fechaAutorizacionLocal.replaceAll("-", ""));
							// SE ACTUALIZA CON ID RESPUESTA, NUEVO ES CON ID HALLAZGO
							bean.setIdHallazgo(hallazgoTablaObj.getIdHallazgo());
							bean.setIdResp(hallazgoRecorridoObj.getIdResp());
							// SE ACTUALIZA CON ID RESPUESTA, NUEVO ES CON ID HALLAZGO

							bean.setObs(hallazgoRecorridoObj.getObsNueva());
							bean.setObsNueva("");
							
							//SE ACTUALIZA USUMODIF POR "ACTUALIZACION POR RECORRIDO" Y MOTIVORECHAZO EN ""
							bean.setUsuModif("ACTUALIZACION DESDE RECORRIDO");
							bean.setMotivrechazo("");
							try {
								//Estatus 3 a 1
								boolean response = actualizaHallazgoSiANo(bean);

								if (response) {

									respuesta = true;

								} else {

									logger.info("Else actualizaHallazgosRecorrido() HallazgosExpBI actualizaHallazgosSiaNo ");
									respuesta = false;

								}

							} catch (Exception e) {

								logger.info("AP actualizaHallazgosRecorrido() HallazgosExpBI actualizaHallazgosSiaNo");
								respuesta = false;

							}

						}

					}
			}
		}

		return respuesta;

	}
	
	public boolean insertaHallazgosAgregados(List<HallazgosExpDTO> listaHallazgosTabla,
			List<HallazgosExpDTO> listaHallazgosRecorrido) {
		/*
		 * Cuando un hallazgo en el NUEVO RECORRIDO no existe en la tabla lo debemos de agregar
		 */

		boolean respuesta = true;
		boolean response = false;
		//Buscar Ocurrencia
		for (HallazgosExpDTO hallazgoRecorridoObj : listaHallazgosRecorrido) {
			
			boolean existe = false;
			
			for (HallazgosExpDTO hallazgoTablaObj : listaHallazgosTabla) {
				
				if (hallazgoRecorridoObj.getIdPreg() == hallazgoTablaObj.getIdPreg()) {
					
					existe = true;
					break;
				}
			}
			
			if (!existe) {
				// SI NO EXISTE SE INSERTA EL HALLAZGO
				//***************************** DEFINIR CON JAVI
					HallazgosExpDTO bean = new HallazgosExpDTO();
					bean.setArbol("1");
					bean.setIdResp(hallazgoRecorridoObj.getIdResp());
					bean.setStatus(1);
					bean.setIdPreg(hallazgoRecorridoObj.getIdPreg());
					bean.setPreg(hallazgoRecorridoObj.getPreg());
					bean.setRespuesta("NO");
					bean.setPregPadre(hallazgoRecorridoObj.getPregPadre());
					bean.setResponsable(hallazgoRecorridoObj.getResponsable());
					//Observacion Nueva
					bean.setDisposi("");
					bean.setArea(hallazgoRecorridoObj.getArea());
					bean.setObs(hallazgoRecorridoObj.getObs());
					bean.setBitGral(hallazgoRecorridoObj.getBitGral());
					bean.setSla("15");
					bean.setAux2(hallazgoRecorridoObj.getIdcheck());
					//SE ACTUALIZA USUMODIF POR "ACTUALIZACION POR RECORRIDO" Y MOTIVORECHAZO EN ""
					bean.setUsuModif("ACTUALIZACION DESDE RECORRIDO");
					bean.setMotivrechazo("");
					//***************************** DEFINIR CON JAVI

					try {

						response = insertaHallazgo(bean);

						if (response) {
							
							if (bean.getPregPadre() != 0) {
								
							//HALLAZGO INSERTADO, validar si tiene papá en la tabla y si tiene ACTUALIZAR el papá a NO
							for (HallazgosExpDTO hallazgoTablaObj2 : listaHallazgosTabla) {
									
								//Si se cumple la condición actualizamos a la pregunta PAPÁ porque un hijo es NO
								if ((bean.getPregPadre() == hallazgoTablaObj2.getIdPreg())) {
									
								if (hallazgoTablaObj2.getRespuesta().equalsIgnoreCase("SI")) {
									
									// actualizarHallazgoPAPÁ
									HallazgosExpDTO beanPapa = new HallazgosExpDTO();
									beanPapa.setIdResp(hallazgoTablaObj2.getIdResp());
									beanPapa.setStatus(1);
									beanPapa.setRespuesta("NO");
									beanPapa.setRuta(bean.getRuta());
									// Fecha de la acción
									String fechaAutorizacionLocal1 = "";
									SimpleDateFormat f1 = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
									fechaAutorizacionLocal1 = f1.format(new Date());
									beanPapa.setFechaAutorizo(fechaAutorizacionLocal1.replaceAll("-", ""));

									// Fecha Fin
									bean.setFechaFin(fechaAutorizacionLocal1.replaceAll("-", ""));
									// SE ACTUALIZA CON ID RESPUESTA, NUEVO ES CON ID HALLAZGO
									beanPapa.setIdHallazgo(hallazgoTablaObj2.getIdHallazgo());
									beanPapa.setIdResp(hallazgoTablaObj2.getIdResp());
									// SE ACTUALIZA CON ID RESPUESTA, NUEVO ES CON ID HALLAZGO
									beanPapa.setObs(bean.getObsNueva());
									beanPapa.setObsNueva("");

									try {

										//Estatus 3 a 1
										boolean response2 = actualizaHallazgoSiANo(beanPapa);

										if (response2) {

											
										} else {

											logger.info("Else actualizaHallazgosRecorrido PAPÁ() HallazgosExpBI insertaHallazgosAgregados");
											respuesta = false;

										}
										

									} catch (Exception e) {

										logger.info("AP actualizaHallazgosRecorrido PAPÁ() HallazgosExpBI insertaHallazgosAgregados");
										respuesta = false;

									}
									
									break;

								} else {
								
									break;
									
								}

									
								}
							
							}
						}

						} else {

							logger.info("Else actualizaHallazgosRecorrido() HallazgosExpBI insertaHallazgosAgregados");
							respuesta = false;

						}

					} catch (Exception e) {

						logger.info("AP GENERAL actualizaHallazgosRecorrido() HallazgosExpBI insertaHallazgosAgregados");
						respuesta = false;

					}
			}

		}
		
		return respuesta;

	}
	//nuevo metodo
	public boolean insertaHallazgosAgregados2(List<HallazgosExpDTO> listaHallazgosTabla,
			List<HallazgosExpDTO> listaHallazgosRecorrido) {
		/*
		 * Cuando un hallazgo en el NUEVO RECORRIDO no existe en la tabla lo debemos de agregar
		 */

		boolean respuesta = true;
		boolean response = false;
		//Buscar Ocurrencia
		for (HallazgosExpDTO hallazgoRecorridoObj : listaHallazgosRecorrido) {
			
			boolean existe = false;
			
			for (HallazgosExpDTO hallazgoTablaObj : listaHallazgosTabla) {
				
				if (hallazgoRecorridoObj.getIdPreg() == hallazgoTablaObj.getIdPreg()) {
					
					existe = true;
					break;
				}
			}
			
			if (!existe) {
				// SI NO EXISTE SE INSERTA EL HALLAZGO
				//***************************** DEFINIR CON JAVI
					HallazgosExpDTO bean = new HallazgosExpDTO();
					bean.setArbol("1");
					bean.setIdResp(hallazgoRecorridoObj.getIdResp());
					bean.setStatus(1);
					bean.setIdPreg(hallazgoRecorridoObj.getIdPreg());
					bean.setPreg(hallazgoRecorridoObj.getPreg());
					bean.setRespuesta("NO");
					bean.setPregPadre(hallazgoRecorridoObj.getPregPadre());
					bean.setResponsable(hallazgoRecorridoObj.getResponsable());
					//Observacion Nueva
					bean.setDisposi("");
					bean.setArea(hallazgoRecorridoObj.getArea());
					bean.setObs(hallazgoRecorridoObj.getObs());
					bean.setBitGral(hallazgoRecorridoObj.getBitGral());
					bean.setSla("15");
					bean.setAux2(hallazgoRecorridoObj.getIdcheck());
					//SE ACTUALIZA USUMODIF POR "ACTUALIZACION POR RECORRIDO" Y MOTIVORECHAZO EN ""
					bean.setUsuModif("ACTUALIZACION DESDE RECORRIDO");
					bean.setMotivrechazo("");
					//***************************** DEFINIR CON JAVI

					try {

						response = insertaHallazgo(bean);
					} catch (Exception e) {

						logger.info("AP GENERAL actualizaHallazgosRecorrido() HallazgosExpBI insertaHallazgosAgregados");
						respuesta = false;

					}
			}

		}
		
		return respuesta;

	}
	//
	
	public boolean insertaHallazgosNuevos(List<HallazgosExpDTO> listaHallazgosRecorrido) {
		/*
		 * Cuando un hallazgo en el NUEVO RECORRIDO no existe en la tabla lo debemos de
		 * agregar
		 */
		boolean respuesta = false;

		for (HallazgosExpDTO hallazgoRecorridoObj : listaHallazgosRecorrido) {
			// SE INSERTA EL HALLAZGO NUEVO
			// ***************************** DEFINIR CON JAVI
			HallazgosExpDTO bean = new HallazgosExpDTO();
			bean.setArbol("1");
			bean.setIdResp(hallazgoRecorridoObj.getIdResp());
			bean.setStatus(1);
			bean.setIdPreg(hallazgoRecorridoObj.getIdPreg());
			bean.setPreg(hallazgoRecorridoObj.getPreg());
			bean.setRespuesta("NO");
			bean.setPregPadre(hallazgoRecorridoObj.getPregPadre());
			bean.setResponsable(hallazgoRecorridoObj.getResponsable());
			//Observacion Nueva
			bean.setDisposi("");
			bean.setArea(hallazgoRecorridoObj.getArea());
			bean.setObs(hallazgoRecorridoObj.getObs());
			bean.setBitGral(hallazgoRecorridoObj.getBitGral());
			bean.setSla("15");
			bean.setAux2(hallazgoRecorridoObj.getIdcheck());
			//SE ACTUALIZA USUMODIF POR "ACTUALIZACION POR RECORRIDO" Y MOTIVORECHAZO EN ""
			bean.setUsuModif("ACTUALIZACION DESDE RECORRIDO");
			bean.setMotivrechazo("");
			// ***************************** DEFINIR CON JAVI

			try {
				boolean response = insertaHallazgo(bean);

				if (response) {
					respuesta = true;
				} else {
					logger.info("Else  insertaHallazgosNuevos() HallazgosExpBI insertaHallazgosNuevos");
					respuesta = false;
				}

			} catch (Exception e) {

				logger.info("AP GENERAL insertaHallazgosNuevos() HallazgosExpBI insertaHallazgosNuevos");
				respuesta = false;
				
			}

		}

		return respuesta;

	}
		
	//Actualiza Papá en Automático Cuando cierro el último hijo
		public boolean actualizaHallazgo(HallazgosExpDTO hallazgo) {
			boolean respuesta = false;
			
			try {
				
				boolean actualizaResponse = hallazgosExpDAO.actualizaResp(hallazgo);
				
				if (actualizaResponse) {
					respuesta = true;
				} else {
					respuesta = false;
				}
				
			} catch (Exception e) {
				logger.info("AP en actualizaHallazgo() HallazgosExpBI");
				respuesta = false;
			}
			
			return respuesta;
		}
		
		//Permite Cambiar de status 3 a 1 (SI a NO)
		public boolean actualizaHallazgoSiANo(HallazgosExpDTO hallazgo) {
			boolean respuesta = false;
			
			try {
				
				boolean actualizaResponse = hallazgosExpDAO.actualiza(hallazgo);	
				
				if (actualizaResponse) {
					respuesta = true;
				} else {
					respuesta = false;
				}
				
			} catch (Exception e) {
				logger.info("AP en actualizaHallazgo() HallazgosExpBI");
				respuesta = false;
			}
			
			return respuesta;
		}
		
		public boolean insertaHallazgo(HallazgosExpDTO hallazgo) {
			boolean respuesta = false;
			
			try {

				int actualizaResponse = hallazgosExpDAO.inserta(hallazgo);				
				
				if (actualizaResponse !=  0) {
					respuesta = true;
					logger.info("");
				} else {
					respuesta = false;
				}
				
			} catch (Exception e) {
				logger.info("AP en inserta Hallazgo() HallazgosExpBI");
				respuesta = false;
			}
			
			return respuesta;
		}
	//FIN Nuevo Proceso para la carga de hallazgos desde la primer caminata

//hallasgos historico
	// elimina por bitacora gral
	public boolean eliminaBitaHisto(String bitGral) {
		boolean respuesta = false;

		try {
			respuesta = hallazgosExpDAO.eliminaBitaHisto(bitGral);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta;
	}

	// INSERTA
	public boolean insertaHallazgosHist(int idHallazgo) {
		boolean respuesta = false;

		try {
			respuesta = hallazgosExpDAO.insertaHallazgosHist(idHallazgo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta;
	}

	// carga historial
	public boolean cargaHallazgosHistorico() {
		boolean respuesta = false;

		try {
			respuesta = hallazgosExpDAO.cargaHallazgosHistorico();
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);
		}

		return respuesta;
	}

	public List<HallazgosExpDTO> obtieneHistFolio(int idFolio) {

		try {
			listafila = hallazgosExpDAO.obtieneHistFolio(idFolio);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	public List<HallazgosExpDTO> obtieneHistorico() {

		try {
			listafila = hallazgosExpDAO.obtieneHistorico();
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}

	public List<HallazgosExpDTO> obtieneHistBita(int bitGral) {

		try {
			listafila = hallazgosExpDAO.obtieneHistBita(bitGral);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);
		}

		return listafila;
	}
		
}