package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteSistemasDTO;


public class ReporteCheckSistRowMapper implements RowMapper<ReporteSistemasDTO> {

	public ReporteSistemasDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteSistemasDTO reporteSistemas = new ReporteSistemasDTO();
		
		reporteSistemas.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		reporteSistemas.setNombreCheck(rs.getString("FCNOMBRE"));
		reporteSistemas.setIdGrupo(rs.getInt("FIORDEN_GRUPO"));
		
		return reporteSistemas;
	}
}
