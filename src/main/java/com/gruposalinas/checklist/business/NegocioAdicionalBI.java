package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.NegocioAdicionalDAO;
import com.gruposalinas.checklist.domain.NegocioAdicionalDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class NegocioAdicionalBI {
private static Logger logger = LogManager.getLogger(NegocioAdicionalBI.class);
	
	@Autowired
	NegocioAdicionalDAO negocioAdicionalDAO;
	
	public List<NegocioAdicionalDTO> obtieneNegocioAdicional(String usuario, String negocio ){
		
		List<NegocioAdicionalDTO> consulta = null;
		
		try {
			consulta = negocioAdicionalDAO.obtieneNegocioAdicional(usuario, negocio);
		} catch (Exception e) {
			logger.info("No fue posible obtener informacion Negocio Adicional");
				
		}
		
		return consulta;
	}
	
	public boolean insertaNegocioAdicional(int usuario, int negocio){
		boolean respuesta = false;
		
		try {
			respuesta = negocioAdicionalDAO.insertaNegocioAdicional(usuario, negocio);
		} catch (Exception e) {
			logger.info("No fue posible insertar informacion Negocio Adicional");
				
		}
		
		return respuesta;
	}
	
	public boolean eliminaNegocioAdicional(int usuario, int negocio){
		boolean respuesta = false;
		
		try {
			respuesta = negocioAdicionalDAO.eliminaNegocioAdicional(usuario, negocio);
		} catch (Exception e) {
			logger.info("No fue posible eliminar informacion Negocio Adicional");
				
		}
		
		return respuesta;
	}
}
