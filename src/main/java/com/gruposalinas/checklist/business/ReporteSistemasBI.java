package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ReporteSistemasDAO;
import com.gruposalinas.checklist.domain.ReporteSistemasDTO;

public class ReporteSistemasBI{

	private static final Logger logger = LogManager.getLogger(ReporteSistemasBI.class);

	@Autowired
	ReporteSistemasDAO reporteSistemasDAO;

	List<ReporteSistemasDTO> listaCheckSistemas = null;
	List<ReporteSistemasDTO> listaCecosSistemas = null;
	
	public List<ReporteSistemasDTO> ReporteChecklistSistemas(int idCeco)
			throws Exception {
		try {
			listaCheckSistemas = reporteSistemasDAO.ReporteChecklistSistemas(idCeco);
		} catch (Exception e) {
			logger.info("No fue posible consultar los checklist" + e);
		}

		return listaCheckSistemas;
		
	}	public List<ReporteSistemasDTO> ReporteCecosSistemas(int idCeco, int idChecklist, String fecha)
			throws Exception {
		try {
			listaCecosSistemas = reporteSistemasDAO.ReporteCecosSistemas(idCeco, idChecklist, fecha);
		} catch (Exception e) {
			logger.info("No fue posible consultar los Cecos" + e);
		}

		return listaCecosSistemas;
	}
}
