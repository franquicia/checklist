package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ConteoTablasDTO;
import com.gruposalinas.checklist.domain.SucUbicacionDTO;
import com.gruposalinas.checklist.mappers.ConteoTablasRowMapper;
import com.gruposalinas.checklist.mappers.SucUbicaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class SucUbicacionDAOImpl extends DefaultDAO implements SucUbicacionDAO {

    private static Logger logger = LogManager.getLogger(SucUbicacionDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodo;
    DefaultJdbcCall jdbcObtieneSuc;
    DefaultJdbcCall jdbcCargaSucUbi;
    DefaultJdbcCall jdbcEliminaSucUbi;
    DefaultJdbcCall jdbcObtieneSucTabla;
    DefaultJdbcCall jdbcObtieneConteoTab;

    public void init() {

        jdbcObtieneTodo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASUCUBICA")
                .withProcedureName("SP_SEL_SUC")
                .returningResultSet("RCL_SUC", new SucUbicaRowMapper());

        jdbcObtieneSuc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASUCUBICA")
                .withProcedureName("SP_SEL_SUC")
                .returningResultSet("RCL_SUC", new SucUbicaRowMapper());

        jdbcCargaSucUbi = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASUCUBICA")
                .withProcedureName("SPCARGAL_SUC");

        jdbcEliminaSucUbi = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASUCUBICA")
                .withProcedureName("SP_DEPURA_SUC");

        jdbcObtieneSucTabla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASUCUBICA")
                .withProcedureName("SP_DET_SUC")
                .returningResultSet("RCL_SUC", new SucUbicaRowMapper());

        jdbcObtieneConteoTab = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PATOTALESTAB")
                .withProcedureName("SP_SEL_CONTTABLAS")
                .returningResultSet("RCL_CONTEO", new ConteoTablasRowMapper());

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<SucUbicacionDTO> obtieneInfo() throws Exception {
        Map<String, Object> out = null;
        List<SucUbicacionDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneTodo.execute();

        //logger.info("Funci�n ejecutada: {FRANQUICIA.PASUCUBICA.SP_SEL_SUC}");
        listaFijo = (List<SucUbicacionDTO>) out.get("RCL_SUC");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las Sucursales");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }
    //parametro

    @SuppressWarnings("unchecked")
    public List<SucUbicacionDTO> obtieneDatos(String idpais) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<SucUbicacionDTO> lista = null;

        //logger.info("entro a daoImp de sucursal ubicacion");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPAIS", idpais);

        out = jdbcObtieneSuc.execute(in);

        //logger.info("Funci�n ejecutada: {FRANQUICIA.PASUCUBICA.SP_SEL_EMP}");
        //lleva el nombre del cursor del procedure
        lista = (List<SucUbicacionDTO>) out.get("RCL_SUC");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las sucursales del pais(" + idpais + ")");
        }
        return lista;

    }

    @Override
    public boolean cargaSucUbic() throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        out = jdbcCargaSucUbi.execute();

        //logger.info("Funci�n ejecutada: {FRANQUICIA.PASUCUBICA.SPCARGAL_SUC}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener cargaSucUbic");
        } else {
            return true;
        }

        return false;

    }

    @Override
    public boolean EliminacargaSucUbic() throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        out = jdbcEliminaSucUbi.execute();

        //logger.info("Funci�n ejecutada: {FRANQUICIA.PASUCUBICA.SP_DEPURA_SUC}");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las Sucursales");
        } else {
            return true;
        }

        return false;

    }

    @SuppressWarnings("unchecked")
    public List<SucUbicacionDTO> obtieneDatosTab(String idpais) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<SucUbicacionDTO> lista = null;

        //logger.info("entro a daoImp de sucursal ubicacion");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDPAIS", idpais);

        out = jdbcObtieneSucTabla.execute(in);

        //logger.info("Funci�n ejecutada: {FRANQUICIA.PASUCUBICA.SP_DET_SUC}");
        //lleva el nombre del cursor del procedure
        lista = (List<SucUbicacionDTO>) out.get("RCL_SUC");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener las sucursales del pais(" + idpais + ")");
        }
        return lista;

    }

    //sin parametro
    @SuppressWarnings("unchecked")
    public List<ConteoTablasDTO> obtieneInfoTab() throws Exception {
        Map<String, Object> out = null;
        List<ConteoTablasDTO> listaFijo = null;
        int error = 0;

        out = jdbcObtieneConteoTab.execute();

        //logger.info("Funci�n ejecutada: {FRANQUICIA.PATOTALESTAB.SP_SEL_CONTTABLAS}");
        listaFijo = (List<ConteoTablasDTO>) out.get("RCL_CONTEO");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener conteo de tablas");
        } else {
            return listaFijo;
        }

        return listaFijo;

    }

}
