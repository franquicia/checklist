package com.gruposalinas.checklist.dao;

import java.util.ArrayList;
import java.util.List;
import com.gruposalinas.checklist.domain.AdmPregZonaDTO;

public interface AdmPregZonasDAO {
	
	public ArrayList<AdmPregZonaDTO> obtienePregZonaById(AdmPregZonaDTO bean) throws Exception;
	
	public boolean insertaPregZona(AdmPregZonaDTO bean) throws Exception;

	public boolean actualizaPregZona(AdmPregZonaDTO bean) throws Exception;
	
	public boolean eliminaPregZona(AdmPregZonaDTO bean) throws Exception;
}
