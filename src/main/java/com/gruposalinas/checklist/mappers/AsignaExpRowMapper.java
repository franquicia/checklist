package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.AsignaExpDTO;

public class AsignaExpRowMapper implements RowMapper<AsignaExpDTO> {

	public AsignaExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AsignaExpDTO AsignaExpDTO = new AsignaExpDTO();
		
		AsignaExpDTO.setCeco(rs.getInt("FCID_CECO"));
	    AsignaExpDTO.setVersion(rs.getInt("FIVERSION"));
	    AsignaExpDTO.setUsuario(rs.getInt("FCUSUARIO"));
	    AsignaExpDTO.setIdestatus(rs.getInt("FCSTATUS"));
	    AsignaExpDTO.setUsuario_asig(rs.getInt("FCUSUASIG"));
	    AsignaExpDTO.setObs(rs.getString("FCOBSERVACION"));
	    AsignaExpDTO.setPeriodo(rs.getString("FCPERIODO"));
	    
		return AsignaExpDTO;
	}

}


