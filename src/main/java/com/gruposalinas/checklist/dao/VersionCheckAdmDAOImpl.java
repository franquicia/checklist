package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.VersionExpanDTO;
import com.gruposalinas.checklist.mappers.VersionesCheckAdmRowMapper;
import com.gruposalinas.checklist.mappers.VersionesNegoAdmRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class VersionCheckAdmDAOImpl extends DefaultDAO implements VersionCheckAdmDAO {

    private static Logger logger = LogManager.getLogger(VersionCheckAdmDAOImpl.class);

    DefaultJdbcCall jdbcInsertaVers;
    DefaultJdbcCall jdbcEliminaVers;
    DefaultJdbcCall jdbcObtieneVers;
    DefaultJdbcCall jbdcActualizaVers;
    DefaultJdbcCall jbdcActualizaVers2;
    DefaultJdbcCall jdbcInsertaVersNego;
    DefaultJdbcCall jdbcEliminaVersNego;
    DefaultJdbcCall jdbcObtieneVersNego;
    DefaultJdbcCall jbdcActualizaVersNego;
    DefaultJdbcCall jbdcCargaTabs;
    DefaultJdbcCall jbdcCargaTabsPROD;

    public void init() {

        jdbcInsertaVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERNEADM")
                .withProcedureName("SP_INS_VERS");

        jdbcEliminaVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERNEADM")
                .withProcedureName("SP_DEL_VERS");

        jdbcObtieneVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERNEADM")
                .withProcedureName("SP_SEL_VERS")
                .returningResultSet("RCL_VERS", new VersionesCheckAdmRowMapper());

        jbdcActualizaVers = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERNEADM")
                .withProcedureName("SP_ACT_VERS");

        jbdcActualizaVers2 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERNEADM")
                .withProcedureName("SP_ACT_VERS2");
//version negocio
        jdbcInsertaVersNego = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PANEGOVERSADM")
                .withProcedureName("SP_INS_VERS");

        jdbcEliminaVersNego = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PANEGOVERSADM")
                .withProcedureName("SP_DEL_VERS");

        jdbcObtieneVersNego = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PANEGOVERSADM")
                .withProcedureName("SP_SEL_VERS")
                .returningResultSet("RCL_VERS", new VersionesNegoAdmRowMapper());

        jbdcActualizaVersNego = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PANEGOVERSADM")
                .withProcedureName("SP_ACT_VERS");

        jbdcCargaTabs = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERNEADM")
                .withProcedureName("SP_CARGA_VERS");

        jbdcCargaTabsPROD = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMVERNEADM")
                .withProcedureName("SP_CARGA_VERSPROD");

    }

    public int inserta(VersionExpanDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", bean.getIdCheck())
                .addValue("PA_VERSION", bean.getIdVers())
                .addValue("PA_STATUS", bean.getIdactivo())
                .addValue("PA_AUX", bean.getAux())
                .addValue("PA_NEGO", bean.getNego())
                .addValue("PA_OBS", bean.getObs());

        out = jdbcInsertaVers.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMVERNEADM.SP_INS_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDTABLA");
        idEmpFij = idTipoReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    public boolean elimina(String idTab) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDTABLA", idTab);

        out = jdbcEliminaVers.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMVERNEADM.SP_DEL_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el dato(" + idTab + ")");
        } else {
            return true;
        }

        return false;

    }

    //parametro
    @SuppressWarnings("unchecked")
    public List<VersionExpanDTO> obtieneDatos(String idVers) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<VersionExpanDTO> listaN = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDVERS", idVers);

        out = jdbcObtieneVers.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PAADMVERNEADM.SP_SEL_VERS}");
        //lleva el nombre del cursor del procedure
        listaN = (List<VersionExpanDTO>) out.get("RCL_VERS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener la version(" + idVers + ")");
        }
        return listaN;

    }

    public boolean actualiza(VersionExpanDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDTABLA", bean.getIdTab())
                .addValue("PA_CHECKLIST", bean.getIdCheck())
                .addValue("PA_VERSION", bean.getIdVers())
                .addValue("PA_STATUS", bean.getIdactivo())
                .addValue("PA_AUX", bean.getAux())
                .addValue("PA_NEGO", bean.getNego())
                .addValue("PA_OBS", bean.getObs());

        out = jbdcActualizaVers.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMVERNEADM.SP_ACT_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar  del  id( " + bean.getIdTab() + ")");
        } else {
            return true;
        }
        return false;

    }

    public boolean actualiza2(VersionExpanDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDTABLA", bean.getIdTab())
                .addValue("PA_CHECKLIST", bean.getIdCheck())
                .addValue("PA_NEGO", bean.getNego())
                .addValue("PA_VERSION", bean.getIdVers());

        out = jbdcActualizaVers2.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMVERNEADM.SP_ACT_VERS2}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Estado del  id( " + bean.getIdTab() + ")");
        } else {
            return true;
        }
        return false;

    }

    @Override
    public int insertaNegoVer(VersionExpanDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idEmpFij = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_VERSION", bean.getIdVers())
                .addValue("PA_STATUS", bean.getIdactivo())
                .addValue("PA_NEGO", bean.getNego())
                .addValue("PA_LIBERA", bean.getFechaLibera())
                .addValue("PA_OBS", bean.getObs());

        out = jdbcInsertaVersNego.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PANEGOVERSADM.SP_INS_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_IDTAB");
        idEmpFij = idTipoReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar");
        } else {
            return idEmpFij;
        }

        return idEmpFij;
    }

    @Override
    public boolean eliminaNegoVer(String idTab) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDTAB", idTab);

        out = jdbcEliminaVersNego.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PANEGOVERSADM.SP_DEL_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar el dato(" + idTab + ")");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<VersionExpanDTO> obtieneDatosNegoVer(String idVers) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<VersionExpanDTO> listaN = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDVERS", idVers);

        out = jdbcObtieneVersNego.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PANEGOVERSADM.SP_SEL_VERS}");
        //lleva el nombre del cursor del procedure
        listaN = (List<VersionExpanDTO>) out.get("RCL_VERS");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener la version(" + idVers + ")");
        }
        return listaN;

    }

    @Override
    public boolean actualizaNegoVer(VersionExpanDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDTAB", bean.getIdTab())
                .addValue("PA_VERSION", bean.getIdVers())
                .addValue("PA_STATUS", bean.getIdactivo())
                .addValue("PA_LIBERA", bean.getFechaLibera())
                .addValue("PA_NEGO", bean.getNego())
                .addValue("PA_OBS", bean.getObs());

        out = jbdcActualizaVersNego.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PANEGOVERSADM.SP_ACT_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar  del  id( " + bean.getIdTab() + ")");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean cargaVesionesTab(String protocolo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_PROTO", protocolo);

        out = jbdcCargaTabs.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PANEGOVERSADM.SP_CARGA_VERS}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al cargar el dato(" + protocolo + ")");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean cargaVesionesTabProd(String protocolo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_PROTO", protocolo);

        out = jbdcCargaTabsPROD.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PANEGOVERSADM.SP_CARGA_VERSPROD}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al cargar el dato(" + protocolo + ")");
        } else {
            return true;
        }

        return false;
    }

}
