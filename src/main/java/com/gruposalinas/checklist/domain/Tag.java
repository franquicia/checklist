package com.gruposalinas.checklist.domain;

/*CLASE PARA LA BUSQUEDA EN AJAX*/
public class Tag {

	public String tagName;
	public String descripcion;
	public String recurso;

	public Tag(String tagName, String descripcion, String recurso) {
		this.tagName = tagName;
		this.descripcion = descripcion;
		this.recurso = recurso;
	}

	public String getRecurso() {
		return recurso;
	}

	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

}
