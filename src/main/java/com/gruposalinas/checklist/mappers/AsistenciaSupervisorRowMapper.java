package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.AsistenciaSupervisorDTO;

public class AsistenciaSupervisorRowMapper implements RowMapper<AsistenciaSupervisorDTO> {

	public AsistenciaSupervisorDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AsistenciaSupervisorDTO asistenciaSupervisorDTO = new AsistenciaSupervisorDTO();
		
		asistenciaSupervisorDTO.setIdUsuario(Integer.parseInt(""+rs.getInt("FCID_USUARIO")));
		asistenciaSupervisorDTO.setNomUsuario(rs.getString("FCSOCIO"));
		asistenciaSupervisorDTO.setFecha(rs.getString("FDFECHA"));
		asistenciaSupervisorDTO.setEntrada(rs.getString("FECHA_ENTRADA"));
		asistenciaSupervisorDTO.setSalida(rs.getString("FECHA_SALIDA"));
		asistenciaSupervisorDTO.setIdSucursal(Integer.parseInt(rs.getString("FCID_CECO")));
		asistenciaSupervisorDTO.setNomSucursal(rs.getString("FCSUCURSAL"));
	
		return asistenciaSupervisorDTO;
	}

}
