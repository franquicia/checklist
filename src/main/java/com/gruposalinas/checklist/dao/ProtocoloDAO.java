package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ProtocoloDTO;


public interface ProtocoloDAO {
	
	public List<ProtocoloDTO> obtieneProtocolo(int idProtocolo) throws Exception;
	
	public int insertaProtocolo(ProtocoloDTO bean)throws Exception;
	
	public boolean modificaProtocolo(ProtocoloDTO bean)throws Exception;
	
	public boolean eliminaProtocolo(int idProtocolo)throws Exception;
        
        List<ProtocoloDTO> getProtocolos() throws Exception;

}
