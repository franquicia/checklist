package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.RepoFilExpDTO;


public interface RepoFilExpDAO {

	// obtiene FILTRO 1
	public List<RepoFilExpDTO> obtieneDatosGen(String ceco,String nombreCeco,String fechaIni,String fechaFin,String idRecorrido) throws Exception;

	
	// preguntas conteo  // obtiene FILTRO 2
	public List<RepoFilExpDTO> obtieneInfo(String ceco ,String idRecorrido) throws Exception;
	
	// preguntas 
	public List<RepoFilExpDTO> obtienePreguntas(String ceco) throws Exception;
	
	// preguntas hijas
	public List<RepoFilExpDTO> obtienePreguntasHijas(String ceco) throws Exception;
		
	//FILTRO 3
	public List<RepoFilExpDTO> obtieneInfofiltro(String bita1,String bita2, String pregFilt) throws Exception;
	
	// DASHBOARD
	public List<RepoFilExpDTO> obtienedash(String ceco,String idRecorrido,String aux) throws Exception;

	// DASHBOARD PREGUNTAS
	public List<RepoFilExpDTO> obtienedash2(String ceco) throws Exception;
	
	// DASHBOARD RESPUESTAS
	public List<RepoFilExpDTO> obtienedash3(String bita1) throws Exception;
		
	// DASHBOARD RESPUESTAS Preg
	public String resp_preg(String bita1) throws Exception;
	
	// reporte fechas 
	public List<RepoFilExpDTO> repoFechas(String fechaIni,String fechaFin) throws Exception;
	
	//REPORTE ACTA
		public List<RepoFilExpDTO> obtieneInfofActa(String bita1) throws Exception;
		
	// preguntas Actaa
	public List<RepoFilExpDTO> obtienePreguntasActa(String bita1) throws Exception;
	
	// reporte fechas 
		public List<RepoFilExpDTO> repoFechasNvo(String fechaIni,String fechaFin) throws Exception;
		
		// obtiene FILTRO CECO FASE PROYECTO WEB
		public List<RepoFilExpDTO> obtieneDatosFiltro1(String ceco,String fase,String proyecto,String nombreCeco,String recorrido,String fechaIni,String fechaFin) throws Exception;
		
		// obtiene FILTRO CECO FASE PROYECTO cursores STRING
		public Map<String, Object> obtieneDatosFiltro1Fase(String ceco,String fase,String proyecto,String nombreCeco,String recorrido,String fechaIni,String fechaFin) throws Exception;
				
	
		// DASHBOARD CECO FASE PROYECTO
		public List<RepoFilExpDTO> obtienedashCecoProyecto(String ceco,String fase,String proyecto) throws Exception;
		
		//REPORTE ACTA CECO FASE PROYECTO
		public List<RepoFilExpDTO> obtieneActaCecoFaseProyecto(String ceco,String fase,String proyecto) throws Exception;
		
		// preguntas Actaa  CECO FASE PROYECTO
		public List<RepoFilExpDTO> obtienePregActaCecoFaseProyecto(String ceco,String fase,String proyecto) throws Exception;
		
		
		
		//REPORTE NUEVO DASHBOARD GENERICO
		public List<RepoFilExpDTO> obtieneInfoDashboardGenerico(String bita1,String parametro) throws Exception;
}

