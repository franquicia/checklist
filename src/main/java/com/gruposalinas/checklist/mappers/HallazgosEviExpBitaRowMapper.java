package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.HallazgosEviExpDTO;
import com.gruposalinas.checklist.domain.HallazgosExpDTO;

public class HallazgosEviExpBitaRowMapper implements RowMapper<HallazgosEviExpDTO> {


	public HallazgosEviExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		HallazgosEviExpDTO HallazgosEviExpDTO = new HallazgosEviExpDTO();
		
		HallazgosEviExpDTO.setIdHallazgo(rs.getInt("FIID_HALLAZGO"));
	    HallazgosEviExpDTO.setIdEvi(rs.getInt("FIID_EVIDENCIA"));
	    HallazgosEviExpDTO.setIdResp(rs.getInt("FIID_RESPUESTA"));
	    HallazgosEviExpDTO.setRuta(rs.getString("FCRUTA"));
	    HallazgosEviExpDTO.setNombre(rs.getString("FCNOMBRE"));
	    HallazgosEviExpDTO.setTipoEvi(rs.getInt("FIID_TIPO"));
	    HallazgosEviExpDTO.setTotEvi(rs.getInt("FIID_NUMEVI"));
	    HallazgosEviExpDTO.setAux(rs.getString("FCAUX"));
	    HallazgosEviExpDTO.setStatus(rs.getInt("FCSTATUS"));
	    HallazgosEviExpDTO.setPeriodo(rs.getString("FCPERIODO"));
	    HallazgosEviExpDTO.setBitaGral(rs.getInt("FIID_BITGRAL"));
	   
	
		return HallazgosEviExpDTO;
		
	}

}


