package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.FaseFirmasDTO;

public class FaseOrdenRowMapper implements RowMapper<FaseFirmasDTO> {

	public FaseFirmasDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		FaseFirmasDTO FaseFirmasDTO = new FaseFirmasDTO();
		
		FaseFirmasDTO.setIdSoft(rs.getString("FIID_SOFTN"));
		FaseFirmasDTO.setCeco(rs.getString("FCID_CECO"));
		FaseFirmasDTO.setIdFase(rs.getInt("FIID_FASE"));
		FaseFirmasDTO.setIdProyecto(rs.getInt("FIID_PROYECTO"));
	    FaseFirmasDTO.setIdOrdenFaFse(rs.getInt("FIID_ORDENFASE"));
	    FaseFirmasDTO.setNombreFase(rs.getString("NOMBFASE"));
	    FaseFirmasDTO.setDetalleFase(rs.getString("DETAFASE"));
	 
	    
	
          
	return FaseFirmasDTO;
	}

}


