package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.RepoFilExpDTO;

public class RepoDashExpCecoFaseRowMapper implements RowMapper<RepoFilExpDTO> {

	public RepoFilExpDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		RepoFilExpDTO com = new RepoFilExpDTO();

		com.setIdSoft(rs.getInt("FIID_SOFTN"));
		com.setCeco(rs.getString("FCID_CECO"));
		com.setFase(rs.getString("FIID_FASE"));
		com.setProyecto(rs.getString("FIID_PROYECTO"));
		//id recorrido soft
		com.setAux(rs.getString("FISTATUS"));
	    com.setIdUsu(rs.getInt("FIID_USUARIO"));
	    com.setVersion(rs.getInt("FIVERSION"));   
	    com.setFase(rs.getString("NOMBREFASE"));
	    //observacion Fase
	    com.setObs(rs.getString("OBSFASE"));
	    com.setIdCheck(rs.getInt("FIID_CHECKLIST"));
	    //detralle version
	    com.setDetalle(rs.getString("DETALLEVERSION"));
	    com.setObsVersion(rs.getString("OBSVERSION"));
	    com.setIdPreg(rs.getInt("FIID_PREGUNTA"));
	    com.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
	    com.setIdcritica(rs.getInt("FICRITICA"));
	    com.setPregunta(rs.getString("PREGUNTA"));  
	    com.setChecklist(rs.getString("CHECKLIST"));
	    com.setIdOrdenGru(rs.getInt("FIORDEN_GRUPO"));
	    com.setClasif(rs.getString("FCCLASIFICA"));
	    com.setRuta(rs.getString("RUTA"));
	    com.setIdPosible(rs.getInt("FCRESPUESTA"));
	    com.setPosible(rs.getString("POSIBLE"));
	    com.setCalCheck(rs.getDouble("FIPODENRACION"));
	    com.setObserv(rs.getString("OBSERVACIONES"));
	    com.setIdResp(rs.getInt("FIID_RESPUESTA"));  
	    com.setIdBita(rs.getInt("FIID_BITACORA"));
	    com.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
	    com.setPerido(rs.getString("FDFECHA_MOD"));
	    com.setFase(rs.getString("NOMBREFASE"));
	    com.setVersion(rs.getInt("FIVERSION"));
	    com.setLiderObra(rs.getString("LIDER_OBRA"));
	    com.setArea(rs.getString("FCAREA"));
	    com.setSla(rs.getInt("FCSLA"));
	    com.setNomUsu(rs.getString("USUFRANQ"));
	   /* com.setPrecalif(rs.getDouble("FIPRECALIF"));
	    com.setCalif(rs.getDouble("FICALIFICACION"));
	    com.setFechaTermino(rs.getString("FDTERMINO"));
	    com.setNombreCeco(rs.getString("NOMCECO"));
	    com.setAux2(rs.getInt("ECO"));*/


		return com;
	}

}
