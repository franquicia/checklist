<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="../css/expancion/modal.css">
<link rel="stylesheet" type="text/css" href="../css/expancion/index.css">

<link rel="stylesheet" type="text/css" href="../css/expancion/detalleExpancion.css">
<link rel="stylesheet" type="text/css" href="../css/expancion/tabla-filtros.css">

<!-- Owl Stylesheets -->
<link rel="stylesheet" href="../css/expancion/owl.carousel.css">
<link rel="stylesheet" href="../css/expancion/owl.theme.default.css">


<script src="../js/expancion/jquery-3.3.1.js"></script>
<script src="../js/expancion/jquery.dataTables.js"></script>
<script src="../js/expancion/tabla.js"></script>

<title>PROTOCOLO DE SUCURSAL</title>
</head>
<body>


	<div class="header">
		<span class="titulagsBold spBold">REALIZA UNA BÚSQUEDA</span>
	</div>
	<div class="page">
		<div class="contHome top60">

			<div class="detped">
				<div class="detlab">
					<span class="spBold">Sucursal:</span>${sucursal[0]}
				</div>

				<div class="detlab">
					<span class="spBold">CECO:</span> ${sucursal[1]}
				</div>

				<div class="detlab">
					<span class="spBold">Zona:</span>${sucursal[2]}
				</div>

				<div class="detlab">
					<span class="spBold">Región:</span> ${sucursal[3]}
				</div>

				<div class="detlab">
					<span class="spBold">Tipo de Visita:</span> ${sucursal[4]}
				</div>

				<div class="detlab">
					<span class="spBold">Fecha:</span> ${sucursal[5]}
				</div>

				<div class="detlab">
					<span class="spBold">Calificación:</span> ${sucursal[6]}
				</div>
			</div>

			<div class="ovtabs">
			<c:choose>
			<c:when test="${tipoNegocio=='EKT'}">
			<div class="tabs">
					<a href="filtrosPadresExpansion.htm?zona=1&${url}" ${zonas[0]}>ELEKTRA</a> 
					<a href="filtrosPadresExpansion.htm?zona=2&${url}" ${zonas[1]}>BANCO AZTECA</a> 
					<a href="filtrosPadresExpansion.htm?zona=3&${url}" ${zonas[2]}>PRESTA PRENDA</a> 
					<a href="filtrosPadresExpansion.htm?zona=4&${url}" ${zonas[3]}>ÁREAS COMUNES</a> 
					<a href="filtrosPadresExpansion.htm?zona=5&${url}" ${zonas[4]}>GENERALES</a>
				</div>
			</c:when>
			<c:when test="${tipoNegocio=='OCC'}">
			<div class="tabs">
					 
					<a href="filtrosPadresExpansion.htm?zona=4&${url}" ${zonas[3]}>ÁREAS COMUNES</a> 
					<a href="filtrosPadresExpansion.htm?zona=5&${url}" ${zonas[4]}>GENERALES</a>
					<a href="filtrosPadresExpansion.htm?zona=6&${url}" ${zonas[5]}>CREDITO Y COBRANZA</a>
				</div>
			</c:when>
			<c:otherwise>
			 	<div class="tabs">
					<a href="filtrosPadresExpansion.htm?zona=1&${url}" ${zonas[0]}>ELEKTRA</a> 
					<a href="filtrosPadresExpansion.htm?zona=2&${url}" ${zonas[1]}>BANCO AZTECA</a> 
					<a href="filtrosPadresExpansion.htm?zona=3&${url}" ${zonas[2]}>PRESTA PRENDA</a> 
					<a href="filtrosPadresExpansion.htm?zona=4&${url}" ${zonas[3]}>ÁREAS COMUNES</a> 
					<a href="filtrosPadresExpansion.htm?zona=5&${url}" ${zonas[4]}>GENERALES</a>
				</div>
			
			</c:otherwise>
			</c:choose>
   
				
			</div>

			<div class="clear"></div>
			<br>

			<div class="marginTotal">
				<div class="tablahija">
					${tabla}
				</div>
			</div>

			<div class="pfixed">
				<div class="lfant">
					<a href="${urlBack}"> Regresar al Buscador </a>
				</div>
			</div>
		</div>
	</div>


</body>
</html>