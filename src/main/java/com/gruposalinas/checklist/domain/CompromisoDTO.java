package com.gruposalinas.checklist.domain;

public class CompromisoDTO {

	private int idCompromiso;
	private int idRespuesta;
	private String descripcion;
	private int estatus;
	private String fechaCompromiso;
	private int commit;
	private int idResponsable;
	private int idPregunta;
	private String desPregunta;
	private String fechaInicio;
	private int idCeco;
	private int idChecklist;
	private String nombreChecklist;
	private int idArbol;
	private String desRespuesta;
	private String nombreUsuario;
	private int idTipoPreg;
	private String economico;
	private String idUsuario; 
	private String evidencia;
	private String folio;
	private String accion;
	private int orden;
	private int idNegocio;

	public int getIdCompromiso() {
		return idCompromiso;
	}
	public void setIdCompromiso(int idCompromiso) {
		this.idCompromiso = idCompromiso;
	}
	public int getIdRespuesta() {
		return idRespuesta;
	}
	public void setIdRespuesta(int idRespuesta) {
		this.idRespuesta = idRespuesta;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public String getFechaCompromiso() {
		return fechaCompromiso;
	}
	public void setFechaCompromiso(String fechaCompromiso) {
		this.fechaCompromiso = fechaCompromiso;
	}
	public int getCommit() {
		return commit;
	}
	public void setCommit(int commit) {
		this.commit = commit;
	}
	public int getIdResponsable() {
		return idResponsable;
	}
	public void setIdResponsable(int idResponsable) {
		this.idResponsable = idResponsable;
	}
	public int getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	public String getDesPregunta() {
		return desPregunta;
	}
	public void setDesPregunta(String desPregunta) {
		this.desPregunta = desPregunta;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public int getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(int idCeco) {
		this.idCeco = idCeco;
	}
	public int getIdChecklist() {
		return idChecklist;
	}
	public void setIdChecklist(int idChecklist) {
		this.idChecklist = idChecklist;
	}
	public String getNombreChecklist() {
		return nombreChecklist;
	}
	public void setNombreChecklist(String nombreChecklist) {
		this.nombreChecklist = nombreChecklist;
	}
	public int getIdArbol() {
		return idArbol;
	}
	public void setIdArbol(int idArbol) {
		this.idArbol = idArbol;
	}
	public String getDesRespuesta() {
		return desRespuesta;
	}
	public void setDesRespuesta(String desRespuesta) {
		this.desRespuesta = desRespuesta;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public int getIdTipoPreg() {
		return idTipoPreg;
	}
	public void setIdTipoPreg(int idTipoPreg) {
		this.idTipoPreg = idTipoPreg;
	}
	public String getEconomico() {
		return economico;
	}
	public void setEconomico(String economico) {
		this.economico = economico;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public String getEvidencia() {
		return evidencia;
	}
	public void setEvidencia(String evidencia) {
		this.evidencia = evidencia;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	public int getIdNegocio() {
		return idNegocio;
	}
	public void setIdNegocio(int idNegocio) {
		this.idNegocio = idNegocio;
	}
	
	
}
