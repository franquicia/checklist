package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.TipoPreguntaDTO;
import com.gruposalinas.checklist.mappers.TipoPreguntaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class TipoPreguntaDAOImpl extends DefaultDAO implements TipoPreguntaDAO {

    private static Logger logger = LogManager.getLogger(TipoPreguntaDAOImpl.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtieneTipoPregunta;
    DefaultJdbcCall jdbcInsertaTipoPregunta;
    DefaultJdbcCall jdbcActualizaTipoPregunta;
    DefaultJdbcCall jdbcEliminaTipoPregunta;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TPREG")
                .withProcedureName("SP_SEL_G_TPREG")
                .returningResultSet("RCL_TPREG", new TipoPreguntaRowMapper());

        jdbcObtieneTipoPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TPREG")
                .withProcedureName("SP_SEL_TPREG")
                .returningResultSet("RCL_TPREG", new TipoPreguntaRowMapper());

        jdbcInsertaTipoPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TPREG")
                .withProcedureName("SP_INS_TPREG");

        jdbcActualizaTipoPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TPREG")
                .withProcedureName("SP_ACT_TPREG");

        jdbcEliminaTipoPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_TPREG")
                .withProcedureName("SP_DEL_TPREG");
    }

    @SuppressWarnings("unchecked")
    public List<TipoPreguntaDTO> obtieneTipoPregunta() throws Exception {

        Map<String, Object> out = null;
        List<TipoPreguntaDTO> listaTipoPregunta = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_TPREG.SP_SEL_G_TPREG}");

        listaTipoPregunta = (List<TipoPreguntaDTO>) out.get("RCL_TPREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener los Tipo Pregunta");
        } else {
            return listaTipoPregunta;
        }

        return listaTipoPregunta;

    }

    @SuppressWarnings("unchecked")
    public List<TipoPreguntaDTO> obtieneTipoPregunta(int idTipoPreg) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<TipoPreguntaDTO> listaTipoPregunta = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_TPREG", idTipoPreg);

        out = jdbcObtieneTipoPregunta.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_TPREG.SP_SEL_TPREG}");

        listaTipoPregunta = (List<TipoPreguntaDTO>) out.get("RCL_TPREG");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al obtener los Tipo Pregunta con id(" + idTipoPreg + ")");
        } else {
            return listaTipoPregunta;
        }

        return null;

    }

    public int insertaTipoPregunta(TipoPreguntaDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idTipoPreg = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CVETIPO", bean.getCvePregunta())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_ACTIVO", bean.getActivo());

        out = jdbcInsertaTipoPregunta.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PA_ADM_TPREG.SP_INS_TPREG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idTipoReturn = (BigDecimal) out.get("PA_TPREG");
        idTipoPreg = idTipoReturn.intValue();

        if (error == 1) {
            logger.info("Algo paso al insertar el Tipo de Pregunta");
        } else {
            return idTipoPreg;
        }

        return idTipoPreg;

    }

    public boolean actualizaTipoPregunta(TipoPreguntaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_TPREG", bean.getIdTipoPregunta())
                .addValue("PA_CVETIPO", bean.getCvePregunta())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_ACTIVO", bean.getActivo());

        out = jdbcActualizaTipoPregunta.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_TPREG.SP_ACT_TPREG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al actualizar Tipo Pregunta  id( " + bean.getIdTipoPregunta() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaTipoPregunta(int idTipoPreg) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_TPREG", idTipoPreg);

        out = jdbcEliminaTipoPregunta.execute(in);

        logger.info("Funcion ejecutada: {checklist.PA_ADM_TPREG.SP_ACT_TPREG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo paso al borrar el Tipo Pregunta id(" + idTipoPreg + ")");
        } else {
            return true;
        }

        return false;

    }

}
