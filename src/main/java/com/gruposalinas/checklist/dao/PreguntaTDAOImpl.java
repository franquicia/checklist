package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.mappers.PreguntaTRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PreguntaTDAOImpl extends DefaultDAO implements PreguntaTDAO {

    private static Logger logger = LogManager.getLogger(PreguntaTDAOImpl.class);

    DefaultJdbcCall jdbcObtienePreguntaTemp;
    DefaultJdbcCall jdbcInsertaPreguntaTemp;
    DefaultJdbcCall jdbcActualizaPreguntaTemp;
    DefaultJdbcCall jdbcEliminaPreguntaTemp;

    public void init() {

        jdbcObtienePreguntaTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPREGTEMP")
                .withProcedureName("SP_SEL_PREG_TEMP")
                .returningResultSet("RCL_PREG", new PreguntaTRowMapper());

        jdbcInsertaPreguntaTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPREGTEMP")
                .withProcedureName("SP_INS_PREG_TEMP");

        jdbcActualizaPreguntaTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPREGTEMP")
                .withProcedureName("SP_ACT_PREG_TEMP");

        jdbcEliminaPreguntaTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPREGTEMP")
                .withProcedureName("SP_DEL_PREG_TEMP");
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PreguntaDTO> obtienePreguntaTemp(int idPregunta) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<PreguntaDTO> listaPregunta = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREG", idPregunta);

        out = jdbcObtienePreguntaTemp.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PAADMPREGTEMP.SP_SEL_PREG_TEMP}");

        listaPregunta = (List<PreguntaDTO>) out.get("RCL_PREG");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener las Preguntas de la tabla temporal");
        } else {
            return listaPregunta;
        }

        return listaPregunta;
    }

    @Override
    public int insertaPreguntaTemp(PreguntaDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idPreg = 0;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDMODULO", bean.getIdModulo())
                .addValue("PA_IDT_PREG", bean.getIdTipo())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_DESCRIPCION", bean.getPregunta())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_NUM_REV", bean.getNumVersion())
                .addValue("PA_TIPO_MOD", bean.getTipoCambio())
                .addValue("PA_FIID_PREG", bean.getIdPregunta());

        out = jdbcInsertaPreguntaTemp.execute(in);

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal returnid = (BigDecimal) out.get("PA_FIID_PREG");
        idPreg = returnid.intValue();
        //System.out.println("id PREG" + idPreg);
        if (error == 1) {
            logger.info("Algo ocurrió al insertar la Pregunta en la tabla temporal");
        } else {
            return idPreg;
        }

        return 0;
    }

    @Override
    public boolean actualizaPreguntaTemp(PreguntaDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREG", bean.getIdPregunta())
                .addValue("PA_IDMODULO", bean.getIdModulo())
                .addValue("PA_IDT_PREG", bean.getIdTipo())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_DESCRIPCION", bean.getPregunta())
                .addValue("PA_NUM_REV", bean.getNumVersion())
                .addValue("PA_TIPO_MOD", bean.getTipoCambio());

        out = jdbcActualizaPreguntaTemp.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PAADMPREGTEMP.SP_ACT_PREG_TEMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar la Pregunta id( " + bean.getIdPregunta() + ") en la tabla temporal");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaPreguntaTemp(int idPregunta) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREG", idPregunta);

        out = jdbcEliminaPreguntaTemp.execute(in);

        logger.info("Funci�n ejecutada: {checklist.PAADMPREGTEMP.SP_DEL_PREG_TEMP}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar la pregunta id(" + idPregunta + ") en la tabla temporal");
        } else {
            return true;
        }

        return false;
    }

}
