package com.gruposalinas.checklist.domain;

public class DepuraBitacoraDTO {
	private int respEliminadas;
	private int bitaEliminadas;
	private int ejecucion;
	
	
	public int getRespEliminadas() {
		return respEliminadas;
	}
	public void setRespEliminadas(int respEliminadas) {
		this.respEliminadas = respEliminadas;
	}
	public int getBitaEliminadas() {
		return bitaEliminadas;
	}
	public void setBitaEliminadas(int bitaEliminadas) {
		this.bitaEliminadas = bitaEliminadas;
	}
	public int getEjecucion() {
		return ejecucion;
	}
	public void setEjecucion(int ejecucion) {
		this.ejecucion = ejecucion;
	}
	
	@Override
	public String toString() {
		return "DepuraBitacoraDTO [respEliminadas=" + respEliminadas + ", bitaEliminadas=" + bitaEliminadas
				+ ", ejecucion=" + ejecucion + "]";
	}
	
}
