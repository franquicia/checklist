package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.MovilInfoDTO;


public interface MovilInfoDAO {
	
	public List<MovilInfoDTO> obtieneInfoMovil(MovilInfoDTO bean) throws Exception;
	
	public boolean insertaInfoMovil(MovilInfoDTO bean) throws Exception;
	
	public boolean actualizaInfoMovil(MovilInfoDTO bean) throws Exception;
	
	public boolean eliminaInfoMovil(int idMovil) throws Exception;

}
