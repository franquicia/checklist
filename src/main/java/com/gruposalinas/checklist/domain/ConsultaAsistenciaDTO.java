package com.gruposalinas.checklist.domain;

public class ConsultaAsistenciaDTO {
	
	private String idUsuario;
	private String fimes;
	private String diasHabiles;
	private String asistencias;
	private String porcentaje;
	
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getFimes() {
		return fimes;
	}
	public void setFimes(String fimes) {
		this.fimes = fimes;
	}
	public String getDiasHabiles() {
		return diasHabiles;
	}
	public void setDiasHabiles(String diasHabiles) {
		this.diasHabiles = diasHabiles;
	}
	public String getAsistencias() {
		return asistencias;
	}
	public void setAsistencias(String asistencias) {
		this.asistencias = asistencias;
	}
	public String getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}
	@Override
	public String toString() {
		return "ConsultaAsistenciaDTO [idUsuario=" + idUsuario + ", fimes=" + fimes + ", diasHabiles=" + diasHabiles
				+ ", asistencias=" + asistencias + ", porcentaje=" + porcentaje + "]";
	}
	
	
	
	}
	
	
	
	

	
	
	