package com.gruposalinas.checklist.domain;

public class ParametroDTO {

	private String clave;
	private String valor;
	private String claveParametro;
	private int valorParametro;
	private int activo;
	
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
	public String getclaveParametro() {
		return claveParametro;
	}
	public void setclaveParametro(String claveParametro) {
		this.claveParametro = claveParametro;
	}
	
	public int getValorParametro() {
		return valorParametro;
	}
	public void setValorParametro(int valorParametro) {
		this.valorParametro = valorParametro;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	
}
