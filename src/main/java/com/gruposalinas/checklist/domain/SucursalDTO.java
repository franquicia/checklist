package com.gruposalinas.checklist.domain;

public class SucursalDTO {
	
	private int idSucursal;
	private int idPais;
	private int idCanal;
	private String idCeco;
	private String nuSucursal;	
	private String nombresuc;
	private double latitud;
	private double longitud;
	
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public int getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(int idSucursal) {
		this.idSucursal = idSucursal;
	}
	public int getIdPais() {
		return idPais;
	}
	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}
	public int getIdCanal() {
		return idCanal;
	}
	public void setIdCanal(int idCanal) {
		this.idCanal = idCanal;
	}
	public String getNombresuc() {
		return nombresuc;
	}
	public void setNombresuc(String nombresuc) {
		this.nombresuc = nombresuc;
	}
	public String getNuSucursal() {
		return nuSucursal;
	}
	public void setNuSucursal(String nuSucursal) {
		this.nuSucursal = nuSucursal;
	}
	public double getLatitud() {
		return latitud;
	}
	public void  setLatitud(double latitud) {
		this.latitud = latitud;
	}
	public double getLongitud() {
		return longitud;
	}
	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}
	

}
