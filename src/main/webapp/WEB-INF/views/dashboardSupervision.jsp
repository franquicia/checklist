<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>Búsqueda de Sucursal</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervision/estilos.css">
</head>

<body ng-app="app" ng-controller="validarForm">

  <div class="page">
    <div class="header">
      <div class="headerMenu">
        <a href="#" id="hamburger"><img src="${pageContext.request.contextPath}/img/supervision/btn_hamburguer.svg" alt="Menú principal" class="imgHamburgesa"></a>
      </div>
      <div class="headerLogo">
        <a href="inicio.htm"><img src="${pageContext.request.contextPath}/img/supervision/logo.svg" id="imgLogo"></a>
      </div>
      <div class="headerUser">
        
      </div>

    </div>
    <div class="MenuUser">
      <div class="MenuUser1">
        <a href="login.htm" class="selMenu">
          <div>Salir</div>
          <div><img src="${pageContext.request.contextPath}/img/supervision/ico5.svg" class="imgConfig"></div>
        </a>
      </div>
    </div>
    <div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="indexSupervision.htm">Reporte de Supervisores</a> 
		</div>
    	Dashboard
    </div>

	<!-- Menu -->
    <div id="effect" class="ui-widget-content ui-corner-all">
      <div id="menuPrincipal">
        <div class="header-usuario">
          <div id="foto"><img src="${pageContext.request.contextPath}/img/supervision/logo1.svg" class="imgLogo"></div>
          <div id="inf-usuario">
            <span>Portal</span><br>
            Menú Principal<br>
          </div>
        </div>
       <%--  <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la página">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> --%>
        <div id="menu">
        	<tiles:insertTemplate template="/WEB-INF/templates/templateMenuAseguramiento.jsp" flush="true">
				<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuAseguramiento.jsp" />
			</tiles:insertTemplate>
          <!-- <ul class="l-navegacion nivel1">
          		<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Asegurador</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>Búsqueda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>Búsqueda Asegurador</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>				
				<li>
					<a href="indexGaleria.htm"><div>Galería de Evidencias</div></a>
				</li>
			</ul> -->
        </div>
      </div>
    </div>

    <!-- Contenido -->
    <div class="contSecc">
    
    <div class="liga">
		<a href="#" onclick="javascript:window.history.back()" class="liga">
		<img style="height: 15px; width: 15px;" src="${pageContext.request.contextPath}/img/supervision/arrowmleft.png">
		<span>PÁGINA ANTERIOR</span>
		</a>
		</div>
    
		<div class="titSec"></div>
		<div class="contBalco">
			<div class="datSuc">
				<div>
					<div><strong>Asegurador:</strong><c:out value="${nombre}" /></div>
					<div><strong>Número de Empleado:</strong> <c:out value="${idUsuario}" /></div>
					<div><strong>Fecha:</strong> <c:out value="${fecha}" /></div>
					<div><strong>Sucursales Visitadas:</strong> <c:out value="${sucVisitada}" /></div>
					<div><strong>Sucursales por Visitar:</strong> <c:out value="${sucFaltantes}" /></div>
					<div><strong>Total de Sucursales:</strong> <c:out value="${sucAsignada}" /></div>
				</div>
			</div>
		</div>
		
		<c:set var="m1" value="${mes1}"/>
		<c:set var="m2" value="${mes2}"/>
		<c:set var="m3" value="${mes3}"/>
		<c:set var="m4" value="${mes4}"/>
		<c:set var="m5" value="${mes5}"/>
		<c:set var="m6" value="${mes6}"/>
		<c:set var="m7" value="${mes7}"/>
		<c:set var="m8" value="${mes8}"/>
		<c:set var="m9" value="${mes9}"/>
		<c:set var="m10" value="${mes10}"/>
		<c:set var="m11" value="${mes11}"/>
		<c:set var="m12" value="${mes12}"/>

		<script type="text/javascript">
			<c:set var="visitadas" value="${visitadas}"/> 
			var visitadas = '<c:out value="${visitadas}"/>';
			<c:set var="visitar" value="${visitar}"/> 
			var visitar = '<c:out value="${visitar}"/>';

			//alert(visitadas+" , "+visitar);
			
			var m1 = '<c:out value="${m1}"/>';
			var m2 = '<c:out value="${m2}"/>';
			var m3 = '<c:out value="${m3}"/>';
			var m4 = '<c:out value="${m4}"/>';
			var m5 = '<c:out value="${m5}"/>';
			var m6 = '<c:out value="${m6}"/>';
			var m7 = '<c:out value="${m7}"/>';
			var m8 = '<c:out value="${m8}"/>';
			var m9 = '<c:out value="${m9}"/>';
			var m10 = '<c:out value="${m10}"/>';
			var m11 = '<c:out value="${m11}"/>';
			var m12 = '<c:out value="${m12}"/>';

			var mesCalif1 = 0;
            var mesCalif2 = 0;
            var mesCalif3 = 0;
            var mesCalif4 = 0;
            var mesCalif5 = 0;
            var mesCalif6 = 0;
            var mesCalif7 = 0;
            var mesCalif8 = 0;
            var mesCalif9 = 0;
            var mesCalif10 = 0;
            var mesCalif11 = 0;
            var mesCalif12 = 0;

            var mesVis1 = 0;
            var mesVis2 = 0;
            var mesVis3 = 0;
            var mesVis4 = 0;
            var mesVis5 = 0;
            var mesVis6 = 0;
            var mesVis7 = 0;
            var mesVis8 = 0;
            var mesVis9 = 0;
            var mesVis10 = 0;
            var mesVis11 = 0;
            var mesVis12 = 0;


		</script>
		
		<div class="divTablero5 " >
            <div class="divVertical divGraficaH" >
                <canvas id="canvas-vertical" class="graVerical" style="height: 145px; width: 100px; "></canvas>
                <canvas id="canvas-vertical3" class="graVerical" style="height: 145px; width: 100px;"></canvas>
            </div>
        </div>

        <div class="clear"></div>
		<div class="divTablero1" >
            <div ><canvas id="chart-dona" class="canvasDona"/></div>
            <div class="divVertical">
                <canvas id="canvas-vertical2" class="graVerical" style="height: 145px; width: 100px;"></canvas>
            </div>
        </div>
        <div class="clear"></div>
       
		
		<div class="titSec">Genera una consulta de Asistencia</div>
		<c:url value="/central/asistenciaSupervision.htm" var="asistenciaSupervision" />
		<form:form method="POST" action="${asistenciaSupervision}" model="command" name="form" id="form">
		<div class="gris">
			<div class="divCol4 flexJusCen">
				
					<div class="col4 col4B">
						Fecha de Inicio<br>
						<input type="text" placeholder="DD/MM/AAAA" class="datapicker1 date" id="datepicker" autocomplete="off">
					</div>
					<div class="col4 w30A"><div><strong>a</strong></div></div>
					<div class="col4 col4B">
						Fecha de Fin<br>
						<input type="text" placeholder="DD/MM/AAAA" class="datapicker1 date" id="datepicker1" autocomplete="off">
					</div>
					<div class="col4 col4B col4Buscar">
						<br>
						<a href="#" class="btn btnBuscar" onclick="return validaFechas();">Buscar</a>
					</div>
					<input id="idUsuario" name="idUsuario" type="hidden" value="${idUsuario}"/>
					<input id="fechaInicio" name="fechaInicio" type="hidden" value=""/>
					<input id="fechaFin" name="fechaFin" type="hidden" value=""/>
				
			</div>
		</div>
		</form:form>
		
	</div>	<!-- Fin conSecc -->
	
	<!-- Footer -->
	<div class="footer">
		<div>Grupo Salinas</div>
		<div>Actualización Febrero 2019</div>
	</div>
</div><!-- Fin page -->
<script type="text/javascript">
function validaFechas(){	
	var f1=document.getElementById("datepicker").value;
	var f2=document.getElementById("datepicker1").value;
	
	//alert("PAso");
	
 	var Fecha1 = stringToDate(f1,"dd/MM/yyyy","/");
 	var Fech2 = stringToDate(f2,"dd/MM/yyyy","/");
 	var FHoy = new Date();
	///alert("PAso"+ FHoy);
	
    var flag1=false;
 	var flag2=false;
	
	if (isNaN(Fecha1)){
		alert("Debes colocar fecha inicio");
		return false;
	}
	else{
		if(Fecha1<FHoy){
		flag1=true;
		}else{
			alert("La fecha inicio no puede ser mayor que hoy");
			return false;
		}
	}
	
	if (isNaN(Fech2)){
		alert("Debes colocar fecha fin");
		return false;
	}
	else{
		if(Fech2<FHoy){
			flag2=true;
			}else{
				alert("La fecha fin no puede ser mayor que hoy");
				return false;
			}
	} 
	
	//alert("LLego");
	if(flag1==true && flag2==true){
		if(Fecha1>Fech2){
			alert("La fecha de inicio no debe ser mayor que la de fin");
			return false;
		} 
		else{
			document.getElementById('fechaInicio').value = f1;
			document.getElementById('fechaFin').value = f2;
			form = document.getElementById("form");
			form.submit();
		}
	} 
	
	return false;

	
}

function stringToDate(_date,_format,_delimiter)
{
            var formatLowerCase=_format.toLowerCase();
            var formatItems=formatLowerCase.split(_delimiter);
            var dateItems=_date.split(_delimiter);
            var monthIndex=formatItems.indexOf("mm");
            var dayIndex=formatItems.indexOf("dd");
            var yearIndex=formatItems.indexOf("yyyy");
            var month=parseInt(dateItems[monthIndex]);
            month-=1;
            var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
            return formatedDate;
}
</script>
</body>
</html>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/custom-file-input.js"></script><!--Unpload -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/supervision/funciones.js"></script>

<!-- Graficas -->

<script src="${pageContext.request.contextPath}/js/supervision/Chart.bundle.min.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/utils.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/Chart.PieceLabel.js"></script>
<script src="${pageContext.request.contextPath}/js/supervision/grafica.js"></script>
    