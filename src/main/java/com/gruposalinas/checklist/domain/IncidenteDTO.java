package com.gruposalinas.checklist.domain;

public class IncidenteDTO {
	
	private String idPlantilla;
	private String director;
	private String descripcion;
	private String prioridad;
	private String tipoError;
	private String id;
	private boolean adjunto;
	private String sucursal;
	
	public boolean isAdjunto() {
		return adjunto;
	}
	public void setAdjunto(boolean adjunto) {
		this.adjunto = adjunto;
	}
	public String getIdPlantilla() {
		return idPlantilla;
	}
	public void setIdPlantilla(String idPlantilla) {
		this.idPlantilla = idPlantilla;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getPrioridad() {
		return prioridad;
	}
	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}
	public String getTipoError() {
		return tipoError;
	}
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	
}
