package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;

public interface ChecklistTDAO {

	public List<ChecklistDTO> buscaChecklistTemp(int idChecklist) throws Exception;
	
	public boolean actualizaChecklistTemp(ChecklistDTO bean) throws Exception;

	public boolean eliminaChecklistTemp(int idCheckList) throws Exception;
	
	public int insertaChecklist(ChecklistProtocoloDTO checklistProtocoloDTO) throws Exception;
	
	public boolean actualizaEdo(int idChecklist, int idEdo) throws Exception;
	
	public boolean ejecutaAutorizados()throws Exception;


}
