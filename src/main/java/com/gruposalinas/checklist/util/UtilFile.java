package com.gruposalinas.checklist.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UtilFile {

	@SuppressWarnings("unused")
	private static final Logger logger = LogManager.getLogger(UtilFile.class);
	/**
	 * 
	 * Escribe un fichero, por default (cuando definedPath viene null) el sistema sabe donde generar la imagen
	 * Se le puede indicar la ruta en definedPath
	 * 
	 * @param file
	 * @param definedPath
	 */
	
	/*
	public static ImagenDTO writeFile(MultipartFile file, String definedPath){

		ImagenDTO imagen = new ImagenDTO();
		definedPath = getPathFromContentType( definedPath, file.getContentType() );
		String ruta = rootDirectory() + definedPath;
		Path p = Paths.get(ruta);
		imagen.setRuta(rootDirectory() + "Sociounico" + p.toString());
		imagen.setNombre( UtilString.normalize( file.getOriginalFilename() ) );
		imagen.setExtension( UtilString.getFileExtension( file.getOriginalFilename() ) );
		imagen.setNombre( imagen.getNombre().replaceAll(" ", "").replace( imagen.getExtension(),"") + "_" +  UtilDate.getSysDate("yyyyMMddhhmmss") );
		
		if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                File fRuta = new File( imagen.getRuta() ); 
                File f = new File( imagen.getRuta() + "/" + imagen.getNombre() + imagen.getExtension());
                fRuta.mkdirs();
                BufferedOutputStream stream = new BufferedOutputStream( new FileOutputStream( f ) );
                
                stream.write(bytes);
                stream.close();
                
                imagen.setOk( true );
                imagen.setRuta(rootDirectory() + "sociounico" + p.toString());
            } catch (Exception e) {
            	logger.info("No fue posible crear el archivo - mensaje: " + e.getMessage());
            }
        } else {
        	logger.info("No fue posible crear el archivo");
        }
		return imagen;
	}
	
	public static ImagenDTO getInfoFile(MultipartFile file, String definedPath){
		ImagenDTO imagen = new ImagenDTO();
		InputStream is = null;
		try {
			is = file.getInputStream();
			
//			if(is != null){
//				String mimeType = URLConnection.guessContentTypeFromStream(is);
//				 
//				if(mimeType != null && (mimeType.contains("image/gif") || mimeType.contains("image/jpeg") || mimeType.contains("image/png") ||
//						mimeType.contains("video/avi") || mimeType.contains("video/msvideo") || mimeType.contains("video/mpeg") ||
//						mimeType.contains("video/quicktime") || mimeType.contains("application/pdf") || mimeType.contains("video/3gpp"))){
					definedPath = getPathFromContentType( definedPath, file.getContentType() );
					String ruta = rootDirectory() + definedPath;
					Path p = Paths.get(ruta);
					imagen.setRuta(p.toString());
					imagen.setNombre( UtilString.normalize( file.getOriginalFilename() ) );
					imagen.setExtension( UtilString.getFileExtension( file.getOriginalFilename() ) );
					imagen.setNombre( imagen.getNombre().replaceAll(" ", "").replace( imagen.getExtension(),"") );
					imagen.setOk( true );
//				} else {
//					imagen.setOk( false );
//				}
//			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return imagen;
	}
	
	public static String rootDirectory(){
		String root = File.listRoots()[0].getAbsolutePath();
        return root;
	}
	
	/**Helper Class - Para almacenar propiedades del último archivo uploadead**/
	
	
	/*public class UploadedFile {
	    public int length;
	    public byte[] bytes;
	    public String name;
	    public String type;
		public int getLength() {
			return length;
		}
		public void setLength(int length) {
			this.length = length;
		}
		public byte[] getBytes() {
			return bytes;
		}
		public void setBytes(byte[] bytes) {
			this.bytes = bytes;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
	}
	
	public static String getPathFromContentType(String definedPath, String contentType){
		String ruta = "";
		
		if(contentType.contains("image")){
			ruta = "/imagenes";
		} else if (contentType.contains("video")) {
			ruta = "/videos";
		} else if (contentType.contains("application") || contentType.contains("text")) {
			ruta = "/documentos";
		}
		
		definedPath = (String) UtilObject.decode(
			definedPath, 
			null, //SGConstantes.SOCIO_UNICO +
			"/sociosgenerosos" + ruta + "/" + UtilDate.getAnioActual() , 
			definedPath
		);
		
		return definedPath;
	}*/
}