package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.SucursalDTO;

public class SucursalPasoRowMapper implements RowMapper<SucursalDTO> {

	public SucursalDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		SucursalDTO sucursalDTO = new SucursalDTO();
		
		sucursalDTO.setIdSucursal(rs.getInt("FISUCURSAL_ID"));
		sucursalDTO.setIdPais(rs.getInt("FIPAIS"));
		sucursalDTO.setIdCanal(rs.getInt("FICANAL"));
		sucursalDTO.setNuSucursal(rs.getString("FISUCURSAL"));
		sucursalDTO.setNombresuc(rs.getString("FCNOMBRECC"));
		sucursalDTO.setLongitud(rs.getDouble("FCLONGITUDE"));
		sucursalDTO.setLatitud(rs.getDouble("FCLATITUDE"));
		
		return sucursalDTO;
	} 

}
