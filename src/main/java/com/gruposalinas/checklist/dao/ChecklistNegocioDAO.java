package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ChecklistNegocioDTO;


public interface ChecklistNegocioDAO {
	
	public List<ChecklistNegocioDTO> obtieneChecklistNegocio(String checklist, String negocio ) throws Exception;
	
	public boolean insertaChecklistNegocio(int checklist, int negocio)throws Exception;
	
	public boolean eliminaChecklistNegocio(int checklist, int negocio)throws Exception;

}
