package com.gruposalinas.checklist.config.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.data.sources.config.GenericConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

/**
 *
 * @author cescobarh
 */
@Import(GenericConfig.class)
@Configuration
@ComponentScan("com.gruposalinas")
@ImportResource("/WEB-INF/spring/app-config.xml")
public class ConfigApp {

    private final Logger logger = LogManager.getLogger();

    public ConfigApp() {
        logger.info("Loading Base " + getClass().getName() + "...!");
    }

}
