package com.gruposalinas.checklist.domain;

public class InformeGeneralDTO {
	


		private String idCeco;
		private String fecha;
		private int idUsuario;
		private String rutaInforme;
		private int idEstatus;
		private String estatus;
		private int numeroBitacora;

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getRutaInforme() {
        return rutaInforme;
    }

    public void setRutaInforme(String rutaInforme) {
        this.rutaInforme = rutaInforme;
    }

    public int getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(int idEstatus) {
        this.idEstatus = idEstatus;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public int getNumeroBitacora() {
        return numeroBitacora;
    }

    public void setNumeroBitacora(int numeroBitacora) {
        this.numeroBitacora = numeroBitacora;
    }

    @Override
    public String toString() {
        return "ReporteGeneralDTO{" + "idCeco=" + idCeco + ", fecha=" + fecha + ", idUsuario=" + idUsuario + ", rutaInforme=" + rutaInforme + ", idEstatus=" + idEstatus + ", estatus=" + estatus + ", numeroBitacora=" + numeroBitacora + '}';
    }

	}