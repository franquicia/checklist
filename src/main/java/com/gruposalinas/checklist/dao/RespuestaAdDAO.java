package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.RespuestaAdDTO;

public interface RespuestaAdDAO {

	public List<RespuestaAdDTO> buscaRespADP(String idRespAD, String idResp) throws Exception;

	public int insertaRespAD(RespuestaAdDTO bean) throws Exception;
	
	public boolean actualizaRespAD(RespuestaAdDTO bean) throws Exception;
	
	public boolean eliminaRespAD(int idRespAD) throws Exception;
	
}
