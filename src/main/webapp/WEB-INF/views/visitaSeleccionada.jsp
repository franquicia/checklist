<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
<title>Galer&iacute;a</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/supervisionPI2/estilos.css">
<!-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/supervisionPI2/colorbox.css"> -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/supervisionPI2/justifiedGallery.css">
	
	<script type="text/javascript">
	var rutas = "";
	var datos="${paso}";
	</script>
	
</head>

<body ng-app="app" ng-controller="validarForm">

	<div class="page">
		<div class="header">
			<div class="headerMenu">
				<a href="#" id="hamburger"><img
					src="${pageContext.request.contextPath}/img/supervisionPI2/btn_hamburguer.svg"
					alt="Menú principal" class="imgHamburgesa"></a>
			</div>
			<div class="headerLogo">
				<a href="inicio.htm"><img
					src="${pageContext.request.contextPath}/img/supervisionPI2/logo.svg"
					id="imgLogo"></a>
			</div>
			<div class="headerUser"></div>

		</div>
		<div class="MenuUser">
			<div class="MenuUser1">
				<a href="login.html" class="selMenu">
					<div>Salir</div>
					<div>
						<img
							src="${pageContext.request.contextPath}/img/supervisionPI2/ico5.svg"
							class="imgConfig">
					</div>
				</a>
			</div>
		</div>
		<div class="clear"></div>

		<div class="titulo">
			<div class="ruta">
				<a href="indexGaleria.htm">Galer&iacute;a de Evidencias</a>
			</div>
			Galer&iacute;a
		</div>

		<!-- Menu -->
		<div id="effect" class="ui-widget-content ui-corner-all">
			<div id="menuPrincipal">
				<div class="header-usuario">
					<div id="foto">
						<img
							src="${pageContext.request.contextPath}/img/supervisionPI2/logo1.svg"
							class="imgLogo">
					</div>
					<div id="inf-usuario">
						<span>Portal</span><br> Men&uacute; Principal<br>
					</div>
				</div>
				<%-- <div id="buscador">
          <form id="miForm" name="miForm" action="" method="get" onsubmit="return valida(this)">
            <div class="w87"><input type="text" id="busca" placeholder="Busca en la página">
              <input type="submit" class="buscar" value=""></div>
          </form>
        </div> --%>
				<div id="menu">
					
        <c:if test="${menu==1}">
        	<ul class="l-navegacion nivel1">
				<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galer&iacute;a de Evidencias</div></a>
				</li>
			</ul>
        </c:if>
        <c:if test="${menu==0}">
	        <tiles:insertTemplate template="/WEB-INF/templates/templateMenuAseguramiento.jsp" flush="true">
				<tiles:putAttribute name="menu" value="/WEB-INF/templates/templateMenuAseguramiento.jsp" />
			</tiles:insertTemplate>
        	<!--
        	 <ul class="l-navegacion nivel1">
				<li>
					<a href="inicio.htm"><div>Inicio</div></a>
				</li>
          		<li>
					<a href="reporteCeco.htm"><div>Reporte Sucursal</div></a>
				</li>
          		<li>
					<a href="reporteUsuario.htm"><div>Reporte Asegurador</div></a>
				</li>
          		<li>
					<a href="reporteAsistencia.htm"><div>Reporte Asistencia</div></a>
				</li>
				<li>
					<a href="indexSupervision.htm"><div>B&uacute;squeda Sucursal</div></a>
				</li>
				<li>
					<a href="busquedaSupervisorSupervision.htm"><div>B&uacute;squeda Asegurador</div></a>
				</li>
				<li>
					<a href="listaSucursales.htm"><div>Folios de Mantenimiento</div></a>
				</li>
				<li>
					<a href="descargaBase.htm"><div>Descarga Base de Protocolos</div></a>
				</li>
				<li>
					<a href="getPerfilSuperGenerica.htm"><div>Perfil Supervision Generica</div></a>
				</li>
				<li>
					<a href="indexGaleria.htm"><div>Galer&iacute;a de Evidencias</div></a>
				</li>
				 <li>
					<a href="#"><div>Administrador</div></a>
					<ul class="nivel2" style="height: 350px; overflow-y: auto;">
			            <li><a href="asignaciones.htm"><div>Asignación de Perfiles</div></a></li>
			            <li><a href="asignaPersonal.htm"><div>Asignación de Personal</div></a></li>
			            <li><a href="asignaCecos.htm"><div>Asignación de Cecos</div></a></li>
			            <li><a href="asignaCecosMasiva.htm"><div>Asignación de Cecos Masiva</div></a></li>
		          	</ul>
				</li> 
			</ul>
			-->
        </c:if>
          
				</div>
			</div>
		</div>

		<!-- Contenido -->
		<div class="contSecc">

			<div class="liga">
				<a href="#" onclick="javascript:history.go(-1)" class="liga"> <img
					id="download" ; style="height: 15px; width: 15px;"
					src="${pageContext.request.contextPath}/img/supervision/arrowmleft.png">
					<span>P&Aacute;GINA ANTERIOR</span>
				</a>
			</div>

			<c:url value="/reporteService/postGenerateZIP.json" var="descarga" />
			<form:form method="POST" action="${descarga}" model="command"
				name="formDescarga" id="formDescarga">

				<div class="titSec titRGaleria">
					<div>
						<span class="txtVerde">${idCeco} - ${nomSucursal} - ${nomChecklist} -</span> ${fecha}
					</div>
					
					<div>
						<a href="#" onclick="return descarga(rutas,datos);"> 
						<img src="${pageContext.request.contextPath}/img/supervisionPI2/icoDownload.svg" class="icoDescarga">
						</a>
					</div>

					<script type="text/javascript">
						var arr = [];
						rutas = '{"nomArchivoRetorno":"${idCeco}","archivos":[';

						if(datos==0){
							//alert("Sin datos");
						}else if(datos==1){
							//alert("Entra on load");
							<c:forEach var="listaRutas" items="${listaRutas}">
							arr.push("${listaRutas}");
							rutas = rutas + '{"nomArchivo":"${listaRutas}"}';
							if(arr.length==0){		
								rutas="";
								//alert("Lista vacia");
							}else if (arr.length >= 1) {
								rutas = rutas + ',';
							}
							</c:forEach>
							if(rutas.endsWith(",")){
								rutas=rutas.slice(0,-1);
							}
							rutas = rutas + ']}';						
							//alert("JSON: " + rutas);
						}
					</script>
					<input id="contenido" name="contenido" type="hidden" value=""/>
				</div>
			</form:form>
			
		<c:choose>
		
		<c:when test="${paso == 1}">
			<div class="blanco">
				<c:url value="/central/imagenSeleccionada.htm" var="envio" />
				<form:form method="POST" action="${envio}" model="command"
					name="formImg" id="formImg">

					<div id="galeria01" class="justified-gallery"
						style="height: 381.873px;">

						<c:forEach var="listaEvidencias" items="${listaEvidencias}">

							<a href="#" class="jg-entry entry-visible "
								style="width: 220px; height: 130px;"
								onclick="javascript:visorImg('${listaEvidencias.ruta}')">
								<img src="${listaEvidencias.ruta}" style="width: 220px; height: 131px;">
								<div class="caption">&nbsp;</div>
							</a>

							<%-- <a href="#" class="jg-entry entry-visible" onclick="javascript:visorImg('${pageContext.request.contextPath}/img/supervisionPI2/galeria/1.jpg')">
					<img src="${pageContext.request.contextPath}/img/supervisionPI2/galeria/1.jpg" style="width: 180px; height: 130px; ">
				<div class="caption">&nbsp;</div></a>

				<a href="#" class="jg-entry entry-visible " style="width: 220px; height: 130px;" onclick="javascript:visorImg('${pageContext.request.contextPath}/img/supervisionPI2/galeria/2.jpg')">
					<img src="${pageContext.request.contextPath}/img/supervisionPI2/galeria/2.jpg" style="width: 220px; height: 131px; ">
				<div class="caption">&nbsp;</div></a>

				<a href="#" class="jg-entry entry-visible " style="width: 390px; height: 130px;" onclick="javascript:visorImg('${pageContext.request.contextPath}/img/supervisionPI2/galeria/3.jpg')">
					<img src="${pageContext.request.contextPath}/img/supervisionPI2/galeria/3.jpg" style="width: 390px; height: 131px; ">
				<div class="caption">&nbsp;</div></a>

				<a href="#" class="jg-entry entry-visible " style="width: 186px; height: 130px;" onclick="javascript:visorImg('${pageContext.request.contextPath}/img/supervisionPI2/galeria/4.jpg')">
					<img src="${pageContext.request.contextPath}/img/supervisionPI2/galeria/4.jpg" style="width: 186px; height: 131px;">
				<div class="caption">&nbsp;</div></a>

				<a href="#" class="jg-entry entry-visible " style="width: 196px; height: 130px; " onclick="javascript:visorImg('${pageContext.request.contextPath}/img/supervisionPI2/galeria/5.jpg')">
					<img src="${pageContext.request.contextPath}/img/supervisionPI2/galeria/5.jpg" style="width: 196px; height: 131px;">
				<div class="caption">&nbsp;</div></a> --%>

						</c:forEach>

						<input id="ruta" name="ruta" type="hidden" value="" />

					</div>
					<!-- Fin galeria01 -->

				</form:form>

			</div>
		</c:when>
		
		<c:when test="${paso == 0}">
			<div class="titSec"><span ><b> No se encontraron im&aacute;genes para esta visita.</b></span>
		</c:when>
		
		</c:choose>		

		</div>
		<!-- Fin conSecc -->

		<!-- Footer -->
		<div class="footer">
			<div>Grupo Salinas</div>
			<div>Actualizaci&oacute;n Noviembre 2019</div>
		</div>
	</div>
	<!-- Fin page -->

</body>
</html>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/supervisionPI2/jquery-1.12.4.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/supervisionPI2/jquery.dropkick.js"></script>
<!--Select -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/supervisionPI2/content_height.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/supervisionPI2/jquery-ui.js"></script>
<script
	src="${pageContext.request.contextPath}/js/supervisionPI2/jquery.simplemodal.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/supervisionPI2/angular.min.js"></script>
<script
	src="${pageContext.request.contextPath}/js/supervisionPI2/custom-file-input.js"></script>
<!--Unpload -->

<!-- <script src="${pageContext.request.contextPath}/js/supervisionPI2/jquery.colorbox-min.js"></script>-->
<script
	src="${pageContext.request.contextPath}/js/supervisionPI2/jquery.justifiedGallery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/supervisionPI2/funciones.js"></script>

<!-- Libreria para zip -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/supervisionPI2/jszip.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/supervisionPI2/jszip-utils.js"></script>

<script type="text/javascript">
	function visorImg(ruta) {
		document.getElementById("ruta").value = ruta;
		//alert("Ruta: "+document.getElementById("ruta").value);
		form = document.getElementById("formImg");
		form.submit();
	}

	function descarga(contenido, datos) {
		if(datos==0){
			alert("Sin datos");
		}else{
			//alert("Entra");
			document.getElementById("contenido").value = contenido;
			form = document.getElementById("formDescarga");
			form.submit();
		}
	}
</script>
