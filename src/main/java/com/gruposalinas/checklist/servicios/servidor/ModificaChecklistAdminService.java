package com.gruposalinas.checklist.servicios.servidor;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.AdmPregZonasBI;
import com.gruposalinas.checklist.business.AdmZonasBI;
import com.gruposalinas.checklist.business.ArbolRespAdiEviAdmBI;
import com.gruposalinas.checklist.business.CheckAutProtoBI;
import com.gruposalinas.checklist.business.CheckSoporteAdmBI;
import com.gruposalinas.checklist.business.ChecklistBI;
import com.gruposalinas.checklist.business.PosiblesAdmBI;
import com.gruposalinas.checklist.business.PreguntaAdmBI;
import com.gruposalinas.checklist.business.VersionCheckAdmBI;
import com.gruposalinas.checklist.dao.BitacoraCheckUsuAdmDAO;
import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.domain.AsignaExpDTO;
import com.gruposalinas.checklist.domain.CheckAutoProtoDTO;
import com.gruposalinas.checklist.domain.CheckSoporteAdmDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistPreguntaDTO;
import com.gruposalinas.checklist.domain.ChecklistUsuarioDTO;
import com.gruposalinas.checklist.domain.EvidenciaDTO;
import com.gruposalinas.checklist.domain.ModuloDTO;
import com.gruposalinas.checklist.domain.PosiblesDTO;
import com.gruposalinas.checklist.domain.PreguntaDTO;
import com.gruposalinas.checklist.domain.RespuestaAdDTO;
import com.gruposalinas.checklist.domain.RespuestaDTO;
import com.gruposalinas.checklist.domain.TipoChecklistDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;
import com.gruposalinas.checklist.domain.VersionExpanDTO;
import com.gruposalinas.checklist.resources.FRQAuthInterceptor;

@Controller
@RequestMapping("/catalogosAdmService")
public class ModificaChecklistAdminService {
	@Autowired
	PreguntaAdmBI preguntaAdmBI;
	@Autowired
	BitacoraCheckUsuAdmDAO bitacoraCheckUsuAdmDAO;
	@Autowired
	ArbolRespAdiEviAdmBI arbolRespAdiEviAdmBI;
	@Autowired
	CheckSoporteAdmBI  checkSoporteAdmBI;
	@Autowired
	CheckAutProtoBI checkAutProtoBI;
	@Autowired
	PosiblesAdmBI posiblesAdmBI;
	@Autowired
	ChecklistBI checklistBI;
	@Autowired
	VersionCheckAdmBI versionCheckAdmBI;
	@Autowired
	AdmPregZonasBI admPregZonasBI;
	@Autowired
	AdmZonasBI admZonasBI;
	
	private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);


	// http://localhost:8080/checklist/catalogosAdmService/updateModulo.json?idModulo=<?>&idModuloPadre=<?>&nombreMod=<?>
	@RequestMapping(value = "/updateModulo", method = RequestMethod.GET)
	public ModelAndView updateModulo(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String idModulo = request.getParameter("idModulo");
		String idModuloPadre = request.getParameter("idModuloPadre");

		char[] ca = { '\u0061', '\u0301' };
		char[] ce = { '\u0065', '\u0301' };
		char[] ci = { '\u0069', '\u0301' };
		char[] co = { '\u006F', '\u0301' };
		char[] cu = { '\u0075', '\u0301' };
		// mayusculas
		char[] c1 = { '\u0041', '\u0301' };
		char[] c2 = { '\u0045', '\u0301' };
		char[] c3 = { '\u0049', '\u0301' };
		char[] c4 = { '\u004F', '\u0301' };
		char[] c5 = { '\u0055', '\u0301' };
		char[] c6 = { '\u006E', '\u0303' };

		String nombreModulo = request.getParameter("nombreMod");

		String nombreModuloCod = "";
		// System.out.println("setPregunta1 " + nombreModulo);
		if (nombreModulo.contains(String.valueOf(ca)) || nombreModulo.contains(String.valueOf(ce))
				|| nombreModulo.contains(String.valueOf(ci)) || nombreModulo.contains(String.valueOf(co))
				|| nombreModulo.contains(String.valueOf(cu)) || nombreModulo.contains(String.valueOf(c1))
				|| nombreModulo.contains(String.valueOf(c2)) || nombreModulo.contains(String.valueOf(c3))
				|| nombreModulo.contains(String.valueOf(c4)) || nombreModulo.contains(String.valueOf(c5))
				|| nombreModulo.contains(String.valueOf(c6))) {

			String rr = nombreModulo.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
					.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
					.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
					.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
					.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
					.replaceAll(String.valueOf(c6), "ñ");
			// System.out.println("rr " + rr);

			nombreModuloCod = rr;
			rr = null;
		} else {
			// System.out.println("setPregunta " + nombreModulo);

			nombreModuloCod = nombreModulo;

		}

		ModuloDTO modulo = new ModuloDTO();
		modulo.setIdModuloPadre(Integer.parseInt(idModuloPadre));
		modulo.setNombre(nombreModuloCod);
		modulo.setIdModulo(Integer.parseInt(idModulo));
		boolean res = preguntaAdmBI.actualizaModulo(modulo);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "MODULO MODIFICADO");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosAdmService/updateCheckServiceCom.json?idChecklist=<?>&fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>&idUsuario=<?>&periodicidad=<?>&dia=<?>&version=<?>&ordenGrupo=<?>&ponderacionTot=<?>&clasifica=<?>&idProtocolo=<?>
	@RequestMapping(value = "/updateCheckServiceCom", method = RequestMethod.GET)
	public @ResponseBody boolean updateCheckServiceCom(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		ChecklistDTO checklist = new ChecklistDTO();
		String fechaIni = request.getParameter("fechaIni");
		String fechaFin = request.getParameter("fechaFin");
		String idEstado = request.getParameter("idEstado");
		String idHorario = request.getParameter("idHorario");
		String idTipoCheck = request.getParameter("idTipoCheck");

		// String nombreCheck = new
		// String(request.getParameter("nombreCheck").getBytes("ISO-8859-1"), "UTF-8");
		char[] ca = { '\u0061', '\u0301' };
		char[] ce = { '\u0065', '\u0301' };
		char[] ci = { '\u0069', '\u0301' };
		char[] co = { '\u006F', '\u0301' };
		char[] cu = { '\u0075', '\u0301' };
		// mayusculas
		char[] c1 = { '\u0041', '\u0301' };
		char[] c2 = { '\u0045', '\u0301' };
		char[] c3 = { '\u0049', '\u0301' };
		char[] c4 = { '\u004F', '\u0301' };
		char[] c5 = { '\u0055', '\u0301' };
		char[] c6 = { '\u006E', '\u0303' };

		String nombreCheck = request.getParameter("nombreCheck");
		// System.out.println("nombreCheck1 " + nombreCheck);
		if (nombreCheck.contains(String.valueOf(ca)) || nombreCheck.contains(String.valueOf(ce))
				|| nombreCheck.contains(String.valueOf(ci)) || nombreCheck.contains(String.valueOf(co))
				|| nombreCheck.contains(String.valueOf(cu)) || nombreCheck.contains(String.valueOf(c1))
				|| nombreCheck.contains(String.valueOf(c2)) || nombreCheck.contains(String.valueOf(c3))
				|| nombreCheck.contains(String.valueOf(c4)) || nombreCheck.contains(String.valueOf(c5))
				|| nombreCheck.contains(String.valueOf(c6))) {

			String rr = nombreCheck.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
					.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
					.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
					.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
					.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
					.replaceAll(String.valueOf(c6), "ñ");
			// System.out.println("rr " + rr);

			checklist.setNombreCheck(rr);
			rr = null;
		} else {
			// System.out.println("nombreCheck " + nombreCheck);

			checklist.setNombreCheck(nombreCheck);

		}

		String setVigente = request.getParameter("setVigente");
		String idChck = request.getParameter("idChecklist");
		String idUsuario = request.getParameter("idUsuario");
		String periodicidad = request.getParameter("periodicidad");
		String version = request.getParameter("version");
		String ordenGrupo = request.getParameter("ordenGrupo");
		String dia = request.getParameter("dia");
		String clasifica = request.getParameter("clasifica");
		String nego = request.getParameter("nego");
		double ponderacionTot = Double.parseDouble(request.getParameter("ponderacionTot"));

		UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
		idUsuario = userSession.getIdUsuario();

		checklist.setFecha_inicio(fechaIni);
		checklist.setFecha_fin(fechaFin);
		checklist.setIdEstado(Integer.parseInt(idEstado));
		checklist.setIdHorario(Integer.parseInt(idHorario));
		TipoChecklistDTO chk = new TipoChecklistDTO();
		chk.setIdTipoCheck(Integer.parseInt(idTipoCheck));
		checklist.setIdTipoChecklist(chk);
		checklist.setNombreCheck(nombreCheck);
		checklist.setVigente(Integer.parseInt(setVigente));
		// Obtengo el ID del checklist
		checklist.setIdChecklist(Integer.parseInt(idChck));
		// SE AGREGA USUARIO RESPONSABLE, DIA Y PERIDO
		checklist.setIdUsuario(Integer.parseInt(idUsuario));
		checklist.setPeriodicidad(periodicidad);
		checklist.setVersion(version);
		checklist.setOrdeGrupo(ordenGrupo);
		checklist.setDia(dia);
		checklist.setPonderacionTot(ponderacionTot);
		checklist.setClasifica(clasifica);
		checklist.setNego(nego);
		boolean res = preguntaAdmBI.actualizaChecklistCom(checklist);

		return res;
	}

	@RequestMapping(value = "/updatePreguntaServiceCom", method = RequestMethod.GET)
	public @ResponseBody boolean updatePreguntaServiceCom(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		PreguntaDTO pregunta = new PreguntaDTO();
		String idMod = request.getParameter("idMod");
		String idTipo = request.getParameter("idTipo");
		String estatus = request.getParameter("estatus");
		// String setPregunta = new
		// String(request.getParameter("setPregunta").getBytes("ISO-8859-1"), "UTF-8");
		String idPregunta = request.getParameter("idPregunta");
		String detalle = request.getParameter("detalle");
		int critica = Integer.parseInt(request.getParameter("critica"));
		char[] ca = { '\u0061', '\u0301' };
		char[] ce = { '\u0065', '\u0301' };
		char[] ci = { '\u0069', '\u0301' };
		char[] co = { '\u006F', '\u0301' };
		char[] cu = { '\u0075', '\u0301' };
		// mayusculas
		char[] c1 = { '\u0041', '\u0301' };
		char[] c2 = { '\u0045', '\u0301' };
		char[] c3 = { '\u0049', '\u0301' };
		char[] c4 = { '\u004F', '\u0301' };
		char[] c5 = { '\u0055', '\u0301' };
		char[] c6 = { '\u006E', '\u0303' };

		String setPregunta = request.getParameter("setPregunta");
		// System.out.println("setPregunta1 " + setPregunta);
		if (setPregunta.contains(String.valueOf(ca)) || setPregunta.contains(String.valueOf(ce))
				|| setPregunta.contains(String.valueOf(ci)) || setPregunta.contains(String.valueOf(co))
				|| setPregunta.contains(String.valueOf(cu)) || setPregunta.contains(String.valueOf(c1))
				|| setPregunta.contains(String.valueOf(c2)) || setPregunta.contains(String.valueOf(c3))
				|| setPregunta.contains(String.valueOf(c4)) || setPregunta.contains(String.valueOf(c5))
				|| setPregunta.contains(String.valueOf(c6))) {

			String rr = setPregunta.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
					.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
					.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
					.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
					.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
					.replaceAll(String.valueOf(c6), "ñ");
			// System.out.println("rr " + rr);

			pregunta.setPregunta(rr);
			rr = null;
		} else {
			// System.out.println("setPregunta " + setPregunta);

			pregunta.setPregunta(setPregunta);

		}

		if (detalle.contains(String.valueOf(ca)) || detalle.contains(String.valueOf(ce))
				|| detalle.contains(String.valueOf(ci)) || detalle.contains(String.valueOf(co))
				|| detalle.contains(String.valueOf(cu)) || detalle.contains(String.valueOf(c1))
				|| detalle.contains(String.valueOf(c2)) || detalle.contains(String.valueOf(c3))
				|| detalle.contains(String.valueOf(c4)) || detalle.contains(String.valueOf(c5))
				|| detalle.contains(String.valueOf(c6))) {

			String rr = detalle.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
					.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
					.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
					.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
					.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
					.replaceAll(String.valueOf(c6), "ñ");

			pregunta.setDetalle(rr);
			rr = null;
		} else {

			pregunta.setDetalle(detalle);

		}

		String sla = request.getParameter("sla");
		// String area="";
		// area=request.getParameter("area");
		pregunta.setIdModulo(Integer.parseInt(idMod));
		pregunta.setIdTipo(Integer.parseInt(idTipo));
		pregunta.setEstatus(Integer.parseInt(estatus));
		pregunta.setPregunta(setPregunta);
		pregunta.setIdPregunta(Integer.parseInt(idPregunta));
		// pregunta.setDetalle(detalle);
		pregunta.setCritica(critica);
		pregunta.setSla(sla);
		// pregunta.setArea(area);
		boolean res = this.preguntaAdmBI.actualizaPreguntaCom(pregunta);

		return res;

	}

	// http://localhost:8080/checklist/catalogosAdmService/updatePregCheckService.json?idPreg=<?>&idCheck=<?>&ordenPreg=<?>&pregPadre=<?>
	@RequestMapping(value = "/updatePregCheckService", method = RequestMethod.GET)
	public @ResponseBody boolean updatePreguntaCheckService(HttpServletRequest request, HttpServletResponse response) {
		String idCheck = request.getParameter("idCheck");
		String ordenPreg = request.getParameter("ordenPreg");
		String idPadre = request.getParameter("pregPadre");
		String idPreg = request.getParameter("idPreg");

		ChecklistPreguntaDTO pregDTO = new ChecklistPreguntaDTO();
		pregDTO.setIdChecklist(Integer.parseInt(idCheck));
		pregDTO.setOrdenPregunta(Integer.parseInt(ordenPreg));
		pregDTO.setPregPadre(Integer.parseInt(idPadre));
		pregDTO.setIdPregunta(Integer.parseInt(idPreg));
		boolean res = preguntaAdmBI.actualizaChecklistPregunta(pregDTO);

		return res;
	}

	// http://localhost:8080/checklist/catalogosAdmService/updateCheckUsuAdm.json?idCheckUsuario=<?>&activo=<?>
	@RequestMapping(value = "/updateCheckUsuAdm", method = RequestMethod.GET)
	public ModelAndView updateCheckUsuAdm(HttpServletRequest request, HttpServletResponse response) {
		try {
			int idCheckUsuario = Integer.parseInt(request.getParameter("idCheckUsuario"));
			int activo = Integer.parseInt(request.getParameter("activo"));

			ChecklistUsuarioDTO che = new ChecklistUsuarioDTO();

			che.setIdCheckUsuario(idCheckUsuario);
			che.setActivo(activo);

			boolean respuesta = bitacoraCheckUsuAdmDAO.actualizaCheckUsuario(che);

			ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
			mv.addObject("tipo", "update   con chekusua:  " + idCheckUsuario + " activo: " + activo + " ");
			mv.addObject("res", respuesta);

			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	/* AGREGUE IDSETARBOLDESICION (ID) */
	// http://10.51.219.179:8080/checklist/catalogosAdmService/updateArbolDesicion.json?idArbolDesicion=4845&estatusEvidencia=1&idCheck=1024&idPreg=12451&idOrdenCheckRes=1&setRes=0&reqAccion=0&reqObs=0&reqOblig=1&etiquetaEvidencia=1&ponderacion=1&idPlantilla=1&idprotocolo=10
	@RequestMapping(value = "/updateArbolDesicion", method = RequestMethod.GET)
	public ModelAndView updateArbolDecision(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String estatusEvidencia = request.getParameter("estatusEvidencia");
		String idCheck = request.getParameter("idCheck");
		String idPreg = request.getParameter("idPreg");
		String idOrdenCheckRes = request.getParameter("idOrdenCheckRes");
		String setRes = new String(request.getParameter("setRes").getBytes("ISO-8859-1"), "UTF-8");
		String idArbolDesicion = request.getParameter("idArbolDesicion");
		String reqAccion = request.getParameter("reqAccion");
		String reqObs = request.getParameter("reqObs");
		String reqOblig = request.getParameter("reqOblig");
		String etiquetaEvi = request.getParameter("etiquetaEvidencia");
		String ponderacion = request.getParameter("ponderacion");
		String idPlantilla = request.getParameter("idPlantilla");
		String idProtocolo = request.getParameter("idProtocolo");

		int idProtocoloI = 0;
		if (idProtocolo != null) {
			idProtocoloI = Integer.parseInt(idProtocolo);
		}

		ArbolDecisionDTO arbol = new ArbolDecisionDTO();
		arbol.setEstatusEvidencia(Integer.parseInt(estatusEvidencia));
		arbol.setIdCheckList(Integer.parseInt(idCheck));
		arbol.setIdPregunta(Integer.parseInt(idPreg));
		arbol.setOrdenCheckRespuesta(Integer.parseInt(idOrdenCheckRes));
		arbol.setRespuesta(setRes);
		arbol.setIdArbolDesicion(Integer.parseInt(idArbolDesicion));
		arbol.setReqAccion(Integer.parseInt(reqAccion));
		arbol.setReqObservacion(Integer.parseInt(reqObs));
		arbol.setReqOblig(Integer.parseInt(reqOblig));
		arbol.setDescEvidencia(etiquetaEvi);
		arbol.setPonderacion(ponderacion);
		arbol.setIdPlantilla(Integer.parseInt(idPlantilla));
		arbol.setIdProtocolo(idProtocoloI);

		boolean res = arbolRespAdiEviAdmBI.actualizaArbolDecision(arbol);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "ARBOLR CREADO CON EXITO--->");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosAdmService/updateRespuesta.json?idRespuesta=<?>&idArbolDesicion=<?>&idBitacora=<?>&commit=<?>&observaciones=<?>
	@RequestMapping(value = "/updateRespuesta", method = RequestMethod.GET)
	public ModelAndView updateRespuesta(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String idArbolDesicion = request.getParameter("idArbolDesicion");
		String idBitacora = request.getParameter("idBitacora");
		String commit = request.getParameter("commit");
		String idRes = request.getParameter("idRespuesta");
		String observaciones = new String(request.getParameter("observaciones").getBytes("ISO-8859-1"), "UTF-8");

		RespuestaDTO respuesta = new RespuestaDTO();
		respuesta.setIdArboldecision(Integer.parseInt(idArbolDesicion));
		respuesta.setIdBitacora(Integer.parseInt(idBitacora));
		respuesta.setCommit(Integer.parseInt(commit));
		respuesta.setIdRespuesta(Integer.parseInt(idRes));
		respuesta.setObservacion(observaciones);
		boolean res = arbolRespAdiEviAdmBI.actualizaRespuesta(respuesta);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "RESPUESTA MODIFICADA");
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosAdmService/updateRespuestaAD.json?idRespAd=<?>&descripcion=<?>&commit=
	@RequestMapping(value = "/updateRespuestaAD", method = RequestMethod.GET)
	public ModelAndView updateRespuestaAD(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			String idRespAd = request.getParameter("idRespAd");
			String descripcion = request.getParameter("descripcion");
			String commit = request.getParameter("commit");

			RespuestaAdDTO res = new RespuestaAdDTO();

			res.setIdRespuesta(Integer.parseInt(idRespAd));
			res.setDescripcion(descripcion);
			res.setCommit(Integer.parseInt(commit));

			boolean resp = arbolRespAdiEviAdmBI.actualizaRespAD(res);

			ModelAndView mv = new ModelAndView("altaModuloRes");
			mv.addObject("tipo", "ID RESPUESTA CREADA-->");
			mv.addObject("res", resp);
			return mv;
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	// http://localhost:8080/checklist/catalogosAdmService/updateEvidencia.json?idEvidencia=<?>&commit=<?>&idRespuesta=<?>&idTipo=<?>&ruta=<?>
	@RequestMapping(value = "/updateEvidencia", method = RequestMethod.GET)
	public ModelAndView updateEvidencia(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		String commit = request.getParameter("commit");
		String idRespuesta = request.getParameter("idRespuesta");
		String idTipo = request.getParameter("idTipo");
		String ruta = request.getParameter("ruta");
		String idEvidencia = request.getParameter("idEvidencia");

		EvidenciaDTO evidencia = new EvidenciaDTO();
		evidencia.setCommit(Integer.parseInt(commit));
		evidencia.setIdRespuesta(Integer.parseInt(idRespuesta));
		evidencia.setIdTipo(Integer.parseInt(idTipo));
		evidencia.setRuta(ruta);
		evidencia.setIdEvidencia(Integer.parseInt(idEvidencia));
		boolean idEvi = arbolRespAdiEviAdmBI.actualizaEvidencia(evidencia);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "EVIDENCIA MODIFICADA");
		mv.addObject("res", idEvi);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosAdmService/updateCheckStat.json?check=<?>&status=
	@RequestMapping(value = "/updateCheckStat", method = RequestMethod.GET)
	public ModelAndView updateCheckStat(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		int check = Integer.parseInt(request.getParameter("check"));
		int status = Integer.parseInt(request.getParameter("status"));

		CheckSoporteAdmDTO chk = new CheckSoporteAdmDTO();
		chk.setIdChecklist(check);
		chk.setEstatus(status);
		boolean res = checkSoporteAdmBI.actualiza(chk);

		ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
		mv.addObject("tipo", "Checklis MODIFICADO: " + check);
		mv.addObject("res", res);
		return mv;
	}

	// http://localhost:8080/checklist/catalogosAdmService/updateProtocoloProductivoById.json?idProtocolo=
	@RequestMapping(value = "/updateProtocoloProductivoById", method = RequestMethod.GET)
	public @ResponseBody String updateProtocoloProductivoById(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idProtocolo", required = false, defaultValue = "") String idProtocolo,
			@RequestParam(value = "status", required = false, defaultValue = "") String status)
			throws KeyException, GeneralSecurityException, IOException {

		String json = "";
		Boolean respuestaGlobal = true;

		try {
			
			//Obtengo lista de id´s de Checklist de esa Versión
			List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkVersParamProd(idProtocolo,"");
			
			for ( CheckSoporteAdmDTO objLista : lista) {

				int checkParam = objLista.getIdChecklist();
				int statusParam = 0;

				CheckSoporteAdmDTO chk = new CheckSoporteAdmDTO();
				chk.setIdChecklist(checkParam);
				chk.setEstatus(statusParam);
				
				boolean respuesta = checklistBI.actualizaCheckVigente(checkParam,statusParam);		
				
				if (!respuesta) {
					respuestaGlobal = false;
				}

			}

			if (respuestaGlobal) {
				
				VersionExpanDTO ver = new VersionExpanDTO();
				ver.setIdTab(Integer.parseInt(idProtocolo));
				ver.setIdVers(0);
				ver.setIdactivo(0);
				ver.setNego("");
				ver.setObs("");
				ver.setFechaLibera("");
				versionCheckAdmBI.actualizaNegoVer(ver);
				
				json = "{\"response\":" + respuestaGlobal + "}";

			} else {
				json = "{\"response\":" + respuestaGlobal + "}";
			}

		} catch (Exception e) {
			logger.info("ERROR" + e);
			json = "{ }";
		}

		return json;

	}

	// http://localhost:8080/checklist/catalogosAdmService/updateProtocoloRevisionById.json?idProtocolo=<?>&status=<?>
	@RequestMapping(value = "/updateProtocoloRevisionById", method = RequestMethod.GET)
	public @ResponseBody String updateProtocoloRevisionById(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "idProtocolo", required = false, defaultValue = "") String idProtocolo,
			@RequestParam(value = "status", required = false, defaultValue = "") String status)
			throws KeyException, GeneralSecurityException, IOException {

		String json = "";
		Boolean respuestaGlobal = true;
		try {
			
			//Obtengo lista de id´s de Checklist de esa Versión
			List<CheckSoporteAdmDTO> lista = checkSoporteAdmBI.checkVersParam(idProtocolo,"0");
			
			for ( CheckSoporteAdmDTO objLista : lista) {

				int checkParam = objLista.getIdChecklist();
				int statusParam = 0;

				CheckSoporteAdmDTO chk = new CheckSoporteAdmDTO();
				chk.setIdChecklist(checkParam);
				chk.setEstatus(statusParam);
				
				boolean respuesta = checkSoporteAdmBI.actualiza(chk);
				
				if (!respuesta) {
					respuestaGlobal = false;
				}

			}
			
			if (respuestaGlobal) {
				
				VersionExpanDTO ver = new VersionExpanDTO();
				ver.setIdTab(Integer.parseInt(idProtocolo));
				ver.setIdVers(0);
				ver.setIdactivo(0);
				ver.setNego("");
				ver.setObs("");
				ver.setFechaLibera("");
				versionCheckAdmBI.actualizaNegoVer(ver);

				json = "{\"response\":" + respuestaGlobal + "}";

			} else {
				json = "{\"response\":" + respuestaGlobal + "}";
			}

		} catch (Exception e) {
			json = "{ }";
		}

				
	return json;

	}
				
				// http://localhost:8080/checklist/catalogosAdmService/updateCheckAutoriza.json?idtab=&version=<?>&usu=<?>&deta=<?>&status=<?>&tipousu=<?>&fechalibera
				@RequestMapping(value = "/updateCheckAutoriza", method = RequestMethod.GET)
				public ModelAndView updateCheckAutoriza(HttpServletRequest request, HttpServletResponse response)
						throws UnsupportedEncodingException {
					try {
						
						int version=0;
						if (request.getParameter("version") != null && request.getParameter("version").length() <= 0) {
							version = 0;
						} else {
							version = Integer.parseInt(request.getParameter("version"));
						}
		
					
						int usu=Integer.parseInt(request.getParameter("usu"));
		
						String deta="";
						deta = request.getParameter("deta");
						int status=0;
						if (request.getParameter("status") != null && request.getParameter("status").length() <= 0) {
							status = 0;
						} else {
							status = Integer.parseInt(request.getParameter("status"));
						}
		
						String tipousu="";
						tipousu = request.getParameter("tipousu");
						String fechalibera="";
						fechalibera = request.getParameter("fechalibera");
						int idtab=Integer.parseInt(request.getParameter("idtab"));
						
						
						CheckAutoProtoDTO aut = new CheckAutoProtoDTO();
						
						aut.setIdTab(idtab);
						aut.setIdCheck(version);
						aut.setIdUsu(usu);
						aut.setDetalle(deta);
						aut.setStatus(status);
						aut.setUsuTipo(tipousu);
						aut.setFechaLiberacion(fechalibera);
						
						boolean resp = checkAutProtoBI.actualizaAutoProto(aut);
						
						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", " Autorizacion Checklist-->");
						mv.addObject("res", resp);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;
					}
				}
				// http://localhost:8080/checklist/catalogosAdmService/updateAutoCheckStat.json?idUsuario=&idtab=&check=<?>&status=<?>&fechalibera
				@RequestMapping(value = "/updateAutoCheckStat", method = RequestMethod.GET)
				public ModelAndView updateAutoCheckStat(HttpServletRequest request, HttpServletResponse response)
						throws UnsupportedEncodingException {
					try {
						int  check = Integer.parseInt(request.getParameter("check"));
						int status = Integer.parseInt(request.getParameter("status"));
						String fechalibera = request.getParameter("fechalibera");
						int idtab=Integer.parseInt(request.getParameter("idtab"));
						int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
						
						CheckAutoProtoDTO aut = new CheckAutoProtoDTO();
						
						aut.setIdTab(idtab);
						aut.setIdCheck(check);
						aut.setStatus(status);
						aut.setFechaLiberacion(fechalibera);
						aut.setIdUsu(idUsuario);
						aut.setDetalle("Automatico");
						aut.setUsuTipo("1");
						
						boolean resp = checkAutProtoBI.actualizaAutoProtoStatus(aut);
						
						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", " Autorizacion Checklist-->");
						mv.addObject("res", resp);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;
					}
				}
				
				//http://localhost:8080/checklist/catalogosAdmService/actualizaPosible.json?idPosible=<?>&respuesta=<?>
				@RequestMapping(value="/actualizaPosible", method = RequestMethod.GET)
				public ModelAndView actualizaPosible(HttpServletRequest request, HttpServletResponse response){
					try{
						String respuesta= new String (request.getParameter("respuesta").getBytes("ISO-8859-1"), "UTF-8");
						int idPosible=Integer.parseInt(request.getParameter("idPosible"));
						PosiblesDTO pos=new PosiblesDTO();
						pos.setIdPosible(idPosible);
						
						char[] ca = { '\u0061', '\u0301' };
						char[] ce = { '\u0065', '\u0301' };
						char[] ci = { '\u0069', '\u0301' };
						char[] co = { '\u006F', '\u0301' };
						char[] cu = { '\u0075', '\u0301' };
						// mayusculas
						char[] c1 = { '\u0041', '\u0301' };
						char[] c2 = { '\u0045', '\u0301' };
						char[] c3 = { '\u0049', '\u0301' };
						char[] c4 = { '\u004F', '\u0301' };
						char[] c5 = { '\u0055', '\u0301' };
						char[] c6 = { '\u006E', '\u0303' };

						String setPregunta = request.getParameter("respuesta");
						//System.out.println("setPregunta1 " + setPregunta);
						if (setPregunta.contains(String.valueOf(ca)) || setPregunta.contains(String.valueOf(ce))
								|| setPregunta.contains(String.valueOf(ci)) || setPregunta.contains(String.valueOf(co))
								|| setPregunta.contains(String.valueOf(cu)) || setPregunta.contains(String.valueOf(c1))
								|| setPregunta.contains(String.valueOf(c2)) || setPregunta.contains(String.valueOf(c3))
								|| setPregunta.contains(String.valueOf(c4)) || setPregunta.contains(String.valueOf(c5))
								|| setPregunta.contains(String.valueOf(c6))) {

							String rr = setPregunta.replaceAll(String.valueOf(ca), "á").replaceAll(String.valueOf(ce), "é")
									.replaceAll(String.valueOf(ci), "í").replaceAll(String.valueOf(co), "ó")
									.replaceAll(String.valueOf(cu), "ú").replaceAll(String.valueOf(c1), "Á")
									.replaceAll(String.valueOf(c2), "É").replaceAll(String.valueOf(c3), "Í")
									.replaceAll(String.valueOf(c4), "Ó").replaceAll(String.valueOf(c5), "Ú")
									.replaceAll(String.valueOf(c6), "ñ");
							//System.out.println("rr " + rr);
							
							setPregunta=rr;
							pos.setDescripcion(rr);
							rr = null;
						}	
						
						
						boolean res = posiblesAdmBI.actualizaPosible(pos);
						
						ModelAndView mv = new ModelAndView("altaModuloRes");
						mv.addObject("tipo", "ID POSIBLE ASIGNADO ");
						mv.addObject("res", res);
						
						return mv;
					}catch(Exception e){
						logger.info(e);
						return null;
					}
				}
				// http://localhost:8080/checklist/catalogosAdmService/updateCheckVersAdm.json?idtab=&check=<?>&version=<?>&deta=<?>&status=<?>&nego=<?>&=&obs=
				@RequestMapping(value = "/updateCheckVersAdm", method = RequestMethod.GET)
				public ModelAndView updateCheckVersAdm(HttpServletRequest request, HttpServletResponse response)
						throws UnsupportedEncodingException {
					try {
						int  idtab = Integer.parseInt(request.getParameter("idtab"));
						int  check = Integer.parseInt(request.getParameter("check"));
						int version = Integer.parseInt(request.getParameter("version"));
						String deta = request.getParameter("deta");
						int status = Integer.parseInt(request.getParameter("status"));
						String nego = request.getParameter("nego");
						String obs = request.getParameter("obs");
						
						VersionExpanDTO ver = new VersionExpanDTO();
						
						ver.setIdTab(idtab);
						ver.setIdCheck(check);
						ver.setIdVers(version);
						ver.setAux(deta);
						ver.setIdactivo(status);
						ver.setNego(nego);
						ver.setObs(obs);
						
						boolean resp = versionCheckAdmBI.actualiza(ver);
						
						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", " modifica versionCheckADM-->");
						mv.addObject("res", resp);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;
					}
				}
				
				// http://localhost:8080/checklist/catalogosAdmService/updateCheckVersAdmNego.json?idtab=&version=<?>&status=<?>&nego=<?>&fechalibera
				@RequestMapping(value = "/updateCheckVersAdmNego", method = RequestMethod.GET)
				public ModelAndView updateCheckVersAdmNego(HttpServletRequest request, HttpServletResponse response)
						throws UnsupportedEncodingException {
					try {
						
						
						int version=0;
						if (request.getParameter("version") != null && request.getParameter("version").length() <= 0) {
							version = 0;
						} else {
							version = Integer.parseInt(request.getParameter("version"));
						}
		
						int  idtab=0;
						if (request.getParameter("idtab") != null && request.getParameter("idtab").length() <= 0) {
							idtab = 0;
						} else {
							idtab = Integer.parseInt(request.getParameter("idtab"));
						}
						
						int status=0;
						if (request.getParameter("status") != null && request.getParameter("status").length() <= 0) {
							status = 0;
						} else {
							status = Integer.parseInt(request.getParameter("status"));
						}
												
						String nego="";
						nego = request.getParameter("nego");
						String obs="";
						obs = request.getParameter("obs");
						String fechalibera="";
						fechalibera = request.getParameter("fechalibera");
						
						VersionExpanDTO ver = new VersionExpanDTO();
						
						ver.setIdTab(idtab);
						ver.setIdVers(version);
						ver.setIdactivo(status);
						ver.setNego(nego);
						ver.setObs(obs);
						ver.setFechaLibera(fechalibera);
					
						boolean resp = versionCheckAdmBI.actualizaNegoVer(ver);
						
						ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
						mv.addObject("tipo", " modifica versionNegoCheckADM-->");
						mv.addObject("res", resp);
						return mv;
					} catch (Exception e) {
						logger.info(e);
						return null;
					}
				}
}
