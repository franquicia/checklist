package com.gruposalinas.checklist.dao;

import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class DepuracionTblDAOImpl extends DefaultDAO implements DepuracionTblDAO {

    private Logger logger = LogManager.getLogger(DepuracionTblDAOImpl.class);

    private DefaultJdbcCall jdbcDepuraTabla;
    private DefaultJdbcCall jdbcDepuraBitacora;
    private DefaultJdbcCall jdbcDepuraErrores;

    public void init() {

        jdbcDepuraTabla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADEPURACION")
                .withProcedureName("SPDEPURATPTABLA");

        jdbcDepuraBitacora = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADEPURACION")
                .withProcedureName("SP_DEPURABITA");

        jdbcDepuraErrores = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PADEPURACION")
                .withProcedureName("SP_DEPURAERROR");

    }

    @SuppressWarnings("unused")
    @Override
    public boolean depuraTabla(String nombreTabla) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_VNOMBRETABLA", nombreTabla);

        out = jdbcDepuraTabla.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PADEPURACION.SPDEPURATPTABLA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        ejecucion = resultado.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al depurar la tabla" + nombreTabla);
            return false;
        }

        return true;
    }

    @SuppressWarnings("unused")
    @Override
    public ArrayList<Object> depuraBitacora(String fechaInicio, String fechaFin) throws Exception {
        Map<String, Object> out = null;
        ArrayList<Object> resp = new ArrayList<>();
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_INICIO", fechaInicio)
                .addValue("PA_FIN", fechaFin);

        out = jdbcDepuraBitacora.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PADEPURACION.SP_DEPURABITA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        BigDecimal regs = (BigDecimal) out.get("PA_ELIMINADAS");
        ejecucion = resultado.intValue();
        double totalRegs = regs.doubleValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al depurar las bitacoras fechaInicio:" + fechaInicio + " fechaFin: " + fechaFin);
        }

        resp.add(ejecucion == 1);
        resp.add(totalRegs);

        return resp;
    }

    @SuppressWarnings("unused")
    @Override
    public ArrayList<Object> depuraErrores(String fechaInicio, String fechaFin) throws Exception {
        ArrayList<Object> resp = new ArrayList<>();
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_INICIO", fechaInicio)
                .addValue("PA_FIN", fechaFin);

        out = jdbcDepuraErrores.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PADEPURACION.SP_DEPURAERROR}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        BigDecimal regs = (BigDecimal) out.get("PA_ELIMINADAS");
        ejecucion = resultado.intValue();
        double totalRegs = regs.doubleValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al depurar las bitacoras fechaInicio:" + fechaInicio + " fechaFin: " + fechaFin);
        }

        resp.add(ejecucion == 1);
        resp.add(totalRegs);

        return resp;
    }

}
