package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.AlertasResumenDTO;

public class AlertasResumenRowMapper implements RowMapper<AlertasResumenDTO>{

	@Override
	public AlertasResumenDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		AlertasResumenDTO alertasResumenDTO = new AlertasResumenDTO();
		alertasResumenDTO.setFecha(rs.getString("HORAREPORTE").substring(0, 10));
		alertasResumenDTO.setHora(rs.getString("HORAREPORTE").substring(10, 19));
		alertasResumenDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
		alertasResumenDTO.setRespuesta(rs.getString("FCDESCRIPCION"));
		
		return alertasResumenDTO;
	}
	
	

}
