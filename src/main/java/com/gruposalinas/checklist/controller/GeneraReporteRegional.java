package com.gruposalinas.checklist.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.TipoChecklistBI;
import com.gruposalinas.checklist.resources.FRQConstantes;

@Controller
public class GeneraReporteRegional {
	
	private static Logger logger = LogManager.getLogger(GeneraReporteRegional.class);
	
	@Autowired
	TipoChecklistBI tipoChecklistBI;
	
	
	//Abre la vista para generar el reporte
	@RequestMapping(value = "central/exportaReporte.htm", method=RequestMethod.POST)
	public ModelAndView exportaReporte(
			@RequestParam(value = "idReporte", required = true, defaultValue = "null") String idReporte,
			HttpServletRequest request){
		
		ModelAndView mv = new ModelAndView("generaReporteRegional");
		
		if(idReporte.equals("adminCheckchecklists")){
			try {
				mv.addObject("listaTipoCheck", tipoChecklistBI.obtieneTipoChecklist());
				mv.addObject("urlServer", FRQConstantes.getURLServer());
			} catch (Exception e) {
				e.printStackTrace();
				logger.info("Algo ocurrió... al traer Resumen " + e);
			}	
		}else{
			mv=null;
		}
		return mv;
	}
}
