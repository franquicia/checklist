package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.BitacoraDTO;
import com.gruposalinas.checklist.domain.ChecklistUsuarioDTO;

public interface BitacoraCheckUsuAdmDAO {
	
	public int insertaBitacora(BitacoraDTO bean)throws Exception;
	
	public boolean actualizaBitacora(BitacoraDTO bean)throws Exception;
	
	public boolean eliminaBitacora(int idBitacora)throws Exception;
	
	public List<BitacoraDTO> buscaBitacora()throws Exception;
	
	public List<BitacoraDTO> buscaBitacora(String checkUsua, String idBitacora, String fechaI, String fechaF)throws Exception;
	
	public int verificaIdBitacora(int checkUsua)throws Exception;
	
	public boolean cierraBitacora() throws Exception;
	
	public List<BitacoraDTO> buscaBitacoraCerradas(String idChecklist)throws Exception;
	
	public List<BitacoraDTO> buscaBitacoraCerradasR(String idChecklist)throws Exception;
	
	//CHECK USUA
	public List<ChecklistUsuarioDTO> obtieneCheckUsuario(int idCheckU) throws Exception;
	
	public List<ChecklistUsuarioDTO> obtieneCheckU (String idUsuario) throws Exception;
	
	public List<ChecklistUsuarioDTO> obtieneCheckUsua (String idUsuario, String ceco, String fechaInicio) throws Exception;
	
	public int insertaCheckUsuario(ChecklistUsuarioDTO bean) throws Exception;
	
	public boolean actualizaCheckUsuario(ChecklistUsuarioDTO bean) throws Exception;
	
	public boolean eliminaCheckUsuario(int idCheckU) throws Exception;
	
	public boolean desactivaChecklist(int idChecklist)throws Exception;
	
	public boolean desactivaCheckUsua(int idCeco,int idChecklist,int idUsu)throws Exception;
	
	public boolean insertaCheckusuaEspecial(int idUsuario, int idChecklist, String ceco)throws Exception;
}
