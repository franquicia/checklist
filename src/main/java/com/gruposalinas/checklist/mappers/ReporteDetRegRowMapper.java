package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ReporteDTO;

public class ReporteDetRegRowMapper implements RowMapper<ReporteDTO>    {
	@Override
	public ReporteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		ReporteDTO reporteDTO = new ReporteDTO();

		reporteDTO.setSemana(rs.getString("DIAFECHA"));
		reporteDTO.setIdCeco(rs.getString("CECOS"));
		
		return reporteDTO;
	}

}
