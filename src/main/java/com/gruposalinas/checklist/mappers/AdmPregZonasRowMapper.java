package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.AdmPregZonaDTO;


public class AdmPregZonasRowMapper implements RowMapper<AdmPregZonaDTO> {

	public AdmPregZonaDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AdmPregZonaDTO admZonasDTO = new AdmPregZonaDTO();

		admZonasDTO.setIdPregZona(String.valueOf(rs.getInt("FIIDPRZO")));
		admZonasDTO.setIdZona(String.valueOf(rs.getInt("FIIDZONA")));
		admZonasDTO.setIdPreg(String.valueOf(rs.getInt("FIIDPREG")));		
		
		return admZonasDTO;
	}
}
