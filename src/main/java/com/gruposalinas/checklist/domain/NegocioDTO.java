package com.gruposalinas.checklist.domain;

public class NegocioDTO {

	private int idNegocio;
	private String descripcion;
	private int activo;
	
	private String idCeco;
	private String nomCeco;

	private String contribucion;
	private String normalidad;
	private String nuncaAbonadas;
	private String vista;
	private String plazo;
	
	private String colorContribucion;
	private String colorNormalidad;
	private String colorNuncaAbonadas;
	private String colorVista;
	private String colorPlazo;
	
	public NegocioDTO() {
		super();
	}

	public int getIdNegocio() {
		return idNegocio;
	}
	public void setIdNegocio(int idNegocio) {
		this.idNegocio = idNegocio;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	public String getIdCeco() {
		return idCeco;
	}
	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}
	public String getNomCeco() {
		return nomCeco;
	}
	public void setNomCeco(String nomCeco) {
		this.nomCeco = nomCeco;
	}
	public String getContribucion() {
		return contribucion;
	}
	public void setContribucion(String contribucion) {
		this.contribucion = contribucion;
	}
	public String getNormalidad() {
		return normalidad;
	}
	public void setNormalidad(String normalidad) {
		this.normalidad = normalidad;
	}
	public String getNuncaAbonadas() {
		return nuncaAbonadas;
	}
	public void setNuncaAbonadas(String nuncaAbonadas) {
		this.nuncaAbonadas = nuncaAbonadas;
	}
	public String getVista() {
		return vista;
	}
	public void setVista(String vista) {
		this.vista = vista;
	}
	public String getPlazo() {
		return plazo;
	}
	public void setPlazo(String plazo) {
		this.plazo = plazo;
	}
	public String getColorContribucion() {
		return colorContribucion;
	}
	public void setColorContribucion(String colorContribucion) {
		this.colorContribucion = colorContribucion;
	}
	public String getColorNormalidad() {
		return colorNormalidad;
	}
	public void setColorNormalidad(String colorNormalidad) {
		this.colorNormalidad = colorNormalidad;
	}
	public String getColorNuncaAbonadas() {
		return colorNuncaAbonadas;
	}
	public void setColorNuncaAbonadas(String colorNuncaAbonadas) {
		this.colorNuncaAbonadas = colorNuncaAbonadas;
	}
	public String getColorVista() {
		return colorVista;
	}
	public void setColorVista(String colorVista) {
		this.colorVista = colorVista;
	}
	public String getColorPlazo() {
		return colorPlazo;
	}
	public void setColorPlazo(String colorPlazo) {
		this.colorPlazo = colorPlazo;
	}

	@Override
	public String toString() {
		return "NegocioDTO [idNegocio=" + idNegocio + ", descripcion=" + descripcion + ", activo=" + activo
				+ ", idCeco=" + idCeco + ", nomCeco=" + nomCeco + ", contribucion=" + contribucion + ", normalidad="
				+ normalidad + ", nuncaAbonadas=" + nuncaAbonadas + ", vista=" + vista + ", plazo=" + plazo
				+ ", colorContribucion=" + colorContribucion + ", colorNormalidad=" + colorNormalidad
				+ ", colorNuncaAbonadas=" + colorNuncaAbonadas + ", colorVista=" + colorVista + ", colorPlazo="
				+ colorPlazo + "]";
	}
}
