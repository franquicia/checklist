package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.EmpFijoDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;


public interface SucursalChecklistDAO {


		public Map<String, Object> obtieneInfo() throws Exception; 
		
		public int inserta(SucursalChecklistDTO bean)throws Exception;
			
		public boolean elimina(String idImag)throws Exception;
		
		public boolean actualiza(SucursalChecklistDTO bean)throws Exception;

		public List<SucursalChecklistDTO> obtieneDatos(int idSucursal) throws Exception; 
		
		public List<SucursalChecklistDTO> obtieneInfoG( ) throws Exception; 
		
		public List<SucursalChecklistDTO> obtieneDatosSuc(String ceco) throws Exception; 
		
		public List<SucursalChecklistDTO> obtieneDatosSucCecoFaseProyecto(String ceco,String fase,String proyecto) throws Exception; 
	
	}