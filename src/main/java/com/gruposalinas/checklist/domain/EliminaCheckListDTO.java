package com.gruposalinas.checklist.domain;

public class EliminaCheckListDTO {
	


	private int ejecucion;
	private String idCeco;
	private String idUsuario;
	
	

	public String getIdCeco() {
		return idCeco;
	}

	public void setIdCeco(String idCeco) {
		this.idCeco = idCeco;
	}

	

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getEjecucion() {
		return ejecucion;
	}

	public void setEjecucion(int ejecucion) {
		this.ejecucion = ejecucion;
	}

}



