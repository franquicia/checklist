package com.gruposalinas.checklist.domain;

import com.google.gson.annotations.SerializedName;

public class TareasCreadorDTO {
	@SerializedName("Activo") private int activo;
	@SerializedName("IdTarea") private String idTarea;
	@SerializedName("Tipo") private String tipo;
	@SerializedName("Inicio") private String inicio;
	@SerializedName("Fin") private String fin;
	@SerializedName("StatusDesc") private int statusDesc;
	@SerializedName("OwnerId") private String ownerId;
	@SerializedName("Titulo") private String titulo;
	@SerializedName("Evidencia") private String evidencia;
	
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	public String getIdTarea() {
		return idTarea;
	}
	public void setIdTarea(String idTarea) {
		this.idTarea = idTarea;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getInicio() {
		return inicio;
	}
	public void setInicio(String inicio) {
		this.inicio = inicio;
	}
	public String getFin() {
		return fin;
	}
	public void setFin(String fin) {
		this.fin = fin;
	}
	public int getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(int statusDesc) {
		this.statusDesc = statusDesc;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getEvidencia() {
		return evidencia;
	}
	public void setEvidencia(String evidencia) {
		this.evidencia = evidencia;
	}
}
