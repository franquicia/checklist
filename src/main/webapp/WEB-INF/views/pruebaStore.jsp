<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>STORE</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
	<link rel="stylesheet" type="text/css" href="../css/css_nuevas/estilos.css">
	<link rel="stylesheet" type="text/css" href="../css/css_nuevas/menu.css">
	<link rel="stylesheet" type="text/css" href="../css/css_nuevas/secciones.css">
	<link rel="stylesheet" type="text/css" href="../css/css_nuevas/dropkick.css">
	<link rel="stylesheet" type="text/css" href="../css/css_nuevas/paginador.css" media="screen">
	<link rel="stylesheet" type="text/css" href="../css/css_nuevas/calendario.css" media="screen">
	
	<script type="text/javascript" src="../js/js/jquery-1.12.4.min.js"></script> 
	<script type="text/javascript" src="../js/js/jquery.dropkick.js"></script>
	<script type="text/javascript" src="../js/js/content_height.js"></script>
	<script type="text/javascript" src="../js/js/jquery-ui.js"></script> 
	
	<script type="text/javascript" src="../js/js/jquery.paginate.js"></script>
	<script type="text/javascript" src="../js/js/funciones.js"></script> 
	
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	
		<script type="text/javascript">
			$(function() {
				$("#demo5").paginate({
					count 		: 3,
					start 		: 1,
					display     : 3,
					border					: false,
					border_color			: false,
					text_color  			: '#8a8a8a',
					background_color    	: 'transparent',	
					border_hover_color		: 'transparent',
					text_hover_color  		: '#000',
					background_hover_color	: 'transparent', 
					images					: true,
					mouse					: 'press',
					onChange     			: function(page){
												$('._current','#paginationdemo').removeClass('_current').hide();
												$('#p'+page).addClass('_current').show();
											  }
				});
			});
		</script>
		<style type="text/css">
		
			div.regresar {
		    width: 100px;
		    margin: auto;
		    float: left;
		    }
			.contenedor {
			  position: relative;
			  width: 40%;
			  margin: 20%;
			  float: left;
			  
			}
			
			.contenedor img {
			  position: absolute;
			  left: 0;
			  transition: opacity 0.5s ease-in-out;
			  max-width:80%;
			}
			
			.contenedor img.top:hover {
			  opacity: 0;
			}
			
			div.apps{ border: 2px solid #F0F0F0;
				 }
				 
			table.apps {
			  
			  border-collapse: separate;
			  border-spacing: 15px 15px;
			}
			
			@font-face {
				font-family: 'Avenir';
				src: url('../fonts/AvenirNextLTPro-Medium.otf');
			}
			
			@font-face {
				font-family: 'AvenirText';
				src: url('../fonts/Avenir_next_thin.ttf');
			}
			
			h1.apps {
				font-family:Avenir;
			    font-size: 25px;
			    color: #43413B;
			}
			
			h2.appsh2 {
				font-family:Avenir;
			    font-size: 16px;
			    color: #43413B;
			}
			p.appsP {
				font-family:AvenirText;
			    font-size: 12px;
			    color: #43413B;
			}

			@media screen and (max-width: 600px) {
			       table {
			           width:100%;
			       }
			       thead {
			           display: none;
			       }
			       tr:nth-of-type(2n) {
			           background-color: inherit;
			       }
			       tr td:first-child {
			           background: #FFFFFF;
			           font-weight:bold;
			           font-size:1.3em;
			       }
			       tbody td {
			           display: block;
			           text-align:center;
			       }
			       tbody td:before {
			           content: attr(data-th);
			           display: block;
			           text-align:center;
			       }
			       img.aplicApps {
			       		width:40%;
			       
			       }
			}
		</style>
	
	<script language="JavaScript">

		function deshabilitaRetroceso(){
		    window.location.hash="no-back-button";
		    window.location.hash="Again-No-back-button" //chrome
		    window.onhashchange=function(){window.location.hash="no-back-button";}
		}

		function changeRegresar() {
			document.getElementById('Regresar').src="../images/regresar-verde.png";
		    	setTimeout(function() {
		    	 
		    	  window.parent.location.reload(true)
		    	    
		    	}, 100);
		}

		
		function changeImage() {
			document.getElementById('BAndroid').src="../images/android-verde.png";
		    	setTimeout(function() {

		    	  muestra_oculta('plataforma');
		    	  muestra_oculta('android'); 
		    	  muestra_oculta('Regresar2');
  
		    	}, 100);
		}
		function changeImageM() {
			document.getElementById('BMac').src="../images/mac-verde.png";
		    	setTimeout(function() {
		    	  muestra_oculta('plataforma');
		    	  muestra_oculta('mac'); 
		    	  muestra_oculta('Regresar2');
		    	    
		    	}, 100);
		    	isMobile();
		}
	
		function isMobile(){
			var userAgent = navigator.userAgent || navigator.vendor || window.opera;
	
			  
			 if (/windows phone/i.test(userAgent)) {
				alert("Windows phome no soportado");
			    return "Windows Phone";
			 }
	
			 if (/android/i.test(userAgent)) {
				muestra_oculta('plataforma');
		    	muestra_oculta('android'); 
				
				return "Android";
			}
	
			if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
				muestra_oculta('plataforma');
			    muestra_oculta('mac');
				
			    return "iOS";
			}
			
			return "desconocido";
		}
	
		
		
		function muestra_oculta(id){
			if (document.getElementById){ 
				var el = document.getElementById(id); 
				el.style.display = (el.style.display == 'none') ? 'block' : 'none'; 
			}
		}
		
	</script>
	
</head>
<body onload="isMobile(); deshabilitaRetroceso()">

<div class="checklists">
		<div class="header">
			<div class="tblHeader">
				 
				<div class="regresar" id="Regresar2" style="display: none;">
				 	<img id="Regresar" width="20px" src="../images/regresar.png" onmouseout="this.src='../images/regresar.png';" onmouseover="this.src = '../images/regresar-verde.png'" style="margin: 20px;" onclick="changeRegresar()"></img>
				 </div>
				
				 <div class="headerLogo">
					
							<img src="../images/logo-baz.png"  id="imgLogo">
				</div>
			</div>
		</div>
		
	</div>
	
	<center>
		
		<div id="plataforma">
		
			<div class="title" id="title">
				
				<center><h1 class="apps" >Selecciona tu plataforma</h1></center>
				
			</div>
			<br/>	
				
				<img id="BAndroid" width="25%" src="../images/android.png" onmouseout="this.src='../images/android.png';" onmouseover="this.src = '../images/android-verde.png'" style="margin: 20px;" onclick="changeImage()"></img>
			   
			  	<img id="BMac" width="25%" src="../images/mac.png" onmouseout="this.src='../images/mac.png';" onmouseover="this.src = '../images/mac-verde.png'" style="margin: 20px;"  onclick="changeImageM()"></img>
			    
			
			
		</div>
	</center>
	<br/>
	
	<div id="android" style="display: none;">
	
		<center><h1 class="apps">Selecciona tu aplicación</h1></center>

		<table  class="apps" >
			<tr>
			
				<td rowspan="6">
					<img class="aplicApps" width="105%" src="../images/android.png" />
				</td>
				
				<c:choose>
					<c:when test="${MG == '2'}">
						<td align="justify" valign="top">
							<a href="https://dl.dropboxusercontent.com/s/fe6figqxabei7j7/mi_gestion.apk">
							<div style="border-radius: 20px; " class="apps">
								<table class="apps">
									<tr>
										<td>
											<img src="../images/MiGestion.png" width="100" height="100"/>
										</td>
										<td valign="top">
											<h2 class="appsh2" >Mi gestión</h2>
											<br/>
											<p class="appsP">Aplicación para la supervisión de sucursales.</p>
										</td>
									</tr>
								</table>
							</div>
							</a>
						</td>
					</c:when>
				</c:choose>
			</tr>
			<tr>
				<c:choose>
					<c:when test="${GM == '1'}">
						<td align="justify" valign="top">
							<a href="https://dl.dropboxusercontent.com/s/hhtfhg91vbieqz4/gestion_movil.apk">
								<div style="border-radius: 20px; " class="apps">
									<table class="apps">
										<tr>
											<td> 
													<img src="../images/GestionMovil1.png" width="100" height="100"/>
											</td>
											<td valign="top">
												<h2 class="appsh2">Gestión móvil</h2>
												<p class="appsP">Aplicación que tiene todos los indicadores del modelo de gestión de Franquicia, te ayuda a gestionar y supervisar tu sucursal.</p>
											</td>
										</tr>
									</table>
								</div>
							</a>
						</td>
					</c:when>
				</c:choose>
			</tr>
			<tr>
				<c:choose>
					<c:when test="${BD == '3'}">
						<td align="justify" valign="bottom">
							<div style="border-radius: 20px; " class="apps">
							<table class="apps">
								<tr>
									<td>
										<img src="../images/BibliotecaDigital1.png" width="100" height="100"/>
									</td>
									<td valign="top">
										<h2 class="appsh2">Biblioteca digital</h2>
										<p class="appsP">Todos los documentos en formato digital del banco, puedes encontrar avisos normativos, avisos de privacidad, lista de comisiones, etc.</p>
									</td>
								</tr>
							</table>
							</div>
						</td>
					</c:when>
				</c:choose>
			</tr>
			<tr>
				<c:choose>
					<c:when test="${PM == '6'}">
						<td align="justify" valign="bottom">
							<a href="https://dl.dropboxusercontent.com/s/htsziduoptuhmro/proveedorMantenimiento.apk">
							<div style="border-radius: 20px; " class="apps">
							<table class="apps">
								<tr>
									<td>
										<img src="../images/app_1b_icon.png" width="100" height="100"/>
									</td>
									<td valign="top">
										<h2 class="appsh2">Proveedor GS</h2>
										<p class="appsP">Herramienta para proveedores de mantemimiento.</p>
									</td>
								</tr>
							</table>
							</div>
							</a>
						</td>
					</c:when>
				</c:choose>
			</tr>
		</table>
		
	</div>
	
	<div id="mac" style="display: none;">
		
		<center><h1 class="apps">Selecciona tu aplicación</h1> </center>

		<table  class="apps" >
			<tr>
				<td rowspan="6">
					<img class="aplicApps" width="105%" src="../images/mac.png" />
				</td>
				
				<c:choose>
					<c:when test="${MG == '2'}">
						<td align="justify" valign="top">
							<a href="itms-services://?action=download-manifest&url=https://dl.dropboxusercontent.com/s/aomowgx9toy2or7/manifestMG.plist">
								<div style="border-radius: 20px; " class="apps">
									<table class="apps">
										<tr>
											<td class="iconTabla">
												<img src="../images/MiGestion.png" width="100" height="100"/>
											</td>
											<td valign="top">
												<h2 class="appsh2" >Mi gestión</h2>
												<br/>
												<p class="appsP">Aplicación para la supervisión de sucursales.</p>
											</td>
										</tr>
									</table>
								</div>
							</a>
						</td>
					</c:when>
				</c:choose>
			</tr>
			<tr>
				<c:choose>
					<c:when test="${GM == '1'}">
						<td align="justify" valign="top">
							<a href="itms-services://?action=download-manifest&url=https://dl.dropboxusercontent.com/s/cp95shgsk8zmodo/manifestGM.plist">
								<div style="border-radius: 20px; " class="apps">
									<table class="apps">
										<tr>
											<td>
													<img src="../images/GestionMovil1.png" width="100" height="100"/>
											</td>
											<td valign="top">
												<h2 class="appsh2">Gestión móvil</h2>
												<p class="appsP">Aplicación que tiene todos los indicadores del modelo de gestión de Franquicia, te ayuda a gestionar y supervisar tu sucursal.</p>
											</td>
										</tr>
									</table>
								</div>
							</a>
						</td>
					</c:when>
				</c:choose>
			</tr>
			<tr>
				<c:choose>
					<c:when test="${GZ == '4'}">
						<td align="justify" valign="bottom">
							<a href="itms-services://?action=download-manifest&url=https://dl.dropboxusercontent.com/s/evolrm4lq13gwwe/manifestGZ.plist">
								<div style="border-radius: 20px; " class="apps">
									<table class="apps">
										<tr>
											<td>
													<img src="../images/GestionZona1.png" width="100" height="100"/>
											</td>
											<td valign="top">
												<h2 class="appsh2">Gestión zona</h2>
												<p class="appsP">Reporteria ejecutiva para la gestión y supervisión de sucursales.</p>
											</td>
										</tr>
									</table>
								</div>
							</a>
						</td>
					</c:when>
				</c:choose>
				
			</tr>
			
			<tr>
				<c:choose>
					<c:when test="${CU == '5'}">
						<td align="justify" valign="bottom">
							<a href="itms-services://?action=download-manifest&url=https://dl.dropboxusercontent.com/s/fses2ujcqcyjx53/cuentanosCU.plist">
								<div style="border-radius: 20px; " class="apps">
									<table class="apps">
										<tr>
											<td>
													<img src="../images/cuentanos.png" width="100" height="100"/>
											</td>
											<td valign="top">
												<h2 class="appsh2">CUENTANOS</h2>
												<p class="appsP">Queremos escucharte, cuéntanos tus ideas, peticiones, experiencias o pasiones.</p>
											</td>
										</tr>
									</table>
								</div>
							</a>
						</td>
					</c:when>
				</c:choose>
				
			</tr>
			<tr>
				<c:choose>
					<c:when test="${PM == '6'}">
						<td align="justify" valign="bottom">
							<a href="itms-services://?action=download-manifest&url=https://dl.dropboxusercontent.com/s/knmsuf4zehj04cp/manifestPM.plist">
								<div style="border-radius: 20px; " class="apps">
									<table class="apps">
										<tr>
											<td>
													<img src="../images/app_1b_icon.png" width="100" height="100"/>
											</td>
											<td valign="top">
												<h2 class="appsh2">Proveedor GS</h2>
												<p class="appsP">Herramienta para proveedores de mantemimiento.</p>
											</td>
										</tr>
									</table>
								</div>
							</a>
						</td>
					</c:when>
				</c:choose>
				
			</tr>
			
		</table>
		
	</div>
	
	<div class="footer">
		<table class="tblFooter">
		  <tbody>
			<tr>
			  <td>Banco Azteca S.A. Institución de Banca Múltiple</td>
			  <td>Derechos Reservados 2014 (Términos y Condiciones de uso del Portal).</td>
			</tr>
		  </tbody>
		</table>
	</div>
	
</body>
</html>
