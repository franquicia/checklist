package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.TipoArchivoDTO;

public class TipoArchivoRowMapper3 implements RowMapper<TipoArchivoDTO> {

	public TipoArchivoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		TipoArchivoDTO tipoArchivoDTO = new TipoArchivoDTO();
		
		tipoArchivoDTO.setIdTipoArchivo(rs.getInt("FIID_TIPO"));
		tipoArchivoDTO.setNombreTipo(rs.getString("FCNOMBRE"));
		
		return tipoArchivoDTO;
	}
	

}
