//Donas 2 
  var config0 = {
        type: 'doughnut',
        data: {
            datasets: [{
               data: [20,80],
                backgroundColor: [
                    window.chartColors.verde2,
                    window.chartColors.verde,
                    window.chartColors.verde1,
                ],
            }],
            labels: ["Sucursales por visitar", "Sucursales visitadas"]
        },
        options: {
      spanGaps: false,
      legend: { display: true},// quita cuadros
            responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: '% Sucursales visitadas por mes'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };


//Donas 1
    var config1 = {
        type: 'doughnut',
        data: {
            datasets: [{
               data: [30,40,30],
                backgroundColor: [
                    window.chartColors.verde,
                    window.chartColors.verde1,
                    window.chartColors.verde2,
                ],
            }],
            labels: ["Valor 1","Valor 2","Valor 3"]
        },
         options: {
      spanGaps: false,
      legend: { display: true},// quita cuadros
            responsive: true,
            legend: {position: 'right',},
            title: {display: true,text: 'Gráfica 1'},
            animation: {animateScale: true, animateRotate: true}
        }
    };


// vertical 1
    var barChartData = {
            labels: ["ENE", "FEB", "MAR", "ABR","MAY","JUN","JUL", "AGO", "SEP", "OCT", "NOV", "DIC"],
            datasets: [{
                label: "Visitas",
                backgroundColor: [
                  window.chartColors.verde,
                  window.chartColors.verde1, 
                  window.chartColors.verde2,
                  window.chartColors.verde,
                  window.chartColors.verde1, 
                  window.chartColors.verde2,
                  window.chartColors.verde,
                  window.chartColors.verde1, 
                  window.chartColors.verde2,
                  window.chartColors.verde,
                  window.chartColors.verde1, 
                  window.chartColors.verde2,
                ],
                borderWidth: 0,
                data: [20, 30, 36, 43, 47, 50, 52, 55, 60, 62, 66, 70]
            }]
        };
// vertical 2   
    var barChartData1 = {
            labels: ["ENE", "FEB", "MAR", "ABR","MAY","JUN","JUL", "AGO", "SEP", "OCT", "NOV", "DIC"],
            datasets: [{
                label: 'Calificación',
                backgroundColor: [
                  window.chartColors.verde1,
                  window.chartColors.verde2, 
                  window.chartColors.verde,
                  window.chartColors.verde1,
                  window.chartColors.verde2, 
                  window.chartColors.verde,
                  window.chartColors.verde1,
                  window.chartColors.verde2, 
                  window.chartColors.verde,
                  window.chartColors.verde1,
                  window.chartColors.verde2, 
                  window.chartColors.verde,

                ],
                borderColor: window.chartColors.verde1,
                borderWidth: 1,
                data: [73, 82, 71, 75, 90, 70, 74, 80, 65, 90, 88, 80]
            }]
        };


//Grafica Lineal
var presets = window.chartColors;
var utils = Samples.utils;
var inputs = {
};

utils.srand(42);

var data = {
  labels: [2015, 2016, 2017, 2018, 2019, 2020],
  datasets: [{
    backgroundColor: utils.transparentize(presets.verde1),
    borderColor: presets.verde1,
    data: [2, 10, 2, 8, 5, 10],
    label: 'Dato1',
    fill: ''
  }, {
    backgroundColor: utils.transparentize(presets.verde2),
    borderColor: presets.verde2,
    data: [4, 10, 4, 8, 5, 10],
    label: 'Dato2',
    fill: ''
  }, {
    backgroundColor: utils.transparentize(presets.verde),
    borderColor: presets.verde,
    data: [8, 10, 8, 8, 5, 10],
    label: 'Dato3',
    fill: ''
  }]
};

var options = {
  spanGaps: false,
  legend: { display: true},// quita cuadros
  legend: {osition: 'bottom',},
  elements: {line: {tension: 0.000001}},
  scales: {yAxes: [{stacked: true}]},
  title: {display: true,text: 'Gráfica 5'}
};

//horizontal
var color = Chart.helpers.color;
        var horizontalBarChartData = {
            labels: ["Type A", "Type C", "Type D", "Type C", "Type D"],
            datasets: [{
                label: 'Dataset 1',
                backgroundColor: color(window.chartColors.verde1).rgbString(),
                borderColor: window.chartColors.verde1,
                borderWidth: 1,
                data: [10,11,12,13,14,15,16]
            }, {
                label: 'Dataset 2',
                backgroundColor: color(window.chartColors.verde2).rgbString(),
                borderColor: window.chartColors.verde2,
                data: [3,11,12,13,14,15,16]
      }, {
                label: 'Dataset 3',
                backgroundColor: color(window.chartColors.verde).rgbString(),
                borderColor: window.chartColors.verde,
                data: [3,5,4,3,14,5,10]
            }]

        };

//Pie
 var configPie = {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        15,
                        10,
                        5,
                        30,
                        40,
                    ],
                    backgroundColor: [
                        window.chartColors.verde,
                        window.chartColors.verde1,
                        window.chartColors.verde2,
                        window.chartColors.verde3,
                        window.chartColors.verde4,
                    ],
                    label: 'Dataset 1'
                }],
                labels: [
                    "Red",
                    "Orange",
                    "Yellow",
                    "Green",
                    "Blue"
                ]
            },
            options: {
                responsive: true,
                pieceLabel: [{
              mode: 'percentage',
              precision: 2,
              arc: true,
              fontColor: '#000',
              position: 'default'
          },
          {
              mode: 'label',
            fontColor: '#000',
            arc: true,
            position: 'outside'
          }],
          title: {
            display: true,
            text: 'Grafica 7'
          }/*percentage*/

            }
        };
/******************************************/

//vertical 
window.onload = function() {
  var ctx = document.getElementById("canvas-vertical").getContext("2d");
  window.myBar = new Chart(ctx, {
    type: 'bar',
    data: barChartData,
    options: {
      spanGaps: false,
      legend: { display: true},// quita cuadros
      responsive: true,
       legend: {
        position: false,
      },
      title: {
        display: true,
        text: 'Visitas realizadas en el año'
      },
       scales: {
          yAxes: [{
              ticks: {
                  beginAtZero:true
              }
          }],
          xAxes: [{
              type: 'category',
              categoryPercentage: 0.5,
              barPercentage: 0.9
          }]
      }
    }
  });

 var ctx2 = document.getElementById("canvas-vertical2").getContext("2d");
 window.myBar = new Chart(ctx2, {
   type: 'bar',
   data: barChartData1,
   options: {
     spanGaps: false,
      legend: { display: true},// quita cuadros
      responsive: true,
       legend: {
        position: false,
      },
     title: {
       display: true,
       text: 'Progreso de Calificación de la Sucursal'
     },
      scales: {
      yAxes: [{
        ticks: {
          beginAtZero:true
        }
      }],
      xAxes: [{
        type: 'category',
        categoryPercentage: 0.5,
        barPercentage: 0.9
      }]
    }
   }
 });


  //Grafica Lineal
  var chart = new Chart('canvas-lineal', {
    type: 'line',
    data: data,
    options: options
  });
      
  /*Donas*/
  var circulo1 = document.getElementById("chart-dona").getContext("2d");
  window.myDoughnut = new Chart(circulo1, config0);

   var circulo2 = document.getElementById("chart-dona2").getContext("2d");
  window.myDoughnut = new Chart(circulo2, config1);
};


// Horizontal

var horizontal = document.getElementById("canvas-horizonatal").getContext("2d");
window.myHorizontalBar = new Chart(horizontal, {
  type: 'horizontalBar',
  data: horizontalBarChartData,
  options: {
    responsive: true,
    legend: { display: true},
  title: {
    display: true,
    text: 'Gráfica 6'
  }

  }
});

//Pie
var ctx = document.getElementById("canvas-pie").getContext("2d");
window.myPie = new Chart(ctx, configPie);



/******************************************/

new Chart(document.getElementById("mixed-chart"), {
    type: 'bar',
    data: {
      labels: ["Semana 30 2018", "Semana 29 2018", "Semana 30 2017"],
      datasets: [{
          label: "HC Activo",
          type: "line",         
          borderColor: "rgb(159, 193, 159)",
          data: [3500,3500,2500],
          fill: false
        }, {
          label: "HC Autorizado",
          type: "line",      
          borderColor: "#35d39d",
          data: [4400,4300,3500],
          fill: false
        }, {
          label: "Nómina Real Bruta",
          type: "bar",
          backgroundColor: color(window.chartColors.verde2).rgbString(),
          data: [4500,3500,400],
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Gráfica'
      },
      legend: { display: true},// quita cuadros
      responsive: true,
      legend: {
        position: 'top',
      },
      scales: {
            yAxes: [{
                ticks: {beginAtZero:true}
            }],
            xAxes: [{
              stacked: true,
                type: 'category',
                categoryPercentage: 0.9,
                barPercentage: 0.5
            }]
        }
    }
});

new Chart(document.getElementById("mixed-chart2"), {
    type: 'bar',
    data: {
      labels: ["1", "2", "3","4","5","6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
      datasets: [{
          label: "HC Activo",
          type: "line",
          borderColor: "#8f2104",
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000, 20000, 20000, 20000, 20000, 21000, 21000, 21000, 21000, 21000, 21000, 21000, 21000 ],
          fill: false
        }, {
          label: "HC Autorizado",
          type: "line",
          borderColor: "#04c046",
          data: [17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 20000, 22000, 22000, 22000, 22000, 22000, 25000, 25000, 25000, 25000, 25000, 25000, 30000, 30000 ],
          fill: false
        }, {
          label: "Nómina Real Bruta",
          type: "bar",
          backgroundColor: color(window.chartColors.verde2).rgbString(),
          data: [1500, 16000, 15000, 14000, 15000, 14000, 17000, 16000, 19000, 14000, 13000, 18000, 16000, 17000, 16000, 16000, 17000, 20000, 16000, 18000, 17000, 21000, 19000, 23000, 22000, 21000, 19000, 21000, 26000, 32000],
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Gráfica'
      },
      legend: { display: true},// quita cuadros
      responsive: true,
      legend: {
        position: 'top',
      },
      scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }],
            xAxes: [{
                type: 'category',
                categoryPercentage: 0.9,
                barPercentage: 0.5
            }]
        }
    }
});


