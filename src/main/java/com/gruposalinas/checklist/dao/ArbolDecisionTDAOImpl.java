package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ArbolDecisionDTO;
import com.gruposalinas.checklist.mappers.ArbolDecisionRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ArbolDecisionTDAOImpl extends DefaultDAO implements ArbolDecisionTDAO {

    private static Logger logger = LogManager.getLogger(ArbolDecisionTDAOImpl.class);

    private DefaultJdbcCall buscaArbolTemp;
    private DefaultJdbcCall insertaArbolDecisionTemp;
    private DefaultJdbcCall actualizaArbolDecisionTemp;
    private DefaultJdbcCall eliminaArbolDecisionTemp;

    public void init() {

        buscaArbolTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMARBOLTEMP")
                .withProcedureName("SP_SEL_G_ARBOL_TEMP")
                .returningResultSet("RCL_ARBOL_DES", new ArbolDecisionRowMapper());

        insertaArbolDecisionTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMARBOLTEMP")
                .withProcedureName("SP_INS_ARBOL_TEMP");

        actualizaArbolDecisionTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMARBOLTEMP")
                .withProcedureName("SP_ACT_ARBOL_TEMP");

        eliminaArbolDecisionTemp = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMARBOLTEMP")
                .withProcedureName("SP_DEL_ARBOL_TEMP");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ArbolDecisionDTO> buscaArbolDecisionTemp(int idChecklist) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        List<ArbolDecisionDTO> listaArbolDecision = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_CHECKLIST", idChecklist);

        out = buscaArbolTemp.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMARBOLTEMP.SP_SEL_G_ARBOL_DES}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listaArbolDecision = (List<ArbolDecisionDTO>) out.get("RCL_ARBOL_DES");

        if (error == 1) {
            logger.info("Algo ocurrió al eliminar arbol de decision de la tabla temporal");
        } else {
            return listaArbolDecision;
        }

        return null;
    }

    @Override
    public int insertaArbolDecisionTemp(ArbolDecisionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int id = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDARBOL", bean.getIdArbolDesicion())
                .addValue("PA_ID_CHECK", bean.getIdCheckList())
                .addValue("PA_ID_PREG", bean.getIdPregunta())
                .addValue("PA_RESPUESTA", bean.getRespuesta())
                .addValue("PA_ESTATUS_E", bean.getEstatusEvidencia())
                .addValue("PA_ORDENPREG", bean.getOrdenCheckRespuesta())
                .addValue("PA_COMMIT", bean.getCommit())
                .addValue("PA_REQACCION", bean.getReqAccion())
                .addValue("PA_REQOBS", bean.getReqObservacion())
                .addValue("PA_OBLIGA", bean.getReqOblig())
                .addValue("PA_DESC_E", bean.getDescEvidencia())
                .addValue("PA_NUM_REV", bean.getNumVersion())
                .addValue("PA_TIPO_MOD", bean.getTipoCambio());

        out = insertaArbolDecisionTemp.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMARBOLTEMP.SP_INS_ARBOL_TEMP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal idReturn = (BigDecimal) out.get("PA_IDARBOL");
        id = idReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar arbol de decision");
        } else {
            return id;
        }

        return 0;
    }

    @Override
    public boolean actualizaArbolDecisionTemp(ArbolDecisionDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_ARBOL_DES", bean.getIdArbolDesicion())
                .addValue("PA_ID_CHECK", bean.getIdCheckList())
                .addValue("PA_ID_PREG", bean.getIdPregunta())
                .addValue("PA_RESPUESTA", bean.getRespuesta())
                .addValue("PA_ESTATUS_E", bean.getEstatusEvidencia())
                .addValue("PA_ORDENPREG", bean.getOrdenCheckRespuesta())
                .addValue("PA_REQACCION", bean.getReqAccion())
                .addValue("PA_REQOBS", bean.getReqObservacion())
                .addValue("PA_OBLIGA", bean.getReqOblig())
                .addValue("PA_DESC_E", bean.getDescEvidencia())
                .addValue("PA_NUM_REV", bean.getNumVersion())
                .addValue("PA_TIPO_MOD", bean.getTipoCambio());

        out = actualizaArbolDecisionTemp.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMARBOLTEMP.SP_ACT_ARBOL_TEMP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar arbol de decision de la tabla temporal");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaArbolDecisionTemp(int idArbolDecision) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_ARBOL_DES", idArbolDecision);

        out = eliminaArbolDecisionTemp.execute(in);

        logger.info("Funci�n ejecutada:{checklist.PAADMARBOLTEMP.SP_DEL_ARBOL_TEMP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al eliminar arbol de decision de la tabla temporal");
        } else {
            return true;
        }

        return false;
    }

}
