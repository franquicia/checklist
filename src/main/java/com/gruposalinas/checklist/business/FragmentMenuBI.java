package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.FragmentMenuDAO;
import com.gruposalinas.checklist.domain.FragmentMenuDTO;

public class FragmentMenuBI {

	private static Logger logger = LogManager.getLogger(FragmentMenuBI.class);

private List<FragmentMenuDTO> listafila;
	
	@Autowired
	FragmentMenuDAO fragmentMenuDAO;
		
	
	public int insertaFragment(FragmentMenuDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = fragmentMenuDAO.insertaFragment(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaFragment(String idEmpFijo){
		boolean respuesta = false;
		
		try {
			respuesta = fragmentMenuDAO.eliminaFragment(idEmpFijo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<FragmentMenuDTO> obtieneDatosFragment( String idUsuario) {
		
		try {
			listafila =  fragmentMenuDAO.obtieneDatosFragment(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
		
	public boolean actualizaFragment(FragmentMenuDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = fragmentMenuDAO.actualizaFragment(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public int insertaMenu(FragmentMenuDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = fragmentMenuDAO.insertaMenu(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaMenu(String idEmpFijo){
		boolean respuesta = false;
		
		try {
			respuesta = fragmentMenuDAO.eliminaMenu(idEmpFijo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<FragmentMenuDTO> obtieneDatosMenu( String idUsuario) {
		
		try {
			listafila =  fragmentMenuDAO.obtieneDatosMenu(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
		
	public boolean actualizaMenu(FragmentMenuDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = fragmentMenuDAO.actualizaMenu(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	public int insertaConf(FragmentMenuDTO bean){
		int respuesta = 0;
		
		try {
			respuesta = fragmentMenuDAO.insertaConf(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	
	
	public boolean eliminaConf(String idEmpFijo){
		boolean respuesta = false;
		
		try {
			respuesta = fragmentMenuDAO.eliminaConf(idEmpFijo);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}

	public List<FragmentMenuDTO> obtieneDatosConf( String idUsuario) {
		
		try {
			listafila =  fragmentMenuDAO.obtieneDatosConf(idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
		
	public boolean actualizaConf(FragmentMenuDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = fragmentMenuDAO.actualizaConf(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
}