package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.LlaveDTO;

public class LlaveRowMapper implements RowMapper<LlaveDTO> {

	public LlaveDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		LlaveDTO llaveDTO = new LlaveDTO();
		
		llaveDTO.setIdLlave(rs.getInt("FIID_LLAVE"));
		llaveDTO.setLlave(rs.getString("FCLLAVE"));
		llaveDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
		
		return llaveDTO;
	}
	

}
