package com.gruposalinas.checklist.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gruposalinas.checklist.business.ChecklistBI;
import com.gruposalinas.checklist.business.ChecklistModificacionesBI;
import com.gruposalinas.checklist.business.ChecklistPreguntasComBI;
import com.gruposalinas.checklist.business.CorreoBI;
import com.gruposalinas.checklist.business.EnviaCorreoVisitasBI;
import com.gruposalinas.checklist.business.FirmaCheckBI;
import com.gruposalinas.checklist.business.ReporteChecksExpBI;
import com.gruposalinas.checklist.business.SucursalChecklistBI;
import com.gruposalinas.checklist.domain.ChecklistPreguntasComDTO;
import com.gruposalinas.checklist.domain.FirmaCheckDTO;
import com.gruposalinas.checklist.domain.ReporteChecksExpDTO;
import com.gruposalinas.checklist.domain.SucursalChecklistDTO;
import com.gruposalinas.checklist.domain.UsuarioDTO;

@Controller
public class Correo {
	
	private static final Logger logger = LogManager.getLogger(Correo.class);
	
	@Autowired
	CorreoBI correoBI;
	@Autowired
	ChecklistBI checklistBI;
	@Autowired
	ChecklistModificacionesBI checklistModificacionesBI;
	@Autowired
	EnviaCorreoVisitasBI enviaCorreoVisitasBI;
	
	@Autowired
	FirmaCheckBI	firmaCheckBI;
	@Autowired
	ReporteChecksExpBI reporteChecksBI;
	@Autowired
	SucursalChecklistBI sucursalChecklistBI;
	@Autowired
	ChecklistPreguntasComBI checklistPreguntasComBI;
	
	
	
	@RequestMapping(value="pruebaCorreo.htm", method = RequestMethod.GET)
	public @ResponseBody String setEmpCausa(HttpServletRequest request){
		try{
			//String basePath = FRQConstantes.getURLApacheServer()+request.getContextPath()+"/";
			//request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
			UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
			
			//int idOwner = 2;
			//int causa = 1;
			
			//if (causaBI.insertaColaboradorCausa(causa, idEmpleado)){
			if (true){
				//Se mandan los correos a creador y apoyador
				//correoBI.sendMailAutorizacion(userSession.getIdUsuario(), userSession, "");
				correoBI.sendMailAprobacion(true,userSession, "test");
			}
		}catch(Exception e){
			logger.info("Algo ocurrió..... " + e);
		}
		return null;
	}
	
	@RequestMapping(value="/central/autorizaCambiosCheck.htm", method = RequestMethod.POST)
	public ModelAndView autorizaCambiosCheck(HttpServletRequest request, 
			@RequestParam(value = "tipo", required = true, defaultValue = "") String tipo,
			@RequestParam(value = "idCheck", required = true, defaultValue = "") String idCheck,
			@RequestParam(value = "nombreCheck", required = true, defaultValue = "") String nombreCheck	){
			ModelAndView mv = new ModelAndView("resumenChecklist");
		try{
			
			////System.out.println("NOMBRE CHECK ARMANDO EL CORREO" + nombreCheck);
			UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
			////System.out.println("*********DATOS USER********* \n"+ userSession.getNombre() +"\n"+userSession.getIdUsuario());
			Boolean mail = false;
			Boolean autoriza = false;
			//Map<String, Object> listas = checklistBI.buscaResumenCheck(Integer.parseInt(userSession.getIdUsuario()));
			
			Map<String, Object> listas = checklistBI.buscaResumenCheck();
			
			if (tipo.equals("true")){
				mail = correoBI.sendMailAprobacion(Boolean.parseBoolean(tipo), userSession , nombreCheck);
				autoriza = checklistModificacionesBI.autorizaChecklist(1, Integer.parseInt(idCheck));
				
				mv.addObject("listaResumen",listas.get("activos"));
				mv.addObject("listaPendientes", listas.get("pendientes"));
				
				mv.addObject("show", tipo);
				//mv.addObject("respuesta", res);
				} else if (tipo.equals("false")){
					mail = correoBI.sendMailAprobacion(Boolean.parseBoolean(tipo), userSession , nombreCheck);
					autoriza = checklistModificacionesBI.autorizaChecklist(0, Integer.parseInt(idCheck));
					
					mv.addObject("listaResumen",listas.get("activos"));
					mv.addObject("listaPendientes", listas.get("pendientes"));
					
					mv.addObject("show", tipo);
					//mv.addObject("respuesta", res);
					}
			
			logger.info("MAIL--->" + mail + "AUTORIZA" + autoriza);
		}catch(Exception e){
			logger.info("Algo ocurrió..... " + e);
		}
		return mv;
	}
	
	
	@RequestMapping(value="pruebaCorreoAsigna.htm", method = RequestMethod.GET)
	public @ResponseBody String pruebaCorreoAsigna(HttpServletRequest request){
		String respuesta = "";
		try{
			respuesta =	enviaCorreoVisitasBI.enviaCorre();
		}catch(Exception e){
			logger.info("Algo ocurrió..... " + e);
		}
		return respuesta;
	}

	
	//http://10.53.33.83/checklist/enviaCorreoAperturaSucursal.htm?idBitacora=20527522&tipoVisita=Soft Opening&fechaVisita=20190801&economico=179&sucursal=480179&tipo=Nueva&territorio=TERRITORIO SB CENTRO PONIENTE&zona=ZONA SB MORELIA OCCIDENTE&imperdonable=0&aperturable=true&rutaImagen=//franquicia/ImagSucursal/48017909145208082019.jpg
	
	//http://10.53.33.83/checklist/enviaCorreoAperturaSucursal.htm?idBitacora=19900876&tipoVisita=Primer Recorrido&fechaVisita=20190725&economico=179&sucursal=480179&tipo=Nueva&territorio=TERRITORIO SB CENTRO PONIENTE&zona=ZONA SB MORELIA OCCIDENTE&imperdonable=0&aperturable=false&rutaImagen=//franquicia/ImagSucursal/48017909145208082019.jpg
	
	
	//http://10.53.33.83/checklist/enviaCorreoAperturaSucursal.htm?idBitacora=19862357&tipoVisita=Primer Recorrido&fechaVisita=20190726&economico=9968&sucursal=489968&tipo=Nueva&territorio=SB SUR&zona=SB TUXTLA SURESTE&imperdonable=0&aperturable=false&rutaImagen=
	//http://10.53.33.83/checklist/enviaCorreoAperturaSucursal.htm?idBitacora=19869597&tipoVisita=Primer Recorrido&fechaVisita=20190726&economico=2393&sucursal=482393&tipo=Nueva&territorio=TERRITORIO SB NORTE&zona=ZONA NORTE&imperdonable=0&aperturable=true&rutaImagen=
	//http://10.53.33.83/checklist/enviaCorreoAperturaSucursal.htm?idBitacora=21098780&tipoVisita=Primer%20Recorrido&fechaVisita=20190806&economico=9994&sucursal=489994&tipo=Nueva&territorio=ZONA%20PROYECTO%20GUADALAJARA&zona=ZONA%20GUADALAJARA&imperdonable=21&aperturable=false&rutaImagen=//franquicia/ImagSucursal/48999416333706082019.jpg
	//http://10.53.33.83/checklist/enviaCorreoAperturaSucursal.htm?idBitacora=28467&tipoVisita=Primer Recorrido&fechaVisita=20190805&economico=0100&sucursal=480100&tipo=Nueva&territorio=TERRITORIO SB CENTRO PONIENTE&zona=ZONA SB TOLUCA MEXICO PONIENTE&imperdonable=&aperturable=false&rutaImagen=//franquicia/ImagSucursal/48010010311005082019.jpg&tipoNegocio=
	//EL TIPO NEGOCIO ES : EKT, OCC y DAZ
	//envioSoporte: 0 = envia ambos correos incluyendo a sandra; 1 = envia correo sin sandra solo actavieja 2 = envia correo sin sandra solo acta nueva 3 = envia corre sin sandra con ambas actas
	@RequestMapping(value = "enviaCorreoAperturaSucursal.htm", method=RequestMethod.GET)
	public @ResponseBody String enviaCorreoActaExpansion(HttpServletRequest request,
			@RequestParam(value = "idBitacora", required = true, defaultValue = "0") int idBitacora,
			@RequestParam(value = "tipoVisita", required = true, defaultValue = "") String tipoVisita,
			@RequestParam(value = "fechaVisita", required = true, defaultValue = "") String fechaVisita,
			@RequestParam(value = "economico", required = true, defaultValue = "") String economico,
			@RequestParam(value = "sucursal", required = true, defaultValue = "") String sucursal,
			@RequestParam(value = "tipo", required = true, defaultValue = "") String tipo,
			@RequestParam(value = "territorio", required = true, defaultValue = "") String territorio,
			@RequestParam(value = "zona", required = true, defaultValue = "") String zona,
			@RequestParam(value = "imperdonable", required = true, defaultValue = "") String imperdonable,
			@RequestParam(value = "aperturable", required = true, defaultValue = "") String aperturable,
			@RequestParam(value = "rutaImagen", required = true, defaultValue = "") String rutaImagen,
			@RequestParam(value = "tipoNegocio", required = true, defaultValue = "") String tipoNegocio,
			@RequestParam(value = "envioSoporte", required = true, defaultValue = "0") int envioSoporte) {
		
		String respuesta = "";
		
		/*
        contenido = "{ " +
        	   "\"eco\":\"9995 \","+
        	   "\"sucursal\":\"MEGA AV BELEN QRO \"," +
        	   "\"territorio\":\"TERRITORIO SB CENTRO PONIENTE\"," +
        	   "\"tipo\":\"Nueva\"," +
        	   "\"zona\":\"ZONA SB QUERETARO BAJIO\"," +
        	   "\"region\":\"REGIONAL SB QUERETARO\","+
        	   "\"imperdonable\":["+  
        	            "{"+  
        	               "\"descripcion\":\"Esto es una Imperdonable nuevo 1 \""+
        	            "}"+
        	               
        	            ","+
        	               
					"{"+  
					"\"descripcion\":\"Esto es una Imperdonable nuevo 2 \""+
					"}"+
					
					","+

					"{"+  
					"\"descripcion\":\"Esto es una Imperdonable nuevo 3 \""+
					"}"+
        	   "],"+
        	   "\"idBitacora\":"+  
        	      "20878" +
        	   ","+
        	   "\"aperturbale\":\"false\""+
        	"}";*/
		
		
		List<String> imperdonablesLista =  new ArrayList<String>();
		
		
		String rutaImg = "";
		if(rutaImagen!=null && !rutaImagen.equals("")) {
			rutaImg = '"'+ rutaImagen + '"';
		}else {
			rutaImg = "/franquicia/firmaChecklist/firmalogo_fachada.png";
		}
        
		
		try {
			
			//AQUI OBTENGO LAS IMPERDONABLES, DEBO OBTENERLAS PERO CONCATENADAS (HIJA + PAPÁ)
			
			List<ChecklistPreguntasComDTO> listaImperdonables  = new ArrayList<ChecklistPreguntasComDTO>();
			
			if (checklistPreguntasComBI.obtieneInfoImp(idBitacora) != null && checklistPreguntasComBI.obtieneInfoImp(idBitacora).size() != 0) {
				
				listaImperdonables = getImperdonablesConcatenadas(checklistPreguntasComBI.obtieneInfoImp(idBitacora));
				
				for (int i = 0; i < listaImperdonables.size();i ++) { 
        			
        			ChecklistPreguntasComDTO obj = listaImperdonables.get(i);
        			String result = obj.getPregunta();
        			imperdonablesLista.add(result); 

				}
				
				
			}
			
		} catch (Exception e1) {
			logger.info("Algo ocurrió..... " + e1);
		}
        
        
		
		//Firmas
		List<FirmaCheckDTO> firmasB = firmaCheckBI.obtieneDatos(idBitacora+"");
		//List<FirmaCheckDTO> firmasB = firmaCheckBI.obtieneDatos("20878");
		//Respuestas NO
		List<ReporteChecksExpDTO> incidencias = reporteChecksBI.obtieneRespuestasNo(idBitacora);
		//List<ReporteChecksExpDTO> incidencias = reporteChecksBI.obtieneRespuestasNo(Integer.parseInt("18365"));
		
		
		if (firmasB!=null && !firmasB.isEmpty() && firmasB.get(0).getCeco() != null  && firmasB.get(0).getCeco() != "") {
			try {
	        		List<SucursalChecklistDTO> lista = sucursalChecklistBI.obtieneDatos(Integer.parseInt(firmasB.get(0).getCeco()));
	        		
	        		if(lista!=null) {
	        			for (SucursalChecklistDTO sucursalChecklistDTO : lista) {
						if(sucursalChecklistDTO!=null && sucursalChecklistDTO.getRuta()!=null && !sucursalChecklistDTO.getRuta().equals("")) {
							rutaImg = sucursalChecklistDTO.getRuta();
						}
					}
	        		}
        		//
			} catch (Exception e) {
				logger.info("SIN IMAGEN DE FACHADA DE SUCURSALES ");	
			}

		}
		
		if(envioSoporte==1) {
			try {
				correoBI.sendMailExpansionNuevoFormatoConteo(idBitacora+"",tipoVisita,fechaVisita,economico,sucursal,tipo,territorio,zona,imperdonablesLista,firmasB,incidencias,aperturable,rutaImg,tipoNegocio,1);
			}catch(Exception e) {
				logger.info("Algo ocurrió con Anterior..... " + e);
			}
		}
		else if(envioSoporte==2) {
			try {
				correoBI.actaEntregaSucursal(idBitacora+"",tipoVisita,fechaVisita,economico,sucursal,tipo,territorio,zona,imperdonablesLista,firmasB,incidencias,aperturable,rutaImg,tipoNegocio,1);
			}catch(Exception e) {
				logger.info("Algo ocurrió con Alexander..... " + e);
			}
		}
		else if(envioSoporte==3) {
			try {
				correoBI.sendMailExpansionNuevoFormatoConteo(idBitacora+"",tipoVisita,fechaVisita,economico,sucursal,tipo,territorio,zona,imperdonablesLista,firmasB,incidencias,aperturable,rutaImg,tipoNegocio,1);
			}catch(Exception e) {
				logger.info("Algo ocurrió con Anterior..... " + e);
			}
			
			try {
				correoBI.actaEntregaSucursal(idBitacora+"",tipoVisita,fechaVisita,economico,sucursal,tipo,territorio,zona,imperdonablesLista,firmasB,incidencias,aperturable,rutaImg,tipoNegocio,1);
			}catch(Exception e) {
				logger.info("Algo ocurrió con Alexander..... " + e);
			}
		}
		else {
			try {
				correoBI.sendMailExpansionNuevoFormatoConteo(idBitacora+"",tipoVisita,fechaVisita,economico,sucursal,tipo,territorio,zona,imperdonablesLista,firmasB,incidencias,aperturable,rutaImg,tipoNegocio);
			}catch(Exception e) {
				logger.info("Algo ocurrió con Anterior..... " + e);
			}
			
			try {
				correoBI.actaEntregaSucursal(idBitacora+"",tipoVisita,fechaVisita,economico,sucursal,tipo,territorio,zona,imperdonablesLista,firmasB,incidencias,aperturable,rutaImg,tipoNegocio);
			}catch(Exception e) {
				logger.info("Algo ocurrió con Alexander..... " + e);
			}
		}	
	
		return respuesta;
	}
	
	//Recibe Una lista Completa de NO (obtieneInfoNoImp , obtieneInfoImp)
	//Concatenado
	public List<ChecklistPreguntasComDTO> getImperdonablesConcatenadas(List<ChecklistPreguntasComDTO> lista) {
				
		List<ChecklistPreguntasComDTO> chklistPreguntasImp  = new ArrayList<ChecklistPreguntasComDTO>();
		List<ChecklistPreguntasComDTO> listaResult  = new ArrayList<ChecklistPreguntasComDTO>();

		listaResult.clear();
		
		chklistPreguntasImp = lista;
		
		for (int i = 0; i < chklistPreguntasImp.size() ; i++) {
			
			if (chklistPreguntasImp.get(i).getPregPadre() == 0 && chklistPreguntasImp.get(i).getIdcritica() == 1 ) {
				
				for (int j = 0; j < chklistPreguntasImp.size() ; j++) {
					
						if ( chklistPreguntasImp.get(i).getIdPreg() == chklistPreguntasImp.get(j).getPregPadre() ) {
							
							ChecklistPreguntasComDTO data = chklistPreguntasImp.get(j);
							data.setPregunta(chklistPreguntasImp.get(i).getPregunta()
									+ " : "+ chklistPreguntasImp.get(j).getPregunta());
							listaResult.add(data);
					}
				}
			}
		}
		
		return listaResult;

	}
	
}
