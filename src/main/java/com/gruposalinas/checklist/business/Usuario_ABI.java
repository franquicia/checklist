package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.Usuario_ADAO;
import com.gruposalinas.checklist.domain.Usuario_ADTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class Usuario_ABI {

	private static Logger logger = LogManager.getLogger(Usuario_ABI.class);
	
	@Autowired 
	Usuario_ADAO usuario_ADAO;
	
	List<Usuario_ADTO> listaUsuarios = null;
	List<Usuario_ADTO> listaUsuario = null;
	List<Usuario_ADTO> listaUsuariosPaso = null;
	List<Usuario_ADTO> listaUsuarioPaso = null;
	

	public List<Usuario_ADTO> obtieneUsuario(){
				
		try{
			listaUsuarios = usuario_ADAO.obtieneUsuario();	
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Usuario obtieneUsuario()");
					
		}
				
		return listaUsuarios;
	}
	
	public List<Usuario_ADTO> obtieneUsuario(int idUsuario){
				
		try{
			listaUsuario = usuario_ADAO.obtieneUsuario(idUsuario);	
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Usuario obtieneUsuario()");
				
		}
				
		return listaUsuario;
	}	

	public List<Usuario_ADTO> obtieneUsuarioPaso(){
				
		try{
			listaUsuarios = usuario_ADAO.obtieneUsuarioPaso();
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Usuario obtieneUsuarioPaso()");
					
		}
				
		return listaUsuarios;
	}
	
	public List<Usuario_ADTO> obtieneUsuarioPaso(String idUsuario, String idPuesto, String idCeco){
		
		if (idUsuario.equals("NULL"))
			idUsuario = null;
		if (idPuesto.equals("NULL"))
			idPuesto = null;
		if (idCeco.equals("NULL"))
			idCeco = null;
				
		try{
			listaUsuario = usuario_ADAO.obtieneUsuarioPaso(idUsuario, idPuesto, idCeco);	
		}
		catch(Exception e){
			logger.info("No fue posible obtener datos de la tabla Usuario obtieneUsuarioPaso()");
				
		}
				
		return listaUsuario;
	}	
	

	public boolean insertaUsuario( Usuario_ADTO usuario ) {
		
		boolean respuesta = false;
		
		try {		
			respuesta = usuario_ADAO.insertaUsuario(usuario);			
		} catch (Exception e) {
			logger.info("No fue posible insertar el Usuario insertaUsuario()");
			
		}
		
		return respuesta;		
	}
	
	public boolean[] cargaUsuarios() {
		
		boolean resCargaUsuario = false;
		boolean resBajaUsuario = false;
		boolean resCargaPerfilU = false;
		boolean[] respuestaPerfilUsua = new boolean[3];
		
		try {		
			resCargaUsuario = usuario_ADAO.cargaUsuarios();	
			resBajaUsuario = usuario_ADAO.bajaUsuarios();
			resCargaPerfilU = usuario_ADAO.cargaPerfilUsuarios();
			
			respuestaPerfilUsua[0] = resCargaUsuario;
			respuestaPerfilUsua[1] = resBajaUsuario;
			respuestaPerfilUsua[2] = resCargaPerfilU;
			
		} catch (Exception e) {
			logger.info("No fue posible realizar la cargaUsuarios cargaUsuarios()");
			
		}
		
		return respuestaPerfilUsua;		
	}

	public boolean actualizaUsuario (Usuario_ADTO usuario){
		
		boolean respuesta = false;
				
		try {
			respuesta = usuario_ADAO.actualizaUsuario(usuario);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Usuario actualizaUsuario()");
			
		}
							
		return respuesta;
	}
	
public boolean actualizaUsuarioCeco (Usuario_ADTO usuario){
		
		boolean respuesta = false;
				
		try {
			respuesta = usuario_ADAO.actualizaUsuarioCeco(usuario);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Usuario actualizaUsuarioCeco()");
			
		}
							
		return respuesta;
	}
	public boolean eliminaUsuario(int idUsuario){
		
		boolean respuesta = false;
		
		try{
			respuesta = usuario_ADAO.eliminaUsuario(idUsuario);
		}catch(Exception e){
			logger.info("No fue posible borrar el Usuario eliminaUsuario()");
			
		}
		
		return respuesta;
	}
}
