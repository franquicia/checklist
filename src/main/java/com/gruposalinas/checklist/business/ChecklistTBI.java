package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.ChecklistTDAO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class ChecklistTBI {

	private static final Logger logger = LogManager.getLogger(ChecklistTBI.class);

	@Autowired
	ChecklistTDAO checklistDAO;
	
	List<ChecklistDTO> listaChecklist= null;
	
	
	public int insertaChecklist(ChecklistProtocoloDTO checklistProtocoloDTO){
		int idChecklist = 0;
		
		try{
			idChecklist = checklistDAO.insertaChecklist(checklistProtocoloDTO);
		}catch(Exception e){
			logger.info("No fue posible insertar el Checklist");
			
		}
		
		return idChecklist;
	}
	
	public List<ChecklistDTO> buscaChecklist(int idChecklist){
		
		try{
			listaChecklist = checklistDAO.buscaChecklistTemp(idChecklist);
		}catch(Exception e){
			logger.info("No fue posible obtener el Checklist");
			
		}
		
		return listaChecklist;
		
	}
	

	public boolean actualizaChecklist(ChecklistDTO bean){
		boolean respuesta= false;
		
		try {
			respuesta = checklistDAO.actualizaChecklistTemp(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el Checklist de la tabla temporal");
			
		}
		
		return respuesta;
		
	}

	
	public boolean eliminaChecklist(int idCheckList){
		boolean respuesta= false;
		
		try {
			respuesta = checklistDAO.eliminaChecklistTemp(idCheckList);
		} catch (Exception e) {
			logger.info("No fue posible eliminar el Checklist de la tabla temporal");
			
		}
		
		return respuesta;
		
	}
	
	public boolean actualizaEdo(int idChecklist, int idEdo){
		boolean respuesta = false;
		
		try {
			respuesta = checklistDAO.actualizaEdo(idChecklist, idEdo);
		} catch (Exception e) {
			logger.info("No fue posible actualizar el estado el Checklist de la tabla temporal");
			
		}
		
		return respuesta;
		
	}
	
	public boolean ejecutaAutorizados(){
		
		boolean respuesta = false;
		
		try {
			respuesta = checklistDAO.ejecutaAutorizados();
			
		} catch (Exception e) {
			logger.info("No fue posible ejecutar el proceso de carga al Historico");
			
		}
		
		return respuesta;
		
	}
	
	
}
