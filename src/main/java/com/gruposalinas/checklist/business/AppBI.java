package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.AppDAO;
import com.gruposalinas.checklist.domain.AppPerfilDTO;


public class AppBI {

	private static Logger logger = LogManager.getLogger(AppBI.class);

	List<AppPerfilDTO> listaApp = null;
	
	@Autowired
	AppDAO appDAO;
	

	public int insertaApp(AppPerfilDTO bean) {
		
		int idCanal = 0;
		
		try {
			idCanal = appDAO.insertaApp(bean);		
		} catch (Exception e) {
			logger.info("No fue posible insertar");
		}
		
		return idCanal;
		
	}
		
	public boolean actualizaApp(AppPerfilDTO bean) {
		
		boolean respuesta = false ;
		
		try {
			respuesta = appDAO.actualizaApp(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualiza");
		}
		
		return respuesta;
	}
	
	public boolean eliminaApp(int idApp) {
		
		boolean respuesta = false ;
		
		try {
			respuesta = appDAO.eliminaApp(idApp);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
		}
		
		return respuesta;		
	}
	
	public List<AppPerfilDTO> buscaApp(String idApp) {
		
		try {
			listaApp = appDAO.buscaApp(idApp);
		} catch (Exception e) {
			logger.info("No fue posible consultar");
		}
				
		return listaApp;		
	}


	public int insertaAppPerfil(AppPerfilDTO bean) {
		
		int idCanal = 0;
		
		try {
			idCanal = appDAO.insertaAppPerfil(bean);	
		} catch (Exception e) {
			logger.info("No fue posible insertar");
		}
		
		return idCanal;
		
	}
		
	public boolean actualizaAppPerfil(AppPerfilDTO bean) {
		
		boolean respuesta = false ;
		
		try {
			respuesta = appDAO.actualizaAppPerfil(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
		}
		
		return respuesta;
	}
	
	public boolean eliminaAppPerfil(int idAppPerfil) {
		
		boolean respuesta = false ;
		
		try {
			respuesta = appDAO.eliminaAppPerfil(idAppPerfil);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
		}
		
		return respuesta;		
	}
	
	public List<AppPerfilDTO> buscaAppPerfil(String idApp, String idUsuario) {
		
		try {
			listaApp = appDAO.buscaAppPerfil(idApp, idUsuario);
		} catch (Exception e) {
			logger.info("No fue posible consultar");
		}
				
		return listaApp;		
	}

	
}
