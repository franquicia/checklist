package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.EdoChecklistDTO;

public interface EdoChecklistDAO {

	public List<EdoChecklistDTO> obtieneEdoChecklist() throws Exception;
	
	public List<EdoChecklistDTO> obtieneEdoChecklist(int idEdoCK) throws Exception;
	
	public int insertaEdoChecklist(EdoChecklistDTO bean) throws Exception;
	
	public boolean actualizaEdoChecklist(EdoChecklistDTO bean) throws Exception;
	
	public boolean eliminaEdoCheckList(int idEdoCK) throws Exception;
	
}
