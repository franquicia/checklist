package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.AdminSupDTO;
import com.gruposalinas.checklist.domain.AsignacionesSupDTO;

public class MenuSupRowMapper implements RowMapper<AdminSupDTO> {

	public AdminSupDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		AdminSupDTO asignaDTO = new AdminSupDTO();
		
		asignaDTO.setIdMenu(rs.getInt("FIMENU_ID"));
		asignaDTO.setDescMenu(rs.getString("FCDESCRIPCION"));
		
		return asignaDTO;
		
		
	}

}
