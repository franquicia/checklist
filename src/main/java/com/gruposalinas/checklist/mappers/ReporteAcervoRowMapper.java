package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gruposalinas.checklist.domain.ReporteImgDTO;

public class ReporteAcervoRowMapper implements RowMapper<ReporteImgDTO> {

	public ReporteImgDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ReporteImgDTO consultaDTO = new ReporteImgDTO();
		consultaDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
		consultaDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		consultaDTO.setRuta(rs.getString("FCRUTA"));
		
		return consultaDTO;
		
		
	}

}
