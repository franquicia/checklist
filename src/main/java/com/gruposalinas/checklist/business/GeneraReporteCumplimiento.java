package com.gruposalinas.checklist.business;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gruposalinas.checklist.domain.CecoDTO;
import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.VistaCDTO;
import com.gruposalinas.checklist.resources.FRQAppContextProvider;

public class GeneraReporteCumplimiento extends Thread {
		
	private String archivoExcel;
	private String ceco;
	private String fecha;
	HttpServletResponse response;
	private String usuario;
	private ReporteBI reporteBI;
	private CecoBI cecobi;
	private Logger logger;
	private String respuesta;
	



	public GeneraReporteCumplimiento(String fecha , String ceco) {
		
		this.ceco = ceco;
		this.fecha = fecha;
		this.response = response;
		
	
		
		reporteBI = (ReporteBI) FRQAppContextProvider.getApplicationContext().getBean("reporteBI");
		cecobi = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
		logger = LogManager.getLogger(GeneraReporteCumplimiento.class);
		
	}
	
	
	@Override
	public void run()  {
	
		respuesta = "";
		ServletOutputStream sos = null;
		String archivo = "";

		try {
			String aux = "";
			
			
			List<List> listaZonasService = consumeServicio("147247", this.fecha, ceco);
			aux += escribeFila(listaZonasService);
			
			List<CecoDTO> listaCecos  = cecobi.buscaCecosPasoP(Integer.parseInt(ceco));
			
			for(int i=0; i<listaCecos.size() ; i++){
				String cecoZona = listaCecos.get(i).getIdCeco();
				//System.out.println("Se ejecuta el ceco "+cecoZona);
				
				List<List> listaRegionesService = consumeServicio("147247", this.fecha, cecoZona);
				aux += escribeFila(listaRegionesService);	
				
				List<CecoDTO> listaRegiones  = cecobi.buscaCecosPasoP(Integer.parseInt(cecoZona));
				
				for(int j=0; j<listaRegiones.size() ; j++){
					String cecoRegion = listaRegiones.get(j).getIdCeco();
					//System.out.println("Se ejecuta el ceco "+cecoRegion);
					
					List<List> listaSucursales = consumeServicio("147247", this.fecha, cecoRegion);
					aux += escribeFila(listaSucursales);
					////System.out.println(listaSucursales);
					}
				 
				////System.out.println(listaZonas);
				}
			
			//System.out.println("AUXUILIAR  : "+aux);
			
			archivo += aux;

			setRespuesta(archivo);
		
			//System.out.println("Termine");
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		//super.run();
	}


	public String getArchivoExcel() {
		return archivoExcel;
	}


	public void setArchivoExcel(String archivoExcel) {
		this.archivoExcel = archivoExcel;
	}


	public String getCeco() {		
		return ceco;
	}


	public void setCeco(String ceco) {
		this.ceco = ceco;
	}


	public String getFecha() {
		return fecha;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
	public String getRespuesta() {
		return respuesta;
	}


	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	public List<List> consumeServicio(String usuario,String fecha,String ceco){
		int idUsuario=0;
		
		idUsuario=Integer.parseInt(usuario);
		logger.info("USUARIO "+ idUsuario);
		
		List<ChecklistDTO> listaChecklist = null;
		List<List> listaReporteCum= new ArrayList<List>();
		List<VistaCDTO> listaReporteCumPila= new ArrayList<VistaCDTO>();
		
		int porcentaje = 0, conteo=0;
		Map<String, Object> map= new HashMap<String, Object>();
		Map<String, Object> mapPila= new HashMap<String, Object>();
		List<List> listRetorno= new ArrayList<List>();
		//String fechaReporte=""+seleccionMes+"/"+seleccionAno;
		logger.info("datos antes de los mapas... ");
		int valorPila = 1;
		
		try {
			map=reporteBI.ReporteVistaCumplimientoSucursal(idUsuario,ceco ,21, 1,fecha,0);
			logger.info("map... " + map);
			if(valorPila==1){
				mapPila=reporteBI.ReporteVistaCumplimientoSucursal(idUsuario,ceco ,21, 1,fecha,2);
				logger.info("mapPila... " + mapPila);			
			}
				
		} catch (Exception e) {
			logger.info("Ocurrio algo... ");
		}
		if(map!=null){
			porcentaje= (Integer) map.get("porcentaje");
			logger.info("porcentaje... "+porcentaje);
			listaChecklist=(List<ChecklistDTO>) map.get("listaChecklist");
			logger.info("listaChecklist... "+listaChecklist);
			listRetorno.add(listaChecklist);
			
			listaReporteCum= (List<List>) map.get("listaSucursales");
			logger.info("listaSucursales... "+listaReporteCum);
			conteo = (Integer) map.get("conteo");
			logger.info("conteo... "+conteo);
		}			
		
		if(valorPila==1){
			if(mapPila!=null){
				listaReporteCumPila= (List<VistaCDTO>) mapPila.get("listaSucursales");
				logger.info("listaReporteCumPila... "+listaReporteCumPila);
				
				// NUEVA VALIDACION
				if(listaReporteCumPila.size()>1){					
					boolean flag = false;
					int l =1;
					int sumaAnt = 0;
					int sumaAct = 0;
					VistaCDTO act = null;
					VistaCDTO ant = null;
					while(l < listaReporteCumPila.size()){	
						sumaAnt= listaReporteCumPila.get(l - 1).getAsignados()+listaReporteCumPila.get(l - 1).getTerminados();
						sumaAct= listaReporteCumPila.get(l).getAsignados()+listaReporteCumPila.get(l).getTerminados();						
						if(sumaAnt < sumaAct){
							ant=listaReporteCumPila.get(l - 1);
							act=listaReporteCumPila.get(l);							
							listaReporteCumPila.set(l - 1, act);
							listaReporteCumPila.set(l, ant);
							flag=true;
						}
						if (flag && (l + 1) == listaReporteCumPila.size()) {
							l = 0;
							flag = false;
						}
						l++;
					}				
				}
				// NUEVA VALIDACION				
			}			
			for (int i = 0; i < listaReporteCumPila.size(); i++) {
				VistaCDTO listaVista = new VistaCDTO();
				listaVista=(VistaCDTO) listaReporteCum.get(i);
				
				VistaCDTO listaVistaPila = new VistaCDTO();
				listaVistaPila=(VistaCDTO) listaReporteCumPila.get(i);

				listaVista.setAsignados(listaVistaPila.getAsignados());
				listaVista.setTerminados(listaVistaPila.getTerminados());
			}
		}
		
		if(conteo==0)
			conteo++;
		
		List<VistaCDTO> listaAux=new ArrayList<VistaCDTO>();
		
		logger.info("listaReporteCum.size() "+ listaReporteCum.size());
		for (int i = 0; i < listaReporteCum.size(); i++) {							
			listaAux.add((VistaCDTO) listaReporteCum.get(i));			
			if((i+1)%conteo==0 ){
				listRetorno.add(listaAux);				
				listaAux=new ArrayList<VistaCDTO>();
			}
		}
		
		logger.info("listRetorno.size() "+listRetorno.size());
		logger.info("listRetorno "+listRetorno);

		logger.info("...SALE DEL AJAX ajaxFiltroSucursal...");
		
		return listRetorno;
	}
		
	public String escribeFila(List<List> lista ) {
		
		String respuesta ="";
		String filaChecklist ="";
	    int[] totales = new int[3];
	    int veces = 0;
		
		for(int i=1; i<lista.size(); i++) {
			
			String idCeco = "";
			String nombreCeco = "";
			
			for(int j =0; j< lista.get(i).size(); j++ ){
				
				VistaCDTO checklist = (VistaCDTO) lista.get(i).get(j);

				idCeco = checklist.getIdCeco();
				nombreCeco = checklist.getDescCeco();
				
				if(checklist.getIdChecklist().equals("97")) 
					totales[0] = checklist.getTotal();
				else if(checklist.getIdChecklist().equals("99"))
					totales[1] = checklist.getTotal();
				else if (checklist.getIdChecklist().equals("156")) 
					totales[2] = checklist.getTotal();	  
				
				
			}
			filaChecklist += "<tr>"
					       + "<td align='center'>"+idCeco+"</td>"
					       + "<td align='center'>"+nombreCeco+"</td>"
					       + "<td align='center'>"+totales[0]+" </td>"
					       + "<td align='center'>"+totales[1]+" </td>"
					       + "<td align='center'>"+totales[2]+" </td>"
					       + "</tr>";
			respuesta += filaChecklist;
			filaChecklist="";
			
			totales[0] =0;
			totales[1] =0;
			totales[2] =0;
			
		}
		
		return respuesta;
	}

}
