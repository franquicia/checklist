package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistDTO;
import com.gruposalinas.checklist.domain.TipoChecklistDTO;

public class ChecklistTRowMapper implements RowMapper<ChecklistDTO>  {

	public ChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistDTO checklistgeneral = new ChecklistDTO();
		TipoChecklistDTO tipochecklist = new TipoChecklistDTO(); 
		
		checklistgeneral.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		tipochecklist.setIdTipoCheck(rs.getInt("FIID_TIPO_CHECK"));
		checklistgeneral.setIdTipoChecklist(tipochecklist);
		checklistgeneral.setNombreCheck(rs.getString("FCNOMBRE"));
		checklistgeneral.setIdHorario(rs.getInt("FIID_HORARIO"));
		checklistgeneral.setVigente(rs.getInt("FIVIGENTE"));
		checklistgeneral.setFecha_inicio(rs.getString("FDFECHA_INICIO"));
		checklistgeneral.setFecha_fin(rs.getString("FDFECHA_FIN"));
		checklistgeneral.setIdEstado(rs.getInt("FIID_ESTADO"));
		checklistgeneral.setNumVersion(rs.getString("FINUMREVISION"));
		checklistgeneral.setTipoCambio(rs.getString("FCTIPOMODIF"));
		
		return checklistgeneral;
	}
}
