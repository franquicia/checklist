package com.gruposalinas.checklist.business;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.gruposalinas.checklist.dao.DetalleRespuestaReporteDAO;
import com.gruposalinas.checklist.domain.AlertasResumenDTO;
import com.gruposalinas.checklist.domain.DatosVisitaResumenDTO;
import com.gruposalinas.checklist.domain.DetalleRespuestaReporteDTO;
import com.gruposalinas.checklist.domain.EvidenciasResumenDTO;
import com.gruposalinas.checklist.domain.FoliosResumenDTO;
import com.gruposalinas.checklist.util.UtilFRQ;

public class DetalleRespuestaReporteBI {
	
	private Logger logger = LogManager.getLogger(DetalleRespuestaReporteBI.class);
	
	@Autowired
	DetalleRespuestaReporteDAO detalleRespuestaReporteDAO;
	
	public String obtieneRespuesta(int checklist, String ceco, String fecha){
		
		List<DetalleRespuestaReporteDTO> respuestas = null;
		
		JSONObject principal = new JSONObject();
					
		try {
			principal.put("checklist", checklist);
			principal.put("ceco", ceco);
			principal.put("fecha", fecha);
			
			respuestas = detalleRespuestaReporteDAO.obtieneRespuesta(checklist, ceco, fecha);
			
			JSONObject respuesta = null;
			JSONArray respuestasArr = new JSONArray();
			
			//Recorre lista de respuestas
			for(int i = 0 ; i < respuestas.size();i++ ){
				
				if(respuestas.get(i).getIdPreguntaPadre() == 0 ){
					
					respuesta = new JSONObject();
					respuesta.put("idModulo", respuestas.get(i).getIdModulo());
					respuesta.put("idPregunta", respuestas.get(i).getIdPregunta());
					respuesta.put("pregunta", respuestas.get(i).getPregunta());					
					respuesta.put("respuesta",respuestas.get(i).getRespuesta());
					respuesta.put("motivo",	"");
					
					String evidencia= "";
					
					if(respuestas.get(i).getEvidencia() != null)
						evidencia = respuestas.get(i).getEvidencia() ;
					
					String compromiso="";
					String fechaCompromiso="";
					
					for(int j = i+1; j < respuestas.size(); j++){
						
						String motivo = "";
						if(respuestas.get(j).getIdPreguntaPadre() == respuestas.get(i).getIdPregunta() ){
							
							if( respuestas.get(j).getEvidencia() != null ){
								evidencia = respuestas.get(j).getEvidencia();
							}
							
							if(respuestas.get(i).getRespuesta().equals("NO")){
																
								if (!respuestas.get(j).getRespuesta().equals("ABIERTA")){
									motivo +=  respuesta.getString("motivo") + respuestas.get(j).getRespuesta();
									respuesta.remove("motivo");
																
									respuesta.put("motivo", motivo);
																															
									if( respuestas.get(j).getAccion()!= null){
										compromiso = respuestas.get(j).getAccion();
									}
									
									if(respuestas.get(j).getFechaCompromiso() != null){
										fechaCompromiso = respuestas.get(j).getFechaCompromiso();
									}
								}
																				
							}
																				    						
						}
					}
					respuesta.put("evidencia",evidencia);
					respuesta.put("fechaCompromiso", fechaCompromiso);
					respuesta.put("folio", compromiso);
					respuestasArr.put(respuesta);
				}
				
				principal.put("respuestas",respuestasArr);
				
				
			}
		
		} catch (Exception e) {
			
			logger.info("No fue posible obtener las respuestas");
			
		}
		
		
		return principal.toString(); 
	}
	
	public Map<String,Object> obtieneResumen(int checklist,String ceco, String fecha){
		
		Map<String, Object> resumen = null ;
		
		try {
			resumen = detalleRespuestaReporteDAO.obtieneResumen(checklist, ceco, fecha);
		} catch (Exception e) {
			logger.info("No fue posible obtener el resumen");
			
		}
		
		
		return resumen;
		
	}
	
	public Map<String, Object> obtieneResumen2(int checklist,String ceco, String fecha){
		Map<String, Object> resumen = new HashMap<String, Object>();
		
		try {
			logger.info("Entro a ejecutar obtieneDatos");
			List<DatosVisitaResumenDTO> datos = detalleRespuestaReporteDAO.obtieneDatos(checklist, ceco, fecha);
			resumen.put("datosVisita", datos);			
			
		} catch (Exception e) {
			
			logger.info("Ocurrio un problema al consultar los datos de la visita");
			resumen.put("datosVisita", null);
			e.printStackTrace();
		}
		
		
		try {
			logger.info("Entro a ejecutar obtieneFolios");
			List<FoliosResumenDTO> folios = detalleRespuestaReporteDAO.obtieneFolios(checklist, ceco, fecha);
			resumen.put("folios", folios);			
			
		} catch (Exception e) {
			
			logger.info("Ocurrio un problema al consultar los folios de la visita");
			resumen.put("folios", null);
			e.printStackTrace();
		}
		
		try {
			logger.info("Entro a ejecutar obtieneEvidencias");
			List<EvidenciasResumenDTO> evidencias = detalleRespuestaReporteDAO.obtieneEvidencias(checklist, ceco, fecha);
			resumen.put("evidencias", evidencias);			
			
		} catch (Exception e) {
			
			logger.info("Ocurrio un problema al consultar las evidencias de la visita");
			resumen.put("evidencias", null);
			e.printStackTrace();
		}
		
		try {
			logger.info("Entro a ejecutar obtieneAlertas");
			List<AlertasResumenDTO> alertas = detalleRespuestaReporteDAO.obtieneAlertas(checklist, ceco, fecha);
			resumen.put("alertas", alertas);			
			
		} catch (Exception e) {
			
			logger.info("Ocurrio un problema al consultar las alertas de la visita");
			resumen.put("alertas", null);
			e.printStackTrace();
		}
		
		return resumen;
		
	}

}
