package com.gruposalinas.checklist.domain;

public class RolesSupDTO {
	
	private int idUsuario;
	private int idPerfil;
	private int idMenu;
	private int activo;
	private String usuaModificado;
	private String fechaModificacion;
	
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdPerfil() {
		return idPerfil;
	}
	public void setIdPerfil(int idPerfil) {
		this.idPerfil = idPerfil;
	}
	public int getIdMenu() {
		return idMenu;
	}
	public void setIdMenu(int idMenu) {
		this.idMenu = idMenu;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	public String getUsuaModificado() {
		return usuaModificado;
	}
	public void setUsuaModificado(String usuaModificado) {
		this.usuaModificado = usuaModificado;
	}
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	@Override
	public String toString() {
		return "RolesSupDTO [idUsuario=" + idUsuario + ", idPerfil=" + idPerfil + ", idMenu=" + idMenu + ", activo="
				+ activo + ", usuaModificado=" + usuaModificado + ", fechaModificacion=" + fechaModificacion + "]";
	}
		
}


