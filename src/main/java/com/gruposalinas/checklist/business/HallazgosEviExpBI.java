package com.gruposalinas.checklist.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.gruposalinas.checklist.util.UtilFRQ;
import com.gruposalinas.checklist.dao.HallazgosEviExpDAO;
import com.gruposalinas.checklist.domain.HallazgosEviExpDTO;

public class HallazgosEviExpBI {

	private static Logger logger = LogManager.getLogger(HallazgosEviExpBI.class);

private List<HallazgosEviExpDTO> listafila;
	
	@Autowired
	HallazgosEviExpDAO hallazgosEviExpDAO;
		
	
	public int inserta(HallazgosEviExpDTO bean){
		int respuesta = 1;
		
		try {
			respuesta = hallazgosEviExpDAO.inserta(bean);
		} catch (Exception e) {
			logger.info("No fue posible insertar ");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;		
	}
	

//elimina respuesta		
	public boolean elimina(String idResp){
		boolean respuesta = false;
		
		try {
			respuesta = hallazgosEviExpDAO.elimina(idResp);
		} catch (Exception e) {
			logger.info("No fue posible eliminar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	
	
	
	public List<HallazgosEviExpDTO> obtieneDatos( int idResp) {
		
		try {
			listafila =  hallazgosEviExpDAO.obtieneDatos(idResp);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	public List<HallazgosEviExpDTO> obtieneInfo() {

		try {
			listafila = hallazgosEviExpDAO.obtieneInfo();
		} catch (Exception e) {
			logger.info("No fue posible obtener datos");
			UtilFRQ.printErrorLog(null, e);	
		}

		return listafila;
	}

	
public List<HallazgosEviExpDTO> obtieneDatosBita( int bitGral) {
		
		try {
			listafila =  hallazgosEviExpDAO.obtieneDatosBita(bitGral);
		} catch (Exception e) {
			logger.info("No fue posible obtener los datos");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return listafila;			
	}
	
	
	
	public boolean actualiza(HallazgosEviExpDTO bean){
		boolean respuesta = false;
		
		try {
			respuesta = hallazgosEviExpDAO.actualiza(bean);
		} catch (Exception e) {
			logger.info("No fue posible actualizar");
			UtilFRQ.printErrorLog(null, e);	
		}
		
		return respuesta;			
	}
	



}