package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.PaisNegocioDTO;

public interface PaisNegocioDAO {
	
	public List<PaisNegocioDTO> obtienePaisNegocio(String negocio, String pais ) throws Exception;
	
	public boolean insertaPaisNegocio(int negocio, int pais)throws Exception;
	
	public boolean eliminaPaisNegocio(int negocio, int pais)throws Exception;

}
