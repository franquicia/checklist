package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.BitacoraGralDTO;



public interface BitacoraGralDAO {
//bita gral
	  public int inserta(BitacoraGralDTO bean)throws Exception;
		
		public boolean elimina(String idBitaGral)throws Exception;
		
		public List<BitacoraGralDTO> obtieneDatos(int idBitaGral) throws Exception; 
		
		public List<BitacoraGralDTO> obtieneInfo() throws Exception; 
		
		public boolean actualiza(BitacoraGralDTO bean)throws Exception;
		
		// bitacoras hijas
		
		  public int insertaH(BitacoraGralDTO bean)throws Exception;
			
			public boolean eliminaH(String idBitaGral)throws Exception;
			
			public List<BitacoraGralDTO> obtieneDatosH(int idBitaGral) throws Exception; 
			
			public List<BitacoraGralDTO> obtieneInfoH() throws Exception; 
			
			public boolean actualizaH(BitacoraGralDTO bean)throws Exception;
			
			//inserta padre e hijas
			
			public int ejecutaSP(String bitacora)throws Exception;
			
			
	}