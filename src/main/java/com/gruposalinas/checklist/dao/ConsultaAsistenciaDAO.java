package com.gruposalinas.checklist.dao;

import java.util.List;

import com.gruposalinas.checklist.domain.ConsultaAsistenciaDTO;


public interface ConsultaAsistenciaDAO {

	public  List<ConsultaAsistenciaDTO> obtieneAsistencia(ConsultaAsistenciaDTO bean ) throws Exception;
	}