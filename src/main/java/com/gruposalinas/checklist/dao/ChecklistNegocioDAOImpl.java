package com.gruposalinas.checklist.dao;

import com.gruposalinas.checklist.domain.ChecklistNegocioDTO;
import com.gruposalinas.checklist.mappers.ChecklistNegocioRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ChecklistNegocioDAOImpl extends DefaultDAO implements ChecklistNegocioDAO {

    private static Logger logger = LogManager.getLogger(ChecklistNegocioDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneChecklistNegocio;
    private DefaultJdbcCall jdbcInsertaChecklistNegocio;
    private DefaultJdbcCall jdbcEliminaChecklistNegocio;

    public void init() {

        jdbcObtieneChecklistNegocio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCKNEGOCIO")
                .withProcedureName("SPOBTIENENEG")
                .returningResultSet("RCL_CONSULTA", new ChecklistNegocioRowMapper());

        jdbcInsertaChecklistNegocio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCKNEGOCIO")
                .withProcedureName("SPINSERTANEG");

        jdbcEliminaChecklistNegocio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMCKNEGOCIO")
                .withProcedureName("SPELIMINANEG");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ChecklistNegocioDTO> obtieneChecklistNegocio(String checklist, String negocio) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistNegocioDTO> checklistNegocio = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCHECK", checklist).addValue("PA_NEGOCIO", negocio);

        out = jdbcObtieneChecklistNegocio.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMCKNEGOCIO.SPOBTIENENEG}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        checklistNegocio = (List<ChecklistNegocioDTO>) out.get("RCL_CONSULTA");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al consultar los Checklist-Negocio ");
        }

        return checklistNegocio;
    }

    @Override
    public boolean insertaChecklistNegocio(int checklist, int negocio) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCHECK", checklist).addValue("PA_NEGOCIO", negocio);

        out = jdbcInsertaChecklistNegocio.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMCKNEGOCIO.SPINSERTANEG}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean eliminaChecklistNegocio(int checklist, int negocio) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCHECK", checklist).addValue("PA_NEGOCIO", negocio);

        out = jdbcEliminaChecklistNegocio.execute(in);

        logger.info("Funcion ejecutada:{checklist.PAADMCKNEGOCIO.SPINSERTANEG}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 1) {
            return true;
        } else {
            return false;
        }
    }

}
