package com.gruposalinas.checklist.business;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.gruposalinas.checklist.dao.AsistenciaSupervisorDAO;
import com.gruposalinas.checklist.domain.AsistenciaSupervisorDTO;

public class AsistenciaSupervisorBI {
		
	private static Logger logger = LogManager.getLogger(AsistenciaSupervisorBI.class);
	
	@Autowired
	AsistenciaSupervisorDAO asistenciaSupervisorDAO;
	
	List<AsistenciaSupervisorDTO> listaAsistencia = null;
	
	public List<AsistenciaSupervisorDTO> buscaAsistencia(AsistenciaSupervisorDTO bean){
	
		try {
			listaAsistencia = asistenciaSupervisorDAO.obtieneAsistenciaSupervisor(bean);
		} catch (Exception e) {
			logger.info("No fue posible consultar la asistencia del supervisor: "+e);
		}			
		
		return listaAsistencia;
	
	}
}
