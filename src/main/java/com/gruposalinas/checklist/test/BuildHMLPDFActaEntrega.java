package com.gruposalinas.checklist.test;

public class BuildHMLPDFActaEntrega {
	String colorOperable = "";
	String operable = "";
	String sucursal ="";
	String territorio ="";
	String zona ="";
	String tipoVisita ="";
	String fecha ="";
	String economico ="";
	String preCalificacion ="";
	
	BuildHMLPDFActaEntrega(String colorOperable, String operable, String sucursal,String territorio, String zona, String tipoVisita, String fecha,String economico, String preCalificacion,
						  String colorCalificacion, String colorLetraCalificacion){
		this.colorOperable=colorOperable;
		this.operable=operable;
		this.sucursal=sucursal;
		this.territorio=territorio;
		this.zona=zona;
		this.tipoVisita=tipoVisita;
		this.fecha=fecha;
		this.economico=economico;
		this.preCalificacion=preCalificacion;
		
	}
	
	public String generaHTML() {
		String html="";
		
	/*	html = "<html>" + "<head>" + "<title>Acta Entrega</title>" + "</head>"
				+"<style>"
				+"div {color:black;}"
				+"table,td,th {font-size: 10px;}"
				+"a:link {color: white;}" 
				+"a:visited {color: white;}"
				+"a:hover {color: white;}"
				+"a:active {color: white;}"
				+ "td,th {border: 0.25px solid gray;}"
				+"#detalle {text-align:center;}"
				+"#mini {text-color:blue;}"
				+"#mini {font-size: 9px;}" 
				+"</style>"
				
				+ "<body>" +
				//Nuevo Formato

				//ABRE DIV LOGOS
				"<div>" + "<h2 align='center'>" +
				"<label>ENTREGA DE SUCURSAL</label>"+
				"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				"<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoElektra.png' width='33' height='33' />" +
				 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
				+"<img src='http://10.53.33.82/franquicia/firmaChecklist/LogoBAZ.png' width='45' height='30'/>"+
				"</h2>" + "</div>" +
				// CIERRA DIV LOGOS

				"<TABLE cellspacing='5' style=' width:100%; height:340px; text-align:center;' >"+
				
					"<TR>"+

						"<TD bgcolor='red' ROWSPAN='3' style=' width:5%; background-color:"+ colorOperable +"; color:white; font-size: 13px;'>"+ operable +"</TD>"+
						
					    "<TD ROWSPAN='3' style=' width:25%;'> " +
						
						"<div style='width:'200'; height:'150';  background-color:red; >" + "<h2 align='center'>" + "<img src='" + "/var/folders/k3/wxxyt2816w1986p6tlgdy26c0000gp/T/ej_suc.png"
						+ "' width='165%' height='111%'/>" + "</h2>" + "</div>"
						
					    + "</TD>" +
	
					    "<TD colspan='2' style='text-align:left; background-color:#F0EFF0;'>" +
					    
					    	"<div style='float:left; width:50%; margin:auto; padding-left:5px; padding-top:10px; padding-bottom:10px; '>" +
					    	
					     		"<b>Sucursal:</b>" + sucursal + " <br></br> <b>Zona: </b>"+ zona +"<br></br> <b>Territorio: </b>"+ territorio +
					    	
					    	"</div>"+
					    	
							"<div style='float:left; width:100%; heigth:200%; padding-left:2px; padding-top:30px;'>" +
							
								"<b>No Económico:</b>" + economico +"<br> </br> <b>Tipo Visita: </b>"+ tipoVisita +"<br></br> <b>Fecha: </b>"+ fecha +
							
							"</div>"+
					    
					    
					    "</TD> " +
				    
				    //"<TD style='text-align:left; font-size: 10px; background-color:#F0EFF0;'> <b>No Económico:</b>"+ "10029988" +"<br></br> <b>Tipo Visita: </b>"+"Soft Opening"+"<br></br> <b>Fecha: </b>"+ "29/10/2019" +" </TD> " +
				    
				    
					"</TR>"+

				// <b></b>  <br></br> 
				"<TR>"+
				"<TD style='text-align:center; font-size: 12px; background-color:#F0EFF0;'><H5>Generales de la recepción</H5> Pre Calificación  <strong style='text-align:center; font-size: 15px;'>"+ preCalificacion +"</strong> </TD> "
				+ "<TD style='background-color:"+ colorCalificacion +"; color:"+ colorLetraCalificacion +"'> Calificación: <strong style='text-align:center; font-size: 15px;'>"+ calificacion +"</strong> </TD>"+
			"</TR>"+

			"<TR>"+
			"<TD style='text-align:center; background-color:#F0EFF0;'> <p><b>Imperdonables:</b> <strong style='text-align:center; font-size: 15px;'>"+ numImperdonables +"</strong></p> </TD>"
			+ "<TD style='background-color:black;'> <a style='color:white;' href='http://10.51.218.140:8080/checklist/central/detalleChecklistExpancionImperdonable.htm?idUsuario=189871&idBitacora="+idBitacora+"'>Ver Detalles</a></TD>"+
			//http://10.51.218.140:8080/checklist/central/detalleChecklistExpancionImperdonable.htm?idBitacora=26282
			"</TR>"+

			"</TABLE>"+
			

			
			//ITEMS
			"<TABLE style=' width:100%; height:70px; text-align:center'>"+			
				"<TR>"+
				"<TD style='text-align:center; font-size: 12px; background-color:#F0EFF0;'> <strong style='text-align:center; font-size: 15px;'>"+ totalItems +"</strong><br></br><br></br> Items Revisados </TD>"+
				"<TD style='text-align:center; font-size: 12px; background-color:#D8D8D8;'><strong style='text-align:center; font-size: 15px;'>"+ itemsAprobados +"</strong><br></br> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados01.png' width='20' height='20'/>&nbsp; Items Aprobados </TD>"+
				"<TD style='text-align:center; font-size: 12px; background-color:#6B696E; color:white;'><strong style='text-align:center; font-size: 15px;'>"+ itemsPorRevisar  +"</strong><br></br> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender01.png' width='20' height='20'/>&nbsp; Items Por Revisar </TD>"+
				"</TR>"+
			//ITEMS
			"</TABLE>"+
				
			"<div id='detalle'> Detalles de la Entrega </div>" +
			//Div items
			"<TABLE style='width:100%; text-align:center' cellspacing='5'>"+
			
			" <TR>"+
			
			 " <TD style='width:50%; height:202px; '> " +
			 
				"<div style='width:100%; height:202px;'>"+
				
						//Titulo
						"<br></br>"+
						"<div>" + "<h5 align='center'>" +
						
								"<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaAreasComunes.png' width='25' height=20'/>"+
								"&nbsp; Áreas Comunes"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "<img src='http://10.53.33.82/franquicia/firmaChecklist/"+logoCom+"' width='60' height='25'/>"+
								 "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "Total: "+ comTotal +
						"</h5>" + "</div>" +			
						
						//Div Imagen
						"<div>"+
							"<div style='float: left; width: 200px;'>"+
							"<img src='"+ graficaComunes.getAbsolutePath() +"' width='180px' height='90%' />" +
						"</div>"+
						
						//Mini tabla
						"<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"+
						
							"<table cellspacing='0'>"+
							  "<tr  style='color:white; background-color:black;'>"+
							    "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"+
							    "<th style='text-align:center; font-size: 8px;'>items</th>"+
							    "<th style='text-align:center; font-size: 8px;' >items %</th>"+
							    "<th style='text-align:center; font-size: 8px;'>Calificación</th>"+
							  "</tr>"+
							  "<tr>"+
							  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados01.png' width='15' height='15'/> </td>"+
							    "<td>"+ comResSi +"</td>"+
							    "<td>"+ comSiPor +"</td>"+
							    "<td rowspan='2' style='background-color:#F0EFF0;'>"+ comCalif +"</td>"+
							  "</tr>"+
							  "<tr>"+
							  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender01.png' width='15' height='15'/> </td>"+
							  	 "<td>"+ comResNo +"</td>"+
								 "<td>"+ comNoPor +"</td>"+
							  "</tr>"+
							"</table>"+
							  
						"</div>"+
					
					"</div>"+

					"<br></br>"+
					"<a style='color:black;' href='http://10.51.218.140:8080/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="+idBitacora+"&tipo=1'>Ver Detalles</a>"+
				
				" </div>"

			 + "</TD>"
			 
			 
			 
			 +"<TD style='width:50%; height:202px; background-color:white;'> " + 				
			 
				"<div style='width:100%; height:202px;'>"+

						//Titulo
						"<br></br>"+
						"<div>" + "<h5 align='center'>" +
						
								"<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaBAZ.png' width='35' height='25'/>"+
								"&nbsp; Banco Azteca"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "<img src='http://10.53.33.82/franquicia/firmaChecklist/"+ logoBaz +"' width='60' height='25'/>"+
								 "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "Total: "+ bazTotal +
						"</h5>" + "</div>" +				
						
						//Div Imagen
						"<div>"+
							"<div style='float: left; width: 200px;'>"+
							"<img src='"+ graficaBanco.getAbsolutePath() +"' width='180px' height='90%' />" +
						"</div>"+
						
							//Mini tabla
							"<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"+
							
								"<table cellspacing='0'>"+
								  "<tr  style='color:white; background-color:black;'>"+
								    "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"+
								    "<th style='text-align:center; font-size: 8px;'>items</th>"+
								    "<th style='text-align:center; font-size: 8px;'>items %</th>"+
								    "<th style='text-align:center; font-size: 8px;'>Calificación</th>"+
								  "</tr>"+
								  "<tr>"+
								  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados01.png' width='15' height='15'/> </td>"+
								  	"<td>"+ bazResSi +"</td>"+
								    "<td>"+ bazSiPor +"</td>"+
								    "<td rowspan='2' style='background-color:#F0EFF0;'>"+ bazCalif +"</td>"+
								  "</tr>"+
								  "<tr>"+
								  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender01.png' width='15' height='15'/> </td>"+
								  	"<td>"+ bazResNo +"</td>"+
								    "<td>"+ bazNoPor +"</td>"+
								  "</tr>"+
								"</table>"+
							  
						"</div>"+
					
					"</div>"+

					"<br></br>"+
					"<a style='color:black;' href='http://10.51.218.140:8080/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="+idBitacora+"&tipo=2'>Ver Detalles</a>"+
				
				" </div>"+
			 
			 "</TD> " +
			 
 	 
			 "</TR>"+
			 
			 
			 "<TR>"+
			 
			 "<TD style='width:50%; height:202px; background-color:white;'> " + 				
			  
				"<div style='width:100%; height:202px;'>"+

						//Titulo
						"<br></br>"+
						"<div>" + "<h5 align='center'>" +
						
								"<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaElektra.png' width='25' height='25'/>"+
								"&nbsp; Elektra"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "<img src='http://10.53.33.82/franquicia/firmaChecklist/"+ logoEkt +"' width='60' height='25'/>"+
								 "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "Total: "+ ektTotal +
						"</h5>" + "</div>" +	
						
						//Div Imagen
						"<div>"+
							"<div style='float: left; width: 200px;'>"+
							"<img src='"+ graficaElektra.getAbsolutePath() +"' width='180px' height='90%' />" +
						"</div>"+
						
							//Mini tabla
							"<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"+
							
								"<table cellspacing='0'>"+
								  "<tr  style='color:white; background-color:black;'>"+
								    "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"+
								    "<th style='text-align:center; font-size: 8px;'>items</th>"+
								    "<th style='text-align:center; font-size: 8px;'>items %</th>"+
								    "<th style='text-align:center; font-size: 8px;'>Calificación</th>"+
								  "</tr>"+
								  "<tr>"+
								  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados01.png' width='15' height='15'/> </td>"+
								  	"<td>"+ ektResSi +"</td>"+
								    "<td>"+ ektSiPor +"</td>"+
								    "<td rowspan='2' style='background-color:#F0EFF0;'>"+ ektCalif +"</td>"+
								  "</tr>"+
								  "<tr>"+
								  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender01.png' width='15' height='15'/> </td>"+
								  	"<td>"+ ektResNo +"</td>"+
								    "<td>"+ ektNoPor +"</td>"+
								  "</tr>"+
								"</table>"+
							  
						"</div>"+
					
					"</div>"+

					"<br></br>"+
					"<a style='color:black;' href='http://10.51.218.140:8080/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="+idBitacora+"&tipo=3'>Ver Detalles</a>"+
				
				" </div>"+
			  
			  "</TD> "+
			  
			 "<TD style='width:50%; height:202px; background-color:white;'> " + 				
			  
					"<div style='width:100%; height:202px;'>"+

						//Titulo
						"<br></br>"+
						"<div>" + "<h5 align='center'>" +
						
								"<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaGenerales.png' width='25' height='25'/>"+
								"&nbsp; Generales"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "<img src='http://10.53.33.82/franquicia/firmaChecklist/"+ logoGen +"' width='60' height='25'/>"+
								 "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "Total: "+ genTotal +
						"</h5>" + "</div>" +					
						
						//Div Imagen
						"<div>"+
							"<div style='float: left; width: 200px;'>"+
							"<img src='"+ graficaGenerales.getAbsolutePath() +"' width='180px' height='90%' />" +
						"</div>"+
						
							//Mini tabla
							"<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"+
							
								"<table cellspacing='0'>"+
								  "<tr  style='color:white; background-color:black;'>"+
								    "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"+
								    "<th style='text-align:center; font-size: 8px;'>items</th>"+
								    "<th style='text-align:center; font-size: 8px;'>items %</th>"+
								    "<th style='text-align:center; font-size: 8px;'>Calificación</th>"+
								  "</tr>"+
								  "<tr>"+
								  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados01.png' width='15' height='15'/> </td>"+
								  	"<td>"+ genResSi +"</td>"+
								    "<td>"+ genSiPor +"</td>"+
								    "<td rowspan='2' style='background-color:#F0EFF0;'>"+ genCalif +"</td>"+
								  "</tr>"+
								  "<tr>"+
								  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender01.png' width='15' height='15'/> </td>"+
								  	"<td>"+ genResNo +"</td>"+
								    "<td>"+ genNoPor +"</td>"+
								  "</tr>"+
								"</table>"+
							  
						"</div>"+
					
					"</div>"+

					"<br></br>"+
					"<a style='color:black;' href='http://10.51.218.140:8080/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="+idBitacora+"&tipo=4'>Ver Detalles</a>"+
				
				" </div>"+
			  
			  "</TD>" +
			  
			 "</TR>"+

			 
 			"<TR>"+
 
			 "<TD style='width:50%; height:202px; background-color:white;'> " + 				
			  
					"<div style='width:100%; height:202px;'>"+

						//Titulo
						"<br></br>"+
						"<div>" + "<h5 align='center'>" +
						
								"<img src='http://10.53.33.82/franquicia/firmaChecklist/zonaPrestaPrenda.png' width='35' height='25'/>"+
								"&nbsp; Presta Prenda"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "<img src='http://10.53.33.82/franquicia/firmaChecklist/"+ logoPres +"' width='60' height='25'/>"+
								 "<br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
								 "Total: "+ prestaTotal +
						"</h5>" + "</div>" +			
						
						//Div Imagen
						"<div>"+
							"<div style='float: left; width: 200px;'>"+
							"<img src='"+ graficaPresta.getAbsolutePath() +"' width='180px' height='90%' />" +
						"</div>"+
						
							//Mini tabla
							"<div style='float: right; width: 80px; bottom:0; padding-top: 50px;'>"+
							
								"<table cellspacing='0'>"+
								  "<tr  style='color:white; background-color:black;'>"+
								    "<th style='color:white; background-color:white; border: 0.25px solid white'></th>"+
								    "<th style='text-align:center; font-size: 8px;'>items</th>"+
								    "<th style='text-align:center; font-size: 8px;'>items %</th>"+
								    "<th style='text-align:center; font-size: 8px;'>Calificación</th>"+
								  "</tr>"+
								  "<tr>"+
								  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados01.png' width='15' height='15'/> </td>"+
								  	"<td>"+ prestaResSi +"</td>"+
								    "<td>"+ prestaSiPor +"</td>"+
								    "<td rowspan='2' style='background-color:#F0EFF0;'>"+ prestaCalif +"</td>"+
								  "</tr>"+
								  "<tr>"+
								  	"<td style='color:white; background-color:white; border: 0.25px solid white'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender01.png' width='15' height='15'/> </td>"+
								  	"<td>"+ prestaResNo +"</td>"+
								    "<td>"+ prestaNoPor +"</td>"+
								  "</tr>"+
								"</table>"+
							  
						"</div>"+
					
					"</div>"+

					"<br></br>"+
					"<a style='color:black;' href='http://10.51.218.140:8080/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="+idBitacora+"&tipo=5'>Ver Detalles</a>"+
				
				" </div>"+
			  
			    "</TD> "
			  
			 +"<TD style='width:50%; height:202px; background-color:#F0EFF0;'> " +
			  
"					<div style='width:100%; height:202px;'>"+

							//Titulo
							"<br></br>"+
							"<div>" + "<h5 align='center'>" +
							
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "TOTAL"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "<img src='http://10.53.33.82/franquicia/firmaChecklist/"+ logoTotal +"' width='60' height='25'/>"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
									 "Total: "+ totalTotal +
									 
									 
							"</h5>" + "</div>" +				
						
						//Div Imagen
						"<div>"+
							"<div style='float: left; width: 200px;'>"+
							"<img src='"+ graficaTotal.getAbsolutePath() +"' width='180px' height='90%' />" +
						"</div>"+
						
							//Mini tabla
							"<div style='float: right; width: 80px; bottom:0; padding-top: 50px; '>"+
							
								"<table cellspacing='0'>"+
								  "<tr  style='color:white; background-color:black;'>"+
								    "<th style='color:white; background-color:#F0EFF0; border: 0 px solid #F0EFF'></th>"+
								    "<th style='text-align:center; font-size: 8px;'>items</th>"+
								    "<th style='text-align:center; font-size: 8px;'>items %</th>"+
								    "<th style='text-align:center; font-size: 8px;'>Calificación</th>"+
								  "</tr>"+
								  "<tr>"+
								  	"<td style='color:white; background-color:#F0EFF0; border: 0 px solid #F0EFF'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAprovados01.png' width='15' height='15'/> </td>"+
								  	"<td>"+ totalResSi +"</td>"+
								    "<td>"+ totalSiPor +"</td>"+
								    "<td rowspan='2' style='background-color:#F0EFF0;'>"+ totalCalif +"</td>"+
								  "</tr>"+
								  "<tr>"+
								  	"<td style='color:white; background-color:#F0EFF0; border: 0 px solid #F0EFF'> <img src='http://10.53.33.82/franquicia/firmaChecklist/ItemsAtender01.png' width='15' height='15'/> </td>"+
								  	"<td>"+ totalResNo +"</td>"+
								    "<td>"+ totalNoPor +"</td>"+
								  "</tr>"+
								"</table>"+
							  
						"</div>"+
					
					"</div>"+

					"<br></br>"+
					"<a style='color:black;' href='http://10.51.218.140:8080/checklist/central/detalleChecklistExpancion.htm?idUsuario=189871&idBitacora="+idBitacora+"&tipo=6'>Ver Detalles</a>"+
				
				" </div>"+
			  
			  "</TD>" +
			  
			 "</TR>"+
			 
			 
			"</TABLE>"+
			
				"<table cellspacing='0' align='right'>"+
				  "<tr align='center'>"+
				    "<td style=' border: 0.25px solid white;'>Código</td>"+
				    "<td style=' border: 0.25px solid white; text-align:center; background-color:black; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>pésimo</td>"+
				    "<td style='border: 0.25px solid white;  text-align:center; background-color:#FE003D; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>en peligro</td>"+
				    "<td style='border: 0.25px solid white;  text-align:center; background-color:#F7CA44; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>alerta</td>"+
				    "<td style='border: 0.25px solid white;  text-align:center; background-color:#006240; color:white; width:60px; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;'>bien</td>"+
				  "</tr>"+
				"</table>"+
			
				"</body>"+"</html>";
				
				*/
		
		
		return html;
	}

}
