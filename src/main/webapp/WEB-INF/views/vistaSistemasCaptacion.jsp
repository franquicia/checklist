<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="es">
<head>



<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />

<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/secciones.css">
<link rel="stylesheet" type="text/css"
	href="../css/supervisionSistemas/calendario.css" media="screen">

<style type="text/css">
#tags {
	width: 100%;
	border: none;
	border-bottom: 1px solid #d7d7d7;
}
</style>

<title>Sistemas Supervisi�n</title>
</head>

<body>

	<div class="page">

		<div class="clear"></div>

		<div class="title" id="title">
			<div class="wrapper">
				<div class="h2">
					<a>P�gina Principal </a> / <a>Supervisi�n</a>
				</div>
				<h1>Supervisi�n</h1>
			</div>
		</div>

		<div class="clear"></div>



		<!-- Contenido -->
		<div class="contHome">
			<div class="titN">
				<table class="tblHeader">
					<tbody>
						<tr>
							<td>&nbsp;</td>
							<c:forEach var="i" begin="0" end="${0}">
								<td>Total visitas programadas:<strong>${10}</strong></td>
						</tr>
						<tr>
							<td>Eligue un sistema para ver su detalle</td>
							<td>Total visitas actuales: <strong>${10}</strong></td>
							</c:forEach>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="clear"></div>

			<br>

			<div class="home">


				<form id="miForm" name="miForm" action="" method="get"
					onsubmit="return prueba(this)">
					<table class="tblBusquedas">
						<tbody>
							<tr>
								<td>Per�odo:</td>
								<td><input type="text" placeholder="--/--/--"
									class="datapicker1 date" id="datepicker2"></td>
								<td><input type="text" placeholder="--/--/--"
									class="datapicker1 date" id="datepicker3"></td>
								<td><select class="normal_select" id="selCheck">
										<option value="">Seleccionar checklist</option>
										<c:forEach var="k" begin="0" end="5">
											<option value="${k}">${k}</option>
										</c:forEach>

								</select></td>
								<td>
									<div class="ui-widget buscar1">
										<input id="tags" class="buscar1">
									</div>
								</td>

							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>Desde</td>
								<td>Hasta</td>
								<td>Cheklist</td>
								<td>Sucursales</td>
							</tr>
						</tbody>
					</table>

					<table style="margin-top: 5%;">
						<tbody>
							<tr>
								<td>
									<div class="ui-widget buscar1">
										<input id="tags" class="buscar1"
											placeholder="No. empleado / Nombre">
									</div>
								</td>
							</tr>
							<tr>
								<td style ="text-align: left; padding: 2px 0px 0px 10px; color: #8a8a8a;">�Qui�n visita sucursal?</td>
							</tr>
						</tbody>
					</table>

					<table style="margin-top: 5%;">
						<tbody>
							<tr>
								<td>
									<div class="ui-widget buscar1">
										<input id="tags" class="buscar1" placeholder="Sucursal">
									</div>
								</td>
							</tr>
							<tr>
								<td style ="text-align: left; padding: 2px 0px 0px 10px; color: #8a8a8a;">�A d�nde va a ir?</td>
							</tr>
						</tbody>
					</table>

					<table style="margin-top: 5%;">
						<tbody>
							<tr>
								<td style ="text-align: left; padding: 2px 0px 0px 0px; color: #8a8a8a;">Fecha:</td>
								<td><input type="text" placeholder="--/--/--"
									class="datapicker1 date" id="datepicker2"></td>
								<td></td>
								<td style ="text-align: left; padding: 2px 0px 0px 0px; color: #8a8a8a;">Hora:</td>
								<!-- <td>
									<input type="text" value="" placeholder="Hora" >
									 <script>
								        $('input').clockpicker();
								    </script>
								</td> -->
							</tr>
						</tbody>
					</table>

					<table style="margin-top: 5%;">
						<tbody>
							<tr>
								<td style ="text-align: left; padding: 2px 0px 0px 0px; color: #8a8a8a;">�Entra a bunker?</td>
								<td style="width: 45%;"><select class="normal_select"
									id="selCheck">
										<option value="0">-</option>
										<option value="1">SI</option>
										<option value="2">NO</option>
								</select></td>
							</tr>
						</tbody>
					</table>

					<table style="margin-top: 5%;">
						<tr>
							<td style ="text-align: left; padding: 2px 0px 0px 0px; color: #8a8a8a;">�A qu� hora va a entrar a bunker?</td>
							<td style="width: 45%;"><select class="normal_select"
								id="selCheck">
									<option value=""></option>
									<c:forEach var="k" begin="0" end="5">
										<option value="${k}">${k}</option>
									</c:forEach>

							</select></td>
						</tr>
					</table>
					
					<table style="margin-top: 5%;">
						<tbody>
							<tr>
							<tr>
								<td>
									<div class="ui-widget buscar1">
										<input id="tags" class="buscar1">
									</div>
								</td>
							</tr>
							<tr>
								<td style ="text-align: left; padding: 2px 0px 0px 10px; color: #8a8a8a;">Justificaci�n</td>
							</tr>
							</tr>
						</tbody>
					</table>

					<table style="margin: auto; margin-top: 5%; margin-bottom: 10%">
						<tbody>
							<tr>
								<td rowspan="2"><input type="submit" value=""
									class="btnbuscarG" /></td>
							</tr>
						</tbody>




					</table>




				</form>




				



			</div>

			<div class="clear"></div>

		</div>

		<div class="clear"></div>

		<div class="footer1">
			<table class="tblFooter1">
				<tbody>
					<tr>
						<td>Sistema Reportes</td>
						<td><a class="btnRojo">Malas Pr�cticas</a></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="footer">
			<table class="tblFooter">
				<tbody>
					<tr>
						<td>Banco Azteca S.A. Instituci�n de Banca M�ltiple</td>
						<td>Derechos Reservados 2014 (T�rminos y Condiciones de uso
							del Portal).</td>
					</tr>
				</tbody>
			</table>
		</div>

	</div>
</body>
</html>

