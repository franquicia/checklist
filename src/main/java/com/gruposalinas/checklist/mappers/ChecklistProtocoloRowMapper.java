package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gruposalinas.checklist.domain.ChecklistProtocoloDTO;
import com.gruposalinas.checklist.domain.TipoChecklistDTO;
public class ChecklistProtocoloRowMapper implements RowMapper<ChecklistProtocoloDTO> {
	
	public ChecklistProtocoloDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		ChecklistProtocoloDTO checklistProtocoloDTO = new ChecklistProtocoloDTO();
		TipoChecklistDTO tipochecklist = new TipoChecklistDTO(); 
		
		checklistProtocoloDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
		tipochecklist.setIdTipoCheck(rs.getInt("FIID_TIPO_CHECK"));
		checklistProtocoloDTO.setNombreCheck(rs.getString("FCNOMBRE"));
		checklistProtocoloDTO.setIdHorario(rs.getInt("FIID_HORARIO"));
		checklistProtocoloDTO.setVigente(rs.getInt("FIVIGENTE"));
		checklistProtocoloDTO.setFecha_inicio(rs.getString("FDFECHA_INICIO"));
		checklistProtocoloDTO.setFecha_fin(rs.getString("FDFECHA_FIN"));
		checklistProtocoloDTO.setIdEstado(rs.getInt("FIID_ESTADO"));
		checklistProtocoloDTO.setDia(rs.getString("FCDIA"));
		checklistProtocoloDTO.setPeriodo(rs.getString("FCPERIODO"));
		checklistProtocoloDTO.setVersion(rs.getString("FIVERSION"));
		checklistProtocoloDTO.setOrdenGrupo(rs.getString("FIORDEN_GRUPO"));
		checklistProtocoloDTO.setPonderacionTot(rs.getInt("FCPONDTOTAL"));
		checklistProtocoloDTO.setClasifica(rs.getString("FCCLASIFICA"));
		checklistProtocoloDTO.setIdProtocolo(rs.getString("FIID_PROTOCOLO"));
		checklistProtocoloDTO.setNegocio(rs.getString("FCNEGOCIO"));
		
		return checklistProtocoloDTO;
	}

}
