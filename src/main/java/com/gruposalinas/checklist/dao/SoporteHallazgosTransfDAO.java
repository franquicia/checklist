package com.gruposalinas.checklist.dao;

import java.util.List;
import java.util.Map;

import com.gruposalinas.checklist.domain.HallazgosTransfDTO;


public interface SoporteHallazgosTransfDAO {

		public List<HallazgosTransfDTO> obtieneHallazgosSoporte(String ceco,String fase,String proyecto) throws Exception; 

		public boolean actualizaFaseHallazgos(HallazgosTransfDTO bean)throws Exception;
		
		public boolean eliminaHallazgosEHistorico(String bita,String ceco)throws Exception;
		
		// TRAE SOFT CECO FASE PROYECTO
		public Map<String, Object> obtieneOrdenFaseFirmas(String ceco,String fase,String proyecto) throws Exception;
		
		public List<HallazgosTransfDTO> obtieneAdicionalesCecoFaseProyecto(String ceco,String fase,String proyecto, int parametro) throws Exception; 
		
		
	}