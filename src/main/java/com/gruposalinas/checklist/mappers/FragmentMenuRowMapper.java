package com.gruposalinas.checklist.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.gruposalinas.checklist.domain.FragmentMenuDTO;

public class FragmentMenuRowMapper implements RowMapper<FragmentMenuDTO> {

	public FragmentMenuDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		FragmentMenuDTO FragmentMenuDTO = new FragmentMenuDTO();
		
		FragmentMenuDTO.setIdConfigMenu(rs.getInt("FIID_CONFIG"));
	    FragmentMenuDTO.setIdPerfil(rs.getInt("FIIDPERFIL"));
		FragmentMenuDTO.setTituloMenu(rs.getString("FCTITULOMENU"));
	    FragmentMenuDTO.setOrdenMenu(rs.getInt("FIID_ORDENMENU"));
		FragmentMenuDTO.setAux(rs.getString("FCAUX"));
		FragmentMenuDTO.setPeriodo(rs.getString("FCPERIODO"));
		
		return FragmentMenuDTO;
	}

}


